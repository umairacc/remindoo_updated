<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_chat extends CI_Controller
{
    public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Common_mdl', 'Security_model'));
        $this->load->library('Excel');
        // $this->load->library('encrypt');
        $this->load->library('encryption');
    }

    public function index($dash_id = 0)
    {
        if (isset($_COOKIE['remindoo_logout']) && $_COOKIE['remindoo_logout'] == 'remindoo_superadmin') {
            $this->Loginchk_model->superAdmin_LoginCheck();
        } else if ($_SESSION['user_type'] == 'FC') {
            $this->Security_model->check_login();
        } else {
            $this->Security_model->chk_login();
        }
        $data['dash_id'] = $dash_id;

        $login_user_id = $_SESSION['userId'];
        if ($_SESSION['user_type'] == 'FU' || $_SESSION['user_type'] == 'FA') {

            $client_data = $this->db->query("SELECT * FROM user where user_type='FU' AND autosave_status!='1' and crm_name!='' and id=" . $login_user_id . " order by id DESC")->result_array();

            $data['getalladmin'] = $this->db->query("SELECT * FROM user where crm_name!='' and user_type='FA' and firm_id=" . $_SESSION['firm_id'] . " order by id DESC")->result_array();
            $user_val = array('-1');

            $ques = Get_Assigned_Datas('CLIENT');

            $ques = !empty($ques) ? implode(',', $ques) : "0";

            $task_user_val = $this->db->query("SELECT * FROM client where id in (" . $ques . ")  order by id DESC")->result_array();
            $task_user_val = array_column($task_user_val, 'user_id');

            $data['getallstaff'] = array();
            $data['getallUser'] = array();

            $newarray = array_values(array_unique($task_user_val));
            if (!empty($newarray)) {
                $last_chat_array = $firm_staff = $firm_user_id = array();

                $chat_count = $this->db->query("SELECT chat_from FROM `user_chat` where chat_to=" . $login_user_id . " and status='not_viewed'   GROUP BY chat_from order by date_time; ")->result_array();

                $get_firm_user = $this->db->query("SELECT * FROM user where  firm_id='" . $_SESSION['firm_id'] . "' and user_type!='FC' and user_type!='SA' order by id DESC")->result_array();

                $last_chat_array = array_column($chat_count, 'chat_from');
                $firm_staff = array_column($get_firm_user, 'id');
                $firm_user_id = array_merge($newarray, $firm_staff);
                $intersect_array = array_intersect($last_chat_array, $firm_user_id);
                foreach ($intersect_array as $key => $value) {
                    array_unshift($firm_user_id, $value);
                }

                $newarray = array_values(array_unique($firm_user_id));

                $data['getallUser'] = $this->db->query("SELECT * FROM user where id in (" . implode(',', $newarray) . ") and firm_id='" . $_SESSION['firm_id'] . "' and user_type='FC' ")->result_array();

                $data['getallstaff'] = $this->db->query("SELECT * FROM user where id in (" . implode(',', $newarray) . ") and firm_id='" . $_SESSION['firm_id'] . "' and user_type!='FC' and user_type!='SA' ")->result_array();

                if ($_SESSION['user_type'] == 'FA') {

                    $data['getallUser'] = $this->db->query("SELECT * FROM user  where firm_id='" . $_SESSION['firm_id'] . "' and user_type='FC' order by id DESC")->result_array();

                    $data_superadmin = $this->db->query("SELECT * FROM user where id ='1' and user_type!='FC' order by id DESC")->row_array();

                    $data['getallstaff'][] = $data_superadmin;
                }
            }
        } else {
            $data['getallstaff'] = array();
            $data['getallUser'] = array();
            $data['getallstaff'] = $this->db->query("SELECT * FROM user where  firm_id='" . $_SESSION['firm_id'] . "' and user_type!='FC' and user_type!='SA' order by id DESC")->result_array();
            $data['getalladmin'] = $this->db->query("SELECT * FROM user where crm_name!='' and user_type='FA' and firm_id=" . $_SESSION['firm_id'] . " order by id DESC")->result_array();
            $data['getallUser'] = $this->db->query("SELECT * FROM user where id='" . $login_user_id . "'  and firm_id='" . $_SESSION['firm_id'] . "' and user_type='FC' order by id DESC")->result_array();
        }

        $data['column_setting'] = $this->Common_mdl->getallrecords('column_setting_new');
        $data['client'] = $this->db->query('select * from column_setting_new where id=1')->row_array();
        $data['feedback'] = $this->db->query("select id,username,crm_email_id from user where username!='' and username!='0' and  crm_email_id!='' and crm_email_id!='0' ")->result();
        
        if ($_SESSION['permission']['Chat']['view'] == 1) {
            $this->load->view('User_chat/new_chat_page', $data);
        } else {
            $this->load->view('users/blank_page', $data);
        }
    }

    public function insert_chat()
    {
        $data['chat_to'] = $_GET['chat_to'];
        $data['message'] = $_GET['message'];
        $data['chat_from'] = $_SESSION['userId'];
        $data['date_time'] = time();
        $data['status'] = "not_viewed";

        //echo $chat_to."--".$chat_from."--".$message."--".$date_time."--".$status;
        $result = $this->Common_mdl->insert('user_chat', $data);
        echo $result;
        // return $this->db->insert_id();      

    }

    public function get_chat()
    {
        $login_user_id = $_SESSION['userId'];
        $chat_id = $_GET['chat_id'];
        $condition = $_GET['condition'];

        if ($condition == 'yes') {

            $for_lst_id = $_GET['lst_id'];
            // $query=$this->db->query("SELECT * FROM `user_chat` where chat_from in (".$chat_id.",".$login_user_id.") and chat_to in (".$chat_id.",".$login_user_id.") and status='not_viewed' and id > ".$for_lst_id." order by date_time limit 1");
            $query = $this->db->query("SELECT * FROM `user_chat` where chat_from in (" . $chat_id . "," . $login_user_id . ") and chat_to in (" . $chat_id . "," . $login_user_id . ") and status='not_viewed' and id = " . $for_lst_id . " order by date_time limit 1");

            // $query=$this->db->query("SELECT * FROM `user_chat` where chat_from in (".$chat_id.",".$login_user_id.") and chat_to in (".$chat_id.",".$login_user_id.") and status='not_viewed' order by date_time");
        } else {
            // $query=$this->db->query("SELECT * FROM `user_chat` where chat_from in (".$chat_id.",".$login_user_id.") and chat_to in (".$chat_id.",".$login_user_id.") and status='viewed' order by date_time");
            $query = $this->db->query("SELECT * FROM `user_chat` where chat_from in (" . $chat_id . "," . $login_user_id . ") and chat_to in (" . $chat_id . "," . $login_user_id . ") order by date_time");
        }


        $data['chat_messages'] = $query->result_array();
        $this->load->view('User_chat/chat_page_chatlist', $data);
    }

    public function chat_status_change()
    {
        $chat_id = $_GET['chat_id'];
        $login_user_id = $_SESSION['userId'];
        $this->db->set('status', 'viewed');
        $this->db->where('chat_from', $chat_id);
        $this->db->where('chat_to', $login_user_id);

        $this->db->update('user_chat');
    }

    public function get_chat_ajax()
    {
        $login_user_id = $_SESSION['userId'];
        $query = $this->db->query("SELECT * FROM `user_chat` where chat_to=" . $login_user_id . " and status='not_viewed'  order by date_time");
        $result = $query->result_array();
        echo json_encode($result);
    }

    public function get_status_online()
    {
        $data_result = array();
        // print_r($_GET);
        foreach (json_decode($_GET['id'], true) as $key => $value) {
            # code...

            //echo $value;
            $id = $value;

            $user_login = $this->db->query("SELECT CreatedTime FROM activity_log where user_id=" . $id . " AND module='Login'  order by id DESC")->result_array();
            if (!empty($user_login)) {
                $login_time = $user_login[0]['CreatedTime'];
                $user_logout = $this->db->query("SELECT CreatedTime FROM activity_log where user_id=" . $id . " AND module='Logout'  order by id DESC")->result_array();
                $logout_time = $user_logout[0]['CreatedTime'];
                $status = "";
                if ($login_time > $logout_time) {
                    if (date('Y-m-d', $login_time) == date('Y-m-d')) {
                        $status = "online";
                        $status_color = "bg-success";
                    } else {
                        $status = "offline";
                        $status_color = "bg-danger";
                    }
                } else {
                    $status = "offline";
                    $status_color = "bg-danger";
                }
            } else {
                $status = "offline";
                $status_color = "bg-danger";
            }
            $data_result[$key]['id'] = $id;
            $data_result[$key]['status'] = $status;
        }

        echo json_encode($data_result);
    }

    public function check_get_chat_ajax()
    {
        $login_user_id = $_SESSION['userId'];

        $query = $this->db->query("SELECT * FROM `user_chat` where chat_from=" . $login_user_id . " and chat_to=" . $_GET['chat_to'] . " and id > " . $_GET['id'] . "  order by id DESC");
        $result = $query->result_array();
        // echo "SELECT * FROM `user_chat` where chat_from=".$login_user_id." and chat_to=".$_POST['chat_to']." and id > ".$_POST['id']."  order by id DESC";
        $res = array();
        foreach ($result as $key => $value) {
            array_push($res, $value['id']);
        }
        echo implode(',', $res);
    }

    public function get_chat_forone()
    {

        $query = $this->db->query("SELECT * FROM `user_chat` where id in (" . $_GET['table_id'] . ")");
        $data['chat_messages'] = $query->result_array();
        $this->load->view('User_chat/chat_page_chatlist', $data);
    }

    public function chat_page_new()
    {
        if ($_SESSION['roleId'] == 1) {
            $login_user_id = $_SESSION['userId'];


            $data['getallUser'] = $this->db->query("SELECT * FROM user where role='4' AND autosave_status!='1' and crm_name!='' and firm_admin_id=" . $login_user_id . " order by id DESC")->result_array();
        }
        if ($_SESSION['roleId'] == 4) {
            $login_user_id = $_SESSION['userId'];
            $client_data = $this->db->query("SELECT * FROM user where role='4' AND autosave_status!='1' and crm_name!='' and id=" . $login_user_id . " order by id DESC")->result_array();

            $firm_id = $client_data[0]['firm_admin_id'];

            $data['getallUser'] = $this->db->query("SELECT * FROM user where crm_name!='' and id=" . $firm_id . " order by id DESC")->result_array();
        }
        $data['column_setting'] = $this->Common_mdl->getallrecords('column_setting_new');
        $data['client'] = $this->db->query('select * from column_setting_new where id=1')->row_array();
        $data['feedback'] = $this->db->query("select id,username,crm_email_id from user where username!='' and username!='0' and  crm_email_id!='' and crm_email_id!='0' ")->result();

        $this->load->view('User_chat/chat_page_new', $data);
    }

    public function create_group()
    {
        $member = $_POST['member'];
        $name = $_POST['name'];
        $data['groupname'] = $name;
        $data['members'] = $member . "," . $_SESSION['id'];
        $data['firm_id'] = $_SESSION['firm_id'];
        $data['create_by'] = $_SESSION['id'];
        $data['createdTime'] = time();
        $result = $this->Common_mdl->insert('chat_group', $data);
        $insert_id = $result;
        if ($result > 0) {
            $group_member_data = $this->db->query("SELECT * FROM user where id in (" . $member . "," . $_SESSION['id'] . ") ")->result_array();
            $member_names = array();
            foreach ($group_member_data as $men_key => $mem_value) {
                array_push($member_names, $mem_value['crm_name']);
            }
        ?>
            <div class="media userlist-box box_<?php echo $insert_id; ?>" data-id="<?php echo $insert_id; ?>" data-username="<?php echo $name; ?>" data-toggle="tooltip" data-placement="left" title="<?php echo $name; ?>" data-group="group" data-member="<?php echo implode(',', $member_names); ?>" data-creater="<?php echo $_SESSION['id']; ?>" data-session="<?php echo $_SESSION['id']; ?>">

                <div class="media-body">
                    <div class="f-13 chat-header"><?php echo $name; ?></div>
                </div>
                <div class="badge badge-primary new_message_count_<?php echo $insert_id; ?>"></div>
            </div>
            <?php
        } else {
            echo 'wrong';
        }
    }

    public function update_group($id)
    {
        $data['groupname'] = $_POST['name'];
        $data['members'] = $_POST['member'];
        $data['firm_id'] = $_SESSION['firm_id'];
        $this->Common_mdl->update('chat_group', $data, 'id', $id);
        $get_group_data = $this->Common_mdl->GetAllWithWhere('chat_group', 'id', $id);
        $members = $get_group_data[0]['members'];
        $group_member_data = $this->db->query("SELECT * FROM user where id in (" . $get_group_data[0]['members'] . "," . $get_group_data[0]['create_by'] . ") ")->result_array();
        $member_names = array();
        foreach ($group_member_data as $men_key => $mem_value) {
            array_push($member_names, $mem_value['crm_name']);
        }
        echo implode(',', $member_names);
        //echo "success";

    }

    /** for group chat messages handle **/
    public function insert_group_chat()
    {
        $data['group_id'] = $_GET['chat_to'];
        $data['message'] = $_GET['message'];
        $data['chat_from'] = $_SESSION['id'];
        $get_group_data = $this->Common_mdl->GetAllWithWhere('chat_group', 'id', $_GET['chat_to']);



        if (count($get_group_data) > 0) {
            $members = $get_group_data[0]['members'];
            $create_by = $get_group_data[0]['create_by'];
            $common_members = $members . "," . $create_by;
            $ex_members = explode(',', $common_members);
            if (($key = array_search($_SESSION['id'], $ex_members)) !== false) {
                unset($ex_members[$key]);
            }
            $data['viewed'] = '';
            $data['not_viewed'] = implode(',', $ex_members);
        }
        $data['date_time'] = time();
        //echo $chat_to."--".$chat_from."--".$message."--".$date_time."--".$status;
        $result = $this->Common_mdl->insert('group_chat_messages', $data);
        echo $result;
        // return $this->db->insert_id();     

    }

    public function get_group_chat()
    {
        $login_user_id = $_SESSION['userId'];
        $group_id = $_GET['chat_id'];
        $condition = $_GET['condition'];

        if ($condition == 'yes') {
            $ids = $_GET['ids'];
            $for = $_GET['for'];
            if ($for == 'left') {
                $ex_ids = explode(',', $ids);
                $mul_id = end($ex_ids);

                $query = $this->db->query("SELECT * FROM `group_chat_messages` where group_id=" . $group_id . " and find_in_set('" . $_SESSION['id'] . "',not_viewed) and id >" . $mul_id . " order by date_time");
                // $query=$this->db->query("SELECT * FROM `group_chat_messages` where group_id=".$group_id." order by date_time");
            } else {
                $ex_ids = explode(',', $ids);
                $mul_id = end($ex_ids);
                $query = $this->db->query("SELECT * FROM `group_chat_messages` where group_id=" . $group_id . " and chat_from=" . $_SESSION['id'] . " and id >" . $mul_id . " order by date_time");
                //   echo "SELECT * FROM `group_chat_messages` where group_id=".$group_id." and chat_from=".$_SESSION['id']." and id >".$mul_id." order by date_time";
            }
        } else {
            $query = $this->db->query("SELECT * FROM `group_chat_messages` where group_id=" . $group_id . " order by date_time");
        }
        $res = $query->result_array();
        if (count($res) > 0) {
            $data['chat_messages'] = $query->result_array();
            $this->load->view('User_chat_chart/chat_page_chatlist_group', $data);
        } else {
            echo 'null';
        }
    }

    public function get_group_chat_unread()
    {
        $data_res = array();
        foreach (json_decode($_GET['chat_id'], true) as $key => $value) {
            # code...
            $group_id = $value;
            $query = $this->db->query("SELECT id FROM `group_chat_messages` where group_id=" . $group_id . " and find_in_set('" . $_SESSION['id'] . "',not_viewed) order by date_time");
            $res = $query->result_array();
            $data_res[$key]['chat_id'] = $group_id;
            $data_res[$key]['count'] = count($res);
        }
        echo json_encode($data_res);
    }
    public function chat_status_change_forgroup()
    {
        $group_id = $_GET['chat_id'];
        $query = $this->db->query("SELECT * FROM `group_chat_messages` where group_id=" . $group_id . " order by date_time");
        $group_data = $query->result_array();
        if (count($group_data) > 0) {
            foreach ($group_data as $key => $value) {
                $viewed = $value['viewed'];
                $not_viewed = $value['not_viewed'];
                if ($viewed != '') {
                    $ex_view = explode(',', $viewed);
                } else {
                    $ex_view = array();
                }

                $ex_not = explode(',', $not_viewed);
                if (($key = array_search($_SESSION['id'], $ex_not)) !== false) {
                    unset($ex_not[$key]);
                }
                if (!in_array($ex_view, $_SESSION['id'])) {
                    array_push($ex_view, $_SESSION['id']);
                }

                $data['viewed'] = implode(',', array_unique($ex_view));
                $data['not_viewed'] = implode(',', array_unique($ex_not));
                $this->Common_mdl->update('group_chat_messages', $data, 'id', $value['id']);
            }
        }
    }

    public function delete_group_chat($id)
    {
        //$this->Common_mdl->delete('chat_group','id',$id);
        $data['status'] = 'delete';
        $this->Common_mdl->update('chat_group', $data, 'id', $id);
    }
    /** end of group msg **/
    public function group_chat_lists()
    {

        if ($_SESSION['roleId'] == 1) {
            $login_user_id = $_SESSION['userId'];


            $data['getallUser'] = $this->db->query("SELECT * FROM user where role='4' AND autosave_status!='1' and crm_name!='' and firm_admin_id=" . $login_user_id . " order by id DESC")->result_array();

            $user_staff_data = $this->db->query("SELECT * FROM user where role='6' AND autosave_status!='1' and crm_name!='' and firm_admin_id=" . $login_user_id . " order by id DESC")->result_array();

            $leads_data = $this->db->query("SELECT * FROM leads where user_id=" . $login_user_id . " order by id DESC")->result_array();
            $taskdata = $this->db->query("SELECT * FROM add_new_task where create_by=" . $login_user_id . " order by id DESC")->result_array();
            $assign = '';
            $worker = '';
            $user_staff = array();
            foreach ($leads_data as $leads_key => $leads_value) {
                if ($leads_value['assigned'] != '') {
                    $assign .= $leads_value['assigned'] . ",";
                }
            }
            foreach ($taskdata as $task_key => $task_value) {
                if ($task_value['worker'] != '') {
                    $worker .= $task_value['worker'] . ",";
                }
            }
            foreach ($user_staff_data as $user_staff_key => $user_staff_value) {
                //user_staff
                array_push($user_staff, $user_staff_value['id']);
            }
            $leadstask = array_values(array_unique(array_merge(array_filter(explode(',', $assign)), array_filter(explode(',', $worker)))));
            $newarray = array_values(array_unique(array_merge(array_filter($leadstask), array_filter($user_staff))));

            if (!empty($newarray)) {
                $data['getallstaff'] = $this->db->query("SELECT * FROM user where role='6' AND autosave_status!='1' and crm_name!='' and id in (" . implode(',', $newarray) . ") order by id DESC")->result_array();
            }

            // echo "SELECT * FROM user where role='6' AND autosave_status!='1' and crm_name!='' and id in (".implode(',',$newarray).") order by id DESC";

        }
        if ($_SESSION['roleId'] == 4) {
            $login_user_id = $_SESSION['userId'];
            $client_data = $this->db->query("SELECT * FROM user where role='4' AND autosave_status!='1' and crm_name!='' and id=" . $login_user_id . " order by id DESC")->result_array();

            $firm_id = $client_data[0]['firm_admin_id'];

            $data['getalladmin'] = $this->db->query("SELECT * FROM user where crm_name!='' and id=" . $firm_id . " order by id DESC")->result_array();

            /** extra added for staff shown **/
            $data['getallUser'] = $this->db->query("SELECT * FROM user where role='4' AND autosave_status!='1' and crm_name!='' and firm_admin_id=" . $login_user_id . " order by id DESC")->result_array();

            $user_staff_data = $this->db->query("SELECT * FROM user where role='6' AND autosave_status!='1' and crm_name!='' and firm_admin_id=" . $login_user_id . " order by id DESC")->result_array();

            $leads_data = $this->db->query("SELECT * FROM leads where user_id=" . $login_user_id . " order by id DESC")->result_array();
            $taskdata = $this->db->query("SELECT * FROM add_new_task where create_by=" . $login_user_id . " order by id DESC")->result_array();
            $assign = '';
            $worker = '';
            $user_staff = array();
            foreach ($leads_data as $leads_key => $leads_value) {
                if ($leads_value['assigned'] != '') {
                    $assign .= $leads_value['assigned'] . ",";
                }
            }
            foreach ($taskdata as $task_key => $task_value) {
                if ($task_value['worker'] != '') {
                    $worker .= $task_value['worker'] . ",";
                }
            }
            foreach ($user_staff_data as $user_staff_key => $user_staff_value) {
                //user_staff
                array_push($user_staff, $user_staff_value['id']);
            }
            $leadstask = array_values(array_unique(array_merge(array_filter(explode(',', $assign)), array_filter(explode(',', $worker)))));
            $newarray = array_values(array_unique(array_merge(array_filter($leadstask), array_filter($user_staff))));

            if (!empty($newarray)) {
                $data['getallstaff'] = $this->db->query("SELECT * FROM user where role='6' AND autosave_status!='1' and crm_name!='' and id in (" . implode(',', $newarray) . ") order by id DESC")->result_array();
                /** for staff end **/
            } else {
                $data['getallstaff'] = '';
            }
        }

        if ($_SESSION['roleId'] == 6) {
            $login_user_id = $_SESSION['userId'];
            $client_data = $this->db->query("SELECT * FROM user where role='6' AND autosave_status!='1' and crm_name!='' and id=" . $login_user_id . " order by id DESC")->result_array();

            $firm_id = $client_data[0]['firm_admin_id'];

            $data['getalladmin'] = $this->db->query("SELECT * FROM user where crm_name!='' and id=" . $firm_id . " order by id DESC")->result_array();

            $leads_data = $this->db->query("SELECT * FROM leads where find_in_set('" . $login_user_id . "',assigned)  order by id DESC")->result_array();
            $taskdata = $this->db->query("SELECT * FROM add_new_task where find_in_set('" . $login_user_id . "',worker) order by id DESC")->result_array();
            $for_assigned = array();
            foreach ($leads_data as $leads_key => $leads_value) {
                array_push($for_assigned, $leads_value['user_id']);
            }
            foreach ($taskdata as $task_key => $task_value) {
                array_push($for_assigned, $task_value['create_by']);
            }
            $newarray = array_values(array_unique($for_assigned));
            if (!empty($newarray)) {
                $data['getallUser'] = $this->db->query("SELECT * FROM user where id in (" . implode(',', $newarray) . ")  order by id DESC")->result_array();
            } else {
                $data['getallUser'] = '';
            }
        }
        $data['column_setting'] = $this->Common_mdl->getallrecords('column_setting_new');
        $data['client'] = $this->db->query('select * from column_setting_new where id=1')->row_array();
        $data['feedback'] = $this->db->query("select id,username,crm_email_id from user where username!='' and username!='0' and  crm_email_id!='' and crm_email_id!='0' ")->result();

        $this->load->view('User_chat/group_chat_list', $data);
    }

    public function edit_chatgroup_popup()
    {
        if ($_SESSION['roleId'] == 1) {
            $login_user_id = $_SESSION['userId'];


            $data['getallUser'] = $this->db->query("SELECT * FROM user where role='4' AND autosave_status!='1' and crm_name!='' and firm_admin_id=" . $login_user_id . " order by id DESC")->result_array();

            $user_staff_data = $this->db->query("SELECT * FROM user where role='6' AND autosave_status!='1' and crm_name!='' and firm_admin_id=" . $login_user_id . " order by id DESC")->result_array();

            $leads_data = $this->db->query("SELECT * FROM leads where user_id=" . $login_user_id . " order by id DESC")->result_array();
            $taskdata = $this->db->query("SELECT * FROM add_new_task where create_by=" . $login_user_id . " order by id DESC")->result_array();
            $assign = '';
            $worker = '';
            $user_staff = array();
            foreach ($leads_data as $leads_key => $leads_value) {
                if ($leads_value['assigned'] != '') {
                    $assign .= $leads_value['assigned'] . ",";
                }
            }
            foreach ($taskdata as $task_key => $task_value) {
                if ($task_value['worker'] != '') {
                    $worker .= $task_value['worker'] . ",";
                }
            }
            foreach ($user_staff_data as $user_staff_key => $user_staff_value) {
                //user_staff
                array_push($user_staff, $user_staff_value['id']);
            }
            $leadstask = array_values(array_unique(array_merge(array_filter(explode(',', $assign)), array_filter(explode(',', $worker)))));
            $newarray = array_values(array_unique(array_merge(array_filter($leadstask), array_filter($user_staff))));

            if (!empty($newarray)) {
                $data['getallstaff'] = $this->db->query("SELECT * FROM user where role='6' AND autosave_status!='1' and crm_name!='' and id in (" . implode(',', $newarray) . ") order by id DESC")->result_array();
            }

            // echo "SELECT * FROM user where role='6' AND autosave_status!='1' and crm_name!='' and id in (".implode(',',$newarray).") order by id DESC";

        }
        if ($_SESSION['roleId'] == 4) {
            $login_user_id = $_SESSION['userId'];
            $client_data = $this->db->query("SELECT * FROM user where role='4' AND autosave_status!='1' and crm_name!='' and id=" . $login_user_id . " order by id DESC")->result_array();

            $firm_id = $client_data[0]['firm_admin_id'];

            $data['getalladmin'] = $this->db->query("SELECT * FROM user where crm_name!='' and id=" . $firm_id . " order by id DESC")->result_array();

            /** extra added for staff shown **/
            $data['getallUser'] = $this->db->query("SELECT * FROM user where role='4' AND autosave_status!='1' and crm_name!='' and firm_admin_id=" . $login_user_id . " order by id DESC")->result_array();

            $user_staff_data = $this->db->query("SELECT * FROM user where role='6' AND autosave_status!='1' and crm_name!='' and firm_admin_id=" . $login_user_id . " order by id DESC")->result_array();

            $leads_data = $this->db->query("SELECT * FROM leads where user_id=" . $login_user_id . " order by id DESC")->result_array();
            $taskdata = $this->db->query("SELECT * FROM add_new_task where create_by=" . $login_user_id . " order by id DESC")->result_array();
            $assign = '';
            $worker = '';
            $user_staff = array();
            foreach ($leads_data as $leads_key => $leads_value) {
                if ($leads_value['assigned'] != '') {
                    $assign .= $leads_value['assigned'] . ",";
                }
            }
            foreach ($taskdata as $task_key => $task_value) {
                if ($task_value['worker'] != '') {
                    $worker .= $task_value['worker'] . ",";
                }
            }
            foreach ($user_staff_data as $user_staff_key => $user_staff_value) {
                //user_staff
                array_push($user_staff, $user_staff_value['id']);
            }
            $leadstask = array_values(array_unique(array_merge(array_filter(explode(',', $assign)), array_filter(explode(',', $worker)))));
            $newarray = array_values(array_unique(array_merge(array_filter($leadstask), array_filter($user_staff))));

            if (!empty($newarray)) {
                $data['getallstaff'] = $this->db->query("SELECT * FROM user where role='6' AND autosave_status!='1' and crm_name!='' and id in (" . implode(',', $newarray) . ") order by id DESC")->result_array();
                /** for staff end **/
            } else {
                $data['getallstaff'] = '';
            }
        }

        if ($_SESSION['roleId'] == 6) {
            $login_user_id = $_SESSION['userId'];
            $client_data = $this->db->query("SELECT * FROM user where role='6' AND autosave_status!='1' and crm_name!='' and id=" . $login_user_id . " order by id DESC")->result_array();

            $firm_id = $client_data[0]['firm_admin_id'];

            $data['getalladmin'] = $this->db->query("SELECT * FROM user where crm_name!='' and id=" . $firm_id . " order by id DESC")->result_array();

            $leads_data = $this->db->query("SELECT * FROM leads where find_in_set('" . $login_user_id . "',assigned)  order by id DESC")->result_array();
            $taskdata = $this->db->query("SELECT * FROM add_new_task where find_in_set('" . $login_user_id . "',worker) order by id DESC")->result_array();
            $for_assigned = array();
            foreach ($leads_data as $leads_key => $leads_value) {
                array_push($for_assigned, $leads_value['user_id']);
            }
            foreach ($taskdata as $task_key => $task_value) {
                array_push($for_assigned, $task_value['create_by']);
            }
            $newarray = array_values(array_unique($for_assigned));
            if (!empty($newarray)) {
                $data['getallUser'] = $this->db->query("SELECT * FROM user where id in (" . implode(',', $newarray) . ")  order by id DESC")->result_array();
            } else {
                $data['getallUser'] = '';
            }
        }
        $this->load->view('User_chat/edit_chatgroup_popup', $data);
    }

    public function check_group_delete()
    {
        $group_id = $_GET['group_id'];
        $get_group_data = $this->db->query("SELECT id FROM chat_group where id=" . $group_id . " and status='delete' ")->result_array();
        // echo "SELECT * FROM chat_group where id=".$group_id." and status='delete' ";
        echo count($get_group_data);
    }

    public function check_group_added()
    {

        $group_ids = explode(',', $_GET['group_ids']);
        $get_group_data = $this->db->query("SELECT id FROM chat_group where find_in_set('" . $_SESSION['id'] . "',members)  ")->result_array();
        $db_group_ids = array();
        //  echo "SELECT * FROM chat_group where find_in_set('".$_SESSION['id']."',members)  ";
        foreach ($get_group_data as $key => $value) {
            array_push($db_group_ids, $value['id']);
        }
        if (!empty($db_group_ids)) {
            $result = array_diff($db_group_ids, $group_ids);
            if (count($result) > 0) {
                echo implode(',', $result);
            } else {
                echo "wrong";
            }
        } else {
            echo 'wrong';
        }
    }

    public function check_group_update_members()
    {
        $group_id = $_GET['group_id'];
        $group_members = $_GET['group_members'];
        $get_group_data = $this->Common_mdl->GetAllWithWhere('chat_group', 'id', $group_id);
        $members = $get_group_data[0]['members'];
        $group_member_data = $this->db->query("SELECT crm_name FROM user where id in (" . $get_group_data[0]['members'] . "," . $get_group_data[0]['create_by'] . ") ")->result_array();
        $member_names = array();
        foreach ($group_member_data as $men_key => $mem_value) {
            array_push($member_names, $mem_value['crm_name']);
        }
        //  echo implode(',', $member_names);
        if (count($member_names) > count(explode(',', $group_members))) {
            $result = array_diff($member_names, explode(',', $group_members));
        } else {
            $result = array_diff(explode(',', $group_members), $member_names);
        }

        // echo json_encode($group_members);
        // echo json_encode($member_names);
        if (count($result) > 0) {
            echo implode(',', $member_names);
        } else {
            echo "wrong";
        }
    }

    /** 27-12-2018 rspt new **/

    public function get_status_online_rspt()
    {
        $data_res = array();
        $res_ids = array_filter(explode(',', $_GET['id']));
        $get_id = implode(',', array_filter(explode(',', $_GET['id'])));
        if (count($get_id) > 0) {
            $user_login = $this->db->query("SELECT id,user_id,CreatedTime FROM activity_log where user_id in (" . $get_id . ") AND module='Login'  order by id DESC")->result_array();

            $user_logout = $this->db->query("SELECT id,user_id,CreatedTime FROM activity_log where user_id in (" . $get_id . ") AND module='Logout'  order by id DESC")->result_array();
            $login_array = array();
            $logout_array = array();

            if (count($user_login) > 0) {
                foreach ($user_login as $log_key => $log_value) {
                    $login_array[$log_value['id']][$log_value['user_id']] = $log_value['CreatedTime'];
                }
            }

            if (count($user_logout) > 0) {
                foreach ($user_logout as $logout_key => $logout_value) {
                    $logout_array[$logout_value['id']][$logout_value['user_id']] = $logout_value['CreatedTime'];
                }
            }

            if (count($res_ids) > 0) {
                foreach ($res_ids as $ids_key => $ids_value) {

                    $cond_in_arry = array_column($login_array, (int)$ids_value);
                    if (count($cond_in_arry) > 0) {
                        $max_login = max(array_column($login_array, (int)$ids_value));
                    } else {
                        $max_login = 0;
                    }

                    $cond_out_arry = array_column($logout_array, (int)$ids_value);
                    if (count($cond_out_arry) > 0) {
                        $max_logout = max(array_column($logout_array, (int)$ids_value));
                    } else {
                        $max_logout = 0;
                    }
                    /** condition **/
                    $status = '';
                    if ($max_login > $max_logout) {

                        if (date('Y-m-d', $max_login) == date('Y-m-d')) {
                            $status = "online";
                            $status_color = "bg-success";
                        } else {
                            $status = "offline";
                            $status_color = "bg-danger";
                        }
                    } else {
                        $status = "offline";
                        $status_color = "bg-danger";
                    }
                    $data_res[(int)$ids_value] = $status;
                    /** end of condition **/
                }
                echo json_encode($data_res);
            } else {
                echo 0;
            }
        }
    }

    /** en dof rspt 27-12-2018 **/
    /** for chart data 28-12-2018 **/
    public function get_chat_ajax_rsptnew()
    {
        $login_user_id = $_SESSION['userId'];

        $shown_box_ids = explode(',', $_GET['shown_box_ids']);
        $query = $this->db->query("SELECT * FROM `user_chat` where chat_to=" . $login_user_id . " and status='not_viewed'  order by date_time");
        $result = $query->result_array();
        //echo json_encode($result);
        foreach ($result as $res_key => $res_value) {
            if (in_array($res_value['chat_from'], $shown_box_ids)) {
                $chat_id = $res_value['chat_from'];
                $for_lst_id = $res_value['id'];
                $query = $this->db->query("SELECT * FROM `user_chat` where chat_from in (" . $chat_id . "," . $login_user_id . ") and chat_to in (" . $chat_id . "," . $login_user_id . ") and status='not_viewed' and id = " . $for_lst_id . " order by date_time limit 1");
                $res_array = $query->result_array();
                $pro_image_new = array();
                foreach ($res_array as $resarray_key => $resarray_value) {
                    $pro_image = $this->db->query("SELECT crm_profile_pic FROM user where  id=" . $resarray_value['chat_from'])->row_array();
                    $img_name = (isset($pro_image['crm_profile_pic'])) ? $pro_image['crm_profile_pic'] : '';
                    $pro_image_new[$resarray_value['chat_from']] = $img_name;
                    $res_array[$resarray_key]['profile_images'] = $img_name;
                }
                $res_json = json_encode($res_array);
                //$data['chat_messages'] = $query->result_array();
                //$this->load->view('User_chat_chart/chat_page_chatlist',$data);


                //  $MyFile = file_get_contents("http://remindoo.uk/application/views/User_chat_chart/chat_page_chatlist.php?base_url=".base_url()."&session_userid=".$_SESSION['userId']."&chat_messages=".$res_json);

                //  $MyFile = file_get_contents("application/views/User_chat_chart/chat_page_chatlist.php?base_url=".base_url()."&session_userid=".$_SESSION['userId']."&chat_messages=".$res_json);
                $chat_messages = $res_array;
                $login_user_id = $_SESSION['userId'];
                $res = '';
                foreach ($chat_messages as $key => $value) {

                    if ($login_user_id == $value['chat_from']) {
                        // $pro_image=$this->db->query("SELECT * FROM user where  id=".$value['chat_from'])->result_array();

                        $pro_image = (isset($value['profile_images']) && $value['profile_images'] != '') ? base_url() . "uploads/" . $value['profile_images'] : base_url() . "assets/images/avatar-3.jpg";

                        $res .= '<div class="message out no-avatar media our_info_right" data-id="' . $value['id'] . '" data-dbid="' . $value['chat_to'] . '" >
                        <div class="body media-body text-right p-l-50"><div class="content msg-reply f-12 bg-primary d-inline-block">' . $value['message'] . '</div><div class="seen"><i class="icon-clock f-12 m-r-5 txt-muted d-inline-block"></i><span><p class="d-inline-block">' . date('m-d-Y h:i:sa', $value['date_time']) . '</p></span><div class="clear"></div> </div></div><div class="sender media-right friend-box"><a href="javascript:void(0);" title="Me"><img src="<?php echo $pro_image;?>" onerror="" class=" img-chat-profile" alt="Me"></a> </div></div>';
                    } else {

                        $pro_image = (isset($value['profile_images']) && $value['profile_images'] != '') ? base_url() . "uploads/" . $value['profile_images'] : base_url() . "assets/images/avatar-3.jpg";

                        // $pro_image=$this->Common_mdl->select_record('user','id',$value['chat_to']);
                        // $pro_image=$this->db->query("SELECT * FROM user where  id=".$value['chat_from'])->result_array();

                        $res .= '<div class="message out no-avatar media our_info" data-id="' . $value['id'] . '" data-dbid="' . $value['chat_to'] . '">
                        <div class="sender media-left friend-box"><a href="javascript:void(0);" title=""><img src="' . $pro_image . '" onerror="" class=" img-chat-profile" alt="Me"></a> </div><div class="body media-body text-left p-l-10"><div class="content msg-reply f-12 bg-primary d-inline-block">' . $value['message'] . '</div><div class="seen"><i class="icon-clock f-12 m-r-5 txt-muted d-inline-block"></i><span><p class="d-inline-block">' . date('m-d-Y h:i:sa', $value['date_time']) . '</p></span><div class="clear"></div> </div></div></div>';
                    }
                }


                //$result[$res_key]['message_content']=$MyFile;
                $result[$res_key]['message_content'] = $res;
            }
        }
        echo json_encode($result);
        // $res=json_encode($result);
        // print_r(json_decode($res,true));
    }
    public function get_group_chat_unread_rsptnew()
    {
        $group_id = $_GET['chat_ids'];
        //$query=$this->db->query("SELECT id FROM `group_chat_messages` where group_id=".$group_id." and find_in_set('".$_SESSION['id']."',not_viewed) order by date_time");

        $group_ids_array = array();
        // $group_id=array(13,11);
        //$group_id=implode(',',$group_id);
        $query = $this->db->query("SELECT id,group_id FROM `group_chat_messages` where group_id in (" . $group_id . ") and find_in_set('" . $_SESSION['id'] . "',not_viewed) order by date_time");
        $res_qury = $query->result_array();
        $explode_arr = explode(',', $group_id);
        if (count($explode_arr) > 0) {
            foreach ($explode_arr as $ids_key => $ids_value) {
                $count = 0;
                foreach ($res_qury as $key => $value) {
                    if ($value['group_id'] == $ids_value) {
                        $count++;
                    }
                }
                //echo $count."--".$ids_value;
                $group_ids_array[$ids_value] = $count;
            }
        }
        echo json_encode($group_ids_array);
    }

    public function get_group_chat_rsptnew()
    {
        //$group_data=$_GET['group_full_data'];     
        $login_user_id = $_SESSION['userId'];
        //$group_data_arr=array_filter(json_decode($group_data,true));
    }

    public function check_group_delete_rsptnew()
    {
        $group_id = $_GET['group_ids'];
        $get_group_data = $this->db->query("SELECT id FROM chat_group where id in(" . $group_id . ") and status='delete' ")->result_array();
        // echo "SELECT * FROM chat_group where id=".$group_id." and status='delete' ";
        //echo count($get_group_data);
        $res_qury = $get_group_data;
        $group_ids_array = array();
        $explode_arr = explode(',', $group_id);
        if (count($explode_arr) > 0) {
            foreach ($explode_arr as $ids_key => $ids_value) {
                $count = 0;
                foreach ($res_qury as $key => $value) {
                    if ($value['id'] == $ids_value) {
                        $count++;
                    }
                }
                //echo $count."--".$ids_value;
                $group_ids_array[$ids_value] = $count;
            }
        }
        echo json_encode($group_ids_array);
    }

    public function check_group_update_members_rsptnew()
    {
        $group_id = $_GET['group_ids'];
        $res_return = array();
        $group_members = json_encode($_GET['members'], true);
        $get_group_data_qry = $this->db->query('select * from chat_group where id in(' . $group_id . ') ')->result_array();
        foreach ($get_group_data_qry as $gr_key => $get_group_data) {
            $members = $get_group_data['members'];
            $group_member_data = $this->db->query("SELECT crm_name FROM user where id in (" . $get_group_data['members'] . "," . $get_group_data['create_by'] . ") ")->result_array();
            $member_names = array();
            foreach ($group_member_data as $men_key => $mem_value) {
                array_push($member_names, $mem_value['crm_name']);
            }
            //  echo implode(',', $member_names);
            if (count($member_names) > count(explode(',', $group_members))) {
                $result = array_diff($member_names, explode(',', $group_members));
            } else {
                $result = array_diff(explode(',', $group_members), $member_names);
            }

            // echo json_encode($group_members);
            // echo json_encode($member_names);
            if (count($result) > 0) {
                //echo implode(',', $member_names);
                $res_return[$get_group_data['id']] = implode(',', $member_names);
            } else {
                //   echo "wrong";
                $res_return[$get_group_data['id']] = "wrong";
            }
        }
        echo json_encode($res_return);
        //  $members=$get_group_data[0]['members'];
        //   $group_member_data=$this->db->query("SELECT crm_name FROM user where id in (".$get_group_data[0]['members'].",".$get_group_data[0]['create_by'].") ")->result_array();
        //  $member_names=array();
        //  foreach ($group_member_data as $men_key => $mem_value)
        //   {
        //      array_push($member_names, $mem_value['crm_name']);
        //   }
        // //  echo implode(',', $member_names);
        //   if(count($member_names) > count(explode(',', $group_members)))
        //   {
        //        $result=array_diff($member_names,explode(',',$group_members));
        //   }
        //   else
        //   {
        //      $result=array_diff(explode(',',$group_members),$member_names);
        //   }

        //  // echo json_encode($group_members);
        //  // echo json_encode($member_names);
        //   if(count($result)>0)
        //      {
        //          echo implode(',', $member_names);
        //      }
        //      else
        //      {
        //          echo "wrong";
        //      }

    }
    /** end of 28-12-2018 **/

    public function get_all_result()
    {

        $data['user_chat'] = $data['group_chat'] = $data['online_status'] = $val_array = $id1 = array();
        // echo '<pre>';
        if (!empty($_POST['id'])) {
            $id1 = json_decode($_POST['id'], true);
            $id = implode(",", $id1);
        } else {
            $id = 0;
        }

        $newTime = strtotime('-10 minutes');

        $login_status = $this->db->query(" SELECT `id` FROM user WHERE id in (" . $id . ") and last_login_time >=" . $newTime . " and firm_id=" . $_SESSION['firm_id'] . " ")->result_array();

        $val_array = array_column($login_status, 'id');

        foreach ($id1 as $key => $value) {

            if (in_array($value, $val_array)) {
                $data['online_status'][$key]['id'] = $value;
                $data['online_status'][$key]['status'] = 'bg-success';
            } else {
                $data['online_status'][$key]['id'] = $value;
                $data['online_status'][$key]['status'] = 'bg-danger';
            }
        }
        // echo $this->db->last_query();
        //    print_r($login_status);



        $login_user_id = $_SESSION['id'];

        // $chat_query=$this->db->query("SELECT * FROM `user_chat` where chat_from in (".$login_user_id.") or chat_to in (".$login_user_id.") order by date_time DESC LIMIT 30")->result_array();

        $data['chat_count'] = $this->db->query("SELECT COUNT(status) as count_status,chat_from FROM `user_chat` where chat_to=" . $login_user_id . " and status='not_viewed'   GROUP BY chat_from order by date_time; ")->result_array();

        $data['group_count'] = $this->db->query("SELECT COUNT(group_chat_messages.id) as count_status ,group_chat_messages.group_id FROM `group_chat_messages` JOIN chat_group on chat_group.id=group_chat_messages.group_id where find_in_set('" . $login_user_id . "',group_chat_messages.not_viewed) and chat_group.status='' and group_chat_messages.chat_from!='" . $login_user_id . "' and  chat_group.firm_id='" . $_SESSION['firm_id'] . "' GROUP BY group_chat_messages.group_id ;")->result_array();

        // $group_query=$this->db->query("SELECT group_chat_messages.* FROM `group_chat_messages` JOIN chat_group on chat_group.id=group_chat_messages.group_id where chat_group.status='' and group_chat_messages.chat_from=".$login_user_id." order by group_chat_messages.date_time DESC LIMIT 30")->result_array();

        // print_r($group_query);
        // exit;


        foreach ($data['chat_count'] as $key => $value) {


            $query = $this->db->query("SELECT * FROM `user_chat` where chat_from in (" . $value['chat_from'] . "," . $login_user_id . ") and chat_to in (" . $value['chat_from'] . "," . $login_user_id . ") ");

            $get_usertype = $this->Common_mdl->select_record('user', 'id', $value['chat_from']);
            $data['chat_count'][$key]['user_type'] = $get_usertype['user_type'];

            $datas['chat_messages'] = $query->result_array();
            $data['chat_count'][$key]['message'] = $this->load->view('User_chat/chat_page_chatlist', $datas, true);


            # code...
        }

        foreach ($data['group_count'] as $key => $value) {

            $query = $this->db->query("SELECT * FROM `group_chat_messages` where group_id=" . $value['group_id'] . " order by date_time");
            $datas['chat_messages'] = $query->result_array();
            $data['group_count'][$key]['message'] = $this->load->view('User_chat_chart/chat_page_chatlist_group', $datas, true);



            # code...
        }




        // foreach (array_reverse($chat_query) as $key => $value) {
        //  $data['user_chat'][$key]['id']=$value['id'];
        //  $data['user_chat'][$key]['message']=$value['message'];
        //  $data['user_chat'][$key]['from_to']=$value['chat_from']== $login_user_id ?'our_info_right':'our_info';
        //  $data['user_chat'][$key]['date_time']=$value['date_time'];
        //  $data['user_chat'][$key]['chat_from']=$value['chat_from'];
        //  $data['user_chat'][$key]['chat_to']=$value['chat_to'];
        //  $data['user_chat'][$key]['status']=$value['status'];

        // }

        // foreach (array_reverse($group_query) as $key => $value) {
        //  $data['group_chat'][$key]['id']=$value['id'];
        //  $data['group_chat'][$key]['message']=$value['message'];
        //  $data['group_chat'][$key]['from_to']=$value['chat_from']== $login_user_id ?'our_info_right':'our_info';
        //  $data['group_chat'][$key]['date_time']=$value['date_time'];
        //  $data['group_chat'][$key]['chat_from']=$value['chat_from'];
        //  $data['group_chat'][$key]['chat_to']=$login_user_id;
        //  $data['group_chat'][$key]['group_id']=$value['group_id'];

        // }
        //echo $this->db->last_query();
        echo json_encode($data);
        //   exit;

    }
}

?>