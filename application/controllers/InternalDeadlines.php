<?php

class InternalDeadlines extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model(array('Common_mdl', 'Security_model', 'Task_creating_model'));		
		$this->load->helper(['template_decode', 'firm_columnSettings']);		
	}	

	public function index(){		
		$this->setup();
	}

	public function setup(){
		$this->Security_model->chk_login();
		$fid    = $_SESSION['firm_id'];
		$uid    = $_SESSION['id'];
		$sql    = "SELECT IDL.*, SL.service_name 
				  FROM internal_deadlines AS IDL
				  INNER JOIN service_lists AS SL
				          ON SL.id = IDL.service_id
				  WHERE IDL.user_id = $uid
				  AND IDL.firm_id = $fid
				  AND IDL.is_del = '0'";
		$intd   = $this->db->query($sql)->result_array();		
		$status = $this->Common_mdl->get_firm_progress();	
		$data   = [
			'settings' => $intd,
			'status'   => $status
		];
		$this->load->view('InternalDeadlines/setup_view', ['data' => $data]);		
	}	

	public function update_ds(){
		$this->Security_model->chk_login();
		$ds  = 	$_REQUEST['ds'];
		$id  =  $_REQUEST['id'];
		$sql = 	"UPDATE internal_deadlines SET task_status = '$ds' WHERE id = $id";
		echo $this->db->query($sql);
	}

	public function update_idl(){
		$this->Security_model->chk_login();
		$idl = 	$_REQUEST['idl'];
		$id  =  $_REQUEST['id'];
		$sql = 	"UPDATE internal_deadlines SET internal_deadline = $idl WHERE id = $id AND internal_deadline = 0";
		echo $this->db->query($sql);
	}

	public function update_edl(){
		$this->Security_model->chk_login();
		$edl = 	$_REQUEST['edl'];
		$id  =  $_REQUEST['id'];
		$sql = 	"UPDATE internal_deadlines SET external_deadline = $edl WHERE id = $id AND external_deadline = 0";
		echo $this->db->query($sql);
	}

	public function update_due_dates(){
		$this->Security_model->chk_login();
		$task_id   = $_REQUEST['task_id'];
		$status_id = $_REQUEST['status_id'];
		$serv_id   = $_REQUEST['serv_id'];
		$sql_0     = "SELECT * FROM progress_status_change WHERE task_id = $task_id";
		$sql_1     = "INSERT progress_status_change SET task_id = $task_id, status_id = $status_id";
		$sql_2     = "UPDATE progress_status_change SET status_id = $status_id WHERE task_id = $task_id";
		$data      = $this->db->query($sql_0)->result_array();

		if(empty($data)){
			// If no records found insert new
			$this->db->query($sql_1);
		}else{
			// On exist update
			$this->db->query($sql_2);
		}				
		echo $this->calculate_due_dates($task_id, $serv_id, $status_id);
	}

	public function calculate_due_dates($task_id, $serv_id, $status_id){
		$this->Security_model->chk_login();
		$fid    = $_SESSION['firm_id'];
		$uid    = $_SESSION['id'];
		$sql_3  = "SELECT ANT.id,
						  ANT.start_date,
						  ANT.end_date,
						  ANT.task_status,
						  PSC.status_id,
						  PSC.created_at,
						  PSC.modified_at,
						  IDL.internal_deadline,
 						  IDL.external_deadline
				  FROM add_new_task AS ANT
				  LEFT JOIN progress_status_change AS PSC
					     ON PSC.task_id = ANT.id  
						AND PSC.status_id = $status_id					  
				  INNER JOIN internal_deadlines AS IDL
				          ON IDL.firm_id = $fid
						 AND IDL.user_id = $uid
						 AND IDL.task_status = PSC.status_id
						 AND IDL.service_id = $serv_id
				  WHERE ANT.id = $task_id
				  ORDER BY PSC.id DESC
				  LIMIT 1";	
		
		$data = $this->db->query($sql_3)->result_array();
		
		if(isset($data[0])){
			$task_status = $data[0]['task_status'];
	  
			if($data[0]['internal_deadline'] > 0){
			  	if(isset($data[0]['modified_at']) && $data[0]['modified_at'] != ''){
					$data[0]['internal_due_date'] = date('Y-m-d',strtotime('+'.$data[0]['internal_deadline'].' days',strtotime($data[0]['modified_at'])));               
			  	}else{
					$data[0]['internal_due_date'] = '';
			  	}        
			}else{
			  	if(isset($data[0]['end_date']) && $data[0]['end_date'] != ''){
					$data[0]['internal_due_date'] = date('Y-m-d',strtotime($data[0]['end_date']));  
			  	}else{
					$data[0]['internal_due_date'] = '';
			  	}        
			}
	  
			if($data[0]['external_deadline'] > 0){
			  	if(isset($data[0]['modified_at']) && $data[0]['modified_at'] != ''){          
					$data[0]['external_due_date'] = date('Y-m-d',strtotime('+'.$data[0]['external_deadline'].' days',strtotime($data[0]['modified_at'])));               
			  	}else{
					$data[0]['external_due_date'] = '-';
			  	}        
			}else{
			  	if(isset($data[0]['end_date']) && $data[0]['end_date'] != ''){
					$data[0]['external_due_date'] = date('Y-m-d',strtotime($data[0]['end_date']));  
			  	}else{
					$data[0]['external_due_date'] = '-';
			  	}        
			}      
	  
			$int_delay                    = round( (time() - strtotime($data[0]['internal_due_date'])) / (60 * 60 * 24));      
			$data[0]['internal_delay']    = ($int_delay > 0 && ($task_status != 'complete' && $task_status != 5)) ? $int_delay . 'Days Delay' : '';
			$ext_delay                    = 0;
			$data[0]['external_delay']    = ($ext_delay > 0 && ($task_status != 'complete' && $task_status != 5)) ? $ext_delay . 'Days Delay' : '';
		}else{      
			if(isset($data[0]['end_date']) && $data[0]['end_date'] != ''){
			  	$data[0]['internal_due_date'] = date('Y-m-d',strtotime($data[0]['end_date']));    
			}else{
			  	$data[0]['internal_due_date'] = '-';
			} 
	  
			if(isset($data[0]['end_date']) && $data[0]['end_date'] != ''){
			  	$data[0]['external_due_date'] = date('Y-m-d',strtotime($data[0]['end_date']));  
			}else{
			  	$data[0]['external_due_date'] = '-';
			}        
			$data[0]['internal_delay']    = '';
			$data[0]['external_delay']    = '';
		} 
	  
		if($data[0]['external_due_date'] != '-'){
			$day_dif = round( ( strtotime($data[0]['external_due_date']) - strtotime($data[0]['internal_due_date']) ) / ( 60 * 60 * 24 ) );        
	  
			if($day_dif < 0){
			  	$data[0]['internal_due_date'] = $data[0]['external_due_date'];
			  	$int_delay                    = round( (time() - strtotime($data[0]['internal_due_date'])) / (60 * 60 * 24));      
			  	$data[0]['internal_delay']    = ($int_delay > 0 && ($task_status != 'complete' && $task_status != 5)) ? $int_delay . 'Days Delay' : '';
			}
		}
		return json_encode($data);
	}
} ?>