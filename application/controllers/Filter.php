<?php
error_reporting(E_STRICT);
class Filter extends CI_Controller 
{
    public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";

	public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Common_mdl','Report_model','Security_model','Proposal_model'));
        $this->load->library('Excel');
	}
    
    public function client_filter()
    {
        $month_start = date("Y-m-01");
        $month_end = date("Y-m-t"); 
        $month_end = date('Y-m-d', strtotime($month_end . ' +1 day'));       

        $data['Private']=$this->Common_mdl->ClientDetailsByLegalForm('Private Limited company',$month_start,$month_end); 
        $data['Public']=$this->Common_mdl->ClientDetailsByLegalForm('Public Limited company',$month_start,$month_end);
        $data['limited']=$this->Common_mdl->ClientDetailsByLegalForm('Limited Liability Partnership',$month_start,$month_end);
        $data['Partnership']=$this->Common_mdl->ClientDetailsByLegalForm('Partnership',$month_start,$month_end);
        $data['self']=$this->Common_mdl->ClientDetailsByLegalForm('Self Assessment',$month_start,$month_end);
        $data['Trust']=$this->Common_mdl->ClientDetailsByLegalForm('Trust',$month_start,$month_end);
        $data['Charity']=$this->Common_mdl->ClientDetailsByLegalForm('Charity',$month_start,$month_end);
        $data['Other']=$this->Common_mdl->ClientDetailsByLegalForm('Other',$month_start,$month_end);

        $this->load->view('reports/client_success',$data);      
    }

    public function client_filter_year()
    {
	    $Year_start = date("Y-01-01");
        $Year_end = date("Y-12-31");        

        $data['Private']=$this->Common_mdl->ClientDetailsByLegalForm('Private Limited company',$Year_start,$Year_end); 
        $data['Public']=$this->Common_mdl->ClientDetailsByLegalForm('Public Limited company',$Year_start,$Year_end);
        $data['limited']=$this->Common_mdl->ClientDetailsByLegalForm('Limited Liability Partnership',$Year_start,$Year_end);
        $data['Partnership']=$this->Common_mdl->ClientDetailsByLegalForm('Partnership',$Year_start,$Year_end);
        $data['self']=$this->Common_mdl->ClientDetailsByLegalForm('Self Assessment',$Year_start,$Year_end);
        $data['Trust']=$this->Common_mdl->ClientDetailsByLegalForm('Trust',$Year_start,$Year_end);
        $data['Charity']=$this->Common_mdl->ClientDetailsByLegalForm('Charity',$Year_start,$Year_end);
        $data['Other']=$this->Common_mdl->ClientDetailsByLegalForm('Other',$Year_start,$Year_end);

        $this->load->view('reports/client_success',$data);      
    }

	public function task_filter()
    {       
		$month_start = date("Y-m-01");
        $month_end = date("Y-m-t"); 
        $month_end = date('Y-m-d', strtotime($month_end . ' +1 day'));

    	$data['total_tasks']=$this->Report_model->task_count();
        $data['notstarted_tasks']=$this->Report_model->task_detailsWithdate('notstarted',$month_start,$month_end);
		$data['completed_tasks']=$this->Report_model->task_detailsWithdate('complete',$month_start,$month_end);
		$data['inprogress_tasks']=$this->Report_model->task_detailsWithdate('inprogress',$month_start,$month_end);
		$data['awaiting_tasks']=$this->Report_model->task_detailsWithdate('awaiting',$month_start,$month_end);
		$data['archive_tasks']=$this->Report_model->task_detailsWithdate('archive',$month_start,$month_end);
		
        $this->load->view('reports/task_success1',$data);
	}

    public function task_filter_year()
    {
		$Year_start = date("Y-01-01");
        $Year_end = date("Y-12-31"); 

		$data['total_tasks']=$this->Report_model->task_count();
        $data['notstarted_tasks']=$this->Report_model->task_detailsWithdate('notstarted',$Year_start,$Year_end);
        $data['completed_tasks']=$this->Report_model->task_detailsWithdate('complete',$Year_start,$Year_end);
        $data['inprogress_tasks']=$this->Report_model->task_detailsWithdate('inprogress',$Year_start,$Year_end);
        $data['awaiting_tasks']=$this->Report_model->task_detailsWithdate('awaiting',$Year_start,$Year_end);
        $data['archive_tasks']=$this->Report_model->task_detailsWithdate('archive',$Year_start,$Year_end);

        $this->load->view('reports/task_success1',$data);
    }

    public function proposal_filter()
    {
        $month_start = date("Y-m-01");
        $month_end = date("Y-m-t"); 
        $month_end = date('Y-m-d', strtotime($month_end . ' +1 day'));

		$data['accept_proposal']=$this->Proposal_model->proposal_details_date('accepted',$month_start,$month_end);
		$data['indiscussion_proposal']=$this->Proposal_model->proposal_details_date('in discussion',$month_start,$month_end);
		$data['sent_proposal']=$this->Proposal_model->proposal_details_date('sent',$month_start,$month_end);
		$data['viewed_proposal']=$this->Proposal_model->proposal_details_date('opened',$month_start,$month_end);
		$data['declined_proposal']=$this->Proposal_model->proposal_details_date('declined',$month_start,$month_end);
		$data['archive_proposal']=$this->Proposal_model->proposal_details_date('archive',$month_start,$month_end);
		$data['draft_proposal']=$this->Proposal_model->proposal_details_date('draft',$month_start,$month_end);
		
        $this->load->view('reports/proposal_success1',$data);
    }

    public function proposal_filter_year()
    {
		$Year_start = date("Y-01-01");
        $Year_end = date("Y-12-31");

		$data['accept_proposal']=$this->Proposal_model->proposal_details_date('accepted',$Year_start,$Year_end);
        $data['indiscussion_proposal']=$this->Proposal_model->proposal_details_date('in discussion',$Year_start,$Year_end);
        $data['sent_proposal']=$this->Proposal_model->proposal_details_date('sent',$Year_start,$Year_end);
        $data['viewed_proposal']=$this->Proposal_model->proposal_details_date('opened',$Year_start,$Year_end);
        $data['declined_proposal']=$this->Proposal_model->proposal_details_date('declined',$Year_start,$Year_end);
        $data['archive_proposal']=$this->Proposal_model->proposal_details_date('archive',$Year_start,$Year_end);
        $data['draft_proposal']=$this->Proposal_model->proposal_details_date('draft',$Year_start,$Year_end);

        $this->load->view('reports/proposal_success1',$data);
    }

    public function leads_filter()
    {
        $month_start = date("Y-m-01");
        $month_end = date("Y-m-t"); 
        $month_end = date('Y-m-d', strtotime($month_end . ' +1 day'));

		$this->session->set_userdata('start',$month_start);  
	    $this->session->set_userdata('end',$month_end);   

	    $data['leads_status']=$this->Common_mdl->getallrecords('leads_status');               
        $data['overall_leads']=$this->Report_model->overallleads_count();      

       	$this->load->view('reports/leads_success1',$data);
    }

    public function leads_filter_year()
    {
        $Year_start = date("Y-01-01");
        $Year_end = date("Y-12-31");    
		
        $this->session->set_userdata('start',$Year_start);  
        $this->session->set_userdata('end',$Year_end);   

        $data['leads_status']=$this->Common_mdl->getallrecords('leads_status');
        $data['overall_leads']=$this->Report_model->overallleads_count();

        $this->load->view('reports/leads_success1',$data);
    }

    public function service_filter()
    {
        $month_start = date("Y-m-01");
        $month_end = date("Y-m-t"); 
        $month_end = date('Y-m-d', strtotime($month_end . ' +1 day')); 
        
        $cols = $this->getColumns();
        $services = $this->Common_mdl->getServicelist();

        foreach($services as $key => $value) 
        {
           $data['services_count'][$value['service_name']]['id'] = $value['id'];
           $data['services_count'][$value['service_name']]['count'] = $this->Report_model->detailsWithDate($cols['columns'][$value['id']],$cols['columns_check'][$value['id']],$month_start,$month_end);
        } 

        $this->load->view('reports/service_success1',$data);
    }

    public function service_filter_year()
    {
    	$Year_start = date("Y-01-01");
        $Year_end = date("Y-12-31");  
	    
        $cols = $this->getColumns();
        $services = $this->Common_mdl->getServicelist();

        foreach($services as $key => $value) 
        {
           $data['services_count'][$value['service_name']]['id'] = $value['id'];
           $data['services_count'][$value['service_name']]['count'] = $this->Report_model->detailsWithDate($cols['columns'][$value['id']],$cols['columns_check'][$value['id']],$Year_start,$Year_end);
        }      

        $this->load->view('reports/service_success1',$data);
    }
    

    public function deadline_filter()
    {
        $month_start = date("Y-m-01");
        $month_end = date("Y-m-t"); 
        $month_end = date('Y-m-d', strtotime($month_end . ' +1 day'));

        $cols = $this->getColumns();
        $services = $this->Common_mdl->getServicelist();

        foreach($services as $key => $value) 
        {
           $data['deadlines_count'][$value['service_name']]['id'] = $value['id'];
           $data['deadlines_count'][$value['service_name']]['count'] = $this->Report_model->deadline_detailsWithDate($cols['columns_check'][$value['id']],$cols['columns'][$value['id']],$month_start,$month_end);
        }        

        $this->load->view('reports/deadline_success1',$data);
    }


    public function deadline_filter_year()
    {
        $Year_start = date("Y-01-01");
        $Year_end = date("Y-12-31");  

        $cols = $this->getColumns();
        $services = $this->Common_mdl->getServicelist();

        foreach($services as $key => $value) 
        {
           $data['deadlines_count'][$value['service_name']]['id'] = $value['id'];
           $data['deadlines_count'][$value['service_name']]['count'] = $this->Report_model->deadline_detailsWithDate($cols['columns_check'][$value['id']],$cols['columns'][$value['id']],$Year_start,$Year_end);
        }

        $this->load->view('reports/deadline_success1',$data);
    }

    public function invoice_filter()
    {
        $month_start = date("Y-m-01");
        $month_end = date("Y-m-t"); 
        $month_end = date('Y-m-d', strtotime($month_end . ' +1 day'));

        $data['expired_invoice']=$this->Report_model->selectInvoiceByType('amount_details','payment_status','pending', 'client_email',$month_start,$month_end); 
        $data['approved_invoice']=$this->Report_model->selectInvoiceByType('amount_details','payment_status','approve','client_email', $month_start,$month_end);
        $data['cancel_invoice']=$this->Report_model->selectInvoiceByType('amount_details','payment_status','decline', 'client_email', $month_start,$month_end);

        $this->load->view('reports/invoice_success1',$data);
    }


    public function invoice_filter_year()
    {
        $Year_start = date("Y-01-01");
        $Year_end = date("Y-12-31");

        $data['expired_invoice']=$this->Report_model->selectInvoiceByType('amount_details','payment_status','pending', 'client_email', $Year_start,$Year_end); 
        $data['approved_invoice']=$this->Report_model->selectInvoiceByType('amount_details','payment_status','approve','client_email', $Year_start,$Year_end);
        $data['cancel_invoice']=$this->Report_model->selectInvoiceByType('amount_details','payment_status','decline', 'client_email', $Year_start,$Year_end);

        $this->load->view('reports/invoice_success1',$data);
    }

    public function summary(){
    	$data['client_details']=$this->Report_model->summary_client_details();    	
    	$data['task_details']=$this->Report_model->summary_task_details();   
    	$data['lead_details']=$this->Report_model->summary_lead_details();   
    	 $this->load->library('Tc');
            $pdf = new Tc('P', 'mm', 'A4', true, 'UTF-8', false); 
            $pdf->AddPage('P','A4');
            $html = $this->load->view('reports/summary_pdf',$data,true);      
            $pdf->SetTitle('Summary list');
            $pdf->SetHeaderMargin(30);
            $pdf->SetTopMargin(20);
            $pdf->setFooterMargin(20);
            $pdf->SetAutoPageBreak(true,20);
            $pdf->SetAuthor('Author');
            $pdf->SetDisplayMode('real', 'default');
            $pdf->WriteHTML($html);          
            $pdf->Output('Summary_list.pdf', 'D'); 
    }

    public function excelsummary_download(){
         $filename = 'Summary_'.date('Ymd').'.xlsx';           
            $data['client']=$this->Report_model->summary_client_details();  
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/csv; "); 
            $file = fopen('php://output', 'w'); 
            $header = array("S.NO","Name","Updates","Active","CompanyStatus","User Type","Status");
            fputcsv($file, $header);
            $i=0;  
              foreach ($data['client'] as $key=>$line){
                $user_id=$line['id'];
            if($line['id']){ $i++; $line['id']=$i; }    
            $update=$this->Common_mdl->select_record('update_client','user_id',$user_id);
            $company_status=$this->Common_mdl->select_record('client','user_id',$user_id);
            $getallUservalue=$this->Common_mdl->select_record('user','id',$user_id);
            $role=$this->Common_mdl->select_record('Role','id',$getallUservalue['role']);
            $line['crm_name']=$getallUservalue['crm_name'];
            if($update['counts']!=''){
               $line['updates']= $update['counts']; 
            } else{
               $line['updates']='0';
            }
            if($getallUservalue['status']=='1'){ 
                 $line['status']="Active"; }
                  elseif($getallUservalue['status']=='0'){ 
                     $line['status']="Inactive"; 
                 }elseif($getallUservalue['status']=='3'){
                   $line['status']="Frozen"; 
               }else{ 
                 $line['status']='-';
            }

            $line['crm_company_status']=$company_status['crm_company_status'];
            $line['role']=$role['role'];

            if($getallUservalue['role']=='6'){
            $line['house']='Manual';
            }elseif($getallUservalue['role']=='4' && $company_status['status']==0 ){
             $line['house']='Manual';
            }elseif($getallUservalue['role']=='4' && $company_status['status']==1){
             $line['house']='Company House';
            }elseif($getallUservalue['role']=='4' && $company_status['status']==2){
             $line['house']='Import';
            }else{
                $line['house'] ='';
            }
           
              fputcsv($file,$line);
            } 

            $data['task_list']=$this->Report_model->summary_task_details();   
            $header = array("S.NO","Task Name","Start Date","Due Date","Status","Assignto");
            fputcsv($file, $header);
            $i=0;  
            $team_array=array();
            $dept_array=array();
            $assignto=array();
              foreach ($data['task_list'] as $key=>$line){ 

                  if($line['id']){ $i++; $line['id']=$i; }
                    if(isset($line['worker'])){
                     $explode_worker=explode(',',$line['worker']);
                        foreach($explode_worker as $key => $val){     
                        $getUserProfilepic = $this->Common_mdl->getUserProfileName($val);
                        array_push($assignto,$getUserProfilepic);
                        }                      
                    }
                    if(isset($value['team'])){
                     $explode_team=explode(',',$line['team']);
                     for($v=0;$v<count($explode_team);$v++){
                        $team = $this->db->query("select * from team where id=".$explode_team[$v]." ")->row_array();
                        array_push($team_array,$team['team']);
                     } 
                    }
                    if(isset($value['department'])){
                     $explode_department=explode(',',$line['department']);
                     for($z=0;$z<count($explode_department);$z++){
                       $department=$this->db->query("select * from department_permission where id=".$explode_department[$z]." ")->row_array();
                       array_push($dept_array,$department['new_dept']);
                     }                    
                    }
                    $line['worker']= implode(',',$assignto);
                    $line['team']= implode(',',$team_array);
                    $line['department']= implode(',',$dept_array);
                   fputcsv($file,$line);
                   $assignto=array(); 
                   $team_array=array();
                   $dept_array=array();
                    } 


                 $data['lead_list']=$this->Report_model->summary_lead_details(); 

            $header = array("S.NO","Lead Name","Company Name","Email","Assigned","Status");
            fputcsv($file, $header);
            $i=0;  
            foreach ($data['lead_list'] as $key=>$line){                  
              if($line['id']){ $i++; $line['id']=$i; }
              $getUserProfilepic=array();
              $ex_assign=explode(',',$line['assigned']);
              foreach ($ex_assign as $ex_key => $ex_value) {
              $getUserProfilepic[] = $this->Common_mdl->getUserProfileName($ex_value);
              }
              if(is_numeric($line['company']))
              {
                $client_query=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and id='".$line['company']."' order by id desc ")->result_array();
              if(count($client_query)>0)
              {
                 $line['company']=$client_query[0]['crm_company_name'];
              }
              else
              {
                 $line['company']='-';
              }
              }
              else
              {
                 $line['company']=$line['company'];
              }              
              $line['assigned']=implode(',',$getUserProfilepic);
              $line['lead_status']=$this->Report_model->lead_status($line['lead_status']);
              fputcsv($file,$line);
              $getUserProfilepic=array();              
            }
            fclose($file);
            exit;     
    }

    public function summary_customization()
    {  
        $fromdate = $_POST['from_date'];
        $todate = $_POST['to_date'];       
        $summary = $_POST['summary'];        
        $filter = $_POST['filter']; 
        $client = $_POST['client'];     


        if($summary[0]=='all')
        {
            $data['firm_users']=$this->Report_model->summary_user_detailsWithDate($fromdate,$todate,$filter);
            $data['completed_tasks']=$this->Report_model->summary_task_detailsWithDate($fromdate,$todate,$filter,$client);   
            $data['lead_details']=$this->Report_model->summary_lead_detailsWithDate($fromdate,$todate,$filter,$client); 
        }
        else
        {
            for($i=0;$i<count($summary);$i++)
            {   
                 if($summary[$i]=='user')
                 {
                    $data['firm_users']=$this->Report_model->summary_user_detailsWithDate($fromdate,$todate,$filter);
                 }
                 else if($summary[$i]=='task')
                 {
                    $data['completed_tasks']=$this->Report_model->summary_task_detailsWithDate($fromdate,$todate,$filter,$client); 
                 }
                 else
                 {
                     $data['lead_details']=$this->Report_model->summary_lead_detailsWithDate($fromdate,$todate,$filter,$client); 
                 }                
            }           
        }
        
        $this->load->view('reports/summary_custom_result',$data);               
     }

    public function summary_sess(){
        $fromdate=$_SESSION['from_date'];
        $todate=$_SESSION['to_date'];
        $summary_option=$_SESSION['summary_option'];
        if($summary_option=='all'){
        $data['client_details']=$this->Report_model->summary_user_detailsWithDate($fromdate,$todate);      
        $data['task_details']=$this->Report_model->summary_task_detailsWithDate($fromdate,$todate);   
        $data['lead_details']=$this->Report_model->summary_lead_detailsWithDate($fromdate,$todate); 
        }else if($summary_option=='user'){
            $data['client_details']=$this->Report_model->summary_user_detailsWithDate($fromdate,$todate); 
        }else if($summary_option=='task'){
            $data['task_details']=$this->Report_model->summary_task_detailsWithDate($fromdate,$todate); 
        }else{
              $data['lead_details']=$this->Report_model->summary_lead_detailsWithDate($fromdate,$todate); 
        }

            $this->load->library('Tc');
            $pdf = new Tc('P', 'mm', 'A4', true, 'UTF-8', false); 
            $pdf->AddPage('P','A4');
            if($_SESSION['summary_option']=='all'){
            $html = $this->load->view('reports/summary_pdf',$data,true);   
            }else{
               $html = $this->load->view('reports/summary_report',$data,true);  
            }   
            $pdf->SetTitle('Summary list');
            $pdf->SetHeaderMargin(30);
            $pdf->SetTopMargin(20);
            $pdf->setFooterMargin(20);
            $pdf->SetAutoPageBreak(true,20);
            $pdf->SetAuthor('Author');
            $pdf->SetDisplayMode('real', 'default');
            $pdf->WriteHTML($html);          
            $pdf->Output('Summary_list.pdf', 'D'); 

    }

    public function excelsummary_download_sess(){
         $fromdate=$_SESSION['from_date'];
        $todate=$_SESSION['to_date'];
        $summary_option=$_SESSION['summary_option'];
        if($summary_option=='all'){
        $data['client_details']=$this->Report_model->summary_user_detailsWithDate($fromdate,$todate);      
        $data['task_details']=$this->Report_model->summary_task_detailsWithDate($fromdate,$todate);   
        $data['lead_details']=$this->Report_model->summary_lead_detailsWithDate($fromdate,$todate); 
        }else if($summary_option=='user'){
            $data['client_details']=$this->Report_model->summary_user_detailsWithDate($fromdate,$todate); 
        }else if($summary_option=='task'){
            $data['task_details']=$this->Report_model->summary_task_detailsWithDate($fromdate,$todate); 
        }else{
              $data['lead_details']=$this->Report_model->summary_lead_detailsWithDate($fromdate,$todate); 
        }

         $filename = 'Summary_'.date('Ymd').'.xlsx';           
           // $data['client']=$this->Report_model->summary_client_details();  
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/csv; "); 
            $file = fopen('php://output', 'w'); 

         if($_SESSION['summary_option']=='all'){
            $data['client']=$this->Report_model->summary_user_detailsWithDate($fromdate,$todate);
            $header = array("S.NO","Name","Updates","Active","CompanyStatus","User Type","Status");
            fputcsv($file, $header);
            $i=0;  
              foreach ($data['client'] as $key=>$line){
                $user_id=$line['id'];
            if($line['id']){ $i++; $line['id']=$i; }    
            $update=$this->Common_mdl->select_record('update_client','user_id',$user_id);
            $company_status=$this->Common_mdl->select_record('client','user_id',$user_id);
            $getallUservalue=$this->Common_mdl->select_record('user','id',$user_id);
            $role=$this->Common_mdl->select_record('Role','id',$getallUservalue['role']);
            $line['crm_name']=$getallUservalue['crm_name'];
            if($update['counts']!=''){
               $line['updates']= $update['counts']; 
            } else{
               $line['updates']='0';
            }
            if($getallUservalue['status']=='1'){ 
                 $line['status']="Active"; }
                  elseif($getallUservalue['status']=='0'){ 
                     $line['status']="Inactive"; 
                 }elseif($getallUservalue['status']=='3'){
                   $line['status']="Frozen"; 
               }else{ 
                 $line['status']='-';
            }

            $line['crm_company_status']=$company_status['crm_company_status'];
            $line['role']=$role['role'];

            if($getallUservalue['role']=='6'){
            $line['house']='Manual';
            }elseif($getallUservalue['role']=='4' && $company_status['status']==0 ){
             $line['house']='Manual';
            }elseif($getallUservalue['role']=='4' && $company_status['status']==1){
             $line['house']='Company House';
            }elseif($getallUservalue['role']=='4' && $company_status['status']==2){
             $line['house']='Import';
            }else{
                $line['house'] ='';
            }
           
              fputcsv($file,$line);
            } 

            $data['task_list']=$this->Report_model->summary_task_detailsWithDate($fromdate,$todate);
            $header = array("S.NO","Task Name","Start Date","Due Date","Status","Assignto");
            fputcsv($file, $header);
            $i=0;  
            $team_array=array();
            $dept_array=array();
            $assignto=array();
              foreach ($data['task_list'] as $key=>$line){ 

                  if($line['id']){ $i++; $line['id']=$i; }
                    if(isset($line['worker'])){
                     $explode_worker=explode(',',$line['worker']);
                        foreach($explode_worker as $key => $val){     
                        $getUserProfilepic = $this->Common_mdl->getUserProfileName($val);
                        array_push($assignto,$getUserProfilepic);
                        }                      
                    }
                    if(isset($value['team'])){
                     $explode_team=explode(',',$line['team']);
                     for($v=0;$v<count($explode_team);$v++){
                        $team = $this->db->query("select * from team where id=".$explode_team[$v]." ")->row_array();
                        array_push($team_array,$team['team']);
                     } 
                    }
                    if(isset($value['department'])){
                     $explode_department=explode(',',$line['department']);
                     for($z=0;$z<count($explode_department);$z++){
                       $department=$this->db->query("select * from department_permission where id=".$explode_department[$z]." ")->row_array();
                       array_push($dept_array,$department['new_dept']);
                     }                    
                    }
                    $line['worker']= implode(',',$assignto);
                    $line['team']= implode(',',$team_array);
                    $line['department']= implode(',',$dept_array);
                   fputcsv($file,$line);
                   $assignto=array(); 
                   $team_array=array();
                   $dept_array=array();
                    } 


            $data['lead_list']=$this->Report_model->summary_lead_detailsWithDate($fromdate,$todate);

            $header = array("S.NO","Lead Name","Company Name","Email","Assigned","Status");
            fputcsv($file, $header);
            $i=0;  
            foreach ($data['lead_list'] as $key=>$line){                  
              if($line['id']){ $i++; $line['id']=$i; }
              $getUserProfilepic=array();
              $ex_assign=explode(',',$line['assigned']);
              foreach ($ex_assign as $ex_key => $ex_value) {
              $getUserProfilepic[] = $this->Common_mdl->getUserProfileName($ex_value);
              }
              if(is_numeric($line['company']))
              {
                $client_query=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and id='".$line['company']."' order by id desc ")->result_array();
              if(count($client_query)>0)
              {
                 $line['company']=$client_query[0]['crm_company_name'];
              }
              else
              {
                 $line['company']='-';
              }
              }
              else
              {
                 $line['company']=$line['company'];
              }              
              $line['assigned']=implode(',',$getUserProfilepic);
              $line['lead_status']=$this->Report_model->lead_status($line['lead_status']);
              fputcsv($file,$line);
              $getUserProfilepic=array();              
            }
            fclose($file);
            exit; 
         }else if($_SESSION['summary_option']=='user'){

            $data['client']=$this->Report_model->summary_user_detailsWithDate($fromdate,$todate);
            $header = array("S.NO","Name","Updates","Active","CompanyStatus","User Type","Status");
            fputcsv($file, $header);
            $i=0;  
              foreach ($data['client'] as $key=>$line){
                $user_id=$line['id'];
            if($line['id']){ $i++; $line['id']=$i; }    
            $update=$this->Common_mdl->select_record('update_client','user_id',$user_id);
            $company_status=$this->Common_mdl->select_record('client','user_id',$user_id);
            $getallUservalue=$this->Common_mdl->select_record('user','id',$user_id);
            $role=$this->Common_mdl->select_record('Role','id',$getallUservalue['role']);
            $line['crm_name']=$getallUservalue['crm_name'];
            if($update['counts']!=''){
               $line['updates']= $update['counts']; 
            } else{
               $line['updates']='0';
            }
            if($getallUservalue['status']=='1'){ 
                 $line['status']="Active"; }
                  elseif($getallUservalue['status']=='0'){ 
                     $line['status']="Inactive"; 
                 }elseif($getallUservalue['status']=='3'){
                   $line['status']="Frozen"; 
               }else{ 
                 $line['status']='-';
            }

            $line['crm_company_status']=$company_status['crm_company_status'];
            $line['role']=$role['role'];

            if($getallUservalue['role']=='6'){
            $line['house']='Manual';
            }elseif($getallUservalue['role']=='4' && $company_status['status']==0 ){
             $line['house']='Manual';
            }elseif($getallUservalue['role']=='4' && $company_status['status']==1){
             $line['house']='Company House';
            }elseif($getallUservalue['role']=='4' && $company_status['status']==2){
             $line['house']='Import';
            }else{
                $line['house'] ='';
            }           
              fputcsv($file,$line);
            } 
            fclose($file);
            exit; 
         }else if($_SESSION['summary_option']=='task'){


            $data['task_list']=$this->Report_model->summary_task_detailsWithDate($fromdate,$todate);  
            $header = array("S.NO","Task Name","Start Date","Due Date","Status","Assignto");
            fputcsv($file, $header);
            $i=0;  
            $team_array=array();
            $dept_array=array();
            $assignto=array();
              foreach ($data['task_list'] as $key=>$line){ 

                  if($line['id']){ $i++; $line['id']=$i; }
                    if(isset($line['worker'])){
                     $explode_worker=explode(',',$line['worker']);
                        foreach($explode_worker as $key => $val){     
                        $getUserProfilepic = $this->Common_mdl->getUserProfileName($val);
                        array_push($assignto,$getUserProfilepic);
                        }                      
                    }
                    if(isset($value['team'])){
                     $explode_team=explode(',',$line['team']);
                     for($v=0;$v<count($explode_team);$v++){
                        $team = $this->db->query("select * from team where id=".$explode_team[$v]." ")->row_array();
                        array_push($team_array,$team['team']);
                     } 
                    }
                    if(isset($value['department'])){
                     $explode_department=explode(',',$line['department']);
                     for($z=0;$z<count($explode_department);$z++){
                       $department=$this->db->query("select * from department_permission where id=".$explode_department[$z]." ")->row_array();
                       array_push($dept_array,$department['new_dept']);
                     }                    
                    }
                    $line['worker']= implode(',',$assignto);
                    $line['team']= implode(',',$team_array);
                    $line['department']= implode(',',$dept_array);
                   fputcsv($file,$line);
                   $assignto=array(); 
                   $team_array=array();
                   $dept_array=array();
                    } 

            fclose($file);
            exit; 

         }else{

                 $data['lead_list']=$this->Report_model->summary_lead_detailsWithDate($fromdate,$todate);

            $header = array("S.NO","Lead Name","Company Name","Email","Assigned","Status");
            fputcsv($file, $header);
            $i=0;  
            foreach ($data['lead_list'] as $key=>$line){                  
              if($line['id']){ $i++; $line['id']=$i; }
              $getUserProfilepic=array();
              $ex_assign=explode(',',$line['assigned']);
              foreach ($ex_assign as $ex_key => $ex_value) {
              $getUserProfilepic[] = $this->Common_mdl->getUserProfileName($ex_value);
              }
              if(is_numeric($line['company']))
              {
                $client_query=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and id='".$line['company']."' order by id desc ")->result_array();
              if(count($client_query)>0)
              {
                 $line['company']=$client_query[0]['crm_company_name'];
              }
              else
              {
                 $line['company']='-';
              }
              }
              else
              {
                 $line['company']=$line['company'];
              }              
              $line['assigned']=implode(',',$getUserProfilepic);
              $line['lead_status']=$this->Report_model->lead_status($line['lead_status']);
              fputcsv($file,$line);
              $getUserProfilepic=array();              
            }
            fclose($file);
            exit; 
         }
    }

    public function invoice_chart(){
         if(isset($_POST['invoice_image']))
            {
                $this->session->set_userdata('invoice_image',$_POST['invoice_image']);            
            }
    }

    public function invoice(){
        $data['invoice']=$this->Report_model->invoice_details();
            $this->load->library('Tc');
            $pdf = new Tc('P', 'mm', 'A4', true, 'UTF-8', false); 
            $pdf->AddPage('L','A4');
            $html = $this->load->view('reports/invoice_pdf',$data,true);      
            $pdf->SetTitle('Invoice list');
            $pdf->SetHeaderMargin(30);
            $pdf->SetTopMargin(20);
            $pdf->setFooterMargin(20);
            $pdf->SetAutoPageBreak(true);
            $pdf->SetAuthor('Author');
            $pdf->SetDisplayMode('real', 'default');
            $pdf->WriteHTML($html);          
            $pdf->Output('Invoice_list.pdf', 'D'); 
    }

    public function excelinvoice_download(){
           $filename = 'invoice_'.date('Ymd').'.xlsx';           
           $data['invoice']=$this->Report_model->invoice_details();
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/csv; "); 
            $file = fopen('php://output', 'w'); 
            $header = array("S.NO","Invoice No","Client Email","Invoice Amount","Payment Status","Invoice date");
            fputcsv($file, $header);          
            if(count($data['invoice'])){
                 $s=1;
                foreach($data['invoice'] as $inv){
                $client_id=$inv['client_id'];
                $amount_details=$this->db->query("select grand_total,payment_status from amount_details where client_id='".$client_id."'")->row_array();
                $user_details=$this->db->query("select crm_email_id from user where id='".$inv['client_email']."'")->row_array();
                 if($inv['client_id']){ $s++; $value['client_id']=$s; } 
                 $value['invoice_no']=$inv['invoice_no'];
                 $value['crm_email_id']=$user_details['crm_email_id'];
                 $value['invoice_amount']=$amount_details['grand_total'];
                 $value['status']=$amount_details['payment_status'];
                 $value['created_at']=date('d-m-y',$inv['created_at']);
                    fputcsv($file,$value);
                }
             }else{
                $value="No records Found";
                fputcsv($file,$value);
             }
              fclose($file);
             exit; 
        }

        public function invoice_customization(){
            $last_Year_start=$_POST['fromdate'];
            $last_month_end=$_POST['todate'];
            $status=$_POST['status'];
            $this->session->set_userdata('fromdate',$last_Year_start);
            $this->session->set_userdata('todate',$last_month_end);
            $this->session->set_userdata('status',$status); 

            if($status!='all'){
            if($status=='Expired'){
               $data['expired_invoice']=$this->Report_model->expired_count($last_Year_start,$last_month_end);
               $data['approved_invoice']['invoice_count']=0;
               $data['cancel_invoice']['invoice_count']=0;
            }else{
                $data['expired_invoice']['invoice_count']=0;
                $data['approved_invoice']=$this->Report_model->others_count($last_Year_start,$last_month_end,'approve');
                 $data['cancel_invoice']=$this->Report_model->others_count($last_Year_start,$last_month_end,'decline');
            }
            $this->load->view('reports/invoice_success',$data);
        }else{
             $status='1';
              echo $status;
        }
        }

          public function invoice_chart_sess(){
         if(isset($_POST['invoice_image_sess']))
            {
                $this->session->set_userdata('invoice_image_sess',$_POST['invoice_image']);            
            }
            echo $_SESSION['invoice_image_sess'];
     }

    public function invoice_sess(){
         $last_Year_start=$_SESSION['fromdate'];
         $last_month_end=$_SESSION['todate'];
         $status=$_SESSION['status'];
        if($status!='all'){
            if($status=='Expired'){
               $data['invoice']=$this->Report_model->invoice_details_check();             
            }else{                
                 $data['invoice']=$this->Report_model->others_details($last_Year_start,$last_month_end,$status);
            }          
        }else{
                $data['invoice']=$this->Report_model->others_details($last_Year_start,$last_month_end,$status);
        }
        $this->load->library('Tc');
            $pdf = new Tc('P', 'mm', 'A4', true, 'UTF-8', false); 
            $pdf->AddPage('L','A4');
            if($_SESSION['status']=='all'){ 
            $html = $this->load->view('reports/invoice_pdf',$data,true);  
            }else{
               $html = $this->load->view('reports/invoice_report',$data,true);       
            } 
            $pdf->SetTitle('Invoice list');
            $pdf->SetHeaderMargin(30);
            $pdf->SetTopMargin(20);
            $pdf->setFooterMargin(20);
            $pdf->SetAutoPageBreak(true);
            $pdf->SetAuthor('Author');
            $pdf->SetDisplayMode('real', 'default');
            $pdf->WriteHTML($html);          
            $pdf->Output('Invoice_list.pdf', 'D'); 
    }

    public function excelinvoice_download_sess(){


            $filename = 'invoice_'.date('Ymd').'.xlsx'; 
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/csv; "); 
            $file = fopen('php://output', 'w'); 
            $header = array("S.NO","Invoice No","Client Email","Invoice Amount","Payment Status","Invoice date");
            fputcsv($file, $header);  
            $last_Year_start=$_SESSION['fromdate'];
            $last_month_end=$_SESSION['todate'];
            $status=$_SESSION['status'];
            if($status!='all'){
            if($status=='Expired'){
             $data['invoice']=$this->Report_model->invoice_details_check();             
            }else{                
                $data['invoice']=$this->Report_model->others_details($last_Year_start,$last_month_end,$status);
            }          
            }else{
                $data['invoice']=$this->Report_model->others_details($last_Year_start,$last_month_end,$status);
            }

            if(count($data['invoice'])){
                 $s=1;
                foreach($data['invoice'] as $inv){
                $client_id=$inv['client_id'];
                $amount_details=$this->db->query("select grand_total,payment_status from amount_details where client_id='".$client_id."'")->row_array();
                $user_details=$this->db->query("select crm_email_id from user where id='".$inv['client_email']."'")->row_array();
                if($amount_details['payment_status']==$_SESSION['status']){
                 if($inv['client_id']){ $s++; $value['client_id']=$s; } 
                 $value['invoice_no']=$inv['invoice_no'];
                 $value['crm_email_id']=$user_details['crm_email_id'];
                 $value['invoice_amount']=$amount_details['grand_total'];
                 $value['status']=$amount_details['payment_status'];
                 $value['created_at']=date('d-m-y',$inv['created_at']);
                    fputcsv($file,$value);
                }
                }
             }else{
                $value="No records Found";
                fputcsv($file,$value);
             }
              fclose($file);
             exit; 
    }

    public function getColumns()
    {
        $other_services = $this->db->query('SELECT * FROM service_lists WHERE id>16')->result_array();               

        $columns = array('1'=> 'conf_statement','2' =>'accounts','3'=>'company_tax_return','4'=>'personal_tax_return','5' => 'vat','6' => 'payroll','7' => 'workplace', '8' => 'cis', '9' => 'cissub','10'=>'bookkeep','11' => 'p11d','12' => 'management','13' => 'investgate','14'=>'registered','15'=>'taxinvest','16' => 'taxadvice');

        foreach($other_services as $key => $value) 
        { 
           $columns[$value['id']] = $value['services_subnames'];
        }

        $columns_check = array('1'=> 'crm_confirmation_statement_due_date','2' =>'crm_ch_accounts_next_due','3'=>'crm_accounts_tax_date_hmrc','4'=>'crm_personal_due_date_return','5' => 'crm_vat_due_date','6' => 'crm_rti_deadline','7' => 'crm_pension_subm_due_date','8' =>'crm_registered_renew_date','9' =>'','10'=>'crm_next_booking_date','11' => 'crm_next_p11d_return_due','12' => 'crm_next_manage_acc_date','13' => 'crm_insurance_renew_date','14'=>'crm_registered_renew_date','15'=>'crm_investigation_end_date','16' => 'crm_investigation_end_date');                  

        foreach($other_services as $key => $value) 
        {
           $columns_check[$value['id']] = 'crm_'.$value['services_subnames'].'_statement_date';
        }

        $data['columns'] = $columns;
        $data['columns_check'] = $columns_check;

        return $data;
    }

}
?>
