<?php
require "vendor/autoload.php";
use Aws\S3\S3Client;
use Aws\Ses\SesClient;
use Aws\Ses\Exception\SesException;
use Aws\Exception\AwsException;

class AwsMailer extends CI_Controller{
	function __construct(){
		$this->baseDomainName = 'accotax.co.uk'; //$baseDomainName;
        $this->fromEmail      = 'reminders@accotax.co.uk'; //$fromEmail;
        //$this->logger         = is_null($logger) ? new NullLogger() : $logger;
        require_once(APPPATH.'controllers/NotificationStatus.php');
        $config               = [
            'version'     => '2010-12-01',
            'region'      => 'eu-west-2',
            'credentials' => [
                'key'    => AWS_KEY, //$awsAccessKey,
                'secret' => AWS_SECRET, //$awsAccessSecret,
            ],
        ];
        $this->sesClient = SesClient::factory($config);
	}	

	public function index(){
	
	}

	public function ajax_email(){                
        $IsSend             = $this->prepare_email_to_send($_POST);
        $NotificationStatus = new NotificationStatus();
        
        echo $NotificationStatus->change_status($IsSend, $_POST['record_key']);
        die();
    }

    public function prepare_email_to_send($data=[], $decode=true){
        $email_to      = json_decode($data['email_to'], true);
        $email_subject = $data['email_subject'];
        $email_body    = ($decode == true) ? base64_decode($data['email_body']) : $data['email_body'];
        $cc            = isset($email_to['cc']) ? $email_to['cc'] : '';
        $tos           = isset($email_to['to']) ? $email_to['to'] : '';
        
        if (!is_array($tos)){
            $to_array[] = $tos;
        }else{ 
            $to_array = $tos;
        } 

        $response = [];

        foreach($to_array as $arr_key => $to){
            if(get_instance()->config->item('environment') != 'production')
            {
                // $to     = "iitb.riteshag@gmail.com";
                $to     = "sourav.skr11@gmail.com";
                $cc     = "";
            }
            // $to     = "iitb.riteshag@gmail.com";
            // $cc     = "";
            $result = $this->send_email($email_subject, $to, $cc, $email_body);     
            if($arr_key == 0){
                $response = $result;
            }
        }
        return $response;
    }

    public function send_email($subject, $toEmail, $ccEmail, $message){
        $message = str_replace('__DOMAIN__', $this->baseDomainName, $message);
        /*return $this->sendWithTemplate(
            'emails.default',
            $subject,
            $toEmail,
            [
                'message' => $message,
            ]
        );*/
        return $this->sendRaw($subject, $toEmail, $ccEmail, $message);
    }

    public function sendWithTemplate($templateName, $subject, $toEmail, $ccEmail, array $templateData){
        $templateData = array_merge(
            [
                'title' => $subject,
                'newsList' => newsList(),
            ],
            $templateData
        );
        return $this->sendRaw($subject, $toEmail, $ccEmail, view($templateName, $templateData));
    }

    public function sendRaw($subject, $toEmail, $ccEmail, $htmlContent){
        /**
         * @see https://docs.aws.amazon.com/ses/latest/DeveloperGuide/send-using-sdk-php.html
         */
        $charSet = 'UTF-8';
        $messageId = 0;
        try {
            if($ccEmail != "" && $ccEmail != null){
                $result = $this->sesClient->sendEmail([
                    'Destination' => [
                        'ToAddresses' => [$toEmail],
                        'CcAddresses' => [$ccEmail],
                    ],
                    'ReplyToAddresses' => [$this->fromEmail],
                    'Source' => $this->fromEmail,
                    'Message' => [
                        'Body' => [
                            'Html' => [
                                'Charset' => $charSet,
                                'Data' => $htmlContent,
                            ],
                            'Text' => [
                                'Charset' => $charSet,
                                'Data' => strip_tags($htmlContent),
                            ],
                        ],
                        'Subject' => [
                            'Charset' => $charSet,
                            'Data' => $subject,
                        ],
                    ],
                ]);
            }else{
                $result = $this->sesClient->sendEmail([
                    'Destination' => [
                        'ToAddresses' => [$toEmail],
                    ],
                    'ReplyToAddresses' => [$this->fromEmail],
                    'Source' => $this->fromEmail,
                    'Message' => [
                        'Body' => [
                            'Html' => [
                                'Charset' => $charSet,
                                'Data' => $htmlContent,
                            ],
                            'Text' => [
                                'Charset' => $charSet,
                                'Data' => strip_tags($htmlContent),
                            ],
                        ],
                        'Subject' => [
                            'Charset' => $charSet,
                            'Data' => $subject,
                        ],
                    ],
                ]);
            }

            $messageId = $result['MessageId'];
            
        } catch (SesException $ex) {
            $response = [
                "result"  => 2,
                "message" => sprintf("Email cannot be sent to [%s] due to aws error : '%s'", $toEmail, $ex)
            ];
            return $response;
            //return sprintf("Email cannot be sent to [%s] due to aws error : '%s'", $toEmail, $ex);
        } catch (Exception $ex) {
            $response = [
                "result"  => 2,
                "message" => sprintf("Email cannot be sent to [%s] due to error : '%s'", $toEmail, $ex->getMessage())
            ];
            return $response;
            //return sprintf("Email cannot be sent to [%s] due to error : '%s'", $toEmail, $ex->getMessage());
        }

        if (intval($messageId) === 0) {
            $response = [
                "result"  => 2,
                "message" => sprintf("Email cannot be sent to [%s] due to some error.", $toEmail)
            ];
            return $response;
            // $this->logger->error(sprintf("Email cannot be sent to [%s] due to some error.", $toEmail), [__CLASS__]);
            //return sprintf("Email cannot be sent to [%s] due to some error.", $toEmail);            
        }
        $response = [
            "result"  => 1,
            "message" => "Email Sent Successfully"
        ];
        return $response;
        //$this->logger->debug(sprintf("Email sent to [%s]. AWS message id [%s]", $toEmail, $messageId), [__CLASS__]);
        //return "Email Sent Successfully";
    }
} ?>	