<?php
//error_reporting(-1);
defined('BASEPATH') or exit('No direct script access allowed');

class Client extends CI_Controller
{
	public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";

	public function __construct()
	{
		parent::__construct();
		require_once(APPPATH.'controllers/AwsMailer.php');
		$this->load->model(array('Common_mdl', 'CompanyHouse_Model', 'Client_model', 'Service_Reminder_Model', 'Security_model', 'Task_invoice_model', 'Task_creating_model', 'Proposal_model', 'FirmFields_model'));

		$this->load->helper(['template_decode', 'firm_columnSettings']);
	}

	public function index($mode, $client_id = '')
	{
		$this->Security_model->chk_login();
		$this->load->view('clients/client_template_view.php');
	}

	public function get_client_list_source()
	{
		$this->Client_model->list_get_source();
	}

	public function get_column_filter_data($table, $column)
	{
		if (!empty($_POST['client_user_id'])) {
			$data = $this->Client_model->get_column_filters($table, $column);
			echo json_encode($data);
		}
	}

	public function statusChange()
	{
		$this->Security_model->chk_login();

		$id      = $this->input->post('rec_id');
		$status  = $this->input->post('status');
		if ($id != '' && $status != '') {
			$this->Client_model->change_client_status($id, $status);
		}
		$data   = $this->Client_model->get_assigned_client_status_count();
		echo json_encode($data);
	}
	public function bulk_archive_unarchive()
	{
		$id      = $this->input->post('id');
		$status  = $this->input->post('status');

		if ($id != '' && $status != '') {
			echo $this->Client_model->bulk_archive_unarchive($id, $status);
		}
	}
	public function SendClient_missingDetails()
	{
		$this->Security_model->chk_login();

		$decode['body']     = $_POST['body'];
		$decode['subject']  = $_POST['subject'];
		$decoded = Decode_ClientTable_Datas($_POST['client_id'], $decode);

		$res = firm_settings_send_mail($_SESSION['firm_id'], $_POST['mail_id'], $decoded['subject'], $decoded['body']);

		if ($res['result']) {
			$log     = "<p>" . strip_tags($decoded['subject']) . " - Email Send By <i>" . $_SESSION['crm_name'] . "</i></p>";

			$user_id = $this->Common_mdl->get_field_value('client', 'user_id', 'id', $_POST['client_id']);
			$this->Common_mdl->Create_Log('client', $user_id, $log);
		}
		echo json_encode($res);
	}
	public function send_draft_reminder()
	{
		$res['result'] = 0;
		if (!empty($_POST['id'])) {
			$res['result']  = $this->Client_model->send_draft_reminder($_POST['id']);
			$res['id']      = $this->input->post('id');
		}
		echo  json_encode($res);
	}
	public function delete_service_reminder()
	{
		$res['result'] = 0;
		if (!empty($_POST['ids']) && !empty($_POST['table'])) {
			$res['result']  = $this->Client_model->delete_reminders();
		}
		echo  json_encode($res);
	}
	public function SaveClientTimelineNotes()
	{
		//print_r( $_POST );
		$data = $this->Client_model->SaveTimelineNotes();
		echo  json_encode($data);
	}
	public function DeleteClientTimelineNotes()
	{
		if (!empty($this->input->post('note_id'))) {
			$data = $this->Client_model->DeleteTimelineNotes($this->input->post('note_id'));
			echo  json_encode($data);
		}
	}
	public function getClientTimelineNoteDetails()
	{
		if (!empty($this->input->post('note_id'))) {
			$note_id  = $this->input->post('note_id');
			$data     = $this->db->get_where('timeline_services_notes_added', 'id=' . $note_id)->row_array();
			echo  json_encode($data);
		}
	}
	public function myuse($query /*$date_str , $u , $rel $id  /*$query ,$Is_company=0*/ /*$firm_id*/)
	{
		echo "<pre>";
		print_r($this->CompanyHouse_Model->Get_Specific_Officer('/officers/' . $query . '/appointments'));
		/*$data = $this->CompanyHouse_Model->Get_Company_Details( $cn );
      print_r( $data );*/

		/*$this->Service_Reminder_Model->check_auto_task_status_update_reminders( $firm_id );*/
		/*echo "<pre>";
      if( $Is_company )
      print_r( $this->CompanyHouse_Model->Get_Company_Details( $query ) );

      print_r( $this->CompanyHouse_Model->Get_Specific_Officer('/officers/'.$query.'/appointments' ) ); */
		/* echo "<pre>";
      print_r( $this->Client_model->get_client_details( $id ) );*/

		/*$query = "SELECT c.id as client_ids,cc.id as client_contacts_id,cc.is_ptr_as_per_firm_reminder as default_r,cc.is_ptr_cus_reminder as custom_r,cc.personal_tax_return_date as due_date,'Annually' as 'duration','4' as service_id 
        FROM client_contacts AS cc 
        INNER JOIN client as c on c.user_id = cc.client_id    
        WHERE cc.personal_tax_return_date!='' and cc.has_ptr_service=1";
      
      $cc = $this->db->query( $query )->result_array();

      foreach ($cc as $key => $value) {

        $fields = [ 'default_r'=>1,'custom_r'=>1,'due_date'=>1,'duration'=>1,'service_id'=>1 ];
        $service_data = array_intersect_key( $value , $fields );
        echo "client_id=".$value['client_ids']."\n";
        echo "client_contacts_id=".$value['client_contacts_id']."\n";

        echo "<pre>service_data\n";
        print_r( $service_data );
        echo "\n======================\n";
        $this->Service_Reminder_Model->generate_reminders( $value['client_ids'] , 4 , $service_data , $value['client_contacts_id'] );
      }*/

		/*$this->load->model('service_model');
      $service = $this->service_model->services_fields;
      print_r( $service );
      echo "field==".$service[1];*/

		/*$test = [''=>"test"];
      echo $test[''];
      print_r( $test );*/
		/*$data = $this->service_model->get_defualt_service_assignee_node_id();
      print_r( $data );*/
		//$this->load->view('test');
		//echo "test ";
		//$this->load->view('test');
		/*$rows  = $this->db->query("SELECT * FROM `tblcustomfieldsvalues` where section_name='Contact_Person' and relid not like '%--%'")->result_array();
      foreach ( $rows as $key => $value ) {
        
        $data = $this->db->query("SELECT id FROM `client_contacts` where client_id=".$value['relid']." order by id desc LIMIT 1")->row_array();

        $relid = $value['relid'].'--'.$data['id'];

        $this->db->update('tblcustomfieldsvalues' , [ 'relid' => $relid ] , "id=".$value['id'] );
      }*/

		/*$data = [];
      foreach ($data as $v) {
        echo "data";
      }*/



		/*$data = $this->Service_Reminder_Model->Get_Service_ReminderFields;
      print_r($data);
      die;
        $this->load->helper('date');
        echo add_relative_date( $date_str , $u.$rel );
        die;*/

		/*$d=new DateTime('29-02-2020 + 1 year');
      echo $d->format('Y-m-d');die;
      echo date('d-m-Y' , strtotime('+ 1 year' , strtotime('29-02-2020') ) );
      die;
      echo "=========";
      echo date_format( date_create_from_format( 'd-m-Y','09-12-2019'  ) , 'U' );
      die;
      $this->Service_Reminder_Model->update_record_creation_date( 1761 , 5);*/
		/*echo date('Y-m-d' , strtotime('yesterday'));
      $content['body']         = "This is content";
      $content['attachment'][] = FCPATH.'attachment/cat22.png';
      $content['attachment'][] = FCPATH.'attachment/cat23.png';
      
      print_r ( firm_settings_send_mail( 204 , 'majith.php@gmail.com','Attachment Test' , $content ) );*/

		/*if( preg_match('/<html>/', "<html>" ) )
      {
        echo "inside -1";
      }
      else
      {
        echo "inside else par";
      }*/

		/*$data = $this->db->select('id')->get_where('client','firm_id=258')->result_array();
      echo implode(',', array_column($data, 'id'));   */

		/*echo "<pre>SESSION";

      print_r( $_SESSION );
      
      echo "===================";

      echo "COOKIE";
      print_r( $_COOKIE );
      die;*/

		/*echo "<pre>";
      $data = $this->CompanyHouse_Model->Get_Company_Details( $cn );
      print_r( $data );*/

		/*echo "==================================";
      print_r( $this->CompanyHouse_Model->Find_NewData_Updates( "1744" ) );*/
		//print_r( $this->CompanyHouse_Model->Get_Specific_Officer( 05266924 ) );
		/*$this->load->model('User_management');
      
      $this->User_management->Get_FirmStaffs();*/

		//echo "<pre>";
		//echo implode (',' , Get_Assigned_Datas('CLIENT') );
		//$this->load->view('users/blank_page');


		/*$datas = $this->db->get_where('firm_column_settings','table_name="staff_list"')->result_array();

      $keys = array_slice(array_keys($datas[0]), 6);
      $keys = array_chunk( $keys , 3 );
      
      foreach ($keys as $k)
      {
        //print_r($k);

        foreach ($datas as $key => $value)
        {
          $this->db->update('firm_column_settings',[$k[0]=>$value['visible_firm0'],$k[1]=>$value['order_firm0']],'id='.$value['id']);
        }  
      }*/

		/*$datas = $this->Common_mdl->GetAllWithExceptFieldOrder("client_contacts","url_slug",'','ASC');
      $data = $this->CompanyHouse_Model->Get_Specific_Officer($value['url_slug'] );//

      echo "<pre>count".count($datas);
      //print_r( $datas );
      
      foreach ($datas as $key => $value)
      {
        
        if(!empty( $data['response']['date_of_birth'] ))
        {
          print_r( $data['response']['date_of_birth'] );  
          echo "======";
        }
       
        //$i++;
        //if($i > 10) break;
      }*/
	}
	public function Get_ClientLimit()
	{
		$this->Security_model->chk_login();
		$res['result'] = $this->Common_mdl->Check_Firm_Clients_limit($_SESSION['firm_id']);
		echo json_encode($res);
	}
	public function Client_Reassign()
	{
		$this->Security_model->chk_login();

		$assignees = implode(',', $_POST['assignees']);

		if (!empty($_POST['user_ids']) && !empty($_POST['assignees'])) {
			foreach ($_POST['user_ids'] as $key => $value) {
				$client = $this->Common_mdl->select_record('client', 'user_id', $value);
				$this->Common_mdl->add_assignto($assignees, $client['id'], 'CLIENT', true);

				$log = "<p>Client Reassigned By <i>" . $_SESSION['crm_name'] . "</i></p>";

				$this->Common_mdl->Create_Log('client', $value, $log);

				if ($_POST['task_also'] == "1") {
					$tasks = $this->Common_mdl->GetAllWithWhereAndFilter('add_new_task', 'company_name', $client['id'], " AND task_status NOT IN (4,5)");



					foreach ($tasks as $key => $value) {
						$this->Common_mdl->add_assignto($assignees, $value['id'], 'TASK', true);
					}
				}
			}
			echo json_encode(['result' => 1]);
		}
	}

	public function addnewclient($user_id = false)
	{
		$this->Security_model->chk_login();

		/*userid may come as query string*/
		$query_str = $this->input->get('id', TRUE);
		if (!empty($query_str)) {
			$user_id = $query_str;
		}

		//error_reporting(-1);
		$client_limit = $this->Common_mdl->Check_Firm_Clients_limit($_SESSION['firm_id']);


		if ($user_id && $_SESSION['permission']['Client_Section']['edit'] != 1) {
			$this->load->view('users/blank_page');
		} else if (empty($user_id) && !($client_limit > 0 || $client_limit == 'unlimited')) {
			$this->load->view('users/client_limit_exit');
		} else if ($_SESSION['permission']['Client_Section']['create'] != 1) {
			$this->load->view('users/blank_page');
		} else {
			$data = $this->Client_model->client_AddEdit_info($user_id);
			$data["email_temp"]     = $this->Common_mdl->getTemplates();
			$this->load->view('clients/client_details_view', $data);
		}
	}

	public function client_CH_sync()
	{
		if (!empty($this->input->post('ids'))) {
			echo $this->Client_model->sync_ch($this->input->post('ids'));
		}
	}

	public function client_info($user_id = false)
	{
		error_reporting(0);
		$this->Security_model->chk_login();
		$data['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user', 'role', '1');
		$data['client'] = $this->db->query('select * from client where user_id=' . $user_id)->row_array();
		$data['assignees_node'] = $this->db->query("select assignees from firm_assignees where module_name='CLIENT' and module_id=" . $data['client']['id'] . " and firm_id=" . $_SESSION['firm_id'])->result_array();
		$data['assignees_node'] = implode(',', array_column($data['assignees_node'], 'assignees'));
		$data['client_legalform'] = $this->db->query('select * from client_legalform where user_id=' . $user_id)->row_array();
		/*$data['client_invoice']=$this->Common_mdl->GetAllWithWhere('client_invoice','user_id',$user_id);
        $data['responsible_team']=$this->Common_mdl->GetAllWithWhere('responsible_team','client_id',$user_id);
        $data['responsible_user']=$this->Common_mdl->GetAllWithWhere('responsible_user','client_id',$user_id);
        $data['responsible_department']=$this->Common_mdl->GetAllWithWhere('responsible_department','client_id',$user_id);
        $data['responsible_member']=$this->Common_mdl->GetAllWithWhere('responsible_members','client_id',$user_id);*/
		//$data['contactRec']=$this->Common_mdl->GetAllWithWhere('client_contacts','client_id',$user_id);
		$data['contactRec'] = $this->db->query("SELECT * FROM  `client_contacts` WHERE client_id =  '" . $user_id . "' ORDER BY make_primary DESC ")->result_array();
		$data['user'] = $this->db->query('select * from user where id=' . $user_id)->row_array();
		//$data['referby'] = $this->db->query("SELECT * FROM `client` where crm_first_name!='' and crm_first_name!='0' GROUP BY `crm_first_name`")->result_array();
		/** 21-08-2018 **/
		$data['referby'] = $this->db->query("SELECT c.id,c.crm_first_name,c.crm_company_name FROM user as u  inner join client as c on c.user_id = u.id where u.firm_id = " . $_SESSION['firm_id'] . " and u.user_type = 'FC'")->result_array();

		/** end of 21-08-2018 **/
		// $data['teamlist']=$this->Common_mdl->getallrecords('team');
		// $data['deptlist']=$this->Common_mdl->getallrecords('department');
		// $data['staff_form'] = $this->Common_mdl->GetAllWithWheretwo('user','role','6','firm_admin_id',$_SESSION['id']);
		// $data['managed_by'] = $this->Common_mdl->GetAllWithWheretwo('user','role','5','firm_admin_id',$_SESSION['id']);
		// $data['staff_form'] =$this->db->query("select * from user where role in(5,6,3) and firm_admin_id=".$_SESSION['id']." ")->result_array();
		//$data['staff_form'] =$this->db->query("select * from user where firm_id=".$_SESSION['firm_id']." ")->result_array();
		//$data['managed_by'] = $this->db->query("select * from user where role in(5,3) and firm_admin_id=".$_SESSION['id']." ")->result_array();

		if ($user_id != '') {
			$data['task_list'] = $this->Common_mdl->GetAllWithWhere('add_new_task', 'user_id', $user_id);
		} else {
			$data['task_list'] = $this->Common_mdl->getallrecords('add_new_task');
		}

		//print_r($data['staff_form']);die;
		$data['reminder_templates'] = $this->Common_mdl->getReminderTemplates();


		//$data['new_department'] = $this->db->query( "SELECT * FROM department_permission WHERE new_dept!=''")->result_array();
		//$data['client_hashtag']=$this->Common_mdl->GetAllWithWhere('client_hashtag','user_id',$_SESSION['id']);

		$this->load->view('clients/client_details_view', $data);
	}
	public function delete_client()
	{
		$this->Security_model->chk_login();
		if (!empty($this->input->post('id'))) {
			echo $this->Client_model->delete_clients($this->input->post('id'));
		}
	}

	public function AddCustom_ServiceReminder()
	{
		$this->Security_model->chk_login();
		if (isset($_POST)) {
			$data['due_type'] = (isset($_POST['due']) && ($_POST['due'] != '')) ? $_POST['due'] : '';
			$data['due_days'] = (isset($_POST['days']) && ($_POST['days'] != '')) ? $_POST['days'] : '';
			$data['subject'] =  (isset($_POST['subject']) && ($_POST['subject'] != '')) ? $_POST['subject'] : '';
			$data['body'] =     (isset($_POST['message']) && ($_POST['message'] != '')) ? $_POST['message'] : '';
			$data['service_id'] = $_POST['service_id'];
			$data['firm_id'] = $_SESSION['firm_id'];
			if (!empty($_POST['user_id'])) {
				$client_id  = $this->Common_mdl->get_field_value('client', 'id', 'user_id', $_POST['user_id']);
				$data['client_id'] = $client_id;
			}
			$data['client_contacts_id'] = (!empty($_POST['client_contacts_id']) ? $_POST['client_contacts_id'] : '');
			$data['headline_color'] = $_POST['color'];
			$this->db->insert('custom_service_reminder', $data);
			$res['result'] = $this->db->insert_id();
			//don't need to call reminder creation function because it still not save reminder id to the field
			echo json_encode($res);
		}
	}

	public function Remove_CusServiceReminder()
	{
		if (!empty($_POST['id'])) {
			$this->db->query("delete from custom_service_reminder where id = " . $_POST['id']);
			$res['result'] = $this->db->affected_rows();
			echo json_encode($res);
		}
	}

	public function check_company_numberExist()
	{
		$this->Security_model->chk_login();
		$Ex_Con = " ";
		if (!empty($_POST['id'])) {
			$Ex_Con = " and user_id!=" . $_POST['id'];
		}
		$data = $this->db->query("select crm_company_number from client where firm_id = " . $_SESSION['firm_id'] . " and crm_company_number='" . $_POST['company_number'] . "' " . $Ex_Con)->row_array();
		if (!empty($data['crm_company_number'])) {
			echo "false";
		} else {
			echo "true";
		}
	}
	public function Get_NewClient_Refrral_ByName()
	{
		$res['code'] =  $this->Common_mdl->Get_ClientReferral_Code($_POST['terms']);
		echo json_encode($res);
	}

	public function check_client_reminders($id){
		$clients = $this->db->query('select id from client WHERE id='.$id)->result_array();
		foreach($clients as $c){
			$client_frequency = $this->db->query('SELECT client_id,service_id,firm_id FROM client_service_frequency_due WHERE client_id='.$c['id'])->result_array();
			$service_ids = array();
			foreach($client_frequency as $cf){
				$sr = $this->db->query('SELECT * FROM service_reminder_cron_data WHERE client_id='.$cf['client_id'].' AND firm_id='.$cf['firm_id'].' AND service_id='.$cf['service_id'])->result_array();
				if(empty($sr)){
					array_push($service_ids,$cf['service_id']);
				}
			}
			print_r($service_ids);
			$ser_str = "ser_" . $cf['service_id'];
			$squery = 'SELECT id FROM add_new_task WHERE company_name='.$cf['client_id'].' AND firm_id='.$cf['firm_id'].' AND related_to!="sub_task" AND (related_to_services="'.$cf['service_id'].'" OR related_to_services="'.$ser_str.'") AND (task_status=4 OR task_status=5)';
			$task = $this->db->query($squery)->result_array();
			if(!empty($task)){
				array_push($service_ids,$cf['service_id']);
			}
			print_r($service_ids);
			$squery1 = 'SELECT id FROM add_new_task WHERE company_name='.$cf['client_id'].' AND firm_id='.$cf['firm_id'].' AND related_to!="sub_task" AND (related_to_services="'.$cf['service_id'].'" OR related_to_services="'.$ser_str.'") AND (task_status!=4 AND task_status!=5)';
			$task1 = $this->db->query($squery1)->result_array();
			if(!empty($task1)){
				for($i=0;$i<count($service_ids);$i++){
					if(in_array($service_ids[$i],$task1)){
						unset($service_ids[$i]);
					}
				}
			}
			print_r($service_ids);
			if(!empty($service_ids)){
				$this->Service_Reminder_Model->Forced_Create_Client_ServiceReminders($c['id'], $service_ids);
			}
			print_r($service_ids);
		}
	}

	public function check_loop_reminders(){
		// $this->check_client_reminders(1308);
		// $this->check_client_reminders(1308);
		$i=1;
		$squery = 'SELECT id,crm_company_name FROM client WHERE cron_job_process=0 LIMIT 500';
		$client = $this->db->query($squery)->result_array();
		foreach($client as $c){
			$squery = 'SELECT id,firm_id,related_to_services,company_name FROM add_new_task  WHERE company_name='.$c['id'].' AND related_to!="sub_task"';
			$task = $this->db->query($squery)->result_array();
			foreach($task as $t){
				if($t['firm_id']!="" && $t['related_to_services']!="" && $t['company_name']!=""){
					$related_to_services=str_replace("ser_","",$t['related_to_services']);
					$today_date = date('Y-m-d');
					if(is_numeric($related_to_services))
					{
						$squery2="Select * from service_reminder_cron_data WHERE status=2 AND client_id=".$t['company_name']." AND service_id=".$related_to_services." AND firm_id=".$t['firm_id'];
						$service_reminder = $this->db->query($squery2)->result_array();
						array_unique($service_reminder);
						//print_r($task2);
						foreach($service_reminder as $t2){
							$sdate = date('Y-m-d',strtotime($t2['date']));
							if($sdate<$today_date){
								$service_reminder_cron_data_id = $t2['id'];
								$data =[
									'firm_id' 		=> $t2['firm_id'],
									'client_id' 	=> $t2['client_id'],
									'service_id' 	=> $t2['service_id'],
									'client_contacts_id' => $t2['client_contacts_id'],
									'template_id' 	=> $t2['template_id'],
									'template_type' => $t2['template_type'],
									'frequency'		=> $t2['frequency'],
									'date' 			=> $t2['date']
								];
								$this->db->insert( 'draft_service_reminder', $data );
								$this->db->query("Delete From service_reminder_cron_data WHERE id=".$service_reminder_cron_data_id);
							}
						}
						$squery1 = "Update service_reminder_cron_data SET status=0 WHERE status=2 AND client_id=".$t['company_name']." AND service_id=".$related_to_services." AND firm_id=".$t['firm_id'];
						$this->db->query($squery1);

						$squery3 = 'SELECT id,task_status,progress_status FROM add_new_task  WHERE id='.$t['id'];
						$t3 = $this->db->query($squery3)->row_array();
				
						if(($t3['task_status']=="1"||$t3['task_status']=="2"||$t3['task_status']=="3") && ($t3['progress_status']!=11)){
							$squery4 = "Update service_reminder_cron_data SET status=2 WHERE status=0 AND client_id=".$t['company_name']." AND service_id=".$related_to_services." AND firm_id=".$t['firm_id'];
							$this->db->query($squery4);
						}
					}
					//echo "Query executed successfully....." . '<br>';
				}
			}
			$update_data = 'UPDATE client SET cron_job_process=1 WHERE id='.$c['id'];
			$this->db->query($update_data);
			echo $i.") Completed for " . $c['crm_company_name'] . "(id:" . $c['id'] . ")" . '<br>';
			$i++;
		}
		echo "----------------Complete All------------------------";
	}

	public function insert_client()
	{
		
		$this->Security_model->chk_login();
		$old_client_data          = [];

		$data['crm_name']         = $this->input->post('company_name', NULL, 1);
		$data['username']         = $this->input->post('user_name', NULL, 1);

		$data['password']         = (!empty($_POST['password'])) ? md5($_POST['password']) : '';
		$data['confirm_password'] = $this->input->post('confirm_password', NULL, 1);

		$data['status']           = (!empty($_POST['company_status']) ? $_POST['company_status'] : 0);
		$data['CreatedTime']      = time();

		$data['company_roles']    = $this->input->post('company_house', NULL, 1);
		$data['crm_packages']     = $this->input->post('packages', NULL, 1);
		$data['crm_gender']       = $this->input->post('gender', NULL, 1);

		$data['autosave_status'] = 0;

		$data['firm_id'] = $_SESSION['firm_id'];
		$data['user_type'] = 'FC';

		if (isset($_POST['user_id']) && ($_POST['user_id'] == '0' || $_POST['user_id'] == '')) {
			$this->db->insert('user', $data);
			$_POST['user_id'] = $in = $this->db->insert_id();
		} else {
			$this->Common_mdl->update('user', $data, 'id', $_POST['user_id']);
			$in = $_POST['user_id'];
			$old_client_data = $this->db->query("select * from client where user_id=" . $_POST['user_id'])->row_array();
		}

		$data1['user_id']   = $_POST['user_id'];
		$data1['firm_id']   = $_SESSION['firm_id'];
		$_POST['pn_no_rec'] = $_POST['cun_code'] . "-" . $_POST['pn_no_rec'];
		$this->send_sms($_POST['pn_no_rec']);

		/*$other_services = $this->db->query('SELECT * FROM service_lists WHERE id>16')->result_array();

      foreach($other_services as $key => $value) 
      { 
          if(!empty($_POST[$value['services_subnames']]))
          {
            if(isset($_POST[$value['services_subnames']]['text']) && $_POST[$value['services_subnames']]['text']=='on')
            {
               $this->send_sms($_POST['pn_no_rec']);
            }
            $data1[$value['services_subnames']] =  json_encode($_POST[$value['services_subnames']]);  
          }
          else
          {
             $data1[$value['services_subnames']] = '';
          }

          $data1['crm_'.$value['services_subnames'].'_email_remainder'] = (isset($_POST['crm_'.$value['services_subnames'].'_email_remainder'])&&($_POST['crm_'.$value['services_subnames'].'_email_remainder']!=''))? $_POST['crm_'.$value['services_subnames'].'_email_remainder'] : '';
          $data1['crm_'.$value['services_subnames'].'_add_custom_reminder'] = (isset($_POST['crm_'.$value['services_subnames'].'_add_custom_reminder'])&&($_POST['crm_'.$value['services_subnames'].'_add_custom_reminder']!=''))? $_POST['crm_'.$value['services_subnames'].'_add_custom_reminder'] : '';
          $data1['crm_'.$value['services_subnames'].'_statement_date'] = (isset($_POST['crm_'.$value['services_subnames'].'_statement_date'])&&($_POST['crm_'.$value['services_subnames'].'_statement_date']!=''))? date("Y-m-d", strtotime($_POST['crm_'.$value['services_subnames'].'_statement_date'])) : '';
        }*/

		if (!empty($_POST['accounts'])) {
			if (isset($_POST['accounts']['text']) && $_POST['accounts']['text'] == 'on') {
				$this->send_sms($_POST['pn_no_rec']);
			}
			$data1['accounts'] = json_encode($_POST['accounts']);

			if (empty($_POST['service_manager'][2]))  $_POST['service_manager'][2]   = $_POST['assignees_values'];
			if (empty($_POST['service_assignee'][2])) $_POST['service_assignee'][2]  = $_POST['assignees_values'];
		} else {
			$data1['accounts'] = '';

			unset($_POST['service_manager'][2], $_POST['service_assignee'][2]);
		}

		if (!empty($_POST['confirm'])) {

			if (isset($_POST['confirm']['text']) && $_POST['confirm']['text'] == 'on') {

				$this->send_sms($_POST['pn_no_rec']);
			}
			$data1['conf_statement'] = json_encode($_POST['confirm']);
			if (empty($_POST['service_manager'][1]))  $_POST['service_manager'][1]   = $_POST['assignees_values'];
			if (empty($_POST['service_assignee'][1])) $_POST['service_assignee'][1]  = $_POST['assignees_values'];
		} else {
			$data1['conf_statement'] = '';
			unset($_POST['service_manager'][1], $_POST['service_assignee'][1]);
		}




		if (!empty($_POST['companytax'])) {
			if (isset($_POST['companytax']['text']) && $_POST['companytax']['text'] == 'on') {
				$this->send_sms($_POST['pn_no_rec']);
			}

			$data1['company_tax_return'] = json_encode($_POST['companytax']);
			if (empty($_POST['service_manager'][3]))  $_POST['service_manager'][3]   = $_POST['assignees_values'];
			if (empty($_POST['service_assignee'][3])) $_POST['service_assignee'][3]  = $_POST['assignees_values'];
		} else {
			$data1['company_tax_return'] = '';
			unset($_POST['service_manager'][3], $_POST['service_assignee'][3]);
		}


		if (!empty($_POST['ccp_ptr_service'])) {
			/* if(isset($_POST['personaltax']['text']) && $_POST['personaltax']['text']=='on')
          {
              $this->send_sms($_POST['pn_no_rec']);
          }
          $data1['personal_tax_return'] = json_encode($_POST['personaltax']);
          */
			if (empty($_POST['service_manager'][4]))  $_POST['service_manager'][4]   = $_POST['assignees_values'];
			if (empty($_POST['service_assignee'][4])) $_POST['service_assignee'][4]  = $_POST['assignees_values'];
		} else {
			$data1['personal_tax_return'] = '';
			unset($_POST['service_manager'][4], $_POST['service_assignee'][4]);
		}


		if (!empty($_POST['payroll'])) {
			if (isset($_POST['payroll']['text']) && $_POST['payroll']['text'] == 'on') {
				$this->send_sms($_POST['pn_no_rec']);
			}
			$data1['payroll'] = json_encode($_POST['payroll']);

			if (empty($_POST['service_manager'][6]))  $_POST['service_manager'][6]   = $_POST['assignees_values'];
			if (empty($_POST['service_assignee'][6])) $_POST['service_assignee'][6]  = $_POST['assignees_values'];
		} else {
			$data1['payroll'] = '';
			unset($_POST['service_manager'][6], $_POST['service_assignee'][6]);
		}


		if (!empty($_POST['workplace'])) {
			if (isset($_POST['workplace']['text']) && $_POST['workplace']['text'] == 'on') {
				$this->send_sms($_POST['pn_no_rec']);
			}
			$data1['workplace'] = json_encode($_POST['workplace']);
			if (empty($_POST['service_manager'][7]))  $_POST['service_manager'][7]   = $_POST['assignees_values'];
			if (empty($_POST['service_assignee'][7])) $_POST['service_assignee'][7]  = $_POST['assignees_values'];
		} else {
			$data1['workplace'] = '';
			unset($_POST['service_manager'][7], $_POST['service_assignee'][7]);
		}


		if (!empty($_POST['vat'])) {
			if (isset($_POST['vat']['text']) && $_POST['vat']['text'] == 'on') {
				$this->send_sms($_POST['pn_no_rec']);
			}
			$data1['vat'] = json_encode($_POST['vat']);
			if (empty($_POST['service_manager'][5]))  $_POST['service_manager'][5]   = $_POST['assignees_values'];
			if (empty($_POST['service_assignee'][5])) $_POST['service_assignee'][5]  = $_POST['assignees_values'];
		} else {
			$data1['vat'] = '';
			unset($_POST['service_manager'][5], $_POST['service_assignee'][5]);
		}



		if (!empty($_POST['cis'])) {
			if (isset($_POST['cis']['text']) && $_POST['cis']['text'] == 'on') {
				$this->send_sms($_POST['pn_no_rec']);
			}
			$data1['cis'] = json_encode($_POST['cis']);
			if (empty($_POST['service_manager'][8]))  $_POST['service_manager'][8]   = $_POST['assignees_values'];
			if (empty($_POST['service_assignee'][8])) $_POST['service_assignee'][8]  = $_POST['assignees_values'];
		} else {
			$data1['cis'] = '';
			unset($_POST['service_manager'][8], $_POST['service_assignee'][8]);
		}

		if (!empty($_POST['cissub'])) {
			if (isset($_POST['cissub']['text']) && $_POST['cissub']['text'] == 'on') {
				$this->send_sms($_POST['pn_no_rec']);
			}
			$data1['cissub'] = json_encode($_POST['cissub']);

			if (empty($_POST['service_manager'][9]))  $_POST['service_manager'][9]   = $_POST['assignees_values'];
			if (empty($_POST['service_assignee'][9])) $_POST['service_assignee'][9]  = $_POST['assignees_values'];
		} else {
			$data1['cissub'] = '';
			unset($_POST['service_manager'][9], $_POST['service_assignee'][9]);
		}



		if (!empty($_POST['p11d'])) {
			if (isset($_POST['p11d']['text']) && $_POST['p11d']['text'] == 'on') {
				$this->send_sms($_POST['pn_no_rec']);
			}
			$data1['p11d'] = json_encode($_POST['p11d']);

			if (empty($_POST['service_manager'][11]))  $_POST['service_manager'][11]   = $_POST['assignees_values'];
			if (empty($_POST['service_assignee'][11])) $_POST['service_assignee'][11]  = $_POST['assignees_values'];
		} else {
			$data1['p11d'] = '';
			unset($_POST['service_manager'][11], $_POST['service_assignee'][11]);
		}


		if (!empty($_POST['bookkeep'])) {
			if (isset($_POST['bookkeep']['text']) && $_POST['bookkeep']['text'] == 'on') {
				$this->send_sms($_POST['pn_no_rec']);
			}
			$data1['bookkeep'] = json_encode($_POST['bookkeep']);

			if (empty($_POST['service_manager'][10]))  $_POST['service_manager'][10]   = $_POST['assignees_values'];
			if (empty($_POST['service_assignee'][10])) $_POST['service_assignee'][10]  = $_POST['assignees_values'];
		} else {
			$data1['bookkeep'] = '';
			unset($_POST['service_manager'][10], $_POST['service_assignee'][10]);
		}


		if (!empty($_POST['management'])) {
			if (isset($_POST['management']['text']) && $_POST['management']['text'] == 'on') {
				$this->send_sms($_POST['pn_no_rec']);
			}
			$data1['management'] = json_encode($_POST['management']);

			if (empty($_POST['service_manager'][12]))  $_POST['service_manager'][12]   = $_POST['assignees_values'];
			if (empty($_POST['service_assignee'][12])) $_POST['service_assignee'][12]  = $_POST['assignees_values'];
		} else {
			$data1['management'] = '';
			unset($_POST['service_manager'][12], $_POST['service_assignee'][12]);
		}


		if (!empty($_POST['investgate'])) {
			if (isset($_POST['investgate']['text']) && $_POST['investgate']['text'] == 'on') {
				$this->send_sms($_POST['pn_no_rec']);
			}
			$data1['investgate'] = json_encode($_POST['investgate']);

			if (empty($_POST['service_manager'][13]))  $_POST['service_manager'][13]   = $_POST['assignees_values'];
			if (empty($_POST['service_assignee'][13])) $_POST['service_assignee'][13]  = $_POST['assignees_values'];
		} else {
			$data1['investgate'] = '';
			unset($_POST['service_manager'][13], $_POST['service_assignee'][13]);
		}

		if (!empty($_POST['registered'])) {
			if (isset($_POST['registered']['text']) && $_POST['registered']['text'] == 'on') {
				$this->send_sms($_POST['pn_no_rec']);
			}
			$data1['registered'] = json_encode($_POST['registered']);
			if (empty($_POST['service_manager'][14]))  $_POST['service_manager'][14]   = $_POST['assignees_values'];
			if (empty($_POST['service_assignee'][14])) $_POST['service_assignee'][14]  = $_POST['assignees_values'];
		} else {
			$data1['registered'] = '';
			unset($_POST['service_manager'][14], $_POST['service_assignee'][14]);
		}

		if (!empty($_POST['taxadvice'])) {
			if (isset($_POST['taxadvice']['text']) && $_POST['taxadvice']['text'] == 'on') {
				$this->send_sms($_POST['pn_no_rec']);
			}
			$data1['taxadvice'] = json_encode($_POST['taxadvice']);

			if (empty($_POST['service_manager'][15]))  $_POST['service_manager'][15]   = $_POST['assignees_values'];
			if (empty($_POST['service_assignee'][15])) $_POST['service_assignee'][15]  = $_POST['assignees_values'];
		} else {
			$data1['taxadvice'] = '';
			unset($_POST['service_manager'][15], $_POST['service_assignee'][15]);
		}
		if (!empty($_POST['taxinvest'])) {
			if (isset($_POST['taxinvest']['text']) && $_POST['taxinvest']['text'] == 'on') {
				$this->send_sms($_POST['pn_no_rec']);
			}
			$data1['taxinvest'] = json_encode($_POST['taxinvest']);
		} else {
			$data1['taxinvest'] = '';
			unset($_POST['service_manager'][16], $_POST['service_assignee'][16]);
		}

		//requried information

		$data1['crm_company_name']          =   $this->input->post('company_name', NULL, 1);
		$data1['crm_legal_form']            =   $this->input->post('legal_form', NULL, 1);
		$data1['crm_allocation_holder']     =   $this->input->post('allocation_holders', NULL, 1);

		//basic deatils
		$data1['crm_company_number']        =   trim($this->input->post('company_number', NULL, 1));
		$data1['crm_company_url']           =   $this->input->post('company_url', NULL, 1);
		$data1['crm_officers_url']          =   $this->input->post('officers_url', NULL, 1);
		$data1['crm_company_name1']         =   $this->input->post('company_name', NULL, 1);

		$data1['crm_register_address']      =   $this->input->post('register_address', NULL, 1);
		$data1['crm_company_status']        =   $this->input->post('company_status', NULL, 1);
		$data1['crm_company_type']          =   $this->input->post('company_type', NULL, 1);
		$data1['crm_company_sic']           =   $this->input->post('company_sic', NULL, 1);
		$data1['crm_sic_codes']             =   $this->input->post('sic_codes', NULL, 1);
		$data1['crm_company_utr']           =   $this->input->post('company_utr', NULL, 1);
		$data1['crm_incorporation_date']    =   (!empty($_POST['date_of_creation'])) ? Change_Date_Format($_POST['date_of_creation'], 'd-m-Y', 'Y-m-d') : '';
		$data1['crm_registered_in']         =   $this->input->post('registered_in', NULL, 1);
		$data1['crm_address_line_one']      =   $this->input->post('address_line_one', NULL, 1);
		$data1['crm_address_line_two']      =   $this->input->post('address_line_two', NULL, 1);
		$data1['crm_address_line_three']    =   $this->input->post('address_line_three', NULL, 1);
		$data1['crm_town_city']             =   $this->input->post('crm_town_city', NULL, 1);
		$data1['crm_post_code']             =   $this->input->post('crm_post_code', NULL, 1);
		$data1['crm_hashtag']               =   $this->input->post('client_hashtag', NULL, 1);
		$data1['crm_accounts_office_reference']             =   $this->input->post('accounts_office_reference', NULL, 1);
		$data1['crm_vat_number']             =   $this->input->post('vat_number', NULL, 1);

		// client information
		$data1['crm_letter_sign']               =   $this->input->post('engagement_letter', NULL, 1);
		$data1['crm_business_website']          =   $this->input->post('business_website', NULL, 1);
		$data1['crm_accounting_system']         =   $this->input->post('accounting_system_inuse', NULL, 1);
		$data1['crm_paye_ref_number']           =   $this->input->post('paye_ref_number', NULL, 1);
		$data1['crm_assign_client_id_verified'] =   $this->input->post('client_id_verified', NULL, 1);

		$data1['crm_assign_type_of_id']         =   (!empty($_POST['type_of_id'])) ? implode(',', $_POST['type_of_id']) : '';

		/** 29-08-2018 proof attch **/
		$proof_attach_file = '';
		if (!empty($_FILES['proof_attach_file']['name'])) {
			/** for multiple **/
			$files = $_FILES;
			$cpt = count($_FILES['proof_attach_file']['name']);
			for ($i = 0; $i < $cpt; $i++) {
				/** end of multiple **/
				$_FILES['proof_attach_file']['name'] = $files['proof_attach_file']['name'][$i];
				$_FILES['proof_attach_file']['type'] = $files['proof_attach_file']['type'][$i];
				$_FILES['proof_attach_file']['tmp_name'] = $files['proof_attach_file']['tmp_name'][$i];
				$_FILES['proof_attach_file']['error'] = $files['proof_attach_file']['error'][$i];
				$_FILES['proof_attach_file']['size'] = $files['proof_attach_file']['size'][$i];
				$uploadPath = 'uploads/client_proof/';
				$config['upload_path'] = $uploadPath;
				$config['allowed_types'] = '*';
				$config['max_size'] = '0';
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if ($this->upload->do_upload('proof_attach_file')) {
					$fileData = $this->upload->data();
					$proof_attach_file = $proof_attach_file . ',uploads/client_proof/' . $fileData['file_name'];
				}
			}
		}
		$proof_attach_file .= (!empty($_POST['already_upload_img']) ? "," . implode(",", $_POST['already_upload_img']) : '');
		$data1['proof_attach_file'] = $proof_attach_file;

		$data1['crm_assign_other_custom']             =   $this->input->post('assign_other_custom', NULL, 1);
		$data1['crm_assign_proof_of_address']         =   $this->input->post('proof_of_address', NULL, 1);
		$data1['crm_assign_meeting_client']           =   $this->input->post('meeting_client', NULL, 1);
		$data1['crm_assign_source']                   =   $this->input->post('source', NULL, 1);
		$data1['crm_refered_by']                      =   $this->input->post('refered_by', NULL, 1);
		$data1['crm_assign_relationship_client']      =   $this->input->post('relationship_client', NULL, 1);
		$data1['crm_assign_notes']                    =   $this->input->post('assign_notes', NULL, 1);

		/** end of 30-08-2018 **/
		// other
		$data1['crm_previous_accountant']             =   $this->input->post('previous_account', NULL, 1);
		$data1['crm_other_name_of_firm']              =   $this->input->post('name_of_firm', NULL, 1);
		$data1['crm_other_address']                   =   $this->input->post('other_address', NULL, 1);



		$data1['crm_other_contact_no']                =   (!empty($_POST['previous_account']) && !empty($_POST['pn_no_rec'])) ? $_POST['pn_no_rec'] : '';

		$data1['crm_other_email ']                    =   (!empty($_POST['previous_account']) && !empty($_POST['emailid'])) ? $_POST['emailid'] : '';

		$data1['crm_other_chase_for_info']            =   (!empty($_POST['previous_account']) && !empty($_POST['chase_for_info'])) ? $_POST['chase_for_info'] : '';

		$data1['crm_other_notes']                     =   (!empty($_POST['previous_account']) && !empty($_POST['other_notes'])) ? $_POST['other_notes'] : '';



		$data1['crm_other_internal_notes']            =   $this->input->post('other_internal_notes', NULL, 1);
		$data1['crm_other_invite_use']                =   $this->input->post('invite_use', NULL, 1);
		$data1['crm_other_crm']                       =   $this->input->post('other_crm', NULL, 1);
		$data1['crm_other_proposal']                  =   $this->input->post('other_proposal', NULL, 1);
		$data1['crm_other_task']                      =   $this->input->post('other_task', NULL, 1);
		$data1['crm_other_send_invit_link']           =   $this->input->post('send_invit_link', NULL, 1);
		$data1['crm_other_username']                  =   $this->input->post('user_name', NULL, 1);
		$data1['crm_other_password']                  =   $this->input->post('password', NULL, 1);
		$data1['crm_other_any_notes']                 =   $this->input->post('other_any_notes', NULL, 1);


		// confirmation
		$data1['crm_confirmation_auth_code']                 =   $this->input->post('confirmation_auth_code', NULL, 1);
		$data1['crm_companies_house_authorisation_code']     =   $this->input->post('auth_code', NULL, 1);
		$data1['crm_confirmation_officers']                  =   $this->input->post('confirmation_officers', NULL, 1);
		$data1['crm_share_capital']                          =   $this->input->post('share_capital', NULL, 1);
		$data1['crm_shareholders']                           =   $this->input->post('shareholders', NULL, 1);
		$data1['crm_people_with_significant_control']        =   $this->input->post('people_with_significant_control', NULL, 1);

		//SERVICE CS
		$data1['crm_confirmation_statement_date']            =    '';
		$data1['crm_confirmation_email_remainder']           =  '';
		$data1['crm_confirmation_add_custom_reminder']       =  '';

		if (!empty($data1['conf_statement'])) {
			$data1['crm_confirmation_statement_date']               = (!empty($_POST['confirmation_next_made_up_to'])) ? $this->date_format($_POST['confirmation_next_made_up_to']) : '';

			$data1['crm_confirmation_email_remainder']              =   $this->input->post('confirmation_next_reminder', NULL, 1);
			$data1['crm_confirmation_add_custom_reminder']          =   $this->input->post('add_custom_reminder', NULL, 1);
		}

		$data1['crm_confirmation_statement_last_made_up_to_date'] = (!empty($_POST['statement_last_made_up_to_date'])) ? $this->date_format($_POST['statement_last_made_up_to_date']) : '';

		$data1['crm_confirmation_statement_due_date']           = (!empty($_POST['confirmation_next_due'])) ? date("Y-m-d", strtotime($_POST['confirmation_next_due'])) : '';
		/* $data1['crm_confirmation_notes']=(isset($_POST['confirmation_notes'])&&($_POST['confirmation_notes']!=''))? $_POST['confirmation_notes'] : '';*/

		// accounts
		$data1['crm_accounts_auth_code']                        =   $this->input->post('accounts_auth_code', NULL, 1);
		$data1['crm_accounts_utr_number']                       =   $this->input->post('accounts_utr_number', NULL, 1);
		$data1['crm_companies_house_email_remainder']           =   $this->input->post('company_house_reminder', NULL, 1);

		//SERVICE accounts
		$data1['crm_hmrc_yearend'] = '';
		$data1['crm_accounts_next_reminder_date'] = '';
		$data1['crm_accounts_custom_reminder']    = '';

		if (!empty($data1['accounts'])) {
			$data1['crm_hmrc_yearend']                              = (!empty($_POST['accounts_next_made_up_to'])) ? $this->date_format($_POST['accounts_next_made_up_to']) : '';
			$data1['crm_accounts_next_reminder_date']           =   $this->input->post('accounts_next_reminder_date', NULL, 1);
			$data1['crm_accounts_custom_reminder']              =   $this->input->post('accounts_custom_reminder', NULL, 1);
		}


		$data1['crm_ch_accounts_next_due']                      =   (!empty($_POST['accounts_next_due'])) ? $this->date_format($_POST['accounts_next_due']) : '';

		$data1['crm_accounts_last_made_up_to_date']              =  (!empty($_POST['accounts_last_made_up_to']))  ? $this->date_format($_POST['accounts_last_made_up_to']) : '';

		$data1['crm_accounts_tax_date_hmrc'] = (!empty($_POST['accounts_tax_date_hmrc'])) ? $this->date_format($_POST['accounts_tax_date_hmrc']) : '';

		$data1['crm_accounts_due_date_hmrc']      = '';
		$data1['crm_company_next_reminder_date']  = '';
		$data1['crm_company_custom_reminder']     = '';

		if (!empty($data1['company_tax_return'])) {
			$data1['crm_accounts_due_date_hmrc']                    = (!empty($_POST['accounts_due_date_hmrc'])) ? $this->date_format($_POST['accounts_due_date_hmrc']) : '';
			$data1['crm_company_next_reminder_date']                =   $this->input->post('company_next_reminder_date', NULL, 1);
			$data1['crm_company_custom_reminder']                   =   $this->input->post('company_custom_reminder', NULL, 1);
		}


		// payroll
		$data1['crm_payroll_acco_off_ref_no']           =   $this->input->post('payroll_acco_off_ref_no', NULL, 1);
		$data1['crm_paye_off_ref_no']                   =   $this->input->post('paye_off_ref_no', NULL, 1);
		$data1['crm_no_of_employees']                   =   $this->input->post('no_of_employees', NULL, 1);



		$data1['crm_payroll_run']                 = '';
		$data1['crm_payroll_run_date']            = '';
		$data1['crm_payroll_next_reminder_date']  = '';
		$data1['crm_payroll_add_custom_reminder'] = '';

		if (!empty($data1['payroll'])) {
			$data1['crm_payroll_run']                   =   $this->input->post('payroll_run', NULL, 1);
			$data1['crm_payroll_run_date']              =   (!empty($_POST['payroll_run_date'])) ? $this->date_format($_POST['payroll_run_date']) : '';

			$data1['crm_payroll_next_reminder_date']    =   $this->input->post('payroll_next_reminder_date', NULL, 1);
			$data1['crm_payroll_add_custom_reminder']   =   $this->input->post('payroll_add_custom_reminder', NULL, 1);
		}

		$data1['crm_payroll_reg_date']    =   (!empty($_POST['payroll_reg_date'])) ? $this->date_format($_POST['payroll_reg_date']) : '';
		$data1['crm_first_pay_date']      =   (!empty($_POST['first_pay_date'])) ? $this->date_format($_POST['first_pay_date']) : '';
		$data1['crm_rti_deadline']        =   (!empty($_POST['rti_deadline'])) ? $this->date_format($_POST['rti_deadline']) : '';
		$data1['crm_paye_scheme_ceased']  =  (!empty($_POST['paye_scheme_ceased'])) ? $this->date_format($_POST['paye_scheme_ceased']) : '';

		$data1['crm_staging_date'] = (isset($_POST['staging_date']) && ($_POST['staging_date'] != '')) ? $this->date_format($_POST['staging_date']) : '';

		$data1['crm_previous_year_require']           =   $this->input->post('previous_year_require', NULL, 1);
		$data1['crm_payroll_if_yes']                  =   $this->input->post('payroll_if_yes', NULL, 1);
		$data1['crm_pension_id']                      =   $this->input->post('pension_id', NULL, 1);

		$data1['crm_pension_subm_due_date']       = '';
		$data1['crm_pension_next_reminder_date']  = '';
		$data1['crm_pension_add_custom_reminder'] = '';

		if (!empty($data1['workplace'])) {

			$data1['crm_pension_subm_due_date']                 =   (!empty($_POST['pension_subm_due_date'])) ? $this->date_format($_POST['pension_subm_due_date']) : '';
			$data1['crm_pension_next_reminder_date']            =   $this->input->post('pension_next_reminder_date', NULL, 1);
			$data1['crm_pension_add_custom_reminder']           =   $this->input->post('pension_add_custom_reminder', NULL, 1);
		}


		$data1['crm_postponement_date']                   =   (!empty($_POST['defer_post_upto'])) ? $this->date_format($_POST['defer_post_upto']) : '';

		$data1['crm_the_pensions_regulator_opt_out_date'] =   (!empty($_POST['pension_regulator_date'])) ? $this->date_format($_POST['pension_regulator_date']) : '';

		$data1['crm_re_enrolment_date']                   =   (!empty($_POST['paye_re_enrolment_date'])) ? $this->date_format($_POST['paye_re_enrolment_date']) : '';

		$data1['crm_declaration_of_compliance_due_date  ']  = (!empty($_POST['declaration_of_compliance_due_date'])) ? $this->date_format($_POST['declaration_of_compliance_due_date']) : '';

		$data1['crm_declaration_of_compliance_submission']  = (!empty($_POST['declaration_of_compliance_last_filed'])) ? $this->date_format($_POST['declaration_of_compliance_last_filed']) : '';

		$data1['crm_paye_pension_provider']                      =   $this->input->post('paye_pension_provider', NULL, 1);
		$data1['crm_paye_pension_provider_password']             =   $this->input->post('paye_pension_provider_password', NULL, 1);
		$data1['crm_employer_contri_percentage']             =   $this->input->post('employer_contri_percentage', NULL, 1);
		$data1['crm_employee_contri_percentage']             =   $this->input->post('employee_contri_percentage', NULL, 1);
		$data1['crm_pension_notes']                          =   $this->input->post('pension_notes', NULL, 1);

		//$data1['crm_pension_id']=(isset($_POST['paye_pension_provider_userid'])&&($_POST['paye_pension_provider_userid']!=''))? $_POST['paye_pension_provider_userid'] : '';



		$data1['crm_cis_contractor']                          =   $this->input->post('cis_contractor', NULL, 1);

		$data1['crm_cis_contractor_start_date'] = '';
		$data1['crm_cis_frequency']             = '';
		$data1['crm_cis_next_reminder_date']    = '';
		$data1['crm_cis_add_custom_reminder']   = '';

		if (!empty($data1['cis'])) {
			$data1['crm_cis_contractor_start_date']             = (!empty($_POST['cis_contractor_start_date'])) ? $this->date_format($_POST['cis_contractor_start_date']) : '';
			$data1['crm_cis_frequency']                          =   $this->input->post('crm_cis_frequency', NULL, 1);
			$data1['crm_cis_next_reminder_date']                 =   $this->input->post('cis_next_reminder_date', NULL, 1);
			$data1['crm_cis_add_custom_reminder']                =   $this->input->post('cis_add_custom_reminder', NULL, 1);
		}
		$data1['crm_cis_scheme_notes']                =   $this->input->post('cis_scheme_notes', NULL, 1);


		$data1['crm_cis_subcontractor']                =   $this->input->post('cis_subcontractor', NULL, 1);


		$data1['crm_cis_subcontractor_start_date']  = '';
		$data1['crm_cis_subcontractor_frequency']   = '';
		$data1['crm_cissub_next_reminder_date'] = '';
		$data1['crm_cissub_add_custom_reminder']    = '';

		if (!empty($data1['cissub'])) {
			$data1['crm_cis_subcontractor_start_date']  = (!empty($_POST['cis_subcontractor_start_date'])) ? $this->date_format($_POST['cis_subcontractor_start_date']) : '';

			$data1['crm_cis_subcontractor_frequency']  =  $this->input->post('crm_cis_subcontractor_frequency', NULL, 1);
			$data1['crm_cissub_next_reminder_date']    =  $this->input->post('cissub_next_reminder_date', NULL, 1);
			$data1['crm_cissub_add_custom_reminder']   =  $this->input->post('cissub_add_custom_reminder', NULL, 1);
		}

		$data1['crm_cis_subcontractor_scheme_notes']   =  $this->input->post('cis_subcontractor_scheme_notes', NULL, 1);

		/** 11-09-2018 **/
		/** end of 11-09-2018 **/
		$data1['crm_next_p11d_return_due']    = '';
		$data1['crm_p11d_next_reminder_date'] = '';
		$data1['crm_p11d_add_custom_reminder'] = '';

		if (!empty($data1['p11d'])) {
			$data1['crm_next_p11d_return_due']      = (!empty($_POST['p11d_due_date'])) ? $this->date_format($_POST['p11d_due_date']) : '';
			$data1['crm_p11d_next_reminder_date']   = $this->input->post('p11d_next_reminder_date', NULL, 1);
			$data1['crm_p11d_add_custom_reminder']  = $this->input->post('p11d_add_custom_reminder', NULL, 1);
		}

		$data1['crm_p11d_latest_action'] = $this->input->post('p11d_todo', NULL, 1);


		$data1['crm_p11d_latest_action_date'] =   (!empty($_POST['p11d_start_date'])) ? $this->date_format($_POST['p11d_start_date']) : '';

		$data1['crm_latest_p11d_submitted']     = (!empty($_POST['p11d_first_benefit_date'])) ? $this->date_format($_POST['p11d_first_benefit_date']) : '';

		$data1['crm_p11d_records_received']     = (!empty($_POST['p11d_paye_scheme_ceased'])) ? $this->date_format($_POST['p11d_paye_scheme_ceased']) : '';

		$data1['crm_p11d_previous_year_require']  = $this->input->post('p11d_previous_year_require', NULL, 1);
		$data1['crm_p11d_payroll_if_yes']         = $this->input->post('p11d_payroll_if_yes', NULL, 1);

		//vat return
		$data1['crm_vat_quater_end_date']     = '';
		$data1['crm_vat_quarters']            = '';
		$data1['crm_vat_next_reminder_date']  = '';
		$data1['crm_vat_add_custom_reminder'] = '';

		if (!empty($data1['vat'])) {
			$data1['crm_vat_quater_end_date'] = (!empty($_POST['vat_quater_end_date'])) ? $this->date_format($_POST['vat_quater_end_date']) : '';
			$data1['crm_vat_quarters']            = $this->input->post('vat_quarters', NULL, 1);
			$data1['crm_vat_next_reminder_date']  = $this->input->post('vat_next_reminder_date', NULL, 1);
			$data1['crm_vat_add_custom_reminder'] = $this->input->post('vat_add_custom_reminder', NULL, 1);
		}


		$data1['crm_vat_number_one']         = $this->input->post('vat_number_one', NULL, 1);

		$data1['crm_vat_date_of_registration'] = (!empty($_POST['vat_registration_date'])) ?  $this->date_format($_POST['vat_registration_date']) : '';

		$data1['crm_vat_frequency']           = $this->input->post('vat_frequency', NULL, 1);


		$data1['crm_last_vat_return_filed_upto']  = (!empty($_POST['crm_last_vat_return_filed_upto'])) ? $this->date_format($_POST['crm_last_vat_return_filed_upto']) : '';

		$data1['crm_vat_scheme']                = $this->input->post('vat_scheme', NULL, 1);
		$data1['crm_flat_rate_category']        = $this->input->post('flat_rate_category', NULL, 1);
		$data1['crm_flat_rate_percentage']      = $this->input->post('flat_rate_percentage', NULL, 1);
		$data1['crm_direct_debit']              = $this->input->post('direct_debit', NULL, 1);
		$data1['crm_annual_accounting_scheme']  = $this->input->post('annual_accounting_scheme', NULL, 1);
		$data1['crm_box5_of_last_quarter_submitted'] = $this->input->post('box5_of_last_quarter_submitted', NULL, 1);
		$data1['crm_vat_address']               = $this->input->post('vat_address', NULL, 1);

		/*$data1['crm_vat_notes']=(isset($_POST['vat_notes'])&&($_POST['vat_notes']!=''))? $_POST['vat_notes'] : '';*/

		//management accounts
		$data1['crm_bookkeeping']                   = '';
		$data1['crm_next_booking_date']             = '';
		$data1['crm_bookkeep_next_reminder_date']   = '';
		$data1['crm_bookkeep_add_custom_reminder']  = '';

		if (!empty($data1['bookkeep'])) {
			$data1['crm_bookkeeping']       = $this->input->post('bookkeeping', NULL, 1);
			$data1['crm_next_booking_date'] = (!empty($_POST['next_booking_date'])) ? $this->date_format($_POST['next_booking_date']) : '';
			$data1['crm_bookkeep_next_reminder_date']   = $this->input->post('bookkeep_next_reminder_date', NULL, 1);
			$data1['crm_bookkeep_add_custom_reminder']  = $this->input->post('bookkeep_add_custom_reminder', NULL, 1);
		}



		$data1['crm_method_bookkeeping']            = $this->input->post('method_bookkeeping', NULL, 1);
		$data1['crm_client_provide_record']         = $this->input->post('client_provide_record', NULL, 1);
		$data1['crm_manage_method_bookkeeping']     = $this->input->post('manage_method_bookkeeping', NULL, 1);
		$data1['crm_manage_client_provide_record']  = $this->input->post('manage_client_provide_record', NULL, 1);

		$data1['crm_manage_acc_fre']              = '';
		$data1['crm_next_manage_acc_date']        = '';
		$data1['crm_manage_next_reminder_date']   = '';
		$data1['crm_manage_add_custom_reminder']  = '';

		if (!empty($data1['management'])) {
			$data1['crm_manage_acc_fre']        = $this->input->post('manage_acc_fre', NULL, 1);
			$data1['crm_next_manage_acc_date']  = (!empty($_POST['next_manage_acc_date'])) ? $this->date_format($_POST['next_manage_acc_date']) : '';
			$data1['crm_manage_next_reminder_date']   =   $this->input->post('manage_next_reminder_date', NULL, 1);
			$data1['crm_manage_add_custom_reminder']  =   $this->input->post('manage_add_custom_reminder', NULL, 1);
		}

		/*$data1['crm_manage_notes']=(isset($_POST['manage_notes'])&&($_POST['manage_notes']!=''))? $_POST['manage_notes'] : '';*/

		//Investigation Insurance

		$data1['crm_invesitgation_insurance'] = (!empty($_POST['insurance_start_date'])) ? $this->date_format($_POST['insurance_start_date']) : '';

		$data1['crm_insurance_renew_date']          = '';
		$data1['crm_insurance_next_reminder_date']  = '';
		$data1['crm_insurance_add_custom_reminder'] = '';

		if (!empty($data1['investgate'])) {
			$data1['crm_insurance_renew_date']          = (!empty($_POST['insurance_renew_date'])) ? $this->date_format($_POST['insurance_renew_date']) : '';
			$data1['crm_insurance_next_reminder_date']  = $this->input->post('insurance_next_reminder_date', NULL, 1);
			$data1['crm_insurance_add_custom_reminder'] = $this->input->post('insurance_add_custom_reminder', NULL, 1);
		}

		$data1['crm_insurance_provider']  = $this->input->post('insurance_provider', NULL, 1);
		$data1['crm_claims_note']         = $this->input->post('claims_note', NULL, 1);

		$data1['crm_registered_renew_date']           = '';
		$data1['crm_registered_next_reminder_date']   = '';
		$data1['crm_registered_add_custom_reminder']  = '';

		if (!empty($data1['registered'])) {
			$data1['crm_registered_renew_date']           =    (!empty($_POST['registered_renew_date'])) ? $this->date_format($_POST['registered_renew_date']) : '';
			$data1['crm_registered_next_reminder_date']   = $this->input->post('registered_next_reminder_date', NULL, 1);
			$data1['crm_registered_add_custom_reminder']  = $this->input->post('registered_add_custom_reminder', NULL, 1);
		}

		$data1['crm_registered_start_date']   = (!empty($_POST['registered_start_date'])) ? $this->date_format($_POST['registered_start_date']) : '';
		$data1['crm_registered_office_inuse'] = $this->input->post('registered_office_inuse', NULL, 1);
		$data1['crm_registered_claims_note']  =  $this->input->post('registered_claims_note', NULL, 1);


		$data1['crm_investigation_start_date'] = (!empty($_POST['investigation_start_date'])) ? $this->date_format($_POST['investigation_start_date']) : '';

		$data1['crm_investigation_end_date']            = '';
		$data1['crm_investigation_next_reminder_date']  = '';
		$data1['crm_investigation_add_custom_reminder'] = '';

		if (!empty($data1['taxadvice'])) {
			$data1['crm_investigation_end_date']            = (!empty($_POST['investigation_end_date'])) ? $this->date_format($_POST['investigation_end_date']) : '';
			$data1['crm_investigation_next_reminder_date']  = $this->input->post('investigation_next_reminder_date', NULL, 1);
			$data1['crm_investigation_add_custom_reminder'] = $this->input->post('investigation_add_custom_reminder', NULL, 1);
		}
		$data1['crm_investigation_note']                = $this->input->post('investigation_note', NULL, 1);

		//Personal Tax Return
		$data1['crm_personal_tax_return_date'] = (isset($_POST['ccp_personal_tax_return_date'][1]) && ($_POST['ccp_personal_tax_return_date'][1] != '')) ? date('Y-m-d', strtotime($this->date_format($_POST['ccp_personal_tax_return_date'][1]))) : '';
		$data1['crm_personal_due_date_return'] = (isset($_POST['ccp_personal_due_date_return'][1]) && ($_POST['ccp_personal_due_date_return'][1] != '')) ? date('Y-m-d', strtotime($this->date_format($_POST['ccp_personal_due_date_return'][1]))) : '';
		$data1['crm_personal_due_date_online'] = (isset($_POST['ccp_personal_due_date_online'][1]) && ($_POST['ccp_personal_due_date_online'][1] != '')) ? date('Y-m-d', strtotime($this->date_format($_POST['ccp_personal_due_date_online'][1]))) : '';

		/*$data1['crm_tax_investigation_note']=(isset($_POST['tax_investigation_note'])&&($_POST['tax_investigation_note']!=''))? $_POST['tax_investigation_note'] : '';*/

		$data1['select_responsible_type'] = $this->input->post('select_responsible_type', NULL, 1);
		$data1['property_income_notes']   = $this->input->post('property_income_notes', NULL, 1);
		// notes tab
		$data1['crm_notes_info']          = $this->input->post('notes_info', NULL, 1);

		/** activity log for notes **/
		/* if(isset($_POST['notes_info'])&&($_POST['notes_info']!=''))
    {

                $activity_datas['log'] = "Notes Added ---".$_POST['notes_info'];
                $activity_datas['createdTime'] = time();
                $activity_datas['module'] = 'Client';
                $activity_datas['sub_module']='Client Notes';              
                $activity_datas['module_id'] = (isset($_POST['user_id'])&&($_POST['user_id']==''))? $in : $_POST['user_id'];

                $this->Common_mdl->insert('activity_log',$activity_datas);
      }*/
		/** end of activity log for notes **/
		// documents tab
		$data1['crm_documents'] = (isset($_FILES['document']['name']) && ($_FILES['document']['name'] != '')) ? $this->Common_mdl->do_upload($_FILES['document'], 'uploads') : '';
		$data1['autosave_status'] = 0;
		//$this->db->insert( 'client', $data1 );
		/*
    $data_inv['user_id']=(isset($_POST['user_id'])&&($_POST['user_id']==''))? $in : $_POST['user_id'];
    $data_inv['confirmation_invoice']=(isset($_POST['invoice_cs'])&&($_POST['invoice_cs']!=''))? $_POST['invoice_cs'] : '';
    $data_inv['accounts_invoice']=(isset($_POST['invoice_as'])&&($_POST['invoice_as']!=''))? $_POST['invoice_as'] : '';
    $data_inv['company_tax_invoice']=(isset($_POST['invoice_ct'])&&($_POST['invoice_ct']!=''))? $_POST['invoice_ct'] : '';
    $data_inv['personal_tax_invoice']=(isset($_POST['invoice_pt'])&&($_POST['invoice_pt']!=''))? $_POST['invoice_pt'] : '';
    $data_inv['payroll_invoice']=(isset($_POST['invoice_pr'])&&($_POST['invoice_pr']!=''))? $_POST['invoice_pr'] : '';
    $data_inv['work_pension_invoice']=(isset($_POST['invoice_pn'])&&($_POST['invoice_pn']!=''))? $_POST['invoice_pn'] : '';
    $data_inv['cis_invoice']=(isset($_POST['invoice_cis'])&&($_POST['invoice_cis']!=''))? $_POST['invoice_cis'] : '';
    $data_inv['cissub_invoice']=(isset($_POST['invoice_sub'])&&($_POST['invoice_sub']!=''))? $_POST['invoice_sub'] : '';
    $data_inv['p11d_invoice']=(isset($_POST['invoice_p11d'])&&($_POST['invoice_p11d']!=''))? $_POST['invoice_p11d'] : '';
    $data_inv['vat_invoice']=(isset($_POST['invoice_vt'])&&($_POST['invoice_vt']!=''))? $_POST['invoice_vt'] : '';
    $data_inv['bookkeep_invoice']=(isset($_POST['invoice_book'])&&($_POST['invoice_book']!=''))? $_POST['invoice_book'] : '';
    $data_inv['management_invoice']=(isset($_POST['invoice_ma'])&&($_POST['invoice_ma']!=''))? $_POST['invoice_ma'] : '';
    $data_inv['invest_insurance_invoice']=(isset($_POST['invoice_insuance'])&&($_POST['invoice_insuance']!=''))? $_POST['invoice_insuance'] : '';
    $data_inv['registered_invoice']=(isset($_POST['invoice_reg'])&&($_POST['invoice_reg']!=''))? $_POST['invoice_reg'] : '';
    $data_inv['tax_advice_invoice']=(isset($_POST['invoice_taxadv'])&&($_POST['invoice_taxadv']!=''))? $_POST['invoice_taxadv'] : '';
    $data_inv['tax_invest_invoice']=(isset($_POST['invoice_taxinves'])&&($_POST['invoice_taxinves']!=''))? $_POST['invoice_taxinves'] : '';
    $data_inv['created_date']=time();

    $this->db->insert( 'client_invoice', $data_inv );*/

		/*buessness tab datas*/
		$buess_tab_types = ['Landlords - UK', 'Non Resident Landlord', 'Partnership', 'Self Employed'];
		if (in_array($_POST['legal_form'], $buess_tab_types)) {
			$datalegal['user_id'] = $_POST['user_id'];

			$datalegal['company_no']                = $this->input->post('fa_cmyno', NULL, 1);
			$datalegal['incorporation_date']        = $this->input->post('fa_incordate', NULL, 1);
			$datalegal['regist_address']            = $this->input->post('fa_registadres', NULL, 1);
			$datalegal['turnover']                  = $this->input->post('fa_turnover', NULL, 1);
			$datalegal['date_of_trading']           = $this->input->post('fa_dateoftrading', NULL, 1);
			$datalegal['sic_code']                  = $this->input->post('fa_siccode', NULL, 1);
			$datalegal['nuture_of_business']        = $this->input->post('fa_nutureofbus', NULL, 1);
			$datalegal['company_utr']               = $this->input->post('fa_cmyutr', NULL, 1);
			$datalegal['company_house_auth_code']   = $this->input->post('fa_cmyhouse', NULL, 1);
			$datalegal['trading_as']                = $this->input->post('bus_tradingas', NULL, 1);
			$datalegal['commenced_trading']         = $this->input->post('bus_commencedtrading', NULL, 1);
			$datalegal['register_sa']               = $this->input->post('bus_regist', NULL, 1);
			$datalegal['bus_turnover']              = $this->input->post('bus_turnover', NULL, 1);
			$datalegal['bus_nuture_of_business']    = $this->input->post('bus_nutureofbus', NULL, 1);
			$check_exist = $this->db->query("select * from client_legalform where user_id=" . $_POST['user_id'])->row_array();
			if (count($check_exist)) {
				$this->db->update('client_legalform', $datalegal, "user_id = " . $_POST['user_id']);
			} else {
				$this->db->insert('client_legalform', $datalegal);
			}
		} else if (!empty($old_client_data) && in_array($_POST['legal_form'], $buess_tab_types)) {
			$this->db->query("delete from client_legalform where user_id= " . $_POST['user_id']);
		}

		if (!empty($old_client_data)) {
			$updates_client =  $this->Common_mdl->update('client', $data1, 'user_id', $_POST['user_id']);
			$client_id      = $old_client_data['id'];

			//add timeline notes when if user click submit
			//don't call service reminder function when auto
			if (!empty($_POST['submit'])) {
				$log = "<p>Client Details Updated By <i>" . $_SESSION['crm_name'] . "</i></p>";
				$this->Common_mdl->Create_Log('client', $_POST['user_id'], $log);

				$this->Check_CreateMissingDetailsTask($client_id);

				ob_start();
				$Edited_service = array_intersect($_POST['is_service_data_edited'], [1]);
				if (count($Edited_service)) {
					$this->Service_Reminder_Model->Setup_Client_ServiceReminders($client_id, $Edited_service);
				}
				ob_end_clean();
			}
		} else {
			$data1['created_date']  =   time();
			$updates_client         =   $this->db->insert('client', $data1);
			$client_id              =   $this->db->insert_id();

			$log = "<p>Client Created From Manual Creation Section By <i>" . $_SESSION['crm_name'] . "</i></p>";
			$this->Common_mdl->Create_Log('client', $_POST['user_id'], $log);
			$this->Common_mdl->Increase_Decrease_ClientCounts($_SESSION['firm_id'], '-1');
		}

		$this->Service_Reminder_Model->setup_client_service_activity_log($old_client_data, $_POST['user_id']);

		//it is for add assignes to firm_assigness table.
		if (!empty($_POST['assignees_values'])) {
			$assignees = explode(',', $_POST['assignees_values']);
			$this->Client_model->Update_Client_assignee($client_id, $assignees);
		}

		/*Delete custom reminder if any cus reminder disabled*/
		if (!empty($_POST['add_reminder_ids'])) {
			$ex_reminder_ids =   explode(',', $_POST['add_reminder_ids']);
			$ex_reminder_ids =   implode(',', array_filter($ex_reminder_ids));

			$this->db->query("DELETE FROM custom_service_reminder where client_id=" . $client_id . " AND id NOT IN(" . $ex_reminder_ids . ")");
		}

		/*Update Custom Firm Fields Values*/
		$this->Common_mdl->update_field($this->input->post(), $_POST['user_id']);

		/*For Create Document to the client*/
		$this->Common_mdl->createparentDirectory($_POST['user_id']);

		if (!empty($old_client_data) && empty($old_client_data['crm_other_chase_for_info']) && !empty($_POST['previous_account']) && !empty($_POST['chase_for_info'])) {
			// $this->Send_ChaseForInfo_mail($old_client_data['user_id']);
		}
		echo json_encode(['user_id' => $_POST['user_id']]);
	}

	public function Import_ClientFrom_Csv()
	{
		$this->Security_model->chk_login();
		$csvMimes = array('application/vnd.ms-excel', 'text/plain', 'text/csv', 'text/tsv');
		if (!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'], $csvMimes)) {
			if (is_uploaded_file($_FILES['file']['tmp_name'])) {

				$result = $this->Client_model->validate_csv_fields($_FILES['file']);

				if (!empty($result)) {
					echo json_encode($result);
					return;
				} else {
					$result = $this->Client_model->import_csv_datas($_FILES['file']);

					foreach ($result as $key => $value) {
						$this->Check_CreateMissingDetailsTask($value);

						$user_id = $this->Common_mdl->get_field_value('client', 'user_id', 'id', $value);
						$this->Send_ClientWelcome_mail($user_id);
					}
					echo (!empty($result) ? count($result) : '0');
				}
			}
		} else {
			echo '0';
		}
		die;
	}
	public function Send_ChaseForInfo_mail($user_id)
	{
		//10 refer to action_id of email_templates_actions it connect with email_tempate.
		$EMAIL_TEMPLATE = $this->Common_mdl->getTemplates(10);


		if (!empty($EMAIL_TEMPLATE)) {

			$EMAIL_TEMPLATE = end($EMAIL_TEMPLATE);

			$client = $this->db->query('select id,firm_id,crm_other_email,user_id from client where user_id="' . $user_id . '"')->row_array();

			$sender_details = $this->db->query('select company_name,company_email from admin_setting where firm_id=' . $client['firm_id'] . '')->row_array();

			$decode['title'] = html_entity_decode($EMAIL_TEMPLATE['title']);
			$decode['body']  = html_entity_decode($EMAIL_TEMPLATE['body']);
			$decode['subject']  = html_entity_decode($EMAIL_TEMPLATE['subject']);

			$decoded = Decode_ClientTable_Datas($client['id'], $decode);

			$Issend = firm_settings_send_mail($client['firm_id'], $client['crm_other_email'], $decoded['subject'], $decoded['body']);

			if ($Issend['result'] == 1) {
				$log = "Chase For Information Mail Send";
				$this->Common_mdl->Create_Log('client', $user_id, $log);
			}
		}
	}

	public function Send_ClientWelcome_mail($user_id)
	{
		$this->Client_model->welcome_mail($user_id);
	}

	public function update_client_contacts()
	{
		$res = ['result' => 0];
		if (!empty($_POST['first_name'])) {
			$res['result']  = 1;
			$res['data']    = $this->Client_model->update_client_contacts();
		}
		echo json_encode($res);
	}

	public function companyhouse_update()
	{
		if (!empty($_POST['client_id'])) {
			echo  $this->Client_model->Update_CH_Datas($_POST['client_id']);
		}
	}
	public function Client_Contact_Delete($id)
	{
		echo $this->Client_model->delete_contact_person($id);
	}
	public function Get_CusReminder_Data($id)
	{
		$data = $this->db->query("select * from custom_service_reminder where id =" . $id)->row_array();
		echo json_encode($data);
	}

	// client information update
	public function updates_client($user_ids = false, $userid)
	{
		if ($user_ids != false) {
			$_POST['user_id'] = $user_ids;
		}

		$Service_Fields = $this->Service_Reminder_Model->Get_Service_ReminderFields();

		$OLD_SERVICE_DATA = $this->db->query("SELECT " . $Service_Fields . " FROM client where user_id = " . $_POST['user_id'])->row_array();

		$image_one = (isset($_POST['image_name']) && ($_POST['image_name'] != '')) ? $_POST['image_name'] : '825898.jpg';
		$image = (isset($_FILES['profile_image']['name']) && ($_FILES['profile_image']['name'] != '')) ? $this->Common_mdl->do_upload($_FILES['profile_image'], 'uploads') : $image_one;
		// $data['crm_name']=$_POST['user_name'];
		$data['username'] = $_POST['user_name'];
		$data['crm_name'] = $_POST['company_name'];
		$data['crm_email_id'] = $_POST['emailid'];
		$data['crm_phone_number'] = (isset($_POST['pn_no_rec']) && ($_POST['pn_no_rec'] != '')) ? $_POST['cun_code'] . $_POST['pn_no_rec'] : '';

		$data['password'] = (isset($_POST['password']) && ($_POST['password'] != '')) ? md5($_POST['password']) : '';

		$data['confirm_password'] = (isset($_POST['confirm_password']) && ($_POST['confirm_password'] != '')) ? $_POST['confirm_password'] : '';

		$data['company_roles'] = (isset($_POST['company_house']) && ($_POST['company_house'] != '')) ? $_POST['company_house'] : 0;
		$data['CreatedTime'] = time();
		$data['client_id'] = (isset($_POST['client_id']) && ($_POST['client_id'] != '')) ? $_POST['client_id'] : '';
		/** 09-08-2018 **/
		//$data['status']=1;            
		$updates_user = $this->Common_mdl->update('user', $data, 'id', $_POST['user_id']);

		$datas['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user', 'role', '1');
		/** for client our check for mail ervices  **/
		$re_conf_statement = '';
		$re_accounts = '';
		$re_company_tax_return = '';
		$re_personal_tax_return = '';
		$re_payroll = '';
		$re_workplace = '';
		$re_vat = '';
		$re_cis = '';
		$re_cissub = '';
		$re_p11d = '';
		$re_bookkeep = '';
		$re_management = '';
		$re_investgate = '';
		$re_registered = '';
		$re_taxadvice = '';
		$re_taxinvest = '';


		$old_client_data = $this->db->query("select * from client where user_id=" . $_POST['user_id'])->row_array();
		if (!empty($old_client_data['conf_statement']))
			$before_data1['conf_statement'] = (isset(json_decode($old_client_data['conf_statement'])->tab)) ? json_decode($old_client_data['conf_statement'])->tab : '';
		else
			$before_data1['conf_statement'] = '';

		if (!empty($old_client_data['accounts']))
			$before_data1['accounts'] = (isset(json_decode($old_client_data['accounts'])->tab)) ? json_decode($old_client_data['accounts'])->tab : '';
		else
			$before_data1['accounts'] = '';
		if (!empty($old_client_data['company_tax_return']))
			$before_data1['company_tax_return'] = (isset(json_decode($old_client_data['company_tax_return'])->tab)) ? json_decode($old_client_data['company_tax_return'])->tab : '';
		else
			$before_data1['company_tax_return'] = '';
		if (!empty($old_client_data['personal_tax_return']))
			$before_data1['personal_tax_return'] = (isset(json_decode($old_client_data['personal_tax_return'])->tab)) ? json_decode($old_client_data['personal_tax_return'])->tab : '';
		else
			$before_data1['personal_tax_return'] = '';

		if (!empty($old_client_data['payroll']))
			$before_data1['payroll'] = (isset(json_decode($old_client_data['payroll'])->tab)) ? json_decode($old_client_data['payroll'])->tab : '';
		else
			$before_data1['payroll'] = '';

		if (!empty($old_client_data['workplace']))
			$before_data1['workplace'] = (isset(json_decode($old_client_data['workplace'])->tab)) ? json_decode($old_client_data['workplace'])->tab : '';
		else
			$before_data1['workplace'] = '';

		if (!empty($old_client_data['vat']))
			$before_data1['vat'] = (isset(json_decode($old_client_data['vat'])->tab)) ? json_decode($old_client_data['vat'])->tab : '';
		else
			$before_data1['vat'] = '';

		if (!empty($old_client_data['cis']))
			$before_data1['cis'] = (isset(json_decode($old_client_data['cis'])->tab)) ? json_decode($old_client_data['cis'])->tab : '';
		else
			$before_data1['cis'] = '';

		if (!empty($old_client_data['cissub']))
			$before_data1['cissub'] = (isset(json_decode($old_client_data['cissub'])->tab)) ? json_decode($old_client_data['cissub'])->tab : '';
		else
			$before_data1['cissub'] = '';

		if (!empty($old_client_data['p11d']))
			$before_data1['p11d'] = (isset(json_decode($old_client_data['p11d'])->tab)) ? json_decode($old_client_data['p11d'])->tab : '';
		else
			$before_data1['p11d'] = '';


		if (!empty($old_client_data['bookkeep']))
			$before_data1['bookkeep'] =  (isset(json_decode($old_client_data['bookkeep'])->tab)) ? json_decode($old_client_data['bookkeep'])->tab : '';
		else
			$before_data1['bookkeep'] = '';
		if (!empty($old_client_data['management']))
			$before_data1['management'] =  (isset(json_decode($old_client_data['management'])->tab)) ? json_decode($old_client_data['management'])->tab : '';
		else
			$before_data1['management'] = '';
		if (!empty($old_client_data['investgate']))
			$before_data1['investgate'] =  (isset(json_decode($old_client_data['investgate'])->tab)) ? json_decode($old_client_data['investgate'])->tab : '';
		else
			$before_data1['investgate'] = '';
		if (!empty($old_client_data['registered']))
			$before_data1['registered'] =  (isset(json_decode($old_client_data['registered'])->tab)) ? json_decode($old_client_data['registered'])->tab : '';
		else
			$before_data1['registered'] = '';
		if (!empty($old_client_data['taxadvice']))
			$before_data1['taxadvice'] =  (isset(json_decode($old_client_data['taxadvice'])->tab)) ? json_decode($old_client_data['taxadvice'])->tab : '';
		else
			$before_data1['taxadvice'] = '';
		if (!empty($old_client_data['taxinvest']))
			$before_data1['taxinvest'] =  (isset(json_decode($old_client_data['taxinvest'])->tab)) ? json_decode($old_client_data['taxinvest'])->tab : '';
		else
			$before_data1['taxinvest'] = '';

		$other_services = $this->db->query('SELECT * FROM service_lists WHERE id>16')->result_array();

		foreach ($other_services as $key => $value) {
			if (!empty($old_client_data[$value['service_subnames']])) {
				$before_data1[$value['service_subnames']] = (isset(json_decode($old_client_data[$value['service_subnames']])->tab)) ? json_decode($old_client_data[$value['service_subnames']])->tab : '';
			} else {
				$before_data1[$value['service_subnames']] = '';
			}
		}

		/** end of services mail chk 09-07-2018 **/

		if (isset($_POST['user_id']) && $_POST['user_id'] != '') {

			$data1['user_id'] = $_POST['user_id'];
			// required information
			if (!empty($_POST['confirm']['tab']))
				$data1['conf_statement'] = json_encode($_POST['confirm']);
			else
				$data1['conf_statement'] = '';

			if (!empty($_POST['accounts']['tab']))
				$data1['accounts'] = json_encode($_POST['accounts']);
			else
				$data1['accounts'] = '';

			if (!empty($_POST['companytax']['tab']))
				$data1['company_tax_return'] = json_encode($_POST['companytax']);
			else
				$data1['company_tax_return'] = '';

			if (!empty($_POST['personaltax']['tab']))
				$data1['personal_tax_return'] = json_encode($_POST['personaltax']);
			else
				$data1['personal_tax_return'] = '';

			if (!empty($_POST['payroll']['tab']))
				$data1['payroll'] = json_encode($_POST['payroll']);
			else
				$data1['payroll'] = '';

			if (!empty($_POST['workplace']['tab']))
				$data1['workplace'] = json_encode($_POST['workplace']);
			else
				$data1['workplace'] = '';

			if (!empty($_POST['vat']['tab']))
				$data1['vat'] = json_encode($_POST['vat']);
			else
				$data1['vat'] = '';

			if (!empty($_POST['cis']['tab']))
				$data1['cis'] = json_encode($_POST['cis']);
			else
				$data1['cis'] = '';

			if (!empty($_POST['cissub']['tab']))
				$data1['cissub'] = json_encode($_POST['cissub']);
			else
				$data1['cissub'] = '';

			if (!empty($_POST['p11d']['tab']))
				$data1['p11d'] = json_encode($_POST['p11d']);
			else
				$data1['p11d'] = '';

			if (!empty($_POST['bookkeep']['tab']))
				$data1['bookkeep'] = json_encode($_POST['bookkeep']);
			else
				$data1['bookkeep'] = '';
			if (!empty($_POST['management']['tab']))
				$data1['management'] = json_encode($_POST['management']);
			else
				$data1['management'] = '';
			if (!empty($_POST['investgate']['tab']))
				$data1['investgate'] = json_encode($_POST['investgate']);
			else
				$data1['investgate'] = '';
			if (!empty($_POST['registered']['tab']))
				$data1['registered'] = json_encode($_POST['registered']);
			else
				$data1['registered'] = '';
			if (!empty($_POST['taxadvice']['tab']))
				$data1['taxadvice'] = json_encode($_POST['taxadvice']);
			else
				$data1['taxadvice'] = '';
			if (!empty($_POST['taxinvest']['tab']))
				$data1['taxinvest'] = json_encode($_POST['taxinvest']);
			else
				$data1['taxinvest'] = '';

			foreach ($other_services as $key => $value) {
				if (!empty($_POST[$value['services_subnames']]['tab'])) {
					$data1[$value['services_subnames']] = json_encode($_POST[$value['services_subnames']]);
				} else {
					$data1[$value['services_subnames']] = '';
				}

				$data1['crm_' . $value['services_subnames'] . '_email_remainder'] = (isset($_POST['crm_' . $value['services_subnames'] . '_email_remainder']) && ($_POST['crm_' . $value['services_subnames'] . '_email_remainder'] != '')) ? $_POST['crm_' . $value['services_subnames'] . '_email_remainder'] : '';
				$data1['crm_' . $value['services_subnames'] . '_add_custom_reminder'] = (isset($_POST['crm_' . $value['services_subnames'] . '_add_custom_reminder']) && ($_POST['crm_' . $value['services_subnames'] . '_add_custom_reminder'] != '')) ? $_POST['crm_' . $value['services_subnames'] . '_add_custom_reminder'] : '';
				$data1['crm_' . $value['services_subnames'] . '_statement_date'] = (isset($_POST['crm_' . $value['services_subnames'] . '_statement_date']) && ($_POST['crm_' . $value['services_subnames'] . '_statement_date'] != '')) ? date("Y-m-d", strtotime($_POST['crm_' . $value['services_subnames'] . '_statement_date'])) : '';
			}

			//requried information
			$data1['crm_company_name1'] = (isset($_POST['company_name1']) && ($_POST['company_name1'] != '')) ? $_POST['company_name1'] : '';
			$data1['crm_legal_form'] = (isset($_POST['legal_form']) && ($_POST['legal_form'] != '')) ? $_POST['legal_form'] : '';
			$data1['crm_allocation_holder'] = (isset($_POST['allocation_holders']) && ($_POST['allocation_holders'] != '')) ? $_POST['allocation_holders'] : '';
			// basic details
			$data1['crm_company_name'] = (isset($_POST['company_name']) && ($_POST['company_name'] != '')) ? $_POST['company_name'] : '';
			$data1['crm_company_number'] = (isset($_POST['company_number']) && ($_POST['company_number'] != '')) ? trim($_POST['company_number']) : '';
			$data1['crm_incorporation_date'] = (isset($_POST['date_of_creation']) && ($_POST['date_of_creation'] != '')) ? $_POST['date_of_creation'] : '';
			$data1['crm_registered_in'] = (isset($_POST['registered_in']) && ($_POST['registered_in'] != '')) ? $_POST['registered_in'] : '';
			$data1['crm_address_line_one'] = (isset($_POST['address_line_one']) && ($_POST['address_line_one'] != '')) ? $_POST['address_line_one'] : '';
			$data1['crm_address_line_two'] = (isset($_POST['address_line_two']) && ($_POST['address_line_two'] != '')) ? $_POST['address_line_two'] : '';
			$data1['crm_address_line_three'] = (isset($_POST['address_line_three']) && ($_POST['address_line_three'] != '')) ? $_POST['address_line_three'] : '';
			$data1['crm_town_city'] = (isset($_POST['crm_town_city']) && ($_POST['crm_town_city'] != '')) ? $_POST['crm_town_city'] : '';
			$data1['crm_post_code'] = (isset($_POST['crm_post_code']) && ($_POST['crm_post_code'] != '')) ? $_POST['crm_post_code'] : '';
			$data1['crm_company_status'] = (isset($_POST['company_status']) && ($_POST['company_status'] != '')) ? $_POST['company_status'] : '';
			$data1['crm_company_type'] = (isset($_POST['company_type']) && ($_POST['company_type'] != '')) ? $_POST['company_type'] : '';
			$data1['crm_company_sic'] = (isset($_POST['company_sic']) && ($_POST['company_sic'] != '')) ? $_POST['company_sic'] : '';
			$data1['crm_accounting_system'] = (isset($_POST['accounting_system']) && ($_POST['accounting_system'] != '')) ? $_POST['accounting_system'] : '';
			$data1['crm_companies_house_authorisation_code'] = (isset($_POST['auth_code']) && ($_POST['auth_code'] != '')) ? $_POST['auth_code'] : '';
			$data1['crm_company_utr'] = (isset($_POST['crm_company_utr']) && ($_POST['crm_company_utr'] != '')) ? $_POST['crm_company_utr'] : '';
			$data1['crm_accounts_office_reference'] = (isset($_POST['acc_ref_number']) && ($_POST['acc_ref_number'] != '')) ? $_POST['acc_ref_number'] : '';
			$data1['crm_vat_number'] = (isset($_POST['vat_number']) && ($_POST['vat_number'] != '')) ? $_POST['vat_number'] : '';
			//$data1['status']=(isset($_POST['company_house'])&&($_POST['company_house']!=''))? $_POST['company_house'] : 0;
			// client information
			$data1['crm_letter_sign'] = (isset($_POST['letter_sign']) && ($_POST['letter_sign'] != '')) ? $_POST['letter_sign'] : '';
			$data1['crm_business_website'] = (isset($_POST['business_website']) && ($_POST['business_website'] != '')) ? $_POST['business_website'] : '';
			$data1['crm_paye_ref_number'] = (isset($_POST['paye_ref_number']) && ($_POST['paye_ref_number'] != '')) ? $_POST['paye_ref_number'] : '';
			//Assign to
			$data1['crm_assign_client_id_verified'] = (isset($_POST['client_id_verified']) && ($_POST['client_id_verified'] != '')) ? $_POST['client_id_verified'] : '';
			$data1['crm_assign_type_of_id'] = (isset($_POST['type_of_id']) && ($_POST['type_of_id'] != '')) ? implode(',', $_POST['type_of_id']) : '';
			/** 29-08-2018 proof attch **/
			$proof_attach_file = '';
			if (!empty($_FILES['proof_attach_file']['name'])) {
				/** for multiple **/
				$files = $_FILES;
				$cpt = count($_FILES['proof_attach_file']['name']);
				for ($i = 0; $i < $cpt; $i++) {
					/** end of multiple **/
					$_FILES['proof_attach_file']['name'] = $files['proof_attach_file']['name'][$i];
					$_FILES['proof_attach_file']['type'] = $files['proof_attach_file']['type'][$i];
					$_FILES['proof_attach_file']['tmp_name'] = $files['proof_attach_file']['tmp_name'][$i];
					$_FILES['proof_attach_file']['error'] = $files['proof_attach_file']['error'][$i];
					$_FILES['proof_attach_file']['size'] = $files['proof_attach_file']['size'][$i];
					$uploadPath = 'uploads/client_proof/';
					$config['upload_path'] = $uploadPath;
					$config['allowed_types'] = '*';
					$config['max_size'] = '0';
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if ($this->upload->do_upload('proof_attach_file')) {
						$fileData = $this->upload->data();
						//  $uploadData[$i]['file_name'] = base_url().'attachment/'.$fileData['file_name'];
						//$data['attach_file']= base_url().'uploads/'.$fileData['file_name'];
						$proof_attach_file = $proof_attach_file . "," . $fileData['file_name'];
					}
				}
			}
			/** alredy uploaded images **/
			$ex_img = (isset($_POST['already_upload_img']) && $_POST['already_upload_img'] != '') ? $_POST['already_upload_img'] : '';
			//echo implode(',',$ex_img);
			if (!empty($ex_img)) {
				$proof_attach_file = $proof_attach_file . "," . implode(',', $ex_img);
			}
			if ($proof_attach_file != '') {
				$data1['proof_attach_file'] = implode(',', array_filter(explode(',', $proof_attach_file)));
			} else {
				$data1['proof_attach_file'] = '';
			}

			/** end of uploaded images **/
			//$data1['proof_attach_file']=$proof_attach_file;
			/** end of 29-08-2018 **/
			/** 30-08-2018 **/
			$data1['crm_assign_other_custom'] = (isset($_POST['assign_other_custom']) && ($_POST['assign_other_custom'] != '')) ? $_POST['assign_other_custom'] : '';
			/** end of 30-08-2018 **/
			$data1['crm_assign_proof_of_address'] = (isset($_POST['proof_of_address']) && ($_POST['proof_of_address'] != '')) ? $_POST['proof_of_address'] : '';
			$data1['crm_assign_meeting_client'] = (isset($_POST['meeting_client']) && ($_POST['meeting_client'] != '')) ? $_POST['meeting_client'] : '';
			$data1['crm_assign_source'] = (isset($_POST['source']) && ($_POST['source'] != '')) ? $_POST['source'] : '';
			/** 30-06-2018 **/
			// $data1['crm_refered_by']=(isset($_POST['refer_exist_client'])&&($_POST['refer_exist_client']!=''))? $_POST['refer_exist_client'] : '';
			$data1['crm_refered_by'] = (isset($_POST['refered_by']) && ($_POST['refered_by'] != '')) ? $_POST['refered_by'] : '';
			/** 30-06-2018 **/
			$data1['crm_assign_relationship_client'] = (isset($_POST['relationship_client']) && ($_POST['relationship_client'] != '')) ? $_POST['relationship_client'] : '';
			$data1['crm_assign_notes'] = (isset($_POST['assign_notes']) && ($_POST['assign_notes'] != '')) ? $_POST['assign_notes'] : '';
			// other
			$data1['crm_previous_accountant'] = (!empty($_POST['previous_account'])) ? $_POST['previous_account'] : '';
			$data1['crm_other_name_of_firm'] = (!empty($_POST['previous_account']) && !empty($_POST['name_of_firm'])) ? $_POST['name_of_firm'] : '';
			$data1['crm_other_address'] = (!empty($_POST['previous_account']) && !empty($_POST['other_address'])) ? $_POST['other_address'] : '';
			$data1['crm_other_contact_no'] = (!empty($_POST['previous_account']) && !empty($_POST['pn_no_rec'])) ? $_POST['cun_code'] . $_POST['pn_no_rec'] : '';
			$data1['crm_other_email'] = (!empty($_POST['previous_account']) && !empty($_POST['emailid'])) ? $_POST['emailid'] : '';
			$data1['crm_other_chase_for_info'] = (!empty($_POST['previous_account']) && !empty($_POST['chase_for_info'])) ? $_POST['chase_for_info'] : '';
			$data1['crm_other_notes'] = (!empty($_POST['previous_account']) && !empty($_POST['other_notes'])) ? $_POST['other_notes'] : '';







			/** task creation chase for information 30-08-2018 **/

			/** end of 30-08-2018 **/

			$data1['crm_other_notes'] = (isset($_POST['other_notes']) && ($_POST['other_notes'] != '')) ? $_POST['other_notes'] : '';

			$data1['crm_other_internal_notes'] = (isset($_POST['other_internal_notes']) && ($_POST['other_internal_notes'] != '')) ? $_POST['other_internal_notes'] : '';
			$data1['crm_other_invite_use'] = (isset($_POST['invite_use']) && ($_POST['invite_use'] != '')) ? $_POST['invite_use'] : '';
			$data1['crm_other_crm'] = (isset($_POST['other_crm']) && ($_POST['other_crm'] != '')) ? $_POST['other_crm'] : '';
			$data1['crm_other_proposal'] = (isset($_POST['other_proposal']) && ($_POST['other_proposal'] != '')) ? $_POST['other_proposal'] : '';
			$data1['crm_other_task'] = (isset($_POST['other_task']) && ($_POST['other_task'] != '')) ? $_POST['other_task'] : '';
			$data1['crm_other_send_invit_link'] = (isset($_POST['send_invit_link']) && ($_POST['send_invit_link'] != '')) ? $_POST['send_invit_link'] : '';
			$data1['crm_other_username'] = (isset($_POST['user_name']) && ($_POST['user_name'] != '')) ? $_POST['user_name'] : '';
			$data1['crm_other_password'] = (isset($_POST['password']) && ($_POST['password'] != '')) ? $_POST['password'] : '';
			$data1['crm_other_any_notes'] = (isset($_POST['other_any_notes']) && ($_POST['other_any_notes'] != '')) ? $_POST['other_any_notes'] : '';
			// confirmation
			$data1['crm_confirmation_auth_code'] = (isset($_POST['confirmation_auth_code']) && ($_POST['confirmation_auth_code'] != '')) ? $_POST['confirmation_auth_code'] : '';
			$data1['crm_confirmation_statement_date'] = (isset($_POST['confirmation_next_made_up_to']) && ($_POST['confirmation_next_made_up_to'] != '')) ? date("Y-m-d", strtotime($_POST['confirmation_next_made_up_to'])) : '';
			$data1['crm_confirmation_statement_last_made_up_to_date'] = (isset($_POST['statement_last_made_up_to_date']) && ($_POST['statement_last_made_up_to_date'] != '')) ? date("Y-m-d", strtotime($_POST['statement_last_made_up_to_date'])) : '';


			$data1['crm_confirmation_statement_due_date'] = (isset($_POST['confirmation_next_due']) && ($_POST['confirmation_next_due'] != '')) ? date("Y-m-d", strtotime($_POST['confirmation_next_due'])) : '';
			$data1['crm_confirmation_email_remainder'] = (isset($_POST['confirmation_next_reminder']) && ($_POST['confirmation_next_reminder'] != '')) ? $_POST['confirmation_next_reminder'] : '';
			$data1['crm_confirmation_create_task_reminder'] = (isset($_POST['create_task_reminder']) && ($_POST['create_task_reminder'] != '')) ? $_POST['create_task_reminder'] : '';
			$data1['crm_confirmation_add_custom_reminder'] = (isset($_POST['add_custom_reminder']) && ($_POST['add_custom_reminder'] != '')) ? $_POST['add_custom_reminder'] : '';
			$data1['crm_confirmation_officers'] = (isset($_POST['confirmation_officers']) && ($_POST['confirmation_officers'] != '')) ? $_POST['confirmation_officers'] : '';
			$data1['crm_share_capital'] = (isset($_POST['share_capital']) && ($_POST['share_capital'] != '')) ? $_POST['share_capital'] : '';
			$data1['crm_shareholders'] = (isset($_POST['shareholders']) && ($_POST['shareholders'] != '')) ? $_POST['shareholders'] : '';
			$data1['crm_people_with_significant_control'] = (isset($_POST['people_with_significant_control']) && ($_POST['people_with_significant_control'] != '')) ? $_POST['people_with_significant_control'] : '';
			/*$data1['crm_confirmation_notes']=(isset($_POST['confirmation_notes'])&&($_POST['confirmation_notes']!=''))? $_POST['confirmation_notes'] : ''; AS CR*/
			// accounts
			$data1['crm_accounts_auth_code'] = (isset($_POST['accounts_auth_code']) && ($_POST['accounts_auth_code'] != '')) ? $_POST['accounts_auth_code'] : '';
			$data1['crm_accounts_utr_number'] = (isset($_POST['accounts_utr_number']) && ($_POST['accounts_utr_number'] != '')) ? $_POST['accounts_utr_number'] : '';
			$data1['crm_companies_house_email_remainder'] = (isset($_POST['company_house_reminder']) && ($_POST['company_house_reminder'] != '')) ? $_POST['company_house_reminder'] : '';
			$data1['crm_hmrc_yearend'] = (isset($_POST['accounts_next_made_up_to']) && ($_POST['accounts_next_made_up_to'] != '')) ? date("Y-m-d", strtotime($_POST['accounts_next_made_up_to'])) : '';
			$data1['crm_accounts_last_made_up_to_date'] = (isset($_POST['accounts_last_made_up_to']) && ($_POST['accounts_last_made_up_to'] != '')) ? $_POST['accounts_last_made_up_to'] : '';
			$data1['crm_ch_accounts_next_due'] = (isset($_POST['accounts_next_due']) && ($_POST['accounts_next_due'] != '')) ? date("Y-m-d", strtotime($_POST['accounts_next_due'])) : '';
			$data1['crm_accounts_next_reminder_date'] = (isset($_POST['accounts_next_reminder_date']) && ($_POST['accounts_next_reminder_date'] != '')) ? $_POST['accounts_next_reminder_date'] : '';
			$data1['crm_accounts_create_task_reminder'] = (isset($_POST['accounts_create_task_reminder']) && ($_POST['accounts_create_task_reminder'] != '')) ? $_POST['accounts_create_task_reminder'] : '';
			$data1['crm_accounts_custom_reminder'] = (isset($_POST['accounts_custom_reminder']) && ($_POST['accounts_custom_reminder'] != '')) ? $_POST['accounts_custom_reminder'] : '';
			$data1['crm_accounts_due_date_hmrc'] = (isset($_POST['accounts_due_date_hmrc']) && ($_POST['accounts_due_date_hmrc'] != '')) ? date("Y-m-d", strtotime($this->date_format($_POST['accounts_due_date_hmrc']))) : '';
			$data1['crm_accounts_tax_date_hmrc'] = (isset($_POST['accounts_tax_date_hmrc']) && ($_POST['accounts_tax_date_hmrc'] != '')) ? date("Y-m-d", strtotime($_POST['accounts_tax_date_hmrc'])) : '';
			$data1['crm_company_next_reminder_date'] = (isset($_POST['company_next_reminder_date']) && ($_POST['company_next_reminder_date'] != '')) ? $_POST['company_next_reminder_date'] : '';
			$data1['crm_company_create_task_reminder'] = (isset($_POST['company_create_task_reminder']) && ($_POST['company_create_task_reminder'] != '')) ? $_POST['company_create_task_reminder'] : '';
			$data1['crm_company_custom_reminder'] = (isset($_POST['company_custom_reminder']) && ($_POST['company_custom_reminder'] != '')) ? $_POST['company_custom_reminder'] : '';
			/*$data1['crm_accounts_notes']=(isset($_POST['accounts_notes'])&&($_POST['accounts_notes']!=''))? $_POST['accounts_notes'] : '';*/
			// personal tax return
			$data1['crm_personal_utr_number'] = (isset($_POST['personal_utr_number']) && ($_POST['personal_utr_number'] != '')) ? $_POST['personal_utr_number'] : '';
			$data1['crm_ni_number'] = (isset($_POST['ni_number']) && ($_POST['ni_number'] != '')) ? json_encode($_POST['ni_number']) : '';
			$data1['crm_property_income'] = (isset($_POST['property_income']) && ($_POST['property_income'] != '')) ? $_POST['property_income'] : '';
			$data1['crm_additional_income'] = (isset($_POST['additional_income']) && ($_POST['additional_income'] != '')) ? $_POST['additional_income'] : '';
			$data1['crm_personal_tax_return_date'] = (isset($_POST['personal_tax_return_date']) && ($_POST['personal_tax_return_date'] != '')) ? date('Y-m-d', strtotime($this->date_format($_POST['personal_tax_return_date']))) : '';
			$data1['crm_personal_due_date_return'] = (isset($_POST['personal_due_date_return']) && ($_POST['personal_due_date_return'] != '')) ? date('Y-m-d', strtotime($_POST['personal_due_date_return'])) : '';
			$data1['crm_personal_due_date_online'] = (isset($_POST['personal_due_date_online']) && ($_POST['personal_due_date_online'] != '')) ?  date('Y-m-d', strtotime($_POST['personal_due_date_online'])) : '';
			$data1['crm_personal_next_reminder_date'] = (isset($_POST['personal_next_reminder_date']) && ($_POST['personal_next_reminder_date'] != '')) ? $_POST['personal_next_reminder_date'] : '';
			$data1['crm_personal_task_reminder'] = (isset($_POST['personal_task_reminder']) && ($_POST['personal_task_reminder'] != '')) ? $_POST['personal_task_reminder'] : '';
			$data1['crm_personal_custom_reminder'] = (isset($_POST['personal_custom_reminder']) && ($_POST['personal_custom_reminder'] != '')) ? $_POST['personal_custom_reminder'] : '';
			/*$data1['crm_personal_notes']=(isset($_POST['personal_notes'])&&($_POST['personal_notes']!=''))? $_POST['personal_notes'] : '';*/
			// payroll
			$data1['crm_payroll_acco_off_ref_no'] = (isset($_POST['payroll_acco_off_ref_no']) && ($_POST['payroll_acco_off_ref_no'] != '')) ? $_POST['payroll_acco_off_ref_no'] : '';
			$data1['crm_paye_off_ref_no'] = (isset($_POST['paye_off_ref_no']) && ($_POST['paye_off_ref_no'] != '')) ? $_POST['paye_off_ref_no'] : '';
			$data1['crm_payroll_reg_date'] = (isset($_POST['payroll_reg_date']) && ($_POST['payroll_reg_date'] != '')) ? date('Y-m-d', strtotime($_POST['payroll_reg_date'])) : '';
			$data1['crm_no_of_employees'] = (isset($_POST['no_of_employees']) && ($_POST['no_of_employees'] != '')) ? $_POST['no_of_employees'] : '';
			$data1['crm_first_pay_date'] = (isset($_POST['first_pay_date']) && ($_POST['first_pay_date'] != '')) ? date('Y-m-d', strtotime($_POST['first_pay_date'])) : '';
			$data1['crm_payroll_run'] = (isset($_POST['payroll_run']) && ($_POST['payroll_run'] != '')) ? $_POST['payroll_run'] : '';
			$data1['crm_payroll_run_date'] = (isset($_POST['payroll_run_date']) && ($_POST['payroll_run_date'] != '')) ? $this->date_format($_POST['payroll_run_date']) : '';
			$data1['crm_rti_deadline'] = (isset($_POST['rti_deadline']) && ($_POST['rti_deadline'] != '')) ? $_POST['rti_deadline'] : '';
			$data1['crm_previous_year_require'] = (isset($_POST['previous_year_require']) && ($_POST['previous_year_require'] != '')) ? $_POST['previous_year_require'] : '';
			$data1['crm_payroll_if_yes'] = (isset($_POST['payroll_if_yes']) && ($_POST['payroll_if_yes'] != '')) ? $_POST['payroll_if_yes'] : '';
			$data1['crm_paye_scheme_ceased'] = (isset($_POST['paye_scheme_ceased']) && ($_POST['paye_scheme_ceased'] != '')) ? date('Y-m-d', strtotime($_POST['paye_scheme_ceased'])) : '';
			$data1['crm_payroll_next_reminder_date'] = (isset($_POST['payroll_next_reminder_date']) && ($_POST['payroll_next_reminder_date'] != '')) ? $_POST['payroll_next_reminder_date'] : '';
			$data1['crm_payroll_create_task_reminder'] = (isset($_POST['payroll_create_task_reminder']) && ($_POST['payroll_create_task_reminder'] != '')) ? $_POST['payroll_create_task_reminder'] : '';
			$data1['crm_payroll_add_custom_reminder'] = (isset($_POST['payroll_add_custom_reminder']) && ($_POST['payroll_add_custom_reminder'] != '')) ? $_POST['payroll_add_custom_reminder'] : '';
			$data1['crm_staging_date'] = (isset($_POST['staging_date']) && ($_POST['staging_date'] != '')) ? date('Y-m-d', strtotime($_POST['staging_date'])) : '';
			$data1['crm_pension_id'] = (isset($_POST['pension_id']) && ($_POST['pension_id'] != '')) ? $_POST['pension_id'] : '';
			$data1['crm_pension_subm_due_date'] = (isset($_POST['pension_subm_due_date']) && ($_POST['pension_subm_due_date'] != '')) ? $this->date_format($_POST['pension_subm_due_date']) : '';
			$data1['crm_postponement_date'] = (isset($_POST['defer_post_upto']) && ($_POST['defer_post_upto'] != '')) ? $_POST['defer_post_upto'] : '';
			$data1['crm_the_pensions_regulator_opt_out_date'] = (isset($_POST['pension_regulator_date']) && ($_POST['pension_regulator_date'] != '')) ? $_POST['pension_regulator_date'] : '';
			$data1['crm_re_enrolment_date'] = (isset($_POST['paye_re_enrolment_date']) && ($_POST['paye_re_enrolment_date'] != '')) ? $this->date_format($_POST['paye_re_enrolment_date']) : '';
			$data1['crm_declaration_of_compliance_due_date  '] = (isset($_POST['declaration_of_compliance_due_date']) && ($_POST['declaration_of_compliance_due_date'] != '')) ? $this->date_format($_POST['declaration_of_compliance_due_date']) : '';
			$data1['crm_declaration_of_compliance_submission  '] = (isset($_POST['declaration_of_compliance_last_filed']) && ($_POST['declaration_of_compliance_last_filed'] != '')) ? $_POST['declaration_of_compliance_last_filed'] : '';
			$data1['crm_paye_pension_provider'] = (isset($_POST['paye_pension_provider']) && ($_POST['paye_pension_provider'] != '')) ? $_POST['paye_pension_provider'] : '';
			$data1['crm_pension_id'] = (isset($_POST['paye_pension_provider_userid']) && ($_POST['paye_pension_provider_userid'] != '')) ? $_POST['paye_pension_provider_userid'] : '';
			$data1['crm_paye_pension_provider_password'] = (isset($_POST['paye_pension_provider_password']) && ($_POST['paye_pension_provider_password'] != '')) ? $_POST['paye_pension_provider_password'] : '';
			$data1['crm_employer_contri_percentage'] = (isset($_POST['employer_contri_percentage']) && ($_POST['employer_contri_percentage'] != '')) ? $_POST['employer_contri_percentage'] : '';
			$data1['crm_employee_contri_percentage'] = (isset($_POST['employee_contri_percentage']) && ($_POST['employee_contri_percentage'] != '')) ? $_POST['employee_contri_percentage'] : '';
			$data1['crm_pension_notes'] = (isset($_POST['pension_notes']) && ($_POST['pension_notes'] != '')) ? $_POST['pension_notes'] : '';
			$data1['crm_pension_next_reminder_date'] = (isset($_POST['pension_next_reminder_date']) && ($_POST['pension_next_reminder_date'] != '')) ? $_POST['pension_next_reminder_date'] : '';
			$data1['crm_pension_create_task_reminder'] = (isset($_POST['pension_create_task_reminder']) && ($_POST['pension_create_task_reminder'] != '')) ? $_POST['pension_create_task_reminder'] : '';
			$data1['crm_pension_add_custom_reminder'] = (isset($_POST['pension_add_custom_reminder']) && ($_POST['pension_add_custom_reminder'] != '')) ? $_POST['pension_add_custom_reminder'] : '';
			$data1['crm_cis_contractor'] = (isset($_POST['cis_contractor']) && ($_POST['cis_contractor'] != '')) ? $_POST['cis_contractor'] : '';
			$data1['crm_cis_contractor_start_date'] = (isset($_POST['cis_contractor_start_date']) && ($_POST['cis_contractor_start_date'] != '')) ? $this->date_format($_POST['cis_contractor_start_date']) : '';
			$data1['crm_cis_scheme_notes'] = (isset($_POST['cis_scheme_notes']) && ($_POST['cis_scheme_notes'] != '')) ? $_POST['cis_scheme_notes'] : '';
			$data1['crm_cis_subcontractor'] = (isset($_POST['cis_subcontractor']) && ($_POST['cis_subcontractor'] != '')) ? $_POST['cis_subcontractor'] : '';
			$data1['crm_cis_subcontractor_start_date'] = (isset($_POST['cis_subcontractor_start_date']) && ($_POST['cis_subcontractor_start_date'] != '')) ? $this->date_format($_POST['cis_subcontractor_start_date']) : '';
			$data1['crm_cis_subcontractor_scheme_notes'] = (isset($_POST['cis_subcontractor_scheme_notes']) && ($_POST['cis_subcontractor_scheme_notes'] != '')) ? $_POST['cis_subcontractor_scheme_notes'] : '';
			$data1['crm_cis_next_reminder_date'] = (isset($_POST['cis_next_reminder_date']) && ($_POST['cis_next_reminder_date'] != '')) ? $_POST['cis_next_reminder_date'] : '';
			$data1['crm_cis_create_task_reminder'] = (isset($_POST['cis_create_task_reminder']) && ($_POST['cis_create_task_reminder'] != '')) ? $_POST['cis_create_task_reminder'] : '';
			$data1['crm_cis_add_custom_reminder'] = (isset($_POST['cis_add_custom_reminder']) && ($_POST['cis_add_custom_reminder'] != '')) ? $_POST['cis_add_custom_reminder'] : '';
			/** 11-09-2018 **/
			$data1['crm_cissub_next_reminder_date'] = (isset($_POST['cissub_next_reminder_date']) && ($_POST['cissub_next_reminder_date'] != '')) ? $_POST['cissub_next_reminder_date'] : '';
			$data1['crm_cissub_create_task_reminder'] = (isset($_POST['cissub_create_task_reminder']) && ($_POST['cissub_create_task_reminder'] != '')) ? $_POST['cissub_create_task_reminder'] : '';
			$data1['crm_cissub_add_custom_reminder'] = (isset($_POST['cissub_add_custom_reminder']) && ($_POST['cissub_add_custom_reminder'] != '')) ? $_POST['cissub_add_custom_reminder'] : '';
			/** end of 11-09-2018 **/
			$data1['crm_p11d_latest_action_date'] = (isset($_POST['p11d_start_date']) && ($_POST['p11d_start_date'] != '')) ? date('Y-m-d', strtotime($_POST['p11d_start_date'])) : '';
			$data1['crm_p11d_latest_action'] = (isset($_POST['p11d_todo']) && ($_POST['p11d_todo'] != '')) ? $_POST['p11d_todo'] : '';
			$data1['crm_latest_p11d_submitted'] = (isset($_POST['p11d_first_benefit_date']) && ($_POST['p11d_first_benefit_date'] != '')) ? date('Y-m-d', strtotime($_POST['p11d_first_benefit_date'])) : '';
			$data1['crm_next_p11d_return_due'] = (isset($_POST['p11d_due_date']) && ($_POST['p11d_due_date'] != '')) ? $this->date_format($_POST['p11d_due_date']) : '';
			$data1['crm_p11d_previous_year_require'] = (isset($_POST['p11d_previous_year_require']) && ($_POST['p11d_previous_year_require'] != '')) ? $_POST['p11d_previous_year_require'] : '';
			$data1['crm_p11d_payroll_if_yes'] = (isset($_POST['p11d_payroll_if_yes']) && ($_POST['p11d_payroll_if_yes'] != '')) ? $_POST['p11d_payroll_if_yes'] : '';
			$data1['crm_p11d_records_received'] = (isset($_POST['p11d_paye_scheme_ceased']) && ($_POST['p11d_paye_scheme_ceased'] != '')) ? $_POST['p11d_paye_scheme_ceased'] : '';
			$data1['crm_p11d_next_reminder_date'] = (isset($_POST['p11d_next_reminder_date']) && ($_POST['p11d_next_reminder_date'] != '')) ? $_POST['p11d_next_reminder_date'] : '';
			$data1['crm_p11d_create_task_reminder'] = (isset($_POST['p11d_create_task_reminder']) && ($_POST['p11d_create_task_reminder'] != '')) ? $_POST['p11d_create_task_reminder'] : '';
			$data1['crm_p11d_add_custom_reminder'] = (isset($_POST['p11d_add_custom_reminder']) && ($_POST['p11d_add_custom_reminder'] != '')) ? $_POST['p11d_add_custom_reminder'] : '';
			/*$data1['crm_p11d_notes']=(isset($_POST['p11d_notes'])&&($_POST['p11d_notes']!=''))? $_POST['p11d_notes'] : '';*/ //vat return
			$data1['crm_vat_number_one'] = (isset($_POST['vat_number_one']) && ($_POST['vat_number_one'] != '')) ? $_POST['vat_number_one'] : '';
			$data1['crm_vat_date_of_registration'] = (isset($_POST['vat_registration_date']) && ($_POST['vat_registration_date'] != '')) ? $_POST['vat_registration_date'] : '';
			$data1['crm_vat_frequency'] = (isset($_POST['vat_frequency']) && ($_POST['vat_frequency'] != '')) ? $_POST['vat_frequency'] : '';
			$data1['crm_vat_quater_end_date'] = (isset($_POST['vat_quater_end_date']) && ($_POST['vat_quater_end_date'] != '')) ? date('Y-m-d', strtotime($_POST['vat_quater_end_date'])) : '';
			$data1['crm_vat_quarters'] = (isset($_POST['vat_quarters']) && ($_POST['vat_quarters'] != '')) ? $_POST['vat_quarters'] : '0';
			$data1['crm_last_vat_return_filed_upto'] = (isset($_POST['crm_last_vat_return_filed_upto']) && ($_POST['crm_last_vat_return_filed_upto'] != '')) ? date('Y-m-d', strtotime($_POST['crm_last_vat_return_filed_upto'])) : '';
			$data1['crm_vat_scheme'] = (isset($_POST['vat_scheme']) && ($_POST['vat_scheme'] != '')) ? $_POST['vat_scheme'] : '';
			$data1['crm_flat_rate_category'] = (isset($_POST['flat_rate_category']) && ($_POST['flat_rate_category'] != '')) ? $_POST['flat_rate_category'] : '';
			$data1['crm_flat_rate_percentage'] = (isset($_POST['flat_rate_percentage']) && ($_POST['flat_rate_percentage'] != '')) ? $_POST['flat_rate_percentage'] : '';
			$data1['crm_direct_debit'] = (isset($_POST['direct_debit']) && ($_POST['direct_debit'] != '')) ? $_POST['direct_debit'] : '';
			$data1['crm_annual_accounting_scheme'] = (isset($_POST['annual_accounting_scheme']) && ($_POST['annual_accounting_scheme'] != '')) ? $_POST['annual_accounting_scheme'] : '';
			$data1['crm_box5_of_last_quarter_submitted'] = (isset($_POST['box5_of_last_quarter_submitted']) && ($_POST['box5_of_last_quarter_submitted'] != '')) ? $_POST['box5_of_last_quarter_submitted'] : '0';
			$data1['crm_vat_address'] = (isset($_POST['vat_address']) && ($_POST['vat_address'] != '')) ? $_POST['vat_address'] : '';
			$data1['crm_vat_next_reminder_date'] = (isset($_POST['vat_next_reminder_date']) && ($_POST['vat_next_reminder_date'] != '')) ? $_POST['vat_next_reminder_date'] : '';
			$data1['crm_vat_create_task_reminder'] = (isset($_POST['vat_create_task_reminder']) && ($_POST['vat_create_task_reminder'] != '')) ? $_POST['vat_create_task_reminder'] : '';
			$data1['crm_vat_add_custom_reminder'] = (isset($_POST['vat_add_custom_reminder']) && ($_POST['vat_add_custom_reminder'] != '')) ? $_POST['vat_add_custom_reminder'] : '';
			/*$data1['crm_vat_notes']=(isset($_POST['vat_notes'])&&($_POST['vat_notes']!=''))? $_POST['vat_notes'] : '';*/
			//management accounts
			$data1['crm_bookkeeping'] = (isset($_POST['bookkeeping']) && ($_POST['bookkeeping'] != '')) ? $_POST['bookkeeping'] : '';
			$data1['crm_next_booking_date'] = (isset($_POST['next_booking_date']) && ($_POST['next_booking_date'] != '')) ? $this->date_format($_POST['next_booking_date']) : '';
			$data1['crm_method_bookkeeping'] = (isset($_POST['method_bookkeeping']) && ($_POST['method_bookkeeping'] != '')) ? $_POST['method_bookkeeping'] : '';
			$data1['crm_client_provide_record'] = (isset($_POST['client_provide_record']) && ($_POST['client_provide_record'] != '')) ? $_POST['client_provide_record'] : '';
			$data1['crm_bookkeep_next_reminder_date'] = (isset($_POST['bookkeep_next_reminder_date']) && ($_POST['bookkeep_next_reminder_date'] != '')) ? $_POST['bookkeep_next_reminder_date'] : '';
			$data1['crm_bookkeep_create_task_reminder'] = (isset($_POST['bookkeep_create_task_reminder']) && ($_POST['bookkeep_create_task_reminder'] != '')) ? $_POST['bookkeep_create_task_reminder'] : '';
			$data1['crm_bookkeep_add_custom_reminder'] = (isset($_POST['bookkeep_add_custom_reminder']) && ($_POST['bookkeep_add_custom_reminder'] != '')) ? $_POST['bookkeep_add_custom_reminder'] : '';
			$data1['crm_manage_acc_fre'] = (isset($_POST['manage_acc_fre']) && ($_POST['manage_acc_fre'] != '')) ? $_POST['manage_acc_fre'] : '';
			$data1['crm_next_manage_acc_date'] = (isset($_POST['next_manage_acc_date']) && ($_POST['next_manage_acc_date'] != '')) ? $this->date_format($_POST['next_manage_acc_date']) : '';
			$data1['crm_manage_method_bookkeeping'] = (isset($_POST['manage_method_bookkeeping']) && ($_POST['manage_method_bookkeeping'] != '')) ? $_POST['manage_method_bookkeeping'] : '';
			$data1['crm_manage_client_provide_record'] = (isset($_POST['manage_client_provide_record']) && ($_POST['manage_client_provide_record'] != '')) ? $_POST['manage_client_provide_record'] : '';
			$data1['crm_manage_next_reminder_date'] = (isset($_POST['manage_next_reminder_date']) && ($_POST['manage_next_reminder_date'] != '')) ? $_POST['manage_next_reminder_date'] : '';
			$data1['crm_manage_create_task_reminder'] = (isset($_POST['manage_create_task_reminder']) && ($_POST['manage_create_task_reminder'] != '')) ? $_POST['manage_create_task_reminder'] : '';
			$data1['crm_manage_add_custom_reminder'] = (isset($_POST['manage_add_custom_reminder']) && ($_POST['manage_add_custom_reminder'] != '')) ? $_POST['manage_add_custom_reminder'] : '';
			/*$data1['crm_manage_notes']=(isset($_POST['manage_notes'])&&($_POST['manage_notes']!=''))? $_POST['manage_notes'] : '';*/
			//Investigation Insurance
			$data1['crm_invesitgation_insurance'] = (isset($_POST['insurance_start_date']) && ($_POST['insurance_start_date'] != '')) ? $this->date_format($_POST['insurance_start_date']) : '';
			$data1['crm_insurance_renew_date'] = (isset($_POST['insurance_renew_date']) && ($_POST['insurance_renew_date'] != '')) ? $this->date_format($_POST['insurance_renew_date']) : '';
			$data1['crm_insurance_provider'] = (isset($_POST['insurance_provider']) && ($_POST['insurance_provider'] != '')) ? $_POST['insurance_provider'] : '';
			$data1['crm_claims_note'] = (isset($_POST['claims_note']) && ($_POST['claims_note'] != '')) ? $_POST['claims_note'] : '';
			$data1['crm_insurance_next_reminder_date'] = (isset($_POST['insurance_next_reminder_date']) && ($_POST['insurance_next_reminder_date'] != '')) ? $_POST['insurance_next_reminder_date'] : '';
			$data1['crm_insurance_create_task_reminder'] = (isset($_POST['insurance_create_task_reminder']) && ($_POST['insurance_create_task_reminder'] != '')) ? $_POST['insurance_create_task_reminder'] : '';
			$data1['crm_insurance_add_custom_reminder'] = (isset($_POST['insurance_add_custom_reminder']) && ($_POST['insurance_add_custom_reminder'] != '')) ? $_POST['insurance_add_custom_reminder'] : '';
			$data1['crm_registered_start_date'] = (isset($_POST['registered_start_date']) && ($_POST['registered_start_date'] != '')) ? $this->date_format($_POST['registered_start_date']) : '';
			$data1['crm_registered_renew_date'] = (isset($_POST['registered_renew_date']) && ($_POST['registered_renew_date'] != '')) ? $this->date_format($_POST['registered_renew_date']) : '';
			$data1['crm_registered_office_inuse'] = (isset($_POST['registered_office_inuse']) && ($_POST['registered_office_inuse'] != '')) ? $_POST['registered_office_inuse'] : '';
			$data1['crm_registered_claims_note'] = (isset($_POST['registered_claims_note']) && ($_POST['registered_claims_note'] != '')) ? $_POST['registered_claims_note'] : '';
			$data1['crm_registered_next_reminder_date'] = (isset($_POST['registered_next_reminder_date']) && ($_POST['registered_next_reminder_date'] != '')) ? $_POST['registered_next_reminder_date'] : '';
			$data1['crm_registered_create_task_reminder'] = (isset($_POST['registered_create_task_reminder']) && ($_POST['registered_create_task_reminder'] != '')) ? $_POST['registered_create_task_reminder'] : '';
			$data1['crm_registered_add_custom_reminder'] = (isset($_POST['registered_add_custom_reminder']) && ($_POST['registered_add_custom_reminder'] != '')) ? $_POST['registered_add_custom_reminder'] : '';
			$data1['crm_investigation_start_date'] = (isset($_POST['investigation_start_date']) && ($_POST['investigation_start_date'] != '')) ? $this->date_format($_POST['investigation_start_date']) : '';
			$data1['crm_investigation_end_date'] = (isset($_POST['investigation_end_date']) && ($_POST['investigation_end_date'] != '')) ? $this->date_format($_POST['investigation_end_date']) : '';
			$data1['crm_investigation_note'] = (isset($_POST['investigation_note']) && ($_POST['investigation_note'] != '')) ? $_POST['investigation_note'] : '';
			$data1['crm_investigation_next_reminder_date'] = (isset($_POST['investigation_next_reminder_date']) && ($_POST['investigation_next_reminder_date'] != '')) ? $_POST['investigation_next_reminder_date'] : '';
			$data1['crm_investigation_create_task_reminder'] = (isset($_POST['investigation_create_task_reminder']) && ($_POST['investigation_create_task_reminder'] != '')) ? $_POST['investigation_create_task_reminder'] : '';
			$data1['crm_investigation_add_custom_reminder'] = (isset($_POST['investigation_add_custom_reminder']) && ($_POST['investigation_add_custom_reminder'] != '')) ? $_POST['investigation_add_custom_reminder'] : '';
			/*$data1['crm_tax_investigation_note']=(isset($_POST['tax_investigation_note'])&&($_POST['tax_investigation_note']!=''))? $_POST['tax_investigation_note'] : '';*/
			$data1['select_responsible_type'] = (isset($_POST['select_responsible_type']) && ($_POST['select_responsible_type'] != '')) ? $_POST['select_responsible_type'] : '';
			// notes tab
			$data1['crm_notes_info'] = (isset($_POST['notes_info']) && ($_POST['notes_info'] != '')) ? $_POST['notes_info'] : '';
			/** activity log for notes **/
			if (isset($_POST['notes_info']) && ($_POST['notes_info'] != '')) {

				$activity_datas['log'] = "Notes Updated ---" . $_POST['notes_info'];
				$activity_datas['createdTime'] = time();
				$activity_datas['module'] = 'Client';
				$activity_datas['sub_module'] = 'Client Notes';
				$activity_datas['user_id'] = $userid;
				$activity_datas['module_id'] = (isset($_POST['user_id']) && ($_POST['user_id'] == '')) ? $in : $_POST['user_id'];

				$this->Common_mdl->insert('activity_log', $activity_datas);
			}
			/** end of activity log for notes **/
			// documents tab
			$data1['crm_documents'] = (isset($_FILES['document']['name']) && ($_FILES['document']['name'] != '')) ? $this->Common_mdl->do_upload($_FILES['document'], 'uploads') : '';
			//$data1['created_date']=time();
			$data1['autosave_status'] = 0;



			$updates_client = $this->Common_mdl->updatewithaffectedrow_count('client', $data1, 'user_id', $_POST['user_id']);

			//it is for add assignes to firm_assigness table.
			if (!empty($_POST['assignees_values'])) {
				$assignees = explode(',', $_POST['assignees_values']);
				$this->Client_model->Update_Client_assignee($old_client_data['id'], $assignees);
			}
			if ($updates_client) {
				$this->Common_mdl->update_field($this->input->post(), $_POST['user_id']);
			}


			/*
        $data_inv['confirmation_invoice']=(isset($_POST['invoice_cs'])&&($_POST['invoice_cs']!=''))? $_POST['invoice_cs'] : '';
        $data_inv['accounts_invoice']=(isset($_POST['invoice_as'])&&($_POST['invoice_as']!=''))? $_POST['invoice_as'] : '';
        $data_inv['company_tax_invoice']=(isset($_POST['invoice_ct'])&&($_POST['invoice_ct']!=''))? $_POST['invoice_ct'] : '';
        $data_inv['personal_tax_invoice']=(isset($_POST['invoice_pt'])&&($_POST['invoice_pt']!=''))? $_POST['invoice_pt'] : '';
        $data_inv['payroll_invoice']=(isset($_POST['invoice_pr'])&&($_POST['invoice_pr']!=''))? $_POST['invoice_pr'] : '';
        $data_inv['work_pension_invoice']=(isset($_POST['invoice_pn'])&&($_POST['invoice_pn']!=''))? $_POST['invoice_pn'] : '';
        $data_inv['cis_invoice']=(isset($_POST['invoice_cis'])&&($_POST['invoice_cis']!=''))? $_POST['invoice_cis'] : '';
        $data_inv['cissub_invoice']=(isset($_POST['invoice_sub'])&&($_POST['invoice_sub']!=''))? $_POST['invoice_sub'] : '';
        $data_inv['p11d_invoice']=(isset($_POST['invoice_p11d'])&&($_POST['invoice_p11d']!=''))? $_POST['invoice_p11d'] : '';
        $data_inv['vat_invoice']=(isset($_POST['invoice_vt'])&&($_POST['invoice_vt']!=''))? $_POST['invoice_vt'] : '';
        $data_inv['bookkeep_invoice']=(isset($_POST['invoice_book'])&&($_POST['invoice_book']!=''))? $_POST['invoice_book'] : '';
        $data_inv['management_invoice']=(isset($_POST['invoice_ma'])&&($_POST['invoice_ma']!=''))? $_POST['invoice_ma'] : '';
        $data_inv['invest_insurance_invoice']=(isset($_POST['invoice_insuance'])&&($_POST['invoice_insuance']!=''))? $_POST['invoice_insuance'] : '';
        $data_inv['registered_invoice']=(isset($_POST['invoice_reg'])&&($_POST['invoice_reg']!=''))? $_POST['invoice_reg'] : '';
        $data_inv['tax_advice_invoice']=(isset($_POST['invoice_taxadv'])&&($_POST['invoice_taxadv']!=''))? $_POST['invoice_taxadv'] : '';
        $data_inv['tax_invest_invoice']=(isset($_POST['invoice_taxinves'])&&($_POST['invoice_taxinves']!=''))? $_POST['invoice_taxinves'] : '';
        $data_inv['created_date']=time();
        $this->Common_mdl->update('client_invoice',$data_inv,'user_id',$_POST['user_id']);*/

			/* ******  */
			if ($_POST['legal_form'] == "Partnership" ||  $_POST['legal_form'] ==  "Self Assessment") {
				$datalegal['user_id'] = $_POST['user_id'];
				$datalegal['company_no'] = isset($_POST['fa_cmyno']) ? $_POST['fa_cmyno'] : "";
				$datalegal['incorporation_date'] = isset($_POST['fa_incordate']) ? $_POST['fa_incordate'] : "";
				$datalegal['regist_address'] = isset($_POST['fa_registadres']) ? $_POST['fa_registadres'] : "";
				$datalegal['turnover'] = isset($_POST['fa_turnover']) ? $_POST['fa_turnover'] : "";
				$datalegal['date_of_trading'] = isset($_POST['fa_dateoftrading']) ? $_POST['fa_dateoftrading'] : "";
				$datalegal['sic_code'] = isset($_POST['fa_siccode']) ? $_POST['fa_siccode'] : "";
				$datalegal['nuture_of_business'] = isset($_POST['fa_nutureofbus']) ? $_POST['fa_nutureofbus'] : "";
				$datalegal['company_utr'] = isset($_POST['fa_cmyutr']) ? $_POST['fa_cmyutr'] : "";
				$datalegal['company_house_auth_code'] = isset($_POST['fa_cmyhouse']) ? $_POST['fa_cmyhouse'] : "";

				$datalegal['trading_as'] = isset($_POST['bus_tradingas']) ? $_POST['bus_tradingas'] : "";
				$datalegal['commenced_trading'] = isset($_POST['bus_commencedtrading']) ? $_POST['bus_commencedtrading'] : "";
				$datalegal['register_sa'] = isset($_POST['bus_regist']) ? $_POST['bus_regist'] : "";
				$datalegal['bus_turnover'] = isset($_POST['bus_turnover']) ? $_POST['bus_turnover'] : "";
				$datalegal['bus_nuture_of_business'] = isset($_POST['bus_nutureofbus']) ? $_POST['bus_nutureofbus'] : "";

				$check_exist = $this->db->query("select * from client_legalform where user_id=" . $_POST['user_id'])->row_array();
				if (count($check_exist)) {
					$this->db->update('client_legalform', $datalegal, "user_id = " . $_POST['user_id']);
				} else {
					$this->db->insert('client_legalform', $datalegal);
				}
			} else if ($old_client_data['crm_legal_form'] == "Partnership" ||  $old_client_data['crm_legal_form'] ==  "Self Assessment") {
				$this->db->query("delete from client_legalform where user_id= " . $_POST['user_id']);
			}



			if (empty($old_client_data['crm_other_chase_for_info']) && isset($_POST['chase_for_info']) == 'on') {
				// $this->Send_ChaseForInfo_mail($old_client_data['user_id']);
			}

			if (!empty($_POST['removed_cus_reminder'])) {
				$this->db->query("DELETE FROM custom_service_reminder where id IN (" . $_POST['removed_cus_reminder'] . ")");
			}

			/*Update sercice reminder*/
			$NEW_SERVICE_DATA = $this->db->query("SELECT id," . $Service_Fields . " FROM client WHERE user_id=" . $_POST['user_id'])->row_array();
			$client_id = $NEW_SERVICE_DATA['id'];
			unset($NEW_SERVICE_DATA['id']);
			$service_ids = array_fill_keys(range(1, 15), 1);
			$this->Service_Reminder_Model->Setup_Client_ServiceReminders($client_id, $service_ids);




			/** 05-07-2018 **/

			//$this->Task_invoice_model->for_sample_client_task($_POST['user_id']);

			/** end of 05-07-2018 **/

			// if($updates_client){
			// $this->email_reminder_sms($_POST['user_id']);
			// }
			//DON'T REMOVE THIS 


			$tot_rec = $this->db->query("select * from client where user_id=" . $_POST['user_id'])->row_array();
			if (!empty($tot_rec['conf_statement']))
				$after_data1['conf_statement'] = (isset(json_decode($tot_rec['conf_statement'])->tab)) ? json_decode($tot_rec['conf_statement'])->tab : '';
			else
				$after_data1['conf_statement'] = '';

			if (!empty($tot_rec['accounts']))
				$after_data1['accounts'] = (isset(json_decode($tot_rec['accounts'])->tab)) ? json_decode($tot_rec['accounts'])->tab : '';
			else
				$after_data1['accounts'] = '';
			if (!empty($tot_rec['company_tax_return']))
				$after_data1['company_tax_return'] = (isset(json_decode($tot_rec['company_tax_return'])->tab)) ? json_decode($tot_rec['company_tax_return'])->tab : '';
			else
				$after_data1['company_tax_return'] = '';
			if (!empty($tot_rec['personal_tax_return']))
				$after_data1['personal_tax_return'] = (isset(json_decode($tot_rec['personal_tax_return'])->tab)) ? json_decode($tot_rec['personal_tax_return'])->tab : '';
			else
				$after_data1['personal_tax_return'] = '';

			if (!empty($tot_rec['payroll']))
				$after_data1['payroll'] = (isset(json_decode($tot_rec['payroll'])->tab)) ? json_decode($tot_rec['payroll'])->tab : '';
			else
				$after_data1['payroll'] = '';

			if (!empty($tot_rec['workplace']))
				$after_data1['workplace'] = (isset(json_decode($tot_rec['workplace'])->tab)) ? json_decode($tot_rec['workplace'])->tab : '';
			else
				$after_data1['workplace'] = '';

			if (!empty($tot_rec['vat']))
				$after_data1['vat'] = (isset(json_decode($tot_rec['vat'])->tab)) ? json_decode($tot_rec['vat'])->tab : '';
			else
				$after_data1['vat'] = '';

			if (!empty($tot_rec['cis']))
				$after_data1['cis'] = (isset(json_decode($tot_rec['cis'])->tab)) ? json_decode($tot_rec['cis'])->tab : '';
			else
				$after_data1['cis'] = '';

			if (!empty($tot_rec['cissub']))
				$after_data1['cissub'] = (isset(json_decode($tot_rec['cissub'])->tab)) ? json_decode($tot_rec['cissub'])->tab : '';
			else
				$after_data1['cissub'] = '';

			if (!empty($tot_rec['p11d']))
				$after_data1['p11d'] = (isset(json_decode($tot_rec['p11d'])->tab)) ? json_decode($tot_rec['p11d'])->tab : '';
			else
				$after_data1['p11d'] = '';


			if (!empty($tot_rec['bookkeep']))
				$after_data1['bookkeep'] =  (isset(json_decode($tot_rec['bookkeep'])->tab)) ? json_decode($tot_rec['bookkeep'])->tab : '';
			else
				$after_data1['bookkeep'] = '';
			if (!empty($tot_rec['management']))
				$after_data1['management'] =  (isset(json_decode($tot_rec['management'])->tab)) ? json_decode($tot_rec['management'])->tab : '';
			else
				$after_data1['management'] = '';
			if (!empty($tot_rec['investgate']))
				$after_data1['investgate'] =  (isset(json_decode($tot_rec['investgate'])->tab)) ? json_decode($tot_rec['investgate'])->tab : '';
			else
				$after_data1['investgate'] = '';
			if (!empty($tot_rec['registered']))
				$after_data1['registered'] =  (isset(json_decode($tot_rec['registered'])->tab)) ? json_decode($tot_rec['registered'])->tab : '';
			else
				$after_data1['registered'] = '';
			if (!empty($tot_rec['taxadvice']))
				$after_data1['taxadvice'] =  (isset(json_decode($tot_rec['taxadvice'])->tab)) ? json_decode($tot_rec['taxadvice'])->tab : '';
			else
				$after_data1['taxadvice'] = '';
			if (!empty($tot_rec['taxinvest']))
				$after_data1['taxinvest'] =  (isset(json_decode($tot_rec['taxinvest'])->tab)) ? json_decode($tot_rec['taxinvest'])->tab : '';
			else
				$after_data1['taxinvest'] = '';

			foreach ($other_services as $key => $value) {
				if (!empty($tot_rec[$value['services_subnames']])) {
					$after_data1[$value['services_subnames']] = (isset(json_decode($tot_rec[$value['services_subnames']])->tab)) ? json_decode($tot_rec[$value['services_subnames']])->tab : '';
				} else {
					$after_data1[$value['services_subnames']] = '';
				}

				if ($before_data1[$value['services_subnames']] != $after_data1[$value['services_subnames']]) {
					$activity_datas['log'] = "" . $value['service_name'] . " Updated";
					$activity_datas['createdTime'] = time();
					$activity_datas['module'] = 'Client';
					$activity_datas['sub_module'] = $value['service_name'];
					$activity_datas['user_id'] = $userid;
					$activity_datas['module_id'] = $_POST['user_id'];

					$this->Common_mdl->insert('activity_log', $activity_datas);
				}
			}

			if ($before_data1['conf_statement'] != $after_data1['conf_statement']) {
				$re_conf_statement = 'yes';
				/** for activity section **/
				$activity_datas['log'] = "Confirmation Statement Updated";
				$activity_datas['createdTime'] = time();
				$activity_datas['module'] = 'Client';
				$activity_datas['sub_module'] = 'Confirmation statement';
				$activity_datas['user_id'] = $userid;
				$activity_datas['module_id'] = $_POST['user_id'];

				$this->Common_mdl->insert('activity_log', $activity_datas);
				/** end of activity section **/
			}
			if ($before_data1['accounts'] != $after_data1['accounts']) {
				$re_accounts = 'yes';
				/** for activity section **/
				$activity_datas['log'] = "Accounts Updated";
				$activity_datas['createdTime'] = time();
				$activity_datas['module'] = 'Client';
				$activity_datas['sub_module'] = 'Accounts';
				$activity_datas['user_id'] = $userid;
				$activity_datas['module_id'] = $_POST['user_id'];

				$this->Common_mdl->insert('activity_log', $activity_datas);
				/** end of activity section **/
			}
			if ($before_data1['company_tax_return'] != $after_data1['company_tax_return']) {
				$re_company_tax_return = 'yes';
				/** for activity section **/
				$activity_datas['log'] = "Company Tax Return Updated";
				$activity_datas['createdTime'] = time();
				$activity_datas['module'] = 'Client';
				$activity_datas['sub_module'] = 'Company Tax Return';
				$activity_datas['user_id'] = $userid;
				$activity_datas['module_id'] = $_POST['user_id'];

				$this->Common_mdl->insert('activity_log', $activity_datas);
				/** end of activity section **/
			}
			if ($before_data1['personal_tax_return'] != $after_data1['personal_tax_return']) {
				$re_personal_tax_return = 'yes';
				/** for activity section **/
				$activity_datas['log'] = "Personal Tax Return Updated";
				$activity_datas['createdTime'] = time();
				$activity_datas['module'] = 'Client';
				$activity_datas['sub_module'] = 'Personal Tax Return';
				$activity_datas['user_id'] = $userid;
				$activity_datas['module_id'] = $_POST['user_id'];

				$this->Common_mdl->insert('activity_log', $activity_datas);
				/** end of activity section **/
			}
			if ($before_data1['payroll'] != $after_data1['payroll']) {
				$re_payroll = 'yes';
				/** for activity section **/
				$activity_datas['log'] = "Payroll Updated";
				$activity_datas['createdTime'] = time();
				$activity_datas['module'] = 'Client';
				$activity_datas['sub_module'] = 'Payroll';
				$activity_datas['user_id'] = $userid;
				$activity_datas['module_id'] = $_POST['user_id'];

				$this->Common_mdl->insert('activity_log', $activity_datas);
				/** end of activity section **/
			}
			if ($before_data1['workplace'] != $after_data1['workplace']) {
				$re_workplace = 'yes';
				/** for activity section **/
				$activity_datas['log'] = "WorkPlace Pension - AE Updated";
				$activity_datas['createdTime'] = time();
				$activity_datas['module'] = 'Client';
				$activity_datas['sub_module'] = 'WorkPlace Pension - AE';
				$activity_datas['user_id'] = $userid;
				$activity_datas['module_id'] = $_POST['user_id'];

				$this->Common_mdl->insert('activity_log', $activity_datas);
				/** end of activity section **/
			}
			if ($before_data1['vat'] != $after_data1['vat']) {
				$re_vat = 'yes';
				/** for activity section **/
				$activity_datas['log'] = "VAT Updated";
				$activity_datas['createdTime'] = time();
				$activity_datas['module'] = 'Client';
				$activity_datas['sub_module'] = 'VAT';
				$activity_datas['user_id'] = $userid;
				$activity_datas['module_id'] = $_POST['user_id'];

				$this->Common_mdl->insert('activity_log', $activity_datas);
				/** end of activity section **/
			}
			if ($before_data1['cis'] != $after_data1['cis']) {
				$re_cis = 'yes';
				/** for activity section **/
				$activity_datas['log'] = "CIS - Contractor Updated";
				$activity_datas['createdTime'] = time();
				$activity_datas['module'] = 'Client';
				$activity_datas['sub_module'] = 'CIS - Contractor';
				$activity_datas['user_id'] = $userid;
				$activity_datas['module_id'] = $_POST['user_id'];

				$this->Common_mdl->insert('activity_log', $activity_datas);
				/** end of activity section **/
			}
			if ($before_data1['cissub'] != $after_data1['cissub']) {
				$re_cissub = 'yes';
				/** for activity section **/
				$activity_datas['log'] = "CIS - Sub Contractor Updated";
				$activity_datas['createdTime'] = time();
				$activity_datas['module'] = 'Client';
				$activity_datas['sub_module'] = 'CIS - Sub Contractor';
				$activity_datas['user_id'] = $userid;
				$activity_datas['module_id'] = $_POST['user_id'];

				$this->Common_mdl->insert('activity_log', $activity_datas);
				/** end of activity section **/
			}
			if ($before_data1['p11d'] != $after_data1['p11d']) {
				$re_p11d = 'yes';
				/** for activity section **/
				$activity_datas['log'] = "P11D Updated";
				$activity_datas['createdTime'] = time();
				$activity_datas['module'] = 'Client';
				$activity_datas['sub_module'] = 'P11D';
				$activity_datas['user_id'] = $userid;
				$activity_datas['module_id'] = $_POST['user_id'];

				$this->Common_mdl->insert('activity_log', $activity_datas);
				/** end of activity section **/
			}
			if ($before_data1['bookkeep'] != $after_data1['bookkeep']) {
				$re_management = 'yes';
				/** for activity section **/
				$activity_datas['log'] = "Bookkeeping Updated";
				$activity_datas['createdTime'] = time();
				$activity_datas['module'] = 'Client';
				$activity_datas['sub_module'] = 'Bookkeeping';
				$activity_datas['user_id'] = $userid;
				$activity_datas['module_id'] = $_POST['user_id'];

				$this->Common_mdl->insert('activity_log', $activity_datas);
				/** end of activity section **/
			}
			if ($before_data1['management'] != $after_data1['management']) {
				$re_management = 'yes';
				/** for activity section **/
				$activity_datas['log'] = "Management Accounts Updated";
				$activity_datas['createdTime'] = time();
				$activity_datas['module'] = 'Client';
				$activity_datas['sub_module'] = 'Management Accounts';
				$activity_datas['user_id'] = $userid;
				$activity_datas['module_id'] = $_POST['user_id'];

				$this->Common_mdl->insert('activity_log', $activity_datas);
				/** end of activity section **/
			}
			if ($before_data1['investgate'] != $after_data1['investgate']) {
				$re_investgate = 'yes';
				/** for activity section **/
				$activity_datas['log'] = "Investigation Insurance Updated";
				$activity_datas['createdTime'] = time();
				$activity_datas['module'] = 'Client';
				$activity_datas['sub_module'] = 'Investigation Insurance';
				$activity_datas['user_id'] = $userid;
				$activity_datas['module_id'] = $_POST['user_id'];

				$this->Common_mdl->insert('activity_log', $activity_datas);
				/** end of activity section **/
			}
			if ($before_data1['registered'] != $after_data1['registered']) {
				$re_registered = 'yes';
				/** for activity section **/
				$activity_datas['log'] = "Registered Address Updated";
				$activity_datas['createdTime'] = time();
				$activity_datas['module'] = 'Client';
				$activity_datas['sub_module'] = 'Registered Address';
				$activity_datas['user_id'] = $userid;
				$activity_datas['module_id'] = $_POST['user_id'];

				$this->Common_mdl->insert('activity_log', $activity_datas);
				/** end of activity section **/
			}
			if ($before_data1['taxadvice'] != $after_data1['taxadvice']) {
				$re_taxadvice = 'yes';
				/** for activity section **/
				$activity_datas['log'] = "Tax Advice Updated";
				$activity_datas['createdTime'] = time();
				$activity_datas['module'] = 'Client';
				$activity_datas['sub_module'] = 'Tax Advice';
				$activity_datas['user_id'] = $userid;
				$activity_datas['module_id'] = $_POST['user_id'];

				$this->Common_mdl->insert('activity_log', $activity_datas);
				/** end of activity section **/
			}
			if ($before_data1['taxinvest'] != $after_data1['taxinvest']) {
				$re_taxinvest = 'yes';
				/** for activity section **/
				$activity_datas['log'] = "Tax Investigation Updated";
				$activity_datas['createdTime'] = time();
				$activity_datas['module'] = 'Client';
				$activity_datas['sub_module'] = 'Tax Investigation';
				$activity_datas['user_id'] = $userid;
				$activity_datas['module_id'] = $_POST['user_id'];

				$this->Common_mdl->insert('activity_log', $activity_datas);
				/** end of activity section **/
			}
			/*temproray*/
			/*$this->email_remindersms_rechange($_POST['user_id'],$re_conf_statement,$re_accounts,$re_company_tax_return,$re_personal_tax_return,$re_payroll,$re_workplace,$re_vat,$re_cis,$re_cissub,$re_p11d,$re_bookkeep,$re_management,$re_investgate,$re_registered,$re_taxadvice,$re_taxinvest);*/
			return $_POST['user_id'];
		} else {
			return false;
		}
		//$this->load->view('clients/addnewclient_view',$data);     
	}



	public function client_list()
	{

		$this->Security_model->chk_login();
		$data['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user', 'role', '1');
		$data['client_list'] = $this->Common_mdl->getallrecords('client');
		$data['column_setting'] = $this->Common_mdl->getallrecords('column_setting');
		$this->load->view('clients/client_list', $data);
	}

	public function client_infomation($user_id = null)
	{		
		if (! $user_id) {
			redirect('user');
		}

		$this->Security_model->chk_login();

		$check_exist = $this->db->get_where('client', 'user_id=' . $user_id . ' and firm_id=' . $_SESSION['firm_id'])->num_rows();

		if ($check_exist) {
			$data = $this->Client_model->get_client_details($user_id);
			//echo "<pre>"; print_r($data); exit; 
			$this->load->view('clients/client_infomation', $data);
		} else {
			redirect('user');
		}
	}

	public function get_client_time_line($user_id)
	{
		$data = $this->Client_model->time_line($user_id);
		echo json_encode($data);
	}



	// add manual client views
	public function addmanalclient($user_id = false)
	{
		$this->Security_model->chk_login();
		$data['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user', 'role', '1');
		$data['client'] = $this->Common_mdl->GetAllWithWhere('client', 'user_id', $user_id);
		$data['user'] = $this->Common_mdl->GetAllWithWhere('user', 'id', $user_id);
		/** 21-08-2018 **/
		$query1 = $this->db->query("SELECT * FROM `user` where role=4 AND autosave_status!='1' and crm_name!='' and firm_admin_id=" . $_SESSION['id'] . " and status=1 order by id DESC");
		$results1 = $query1->result_array();
		$res = array();
		if (count($results1) > 0) {
			foreach ($results1 as $key => $value) {
				array_push($res, $value['id']);
			}
		}
		if (!empty($res)) {
			$im_val = implode(',', $res);
			//echo $im_val."abc abc";
			$data['referby'] = $this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id in (" . $im_val . ") order by id desc ")->result_array();
		} else {
			$data['referby'] = array();
		}
		/** end of 21-08-2018 **/
		$data['currency_set'] = $this->Common_mdl->GetAllWithWhere('admin_setting', 'user_id', $_SESSION['id']);
		$this->load->view('clients/add_manual_client_view', $data);
	}
	public function SearchCompany()
	{
		$this->Security_model->chk_login();

		$searchKey   = $this->input->post('term');

		$searchKey   = $this->input->post('term');
		$alredyExist = $this->db->query("select crm_company_number,user_id from client where firm_id =" . $_SESSION['firm_id'])->result_array();
		$alredyExist = array_column($alredyExist, 'crm_company_number', 'user_id');

		$content = $this->CompanyHouse_Model->SearchResults($searchKey, $alredyExist);
		echo $content;
	}

	public function object_2_array($result)
	{
		$array = array();
		if (!empty($result)) {
			foreach ($result as $key => $value) {

				# if $value is an array then
				if (is_array($value)) {
					#you are feeding an array to object_2_array function it could potentially be a perpetual loop.
					$array[$key] = $this->object_2_array($value);
				}

				# if $value is not an array then (it also includes objects)
				else {
					# if $value is an object then
					if (is_object($value)) {
						$array[$key] = $this->object_2_array($value);
					} else {

						$array[$key] = $value;
					}
				}
			}
		}
		return $array;
	}

	public function CompanyDetails()
	{
		$companyNo = $_POST['companyNo'];
		$content = $this->CompanyHouse_Model->Get_Profile_Content($companyNo);
		echo $content;
	}

	public function selectcompany($user_id = 0)
	{
		$this->session->set_userdata('c_status', '1');
		$companyNo = $_POST['companyNo'];
		$Company_Details = $this->CompanyHouse_Model->Get_Company_Details($companyNo);
		$content = '';
		if (!isset($Company_Details['error'])) {
			$company_details = $Company_Details['response'];
			$address1 = (isset($company_details['registered_office_address']['address_line_1']) && ($company_details['registered_office_address']['address_line_1'] != '')) ? $company_details['registered_office_address']['address_line_1'] : '';

			$address2 = (isset($company_details['registered_office_address']['address_line_2']) && ($company_details['registered_office_address']['address_line_2'] != '')) ? $company_details['registered_office_address']['address_line_2'] : '';
			$address3 = (isset($company_details['registered_office_address']['address_line_3']) && ($company_details['registered_office_address']['address_line_3'] != '')) ? $company_details['registered_office_address']['address_line_3'] : '';
			$registered_in = (isset($company_details['jurisdiction']) && ($company_details['jurisdiction'] != '')) ? $company_details['jurisdiction'] : '';
			$crm_town_city = (isset($company_details['registered_office_address']['locality']) && ($company_details['registered_office_address']['locality'] != '')) ? $company_details['registered_office_address']['locality'] : '';
			$crm_post_code = (isset($company_details['registered_office_address']['postal_code']) && ($company_details['registered_office_address']['postal_code'] != '')) ? $company_details['registered_office_address']['postal_code'] : '';

			$period_end_on = (isset($company_details['accounts']['next_accounts']['period_end_on']) && ($company_details['accounts']['next_accounts']['period_end_on'] != '')) ? $company_details['accounts']['next_accounts']['period_end_on'] : '';

			$next_made_up_to = (isset($company_details['accounts']['next_made_up_to']) && ($company_details['accounts']['next_made_up_to'] != '')) ? $company_details['accounts']['next_made_up_to'] : '';

			$next_due = (isset($company_details['accounts']['next_due']) && ($company_details['accounts']['next_due'] != '')) ? $company_details['accounts']['next_due'] : '';
			$is_ch_accounts_overdue = (!empty($company_details['accounts']['overdue']) ? $company_details['accounts']['overdue'] : '');

			$confirmation_next_made_up_to = (isset($company_details['confirmation_statement']['next_made_up_to']) && ($company_details['confirmation_statement']['next_made_up_to'] != '')) ? $company_details['confirmation_statement']['next_made_up_to'] : '';
			$confirmation_next_due = (isset($company_details['confirmation_statement']['next_due']) && ($company_details['confirmation_statement']['next_due'] != '')) ? $company_details['confirmation_statement']['next_due'] : '';
			$is_ch_cs_overdue = (!empty($company_details['confirmation_statement']['overdue']) ? $company_details['confirmation_statement']['overdue'] : '');

			$company_url = '';
			$officers_url = '';
			if (!empty($company_details['links'])) {
				$company_url = 'https://beta.companieshouse.gov.uk' . $company_details['links']['self'];
				$officers_url = (isset($company_details['links']['officers'])) ? 'https://beta.companieshouse.gov.uk' . $company_details['links']['officers'] : '';
			}

			// $company_url= 'https://beta.companieshouse.gov.uk'.$company_details['links']['self'];
			// $officers_url='https://beta.companieshouse.gov.uk'.$company_details['links']['officers'];
			$sic_code = '';
			$sic_codes = '';
			$nature_business = '';
			if (isset($company_details['sic_codes']) && $company_details['sic_codes'] != '') {

				/* foreach($company_details['sic_codes'] as $key => $val){
        $code=$this->db->query('SELECT * FROM sic_code WHERE COL1="'.$val.'"')->row_array();
        $sic_code[]='<option value='.$code['COL1'].'>'.$code['COL1'].'-'.$code['COL2'].'</option>';

        }*/
				$code = $this->db->query('SELECT * FROM sic_code WHERE COL1="' . $company_details['sic_codes'][0] . '"')->row_array();
				$sic_code = $nature_business = $code['COL1'] . '-' . $code['COL2'];
				$sic_codes = $company_details['sic_codes'];
			}

			$Company_types = $this->Common_mdl->Company_types();

			$cmpny_type = '';

			if (!empty($company_details['type'])) {
				$type = strtolower($company_details['type']);

				if (!empty($Company_types[$type])) {
					$cmpny_type = $Company_types[$type];
				} else {
					$cmpny_type = $type;
				}
			}
			/*should not insert data exist company*/
			if ($user_id == 0) {
				$user_value =
					array(
						'role' => '',
						'status' => 1,
						'crm_name' => $company_details['company_name'],
						//'autosave_status'=>1,
						'autosave_status' => 0, // rs 30-06-2018
						//'company_roles'=>1,
						'CreatedTime' => time(),
						'user_type' => 'FC',
						'firm_id' => $_SESSION['firm_id']
					);

				$this->db->insert('user', $user_value);
				$user_id = $this->db->insert_id();
				//$this->Common_mdl->createparentDirectory( $user_id );

				$Refrral_code = $this->Common_mdl->Get_ClientReferral_Code($company_details['company_name']);
				$client_value = array(
					'crm_company_name'  => (!empty($company_details['company_name'])) ? $company_details['company_name'] : '',
					'crm_company_status' =>  1,
					'crm_allocation_holder' => ' ',
					'crm_company_number' => trim($companyNo),
					'crm_register_address' => (!empty($company_details['registered_office_address'])) ? $company_details['registered_office_address']['address_line_1'] . $address2 . $company_details['registered_office_address']['locality'] . $company_details['registered_office_address']['postal_code'] : '',
					'crm_company_url' => $company_url,
					'crm_officers_url' => $officers_url,
					'crm_ch_status'   => (!empty($company_details['company_status'])) ? $company_details['company_status'] : '',
					// 'crm_company_type'=>$company_details['type'],
					'crm_company_type' => $cmpny_type,
					'crm_legal_form'  => $cmpny_type,
					'crm_incorporation_date' => (!empty($company_details['date_of_creation'])) ? $company_details['date_of_creation'] : '',
					'crm_ch_yearend'   => $period_end_on,
					'crm_hmrc_yearend'  => $next_made_up_to,
					'crm_accounts_last_made_up_to_date' => (!empty($company_details['accounts']['last_accounts']['made_up_to'])) ? $company_details['accounts']['last_accounts']['made_up_to'] : '',
					'crm_ch_accounts_next_due'  => $next_due,
					'is_ch_accounts_overdue'    => $is_ch_accounts_overdue,
					'crm_confirmation_statement_date' => $confirmation_next_made_up_to,
					'crm_confirmation_statement_last_made_up_to_date' => (!empty($company_details['confirmation_statement']['last_made_up_to'])) ? $company_details['confirmation_statement']['last_made_up_to'] : '',
					'crm_confirmation_statement_due_date' => (strtotime($confirmation_next_due) != '' ?  date('Y-m-d', strtotime($confirmation_next_due)) : ''), //$confirmation_next_due,
					'is_ch_cs_overdue' => $is_ch_cs_overdue,
					'created_date' => time(),
					//'user_id'=>(isset($_POST['user_id'])&&($_POST['user_id']==''))? $idd : $_POST['user_id'],
					'user_id' => $user_id,
					//'autosave_status'=>1,
					'autosave_status' => 0, //rs 30-06-2018
					"crm_company_sic" => $sic_code,
					'crm_accounts_due_date_hmrc' => date("Y-m-d", strtotime("+1 years", strtotime($next_made_up_to))),
					'crm_accounts_tax_date_hmrc' => date("Y-m-d", strtotime("+1 days", strtotime($next_due))),
					"crm_allocation_holder" => '',
					'crm_registered_in'     => $registered_in,
					'crm_address_line_one'  => $address1,
					'crm_address_line_two'  => $address2,
					'crm_address_line_three' => $address3,
					'crm_town_city'         => $crm_town_city,
					'crm_post_code'         => $crm_post_code,
					'firm_id'               => $_SESSION['firm_id'],
					'crm_other_send_invit_link' => $Refrral_code
				);
				$this->db->insert('client', $client_value);
				$client_id = $this->db->insert_id();
				$this->Common_mdl->Increase_Decrease_ClientCounts($_SESSION['firm_id'], '-1');

				$Find_firm_admin_NodeID = $this->db->query("select ot.id from user as u inner join organisation_tree as ot on ot.firm_id = u.firm_id and ot.user_id = u.id where u.user_type='FA' and u.firm_id =" . $_SESSION['firm_id'])->row_array();
				if (!empty($Find_firm_admin_NodeID['id'])) {
					$data = [
						'firm_id'    =>  $_SESSION['firm_id'],
						'module_name' =>  "CLIENT",
						'module_id'  =>  $client_id,
						'assignees'  =>  $Find_firm_admin_NodeID['id']
					];
					$this->db->insert("firm_assignees", $data);
				}

				$log = "<p>Client Added From Company house By <i>" . $_SESSION['crm_name'] . "</i></p>";
				$this->Common_mdl->Create_Log('client', $user_id, $log);
				$this->Check_CreateMissingDetailsTask($client_id);
			}
			$content .= '<h3>' . $company_details['company_name'] . '</h3>';
			$content .=  $this->CompanyHouse_Model->Get_Company_Contact_Content($companyNo, $user_id);
		}

		$content .= '<span class="view_contacts" onclick="return addmorecontact(' . $user_id . ');">Next</span>';

		echo json_encode(array("html" => $content));
	}

	public function maincontact()
	{
		$data_c = $this->CompanyHouse_Model->Add_Contact('client_contacts');

		if (isset($_POST['cnt']) && $_POST['cnt'] != '') {
			$data['cnt'] = $_POST['cnt'];
			$data['contact_from'] = 'company_house';
			$data = array_merge($data_c, $data);
			$settings = $this->Common_mdl->Get_Client_fields_ManagementSettings($_SESSION['firm_id']);
			$data = array_merge($data, $settings);
			$this->load->view('clients/contact_form', $data);
		} else {
			echo $data_c['contact_id'];
		}
	}

	// main contact info
	public function maincontact_info()
	{
		$companyNo = $_POST['companyNo'];
		$appointments = $_POST['appointments'];
		$cnt = $_POST['cnt'];
		$count_house = (isset($_POST['count'])) ? $_POST['count'] : '';
		//21-08-2018 
		$count_no = $_POST['count'];

		$birth = (isset($_POST['birth']) && ($_POST['birth'] != '')) ? $_POST['birth'] : '';
		// first api
		$curl = curl_init();
		$items_per_page = '10';
		//curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk/company/'.$companyNo.'/officers?items_per_page=10&register_type=directors&register_view=false');
		curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk' . $appointments);

		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_USERPWD, $this->companieshouseAPIkey);
		$response = curl_exec($curl);
		if ($errno = curl_errno($curl)) {
			$error_message = curl_strerror($errno);
			echo "cURL error ({$errno}):\n {$error_message}";
		} else {
			curl_close($curl);
			$data = json_decode($response);
			$array_rec = $this->object_2_array($data);

			// second api
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk/company/' . $companyNo . '/persons-with-significant-control/');

			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_USERPWD, $this->companieshouseAPIkey);
			$response1 = curl_exec($curl);

			if ($errno = curl_errno($curl)) {
				$error_message = curl_strerror($errno);
				echo "cURL error ({$errno}):\n {$error_message}";
			}
			curl_close($curl);
			$data1 = json_decode($response1);
			$array_rec1 = $this->object_2_array($data1);

			if (isset($array_rec1['items'])) {
				foreach ($array_rec1['items'] as $key => $value) {
					$nature_of_control = $value['natures_of_control'];
					$psc = 'yes';
				}

				$implode_nature = implode(',', $nature_of_control);
			} else {
				$implode_nature = '';
				$psc = '';
			}
			foreach ($array_rec['items'] as $key => $value) {
				if ($value['appointed_to']['company_number'] == $companyNo) {
					$content = $value;
				} else {
					$content = $value;
				}
			}

			//echo json_encode($datas);
			$title = (isset($content['name_elements']['title']) && ($content['name_elements']['title'] != '')) ? $content['name_elements']['title'] : '';
			$fore_name = (isset($content['name_elements']['forename']) && ($content['name_elements']['forename'] != '')) ? $content['name_elements']['forename'] : '';
			$otherfore_name = (isset($content['name_elements']['other_forenames']) && ($content['name_elements']['other_forenames'] != '')) ? $content['name_elements']['other_forenames'] : '';
			$sur_name = (isset($content['name_elements']['surname']) && ($content['name_elements']['surname'] != '')) ? $content['name_elements']['surname'] : '';
			$address_line_1 = (isset($content['address']['address_line_1']) && ($content['address']['address_line_1'] != '')) ? $content['address']['address_line_1'] : '';
			$address_line_2 = (isset($content['address']['address_line_2']) && ($content['address']['address_line_2'] != '')) ? $content['address']['address_line_2'] : '';
			$premises = (isset($content['address']['premises']) && ($content['address']['premises'] != '')) ? $content['address']['premises'] : '';
			$region = (isset($content['address']['region']) && ($content['address']['region'] != '')) ? $content['address']['region'] : '';
			$country = (isset($content['address']['country']) && ($content['address']['country'] != '')) ? $content['address']['country'] : '';
			$locality = (isset($content['address']['locality']) && ($content['address']['locality'] != '')) ? $content['address']['locality'] : '';
			$postal_code = (isset($content['address']['postal_code']) && ($content['address']['postal_code'] != '')) ? $content['address']['postal_code'] : '';
			$nationality = (isset($content['nationality']) && ($content['nationality'] != '')) ? $content['nationality'] : '';
			$occupation = (isset($content['occupation']) && ($content['occupation'] != '')) ? $content['occupation'] : '';
			$appointed_on = (isset($content['appointed_on']) && ($content['appointed_on'] != '')) ? $content['appointed_on'] : '';
			$country_of_residence = (isset($content['country_of_residence']) && ($content['country_of_residence'] != '')) ? $content['country_of_residence'] : '';


			//echo $content;
			// if($_SESSION['roleId']=='1'){

			// $role=2;

			// }else{
			$role = 4;
			//}


			$user_value = array(
				'role' => $role,
				'status' => 0,
				'autosave_status' => 0,
				'company_roles' => 1,
				'CreatedTime' => time(),
				'firm_admin_id' => $_SESSION['id'],
			);
			$updates_user = (isset($_POST['user_id']) && ($_POST['user_id'] == '')) ? $this->db->insert('user', $user_value) : $this->Common_mdl->update('user', $user_value, 'id', $_POST['user_id']);
			//$client_autoId = $this->db->last_id();
			$insert_data = $updates_user;
			$in = $this->db->insert_id();
			if ($insert_data) {

				$data_c = array();
				$data = array();
				$data_c['client_id'] = $_POST['user_id'];
				$data_c['title'] = $title;
				$data_c['first_name'] = $fore_name;
				$data_c['surname'] = $sur_name;
				$data_c['preferred_name'] = $otherfore_name;
				$data_c['address_line1'] = $address_line_1;
				$data_c['address_line2'] = $address_line_2;
				$data_c['premises'] = $premises;
				$data_c['region'] = $region;
				$data_c['country'] = $country;
				$data_c['locality'] = $locality;
				$data_c['post_code'] = $postal_code;
				$data_c['nationality'] = $nationality;
				$data_c['occupation'] = $occupation;
				$data_c['appointed_on'] = $appointed_on;
				$data_c['country_of_residence'] = $country_of_residence;
				$data_c['psc'] = $psc;
				$data_c['date_of_birth'] = $birth;
				$data_c['nature_of_control'] = $implode_nature;

				$data_c['created_date'] = time();
				/** 21-08-2018 **/
				$data_c['contact_count'] = $count_house;
				/** end of 21-08-2018 **/

				$result = $this->Common_mdl->insert('client_contacts', $data_c);
				//echo $in;die;
				$data = array();
				$data['client_id'] = $_POST['user_id'];
				$data['title'] = $title;
				$data['first_name'] = $fore_name;
				$data['surname'] = $sur_name;
				$data['preferred_name'] = $otherfore_name;
				$data['address_line1'] = $address_line_1;
				$data['address_line2'] = $address_line_2;
				$data['premises'] = $premises;
				$data['region'] = $region;
				$data['country'] = $country;
				$data['locality'] = $locality;
				$data['post_code'] = $postal_code;
				$data['nationality'] = $nationality;
				$data['occupation'] = $occupation;
				$data['appointed_on'] = $appointed_on;
				$data['country_of_residence'] = $country_of_residence;
				$data['birth'] = $birth;
				$data['make_primary'] = '0';
				$data['cnt'] = $cnt;
				$data['incre'] = $_POST['incre'];
				$data['created_date'] = time();
				//$data['contact_count']=$count_house;
				//$result = $this->Common_mdl->insert('client_contacts',$data);



				$client_value = array(
					'crm_first_name' => $fore_name,
					'crm_middle_name' => $otherfore_name,
					'crm_last_name' => $sur_name,
					'status' => 1,
					'created_date' => time(),
					'user_id' => (isset($_POST['user_id']) && ($_POST['user_id'] == '')) ? $in : $_POST['user_id'],
					//'autosave_status'=>1
					'autosave_status' => 0, //rs 30-06-2018
				);
				$updates_client = (isset($_POST['user_id']) && ($_POST['user_id'] == '')) ? $this->db->insert('client', $client_value) : $this->Common_mdl->update('client', $client_value, 'user_id', $_POST['user_id']);
			}
			$datas['rec'] = $this->Common_mdl->GetAllWithWhere('client_contacts', 'client_id', $_POST['user_id']);

			$data['formCount'] = $_POST['cnt'];

			//echo $this->db->last_query()
			$this->load->view('clients/contact_form_info', $data);
		}
	}















	//add manual client new client info based


	// add manual client
	public function insert_manualclient()
	{
		$this->Security_model->chk_login();
		$datas['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user', 'role', '1');
		if (isset($_POST['name']) && $_POST['name'] != '' && $_POST['company_name'] != '') {
			$var = rand(1, 10);
			//$user = $this->Common_mdl->GetAllWithWhere('user','id',$_POST['user_id']);
			$image_one = (isset($_POST['image_name']) && ($_POST['image_name'] != '')) ? $_POST['image_name'] : '825898.jpg';
			$image = (isset($_FILES['profile_image']['name']) && ($_FILES['profile_image']['name'] != '')) ? $this->Common_mdl->do_upload($_FILES['profile_image'], 'uploads') : $image_one;
			$data['crm_name'] = $_POST['name'];
			$data['username'] = $_POST['name'];
			$data['crm_email_id'] = $_POST['emailid'];
			$data['password'] = (isset($_POST['password']) && ($_POST['password'] != '')) ? md5($_POST['password']) : md5($var);
			$data['confirm_password'] = (isset($_POST['confirm_password']) && ($_POST['confirm_password'] != '')) ? $_POST['confirm_password'] : $var;
			$data['role'] = 4;
			$data['status'] = 1;
			$data['company_roles'] = (isset($_POST['company_house']) && ($_POST['company_house'] != '')) ? $_POST['company_house'] : 0;
			$data['CreatedTime'] = time();
			$data['client_id'] = (isset($_POST['client_id']) && ($_POST['client_id'] != '')) ? $_POST['client_id'] : rand(1, 5);
			$data['crm_packages'] = (isset($_POST['packages']) && ($_POST['packages'] != '')) ? $_POST['packages'] : '';
			$data['crm_gender'] = (isset($_POST['gender']) && ($_POST['gender'] != '')) ? $_POST['gender'] : '';
			$data['crm_profile_pic'] = $image;
			$data['autosave_status'] = 0;
			$data['firm_admin_id'] = $_SESSION['id'];
			$updates_user = (isset($_POST['user_id']) && ($_POST['user_id'] == '')) ? $this->db->insert('user', $data) : $this->Common_mdl->update('user', $data, 'id', $_POST['user_id']);
			$insert_data = $updates_user;
			if ($insert_data) {
				$in = $this->db->insert_id();
				$data1['user_id'] = (isset($_POST['user_id']) && ($_POST['user_id'] == '')) ? $in : $_POST['user_id'];
				// required information
				$data1['crm_company_name'] = (isset($_POST['company_name1']) && ($_POST['company_name1'] != '')) ? $_POST['company_name1'] : '0';
				$data1['crm_legal_form'] = (isset($_POST['legal_form']) && ($_POST['legal_form'] != '')) ? $_POST['legal_form'] : '0';
				$data1['crm_allocation_holder'] = (isset($_POST['allocation_holder']) && ($_POST['allocation_holder'] != '')) ? $_POST['allocation_holder'] : '0';
				// main contact
				$data1['crm_person'] = (isset($_POST['person']) && ($_POST['person'] != '')) ? $_POST['person'] : '0';
				$data1['crm_title'] = (isset($_POST['title']) && ($_POST['title'] != '')) ? $_POST['title'] : '0';
				$data1['crm_first_name'] = (isset($_POST['first_name']) && ($_POST['first_name'] != '')) ? $_POST['first_name'] : '0';
				$data1['crm_middle_name'] = (isset($_POST['middle_name']) && ($_POST['middle_name'] != '')) ? $_POST['middle_name'] : '0';
				$data1['crm_last_name'] = (isset($_POST['last_name']) && ($_POST['last_name'] != '')) ? $_POST['last_name'] : '0';
				$data1['crm_create_self_assessment_client'] = (isset($_POST['create_self_assessment_client']) && ($_POST['last_name'] != '')) ? $_POST['create_self_assessment_client'] : '0';
				$data1['crm_client_does_self_assessment'] = (isset($_POST['client_does_self_assessment']) && ($_POST['client_does_self_assessment'] != '')) ? $_POST['client_does_self_assessment'] : '0';
				$data1['crm_preferred_name'] = (isset($_POST['name']) && ($_POST['name'] != '')) ? $_POST['name'] : '0';
				$data1['crm_date_of_birth'] = (isset($_POST['dob']) && ($_POST['dob'] != '')) ? $_POST['dob'] : '0';
				$data1['crm_email'] = (isset($_POST['emailid']) && ($_POST['emailid'] != '')) ? $_POST['emailid'] : '0';
				$data1['crm_postal_address'] = (isset($_POST['postal_address']) && ($_POST['postal_address'] != '')) ? $_POST['postal_address'] : '0';
				$data1['crm_telephone_number'] = (isset($_POST['telephone_number']) && ($_POST['telephone_number'] != '')) ? $_POST['telephone_number'] : '0';
				$data1['crm_mobile_number'] = (isset($_POST['mobile_number']) && ($_POST['mobile_number'] != '')) ? $_POST['mobile_number'] : '0';
				$data1['crm_ni_number'] = (isset($_POST['ni_number']) && ($_POST['ni_number'] != '')) ? $_POST['ni_number'] : '0';
				$data1['crm_personal_utr_number'] = (isset($_POST['personal_utr_number']) && ($_POST['personal_utr_number'] != '')) ? $_POST['personal_utr_number'] : '0';
				$data1['crm_terms_signed'] = (isset($_POST['terms_signed']) && ($_POST['terms_signed'] != '')) ? $_POST['terms_signed'] : '0';
				$data1['crm_photo_id_verified'] = (isset($_POST['photo_id_verified']) && ($_POST['photo_id_verified'] != '')) ? $_POST['photo_id_verified'] : '0';
				$data1['crm_address_verified'] = (isset($_POST['address_verified']) && ($_POST['address_verified'] != '')) ? $_POST['address_verified'] : '0';
				// income details
				$data1['crm_previous'] = (isset($_POST['previous']) && ($_POST['previous'] != '')) ? $_POST['previous'] : '0';
				$data1['crm_current'] = (isset($_POST['current']) && ($_POST['current'] != '')) ? $_POST['current'] : '0';
				$data1['crm_ir_35_notes'] = (isset($_POST['ir_35_notes']) && ($_POST['ir_35_notes'] != '')) ? $_POST['ir_35_notes'] : '0';
				// other details
				$data1['crm_previous_accountant'] = (isset($_POST['previous_accountant']) && ($_POST['previous_accountant'] != '')) ? $_POST['previous_accountant'] : '0';
				echo $_POST['refered_by'];
				$data1['crm_refered_by'] = (isset($_POST['refered_by']) && ($_POST['refered_by'] != '')) ? $_POST['refered_by'] : '0';
				$data1['crm_allocated_office'] = (isset($_POST['allocated_office']) && ($_POST['allocated_office'] != '')) ? $_POST['allocated_office'] : '0';
				$data1['crm_inital_contact'] = (isset($_POST['inital_contact']) && ($_POST['inital_contact'] != '')) ? $_POST['inital_contact'] : '0';
				$data1['crm_quote_email_sent'] = (isset($_POST['quote_email_sent']) && ($_POST['quote_email_sent'] != '')) ? $_POST['quote_email_sent'] : '0';
				$data1['crm_welcome_email_sent'] = (isset($_POST['welcome_email_sent']) && ($_POST['welcome_email_sent'] != '')) ? $_POST['welcome_email_sent'] : '0';
				$data1['crm_internal_reference'] = (isset($_POST['internal_reference']) && ($_POST['internal_reference'] != '')) ? $_POST['internal_reference'] : '0';
				$data1['crm_profession'] = (isset($_POST['profession']) && ($_POST['profession'] != '')) ? $_POST['profession'] : '0';
				$data1['crm_website'] = (isset($_POST['website']) && ($_POST['website'] != '')) ? $_POST['website'] : '0';
				$data1['crm_accounting_system'] = (isset($_POST['accounting_system']) && ($_POST['accounting_system'] != '')) ? $_POST['accounting_system'] : '0';
				$data1['crm_notes'] = (isset($_POST['notes']) && ($_POST['notes'] != '')) ? $_POST['notes'] : '0';
				//serices required
				$data1['crm_accounts'] = (isset($_POST['accounts']) && ($_POST['accounts'] != '')) ? $_POST['accounts'] : '0';
				$data1['crm_bookkeeping'] = (isset($_POST['bookkeeping']) && ($_POST['bookkeeping'] != '')) ? $_POST['bookkeeping'] : '0';
				$data1['crm_ct600_return'] = (isset($_POST['ct600_return']) && ($_POST['ct600_return'] != '')) ? $_POST['ct600_return'] : '0';
				$data1['crm_payroll'] = (isset($_POST['payroll']) && ($_POST['payroll'] != '')) ? $_POST['payroll'] : '0';
				$data1['crm_auto_enrolment'] = (isset($_POST['auto_enrolment']) && ($_POST['auto_enrolment'] != '')) ? $_POST['auto_enrolment'] : '0';
				$data1['crm_vat_returns'] = (isset($_POST['vat_returns']) && ($_POST['vat_returns'] != '')) ? $_POST['vat_returns'] : '0';
				$data1['crm_confirmation_statement'] = (isset($_POST['confirmation_statement']) && ($_POST['confirmation_statement'] != '')) ? $_POST['confirmation_statement'] : '0';
				$data1['crm_cis'] = (isset($_POST['cis']) && ($_POST['cis'] != '')) ? $_POST['cis'] : '0';
				$data1['crm_p11d'] = (isset($_POST['p11d']) && ($_POST['p11d'] != '')) ? $_POST['p11d'] : '0';
				$data1['crm_invesitgation_insurance'] = (isset($_POST['invesitgation_insurance']) && ($_POST['invesitgation_insurance'] != '')) ? $_POST['invesitgation_insurance'] : '0';
				$data1['crm_services_registered_address'] = (isset($_POST['services_registered_address']) && ($_POST['services_registered_address'] != '')) ? $_POST['services_registered_address'] : '0';
				$data1['crm_bill_payment'] = (isset($_POST['bill_payment']) && ($_POST['bill_payment'] != '')) ? $_POST['bill_payment'] : '0';
				$data1['crm_advice'] = (isset($_POST['advice']) && ($_POST['advice'] != '')) ? $_POST['advice'] : '0';
				$data1['crm_monthly_charge'] = (isset($_POST['monthly_charge']) && ($_POST['monthly_charge'] != '')) ? $_POST['monthly_charge'] : '0';
				$data1['crm_annual_charge'] = (isset($_POST['annual_charge']) && ($_POST['annual_charge'] != '')) ? $_POST['annual_charge'] : '0';
				// accounts and return details
				$data1['crm_accounts_periodend'] = (isset($_POST['accounts_periodend']) && ($_POST['accounts_periodend'] != '')) ? $_POST['accounts_periodend'] : '0';
				$data1['crm_ch_yearend'] = (isset($_POST['period_end_on']) && ($_POST['period_end_on'] != '')) ? $_POST['period_end_on'] : '0';
				$data1['crm_hmrc_yearend'] = (isset($_POST['next_made_up_to']) && ($_POST['next_made_up_to'] != '')) ? $_POST['next_made_up_to'] : '0';
				$data1['crm_ch_accounts_next_due'] = (isset($_POST['next_due']) && ($_POST['next_due'] != '')) ? date('Y-m-d', strtotime($_POST['next_due'])) : '0';
				$data1['crm_ct600_due'] = (isset($_POST['ct600_due']) && ($_POST['ct600_due'] != '')) ? $_POST['ct600_due'] : '0';
				$data1['crm_companies_house_email_remainder'] = (isset($_POST['companies_house_email_remainder']) && ($_POST['companies_house_email_remainder'] != '')) ? $_POST['companies_house_email_remainder'] : '0';
				$data1['crm_latest_action'] = (isset($_POST['latest_action']) && ($_POST['latest_action'] != '')) ? $_POST['latest_action'] : '0';
				$data1['crm_latest_action_date'] = (isset($_POST['latest_action_date']) && ($_POST['latest_action_date'] != '')) ? $_POST['latest_action_date'] : '0';
				$data1['crm_records_received_date'] = (isset($_POST['records_received_date']) && ($_POST['records_received_date'] != '')) ? $_POST['records_received_date'] : '0';
				//confirmation statements
				$data1['crm_confirmation_statement_date'] = (isset($_POST['confirm_next_made_up_to']) && ($_POST['confirm_next_made_up_to'] != '')) ? $_POST['confirm_next_made_up_to'] : '0';
				$data1['crm_confirmation_statement_due_date'] = (isset($_POST['confirm_next_due']) && ($_POST['confirm_next_due'] != '')) ? date('Y-m-d', strtotime($_POST['confirm_next_due'])) : '0';
				$data1['crm_confirmation_latest_action'] = (isset($_POST['confirmation_latest_action']) && ($_POST['confirmation_latest_action'] != '')) ? $_POST['confirmation_latest_action'] : '0';
				$data1['crm_confirmation_latest_action_date'] = (isset($_POST['confirmation_latest_action_date']) && ($_POST['confirmation_latest_action_date'] != '')) ? $_POST['confirmation_latest_action_date'] : '0';
				$data1['crm_confirmation_records_received_date'] = (isset($_POST['confirmation_records_received_date']) && ($_POST['confirmation_records_received_date'] != '')) ? $_POST['confirmation_records_received_date'] : '0';
				$data1['crm_confirmation_officers'] = (isset($_POST['confirmation_officers']) && ($_POST['confirmation_officers'] != '')) ? $_POST['confirmation_officers'] : '0';
				$data1['crm_share_capital'] = (isset($_POST['share_capital']) && ($_POST['share_capital'] != '')) ? $_POST['share_capital'] : '';
				$data1['crm_shareholders'] = (isset($_POST['shareholders']) && ($_POST['shareholders'] != '')) ? $_POST['shareholders'] : '';
				$data1['crm_people_with_significant_control'] = (isset($_POST['people_with_significant_control']) && ($_POST['people_with_significant_control'] != '')) ? $_POST['people_with_significant_control'] : '';
				// vat details
				$data1['crm_vat_quarters'] = (isset($_POST['vat_quarters']) && ($_POST['vat_quarters'] != '')) ? $_POST['vat_quarters'] : '0';
				$data1['crm_vat_quater_end_date'] = (isset($_POST['vat_quater_end_date']) && ($_POST['vat_quater_end_date'] != '')) ? date('Y-m-d', strtotime($_POST['vat_quater_end_date'])) : '0';
				$data1['crm_next_return_due_date'] = (isset($_POST['next_return_due_date']) && ($_POST['next_return_due_date'] != '')) ? $_POST['next_return_due_date'] : '0';
				$data1['crm_vat_latest_action'] = (isset($_POST['vat_latest_action']) && ($_POST['vat_latest_action'] != '')) ? $_POST['vat_latest_action'] : '0';
				$data1['crm_vat_latest_action_date'] = (isset($_POST['vat_latest_action_date']) && ($_POST['vat_latest_action_date'] != '')) ? $_POST['vat_latest_action_date'] : '0';
				$data1['crm_vat_records_received_date'] = (isset($_POST['vat_records_received_date']) && ($_POST['vat_records_received_date'] != '')) ? $_POST['vat_records_received_date'] : '0';
				$data1['crm_vat_member_state'] = (isset($_POST['vat_member_state']) && ($_POST['vat_member_state'] != '')) ? $_POST['vat_member_state'] : '0';
				$data1['crm_vat_number'] = (isset($_POST['vat_number']) && ($_POST['vat_number'] != '')) ? $_POST['vat_number'] : '0';
				$data1['crm_vat_date_of_registration'] = (isset($_POST['vat_date_of_registration']) && ($_POST['vat_date_of_registration'] != '')) ? $_POST['vat_date_of_registration'] : '0';
				$data1['crm_vat_effective_date'] = (isset($_POST['vat_effective_date']) && ($_POST['vat_effective_date'] != '')) ? $_POST['vat_effective_date'] : '0';
				$data1['crm_vat_estimated_turnover'] = (isset($_POST['vat_estimated_turnover']) && ($_POST['vat_estimated_turnover'] != '')) ? $_POST['vat_estimated_turnover'] : '0';
				$data1['crm_transfer_of_going_concern'] = (isset($_POST['transfer_of_going_concern']) && ($_POST['transfer_of_going_concern'] != '')) ? $_POST['transfer_of_going_concern'] : '0';
				$data1['crm_involved_in_any_other_business'] = (isset($_POST['involved_in_any_other_business']) && ($_POST['involved_in_any_other_business'] != '')) ? $_POST['involved_in_any_other_business'] : '0';
				$data1['crm_flat_rate'] = (isset($_POST['flat_rate']) && ($_POST['flat_rate'] != '')) ? $_POST['flat_rate'] : '0';
				$data1['crm_direct_debit'] = (isset($_POST['direct_debit']) && ($_POST['direct_debit'] != '')) ? $_POST['direct_debit'] : '0';
				$data1['crm_annual_accounting_scheme'] = (isset($_POST['annual_accounting_scheme']) && ($_POST['annual_accounting_scheme'] != '')) ? $_POST['annual_accounting_scheme'] : '0';
				$data1['crm_flat_rate_category'] = (isset($_POST['flat_rate_category']) && ($_POST['flat_rate_category'] != '')) ? $_POST['flat_rate_category'] : '0';
				$data1['crm_month_of_last_quarter_submitted'] = (isset($_POST['month_of_last_quarter_submitted']) && ($_POST['month_of_last_quarter_submitted'] != '')) ? $_POST['month_of_last_quarter_submitted'] : '0';
				$data1['crm_box5_of_last_quarter_submitted'] = (isset($_POST['box5_of_last_quarter_submitted']) && ($_POST['box5_of_last_quarter_submitted'] != '')) ? $_POST['box5_of_last_quarter_submitted'] : '0';
				$data1['crm_vat_address'] = (isset($_POST['vat_address']) && ($_POST['vat_address'] != '')) ? $_POST['vat_address'] : '';
				/*$data1['crm_vat_notes']=(isset($_POST['vat_notes'])&&($_POST['vat_notes']!=''))? $_POST['vat_notes']:'0';*/
				//paye details
				$data1['crm_employers_reference'] = (isset($_POST['employers_reference']) && ($_POST['employers_reference'] != '')) ? $_POST['employers_reference'] : '0';
				$data1['crm_accounts_office_reference'] = (isset($_POST['accounts_office_reference']) && ($_POST['accounts_office_reference'] != '')) ? $_POST['accounts_office_reference'] : '0';
				$data1['crm_years_required'] = (isset($_POST['years_required']) && ($_POST['years_required'] != '')) ? $_POST['years_required'] : '0';
				$data1['crm_annual_or_monthly_submissions'] = (isset($_POST['annual_or_monthly_submissions']) && ($_POST['annual_or_monthly_submissions'] != '')) ? $_POST['annual_or_monthly_submissions'] : '0';
				$data1['crm_irregular_montly_pay'] = (isset($_POST['irregular_montly_pay']) && ($_POST['irregular_montly_pay'] != '')) ? $_POST['irregular_montly_pay'] : '0';
				$data1['crm_nil_eps'] = (isset($_POST['nil_eps']) && ($_POST['nil_eps'] != '')) ? $_POST['nil_eps'] : '0';
				$data1['crm_no_of_employees'] = (isset($_POST['no_of_employees']) && ($_POST['no_of_employees'] != '')) ? $_POST['vat_number'] : '0';
				$data1['crm_first_pay_date'] = (isset($_POST['first_pay_date']) && ($_POST['first_pay_date'] != '')) ? date('Y-m-d', strtotime($_POST['first_pay_date'])) : '0';
				$data1['crm_rti_deadline'] = (isset($_POST['rti_deadline']) && ($_POST['rti_deadline'] != '')) ? $_POST['rti_deadline'] : '0';
				$data1['crm_paye_scheme_ceased'] = (isset($_POST['paye_scheme_ceased']) && ($_POST['paye_scheme_ceased'] != '')) ? date('Y-m-d', strtotime($_POST['paye_scheme_ceased'])) : '0';
				$data1['crm_paye_latest_action'] = (isset($_POST['paye_latest_action']) && ($_POST['paye_latest_action'] != '')) ? $_POST['paye_latest_action'] : '0';
				$data1['crm_paye_latest_action_date'] = (isset($_POST['paye_latest_action_date']) && ($_POST['paye_latest_action_date'] != '')) ? $_POST['paye_latest_action_date'] : '0';
				$data1['crm_paye_records_received'] = (isset($_POST['paye_records_received']) && ($_POST['paye_records_received'] != '')) ? $_POST['paye_records_received'] : '0';
				$data1['crm_next_p11d_return_due'] = (isset($_POST['next_p11d_return_due']) && ($_POST['next_p11d_return_due'] != '')) ? $_POST['next_p11d_return_due'] : '0';
				$data1['crm_latest_p11d_submitted'] = (isset($_POST['latest_p11d_submitted']) && ($_POST['latest_p11d_submitted'] != '')) ? $_POST['latest_p11d_submitted'] : '0';
				$data1['crm_p11d_latest_action'] = (isset($_POST['p11d_latest_action']) && ($_POST['p11d_latest_action'] != '')) ? $_POST['p11d_latest_action'] : '0';
				$data1['crm_p11d_latest_action_date'] = (isset($_POST['p11d_latest_action_date']) && ($_POST['p11d_latest_action_date'] != '')) ? date('Y-m-d', strtotime($_POST['p11d_latest_action_date'])) : '0';
				$data1['crm_p11d_records_received'] = (isset($_POST['p11d_records_received']) && ($_POST['p11d_records_received'] != '')) ? $_POST['p11d_records_received'] : '0';
				$data1['crm_cis_contractor'] = (isset($_POST['cis_contractor']) && ($_POST['cis_contractor'] != '')) ? $_POST['cis_contractor'] : '0';
				$data1['crm_cis_subcontractor'] = (isset($_POST['cis_subcontractor']) && ($_POST['cis_subcontractor'] != '')) ? $_POST['cis_subcontractor'] : '0';
				$data1['crm_cis_deadline'] = (isset($_POST['cis_deadline']) && ($_POST['cis_deadline'] != '')) ? $_POST['cis_deadline'] : '0';
				$data1['crm_auto_enrolment_latest_action'] = (isset($_POST['auto_enrolment_latest_action']) && ($_POST['auto_enrolment_latest_action'] != '')) ? $_POST['auto_enrolment_latest_action'] : '0';
				$data1['crm_auto_enrolment_latest_action_date'] = (isset($_POST['auto_enrolment_latest_action_date']) && ($_POST['auto_enrolment_latest_action_date'] != '')) ? $_POST['auto_enrolment_latest_action_date'] : '0';
				$data1['crm_auto_enrolment_records_received'] = (isset($_POST['auto_enrolment_records_received']) && ($_POST['auto_enrolment_records_received'] != '')) ? $_POST['auto_enrolment_records_received'] : '0';
				$data1['crm_auto_enrolment_staging'] = (isset($_POST['auto_enrolment_staging']) && ($_POST['auto_enrolment_staging'] != '')) ? $_POST['auto_enrolment_staging'] : '0';
				$data1['crm_postponement_date'] = (isset($_POST['postponement_date']) && ($_POST['postponement_date'] != '')) ? $_POST['postponement_date'] : '0';
				$data1['crm_the_pensions_regulator_opt_out_date'] = (isset($_POST['the_pensions_regulator_opt_out_date']) && ($_POST['the_pensions_regulator_opt_out_date'] != '')) ? $_POST['the_pensions_regulator_opt_out_date'] : '0';
				$data1['crm_re_enrolment_date'] = (isset($_POST['re_enrolment_date']) && ($_POST['re_enrolment_date'] != '')) ? $_POST['re_enrolment_date'] : '0';
				$data1['crm_pension_provider'] = (isset($_POST['pension_provider']) && ($_POST['pension_provider'] != '')) ? $_POST['pension_provider'] : '0';
				$data1['crm_paye_re_enrolment_date'] = (isset($_POST['paye_re_enrolment_date']) && ($_POST['paye_re_enrolment_date'] != '')) ? $_POST['paye_re_enrolment_date'] : '0';
				$data1['crm_paye_pension_provider'] = (isset($_POST['paye_pension_provider']) && ($_POST['paye_pension_provider'] != '')) ? $_POST['paye_pension_provider'] : '0';
				$data1['crm_pension_id'] = (isset($_POST['pension_id']) && ($_POST['pension_id'] != '')) ? $_POST['pension_id'] : '0';
				$data1['crm_declaration_of_compliance_due_date'] = (isset($_POST['declaration_of_compliance_due_date']) && ($_POST['declaration_of_compliance_due_date'] != '')) ? $_POST['declaration_of_compliance_due_date'] : '0';
				$data1['crm_declaration_of_compliance_submission'] = (isset($_POST['declaration_of_compliance_submission']) && ($_POST['declaration_of_compliance_submission'] != '')) ? $_POST['declaration_of_compliance_submission'] : '0';
				$data1['crm_pension_deadline'] = (isset($_POST['pension_deadline']) && ($_POST['pension_deadline'] != '')) ? $_POST['pension_deadline'] : '0';
				$data1['crm_note'] = (isset($_POST['note']) && ($_POST['note'] != '')) ? $_POST['note'] : '0';
				//registration
				$data1['crm_registration_fee_paid'] = (isset($_POST['registration_fee_paid']) && ($_POST['registration_fee_paid'] != '')) ? $_POST['registration_fee_paid'] : '0';
				$data1['crm_64_8_registration'] = (isset($_POST['64_8_registration']) && ($_POST['64_8_registration'] != '')) ? $_POST['64_8_registration'] : '0';
				// company details
				$data1['crm_company_number'] = (isset($_POST['company_number']) && ($_POST['company_number'] != '')) ? trim($_POST['company_number']) : '0';
				$data1['crm_company_url'] = (isset($_POST['company_url']) && ($_POST['company_url'] != '')) ? $_POST['company_url'] : '0';
				$data1['crm_officers_url'] = (isset($_POST['officers_url']) && ($_POST['officers_url'] != '')) ? $_POST['officers_url'] : '0';
				$data1['crm_company_name1'] = (isset($_POST['company_name']) && ($_POST['company_name'] != '')) ? $_POST['company_name'] : '0';
				$data1['crm_register_address'] = (isset($_POST['register_address']) && ($_POST['register_address'] != '')) ? $_POST['register_address'] : '0';
				$data1['crm_company_status'] = (isset($_POST['company_status']) && ($_POST['company_status'] != '')) ? $_POST['company_status'] : '0';
				$data1['crm_company_type'] = (isset($_POST['company_type']) && ($_POST['company_type'] != '')) ? $_POST['company_type'] : '0';
				$data1['crm_company_sic'] = (isset($_POST['company_sic']) && ($_POST['company_sic'] != '')) ? $_POST['company_sic'] : '0';
				$data1['crm_sic_codes'] = (isset($_POST['sic_codes']) && ($_POST['sic_codes'] != '')) ? $_POST['sic_codes'] : '0';
				$data1['crm_companies_house_authorisation_code'] = (isset($_POST['companies_house_authorisation_code']) && ($_POST['companies_house_authorisation_code'] != '')) ? $_POST['companies_house_authorisation_code'] : '0';
				$data1['crm_company_utr'] = (isset($_POST['company_utr']) && ($_POST['company_utr'] != '')) ? $_POST['company_utr'] : '0';
				$data1['crm_incorporation_date'] = (isset($_POST['date_of_creation']) && ($_POST['date_of_creation'] != '')) ? $_POST['date_of_creation'] : '0';
				$data1['crm_managed'] = (isset($_POST['managed']) && ($_POST['managed'] != '')) ? $_POST['managed'] : '0';
				//Business details
				$data1['crm_tradingas'] = (isset($_POST['tradingas']) && ($_POST['tradingas'] != '')) ? $_POST['tradingas'] : '0';
				$data1['crm_commenced_trading'] = (isset($_POST['commenced_trading']) && ($_POST['commenced_trading'] != '')) ? $_POST['commenced_trading'] : '0';
				$data1['crm_registered_for_sa'] = (isset($_POST['registered_for_sa']) && ($_POST['registered_for_sa'] != '')) ? $_POST['registered_for_sa'] : '0';
				$data1['crm_business_details_turnover'] = (isset($_POST['business_details_turnover']) && ($_POST['business_details_turnover'] != '')) ? $_POST['business_details_turnover'] : '0';
				$data1['crm_business_details_nature_of_business'] = (isset($_POST['business_details_nature_of_business']) && ($_POST['business_details_nature_of_business'] != '')) ? $_POST['business_details_nature_of_business'] : '0';
				$data1['status'] = (isset($_POST['company_house']) && ($_POST['company_house'] != '')) ? $_POST['company_house'] : 0;
				$data1['crm_packages'] = (isset($_POST['packages']) && ($_POST['packages'] != '')) ? $_POST['packages'] : '';
				$data1['crm_gender'] = (isset($_POST['gender']) && ($_POST['gender'] != '')) ? $_POST['gender'] : '';
				// client information
				$data1['crm_letter_sign'] = (isset($_POST['letter_sign']) && ($_POST['letter_sign'] != '')) ? $_POST['letter_sign'] : '';
				$data1['crm_business_website'] = (isset($_POST['business_website']) && ($_POST['business_website'] != '')) ? $_POST['business_website'] : '';
				$data1['crm_paye_ref_number'] = (isset($_POST['paye_ref_number']) && ($_POST['paye_ref_number'] != '')) ? $_POST['paye_ref_number'] : '';
				$data1['created_date'] = time();
				$data1['autosave_status'] = 0;
				//$this->db->insert( 'client', $data1 );
				$updates_client = (isset($_POST['user_id']) && ($_POST['user_id'] == '')) ? $this->db->insert('client', $data1) : $this->Common_mdl->update('client', $data1, 'user_id', $_POST['user_id']);
				$email = $this->input->post('email_id');
				$query = $this->db->query('select * from user where crm_email_id="' . $_POST['email_id'] . '"')->row_array();
				if ($query['username'] != '') {
					$data2['username'] = $query['username'];
					$user_id = $query['id'];
					$password = $query['password'];
					$encrypt_id = base64_encode($user_id);
					$email_subject  = 'Welcome Email';
					$data2['email_content']  = '<a href=' . base_url() . 'login/resetPwd/' . $encrypt_id . ' style="display: inline-block;text-align: center;color: #00aeef;text-decoration: none;font-family:;background: #00aeef;color: #fff;font-weight: 600;padding: 5px 10px;border-radius: 3px;">Click Here</a> for Reset Your CRM Account Password';
					$this->session->set_flashdata('email', $email);
					//email template
					$body = $this->load->view('forgot_email_format.php', $data2, TRUE);
					$this->mailsettings();
					$this->email->from('info@techleafsolutions.com');
					$this->email->to($email);
					$this->email->subject($email_subject);
					$this->email->message($body);
					$send = $this->email->send();
				}
				echo '1';
			} else {
				echo '0';
			}
		} else {
			echo '0';
		}
		//$this->load->view('clients/addnewclient_view',$data);
	}


	public function mailsettings()
	{
		$this->load->library('email');
		$config['wrapchars'] = 76; // Character count to wrap at.
		$config['mailtype'] = 'html'; // text or html Type of mail. If you send HTML email you must send it as a complete web page. Make sure you don't have any relative links or relative image paths otherwise they will not work.
		//$config['charset'] = 'utf-8'; // Character set (utf-8, iso-8859-1, etc.).
		$this->email->initialize($config);
	}

	public function client_options()
	{

		$this->Security_model->chk_login();
		$data['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user', 'role', '1');
		$data['client_list'] = $this->Common_mdl->getallrecords('client');
		$data['column_setting'] = $this->Common_mdl->getallrecords('column_setting');
		$this->load->view('clients/clients_button_view', $data);
	}
	public function add_client($user_id = false)
	{
		$this->Security_model->chk_login();
		$data['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user', 'role', '1');
		$data['client'] = $this->Common_mdl->GetAllWithWhere('client', 'user_id', $user_id);
		$data['user'] = $this->Common_mdl->GetAllWithWhere('user', 'id', $user_id);
		$data['Managed_by_person'] = $this->Common_mdl->getallrecords('Managed_by_person');
		//$data['referby'] = $this->db->query("SELECT * FROM `client` where crm_first_name!='' and crm_first_name!='0' GROUP BY `crm_first_name`")->result_array();
		/** 21-08-2018 **/
		$query1 = $this->db->query("SELECT * FROM `user` where role=4 AND autosave_status!='1' and crm_name!='' and firm_admin_id=" . $_SESSION['id'] . " and status=1 order by id DESC");
		$results1 = $query1->result_array();
		$res = array();
		if (count($results1) > 0) {
			foreach ($results1 as $key => $value) {
				array_push($res, $value['id']);
			}
		}
		if (!empty($res)) {
			$im_val = implode(',', $res);
			//echo $im_val."abc abc";
			$data['referby'] = $this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id in (" . $im_val . ") order by id desc ")->result_array();
			//$results = $query->result_array(); 
		} else {
			$data['referby'] = array();
		}
		/** end of 21-08-2018 **/

		$data['country_result'] = $this->db->query('select * from countries')->result_array();
		$data['currency_set'] = $this->Common_mdl->GetAllWithWhere('admin_setting', 'user_id', $_SESSION['id']);

		$this->load->view('clients/add_client_view', $data);
	}
	public function state()
	{
		$id = $this->input->post('country_id');

		$query = $this->db->query('select * from states where country_id=' . $id . '')->result_array();

		$state_name = "";

		foreach ($query as $value) {

			$state_name .= "<option value=" . $value['id'] . ">" . $value['name'] . "</option>";
		}
		echo $state_name;
	}

	public function city()
	{
		$id = $this->input->post('state_id');

		$query = $this->db->query('select * from cities where state_id=' . $id . '')->result_array();

		$city_name = "";

		foreach ($query as $value) {

			$city_name .= "<option value=" . $value['id'] . ">" . $value['name'] . "</option>";
		}
		echo $city_name;
	}
	public function new_user()
	{
		$this->Security_model->chk_login();
		$datass['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user', 'role', '1');
		if (isset($_POST) && $_POST['first_name'] != '' && $_POST['email_id'] != '') {
			$data['name'] = $_POST['first_name'];
			$data['last_name'] = $_POST['last_name'];
			$data['username'] = $_POST['first_name'];
			$data['password'] = md5($_POST['password']);
			$data['confirm_password'] = $_POST['confirm_password'];
			$data['email_id'] = $_POST['email_id'];
			$data['phone_number'] = $_POST['phone_number'];
			$data['date_of_birth'] = $_POST['dob'];
			$data['gender'] = $_POST['gender'];
			$data['street1'] = $_POST['street1'];
			$data['street2'] = (isset($_POST['street2']) && ($_POST['street2'] != '')) ? $_POST['street2'] : '';
			$data['country'] = $_POST['country'];
			$data['state'] = $_POST['state'];
			$data['city'] = $_POST['city'];
			$data['postal_code'] = $_POST['postal_code'];
			$data['packages'] = $_POST['packages'];
			$data['firm_name'] = $_POST['firm_name'];
			$data['firm_status'] = $_POST['firm_status'];
			$data['role'] = 4;
			$data['status'] = 0;
			$data['CreatedTime'] = time();
			$data['client_id'] = (isset($_POST['client_id']) && ($_POST['client_id'] != '')) ? $_POST['client_id'] : rand(1, 5);
			$data['profile_pic'] = '825898.jpg';
			$data['firm_admin_id'] = $_SESSION['id'];
			/* print_r($data);
            exit;*/
			$updates_user = (isset($_POST['user_id']) && ($_POST['user_id'] == '')) ? $this->db->insert('user', $data) : $this->Common_mdl->update('user', $data, 'id', $_POST['user_id']);

			$insert_data = $updates_user;
			if ($insert_data) {
				$in = $this->db->insert_id();

				$datas['user_id'] = (isset($_POST['user_id']) && ($_POST['user_id'] == '')) ? $in : $_POST['user_id'];
				$datas['first_name'] = $_POST['first_name'];
				$datas['last_name'] = $_POST['last_name'];
				$datas['preferred_name'] = $_POST['first_name'];
				$datas['email'] = $_POST['email_id'];
				$datas['telephone_number'] = $_POST['phone_number'];
				$datas['date_of_birth'] = $_POST['dob'];
				$datas['registration_fee_paid'] = $_POST['packages'];
				$datas['company_name'] = $_POST['firm_name'];
				$datas['company_status'] = $_POST['firm_status'];
				$datas['status'] = 1;
				$datas['created_date'] = time();
				$updates_client = (isset($_POST['user_id']) && ($_POST['user_id'] == '')) ? $this->db->insert('client', $datas) : $this->Common_mdl->update('client', $datas, 'user_id', $_POST['user_id']);
				//$this->db->insert( 'client', $datas );

				echo 'success';
			} else {
				echo 'error';
			}
		} else {
			echo 'error';
		}
	}



	public function cron_job()
	{

		$records = $this->db->query("SELECT * FROM client")->result_array();

		foreach ($records as $key => $value) {

			$old_record[] = $value;
			// second api
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk/company/' . trim($value['crm_company_number']) . '/');

			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_USERPWD, $this->companieshouseAPIkey);
			$response1 = curl_exec($curl);

			if ($errno = curl_errno($curl)) {
				$error_message = curl_strerror($errno);
				echo "cURL error ({$errno}):\n {$error_message}";
			}
			curl_close($curl);
			$data1 = json_decode($response1);
			$array_rec1 = $this->object_2_array($data1);
			/** 29-06-2018 **/
			// echo "<pre>";       
			// print_r($array_rec1);
			if (isset($array_rec1['errors'])) {
				echo $array_rec1['errors'][0]['error'];
			}
			//echo "</pre>";
			/** end 29-06-2018 **/
			if (!isset($array_rec1['errors'])) {

				if ($value['crm_company_name'] != $array_rec1['company_name']) {
					$data['crm_company_name'] = $datas['crm_company_name'] = (isset($array_rec1['company_name']) && ($array_rec1['company_name'] != '')) ? $array_rec1['company_name'] : '0';
				} else {
					$datas['crm_company_name'] = '0';
				}


				if ($value['crm_company_status'] != $array_rec1['company_status']) {
					$data['crm_company_status'] = $datas['crm_company_status'] = (isset($array_rec1['company_status']) && ($array_rec1['company_status'] != '')) ? $array_rec1['company_status'] : '0';
				} else {
					$datas['crm_company_status'] = '0';
				}


				if ($value['crm_company_type'] != $array_rec1['type']) {
					$data['crm_company_type'] = $datas['crm_company_type'] = (isset($array_rec1['type']) && ($array_rec1['type'] != '')) ? $array_rec1['type'] : '0';
				} else {
					$datas['crm_company_type'] = '0';
				}

				/* $sic_api=implode(",",$array_rec1['sic_codes']);
  $sic=$value['sic_codes'];
  $com_sci=strcmp($sic,$sic_api);

        if($sic!=$sic_api){
        $data['sic_codes']=(isset($array_rec1['sic_codes']) &&($array_rec1['sic_codes']!=''))? $sic_api : ''; }*/

				if ($value['crm_company_url'] != 'https://beta.companieshouse.gov.uk' . $array_rec1['links']['self']) {
					$data['crm_company_url'] = $datas['crm_company_url'] = (isset($array_rec1['links']['self']) && ($array_rec1['links']['self'] != '')) ? 'https://beta.companieshouse.gov.uk' . $array_rec1['links']['self'] : '';
				} else {
					$datas['crm_company_url'] = '0';
				}

				if ($value['crm_officers_url'] != 'https://beta.companieshouse.gov.uk' . $array_rec1['links']['officers']) {
					$data['crm_officers_url'] = $datas['crm_officers_url'] = (isset($array_rec1['links']['officers']) && ($array_rec1['links']['officers'] != '')) ? 'https://beta.companieshouse.gov.uk' . $array_rec1['links']['officers'] : '';
				} else {
					$datas['crm_officers_url'] = '0';
				}

				$reg_add = preg_replace('/\s+/', '', $value['crm_register_address']);

				$address1 = (isset($array_rec1['registered_office_address']['address_line_1']) && ($array_rec1['registered_office_address']['address_line_1'] != '')) ? $array_rec1['registered_office_address']['address_line_1'] : '';

				$address2 = (isset($array_rec1['registered_office_address']['address_line_2']) && ($array_rec1['registered_office_address']['address_line_2'] != '')) ? $array_rec1['registered_office_address']['address_line_2'] : '';

				$locality = (isset($array_rec1['registered_office_address']['locality']) && ($array_rec1['registered_office_address']['locality'] != '')) ? $array_rec1['registered_office_address']['locality'] : '';

				$postal = (isset($array_rec1['registered_office_address']['postal_code']) && ($array_rec1['registered_office_address']['postal_code'] != '')) ? $array_rec1['registered_office_address']['postal_code'] : '';

				$add = preg_replace('/\s+/', '', $address1 . $address2 . $locality . $postal);


				if (strcmp($reg_add, $add) != 0) {
					$data['crm_register_address'] = $datas['crm_register_address'] = $address1 . "\n" . $address2 . "\n" . $locality . "\n" . $postal;
				} else {
					$datas['crm_register_address'] = '0';
				}

				/* if($value['address2']!=$array_rec1['registered_office_address']['address_line_2']){
        $data['address2']=(isset($array_rec1['registered_office_address']['address_line_2'])&&($array_rec1['registered_office_address']['address_line_2']!=''))? $array_rec1['registered_office_address']['address_line_2']:''; }

        if($value['locality']!=$array_rec1['registered_office_address']['locality']){
        $data['locality']=(isset($array_rec1['registered_office_address']['locality'])&&($array_rec1['registered_office_address']['locality']!=''))? $array_rec1['registered_office_address']['locality']:''; }

       if($value['postal']!=$array_rec1['registered_office_address']['postal_code']){
        $data['postal']=(isset($array_rec1['registered_office_address']['postal_code'])&&($array_rec1['registered_office_address']['postal_code']!=''))? $array_rec1['registered_office_address']['postal_code']:''; }*/

				if ($value['crm_incorporation_date'] != $array_rec1['date_of_creation']) {
					$data['crm_incorporation_date'] = $datas['crm_incorporation_date'] = (isset($array_rec1['date_of_creation']) && ($array_rec1['date_of_creation'] != '')) ? $array_rec1['date_of_creation'] : '';
				} else {
					$datas['crm_incorporation_date'] = '0';
				}

				if ($value['crm_accounts_periodend'] != $array_rec1['accounts']['next_accounts']['period_end_on']) {
					$data['crm_accounts_periodend'] = $datas['crm_accounts_periodend'] = (isset($array_rec1['accounts']['next_accounts']['period_end_on']) && ($array_rec1['accounts']['next_accounts']['period_end_on'] != '')) ? $array_rec1['accounts']['next_accounts']['period_end_on'] : '';
				} else {
					$datas['crm_accounts_periodend'] = '0';
				}

				if ($value['crm_hmrc_yearend'] != $array_rec1['accounts']['next_made_up_to']) {
					$data['crm_hmrc_yearend'] = $datas['crm_hmrc_yearend'] = (isset($array_rec1['accounts']['next_made_up_to']) && ($array_rec1['accounts']['next_made_up_to'] != '')) ? $array_rec1['accounts']['next_made_up_to'] : '';
				} else {
					$datas['crm_hmrc_yearend'] = '0';
				}

				if ($value['crm_ch_accounts_next_due'] != $array_rec1['accounts']['next_due']) {
					$data['crm_ch_accounts_next_due'] = $datas['crm_ch_accounts_next_due'] = (isset($array_rec1['accounts']['next_due']) && ($array_rec1['accounts']['next_due'] != '')) ? $array_rec1['accounts']['next_due'] : '';
				} else {
					$datas['crm_ch_accounts_next_due'] = '0';
				}

				if ($value['crm_confirmation_statement_date'] != $array_rec1['confirmation_statement']['next_made_up_to']) {
					$data['crm_confirmation_statement_date'] = $datas['crm_confirmation_statement_date'] = (isset($array_rec1['confirmation_statement']['next_made_up_to']) && ($array_rec1['confirmation_statement']['next_made_up_to'] != '')) ? $array_rec1['confirmation_statement']['next_made_up_to'] : '';
				} else {
					$datas['crm_confirmation_statement_date'] = '0';
				}

				if ($value['crm_confirmation_statement_due_date'] != $array_rec1['confirmation_statement']['next_due']) {
					$data['crm_confirmation_statement_due_date'] = $datas['crm_confirmation_statement_due_date'] = (isset($array_rec1['confirmation_statement']['next_due']) && ($array_rec1['confirmation_statement']['next_due'] != '')) ? $array_rec1['confirmation_statement']['next_due'] : '';
				} else {
					$datas['crm_confirmation_statement_due_date'] = '0';
				}


				if (!empty($data)) {
					$update = $this->Common_mdl->update('client', $data, 'user_id', $value['user_id']);
				}
				if (isset($update) && $update == 1) {

					$new_records = $this->db->query("SELECT * FROM client")->result_array();
					$up_client = $this->db->query("SELECT count(*) as ccount FROM update_client WHERE user_id='" . $value['user_id'] . "'")->row_array();
					if ($up_client['ccount'] != 0) {
						$datas['counts'] = count($data);
						$datas['created_date'] = time();
						$this->Common_mdl->update('update_client', $datas, 'user_id', $value['user_id']);
					} else {
						$datas['counts'] = count($data);
						$datas['user_id'] = $value['user_id'];
						$datas['created_date'] = time();
						$this->db->insert('update_client', $datas);
					}
				}
			} // isset($array_rec1['error']) close
		} // foreach close


	} // function close 


	public function register_user_exists()
	{

		if (array_key_exists('name', $_POST)) {
			if ($this->username_exists($this->input->post('name')) == TRUE) {
				echo json_encode(FALSE);
			} else {
				echo json_encode(TRUE);
			}
		}
	}
	private function username_exists($username)
	{
		$this->db->where('username', $username);
		$query = $this->db->get('user');
		if ($query->num_rows() > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function register_email_exists()
	{
		if (array_key_exists('email_id', $_POST)) {
			if ($this->email_exists($this->input->post('email_id')) == TRUE) {
				echo json_encode(FALSE);
			} else {
				echo json_encode(TRUE);
			}
		}
	}

	private function email_exists($email)
	{
		$this->db->where('crm_email_id', $email);
		$query = $this->db->get('user');
		if ($query->num_rows() > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function addmanual_client()
	{

		/*     print_r($_POST);
     exit;*/
		$this->Security_model->chk_login();
		$datas['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user', 'role', '1');

		/*print_r($_POST);
         exit;*/

		if (isset($_POST)) {

			$var = rand(1, 10);
			//$user = $this->Common_mdl->GetAllWithWhere('user','id',$_POST['user_id']);
			$image_one = (isset($_POST['image_name']) && ($_POST['image_name'] != '')) ? $_POST['image_name'] : '825898.jpg';
			$image = (isset($_FILES['profile_image']['name']) && ($_FILES['profile_image']['name'] != '')) ? $this->Common_mdl->do_upload($_FILES['profile_image'], 'uploads') : $image_one;
			$data['crm_name'] = $_POST['user_name'];
			$data['username'] = $_POST['user_name'];
			$data['crm_email_id'] = $_POST['emailid'];
			$data['password'] = (isset($_POST['password']) && ($_POST['password'] != '')) ? md5($_POST['password']) : md5($var);
			$data['confirm_password'] = (isset($_POST['confirm_password']) && ($_POST['confirm_password'] != '')) ? $_POST['confirm_password'] : $var;
			$data['role'] = 4;
			$data['status'] = 1;
			//$data['company_roles']=(isset($_POST['company_house'])&&($_POST['company_house']!=''))? $_POST['company_house'] : 0;
			$data['CreatedTime'] = time();
			$data['client_id'] = (isset($_POST['client_id']) && ($_POST['client_id'] != '')) ? $_POST['client_id'] : rand(1, 5);
			$data['crm_packages'] = (isset($_POST['packages']) && ($_POST['packages'] != '')) ? $_POST['packages'] : '';
			$data['crm_gender'] = (isset($_POST['gender']) && ($_POST['gender'] != '')) ? $_POST['gender'] : '';
			$data['crm_profile_pic'] = $image;
			$data['autosave_status'] = 0;
			$data['firm_admin_id'] = $_SESSION['id'];


			$updates_user = (isset($_POST['user_id']) && ($_POST['user_id'] == '0' || $_POST['user_id'] == '')) ? $this->db->insert('user', $data) : $this->Common_mdl->update('user', $data, 'id', $_POST['user_id']);
			//echo $this->db->last_query();die;
			$insert_data = $updates_user;

			//  echo 'priya';
			//print_r($insert_data);die;
			if ($_POST['user_id'] == '') {
				$_POST['user_id'] = $this->db->insert_id();
			}
			if ($_POST['user_id']) {
				//$in=$this->db->insert_id();
				$in = $this->db->insert_id();
				$data1['user_id'] = (isset($_POST['user_id']) && ($_POST['user_id'] == '')) ? $in : $_POST['user_id'];
				// required information

				$other_services = $this->db->query('SELECT * FROM service_lists WHERE id>16')->result_array();

				foreach ($other_services as $key => $value) {
					if (!empty($_POST[$value['services_subnames']])) {
						$data1[$value['services_subnames']] =  json_encode($_POST[$value['services_subnames']]);
					} else {
						$data1[$value['services_subnames']] = '';
					}

					$data1['crm_' . $value['services_subnames'] . '_email_remainder'] = (isset($_POST['crm_' . $value['services_subnames'] . '_email_remainder']) && ($_POST['crm_' . $value['services_subnames'] . '_email_remainder'] != '')) ? $_POST['crm_' . $value['services_subnames'] . '_email_remainder'] : '';
					$data1['crm_' . $value['services_subnames'] . '_add_custom_reminder'] = (isset($_POST['crm_' . $value['services_subnames'] . '_add_custom_reminder']) && ($_POST['crm_' . $value['services_subnames'] . '_add_custom_reminder'] != '')) ? $_POST['crm_' . $value['services_subnames'] . '_add_custom_reminder'] : '';
					$data1['crm_' . $value['services_subnames'] . '_statement_date'] = (isset($_POST['crm_' . $value['services_subnames'] . '_statement_date']) && ($_POST['crm_' . $value['services_subnames'] . '_statement_date'] != '')) ? date("Y-m-d", strtotime($_POST['crm_' . $value['services_subnames'] . '_statement_date'])) : '';
				}

				if (!empty($_POST['confirm']))
					$data1['conf_statement'] = json_encode($_POST['confirm']);
				else
					$data1['conf_statement'] = '';

				if (!empty($_POST['accounts']))
					$data1['accounts'] = json_encode($_POST['accounts']);
				else
					$data1['accounts'] = '';
				if (!empty($_POST['companytax']))
					$data1['company_tax_return'] = json_encode($_POST['companytax']);
				else
					$data1['company_tax_return'] = '';
				if (!empty($_POST['personaltax']))
					$data1['personal_tax_return'] = json_encode($_POST['personaltax']);
				else
					$data1['personal_tax_return'] = '';

				if (!empty($_POST['payroll']))
					$data1['payroll'] = json_encode($_POST['payroll']);
				else
					$data1['payroll'] = '';

				if (!empty($_POST['workplace']))
					$data1['workplace'] = json_encode($_POST['workplace']);
				else
					$data1['workplace'] = '';

				if (!empty($_POST['vat']))
					$data1['vat'] = json_encode($_POST['vat']);
				else
					$data1['vat'] = '';

				if (!empty($_POST['cis']))
					$data1['cis'] = json_encode($_POST['cis']);
				else
					$data1['cis'] = '';

				if (!empty($_POST['cissub']))
					$data1['cissub'] = json_encode($_POST['cissub']);
				else
					$data1['cissub'] = '';

				if (!empty($_POST['p11d']))
					$data1['p11d'] = json_encode($_POST['p11d']);
				else
					$data1['p11d'] = '';


				if (!empty($_POST['bookkeep']))
					$data1['bookkeep'] = json_encode($_POST['bookkeep']);
				else
					$data1['bookkeep'] = '';


				if (!empty($_POST['management']))
					$data1['management'] = json_encode($_POST['management']);
				else
					$data1['management'] = '';


				if (!empty($_POST['investgate']))
					$data1['investgate'] = json_encode($_POST['investgate']);
				else
					$data1['investgate'] = '';

				if (!empty($_POST['registered']))
					$data1['registered'] = json_encode($_POST['registered']);
				else
					$data1['registered'] = '';

				if (!empty($_POST['taxadvice']))
					$data1['taxadvice'] = json_encode($_POST['taxadvice']);
				else
					$data1['taxadvice'] = '';

				if (!empty($_POST['taxinvest']))
					$data1['taxinvest'] = json_encode($_POST['taxinvest']);
				else
					$data1['taxinvest'] = '';

				$data1['firm_admin_id'] = $_SESSION['id'];


				//requried information
				$data1['crm_company_name'] = (isset($_POST['company_name1']) && ($_POST['company_name1'] != '')) ? $_POST['company_name1'] : '';
				$data1['crm_legal_form'] = (isset($_POST['legal_form']) && ($_POST['legal_form'] != '')) ? $_POST['legal_form'] : '';
				$data1['crm_allocation_holder'] = (isset($_POST['allocation_holders']) && ($_POST['allocation_holders'] != '')) ? $_POST['allocation_holders'] : '';
				//basic deatils
				$data1['crm_company_number'] = (isset($_POST['company_number']) && ($_POST['company_number'] != '')) ? trim($_POST['company_number']) : '';
				$data1['crm_company_url'] = (isset($_POST['company_url']) && ($_POST['company_url'] != '')) ? $_POST['company_url'] : '';
				$data1['crm_officers_url'] = (isset($_POST['officers_url']) && ($_POST['officers_url'] != '')) ? $_POST['officers_url'] : '';
				$data1['crm_company_name1'] = (isset($_POST['company_name']) && ($_POST['company_name'] != '')) ? $_POST['company_name'] : '';
				$data1['crm_register_address'] = (isset($_POST['register_address']) && ($_POST['register_address'] != '')) ? $_POST['register_address'] : '';
				$data1['crm_company_status'] = (isset($_POST['company_status']) && ($_POST['company_status'] != '')) ? $_POST['company_status'] : '';
				$data1['crm_company_type'] = (isset($_POST['company_type']) && ($_POST['company_type'] != '')) ? $_POST['company_type'] : '';
				$data1['crm_company_sic'] = (isset($_POST['company_sic']) && ($_POST['company_sic'] != '')) ? $_POST['company_sic'] : '';
				$data1['crm_sic_codes'] = (isset($_POST['sic_codes']) && ($_POST['sic_codes'] != '')) ? $_POST['sic_codes'] : '';
				$data1['crm_companies_house_authorisation_code'] = (isset($_POST['companies_house_authorisation_code']) && ($_POST['companies_house_authorisation_code'] != '')) ? end($_POST['companies_house_authorisation_code']) : '';
				$data1['crm_company_utr'] = (isset($_POST['company_utr']) && ($_POST['company_utr'] != '')) ? $_POST['company_utr'] : '';
				$data1['crm_incorporation_date'] = (isset($_POST['date_of_creation']) && ($_POST['date_of_creation'] != '')) ? $_POST['date_of_creation'] : '';

				$data1['crm_registered_in'] = (isset($_POST['registered_in']) && ($_POST['registered_in'] != '')) ? $_POST['registered_in'] : '';
				$data1['crm_address_line_one'] = (isset($_POST['address_line_one']) && ($_POST['address_line_one'] != '')) ? $_POST['address_line_one'] : '';
				$data1['crm_address_line_two'] = (isset($_POST['address_line_two']) && ($_POST['address_line_two'] != '')) ? $_POST['address_line_two'] : '';
				$data1['crm_address_line_three'] = (isset($_POST['address_line_three']) && ($_POST['address_line_three'] != '')) ? $_POST['address_line_three'] : '';
				$data1['crm_town_city'] = (isset($_POST['crm_town_city']) && ($_POST['crm_town_city'] != '')) ? $_POST['crm_town_city'] : '';
				$data1['crm_post_code'] = (isset($_POST['crm_post_code']) && ($_POST['crm_post_code'] != '')) ? $_POST['crm_post_code'] : '';
				$data1['crm_accounts_office_reference'] = (isset($_POST['accounts_office_reference']) && ($_POST['accounts_office_reference'] != '')) ? $_POST['accounts_office_reference'] : '';
				$data1['crm_vat_number'] = (isset($_POST['vat_number']) && ($_POST['vat_number'] != '')) ? $_POST['vat_number'] : '';
				// client information
				$data1['crm_letter_sign'] = (isset($_POST['engagement_letter']) && ($_POST['engagement_letter'] != '')) ? $_POST['engagement_letter'] : '';
				$data1['crm_business_website'] = (isset($_POST['business_website']) && ($_POST['business_website'] != '')) ? $_POST['business_website'] : '';
				$data1['crm_accounting_system'] = (isset($_POST['accounting_system_inuse']) && ($_POST['accounting_system_inuse'] != '')) ? $_POST['accounting_system_inuse'] : '';
				$data1['crm_paye_ref_number'] = (isset($_POST['paye_ref_number']) && ($_POST['paye_ref_number'] != '')) ? $_POST['paye_ref_number'] : '';

				//Assign to
				//$data1['crm_assign_team']=(isset($_POST['team'])&&($_POST['team']!=''))? $_POST['team'] : '0';
				//$data1['crm_allocation_holder']=(isset($_POST['allocation_holder'])&&($_POST['allocation_holder']!=''))? $_POST['team'] : '0';
				//$data1['crm_assign_manager_reviewer']=(isset($_POST['manager_reviewer'])&&($_POST['manager_reviewer']!=''))? $_POST['manager_reviewer'] : '0';
				//$data1['crm_assign_managed']=(isset($_POST['managed'])&&($_POST['managed']!=''))? $_POST['managed'] : '0';
				/*
$data1['crm_assign_team'] = (isset($_POST['team']) && !empty($_POST['team']))? implode(',', $_POST['team']):''; 
$data1['crm_allocation_holder'] = (isset($_POST['allocation_holder']) && !empty($_POST['allocation_holder']))? implode(' ^ ', $_POST['allocation_holder']):'';*/
				//$data1['crm_allocation_holder']=(isset($_POST['allocation_holder'])&&($_POST['allocation_holder']!=''))? $_POST['team'] : '0';

				$data1['crm_assign_client_id_verified'] = (isset($_POST['client_id_verified']) && ($_POST['client_id_verified'] != '')) ? $_POST['client_id_verified'] : '';
				$data1['crm_assign_type_of_id'] = (isset($_POST['type_of_id']) && ($_POST['type_of_id'] != '')) ? implode(',', $_POST['type_of_id']) : '';
				/** 30-08-2018 **/
				$data1['crm_assign_other_custom'] = (isset($_POST['assign_other_custom']) && ($_POST['assign_other_custom'] != '')) ? $_POST['assign_other_custom'] : '';
				/** end of 30-08-2018 **/
				$data1['crm_assign_proof_of_address'] = (isset($_POST['proof_of_address']) && ($_POST['proof_of_address'] != '')) ? $_POST['proof_of_address'] : '';
				$data1['crm_assign_meeting_client'] = (isset($_POST['meeting_client']) && ($_POST['meeting_client'] != '')) ? $_POST['meeting_client'] : '';
				$data1['crm_assign_source'] = (isset($_POST['source']) && ($_POST['source'] != '')) ? $_POST['source'] : '';
				$data1['crm_refered_by'] = (isset($_POST['refer_exist_client']) && ($_POST['refer_exist_client'] != '')) ? $_POST['refer_exist_client'] : '';
				$data1['crm_assign_relationship_client'] = (isset($_POST['relationship_client']) && ($_POST['relationship_client'] != '')) ? $_POST['relationship_client'] : '';
				$data1['crm_assign_notes'] = (isset($_POST['assign_notes']) && ($_POST['assign_notes'] != '')) ? $_POST['assign_notes'] : '';
				// other
				$data1['crm_previous_accountant'] = (isset($_POST['previous_account']) && ($_POST['previous_account'] != '')) ? $_POST['previous_account'] : '';
				$data1['crm_other_name_of_firm'] = (isset($_POST['name_of_firm']) && ($_POST['name_of_firm'] != '')) ? $_POST['name_of_firm'] : '';
				$data1['crm_other_address'] = (isset($_POST['other_address']) && ($_POST['other_address'] != '')) ? $_POST['other_address'] : '';
				$data1['crm_other_contact_no'] = (isset($_POST['other_contact_no']) && ($_POST['other_contact_no'] != '')) ? $_POST['other_contact_no'] : '';
				$data1['crm_email'] = (isset($_POST['emailid']) && ($_POST['emailid'] != '')) ? $_POST['emailid'] : '';
				$data1['crm_other_chase_for_info'] = (isset($_POST['chase_for_info']) && ($_POST['chase_for_info'] != '')) ? $_POST['chase_for_info'] : '';
				$data1['crm_other_notes'] = (isset($_POST['other_notes']) && ($_POST['other_notes'] != '')) ? $_POST['other_notes'] : '';
				$data1['crm_other_internal_notes'] = (isset($_POST['other_internal_notes']) && ($_POST['other_internal_notes'] != '')) ? $_POST['other_internal_notes'] : '';
				$data1['crm_other_invite_use'] = (isset($_POST['invite_use']) && ($_POST['invite_use'] != '')) ? $_POST['invite_use'] : '';
				$data1['crm_other_crm'] = (isset($_POST['other_crm']) && ($_POST['other_crm'] != '')) ? $_POST['other_crm'] : '';
				$data1['crm_other_proposal'] = (isset($_POST['other_proposal']) && ($_POST['other_proposal'] != '')) ? $_POST['other_proposal'] : '';
				$data1['crm_other_task'] = (isset($_POST['other_task']) && ($_POST['other_task'] != '')) ? $_POST['other_task'] : '';
				$data1['crm_other_send_invit_link'] = (isset($_POST['send_invit_link']) && ($_POST['send_invit_link'] != '')) ? $_POST['send_invit_link'] : '';
				$data1['crm_other_username'] = (isset($_POST['user_name']) && ($_POST['user_name'] != '')) ? $_POST['user_name'] : '';
				$data1['crm_other_password'] = (isset($_POST['password']) && ($_POST['password'] != '')) ? $_POST['password'] : '';
				$data1['crm_other_any_notes'] = (isset($_POST['other_any_notes']) && ($_POST['other_any_notes'] != '')) ? $_POST['other_any_notes'] : '';
				// confirmation
				$data1['crm_confirmation_auth_code'] = (isset($_POST['confirmation_auth_code']) && ($_POST['confirmation_auth_code'] != '')) ? $_POST['confirmation_auth_code'] : '';
				$data1['crm_confirmation_statement_date'] = (isset($_POST['confirmation_next_made_up_to']) && ($_POST['confirmation_next_made_up_to'] != '')) ? $_POST['confirmation_next_made_up_to'] : '';
				$data1['crm_confirmation_statement_due_date'] = (isset($_POST['confirmation_next_due']) && ($_POST['confirmation_next_due'] != '')) ? $_POST['confirmation_next_due'] : '';
				$data1['crm_confirmation_email_remainder'] = (isset($_POST['confirmation_next_reminder']) && ($_POST['confirmation_next_reminder'] != '')) ? $_POST['confirmation_next_reminder'] : '';
				$data1['crm_confirmation_create_task_reminder'] = (isset($_POST['create_task_reminder']) && ($_POST['create_task_reminder'] != '')) ? $_POST['create_task_reminder'] : '';
				$data1['crm_confirmation_add_custom_reminder'] = (isset($_POST['add_custom_reminder']) && ($_POST['add_custom_reminder'] != '')) ? $_POST['add_custom_reminder'] : '';
				$data1['crm_confirmation_officers'] = (isset($_POST['confirmation_officers']) && ($_POST['confirmation_officers'] != '')) ? $_POST['confirmation_officers'] : '';
				$data1['crm_share_capital'] = (isset($_POST['share_capital']) && ($_POST['share_capital'] != '')) ? $_POST['share_capital'] : '';
				$data1['crm_shareholders'] = (isset($_POST['shareholders']) && ($_POST['shareholders'] != '')) ? $_POST['shareholders'] : '';
				$data1['crm_people_with_significant_control'] = (isset($_POST['people_with_significant_control']) && ($_POST['people_with_significant_control'] != '')) ? $_POST['people_with_significant_control'] : '';
				/*$data1['crm_confirmation_notes']=(isset($_POST['confirmation_notes'])&&($_POST['confirmation_notes']!=''))? $_POST['confirmation_notes'] : '';*/
				// accounts
				$data1['crm_accounts_auth_code'] = (isset($_POST['accounts_auth_code']) && ($_POST['accounts_auth_code'] != '')) ? $_POST['accounts_auth_code'] : '';
				$data1['crm_accounts_utr_number'] = (isset($_POST['accounts_utr_number']) && ($_POST['accounts_utr_number'] != '')) ? $_POST['accounts_utr_number'] : '';
				$data1['crm_companies_house_email_remainder'] = (isset($_POST['company_house_reminder']) && ($_POST['company_house_reminder'] != '')) ? $_POST['company_house_reminder'] : '';
				$data1['crm_ch_yearend'] = (isset($_POST['accounts_next_made_up_to']) && ($_POST['accounts_next_made_up_to'] != '')) ? $_POST['accounts_next_made_up_to'] : '';
				$data1['crm_ch_accounts_next_due'] = (isset($_POST['accounts_next_due']) && ($_POST['accounts_next_due'] != '')) ? $_POST['accounts_next_due'] : '';
				$data1['crm_accounts_next_reminder_date'] = (isset($_POST['accounts_next_reminder_date']) && ($_POST['accounts_next_reminder_date'] != '')) ? $_POST['accounts_next_reminder_date'] : '';
				$data1['crm_accounts_create_task_reminder'] = (isset($_POST['accounts_create_task_reminder']) && ($_POST['accounts_create_task_reminder'] != '')) ? $_POST['accounts_create_task_reminder'] : '';
				$data1['crm_accounts_custom_reminder'] = (isset($_POST['accounts_custom_reminder']) && ($_POST['accounts_custom_reminder'] != '')) ? $_POST['accounts_custom_reminder'] : '';
				$data1['crm_accounts_due_date_hmrc'] = (isset($_POST['accounts_due_date_hmrc']) && ($_POST['accounts_due_date_hmrc'] != '')) ? date("Y-m-d", strtotime($this->date_format($_POST['accounts_due_date_hmrc']))) : '';
				$data1['crm_accounts_tax_date_hmrc'] = (isset($_POST['accounts_tax_date_hmrc']) && ($_POST['accounts_tax_date_hmrc'] != '')) ? date("Y-m-d", strtotime($_POST['accounts_tax_date_hmrc'])) : '';
				$data1['crm_company_next_reminder_date'] = (isset($_POST['company_next_reminder_date']) && ($_POST['company_next_reminder_date'] != '')) ? $_POST['company_next_reminder_date'] : '';
				$data1['crm_company_create_task_reminder'] = (isset($_POST['company_create_task_reminder']) && ($_POST['company_create_task_reminder'] != '')) ? $_POST['company_create_task_reminder'] : '';
				$data1['crm_company_custom_reminder'] = (isset($_POST['company_custom_reminder']) && ($_POST['company_custom_reminder'] != '')) ? $_POST['company_custom_reminder'] : '';
				// $data1['crm_accounts_notes']=(isset($_POST['accounts_notes'])&&($_POST['accounts_notes']!=''))? $_POST['accounts_notes'] : '';
				// personal tax return
				$data1['crm_personal_utr_number'] = (isset($_POST['personal_utr_number']) && ($_POST['personal_utr_number'] != '')) ? $_POST['personal_utr_number'] : '';
				$data1['crm_ni_number'] = (isset($_POST['ni_number']) && ($_POST['ni_number'] != '')) ? $_POST['ni_number'] : '';
				$data1['crm_property_income'] = (isset($_POST['property_income']) && ($_POST['property_income'] != '')) ? $_POST['property_income'] : '';
				$data1['crm_additional_income'] = (isset($_POST['additional_income']) && ($_POST['additional_income'] != '')) ? $_POST['additional_income'] : '';
				$data1['crm_personal_tax_return_date'] = (isset($_POST['personal_tax_return_date']) && ($_POST['personal_tax_return_date'] != '')) ? date('Y-m-d', strtotime($this->date_format($_POST['personal_tax_return_date']))) : '';
				$data1['crm_personal_due_date_return'] = (isset($_POST['personal_due_date_return']) && ($_POST['personal_due_date_return'] != '')) ? date('Y-m-d', strtotime($_POST['personal_due_date_return'])) : '';
				$data1['crm_personal_due_date_online'] = (isset($_POST['personal_due_date_online']) && ($_POST['personal_due_date_online'] != '')) ?  date('Y-m-d', strtotime($_POST['personal_due_date_online'])) : '';
				$data1['crm_personal_next_reminder_date'] = (isset($_POST['personal_next_reminder_date']) && ($_POST['personal_next_reminder_date'] != '')) ? $_POST['personal_next_reminder_date'] : '';
				$data1['crm_personal_task_reminder'] = (isset($_POST['personal_task_reminder']) && ($_POST['personal_task_reminder'] != '')) ? $_POST['personal_task_reminder'] : '';
				$data1['crm_personal_custom_reminder'] = (isset($_POST['personal_custom_reminder']) && ($_POST['personal_custom_reminder'] != '')) ? $_POST['personal_custom_reminder'] : '';
				/*$data1['crm_personal_notes']=(isset($_POST['personal_notes'])&&($_POST['personal_notes']!=''))? $_POST['personal_notes'] : '';*/

				// payroll
				$data1['crm_payroll_acco_off_ref_no'] = (isset($_POST['payroll_acco_off_ref_no']) && ($_POST['payroll_acco_off_ref_no'] != '')) ? $_POST['payroll_acco_off_ref_no'] : '';
				$data1['crm_paye_off_ref_no'] = (isset($_POST['paye_off_ref_no']) && ($_POST['paye_off_ref_no'] != '')) ? $_POST['paye_off_ref_no'] : '';
				$data1['crm_payroll_reg_date'] = (isset($_POST['payroll_reg_date']) && ($_POST['payroll_reg_date'] != '')) ? date('Y-m-d', strtotime($_POST['payroll_reg_date'])) : '';
				$data1['crm_no_of_employees'] = (isset($_POST['no_of_employees']) && ($_POST['no_of_employees'] != '')) ? $_POST['no_of_employees'] : '';
				$data1['crm_first_pay_date'] = (isset($_POST['first_pay_date']) && ($_POST['first_pay_date'] != '')) ? date('Y-m-d', strtotime($_POST['first_pay_date'])) : '';
				$data1['crm_payroll_run'] = (isset($_POST['payroll_run']) && ($_POST['payroll_run'] != '')) ? $_POST['payroll_run'] : '';
				$data1['crm_payroll_run_date'] = (isset($_POST['payroll_run_date']) && ($_POST['payroll_run_date'] != '')) ? $this->date_format($_POST['payroll_run_date']) : '';
				$data1['crm_rti_deadline'] = (isset($_POST['rti_deadline']) && ($_POST['rti_deadline'] != '')) ? $_POST['rti_deadline'] : '';
				$data1['crm_previous_year_require'] = (isset($_POST['previous_year_require']) && ($_POST['previous_year_require'] != '')) ? $_POST['previous_year_require'] : '';
				$data1['crm_payroll_if_yes'] = (isset($_POST['payroll_if_yes']) && ($_POST['payroll_if_yes'] != '')) ? $_POST['payroll_if_yes'] : '';
				$data1['crm_paye_scheme_ceased'] = (isset($_POST['paye_scheme_ceased']) && ($_POST['paye_scheme_ceased'] != '')) ? date('Y-m-d', strtotime($_POST['paye_scheme_ceased'])) : '';
				$data1['crm_payroll_next_reminder_date'] = (isset($_POST['payroll_next_reminder_date']) && ($_POST['payroll_next_reminder_date'] != '')) ? $_POST['payroll_next_reminder_date'] : '';
				$data1['crm_payroll_create_task_reminder'] = (isset($_POST['payroll_create_task_reminder']) && ($_POST['payroll_create_task_reminder'] != '')) ? $_POST['payroll_create_task_reminder'] : '';
				$data1['crm_payroll_add_custom_reminder'] = (isset($_POST['payroll_add_custom_reminder']) && ($_POST['payroll_add_custom_reminder'] != '')) ? $_POST['payroll_add_custom_reminder'] : '';
				$data1['crm_staging_date'] = (isset($_POST['staging_date']) && ($_POST['staging_date'] != '')) ? date('Y-m-d', strtotime($_POST['staging_date'])) : '';
				$data1['crm_pension_id'] = (isset($_POST['pension_id']) && ($_POST['pension_id'] != '')) ? $_POST['pension_id'] : '';
				$data1['crm_pension_subm_due_date'] = (isset($_POST['pension_subm_due_date']) && ($_POST['pension_subm_due_date'] != '')) ? $this->date_format($_POST['pension_subm_due_date']) : '';
				$data1['crm_postponement_date'] = (isset($_POST['defer_post_upto']) && ($_POST['defer_post_upto'] != '')) ? $this->date_format($_POST['defer_post_upto']) : '';
				$data1['crm_the_pensions_regulator_opt_out_date'] = (isset($_POST['pension_regulator_date']) && ($_POST['pension_regulator_date'] != '')) ? $_POST['pension_regulator_date'] : '';
				$data1['crm_re_enrolment_date'] = (isset($_POST['paye_re_enrolment_date']) && ($_POST['paye_re_enrolment_date'] != '')) ? $_POST['paye_re_enrolment_date'] : '';
				$data1['crm_declaration_of_compliance_due_date  '] = (isset($_POST['declaration_of_compliance_due_date']) && ($_POST['declaration_of_compliance_due_date'] != '')) ? $this->date_format($_POST['declaration_of_compliance_due_date']) : '';
				$data1['crm_declaration_of_compliance_submission  '] = (isset($_POST['declaration_of_compliance_last_filed']) && ($_POST['declaration_of_compliance_last_filed'] != '')) ? $_POST['declaration_of_compliance_last_filed'] : '';
				$data1['crm_paye_pension_provider'] = (isset($_POST['paye_pension_provider']) && ($_POST['paye_pension_provider'] != '')) ? $_POST['paye_pension_provider'] : '';
				$data1['crm_pension_id'] = (isset($_POST['paye_pension_provider_userid']) && ($_POST['paye_pension_provider_userid'] != '')) ? $_POST['paye_pension_provider_userid'] : '';
				$data1['crm_paye_pension_provider_password'] = (isset($_POST['paye_pension_provider_password']) && ($_POST['paye_pension_provider_password'] != '')) ? $_POST['paye_pension_provider_password'] : '';
				$data1['crm_employer_contri_percentage'] = (isset($_POST['employer_contri_percentage']) && ($_POST['employer_contri_percentage'] != '')) ? $_POST['employer_contri_percentage'] : '';
				$data1['crm_employee_contri_percentage'] = (isset($_POST['employee_contri_percentage']) && ($_POST['employee_contri_percentage'] != '')) ? $_POST['employee_contri_percentage'] : '';
				$data1['crm_pension_notes'] = (isset($_POST['pension_notes']) && ($_POST['pension_notes'] != '')) ? $_POST['pension_notes'] : '';
				$data1['crm_pension_next_reminder_date'] = (isset($_POST['pension_next_reminder_date']) && ($_POST['pension_next_reminder_date'] != '')) ? $_POST['pension_next_reminder_date'] : '';
				$data1['crm_pension_create_task_reminder'] = (isset($_POST['pension_create_task_reminder']) && ($_POST['pension_create_task_reminder'] != '')) ? $_POST['pension_create_task_reminder'] : '';
				$data1['crm_pension_add_custom_reminder'] = (isset($_POST['pension_add_custom_reminder']) && ($_POST['pension_add_custom_reminder'] != '')) ? $_POST['pension_add_custom_reminder'] : '';
				$data1['crm_cis_contractor'] = (isset($_POST['cis_contractor']) && ($_POST['cis_contractor'] != '')) ? $_POST['cis_contractor'] : '';
				$data1['crm_cis_contractor_start_date'] = (isset($_POST['cis_contractor_start_date']) && ($_POST['cis_contractor_start_date'] != '')) ? $this->date_format($_POST['cis_contractor_start_date']) : '';
				$data1['crm_cis_scheme_notes'] = (isset($_POST['cis_scheme_notes']) && ($_POST['cis_scheme_notes'] != '')) ? $_POST['cis_scheme_notes'] : '';
				$data1['crm_cis_subcontractor'] = (isset($_POST['cis_subcontractor']) && ($_POST['cis_subcontractor'] != '')) ? $_POST['cis_subcontractor'] : '';
				$data1['crm_cis_subcontractor_start_date'] = (isset($_POST['cis_subcontractor_start_date']) && ($_POST['cis_subcontractor_start_date'] != '')) ? $this->date_format($_POST['cis_subcontractor_start_date']) : '';
				$data1['crm_cis_subcontractor_scheme_notes'] = (isset($_POST['cis_subcontractor_scheme_notes']) && ($_POST['cis_subcontractor_scheme_notes'] != '')) ? $_POST['cis_subcontractor_scheme_notes'] : '';
				$data1['crm_cis_next_reminder_date'] = (isset($_POST['cis_next_reminder_date']) && ($_POST['cis_next_reminder_date'] != '')) ? $_POST['cis_next_reminder_date'] : '';
				$data1['crm_cis_create_task_reminder'] = (isset($_POST['cis_create_task_reminder']) && ($_POST['cis_create_task_reminder'] != '')) ? $_POST['cis_create_task_reminder'] : '';
				$data1['crm_cis_add_custom_reminder'] = (isset($_POST['cis_add_custom_reminder']) && ($_POST['cis_add_custom_reminder'] != '')) ? $_POST['cis_add_custom_reminder'] : '';
				/** 11-09-2018 **/
				$data1['crm_cissub_next_reminder_date'] = (isset($_POST['cissub_next_reminder_date']) && ($_POST['cissub_next_reminder_date'] != '')) ? $_POST['cissub_next_reminder_date'] : '';
				$data1['crm_cissub_create_task_reminder'] = (isset($_POST['cissub_create_task_reminder']) && ($_POST['cissub_create_task_reminder'] != '')) ? $_POST['cissub_create_task_reminder'] : '';
				$data1['crm_cissub_add_custom_reminder'] = (isset($_POST['cissub_add_custom_reminder']) && ($_POST['cissub_add_custom_reminder'] != '')) ? $_POST['cissub_add_custom_reminder'] : '';
				/** end of 11-09-2018 **/
				$data1['crm_p11d_latest_action_date'] = (isset($_POST['p11d_start_date']) && ($_POST['p11d_start_date'] != '')) ? date('Y-m-d', strtotime($_POST['p11d_start_date'])) : '';
				$data1['crm_p11d_latest_action'] = (isset($_POST['p11d_todo']) && ($_POST['p11d_todo'] != '')) ? $_POST['p11d_todo'] : '';
				$data1['crm_latest_p11d_submitted'] = (isset($_POST['p11d_first_benefit_date']) && ($_POST['p11d_first_benefit_date'] != '')) ? date('Y-m-d', strtotime($_POST['p11d_first_benefit_date'])) : '0';
				$data1['crm_next_p11d_return_due'] = (isset($_POST['p11d_due_date']) && ($_POST['p11d_due_date'] != '')) ? $this->date_format($_POST['p11d_due_date']) : '';
				$data1['crm_p11d_previous_year_require'] = (isset($_POST['p11d_previous_year_require']) && ($_POST['p11d_previous_year_require'] != '')) ? $_POST['p11d_previous_year_require'] : '';
				$data1['crm_p11d_payroll_if_yes'] = (isset($_POST['p11d_payroll_if_yes']) && ($_POST['p11d_payroll_if_yes'] != '')) ? $_POST['p11d_payroll_if_yes'] : '';
				$data1['crm_p11d_records_received'] = (isset($_POST['p11d_paye_scheme_ceased']) && ($_POST['p11d_paye_scheme_ceased'] != '')) ? $_POST['p11d_paye_scheme_ceased'] : '';
				$data1['crm_p11d_next_reminder_date'] = (isset($_POST['p11d_next_reminder_date']) && ($_POST['p11d_next_reminder_date'] != '')) ? $_POST['p11d_next_reminder_date'] : '';
				$data1['crm_p11d_create_task_reminder'] = (isset($_POST['p11d_create_task_reminder']) && ($_POST['p11d_create_task_reminder'] != '')) ? $_POST['p11d_create_task_reminder'] : '';
				$data1['crm_p11d_add_custom_reminder'] = (isset($_POST['p11d_add_custom_reminder']) && ($_POST['p11d_add_custom_reminder'] != '')) ? $_POST['p11d_add_custom_reminder'] : '';
				/*$data1['crm_p11d_notes']=(isset($_POST['p11d_notes'])&&($_POST['p11d_notes']!=''))? $_POST['p11d_notes'] : '';*/
				//vat return
				$data1['crm_vat_number_one'] = (isset($_POST['vat_number_one']) && ($_POST['vat_number_one'] != '')) ? $_POST['vat_number_one'] : '';
				$data1['crm_vat_date_of_registration'] = (isset($_POST['vat_registration_date']) && ($_POST['vat_registration_date'] != '')) ? $_POST['vat_registration_date'] : '';
				$data1['crm_vat_frequency'] = (isset($_POST['vat_frequency']) && ($_POST['vat_frequency'] != '')) ? $_POST['vat_frequency'] : '';
				$data1['crm_vat_quater_end_date'] = (isset($_POST['vat_quater_end_date']) && ($_POST['vat_quater_end_date'] != '')) ? date('Y-m-d', strtotime($_POST['vat_quater_end_date'])) : '';
				$data1['crm_vat_quarters'] = (isset($_POST['vat_quarters']) && ($_POST['vat_quarters'] != '')) ? $_POST['vat_quarters'] : '';
				$data1['crm_last_vat_return_filed_upto'] = (isset($_POST['crm_last_vat_return_filed_upto']) && ($_POST['crm_last_vat_return_filed_upto'] != '')) ? date('Y-m-d', strtotime($_POST['crm_last_vat_return_filed_upto'])) : '';
				$data1['crm_vat_scheme'] = (isset($_POST['vat_scheme']) && ($_POST['vat_scheme'] != '')) ? $_POST['vat_scheme'] : '';
				$data1['crm_flat_rate_category'] = (isset($_POST['flat_rate_category']) && ($_POST['flat_rate_category'] != '')) ? $_POST['flat_rate_category'] : '';
				$data1['crm_flat_rate_percentage'] = (isset($_POST['flat_rate_percentage']) && ($_POST['flat_rate_percentage'] != '')) ? $_POST['flat_rate_percentage'] : '';
				$data1['crm_direct_debit'] = (isset($_POST['direct_debit']) && ($_POST['direct_debit'] != '')) ? $_POST['direct_debit'] : '';
				$data1['crm_annual_accounting_scheme'] = (isset($_POST['annual_accounting_scheme']) && ($_POST['annual_accounting_scheme'] != '')) ? $_POST['annual_accounting_scheme'] : '';
				$data1['crm_box5_of_last_quarter_submitted'] = (isset($_POST['box5_of_last_quarter_submitted']) && ($_POST['box5_of_last_quarter_submitted'] != '')) ? $_POST['box5_of_last_quarter_submitted'] : '0';
				$data1['crm_vat_address'] = (isset($_POST['vat_address']) && ($_POST['vat_address'] != '')) ? $_POST['vat_address'] : '';
				$data1['crm_vat_next_reminder_date'] = (isset($_POST['vat_next_reminder_date']) && ($_POST['vat_next_reminder_date'] != '')) ? $_POST['vat_next_reminder_date'] : '';
				$data1['crm_vat_create_task_reminder'] = (isset($_POST['vat_create_task_reminder']) && ($_POST['vat_create_task_reminder'] != '')) ? $_POST['vat_create_task_reminder'] : '';
				$data1['crm_vat_add_custom_reminder'] = (isset($_POST['vat_add_custom_reminder']) && ($_POST['vat_add_custom_reminder'] != '')) ? $_POST['vat_add_custom_reminder'] : '';
				/*$data1['crm_vat_notes']=(isset($_POST['vat_notes'])&&($_POST['vat_notes']!=''))? $_POST['vat_notes'] : '';*/

				//management accounts
				$data1['crm_bookkeeping'] = (isset($_POST['bookkeeping']) && ($_POST['bookkeeping'] != '')) ? $_POST['bookkeeping'] : '';
				$data1['crm_next_booking_date'] = (isset($_POST['next_booking_date']) && ($_POST['next_booking_date'] != '')) ? $this->date_format($_POST['next_booking_date']) : '';
				$data1['crm_method_bookkeeping'] = (isset($_POST['method_bookkeeping']) && ($_POST['method_bookkeeping'] != '')) ? $_POST['method_bookkeeping'] : '';
				$data1['crm_client_provide_record'] = (isset($_POST['client_provide_record']) && ($_POST['client_provide_record'] != '')) ? $_POST['client_provide_record'] : '';
				$data1['crm_bookkeep_next_reminder_date'] = (isset($_POST['bookkeep_next_reminder_date']) && ($_POST['bookkeep_next_reminder_date'] != '')) ? $_POST['bookkeep_next_reminder_date'] : '';
				$data1['crm_bookkeep_create_task_reminder'] = (isset($_POST['bookkeep_create_task_reminder']) && ($_POST['bookkeep_create_task_reminder'] != '')) ? $_POST['bookkeep_create_task_reminder'] : '';
				$data1['crm_bookkeep_add_custom_reminder'] = (isset($_POST['bookkeep_add_custom_reminder']) && ($_POST['bookkeep_add_custom_reminder'] != '')) ? $_POST['bookkeep_add_custom_reminder'] : '';
				$data1['crm_manage_acc_fre'] = (isset($_POST['manage_acc_fre']) && ($_POST['manage_acc_fre'] != '')) ? $_POST['manage_acc_fre'] : '';
				$data1['crm_next_manage_acc_date'] = (isset($_POST['next_manage_acc_date']) && ($_POST['next_manage_acc_date'] != '')) ? $this->date_format($_POST['next_manage_acc_date']) : '';
				$data1['crm_manage_method_bookkeeping'] = (isset($_POST['manage_method_bookkeeping']) && ($_POST['manage_method_bookkeeping'] != '')) ? $_POST['manage_method_bookkeeping'] : '';
				$data1['crm_manage_client_provide_record'] = (isset($_POST['manage_client_provide_record']) && ($_POST['manage_client_provide_record'] != '')) ? $_POST['manage_client_provide_record'] : '';
				$data1['crm_manage_next_reminder_date'] = (isset($_POST['manage_next_reminder_date']) && ($_POST['manage_next_reminder_date'] != '')) ? $_POST['manage_next_reminder_date'] : '';
				$data1['crm_manage_create_task_reminder'] = (isset($_POST['manage_create_task_reminder']) && ($_POST['manage_create_task_reminder'] != '')) ? $_POST['manage_create_task_reminder'] : '';
				$data1['crm_manage_add_custom_reminder'] = (isset($_POST['manage_add_custom_reminder']) && ($_POST['manage_add_custom_reminder'] != '')) ? $_POST['manage_add_custom_reminder'] : '';
				/*$data1['crm_manage_notes']=(isset($_POST['manage_notes'])&&($_POST['manage_notes']!=''))? $_POST['manage_notes'] : '';*/

				//Investigation Insurance

				$data1['crm_invesitgation_insurance'] = (isset($_POST['insurance_start_date']) && ($_POST['insurance_start_date'] != '')) ? $this->date_format($_POST['insurance_start_date']) : '';
				$data1['crm_insurance_renew_date'] = (isset($_POST['insurance_renew_date']) && ($_POST['insurance_renew_date'] != '')) ? $this->date_format($_POST['insurance_renew_date']) : '';
				$data1['crm_insurance_provider'] = (isset($_POST['insurance_provider']) && ($_POST['insurance_provider'] != '')) ? $_POST['insurance_provider'] : '';
				$data1['crm_claims_note'] = (isset($_POST['claims_note']) && ($_POST['claims_note'] != '')) ? $_POST['claims_note'] : '';
				$data1['crm_insurance_next_reminder_date'] = (isset($_POST['insurance_next_reminder_date']) && ($_POST['insurance_next_reminder_date'] != '')) ? $_POST['insurance_next_reminder_date'] : '';
				$data1['crm_insurance_create_task_reminder'] = (isset($_POST['insurance_create_task_reminder']) && ($_POST['insurance_create_task_reminder'] != '')) ? $_POST['insurance_create_task_reminder'] : '';
				$data1['crm_insurance_add_custom_reminder'] = (isset($_POST['insurance_add_custom_reminder']) && ($_POST['insurance_add_custom_reminder'] != '')) ? $_POST['insurance_add_custom_reminder'] : '';
				$data1['crm_registered_start_date'] = (isset($_POST['registered_start_date']) && ($_POST['registered_start_date'] != '')) ? $this->date_format($_POST['registered_start_date']) : '';
				$data1['crm_registered_renew_date'] = (isset($_POST['registered_renew_date']) && ($_POST['registered_renew_date'] != '')) ? $this->date_format($_POST['registered_renew_date']) : '';
				$data1['crm_registered_office_inuse'] = (isset($_POST['registered_office_inuse']) && ($_POST['registered_office_inuse'] != '')) ? $_POST['registered_office_inuse'] : '';
				$data1['crm_registered_claims_note'] = (isset($_POST['registered_claims_note']) && ($_POST['registered_claims_note'] != '')) ? $_POST['registered_claims_note'] : '';
				$data1['crm_registered_next_reminder_date'] = (isset($_POST['registered_next_reminder_date']) && ($_POST['registered_next_reminder_date'] != '')) ? $_POST['registered_next_reminder_date'] : '';
				$data1['crm_registered_create_task_reminder'] = (isset($_POST['registered_create_task_reminder']) && ($_POST['registered_create_task_reminder'] != '')) ? $_POST['registered_create_task_reminder'] : '';
				$data1['crm_registered_add_custom_reminder'] = (isset($_POST['registered_add_custom_reminder']) && ($_POST['registered_add_custom_reminder'] != '')) ? $_POST['registered_add_custom_reminder'] : '';
				$data1['crm_investigation_start_date'] = (isset($_POST['investigation_start_date']) && ($_POST['investigation_start_date'] != '')) ? $this->date_format($_POST['investigation_start_date']) : '';
				$data1['crm_investigation_end_date'] = (isset($_POST['investigation_end_date']) && ($_POST['investigation_end_date'] != '')) ? $this->date_format($_POST['investigation_end_date']) : '';
				$data1['crm_investigation_note'] = (isset($_POST['investigation_note']) && ($_POST['investigation_note'] != '')) ? $_POST['investigation_note'] : '';
				$data1['crm_investigation_next_reminder_date'] = (isset($_POST['investigation_next_reminder_date']) && ($_POST['investigation_next_reminder_date'] != '')) ? $_POST['investigation_next_reminder_date'] : '';
				$data1['crm_investigation_create_task_reminder'] = (isset($_POST['investigation_create_task_reminder']) && ($_POST['investigation_create_task_reminder'] != '')) ? $_POST['investigation_create_task_reminder'] : '';
				$data1['crm_investigation_add_custom_reminder'] = (isset($_POST['investigation_add_custom_reminder']) && ($_POST['investigation_add_custom_reminder'] != '')) ? $_POST['investigation_add_custom_reminder'] : '';
				/*$data1['crm_tax_investigation_note']=(isset($_POST['tax_investigation_note'])&&($_POST['tax_investigation_note']!=''))? $_POST['tax_investigation_note'] : '';*/
				$data1['select_responsible_type'] = (isset($_POST['select_responsible_type']) && ($_POST['select_responsible_type'] != '')) ? $_POST['select_responsible_type'] : '';

				// notes tab
				$data1['crm_notes_info'] = (isset($_POST['notes_info']) && ($_POST['notes_info'] != '')) ? $_POST['notes_info'] : '';
				/** activity log for notes **/
				if (isset($_POST['notes_info']) && ($_POST['notes_info'] != '')) {

					$activity_datas['log'] = "Notes Added ---" . $_POST['notes_info'];
					$activity_datas['createdTime'] = time();
					$activity_datas['module'] = 'Client';
					$activity_datas['sub_module'] = 'Client Notes';
					$activity_datas['user_id'] = $_SESSION['admin_id'];
					$activity_datas['module_id'] = (isset($_POST['user_id']) && ($_POST['user_id'] == '')) ? $in : $_POST['user_id'];

					$this->Common_mdl->insert('activity_log', $activity_datas);
				}
				/** end of activity log for notes **/
				// documents tab
				$data1['crm_documents'] = (isset($_FILES['document']['name']) && ($_FILES['document']['name'] != '')) ? $this->Common_mdl->do_upload($_FILES['document'], 'uploads') : '';

				$data1['created_date'] = time();
				$data1['autosave_status'] = 0;
				//$this->db->insert( 'client', $data1 );
				$c_id = $_POST['user_id'];
				$sql_c = $this->db->query("select * from client where user_id = $c_id")->row_array();
				if (!empty($sql_c)) {
					$updates_client =  $this->Common_mdl->update('client', $data1, 'user_id', $_POST['user_id']);
				} else {
					$updates_client =  $this->db->insert('client', $data1);
				}
				//$updates_client=(isset($_POST['user_id'])&&($_POST['user_id']==''))? $this->db->insert( 'client', $data1 ) : $this->Common_mdl->update('client',$data1,'user_id',$_POST['user_id']);
				//echo $this->db->last_query();die;
				//$data_user['crm_email_id']=(isset($_POST['other_email'])&&($_POST['other_email']!=''))? $_POST['other_email'] : '';
				//$updates_user=$this->Common_mdl->update('user',$data_user,'id',$_POST['user_id']);
				if ($updates_client && isset($_POST['chase_for_info']) == 'on') {
					$query = $this->db->query('select * from user where id="' . $_POST['user_id'] . '"')->row_array();
					$data2['username'] = $query['username'];
					$user_id = $query['id'];
					$password = $query['password'];
					$encrypt_id = base64_encode($user_id);
					$email_subject  = 'Email For Others';
					$data2['email_content']  = '<a href=' . base_url() . ' style="display: inline-block;text-align: center;color: #00aeef;text-decoration: none;font-family:;background: #00aeef;color: #fff;font-weight: 600;padding: 5px 10px;border-radius: 3px;">Click Here</a><p>Notes :' . $_POST['other_notes'] . '</p>';
					$data2['title'] = 'Others Chase for Information Mail';
					//email template
					$body = $this->load->view('remainder_email.php', $data2, TRUE);
					$this->load->library('email');
					$this->email->set_mailtype("html");
					$this->email->from('info@remindoo.org');
					$this->email->to($query['crm_email_id']);
					$this->email->subject($email_subject);
					$this->email->message($body);
					$send = $this->email->send();
					/*if($send){
                echo 'welcome email remainder send';
            
            }else{
                echo 'welcome email remainder not send';
               
            }*/
				}


				$email = $this->input->post('emailid');
				$query = $this->db->query('select * from user where crm_email_id="' . $_POST['emailid'] . '"')->row_array();
				if ($query['username'] != '') {

					$data2['username'] = $query['username'];
					$user_id = $query['id'];
					$password = $query['password'];
					$encrypt_id = base64_encode($user_id);
					$email_subject  = 'Welcome Email';
					$data2['email_content']  = '<a href=' . base_url() . 'login/resetPwd/' . $encrypt_id . ' style="display: inline-block;text-align: center;color: #00aeef;text-decoration: none;font-family:;background: #00aeef;color: #fff;font-weight: 600;padding: 5px 10px;border-radius: 3px;">Click Here</a> for Reset Your CRM Account Password';
					$this->session->set_flashdata('email', $email);
					//email template
					$body = $this->load->view('forgot_email_format.php', $data2, TRUE);
					//redirect('login/email_format');
					//echo $email_content;
					//die;
					$this->mailsettings();
					$this->email->from('info@techleafsolutions.com');
					$this->email->to($email);
					$this->email->subject($email_subject);
					$this->email->message($body);
					$send = $this->email->send();
					/*  if($send){
                echo 'send';
            $this->session->set_flashdata('success', "Kindly Check Your Mail Id");
            }else{
                echo 'not send';
            $this->session->set_flashdata('warning', "Mail Not sent"); 
            }
*/
				}
				if ($in == '0') {
					echo $_POST['user_id'];
				} else {
					echo $in;
				}
			} else {
				echo '0';
			}
		} else {
			$sql = $this->db->query("select * from user order by id desc limit 0,1")->row_array();
			echo $sql['id'];
		}
	}
	public function reports()
	{
		$this->Security_model->chk_login();
		$data['getallUser'] = $this->db->query("SELECT * FROM user where role!='1' AND autosave_status!='1' order by id DESC")->result_array();
		$this->load->view('users/reports_view');
	}


	// add manual info view page

	public function client_manualinfo($user_id = false)
	{

		$this->Security_model->chk_login();
		$data['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user', 'role', '1');
		$data['client'] = $this->Common_mdl->GetAllWithWhere('client', 'user_id', $user_id);
		$data['responsible_team'] = $this->Common_mdl->GetAllWithWhere('responsible_team', 'client_id', $user_id);
		$data['responsible_user'] = $this->Common_mdl->GetAllWithWhere('responsible_user', 'client_id', $user_id);
		$data['responsible_department'] = $this->Common_mdl->GetAllWithWhere('responsible_department', 'client_id', $user_id);
		$data['responsible_member'] = $this->Common_mdl->GetAllWithWhere('responsible_members', 'client_id', $user_id);
		$data['contactRec'] = $this->Common_mdl->GetAllWithWhere('client_contacts', 'client_id', $user_id);
		$data['user'] = $this->Common_mdl->GetAllWithWhere('user', 'id', $user_id);
		// $data['referby'] = $this->db->query("SELECT * FROM `client` where crm_first_name!='' and crm_first_name!='0' GROUP BY `crm_first_name`")->result_array();
		/** 21-08-2018 **/
		$query1 = $this->db->query("SELECT * FROM `user` where role=4 AND autosave_status!='1' and crm_name!='' and firm_admin_id=" . $_SESSION['id'] . " and status=1 order by id DESC");
		$results1 = $query1->result_array();
		$res = array();
		if (count($results1) > 0) {
			foreach ($results1 as $key => $value) {
				array_push($res, $value['id']);
			}
		}
		if (!empty($res)) {
			$im_val = implode(',', $res);
			//echo $im_val."abc abc";
			$data['referby'] = $this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id in (" . $im_val . ") order by id desc ")->result_array();
			//$results = $query->result_array(); 
		} else {
			$data['referby'] = array();
		}
		/** end of 21-08-2018 **/
		$data['staff_form'] = $this->Common_mdl->GetAllWithWhere('user', 'role', '6');
		$data['managed_by'] = $this->Common_mdl->GetAllWithWhere('user', 'role', '5');
		if ($user_id != '') {
			$data['task_list'] = $this->Common_mdl->GetAllWithWhere('add_new_task', 'user_id', $user_id);
		} else {
			$data['task_list'] = $this->Common_mdl->getallrecords('add_new_task');
		}


		$this->load->view('clients/add_manual_client_info', $data);
	}

	function updates_clientcontact()
	{
		$data['title'] = $_POST['title'];
		$data['first_name'] = $_POST['first_name'];
		$data['middle_name'] = $_POST['middle_name'];
		$data['surname'] = $_POST['surname'];
		$data['preferred_name'] = $_POST['preferred_name'];
		$data['mobile'] = $_POST['mobile'];
		$data['main_email'] = $_POST['main_email'];
		$data['nationality'] = $_POST['nationality'];
		$data['psc'] = $_POST['psc'];
		$data['ni_number'] = $_POST['ni_number'];
		$data['address_line1'] = $_POST['address_line1'];
		$data['address_line2'] = $_POST['address_line2'];
		$data['town_city'] = $_POST['town_city'];
		$data['post_code'] = $_POST['post_code'];
		$data['landline'] = $_POST['landline'];
		$data['work_email'] = $_POST['work_email'];
		$data['date_of_birth'] = $_POST['date_of_birth'];
		$data['nature_of_control'] = $_POST['nature_of_control'];
		$data['utr_number'] = $_POST['utr_number'];
		$Contact_id = $_POST['Contact_id'];
		$data['shareholder'] = $_POST['shareholder'];
		$data['contact_type'] = $_POST['contact_type'];
		$data['marital_status'] = $_POST['marital_status'];

		if ($this->Common_mdl->update('client_contacts', $data, 'id', $Contact_id)) {
			return true;
		} else {
			return false;
		}
	}
	function insert_clientcontact($Contact_id)
	{

		//$unset=$_POST['Contact_id'];
		//unset($_POST['Contact_id']);
		$this->db->where('client_id', $Contact_id);
		$d = $this->db->delete('client_contacts');

		parse_str($_POST["formdata"], $_POST);


		$data['title'] = $_POST['title'];
		$data['first_name'] = $_POST['first_name'];
		$data['middle_name'] = $_POST['middle_name'];
		$data['surname'] = $_POST['surname'];
		$data['preferred_name'] = $_POST['preferred_name'];
		$data['mobile'] = $_POST['mobile'];
		$data['main_email'] = $_POST['main_email'];
		$data['nationality'] = $_POST['nationality'];
		$data['psc'] = $_POST['psc'];
		$data['ni_number'] = $_POST['ni_number'];
		$data['address_line1'] = $_POST['address_line1'];
		$data['address_line2'] = $_POST['address_line2'];
		$data['town_city'] = $_POST['town_city'];
		$data['post_code'] = $_POST['post_code'];
		$data['landline'] = $_POST['landline'];
		$data['work_email'] = $_POST['work_email'];
		$data['date_of_birth'] = $_POST['date_of_birth'];
		$data['nature_of_control'] = $_POST['nature_of_control'];
		$data['utr_number'] = $_POST['utr_number'];
		$data['client_id'] =  $Contact_id;
		$data['shareholder'] = $_POST['shareholder'];
		$data['contact_type'] = $_POST['contact_type'];
		$data['marital_status'] = $_POST['marital_status'];

		if ($this->db->insert('client_contacts', $data)) {
			return true;
		} else {
			return false;
		}
	}

	public function delete_contact()
	{
		/*print_r($_POST);
exit;*/
		$this->db->where('id', $_POST['id']);
		$this->db->delete('client_contacts');
	}

	function new_contact()
	{
		$data['title'] = '';
		$data['first_name'] = '';
		$data['surname'] = '';
		$data['preferred_name'] = '';
		$data['address_line1'] = '';
		$data['address_line2'] = '';
		$data['premises'] = '';
		$data['region'] = '';
		$data['country'] = '';
		$data['locality'] = '';
		$data['post_code'] = '';
		$data['make_primary'] = '0';
		$data['cnt'] = $_POST['cnt'];
		$data['nationality'] = '';
		$data['occupation'] = '';
		$data['appointed_on'] = '';
		$data['country_of_residence'] = '';
		$data['birth'] = '';
		$data['psc'] = '';
		$data['nature_of_control'] = '';
		$data['incre'] = $_POST['incre'];
		$data['contact_from'] = '';


		if (isset($_SESSION['firm_id']) && $_SESSION['firm_id'] != 0) {
			$settings = $this->Common_mdl->Get_Client_fields_ManagementSettings($_SESSION['firm_id']);
			$data = array_merge($data, $settings);

			//each contact person id need for PTR content on change identification
			$user_id = (!empty($this->input->post('user_id')) ? $this->input->post('user_id') : 0);

			$this->db->insert("client_contacts", ['client_id' => $user_id]);
			$data['contact_id'] = $this->db->insert_id();

			$this->load->view('clients/contact_form', $data);
		} else if (isset($_SESSION['firm_id']) && $_SESSION['firm_id'] == 0) {
			$this->load->view('Firm/contact_form', $data);
		}
	}

	function new_contact_clientinfo()
	{
		$data['title'] = '';
		$data['first_name'] = '';
		$data['surname'] = '';
		$data['preferred_name'] = '';
		$data['address_line1'] = '';
		$data['address_line2'] = '';
		$data['premises'] = '';
		$data['region'] = '';
		$data['country'] = '';
		$data['locality'] = '';
		$data['post_code'] = '';
		$data['make_primary'] = '0';
		$data['cnt'] = $_POST['cnt'];
		$data['incre'] = $_POST['incre'];
		$data['contact_from'] = '';

		$this->load->view('clients/contact_form_newperson', $data);
	}

	function update_primary_contact()
	{
		$contactId = $_POST['contact_id'];
		$clientId = $_POST['clientId'];
		//$data = array(); 
		$data['make_primary'] = '0';
		/*$this->Common_mdl->update('client_contacts',$data,'client_id',$clientId);
           echo $this->db->last_query();
die;*/
		$this->Common_mdl->update('client_contacts', $data, 'client_id', $clientId);
		$datas['make_primary'] = '1';
		$this->Common_mdl->update('client_contacts', $datas, 'id', $contactId);
		//echo $this->db->last_query();


		$data['contactRec'] = $this->db->query("SELECT * FROM  `client_contacts` WHERE client_id =  '" . $clientId . "' ORDER BY make_primary DESC ")->result_array();
		$this->load->view('clients/contact_info', $data);
	}

	function add_assignto()
	{
		if (isset($_POST['clientId']) && isset($_POST['proposal_id'])) {
			$proposal = $this->db->query("SELECT lead_id,firm_id FROM proposals WHERE id = '" . $_POST['proposal_id'] . "'")->row_array();

			if ($proposal['lead_id'] != "" && $proposal['lead_id'] != 0) {
				$assignees = $this->db->query("SELECT assignees FROM firm_assignees WHERE module_name = 'LEADS' AND module_id = '" . $proposal['lead_id'] . "' AND firm_id = '" . $proposal['firm_id'] . "'")->row_array();
				$assignees = $assignees['assignees'];
			} else {
				$assignees = $proposal['firm_id'];
			}

			$data = ['firm_id' => $proposal['firm_id'], 'module_name' => 'CLIENT', 'module_id' => $_POST['clientId'], 'assignees' => $assignees];

			$this->db->insert('firm_assignees', $data);
		}

		return true;
	}

	function add_assigntodepart()
	{
		$clientId = $_POST['clientId'];
		$this->db->where('client_id', $clientId);
		$this->db->delete('responsible_department');


		//print_r($_POST);die;
		$data = array();
		$data['client_id'] = $_POST['clientId'];
		if (isset($_POST['depart']) && $_POST['depart'] != '') {
			foreach ($_POST['depart'] as $key => $details) {
				$data['depart'] = (isset($_POST['depart'][$key]) && ($_POST['depart'][$key] != '')) ? $_POST['depart'][$key] : '';
				$data['allocation_holder'] = (isset($_POST['allocation_holder'][$key]) && ($_POST['allocation_holder'][$key] != '')) ? $_POST['allocation_holder'][$key] : '';
				$data['CreatedTime'] = time();
				//$data['allocation_holder']=(isset($_POST['allocation_holder'])&&($_POST['allocation_holder']!=''))? $_POST['team'] : '0';
				$this->Common_mdl->insert('responsible_department', $data);
			}
		}
		return true;
	}

	function add_responsibleuser()
	{
		$clientId = $_POST['clientId'];
		$this->db->where('client_id', $clientId);
		$this->db->delete('responsible_user');


		//print_r($_POST);die;
		$data = array();
		$data['client_id'] = $_POST['clientId'];
		$data['CreatedTime'] = time();
		if (isset($_POST['manager_reviewer']) && $_POST['manager_reviewer'] != '') {
			foreach ($_POST['manager_reviewer'] as $key => $details) {
				//foreach ($key as $keys => $values) {
				// if(isset($_POST['assign_managed']))
				$data['assign_managed'] = (isset($_POST['assign_managed'][$key]) && ($_POST['assign_managed'][$key] != '')) ? $_POST['assign_managed'][$key] : '';
				$data['manager_reviewer'] = (isset($_POST['manager_reviewer'][$key]) && ($_POST['manager_reviewer'][$key] != '')) ? $_POST['manager_reviewer'][$key] : '';


				//}
				$this->Common_mdl->insert('responsible_user', $data);
				//$data['allocation_holder']=(isset($_POST['allocation_holder'])&&($_POST['allocation_holder']!=''))? $_POST['team'] : '0';
			}
		}
		return true;
	}

	function add_responsiblemember()
	{
		//print_r($_POST);die;
		$clientId = $_POST['clientId'];
		$this->db->where('client_id', $clientId);
		$this->db->delete('responsible_members');



		$data = array();
		$data['client_id'] = $_POST['clientId'];
		$data['CreatedTime'] = time();
		if (isset($_POST['manager_reviewer_member']) && $_POST['manager_reviewer_member'] != '') {
			foreach ($_POST['manager_reviewer_member'] as $key => $details) {
				//foreach ($key as $keys => $values) {
				// if(isset($_POST['assign_managed']))
				$data['assign_managed'] = (isset($_POST['assign_managed_member'][$key]) && ($_POST['assign_managed_member'][$key] != '')) ? $_POST['assign_managed_member'][$key] : '';
				$data['manager_reviewer'] = (isset($_POST['manager_reviewer_member'][$key]) && ($_POST['manager_reviewer_member'][$key] != '')) ? $_POST['manager_reviewer_member'][$key] : '';


				//}
				$this->Common_mdl->insert('responsible_members', $data);
				//$data['allocation_holder']=(isset($_POST['allocation_holder'])&&($_POST['allocation_holder']!=''))? $_POST['team'] : '0';
			}
		}
		return true;
	}
	public function date_format($dateform)
	{

		if ($dateform !== NULL) {
			return date('Y-m-d', strtotime($dateform));

			/* $diff= new DateTime($dateform);
    echo $diff;die;
    $date = $diff->format('Y-m-d');
    return $date;*/
		} elseif ($dateform == '') {
			return '';
		}
		return '';
	}


	function weekOfMonth($date)
	{
		/*  // estract date parts
    list($y, $m, $d) = explode('-', date('Y-m-d', strtotime($date)));

    // current week, min 1
    $w = 1;

    // for each day since the start of the month
    for ($i = 1; $i <= $d; ++$i) {
        // if that day was a sunday and is not the first day of month
        if ($i > 1 && date('w', strtotime("$y-$m-$i")) == 0) {
            // increment current week
            ++$w;
        }
    }

    // now return
    echo $w;*/

		$ddate = "2012-10-18";
		$duedt = explode("-", $ddate);
		$date  = mktime(0, 0, 0, $duedt[1], $duedt[2], $duedt[0]);
		$week  = (int)date('W', $date);
		echo "Weeknummer: " . $week;

		$monday = strtotime("last monday");
		$monday = date('w', $monday) == date('w') ? $monday + 7 * 86400 : $monday;

		$sunday = strtotime(date("Y-m-d", $monday) . " +6 days");

		$this_week_sd = date("Y-m-d", $monday);
		$this_week_ed = date("Y-m-d", $sunday);

		echo "Current week range from $this_week_sd to $this_week_ed ";

		$currentWeekNumber = date('W');
		echo 'Week number:' . $currentWeekNumber;

		$week_start = new DateTime();
		$week_start->setISODate('2018', '7');
		echo $week_start->format('d-M-Y');
		//echo $week_end->format('d-M-Y');


		/*function getStartAndEndDate($week, $year)
{
*/
		$week = '7';
		$year = '2018';
		$time = strtotime("1 January $year", time());
		$day = date('w', $time);
		$time += ((7 * $week) + 1 - $day) * 24 * 3600;
		$return[0] = date('Y-n-j', $time);
		$time += 6 * 24 * 3600;
		$return[1] = date('Y-n-j', $time);
		/** 29-06-2018 **/
		echo '<pre>';
		print_r($return);
		/** 29-06-2018 **/
		//}

		echo date('m-01-Y 00:00:00', strtotime('this month')) . '<br/>';
		echo date('m-t-Y 12:59:59', strtotime('this month')) . '<br/>';

		/*$date_string = "2012-10-18";
$date_int = strtotime($date_string);
$date_date = date($date_int);
$week_number = date('W', $date_date);
echo "Weeknumber: {$week_number}.";*/
	}

	function sms_integration($to)
	{/*
require_once "/home/miqbzuekjplp/public_html/CRMTool/vendor/autoload.php";
$key = "226a930f";
$scert = "56231124c516302f";
$basic  = new \Nexmo\Client\Credentials\Basic("226a930f", "56231124c516302f");
$client = new \Nexmo\Client($basic);

//$client = new Nexmo\Client(new Nexmo\Client\Credentials\Basic($key, $scert));     
$message = $client->message()->send([
    'to' => '9566945538',
    'from' => '9566945538',
    'text' => 'Test message from the Nexmo PHP Client'
]);
echo "Sent message to " . $message['to'] . ". Balance is now " . $message['remaining-balance'] . PHP_EOL;
*/

		$url = 'https://rest.nexmo.com/sms/json?' . http_build_query([
			'api_key' => "7ff04386",
			'api_secret' => "GHjMwCJcHSsysY8K",
			'to' => "+91" . $to,
			'from' => "NEXMO",
			'text' => 'Hello from Nexmo'
		]);

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		/** 29-06-2018 **/
		echo '<pre>';
		print_r($response);
		/** 29-06-2018 **/
	}


	public function send_sms($to)
	{
		// $to = "9566945538";
		$url = 'https://rest.nexmo.com/sms/json?' . http_build_query([
			'api_key' => "7ff04386",
			'api_secret' => "GHjMwCJcHSsysY8K",
			'to' => "+" . $to,
			'from' => "NEXMO",
			'text' => 'Hello from Remindoo, This is a Test SMS for Client adding'
		]);

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		/*echo '<pre>';
print_r($response);die;*/
		return true;
	}



	public function send_sms_timeline($to, $message)
	{
		// $to = "9566945538";
		$url = 'https://rest.nexmo.com/sms/json?' . http_build_query([
			'api_key' => "7ff04386",
			'api_secret' => "GHjMwCJcHSsysY8K",
			'to' => "+" . $to,
			'from' => "NEXMO",
			'text' => 'Hello from Remindoo, This is a Test SMS for Client adding'
		]);

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		/*echo '<pre>';
print_r($response);die;*/
		return true;
	}




	public function send_sms_email($to, $msg)
	{
		//  $to = "919566945538";
		//echo $to;die;
		$c_apikey = '7ff04386';
		$c_sec = 'GHjMwCJcHSsysY8K';
		$url = 'https://rest.nexmo.com/sms/json?' . http_build_query([
			'api_key' => "a4499da7",
			'api_secret' => "o8jlh6fvL3Fs9A00",
			'to' => "+" . $to,
			'from' => "NEXMO",
			'text' => $msg
		]);

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		//echo '<pre>';
		//print_r($response);
		//return true;
	}

	/* public function send_sms_email_tst()
{
    $to = "919566945538";
    $msg ="ergtsdfg";

  $c_apikey = '7ff04386';
  $c_sec = 'GHjMwCJcHSsysY8K';
    $url = 'https://rest.nexmo.com/sms/json?' . http_build_query([
        'api_key' => "a4499da7",
        'api_secret' => "o8jlh6fvL3Fs9A00",
        'to' => "+".$to,
        'from' => "NEXMO",
        'text' => $msg
    ]);

$ch = curl_init($url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$response = curl_exec($ch);
echo '<pre>';
print_r($response);
return true;
}*/


	public function email_reminder_sms($id)
	{

		$queries = $this->Common_mdl->get_price('admin_setting', 'id', '1', 'service_reminder');
		if ($queries == '1') {
			//echo "zero";
			$ab = 0;
			$admin = $this->Common_mdl->GetAllWithWhere('admin_setting', 'user_id', '1');
			$curr_time = date("h:i A", time());
			if ($admin[0]['client_reminder_time'] == '10:30 PM') {
				// accounts reminders
				$title[] = '';
				$email_contents[] = '';

				$records = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_ch_accounts_next_due!="0" AND crm_email!="0" AND user_id="' . $id . '" AND DATE(crm_ch_accounts_next_due) > DATE_FORMAT(NOW(),"%Y-%m-%d") ')->result_array();
				$accounts = $this->db->query('SELECT * FROM reminder_setting WHERE service_type="accounts"')->result_array();
				foreach ($records as $key => $value) {
					// echo "1st";
					$ab = 1;
					(isset(json_decode($value['accounts'])->reminder) && $value['accounts'] != '') ? $jsnaccounts =  json_decode($value['accounts'])->reminder : $jsnaccounts = '';
					(isset(json_decode($value['accounts'])->text) && $value['accounts'] != '') ? $jsnaccounts_t =  json_decode($value['accounts'])->text : $jsnaccounts_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_ch_accounts_next_due']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where crm_email_id="' . $value['crm_email'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];
					$a  = array('{crm_ch_yearend}' => $value['crm_ch_yearend'], '{crm_ch_accounts_next_due}' => $value['crm_ch_accounts_next_due'], '{crm_accounts_custom_reminder}' => $value['crm_accounts_custom_reminder']);
					foreach ($accounts as $acckey => $accvalue) {
						$email_content1    = $accvalue['message'];
						$email_content  = strtr($email_content1, $a);
						if (($query['username'] != '' && $jsnaccounts == 'on')) {
							if ($jsnaccounts_t == 'on') {
								$this->send_sms_email($to_number, $email_content);
							}
							$data2['username'] = $query['username'];
							$user_id = $query['id'];
							$password = $query['password'];
							$encrypt_id = base64_encode($user_id);
							$email_subject  = 'Reminder Notification';
							$email_contents[]  = '<p>' . $email_content . '</p>';
							$title[] = 'Accounts Statement Remainder Mail';
						}
					} //foreach close
				} // foreach close

				// confirmation reminders
				$confirmation = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_confirmation_statement_due_date!="0" AND crm_email!="0" AND DATE(crm_confirmation_statement_due_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '" ')->result_array();
				//echo $this->db->last_query();
				$confirmation_re = $this->db->query('SELECT * FROM reminder_setting WHERE service_type="confirmation"')->result_array();
				foreach ($confirmation as $key => $value) {
					//  echo "2st";
					$ab = 1;
					(isset(json_decode($value['conf_statement'])->reminder) && $value['conf_statement'] != '') ? $jsnconf =  json_decode($value['conf_statement'])->reminder : $jsnconf = '';
					(isset(json_decode($value['conf_statement'])->text) && $value['conf_statement'] != '') ? $jsnconf_t =  json_decode($value['conf_statement'])->text : $jsnconf_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_confirmation_statement_due_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where crm_email_id="' . $value['crm_email'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];
					$a  = array('{crm_confirmation_statement_date}' => $value['crm_confirmation_statement_date'], '{crm_confirmation_statement_due_date}' => $value['crm_confirmation_statement_due_date'], '{crm_confirmation_add_custom_reminder}' => $value['crm_confirmation_add_custom_reminder']);
					foreach ($confirmation_re as $acckey => $accvalue) {
						$email_content1    = $accvalue['message'];
						$email_content  = strtr($email_content1, $a);
						if (($query['username'] != '' && $jsnconf == 'on')) {
							if ($jsnconf_t == 'on') {
								$this->send_sms_email($to_number, $email_content);
							}
							$data2['username'] = $query['username'];
							$user_id = $query['id'];
							$password = $query['password'];
							$encrypt_id = base64_encode($user_id);
							$email_subject  = 'Email Remainder For Confirmation';
							$email_contents[]  = '<p>' . $email_content . '</p>';
							$title[] = 'Confirmation Statement Remainder Mail';
							//email template
						}
					} // foreach close
				} // foreach close

				// Tax Return  -- Accounts Due Date - HMRC


				$tax_return = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_accounts_due_date_hmrc) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '"')->result_array();
				$taxreturn_re = $this->db->query('SELECT * FROM reminder_setting WHERE service_type="tax_return"')->result_array();

				foreach ($tax_return as $key => $value) {
					//  echo "3st";
					$ab = 1;
					(isset(json_decode($value['company_tax_return'])->reminder) && $value['company_tax_return'] != '') ? $jsnAcc =  json_decode($value['company_tax_return'])->reminder : $jsnAcc = '';
					(isset(json_decode($value['company_tax_return'])->text) && $value['company_tax_return'] != '') ? $jsnAcc_t =  json_decode($value['company_tax_return'])->text : $jsnAcc_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_accounts_due_date_hmrc']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');

					$old_record[] = $value;

					$query = $this->db->query('select * from user where crm_email_id="' . $value['crm_email'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];

					$a  = array('{crm_ch_yearend}' => $value['crm_ch_yearend'], '{crm_accounts_due_date_hmrc}' => $value['crm_accounts_due_date_hmrc'], '{crm_accounts_custom_reminder}' => $value['crm_accounts_custom_reminder']);
					foreach ($taxreturn_re as $acckey => $accvalue) {

						$email_content1    = $accvalue['message'];
						$email_content  = strtr($email_content1, $a);

						if ($query['username'] != '' && $jsnAcc == 'on') {
							if ($jsnAcc_t) {
								$this->send_sms_email($to_number, $email_content);
							}
							$data2['username'] = $query['username'];
							$user_id = $query['id'];
							$password = $query['password'];
							$encrypt_id = base64_encode($user_id);
							$email_subject  = 'Email Remainder For Tax Return';
							$email_contents[]  = '<p>' . $email_content . '</p>';
							$title[] = 'Accounts Tax Return Remainder Mail';
						}
					} // foreach close
				} // foreach close

				// Personal Tax Return  -- Tax Return Date
				$personal_tax_return = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_personal_tax_return_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '"')->result_array();
				$personaltax_re = $this->db->query('SELECT * FROM reminder_setting WHERE service_type="personal_tax"')->result_array();

				foreach ($personal_tax_return as $key => $value) {
					// echo "4st";
					$ab = 1;
					(isset(json_decode($value['personal_tax_return'])->reminder) && $value['personal_tax_return'] != '') ? $jsnpertax =  json_decode($value['personal_tax_return'])->reminder : $jsnpertax = '';
					(isset(json_decode($value['personal_tax_return'])->text) && $value['personal_tax_return'] != '') ? $jsnpertax_t =  json_decode($value['personal_tax_return'])->text : $jsnpertax_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_personal_tax_return_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where crm_email_id="' . $value['crm_email'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];
					$a  = array('{crm_personal_tax_return_date}' => $value['crm_personal_tax_return_date'], '{crm_personal_custom_reminder}' => $value['crm_personal_custom_reminder']);
					foreach ($personaltax_re as $acckey => $accvalue) {
						$email_content1    = $accvalue['message'];
						$email_content  = strtr($email_content1, $a);
						if (($query['username'] != '') && $jsnpertax == 'on') {
							if ($jsnpertax_t == 'on') {
								$this->send_sms_email($to_number, $email_content);
							}
							$data2['username'] = $query['username'];
							$user_id = $query['id'];
							$password = $query['password'];
							$encrypt_id = base64_encode($user_id);
							$email_subject  = 'Email Remainder For Personal Tax Return';
							$email_contents[]  = '<p>' . $email_content . '</p>';
							$title[] = 'Personal Tax Return Remainder Mail';
						}
					} // foreach close
				} // foreach close

				// payroll --Payroll Run Date

				$Payroll = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_payroll_run_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '"')->result_array();
				$payroll_re = $this->db->query('SELECT * FROM reminder_setting WHERE service_type="payroll"')->result_array();
				foreach ($Payroll as $key => $value) {
					//echo "5st";
					$ab = 1;
					(isset(json_decode($value['payroll'])->reminder) && $value['payroll'] != '') ? $jsnpay =  json_decode($value['payroll'])->reminder : $jsnpay = '';
					(isset(json_decode($value['payroll'])->text) && $value['payroll'] != '') ? $jsnpay_t =  json_decode($value['payroll'])->text : $jsnpay_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_payroll_run_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where crm_email_id="' . $value['crm_email'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];
					$a  = array('{crm_payroll_run_date}' => $value['crm_payroll_run_date'], '{crm_payroll_add_custom_reminder}' => $value['crm_payroll_add_custom_reminder']);
					foreach ($payroll_re as $acckey => $accvalue) {
						$email_content1    = $accvalue['message'];
						$email_content  = strtr($email_content1, $a);
						if (($query['username'] != '') && $jsnpay == 'on') {
							if ($jsnpay_t == 'on') {
								$this->send_sms_email($to_number, $email_content);
							}
							$data2['username'] = $query['username'];
							$user_id = $query['id'];
							$password = $query['password'];
							$encrypt_id = base64_encode($user_id);
							$email_subject  = 'Email Remainder For Payroll Run Date';
							$email_contents[]  = '<p>' . $email_content . '</p>';
							$title[] = 'Payroll Run Date Remainder Mail';
						}
					} // foreach close
				} // foreach close

				//WorkPlace Pension-AE -- Pension Submission Due date
				$Pension_ae = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_pension_subm_due_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '"')->result_array();
				$pension_re = $this->db->query('SELECT * FROM reminder_setting WHERE service_type="pension"')->result_array();
				foreach ($Pension_ae as $key => $value) {
					// echo "6st"; 
					$ab = 1;
					(isset(json_decode($value['workplace'])->reminder) && $value['workplace'] != '') ? $jsnwork =  json_decode($value['workplace'])->reminder : $jsnwork = '';
					(isset(json_decode($value['workplace'])->text) && $value['workplace'] != '') ? $jsnwork_t =  json_decode($value['workplace'])->text : $jsnwork_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_pension_subm_due_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where crm_email_id="' . $value['crm_email'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];
					$a  = array('{crm_pension_subm_due_date}' => $value['crm_pension_subm_due_date'], '{crm_pension_add_custom_reminder}' => $value['crm_pension_add_custom_reminder']);
					foreach ($pension_re as $acckey => $accvalue) {
						$email_content1    = $accvalue['message'];
						$email_content  = strtr($email_content1, $a);
						if (($query['username'] != '') && $jsnwork == 'on') {
							if ($jsnwork_t = 'on') {
								$this->send_sms_email($to_number, $email_content);
							}
							$data2['username'] = $query['username'];
							$user_id = $query['id'];
							$password = $query['password'];
							$encrypt_id = base64_encode($user_id);
							$email_subject  = 'Email Remainder For Pension Submission Due date';
							$email_contents[]  = '<p>' . $email_content . '</p>';
							$title[] = 'Pension Submission Due Date Remainder Mail';
						}
					} // foreach close
				} // foreach close

				//WorkPlace Pension-AE --Declaration of Compliance Due
				$Pension_dec = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_declaration_of_compliance_due_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '"')->result_array();
				$declaration_re = $this->db->query('SELECT * FROM reminder_setting WHERE service_type="declaration"')->result_array();

				foreach ($Pension_dec as $key => $value) {
					//   echo "7st";
					$ab = 1;
					(isset(json_decode($value['workplace'])->reminder) && $value['workplace'] != '') ? $jsnworkde =  json_decode($value['workplace'])->reminder : $jsnworkde = '';
					(isset(json_decode($value['workplace'])->text) && $value['workplace'] != '') ? $jsnworkde_t =  json_decode($value['workplace'])->text : $jsnworkde_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_declaration_of_compliance_due_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where crm_email_id="' . $value['crm_email'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];
					$a  = array('{crm_declaration_of_compliance_due_date}' => $value['crm_declaration_of_compliance_due_date'], '{crm_pension_add_custom_reminder}' => $value['crm_pension_add_custom_reminder']);
					foreach ($declaration_re as $acckey => $accvalue) {
						$email_content1    = $accvalue['message'];
						$email_content  = strtr($email_content1, $a);
						if (($query['username'] != '') && $jsnworkde == 'on') {
							if ($jsnworkde_t == 'on') {
								$this->send_sms_email($to_number, $email_content);
							}
							$data2['username'] = $query['username'];
							$user_id = $query['id'];
							$password = $query['password'];
							$encrypt_id = base64_encode($user_id);
							$email_subject  = 'Email Remainder For Declaration of Compliance Due Date';
							$email_contents[]  = '<p>' . $email_content . '</p>';
							$title[] = 'Declaration of Compliance Due Date Remainder Mail';
						}
					} // foreach close
				} // foreach close

				// CIS Contractor --  Start Date

				$cis_contractor = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_cis_contractor_start_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '"')->result_array();
				$cis_re = $this->db->query('SELECT * FROM reminder_setting WHERE service_type="cis"')->result_array();

				foreach ($cis_contractor as $key => $value) {
					// echo "8st";
					$ab = 1;
					(isset(json_decode($value['cis'])->reminder) && $value['cis'] != '') ? $jsncontra =  json_decode($value['cis'])->reminder : $jsncontra = '';
					(isset(json_decode($value['cis'])->text) && $value['cis'] != '') ? $jsncontra_t =  json_decode($value['cis'])->text : $jsncontra_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_cis_contractor_start_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where crm_email_id="' . $value['crm_email'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];

					$a  = array('{crm_cis_contractor_start_date}' => $value['crm_cis_contractor_start_date'], '{crm_cis_add_custom_reminder}' => $value['crm_cis_add_custom_reminder']);
					foreach ($cis_re as $acckey => $accvalue) {

						$email_content1    = $accvalue['message'];
						$email_content  = strtr($email_content1, $a);

						if (($query['username'] != '') && $jsncontra == 'on') {
							if ($jsncontra_t == 'on') {
								$this->send_sms_email($to_number, $email_content);
							}
							$data2['username'] = $query['username'];
							$user_id = $query['id'];
							$password = $query['password'];
							$encrypt_id = base64_encode($user_id);
							$email_subject  = 'Email Remainder For C.I.S Contractor Start Date';
							$email_contents[]  = '<p>' . $email_content . '</p>';
							$title[] = 'C.I.S Contractor Start Date Remainder Mail';
						}
					} // foreach close
				} // foreach close
				// CIS Sub Contractor --  Start Date

				$cis_subcontractor = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_cis_subcontractor_start_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '"')->result_array();
				$cis_sub_re = $this->db->query('SELECT * FROM reminder_setting WHERE service_type="cis_sub"')->result_array();
				foreach ($cis_subcontractor as $key => $value) {
					//echo "9st"; 
					$ab = 1;
					(isset(json_decode($value['cissub'])->reminder) && $value['cissub'] != '') ? $jsncontrasub =  json_decode($value['cissub'])->reminder : $jsncontrasub = '';
					(isset(json_decode($value['cissub'])->text) && $value['cissub'] != '') ? $jsncontrasub_t =  json_decode($value['cissub'])->text : $jsncontrasub_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_cis_subcontractor_start_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where crm_email_id="' . $value['crm_email'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];

					$a  = array('{crm_cis_subcontractor_start_date}' => $value['crm_cis_subcontractor_start_date'], '{crm_cis_add_custom_reminder}' => $value['crm_cis_add_custom_reminder']);
					foreach ($cis_sub_re as $acckey => $accvalue) {

						$email_content1    = $accvalue['message'];
						$email_content  = strtr($email_content1, $a);
						if (($query['username'] != '') && $jsncontrasub == 'on') {
							if ($jsncontrasub_t == 'on') {
								$this->send_sms_email($to_number, $email_content);
							}
							$data2['username'] = $query['username'];
							$user_id = $query['id'];
							$password = $query['password'];
							$encrypt_id = base64_encode($user_id);
							$email_subject  = 'Email Remainder For C.I.S Subcontractor Start Date';
							$email_contents[]  = '<p>' . $email_content . '</p>';
							$title[] = 'C.I.S Subcontractor Start Date Remainder Mail';
						}
					} // foreach close
				} // foreach close

				// P11D -- P11D Due date

				$p11d = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_next_p11d_return_due) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '"')->result_array();
				$p11d_re = $this->db->query('SELECT * FROM reminder_setting WHERE service_type="p11d"')->result_array();
				foreach ($p11d as $key => $value) {
					//  echo "10st";
					$ab = 1;
					(isset(json_decode($value['p11d'])->reminder) && $value['p11d'] != '') ? $jsnp11d =  json_decode($value['p11d'])->reminder : $jsnp11d = '';
					(isset(json_decode($value['p11d'])->text) && $value['p11d'] != '') ? $jsnp11d_t =  json_decode($value['p11d'])->text : $jsnp11d_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_next_p11d_return_due']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where crm_email_id="' . $value['crm_email'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];
					$a  = array('{crm_next_p11d_return_due}' => $value['crm_next_p11d_return_due'], '{crm_p11d_add_custom_reminder}' => $value['crm_p11d_add_custom_reminder']);
					foreach ($p11d_re as $acckey => $accvalue) {
						$email_content1    = $accvalue['message'];
						$email_content  = strtr($email_content1, $a);
						if (($query['username'] != '') && $jsnp11d == 'on') {
							if ($jsnp11d_t == 'on') {
								$this->send_sms_email($to_number, $email_content);
							}
							$data2['username'] = $query['username'];
							$user_id = $query['id'];
							$password = $query['password'];
							$encrypt_id = base64_encode($user_id);
							$email_subject  = 'Email Remainder For P11D Due Date';
							$email_content[]  = '<p>' . $email_content . '</p>';
							$title[] = 'P11D Due Date Remainder Mail';
						}
					} // foreach close
				} // foreach close

				// Bookkeeping --  Next Bookkeeping Date

				$Bookkeeping = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_next_booking_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '"')->result_array();
				$bookkeep_re = $this->db->query('SELECT * FROM reminder_setting WHERE service_type="bookkeep"')->result_array();

				foreach ($Bookkeeping as $key => $value) {
					//  echo "11st";
					$ab = 1;
					(isset(json_decode($value['bookkeep'])->reminder) && $value['bookkeep'] != '') ? $jsnbookkeep =  json_decode($value['bookkeep'])->reminder : $jsnbookkeep = '';
					(isset(json_decode($value['bookkeep'])->text) && $value['bookkeep'] != '') ? $jsnbookkeep_t =  json_decode($value['bookkeep'])->text : $jsnbookkeep_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_next_booking_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;

					$query = $this->db->query('select * from user where crm_email_id="' . $value['crm_email'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];

					$a  = array('{crm_next_booking_date}' => $value['crm_next_booking_date'], '{crm_bookkeep_add_custom_reminder}' => $value['crm_bookkeep_add_custom_reminder']);
					foreach ($bookkeep_re as $acckey => $accvalue) {

						$email_content1    = $accvalue['message'];
						$email_content  = strtr($email_content1, $a);

						if (($query['username'] != '') && $jsnbookkeep == 'on') {
							if ($jsnbookkeep_t == 'on') {
								$this->send_sms_email($to_number, $email_content);
							}
							$data2['username'] = $query['username'];
							$user_id = $query['id'];
							$password = $query['password'];
							$encrypt_id = base64_encode($user_id);
							$email_subject  = 'Email Remainder For Next Bookkeeping Date';
							$email_content[]  = '<p>' . $email_content . '</p>';
							$title[] = 'Next Bookkeeping Date Remainder Mail';
						}
					} // foreach close
				} // foreach close

				// Management Accounts --  Next Management Accounts Due date

				$Management = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_next_manage_acc_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '"')->result_array();
				$management_re = $this->db->query('SELECT * FROM reminder_setting WHERE service_type="management"')->result_array();

				foreach ($Management as $key => $value) {
					//   echo "12st";
					$ab = 1;
					(isset(json_decode($value['management'])->reminder) && $value['management'] != '') ? $jsnmanagement =  json_decode($value['management'])->reminder : $jsnmanagement = '';
					(isset(json_decode($value['management'])->text) && $value['management'] != '') ? $jsnmanagement_t =  json_decode($value['management'])->text : $jsnmanagement_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_next_manage_acc_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where crm_email_id="' . $value['crm_email'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];
					$a  = array('{crm_next_manage_acc_date}' => $value['crm_next_manage_acc_date'], '{crm_manage_add_custom_reminder}' => $value['crm_manage_add_custom_reminder']);
					foreach ($management_re as $acckey => $accvalue) {
						$email_content1    = $accvalue['message'];
						$email_content  = strtr($email_content1, $a);
						if (($query['username'] != '') && $jsnmanagement == 'on') {
							if ($jsnmanagement_t == 'on') {
								$this->send_sms_email($to_number, $email_content);
							}
							$data2['username'] = $query['username'];
							$user_id = $query['id'];
							$password = $query['password'];
							$encrypt_id = base64_encode($user_id);
							$email_subject  = 'Email Remainder For Next Management Accounts Due Date';
							$email_contents[]  = '<p>' . $email_content . '</p>';
							$title[] = 'Next Management Accounts Due Date Remainder Mail';
						}
					} // foreach close
				} // foreach close
				// VAT Quaters --  VAT Quarters

				$vat_quarters = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND user_id="' . $id . '"')->result_array();
				$vat_re = $this->db->query('SELECT * FROM reminder_setting WHERE service_type="vat"')->result_array();
				foreach ($vat_quarters as $vat_quarterskey => $vat_quartersval) {

					(isset(json_decode($vat_quartersval['vat'])->reminder) && $vat_quartersval['vat'] != '') ? $vatmanagement =  json_decode($vat_quartersval['vat'])->reminder : $vatmanagement = '';
					(isset(json_decode($vat_quartersval['vat'])->text) && $vat_quartersval['vat'] != '') ? $vatmanagement_t =  json_decode($vat_quartersval['vat'])->text : $vatmanagement_t = '';

					if ($vatmanagement == 'on') {
						// echo "13st";
						$ab = 1;
						$crm_vat_quarters = $vat_quartersval['crm_vat_quarters'];
						if ($crm_vat_quarters != '0') {
							if ($crm_vat_quarters == 'Annual End Jan') {
								$month = '01';
							} elseif ($crm_vat_quarters == 'Annual End Feb') {
								$month = '02';
							} elseif ($crm_vat_quarters == 'Annual End Mar') {
								$month = '03';
							} elseif ($crm_vat_quarters == 'Annual End Apr') {
								$month = '04';
							} elseif ($crm_vat_quarters == 'Annual End May') {
								$month = '05';
							} elseif ($crm_vat_quarters == 'Annual End Jun') {
								$month = '06';
							} elseif ($crm_vat_quarters == 'Annual End Jul') {
								$month = '07';
							} elseif ($crm_vat_quarters == 'Annual End Aug') {
								$month = '08';
							} elseif ($crm_vat_quarters == 'Annual End Sep') {
								$month = '09';
							} elseif ($crm_vat_quarters == 'Annual End Oct') {
								$month = '10';
							} elseif ($crm_vat_quarters == 'Annual End Nov') {
								$month = '11';
							} elseif ($crm_vat_quarters == 'Annual End Dec') {
								$month = '12';
							} elseif ($crm_vat_quarters == 'Jan/Apr/Jul/Oct') {
								$month = '01_04_07_10';
							} elseif ($crm_vat_quarters == 'Feb/May/Aug/Nov') {
								$month = '02_05_08_11';
							} elseif ($crm_vat_quarters == 'Mar/Jun/Sep/Dec') {
								$month = '03_06_09_12';
							} elseif ($crm_vat_quarters == 'Monthly') {
								$month = '01_02_03_04_05_06_07_08_09_10_11_12';
							}
							$array = explode('_', $month);
							foreach ($array as $key => $value) {
								$y = date('Y');
								$e_date = date('01-' . $value . '-' . $y);
								//echo $e_date;
								$c_date = date('d-m-Y');
								//if($c_date==$e_date)
								//{
								$query = $this->db->query('select * from user where crm_email_id="' . $vat_quartersval['crm_email'] . '"')->row_array();

								$to_number = $query['crm_phone_number'];

								$a  = array('{crm_vat_quarters}' => $crm_vat_quarters, '{crm_vat_add_custom_reminder}' => $vat_quartersval['crm_vat_add_custom_reminder']);
								foreach ($vat_re as $acckey => $accvalue) {

									$email_content1    = $accvalue['message'];
									$email_content  = strtr($email_content1, $a);

									if (($query['username'] != '') && $vatmanagement == 'on') {
										if ($vatmanagement_t == 'on') {
											$this->send_sms_email($to_number, $email_content);
										}
										$data2['username'] = $query['username'];
										$user_id = $query['id'];
										$password = $query['password'];
										$encrypt_id = base64_encode($user_id);
										$email_subject  = 'Email Remainder For VAT Quaters Due Date';
										$email_contents[]  = '<p>' . $email_content . '</p>';
										$title[] = 'Next VAT Quaters Due Date Remainder Mail';
									}
								} //close foreach
								//}
							}
						}
					}
				}
				//echo $ab;
				if ($ab == 1) {
					// echo "true";
					// $data2["title"]=$title;
					// $data2["email_contents"]=$email_contents;
					// $data2['link']='<a href='.base_url().' style="display: inline-block;text-align: center;color: #00aeef;text-decoration: none;font-family:;background: #00aeef;color: #fff;font-weight: 600;padding: 5px 10px;border-radius: 3px;">Click Here</a>';



					/* 18.09.2018 */

					$proposal_content = $this->Common_mdl->select_record('proposal_proposaltemplate', 'title', 'leads_assigne_members_mail');

					$sender_details = $this->db->query('select company_name,crm_name from admin_setting where user_id=' . $_SESSION['id'] . '')->row_array();

					$sender_company = $sender_details['company_name'];
					$sender_name = $sender_details['crm_name'];
					$body1 = $proposal_content['body'];
					$subject = $proposal_content['subject'];


					$link = '<a href=' . base_url() . ' style="display: inline-block;text-align: center;color: #00aeef;text-decoration: none;font-family:;background: #00aeef;color: #fff;font-weight: 600;padding: 5px 10px;border-radius: 3px;">Click Here</a>';


					$a1  =   array(
						':: Client Name::' => $query['username'],
						':: Service Name:: ' => $title,
						':: service Deadlines::' => $email_contents,
						':: Service Link::' => $link
					);

					$data2['email_contents']  =   strtr($body1, $a1);
					$email_subject  =   strtr($subject, $a1);
					$user_data = $this->db->query("select * from user where id=" . $id . " ")->row_array();
					$contact_data = $this->Common_mdl->GetAllWithWheretwo('client_contacts', 'client_id', $id, 'make_primary', 1);

					// echo '<pre>';
					// print_r($data2);
					// echo '</pre>';



					$body = $this->load->view('service_remainder_email.php', $data2, TRUE);
					$this->load->library('email');
					$this->email->set_mailtype("html");
					$this->email->from('info@remindoo.org');
					$this->email->to($contact_data[0]['main_email']);
					$this->email->subject(preg_replace('/ {2,}/', ' ', str_replace('&nbsp;', ' ', strip_tags($email_subject))));
					$this->email->message($body);
					$send = $this->email->send();

					$da['user_id'] = 2;
					$da['crm_name'] = 'test';
					$da['crm_currency'] = '₨';
					$da['crm_profile_image'] = '';
					$da['logo_image'] = '';
					$da['created_date'] = time();
					$this->Common_mdl->insert('admin_setting', $da);
				}
			} //close if

		}
	} // function close


	function update_client_statuss()
	{
		$status = $_POST['status'];
		$id = $_POST['id'];
		$erc = $this->db->query("UPDATE `client` SET `status` = 1  WHERE `user_id` = $id ");
		$erc1 = $this->db->query("UPDATE `user` SET `company_roles` = 1  WHERE `id` = $id ");
		return true;
	}

	public function vat_email($id)
	{
		// VAT Quaters --  VAT Quarters

		$vat_quarters = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND user_id="' . $id . '"')->result_array();
		$vat_re = $this->db->query('SELECT * FROM reminder_setting WHERE service_type="vat"')->result_array();
		foreach ($vat_quarters as $vat_quarterskey => $vat_quartersval) {

			(isset(json_decode($vat_quartersval['vat'])->reminder) && $vat_quartersval['vat'] != '') ? $vatmanagement =  json_decode($vat_quartersval['vat'])->reminder : $vatmanagement = '';
			(isset(json_decode($vat_quartersval['vat'])->text) && $vat_quartersval['vat'] != '') ? $vatmanagement_t =  json_decode($vat_quartersval['vat'])->text : $vatmanagement_t = '';

			if ($vatmanagement == 'on') {
				$crm_vat_quarters = $vat_quartersval['crm_vat_quarters'];
				if ($crm_vat_quarters != '0') {
					if ($crm_vat_quarters == 'Annual End Jan') {
						$month = '01';
					} elseif ($crm_vat_quarters == 'Annual End Feb') {
						$month = '02';
					} elseif ($crm_vat_quarters == 'Annual End Mar') {
						$month = '03';
					} elseif ($crm_vat_quarters == 'Annual End Apr') {
						$month = '04';
					} elseif ($crm_vat_quarters == 'Annual End May') {
						$month = '05';
					} elseif ($crm_vat_quarters == 'Annual End Jun') {
						$month = '06';
					} elseif ($crm_vat_quarters == 'Annual End Jul') {
						$month = '07';
					} elseif ($crm_vat_quarters == 'Annual End Aug') {
						$month = '08';
					} elseif ($crm_vat_quarters == 'Annual End Sep') {
						$month = '09';
					} elseif ($crm_vat_quarters == 'Annual End Oct') {
						$month = '10';
					} elseif ($crm_vat_quarters == 'Annual End Nov') {
						$month = '11';
					} elseif ($crm_vat_quarters == 'Annual End Dec') {
						$month = '12';
					} elseif ($crm_vat_quarters == 'Jan/Apr/Jul/Oct') {
						$month = '01_04_07_10';
					} elseif ($crm_vat_quarters == 'Feb/May/Aug/Nov') {
						$month = '02_05_08_11';
					} elseif ($crm_vat_quarters == 'Mar/Jun/Sep/Dec') {
						$month = '03_06_09_12';
					} elseif ($crm_vat_quarters == 'Monthly') {
						$month = '01_02_03_04_05_06_07_08_09_10_11_12';
					}
					$array = explode('_', $month);
					foreach ($array as $key => $value) {
						$y = date('Y');
						$e_date = date('01-' . $value . '-' . $y);
						//echo $e_date;
						$c_date = date('d-m-Y');
						//if($c_date==$e_date)
						//{
						$query = $this->db->query('select * from user where crm_email_id="' . $vat_quartersval['crm_email'] . '"')->row_array();

						$to_number = $query['crm_phone_number'];

						$a  = array('{crm_vat_quarters}' => $crm_vat_quarters, '{crm_vat_add_custom_reminder}' => $vat_quartersval['crm_vat_add_custom_reminder']);
						foreach ($vat_re as $acckey => $accvalue) {

							$email_content1    = $accvalue['message'];
							$email_content  = strtr($email_content1, $a);

							if (($query['username'] != '') && $vatmanagement == 'on') {
								if ($vatmanagement_t == 'on') {
									$this->send_sms_email($to_number, $email_content);
								}
								$data2['username'] = $query['username'];
								$user_id = $query['id'];
								$password = $query['password'];
								$encrypt_id = base64_encode($user_id);
								$email_subject  = 'Email Remainder For VAT Quaters Due Date';
								$data2['email_content']  = '<a href=' . base_url() . ' style="display: inline-block;text-align: center;color: #00aeef;text-decoration: none;font-family:;background: #00aeef;color: #fff;font-weight: 600;padding: 5px 10px;border-radius: 3px;">Click Here</a><p>' . $email_content . '</p>';
								$data2['title'] = 'Next VAT Quaters Due Date Remainder Mail';
								//$data2['link']=base_url();
								//email template
								$body = $this->load->view('remainder_email.php', $data2, TRUE);
								//redirect('login/email_format');

								$this->load->library('email');
								$this->email->set_mailtype("html");
								//$this->mailsettings();
								$this->email->from('info@remindoo.org');
								$this->email->to($vat_quartersval['crm_email']);
								$this->email->subject($email_subject);
								$this->email->message($body);
								$send = $this->email->send();
								/*  print_r($body);
            die;*/
								if ($send) {
									echo 'Next VAT Quaters Accounts Due Date email remainder send';
									echo '<br>';

									$this->session->set_flashdata('success', "Kindly Check Your Mail Id");
								} else {
									echo 'Next VAT Quaters Due Date email remainder not send';
									echo '<br>';
									$this->session->set_flashdata('warning', "Mail Not sent");
								}
							}
						} //close foreach
						//}
					}
				}
			}
		}
	}

	public function getPSC($uri)
	{
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk/' . $uri);

		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_USERPWD, $this->companieshouseAPIkey);
		$response1 = curl_exec($curl);
		/** 29-06-2018 **/
		echo '<pre>';
		print_r($response1);
		die;
		/** 29-06-2018 **/
		if ($errno = curl_errno($curl)) {
			$error_message = curl_strerror($errno);
			echo "cURL error ({$errno}):\n {$error_message}";
		}
		curl_close($curl);
		$data1 = json_decode($response1);
		$array_rec1 = $this->object_2_array($data1);
		return $array_rec1;
	}

	public function search_client()
	{
		$this->Security_model->chk_login();
		if (isset($_POST["query"])) {
			$output = '';

			$query = $this->db->query("SELECT * FROM client WHERE crm_first_name LIKE '%" . $_POST["query"] . "%' AND autosave_status=0");


			$results = $query->result_array();
			$output .= '<ul class="list-unstyled">';
			if (count($results) > 0) {
				foreach ($results as $key => $val) {
					$output .= '<li class="client_name" data-id="' . $val['crm_first_name'] . '">' . $val["crm_first_name"] . '</li>';
				}
			} else {
				$output .= '<b>Data Not Found</b>';
			}
			$output .= '</ul>';
			echo $output;
		}
	}
	public function get_placeholder()
	{
		$this->Security_model->chk_login();
		if (isset($_POST["query"])) {
			$output = '';

			$email_temp = $this->Common_mdl->GetAllWithWhere('tblemailtemplates', 'id', $_POST["query"]);
			$results = explode(',', $email_temp[0]['placeholder']);
			//$output .= '<ul class="list-unstyled">';  
			if (!empty($results)) {
				foreach ($results as $key => $val) {
					$output .= '<span target="message" class="add_merge">' . $val . '</span><br>';
				}
			} else {
				$output .= '<b>Data Not Found</b>';
			}
			//$output .= '</ul>';  
			echo $output;
		}
	}
	public function Update_CusReminder()
	{
		if (isset($_POST)) {
			$old_tem_data = $this->db->query("select * from custom_service_reminder where id=" . $_POST['table_id'])->row_array();
			$data = [
				'due_type' => $_POST['due'],
				'due_days' => $_POST['days'],
				'subject' => $_POST['subject'],
				'body'    => $_POST['message'],
				'headline_color' => $_POST['color']
			];
			$this->db->update('custom_service_reminder', $data, 'id=' . $_POST['table_id']);
			$res = [
				'result' => $this->db->affected_rows()
			];
			if ($old_tem_data['due_type'] != $_POST['due'] || $old_tem_data['due_days'] != $_POST['days']) {
				if ($old_tem_data['service_id'] == 4) {
					$service_id = ["4_" . $old_tem_data['client_contacts_id']];
					$user_id    = $this->Common_mdl->get_field_value('client', 'user_id', 'id', $old_tem_data['client_id']);
					ob_start();
					$this->Service_Reminder_Model->Setup_ClientContactPerson_ServiceReminders($user_id, $service_id);
					ob_clean();
				} else {
					$service_id = [$old_tem_data['service_id']];
					ob_start();
					$this->Service_Reminder_Model->Setup_Client_ServiceReminders($old_tem_data['client_id'], $service_id);
					ob_clean();
				}



				/*$this->db->query("DELETE FROM service_reminder_cron_data  WHERE client_id=".$old_tem_data['client_id']." and service_id=".$old_tem_data['service_id']);
      $FIELDS = $this->Service_Reminder_Model->Get_Service_ReminderFields();
      $FIELDS = array_chunk( array_map( 'trim' , explode( ',' , $FIELDS ) ), 5);
      
      if($old_tem_data['service_id'] <= 15)
      {
        $indx =  $old_tem_data['service_id']-1;     
      }
      else if($old_tem_data['service_id']>16)
      { 
        foreach($Fields as $key => $value) 
        {
          if($value[4] == $old_tem_data['service_id'])
          { 
             $indx = $key;
          }       
        }        
      }      
     
      $serviceData = $this->db->query("SELECT ".implode(',',$FIELDS[ $indx ])." FROM client where id=".$old_tem_data['client_id'])->row_array();
      //change integer 
      $serviceData = array_values($serviceData);

      if($old_tem_data['service_id'] == 5)//vat
      {
        $this->Service_Reminder_Model->QuartersBase_ServiceReminder($old_tem_data['client_id'],$old_tem_data['service_id'],$serviceData);
      }
      else
      {
        $this->Service_Reminder_Model->Create_ServiceTask( $old_tem_data['client_id'] , $old_tem_data['service_id'] , $serviceData );
      }*/
			}


			echo json_encode($res);
		}
	}
	public function  edit_reminder($id)
	{


		$this->Security_model->chk_login();
		if (isset($_POST)) {
			//$email_temp = $this->Common_mdl->GetAllWithWhere('tblemailtemplates','id',$id);

			$service_id = $_POST['service_id'];
			//$data['service_type']=$email_temp[0]['type'];
			//$data['name']=(isset($_POST['name'])&&($_POST['name']!=''))? $_POST['name']:'';
			$data['subject'] = (isset($_POST['subject']) && ($_POST['subject'] != '')) ? $_POST['subject'] : '';
			$data['message'] = (isset($_POST['message']) && ($_POST['message'] != '')) ? $_POST['message'] : '';
			//$data['fromname']=(isset($_POST['fromname'])&&($_POST['fromname']!=''))? $_POST['fromname']:'';
			$data['due'] = (isset($_POST['due']) && ($_POST['due'] != '')) ? $_POST['due'] : '';
			$data['days'] = (isset($_POST['days']) && ($_POST['days'] != '')) ? $_POST['days'] : '';
			$data['status'] = 1;
			$data['created_time'] = time();
			$this->Common_mdl->update('reminder_setting', $data, 'id', $id);
		}
		redirect('Client/client_info/' . $_POST['userid']);
	}
	public function reminder_set_delete($id)
	{

		$email_temp = $this->Common_mdl->GetAllWithWhere('reminder_setting', 'id', $id);
		$this->Common_mdl->delete('reminder_setting', 'id', $id);

		redirect('Client/client_info/' . $email_temp[0]['user_id']);
	}


	public function email_remindersms($id)
	{
		$queries = $this->Common_mdl->get_price('admin_setting', 'id', '1', 'service_reminder');
		if ($queries == '1') {
			$ab = 0;
			$cd = 0;
			$admin = $this->Common_mdl->GetAllWithWhere('admin_setting', 'user_id', '1');
			$curr_time = date("h:i A", time());
			if ($admin[0]['client_reminder_time'] == '10:30 PM') {

				$title[] = '';
				$email_contents[] = '';
				$records = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_ch_accounts_next_due!="0" AND crm_email!="0" AND user_id="' . $id . '" AND DATE(crm_ch_accounts_next_due) > DATE_FORMAT(NOW(),"%Y-%m-%d") and reminder_block=0')->result_array();
				foreach ($records as $key => $value) {
					// echo "1st";
					$ab = 1;
					(isset(json_decode($value['accounts'])->reminder) && $value['accounts'] != '') ? $jsnaccounts =  json_decode($value['accounts'])->reminder : $jsnaccounts = '';
					(isset(json_decode($value['accounts'])->text) && $value['accounts'] != '') ? $jsnaccounts_t =  json_decode($value['accounts'])->text : $jsnaccounts_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_ch_accounts_next_due']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where id="' . $value['user_id'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];
					$a  = array('{crm_ch_yearend}' => $value['crm_ch_yearend'], '{crm_ch_accounts_next_due}' => $value['crm_ch_accounts_next_due'], '{crm_accounts_custom_reminder}' => $value['crm_accounts_custom_reminder']);
					$email_content  = '<p> Accounts Statement Remainder </p><br/>
                      <p> Next Statement Date</p><p>' . $value['crm_ch_yearend'] . '</p><p> Due By</p><p>' . $value['crm_ch_accounts_next_due'] . '</p>';
					$email_content_sms  = ' Next Statement Date ' . $value['crm_ch_yearend'] . '  Due By ' . $value['crm_ch_accounts_next_due'] . ' ';
					if (($query['username'] != '' && $jsnaccounts == 'on')) {
						$cd = 1;
						if ($jsnaccounts_t == 'on') {
							// $this->send_sms_email($to_number,$email_content);
						}
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Reminder Notification';
						$email_contents[]  = '<p>' . $email_content . '</p>';
						$email_content_new[]  = $email_content_sms;
						$title[] = 'Accounts Statement Remainder';

						$insert_content = array('user_id' => $query['id'], 'reminder_subject' => 'Accounts Statement Remainder', 'reminder_message' => $email_content);
						$this->Common_mdl->insert('client_reminder', $insert_content);
					}
				} // foreach close

				// confirmation reminders
				$confirmation = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_confirmation_statement_due_date!="0" AND crm_email!="0" AND DATE(crm_confirmation_statement_due_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '" AND reminder_block=0')->result_array();
				//echo $this->db->last_query();
				//$confirmation_re=$this->db->query('SELECT * FROM reminder_setting WHERE service_type="confirmation"')->result_array();
				foreach ($confirmation as $key => $value) {
					//  echo "2st";
					$ab = 1;
					(isset(json_decode($value['conf_statement'])->reminder) && $value['conf_statement'] != '') ? $jsnconf =  json_decode($value['conf_statement'])->reminder : $jsnconf = '';
					(isset(json_decode($value['conf_statement'])->text) && $value['conf_statement'] != '') ? $jsnconf_t =  json_decode($value['conf_statement'])->text : $jsnconf_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_confirmation_statement_due_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where id="' . $value['user_id'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];
					$a  = array('{crm_confirmation_statement_date}' => $value['crm_confirmation_statement_date'], '{crm_confirmation_statement_due_date}' => $value['crm_confirmation_statement_due_date'], '{crm_confirmation_add_custom_reminder}' => $value['crm_confirmation_add_custom_reminder']);
					// foreach ($confirmation_re as $acckey => $accvalue) {
					// $email_content1    = $accvalue['message'];
					$email_content  = '<p>Confirmation Statement Remainder</p><br/>
                              <p> Next Statement Date</p><p>' . $value['crm_confirmation_statement_date'] . '</p><p> Due By</p><p>' . $value['crm_confirmation_statement_due_date'] . '</p>';
					$email_content_sms  = ' Next Statement Date ' . $value['crm_confirmation_statement_date'] . '  Due By ' . $value['crm_confirmation_statement_due_date'] . ' ';
					if (($query['username'] != '' && $jsnconf == 'on')) {
						$cd = 1;
						if ($jsnconf_t == 'on') {
							// $this->send_sms_email($to_number,$email_content);
						}
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For Confirmation';
						$email_contents[]  = '<p>' . $email_content . '</p>';
						$email_content_new[]  = $email_content_sms;
						$title[] = 'Confirmation Statement Remainder';

						$insert_content = array('user_id' => $query['id'], 'reminder_subject' => 'Confirmation Statement Remainder', 'reminder_message' => $email_content);
						$this->Common_mdl->insert('client_reminder', $insert_content);
						//email template
					}
				} // foreach close

				// Tax Return  -- Accounts Due Date - HMRC
				$tax_return = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_accounts_due_date_hmrc) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '" AND reminder_block=0')->result_array();
				foreach ($tax_return as $key => $value) {
					$ab = 1;
					(isset(json_decode($value['company_tax_return'])->reminder) && $value['company_tax_return'] != '') ? $jsnAcc =  json_decode($value['company_tax_return'])->reminder : $jsnAcc = '';
					(isset(json_decode($value['company_tax_return'])->text) && $value['company_tax_return'] != '') ? $jsnAcc_t =  json_decode($value['company_tax_return'])->text : $jsnAcc_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_accounts_due_date_hmrc']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where id="' . $value['user_id'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];
					$a  = array('{crm_ch_yearend}' => $value['crm_ch_yearend'], '{crm_accounts_due_date_hmrc}' => $value['crm_accounts_due_date_hmrc'], '{crm_accounts_custom_reminder}' => $value['crm_accounts_custom_reminder']);
					$email_content  = '<p>Accounts Tax Return Remainder</p><br/><p> Next Statement Date</p><p>' . $value['crm_ch_yearend'] . '</p><p> Due By</p><p>' . $value['crm_accounts_due_date_hmrc'] . '</p>';
					$email_content_sms  = '  Next Statement Date ' . $value['crm_ch_yearend'] . '  Due By' . $value['crm_accounts_due_date_hmrc'] . ' ';

					if ($query['username'] != '' && $jsnAcc == 'on') {
						$cd = 1;
						if ($jsnAcc_t) {
							// $this->send_sms_email($to_number,$email_content);
						}
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For Tax Return';
						$email_contents[]  = '<p>' . $email_content . '</p>';
						$email_content_new[]  = $email_content_sms;
						$title[] = 'Accounts Tax Return Remainder';
						$insert_content = array('user_id' => $query['id'], 'reminder_subject' => 'Accounts Tax Remainder', 'reminder_message' => $email_content);
						$this->Common_mdl->insert('client_reminder', $insert_content);
					}
				} // foreach close

				// Personal Tax Return  -- Tax Return Date
				$personal_tax_return = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_personal_tax_return_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '" AND reminder_block=0')->result_array();
				foreach ($personal_tax_return as $key => $value) {
					$ab = 1;
					(isset(json_decode($value['personal_tax_return'])->reminder) && $value['personal_tax_return'] != '') ? $jsnpertax =  json_decode($value['personal_tax_return'])->reminder : $jsnpertax = '';
					(isset(json_decode($value['personal_tax_return'])->text) && $value['personal_tax_return'] != '') ? $jsnpertax_t =  json_decode($value['personal_tax_return'])->text : $jsnpertax_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_personal_tax_return_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where id="' . $value['user_id'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];
					$a  = array('{crm_personal_tax_return_date}' => $value['crm_personal_tax_return_date'], '{crm_personal_custom_reminder}' => $value['crm_personal_custom_reminder']);
					$email_content  = '<p>Personal Tax Return Remainder</p><br/><p> Next Statement Date</p><p>' . $value['crm_personal_tax_return_date'] . '</p><p> Personal Custom Reminder</p><p>' . $value['crm_personal_custom_reminder'] . '</p>';
					$email_content_sms  = '  Next Statement Date ' . $value['crm_personal_tax_return_date'] . '  Personal Custom Reminder ' . $value['crm_personal_custom_reminder'] . ' ';
					if (($query['username'] != '') && $jsnpertax == 'on') {
						$cd = 1;
						if ($jsnpertax_t == 'on') {
							// $this->send_sms_email($to_number,$email_content);
						}
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For Personal Tax Return';
						$email_contents[]  = '<p>' . $email_content . '</p>';
						$email_content_new[]  = $email_content_sms;
						$title[] = 'Personal Tax Return Remainder';
						$insert_content = array('user_id' => $query['id'], 'reminder_subject' => 'Personal Tax Remainder', 'reminder_message' => $email_content);
						$this->Common_mdl->insert('client_reminder', $insert_content);
					}
				} // foreach close

				// payroll --Payroll Run Date

				$Payroll = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_payroll_run_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '" AND reminder_block=0')->result_array();
				$payroll_re = $this->db->query('SELECT * FROM reminder_setting WHERE service_type="payroll"')->result_array();
				foreach ($Payroll as $key => $value) {
					//echo "5st";
					$ab = 1;
					(isset(json_decode($value['payroll'])->reminder) && $value['payroll'] != '') ? $jsnpay =  json_decode($value['payroll'])->reminder : $jsnpay = '';
					(isset(json_decode($value['payroll'])->text) && $value['payroll'] != '') ? $jsnpay_t =  json_decode($value['payroll'])->text : $jsnpay_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_payroll_run_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where id="' . $value['user_id'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];
					$a  = array('{crm_payroll_run_date}' => $value['crm_payroll_run_date'], '{crm_payroll_add_custom_reminder}' => $value['crm_payroll_add_custom_reminder']);
					//   foreach ($payroll_re as $acckey => $accvalue) {
					//  $email_content1    = $accvalue['message'];
					$email_content  = '<p>Payroll Run Date Remainder</p></br><p> Payroll Run Date</p><p>' . $value['crm_payroll_run_date'] . '</p><p>Payroll Custom Reminder date</p><p>' . $value['crm_payroll_add_custom_reminder'] . '</p>';
					$email_content_sms  = '  Payroll Run Date ' . $value['crm_payroll_run_date'] . ' Payroll Custom Reminder date ' . $value['crm_payroll_add_custom_reminder'] . ' ';
					if (($query['username'] != '') && $jsnpay == 'on') {
						$cd = 1;
						if ($jsnpay_t == 'on') {
							//  $this->send_sms_email($to_number,$email_content);
						}
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For Payroll Run Date';
						$email_contents[]  = '<p>' . $email_content . '</p>';
						$email_content_new[]  = $email_content_sms;
						$title[] = 'Payroll Run Date Remainder';
						$insert_content = array('user_id' => $query['id'], 'reminder_subject' => 'Payroll Run Remainder', 'reminder_message' => $email_content);
						$this->Common_mdl->insert('client_reminder', $insert_content);
					}
					// } // foreach close
				} // foreach close

				//WorkPlace Pension-AE -- Pension Submission Due date
				$Pension_ae = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_pension_subm_due_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '" AND reminder_block=0')->result_array();
				//$pension_re=$this->db->query('SELECT * FROM reminder_setting WHERE service_type="pension"')->result_array();
				foreach ($Pension_ae as $key => $value) {
					// echo "6st"; 
					$ab = 1;
					(isset(json_decode($value['workplace'])->reminder) && $value['workplace'] != '') ? $jsnwork =  json_decode($value['workplace'])->reminder : $jsnwork = '';
					(isset(json_decode($value['workplace'])->text) && $value['workplace'] != '') ? $jsnwork_t =  json_decode($value['workplace'])->text : $jsnwork_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_pension_subm_due_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where id="' . $value['user_id'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];
					$a  = array('{crm_pension_subm_due_date}' => $value['crm_pension_subm_due_date'], '{crm_pension_add_custom_reminder}' => $value['crm_pension_add_custom_reminder']);
					//foreach ($pension_re as $acckey => $accvalue) {
					//  $email_content1    = $accvalue['message'];
					$email_content  = '<p>Pension Submission Due Date Remainder</p><br/><p> pension Submission Due Date</p><p>' . $value['crm_pension_subm_due_date'] . '</p><p> pension Add Custom Reminder </p><p>' . $value['crm_pension_add_custom_reminder'] . '</p>';
					$email_content_sms  = '  pension Submission Due Date ' . $value['crm_pension_subm_due_date'] . '  pension Add Custom Reminder  ' . $value['crm_pension_add_custom_reminder'] . ' ';
					//'<p>Payroll Run Date';
					if (($query['username'] != '') && $jsnwork == 'on') {
						$cd = 1;
						if ($jsnwork_t = 'on') {
							// $this->send_sms_email($to_number,$email_content);
						}
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For Pension Submission Due date';
						$email_contents[]  = '<p>' . $email_content . '</p>';
						$email_content_new[]  = $email_content_sms;
						$title[] = 'Pension Submission Due Date Remainder';
						$insert_content = array('user_id' => $query['id'], 'reminder_subject' => 'Pension Submission Due Date Remainder', 'reminder_message' => $email_content);
						$this->Common_mdl->insert('client_reminder', $insert_content);
					}
					//   } // foreach close
				} // foreach close

				//WorkPlace Pension-AE --Declaration of Compliance Due
				$Pension_dec = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_declaration_of_compliance_due_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '" AND reminder_block=0')->result_array();
				//$declaration_re=$this->db->query('SELECT * FROM reminder_setting WHERE service_type="declaration"')->result_array();

				foreach ($Pension_dec as $key => $value) {
					//   echo "7st";
					$ab = 1;
					(isset(json_decode($value['workplace'])->reminder) && $value['workplace'] != '') ? $jsnworkde =  json_decode($value['workplace'])->reminder : $jsnworkde = '';
					(isset(json_decode($value['workplace'])->text) && $value['workplace'] != '') ? $jsnworkde_t =  json_decode($value['workplace'])->text : $jsnworkde_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_declaration_of_compliance_due_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where id="' . $value['user_id'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];
					$a  = array('{crm_declaration_of_compliance_due_date}' => $value['crm_declaration_of_compliance_due_date'], '{crm_pension_add_custom_reminder}' => $value['crm_pension_add_custom_reminder']);
					//foreach ($declaration_re as $acckey => $accvalue) {
					// $email_content1    = $accvalue['message'];
					$email_content  = '<p>Declaration of Compliance Due Date Remainder</p></br><p> Compliance Due Date</p><p>' . $value['crm_declaration_of_compliance_due_date'] . '</p><p> crm pension add custom reminder </p><p>' . $value['crm_pension_add_custom_reminder'] . '</p>';
					$email_content_sms  = '  Compliance Due Date ' . $value['crm_declaration_of_compliance_due_date'] . ' crm pension add custom reminder  ' . $value['crm_pension_add_custom_reminder'] . ' ';
					if (($query['username'] != '') && $jsnworkde == 'on') {
						$cd = 1;
						if ($jsnworkde_t == 'on') {
							//  $this->send_sms_email($to_number,$email_content);
						}
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For Declaration of Compliance Due Date';
						$email_contents[]  = '<p>' . $email_content . '</p>';
						$email_content_new[]  = $email_content_sms;
						$title[] = 'Declaration of Compliance Due Date Remainder';

						$insert_content = array('user_id' => $query['id'], 'reminder_subject' => 'Declaration of Compliance Due Date Remainder', 'reminder_message' => $email_content);
						$this->Common_mdl->insert('client_reminder', $insert_content);
					}
					//  } // foreach close
				} // foreach close

				// CIS Contractor --  Start Date

				$cis_contractor = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_cis_contractor_start_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '" AND reminder_block=0')->result_array();
				//$cis_re=$this->db->query('SELECT * FROM reminder_setting WHERE service_type="cis"')->result_array();

				foreach ($cis_contractor as $key => $value) {
					// echo "8st";
					$ab = 1;
					(isset(json_decode($value['cis'])->reminder) && $value['cis'] != '') ? $jsncontra =  json_decode($value['cis'])->reminder : $jsncontra = '';
					(isset(json_decode($value['cis'])->text) && $value['cis'] != '') ? $jsncontra_t =  json_decode($value['cis'])->text : $jsncontra_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_cis_contractor_start_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where id="' . $value['user_id'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];

					$a  = array('{crm_cis_contractor_start_date}' => $value['crm_cis_contractor_start_date'], '{crm_cis_add_custom_reminder}' => $value['crm_cis_add_custom_reminder']);
					//foreach ($cis_re as $acckey => $accvalue) {

					//  $email_content1    = $accvalue['message'];
					$email_content  = '<p>C.I.S Contractor Start Date Remainder</p><br/><p> Cis Contractor start Date</p><p>' . $value['crm_cis_contractor_start_date'] . '</p><p> Cis Add Custom reminder </p><p>' . $value['crm_cis_add_custom_reminder'] . '</p>';
					$email_content_sms  = ' Cis Contractor start Date ' . $value['crm_cis_contractor_start_date'] . '  Cis Add Custom reminder  ' . $value['crm_cis_add_custom_reminder'] . '';

					if (($query['username'] != '') && $jsncontra == 'on') {

						$cd = 1;
						if ($jsncontra_t == 'on') {
							// $this->send_sms_email($to_number,$email_content);
						}
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For C.I.S Contractor Start Date';
						$email_contents[]  = '<p>' . $email_content . '</p>';
						$email_content_new[]  = $email_content_sms;
						$title[] = 'C.I.S Contractor Start Date Remainder';

						$insert_content = array('user_id' => $query['id'], 'reminder_subject' => 'CIS Contractor Date Remainder', 'reminder_message' => $email_content);
						$this->Common_mdl->insert('client_reminder', $insert_content);
					}
					// } // foreach close
				} // foreach close
				// CIS Sub Contractor --  Start Date

				$cis_subcontractor = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_cis_subcontractor_start_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '" and reminder_block=0')->result_array();
				//$cis_sub_re=$this->db->query('SELECT * FROM reminder_setting WHERE service_type="cis_sub"')->result_array();
				foreach ($cis_subcontractor as $key => $value) {
					//echo "9st"; 
					$ab = 1;
					(isset(json_decode($value['cissub'])->reminder) && $value['cissub'] != '') ? $jsncontrasub =  json_decode($value['cissub'])->reminder : $jsncontrasub = '';
					(isset(json_decode($value['cissub'])->text) && $value['cissub'] != '') ? $jsncontrasub_t =  json_decode($value['cissub'])->text : $jsncontrasub_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_cis_subcontractor_start_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where id="' . $value['user_id'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];

					$a  = array('{crm_cis_subcontractor_start_date}' => $value['crm_cis_subcontractor_start_date'], '{crm_cis_add_custom_reminder}' => $value['crm_cis_add_custom_reminder']);
					// foreach ($cis_sub_re as $acckey => $accvalue) {

					//  $email_content1    = $accvalue['message'];
					$email_content  = '<p>C.I.S Subcontractor Start Date Remainder</p><br/><p> Crm cis subcontractor start date</p><p>' . $value['crm_cis_subcontractor_start_date'] . '</p><p> Cis add custom reminder </p><p>' . $value['crm_cis_add_custom_reminder'] . '</p>';
					$email_content_sms  = ' Crm cis subcontractor start date ' . $value['crm_cis_subcontractor_start_date'] . '  Cis add custom reminder  ' . $value['crm_cis_add_custom_reminder'] . ' ';
					if (($query['username'] != '') && $jsncontrasub == 'on') {
						$cd = 1;
						if ($jsncontrasub_t == 'on') {
							// $this->send_sms_email($to_number,$email_content);
						}
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For C.I.S Subcontractor Start Date';
						$email_contents[]  = '<p>' . $email_content . '</p>';
						$email_content_new[]  = $email_content_sms;
						$title[] = 'C.I.S Subcontractor Start Date Remainder';

						$insert_content = array('user_id' => $query['id'], 'reminder_subject' => 'CIS Subcontractor Date Remainder', 'reminder_message' => $email_content);
						$this->Common_mdl->insert('client_reminder', $insert_content);
					}
					//   } // foreach close
				} // foreach close

				// P11D -- P11D Due date

				$p11d = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_next_p11d_return_due) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '" and reminder_block=0')->result_array();
				//$p11d_re=$this->db->query('SELECT * FROM reminder_setting WHERE service_type="p11d"')->result_array();
				foreach ($p11d as $key => $value) {
					//  echo "10st";
					$ab = 1;
					(isset(json_decode($value['p11d'])->reminder) && $value['p11d'] != '') ? $jsnp11d =  json_decode($value['p11d'])->reminder : $jsnp11d = '';
					(isset(json_decode($value['p11d'])->text) && $value['p11d'] != '') ? $jsnp11d_t =  json_decode($value['p11d'])->text : $jsnp11d_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_next_p11d_return_due']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where id="' . $value['user_id'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];
					$a  = array('{crm_next_p11d_return_due}' => $value['crm_next_p11d_return_due'], '{crm_p11d_add_custom_reminder}' => $value['crm_p11d_add_custom_reminder']);
					//  foreach ($p11d_re as $acckey => $accvalue) {
					//   $email_content1    = $accvalue['message'];
					$email_content  = '<p>P11D Due Date Remainder</p></br><p> Next P11d Return Date</p><p>' . $value['crm_next_p11d_return_due'] . '</p><p> p11d add custom reminder </p><p>' . $value['crm_p11d_add_custom_reminder'] . '</p>';
					$email_content_sms  = ' Next P11d Return Date ' . $value['crm_next_p11d_return_due'] . '  p11d add custom reminder  ' . $value['crm_p11d_add_custom_reminder'] . ' ';
					if (($query['username'] != '') && $jsnp11d == 'on') {
						$cd = 1;
						if ($jsnp11d_t == 'on') {
							// $this->send_sms_email($to_number,$email_content);
						}
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For P11D Due Date';
						$email_contents[]  = '<p>' . $email_content . '</p>';
						$email_content_new[]  = $email_content_sms;
						$title[] = 'P11D Due Date Remainder';

						$insert_content = array('user_id' => $query['id'], 'reminder_subject' => 'P11D Due Date Remainder', 'reminder_message' => $email_content);
						$this->Common_mdl->insert('client_reminder', $insert_content);
					}
					// } // foreach close
				} // foreach close

				// Bookkeeping --  Next Bookkeeping Date

				$Bookkeeping = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_next_booking_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '" and reminder_block=0')->result_array();
				//$bookkeep_re=$this->db->query('SELECT * FROM reminder_setting WHERE service_type="bookkeep"')->result_array();

				foreach ($Bookkeeping as $key => $value) {
					//  echo "11st";
					$ab = 1;
					(isset(json_decode($value['bookkeep'])->reminder) && $value['bookkeep'] != '') ? $jsnbookkeep =  json_decode($value['bookkeep'])->reminder : $jsnbookkeep = '';
					(isset(json_decode($value['bookkeep'])->text) && $value['bookkeep'] != '') ? $jsnbookkeep_t =  json_decode($value['bookkeep'])->text : $jsnbookkeep_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_next_booking_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;

					$query = $this->db->query('select * from user where id="' . $value['user_id'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];

					$a  = array('{crm_next_booking_date}' => $value['crm_next_booking_date'], '{crm_bookkeep_add_custom_reminder}' => $value['crm_bookkeep_add_custom_reminder']);
					//foreach ($bookkeep_re as $acckey => $accvalue) {

					//  $email_content1    = $accvalue['message'];
					$email_content  = '<p>Next Bookkeeping Date Remainder</p><br/><p> Next Booking  Date</p><p>' . $value['crm_next_booking_date'] . '</p><p>bookkeep add custom reminder </p><p>' . $value['crm_bookkeep_add_custom_reminder'] . '</p>';
					$email_content_sms  = 'Next Booking  Date ' . $value['crm_next_booking_date'] . ' bookkeep add custom reminder  ' . $value['crm_bookkeep_add_custom_reminder'] . ' ';

					if (($query['username'] != '') && $jsnbookkeep == 'on') {
						$cd = 1;
						if ($jsnbookkeep_t == 'on') {
							//$this->send_sms_email($to_number,$email_content);
						}
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For Next Bookkeeping Date';
						$email_contents[]  = '<p>' . $email_content . '</p>';
						$email_content_new[]  = $email_content_sms;
						$title[] = 'Next Bookkeeping Date Remainder';
						$insert_content = array('user_id' => $query['id'], 'reminder_subject' => 'Bookkeeping Due Date Remainder', 'reminder_message' => $email_content);
						$this->Common_mdl->insert('client_reminder', $insert_content);
					}
					//} // foreach close
				} // foreach close

				// Management Accounts --  Next Management Accounts Due date

				$Management = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_next_manage_acc_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '" and reminder_block=0')->result_array();
				//$management_re=$this->db->query('SELECT * FROM reminder_setting WHERE service_type="management"')->result_array();

				foreach ($Management as $key => $value) {
					//   echo "12st";
					$ab = 1;
					(isset(json_decode($value['management'])->reminder) && $value['management'] != '') ? $jsnmanagement =  json_decode($value['management'])->reminder : $jsnmanagement = '';
					(isset(json_decode($value['management'])->text) && $value['management'] != '') ? $jsnmanagement_t =  json_decode($value['management'])->text : $jsnmanagement_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_next_manage_acc_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where id="' . $value['user_id'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];
					$a  = array('{crm_next_manage_acc_date}' => $value['crm_next_manage_acc_date'], '{crm_manage_add_custom_reminder}' => $value['crm_manage_add_custom_reminder']);
					// foreach ($management_re as $acckey => $accvalue) {
					//$email_content1    = $accvalue['message'];
					$email_content  = '<p>Next Management Accounts Due Date Remainder</p><br/><p>Nex Manage Account date</p><p>' . $value['crm_next_manage_acc_date'] . '</p><p> crm Manage add custom reminder </p><p>' . $value['crm_manage_add_custom_reminder'] . '</p>';
					$email_content_sms  = 'Nex Manage Account date' . $value['crm_next_manage_acc_date'] . ' crm Manage add custom reminder ' . $value['crm_manage_add_custom_reminder'] . '';
					if (($query['username'] != '') && $jsnmanagement == 'on') {
						$cd = 1;
						if ($jsnmanagement_t == 'on') {
							//$this->send_sms_email($to_number,$email_content);
						}
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For Next Management Accounts Due Date';
						$email_contents[]  = '<p>' . $email_content . '</p>';
						$email_content_new[]  = $email_content_sms;
						$title[] = 'Next Management Accounts Due Date Remainder';

						$insert_content = array('user_id' => $query['id'], 'reminder_subject' => 'Management Accounts Due Date Remainder', 'reminder_message' => $email_content);
						$this->Common_mdl->insert('client_reminder', $insert_content);
					}
					//   } // foreach close
				} // foreach close
				// VAT Quaters --  VAT Quarters

				$vat_quarters = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND user_id="' . $id . '" and reminder_block=0')->result_array();
				//$vat_re=$this->db->query('SELECT * FROM reminder_setting WHERE service_type="vat"')->result_array();
				foreach ($vat_quarters as $vat_quarterskey => $vat_quartersval) {

					(isset(json_decode($vat_quartersval['vat'])->reminder) && $vat_quartersval['vat'] != '') ? $vatmanagement =  json_decode($vat_quartersval['vat'])->reminder : $vatmanagement = '';
					(isset(json_decode($vat_quartersval['vat'])->text) && $vat_quartersval['vat'] != '') ? $vatmanagement_t =  json_decode($vat_quartersval['vat'])->text : $vatmanagement_t = '';

					if ($vatmanagement == 'on') {
						// echo "13st";
						$ab = 1;
						$crm_vat_quarters = $vat_quartersval['crm_vat_quarters'];
						if ($crm_vat_quarters != '0') {
							if ($crm_vat_quarters == 'Annual End Jan') {
								$month = '01';
							} elseif ($crm_vat_quarters == 'Annual End Feb') {
								$month = '02';
							} elseif ($crm_vat_quarters == 'Annual End Mar') {
								$month = '03';
							} elseif ($crm_vat_quarters == 'Annual End Apr') {
								$month = '04';
							} elseif ($crm_vat_quarters == 'Annual End May') {
								$month = '05';
							} elseif ($crm_vat_quarters == 'Annual End Jun') {
								$month = '06';
							} elseif ($crm_vat_quarters == 'Annual End Jul') {
								$month = '07';
							} elseif ($crm_vat_quarters == 'Annual End Aug') {
								$month = '08';
							} elseif ($crm_vat_quarters == 'Annual End Sep') {
								$month = '09';
							} elseif ($crm_vat_quarters == 'Annual End Oct') {
								$month = '10';
							} elseif ($crm_vat_quarters == 'Annual End Nov') {
								$month = '11';
							} elseif ($crm_vat_quarters == 'Annual End Dec') {
								$month = '12';
							} elseif ($crm_vat_quarters == 'Jan/Apr/Jul/Oct') {
								$month = '01_04_07_10';
							} elseif ($crm_vat_quarters == 'Feb/May/Aug/Nov') {
								$month = '02_05_08_11';
							} elseif ($crm_vat_quarters == 'Mar/Jun/Sep/Dec') {
								$month = '03_06_09_12';
							} elseif ($crm_vat_quarters == 'Monthly') {
								$month = '01_02_03_04_05_06_07_08_09_10_11_12';
							}
							$array = explode('_', $month);
							foreach ($array as $key => $value) {
								$y = date('Y');
								$e_date = date('01-' . $value . '-' . $y);
								//echo $e_date;
								$c_date = date('d-m-Y');
								//if($c_date==$e_date)
								//{
								$query = $this->db->query('select * from user where id="' . $value['user_id'] . '"')->row_array();

								$to_number = $query['crm_phone_number'];

								$a  = array('{crm_vat_quarters}' => $crm_vat_quarters, '{crm_vat_add_custom_reminder}' => $vat_quartersval['crm_vat_add_custom_reminder']);
								//foreach ($vat_re as $acckey => $accvalue) {

								//$email_content1    = $accvalue['message'];
								$email_content  = '<p>Next VAT Quaters Due Date Remainder</p></br><p> Vat quarters</p><p>' . $crm_vat_quarters . '</p><p> vat custom reminder </p><p>' . $vat_quartersval['crm_vat_add_custom_reminder'] . '</p>';
								$email_content_sms  = 'Vat quarters' . $crm_vat_quarters . ' vat custom reminder ' . $vat_quartersval['crm_vat_add_custom_reminder'] . '';

								if (($query['username'] != '') && $vatmanagement == 'on') {
									$cd = 1;
									if ($vatmanagement_t == 'on') {
										//  $this->send_sms_email($to_number,$email_content);
									}
									$data2['username'] = $query['username'];
									$user_id = $query['id'];
									$password = $query['password'];
									$encrypt_id = base64_encode($user_id);
									$email_subject  = 'Email Remainder For VAT Quaters Due Date';
									$email_contents[]  = '<p>' . $email_content . '</p>';
									$email_content_new[]  = $email_content_sms;
									$title[] = 'Next VAT Quaters Due Date Remainder';
									$insert_content = array('user_id' => $query['id'], 'reminder_subject' => 'VAT Quaters Due Date Remainder', 'reminder_message' => $email_content);
									$this->Common_mdl->insert('client_reminder', $insert_content);
								}

								// }//close foreach
								//}
							}
						}
					}
				}
				// echo $ab;

				if ($cd == 1) {
					if ($ab == 1) {
						$proposal_content = $this->Common_mdl->select_record('proposal_proposaltemplate', 'title', 'services_mail');
						$sender_details = $this->db->query('select company_name,crm_name from admin_setting where user_id=' . $_SESSION['id'] . '')->row_array();
						$sender_company = $sender_details['company_name'];
						$sender_name = $sender_details['crm_name'];
						$body1 = $proposal_content['body'];
						$subject = $proposal_content['subject'];
						$link = '<a href=' . base_url() . ' style="display: inline-block;text-align: center;color: #00aeef;text-decoration: none;font-family:;background: #00aeef;color: #fff;font-weight: 600;padding: 5px 10px;border-radius: 3px;">Click Here</a>';
						$a1  =   array(
							':: Client Name::' => $query['username'],
							':: Service Name:: ' => implode(',', $title),
							':: service Deadlines::' => implode(',', $email_contents),
							':: Service Link::' => $link,
							':: Sender Company::' => $sender_company
						);
						$mail_data['email_contents']  =   strtr($body1, $a1);
						$email_subject  =   strtr($subject, $a1);
						$user_data = $this->db->query("select * from user where id=" . $id . " ")->row_array();

						$contact_data = $this->Common_mdl->GetAllWithWheretwo('client_contacts', 'client_id', $id, 'make_primary', 1);

						$body = $this->load->view('service_remainder_email.php', $mail_data, TRUE);
						$this->load->library('email');
						$this->email->set_mailtype("html");
						$this->email->from('info@remindoo.org');
						$this->email->to($contact_data[0]['main_email']);
						$this->email->subject(preg_replace('/ {2,}/', ' ', str_replace('&nbsp;', ' ', strip_tags($email_subject))));
						$this->email->message($body);
						$send = $this->email->send();
						$da['user_id'] = 2;
						$da['crm_name'] = 'test';
						$da['crm_currency'] = '₨';
						$da['crm_profile_image'] = '';
						$da['logo_image'] = '';
						$da['created_date'] = time();
						$this->Common_mdl->insert('admin_setting', $da);
						$emailcontent = array_values(array_filter($email_content_new));
						$title = array_values(array_filter($title));
						$first_key = key($title);
						$cnt = 'Hi ' . $query['username'] . ',';
						$a = $title[$first_key];

						for ($i = 0; $i < count($emailcontent); $i++) {
							$b = $title[$i];

							if ($a == $b) {
								if ($i == '0') {
									$cnt .= $title[$i];
								}
							} else {
								$cnt  .= $title[$i];
								$a = $title[$i];
							}
							$cnt .= $emailcontent[$i];
						}
						$this->send_sms_email($to_number, $cnt);
					}
				}
			} //close if
		}
	} // function close


	public function delete_proposal($id)
	{
		die($id);
		$sql = $this->db->query("DELETE FROM proposals WHERE id in ($id)");
		redirect('client_infomation');
	}
	public function insert_remindertask()
	{
		$user_id = $_POST['user_id'];
		$this->Task_invoice_model->for_sample_client_task($_POST['user_id']);
		$this->Task_invoice_model->for_default_cus_reminder($_POST['user_id']); //firm settings
		echo true;
	}

	/** for newly added reminder email check 09-07-2018 **/
	public function email_remindersms_rechange($id, $re_conf_statement = false, $re_accounts = false, $re_company_tax_return = false, $re_personal_tax_return = false, $re_payroll = false, $re_workplace = false, $re_vat = false, $re_cis = false, $re_cissub = false, $re_p11d = false, $re_bookkeep = false, $re_management = false, $re_investgate = false, $re_registered = false, $re_taxadvice = false, $re_taxinvest = false)
	{
		//echo "zero";

		//echo $_POST['user_id']."-".$re_conf_statement."-".$re_accounts."-".$re_company_tax_return."-".$re_personal_tax_return."-".$re_payroll."-".$re_workplace."-".$re_vat."-".$re_cis."-".$re_cissub."-".$re_p11d."-".$re_bookkeep."-".$re_management."-".$re_investgate."-".$re_registered."-".$re_taxadvice."-".$re_taxinvest; 
		$ab = 0;
		$cd = 0;
		$admin = $this->Common_mdl->GetAllWithWhere('admin_setting', 'user_id', '1');
		$curr_time = date("h:i A", time());
		if ($admin[0]['client_reminder_time'] == '10:30 PM') {
			// accounts reminders
			$title[] = '';
			$email_contents[] = '';
			$user_email_id = $this->db->query('select * from client where user_id=' . $id)->row_array();
			$records = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_ch_accounts_next_due!="0" AND crm_email!="0" AND user_id="' . $id . '" AND DATE(crm_ch_accounts_next_due) > DATE_FORMAT(NOW(),"%Y-%m-%d") and reminder_block=0')->result_array();
			//$accounts=$this->db->query('SELECT * FROM reminder_setting WHERE service_type="accounts"')->result_array();
			if ($re_accounts != '') {
				foreach ($records as $key => $value) {
					// echo "1st";
					$ab = 1;
					(isset(json_decode($value['accounts'])->reminder) && $value['accounts'] != '') ? $jsnaccounts =  json_decode($value['accounts'])->reminder : $jsnaccounts = '';
					(isset(json_decode($value['accounts'])->text) && $value['accounts'] != '') ? $jsnaccounts_t =  json_decode($value['accounts'])->text : $jsnaccounts_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_ch_accounts_next_due']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where crm_email_id="' . $value['crm_email'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];
					$a  = array('{crm_ch_yearend}' => $value['crm_ch_yearend'], '{crm_ch_accounts_next_due}' => $value['crm_ch_accounts_next_due'], '{crm_accounts_custom_reminder}' => $value['crm_accounts_custom_reminder']);
					// foreach ($accounts as $acckey => $accvalue) {
					// $email_content1    = $accvalue['message'];
					$email_content  = '<p> Next Statement Date</p><p>' . $value['crm_ch_yearend'] . '</p><p> Due By</p><p>' . $value['crm_ch_accounts_next_due'] . '</p>';
					$email_content_sms  = ' Next Statement Date ' . $value['crm_ch_yearend'] . '  Due By ' . $value['crm_ch_accounts_next_due'] . ' ';
					if (($query['username'] != '' && $jsnaccounts == 'on')) {
						$cd = 1;
						if ($jsnaccounts_t == 'on') {
							// $this->send_sms_email($to_number,$email_content);
						}
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Reminder Notification';
						$email_contents[]  = '<p>' . $email_content . '</p>';
						$email_content_new[]  = $email_content_sms;
						$title[] = 'Accounts Statement Remainder';
					}
					//print_r($email_content_new);
					// }//foreach close
				} // foreach close
			}
			// confirmation reminders
			$confirmation = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_confirmation_statement_due_date!="0" AND crm_email!="0" AND DATE(crm_confirmation_statement_due_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '" and reminder_block=0')->result_array();
			//echo $this->db->last_query();
			//$confirmation_re=$this->db->query('SELECT * FROM reminder_setting WHERE service_type="confirmation"')->result_array();
			if ($re_conf_statement != '') {
				foreach ($confirmation as $key => $value) {
					//  echo "2st";
					$ab = 1;
					(isset(json_decode($value['conf_statement'])->reminder) && $value['conf_statement'] != '') ? $jsnconf =  json_decode($value['conf_statement'])->reminder : $jsnconf = '';
					(isset(json_decode($value['conf_statement'])->text) && $value['conf_statement'] != '') ? $jsnconf_t =  json_decode($value['conf_statement'])->text : $jsnconf_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_confirmation_statement_due_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where crm_email_id="' . $value['crm_email'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];
					$a  = array('{crm_confirmation_statement_date}' => $value['crm_confirmation_statement_date'], '{crm_confirmation_statement_due_date}' => $value['crm_confirmation_statement_due_date'], '{crm_confirmation_add_custom_reminder}' => $value['crm_confirmation_add_custom_reminder']);
					// foreach ($confirmation_re as $acckey => $accvalue) {
					// $email_content1    = $accvalue['message'];
					$email_content  = '<p> Next Statement Date</p><p>' . $value['crm_confirmation_statement_date'] . '</p><p> Due By</p><p>' . $value['crm_confirmation_statement_due_date'] . '</p>';
					$email_content_sms  = ' Next Statement Date ' . $value['crm_confirmation_statement_date'] . '  Due By ' . $value['crm_confirmation_statement_due_date'] . ' ';
					if (($query['username'] != '' && $jsnconf == 'on')) {
						$cd = 1;
						if ($jsnconf_t == 'on') {
							// $this->send_sms_email($to_number,$email_content);
						}
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For Confirmation';
						$email_contents[]  = '<p>' . $email_content . '</p>';
						$email_content_new[]  = $email_content_sms;
						$title[] = 'Confirmation Statement Remainder';
						//email template
					}

					//  }// foreach close
				} // foreach close
			}
			// Tax Return  -- Accounts Due Date - HMRC


			$tax_return = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_accounts_due_date_hmrc) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '" and reminder_block=0')->result_array();
			//$taxreturn_re=$this->db->query('SELECT * FROM reminder_setting WHERE service_type="tax_return"')->result_array();
			if ($re_company_tax_return != '') {


				foreach ($tax_return as $key => $value) {
					//  echo "3st";
					$ab = 1;
					(isset(json_decode($value['company_tax_return'])->reminder) && $value['company_tax_return'] != '') ? $jsnAcc =  json_decode($value['company_tax_return'])->reminder : $jsnAcc = '';
					(isset(json_decode($value['company_tax_return'])->text) && $value['company_tax_return'] != '') ? $jsnAcc_t =  json_decode($value['company_tax_return'])->text : $jsnAcc_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_accounts_due_date_hmrc']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');

					$old_record[] = $value;

					$query = $this->db->query('select * from user where crm_email_id="' . $value['crm_email'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];

					$a  = array('{crm_ch_yearend}' => $value['crm_ch_yearend'], '{crm_accounts_due_date_hmrc}' => $value['crm_accounts_due_date_hmrc'], '{crm_accounts_custom_reminder}' => $value['crm_accounts_custom_reminder']);
					//foreach ($taxreturn_re as $acckey => $accvalue) {

					//  $email_content1    = $accvalue['message'];
					$email_content  = '<p> Next Statement Date</p><p>' . $value['crm_ch_yearend'] . '</p><p> Due By</p><p>' . $value['crm_accounts_due_date_hmrc'] . '</p>';
					$email_content_sms  = '  Next Statement Date ' . $value['crm_ch_yearend'] . '  Due By' . $value['crm_accounts_due_date_hmrc'] . ' ';

					if ($query['username'] != '' && $jsnAcc == 'on') {
						$cd = 1;
						if ($jsnAcc_t) {
							// $this->send_sms_email($to_number,$email_content);
						}
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For Tax Return';
						$email_contents[]  = '<p>' . $email_content . '</p>';
						$email_content_new[]  = $email_content_sms;
						$title[] = 'Accounts Tax Return Remainder';
					}
					// } // foreach close
				} // foreach close
			}

			// Personal Tax Return  -- Tax Return Date
			$personal_tax_return = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_personal_tax_return_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '" and reminder_block=0')->result_array();
			//$personaltax_re=$this->db->query('SELECT * FROM reminder_setting WHERE service_type="personal_tax"')->result_array();
			if ($re_personal_tax_return != '') {
				foreach ($personal_tax_return as $key => $value) {
					// echo "4st";
					$ab = 1;
					(isset(json_decode($value['personal_tax_return'])->reminder) && $value['personal_tax_return'] != '') ? $jsnpertax =  json_decode($value['personal_tax_return'])->reminder : $jsnpertax = '';
					(isset(json_decode($value['personal_tax_return'])->text) && $value['personal_tax_return'] != '') ? $jsnpertax_t =  json_decode($value['personal_tax_return'])->text : $jsnpertax_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_personal_tax_return_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where crm_email_id="' . $value['crm_email'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];
					$a  = array('{crm_personal_tax_return_date}' => $value['crm_personal_tax_return_date'], '{crm_personal_custom_reminder}' => $value['crm_personal_custom_reminder']);
					// foreach ($personaltax_re as $acckey => $accvalue) {
					//  $email_content1    = $accvalue['message'];
					$email_content  = '<p> Next Statement Date</p><p>' . $value['crm_personal_tax_return_date'] . '</p><p> Personal Custom Reminder</p><p>' . $value['crm_personal_custom_reminder'] . '</p>';
					$email_content_sms  = '  Next Statement Date ' . $value['crm_personal_tax_return_date'] . '  Personal Custom Reminder ' . $value['crm_personal_custom_reminder'] . ' ';
					if (($query['username'] != '') && $jsnpertax == 'on') {
						$cd = 1;
						if ($jsnpertax_t == 'on') {
							// $this->send_sms_email($to_number,$email_content);
						}
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For Personal Tax Return';
						$email_contents[]  = '<p>' . $email_content . '</p>';
						$email_content_new[]  = $email_content_sms;
						$title[] = 'Personal Tax Return Remainder';
					}
					//  } // foreach close
				} // foreach close
			}
			// payroll --Payroll Run Date

			$Payroll = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_payroll_run_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '" and reminder_block=0')->result_array();
			$payroll_re = $this->db->query('SELECT * FROM reminder_setting WHERE service_type="payroll"')->result_array();
			if ($re_payroll != '') {
				foreach ($Payroll as $key => $value) {
					//echo "5st";
					$ab = 1;
					(isset(json_decode($value['payroll'])->reminder) && $value['payroll'] != '') ? $jsnpay =  json_decode($value['payroll'])->reminder : $jsnpay = '';
					(isset(json_decode($value['payroll'])->text) && $value['payroll'] != '') ? $jsnpay_t =  json_decode($value['payroll'])->text : $jsnpay_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_payroll_run_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where crm_email_id="' . $value['crm_email'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];
					$a  = array('{crm_payroll_run_date}' => $value['crm_payroll_run_date'], '{crm_payroll_add_custom_reminder}' => $value['crm_payroll_add_custom_reminder']);
					//   foreach ($payroll_re as $acckey => $accvalue) {
					//  $email_content1    = $accvalue['message'];
					$email_content  = '<p> Payroll Run Date</p><p>' . $value['crm_payroll_run_date'] . '</p><p>Payroll Custom Reminder date</p><p>' . $value['crm_payroll_add_custom_reminder'] . '</p>';
					$email_content_sms  = '  Payroll Run Date ' . $value['crm_payroll_run_date'] . ' Payroll Custom Reminder date ' . $value['crm_payroll_add_custom_reminder'] . ' ';
					if (($query['username'] != '') && $jsnpay == 'on') {
						$cd = 1;
						if ($jsnpay_t == 'on') {
							//  $this->send_sms_email($to_number,$email_content);
						}
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For Payroll Run Date';
						$email_contents[]  = '<p>' . $email_content . '</p>';
						$email_content_new[]  = $email_content_sms;
						$title[] = 'Payroll Run Date Remainder';
					}
					// } // foreach close
				} // foreach close
			}
			//WorkPlace Pension-AE -- Pension Submission Due date
			$Pension_ae = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_pension_subm_due_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '" and reminder_block=0')->result_array();
			//$pension_re=$this->db->query('SELECT * FROM reminder_setting WHERE service_type="pension"')->result_array();
			if ($re_workplace != '') {
				foreach ($Pension_ae as $key => $value) {
					// echo "6st"; 
					$ab = 1;
					(isset(json_decode($value['workplace'])->reminder) && $value['workplace'] != '') ? $jsnwork =  json_decode($value['workplace'])->reminder : $jsnwork = '';
					(isset(json_decode($value['workplace'])->text) && $value['workplace'] != '') ? $jsnwork_t =  json_decode($value['workplace'])->text : $jsnwork_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_pension_subm_due_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where crm_email_id="' . $value['crm_email'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];
					$a  = array('{crm_pension_subm_due_date}' => $value['crm_pension_subm_due_date'], '{crm_pension_add_custom_reminder}' => $value['crm_pension_add_custom_reminder']);
					//foreach ($pension_re as $acckey => $accvalue) {
					//  $email_content1    = $accvalue['message'];
					$email_content  = '<p> pension Submission Due Date</p><p>' . $value['crm_pension_subm_due_date'] . '</p><p> pension Add Custom Reminder </p><p>' . $value['crm_pension_add_custom_reminder'] . '</p>';
					$email_content_sms  = '  pension Submission Due Date ' . $value['crm_pension_subm_due_date'] . '  pension Add Custom Reminder  ' . $value['crm_pension_add_custom_reminder'] . ' ';
					//'<p>Payroll Run Date';
					if (($query['username'] != '') && $jsnwork == 'on') {
						$cd = 1;
						if ($jsnwork_t = 'on') {
							// $this->send_sms_email($to_number,$email_content);
						}
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For Pension Submission Due date';
						$email_contents[]  = '<p>' . $email_content . '</p>';
						$email_content_new[]  = $email_content_sms;
						$title[] = 'Pension Submission Due Date Remainder';
					}
					//   } // foreach close
				} // foreach close
			}
			//WorkPlace Pension-AE --Declaration of Compliance Due
			$Pension_dec = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_declaration_of_compliance_due_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '" and reminder_block=0')->result_array();
			//$declaration_re=$this->db->query('SELECT * FROM reminder_setting WHERE service_type="declaration"')->result_array();
			if ($re_workplace != '') {
				foreach ($Pension_dec as $key => $value) {
					//   echo "7st";
					$ab = 1;
					(isset(json_decode($value['workplace'])->reminder) && $value['workplace'] != '') ? $jsnworkde =  json_decode($value['workplace'])->reminder : $jsnworkde = '';
					(isset(json_decode($value['workplace'])->text) && $value['workplace'] != '') ? $jsnworkde_t =  json_decode($value['workplace'])->text : $jsnworkde_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_declaration_of_compliance_due_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where crm_email_id="' . $value['crm_email'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];
					$a  = array('{crm_declaration_of_compliance_due_date}' => $value['crm_declaration_of_compliance_due_date'], '{crm_pension_add_custom_reminder}' => $value['crm_pension_add_custom_reminder']);
					//foreach ($declaration_re as $acckey => $accvalue) {
					// $email_content1    = $accvalue['message'];
					$email_content  = '<p> Compliance Due Date</p><p>' . $value['crm_declaration_of_compliance_due_date'] . '</p><p> crm pension add custom reminder </p><p>' . $value['crm_pension_add_custom_reminder'] . '</p>';
					$email_content_sms  = '  Compliance Due Date ' . $value['crm_declaration_of_compliance_due_date'] . ' crm pension add custom reminder  ' . $value['crm_pension_add_custom_reminder'] . ' ';
					if (($query['username'] != '') && $jsnworkde == 'on') {
						$cd = 1;
						if ($jsnworkde_t == 'on') {
							//  $this->send_sms_email($to_number,$email_content);
						}
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For Declaration of Compliance Due Date';
						$email_contents[]  = '<p>' . $email_content . '</p>';
						$email_content_new[]  = $email_content_sms;
						$title[] = 'Declaration of Compliance Due Date Remainder';
					}
					//  } // foreach close
				} // foreach close
			}
			// CIS Contractor --  Start Date

			$cis_contractor = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_cis_contractor_start_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '" and reminder_block=0')->result_array();
			//$cis_re=$this->db->query('SELECT * FROM reminder_setting WHERE service_type="cis"')->result_array();
			if ($re_cis != '') {
				foreach ($cis_contractor as $key => $value) {
					// echo "8st";
					$ab = 1;
					(isset(json_decode($value['cis'])->reminder) && $value['cis'] != '') ? $jsncontra =  json_decode($value['cis'])->reminder : $jsncontra = '';
					(isset(json_decode($value['cis'])->text) && $value['cis'] != '') ? $jsncontra_t =  json_decode($value['cis'])->text : $jsncontra_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_cis_contractor_start_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where crm_email_id="' . $value['crm_email'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];

					$a  = array('{crm_cis_contractor_start_date}' => $value['crm_cis_contractor_start_date'], '{crm_cis_add_custom_reminder}' => $value['crm_cis_add_custom_reminder']);
					//foreach ($cis_re as $acckey => $accvalue) {

					//  $email_content1    = $accvalue['message'];
					$email_content  = '<p> Cis Contractor start Date</p><p>' . $value['crm_cis_contractor_start_date'] . '</p><p> Cis Add Custom reminder </p><p>' . $value['crm_cis_add_custom_reminder'] . '</p>';
					$email_content_sms  = ' Cis Contractor start Date ' . $value['crm_cis_contractor_start_date'] . '  Cis Add Custom reminder  ' . $value['crm_cis_add_custom_reminder'] . '';

					if (($query['username'] != '') && $jsncontra == 'on') {

						$cd = 1;
						if ($jsncontra_t == 'on') {
							// $this->send_sms_email($to_number,$email_content);
						}
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For C.I.S Contractor Start Date';
						$email_contents[]  = '<p>' . $email_content . '</p>';
						$email_content_new[]  = $email_content_sms;
						$title[] = 'C.I.S Contractor Start Date Remainder';
					}
					// } // foreach close
				} // foreach close
			}
			// CIS Sub Contractor --  Start Date

			$cis_subcontractor = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_cis_subcontractor_start_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '" and reminder_block=0')->result_array();
			//$cis_sub_re=$this->db->query('SELECT * FROM reminder_setting WHERE service_type="cis_sub"')->result_array();
			if ($re_cissub != '') {
				foreach ($cis_subcontractor as $key => $value) {
					//echo "9st"; 
					$ab = 1;
					(isset(json_decode($value['cissub'])->reminder) && $value['cissub'] != '') ? $jsncontrasub =  json_decode($value['cissub'])->reminder : $jsncontrasub = '';
					(isset(json_decode($value['cissub'])->text) && $value['cissub'] != '') ? $jsncontrasub_t =  json_decode($value['cissub'])->text : $jsncontrasub_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_cis_subcontractor_start_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where crm_email_id="' . $value['crm_email'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];

					$a  = array('{crm_cis_subcontractor_start_date}' => $value['crm_cis_subcontractor_start_date'], '{crm_cis_add_custom_reminder}' => $value['crm_cis_add_custom_reminder']);
					// foreach ($cis_sub_re as $acckey => $accvalue) {

					//  $email_content1    = $accvalue['message'];
					$email_content  = '<p> Crm cis subcontractor start date</p><p>' . $value['crm_cis_subcontractor_start_date'] . '</p><p> Cis add custom reminder </p><p>' . $value['crm_cis_add_custom_reminder'] . '</p>';
					$email_content_sms  = ' Crm cis subcontractor start date ' . $value['crm_cis_subcontractor_start_date'] . '  Cis add custom reminder  ' . $value['crm_cis_add_custom_reminder'] . ' ';
					if (($query['username'] != '') && $jsncontrasub == 'on') {
						$cd = 1;
						if ($jsncontrasub_t == 'on') {
							// $this->send_sms_email($to_number,$email_content);
						}
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For C.I.S Subcontractor Start Date';
						$email_contents[]  = '<p>' . $email_content . '</p>';
						$email_content_new[]  = $email_content_sms;
						$title[] = 'C.I.S Subcontractor Start Date Remainder';
					}
					//   } // foreach close
				} // foreach close
			}
			// P11D -- P11D Due date

			$p11d = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_next_p11d_return_due) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '" and reminder_block=0')->result_array();

			if ($re_p11d != '') {
				foreach ($p11d as $key => $value) {
					$ab = 1;
					(isset(json_decode($value['p11d'])->reminder) && $value['p11d'] != '') ? $jsnp11d =  json_decode($value['p11d'])->reminder : $jsnp11d = '';
					(isset(json_decode($value['p11d'])->text) && $value['p11d'] != '') ? $jsnp11d_t =  json_decode($value['p11d'])->text : $jsnp11d_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_next_p11d_return_due']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where crm_email_id="' . $value['crm_email'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];
					$a  = array('{crm_next_p11d_return_due}' => $value['crm_next_p11d_return_due'], '{crm_p11d_add_custom_reminder}' => $value['crm_p11d_add_custom_reminder']);
					$email_content  = '<p> Next P11d Return Date</p><p>' . $value['crm_next_p11d_return_due'] . '</p><p> p11d add custom reminder </p><p>' . $value['crm_p11d_add_custom_reminder'] . '</p>';
					$email_content_sms  = ' Next P11d Return Date ' . $value['crm_next_p11d_return_due'] . '  p11d add custom reminder  ' . $value['crm_p11d_add_custom_reminder'] . ' ';
					if (($query['username'] != '') && $jsnp11d == 'on') {
						$cd = 1;
						if ($jsnp11d_t == 'on') {
							// $this->send_sms_email($to_number,$email_content);
						}
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For P11D Due Date';
						$email_contents[]  = '<p>' . $email_content . '</p>';
						$email_content_new[]  = $email_content_sms;
						$title[] = 'P11D Due Date Remainder';
					}
				} // foreach close
			}

			/*********************************************/
			// Investigation Insurance 
			$confirmation = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_insurance_renew_date!="0" AND crm_email!="0" AND DATE(crm_insurance_renew_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '" and reminder_block=0')->result_array();
			if ($re_investgate != '') {
				foreach ($confirmation as $key => $value) {
					$ab = 1;
					(isset(json_decode($value['investgate'])->reminder) && $value['investgate'] != '') ? $jsnconf =  json_decode($value['investgate'])->reminder : $jsnconf = '';
					(isset(json_decode($value['investgate'])->text) && $value['investgate'] != '') ? $jsnconf_t =  json_decode($value['investgate'])->text : $jsnconf_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_insurance_renew_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where crm_email_id="' . $value['crm_email'] . '"')->row_array();

					$email_content  = '<p> Next Statement Date</p><p>' . $value['crm_invesitgation_insurance'] . '</p><p> Due By</p><p>' . $value['crm_insurance_renew_date'] . '</p>';
					$email_content_sms  = ' Next Statement Date ' . $value['crm_invesitgation_insurance'] . '  Due By ' . $value['crm_insurance_renew_date'] . ' ';
					if (($query['username'] != '' && $jsnconf == 'on')) {
						$cd = 1;
						if ($jsnconf_t == 'on') {
							// $this->send_sms_email($to_number,$email_content);
						}
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For Investigation Insurance';
						$email_contents[]  = '<p>' . $email_content . '</p>';
						$email_content_new[]  = $email_content_sms;
						$title[] = 'Investigation Insurance Remainder';
						//email template
					}
				} // foreach close
			}

			// Registered Address
			$confirmation = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_registered_renew_date!="0" AND crm_email!="0" AND DATE(crm_registered_renew_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '" and reminder_block=0')->result_array();

			if ($re_registered != '') {
				foreach ($confirmation as $key => $value) {
					//  echo "2st";
					$ab = 1;
					(isset(json_decode($value['registered'])->reminder) && $value['registered'] != '') ? $jsnconf =  json_decode($value['registered'])->reminder : $jsnconf = '';
					(isset(json_decode($value['registered'])->text) && $value['registered'] != '') ? $jsnconf_t =  json_decode($value['registered'])->text : $jsnconf_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_registered_renew_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where crm_email_id="' . $value['crm_email'] . '"')->row_array();

					$email_content  = '<p> Next Statement Date</p><p>' . $value['crm_registered_start_date'] . '</p><p> Due By</p><p>' . $value['crm_registered_renew_date'] . '</p>';
					$email_content_sms  = ' Next Statement Date ' . $value['crm_registered_start_date'] . '  Due By ' . $value['crm_registered_renew_date'] . ' ';
					if (($query['username'] != '' && $jsnconf == 'on')) {
						$cd = 1;
						if ($jsnconf_t == 'on') {
							// $this->send_sms_email($to_number,$email_content);
						}
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For Registered Address';
						$email_contents[]  = '<p>' . $email_content . '</p>';
						$email_content_new[]  = $email_content_sms;
						$title[] = 'Registered Address Remainder';
						//email template
					}
				} // foreach close
			}

			// Tax Advice
			$confirmation = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_investigation_end_date!="0" AND crm_email!="0" AND DATE(crm_investigation_end_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '" and reminder_block=0')->result_array();

			if ($re_registered != '') {
				foreach ($confirmation as $key => $value) {
					//  echo "2st";
					$ab = 1;
					(isset(json_decode($value['taxadvice'])->reminder) && $value['taxadvice'] != '') ? $jsnconf =  json_decode($value['taxadvice'])->reminder : $jsnconf = '';
					(isset(json_decode($value['taxadvice'])->text) && $value['taxadvice'] != '') ? $jsnconf_t =  json_decode($value['taxadvice'])->text : $jsnconf_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_investigation_end_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where crm_email_id="' . $value['crm_email'] . '"')->row_array();

					$email_content  = '<p> Next Statement Date</p><p>' . $value['crm_investigation_start_date'] . '</p><p> Due By</p><p>' . $value['crm_investigation_end_date'] . '</p>';
					$email_content_sms  = ' Next Statement Date ' . $value['crm_investigation_start_date'] . '  Due By ' . $value['crm_investigation_end_date'] . ' ';
					if (($query['username'] != '' && $jsnconf == 'on')) {
						$cd = 1;
						if ($jsnconf_t == 'on') {
							// $this->send_sms_email($to_number,$email_content);
						}
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For Tax Advice';
						$email_contents[]  = '<p>' . $email_content . '</p>';
						$email_content_new[]  = $email_content_sms;
						$title[] = 'Tax Advice Remainder';
						//email template
					}
				} // foreach close
			}

			// Tax Investigation
			$confirmation = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_investigation_end_date!="0" AND crm_email!="0" AND DATE(crm_investigation_end_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '" and reminder_block=0')->result_array();
			if ($re_taxinvest != '') {
				foreach ($confirmation as $key => $value) {
					$ab = 1;
					(isset(json_decode($value['taxinvest'])->reminder) && $value['taxinvest'] != '') ? $jsnconf =  json_decode($value['taxinvest'])->reminder : $jsnconf = '';
					(isset(json_decode($value['taxinvest'])->text) && $value['taxinvest'] != '') ? $jsnconf_t =  json_decode($value['taxinvest'])->text : $jsnconf_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_investigation_end_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where crm_email_id="' . $value['crm_email'] . '"')->row_array();

					$email_content  = '<p> Next Statement Date</p><p>' . $value['crm_investigation_start_date'] . '</p><p> Due By</p><p>' . $value['crm_investigation_end_date'] . '</p>';
					$email_content_sms  = ' Next Statement Date ' . $value['crm_investigation_start_date'] . '  Due By ' . $value['crm_investigation_end_date'] . ' ';
					if (($query['username'] != '' && $jsnconf == 'on')) {
						$cd = 1;
						if ($jsnconf_t == 'on') {
							// $this->send_sms_email($to_number,$email_content);
						}
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For Tax Investigation';
						$email_contents[]  = '<p>' . $email_content . '</p>';
						$email_content_new[]  = $email_content_sms;
						$title[] = 'Tax Investigation Remainder';
						//email template
					}

					//  }// foreach close
				} // foreach close
			}
			/**********************************************/

			// Bookkeeping --  Next Bookkeeping Date


			$Bookkeeping = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_next_booking_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '" and reminder_block=0')->result_array();
			//$bookkeep_re=$this->db->query('SELECT * FROM reminder_setting WHERE service_type="bookkeep"')->result_array();
			if ($re_bookkeep != '') {
				foreach ($Bookkeeping as $key => $value) {

					//  echo "11st";
					$ab = 1;
					(isset(json_decode($value['bookkeep'])->reminder) && $value['bookkeep'] != '') ? $jsnbookkeep =  json_decode($value['bookkeep'])->reminder : $jsnbookkeep = '';
					(isset(json_decode($value['bookkeep'])->text) && $value['bookkeep'] != '') ? $jsnbookkeep_t =  json_decode($value['bookkeep'])->text : $jsnbookkeep_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_next_booking_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;

					$query = $this->db->query('select * from user where crm_email_id="' . $value['crm_email'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];

					$a  = array('{crm_next_booking_date}' => $value['crm_next_booking_date'], '{crm_bookkeep_add_custom_reminder}' => $value['crm_bookkeep_add_custom_reminder']);
					//foreach ($bookkeep_re as $acckey => $accvalue) {

					//  $email_content1    = $accvalue['message'];
					$email_content  = '<p> Next Booking  Date</p><p>' . $value['crm_next_booking_date'] . '</p><p>bookkeep add custom reminder </p><p>' . $value['crm_bookkeep_add_custom_reminder'] . '</p>';
					$email_content_sms  = 'Next Booking  Date ' . $value['crm_next_booking_date'] . ' bookkeep add custom reminder  ' . $value['crm_bookkeep_add_custom_reminder'] . ' ';

					if (($query['username'] != '') && $jsnbookkeep == 'on') {
						$cd = 1;
						if ($jsnbookkeep_t == 'on') {
							//$this->send_sms_email($to_number,$email_content);
						}
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For Next Bookkeeping Date';
						$email_contents[]  = '<p>' . $email_content . '</p>';
						$email_content_new[]  = $email_content_sms;
						$title[] = 'Next Bookkeeping Date Remainder';
					}
					//} // foreach close
				} // foreach close
			}
			// Management Accounts --  Next Management Accounts Due date

			$Management = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_next_manage_acc_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") AND user_id="' . $id . '" and reminder_block=0')->result_array();
			//$management_re=$this->db->query('SELECT * FROM reminder_setting WHERE service_type="management"')->result_array();
			if ($re_management != '') {
				foreach ($Management as $key => $value) {
					//   echo "12st";
					$ab = 1;
					(isset(json_decode($value['management'])->reminder) && $value['management'] != '') ? $jsnmanagement =  json_decode($value['management'])->reminder : $jsnmanagement = '';
					(isset(json_decode($value['management'])->text) && $value['management'] != '') ? $jsnmanagement_t =  json_decode($value['management'])->text : $jsnmanagement_t = '';
					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_next_manage_acc_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					$old_record[] = $value;
					$query = $this->db->query('select * from user where crm_email_id="' . $value['crm_email'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];
					$a  = array('{crm_next_manage_acc_date}' => $value['crm_next_manage_acc_date'], '{crm_manage_add_custom_reminder}' => $value['crm_manage_add_custom_reminder']);
					// foreach ($management_re as $acckey => $accvalue) {
					//$email_content1    = $accvalue['message'];
					$email_content  = '<p>Nex Manage Account date</p><p>' . $value['crm_next_manage_acc_date'] . '</p><p> crm Manage add custom reminder </p><p>' . $value['crm_manage_add_custom_reminder'] . '</p>';
					$email_content_sms  = 'Nex Manage Account date' . $value['crm_next_manage_acc_date'] . ' crm Manage add custom reminder ' . $value['crm_manage_add_custom_reminder'] . '';
					if (($query['username'] != '') && $jsnmanagement == 'on') {
						$cd = 1;
						if ($jsnmanagement_t == 'on') {
							//$this->send_sms_email($to_number,$email_content);
						}
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For Next Management Accounts Due Date';
						$email_contents[]  = '<p>' . $email_content . '</p>';
						$email_content_new[]  = $email_content_sms;
						$title[] = 'Next Management Accounts Due Date Remainder';
					}
					//   } // foreach close
				} // foreach close
			}
			// VAT Quaters --  VAT Quarters

			$vat_quarters = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND user_id="' . $id . '" and reminder_block=0')->result_array();
			//$vat_re=$this->db->query('SELECT * FROM reminder_setting WHERE service_type="vat"')->result_array();
			if ($re_vat != '') {
				foreach ($vat_quarters as $vat_quarterskey => $vat_quartersval) {

					(isset(json_decode($vat_quartersval['vat'])->reminder) && $vat_quartersval['vat'] != '') ? $vatmanagement =  json_decode($vat_quartersval['vat'])->reminder : $vatmanagement = '';
					(isset(json_decode($vat_quartersval['vat'])->text) && $vat_quartersval['vat'] != '') ? $vatmanagement_t =  json_decode($vat_quartersval['vat'])->text : $vatmanagement_t = '';

					if ($vatmanagement == 'on') {
						// echo "13st";
						$ab = 1;
						$crm_vat_quarters = $vat_quartersval['crm_vat_quarters'];
						if ($crm_vat_quarters != '0') {
							if ($crm_vat_quarters == 'Annual End Jan') {
								$month = '01';
							} elseif ($crm_vat_quarters == 'Annual End Feb') {
								$month = '02';
							} elseif ($crm_vat_quarters == 'Annual End Mar') {
								$month = '03';
							} elseif ($crm_vat_quarters == 'Annual End Apr') {
								$month = '04';
							} elseif ($crm_vat_quarters == 'Annual End May') {
								$month = '05';
							} elseif ($crm_vat_quarters == 'Annual End Jun') {
								$month = '06';
							} elseif ($crm_vat_quarters == 'Annual End Jul') {
								$month = '07';
							} elseif ($crm_vat_quarters == 'Annual End Aug') {
								$month = '08';
							} elseif ($crm_vat_quarters == 'Annual End Sep') {
								$month = '09';
							} elseif ($crm_vat_quarters == 'Annual End Oct') {
								$month = '10';
							} elseif ($crm_vat_quarters == 'Annual End Nov') {
								$month = '11';
							} elseif ($crm_vat_quarters == 'Annual End Dec') {
								$month = '12';
							} elseif ($crm_vat_quarters == 'Jan/Apr/Jul/Oct') {
								$month = '01_04_07_10';
							} elseif ($crm_vat_quarters == 'Feb/May/Aug/Nov') {
								$month = '02_05_08_11';
							} elseif ($crm_vat_quarters == 'Mar/Jun/Sep/Dec') {
								$month = '03_06_09_12';
							} elseif ($crm_vat_quarters == 'Monthly') {
								$month = '01_02_03_04_05_06_07_08_09_10_11_12';
							}
							$array = explode('_', $month);
							foreach ($array as $key => $value) {
								$y = date('Y');
								$e_date = date('01-' . $value . '-' . $y);
								//echo $e_date;
								$c_date = date('d-m-Y');
							}
							//if($c_date==$e_date)
							//{
							$query = $this->db->query('select * from user where crm_email_id="' . $vat_quartersval['crm_email'] . '"')->row_array();

							$to_number = $query['crm_phone_number'];

							$a  = array('{crm_vat_quarters}' => $crm_vat_quarters, '{crm_vat_add_custom_reminder}' => $vat_quartersval['crm_vat_add_custom_reminder']);
							//foreach ($vat_re as $acckey => $accvalue) {

							//$email_content1    = $accvalue['message'];
							$email_content  = '<p> Vat quarters</p><p>' . $crm_vat_quarters . '</p><p> vat custom reminder </p><p>' . $vat_quartersval['crm_vat_add_custom_reminder'] . '</p>';
							$email_content_sms  = 'Vat quarters' . $crm_vat_quarters . ' vat custom reminder ' . $vat_quartersval['crm_vat_add_custom_reminder'] . '';

							if (($query['username'] != '') && $vatmanagement == 'on') {
								$cd = 1;
								if ($vatmanagement_t == 'on') {
									//  $this->send_sms_email($to_number,$email_content);
								}
								$data2['username'] = $query['username'];
								$user_id = $query['id'];
								$password = $query['password'];
								$encrypt_id = base64_encode($user_id);
								$email_subject  = 'Email Remainder For VAT Quaters Due Date';
								$email_contents[]  = '<p>' . $email_content . '</p>';
								$email_content_new[]  = $email_content_sms;
								$title[] = 'Next VAT Quaters Due Date Remainder';
							}
						}
					}
				}
			}

			if ($cd == 1) {
				if ($ab == 1) {
					$data2["title"] = $title;
					$data2["email_contents"] = $email_contents;
					$url = base_url() . "client/client_infomation/" . $id;
					$data2['link'] = '<a href=' . $url . ' style="display: inline-block;text-align: center;color: #00aeef;text-decoration: none;font-family:;background: #00aeef;color: #fff;font-weight: 600;padding: 5px 10px;border-radius: 3px;">Click Here</a>';


					$body = $this->load->view('service_remainder_email.php', $data2, TRUE);
					$this->load->library('email');
					$this->email->set_mailtype("html");
					$this->email->from('info@remindoo.org');
					$this->email->to($user_email_id['crm_email']);
					$this->email->subject('Email Remainder From Remindoo');
					$this->email->message($body);
					$send = $this->email->send();
					$da['user_id'] = 2;
					$da['crm_name'] = 'test';
					$da['crm_currency'] = '₨';
					$da['crm_profile_image'] = '';
					$da['logo_image'] = '';
					$da['created_date'] = time();
					$this->Common_mdl->insert('admin_setting', $da);
					$emailcontent = array_values(array_filter($email_content_new));
					$title = array_values(array_filter($title));
					$first_key = key($title);
					$cnt = 'Hi ' . $query['username'] . ',';
					$a = $title[$first_key];
					for ($i = 0; $i < count($emailcontent); $i++) {
						$b = $title[$i];
						if ($a == $b) {
							if ($i == '0') {
								$cnt .= $title[$i];
							}
						} else {
							$cnt  .= $title[$i];
							$a = $title[$i];
						}
						$cnt .= $emailcontent[$i];
					}
				}
			}
		} //close if
	} // function close
	/** end of service reminder mail **/

	/** for Timeline services 06-09-2018 **/
	public function client_timeline_notes_add()
	{
		$notes_id = $_POST['update_notes_id'];

		$notes_data['notes'] = $_POST['notes'];
		$notes_data['services_type'] = $_POST['services_type'];
		$notes_data['user_id'] = $_POST['user_id'];
		$notes_data['module'] = 'notes';
		$notes_data['create_by'] = $_SESSION['id'];
		

		if ($notes_id == '') {
			$notes_data['created_time'] = time();
			$res = $this->Common_mdl->insert('timeline_services_notes_added', $notes_data);
		} else {
			$notes_data['updated_time'] = time();
			$res = $this->Common_mdl->update('timeline_services_notes_added', $notes_data, 'id', $notes_id);
		}

		//$res=$this->Common_mdl->insert('timeline_services_notes_added',$notes_data);
		echo $res;
	}
	/** for Timeline services schedule 19-09-2018 **/
	public function client_timeline_schedule_add()
	{
		$notes_id = $_POST['update_schedule_id'];
		$notes_data['notes'] = $_POST['notes'];
		$notes_data['services_type'] = '';
		$notes_data['user_id'] = $_POST['user_id'];
		$notes_data['module'] = 'schedule';
		$notes_data['create_by'] = $_SESSION['id'];
		$notes_data['date'] = $_POST['schedule_date'];

		if ($notes_id == '') {
			$notes_data['created_time'] = time();
			$dateFormate = array_reverse(explode('-', $_POST['schedule_date']));
			$notes_data['comments_for_reference'] = implode('-', $dateFormate);
			$res = $this->Common_mdl->insert('timeline_services_notes_added', $notes_data);
		} else {
			$notes_data['updated_time'] = time();
			$res = $this->Common_mdl->update('timeline_services_notes_added', $notes_data, 'id', $notes_id);
		}
		//$res=$this->Common_mdl->insert('timeline_services_notes_added',$notes_data);
		echo $res;
	}


	public function get_client_timeline_notes_add()
	{
		$note_id = $_POST['note_id'];
		$get_data = $this->db->query('select * from timeline_services_notes_added where id=' . $note_id . ' ')->row_array();
		$data['notes'] = $get_data['notes'];
		$data['services_type'] = $get_data['services_type'];
		$data['date'] = $get_data['date'];
		echo json_encode($data);
	}

	/** timeline delete note **/
	public function delete_timeline_notes()
	{
		echo $_POST['note_id'];
		$sql = $this->db->query("DELETE FROM timeline_services_notes_added WHERE id=" . $_POST['note_id'] . " ");
	}

	/** end of delete note section **/
	/** for clienttimeline services **/
	public function jquery_append_client_timeline_services($id, $module)
	{
		$client_data             = $this->db->query('select * from client where user_id=' . $id . ' ')->result_array();
		$data['client_services'] = $this->Common_mdl->getClientServices($id);

		$data['client_details']  = $client_data;
		$data['client_id']       = $id;
		$data['module']          = $module;

		if ($module == 'allactivity') {
			$this->load->view('Timeline/all_activitylog_timeline_section', $data);
		} else {
			$this->load->view('Timeline/notessection_timeline_sub', $data);
		}
	}
	/** end of clienttimeline services **/
	/** end of 06-09-2018 **/

	/** 14-09-2018 for timeline services email **/
	public function client_timeline_email_add()
	{
		$email_data['notes'] = $_POST['notes'];
		$email_data['services_type'] = "";
		$email_data['user_id'] = $_POST['user_id'];
		$email_data['module'] = 'email';
		$email_data['create_by'] = $_SESSION['id'];
		$email_data['created_time'] = time();
		$update_id = trim($_POST['update_id']);

		if (empty($update_id)) {
			$res = $this->Common_mdl->insert('timeline_services_notes_added', $email_data);
		} else {
			$email_data['updated_time'] = time();
			$res = $this->Common_mdl->update('timeline_services_notes_added', $email_data, 'id', $update_id);
		}

		echo $res;
	}
	public function client_timeline_task_add()
	{
		$email_data['notes'] = $_POST['notes'];
		$email_data['services_type'] = '';
		$email_data['user_id'] = $_POST['user_id'];
		$email_data['module'] = 'newtask';
		$email_data['create_by'] = $_SESSION['id'];


		$email_data['created_time'] = time();
		$res = $this->Common_mdl->insert('timeline_services_notes_added', $email_data);

		//$res=$this->Common_mdl->insert('timeline_services_notes_added',$notes_data);
		echo $res;
	}

	/**end of timeline sectives email **/
	/** for Timeline services 15-09-2018 **/
	public function client_timeline_calllog_add()
	{
		$calllog_id = $_POST['update_calllog_id'];

		$notes_data['notes'] = $_POST['notes'];

		$notes_data['user_id'] = $_POST['user_id'];
		$notes_data['module'] = 'calllog';
		$notes_data['create_by'] = $_SESSION['id'];
		$notes_data['date'] = $_POST['call_date'];

		if ($calllog_id == '') {
			$notes_data['created_time'] = time();
			$res = $this->Common_mdl->insert('timeline_services_notes_added', $notes_data);
		} else {
			$notes_data['updated_time'] = time();
			$res = $this->Common_mdl->update('timeline_services_notes_added', $notes_data, 'id', $calllog_id);
		}

		//$res=$this->Common_mdl->insert('timeline_services_notes_added',$notes_data);
		echo $res;
	}



	public function client_timeline_sms_add()
	{


		$calllog_id = $_POST['update_calllog_id'];

		$notes_data['notes'] = $_POST['notes'];

		$notes_data['user_id'] = $_POST['user_id'];
		$notes_data['module'] = 'smslog';
		$notes_data['create_by'] = $_SESSION['id'];

		$mobile_number = $_POST['mobile_number'];
		$notes = $_POST['notes'];

		if ($calllog_id == '') {
			$notes_data['created_time'] = time();
			$res = $this->Common_mdl->insert('timeline_services_notes_added', $notes_data);

			$this->send_sms_timeline($mobile_number, $notes);
		} else {
			$notes_data['updated_time'] = time();
			$res = $this->Common_mdl->update('timeline_services_notes_added', $notes_data, 'id', $calllog_id);
		}

		//$res=$this->Common_mdl->insert('timeline_services_notes_added',$notes_data);
		echo $res;
	}



	public function client_timeline_meeting_add()
	{


		$calllog_id = $_POST['update_calllog_id'];

		$notes_data['notes'] = $_POST['notes'];

		$notes_data['user_id'] = $_POST['user_id'];
		$notes_data['module'] = 'meeting';
		$notes_data['create_by'] = $_SESSION['id'];
		$notes_data['date'] = $_POST['meeting_date'];

		$notes = $_POST['notes'];

		if ($calllog_id == '') {
			$notes_data['created_time'] = time();
			$res = $this->Common_mdl->insert('timeline_services_notes_added', $notes_data);
		} else {
			$notes_data['updated_time'] = time();
			$res = $this->Common_mdl->update('timeline_services_notes_added', $notes_data, 'id', $calllog_id);
		}

		//$res=$this->Common_mdl->insert('timeline_services_notes_added',$notes_data);
		echo $res;
	}

	/** end of 15-09-2018 **/

	/** for Timeline services 15-09-2018 **/
	public function client_timeline_log_add()
	{
		$calllog_id = $_POST['update_log_id'];

		$notes_data['notes'] = $_POST['notes'];

		$notes_data['user_id'] = $_POST['user_id'];
		$notes_data['module'] = 'logactivity';
		$notes_data['create_by'] = $_SESSION['id'];

		if ($calllog_id == '') {
			$notes_data['created_time'] = time();
			$res = $this->Common_mdl->insert('timeline_services_notes_added', $notes_data);
		} else {
			$notes_data['updated_time'] = time();
			$res = $this->Common_mdl->update('timeline_services_notes_added', $notes_data, 'id', $calllog_id);
		}

		//$res=$this->Common_mdl->insert('timeline_services_notes_added',$notes_data);
		echo $res;
	}
	/** end of 15-09-2018 **/
	/** 18-09-2018 **/
	public function jquery_append_client_timeline_services_allactivity()
	{
		error_reporting(0);
		$id = $_POST['user_id'];
		$filter_values = $_POST['filter_values'];

		$client_data   = $this->db->query('select * from client where user_id=' . $id . ' ')->result_array();

		$data['client_services'] = array("VAT" => "VAT", "Payroll" => "Payroll", "Accounts" => "Accounts", "Confirmation_statement" => "Confirmation statement", "Company_Tax_Return" => "Company Tax Return", "Personal_Tax_Return" => "Personal Tax Return", "WorkPlace_Pension" => "WorkPlace Pension - AE", "CIS_Contractor" => "CIS - Contractor", "CIS_Sub_Contractor" => "CIS - Sub Contractor", "P11D" => "P11D", "Bookkeeping" => "Bookkeeping", "Management_Accounts" => "Management Accounts", "Investigation_Insurance" => "Investigation Insurance", "Registered_Address" => "Registered Address", "Tax_Advice" => "Tax Advice", "Tax_Investigation" => "Tax Investigation");
		$data['client_details'] = $client_data;
		$data['client_id'] = $id;

		$data['filter_values'] = $filter_values;
		$data['services_type'] = isset($_POST['services_type']) ? $_POST['services_type'] : '';

		$this->load->view('Timeline/all_activitylog_timeline_section', $data);
	}
	/** end of 18-09-2018 **/


	public function timeline_task()
	{

		$company_name = $_POST['company_name'];
		$company_no = $_POST['company_no'];
		$task_name = $_POST['task_name'];

		$datas['subject'] = 'create/update Client-' . $proposal_data['company_name'];
		$datas['service_due_date'] = date('Y-m-d');
		$datas['start_date'] = date('Y-m-d');
		$datas['user_id'] = $in1;
		$datas['end_date'] = $date = date('Y-m-d', strtotime('+2 days'));;
		$datas['company_name'] = $in1;
		$datas['worker'] = '';
		$datas['team'] = '';
		$datas['department'] = '';
		$datas['manager'] = '';
		$datas['billable'] = 'Billable';
		$datas['description'] = '';
		$datas['lead_id'] = $proposal_data['lead_id'];
		$datas['created_date'] = time();
		$datas['create_by'] = $_SESSION['id'];
		$datas['priority'] = 'Low';
		$datas['related_to'] = 'Auto created task';
		$datas['task_status'] = 'notstarted';
		$insert_data = $this->db->insert('add_new_task', $datas);

		$in = $this->db->insert_id();


		$username = $this->Common_mdl->getUserProfileName($_SESSION['id']);

		$activity_datas['log'] = "Task was created  by " . $username;
		$activity_datas['createdTime'] = time();
		$activity_datas['module'] = 'Task';
		$activity_datas['sub_module'] = '';
		$activity_datas['user_id'] = $_SESSION['id'];
		$activity_datas['module_id'] = $in;

		$this->Common_mdl->insert('activity_log', $activity_datas);
	}

	public function header_notification()
	{
		$result = array();
		$query = $this->db->query('select * from client where firm_admin_id=' . $_SESSION['id'] . ' and status=1 and reminder_block=0')->result_array();
		$notification = array();
		$sample = array();
		$up = array('id', 'crm_company_name', 'crm_company_number', 'crm_incorporation_date', 'crm_register_address', 'crm_company_type', 'accounts_next_made_up_to', 'accounts_next_due', 'confirmation_next_made_up_to', 'confirmation_next_due');

		foreach ($query as $data) {
			array_push($sample, '');
			array_push($sample, $data['crm_company_name']);
			array_push($sample, trim($data['crm_company_number']));
			array_push($sample, $data['crm_incorporation_date']);
			array_push($sample, $data['crm_register_address']);
			array_push($sample, $data['crm_company_type']);
			array_push($sample, (isset($data['crm_hmrc_yearend']) && ($data['crm_hmrc_yearend'] != '')) ? $data['crm_hmrc_yearend'] : '');
			array_push($sample, (isset($data['crm_ch_accounts_next_due']) && ($data['crm_ch_accounts_next_due'] != '')) ?  date('Y-m-d', strtotime($data['crm_ch_accounts_next_due'])) : '');
			array_push($sample, (isset($data['crm_confirmation_statement_date']) && ($data['crm_confirmation_statement_date'] != '')) ?   date('Y-m-d', strtotime($data['crm_confirmation_statement_date'])) : '');
			array_push($sample, (isset($data['crm_confirmation_statement_due_date']) && ($data['crm_confirmation_statement_due_date'] != '')) ?  date('Y-m-d', strtotime($data['crm_confirmation_statement_due_date'])) : '');

			$c = array_combine($up, $sample);

			$company_number =trim( $data['crm_company_number']);
			$user_id = $data['user_id'];
			$table_records = json_encode($data);
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk/company/' . trim($data['crm_company_number']) . '/');
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_USERPWD, $this->companieshouseAPIkey);
			$response = curl_exec($curl);
			if ($errno = curl_errno($curl)) {
				$error_message = curl_strerror($errno);
				echo "cURL error ({$errno}):\n {$error_message}";
			}
			curl_close($curl);
			$data_api = json_decode($response);
			$array_rec = $this->object_2_array($data_api);
			$fields = array();
			if (!empty($array_rec)) {
				array_push($fields, $data['id']);
				if (isset($array_rec['company_name']) && $array_rec['company_name'] != '') {
					$company_name = $array_rec['company_name'];
				} else {
					$company_name = '';
				}
				array_push($fields, $company_name);
				if (isset($array_rec['company_number']) && $array_rec['company_number'] != '') {
					$company_number = $array_rec['company_number'];
				} else {
					$company_number = '';
				}
				array_push($fields, $company_number);
				if (isset($array_rec['date_of_creation']) && $array_rec['date_of_creation'] != '') {
					$date_of_creation = $array_rec['date_of_creation'];
				} else {
					$date_of_creation = '';
				}
				array_push($fields, date('d-m-Y', strtotime($date_of_creation)));
				if (isset($array_rec['registered_office_address']['address_line_1']) && $array_rec['registered_office_address']['address_line_1'] != '') {
					$address_line_1 = $array_rec['registered_office_address']['address_line_1'];
				} else {
					$address_line_1 = '';
				}
				if (isset($array_rec['registered_office_address']['address_line_2']) && $array_rec['registered_office_address']['address_line_2'] != '') {
					$address_line_2 = $array_rec['registered_office_address']['address_line_2'];
				} else {
					$address_line_2 = '';
				}
				if (isset($array_rec['registered_office_address']['postal_code']) && $array_rec['registered_office_address']['postal_code'] != '') {
					$postal_code = $array_rec['registered_office_address']['postal_code'];
				} else {
					$postal_code = '';
				}
				if (isset($array_rec['registered_office_address']['locality']) && $array_rec['registered_office_address']['locality'] != '') {
					$locality = $array_rec['registered_office_address']['locality'];
				} else {
					$locality = '';
				}

				$next_made_up_to = (isset($array_rec['accounts']['next_made_up_to']) && ($array_rec['accounts']['next_made_up_to'] != '')) ? $array_rec['accounts']['next_made_up_to'] : '';

				$next_due = (isset($array_rec['accounts']['next_due']) && ($array_rec['accounts']['next_due'] != '')) ? $array_rec['accounts']['next_due'] : '';

				$confirmation_next_made_up_to = (isset($array_rec['confirmation_statement']['next_made_up_to']) && ($array_rec['confirmation_statement']['next_made_up_to'] != '')) ? $array_rec['confirmation_statement']['next_made_up_to'] : '';

				$confirmation_next_due = (isset($array_rec['confirmation_statement']['next_due']) && ($array_rec['confirmation_statement']['next_due'] != '')) ? $array_rec['confirmation_statement']['next_due'] : '';



				$address = $address_line_1 . $address_line_2 . $locality . $postal_code;
				array_push($fields, $address);
				if (isset($array_rec['type']) && $array_rec['type'] != '') {
					$type = $array_rec['type'];
				} else {
					$type = '';
				}
				array_push($fields, $type);
				array_push($fields, $next_made_up_to);
				array_push($fields, $next_due);
				array_push($fields, $confirmation_next_made_up_to);
				array_push($fields, $confirmation_next_due);

				$c1 = array_combine($up, $fields);

				echo '<pre>';
				print_r($c1);
				echo '</pre>';

				echo '<pre>';
				print_r($c);
				echo '</pre>';


				array_push($result, array_diff($c1, $c));

				print_r($result);
			}
			$sample = array();
			$fields = array();
		}
		// print_r($result);
	}



	public function clienthashtag()
	{
		$data['client_hashtag'] = $this->Common_mdl->getallrecords('client_hashtag');
		$this->load->view('clients/addhashtag', $data);
	}

	public function insert_hashtag()
	{
		$data = array('hashtag' => $_POST['client_hashtag'], 'status' => 1, 'user_id' => $_SESSION['id']);
		$this->Common_mdl->insert('client_hashtag', $data);
		redirect('client/clienthashtag');
	}

	public function hashtag_select()
	{
		$id = $_POST['id'];
		$records = $this->Common_mdl->select_record('client_hashtag', 'id', $id);
		echo json_encode($records);
	}

	public function update_hashtag()
	{
		$data = array('hashtag' => $_POST['client_hashtag']);
		$this->Common_mdl->update('client_hashtag', $data, 'id', $_POST['hashtag_id']);
		redirect('client/clienthashtag');
	}

	public function delete_hashtag()
	{
		$id = $_POST['hashtag_id'];
		$this->Common_mdl->delete('client_hashtag', 'id', $id);
		redirect('client/clienthashtag');
	}

	public function update_activity()
	{
		// $id = $this->session->userdata('id');
		// $datas['log'] = "Deactivate an account( Logout )";
		// $datas['createdTime'] = time();
		// $datas['module'] = 'Logout';
		// $datas['user_id'] = $id;
		$datas['value'] = 'test';
		$this->Common_mdl->insert('test_records', $datas);
	}

	public function Missing_task($id, $lead_id = false)
	{
		$client_details = $this->db->query("select * from client where id='" . $id . "'")->row_array();

		$in['user_id']  = $client_details['user_id'];
		$this->Common_mdl->insert('section_settings', $in);

		$query = $this->Common_mdl->empty_field_find($client_details['user_id']);
		$content = '';
		foreach ($query as $res) {
			$content .=  '<p>Missing:' . $res . '</p>';
		}
		//  $client_details=$this->db->query("select * from client where id='".$id."'")->row_array();
		$datas['subject']       = $client_details['crm_company_name'] . '-Missing Information';
		$datas['service_due_date'] = date('Y-m-d');
		$datas['start_date']    = date('Y-m-d');
		$datas['user_id']       = $client_details['user_id'];
		$datas['end_date']      = $date = date('Y-m-d', strtotime('+2 days'));;
		$datas['company_name']  = $client_details['id'];
		$datas['worker']        = '';
		$datas['team']          = '';
		$datas['department']    = '';
		$datas['manager']       = '';
		$datas['billable']      = 'Billable';
		$datas['description']   = $content;
		$datas['lead_id']       = $lead_id;
		$datas['created_date']  = time();
		$datas['priority']      = 'Low';
		$datas['related_to']    = 'tasks';
		$datas['task_status']   = '1';
		$datas['firm_id']       = $client_details['firm_id'];
		$datas['missing_reminder'] = 1;
		$insert_data = $this->db->insert('add_new_task', $datas);

		$in = $this->db->insert_id();

		$assignees = $this->db->query("SELECT assignees FROM firm_assignees WHERE module_name = 'CLIENT' AND module_id = '" . $client_details['id'] . "' AND firm_id = '" . $client_details['firm_id'] . "' AND sub_module_name=''")->result_array();

		foreach ($assignees as $key => $value) {
			$assignees = $value['assignees'];
			$data = ['firm_id' => $client_details['firm_id'], 'module_name' => 'TASK', 'module_id' => $in, 'assignees' => $assignees];
			$this->db->insert('firm_assignees', $data);
		}

		$log = " Missing Details Task Created";
		$this->Common_mdl->Create_Log('client', $client_details['user_id'], $log);
		$this->Common_mdl->Create_Log('Task', $in, $log);
	}

	public function Check_CreateMissingDetailsTask($client_id)
	{
		$SETINGS = $this->db->query('SELECT missing_task FROM admin_setting where missing_task=1 and firm_id=' . $_SESSION['firm_id'])->row_array();

		$Is_already_created = $this->db->get_where("add_new_task", ['company_name' => $client_id, 'missing_reminder' => 1])->num_rows();

		if (!$Is_already_created && $SETINGS['missing_task'] == '1') {
			$this->Missing_task($client_id);
		}
	}

	public function insert_proposal_client()
	{
		$proposal_id = $this->Common_mdl->get_price('proposals', 'id', $_POST['proposal_id'], 'user_id');
		$lead_id = $this->Common_mdl->get_price('proposals', 'id', $_POST['proposal_id'], 'lead_id');
		$firm_id = $this->Common_mdl->get_price('proposals', 'id', $_POST['proposal_id'], 'firm_id');

		$datas['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user', 'role', '1');

		if (!isset($_POST['user_id']) || $_POST['user_id'] == '') {
			if (isset($_POST['user_name']) && $_POST['user_name'] != '' && $_POST['company_name'] != '') {
				if ($lead_id != "" && $lead_id != 0) {
					$assignees = $this->db->query("SELECT assignees FROM firm_assignees WHERE module_name = 'LEADS' AND module_id = '" . $lead_id . "' AND firm_id = '" . $firm_id . "'")->row_array();
					$assignees = $assignees['assignees'];
				} else {
					$assignees = $firm_id;
				}

				$var = rand(1, 10);
				$image_one = (isset($_POST['image_name']) && ($_POST['image_name'] != '')) ? $_POST['image_name'] : '825898.png';
				$image = (isset($_FILES['profile_image']['name']) && ($_FILES['profile_image']['name'] != '')) ? $this->Common_mdl->do_upload($_FILES['profile_image'], 'uploads') : $image_one;
				$data['crm_name'] = $_POST['user_name'];
				$data['username'] = $_POST['user_name'];
				$data['crm_email_id'] = $_POST['emailid'];
				$data['crm_phone_number'] = $_POST['cun_code'] . $_POST['pn_no_rec'];
				$data['password'] = (isset($_POST['password']) && ($_POST['password'] != '')) ? md5($_POST['password']) : md5($var);
				$data['confirm_password'] = (isset($_POST['confirm_password']) && ($_POST['confirm_password'] != '')) ? $_POST['confirm_password'] : $var;
				$data['role'] = 4;
				$data['status'] = 1;
				$data['company_roles'] = (isset($_POST['company_house']) && ($_POST['company_house'] != '')) ? $_POST['company_house'] : 0;
				$data['CreatedTime'] = time();
				$data['client_id'] = (isset($_POST['client_id']) && ($_POST['client_id'] != '')) ? $_POST['client_id'] : rand(1, 5);
				$data['crm_packages'] = (isset($_POST['packages']) && ($_POST['packages'] != '')) ? $_POST['packages'] : '';
				$data['crm_gender'] = (isset($_POST['gender']) && ($_POST['gender'] != '')) ? $_POST['gender'] : '';
				$data['crm_profile_pic'] = $image;
				$data['autosave_status'] = 0;
				$data['firm_admin_id'] = $proposal_id;
				$data['user_type'] = 'FC';
				$data['firm_id'] = $firm_id;

				$updates_user = (isset($_POST['user_id']) && ($_POST['user_id'] == '0' || $_POST['user_id'] == '')) ? $this->db->insert('user', $data) : $this->Common_mdl->update('user', $data, 'id', $_POST['user_id']);

				$insert_data = $updates_user;

				if ($_POST['user_id'] == '') {
					$_POST['user_id'] = $this->db->insert_id();
				}

				if ($_POST['user_id']) {
					$in = $this->db->insert_id();
					$data1['user_id'] = (isset($_POST['user_id']) && ($_POST['user_id'] == '')) ? $in : $_POST['user_id'];
					// required information
					$_POST['pn_no_rec'] = $_POST['cun_code'] . $_POST['pn_no_rec'];
					//echo $_POST['pn_no_rec'];die;
					$this->send_sms($_POST['pn_no_rec']);
					if (!empty($_POST['confirm'])) {

						if (isset($_POST['confirm']['text']) && $_POST['confirm']['text'] == 'on') {

							$this->send_sms($_POST['pn_no_rec']);
						}
						$data1['conf_statement'] = (isset($_POST['confirm']['tab']) && $_POST['confirm']['tab'] == 'on') ? json_encode($_POST['confirm']) : '';
					} else {
						$data1['conf_statement'] = '';
					}


					if (!empty($_POST['accounts'])) {
						if (isset($_POST['accounts']['text']) && $_POST['accounts']['text'] == 'on') {
							$this->send_sms($_POST['pn_no_rec']);
						}
						$data1['accounts'] =  (isset($_POST['accounts']['tab']) && $_POST['accounts']['tab'] == 'on') ? json_encode($_POST['accounts']) : '';
					} else {
						$data1['accounts'] = '';
					}


					if (!empty($_POST['companytax'])) {
						if (isset($_POST['companytax']['text']) && $_POST['companytax']['text'] == 'on') {
							$this->send_sms($_POST['pn_no_rec']);
						}

						$data1['company_tax_return'] =   (isset($_POST['companytax']['tab']) && $_POST['companytax']['tab'] == 'on') ? json_encode($_POST['companytax']) : '';
					} else {

						$data1['company_tax_return'] = '';
					}


					if (!empty($_POST['personaltax'])) {
						if (isset($_POST['personaltax']['text']) && $_POST['personaltax']['text'] == 'on') {
							$this->send_sms($_POST['pn_no_rec']);
						}
						$data1['personal_tax_return'] =  (isset($_POST['personaltax']['tab']) && $_POST['personaltax']['tab'] == 'on') ? json_encode($_POST['personaltax']) : '';
					} else {
						$data1['personal_tax_return'] = '';
					}

					if (!empty($_POST['payroll'])) {
						if (isset($_POST['payroll']['text']) && $_POST['payroll']['text'] == 'on') {
							$this->send_sms($_POST['pn_no_rec']);
						}
						$data1['payroll'] =  (isset($_POST['payroll']['tab']) && $_POST['payroll']['tab'] == 'on') ? json_encode($_POST['payroll']) : '';
					} else {
						$data1['payroll'] = '';
					}


					if (!empty($_POST['workplace'])) {
						if (isset($_POST['workplace']['text']) && $_POST['workplace']['text'] == 'on') {
							$this->send_sms($_POST['pn_no_rec']);
						}
						$data1['workplace'] =  (isset($_POST['workplace']['tab']) && $_POST['workplace']['tab'] == 'on') ? json_encode($_POST['workplace']) : '';
					} else {
						$data1['workplace'] = '';
					}


					if (!empty($_POST['vat'])) {
						if (isset($_POST['vat']['text']) && $_POST['vat']['text'] == 'on') {
							$this->send_sms($_POST['pn_no_rec']);
						}
						$data1['vat'] =  (isset($_POST['vat']['tab']) && $_POST['vat']['tab'] == 'on') ? json_encode($_POST['vat']) : '';
					} else {
						$data1['vat'] = '';
					}



					if (!empty($_POST['cis'])) {
						if (isset($_POST['cis']['text']) && $_POST['cis']['text'] == 'on') {
							$this->send_sms($_POST['pn_no_rec']);
						}
						$data1['cis'] =  (isset($_POST['cis']['tab']) && $_POST['cis']['tab'] == 'on') ? json_encode($_POST['cis']) : '';
					} else {
						$data1['cis'] = '';
					}

					if (!empty($_POST['cissub'])) {
						if (isset($_POST['cissub']['text']) && $_POST['cissub']['text'] == 'on') {
							$this->send_sms($_POST['pn_no_rec']);
						}
						$data1['cissub'] =  (isset($_POST['cissub']['tab']) && $_POST['cissub']['tab'] == 'on') ? json_encode($_POST['cissub']) : '';
					} else {
						$data1['cissub'] = '';
					}



					if (!empty($_POST['p11d'])) {
						if (isset($_POST['p11d']['text']) && $_POST['p11d']['text'] == 'on') {
							$this->send_sms($_POST['pn_no_rec']);
						}
						$data1['p11d'] =  (isset($_POST['p11d']['tab']) && $_POST['p11d']['tab'] == 'on') ? json_encode($_POST['p11d']) : '';
					} else {
						$data1['p11d'] = '';
					}


					if (!empty($_POST['bookkeep'])) {
						if (isset($_POST['bookkeep']['text']) && $_POST['bookkeep']['text'] == 'on') {
							$this->send_sms($_POST['pn_no_rec']);
						}
						$data1['bookkeep'] =  (isset($_POST['bookkeep']['tab']) && $_POST['bookkeep']['tab'] == 'on') ? json_encode($_POST['bookkeep']) : '';
					} else {
						$data1['bookkeep'] = '';
					}


					if (!empty($_POST['management'])) {
						if (isset($_POST['management']['text']) && $_POST['management']['text'] == 'on') {
							$this->send_sms($_POST['pn_no_rec']);
						}
						$data1['management'] =  (isset($_POST['management']['tab']) && $_POST['management']['tab'] == 'on') ? json_encode($_POST['management']) : '';
					} else {
						$data1['management'] = '';
					}


					if (!empty($_POST['investgate'])) {
						if (isset($_POST['investgate']['text']) && $_POST['investgate']['text'] == 'on') {
							$this->send_sms($_POST['pn_no_rec']);
						}
						$data1['investgate'] =  (isset($_POST['investgate']['tab']) && $_POST['investgate']['tab'] == 'on') ? json_encode($_POST['investgate']) : '';
					} else {
						$data1['investgate'] = '';
					}

					if (!empty($_POST['registered'])) {
						if (isset($_POST['registered']['text']) && $_POST['registered']['text'] == 'on') {
							$this->send_sms($_POST['pn_no_rec']);
						}
						$data1['registered'] =  (isset($_POST['registered']['tab']) && $_POST['registered']['tab'] == 'on') ? json_encode($_POST['registered']) : '';
					} else {
						$data1['registered'] = '';
					}

					if (!empty($_POST['taxadvice'])) {
						if (isset($_POST['taxadvice']['text']) && $_POST['taxadvice']['text'] == 'on') {
							$this->send_sms($_POST['pn_no_rec']);
						}
						$data1['taxadvice'] =  (isset($_POST['taxadvice']['tab']) && $_POST['taxadvice']['tab'] == 'on') ? json_encode($_POST['taxadvice']) : '';
					} else {
						$data1['taxadvice'] = '';
					}
					if (!empty($_POST['taxinvest'])) {
						if (isset($_POST['taxinvest']['text']) && $_POST['taxinvest']['text'] == 'on') {
							$this->send_sms($_POST['pn_no_rec']);
						}
						$data1['taxinvest'] =  (isset($_POST['taxinvest']['tab']) && $_POST['taxinvest']['tab'] == 'on') ? json_encode($_POST['taxinvest']) : '';
					} else {
						$data1['taxinvest'] = '';
					}

					$other_services = $this->db->query('SELECT * FROM service_lists WHERE id>16')->result_array();

					foreach ($other_services as $key => $value) {
						if (!empty($_POST[$value['services_subnames']])) {
							if (isset($_POST[$value['services_subnames']]['text']) && $_POST[$value['services_subnames']]['text'] == 'on') {
								$this->send_sms($_POST['pn_no_rec']);
							}
							$data1[$value['services_subnames']] =  (isset($_POST[$value['services_subnames']]['tab']) && $_POST[$value['services_subnames']]['tab'] == 'on') ? json_encode($_POST[$value['services_subnames']]) : '';
						} else {
							$data1[$value['services_subnames']] = '';
						}

						$data1['crm_' . $value['services_subnames'] . '_email_remainder'] = (isset($_POST['crm_' . $value['services_subnames'] . '_email_remainder']) && ($_POST['crm_' . $value['services_subnames'] . '_email_remainder'] != '')) ? $_POST['crm_' . $value['services_subnames'] . '_email_remainder'] : '';
						$data1['crm_' . $value['services_subnames'] . '_add_custom_reminder'] = (isset($_POST['crm_' . $value['services_subnames'] . '_add_custom_reminder']) && ($_POST['crm_' . $value['services_subnames'] . '_add_custom_reminder'] != '')) ? $_POST['crm_' . $value['services_subnames'] . '_add_custom_reminder'] : '';
						$data1['crm_' . $value['services_subnames'] . '_statement_date'] = (isset($_POST['crm_' . $value['services_subnames'] . '_statement_date']) && ($_POST['crm_' . $value['services_subnames'] . '_statement_date'] != '')) ? date("Y-m-d", strtotime($_POST['crm_' . $value['services_subnames'] . '_statement_date'])) : '';
					}

					$data1['firm_admin_id'] = $proposal_id;
					//requried information
					$data1['crm_company_name'] = (isset($_POST['company_name']) && ($_POST['company_name'] != '')) ? $_POST['company_name'] : '';
					$data1['crm_legal_form'] = (isset($_POST['legal_form']) && ($_POST['legal_form'] != '')) ? $_POST['legal_form'] : '';
					$data1['crm_allocation_holder'] = (isset($_POST['allocation_holders']) && ($_POST['allocation_holders'] != '')) ? $_POST['allocation_holders'] : '';
					//basic deatils
					$data1['crm_company_number'] = (isset($_POST['company_number']) && ($_POST['company_number'] != '')) ? trim($_POST['company_number']) : '';
					$data1['crm_company_url'] = (isset($_POST['company_url']) && ($_POST['company_url'] != '')) ? $_POST['company_url'] : '';
					$data1['crm_officers_url'] = (isset($_POST['officers_url']) && ($_POST['officers_url'] != '')) ? $_POST['officers_url'] : '';
					$data1['crm_company_name1'] = (isset($_POST['company_name1']) && ($_POST['company_name1'] != '')) ? $_POST['company_name1'] : '';
					$data1['crm_register_address'] = (isset($_POST['register_address']) && ($_POST['register_address'] != '')) ? $_POST['register_address'] : '';
					$data1['crm_company_status'] = (isset($_POST['company_status']) && ($_POST['company_status'] != '')) ? $_POST['company_status'] : '';
					$data1['crm_company_type'] = (isset($_POST['company_type']) && ($_POST['company_type'] != '')) ? $_POST['company_type'] : '';
					$data1['crm_company_sic'] = (isset($_POST['company_sic']) && ($_POST['company_sic'] != '')) ? $_POST['company_sic'] : '';
					$data1['crm_sic_codes'] = (isset($_POST['sic_codes']) && ($_POST['sic_codes'] != '')) ? $_POST['sic_codes'] : '';
					$data1['crm_company_utr'] = (isset($_POST['company_utr']) && ($_POST['company_utr'] != '')) ? $_POST['company_utr'] : '';
					$data1['crm_incorporation_date'] = (isset($_POST['date_of_creation']) && ($_POST['date_of_creation'] != '')) ? $_POST['date_of_creation'] : '';
					$data1['crm_registered_in'] = (isset($_POST['registered_in']) && ($_POST['registered_in'] != '')) ? $_POST['registered_in'] : '';
					$data1['crm_address_line_one'] = (isset($_POST['address_line_one']) && ($_POST['address_line_one'] != '')) ? $_POST['address_line_one'] : '';
					$data1['crm_address_line_two'] = (isset($_POST['address_line_two']) && ($_POST['address_line_two'] != '')) ? $_POST['address_line_two'] : '';
					$data1['crm_address_line_three'] = (isset($_POST['address_line_three']) && ($_POST['address_line_three'] != '')) ? $_POST['address_line_three'] : '';
					$data1['crm_town_city'] = (isset($_POST['crm_town_city']) && ($_POST['crm_town_city'] != '')) ? $_POST['crm_town_city'] : '';
					$data1['crm_post_code'] = (isset($_POST['crm_post_code']) && ($_POST['crm_post_code'] != '')) ? $_POST['crm_post_code'] : '';
					$data1['crm_hashtag'] = (isset($_POST['client_hashtag']) && ($_POST['client_hashtag'] != '')) ? $_POST['client_hashtag'] : '';
					$data1['crm_accounts_office_reference'] = (isset($_POST['accounts_office_reference']) && ($_POST['accounts_office_reference'] != '')) ? $_POST['accounts_office_reference'] : '';

					$data1['crm_vat_number'] = (isset($_POST['vat_number']) && ($_POST['vat_number'] != '')) ? $_POST['vat_number'] : '';
					// client information
					$data1['crm_letter_sign'] = (isset($_POST['engagement_letter']) && ($_POST['engagement_letter'] != '')) ? $_POST['engagement_letter'] : '';
					$data1['crm_business_website'] = (isset($_POST['business_website']) && ($_POST['business_website'] != '')) ? $_POST['business_website'] : '';
					$data1['crm_accounting_system'] = (isset($_POST['accounting_system_inuse']) && ($_POST['accounting_system_inuse'] != '')) ? $_POST['accounting_system_inuse'] : '';
					$data1['crm_paye_ref_number'] = (isset($_POST['paye_ref_number']) && ($_POST['paye_ref_number'] != '')) ? $_POST['paye_ref_number'] : '';
					$data1['crm_assign_client_id_verified'] = (isset($_POST['client_id_verified']) && ($_POST['client_id_verified'] != '')) ? $_POST['client_id_verified'] : '';
					$data1['crm_assign_type_of_id'] = (isset($_POST['type_of_id']) && ($_POST['type_of_id'] != '')) ? implode(',', $_POST['type_of_id']) : '';
					/** 29-08-2018 proof attch **/
					$proof_attach_file = '';
					if (!empty($_FILES['proof_attach_file']['name'])) {
						/** for multiple **/
						$files = $_FILES;
						$cpt = count($_FILES['proof_attach_file']['name']);
						for ($i = 0; $i < $cpt; $i++) {
							/** end of multiple **/
							$_FILES['proof_attach_file']['name'] = $files['proof_attach_file']['name'][$i];
							$_FILES['proof_attach_file']['type'] = $files['proof_attach_file']['type'][$i];
							$_FILES['proof_attach_file']['tmp_name'] = $files['proof_attach_file']['tmp_name'][$i];
							$_FILES['proof_attach_file']['error'] = $files['proof_attach_file']['error'][$i];
							$_FILES['proof_attach_file']['size'] = $files['proof_attach_file']['size'][$i];
							$uploadPath = 'uploads/client_proof/';
							$config['upload_path'] = $uploadPath;
							$config['allowed_types'] = '*';
							$config['max_size'] = '0';
							$this->load->library('upload', $config);
							$this->upload->initialize($config);
							if ($this->upload->do_upload('proof_attach_file')) {
								$fileData = $this->upload->data();
								//  $uploadData[$i]['file_name'] = base_url().'attachment/'.$fileData['file_name'];
								//$data['attach_file']= base_url().'uploads/'.$fileData['file_name'];
								$proof_attach_file = $proof_attach_file . "," . base_url() . 'uploads/client_proof/' . $fileData['file_name'];
							}
						}
					}
					$data1['proof_attach_file'] = $proof_attach_file;
					/** end of 29-08-2018 **/
					/** 30-08-2018 **/
					$data1['crm_assign_other_custom'] = (isset($_POST['assign_other_custom']) && ($_POST['assign_other_custom'] != '')) ? $_POST['assign_other_custom'] : '';
					/** end of 30-08-2018 **/
					$data1['crm_assign_proof_of_address'] = (isset($_POST['proof_of_address']) && ($_POST['proof_of_address'] != '')) ? $_POST['proof_of_address'] : '';
					$data1['crm_assign_meeting_client'] = (isset($_POST['meeting_client']) && ($_POST['meeting_client'] != '')) ? $_POST['meeting_client'] : '';
					$data1['crm_assign_source'] = (isset($_POST['source']) && ($_POST['source'] != '')) ? $_POST['source'] : '';
					$data1['crm_refered_by'] = (isset($_POST['refered_by']) && ($_POST['refered_by'] != '')) ? $_POST['refered_by'] : '';
					$data1['crm_assign_relationship_client'] = (isset($_POST['relationship_client']) && ($_POST['relationship_client'] != '')) ? $_POST['relationship_client'] : '';
					$data1['crm_assign_notes'] = (isset($_POST['assign_notes']) && ($_POST['assign_notes'] != '')) ? $_POST['assign_notes'] : '';
					// other
					$data1['crm_previous_accountant'] = (isset($_POST['previous_account']) && ($_POST['previous_account'] != '')) ? $_POST['previous_account'] : '';
					$data1['crm_other_name_of_firm'] = (isset($_POST['name_of_firm']) && ($_POST['name_of_firm'] != '')) ? $_POST['name_of_firm'] : '';
					$data1['crm_other_address'] = (isset($_POST['other_address']) && ($_POST['other_address'] != '')) ? $_POST['other_address'] : '';
					$data1['crm_other_contact_no'] = (isset($_POST['pn_no_rec']) && ($_POST['pn_no_rec'] != '')) ? $_POST['cun_code'] . $_POST['pn_no_rec'] : '';
					$data1['crm_email'] = (isset($_POST['emailid']) && ($_POST['emailid'] != '')) ? $_POST['emailid'] : '';
					$data1['crm_other_chase_for_info'] = (isset($_POST['chase_for_info']) && ($_POST['chase_for_info'] != '')) ? $_POST['chase_for_info'] : '';
					$data1['crm_other_notes'] = (isset($_POST['other_notes']) && ($_POST['other_notes'] != '')) ? $_POST['other_notes'] : '';
					$data1['crm_other_internal_notes'] = (isset($_POST['other_internal_notes']) && ($_POST['other_internal_notes'] != '')) ? $_POST['other_internal_notes'] : '';
					$data1['crm_other_invite_use'] = (isset($_POST['invite_use']) && ($_POST['invite_use'] != '')) ? $_POST['invite_use'] : '';
					$data1['crm_other_crm'] = (isset($_POST['other_crm']) && ($_POST['other_crm'] != '')) ? $_POST['other_crm'] : '';
					$data1['crm_other_proposal'] = (isset($_POST['other_proposal']) && ($_POST['other_proposal'] != '')) ? $_POST['other_proposal'] : '';
					$data1['crm_other_task'] = (isset($_POST['other_task']) && ($_POST['other_task'] != '')) ? $_POST['other_task'] : '';
					$data1['crm_other_send_invit_link'] = (isset($_POST['send_invit_link']) && ($_POST['send_invit_link'] != '')) ? $_POST['send_invit_link'] : '';
					$data1['crm_other_username'] = (isset($_POST['user_name']) && ($_POST['user_name'] != '')) ? $_POST['user_name'] : '';
					$data1['crm_other_password'] = (isset($_POST['password']) && ($_POST['password'] != '')) ? $_POST['password'] : '';
					$data1['crm_other_any_notes'] = (isset($_POST['other_any_notes']) && ($_POST['other_any_notes'] != '')) ? $_POST['other_any_notes'] : '';
					// confirmation
					$data1['crm_confirmation_auth_code'] = (isset($_POST['confirmation_auth_code']) && ($_POST['confirmation_auth_code'] != '')) ? $_POST['confirmation_auth_code'] : '';
					$data1['crm_confirmation_statement_date'] = (isset($_POST['confirmation_next_made_up_to']) && ($_POST['confirmation_next_made_up_to'] != '')) ? date("Y-m-d", strtotime($_POST['confirmation_next_made_up_to'])) : '';
					$data1['crm_confirmation_statement_due_date'] = (isset($_POST['confirmation_next_due']) && ($_POST['confirmation_next_due'] != '')) ? date("Y-m-d", strtotime($_POST['confirmation_next_due'])) : '';
					$data1['crm_confirmation_email_remainder'] = (isset($_POST['confirmation_next_reminder']) && ($_POST['confirmation_next_reminder'] != '')) ? $_POST['confirmation_next_reminder'] : '';
					$data1['crm_confirmation_create_task_reminder'] = (isset($_POST['create_task_reminder']) && ($_POST['create_task_reminder'] != '')) ? $_POST['create_task_reminder'] : '';
					$data1['crm_confirmation_add_custom_reminder'] = (isset($_POST['add_custom_reminder']) && ($_POST['add_custom_reminder'] != '')) ? $_POST['add_custom_reminder'] : '';
					$data1['crm_confirmation_officers'] = (isset($_POST['confirmation_officers']) && ($_POST['confirmation_officers'] != '')) ? $_POST['confirmation_officers'] : '';
					$data1['crm_share_capital'] = (isset($_POST['share_capital']) && ($_POST['share_capital'] != '')) ? $_POST['share_capital'] : '';
					$data1['crm_shareholders'] = (isset($_POST['shareholders']) && ($_POST['shareholders'] != '')) ? $_POST['shareholders'] : '';
					$data1['crm_people_with_significant_control'] = (isset($_POST['people_with_significant_control']) && ($_POST['people_with_significant_control'] != '')) ? $_POST['people_with_significant_control'] : '';
					/*$data1['crm_confirmation_notes']=(isset($_POST['confirmation_notes'])&&($_POST['confirmation_notes']!=''))? $_POST['confirmation_notes'] : '';*/
					// accounts
					$data1['crm_accounts_auth_code'] = (isset($_POST['accounts_auth_code']) && ($_POST['accounts_auth_code'] != '')) ? $_POST['accounts_auth_code'] : '';
					$data1['crm_accounts_utr_number'] = (isset($_POST['accounts_utr_number']) && ($_POST['accounts_utr_number'] != '')) ? $_POST['accounts_utr_number'] : '';
					$data1['crm_companies_house_email_remainder'] = (isset($_POST['company_house_reminder']) && ($_POST['company_house_reminder'] != '')) ? $_POST['company_house_reminder'] : '';
					$data1['crm_ch_yearend'] = (isset($_POST['accounts_next_made_up_to']) && ($_POST['accounts_next_made_up_to'] != '')) ? date("Y-m-d", strtotime($_POST['accounts_next_made_up_to'])) : '';
					$data1['crm_ch_accounts_next_due'] = (isset($_POST['accounts_next_due']) && ($_POST['accounts_next_due'] != '')) ? date("Y-m-d", strtotime($_POST['accounts_next_due'])) : '';
					$data1['crm_accounts_next_reminder_date'] = (isset($_POST['accounts_next_reminder_date']) && ($_POST['accounts_next_reminder_date'] != '')) ? $_POST['accounts_next_reminder_date'] : '';
					$data1['crm_accounts_create_task_reminder'] = (isset($_POST['accounts_create_task_reminder']) && ($_POST['accounts_create_task_reminder'] != '')) ? $_POST['accounts_create_task_reminder'] : '';
					$data1['crm_accounts_custom_reminder'] = (isset($_POST['accounts_custom_reminder']) && ($_POST['accounts_custom_reminder'] != '')) ? $_POST['accounts_custom_reminder'] : '';
					$data1['crm_accounts_due_date_hmrc'] = (isset($_POST['accounts_due_date_hmrc']) && ($_POST['accounts_due_date_hmrc'] != '')) ? date("Y-m-d", strtotime($this->date_format($_POST['accounts_due_date_hmrc']))) : '';
					$data1['crm_accounts_tax_date_hmrc'] = (isset($_POST['accounts_tax_date_hmrc']) && ($_POST['accounts_tax_date_hmrc'] != '')) ? date("Y-m-d", strtotime($_POST['accounts_tax_date_hmrc'])) : '';
					$data1['crm_company_next_reminder_date'] = (isset($_POST['company_next_reminder_date']) && ($_POST['company_next_reminder_date'] != '')) ? $_POST['company_next_reminder_date'] : '';
					$data1['crm_company_create_task_reminder'] = (isset($_POST['company_create_task_reminder']) && ($_POST['company_create_task_reminder'] != '')) ? $_POST['company_create_task_reminder'] : '';
					$data1['crm_company_custom_reminder'] = (isset($_POST['company_custom_reminder']) && ($_POST['company_custom_reminder'] != '')) ? $_POST['company_custom_reminder'] : '';
						/* $data1['crm_accounts_notes']=(isset($_POST['accounts_notes'])&&($_POST['accounts_notes']!=''))? $_POST['accounts_notes'] : ''*/;
					// personal tax return
					$data1['crm_personal_utr_number'] = (isset($_POST['personal_utr_number']) && ($_POST['personal_utr_number'] != '')) ? $_POST['personal_utr_number'] : '';
					$data1['crm_ni_number'] = (isset($_POST['ni_number']) && ($_POST['ni_number'] != '')) ? $_POST['ni_number'] : '';
					$data1['crm_property_income'] = (isset($_POST['property_income']) && ($_POST['property_income'] != '')) ? $_POST['property_income'] : '';
					$data1['crm_additional_income'] = (isset($_POST['additional_income']) && ($_POST['additional_income'] != '')) ? $_POST['additional_income'] : '';
					$data1['crm_personal_tax_return_date'] = (isset($_POST['personal_tax_return_date']) && ($_POST['personal_tax_return_date'] != '')) ? date('Y-m-d', strtotime($this->date_format($_POST['personal_tax_return_date']))) : '';
					$data1['crm_personal_due_date_return'] = (isset($_POST['personal_due_date_return']) && ($_POST['personal_due_date_return'] != '')) ? date('Y-m-d', strtotime($_POST['personal_due_date_return'])) : '';
					$data1['crm_personal_due_date_online'] = (isset($_POST['personal_due_date_online']) && ($_POST['personal_due_date_online'] != '')) ?  date('Y-m-d', strtotime($_POST['personal_due_date_online'])) : '';
					$data1['crm_personal_next_reminder_date'] = (isset($_POST['personal_next_reminder_date']) && ($_POST['personal_next_reminder_date'] != '')) ? $_POST['personal_next_reminder_date'] : '';
					$data1['crm_personal_task_reminder'] = (isset($_POST['personal_task_reminder']) && ($_POST['personal_task_reminder'] != '')) ? $_POST['personal_task_reminder'] : '';
					$data1['crm_personal_custom_reminder'] = (isset($_POST['personal_custom_reminder']) && ($_POST['personal_custom_reminder'] != '')) ? $_POST['personal_custom_reminder'] : '';
					/* $data1['crm_personal_notes']=(isset($_POST['personal_notes'])&&($_POST['personal_notes']!=''))? $_POST['personal_notes'] : '';*/

					// payroll
					$data1['crm_payroll_acco_off_ref_no'] = (isset($_POST['payroll_acco_off_ref_no']) && ($_POST['payroll_acco_off_ref_no'] != '')) ? $_POST['payroll_acco_off_ref_no'] : '';
					$data1['crm_paye_off_ref_no'] = (isset($_POST['paye_off_ref_no']) && ($_POST['paye_off_ref_no'] != '')) ? $_POST['paye_off_ref_no'] : '';
					$data1['crm_payroll_reg_date'] = (isset($_POST['payroll_reg_date']) && ($_POST['payroll_reg_date'] != '')) ? date('Y-m-d', strtotime($_POST['payroll_reg_date'])) : '';
					$data1['crm_no_of_employees'] = (isset($_POST['no_of_employees']) && ($_POST['no_of_employees'] != '')) ? $_POST['no_of_employees'] : '';
					$data1['crm_first_pay_date'] = (isset($_POST['first_pay_date']) && ($_POST['first_pay_date'] != '')) ? date('Y-m-d', strtotime($_POST['first_pay_date'])) : '';
					$data1['crm_payroll_run'] = (isset($_POST['payroll_run']) && ($_POST['payroll_run'] != '')) ? $_POST['payroll_run'] : '';
					$data1['crm_payroll_run_date'] = (isset($_POST['payroll_run_date']) && ($_POST['payroll_run_date'] != '')) ? $this->date_format($_POST['payroll_run_date']) : '';
					$data1['crm_rti_deadline'] = (isset($_POST['rti_deadline']) && ($_POST['rti_deadline'] != '')) ? $_POST['rti_deadline'] : '';
					$data1['crm_previous_year_require'] = (isset($_POST['previous_year_require']) && ($_POST['previous_year_require'] != '')) ? $_POST['previous_year_require'] : '';
					$data1['crm_payroll_if_yes'] = (isset($_POST['payroll_if_yes']) && ($_POST['payroll_if_yes'] != '')) ? $_POST['payroll_if_yes'] : '';
					$data1['crm_paye_scheme_ceased'] = (isset($_POST['paye_scheme_ceased']) && ($_POST['paye_scheme_ceased'] != '')) ? date('Y-m-d', strtotime($_POST['paye_scheme_ceased'])) : '';
					$data1['crm_payroll_next_reminder_date'] = (isset($_POST['payroll_next_reminder_date']) && ($_POST['payroll_next_reminder_date'] != '')) ? $_POST['payroll_next_reminder_date'] : '';
					$data1['crm_payroll_create_task_reminder'] = (isset($_POST['payroll_create_task_reminder']) && ($_POST['payroll_create_task_reminder'] != '')) ? $_POST['payroll_create_task_reminder'] : '';
					$data1['crm_payroll_add_custom_reminder'] = (isset($_POST['payroll_add_custom_reminder']) && ($_POST['payroll_add_custom_reminder'] != '')) ? $_POST['payroll_add_custom_reminder'] : '';
					$data1['crm_staging_date'] = (isset($_POST['staging_date']) && ($_POST['staging_date'] != '')) ? date('Y-m-d', strtotime($_POST['staging_date'])) : '';
					$data1['crm_pension_id'] = (isset($_POST['pension_id']) && ($_POST['pension_id'] != '')) ? $_POST['pension_id'] : '';
					$data1['crm_pension_subm_due_date'] = (isset($_POST['pension_subm_due_date']) && ($_POST['pension_subm_due_date'] != '')) ? $this->date_format($_POST['pension_subm_due_date']) : '';
					$data1['crm_postponement_date'] = (isset($_POST['defer_post_upto']) && ($_POST['defer_post_upto'] != '')) ? $_POST['defer_post_upto'] : '';
					$data1['crm_the_pensions_regulator_opt_out_date'] = (isset($_POST['pension_regulator_date']) && ($_POST['pension_regulator_date'] != '')) ? $_POST['pension_regulator_date'] : '';
					$data1['crm_re_enrolment_date'] = (isset($_POST['paye_re_enrolment_date']) && ($_POST['paye_re_enrolment_date'] != '')) ? $_POST['paye_re_enrolment_date'] : '';
					$data1['crm_declaration_of_compliance_due_date  '] = (isset($_POST['declaration_of_compliance_due_date']) && ($_POST['declaration_of_compliance_due_date'] != '')) ? $this->date_format($_POST['declaration_of_compliance_due_date']) : '';
					$data1['crm_declaration_of_compliance_submission'] = (isset($_POST['declaration_of_compliance_last_filed']) && ($_POST['declaration_of_compliance_last_filed'] != '')) ? $_POST['declaration_of_compliance_last_filed'] : '';
					$data1['crm_paye_pension_provider'] = (isset($_POST['paye_pension_provider']) && ($_POST['paye_pension_provider'] != '')) ? $_POST['paye_pension_provider'] : '';
					$data1['crm_pension_id'] = (isset($_POST['paye_pension_provider_userid']) && ($_POST['paye_pension_provider_userid'] != '')) ? $_POST['paye_pension_provider_userid'] : '';
					$data1['crm_paye_pension_provider_password'] = (isset($_POST['paye_pension_provider_password']) && ($_POST['paye_pension_provider_password'] != '')) ? $_POST['paye_pension_provider_password'] : '';
					$data1['crm_employer_contri_percentage'] = (isset($_POST['employer_contri_percentage']) && ($_POST['employer_contri_percentage'] != '')) ? $_POST['employer_contri_percentage'] : '';
					$data1['crm_employee_contri_percentage'] = (isset($_POST['employee_contri_percentage']) && ($_POST['employee_contri_percentage'] != '')) ? $_POST['employee_contri_percentage'] : '';

					$data1['crm_pension_notes'] = (isset($_POST['pension_notes']) && ($_POST['pension_notes'] != '')) ? $_POST['pension_notes'] : '';
					$data1['crm_pension_next_reminder_date'] = (isset($_POST['pension_next_reminder_date']) && ($_POST['pension_next_reminder_date'] != '')) ? $_POST['pension_next_reminder_date'] : '';
					$data1['crm_pension_create_task_reminder'] = (isset($_POST['pension_create_task_reminder']) && ($_POST['pension_create_task_reminder'] != '')) ? $_POST['pension_create_task_reminder'] : '';
					$data1['crm_pension_add_custom_reminder'] = (isset($_POST['pension_add_custom_reminder']) && ($_POST['pension_add_custom_reminder'] != '')) ? $_POST['pension_add_custom_reminder'] : '';
					$data1['crm_cis_contractor'] = (isset($_POST['cis_contractor']) && ($_POST['cis_contractor'] != '')) ? $_POST['cis_contractor'] : '';
					$data1['crm_cis_contractor_start_date'] = (isset($_POST['cis_contractor_start_date']) && ($_POST['cis_contractor_start_date'] != '')) ? $this->date_format($_POST['cis_contractor_start_date']) : '';
					$data1['crm_cis_scheme_notes'] = (isset($_POST['cis_scheme_notes']) && ($_POST['cis_scheme_notes'] != '')) ? $_POST['cis_scheme_notes'] : '';
					$data1['crm_cis_subcontractor'] = (isset($_POST['cis_subcontractor']) && ($_POST['cis_subcontractor'] != '')) ? $_POST['cis_subcontractor'] : '';
					$data1['crm_cis_subcontractor_start_date'] = (isset($_POST['cis_subcontractor_start_date']) && ($_POST['cis_subcontractor_start_date'] != '')) ? $this->date_format($_POST['cis_subcontractor_start_date']) : '';
					$data1['crm_cis_subcontractor_scheme_notes'] = (isset($_POST['cis_subcontractor_scheme_notes']) && ($_POST['cis_subcontractor_scheme_notes'] != '')) ? $_POST['cis_subcontractor_scheme_notes'] : '';
					$data1['crm_cis_next_reminder_date'] = (isset($_POST['cis_next_reminder_date']) && ($_POST['cis_next_reminder_date'] != '')) ? $_POST['cis_next_reminder_date'] : '';
					$data1['crm_cis_create_task_reminder'] = (isset($_POST['cis_create_task_reminder']) && ($_POST['cis_create_task_reminder'] != '')) ? $_POST['cis_create_task_reminder'] : '';
					$data1['crm_cis_add_custom_reminder'] = (isset($_POST['cis_add_custom_reminder']) && ($_POST['cis_add_custom_reminder'] != '')) ? $_POST['cis_add_custom_reminder'] : '';
					/** 11-09-2018 **/
					$data1['crm_cissub_next_reminder_date'] = (isset($_POST['cissub_next_reminder_date']) && ($_POST['cissub_next_reminder_date'] != '')) ? $_POST['cissub_next_reminder_date'] : '';
					$data1['crm_cissub_create_task_reminder'] = (isset($_POST['cissub_create_task_reminder']) && ($_POST['cissub_create_task_reminder'] != '')) ? $_POST['cissub_create_task_reminder'] : '';
					$data1['crm_cissub_add_custom_reminder'] = (isset($_POST['cissub_add_custom_reminder']) && ($_POST['cissub_add_custom_reminder'] != '')) ? $_POST['cissub_add_custom_reminder'] : '';
					/** end of 11-09-2018 **/
					$data1['crm_p11d_latest_action_date'] = (isset($_POST['p11d_start_date']) && ($_POST['p11d_start_date'] != '')) ? date('Y-m-d', strtotime($_POST['p11d_start_date'])) : '';
					$data1['crm_p11d_latest_action'] = (isset($_POST['p11d_todo']) && ($_POST['p11d_todo'] != '')) ? $_POST['p11d_todo'] : '';
					$data1['crm_latest_p11d_submitted'] = (isset($_POST['p11d_first_benefit_date']) && ($_POST['p11d_first_benefit_date'] != '')) ? date('Y-m-d', strtotime($_POST['p11d_first_benefit_date'])) : '';
					$data1['crm_next_p11d_return_due'] = (isset($_POST['p11d_due_date']) && ($_POST['p11d_due_date'] != '')) ? $this->date_format($_POST['p11d_due_date']) : '';
					$data1['crm_p11d_previous_year_require'] = (isset($_POST['p11d_previous_year_require']) && ($_POST['p11d_previous_year_require'] != '')) ? $_POST['p11d_previous_year_require'] : '';
					$data1['crm_p11d_payroll_if_yes'] = (isset($_POST['p11d_payroll_if_yes']) && ($_POST['p11d_payroll_if_yes'] != '')) ? $_POST['p11d_payroll_if_yes'] : '';
					$data1['crm_p11d_records_received'] = (isset($_POST['p11d_paye_scheme_ceased']) && ($_POST['p11d_paye_scheme_ceased'] != '')) ? $_POST['p11d_paye_scheme_ceased'] : '';
					$data1['crm_p11d_next_reminder_date'] = (isset($_POST['p11d_next_reminder_date']) && ($_POST['p11d_next_reminder_date'] != '')) ? $_POST['p11d_next_reminder_date'] : '';
					$data1['crm_p11d_create_task_reminder'] = (isset($_POST['p11d_create_task_reminder']) && ($_POST['p11d_create_task_reminder'] != '')) ? $_POST['p11d_create_task_reminder'] : '';
					$data1['crm_p11d_add_custom_reminder'] = (isset($_POST['p11d_add_custom_reminder']) && ($_POST['p11d_add_custom_reminder'] != '')) ? $_POST['p11d_add_custom_reminder'] : '';
					/* $data1['crm_p11d_notes']=(isset($_POST['p11d_notes'])&&($_POST['p11d_notes']!=''))? $_POST['p11d_notes'] : '';*/ //vat return
					$data1['crm_vat_number_one'] = (isset($_POST['vat_number_one']) && ($_POST['vat_number_one'] != '')) ? $_POST['vat_number_one'] : '';
					$data1['crm_vat_date_of_registration'] = (isset($_POST['vat_registration_date']) && ($_POST['vat_registration_date'] != '')) ? $_POST['vat_registration_date'] : '';
					$data1['crm_vat_frequency'] = (isset($_POST['vat_frequency']) && ($_POST['vat_frequency'] != '')) ? $_POST['vat_frequency'] : '';
					$data1['crm_vat_quater_end_date'] = (isset($_POST['vat_quater_end_date']) && ($_POST['vat_quater_end_date'] != '')) ? date('Y-m-d', strtotime($_POST['vat_quater_end_date'])) : '';
					$data1['crm_vat_quarters'] = (isset($_POST['vat_quarters']) && ($_POST['vat_quarters'] != '')) ? $_POST['vat_quarters'] : '0';
					$data1['crm_last_vat_return_filed_upto'] = (isset($_POST['crm_last_vat_return_filed_upto']) && ($_POST['crm_last_vat_return_filed_upto'] != '')) ? date('Y-m-d', strtotime($_POST['crm_last_vat_return_filed_upto'])) : '';
					$data1['crm_vat_scheme'] = (isset($_POST['vat_scheme']) && ($_POST['vat_scheme'] != '')) ? $_POST['vat_scheme'] : '';
					$data1['crm_flat_rate_category'] = (isset($_POST['flat_rate_category']) && ($_POST['flat_rate_category'] != '')) ? $_POST['flat_rate_category'] : '';
					$data1['crm_flat_rate_percentage'] = (isset($_POST['flat_rate_percentage']) && ($_POST['flat_rate_percentage'] != '')) ? $_POST['flat_rate_percentage'] : '';
					$data1['crm_direct_debit'] = (isset($_POST['direct_debit']) && ($_POST['direct_debit'] != '')) ? $_POST['direct_debit'] : '';
					$data1['crm_annual_accounting_scheme'] = (isset($_POST['annual_accounting_scheme']) && ($_POST['annual_accounting_scheme'] != '')) ? $_POST['annual_accounting_scheme'] : '';
					$data1['crm_box5_of_last_quarter_submitted'] = (isset($_POST['box5_of_last_quarter_submitted']) && ($_POST['box5_of_last_quarter_submitted'] != '')) ? $_POST['box5_of_last_quarter_submitted'] : '0';
					$data1['crm_vat_address'] = (isset($_POST['vat_address']) && ($_POST['vat_address'] != '')) ? $_POST['vat_address'] : '';
					$data1['crm_vat_next_reminder_date'] = (isset($_POST['vat_next_reminder_date']) && ($_POST['vat_next_reminder_date'] != '')) ? $_POST['vat_next_reminder_date'] : '';
					$data1['crm_vat_create_task_reminder'] = (isset($_POST['vat_create_task_reminder']) && ($_POST['vat_create_task_reminder'] != '')) ? $_POST['vat_create_task_reminder'] : '';
					$data1['crm_vat_add_custom_reminder'] = (isset($_POST['vat_add_custom_reminder']) && ($_POST['vat_add_custom_reminder'] != '')) ? $_POST['vat_add_custom_reminder'] : '';
					/* $data1['crm_vat_notes']=(isset($_POST['vat_notes'])&&($_POST['vat_notes']!=''))? $_POST['vat_notes'] : '';*/
					//management accounts
					$data1['crm_bookkeeping'] = (isset($_POST['bookkeeping']) && ($_POST['bookkeeping'] != '')) ? $_POST['bookkeeping'] : '';
					$data1['crm_next_booking_date'] = (isset($_POST['next_booking_date']) && ($_POST['next_booking_date'] != '')) ? $this->date_format($_POST['next_booking_date']) : '';
					$data1['crm_method_bookkeeping'] = (isset($_POST['method_bookkeeping']) && ($_POST['method_bookkeeping'] != '')) ? $_POST['method_bookkeeping'] : '';
					$data1['crm_client_provide_record'] = (isset($_POST['client_provide_record']) && ($_POST['client_provide_record'] != '')) ? $_POST['client_provide_record'] : '';
					$data1['crm_bookkeep_next_reminder_date'] = (isset($_POST['bookkeep_next_reminder_date']) && ($_POST['bookkeep_next_reminder_date'] != '')) ? $_POST['bookkeep_next_reminder_date'] : '';
					$data1['crm_bookkeep_create_task_reminder'] = (isset($_POST['bookkeep_create_task_reminder']) && ($_POST['bookkeep_create_task_reminder'] != '')) ? $_POST['bookkeep_create_task_reminder'] : '';
					$data1['crm_bookkeep_add_custom_reminder'] = (isset($_POST['bookkeep_add_custom_reminder']) && ($_POST['bookkeep_add_custom_reminder'] != '')) ? $_POST['bookkeep_add_custom_reminder'] : '';
					$data1['crm_manage_acc_fre'] = (isset($_POST['manage_acc_fre']) && ($_POST['manage_acc_fre'] != '')) ? $_POST['manage_acc_fre'] : '';
					$data1['crm_next_manage_acc_date'] = (isset($_POST['next_manage_acc_date']) && ($_POST['next_manage_acc_date'] != '')) ? $this->date_format($_POST['next_manage_acc_date']) : '';
					$data1['crm_manage_method_bookkeeping'] = (isset($_POST['manage_method_bookkeeping']) && ($_POST['manage_method_bookkeeping'] != '')) ? $_POST['manage_method_bookkeeping'] : '';
					$data1['crm_manage_client_provide_record'] = (isset($_POST['manage_client_provide_record']) && ($_POST['manage_client_provide_record'] != '')) ? $_POST['manage_client_provide_record'] : '';
					$data1['crm_manage_next_reminder_date'] = (isset($_POST['manage_next_reminder_date']) && ($_POST['manage_next_reminder_date'] != '')) ? $_POST['manage_next_reminder_date'] : '';
					$data1['crm_manage_create_task_reminder'] = (isset($_POST['manage_create_task_reminder']) && ($_POST['manage_create_task_reminder'] != '')) ? $_POST['manage_create_task_reminder'] : '';
					$data1['crm_manage_add_custom_reminder'] = (isset($_POST['manage_add_custom_reminder']) && ($_POST['manage_add_custom_reminder'] != '')) ? $_POST['manage_add_custom_reminder'] : '';
					/*  $data1['crm_manage_notes']=(isset($_POST['manage_notes'])&&($_POST['manage_notes']!=''))? $_POST['manage_notes'] : '';*/
					$data1['crm_invesitgation_insurance'] = (isset($_POST['insurance_start_date']) && ($_POST['insurance_start_date'] != '')) ? $this->date_format($_POST['insurance_start_date']) : '';
					$data1['crm_insurance_renew_date'] = (isset($_POST['insurance_renew_date']) && ($_POST['insurance_renew_date'] != '')) ? $this->date_format($_POST['insurance_renew_date']) : '';
					$data1['crm_insurance_provider'] = (isset($_POST['insurance_provider']) && ($_POST['insurance_provider'] != '')) ? $_POST['insurance_provider'] : '';
					$data1['crm_claims_note'] = (isset($_POST['claims_note']) && ($_POST['claims_note'] != '')) ? $_POST['claims_note'] : '';
					$data1['crm_insurance_next_reminder_date'] = (isset($_POST['insurance_next_reminder_date']) && ($_POST['insurance_next_reminder_date'] != '')) ? $_POST['insurance_next_reminder_date'] : '';
					$data1['crm_insurance_create_task_reminder'] = (isset($_POST['insurance_create_task_reminder']) && ($_POST['insurance_create_task_reminder'] != '')) ? $_POST['insurance_create_task_reminder'] : '';
					$data1['crm_insurance_add_custom_reminder'] = (isset($_POST['insurance_add_custom_reminder']) && ($_POST['insurance_add_custom_reminder'] != '')) ? $_POST['insurance_add_custom_reminder'] : '';
					$data1['crm_registered_start_date'] = (isset($_POST['registered_start_date']) && ($_POST['registered_start_date'] != '')) ? $this->date_format($_POST['registered_start_date']) : '';
					$data1['crm_registered_renew_date'] = (isset($_POST['registered_renew_date']) && ($_POST['registered_renew_date'] != '')) ? $this->date_format($_POST['registered_renew_date']) : '';
					$data1['crm_registered_office_inuse'] = (isset($_POST['registered_office_inuse']) && ($_POST['registered_office_inuse'] != '')) ? $_POST['registered_office_inuse'] : '';
					$data1['crm_registered_claims_note'] = (isset($_POST['registered_claims_note']) && ($_POST['registered_claims_note'] != '')) ? $_POST['registered_claims_note'] : '';
					$data1['crm_registered_next_reminder_date'] = (isset($_POST['registered_next_reminder_date']) && ($_POST['registered_next_reminder_date'] != '')) ? $_POST['registered_next_reminder_date'] : '';
					$data1['crm_registered_create_task_reminder'] = (isset($_POST['registered_create_task_reminder']) && ($_POST['registered_create_task_reminder'] != '')) ? $_POST['registered_create_task_reminder'] : '';
					$data1['crm_registered_add_custom_reminder'] = (isset($_POST['registered_add_custom_reminder']) && ($_POST['registered_add_custom_reminder'] != '')) ? $_POST['registered_add_custom_reminder'] : '';
					$data1['crm_investigation_start_date'] = (isset($_POST['investigation_start_date']) && ($_POST['investigation_start_date'] != '')) ? $this->date_format($_POST['investigation_start_date']) : '';
					$data1['crm_investigation_end_date'] = (isset($_POST['investigation_end_date']) && ($_POST['investigation_end_date'] != '')) ? $this->date_format($_POST['investigation_end_date']) : '';
					$data1['crm_investigation_note'] = (isset($_POST['investigation_note']) && ($_POST['investigation_note'] != '')) ? $_POST['investigation_note'] : '';
					$data1['crm_investigation_next_reminder_date'] = (isset($_POST['investigation_next_reminder_date']) && ($_POST['investigation_next_reminder_date'] != '')) ? $_POST['investigation_next_reminder_date'] : '';
					$data1['crm_investigation_create_task_reminder'] = (isset($_POST['investigation_create_task_reminder']) && ($_POST['investigation_create_task_reminder'] != '')) ? $_POST['investigation_create_task_reminder'] : '';
					$data1['crm_investigation_add_custom_reminder'] = (isset($_POST['investigation_add_custom_reminder']) && ($_POST['investigation_add_custom_reminder'] != '')) ? $_POST['investigation_add_custom_reminder'] : '';
					/* $data1['crm_tax_investigation_note']=(isset($_POST['tax_investigation_note'])&&($_POST['tax_investigation_note']!=''))? $_POST['tax_investigation_note'] : '';*/
					$data1['select_responsible_type'] = (isset($_POST['select_responsible_type']) && ($_POST['select_responsible_type'] != '')) ? $_POST['select_responsible_type'] : '';

					// notes tab
					$data1['crm_notes_info'] = (isset($_POST['notes_info']) && ($_POST['notes_info'] != '')) ? $_POST['notes_info'] : '';
					/** activity log for notes **/
					if (isset($_POST['notes_info']) && ($_POST['notes_info'] != '')) {

						$activity_datas['log'] = "Notes Added ---" . $_POST['notes_info'];
						$activity_datas['createdTime'] = time();
						$activity_datas['module'] = 'Client';
						$activity_datas['sub_module'] = 'Client Notes';
						$activity_datas['user_id'] = $_SESSION['admin_id'];
						$activity_datas['module_id'] = (isset($_POST['user_id']) && ($_POST['user_id'] == '')) ? $in : $_POST['user_id'];

						$this->Common_mdl->insert('activity_log', $activity_datas);
					}
					/** end of activity log for notes **/
					// documents tab
					$data1['crm_documents'] = (isset($_FILES['document']['name']) && ($_FILES['document']['name'] != '')) ? $this->Common_mdl->do_upload($_FILES['document'], 'uploads') : '';
					$data1['created_date'] = time();
					$data1['autosave_status'] = 0;
					$data1['firm_id'] = $firm_id;

					$data_inv['user_id'] = (isset($_POST['user_id']) && ($_POST['user_id'] == '')) ? $in : $_POST['user_id'];
					$data_inv['confirmation_invoice'] = (isset($_POST['invoice_cs']) && ($_POST['invoice_cs'] != '')) ? $_POST['invoice_cs'] : '';
					$data_inv['accounts_invoice'] = (isset($_POST['invoice_as']) && ($_POST['invoice_as'] != '')) ? $_POST['invoice_as'] : '';
					$data_inv['company_tax_invoice'] = (isset($_POST['invoice_ct']) && ($_POST['invoice_ct'] != '')) ? $_POST['invoice_ct'] : '';
					$data_inv['personal_tax_invoice'] = (isset($_POST['invoice_pt']) && ($_POST['invoice_pt'] != '')) ? $_POST['invoice_pt'] : '';
					$data_inv['payroll_invoice'] = (isset($_POST['invoice_pr']) && ($_POST['invoice_pr'] != '')) ? $_POST['invoice_pr'] : '';
					$data_inv['work_pension_invoice'] = (isset($_POST['invoice_pn']) && ($_POST['invoice_pn'] != '')) ? $_POST['invoice_pn'] : '';
					$data_inv['cis_invoice'] = (isset($_POST['invoice_cis']) && ($_POST['invoice_cis'] != '')) ? $_POST['invoice_cis'] : '';
					$data_inv['cissub_invoice'] = (isset($_POST['invoice_sub']) && ($_POST['invoice_sub'] != '')) ? $_POST['invoice_sub'] : '';
					$data_inv['p11d_invoice'] = (isset($_POST['invoice_p11d']) && ($_POST['invoice_p11d'] != '')) ? $_POST['invoice_p11d'] : '';
					$data_inv['vat_invoice'] = (isset($_POST['invoice_vt']) && ($_POST['invoice_vt'] != '')) ? $_POST['invoice_vt'] : '';
					$data_inv['bookkeep_invoice'] = (isset($_POST['invoice_book']) && ($_POST['invoice_book'] != '')) ? $_POST['invoice_book'] : '';
					$data_inv['management_invoice'] = (isset($_POST['invoice_ma']) && ($_POST['invoice_ma'] != '')) ? $_POST['invoice_ma'] : '';
					$data_inv['invest_insurance_invoice'] = (isset($_POST['invoice_insuance']) && ($_POST['invoice_insuance'] != '')) ? $_POST['invoice_insuance'] : '';
					$data_inv['registered_invoice'] = (isset($_POST['invoice_reg']) && ($_POST['invoice_reg'] != '')) ? $_POST['invoice_reg'] : '';
					$data_inv['tax_advice_invoice'] = (isset($_POST['invoice_taxadv']) && ($_POST['invoice_taxadv'] != '')) ? $_POST['invoice_taxadv'] : '';
					$data_inv['tax_invest_invoice'] = (isset($_POST['invoice_taxinves']) && ($_POST['invoice_taxinves'] != '')) ? $_POST['invoice_taxinves'] : '';
					$data_inv['created_date'] = time();
					$this->db->insert('      ', $data_inv);
					$success = $this->Common_mdl->update_field($this->input->post(), $data_inv['user_id']);
					/* ******  */
					$datalegal['user_id'] = $_POST['user_id'];
					$datalegal['company_no'] = isset($_POST['fa_cmyno']) ? $_POST['fa_cmyno'] : "";
					$datalegal['incorporation_date'] = isset($_POST['fa_incordate']) ? $_POST['fa_incordate'] : "";
					$datalegal['regist_address'] = isset($_POST['fa_registadres']) ? $_POST['fa_registadres'] : "";
					$datalegal['turnover'] = isset($_POST['fa_turnover']) ? $_POST['fa_turnover'] : "";
					$datalegal['date_of_trading'] = isset($_POST['fa_dateoftrading']) ? $_POST['fa_dateoftrading'] : "";
					$datalegal['sic_code'] = isset($_POST['fa_siccode']) ? $_POST['fa_siccode'] : "";
					$datalegal['nuture_of_business'] = isset($_POST['fa_nutureofbus']) ? $_POST['fa_nutureofbus'] : "";
					$datalegal['company_utr'] = isset($_POST['fa_cmyutr']) ? $_POST['fa_cmyutr'] : "";
					$datalegal['company_house_auth_code'] = isset($_POST['fa_cmyhouse']) ? $_POST['fa_cmyhouse'] : "";
					$datalegal['trading_as'] = isset($_POST['bus_tradingas']) ? $_POST['bus_tradingas'] : "";
					$datalegal['commenced_trading'] = isset($_POST['bus_commencedtrading']) ? $_POST['bus_commencedtrading'] : "";
					$datalegal['register_sa'] = isset($_POST['bus_regist']) ? $_POST['bus_regist'] : "";
					$datalegal['bus_turnover'] = isset($_POST['bus_turnover']) ? $_POST['bus_turnover'] : "";
					$datalegal['bus_nuture_of_business'] = isset($_POST['bus_nutureofbus']) ? $_POST['bus_nutureofbus'] : "";
					/* ****** */
					$c_id = $_POST['user_id'];
					$sql_c = $this->db->query("select * from client where user_id = $c_id")->row_array();
					if (!empty($sql_c)) {
						$updates_client =  $this->Common_mdl->update('client', $data1, 'user_id', $_POST['user_id']);
					} else {
						$updates_client =  $this->db->insert('client', $data1);
						$id = $this->db->insert_id();

						$log = "Client Create From Proposals Accept Section";
						$this->Common_mdl->Create_Log('client', $c_id, $log);

						$this->Proposal_model->addSubscription($_POST['proposal_id'], $id);
						$this->Common_mdl->createparentDirectory($data1['user_id']);

						$data = ['firm_id' => $firm_id, 'module_name' => 'CLIENT', 'module_id' => $id, 'assignees' => $assignees];
						$this->db->insert('firm_assignees', $data);

						$this->Missing_task($id, $lead_id);
						$legalFrom = $this->db->insert('client_legalform', $datalegal);
					}
					/** 06-07-2018 rs for auto cretaed task**/
					$add_reminder_ids =  isset($_POST['add_reminder_ids']) ? $_POST['add_reminder_ids'] : "";
					if ($add_reminder_ids != '') {
						$ex_reminder_ids = explode(',', $add_reminder_ids);
						foreach ($ex_reminder_ids as $reid_key => $reid_value) {
							$re_data['user_id'] = $_POST['user_id'];
							$update_rem_ids =  $this->Common_mdl->update('reminder_setting', $re_data, 'id', $reid_value);
						}
					}

					if ($updates_client && isset($_POST['chase_for_info']) == 'on') {
						$query = $this->db->query('select * from user where id="' . $_POST['user_id'] . '"')->row_array();
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);

						$email_subject  = 'Email For Others';

						$body  = 'Others Chase for Information Mail <a href=' . base_url() . ' style="display: inline-block;text-align: center;color: #00aeef;text-decoration: none;font-family:;background: #00aeef;color: #fff;font-weight: 600;padding: 5px 10px;border-radius: 3px;">Click Here</a><p>Notes :' . $_POST['other_notes'] . '</p>';

						//email template
						$body = $this->load->view('remainder_email.php', $data2, TRUE);

						$send = firm_settings_send_mail($query['firm_id'], $query['crm_email_id'], $email_subject, $body);
					}


					$email = $this->input->post('emailid');
					$query = $this->db->query('select * from user where crm_email_id="' . $_POST['emailid'] . '"')->row_array();
					if ($query['username'] != '') {

						$user_id = $query['id'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Welcome Email';
						$body  = 'Hi ' . $query['username'] . '</br>we got a request to reset your password.</br><a href=' . base_url() . 'login/resetPwd/' . $encrypt_id . ' style="display: inline-block;text-align: center;color: #00aeef;text-decoration: none;font-family:;background: #00aeef;color: #fff;font-weight: 600;padding: 5px 10px;border-radius: 3px;">Click Here</a> for Reset Your CRM Account Password</br>if you ignore this message, your password won\'t be changed.';

						$this->session->set_flashdata('email', $email);
						$send = firm_settings_send_mail($query['firm_id'], $email, $email_subject, $body);
					}
					if (!empty($_POST['user_id'])) {
						$return = $_POST['user_id'];
					} else {
						$return = $in;
					}
				} else {
					$return = '0';
				}
			}
			if (isset($_POST['chase_for_info']) && $_POST['chase_for_info'] != '') {
				if ($in == '0') {
					$ct_id = $_POST['user_id'];
				} else {
					$ct_id = $in;
				}
				$this->Task_creating_model->client_create_task($ct_id);
			}
		} elseif ($_POST['user_id'] != '' && isset($_POST['user_id'])) {
			$return = $this->updates_client($_POST['user_id'], $proposal_id);
		} else {
			$return = '0';
		}

		echo json_encode(['return' => $return]);
		exit;
	}

	public function service_settings()
	{
		if ($_SESSION['role'] == '4') {
			$data['getCompanyvalue'] = $this->db->query("SELECT * FROM client WHERE user_id ='" . $_SESSION['id'] . "'")->row_array();
			$data['manager'] = $this->Common_mdl->GetAllWithWheretwo('user', 'role', '5', 'status', 1);
			$this->load->view('users/service_enable', $data);
		} else {
			redirect('user');
		}
	}
	public function get_responsple_interlink()
	{
		$return = ['team' => [], 'dept' => [], 'staff' => []];
		$ids = (($_POST['ids'] != '') ? explode(",", $_POST['ids']) : []);

		if ($_POST['trigger'] == 'team') {
			//get team staff
			$staff_array = $this->db->query("select * from team_assign_staff where team_id IN (" . implode(',', $ids) . ") ")->result_array();

			$staff = [];

			foreach ($staff_array as $arr) {
				$staff = array_merge($staff, explode(",", $arr['staff_id']));
			}
			//get team depat
			$dept = $this->db->query("SELECT * FROM `department_assign_team` where CONCAT(',', `team_id`, ',') REGEXP ',(" . implode('|', $ids) . "),'")->result_array();

			$return['staff'] = $staff;
			$return['dept'] = array_column($dept, 'depart_id');
			$return['team'] = $ids;
		} else if ($_POST['trigger'] == 'dept') {
			//get dept team
			$team_array = $this->db->query("select * from department_assign_team where depart_id IN (" . implode(',', $ids) . ") ")->result_array();
			$team = [];

			foreach ($team_array as $arr) {
				$arr['team_id'] = (!empty($arr['team_id']) ? $arr['team_id'] : []);

				$team = array_merge($team, explode(",", $arr['team_id']));
			}
			//get dept->team staff
			$staff = [];
			if (count($team)) {
				$staff_array = $this->db->query("select * from team_assign_staff where team_id IN (" . implode(',', $team) . ") ")->result_array();

				foreach ($staff_array as $arr) {
					$staff = array_merge($team, explode(",", $arr['staff_id']));
				}
			}
			$return['staff'] = $staff;
			$return['dept'] = $ids;
			$return['team'] = $team;
		} elseif ($_POST['trigger'] == 'staff') {
			$team_array = $this->db->query("SELECT * FROM `team_assign_staff` where CONCAT(',', `staff_id`, ',') REGEXP ',(" . implode('|', $ids) . "),'")->result_array();
			$team = array_column($team_array, 'team_id');

			$dept = [];
			if (count($team)) {
				$dept_array = $this->db->query("SELECT * FROM `department_assign_team` where CONCAT(',', `team_id`, ',') REGEXP ',(" . implode('|', $team) . "),'")->result_array();
				$dept = array_column($dept_array, 'depart_id');
			}
			$return['staff'] = $ids;
			$return['dept'] = $dept;
			$return['team'] = $team;
		}
		//print_r($return);
		//if any empty

		$return = array_map(function ($a) {
			if (empty($a)) return ['0'];
			else return  $a;
		}, $return);
		//print_r($return);

		// get staff manager
		foreach ($return['staff'] as $sk => $sv) {

			$manger_array = $this->db->query("select * from assign_manager where  FIND_IN_SET('" . $sv . "',staff) ")->row_array();
			$manger_array['manager'] = (!empty($manger_array['manager']) ? $manger_array['manager'] : '');

			$return['staff'][$sk] = [$sv, $manger_array['manager']];
		}

		echo json_encode($return);
	}


	public function client_infomation_design($user_id = false)
	{
		error_reporting(0);
		$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$this->session->set_userdata('mail_url', $actual_link);
		$this->Security_model->chk_login();
		$data['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user', 'role', '1');
		$data['client'] = $this->Common_mdl->GetAllWithWhere('client', 'user_id', $user_id);
		$data['responsible_team'] = $this->Common_mdl->GetAllWithWhere('responsible_team', 'client_id', $user_id);
		$data['responsible_user'] = $this->Common_mdl->GetAllWithWhere('responsible_user', 'client_id', $user_id);
		$data['responsible_department'] = $this->Common_mdl->GetAllWithWhere('responsible_department', 'client_id', $user_id);
		$data['responsible_member'] = $this->Common_mdl->GetAllWithWhere('responsible_members', 'client_id', $user_id);
		//$data['contactRec']=$this->Common_mdl->GetAllWithWhere('client_contacts','client_id',$user_id);
		$data['contactRec'] = $this->db->query("SELECT * FROM  `client_contacts` WHERE client_id =  '" . $user_id . "' ORDER BY make_primary DESC ")->result_array();
		$data['user'] = $this->Common_mdl->GetAllWithWhere('user', 'id', $user_id);

		$data['staff_form'] = $this->Common_mdl->GetAllWithWheretwo('user', 'role', '6', 'firm_admin_id', $_SESSION['id']);
		// $data['referby'] = $this->db->query("SELECT * FROM `client` where crm_first_name!='' and crm_first_name!='0' GROUP BY `crm_first_name`")->result_array();
		/** 21-08-2018 **/
		$query1 = $this->db->query("SELECT * FROM `user` where role=4 AND autosave_status!='1' and crm_name!='' and firm_admin_id=" . $_SESSION['id'] . " and status=1 order by id DESC");
		$results1 = $query1->result_array();
		$res = array();
		if (count($results1) > 0) {
			foreach ($results1 as $key => $value) {
				array_push($res, $value['id']);
			}
		}
		if (!empty($res)) {
			$im_val = implode(',', $res);
			//echo $im_val."abc abc";
			$data['referby'] = $this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id in (" . $im_val . ") order by id desc ")->result_array();
			//$results = $query->result_array(); 
		} else {
			$data['referby'] = array();
		}
		/** end of 21-08-2018 **/
		$data['teamlist'] = $this->Common_mdl->getallrecords('team');
		$data['deptlist'] = $this->Common_mdl->getallrecords('department');
		// $data['staff_form'] = $this->Common_mdl->GetAllWithWhere('user','role','6');
		// $data['managed_by'] = $this->Common_mdl->GetAllWithWhere('user','role','5');
		$data['staff_form'] = $this->db->query("select * from user where role in(5,6,3) and firm_admin_id=" . $_SESSION['id'] . " ")->result_array();
		$data['managed_by'] = $this->db->query("select * from user where role in(5,3) and firm_admin_id=" . $_SESSION['id'] . " ")->result_array();
		if ($user_id != '') {
			$query = $this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id='" . $user_id . "' order by id desc ")->result_array();
			$res = array();
			foreach ($query as $key => $value) {
				array_push($res, $value['id']);
			}
			$im_val = implode(',', $res);
			$query_res = $this->db->query("SELECT * FROM add_new_task WHERE company_name in ('" . $im_val . "') order by id desc ");
			// $data['task_list']=$this->Common_mdl->GetAllWithWhere('add_new_task','user_id',$user_id);
			$data['task_list'] = $query_res->result_array();
		} else {
			$data['task_list'] = $this->Common_mdl->getallrecords('add_new_task');
		}

		if ($user_id != '') {

			// $query=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id='".$user_id."' order by id desc ")->result_array();
			//  $res=array();
			//     foreach ($query as $key => $value) {
			//          array_push($res, $value['id']);
			//       }  
			//       $im_val=implode(',',$res); 
			//       $query=$this->db->query("SELECT * FROM proposals WHERE company_id in ('".$im_val."') order by id desc "); 
			$query = $this->db->query("SELECT * FROM proposals WHERE company_id=" . $user_id . "  order by id desc ");
			$data['proposals'] = $query->result_array();
		} else {
			$data['proposals'] = $this->Common_mdl->getallrecords('proposals');
		}

		/** rspt for leads **/
		if ($user_id != '') {
			$query = $this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id='" . $user_id . "' order by id desc ")->result_array();
			$res = array();
			foreach ($query as $key => $value) {
				array_push($res, $value['id']);
			}
			$im_val = implode(',', $res);
			$query = $this->db->query("SELECT * FROM leads WHERE company in ('" . $im_val . "') order by id desc ");
			// echo "SELECT * FROM leads WHERE user_id in ('".$im_val."') order by id desc";
			$data['leads'] = $query->result_array();
		} else {
			$data['leads'] = $this->Common_mdl->getallrecords('leads');
		}

		/** rspt 20-08-2018 for invoice view **/
		if ($user_id != '') {
			$data['invoice_details'] = $this->Invoice_model->selectAllRecord('Invoice_details', 'client_email', $user_id);
		}
		/** end of 20-08-2018 **/

		/** end opf leads **/

		//print_r($data['staff_form']);die;
		//first api
		$curl = curl_init();
		$items_per_page = '10';

		curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk/company/' . trim($data['client'][0]['crm_company_number']) . '/');

		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_USERPWD, $this->companieshouseAPIkey);
		$response = curl_exec($curl);
		/* echo '<pre>';
          print_r($response);
          die;*/
		if ($errno = curl_errno($curl)) {
			$error_message = curl_strerror($errno);
			echo "cURL error ({$errno}):\n {$error_message}";
		} else {
			curl_close($curl);
			$data_rec = json_decode($response);
			$array_rec = $this->object_2_array($data_rec);
			$data['company'] = $array_rec;

			// second api
			$curl = curl_init();

			curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk/company/' . trim($data['client'][0]['crm_company_number']) . '/officers?items_per_page=10&register_type=directors&register_view=false');

			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_USERPWD, $this->companieshouseAPIkey);
			$response1 = curl_exec($curl);


			if ($errno = curl_errno($curl)) {
				$error_message = curl_strerror($errno);
				echo "cURL error ({$errno}):\n {$error_message}";
			}
			curl_close($curl);
			$data1 = json_decode($response1);
			$array_rec1 = $this->object_2_array($data1);
			/*echo '<pre>';
          print_r($array_rec1);
         // die; */


			$data['officers'] = $array_rec1['items'];

			foreach ($array_rec1['items'] as $key => $value) {
				if ($value['officer_role'] == 'director') {

					// third api
					$curl = curl_init();
					$items_per_page = '10';

					curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk' . $value['links']['officer']['appointments']);

					curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($curl, CURLOPT_HEADER, false);
					curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
					curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($curl, CURLOPT_USERPWD, $this->companieshouseAPIkey);
					$response2 = curl_exec($curl);


					if ($errno = curl_errno($curl)) {
						$error_message = curl_strerror($errno);
						echo "cURL error ({$errno}):\n {$error_message}";
					}
					curl_close($curl);
					$data2 = json_decode($response2);
					//$data['array_rec2'][]= $this->object_2_array($data2);
					$array_rec2 = $this->object_2_array($data2);
					/*echo "<pre>";
          print_r($array_rec2['items']);die; */

					foreach ($array_rec2['items'] as $key => $val) {

						if ((isset($val['appointed_to']['company_number']) == isset($data['client'][0]['crm_company_number'])) &&  (isset($val['appointed_on']) == isset($value['appointed_on']))) {

							$data['content'][] = $val;
						} /*else{
     $data['content'][]=$value; 
  }*/
					}
					//echo json_encode($datas);
					//echo $content;
				}
			}

			// fourth filling history api
			$curl = curl_init();

			curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk/company/' . trim($data['client'][0]['crm_company_number']) . '/filing-history');

			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_USERPWD, $this->companieshouseAPIkey);
			$response3 = curl_exec($curl);


			if ($errno = curl_errno($curl)) {
				$error_message = curl_strerror($errno);
				echo "cURL error ({$errno}):\n {$error_message}";
			}
			curl_close($curl);
			$data3 = json_decode($response3);
			$data['array_rec3'] = $this->object_2_array($data3);
			// echo '<pre>';
			// print_r( $data['array_rec3']);
			// echo '</pre>';  
		}  //else close






		$data['countries'] = $this->db->query("SELECT * FROM `countries` order by name asc")->result_array();

		$data['feedback'] = $this->db->query("select id,username,crm_email_id from user where username!='' and username!='0' and  crm_email_id!='' and crm_email_id!='0' ")->result();

		$this->load->view('clients/client_infomationBK', $data);
	}

	public function GetTimelineNotesCount()
	{
		$All_NotesCount = $this->db->query('SELECT count(*) as num,module FROM `timeline_services_notes_added` where user_id = "' . $_POST['clientId'] . '" GROUP by module')->result_array();

		$notes_type = array('notes' => '0', 'email' => '0', 'calllog' => '0', 'smslog' => '0', 'logactivity' => '0', 'meeting' => '0', 'schedule' => '0');

		$arr = array();
		$arr = array_map(function ($a) {
			return ($a == '') ? 0 : $a;
		}, array_column($All_NotesCount, 'num', 'module'));
		$diff = array_diff_key($notes_type, $arr);
		$arr = array_merge_recursive($arr, $diff);

		echo json_encode($arr);
	}


	// FORCE SEND REMINDERS FROM CLIENT INFO LIST
	public function force_send_reminders()
	{
		$input = $_POST;
		$is_email = $input['is_email'];
		$reminder_id = $input['reminder_id'];
		$reminders = $this->db->where( "id=".$reminder_id )
		                      ->get( "service_reminder_cron_data" )
		                      ->result_array();
		$return['result'] = 0;                      
        foreach ( $reminders as $key => $reminder_data )
		{
			$query = "SELECT FS.* 
              FROM admin_setting as FS 
              INNER JOIN firm as F 
                      ON F.firm_id=FS.firm_id 
                     AND F.is_deleted!=1 
              WHERE FS.firm_id=".$reminder_data['firm_id'];

            $firm_Settings = $this->db->query( $query )->result_array();

            $Firm_Data = [];

            if(isset($firm_Settings))
            {
            	$Firm_Data = $firm_Settings[0];
            }



			$check_task_query_status=1;



			if( $reminder_data['status']==0 && $check_task_query_status==1)
			{
				$this->firm_exc_sr_log .= "inside reminder progress condtion\n";

				$client_service_contact_cond  = [
												'client_id' 		=> 	$reminder_data['client_id'] ,
												'service_id'		=>	$reminder_data['service_id'],
												'client_contacts_id'=>	$reminder_data['client_contacts_id']
											];

				$frequency_data = $this->db->where( $client_service_contact_cond )
										   ->get( "client_service_frequency_due" )
										   ->row_array();
			
				if( $frequency_data['is_create_task'] == 0  )
				{	
					$this->firm_exc_sr_log .= "TASK not created yet\n";
					$this->Service_Reminder_Model->Create_ServiceTask( $reminder_data );
					$this->firm_exc_sr_log .= "Task created\n";
				}

				$query = "SELECT c.user_id,c.id,u.status 
							FROM client as c  
								INNER JOIN user as u 
									ON u.id=c.user_id 
								WHERE c.id=".$reminder_data['client_id'];

				$client_data = $this->db->query( $query )->row_array();

				$this->firm_exc_sr_log .= "client data".json_encode( $client_data )."\n";

				$Reminder_SendAble_Status = [];

				if(isset($Firm_Data['reminder_send_status']))
				{
					$Reminder_SendAble_Status		=	json_decode( $Firm_Data['reminder_send_status'] , true );
				}

				if( $reminder_data['template_id'] !='' && in_array( $client_data['status'] , $Reminder_SendAble_Status ) )
				{

					
					$this->firm_exc_sr_log .= "Inside reminder creatable client status\n";
					
					$this->db->select("main_email,work_email");

					if( $reminder_data['client_contacts_id'] != 0 )
						$this->db->where( "id=".$reminder_data['client_contacts_id'] );
					else 
						$this->db->where( "client_id=".$client_data['user_id']." AND make_primary=1" );

					$contact_details = $this->db->get("client_contacts")->row_array();

					$contact_details['mails'][] 	=	$contact_details['main_email']; 

					if( !empty( $contact_details['work_email'] ) )
					{
						$work_email            	   = json_decode( $contact_details['work_email'] , true );

						$contact_details['mails']  = array_merge( $contact_details['mails'] , $work_email );
					}

					if( !empty( $contact_details['mails'] ) && $Firm_Data['service_reminder'] == 1 )
					{				
						$client 				= [ 
													'client_id' 			=>	$reminder_data['client_id'],
													'client_contacts_id'	=>	$reminder_data['client_contacts_id']
												];

						$DATA 					=	$this->Service_Reminder_Model->get_reminder_template_data( $client , $reminder_data['template_id'] , $reminder_data['template_type'] );					

						$send_to['to']	= 	$contact_details['mails'];
						$send_to['cc']	=	$Firm_Data['company_email'];	 
						$body 			=   setup_body_content( $Firm_Data['firm_id'] , $DATA['body'] );

                      
	                    $data = [
	                        'send_from'             =>  $Firm_Data['firm_id'],
	                        'send_to'               =>  json_encode( $send_to ),
	                        'subject'               =>  strip_tags( $DATA['subject'] ),
	                        'content'               =>  $body,
	                        'status'                =>  0,
	                        'relational_module'     =>  'service_reminder',
	                        'relational_module_id'  =>  $reminder_data['id'],
	                        'created_at'            =>  date('Y-m-d H:i:s')
	                    ];

                      	$this->db->insert( "email" , $data );
						$insert_id = $this->db->insert_id();

						$current_task_date = date("Y-m-d", strtotime($reminder_data['date']) );

						$check_days = 5;

						if($reminder_data['frequency'] == '1 month')
						{
							$check_days = 15;
						}
						elseif($reminder_data['frequency'] == '3 Months')
						{
							$check_days = 75;
						}
						elseif($reminder_data['frequency'] == '1 Year')
						{
							$check_days = 350;
						}
						elseif($reminder_data['frequency'] == '7 days')
						{
							$check_days = 5;
						}

						$starting_task_date = date('Y-m-d', strtotime($current_task_date. ' - '.$check_days.' days'));

						$service_id_plain = $reminder_data['service_id'];

						$service_id_with_ser = "ser_" . $reminder_data['service_id'];

						$task_query = "SELECT id,task_status,progress_status,sent_email_count,email_count FROM add_new_task WHERE firm_id=".$Firm_Data['firm_id']." AND company_name = '".$reminder_data['client_id']."' AND client_contacts_id = ".$reminder_data['client_contacts_id']." AND related_to = 'service_reminder' AND (service_due_date BETWEEN '".$starting_task_date."' AND '".$current_task_date."') AND (related_to_services = '".$service_id_plain."' OR related_to_services = '".$service_id_with_ser."')";

						$task_data = $this->db->query($task_query)->result_array();
						$td_statuses=[];
						if (is_array($task_data) ){
							if ( count($task_data) > 0 ){
								foreach($task_data as $td){
									$td_statuses[] = $td['task_status'];
									if(($td['task_status']=="1" || $td['task_status']=="2" || $td['task_status']=="3") && $td['progress_status']==11){
										if($td['sent_email_count'] == null)
										{
											$this->db->update( 'add_new_task' , [ 'sent_email_count'=> 1 ] , "id=".$td['id'] );
										}
										else
										{
											$sent_email_count = $td['sent_email_count'] + 1;
											$this->db->update( 'add_new_task' , [ 'sent_email_count'=> $sent_email_count ] , "id=".$td['id'] );
										}
									}
								}
							}
						}

						$this->firm_exc_sr_log .= $insert_id."-->reminder insert in email table\n".json_encode($data)."\n";

						$aws_mailer = new AwsMailer();
						$emails = $this->db
				                  ->where( "id=".$insert_id )
				                  ->limit(50)
				                  ->get( "email" )
				                  ->result_array();

						
						
                  		foreach ($emails as $key => $emails_data ){

							$emailData    = [
							'email_to'      => $emails_data['send_to'],
							'email_subject' => $emails_data['subject'],
							'email_body'    => $emails_data['content']            
							];

	
							if($is_email == 1)
							{					          
								$IsSend       = $aws_mailer->prepare_email_to_send($emailData, false);
								$update_data  = [
								'smtp_response' => json_encode( $IsSend ),
								'status'        => ($IsSend['result'] == 1 || $IsSend['result'] == '1') ? 1 : 2
								];

							}
							else
							{
								$update_data  = [
								'smtp_response' => '{"result":1,"message":"Email marked as sent manually"}',
								'status'        => 1
								];
							}

							if($this->db->update( 'email' , $update_data ,'id='.$emails_data['id'] ))
							{
							$emails_data['status'] = 1;
							}


							if( $emails_data['relational_module']  == 'service_reminder' ){
								$this->Service_Reminder_Model->update_reminder_status( $emails_data );            
							}

					      }
						$return['result'] = 1;
					}
					else
					{
						$this->firm_exc_sr_log .= "client mail empty or service reminder off in firm settings\n";
					}		
				}
				else
				{
					$this->firm_exc_sr_log .= "client status differ from serminder sendable status\n";
					$this->db->update( 'service_reminder_cron_data' , [ 'status'=> 3 ] , "id=".$reminder_data['id'] );
				}
			}
			else
			{
				$this->firm_exc_sr_log .= "inside reminder stop condtion\n";
			}

			if( $reminder_data['create_records'] == 1 )
			{
				$this->firm_exc_sr_log 	.= "inside Create recordes condition\n";

				$this->Service_Reminder_Model->Setup_Next_Frequency_Data( $reminder_data['client_id'] , $reminder_data['service_id'] , $reminder_data['client_contacts_id'] );

				$this->firm_exc_sr_log 	.= "Setup next frequncy reminders\n";
			}

			$this->firm_exc_sr_log .= $key." Reminder Execution End\n";


		}

		echo json_encode($return);
	}

	// PIN ACTIVITY TO TOP
	public function pin_to_top()
	{
		$input = $_POST;
		$timeline_id = $input['timeline_id'];
		$section = $input['section'];
		$activity = $input['activity'];
		$return['result'] = 0;
		if($activity == 'pin')
		{
			$pinned_content = time();
		}
		else
		{
			$pinned_content = 0;
		}

		if($section == '')
		{
			if($this->db->update( 'activity_log' , [ 'pinned_to_top'=> $pinned_content ] , "id=".$timeline_id ))
			{
				$return['result'] = 1;
			}
		}
		else
		{
			if($this->db->update( 'timeline_services_notes_added' , [ 'pinned_to_top'=> $pinned_content ] , "id=".$timeline_id ))
			{
				$return['result'] = 1;
			}
		}
		echo json_encode($return);
	}

	//SET REMINDERS IN QUEUE
	public function send_reminders_in_queue()
	{
		$input = $_POST;
		$client_id = $input['client_id'];
		$user_id = $input['user_id'];
		$contact_id = $input['contact_id'];
		$date = $input['date'];
		$client_service_contact_cond  = [
												'client_id' 		=> 	$client_id,
												'service_id'		=>	$contact_id,
												'client_contacts_id'=>	$user_id
											];

		$due_data = $this->db->get_where( "client_service_frequency_due" , $client_service_contact_cond )->row_array();
		$return['result'] = 0;
		if($due_data)
		{
			$this->db->update('client_service_frequency_due' ,[ 'date'=> $date], "id=".$due_data['id'] );
			if($this->Service_Reminder_Model->Setup_Next_Frequency_Data( $client_id ,$contact_id , $user_id ))
			{
				$return['result'] = 1;
			}
			else
			{
				$return['result'] = 1;
			}
		}
		
		echo json_encode($return);
	}
}
