<?php

class Deadlinecustomisation extends CI_Controller 
{
    public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('Common_mdl','Report_model','Security_model'));
		$this->load->library('Excel');
	}

    public function custom()
    {
		$search=array();
		$search_value=array();
		$services = $this->Common_mdl->getServicelist();
        $other_services = $this->db->query('SELECT * FROM service_lists WHERE id>16')->result_array();   

		if(empty($_POST['deadline']))
		{
			$cols = array('conf_statement','accounts','company_tax_return','personal_tax_return','workplace','vat','cis','cissub','p11d','bookkeep','management','investgate','registered','taxadvice','payroll','taxinvest');

            foreach ($services as $key => $value) 
            {
            	if($value['id']<17)
            	{
                  $columns[] = $cols[$value['id']-1];                    
            	}
            	else
            	{
            	  $columns[] = $value['services_subnames'];
            	  $checks = 'or '.$value['services_subnames'].' != ""';
            	  $checks1 = 'or (crm_'.$value['services_subnames'].'_statement_date != "")';
            	}
            }

			$_SESSION['service'] = $columns;

			$deadline_cond = 'AND (conf_statement!= "" or accounts!= "" or company_tax_return!= "" or personal_tax_return!= "" or workplace!= "" or vat!= "" or cis!= "" or cissub!= "" or p11d!= "" or bookkeep!= "" or management!= "" or investgate!= "" or registered!= "" or taxadvice!= "" or payroll!= "" or taxinvest!= "" '.$checks.')';
		}
		else
		{ 				
			$stat = $_POST['deadline'];	
            $_SESSION['service'] = $stat;

		    for($i=0;$i<count($stat);$i++)
		    {				
			   if($stat[$i]=='confirmation')
			   {
					$stat[$i] = 'conf_statement';
					array_push($search,$stat[$i]);
					array_push($search_value,'crm_confirmation_statement_due_date');
			   }
			   if($stat[$i]=='accounts'){ 
			   	array_push($search,$stat[$i]);
			   	array_push($search_value,'crm_ch_accounts_next_due');
			   }
			   if($stat[$i]=='company_tax_return'){  
			   	array_push($search,$stat[$i]);
			   	array_push($search_value,'crm_accounts_tax_date_hmrc');
			   }
			   if($stat[$i]=='personal_tax')
			   {
				   	$stat[$i] = 'personal_tax_return';
				   	array_push($search,$stat[$i]);
				   	array_push($search_value,'crm_personal_due_date_return');
			   }
			   if($stat[$i]=='vat'){  
			   	array_push($search,$stat[$i]);
			   	array_push($search_value,'crm_vat_due_date');
			   }
			   if($stat[$i]=='payroll'){ 
			   	array_push($search,$stat[$i]);
			   	array_push($search_value,'crm_rti_deadline');
			   }
			   if($stat[$i]=='pension')
			   	{  
				   	$stat[$i] = 'workplace';
				   	array_push($search,$stat[$i]);
				   	array_push($search_value,'crm_pension_subm_due_date');
			   }
			   if($stat[$i]=='cis'){ 
			   	array_push($search,$stat[$i]);
			   	//array_push($search_value,'');
			   }
			   if($stat[$i]=='cis_sub')
			   {  
			      $stat[$i] = 'cissub';
			      array_push($search,$stat[$i]);	
			   }
			   if($stat[$i]=='p11d'){ 
			   	array_push($search,$stat[$i]);
			   	array_push($search_value,'crm_next_p11d_return_due');
			   }
			   if($stat[$i]=='management'){ 
			   	array_push($search,$stat[$i]);
			   	array_push($search_value,'crm_next_manage_acc_date');
			   }
			   if($stat[$i]=='bookkeep'){ 
			   	array_push($search,$stat[$i]);
			   	array_push($search_value,'crm_next_booking_date');
			   }
			   if($stat[$i]=='investigate')
			   {   	
			   	  $stat[$i] = 'investgate';
			   	  array_push($search,$stat[$i]);				
			   	  array_push($search_value,'crm_insurance_renew_date');
			   }
			   if($stat[$i]=='registered'){ 
			   	array_push($search,$stat[$i]);
			   	array_push($search_value,'crm_registered_renew_date');
			   }
			   if($stat[$i]=='taxadvice'){ 
			   	array_push($search,$stat[$i]);
			   	array_push($search_value,'crm_investigation_end_date');
			   }
			   if($stat[$i]=='taxinvest'){ 
			   	array_push($search,$stat[$i]);
			   	array_push($search_value,'crm_investigation_end_date');
			   }

			   if(in_array($stat[$i], array_column($other_services,'services_subnames')))
			   { 
			   	  array_push($search,$stat[$i]);
			   	  array_push($search_value,'crm_'.$stat[$i].'_statement_date');
			   } 

			   $_SESSION['service'] = $stat; 
		    }

		    $deadline_cond = 'AND (';			   

		    for($j=0;$j<count($search);$j++)
		    {
		    	$deadline_cond .= '('.$search[$j].' != "")';

		    	if($j!=(count($search)-1))
		    	{
		    		$deadline_cond .= ' OR ';
		    	}
		    }

		    $deadline_cond.= ')';		   		    
		}         
        	
		if(!empty($_POST['filter']) && $_POST['filter'] != 'all')
		{					
			if($_POST['filter']=='week')
			{
				$current_date = date('Y-m-d');
				$week = date('W', strtotime($current_date));
				$year = date('Y', strtotime($current_date));

				$dto = new DateTime();
				$start_date = $dto->setISODate($year, $week, 0)->format('Y-m-d');
				$end_date = $dto->setISODate($year, $week, 6)->format('Y-m-d');
				$end_date = date('Y-m-d', strtotime($end_date . ' +1 day')); 
			}
            else if($_POST['filter']=='three_week')
            {
				$current_date = date('Y-m-d');
				$weeks = strtotime('- 3 week', strtotime( $current_date ));
				$start_date = date('Y-m-d', $weeks); 
				$addweeks = strtotime('+ 3 week', strtotime( $start_date ));
				$end_Week = date('Y-m-d', $addweeks); 
				$end_date = date('Y-m-d', strtotime($end_Week . ' +1 day'));
			}

			else if($_POST['filter']=='month')
			{
				$start_date = date("Y-m-01");
				$month_end = date("Y-m-t"); 
				$end_date = date('Y-m-d', strtotime($month_end . ' +1 day'));
			}

			if($_POST['filter']=='Three_month')
			{
				$end_date = date('Y-m-d',strtotime(date('Y-m-d').'+1 day'));
				$start_date = date('Y-m-d',strtotime('- 3 month', strtotime($end_date)));
			}

            if(count($search_value)>0)
            {
				$filter = 'AND (';

				for($j=0;$j<count($search_value);$j++)
			    {			    	
			    	$filter .= '('.$search_value[$j].' BETWEEN "'.$start_date.'" AND "'.$end_date.'")';

			    	if($j!=(count($search_value)-1))
			    	{
			    		$filter .= ' OR';
			    	}	
			    }

			    $filter .= ')';
            }
            else
            { 
                $filter = 'AND ((crm_confirmation_statement_due_date BETWEEN "'.$start_date.'" AND "'.$end_date.'" ) or (crm_ch_accounts_next_due BETWEEN "'.$start_date.'" AND "'.$end_date.'" ) or (crm_accounts_tax_date_hmrc BETWEEN "'.$start_date.'" AND "'.$end_date.'" ) or (crm_personal_due_date_return BETWEEN "'.$start_date.'" AND "'.$end_date.'" ) or (crm_vat_due_date BETWEEN "'.$start_date.'" AND "'.$end_date.'" ) or (crm_rti_deadline BETWEEN "'.$start_date.'" AND "'.$end_date.'" )  or (crm_pension_subm_due_date BETWEEN "'.$start_date.'" AND "'.$end_date.'" ) or (crm_next_p11d_return_due BETWEEN "'.$start_date.'" AND "'.$end_date.'" )  or (crm_next_manage_acc_date BETWEEN "'.$start_date.'" AND "'.$end_date.'" ) or (crm_next_booking_date BETWEEN "'.$start_date.'" AND "'.$end_date.'" ) or (crm_insurance_renew_date BETWEEN "'.$start_date.'" AND "'.$end_date.'" )  or (crm_registered_renew_date BETWEEN "'.$start_date.'" AND "'.$end_date.'" ) or (crm_investigation_end_date BETWEEN "'.$start_date.'" AND "'.$end_date.'") '.str_replace('!= ""', 'BETWEEN "'.$start_date.'" AND "'.$end_date.'"', $checks1).')';
            }	
	    }
	    else if(!empty($_POST['from_date']) && !empty($_POST['to_date']))
		{
			$start_date = $_POST['from_date'];
			$end_date = $_POST['to_date'];
			
            if(count($search_value)>0)
            {
				$filter = 'AND (';

				for($j=0;$j<count($search_value);$j++)
			    {			    	
			    	$filter .= '('.$search_value[$j].' BETWEEN "'.$start_date.'" AND "'.$end_date.'")';

			    	if($j!=(count($search_value)-1))
			    	{
			    		$filter .= ' OR';
			    	}	
			    }

			    $filter .= ')';
            }
            else
            {
                $filter = 'AND ((crm_confirmation_statement_due_date BETWEEN "'.$start_date.'" AND "'.$end_date.'" ) or (crm_ch_accounts_next_due BETWEEN "'.$start_date.'" AND "'.$end_date.'" ) or (crm_accounts_tax_date_hmrc BETWEEN "'.$start_date.'" AND "'.$end_date.'" ) or (crm_personal_due_date_return BETWEEN "'.$start_date.'" AND "'.$end_date.'" ) or (crm_vat_due_date BETWEEN "'.$start_date.'" AND "'.$end_date.'" ) or (crm_rti_deadline BETWEEN "'.$start_date.'" AND "'.$end_date.'" )  or (crm_pension_subm_due_date BETWEEN "'.$start_date.'" AND "'.$end_date.'" ) or (crm_next_p11d_return_due BETWEEN "'.$start_date.'" AND "'.$end_date.'" )  or (crm_next_manage_acc_date BETWEEN "'.$start_date.'" AND "'.$end_date.'" ) or (crm_next_booking_date BETWEEN "'.$start_date.'" AND "'.$end_date.'" ) or (crm_insurance_renew_date BETWEEN "'.$start_date.'" AND "'.$end_date.'" )  or (crm_registered_renew_date BETWEEN "'.$start_date.'" AND "'.$end_date.'" ) or (crm_investigation_end_date BETWEEN "'.$start_date.'" AND "'.$end_date.'" ) '.str_replace('!= ""', 'BETWEEN "'.$start_date.'" AND "'.$end_date.'"', $checks1).')';
            }	
		}
	    else
	    {
            if(count($search_value)>0)
            {
				$filter = 'AND (';

				for($j=0;$j<count($search_value);$j++)
			    {			    	
			    	$filter .= '('.$search_value[$j].' != "")';

			    	if($j!=(count($search_value)-1))
			    	{
			    		$filter .= ' OR';
			    	}	
			    }

			    $filter .= ')';
            }
            else
            {
                $filter = 'AND ((crm_confirmation_statement_due_date != "" ) or (crm_ch_accounts_next_due != "" ) or (crm_accounts_tax_date_hmrc != "" ) or (crm_personal_due_date_return != "" ) or (crm_vat_due_date != "" ) or (crm_rti_deadline != "" )  or (crm_pension_subm_due_date != "" ) or (crm_next_p11d_return_due != "" )  or (crm_next_manage_acc_date != "" ) or (crm_next_booking_date != "" ) or (crm_insurance_renew_date != "" )  or (crm_registered_renew_date != "" ) or (crm_investigation_end_date != "" '.$checks1.'))';
            }	
	    }		

		if(!empty($_POST['client']))
		{
           $client_ids = $_POST['client'];
		}
		else
		{
		   $this->load->helper('comman');
		   $Assigned_Client = Get_Assigned_Datas('CLIENT');
		   $client_ids = implode(',', $Assigned_Client);
		}	    
		
		$data['start_date'] = isset($start_date)?$start_date:"";
		$data['end_date'] = isset($end_date)?$end_date:"";
		
		$data['records'] = $this->db->query('SELECT * FROM client WHERE FIND_IN_SET(id,"'.$client_ids.'") AND crm_company_name != "" '.$deadline_cond.' '.$filter.' ORDER BY id ASC')->result_array();	
			
		$this->load->view('reports/deadline_customresult',$data);
    }
} 

?>
