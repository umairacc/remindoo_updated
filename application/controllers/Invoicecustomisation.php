<?php

class Invoicecustomisation extends CI_Controller 
{
        public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
		public function __construct()
		{
			parent::__construct();
			$this->load->model(array('Common_mdl','Report_model','Security_model'));
			$this->load->library('Excel');
		}

		public function custom()
		{
            if(empty($_POST['status']))
			{
				$status_type = "";
			}
			else
			{
				if(in_array('Expired', $_POST['status']))
				{
                   foreach ($_POST['status'] as $key => $value) 
                   {
                   	  if($value == 'Expired')
                   	  {
                   	  	 unset($_POST['status'][$key]);
                   	  }
                   }

                   $expired = 'FIND_IN_SET(Invoice_details.invoice_duedate,"Expired")';
				}
				else
				{
					$expired = '';
				}				

				if(count($_POST['status'])>0)
				{
					$status = implode(',',$_POST['status']);
					$status_type = 'FIND_IN_SET(amount_details.payment_status,"'.$status.'")';
                }

				if(!empty($expired) && !empty($status_type))
				{
					$status_type = 'AND ('.$status_type.' OR '.$expired.')';
				}
				else if(empty($status_type) && !empty($expired))
				{
                    $status_type = 'AND '.$expired;
				}
				else 
				{
					$status_type = 'AND '.$status_type;
				}
			}			

			if(!empty($_POST['filter']) && $_POST['filter'] != 'all')
			{					
				if($_POST['filter']=='week')
				{
					$current_date = date('Y-m-d');
					$week = date('W', strtotime($current_date));
					$year = date('Y', strtotime($current_date));

					$dto = new DateTime();
					$start_date = $dto->setISODate($year, $week, 0)->format('Y-m-d');
					$end_date = $dto->setISODate($year, $week, 6)->format('Y-m-d');
					$end_date = date('Y-m-d', strtotime($end_date . ' +1 day')); 
				}
                else if($_POST['filter']=='three_week')
                {
					$current_date = date('Y-m-d');
					$weeks = strtotime('- 3 week', strtotime( $current_date ));
					$start_date = date('Y-m-d', $weeks); 
					$addweeks = strtotime('+ 3 week', strtotime( $start_date ));
					$end_Week = date('Y-m-d', $addweeks); 
					$end_date = date('Y-m-d', strtotime($end_Week . ' +1 day'));
				}

				else if($_POST['filter']=='month')
				{
					$start_date = date("Y-m-01");
					$month_end = date("Y-m-t"); 
					$end_date = date('Y-m-d', strtotime($month_end . ' +1 day'));
				}

				if($_POST['filter']=='Three_month')
				{
					$end_date = date('Y-m-d',strtotime(date('Y-m-d').'+1 day'));
					$start_date = date('Y-m-d',strtotime('- 3 month', strtotime($end_date)));
				}	

				$filter = 'AND amount_details.created_at BETWEEN "'.$start_date.'" AND "'.$end_date.'"';
		    }
		    else if(!empty($_POST['from_date']) && !empty($_POST['to_date']))
			{
				$start_date = $_POST['from_date'];
				$end_date = $_POST['to_date'];
				$filter = 'AND amount_details.created_at BETWEEN "'.$start_date.'" AND "'.$end_date.'"';
			}
		    else
		    {
		    	$filter = '';
		    }			

			if(!empty($_POST['client']))
			{
                $rec = $this->Report_model->selectRecord('client','id', $_POST['client']);
                $client = 'Invoice_details.client_email = "'.$rec['user_id'].'"';
			}
			else
			{
				$clients_user_ids = $this->Report_model->getClientsUserIds();
				$client = 'FIND_IN_SET(Invoice_details.client_email,"'.$clients_user_ids.'")';
			}	
				
            $data['records'] = $this->db->query('SELECT Invoice_details.*,amount_details.* FROM Invoice_details LEFT JOIN amount_details ON Invoice_details.client_id = amount_details.client_id where '.$client.' '.$status_type.' '.$filter.' ORDER BY Invoice_details.client_id ASC')->result_array();

            $this->load->view('reports/invoice_customresult',$data);
		}

} 

?>	
