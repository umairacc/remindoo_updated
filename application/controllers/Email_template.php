<?php
error_reporting(E_ERROR);
class Email_template extends CI_Controller 
{
    public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Common_mdl','Proposal_model','Security_model','Loginchk_model'));
      //   $this->load->library('Excel');
  }

    public function index()
    { 
        if($_SESSION['firm_id'] == '0' || $_COOKIE['remindoo_logout'] == 'remindoo_superadmin') 
        {           
           $this->Loginchk_model->superAdmin_LoginCheck();
        }
        else
        {
           $this->Security_model->chk_login();
        } 

        if($_SESSION['permission']['Email_Template']['view'] == '1')
        {
           $sql = "SELECT * FROM email_templates WHERE firm_id IN('".$_SESSION['firm_id']."','0')";       
             
           $email_templates = $this->db->query($sql)->result_array();

           if($_SESSION['firm_id'] != 0)
           {
              $record = $this->db->query("SELECT super_admin_owned FROM email_templates WHERE firm_id = '".$_SESSION['firm_id']."'")->result_array();
           
              foreach ($email_templates as $key => $value) 
              {
                 foreach ($record as $value1) 
                 {
                    if($value['id'] == $value1['super_admin_owned'])
                    {
                       unset($email_templates[$key]);
                    }
                 }

                 if($value['firm_id'] == 0)
                 {
                    if($value['status'] == 0)
                    {
                       unset($email_templates[$key]);
                    }
                 }
              }
           }

           $data['email_templates'] = $email_templates;         
           $data['email_actions'] = $this->db->query('SELECT * FROM email_templates_actions')->result_array();

           $this->load->view('email_template/email_templates',$data);
           unset($_SESSION['success_msg']);
           unset($_SESSION['fail_msg']);
        }
        else
        {
            $this->load->view('users/blank_page');
        }
      
    }

    public function add_template()
    {
        if($_SESSION['firm_id'] == '0') 
        {
           $this->Loginchk_model->superAdmin_LoginCheck();
        }
        else
        {
           $this->Security_model->chk_login();
        }
     
        $data['email_type'] = $this->getEmailactions();         
        $this->load->view('email_template/all_email_template',$data);
    }

    public function edit_template($eid)
    {
        if($_SESSION['firm_id'] == '0') 
        {
           $this->Loginchk_model->superAdmin_LoginCheck();
        }
        else
        {
           $this->Security_model->chk_login();
        }
        
        $data['email_type'] = $this->getEmailactions();
        $data['template'] = $this->Common_mdl->select_record('email_templates','id',$eid); 
        
        $this->load->view('email_template/all_email_template',$data);
    }

    public function add_reminder()
    {
        if($_SESSION['firm_id'] == '0') 
        {
           $this->Loginchk_model->superAdmin_LoginCheck();
        }
        else
        {
           $this->Security_model->chk_login();
        }      
         
        $this->load->view('email_template/all_email_template');
    }

    public function edit_reminder($eid)
    {
        if($_SESSION['firm_id'] == '0') 
        {
           $this->Loginchk_model->superAdmin_LoginCheck();
        }
        else
        {
           $this->Security_model->chk_login();
        }      
         
        $data['template'] = $this->Common_mdl->select_record('email_templates','id',$eid); 
        $this->load->view('email_template/all_email_template',$data);
    }

    public function insert()
    { 
        if($_SESSION['firm_id'] == '0' || $_COOKIE['remindoo_logout'] == 'remindoo_superadmin') 
        {
           $this->Loginchk_model->superAdmin_LoginCheck();
        }
        else
        {
           $this->Security_model->chk_login();
        }
        
        $data['subject'] = $_POST['subject'];
        $data['body'] = $_POST['body'];
        $data['action_id'] = $_POST['type'];
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['firm_id'] = $_SESSION['firm_id'];
        $data['status'] = $_POST['status'];           
        $data['reminder_heading'] = $_POST['reminder_heading'];  
        $data['from_address'] = $_POST['from_address']; 

        if(!empty($_POST['type']) && $_POST['type'] != '22')
        {
           $title = $this->Common_mdl->get_price('email_templates_actions','action_id',$_POST['type'],'action');
           $data['title'] = ucwords(str_replace('_',' ', $title));
        }
        else
        {
           $data['title'] = $_POST['title'];
        }          

        $insert = $this->Common_mdl->insert('email_templates',$data);

        if($insert != 0)
        {
            $_SESSION['success_msg'] = "You Have Successfully Added Template!";
        }
        else
        {
            $_SESSION['fail_msg'] = 1;
        }

        echo 1; 
    }

    public function update($id)
    { 
        if($_SESSION['firm_id'] == '0' || $_COOKIE['remindoo_logout'] == 'remindoo_superadmin') 
        {
           $this->Loginchk_model->superAdmin_LoginCheck();
        }
        else
        {
           $this->Security_model->chk_login();
        }

        if(isset($_POST))
        {
            $record = $this->db->query('SELECT * FROM email_templates WHERE id = "'.$id.'"')->row_array();
            
            $data['subject'] = $_POST['subject'];
            $data['body'] = $_POST['body'];
            $data['action_id'] = $_POST['type'];
            $data['status'] = $_POST['status'];           
            $data['reminder_heading'] = $_POST['reminder_heading'];
            $data['from_address'] = $_POST['from_address']; 

            if(!empty($_POST['type']) && $_POST['type'] != '22')
            {
               $title = $this->Common_mdl->get_price('email_templates_actions','action_id',$_POST['type'],'action');
               $data['title'] = ucwords(str_replace('_',' ', $title));
            }
            else
            {
               $data['title'] = $_POST['title'];
            } 

            if($record['firm_id'] == $_SESSION['firm_id'])
            {
               $data['updated_at'] = date('Y-m-d H:i:s');
               $update = $this->Common_mdl->update('email_templates',$data,'id',$id);
            }
            else
            {   
               $data['created_at'] = date('Y-m-d H:i:s');
               $data['firm_id'] = $_SESSION['firm_id'];
               $data['super_admin_owned'] = $record['id'];

               $update = $this->Common_mdl->insert('email_templates',$data); 
            } 

            if($update != 0)
            {
                $_SESSION['success_msg'] = "You Have Successfully Updated Template!";
            }
            else
            {
                $_SESSION['fail_msg'] = 1;
            }

            echo 1;                   
        }
    }

    public function check_distinct()
    {
        $segment = $_POST['segment'];
        $id = $_POST['id'];

        $sql = "SELECT * FROM email_templates WHERE ".$_POST['field']." LIKE '".trim($_POST['value'])."' AND firm_id = '".$_SESSION['firm_id']."'";

        if($segment == 'edit_template' || $segment == 'edit_reminder')
        {
           $sql = $sql." AND id!='".$id."'";
        }
      
        $data = $this->db->query($sql)->result_array(); 

        echo count($data);
    }

    public function delete_template()
    {  
       $ids = str_replace('[', '', $_POST['record_ids']);
       $ids = str_replace(']', '', trim($ids)); 
      
       $delete = $this->db->query("DELETE FROM email_templates WHERE id IN(".$ids.")");  
       
       if($delete!=0)
       {
          $_SESSION['success_msg'] = "Template(s) Deleted Successfully!";
       }
       else
       {
           $_SESSION['fail_msg'] = 1;
       }

       echo 1;       
    }

    public function getEmailactions()
    {
       $email_actions = $this->db->query('SELECT * FROM email_templates_actions')->result_array();
       $email_type = array();

       foreach($email_actions as $value)
       {
          $email_type[$value['module']][$value['action_id']]  =  $value['action'];
       }

       return $email_type;
    }

    public function updateStatus()
    {
         if($_SESSION['permission']['Email_Template']['edit'] == '1')
         {
             $record = $this->db->query('SELECT * FROM email_templates WHERE id = "'.$_POST['rid'].'"')->row_array();

             if($_POST['status'] == 'active')
             {
                $status = '1';
             }
             else
             {
                $status = '0';
             }

             if($record['firm_id'] == $_SESSION['firm_id'])
             {
                $data['status'] = $status;
                $data['updated_at'] = date('Y-m-d H:i:s');
                $update = $this->Common_mdl->update('email_templates',$data,'id',$_POST['rid']);
             }
             else
             {   
                $data['title'] = $record['title'];
                $data['subject'] = $record['subject'];
                $data['body'] = $record['body'];
                $data['action_id'] = $record['action_id'];
                $data['status'] = $status;           
                $data['reminder_heading'] = $record['reminder_heading'];
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['firm_id'] = $_SESSION['firm_id'];
                $data['super_admin_owned'] = $record['id'];

                $update = $this->Common_mdl->insert('email_templates',$data); 
             } 

             if($update != 0)
             {
                 $_SESSION['success_msg'] = "You Have Successfully Updated Template!";
             }
             else
             {
                 $_SESSION['fail_msg'] = 1;
             }  

             echo 1;           
         }
         else
         {
            echo "You Do Not Have Permission To Edit!";
         }   
    }

}

?>