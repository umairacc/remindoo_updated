<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tasksummary extends CI_Controller {
public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct(){
        parent::__construct();
        $this->load->model(array('Service_model','Common_mdl','Security_model','Invoice_model','Task_invoice_model','Task_recuring_model','Task_section_model'));
    }

    public function index()
    {
        //$data['task_list']=$this->Common_mdl->getallrecords('add_new_task');
        $data['notstartedTasks'] = $this->Common_mdl->GetAllWithWhere('add_new_task','task_status','notstarted');
        $data['inprogressTasks'] = $this->Common_mdl->GetAllWithWhere('add_new_task','task_status','inprogress');
        $data['awaitingTasks'] = $this->Common_mdl->GetAllWithWhere('add_new_task','task_status','awaiting');
        $data['testingTasks'] = $this->Common_mdl->GetAllWithWhere('add_new_task','task_status','testing');
        $data['completeTasks'] = $this->Common_mdl->GetAllWithWhere('add_new_task','task_status','complete');

        $this->load->view('tasks/task_summary_view',$data);
    }
    public function import_task()
    {
        $data['tag']=$this->Common_mdl->getallrecords('Tag');
        $data['staff_form'] = $this->Common_mdl->GetAllWithWhere('user','role','6');
        $data['manager'] = $this->Common_mdl->GetAllWithWhere('user','role','5');
        $this->load->view('tasks/import_task',$data);
    }


    public function import_tasks()
    {
        //$client_id = $_POST['client_id'];
        //$client_id =$_SESSION['id'];
        $client_id = $this->Common_mdl->select_record("client","id",$_POST['company_name']);
       /* echo '<pre>';
        print_r($_POST);die;*/
        $csvMimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');

        if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes))
        {
          if(is_uploaded_file($_FILES['file']['tmp_name']))
          {
            $csvFile = fopen($_FILES['file']['tmp_name'], 'r');   
            fgetcsv($csvFile);   
            $data_exist =0; 

            while(($line = fgetcsv($csvFile)) !== FALSE)
            {  
                 $data_exist =1;
                 //echo  ($line = fgetcsv($csvFile)) !== FALSE;
                 
                 $var=rand(1,10);
                 $priority=(isset($_POST['priority'])&&($_POST['priority']!=''))? $_POST['priority']:'0';
                 //$data['related_to']=(isset($_POST['related'])&&($_POST['related']!=''))? $_POST['related']:'0';
                // $related_to=(isset($_POST['related_to'])&&($_POST['related_to']!=''))? $_POST['related_to']:'';
                 //$project_id=(isset($_POST['r_project'])&&($_POST['r_project']!=''))? $_POST['r_project']:'';
                 $worker=(isset($_POST['worker'])&&($_POST['worker']!=''))? implode(',',$_POST['worker']):'';
                 /**** 25-06-2018 **/
                 $team=array();
                 $department=array();
                 $assign=array();
                 $for_old_values=array();
                 //$data['assigned'] = implode(',',$_POST['assigned']);
                 /** for assign team and department  14-06-2018 **/
                 // if(isset($_POST['worker']))
                 //   {
                 //     foreach ($_POST['worker'] as $key => $value)
                 //     {
                 //       $ex_val=explode('_', $value);
                 //       if(count($ex_val)>1)
                 //       {
                 //         if($ex_val[0]=='tm')
                 //         {
                 //            array_push($team, $ex_val[1]);
                 //         }
                 //         if($ex_val[0]=='de')
                 //         {
                 //             array_push($department, $ex_val[1]);
                 //         }
                 //       }
                 //       else
                 //       {
                 //          array_push($assign, $ex_val[0]);
                 //       }
                 //     }
                 //     $data['worker'] = implode(',',$assign);
                 //     $data['team'] = implode(',',$team);
                 //     $data['department'] = implode(',',$department);

                 //     $for_old_values['assign']=$assign;
                 //     $for_old_values['team']=$team;
                 //     $for_old_values['department']=$department;                
                 //   }
                 // $for_old_values['manager']=(isset($_POST['manager'])&& is_array($_POST['manager']) )? $_POST['manager']:array();
                 // $data['for_old_assign']=json_encode($for_old_values);       
                 
               //  $manager=(isset($_POST['manager'])&&($_POST['manager']!=''))? implode(',',$_POST['manager']):'';

                 $dateTimestamp1 = strtotime($line[2]); 
                 $dateTimestamp2 = strtotime($line[3]); 
                 $current_date = strtotime(date('Y-m-d'));
                 // echo $dateTimestamp1.'<br>';
                 // echo $dateTimestamp2.'<br>';
                 // echo $current_date;
                 // die;
                //  if( $current_date > $dateTimestamp1 || $current_date > $dateTimestamp2 || $dateTimestamp1 > $dateTimestamp2 )
                //  { 
                //    echo json_encode( [0," Please Check The Start or End date"] );die;
                //  }

                 if(gettype($line[4]) != 'string')
                 {
                   echo json_encode([0," Please Check The Tag"]); die;
                 }

                  $tag = (isset($line[4]) && ($line[4]!='')) ? $line[4] : ''; 
                  $data['user_id']=$client_id['user_id'];
                  $data['billable']=$line[0];
                  $data['subject']=$line[1];
                  $data['start_date']=($line[2]!=0 && $line[2]!='0')?date('Y-m-d',strtotime($line[2])):'0';
                  $data['end_date']=($line[3]!=0 && $line[3]!='0')?date('Y-m-d',strtotime($line[3])):'0';
                  $data['company_name']=$_POST['company_name'];
                  $data['task_status']=$_POST['task_status'];
                  $data['tag']=$tag;
                  $data['related_to']="tasks";
                  $data['priority']=$priority;
                 // $data['manager']=$manager;
                   $data['firm_id']=(isset($_SESSION['firm_id']))?$_SESSION['firm_id'] : '0';
                  $data['create_by']=$_SESSION['id'];
                  $data['created_date']=time();
                  $this->Common_mdl->insert("add_new_task",$data);
                  $in=$this->db->insert_id();
                  $this->Common_mdl->task_setting($in);
                  $this->Common_mdl->add_assignto($_POST['assign_role'],$in,'TASK');
                  $invoice_id='';
                  if((isset($data['billable'])&&($data['billable']!='')) && $data['task_status']=='5')
                  {
                     $invoice_id=$this->Task_invoice_model->task_billable_createInvoice($in);
                     if($invoice_id!='')
                     {
                      $datas_invoice['invoice_id']=$invoice_id;
                      $result =  $this->Common_mdl->update('add_new_task',$datas_invoice,'id',$in);
                     }
                  }
                $this->Task_invoice_model->task_new_mail($in,'for_add');         
               
            }

            if(!$data_exist)
            {
              echo json_encode( [0,"Data Not Exists Please Check The File"] );die;
            }

            fclose($csvFile);
          }
          else
          {
              echo json_encode( [0,'unable to process the file'] );die;
          }
        }
        else
        {
            echo json_encode( [0,'File Type Not allowed'] ) ;die;
        }

         echo json_encode([$this->db->affected_rows() , 'Import Task SuccessFully.']); die;
    }

    public function taskFilter()
    {
        $status = $_POST['filterstatus'];
        $data['task_list'] = $this->Common_mdl->getfilterRec($status);
        $this->load->view('tasks/taskfilterRec',$data);
    }
     
    public function task_AddComments(){
      $t_id     = isset($_POST['t_id'])     ? $_POST['t_id']     : "";     
      $cmt_id   = isset($_POST['cmt_id'])   ? $_POST['cmt_id']   : ""; 
      $cmts     = isset($_POST['cmt_val'])  ? $_POST['cmt_val']  : ""; 
      $Ses_ID   = isset($_POST['Ses_ID'])   ? $_POST['Ses_ID']   : ""; 
      $Ses_name = isset($_POST['Ses_name']) ? $_POST['Ses_name'] : "";
      $current_task = $t_id;
      $task_arr[] = $t_id;

      $parent_task = $this->db->query("select id from add_new_task where sub_task_id like '%".$t_id."%' ")->result_array();

      if(isset($parent_task[0]['id'])){
        $task_arr[] = $parent_task[0]['id'];
      }

      $all_inserted_flag = 1;

      foreach($task_arr as $t_id){
        $data1    = array(
          'module_id'   => $t_id,
          'user_id'     => $Ses_ID,
          'log'         => $cmts.' - comment added by ',
          'module'      => 'Task',
          'CreatedTime' => time()
        );
        $this->db->insert( 'activity_log', $data1 );
  
        $data = array(
          'task_id'           => $t_id,
          'user_id'           => $Ses_ID,
          'parent_comment_id' => $cmt_id,
          'name'              => $Ses_name,
          'message'           => $cmts,
          'status'            => 'Active',
          'created_at'        => time()
        );
        $insert_data = $this->db->insert( 'task_comments', $data );
        $Message     = 'You Have Command From {TASK_SUBJECT} Task';
        
        $this->Common_mdl->Task_Members_Notify( $t_id ,'Task_Comment_Added', $Message );
        $this->Common_mdl->Task_Members_Notify( $t_id ,'Task_Comment_Added', $Message, $cmts );
        
        if($insert_data) {
         // do nothing              
        } else {
          $all_inserted_flag = 0;
        } 
      }

      if($all_inserted_flag == 1) {
        $data['activity_log'] = $this->db->query("select * from activity_log where  module='Task' and module_id=".$current_task." order by id desc")->result_array();
        $data['task_form']    = $this->Common_mdl->GetAllWithWhere('add_new_task','id',$current_task); 
        $hold                 = $this->load->view('users/ajax_comments',$data,true);
        
        echo  $hold;               
      } else {
        echo '0';
      }

    }

    public function Comment_list($task_id)
    {
        // $task_info = $this->db->query("SELECT * FROM task_comments where task_id='".$id."' ")->result_array();
        $task_info = $this->db->query("SELECT * FROM task_comments where task_id=".$task_id." ORDER BY created_at ASC ")->result_array();
        $record_set = array();
        foreach ($task_info as $key => $task) {
            array_push($record_set, $task);
        }
        echo json_encode($record_set);
        // echo "saro";

    }
       public function delete_fileupload()
    {
        // $user = $this->Common_mdl->GetAllWithWhere('add_new_task','id',2157);
       $user = $this->Common_mdl->GetAllWithWhere('add_new_task','id',$_POST['task_id']);

       // $attchFile = (isset($_FILES['attach_file']['name']) && ($_FILES['attach_file']['name'] != ''))? $this->Common_mdl->do_upload($_FILES['attach_file'], 'uploads') : $user[0]['attach_file'];
        /** 02-07-2018 rs **/
       
        $attach_files='';
             if(!empty($user)){

                $db_file= explode(',', $user[0]['attach_file']);
                $ex_img=(isset($_POST['file_name']) && $_POST['file_name']!='')?$_POST['file_name']:'';

  
           if($ex_img!='')
           {

            if (false !== $key = array_search($ex_img,  $db_file)) {
              unset( $db_file[$key]);
                //do something
            }

            if(!empty($db_file))
            {

              $data['attach_file']=implode(',', array_filter(explode(',', $db_file)));
            }
            else
            {
              $data['attach_file']='';

            }

          }
         


            
            $update_data = $this->Common_mdl->update('add_new_task', $data, 'id', $_POST['task_id']);

            $Message = 'Attachment Deleted To {TASK_SUBJECT} Task';

            $this->Common_mdl->Task_Members_Notify( $_POST['task_id'] ,'Task_Attachment_Uploaded', $Message );

             $data1= array('module_id' => $_POST['task_id'],
                        'user_id' => $_SESSION['id'],                      
                        'log' => 'Attachment Deleted by ',
                        'module' => 'Task',
                        'CreatedTime' => time()
                );
             $this->db->insert( 'activity_log', $data1 );

      }
    }

    public function UploadAttachFile()
    {
        $user = $this->Common_mdl->GetAllWithWhere('add_new_task','id',$_POST['taskID']);
       // $attchFile = (isset($_FILES['attach_file']['name']) && ($_FILES['attach_file']['name'] != ''))? $this->Common_mdl->do_upload($_FILES['attach_file'], 'uploads') : $user[0]['attach_file'];
        /** 02-07-2018 rs **/
       
        $attach_files='';
             if(!empty($_FILES['attach_file']['name']) && isset($_FILES['attach_file']['name']) ){
              /** for multiple **/
        $files = $_FILES;
    $cpt = count($_FILES['attach_file']['name']);
    for($i=0; $i<$cpt; $i++)
    {           
              /** end of multiple **/
              $_FILES['attach_file']['name']= $files['attach_file']['name'][$i];
        $_FILES['attach_file']['type']= $files['attach_file']['type'][$i];
        $_FILES['attach_file']['tmp_name']= $files['attach_file']['tmp_name'][$i];
        $_FILES['attach_file']['error']= $files['attach_file']['error'][$i];
        $_FILES['attach_file']['size']= $files['attach_file']['size'][$i]; 
               $uploadPath = 'uploads/';
               $config['upload_path'] = $uploadPath;
               $config['allowed_types'] = '*';    
               $config['max_size']='0';
               $this->load->library('upload', $config);
               $this->upload->initialize($config);
               if($this->upload->do_upload('attach_file')){
                   $fileData = $this->upload->data();
                 //  $uploadData[$i]['file_name'] = base_url().'attachment/'.$fileData['file_name'];
                    //$data['attach_file']= base_url().'uploads/'.$fileData['file_name'];
                   $attach_files=$attach_files.",".base_url().'uploads/'.$fileData['file_name'];
                
               }
              
             }
           //  echo $attach_files;
           }
           $ex_img=(isset($_POST['already_upload_img']) && $_POST['already_upload_img']!='')?$_POST['already_upload_img']:'';
           if(!empty($ex_img))
           {
            $attach_files=$attach_files.",".implode(',', $ex_img);
           }
if($attach_files!='')
{
  $data['attach_file']=implode(',', array_filter(explode(',', $attach_files)));
}
else
{
  $data['attach_file']='';
}

           // else
           // {
           //   $data['attach_file']= $user[0]['attach_file'];
           // }
/** end of 02-07-2018 **/
       // $data['attach_file'] = $attchFile;
            
            $update_data = $this->Common_mdl->update('add_new_task', $data, 'id', $_POST['taskID']);

            $Message = 'Attachment Uploaded To {TASK_SUBJECT} Task';

            $this->Common_mdl->Task_Members_Notify( $_POST['taskID'] ,'Task_Attachment_Uploaded', $Message );

             $data1= array('module_id' => $_POST['taskID'],
                        'user_id' => $_SESSION['id'],                      
                        'log' => 'Attachment Uploaded by ',
                        'module' => 'Task',
                        'CreatedTime' => time()
                );
             $this->db->insert( 'activity_log', $data1 );


        echo '<script>window.location.href="'.base_url().'user/task_details/'.$_POST['taskID'].'"';
        echo "</script>";
    }

    public function addTaskSetting()
    {      
        $id = $_POST['task_setting_id'];
        $comment = isset($_POST['hidden_cmts']) ? $_POST['hidden_cmts'] : "";
        $task_timer = isset($_POST['hidden_task_timer']) ? $_POST['hidden_task_timer'] : "";        
        $attach_file = isset($_POST['hidden_alw_attach']) ? $_POST['hidden_alw_attach'] : "";
        $user_edit_task=isset($_POST['hidden_task']) ? $_POST['hidden_task'] : "";

       if($user_edit_task==1)
       {
       // $datas['user_edit_task']='on';
        $datas['customize']=1;
        $this->Common_mdl->update('add_new_task', $datas, 'id', $id);
       }
       else
       {
       // $datas['user_edit_task']='';
        $datas['customize']=0;
        $this->Common_mdl->update('add_new_task', $datas, 'id', $id);
       }
        $data = array('task_id' => $id,
                        'comment' => $comment,
                        'task_timer' => $task_timer,
                        'attach_file' => $attach_file,
                        'user_edit_task'=>$user_edit_task,
                );

        $query = $this->Common_mdl->GetAllWithWhere('task_setting','task_id',$id);

        if(!empty($query))
        {
          $up_sql = $this->Common_mdl->update('task_setting', $data, 'task_id', $id);
        }
        else
        {
          $inn_sql = $this->Common_mdl->insert('task_setting',$data);
        }
        
          $result =  $this->db->affected_rows();

        if($result)
        {
          
          $username=$this->Common_mdl->getUserProfileName($_SESSION['id']); 

          $activity_datas['log'] = "Task Settings Was updated  by " .$username;          
          $activity_datas['createdTime'] = time();
          $activity_datas['module'] = 'Task';
          $activity_datas['sub_module']='';
          $activity_datas['user_id'] = $_SESSION['id'];
          $activity_datas['module_id'] = $id;

          $this->Common_mdl->insert('activity_log',$activity_datas);

        }

        echo $result;
         
                
    }


    public function task_UpdateComments()
    {
        $pri_id = isset($_POST['primary_id']) ? $_POST['primary_id'] : "";  
        $megs = isset($_POST['megs']) ? $_POST['megs'] : "";  

        $sql = $this->Common_mdl->UpdateStatus('task_comments', 'id', $pri_id, 'message', $megs);
        


    }

    /*add extr*/
    public function get_services_options()
    {
      if(!empty($_POST['services']))
      {
      
      $services = trim( $_POST['services'] );
      $services=explode("_",$services );
      $action= isset( $_POST['action'] ) ? $_POST['action'] : '';
      $task_id= isset($_POST['task_id'] ) ? $_POST['task_id'] : '';


      if(isset($services[1]) && $services[0]=='wor')
      {
        $table='work_flow';
        $service_id=$services[1];

      }
      else
      {
         $table='service_lists';
        $service_id=(isset($services[1]))?$services[1]:$services[0];
      }
      $data['table']=$table;
      $data['service_id']=$service_id;
      $data['service']=$this->db->query("select * from ".$table." where id=".$service_id."  ")->row_array();



      if($action=="new_inc_exclude")
        {   
          $this->load->view('tasks/inc_exc_service_options',$data);
        }
      else
        {
            if($action=='extra_steps')
            { 
              $this->load->view('tasks/services_mainsub_steps_added',$data);
            }
            else
            {

              $task_data = $this->db->query("select * from add_new_task where id = ".$task_id." 
              ")->row_array();

              if(!empty($task_data['sub_task_id']))
              {
                
                $steps_data = $this->db->query("select * from add_new_task where id IN (".$task_data['sub_task_id'].") ")->result_array(); 

                $data['exStepsId'] = array_column( $steps_data, 'services_main_id' );
              }
              else
              {
                $data['exStepsId'] = [];
              }  
              //print_r(  $data['exStepsId'] );die;
              
                $this->load->view('tasks/inc_exc_service_options',$data);
            }
        }
      
     }
      
    }
    public function get_services_options_insert(){
      
     
      $task_id=$_POST['task_id'];
      $action=$_POST['action'];
     
      if($action=='included_options')// included options
      { 
        if(!empty($_POST['services']))
         {
            $services = explode(",",$_POST['services']);
            $services = "'".implode("','",$services)."'"; 

            $get_task_data=$this->Common_mdl->select_record('add_new_task','id',$task_id);
            if(!empty($get_task_data['sub_task_id']))
            {
              
              $this->db->query("delete from add_new_task where id IN (".$get_task_data['sub_task_id'].")");
              $datas['sub_task_id']='';
              $this->Common_mdl->update('add_new_task',$datas,'id',$task_id);
              
            }
            $services_lists=$this->db->query("select * from service_lists where id IN (".$services.") ")->result_array();

            // $services_lists=$this->db->query("select * from service_lists where services_subnames IN (".$services.") and (created_user=0 or created_user=".$_SESSION['id'].") ")->result_array();
            /*$service_db=$this->db->query("select * from service_lists where services_subnames='".$services."' and (created_user=0 or created_user=".$_SESSION['id'].") ")->row_array();  */ 
            foreach ($services_lists as $services_data)
            {
              $records=$this->db->query("select * from service_steps where service_id=".$services_data['id']." and firm_id=".$_SESSION['firm_id']." ")->result_array(); 
              $i=1;
              $new_array=array();
              foreach($records as $rec)
              {
                  $main_task_title=$rec['title'];
                  $main_task_id=$rec['id'];

                  $res=$this->Task_section_model->task_creation($task_id,$main_task_title,$main_task_id,'');

                  array_push($new_array, $res);
                  
                  $steps_details=$this->db->query("select * from service_step_details where service_id=".$services_data['id']." and step_id=".$rec['id']."  ")->result_array();

                   foreach($steps_details as $steps)
                   { 

                      $sub_task_content=$steps['step_content'];
                      $sub_task_id=$steps['id'];

                      $res1=$this->Task_section_model->task_creation($task_id,$sub_task_content,$main_task_id,$sub_task_id);

                      array_push($new_array, $res1);
                     
                    }
              }

            }

            $datas['sub_task_id']=implode(',',$new_array);
            $this->Common_mdl->update('add_new_task',$datas,'id',$task_id);

            $username=$this->Common_mdl->getUserProfileName($_SESSION['id']); 
            $activity_datas['log'] = "Sub Tasks Included by " .$username;
            $activity_datas['createdTime'] = time();
            $activity_datas['module'] = 'Task';
            $activity_datas['sub_module']='';
            $activity_datas['user_id'] = $_SESSION['id'];
            $activity_datas['module_id'] = $task_id;

            $this->Common_mdl->insert('activity_log',$activity_datas);
      }
    }
    else // end of excluded options
    {
      $get_task_data=$this->Common_mdl->select_record('add_new_task','id',$task_id);
      if(count($get_task_data)>0)
      {
        $sub_ids=($get_task_data['sub_task_id']!='')?explode(',',$get_task_data['sub_task_id']):'';
        if($sub_ids!='')
        {
          foreach ($sub_ids as $sub_key => $sub_value) {
            $this->Common_mdl->delete('add_new_task','id',$sub_value);
          }
          $datas['sub_task_id']='';
          $this->Common_mdl->update('add_new_task',$datas,'id',$task_id);


                $username=$this->Common_mdl->getUserProfileName($_SESSION['id']); 

                $activity_datas['log'] = "Sub Task Excluded by " .$username;
                $activity_datas['createdTime'] = time();
                $activity_datas['module'] = 'Task';
                $activity_datas['sub_module']='';
                $activity_datas['user_id'] = $_SESSION['id'];
                $activity_datas['module_id'] = $task_id;

                $this->Common_mdl->insert('activity_log',$activity_datas);

        }

      }
    }
  }

public function remider_summary_task()
 {

$reminder_specific_time = $_POST['date'];

$reminder_specific_timepic = $_POST['time'];

$time = strtotime($reminder_specific_timepic);
      $round = 5*60;
      $rounded = round($time / $round) * $round;
      $reminder_specific_timepic=date("h:i A", $rounded);

$remainder = explode( "," , $_POST['remainder_data'] );



      $if_no=array("no"=>"nobody");
      $if_hour=array("+1 hour"=>"1hour","+2 hour"=>"2hour","+4 hour"=>"4hour","+5 hour"=>"5hour","+8 hour"=>"8hour");
      $if_to=array("10:00 AM"=>"tomorrow_morning","01:00 PM"=>"tomorrow_afternoon");
      $if_day=array("+1 days"=>"1day","+2 days"=>"2day","+4 days"=>"4day","+6 days"=>"6day","+8 days"=>"8day");
      $if_week=array("+1 week"=>"1week","+2 week"=>"2week","+4 week"=>"4week","+6 week"=>"6week","8week");
      $if_month=array("+1 months"=>"1month","+2 months"=>"2month","+4 months"=>"4month","+6 months"=>"6month","+8 months"=>"8month");

      $new_array=array("no"=>"nobody","+1 hour"=>"1hour","+2 hour"=>"2hour","+4 hour"=>"4hour","+5 hour"=>"5hour","+8 hour"=>"8hour","tomorrow_morning","tomorrow_afternoon","+1 days"=>"1day","+2 days"=>"2day","+4 days"=>"4day","+6 days"=>"6day","+8 days"=>"8day","+1 week"=>"1week","+2 week"=>"2week","+4 week"=>"4week","+6 week"=>"6week","8week","+1 months"=>"1month","+2 months"=>"2month","+4 months"=>"4month","+6 months"=>"6month","+8 months"=>"8month");

      $for_new_array=array();

     // echo date("h:i:A", strtotime(''.$reminder_specific_timepic.' '.$zzx));
      foreach ($remainder as $re_key => $re_value) {

      $if_no_arr = array_search($re_value, $if_no);
       if ($if_no_arr !== false) {
        // echo $if_no[$if_no_arr];
        // echo "<br>";
       }
      $if_hour_arr = array_search($re_value, $if_hour);
       if($if_hour_arr != false)
       {
    
     $its_time= date("h:i A", strtotime(''.$reminder_specific_timepic.' '.$if_hour_arr));

     $for_new_array[$re_value]=date("jS-M-Y", strtotime($reminder_specific_time) )."//".$its_time;

   //  echo $for_new_array[$re_value];
     
       }
      $if_to_arr = array_search($re_value, $if_to);
       if($if_to_arr != false)
       {
     
       $its_date= date("jS-M-Y", strtotime(''.$reminder_specific_time.' +1 days'));

     $for_new_array[$re_value]=$its_date."//".$if_to_arr;
      
       }
      $if_day_arr = array_search($re_value, $if_day);
       if($if_day_arr != false)
       {
     
     $its_date= date("jS-M-Y", strtotime(''.$reminder_specific_time.' '.$if_day_arr));
     $for_new_array[$re_value]=$its_date."//".$reminder_specific_timepic;
      
       }
      $if_week_arr = array_search($re_value, $if_week);
       if($if_week_arr != false)
       {
     
        $its_date= date("jS-M-Y", strtotime(''.$reminder_specific_time.' '.$if_week_arr));
     $for_new_array[$re_value]=$its_date."//".$reminder_specific_timepic;
      
       }
      $if_month_arr = array_search($re_value, $if_month);
       if($if_month_arr != false)
       {
     
        $its_date= date("jS-M-Y", strtotime(''.$reminder_specific_time.' '.$if_month_arr));
     $for_new_array[$re_value]=$its_date."//".$reminder_specific_timepic;
      
       }


       
      }

      echo json_encode($for_new_array);

 }


     public function get_services_options_for_task()
    {
      if(!empty($_POST['services']))
      {
      
      $services = trim( $_POST['services'] );
      $services=explode("_",$services );
      $action= isset( $_POST['action'] ) ? $_POST['action'] : '';
      $task_id= isset($_POST['task_id'] ) ? $_POST['task_id'] : '';


      if(isset($services[1]) && $services[0]=='wor')
      {
        $table='work_flow';
        $service_id=$services[1];

      }
      else
      {
         $table='service_lists';
        $service_id=(isset($services[1]))?$services[1]:$services[0];
      }
      $data['table']=$table;
      $data['service_id']=$service_id;
      $data['service']=$this->db->query("select * from ".$table." where id=".$service_id."  ")->row_array();


        $task_data = $this->db->query("select * from add_new_task where id = ".$task_id." 
        ")->row_array();

        if(!empty($task_data['sub_task_id']))
        {
          
          $steps_data = $this->db->query("select * from add_new_task where id IN (".$task_data['sub_task_id'].") ")->result_array(); 

          $data['exStepsId'] = array_column( $steps_data, 'services_main_id' );
          $data['sub_task_id'] = array_column( $steps_data, 'id' );
        }
        else
        {
          $data['exStepsId'] = [];
          $data['sub_task_id']=[];
        }  
        //print_r(  $data['exStepsId'] );die;
        
          $this->load->view('tasks/inc_exc_service_options_for_task',$data);
      }
        
      
     }
   
}
?>
