<?php
// error_reporting(E_STRICT);
class Proposal_page extends CI_Controller 
{
        public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";

        public function __construct()
        {
            parent::__construct();
            $this->load->model(array('Common_mdl','Proposal_model','Security_model','Loginchk_model'));
            // $this->load->library('Excel');
            //$this->load->library('encrypt');
            //$this->load->library('encryption');
            $this->load->helper(['comman']);
        }

        public function index()
        {
            $this->Proposal_model->index();             
        }
        
        /* Add Taxes Fees Schedule */
        public function add_taxes()
        {
            $this->Security_model->chk_login(); 
            $this->session->unset_userdata('tab_name');
            $this->session->set_userdata('tab_name', 'tax_tab');
            $user_id = $this->session->userdata['userId'];

            $value=array('tax_name'=>$_POST['tax_name'],'tax_rate'=>$_POST['tax_rate'],'tax_status'=>'0','status'=>'active','user_id'=>$user_id,'firm_id' => $_SESSION['firm_id'],'active_status'=>'1');
            $data=$this->Common_mdl->insert('proposal_tax',$value);

            $this->session->set_flashdata('success', 'Record Added Successfully!'); 
            redirect('Proposal/fee_schedule');
        }
        /* Tax Edit */
        public function edit_tax(){
            $this->Security_model->chk_login();
            $this->session->unset_userdata('tab_name');
            $this->session->set_userdata('tab_name', 'tax_tab');    
            $id=$_POST['id'];
            $value=array('tax_name'=>$_POST['tax_name'],'tax_rate'=>$_POST['tax_rate'],'tax_status'=>'0','status'=>'active');
             $data=$this->Common_mdl->update('proposal_tax',$value,'id',$id);
              $this->session->set_flashdata('success', 'Updated Successfully!'); 
            redirect('Proposal/fee_schedule');
        }
        /* Delete Tax */
        public function delete_tax(){
            $this->session->unset_userdata('tab_name');
            $this->session->set_userdata('tab_name', 'tax_tab');
            $checkbox=$_POST['checkbox'];
            for($i=0;$i<count($checkbox);$i++){           
            $id=$checkbox[$i];  
             $delete=$this->Common_mdl->delete('proposal_tax','id',$id);
            }
            $this->session->set_flashdata('success', 'Record Deleted Successfully!'); 
            redirect('Proposal/fee_schedule');
        }
        /* Update Tax Status */
        public function update_tax_status(){            
            $id=$_POST['id'];          
            $active_check=$this->db->query("select active_status from proposal_tax where id=".$id."")->row_array();         
            if($active_check['active_status']=='1'){
            $this->db->query("UPDATE `proposal_tax` SET `tax_status`='0'");         
            $value=array('tax_status'=>'1');
            $data['status']=$this->Common_mdl->update('proposal_tax',$value,'id',$id);   
            }else{
                $data['status']='0';
            }  
            echo json_encode($data);            
        }
         public function update_active_status(){           
            $id=$_POST['id'];
            $status = ($_POST['status'] == 1) ? 1 : 0;
            $value=array( 'active_status' => $status );
            $this->Common_mdl->update('proposal_tax',$value,'id',$id);              
        }
        /*New Proposal View page */
        public function new_proposal(){
            $this->Security_model->chk_login();
            $this->load->view('proposal/new_proposal');
        }
        /* Insert Proposal */
        public function insert_proposal(){
            $this->Security_model->chk_login();
            $title=$_POST['title'];
            $value=array('body'=>$_POST['body'],'subject'=>$_POST['subject'],'status'=>'active');
            $data=$this->Common_mdl->update('proposal_proposaltemplate',$value,'title',$title);
             echo json_encode($data);
        }
        /*Update Proposal */
        public function update_proposal(){
            $this->Security_model->chk_login();
            $title=$_POST['title'];
            $value=array("type"=>$_POST['type'],'body'=>$_POST['body'],'subject'=>$_POST['subject'],'status'=>'active','title'=>$_POST['title'],'reminder_heading'=>1,'user_id'=>$_SESSION['id']);
            $data=$this->Common_mdl->insert('proposal_proposaltemplate',$value);
             echo json_encode($data);
        }        
        /*Select proposal */
        public function select_proposal(){
            $title=$_POST['title'];        
            $data=$this->Common_mdl->select_record('proposal_proposaltemplate','title',$title);         
            echo json_encode($data);
        }
        /*Title Check */
        public function title_check(){
            $title=$_POST['title'];
            $query=$this->db->query("select * from proposal_proposaltemplate where title='".$title."'")->result_array();
            $count=count($query);
            echo $count;
        }

        public function step_proposal($id=false)
        {
            $this->Security_model->chk_login();  

            $Assigned_Client = Get_Assigned_Datas('CLIENT'); 
            $assigned_client = implode(',',$Assigned_Client);

            $Assigned_Leads = Get_Assigned_Datas('LEADS');
            $assigned_leads = implode(',',$Assigned_Leads);      
            
            $data['clients'] = $this->db->query("SELECT * FROM client WHERE autosave_status = 0 and firm_id = '".$_SESSION['firm_id']."' AND FIND_IN_SET(id,'".$assigned_client."') AND crm_company_name != '' order by id desc")->result_array();

            $proposal_templates = $this->Common_mdl->getproposal_templates();

            $data['my_template'] =  $proposal_templates['firm'];
            $data['all'] =  $proposal_templates['all'];
            //$data['template_title'] = array_merge($proposal_templates['firm'],$proposal_templates['all']);

            $proposal_number=$this->db->query("SELECT max(id) as id FROM proposals")->row_array();
          
            if(empty($proposal_number))
            {
               $data['proposal_number']='0001';
            }
            else
            {
               $data['proposal_number']= $proposal_number['id'] + 1;
            }

            $data['taxes'] = $this->Common_mdl->GetAllWithWheretwo('proposal_tax','active_status','1','firm_id',$_SESSION['firm_id']);
            $tax = $this->Common_mdl->GetAllWithWheretwo('proposal_tax','tax_status','1','firm_id',$_SESSION['firm_id']);
            $data['tax_details'] = $tax[0];
            $data['category']=$this->Common_mdl->GetAllWithWhere('proposal_category','firm_id',$_SESSION['firm_id']);

            $category = $data['category'];
            $product_category=array();
            $service_category=array();
            $subscription_category=array();

            foreach($category as $cat)
            {
                if($cat['category_type']=='product'){
                     array_push($product_category,"'".$cat['category_name']."'");
                }

                 if($cat['category_type']=='service'){
                     array_push($service_category,"'".$cat['category_name']."'");
                }

                 if($cat['category_type']=='subscription'){
                     array_push($subscription_category,"'".$cat['category_name']."'");
                }
            }

             $subscription_category=implode(',',$subscription_category);
             $product_category=implode(',',$product_category);
             $service_category=implode(',',$service_category);

             $count1=explode(',',$subscription_category);
             $count2=explode(',',$product_category);
             $count3=explode(',',$service_category);

             $firm_services = $this->Common_mdl->getServicelist();
             $firm_enabled_services = array();        

             function firm_enabled($val)
             {          
                return '"'.$val['service_name'].'"';
             }
             
            $firm_enabled_services = array_map('firm_enabled',$firm_services); 
             //$firm_enabled_services = implode("','", $firm_enabled_services);

            $default_services = $this->db->query('SELECT service_name FROM service_lists WHERE id<17')->result_array();
            $default_services = array_map('firm_enabled',$default_services); 
            //$default_services = implode("','", $default_services);

            $array_diff = array_diff($default_services, $firm_enabled_services);
            
            if(count($array_diff) > '0')
            {
               $firm_disabled_services = implode(',',$array_diff);
            }

            if($count1['0']!='')
            {
               $data['subscription']=$this->db->query("select * from proposal_subscriptions where firm_id = '".$_SESSION['firm_id']."' and  subscription_category in (".$subscription_category.")")->result_array();
            }
            else
            {
               $data['subscription']=$this->db->query("select * from proposal_subscriptions where firm_id = '".$_SESSION['firm_id']."' and  ( subscription_category!='' and subscription_category!='0')")->result_array();
            }


            if($count3['0']!='')
            {
               $sql = "SELECT * from proposal_service where firm_id = '".$_SESSION['firm_id']."' and  service_category in (".$service_category.")";
            }
            else
            {
               $sql = "SELECT * from proposal_service where firm_id = '".$_SESSION['firm_id']."' and ( service_category!='' and service_category!='0')"; 
            }

            if(isset($firm_disabled_services))
            {
               $sql .= " AND service_name NOT IN(".$firm_disabled_services.")";
            }

            $data['services'] = $this->db->query($sql)->result_array();

            if($count2['0']!=''){
                $data['products']=$this->db->query("select * from proposal_products where firm_id = '".$_SESSION['firm_id']."' and  product_category in (".$product_category.")")->result_array();

            }else{
               $data['products']=$this->db->query("select * from proposal_products where firm_id = '".$_SESSION['firm_id']."' and ( product_category!='')")->result_array();
            }

          
            $data['pricelist_settings']=$this->Common_mdl->select_record('pricelist_settings','firm_id',$_SESSION['firm_id']);
            $data['pricelist']=$this->Common_mdl->GetAllWithWhere('proposal_pricelist','firm_id',$_SESSION['firm_id']);
            $data['countries']=$this->Common_mdl->getallrecords('countries');
            $data['currency']=$this->Common_mdl->getallrecords('currency');
            $data['documents']=$this->Common_mdl->getallrecords('document_record');
            $data['admin_settings']=$this->Common_mdl->select_record('admin_setting','firm_id',$_SESSION['firm_id']);
            $data['leads_status']=$this->Common_mdl->getallrecords('leads_status');
            $data['source']=$this->Common_mdl->getallrecords('source');

            $leads_status = $this->db->query("SELECT id FROM `leads_status` where status_name = 'Win' AND firm_id = '0'")->row_array();

            $data['leads'] = $this->db->query("SELECT * FROM leads WHERE firm_id = '".$_SESSION['firm_id']."' AND FIND_IN_SET(id,'".$assigned_leads."') AND lead_status != '".$leads_status['id']."' AND lead_status!=''")->result_array();

            $proposal_actions = $this->Common_mdl->GetAllWithWhere('email_templates_actions','module','proposal');
            $proposal_actions = array_column($proposal_actions, 'action_id'); 
            $proposal_actions = implode(',', $proposal_actions);

            $data['proposal_templates'] = $this->Common_mdl->getTemplates('',$proposal_actions);

            if(!empty($_GET['lead_id']))
            { 
              $data['get_lead_id'] = $_GET['lead_id'];
            }                

            if($id!='' && $id!=false)
            {              
                 $value=explode( "---", $id );            
                 $values=end($value); 
                 $this->session->set_userdata('proposal_id', $values);
                 $data['records']=$this->Common_mdl->select_record('proposals','id',$values);
                 $company_name=  $data['records']['company_name'];
                 $data['company_details']=$this->Common_mdl->select_record('client','crm_company_name',$company_name);
                 $data['conversation']=$this->Common_mdl->GetAllWithWhereorderBy('proposal_discussion','proposal_id', $values);
                 $data['proposal_history']=$this->db->query("SELECT * FROM `proposal_history` where proposal_id='".$values."' group by tag,DATE_FORMAT(`created_at`, '%Y-%m-%d %H:%i') order by id desc")->result_array();  

                 $followup=$this->Common_mdl->getTemplates(6);
                 $followup = $followup[0];

                 $sender_details=$this->Common_mdl->select_record('proposals','id',$values);

                 $senders_details=$this->Common_mdl->select_record('user','id',$sender_details['user_id']);
                 $sender_email=$senders_details['crm_email_id']; 
                 $sender_name=$senders_details['crm_name']; 
                 $sender_last_name=$senders_details['crm_last_name']; 
                 $sender_country=$senders_details['crm_country'];
                 $sender_city=$senders_details['crm_city'];
                 $sender_religion=$senders_details['crm_state']; 
        
                 $sender_company_details=$this->Common_mdl->select_record('client','user_id',$sender_details['user_id']);
                 if($sender_company_details[0]==''){
                  $sender_company=$sender_details['sender_company_name'];
                  $sender_mobile_number='';
                 }else{
                  $sender_company=$sender_company_details['crm_company_name'];
                  $sender_mobile_number=$sender_company_details['crm_mobile_number'];
                 }

                 $currency=$sender_details['currency'];
                 $expiration_date=$sender_details['expiration_date'];
                 $sender_id=$sender_details['company_id'];  
                 $proposal_name=$sender_details['proposal_name'];
                 $proposal_no=$sender_details['proposal_no'];
                 $sender_company=$sender_details['sender_company_name'];

                 if($sender_id!=1)
                 {
                    $user_details=$this->Common_mdl->select_record('user','id',$sender_id);
                    $client_details=$this->Common_mdl->select_record('client','user_id',$sender_id);
                    $client_website=$client_details['crm_company_url'];
                    $client_mobile_number=$client_details['crm_mobile_number'];
                    $email=$user_details['crm_email_id']; 
                    $name=$user_details['crm_name']; 
                    $last_name=$user_details['crm_last_name']; 
                    $country=$user_details['crm_country'];
                    $city=$user_details['crm_city'];
                    $religion=$user_details['crm_state'];   
                    $company_name=$sender_details['company_name'];          
                }
                else
                {
                    $email=$sender_details['receiver_mail_id']; 
                    $client_website='';
                    $client_mobile_number='';
                    $name=''; 
                    $last_name=''; 
                    $country='';
                    $city='';
                    $religion=''; 
                    $company_name=$sender_details['receiver_company_name'];
                }
              
              $pdf_signature=$this->Common_mdl->select_record('pdf_signatures','proposal_no',$values);
            
              if(is_array($pdf_signature) && count($pdf_signature) < 0 ){
                $email_signature='';
              }else{
                $email_signature='<img src='.$pdf_signature['signature_path'].'>';
              }

             $body1=$followup['body'];
             $subject=$followup['subject'];
             $random_string=$this->generateRandomString();
             $link=base_url().'Proposal_page/step_proposal/'.$random_string.'---'.$values;
       
               $a1  =   array('::Client Name::'=>$name,
                '::Client Company::'=>$company_name,
                '::Client Company City::'=>$city,
                '::Client Company Country::'=>$country,
                '::Client Company Religion::'=>$religion,
                '::Client First Name::'=>$name,
                '::Client Last Name::'=>$last_name,
                ':: Client Name::'=>$name,
                '::ClientPhoneno::'=> $client_mobile_number,
                '::ClientWebsite::'=>$client_website,
                ':: Proposal Link ::'=>$link,
                ':: ProposalCurrency::'=>$currency,     
                ':: ProposalExpirationDate::'=>$expiration_date,
                ':: ProposalName::'=>$proposal_name,
                ':: ProposalCodeNumber::'=>$proposal_no,
                '::SenderCompany::'=>$sender_company,
                '::SenderCompanyAddress::'=>'',
                '::SenderCompanyCity::'=>$sender_city,
                '::SenderCompanyCountry::'=>$sender_country,
                '::SenderCompanyReligion::'=>$sender_religion,
                '::SenderEmail::'=>$sender_email,
                '::SenderEmailSignature::'=>$email_signature,
                '::SenderName::'=>$sender_name,
                '::senderPhoneno::'=>$sender_mobile_number,
                ':: Date ::'=>date("Y-m-d h:i:s"));

                $data['followup_body']  =   strtr($body1,$a1); 
                $data['followup_subject']  =   strtr($subject,$a1);
                $data['internal_notes']=$this->Common_mdl->GetAllWithWhere('proposal_internalnotes','proposal_id',$values); 
                $data['discussion_attachments']=$this->Common_mdl->GetAllWithWhere('proposal_discussion_attachments','proposal_id',$values); 
                
                $this->load->view('proposal/end_proposal_view',$data);
            }
            else
            {  
                $this->load->view('proposal/step_proposal_view',$data);
            }
        }
        public function insert_Company()
        {
            $company_name=$_POST['company_name'];
            $phone=$_POST['phone'];
            $country=$_POST['country'];  
            $state=$_POST['state'];  
            $postal_code=$_POST['postal_code'];  
            $address=$_POST['address'];    
            $website=$_POST['website'];  
            $first_name=$_POST['first_name'];  
            $last_name=$_POST['last_name'];  
            $email=$_POST['email'];
            
            $leads_status = $this->db->query('SELECT id FROM leads_status WHERE firm_id = "0" AND status_name IN ("New Lead")')->row_array();

            $leads=array('lead_status'=>$leads_status['id'],'name'=>$company_name,'email_address'=>$email,'website'=>$website,'phone'=>$phone,'address'=>$address,'state'=>$state,'country'=>$country,'zip_code'=>$postal_code,'default_language'=>1,'user_id'=>$_SESSION['id'],'createdTime'=>time(),'firm_id'=>$_SESSION['firm_id'],'public' => 'on');
            $value=$this->Common_mdl->insert('leads',$leads);
            $in1=$this->db->insert_id();

            $assignee_FA = $this->db->query("SELECT id FROM organisation_tree WHERE firm_id = '".$_SESSION['firm_id']."' AND parent_id = '0'")->row_array();
            $assignee_FU = $this->db->query("SELECT id FROM organisation_tree WHERE firm_id = '".$_SESSION['firm_id']."' AND user_id = '".$_SESSION['id']."'")->row_array();
            $assignees = $assignee_FA['id'];

            if($assignee_FU['id']!=$assignees)
            {
                $assignees = $assignees.":".$assignee_FU['id'];
            }
                         
            $data = ['firm_id'=>$_SESSION['firm_id'],'module_name'=>'LEADS','module_id'=>$in1,'assignees'=>$assignees];
            $this->db->insert('firm_assignees',$data); 

            $data=array('company_name'=>$company_name,'phone'=>$phone,'country'=>$country,'state'=>$state,'postal_code'=>$postal_code,'address'=>$address,'website'=>$website,'first_name'=>$first_name,'last_name'=>$last_name,'email'=>$email,'firm_id'=>$_SESSION['firm_id']);
            $last_id=$this->Common_mdl->insert('proposal_compamy',$data);

            $values['category_add']='';  

            $Assigned_Leads = Get_Assigned_Datas('LEADS');
            $assigned_leads = implode(',',$Assigned_Leads);

            $leads_status = $this->db->query("SELECT id FROM `leads_status` where status_name='customer'")->row_array();
            $results = $this->db->query("SELECT * FROM leads WHERE firm_id = '".$_SESSION['firm_id']."' AND FIND_IN_SET(id,'".$assigned_leads."') AND lead_status != '".$leads_status['id']."' AND lead_status!=''")->result_array();  
             
            $values['category_add'].='<div class="dropdown-sin-4 company_search"><select name="company_name" id="company_name" class="required" onchange="company_changing(this)"><option value="">Select Company</option>';
                
            foreach($results as $cat)
            { 
                $cat['id'] == $in1?$selected = "selected":$selected = "";
                $values['category_add'].='<option value="Leads_'.$cat['id'].'" '.$selected.'>'.$cat['name'].'</option>';
            } 

            $Assigned_Client = Get_Assigned_Datas('CLIENT'); 
            $assigned_client = implode(',',$Assigned_Client);

            $clients = $this->db->query("SELECT * FROM client WHERE autosave_status = 0 and firm_id = '".$_SESSION['firm_id']."' AND FIND_IN_SET(id,'".$assigned_client."') AND crm_company_name != '' order by id desc")->result_array();  

            if(count($clients) > 0)  
            {  
                foreach($clients as $key => $val)
                {  
                    $values['category_add'].=' <option value="'.$val['user_id'].'">'.$val["crm_company_name"].'</option>';
                }
            }

           $values['category_add'].='</select></div><div class="gmail-tax"><div class="tax-consul-new"><a href="#"  data-toggle="modal" data-target="#send-to" class="linking"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> Add Company</a></div> <div class="cmn-tax-div">
                               <div class="tax-consul">
                                 <div class="img-tax profile_pics">
                                    <img src="'.base_url().'uploads/825898.png" alt="image">
                                 </div>
                                 <div class="tax-content con12 client_details">
                                  <span>'.ucwords($company_name).'</span>
                                     <strong>'.$email.'</strong>
                                     <input type="hidden" name="receiver_company_name" id="receiver_company_name" value="'.$company_name.'">
                                     <input type="hidden" name="receiver_company_name_old" id="receiver_company_name_old" value="'.$company_name.'">
                                  <input type="hidden" name="receiver_mail_id" id="receiver_mail_id" value="'.$email.'">
                                   <input type="hidden" name="receiver_mail_id_old" id="receiver_mail_id_old" value="'.$email.'">';
            !empty($phone)? $values['category_add'] .= '<a href="#"><b>ph:</b>'.$phone.'</a></div></div></div></div>': $values['category_add'] .= '</div></div></div></div>';  

            echo json_encode($values);
        }

        /*Shashmethah*/
        public function proposal_template()
        {
            $this->Security_model->chk_login();                
         
            $data['send']=$this->Proposal_model->proposal_details('sent');
            $data['opened']=$this->Proposal_model->proposal_details('opened');
            $data['indiscussion']=$this->Proposal_model->proposal_details('in discussion');
            $data['accepted']=$this->Proposal_model->proposal_details('accepted');
            $data['decline']=$this->Proposal_model->proposal_details('declined');
            $data['draft']=$this->Proposal_model->proposal_details('draft');
            $data['expired']=$this->Proposal_model->proposal_details('expired');
            $data['archive']=$this->Proposal_model->proposal_details('archive');
            $data['proposal']=$this->Proposal_model->proposals();

            $this->load->view('proposal/proposal-template',$data);
        }

        public function deleteRecords()
        {            
            if( !empty( $_POST['data_id'] ) )
            { 
                $id = implode(',', $_POST['data_id'] ); 
                
                $sql = $this->db->query("DELETE FROM proposals WHERE id in ($id)");
                echo $this->db->affected_rows();
            }   
        }
        
        public function templates()
        { 
            if($_SESSION['firm_id'] == '0' || $_COOKIE['remindoo_logout'] == 'remindoo_superadmin') 
            {
               $this->Loginchk_model->superAdmin_LoginCheck();
            }
            else
            {
               $this->Security_model->chk_login();
            }                                     
            
            $proposal_templates = $this->Common_mdl->getproposal_templates();

            $data['my_template'] =  $proposal_templates['firm'];
            $data['all'] =  $proposal_templates['all'];
            
            $this->load->view('proposal/steps_proposal',$data);
        }

        public function check_distinct()
        {           
            $id = $_POST['id'];           

            $sql = "SELECT * FROM proposal_template WHERE firm_id = '".$_SESSION['firm_id']."' AND ".$_POST['field']." LIKE '".trim($_POST['value'])."'";

            if($id != '')
            {
               $sql = $sql." AND id!='".$id."'";
            }
          
            $data = $this->db->query($sql)->result_array(); 
            echo count($data);
        }

        public function delete_pro_template()
        {
            $data = $this->Common_mdl->delete('proposal_template','id',$_POST['id']);
            echo $data;
        }
         /*Shashmethah*/
         public function template_design(){
            $user_id=$this->session->userdata['userId'];       
            $this->Security_model->chk_login();
            $data['category']=$this->Common_mdl->GetAllWithWhere('proposal_category','user_id',$user_id);
            $data['products']=$this->Common_mdl->GetAllWithWhere('proposal_products','user_id',$user_id);
            $data['services']=$this->Common_mdl->GetAllWithWhere('proposal_service','user_id',$user_id);
            $data['taxes']=$this->Common_mdl->GetAllWithWhere('proposal_tax','user_id',$user_id);
            $this->load->view('proposal/template_design',$data);
        }

        public function search_Service()
        {
            $service_name = $_POST['service_name'];

            $firm_enabled_services = $this->Common_mdl->getServicelist();        

            function firm_enabled($val)
            {          
               return '"'.$val['service_name'].'"';
            }
             
            $firm_enabled_services = array_map('firm_enabled',$firm_enabled_services); 

            $default_services = $this->db->query('SELECT service_name FROM service_lists WHERE id<17')->result_array();
            $default_services = array_map('firm_enabled',$default_services); 

            $array_diff = array_diff($default_services, $firm_enabled_services);
            $sql = '';

            if(count($array_diff) > '0')
            {
               $sql .= " AND service_name NOT IN(".implode(',',$array_diff).")";
            }

            $result=$this->db->query("SELECT * FROM `proposal_service` where service_name like '%".$service_name."%' AND firm_id = '".$_SESSION['firm_id']."'".$sql)->result_array();

            $data['contents']='';
            if(!empty($result))
            {
                foreach($result as $res)
                {
                     $data['contents'].='<div class="annual-lorem" id="'.$res['id'].'" onclick="myFunction(this.id)"><h3>'.$res['service_name'].'</h3> <strong>- &euro;'.$res['service_price'].'/'.$res['service_unit'].'</strong></div>';
                }
            }
            else
            {
                 $data['contents'].='<div class="annual-lorem"><h3>No Results Found</h3></div>';
            }

            echo json_encode($data);
        }

        public function search_Product(){
            $service_name=$_POST['service_name'];
            $result=$this->db->query("SELECT * FROM `proposal_products` where product_name like '%".$service_name."%' AND firm_id = '".$_SESSION['firm_id']."'")->result_array();
            $data['contents']='';
            if(!empty($result)){
                foreach($result as $res){
                     $data['contents'].='<div class="annual-lorem" id="'.$res['id'].'" onclick="myFunction2(this.id)"><h3>'.$res['product_name'].'</h3> <strong>- &euro;'.$res['product_price'].'</strong></div>';
                }
            }else{
                 $data['contents'].='<div class="annual-lorem"><h3>No Results Found</h3></div>';
            }
            echo json_encode($data);
        }
        public function search_ServiceBYid(){
            $id=$_POST['id'];
             $result=$this->Common_mdl->select_record('proposal_service','id',$id);
                $data['item_name']=$result['service_name'];
                $data['item_price']=$result['service_price'];
                $data['item_qty']=$result['service_qty'];
                $data['item_unit']=$result['service_unit'];
                $data['item_description']=$result['service_description'];
                $data['service_category']=$result['service_category'];
                echo json_encode($data);
        }
        public function search_ProductBYid(){
            $id=$_POST['id'];
             $result=$this->Common_mdl->select_record('proposal_products','id',$id);
                $data['item_name']=$result['product_name'];
                $data['item_price']=$result['product_price'];                     
                $data['item_description']=$result['product_description'];
                 $data['product_category']=$result['product_category'];
                echo json_encode($data);
        }
        public function add_subscription()
        {
            $this->Security_model->chk_login();  
            $this->session->unset_userdata('tab_name');
            $this->session->set_userdata('tab_name', 'subscription_tab');

            $user_id=$this->session->userdata['userId'];
            $service_category=$_POST['subscription_category'];
            $service_category_name=$this->db->query("select category_name from proposal_category where id='".$service_category."'  and  category_type='subscription'")->row_array();
            $service_category_name=$service_category_name['category_name'];

            $value=array('subscription_name'=>$_POST['subscription_name'],'subscription_price'=>$_POST['subscription_price'],'subscription_unit'=>$_POST['subscription_unit'],'status'=>'active','user_id'=>$user_id,'subscription_description'=>$_POST['subscription_description'],'subscription_category'=>$service_category_name,'subscription_category_id'=>$_POST['subscription_category'],'firm_id' => $_SESSION['firm_id']);
            $data=$this->Common_mdl->insert('proposal_subscriptions',$value);

            redirect('Proposal/fee_schedule'); 
        }
        public function delete_subscription(){
            $this->session->unset_userdata('tab_name');
            $this->session->set_userdata('tab_name', 'subscription_tab');
            $checkbox=$_POST['checkbox'];
            for($i=0;$i<count($checkbox);$i++){           
            $id=$checkbox[$i];  
             $delete=$this->Common_mdl->delete('proposal_subscriptions','id',$id);
            }
            $this->session->set_flashdata('success', 'Record Deleted Successfully!'); 
            redirect('Proposal/fee_schedule');
        }
        public function edit_subscription(){
            $id=$_POST['id'];
             $this->session->set_userdata('tab_name', 'subscription_tab');
            $user_id=$this->session->userdata['userId'];
              $service_category=$_POST['subscription_category'];
             $service_category_name=$this->db->query("select category_name from proposal_category where id='".$service_category."'  and  category_type='subscription'")->row_array();
             $service_category_name=$service_category_name['category_name'];

            $value=array('subscription_name'=>$_POST['subscription_name'],'subscription_price'=>$_POST['subscription_price'],'subscription_unit'=>$_POST['subscription_unit'],'status'=>'active','user_id'=>$user_id,'subscription_description'=>$_POST['subscription_description'],'subscription_category'=>$service_category_name,'subscription_category_id'=>$_POST['subscription_category']);
            $data=$this->Common_mdl->update('proposal_subscriptions',$value,'id',$id);
            redirect('Proposal/fee_schedule'); 
        }
        public function Subscription_Export()
        {
            $user_id=$this->session->userdata['userId'];  
            $this->session->unset_userdata('tab_name');

            $records=$this->Common_mdl->GetAllWithWhere('proposal_subscriptions','firm_id',$_SESSION['firm_id']);

            $filename = 'Subscription-list.xlsx';   
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/csv; "); 
            $file = fopen('php://output', 'w'); 
            $header = array("S.NO","Subscription Name","Subscription Price","Subscription Unit","Subscription Description");
            fputcsv($file, $header);
            $i=1;

            foreach($records as $data)
            {
                $value['id']=$i; 
                $value['subscription_name']=$data['subscription_name']; 
                $value['subscription_price']=$data['subscription_price']; 
                $value['subscription_unit']=$data['subscription_unit'];
                $value['subscription_description']=$data['subscription_description'];
                $i++;
                fputcsv($file,$value);
            }   

            fclose($file);
            exit; 
          //  $this->load->view('proposal/subscriptionexcel_list',$post);
        }

        public function Price_list_Export()
        {
            $user_id=$this->session->userdata['userId'];  
            $this->session->unset_userdata('tab_name');

            $records=$this->Common_mdl->GetAllWithWhere('proposal_pricelist','firm_id',$_SESSION['firm_id']);
            $filename = 'Pricelist.xlsx';   
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/csv; "); 
            $file = fopen('php://output', 'w'); 
            $header = array("S.NO","pricelist_name","service_name","service_price","service_unit","service_description","service_category","service_qty","product_name","product_price","product_category","product_description","product_qty","subscription_name","subscription_price","subscription_unit","subscription_description",    "subscription_category","Service_tax","service_discount","product_tax","product_discount","  subscription_tax","subscription_discount");
            fputcsv($file, $header);
            $i=1;

            foreach($records as $data)
            {
                $value['id']=$i; 
                $value['pricelist_name']=$data['pricelist_name'];            
                $value['service_name']=$data['service_name']; 
                $value['service_price']=$data['service_price'];  
                $value['service_unit']=$data['service_unit']; 
                $value['service_description']=$data['service_description'];
                $value['service_category']=$data['service_category']; 
                $value['service_qty']=$data['service_qty'];
                $value['product_name']=$data['product_name']  ; 
                $value['product_price']=$data['product_price']; 
                $value['product_category']=$data['product_category']; 
                $value['product_description']=$data['product_description'];
                $value['product_qty']=$data['product_qty'];
                $value['subscription_name']=$data['subscription_name'];
                $value['subscription_price']=$data['subscription_price'];
                $value['subscription_unit']= $data['subscription_unit'];
                $value['subscription_description']= $data['subscription_description'];
                $value['subscription_category']=$data['subscription_category'] ;
                $value['service_tax']=$data['tax'];
                $value['service_discount ']=$data['discount'];
                $value['product_tax']=$data['product_tax'];
                $value['product_discount'] =$data['product_discount'];
                $value['subscription_tax']=$data['subscription_tax'];   
                $value['subscription_discount']=$data['subscription_discount'];
              
              $i++;
              fputcsv($file,$value);
            }  

            fclose($file);
            exit;          
        }

         public function Tax_Export()
         {     
            $this->session->unset_userdata('tab_name');

            $records=$this->Common_mdl->GetAllWithWhere('proposal_tax','firm_id',$_SESSION['firm_id']);
            $filename = 'Tax-list.xlsx';   
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/csv; "); 
            $file = fopen('php://output', 'w'); 
            $header = array("S.NO","Tax Name","Tax Rate");
            fputcsv($file, $header);
            $i=1;

            foreach($records as $data)
            {
                $value['id']=$i; 
                $value['tax_name']=$data['tax_name']; 
                $value['tax_rate']=$data['tax_rate'];
                $i++;
                fputcsv($file,$value);
            }    

            fclose($file);
            exit;       
        }

        public function import_subscription()
        { 
            $user_id=$this->session->userdata['userId'];  
            $this->session->unset_userdata('tab_name');
            $this->session->set_userdata('tab_name', 'subscription_tab');
            $file_info = pathinfo($_FILES["file"]["name"]);
            $file_directory = "uploads/excel";
            $new_file_name = date("d-m-Y ") . rand(000000, 999999) .".". $file_info["extension"];

            function check_for_null($arr)
            {
               if(empty($arr))
               { 
                  return 'Not Valid';
               }

               return $arr;
            }

            function check_for_valid($value)
            {
                if(!is_numeric($value))
                { 
                   return 'Not Valid';
                }
                else
                {
                   if($value <= '0')
                   { 
                      return 'Not Valid';
                   } 
                }

                return $value;
            }

            if(in_array($file_info["extension"], ['xls','xlsx','xlt','xla']))
            {
               if(move_uploaded_file($_FILES["file"]["tmp_name"], $file_directory . $new_file_name))
               {   
                   $file_type  = PHPExcel_IOFactory::identify($file_directory . $new_file_name);
                   $objReader  = PHPExcel_IOFactory::createReader($file_type); 
                   $objPHPExcel = $objReader->load($file_directory . $new_file_name);
                   $sheet_data = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
                   unset($sheet_data[1]);
                   
                   $exist = array();
                   $price_error = array();                   
                   $insuff_error = 0;
                   $unitprice_err = array();
                   $unit_price = array('per_hour','per_day','per_week','per_month','per_2month','per_5month','per_year');

                   foreach($sheet_data as $data)
                   {
                       $array_map = array_map('check_for_null', $data);
                       $check_for_valid = array_map('check_for_valid',[$data['C']]);

                       if(!in_array('Not Valid', $array_map) && in_array($data['D'], $unit_price) && !in_array('Not Valid', $check_for_valid))
                       {
                          $result = $this->db->query("SELECT count(*) as count FROM proposal_subscriptions where subscription_name = '".$data['B']."' and firm_id = '".$_SESSION['firm_id']."' and (subscription_category!='' and subscription_category!='0')")->row_array();

                          if($result['count'] == 0)
                          {
                              $category = $this->db->query("SELECT id,category_name FROM proposal_category where category_name = '".$data['F']."' and category_type = 'subscription' and firm_id ='".$_SESSION['firm_id']."'")->row_array();
                              
                              if(empty($category['id']))
                              {
                                  $value = array('category_name'=>$data['F'],'status'=>'active','user_id'=>$user_id,'category_type'=>'subscription','firm_id' => $_SESSION['firm_id']);
                                  $this->Common_mdl->insert('proposal_category',$value);
                                  $category['id'] = $this->db->insert_id();
                              }

                              if(!empty($category['id']))
                              {
                                   $data = array(
                                       'subscription_name' => $data['B'],
                                       'subscription_price' => $data['C'],
                                       'subscription_unit' => $data['D'],
                                       'subscription_description'=> $data['E'],  
                                       'subscription_category' => (isset($category['category_name']) && $category['category_name'] != '') ? $category['category_name'] : $data['F'],
                                       'subscription_category_id' => $category['id'],
                                       'status' => 'active',
                                       'user_id'=>$user_id,
                                       'firm_id' => $_SESSION['firm_id']);
                                   
                                   $insertId = $this->Common_mdl->insertCSV('proposal_subscriptions',$data);       
                              }                              
                          }
                          else
                          {
                              array_push($exist, $data['B']);
                          }                           
                       }
                       else
                       {
                          if(!in_array($data['D'], $unit_price))
                          {
                              array_push($unitprice_err, $data['B']);
                          }

                          if(in_array('Not Valid', $array_map))
                          {
                              $insuff_error++;
                          }

                          if(in_array('Not Valid', $check_for_valid))
                          {             
                              array_push($price_error, $data['B']);
                          }
                       }   
                   }

                   $existing = implode(',', $exist);
                   $price_err = implode(',', $price_error);                   
                   $unit_err = implode(',', $unitprice_err);
               }

               $success_msg = '';

               if(!empty($insertId) && $insertId != '0')
               {
                  $success_msg = 'Record(s) Added Successfully!';
               }
               else
               {
                  $success_msg = 'Process Failed!';
               }

               if(count($exist) > 0)
               {
                  $success_msg .= ', "'.$existing.'" - Already Exists.';
               }

               if(count($unitprice_err) > 0)
               {
                  $success_msg .= ', "'.$unit_err.'" - Invalid Unitprice.';
               }

               if(count($price_error) > 0)
               {
                  $success_msg .= ', "'.$price_err.'" - Invalid Price.';
               }

               if($insuff_error > '0')
               {
                  $success_msg .= ', '.$insuff_error.' Record(s) Have Insufficient Data.';
               }                          
                 
               $this->session->set_userdata('success', $success_msg);
            }
            else
            {
               $this->session->set_userdata('success', 'Invalid File Format.');
            } 

            redirect('Proposal/fee_schedule');  
        }

        public function import_price_list()
        {
            $user_id=$this->session->userdata['userId'];  
            $this->session->unset_userdata('tab_name');
            $this->session->set_userdata('tab_name', 'price_tab');
            $file_info = pathinfo($_FILES["file"]["name"]);
            $file_directory = "uploads/excel";
            $new_file_name = date("d-m-Y ") . rand(000000, 999999) .".". $file_info["extension"];

            function check_for_null($arr)
            {
               if(empty($arr))
               { 
                  return 'Not Valid';
               }

               return $arr;
            }

            function check_for_valid($value)
            {
                if(!is_numeric($value))
                { 
                   return 'Not Valid';
                }
                else
                {
                   if($value <= '0')
                   { 
                      return 'Not Valid';
                   } 
                }

                return $value;
            }

            if(in_array($file_info["extension"], ['xls','xlsx','xlt','xla']))
            {
                if(move_uploaded_file($_FILES["file"]["tmp_name"], $file_directory . $new_file_name))
                {
                    $file_type  = PHPExcel_IOFactory::identify($file_directory . $new_file_name);
                    $objReader  = PHPExcel_IOFactory::createReader($file_type);
                    $objPHPExcel = $objReader->load($file_directory . $new_file_name);
                    $sheet_data = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
                    unset($sheet_data[1]);
                   
                    $exist = array();                    
                    $insuff_error = 0;
                    $invalid_input = array();
                    $sub_unit_err = array();
                    $sub_unit_price = array('per_hour','per_day','per_week','per_month','per_2month','per_5month','per_year');

                    foreach($sheet_data as $data)
                    {
                        $check_for_null = array_map('check_for_null', $data);
                        $check_for_valid = array_map('check_for_valid',[$data['D'],$data['F'],$data['H'],$data['K'],$data['L'],$data['M'],$data['N']]);

                        if(!in_array('Not Valid', $check_for_null) && in_array($data['I'], $sub_unit_price) && !in_array('Not Valid', $check_for_valid) && ($data['F'] % 1) == '0')
                        {
                           $result = $this->db->query("SELECT count(*) as count FROM proposal_pricelist where pricelist_name = '".$data['B']."' and firm_id = '".$_SESSION['firm_id']."'")->row_array();

                           if($result['count'] == 0)
                           {
                              $data = array('pricelist_name' => $data['B'],'product_name' => $data['C'],'product_price'    => $data['D'],'product_description' => $data['E'], 'product_qty' => $data['F'], 'subscription_name' => $data['G'],   'subscription_price' => $data['H'],  'subscription_unit'  => $data['I'],  'subscription_description' => $data['J'], 'product_tax'  => $data['K'],'product_discount' => $data['L'],    'subscription_tax'  => $data['M'],   'subscription_discount' => $data['N'], 'user_id'=> $user_id,'firm_id' => $_SESSION['firm_id']);

                              $insertId = $this->Common_mdl->insertCSV('proposal_pricelist',$data);
                           }
                           else
                           {
                               array_push($exist, $data['B']);
                           }
                            
                        }
                        else
                        {
                           if(!in_array($data['I'], $sub_unit_price))
                           {
                               array_push($sub_unit_err, $data['B']);
                           }

                           if(in_array('Not Valid', $check_for_null))
                           {
                               $insuff_error++;
                           }

                           if((in_array('Not Valid', $check_for_valid) || ($data['F'] % 1) != '0') && !in_array($data['B'], $invalid_input))
                           {             
                               array_push($invalid_input, $data['B']);
                           }
                        }   

                        $existing = implode(',', $exist);
                        $invalidinput = implode(',', $invalid_input);
                        $subuniterr = implode(',', $sub_unit_err);                    
                    }

                    $success_msg = '';

                    if(!empty($insertId) && $insertId != '0')
                    {
                       $success_msg = 'Record(s) Added Successfully!';
                    }
                    else
                    {
                       $success_msg = 'Process Failed!';
                    }

                    if(count($exist) > 0)
                    {
                       $success_msg .= ', "'.$existing.'" - Already Exists.';
                    }

                    if(count($sub_unit_err) > 0)
                    {
                       $success_msg .= ', "'.$subuniterr.'" - Invalid Unitprice.';
                    }

                    if(count($invalid_input) > 0)
                    {
                       $success_msg .= ', "'.$invalidinput.'" - Has Invalid Input(s).';
                    }

                    if($insuff_error > '0')
                    {
                       $success_msg .= ', '.$insuff_error.' Record(s) Have Insufficient Data.';
                    }               
                     
                    $this->session->set_userdata('success', $success_msg);  
                }                
            }
            else
            {
                $this->session->set_userdata('success', 'Invalid File Format.');
            }     

            redirect('Proposal/fee_schedule');
        }


      public function import_tax_list()
      {
          $user_id=$this->session->userdata['userId'];  
          $this->session->unset_userdata('tab_name');
          $this->session->set_userdata('tab_name', 'tax_tab');
          $file_info = pathinfo($_FILES["file"]["name"]);
          $file_directory = "uploads/excel";
          $new_file_name = date("d-m-Y ") . rand(000000, 999999) .".". $file_info["extension"];

          function check_for_null($arr)
          {
             if(empty($arr))
             { 
                return 'Not Valid';
             }

             return $arr;
          }

          function check_for_valid($value)
          {
              if(!is_numeric($value))
              { 
                 return 'Not Valid';
              }
              else
              {
                 if($value <= '0')
                 { 
                    return 'Not Valid';
                 } 
              }

              return $value;
          }

          if(in_array($file_info["extension"], ['xls','xlsx','xlt','xla']))
          { 
             if(move_uploaded_file($_FILES["file"]["tmp_name"], $file_directory . $new_file_name))
             {   
                 $file_type  = PHPExcel_IOFactory::identify($file_directory . $new_file_name);
                 $objReader  = PHPExcel_IOFactory::createReader($file_type);
                 $objPHPExcel = $objReader->load($file_directory . $new_file_name);
                 $sheet_data = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
                 unset($sheet_data[1]);
                 
                 $exist = array();
                 $insuff_error = 0;
                 $price_error = array();

                 foreach($sheet_data as $data)
                 {
                     $check_for_null = array_map('check_for_null', $data);
                     $check_for_valid = array_map('check_for_valid',[$data['C']]);

                     if(!in_array('Not Valid', $check_for_null) && !in_array('Not Valid', $check_for_valid))
                     {
                        $result = $this->db->query("SELECT count(*) as count FROM proposal_tax where tax_name = '".$data['B']."' and firm_id = '".$_SESSION['firm_id']."'")->row_array();

                        if($result['count'] == 0)
                        {
                            $data = array(
                                'tax_name' => $data['B'],
                                'tax_rate' => $data['C'],
                                'tax_status' => '0',
                                'active_status' => '1',
                                'user_id'=>$user_id,
                                'firm_id' => $_SESSION['firm_id']);
                            $insertId = $this->Common_mdl->insertCSV('proposal_tax',$data);
                        }
                        else
                        {
                            array_push($exist, $data['B']);
                        }                        
                     }
                     else
                     {
                        if(in_array('Not Valid', $check_for_null))
                        {
                            $insuff_error++;
                        }

                        if(in_array('Not Valid', $check_for_valid))
                        {             
                            array_push($price_error, $data['B']);
                        }
                     }
                 }

                 $existing = implode(',', $exist);
                 $price_err = implode(',', $price_error);
             } 

             $success_msg = '';

             if(!empty($insertId) && $insertId != '0')
             {
                $success_msg = 'Record(s) Added Successfully!';
             }
             else
             {
                $success_msg = 'Process Failed!';
             }

             if(count($exist) > 0)
             {
                $success_msg .= ', "'.$existing.'" - Already Exists.';
             }

             if(count($price_error) > 0)
             {
                $success_msg .= ', "'.$price_err.'" - Invalid Tax Rate.';
             }           

             if($insuff_error > '0')
             {
                $success_msg .= ', '.$insuff_error.' Record(s) Have Insufficient Data.';
             }

             $this->session->set_userdata('success', $success_msg);   
          }
          else
          {
             $this->session->set_userdata('success', 'Invalid File Format.');
          } 

          redirect('Proposal/fee_schedule');
      }

        public function search_subscriptionBYid(){
            $id=$_POST['id'];
            $result=$this->Common_mdl->select_record('proposal_subscriptions','id',$id);
            $data['item_name']=$result['subscription_name'];
            $data['item_price']=$result['subscription_price'];          
            $data['item_unit']=$result['subscription_unit'];
            $data['item_description']=$result['subscription_description'];
            $data['subscription_category']=$result['subscription_category'];
            echo json_encode($data);
        }
        public function search_Subscription()
        {
            $service_name=$_POST['service_name'];
            $result=$this->db->query("SELECT * FROM `proposal_subscriptions` where subscription_name like '%".$service_name."%' AND firm_id = '".$_SESSION['firm_id']."'")->result_array();
            $data['contents']='';
            if(!empty($result)){
                foreach($result as $res){
                     $data['contents'].='<div class="annual-lorem" id="'.$res['id'].'" onclick="myFunction2(this.id)"><h3>'.$res['subscription_name'].'</h3> <strong>- &euro;'.$res['subscription_price'].'/'.$res['subscription_unit'].'</strong></div>';
                }
            }else{
                 $data['contents'].='<div class="annual-lorem"><h3>No Results Found</h3></div>';
            }
            echo json_encode($data);  
        }

        public function Googledrive()
        {
           $user_id=$this->session->userdata['userId']; 
           $values=$this->session->userdata['proposal_id']; 
           $url_array = explode('?', 'https://'.$_SERVER ['HTTP_HOST'].$_SERVER['REQUEST_URI']); 
           $data['url'] = $url_array[0]; 
           require_once(APPPATH.'libraries/google-api-php-client/src/Google_Client.php');
           require_once(APPPATH.'libraries/google-api-php-client/src/contrib/Google_DriveService.php');

            $client = new Google_Client();
            $client->setClientId('541372697628-eip7bl75evddhimce459nc4jqfsl1gkb.apps.googleusercontent.com');
            $client->setClientSecret('Et5ZgxtGC793IsQtRjGXq8a0');
            $client->setRedirectUri(base_url().'proposal_page/Googledrive');
            $client->setScopes(array('https://www.googleapis.com/auth/drive'));
           
            if(isset($_GET['code'])) 
            { 
                $_SESSION['accessToken'] = $client->authenticate($_GET['code']);
                header('location:'.$data['url']);exit;
            }
            elseif (!isset($_SESSION['accessToken'])) 
            { 
                $client->authenticate();
            }

            $record=$this->db->query('select pdf_file from proposals where id="'.$values.'"')->row_array();
            $files_name=$record['pdf_file'];
            $files= array();
            $dir = dir('uploads/proposal_pdf');
            while ($file = $dir->read()) 
            {
                if ($file != '.' && $file != '..' && $file == $files_name) 
                {
                    $files[] = $file;               
                }
                $data['files']=$files;
            }
            $dir->close();  
            
            if(!empty($_POST['googledrive'])) 
            { 
                $this->session->set_flashdata('close_status', 'google_drive');
                $client->setAccessToken($_SESSION['accessToken']);
                $service = new Google_DriveService($client);
                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                $file = new Google_DriveFile();
                foreach ($files as $file_name) 
                {
                    $file_path = 'uploads/proposal_pdf/'.$file_name;
                    $mime_type = finfo_file($finfo, $file_path);
                    $file->setTitle($file_name);
                    $file->setDescription('This is a '.$mime_type.' document');
                    $file->setMimeType($mime_type);
                    $is_upload_data = $service->files->insert($file,array('data' => file_get_contents($file_path),'mimeType' => $mime_type));
                }         
                finfo_close($finfo); 
                header('location:'.$data['url']);exit;                
            }

            if(isset($_SESSION['popup']))
            {                 
                $this->session->unset_userdata('popup');
            }
            else
            {                   
                $this->session->set_flashdata('popup', 'google_drive');
            }            
             $values=$this->session->userdata['proposal_id']; 
               $data['taxes']=$this->Common_mdl->GetAllWithWhere('proposal_tax','active_status','1');
             $data['records']=$this->Common_mdl->select_record('proposals','id',$values);
             $company_name=  $data['records']['company_name'];
             $data['company_details']=$this->Common_mdl->select_record('client','crm_company_name',$company_name);
             $data['conversation']=$this->Common_mdl->GetAllWithWhere('proposal_discussion','proposal_id', $values);
            $data['proposal_history']=$this->db->query("SELECT * FROM `proposal_history` where proposal_id='".$values."' group by tag,DATE_FORMAT(`created_at`, '%Y-%m-%d %H:%i') order by id desc")->result_array();

            $followup=$this->Common_mdl->getTemplates(6);              
            $data['followup']=$followup[0];

            if(isset($_SESSION['close_status']))
            {
                 $random_string=$this->generateRandomString();
                 redirect('Proposal_page/step_proposal/'.$random_string.'---'.$values.'');
            }
            else
            {                   
                $this->load->view('proposal/end_proposal_view',$data);
            }           
        }

        public function generateRandomString($length = 100) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }

        public function copy_proposal($id)
        {
            $this->Security_model->chk_login();

            $Assigned_Client = Get_Assigned_Datas('CLIENT'); 
            $assigned_client = implode(',',$Assigned_Client);

            $Assigned_Leads = Get_Assigned_Datas('LEADS');
            $assigned_leads = implode(',',$Assigned_Leads);

            $data['clients'] = $this->db->query("SELECT * FROM client WHERE autosave_status = 0 and firm_id = '".$_SESSION['firm_id']."' AND FIND_IN_SET(id,'".$assigned_client."') AND crm_company_name != '' order by id desc")->result_array();

            $value = explode( "---", $id );            
            $proposal_id = end($value);      

            $proposal_templates = $this->Common_mdl->getproposal_templates();

            $data['my_template'] =  $proposal_templates['firm'];
            $data['all'] =  $proposal_templates['all'];
            //$data['template_title'] = array_merge($proposal_templates['firm'],$proposal_templates['all']);          

            //$proposal_number=$this->db->query("SELECT max(id) as id FROM proposals")->row_array();
                      
         /*   if(empty($proposal_number))
            {
               $data['proposal_number']='0001';
            }
            else
            {*/
               $data['proposal_number'] = $proposal_id;
            /*}*/
            // echo "<pre>"; print_r($data['proposal_number']); exit;
            // echo "<pre>"; print_r($_SESSION); exit;
            $data['taxes'] = $this->Common_mdl->GetAllWithWheretwo('proposal_tax','active_status','1','firm_id',$_SESSION['firm_id']);
            $tax = $this->Common_mdl->GetAllWithWheretwo('proposal_tax','tax_status','1','firm_id',$_SESSION['firm_id']);
            $data['tax_details'] = $tax[0];
            $data['category']=$this->Common_mdl->GetAllWithWhere('proposal_category','firm_id',$_SESSION['firm_id']);

            $category = $data['category'];
            $product_category=array();
            $service_category=array();
            $subscription_category=array();

            foreach($category as $cat)
            {
                if($cat['category_type']=='product'){
                     array_push($product_category,"'".$cat['category_name']."'");
                }

                 if($cat['category_type']=='service'){
                     array_push($service_category,"'".$cat['category_name']."'");
                }

                 if($cat['category_type']=='subscription'){
                     array_push($subscription_category,"'".$cat['category_name']."'");
                }
            }

             $subscription_category=implode(',',$subscription_category);
             $product_category=implode(',',$product_category);
             $service_category=implode(',',$service_category);

             $count1=explode(',',$subscription_category);
             $count2=explode(',',$product_category);
             $count3=explode(',',$service_category);

             $firm_services = $this->Common_mdl->getServicelist();
             $firm_enabled_services = array();        

             function firm_enabled($val)
             {          
                return $val['service_name'];
             }

             $firm_enabled_services = array_map('firm_enabled',$firm_services); 
             $firm_enabled_services = implode("','", $firm_enabled_services);

             if($count1['0']!=''){
               $data['subscription']=$this->db->query("select * from proposal_subscriptions where firm_id = '".$_SESSION['firm_id']."' and  subscription_category in (".$subscription_category.")")->result_array();
            }else{
               $data['subscription']=$this->db->query("select * from proposal_subscriptions where firm_id = '".$_SESSION['firm_id']."' and  ( subscription_category!='' and subscription_category!='0')")->result_array();
            }


            if($count3['0']!=''){
                $data['services']=$this->db->query("select * from proposal_service where firm_id = '".$_SESSION['firm_id']."' and  service_category in (".$service_category.")")->result_array();

            }else{
                $data['services']=$this->db->query("select * from proposal_service where firm_id = '".$_SESSION['firm_id']."' and ( service_category!='' and service_category!='0') ")->result_array();
            }


            if($count2['0']!=''){
                $data['products']=$this->db->query("select * from proposal_products where firm_id = '".$_SESSION['firm_id']."' and  product_category in (".$product_category.")")->result_array();

            }else{
               $data['products']=$this->db->query("select * from proposal_products where firm_id = '".$_SESSION['firm_id']."' and ( product_category!='')")->result_array();
            }

            $data['pricelist']=$this->Common_mdl->GetAllWithWhere('proposal_pricelist','firm_id',$_SESSION['firm_id']);
            $data['records']=$this->Common_mdl->select_record('proposals','id',$proposal_id);

            //print_r($data['records']);exit;
            $data['currency']=$this->Common_mdl->getallrecords('currency');
            $data['documents']=$this->Common_mdl->getallrecords('document_record');
            $data['admin_settings']=$this->Common_mdl->select_record('admin_setting','firm_id',$_SESSION['firm_id']);
            $data['leads_status']=$this->Common_mdl->getallrecords('leads_status');
            $data['source']=$this->Common_mdl->getallrecords('source'); 
            
            $leads_status = $this->db->query("SELECT id FROM `leads_status` where status_name = 'Win' AND firm_id = '0'")->row_array();

            $data['leads'] = $this->db->query("SELECT * FROM leads WHERE firm_id = '".$_SESSION['firm_id']."' AND FIND_IN_SET(id,'".$assigned_leads."') AND lead_status != '".$leads_status['id']."' AND lead_status!=''")->result_array();

            $proposal_actions = $this->Common_mdl->GetAllWithWhere('email_templates_actions','module','proposal');
            $proposal_actions = array_column($proposal_actions, 'action_id'); 
            $proposal_actions = implode(',', $proposal_actions);

            $data['proposal_templates'] = $this->Common_mdl->getTemplates('',$proposal_actions);
            // echo "<pre>"; print_r($data['proposal_templates']); exit;
            $this->load->view('proposal/step_proposal_view',$data);

        }

        public function lead_proposal($id=false){            
            $this->Security_model->chk_login();
            $user_id=$this->session->userdata['userId'];  
            $data['company_name']=$this->db->query("SELECT `crm_company_name` FROM `client` where `crm_company_name` <> '' group By `crm_company_name`")->result_array();
            $data['template_title']=$this->db->query('select * from proposal_template where status="active" and user_id="0" or user_id='.$user_id.'')->result_array();            
            $proposal_number=$this->db->query("SELECT max(id) as id FROM proposals")->row_array();
            //print_r($proposal_number);
            if(empty($proposal_number)){
            $data['proposal_number']='0001';
            }else{
            $data['proposal_number']= $proposal_number['id'] + 1;
            }
              $data['taxes']=$this->Common_mdl->GetAllWithWhere('proposal_tax','active_status','1');
            $data['tax_details']=$this->Common_mdl->select_record('proposal_tax','tax_status','1');
            $data['category']=$this->Common_mdl->GetAllWithWhere('proposal_category','user_id',$user_id);
              $category=$this->Common_mdl->GetAllWithWhere('proposal_category','user_id',$user_id);
            $product_category=array();
             $service_category=array();
              $subscription_category=array();
            foreach($category as $cat){
            if($cat['category_type']=='product'){
                 array_push($product_category,"'".$cat['category_name']."'");
            }

             if($cat['category_type']=='service'){
                 array_push($service_category,"'".$cat['category_name']."'");
            }

             if($cat['category_type']=='subscription'){
                 array_push($subscription_category,"'".$cat['category_name']."'");
            }



            }

             $subscription_category=implode(',',$subscription_category);
             $product_category=implode(',',$product_category);
             $service_category=implode(',',$service_category);
              $count1=explode(',',$subscription_category);
             $count2=explode(',',$product_category);
             $count3=explode(',',$service_category);


            if($count1['0']!=''){
                  $data['subscription']=$this->db->query("select * from proposal_subscriptions where user_id='".$user_id."' and  subscription_category in (".$subscription_category.")")->result_array();


             }else{
                $data['subscription']=$this->db->query("select * from proposal_subscriptions where user_id='".$user_id."' and  ( subscription_category!='' and subscription_category!='0')")->result_array();
             }


             if($count3['0']!=''){
                 $data['services']=$this->db->query("select * from proposal_service where user_id='".$user_id."' and  service_category in (".$service_category.")")->result_array();

             }else{
                 $data['services']=$this->db->query("select * from proposal_service where user_id='".$user_id."' and ( service_category!='' and service_category!='0') ")->result_array();
             }


             if($count2['0']!=''){
                 $data['products']=$this->db->query("select * from proposal_products where user_id='".$user_id."' and  product_category in (".$product_category.")")->result_array();

             }else{
                $data['products']=$this->db->query("select * from proposal_products where user_id='".$user_id."' and ( product_category!='')")->result_array();
             }

            // $data['products']=$this->Common_mdl->GetAllWithWhere('proposal_products','user_id',$user_id);
            // $data['services']=$this->Common_mdl->GetAllWithWhere('proposal_service','user_id',$user_id);
            // $data['subscription']=$this->Common_mdl->GetAllWithWhere('proposal_subscriptions','user_id',$user_id);
            $data['pricelist']=$this->Common_mdl->GetAllWithWhere('proposal_pricelist','user_id',$user_id);
            $data['countries']=$this->Common_mdl->getallrecords('countries');
            $data['currency']=$this->Common_mdl->getallrecords('currency');

            $it_session_id='';
            $get_this_qry=$this->db->query("select * from user where id=".$_SESSION['id'])->row_array();
            $created_id=$get_this_qry['firm_admin_id'];
            $its_firm_admin_id=$get_this_qry['firm_admin_id'];
            if($_SESSION['role']==6) // staff
            {
            $it_session_id=$_SESSION['id'];
            }
            else if($_SESSION['role']==5) //manager
            {
            $it_session_id=$created_id;
            }
            else if($_SESSION['role']==4)//client
            {
            $it_session_id=$created_id;
            }
            else if($_SESSION['role']==3) //director
            {
            $it_session_id=$created_id;
            }
            else if($_SESSION['role']==2) // sub admin
            {
            $it_session_id=$created_id;
            }
            else
            {
            $it_session_id=$_SESSION['id'];
            }



            /** end of permission 16-07-2018 **/
            if($_SESSION['role']==6)
            {
            $data['leads']=$this->db->query("SELECT * FROM leads li where li.user_id=".$it_session_id." 
            UNION
            SELECT * FROM leads lis where lis.user_id!=".$it_session_id." and public='on'
            UNION
            SELECT * FROM leads tls where tls.user_id!=".$it_session_id." and (FIND_IN_SET('".$it_session_id."',assigned) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `dept`, ',') REGEXP ',($res_dept_num),') ) 
            ")->result_array();
            }
            else if($_SESSION['role']==4)
            {
            $result=array();
            array_push($result, $it_session_id);
            $reassign_firm_id=$its_firm_admin_id;
            $tot_val=1;
            for($z=0;$z<$tot_val;$z++)
            {
            if($reassign_firm_id==0)
            {
            $tot_val=0;
            }
            else
            {
            $get_this_qry=$this->db->query("select * from user where id=".$reassign_firm_id)->row_array();
            if(!empty($get_this_qry)){
            array_push($result, $get_this_qry['id']);
            $new_created_id=$get_this_qry['firm_admin_id'];
            $reassign_firm_id=$get_this_qry['firm_admin_id'];
            }
            else
            {
            $tot_val=0;
            }

            }

            }
            $res=(!empty($result))?implode(',',array_filter(array_unique($result))):'';
            $sql=$this->db->query("select * from client where user_id=".$_SESSION['id']." ")->row_array();
            $its_id=$sql['id'];
            $data['leads']=$this->db->query("SELECT * FROM leads li where li.company=".$its_id." UNION
            SELECT * FROM leads lis where lis.user_id in($res) and public='on'")->result_array();

            }
            else if($_SESSION['role']==5 || $_SESSION['role']==3 || $_SESSION['role']==2)
            {
            $result=array();
            $reassign_firm_id=$its_firm_admin_id;
            // if($its_firm_admin_id==0)
            // {
            array_push($result, $it_session_id);
            // }
            // else
            // {
            $tot_val=1;
            for($z=0;$z<$tot_val;$z++)
            {
            if($reassign_firm_id==0)
            {
            $tot_val=0;
            }
            else
            {
            $get_this_qry=$this->db->query("select * from user where id=".$reassign_firm_id)->row_array();
            if(!empty($get_this_qry)){
            array_push($result, $get_this_qry['id']);
            $new_created_id=$get_this_qry['firm_admin_id'];
            $reassign_firm_id=$get_this_qry['firm_admin_id'];
            }
            else
            {
            $tot_val=0;
            }

            }

            }
            //}
            $res=(!empty($result))?implode(',',array_filter(array_unique($result))):'';
            // $res=implode(',', $over_all_res);
            $data['leads']=$this->db->query("SELECT * FROM leads li where li.user_id in ($res) 
            UNION
            SELECT * FROM leads lis where lis.user_id in($res) and public='on' ")->result_array();
            }
            else
            {
            $result=array();
            array_push($result, $it_session_id);
            $reassign_firm_id=$its_firm_admin_id;
            $tot_val=1;
            for($z=0;$z<$tot_val;$z++)
            {
            if($reassign_firm_id==0)
            {
            $tot_val=0;
            }
            else
            {
            $get_this_qry=$this->db->query("select * from user where id=".$reassign_firm_id)->row_array();
            if(!empty($get_this_qry)){
            array_push($result, $get_this_qry['id']);
            $new_created_id=$get_this_qry['firm_admin_id'];
            $reassign_firm_id=$get_this_qry['firm_admin_id'];
            }
            else
            {
            $tot_val=0;
            }

            }

            }
            $res=(!empty($result))?implode(',',array_filter(array_unique($result))):'';
              $leads_status=$this->db->query("SELECT id FROM `leads_status` where status_name='customer'")->row_array();


        //     $queries="SELECT * FROM leads li where li.user_id in ($res) 
        // UNION
        // SELECT * FROM leads lis where lis.user_id in($res) and public='on' and (lead_status!='".$leads_status['id']."' and lead_status!='') ";


        // echo $queries;
            // $res=implode(',', $over_all_res);
           
        // $res=implode(',', $over_all_res);
        $data['leads']=$this->db->query("SELECT * FROM leads li where li.user_id in ($res) and (li.lead_status!='".$leads_status['id']."' and li.lead_status!='')
        UNION
        SELECT * FROM leads lis where lis.user_id in($res) and public='on'  ")->result_array();
            // $data['leads']=$this->db->query("SELECT * FROM leads li where li.user_id=".$it_session_id." ")->result_array();
            }


        $data['leads_status']=$this->Common_mdl->getallrecords('leads_status');
        $data['source']=$this->Common_mdl->getallrecords('source');
             $data['admin_settings']=$this->Common_mdl->select_record('admin_setting','user_id',$user_id);

            if($id!=false){              
                 $value=explode( "---", $id );            
                 $values=end($value); 
                 $this->session->set_userdata('proposal_id', $values);
                 $data['records']=$this->Common_mdl->select_record('proposals','id',$values);
                 $company_name=  $data['records']['company_name'];
                 $data['company_details']=$this->Common_mdl->select_record('client','crm_company_name',$company_name);
                 $data['conversation']=$this->Common_mdl->GetAllWithWhere('proposal_discussion','proposal_id', $values);
               //  $data['proposal_history']=$this->Common_mdl->GetAllWithWhere('proposal_history','proposal_id', $values);


                  $data['proposal_history']=$this->db->query("SELECT * FROM `proposal_history` where proposal_id='".$values."' group by tag,DATE_FORMAT(`created_at`, '%Y-%m-%d %H:%i') order by id desc")->result_array();
                 
                 $followup = $this->Common_mdl->getTemplates(6);
                 $data['followup']=$followup[0]; 
                 $data['internal_notes']=$this->Common_mdl->GetAllWithWhere('proposal_internalnotes','proposal_id',$values); 

                 $this->load->view('proposal/end_proposal_view',$data);
            }else{            
           
            $this->load->view('proposal/step_proposal_view',$data);
            }        
        }
}
