<?php 
$this->load->view('includes/header');
$uri_seg = $this->uri->segment(3);
?>
<link href="<?php echo base_url();?>assets/css/jquery.filer.css" type="text/css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/timepicki.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/tree_select/style.css">
<style>
    
</style>
<style type="text/css">
   .disabled{
   opacity: 0.5;
   pointer-events: none;    
   }
/** 28-08-2018 for hide edit option on company house basic detailes**/
   .edit-button-confim
   {
       display: none;
   }
/** end of 28-08-2018 **/

   li.show-block
   {
      display: none;
      float: left;
      width: 100%;
   }
   .inner-views
   {
      display: block;
   }
   .make_primary_section.active
   {
      background: #4dad2f;
   }
</style>
<div class="modal-alertsuccess  alert alert-success-reminder succs" style="display: none;">
  <div class="newupdate_alert">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <div class="pop-realted1"> 
      <div class="position-alert1 message">Reminder Added Successfully.</div>
  </div>
</div>
</div>

<div class="modal-alertsuccess alert alert-success" style="display:none;">
<div class="newupdate_alert"> <a href="javascript:;" class="close alert_close"  aria-label="close">x</a>
 <div class="pop-realted1">
    <div class="position-alert1 sample_check">
       YOU HAVE SUCCESSFULLY UPDATED CLIENT 
       <div class="alertclsnew">
          <a href="<?php echo base_url(); ?>user"> GO BACK</a>
       </div>
    </div>
 </div>
</div>
</div>

<div class="succs"></div>
<!--  <div class="modal-alertsuccess  alert alert-success succs" style="display:none;">
   <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><div class="pop-realted1">
   <div class="position-alert1">
     Your client was updated successfully.
     </div></div></div>
     
    <div class="modal-alertsuccess  alert alert-danger" style="display:none;">
   <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   <div class="pop-realted1">
   <div class="position-alert1">
   Please fill the required fields.
   </div></div></div> -->
<section class="client-details-view hidden-user01 floating_set card-removes pcoded-content ">
   <!-- <h2><i class="fa fa-users fa-6" aria-hidden="true"></i> Client's Information</h2> -->
   <!-- <div class="text-right select-box-option1">
      <div class="form-group">
      <label>Client</label> <select><option>Accotax london ltd</option></select></div>
      </div>   -->
   <form id="insert_form" class="required-save client-firm-info1 validation" method="post" action="" enctype="multipart/form-data">
      <div class="information-tab main-body clientredesign floating_set">
         <div class="space-required">
            <div class="deadline-crm1 floating_set">
              <!--  <ul class="nav nav-tabs1 all_user1 md-tabs pull-left u-dashboard">
                  <li class="nav-item">
                     <a class="nav-link active" href="<?php echo base_url().'user';?>"><img class="themicon" src="<?php echo base_url();?>assets/images/tabicon1.png" alt="themeicon" /> All Clients</a>
                     <div class="slide"></div>
                  </li> -->
                  <!--  <li class="nav-item">
                     <a class="nav-link" href="<?php echo base_url().'client/addnewclient';?>">Add new client</a>
                     <div class="slide"></div>
                     </li> -->
                  <!-- <li class="nav-item">
                     <a class="nav-link" href="<?php echo base_url().'user';?>">Email all</a>
                     <div class="slide"></div>
                     </li>
                     <li class="nav-item">
                     <a class="nav-link" href="<?php echo base_url().'user';?>">Give feedback</a>
                     <div class="slide"></div>
                     </li> -->
               <!-- </ul> -->

                 <ul class="nav nav-tabs1 all_user1 md-tabs pull-left u-dashboard">
                                       <li class="nav-item">
                                          <a class="nav-link" href="<?php echo base_url().'user';?>">
                                          <img class="themicon" src="<?php echo base_url();?>assets/images/tabicon1.png" alt="themeicon" />
                                       All Clients</a>
                                          <div class="slide"></div>
                                       </li>
                                       <li class="nav-item">
                                          <a class="nav-link active" href="#addnewclient">
                                             <img class="themicon" src="<?php echo base_url();?>assets/images/tabicon2.png" alt="themeicon" />Edit Client</a>
                                          <div class="slide"></div>
                                       </li> 
                                       <!-- <li class="nav-item">
                                          <a class="nav-link" href="<?php echo base_url().'user';?>">Email all</a>
                                          <div class="slide"></div>
                                          </li>
                                          <li class="nav-item">
                                          <a class="nav-link" href="<?php echo base_url().'user';?>">Give feedback</a>
                                          <div class="slide"></div>
                                          </li> -->
                                    </ul>
               <div class="Footer common-clienttab pull-right">

               <!-- <?php if($_SESSION['permission']['Client_Section']['edit']=='1'){ ?> 
              <?php } ?> -->
                     <div class="change-client-bts1">
                        <input type="submit" class="signed-change1 sv_ex" id="save_exit" value ="Save & Exit">
                     </div>
                     <div class="divleft">
                        <button  class="signed-change2"  type="button" value="Previous Tab" text="Previous Tab" >Previous 
                        </button>
                     </div>
                     <div class="divright">
                        <button  class="signed-change3"  type="button" value="Next Tab"  text="Next Tab">Next
                        </button>
                     </div>
                  </div>
            </div>
         </div>
         <div class="space-required">
            <div class="document-center client-infom-1 floating_set">
               <div class="Companies_House floating_set">
                  <div class="pull-left">
                  <?php 
                        /*$cmy_role = $client['status'];
                        //echo $cmy_role;
                        if($cmy_role == 0) {
                          $cmy_status = 'Manual'; 
                        } else if($cmy_role == 1) {
                          $cmy_status = 'Company House';
                        }else if($cmy_role==2)
                        {
                          $cmy_status = 'Import';
                        }
                         else if($cmy_role==3){
                          $cmy_status = 'From Lead';
                        }*/ 
                      if(isset($client['crm_company_name']) && $client['crm_company_name']!='')
                      { 
                        $client_name=$client['crm_company_name'];

                      }
                      else
                      {
                       $client_name=$this->Common_mdl->get_crm_name($client['user_id']);
                      }
                      echo '<h2>'.$client_name.'</h2>';  ?>    
                  </div>
                  
               </div>
               <div class="addnewclient_pages1 floating_set bg_new1">
                  <ol class="nav nav-tabs all_user1 md-tabs floating_set" id="tabs">
                    <!--  <li class="nav-item it0" value="0"><a class="nav-link active" data-toggle="tab" href="#required_information">Required information</a></li> -->                                            
                      <li class="nav-item it0" value="0"><a class="nav-link
                                             active" data-toggle="tab" href="#required_information">
                                             Required information</a>
                      </li>
                     <li class="nav-item it0 bbs basic_details_tab " value="0" ><a class="nav-link" data-toggle="tab" 
                        href="#details">Basic Details</a></li>
                     <!-- <li class="nav-item it1" value="1"><a class="nav-link" data-toggle="tab" href="#contacts">Contacts</a></li> --> 
                     <li class="nav-item it1" value="1"><a class="nav-link" data-toggle="tab" href="#contacts1">Contacts</a></li>
                     <li class="nav-item it2 hide" value="2"><a class="nav-link" data-toggle="tab" href="#service">Service</a></li>
                     <li class="nav-item it2 hide" value="2"><a class="nav-link" data-toggle="tab" href="#assignto">Assign to</a></li>
                     <li class="nav-item it2 hide" value="2"><a class="nav-link" data-toggle="tab" href="#amlchecks">AML Checks</a></li>

                  <?php /** $legalForm = $client['crm_legal_form']; 
                        if(($legalForm == 'Private Limited company') || ($legalForm == 'Public Limited company') || ($legalForm == 'Limited Liability Partnership')) { ?>

                           <li class="nav-item it2 hide filing-alcat-tab" value="2"><a class="nav-link" data-toggle="tab" href="#filing-allocation"> Filing Allocation</a></li>          
                  <?php } if(($legalForm == 'Partnership') || ($legalForm == 'Self Assessment')) { ?> 
                        <li class="nav-item it2 hide business-info-tab" value="2"><a class="nav-link" data-toggle="tab" href="#business-det"> Business Details</a></li>

                  <?php  } **/ ?>   

                  <!-- 30-06-2018 -->
                  <!-- client says hide on 16-08-2018 -->
<!--      <li class="nav-item it2 hide filing-alcat-tab" value="2" style="display: none;"><a class="nav-link" data-toggle="tab" href="#filing-allocation"> Filing Allocation</a></li>    -->
                  <!-- end of 16-08-2018 -->
                    <li class="nav-item it2 hide business-info-tab" value="2" style="display: none;"><a class="nav-link" data-toggle="tab" href="#business-det"> Business Details</a></li>
                  <!-- 30-06-208 end -->
                     <li class="nav-item it2 hide" value="2"><a class="nav-link" data-toggle="tab" href="#other">Other</a></li>
                      <!-- for 30-08-2018 -->
                     <li class="nav-item it2 hide" value="2"><a class="nav-link" data-toggle="tab" href="#referral">Referral</a></li>
                      <!-- end of 30-08-2018 -->

                     <?php 

                     $tab_column_name = array_fill_keys( ['conf_statement','accounts','company_tax_return','personal_tax_return','payroll','workplace','vat','cis','cissub','p11d','bookkeep','management','investgate','registered','taxadvice','taxinvest'], 0 );

                      $tab_on = array_intersect_key( $client , $tab_column_name );

                      function jsonToArray($v)
                      {
                        $return  = ["tab"=>0,"reminder"=>0,"text"=>0,"invoice"=>0];
                         
                        if( !empty( json_decode( $v  , true ) ) )
                        {
                          $return  = array_replace($return, json_decode( $v  , true ) );
                        }
                        return $return;
                      }
                      $tab_on = array_map('jsonToArray', $tab_on);
                       
                        $import_tab_on = ( in_array("on", array_column($tab_on,'tab'),TRUE ) ) ? "display:block" :   "display:none" ;
                        
        
                     
                        $tax = ( $tab_on['company_tax_return']['tab']!==0 ) ?  "checked='checked'" : "";   
                        $taxtab = ( $tab_on['company_tax_return']['tab']!==0 ) ? "" :"display:none;";

                        $taxre = ( $tab_on['company_tax_return']['reminder']!==0 ) ?  "checked='checked'" : "";
                        $taxrecom = ( $tab_on['company_tax_return']['reminder']!==0 ) ? "" :"display:none;";

                        $taxtext = ( $tab_on['company_tax_return']['text']!==0 ) ? "checked='checked'" : "";
                        $threet = ( $tab_on['company_tax_return']['text']!==0 ) ? "" :"display:none;";

                        $taxinvoice = ( $tab_on['company_tax_return']['invoice']!==0 ) ? "checked='checked'" : "";
                        $threet_inv = ( $tab_on['company_tax_return']['invoice']!==0 ) ? "" :"display:none;";
                       
                                       

                        ?>
                     
                     <li class="nav-item" id="import_tab_li" style="<?php echo $import_tab_on; ?> "><a class="nav-link" data-toggle="tab" href="#import_tab">Important Information</a></li>

                  </ol>
               </div>
               <!-- tab close --> 
            </div>
         </div>
         <?php
            $rel_id=( isset($client) ? $client['user_id'] : false);       
            ?>
			<!-- <?php if($_SESSION['permission']['Client_Section']['edit']!='1'){?> permission_deined <?php } ?> --> 
			<div class="newupdate_design">    
         <div class="management_section client-details-tab01 data-mang-06  floating_set realign newmanage-animation">
            <div class="tab-content card">
               <div id="detailss" class="tab-pane fade">
                  <div class="search-client-details01 floating_set">
                     <div class="pull-left detail-pull-left">
                        <span>Details</span>
                     </div>
                     <div class="pull-right edit-tag1"> 
                        <a href="#"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                     </div>
                  </div>
                  <div id="accordion" role="tablist" aria-multiselectable="true">
                     <div class="accordion-panel">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Basic Info</a>
                              </h3>
                           </div>
                           <div id="collapseOne" class="panel-collapse collapse in show" role="tabpanel" aria-labelledby="headingOne">
                              <div class="basic-info-client1">
                                 <table class="client-detail-table01">
                                    <tr>
                                       <th>Client ID</th>
                                       <td><?php if(isset($user['client_id'])){ echo $user['client_id']; } ?></td>
                                       <th>Account Office Reference</th>
                                       <td><?php if(isset($client['crm_accounts_office_reference'])){ echo $client['crm_accounts_office_reference']; } ?></td>
                                    </tr>
                                    <tr>
                                       <th>Client Type</th>
                                       <td><?php if(isset($client['crm_company_type'])){ echo $client['crm_company_type']; } ?></td>
                                       <th>PAYE Reference Number</th>
                                       <td><?php if(isset($client['crm_employers_reference'])){ echo $client['crm_employers_reference']; } ?></td>
                                    </tr>
                                    <tr>
                                       <th>Company Name</th>
                                       <td><?php if(isset($client['crm_company_name'])){ echo $client['crm_company_name']; } ?></td>
                                    </tr>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- accordion1 -->
                     <div class="accordion-panel">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingTwo">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseOne">Address</a>
                              </h3>
                           </div>
                           <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                              <div class="basic-info-client1">
                                 <table class="client-detail-table01">
                                    <tr>
                                       <th>Address Line 1</th>
                                       <td><?php if(isset($company['registered_office_address']['address_line_1'])){ echo $company['registered_office_address']['address_line_1']; }  ?></td>
                                       <th>Post Code</th>
                                       <td><?php if(isset($company['registered_office_address']['postal_code'])){ echo $company['registered_office_address']['postal_code']; }  ?></td>
                                    </tr>
                                    <tr>
                                       <th>Address Line 2</th>
                                       <td><?php if(isset($company['registered_office_address']['address_line_2'])){ echo $company['registered_office_address']['address_line_2']; }  ?></td>
                                       <th>country</th>
                                       <td><?php if(isset($company['registered_office_address']['country'])){ echo $company['registered_office_address']['country']; }  ?></td>
                                    </tr>
                                    <tr>
                                       <th>Town/City</th>
                                       <td><?php if(isset($company['registered_office_address']['locality'])){ echo $company['registered_office_address']['locality']; }  ?></td>
                                       <th>Region</th>
                                       <td><?php if(isset($company['registered_office_address']['region'])){ echo $company['registered_office_address']['region']; }  ?></td>
                                    </tr>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- accordion1 -->
                     <div class="accordion-panel">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Contact Info</a>
                              </h3>
                           </div>
                           <div id="collapseOne" class="panel-collapse collapse in show" role="tabpanel" aria-labelledby="headingOne">
                              <div class="basic-info-client1">
                                 <table class="client-detail-table01">
                                    <tr>
                                       <th>Phone</th>
                                       <td><?php if(isset($client['crm_telephone_number'])){ echo $client['crm_telephone_number']; } ?></td>
                                       <th>Website</th>
                                       <td><?php if(isset($client['crm_website'])){ echo $client['crm_website']; } ?></td>
                                    </tr>
                                    <tr>
                                       <th>Email</th>
                                       <td><?php if(isset($client['crm_email'])){ echo $client['crm_email']; } ?></td>
                                    </tr>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- accordion1 -->
                     <div class="accordion-panel">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Business Info</a>
                              </h3>
                           </div>
                           <div id="collapseOne" class="panel-collapse collapse in show" role="tabpanel" aria-labelledby="headingOne">
                              <div class="basic-info-client1">
                                 <table class="client-detail-table01">
                                    <tr>
                                       <th>Business Start Date</th>
                                       <td><?php if(isset($company['date_of_creation'])){ echo $company['date_of_creation']; } ?></td>
                                       <th>VAT Scheme</th>
                                       <td></td>
                                    </tr>
                                    <tr>
                                       <th>Book Start Date</th>
                                       <td></td>
                                       <th>VAT Submit Type</th>
                                       <td></td>
                                    </tr>
                                    <tr>
                                       <th>Year End Date</th>
                                       <td><?php if(isset($company['accounts']['accounting_reference_date'])){ echo $company['accounts']['accounting_reference_date']['day'].'/'.$company['accounts']['accounting_reference_date']['month']; } ?></td>
                                       <th>VAT Reg.No</th>
                                       <td><?php if(isset($client['vat_number'])){ echo $client['vat_number']; } ?></td>
                                    </tr>
                                    <tr>
                                       <th>Company Reg.No</th>
                                       <td><?php if(isset($company['company_number'])){ echo $company['company_number']; } ?></td>
                                       <th>VAT Reg.Date</th>
                                       <td><?php if(isset($client['crm_vat_date_of_registration'])){ echo $client['crm_vat_date_of_registration']; } ?></td>
                                    </tr>
                                    <tr>
                                       <th>UTR No</th>
                                       <td><?php if(isset($client['crm_company_utr'])){ echo $client['crm_company_utr']; } ?></td>
                                       <th>Company Status</th>
                                       <td><?php if(isset($company['company_status'])){ echo $company['company_status']; } ?></td>
                                    </tr>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- accordion1 -->
                  </div>
               </div>
               <!-- required information tab open-->
               <div id="required_information" class="columns-two tab-pane fade in active">
                  <div class="accordion-panel">
                        <!-- <div class="accordion-heading" role="tab" id="headingOne">
                           <h3 class="card-title accordion-title">
                              <a class="accordion-msg">Required Information</a>
                           </h3>
                        </div> -->
                        <div class="space-required">
                            <div class="main-pane-border1 cmn-errors1" id="required_information_error">
                            </div>
                             <div class="alert-ss"></div>
                        </div>
                        <div id="collapse" class="panel-collapse">
                           <div class="basic-info-client1">
                              <div class="form-group row name_fields">
                                 <label class="col-sm-4 col-form-label">Name</label>
                                 <div class="col-sm-8">
                                    <input type="text" name="company_name" id="company_name" placeholder="" value="<?php if(isset($client['crm_company_name']) && ($client['crm_company_name']!='') ){ echo $client['crm_company_name'];}?>" class="fields">
                                 </div>
                              </div>
                              <div class="form-group row name_fields">
                                 <label class="col-sm-4 col-form-label">Legal Form</label>
                                 <div class="col-sm-8">
                                    <select name="legal_form" class="form-control fields sel-legal-form" id="legal_form">
                                    <!-- <option value="">Select</option> -->
                                       <option value="Private Limited company" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Private Limited company') {?> selected="selected"<?php } ?> data-id="1">Private Limited company</option>
                                       <option value="Public Limited company" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Public Limited company') {?> selected="selected"<?php } ?> data-id="1">Public Limited company</option>
                                       <option value="Limited Liability Partnership" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Limited Liability Partnership') {?> selected="selected"<?php } ?> data-id="1">Limited Liability Partnership</option>
                                       <option value="Partnership" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Partnership') {?> selected="selected"<?php } ?> data-id="2">Partnership</option>
                                       <option value="Self Assessment" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Self Assessment') {?> selected="selected"<?php } ?> data-id="2">Self Assessment</option>
                                       <option value="Trust" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Trust') {?> selected="selected"<?php } ?> data-id="1">Trust</option>
                                       <option value="Charity" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Charity') {?> selected="selected"<?php } ?> data-id="1">Charity</option>
                                       <option value="Other" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Other') {?> selected="selected"<?php } ?> data-id="1">Other</option>
                                    </select>
                                 </div>
                              </div>
                              <!--<div class="form-group row name_fields">
                                 <label class="col-sm-4 col-form-label">Allocation Holder</label>
                                 <div class="col-sm-8">
                                    <input type="text" name="allocation_holders" id="allocation_holders" placeholder="Allocation holder" value="<?php if(isset($client['crm_allocation_holder']) && ($client['crm_allocation_holder']!='') ){ echo $client['crm_allocation_holder'];}?>" class="fields">
                                 </div>
                              </div>-->
                           </div>
                        </div>
                     
                  </div>
               </div>
               <!-- required information -->
               <!-- basic details tab open -->
                <div id="details" class="width-value accord-proposal1 tab-pane fade ">
                <div class="space-required">
                  <div class="main-pane-border1 cmn-errors1" id="basic_details_error">
                     <div class="alert-ss"></div>
                 </div>
               </div>                             
                              <div class="management_form1 three-animation three-animation_client management_accordion" id="important_details_section">
                                 <?php 
                                  //  $read_edit=(isset($client['status']) && $client['status']==1)? '' :'display:none;'; 
                                 $read_edit='display:none';
                                    
                                    ?>
                                 <div class="form-group row name_fields sorting  <?php if(isset($client['crm_company_url']) && ($client['crm_company_url']!='') ){?>light-color_company<?php } ?>" id="<?php echo $this->Common_mdl->get_order_details(1); ?>" <?php echo $this->Common_mdl->get_delete_details(1); ?>>
                                    <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(1); ?></label>
                                    <div class="col-sm-8 edit-field-popup1">
                                       <?php if($uri_seg!=''){ ?> 
                                       <a href="<?php if(isset($client['crm_company_url']) && ($client['crm_company_url']!='') ){ echo $client['crm_company_url'];} else { echo "#"; }?>" id="company_url_anchor" target="_blank"><?php if(isset($client['crm_company_name']) && ($client['crm_company_name']!='') ){ echo $client['crm_company_name'];}?></a> 
                                       <input type="hidden" name="company_url" id="company_url" value="<?php if(isset($client['crm_company_url']) && ($client['crm_company_url']!='') ){ echo $client['crm_company_url'];}?>" class="fields" placeholder="www.google.com"> 
                                       <input type="hidden" name="company_name" id="company_name" placeholder="Accotax Limited" value="<?php if(isset($client['crm_company_name']) && ($client['crm_company_name']!='') ){ echo $client['crm_company_name'];}?>" class="fields edit_classname" >
                                       <!-- <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                          <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p> -->
                                   
                                    <?php }else { ?> 
                                   

                                    <input type="text" name="company_name1" id="company_name1" placeholder="Accotax Limited" value="<?php if(isset($client['crm_company_name']) && ($client['crm_company_name']!='') ){ echo $client['crm_company_name'];}?>" class="fields edit_classname" >
                                    <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                       <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                    </div> 
                                  
                                    <?php } ?>
                                     </div>
                                 </div>

                              <div class="form-group row sorting newappend-03  <?php if(isset($client['crm_company_number']) && ($client['crm_company_number']!='') ){?>light-color_company<?php } ?>" id="<?php echo $this->Common_mdl->get_order_details(2); ?>" <?php echo $this->Common_mdl->get_delete_details(2); ?>>
                                 <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(2); ?></label>
                               
                                 <div class="col-sm-8 edit-field-popup1">
                                    <input type="number" name="company_number" id="company_number" placeholder="Company number" value="<?php if(isset($client['crm_company_number']) && ($client['crm_company_number']!='') ){ echo $client['crm_company_number'];}?>" class="fields edit_classname"  >
                                    <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                       <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                    </div>
                                 </div>
                                 
                              </div>
                              <div class="clearfix"></div>
                              <div class="form-group row date_birth name_fields sorting <?php if(isset($client['crm_incorporation_date']) && ($client['crm_incorporation_date']!='') ){?>light-color_company<?php } ?>" id="<?php echo $this->Common_mdl->get_order_details(3); ?>" <?php echo $this->Common_mdl->get_delete_details(3); ?>>
                                 <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(3); ?></label>
                                 <div class="col-sm-8 edit-field-popup1">
                                    <!--  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span> -->
                                    <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                    <input class="fields edit_classname <?php if(isset($client['crm_incorporation_date']) && ($client['crm_incorporation_date']!='') ){?>  light-color_company <?php }else{?> date_picker_dob <?php } ?>" type="text" name="date_of_creation" id="date_of_creation" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_incorporation_date']) && ($client['crm_incorporation_date']!='') ){ echo $client['crm_incorporation_date'];}?>" >
                                    <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                       <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                    </div>
                                 </div>
                              </div>
                              <div class="form-group row name_fields sorting <?php if(isset($client['crm_registered_in']) && ($client['crm_registered_in']!='') ){?>light-color_company<?php } ?>" id="<?php echo $this->Common_mdl->get_order_details(4); ?>" <?php echo $this->Common_mdl->get_delete_details(4); ?>>
                                 <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(4); ?></label>
                                 <div class="col-sm-8 edit-field-popup1">
                                    <input type="text" name="registered_in" id="registered_in" placeholder="" value="<?php if(isset($client['crm_registered_in']) && ($client['crm_registered_in']!='')  ){ echo $client['crm_registered_in'];}?>" class="fields edit_classname" >
                                    <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                       <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                    </div>
                                 </div>
                              </div>
                              <div class="form-group row name_fields sorting <?php if(isset($client['crm_address_line_one']) && ($client['crm_address_line_one']!='') ){?>light-color_company<?php } ?>" id="<?php echo $this->Common_mdl->get_order_details(5); ?>" <?php echo $this->Common_mdl->get_delete_details(5); ?>>
                                 <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(5); ?></label>
                                 <div class="col-sm-8 edit-field-popup1">
                                    <input type="text" name="address_line_one" id="address_line_one" placeholder="" value="<?php if(isset($client['crm_address_line_one']) && ($client['crm_address_line_one']!='')  ){ echo $client['crm_address_line_one'];}?>" class="fields edit_classname" >
                                    <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                       <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                    </div>
                                 </div>
                              </div>
                              <div class="form-group row name_fields sorting <?php if(isset($client['crm_address_line_two']) && ($client['crm_address_line_two']!='') ){?>light-color_company<?php } ?>"  id="<?php echo $this->Common_mdl->get_order_details(6); ?>" <?php echo $this->Common_mdl->get_delete_details(6); ?>>
                                 <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(6); ?></label>
                                 <div class="col-sm-8 edit-field-popup1">
                                    <input type="text" name="address_line_two" id="address_line_two" placeholder="" value="<?php if(isset($client['crm_address_line_two']) && ($client['crm_address_line_two']!='')  ){ echo $client['crm_address_line_two'];}?>" class="fields edit_classname" >
                                    <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                       <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                    </div>
                                 </div>
                              </div>
                              <div class="form-group row name_fields sorting <?php if(isset($client['crm_address_line_three']) && ($client['crm_address_line_three']!='') ){?>light-color_company<?php } ?>" id="<?php echo $this->Common_mdl->get_order_details(7); ?>" <?php echo $this->Common_mdl->get_delete_details(7); ?>>
                                 <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(7); ?></label>
                                 <div class="col-sm-8 edit-field-popup1">
                                    <input type="text" name="address_line_three" id="address_line_three" placeholder="" value="<?php if(isset($client['crm_address_line_three']) && ($client['crm_address_line_three']!='')  ){ echo $client['crm_address_line_three'];}?>" class="fields edit_classname" >
                                    <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                       <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                    </div>
                                 </div>
                              </div>
                              <div class="form-group row name_fields sorting <?php if(isset($client['crm_town_city']) && ($client['crm_town_city']!='') ){?>light-color_company<?php } ?>" id="<?php echo $this->Common_mdl->get_order_details(8); ?>" <?php echo $this->Common_mdl->get_delete_details(8); ?>>
                                 <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(8); ?></label>
                                 <div class="col-sm-8 edit-field-popup1">
                                    <input type="text" name="crm_town_city" id="crm_town_city" placeholder="" value="<?php if(isset($client['crm_town_city']) && ($client['crm_town_city']!='')  ){ echo $client['crm_town_city'];}?>" class="fields edit_classname" >
                                    <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                       <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                    </div>
                                 </div>
                              </div>
                              <div class="form-group row name_fields sorting <?php if(isset($client['crm_post_code']) && ($client['crm_post_code']!='') ){?>light-color_company<?php } ?>" id="<?php echo $this->Common_mdl->get_order_details(9); ?>" <?php echo $this->Common_mdl->get_delete_details(9); ?>>
                                 <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(9); ?></label>
                                 <div class="col-sm-8 edit-field-popup1">
                                    <input type="text" name="crm_post_code" id="crm_post_code" placeholder="" value="<?php if(isset($client['crm_post_code']) && ($client['crm_post_code']!='')  ){ echo $client['crm_post_code'];}?>" class="fields edit_classname" >
                                    <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                       <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                    </div>
                                 </div>
                              </div>
                              <div class="form-group row name_fields sorting  <?php if(isset($client['crm_company_status']) && ($client['crm_company_status']!='') ){?>light-color_company<?php } ?>" id="<?php echo $this->Common_mdl->get_order_details(10); ?>" <?php echo $this->Common_mdl->get_delete_details(10); ?>>
                                 <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(10); ?></label>
                                 <div class="col-sm-8 edit-field-popup1">
                                    <!-- <input type="text" name="company_status" id="company_status" placeholder="active" value="<?php if(isset($client['crm_company_status']) && ($client['crm_company_status']!='')  ){ echo $client['crm_company_status'];}else{ echo '
                                    active'; }?>" class="fields edit_classname" > -->
                                    
                                    <select name="company_status" id="company_status" class="fields edit_classname">
                                       <option value="1">Active</option>
                                       <option value="0">Inactive</option>
                                       <option value="3">Frozen</option>
                                       <option value="5">Archive</option>
                                    </select>
                                    <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                       <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                    </div>
                                 </div>
                              </div>
                              <div class="form-group row name_fields sorting <?php if(isset($client['crm_company_type']) && ($client['crm_company_type']!='') ){?>light-color_company<?php } ?>" id="<?php echo $this->Common_mdl->get_order_details(11); ?>" <?php echo $this->Common_mdl->get_delete_details(11); ?>>
                                 <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(11); ?></label>
                                 <div class="col-sm-8 edit-field-popup1">
                                    <input type="text" name="company_type" id="company_type" placeholder="Private Limited Company" value="<?php if(isset($client['crm_company_type']) && ($client['crm_company_type']!='') ){ echo $client['crm_company_type'];}?>" class="fields edit_classname" readonly>
                                    <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                       <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                    </div>
                                 </div>
                              </div>
                              <div class="form-group row name_fields sorting <?php if(isset($client['crm_company_sic']) && ($client['crm_company_sic']!='') ){?>light-color_company<?php } ?>" id="<?php echo $this->Common_mdl->get_order_details(12); ?>" <?php echo $this->Common_mdl->get_delete_details(12); ?>>
                                 <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(12); ?></label>
                                 <div class="col-sm-8 edit-field-popup1">
                                    <input type="text" name="company_sic" id="company_sic" value="<?php if(isset($client['crm_company_sic']) && ($client['crm_company_sic']!='') ){ echo $client['crm_company_sic'];}?>" class="fields edit_classname" > 
                                    <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                       <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                    </div>
                                    <input type="hidden" name="sic_codes" id="sic_codes" value="">
                                 </div>
                              </div>
                              <div class="form-group row date_birth sorting <?php if(isset($client['crm_engagement_letter']) && ($client['crm_engagement_letter']!='') ){?>light-color_company<?php } ?>" id="<?php echo $this->Common_mdl->get_order_details(13); ?>" <?php echo $this->Common_mdl->get_delete_details(13); ?>>
                                 <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(13); ?></label>
                                 <div class="col-sm-8">
                                    <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                    <input class="form-control fields dob_picker" placeholder="dd-mm-yyyy" type="text" name="engagement_letter" id="engagement_letter" value="<?php if(isset($client['crm_letter_sign']) && ($client['crm_letter_sign']!='') ){ echo $client['crm_letter_sign'];}?>"/>
                                 </div>
                              </div>
                              <div class="form-group row name_fields sorting <?php if(isset($client['crm_business_website']) && ($client['crm_business_website']!='') ){?>light-color_company<?php } ?>" id="<?php echo $this->Common_mdl->get_order_details(14); ?>" <?php echo $this->Common_mdl->get_delete_details(14); ?>>
                                 <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(14); ?></label>
                                 <div class="col-sm-8">
                                    <input type="text" class="fields" name="business_website" id="business_website" placeholder="Business Website" value="<?php if(isset($client['crm_business_website']) && ($client['crm_business_website']!='') ){ echo $client['crm_business_website'];}?>">
                                 </div>
                              </div>
                              <!-- rsptrspt-->

                              <div class="form-group row sorting  <?php if(isset($client['crm_company_url']) && ($client['crm_company_url']!='') ){?>light-color_company<?php } ?>" id="<?php echo $this->Common_mdl->get_order_details(15); ?>" <?php echo $this->Common_mdl->get_delete_details(15); ?>>
                                 <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(15); ?></label>
                                 <div class="col-sm-8">
                                    
                                    <input type="text" name="company_url" id="company_url" value="<?php if(isset($client['crm_company_url']) && ($client['crm_company_url']!='') ){ echo $client['crm_company_url'];}?>" class="fields" placeholder="www.google.com">
                                 </div>
                              </div>

                              <div class="form-group row sorting <?php if(isset($client['crm_officers_url']) && ($client['crm_officers_url']!='') ){?>light-color_company<?php } ?>" id="<?php echo $this->Common_mdl->get_order_details(16); ?>" <?php echo $this->Common_mdl->get_delete_details(16); ?>>
                                 <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(16); ?></label>
                                 <div class="col-sm-8">
                                   
                                    <input type="text" name="officers_url" id="officers_url" value="<?php if(isset($client['crm_officers_url']) && ($client['crm_officers_url']!='') ){ echo $client['crm_officers_url'];}?>" class="fields" placeholder="www.google.com">
                                   
                                 </div>
                              </div>


                              <div class="form-group row name_fields sorting" id="<?php echo $this->Common_mdl->get_order_details(17); ?>" <?php echo $this->Common_mdl->get_delete_details(17); ?>>
                                 <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(17); ?></label>
                                 <div class="col-sm-8">
                                    <input type="text" class="fields" name="accounting_system_inuse" id="accounting_system_inuse" value="<?php if(isset($client['crm_accounting_system_inuse']) && ($client['crm_accounting_system_inuse']!='') ){ echo $client['crm_accounting_system_inuse'];}?>">
                                 </div>
                              </div>


                               <div class="form-group row name_fields sorting" id="<?php echo $this->Common_mdl->get_order_details(178); ?>" <?php echo $this->Common_mdl->get_delete_details(178); ?>>
                                 <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(178); ?></label>
                                 <div class="col-sm-8">                                              

                                  <input type="text" value="<?php if(!empty($client['crm_hashtag'])){echo $client['crm_hashtag'];}?>" name="client_hashtag" id="tags" class="hashtag" />
                                   
                                 </div>
                              </div>
                              </div>
               </div>
               <!-- basic details tab close -->

               <div id="contacts1" class="tab-pane fade">
                <div class="space-required">
                  <div class="main-pane-border1 main_Contact" id="main_Contact_error">
                     <div class="alert-ss"></div>
                  </div>
               </div>
                  <div class="space-required remove_updatespace">
                     <div class="main-pane-border1 manage-border013">
                        <div class="pane-border1">
                           <div class="management_form1 management_accordion">
                              
                                 
                                 <div class="new_bts">
                                    <!--<select name="person" id="person" class="form-control fields">
                                       <option value="" selected="selected">--Select--</option>
                                       <?php if(isset($user['company_roles'])&& $user['company_roles']==1){ ?>
                                       <option value="opt1">Select Existing Person</option>
                                       <?php } ?>
                                       <option value="opt2">Select New Person</option>
                                    </select>-->
                                       <?php if(!empty($client['crm_company_number'])){ ?>
                                        <a href="javascript:;" id="" class="btn btn-primary add_existing_person person"  data-id="opt1" >Add Existing Person</a> 
                                       <?php } ?>
                                    <a href="javascript:;" id="Add_New_Contact" class="">Add New Person</a> 
                                 </div>
                                     <!-- 01-09-2018 -->
                                     
                                    <!-- 01-09-2018 -->
                               
                           </div>
                        </div>
                     </div>
                  </div>
                  <div id="contactss"></div>
                  <!--                                                    <form name="contact_info" id="contact_info" method="post">
                     -->
                     <div class="contact_form companies-house-form">
                  <?php 
                     $i=1;
                     
                     /*echo "<pre>";
                     print_r($contactRec);die;*/
                     foreach ($contactRec as $contactRec_key => $contactRec_value) {
                     
                               $title = $contactRec_value['title'];
                               $first_name = $contactRec_value['first_name'];
                               $middle_name = $contactRec_value['middle_name'];
                               $surname = $contactRec_value['surname']; 
                               $preferred_name = $contactRec_value['preferred_name']; 
                               $mobile = $contactRec_value['mobile']; 
                               $main_email = $contactRec_value['main_email']; 
                               $nationality = $contactRec_value['nationality']; 
                               $psc = $contactRec_value['psc']; 
                               $shareholder = $contactRec_value['shareholder']; 
                               $ni_number = $contactRec_value['ni_number']; 
                               $contact_type = $contactRec_value['contact_type']; 
                               $address_line1 = $contactRec_value['address_line1']; 
                               $address_line2 = $contactRec_value['address_line2']; 
                               $town_city = $contactRec_value['town_city']; 
                               $post_code = $contactRec_value['post_code']; 
                               $landline = $contactRec_value['landline']; 
                               $pre_landline = $contactRec_value['pre_landline'];
                               $work_email   = $contactRec_value['work_email']; 
                               $date_of_birth   = $contactRec_value['date_of_birth']; 
                               $nature_of_control   = $contactRec_value['nature_of_control']; 
                               $marital_status   = $contactRec_value['marital_status']; 
                               $utr_number   = $contactRec_value['utr_number']; 
                               $id   = $contactRec_value['id']; 
                               $client_id   = $contactRec_value['client_id']; 
                               $make_primary   = $contactRec_value['make_primary']; 
                     
                               $contact_names = $this->Common_mdl->numToOrdinalWord($i).' Contact ';
                               $name=ucfirst($contactRec_value['first_name']).' '.ucfirst($contactRec_value['surname']);
                           ?>
                               <div class="space-required new_update1">
                                          <div class="update-data01 make_a_primary  common_div_remove-<?php echo $i; ?>" id="common_div_remove-<?php echo $i; ?>">
                           
                                              <input type="hidden" class="contact_table_id" name="contact_table_id[]" value="<?php echo $id;?>">

                                             <div class="client-info-circle1 floating_set">
                                                <div class=" remove<?php echo $id;?> contactcc ">
                                                   <div class="main-contact append_contact">
                                                      <span class="h4"><!-- <?php echo $name;?> -->Contact Person Details</span>
                                                      <div class="dead-primary1 for_row_count-<?php echo $i; ?>">

                                                        <input type="hidden" name="make_primary_loop[]" id="make_primary_loop" value="<?php echo $i; ?>">
                                                         <?php if($make_primary==1){?>
                                                             <div class="radio radio-inline">
                                                            <label>
                                                             <input type="radio" name="make_primary" id="make_primary<?php echo $i;?>" value="<?php echo $i;?>" checked>
                                                              <i class="helper" style="display: none;"></i>
                                                            </label>
                                                            <a href="javascript:void(0)" class="make_primary_section active" data-id="make_primary<?php echo $i;?>" ><span>Primary Contact</span></a>
                                                         </div>
                                                         <?php }else{
                                                            /** 01-09-2018 **/
                                                            ?>
                                                         <div class="radio radio-inline">
                                                            <label>
                                                           <!--  <input type="radio" name="" id="" value="" onclick="return make_a_primary(<?php echo $id.','.$client_id;?>);"> -->
                                                            <input type="radio" name="make_primary" id="make_primary<?php echo $i;?>" value="<?php echo $i;?>">

                                                            <i class="helper" style="display: none;"></i>
                                                            </label>
                                                            <a href="javascript:void(0)" class="make_primary_section"  data-id="make_primary<?php echo $i;?>" ><span>Make a primary</span></a>
                                                         </div>
                                                           
                                                            <?php
                                                            } ?>
                                                      </div>
                                                      <!-- <a href="#" data-toggle="modal" data-target="#modalcontact<?php echo $id;?>"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a> -->
                                                   </div>
                                                   <div class="primary-info02 common-spent01 addnewclient">
                                                      <div class="primary-info03 floating_set ">
                                                         <div id="collapse" class="panel-collapse">
                                                            <div class="basic-info-client15">
                                                                  <span class="primary-inner">
                                                                     <label>title</label>
                                                                     <!-- <input type="text" class="text-info title"  name="title[]" id="title<?php echo $id;?>" value="<?php echo $title;?>">  -->
                                                                     <select class="text-info title"  name="title" id="title<?php echo $id;?>">
                                                                        <option value="Mr" <?php if(isset($title) && $title=='Mr') {?> selected="selected"<?php } ?>>Mr</option>
                                                                        <option value="Mrs" <?php if(isset($title) && $title=='Mrs') {?> selected="selected"<?php } ?>>Mrs</option>
                                                                        <option value="Miss" <?php if(isset($title) && $title=='Miss') {?> selected="selected"<?php } ?>>Miss</option>
                                                                        <option value="Dr" <?php if(isset($title) && $title=='Dr') {?> selected="selected"<?php } ?>>Dr</option>
                                                                        <option value="Ms" <?php if(isset($title) && $title=='Ms') {?> selected="selected"<?php } ?>>Ms</option>
                                                                        <option value="Prof" <?php if(isset($title) && $title=='Prof') {?> selected="selected"<?php } ?>>Prof</option>
                                                                     </select>
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($first_name) && ($first_name!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>first name</label>
                                                                  <input type="text" class="text-info" name="first_name[<?php echo $i;?>]" id="first_name" value="<?php echo $first_name;?>">
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($middle_name) && ($middle_name!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>middle name</label>
                                                                  <input type="text" class="text-info" name="middle_name[]" id="middle_name<?php echo $id;?>" value="<?php echo $middle_name;?>">
                                                                  </span>
                                                                   <span class="primary-inner <?php  if(isset($last_name) && ($last_name!='') ){ ?>light-color_company<?php } ?>">
                                                                    <label>Last name</label>
                                                                    <input type="text" name="last_name[]" id="last_name" placeholder="" value="<?php if(isset($last_name) && ($last_name!='') ){ echo $last_name;}?>" class="text-info">
                                                                    </span>
                                                                  <span class="primary-inner <?php  if(isset($surname) && ($surname!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>surname</label>
                                                                  <input type="text" class="text-info" name="surname[]" id="surname<?php echo $id;?>" value="<?php echo $surname;?>">
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($preferred_name) && ($preferred_name!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>prefered name</label>
                                                                  <input type="text" class="text-info" name="preferred_name[]" id="preferred_name<?php echo $id;?>" value="<?php echo $preferred_name;?>">
                                                                  </span> 
                                                                  <span class="primary-inner <?php  if(isset($mobile) && ($mobile!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>mobile</label>
                                                                  <input type="text" class="text-info" name="mobile_number[<?php echo $i;?>]" id="mobile<?php echo $id;?>" value="<?php echo $mobile;?>">
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($main_email) && ($main_email!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>main E-Mail address</label>
                                                                  <input type="text" class="text-info" name="main_email[<?php echo $i;?>]" id="email_id<?php echo $id;?>"  value="<?php echo $main_email;?>">
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($nationality) && ($nationality!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>Nationality</label>
                                                                  <input type="text" class="text-info" name="nationality[]" id="nationality<?php echo $id;?>"  value="<?php echo $nationality;?>">
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($psc) && ($psc!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>PSC</label>
                                                                  <input type="text" class="text-info" name="psc[]" id="psc<?php echo $id;?>"  value="<?php echo $psc;?>">
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($shareholder) && ($shareholder!='') ){ ?>light-color_company<?php } ?>">
                                                                     <label>shareholder</label>
                                                                     <select name="shareholder" id="shareholder<?php echo $id;?>">
                                                                        <option value="yes" <?php if(isset($shareholder) && $shareholder=='yes') {?> selected="selected"<?php } ?>>Yes</option>
                                                                        <option value="no" <?php if(isset($shareholder) && $shareholder=='no') {?> selected="selected"<?php } ?>>No</option>
                                                                     </select>
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($ni_number) && ($ni_number!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>national insurance number</label>
                                                                  <input type="text" class="text-info" name="ni_number[]" id="ni_number<?php echo $id;?>"  value="<?php echo $ni_number;?>">
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($contactRec_value['country_of_residence']) && ($contactRec_value['country_of_residence']!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>Country Of Residence</label>
                                                                  <input type="text" name="country_of_residence[]" id="country_of_residence<?php echo $id;?>" class="text-info" value="<?php if(isset($contactRec_value['country_of_residence']) && ($contactRec_value['country_of_residence']!='') ){ echo $contactRec_value['country_of_residence'];}?>">
                                                                  </span>
                                                               
                                                               
                                                                  <span class="primary-inner contact_type <?php  if(isset($contact_type) && ($contact_type!='') ){ ?>light-color_company<?php } ?>">
                                                                     <label>contact type</label>
                                                                     <select name="contact_type" id="contact_type<?php echo $id;?>" class="othercus">
                                                                        <option value="Director" <?php if(isset($contact_type) && $contact_type=='Director') {?> selected="selected"<?php } ?>>Director</option>
                                                                        <option value="Director/Shareholder" <?php if(isset($contact_type) && $contact_type=='Director/Shareholder') {?> selected="selected"<?php } ?>>Director/Shareholder</option>
                                                                        <option value="Shareholder" <?php if(isset($contact_type) && $contact_type=='Shareholder') {?> selected="selected"<?php } ?>>Shareholder</option>
                                                                        <option value="Accountant" <?php if(isset($contact_type) && $contact_type=='Accountant') {?> selected="selected"<?php } ?>>Accountant</option>
                                                                        <option value="Bookkeeper" <?php if(isset($contact_type) && $contact_type=='Bookkeeper') {?> selected="selected"<?php } ?>>Bookkeeper</option>
                                                                        <option value="Other" <?php if(isset($contact_type) && $contact_type=='Other') {?> selected="selected"<?php } ?>>Other(Custom)</option>
                                                                     </select>
                                                                  </span>
                                                                  <span class="primary-inner spnMulti" id="others_customs" style="<?php if($contact_type=="Other"){echo 'display:block';}else {echo "display: none;";}?>">
                                                                  <label>Other(Custom)</label>
                                                                  <input type="text" class="text-info" name="other_custom[]" id="other_custom<?php echo $id;?>"  value="<?php if(isset($contactRec_value['other_custom']) && ($contactRec_value['other_custom']!='') ){ echo $contactRec_value['other_custom'];}?>">
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($address_line1) && ($address_line1=!'') ){ ?>light-color_company<?php } ?>">
                                                                  <label>address line1</label>
                                                                  <input type="text" class="text-info" name="address_line1[]" id="address_line1<?php echo $id;?>"  value="<?php echo $address_line1;?>">
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($address_line2) && ($address_line2!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>address line2</label>
                                                                  <input type="text" class="text-info" name="address_line2[]" id="address_line2<?php echo $id;?>"  value="<?php echo $address_line2;?>">
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($town_city) && ($town_city!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>town/city</label>
                                                                  <input type="text" class="text-info" name="town_city[]" id="town_city<?php echo $id;?>"  value="<?php echo $town_city;?>">
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($post_code) && ($post_code!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>post code</label>
                                                                  <input type="text" class="text-info" name="post_code[]" id="post_code<?php echo $id;?>"  value="<?php echo $post_code;?>">
                                                                  </span>
                                                  <span class="primary-inner landlnecls <?php  if(isset($contactRec_value['landline']) && ($contactRec_value['landline']!='') ){ ?>light-color_company<?php } ?>">
                                                  <div class="cmn-land-append1 update-primary pack_add_row_wrpr_landline">
                                                                     <label>landline</label>
                                                                     <div class="remove_one_row">
                                                                  <?php 
                                                                     $land = json_decode($contactRec_value['landline'],true);
                                                                     $pre_landline = json_decode($contactRec_value['pre_landline'],true);
                                                                     //print_r( $land);
                                                                     if(!empty($land)){
                                                                     foreach($land as $key =>$val) {
                                                                        
                                                                        $preselect = ['mobile'=>'','work'=>'','home'=>'','main'=>'','workfax'=>'','homefax'=>''];
                                                                        foreach ($preselect as $preselect_key => $preselect_val)
                                                                        {
                                                                           if( $preselect_key == $pre_landline[$key])
                                                                              {
                                                                                 $preselect[$preselect_key] = "selected='selected'";
                                                                              }
                                                                        }
                                                                     ?>
                                                                  
                                                   

                                                                     <select name="pre_landline[<?php echo $i;?>][<?php echo $key;?>]" id="pre_landline<?php echo $id;?>">
                                                                        <option value="mobile" <?php echo $preselect['mobile'];?>>Mobile</option>
                                                                        <option value="work" <?php echo $preselect['mobile'];?>>Work</option>
                                                                        <option value="home" <?php echo $preselect['home'];?>>Home</option>
                                                                        <option value="main" <?php echo $preselect['main'];?>>Main</option>
                                                                        <option value="workfax" <?php echo $preselect['workfax'];?>>Work Fax</option>
                                                                        <option value="homefax" <?php echo $preselect['homefax'];?>>Home Fax</option>
                                                                     </select>
                                                                     <div class="land-spaces">   <input type="text" class="text-info" name="landline[<?php echo $i;?>][<?php echo $key;?>]" id="landline<?php echo $id;?>"  value="<?php echo $val;?>"> </div>
                                                                 
                                                
                                                                  <?php } } else {?>
                                                                  
                                                                     <select name="pre_landline[<?php echo $i;?>][0]" id="pre_landline<?php echo $id;?>">
                                                                        <option value="mobile">Mobile</option>
                                                                        <option value="work">Work</option>
                                                                        <option value="home">Home</option>
                                                                        <option value="main">Main</option>
                                                                        <option value="workfax">Work Fax</option>
                                                                        <option value="homefax">Home Fax</option>
                                                                     </select>
                                                                     <div class="land-spaces">    <input type="text" class="text-info" name="landline[<?php echo $i;?>][0]" id="landline<?php echo $id;?>"  value=""> </div>
                                                                  
                                                  
                                                                  <?php } ?>
                                                  
                                                                  <button type="button" class="btn btn-primary yk_pack_addrow_landline" data-id="<?php echo $i;?>">Add Landline</button>
                                                               
                                                               </div>
                                                               </div>
                                                </span>
                                                
                                                <span class="primary-inner displylinblock <?php  if(isset($contactRec_value['work_email']) && ($contactRec_value['work_email']!='') ){ ?>light-color_company <?php } ?>">
                                                                    <div class="update-primary pack_add_row_wrpr_email">
                                                                           <label>Work email</label>
                                                                        <div class="remove_one_row ">
                                                                  <?php 
                                                                     $work=json_decode($contactRec_value['work_email'],true);
                                                                                     if(!empty($work)){
                                                                     foreach($work as $key =>$val) {
                                                                     ?>
                                                                  
                                                                                    <input type="text" class="text-info" name="work_email[<?php echo$i;?>][<?php echo $key;?>]" id="work_email<?php echo $id;?>"  value="<?php echo $val;?>">

                                                                  <?php } } else {?>
                                                                  <input type="text" class="text-info" name="work_email[<?php echo$i;?>][0]" id="work_email<?php echo $id;?>"  value="">
                                                                  <?php } ?>
                                                                  <button type="button" class="btn btn-primary yk_pack_addrow_email" data-id="<?php echo $i;?>">Add Email</button>
                                                                     </div>
                                                                  </div>
                                                </span>  

                                                                  <span class="primary-inner date_birth <?php  if(isset($date_of_birth) && ($date_of_birth!='') ){ ?>light-color_company<?php } ?>">
                                                                     <label>dateof birth</label>
                                                                     <div class="picker-appoint">
                                                                        <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span> <input type="text" class="text-info date_picker_dob" name="date_of_birth[]"  placeholder="dd-mm-yyyy" value="<?php if(isset($date_of_birth) && ($date_of_birth!='') ){ echo date('d-m-Y',strtotime($date_of_birth));}?>">
                                                                     </div>
                                                                  </span>


                                                                  <span class="primary-inner <?php  if(isset($nature_of_control) && ($nature_of_control!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>nature of control</label>
                                                                  <input type="text" class="text-info" name="nature_of_control[]" id="nature_of_control<?php echo $id;?>"  value="<?php echo $nature_of_control;?>">
                                                                  </span>


                                                                  <span class="primary-inner <?php  if(isset($marital_status) && ($marital_status!='') ){ ?>light-color_company<?php } ?>">
                                                                     <label>marital status</label>
                                                                     <!-- <input type="text" class="text-info" name="marital_status" id="marital_status<?php// echo $id;?>"  value="<?php //echo $marital_status;?>"> -->
                                                                     <select name="marital_status" id="marital_status<?php echo $id;?>">
                                                                        <option value="Single" <?php if(isset($marital_status) && $marital_status=='Single') {?> selected="selected"<?php } ?>>Single</option>
                                                                        <option value="Living_together" <?php if(isset($marital_status) && $marital_status=='Living_together') {?> selected="selected"<?php } ?>>Living together</option>
                                                                        <option value="Engaged" <?php if(isset($marital_status) && $marital_status=='Engaged') {?> selected="selected"<?php } ?>>Engaged</option>
                                                                        <option value="Married" <?php if(isset($marital_status) && $marital_status=='Married') {?> selected="selected"<?php } ?>>Married</option>
                                                                        <option value="Civil_partner" <?php if(isset($marital_status) && $marital_status=='Civil_partner') {?> selected="selected"<?php } ?>>Civil partner</option>
                                                                        <option value="Separated" <?php if(isset($marital_status) && $marital_status=='Separated') {?> selected="selected"<?php } ?>>Separated</option>
                                                                        <option value="Divorced" <?php if(isset($marital_status) && $marital_status=='Divorced') {?> selected="selected"<?php } ?>>Divorced</option>
                                                                        <option value="Widowed" <?php if(isset($marital_status) && $marital_status=='Widowed') {?> selected="selected"<?php } ?>>Widowed</option>
                                                                     </select>
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($utr_number) && ($utr_number=!'') ){ ?>light-color_company<?php } ?>">
                                                                  <label>utr number</label>
                                                                  <input type="text" class="text-info" name="utr_number[]" id="utr_number<?php echo $id;?>"  value="<?php echo $utr_number;?>">
                                                                  </span>


                                                                  <span class="primary-inner <?php  if(isset($contactRec_value['occupation']) && ($contactRec_value['occupation']!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>Occupation</label>
                                                                  <input type="text" class="text-info" id="occupation<?php echo $id;?>"  name="occupation[]" value="<?php if(isset($contactRec_value['occupation']) && ($contactRec_value['occupation']!='') ){ echo $contactRec_value['occupation'];}?>">
                                                                  </span>

                                                                  <span class="primary-inner date_birth <?php  if(isset($contactRec_value['appointed_on']) && ($contactRec_value['appointed_on']!='') ){ ?>light-color_company<?php } ?>">
                                                                     <label>Appointed On</label>
                                                                     <div class="picker-appoint">
                                                                        <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                                        <input type="text" class="text-info" id="appointed_on<?php echo $id;?>"  name="appointed_on[]" value="<?php if(isset($contactRec_value['appointed_on']) && ($contactRec_value['appointed_on']!='') ){ echo $contactRec_value['appointed_on'];}?>">
                                                                     </div>
                                                                  </span>                                                              
                                                            
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div> 
                <!--   <input type="hidden" name="make_primary[]" id="make_primary[]" value="<?php echo $make_primary;?>"> -->
                  <?php $i++;  } ?> 
                  <input type="hidden" name="incre" id="incre" value="<?php echo $i-1;?>">
                  <!--                        </form>
                     -->
                     </div>
                  <!-- <div class="contact_form companies-house-form"></div> -->
                  <!-- <input type="hidden" name="append_cnt" id="append_cnt" value=""> -->
                  <!-- 05-07-2018 -->
                  <input type="hidden" name="append_cnt" id="append_cnt" value="<?php echo $i-1; ?>">
                  <!-- end of 05-07-2018 -->
               </div>
               <!-- contact1 -->







                                    <!-- Business Details -->   
                                     <?php 
               $service_all = array_intersect( array_column($tab_on,'tab') , ['on'] ); 
               $service_all = count($service_all)==16 ? "checked='checked'" : "";

               $reminder_all = array_intersect( array_column($tab_on,'reminder') , ['on'] ); 
               $reminder_all = count($reminder_all)==16 ? "checked='checked'" : "";

               $text_all = array_intersect( array_column($tab_on,'text') , ['on'] ); 
               $text_all = count($text_all)==16 ? "checked='checked'" : "";

               $inv_all = array_intersect( array_column($tab_on,'invoice') , ['on'] ); 
               $inv_all = count($inv_all)==16 ? "checked='checked'" : "";

               ?>
               <!-- service tab alway stay up to importend tab service tab content variable used importent tab -->
               <div id="service" class="tab-pane fade">
                  <div class="space-required">
                     <div class="basic-available-data">
                        <div class="basic-available-data1">
                           <table class="client-detail-table01 service-table-client">
                              <thead>
                                 <tr>
                                    <th>SERVICES NAME </th>
                                    <!-- <th class="switching"> <input type="checkbox" class="js-small f-right checkall" name="checkall" data-id="consu" checked="checked">  SERVICES( TABS )</th> -->
                                     <th class="switching"> <input type="checkbox" <?php echo $service_all; ?>  class="js-small f-right checkall" name="checkall" data-id="consu">  SERVICES</th>
                                    <th class="reminds"><input type="checkbox" <?php echo $reminder_all; ?> class="checkall_reminder js-small"  name="checkall" data-id="consu"> <span class="js-services1">Reminders</span></th>
                                    <th><input type="checkbox" <?php echo $text_all; ?> class="checkall_text js-small"  name="checkall" data-id="consu"> <span class="js-services1">Text</span></th>
                                    <th><input type="checkbox" <?php echo $inv_all; ?> class="checkall_notification js-small"  name="checkall" data-id="consu"> <span class="js-services1">Invoice Notifications</span></th>
                                 </tr>
                              </thead>
                            <tfoot class="ex_data1">
                                <tr>
                                <th>
                                </th>
                                </tr>
                                </tfoot>
                              <tbody>
                                 <tr>
                                    <?php 



                        
                        
                    


/*

                       $pertax = ( $tab_on['personal_tax_return']['tab']=='on' ) ? "block" : "none";

                       $pay = ( $tab_on['payroll']['tab']=='on' ) ? "block" : "none";

                      $vat =  ( $tab_on['vat']['tab']=='on' ) ? "block" : "none";

                     $management = ( $tab_on['management']['tab']=='on' ) ?"block" : "none";
*/

                     /** 12-08-2018**/
                     $investgate_block='';$investgate_none='';
                        
                        ($tab_on['investgate']['tab']=='on') ? $investgate_block = "block" : $investgate_none = "none";

                       ($tab_on['registered']['tab']=='on') ? $investgate_block = "block" : $investgate_none = "none";

                       ($tab_on['taxadvice']['tab']=='on') ? $investgate_block = "block" : $investgate_none = "none";


                       if($investgate_block!='')
                       {
                         $investgate="block";
                       }
                       else
                       {
                         $investgate="none";
                       }

                        $cst = ( $tab_on['conf_statement']['tab']!==0  ?  "checked='checked'" : "" );
                        //echo $cst."----".$tab_on['conf_statement']['tab'];die;
                        $cstre = ( $tab_on['conf_statement']['reminder']!==0 ) ?  "checked='checked'" : "";
                        $csttext = ( $tab_on['conf_statement']['text']!==0 ) ? "checked='checked'" : "";
                        $onet = ( $tab_on['conf_statement']['text']!==0 ) ? "" :"display:none;";
                        $cstinvoice = ( $tab_on['conf_statement']['invoice']!==0 ) ? "checked='checked'" : "";
                        $onet_inv = ( $tab_on['conf_statement']['invoice']!==0 ) ? "" :"display:none;";


                                    
                                       ?>
                                    <td>Confirmation Statement </td>
                                    <td class="switching"><input type="checkbox" class="js-small f-right services" name="confirm[tab]" data-id="cons" <?php echo $cst;?>></td>
                                    <td class="swi1 splwitch"><input type="checkbox" class="js-small f-right reminder" name="confirm[reminder]" <?php echo $cstre;?> data-id="enableconf"></td>
                                    <td class="swi2 textwitch"><input type="checkbox" class="js-small f-right text" name="confirm[text]" <?php echo $csttext;?> data-id="onet"></td>
                                    <td class="swi3 invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="confirm[invoice]" <?php echo $cstinvoice;?> data-id="incs"></td>
                                 </tr>
                                 <tr>
                                    <?php 
                                        $acc = ( $tab_on['accounts']['tab']!==0 ) ?  "checked='checked'" : "";                        
                                        $accre = ( $tab_on['accounts']['reminder']!==0 ) ?  "checked='checked'" : "";
                                        $acctext = ( $tab_on['accounts']['text']!==0 ) ? "checked='checked'" : "";
                                        $twot = ( $tab_on['accounts']['text']!==0 ) ? "" :"display:none;";
                                        $accinvoice = ( $tab_on['accounts']['invoice']!==0 ) ? "checked='checked'" : "";
                                        $twot_inv = ( $tab_on['accounts']['invoice']!==0 ) ? "" :"display:none;";
                                       ?>
                                    <td>Accounts</td>
                                    <td class="switching"><input type="checkbox" class="js-small f-right services" name="accounts[tab]" data-id="accounts" <?php echo $acc;?>></td>
                                    <td class="swi1 splwitch"><input type="checkbox" class="js-small f-right reminder" name="accounts[reminder]" <?php echo $accre;?> data-id="enableacc"></td>
                                    <td class="swi2 textwitch"><input type="checkbox" class="js-small f-right text" name="accounts[text]" <?php echo $acctext;?> data-id="twot"></td>
                                    <td class="swi3 invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="accounts[invoice]" <?php echo $accinvoice;?> data-id="inas"></td>
                                 </tr>
                                 <tr>
                                    
                                    <td>Company Tax Return</td>
                                    <td class="switching"><input type="checkbox" class="js-small f-right services" name="companytax[tab]" data-id="companytax" <?php echo $tax;?>></td>
                                    <td class="swi1 splwitch"><input type="checkbox" class="js-small f-right reminder" name="companytax[reminder]" <?php echo $taxre;?> data-id="enabletax"></td>
                                    <td class="swi2 textwitch"><input type="checkbox" class="js-small f-right text" name="companytax[text]" <?php echo $taxtext;?> data-id="threet"></td>
                                    <td class="swi3 invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="companytax[invoice]" <?php echo $taxinvoice;?> data-id="inct"></td>
                                 </tr>
                                 <tr>
                                    <?php 
                                       
                                        $pertax = ( $tab_on['personal_tax_return']['tab']!==0 ) ?  "checked='checked'" : "";   

                                        $pertaxre = ( $tab_on['personal_tax_return']['reminder']!==0 ) ?  "checked='checked'" : "";
                                       

                                        $pertaxtext = ( $tab_on['personal_tax_return']['text']!==0 ) ? "checked='checked'" : "";
                                        $fourt = ( $tab_on['personal_tax_return']['text']!==0 ) ? "" :"display:none;";

                                        $pertaxinvoice = ( $tab_on['personal_tax_return']['invoice']!==0 ) ? "checked='checked'" : "";
                                        $fourt_inv = ( $tab_on['personal_tax_return']['invoice']!==0 ) ? "" :"display:none;";
                                       
                                       ?>
                                    <td>Personal Tax Return</td>
                                    <td class="switching"><input type="checkbox" class="js-small f-right services" name="personaltax[tab]" data-id="personaltax" <?php echo $pertax;?>></td>
                                    <td class="swi1 splwitch"><input type="checkbox" class="js-small f-right reminder" name="personaltax[reminder]" <?php echo $pertaxre;?> data-id="enablepertax"></td>
                                    <td class="swi2 textwitch"><input type="checkbox" class="js-small f-right text" name="personaltax[text]" <?php echo $pertaxtext;?> data-id="fourt"></td>
                                    <td class="swi3 invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="personaltax[invoice]" <?php echo $pertaxinvoice;?> data-id="inpt"></td>
                                 </tr>
                                 <tr>
                                    <?php                                        
                                         $vat = ( $tab_on['vat']['tab']!==0 ) ?  "checked='checked'" : "";   

                                        $vatre = ( $tab_on['vat']['reminder']!==0 ) ?  "checked='checked'" : "";
                                        $six = ( $tab_on['vat']['reminder']!==0 ) ? "" :"display:none;";

                                        $vattext = ( $tab_on['vat']['text']!==0 ) ? "checked='checked'" : "";
                                        $sevent = ( $tab_on['vat']['text']!==0 ) ? "" :"display:none;";

                                        $vatinvoice = ( $tab_on['vat']['invoice']!==0 ) ? "checked='checked'" : "";
                                        $sevent_inv = ( $tab_on['vat']['invoice']!==0 ) ? "" :"display:none;";

                                       ?>
                                    <td>VAT Returns</td>
                                    <td class="switching"><input type="checkbox" class="js-small f-right services" name="vat[tab]" data-id="vat" <?php echo $vat;?>></td>
                                    <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="vat[reminder]" <?php echo $vatre;?> data-id="enablevat"></td>
                                    <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="vat[text]" <?php echo $vattext;?> data-id="sevent"></td>
                                    <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="vat[invoice]" <?php echo $vatinvoice;?> data-id="invt"></td>
                                 </tr>
                                 <tr>
                                    <?php 
                                       
                                       $pay = ( $tab_on['payroll']['tab']!==0 ) ?  "checked='checked'" : "";   
                                         
                                        $payre = ( $tab_on['payroll']['reminder']!==0 ) ?  "checked='checked'" : "";
                                        $four = ( $tab_on['payroll']['reminder']!==0 ) ? "" :"display:none;";

                                        $paytext = ( $tab_on['payroll']['text']!==0 ) ? "checked='checked'" : "";
                                        $fivet = ( $tab_on['payroll']['text']!==0 ) ? "" :"display:none;";

                                        $payinvoice = ( $tab_on['payroll']['invoice']!==0 ) ? "checked='checked'" : "";
                                        $fivet_inv = ( $tab_on['payroll']['invoice']!==0 ) ? "" :"display:none;";
                                       
                                       ?>
                                    <td>Payroll</td>
                                    <td class="switching"><input type="checkbox" class="js-small f-right services" name="payroll[tab]" data-id="payroll" <?php echo $pay;?>></td>
                                    <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="payroll[reminder]" <?php echo $payre;?> data-id="enablepay"></td>
                                    <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="payroll[text]" <?php echo $paytext;?> data-id="fivet"></td>
                                    <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="payroll[invoice]" <?php echo $payinvoice;?> data-id="inpr"></td>
                                 </tr>
                                 <tr>
                                    <?php 
                                     
                                        $work = ( $tab_on['workplace']['tab']!==0 ) ?  "checked='checked'" : "";   
                                        $worktab = ( $tab_on['workplace']['tab']!==0 ) ? "" :"display:none;";
                                         
                                        $workre = ( $tab_on['workplace']['reminder']!==0 ) ?  "checked='checked'" : "";
                                        $five = ( $tab_on['workplace']['reminder']!==0 ) ? "" :"display:none;";

                                        $worktext = ( $tab_on['workplace']['text']!==0 ) ? "checked='checked'" : "";
                                        $sixt = ( $tab_on['workplace']['text']!==0 ) ? "" :"display:none;";

                                        $workinvoice = ( $tab_on['workplace']['invoice']!==0 ) ? "checked='checked'" : "";
                                        $sixt_inv = ( $tab_on['workplace']['invoice']!==0 ) ? "" :"display:none;";

                                       
                                       ?>
                                    <td>WorkPlace Pension - AE</td>
                                    <td class="switching"><input type="checkbox" class="js-small f-right services" name="workplace[tab]" data-id="workplace" <?php echo $work;?>></td>
                                    <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="workplace[reminder]" <?php echo $workre;?> data-id="enablework"></td>
                                    <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="workplace[text]" <?php echo $worktext;?> data-id="sixt"></td>
                                    <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="workplace[invoice]" <?php echo $workinvoice;?> data-id="inpn"></td>
                                 </tr>
                                 <tr>
                                    <?php 
                                      
                                        $contra = ( $tab_on['cis']['tab']!==0 ) ?  "checked='checked'" : "";   
                                        $contratab = ( $tab_on['cis']['tab']!==0 ) ? "" :"display:none;";

                                        $contrare = ( $tab_on['cis']['reminder']!==0 ) ?  "checked='checked'" : "";
                                        $seven = ( $tab_on['cis']['reminder']!==0 ) ? "" :"display:none;";

                                        $contratext = ( $tab_on['cis']['text']!==0 ) ? "checked='checked'" : "";
                                        $eightt = ( $tab_on['cis']['text']!==0 ) ? "" :"display:none;";

                                        $contrainvoice = ( $tab_on['cis']['invoice']!==0 ) ? "checked='checked'" : "";
                                        $eightt_inv = ( $tab_on['cis']['invoice']!==0 ) ? "" :"display:none;";

                                       ?>
                                    <td>CIS - Contractor</td>
                                    <td class="switching"><input type="checkbox" class="js-small f-right services" name="cis[tab]" data-id="cis" <?php echo $contra;?>></td>
                                    <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="cis[reminder]" <?php echo $contrare;?> data-id="enablecis"></td>
                                    <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="cis[text]" <?php echo $contratext;?> data-id="eightt"></td>
                                    <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="cis[invoice]" <?php echo $contrainvoice;?> data-id="incis"></td>
                                 </tr>
                                 <tr>
                                    <?php 
                                       

                                         $contrasub = ( $tab_on['cissub']['tab']!==0 ) ?  "checked='checked'" : "";   
                                        $contrasubtab = ( $tab_on['cissub']['tab']!==0 ) ? "" :"display:none;";

                                        $contrasubre = ( $tab_on['cissub']['reminder']!==0 ) ?  "checked='checked'" : "";
                                        $eight = ( $tab_on['cissub']['reminder']!==0 ) ? "" :"display:none;";

                                        $contrasubtext = ( $tab_on['cissub']['text']!==0 ) ? "checked='checked'" : "";
                                        $ninet = ( $tab_on['cissub']['text']!==0 ) ? "" :"display:none;";

                                        $contrasubinvoice = ( $tab_on['cissub']['invoice']!==0 ) ? "checked='checked'" : "";
                                        $ninet_inv = ( $tab_on['cissub']['invoice']!==0 ) ? "" :"display:none;";
                                       
                                       ?>
                                    <td>CIS - Sub Contractor</td>
                                    <td class="switching"><input type="checkbox" class="js-small f-right services" name="cissub[tab]" data-id="cissub" <?php echo $contrasub;?>></td>
                                    <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="cissub[reminder]" <?php echo $contrasubre;?> data-id="enablecissub"></td>
                                    <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="cissub[text]" <?php echo $contrasubtext;?> data-id="ninet"></td>
                                    <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="cissub[invoice]" <?php echo $contrasubinvoice;?> data-id="incis_sub"></td>
                                 </tr>
                                 <tr>
                                    <?php 
                                     


                                         $p11d = ( $tab_on['p11d']['tab']!==0 ) ?  "checked='checked'" : "";   
                                        $p11dtab = ( $tab_on['p11d']['tab']!==0 ) ? "" :"display:none;";

                                        $p11dre = ( $tab_on['p11d']['reminder']!==0 ) ?  "checked='checked'" : "";
                                        $nine = ( $tab_on['p11d']['reminder']!==0 ) ? "" :"display:none;";

                                        $p11dtext = ( $tab_on['p11d']['text']!==0 ) ? "checked='checked'" : "";
                                        $tent = ( $tab_on['p11d']['text']!==0 ) ? "" :"display:none;";

                                        $p11dinvoice = ( $tab_on['p11d']['invoice']!==0 ) ? "checked='checked'" : "";
                                        $tent_inv = ( $tab_on['p11d']['invoice']!==0 ) ? "" :"display:none;";



                                       ?>
                                    <td>P11D</td>
                                    <td class="switching"><input type="checkbox" class="js-small f-right services" name="p11d[tab]" data-id="p11d" <?php echo $p11d;?>></td>
                                    <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="p11d[reminder]" <?php echo $p11dre;?> data-id="enableplld"></td>
                                    <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="p11d[text]" <?php echo $p11dtext;?> data-id="tent"></td>
                                    <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="p11d[invoice]" <?php echo $p11dinvoice;?> data-id="inp11d"></td>
                                 </tr>
                                 <tr>
                                    <?php 



                                         $management = ( $tab_on['management']['tab']!==0 ) ?  "checked='checked'" : "";   
                                        $managementtab = ( $tab_on['management']['tab']!==0 ) ? "" :"display:none;";

                                        $managementre = ( $tab_on['management']['reminder']!==0 ) ?  "checked='checked'" : "";
                                        $eleven = ( $tab_on['management']['reminder']!==0 ) ? "" :"display:none;";

                                        $managementtext = ( $tab_on['management']['text']!==0 ) ? "checked='checked'" : "";
                                        $twelvet = ( $tab_on['management']['text']!==0 ) ? "" :"display:none;";

                                        $managementinvoice = ( $tab_on['management']['invoice']!==0 ) ? "checked='checked'" : "";
                                        $twelvet_inv = ( $tab_on['management']['invoice']!==0 ) ? "" :"display:none;";





                                       ?>
                                    <td>Management Accounts</td>
                                    <td class="switching"><input type="checkbox" class="js-small f-right services" name="management[tab]" data-id="management" <?php echo $management;?>></td>
                                    <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="management[reminder]" <?php echo $managementre;?> data-id="enablemanagement"></td>
                                    <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="management[text]" <?php echo $managementtext;?> data-id="twelvet"></td>
                                    <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="management[invoice]" <?php echo $managementinvoice;?> data-id="inma"></td>
                                 </tr>
                                 <tr>
                                    <?php 


                                         $bookkeep = ( $tab_on['bookkeep']['tab']!==0 ) ?  "checked='checked'" : "";   
                                        $bookkeeptab = ( $tab_on['bookkeep']['tab']!==0 ) ? "" :"display:none;";

                                        $bookkeepre = ( $tab_on['bookkeep']['reminder']!==0 ) ?  "checked='checked'" : "";
                                        $ten = ( $tab_on['bookkeep']['reminder']!==0 ) ? "" :"display:none;";

                                        $bookkeeptext = ( $tab_on['bookkeep']['text']!==0 ) ? "checked='checked'" : "";
                                        $elevent = ( $tab_on['bookkeep']['text']!==0 ) ? "" :"display:none;";

                                        $bookkeepinvoice = ( $tab_on['bookkeep']['invoice']!==0 ) ? "checked='checked'" : "";
                                        $elevent_inv = ( $tab_on['bookkeep']['invoice']!==0 ) ? "" :"display:none;";


                                       
                                       ?>
                                    <td>Bookkeeping</td>
                                    <td class="switching"><input type="checkbox" class="js-small f-right services" name="bookkeep[tab]" data-id="bookkeep" <?php echo $bookkeep;?>></td>
                                    <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="bookkeep[reminder]" <?php echo $bookkeepre;?> data-id="enablebook"></td>
                                    </td>
                                    <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="bookkeep[text]" <?php echo $bookkeeptext;?> data-id="elevent"></td>
                                    <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="bookkeep[invoice]" <?php echo $bookkeepinvoice;?> data-id="inbook"></td>
                                 </tr>
                                 <!-- ********** -->
                                 <tr>
                                    <?php 
                                     $investgate = ( $tab_on['investgate']['tab']!==0 ) ?  "checked='checked'" : "";   

                                        $investgatere = ( $tab_on['investgate']['reminder']!==0 ) ?  "checked='checked'" : "";
                                        $twelve = ( $tab_on['investgate']['reminder']!==0 ) ? "" :"display:none;";

                                        $investgatetext = ( $tab_on['investgate']['text']!==0 ) ? "checked='checked'" : "";
                                        $thirteent = ( $tab_on['investgate']['text']!==0 ) ? "" :"display:none;";

                                        $investgateinvoice = ( $tab_on['investgate']['invoice']!==0 ) ? "checked='checked'" : "";
                                        $thirteent_inv = ( $tab_on['investgate']['invoice']!==0 ) ? "" :"display:none;";
                                       
                                       ?>
                                    <td>Investigation Insurance</td>
                                    <td class="switching"><input type="checkbox" class="js-small f-right services" name="investgate[tab]" data-id="investgate" <?php echo $investgate;?>></td>
                                    <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="investgate[reminder]" <?php echo $investgatere;?> data-id="enableinvest"></td>
                                    <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="investgate[text]" <?php echo $investgatetext;?> data-id="thirteent"></td>
                                    <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="investgate[invoice]" <?php echo $investgateinvoice;?> data-id="ininves"></td>
                                 </tr>
                                 <tr>
                                    <?php 

                                       $registered = ( $tab_on['registered']['tab']!==0 ) ?  "checked='checked'" : "";   
                                        $registeredtab = ( $tab_on['registered']['tab']!==0 ) ? "" :"display:none;";

                                        $registeredre = ( $tab_on['registered']['reminder']!==0 ) ?  "checked='checked'" : "";
                                        $thirteen = ( $tab_on['registered']['reminder']!==0 ) ? "" :"display:none;";

                                        $registeredtext = ( $tab_on['registered']['text']!==0 ) ? "checked='checked'" : "";
                                        $fourteent = ( $tab_on['registered']['text']!==0 ) ? "" :"display:none;";

                                        $registeredinvoice = ( $tab_on['registered']['invoice']!==0 ) ? "checked='checked'" : "";
                                        $fourteent_inv = ( $tab_on['registered']['invoice']!==0 ) ? "" :"display:none;";                                
                                       
                                       ?>
                                    <td>Registered Address</td>
                                    <td class="switching"><input type="checkbox" class="js-small f-right services" name="registered[tab]" data-id="registered" <?php echo $registered;?>></td>
                                    <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="registered[reminder]" <?php echo $registeredre;?> data-id="enablereg"></td>
                                    <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="registered[text]" <?php echo $registeredtext;?> data-id="fourteent"></td>
                                    <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="registered[invoice]" <?php echo $registeredinvoice;?> data-id="inreg"></td>
                                 </tr>
                                 <tr>
                                    <?php 
                                    
                                         $taxadvice = ( $tab_on['taxadvice']['tab']!==0 ) ?  "checked='checked'" : "";   
                                        $taxadvicetab = ( $tab_on['taxadvice']['tab']!==0 ) ? "" :"display:none;";

                                        $taxadvicere = ( $tab_on['taxadvice']['reminder']!==0 ) ?  "checked='checked'" : "";
                                        $fourteen = ( $tab_on['taxadvice']['reminder']!==0 ) ? "" :"display:none;";

                                        $taxadvicetext = ( $tab_on['taxadvice']['text']!==0 ) ? "checked='checked'" : "";
                                        $fifteent = ( $tab_on['taxadvice']['text']!==0 ) ? "" :"display:none;";

                                        $taxadviceinvoice = ( $tab_on['taxadvice']['invoice']!==0 ) ? "checked='checked'" : "";
                                        $fifteent_inv = ( $tab_on['taxadvice']['invoice']!==0 ) ? "" :"display:none;";

                                       
                                       ?>
                                    <td>Tax Advice/Investigation</td>
                                    <td class="switching"><input type="checkbox" class="js-small f-right services" name="taxadvice[tab]" data-id="taxadvice" <?php echo $taxadvice;?>></td>
                                    <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="taxadvice[reminder]" <?php echo $taxadvicere;?> data-id="enabletaxad"></td>
                                    <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="taxadvice[text]" <?php echo $taxadvicetext;?> data-id="fifteent"></td>
                                    <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="taxadvice[invoice]" <?php echo $taxadviceinvoice;?> data-id="intaxadv"></td>
                                 </tr>
                                 <tr style="display: none;">
                                    <?php 
                                       

                                         $inves = ( $tab_on['taxinvest']['tab']!==0 ) ?  "checked='checked'" : "";   
                                        $investab = ( $tab_on['taxinvest']['tab']!==0 ) ? "" :"display:none;";

                                        $invesre = ( $tab_on['taxinvest']['reminder']!==0 ) ?  "checked='checked'" : "";
                                        $fifteen = ( $tab_on['taxinvest']['reminder']!==0 ) ? "" :"display:none;";

                                        $investext = ( $tab_on['taxinvest']['text']!==0 ) ? "checked='checked'" : "";
                                        $sixteent = ( $tab_on['taxinvest']['text']!==0 ) ? "" :"display:none;";

                                        $invesinvoice = ( $tab_on['taxinvest']['invoice']!==0 ) ? "checked='checked'" : "";
                                        $sixteent_inv = ( $tab_on['taxinvest']['invoice']!==0 ) ? "" :"display:none;";

                                       ?>
                                    <td>Tax Investigation</td>
                                    <td class="switching"><input type="checkbox" class="js-small f-right services" name="taxinvest[tab]" data-id="taxinvest" <?php echo $inves;?>></td>
                                    <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="taxinvest[reminder]" <?php echo $invesre;?> data-id="enabletaxinves"></td>
                                    <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="taxinvest[text]" <?php echo $investext;?> data-id="sixteent"></td>
                                    <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="taxinvest[invoice]" <?php echo $invesinvoice;?> data-id="intaxinves"></td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Don't move this position to any where -->
               <!-- service -->

               <div id="assignto" class="tab-pane fade">
                  <div class="space-required">
                        <div class="main-pane-border1" id="assignto_error">
                           <div class="alert-ss"></div>
                        </div>
                  </div>
                   <input type="text" id="tree_select" name="assignees" placeholder="Select">
                   <!-- IT's only fro assinees validation -->
                    <input type="hidden" id="tree_select_values" name="assignees_values" placeholder="Select">
               </div>
               <!-- assignto --> 

<div id="import_tab" class="tab-pane fade">
  <div class="space-required">
                                          <div class="main-pane-border1 cmn-errors1" id="services_information_error">
                                             <div class="alert-ss"></div>
                                          </div>
                                       </div>
                           <ul class="accordion-views">
                              
                              <!-- confirm tab start-->
                              <?php 
                              $cnsService = ($tab_on['conf_statement']['tab']!==0) ? 'display:block' : 'display:none';
                              ?>
                              <li class="show-block" style="<?php echo $cnsService?>">
                                 <a href="#" class="toggle">  Confirmation Statement</a>
                            <div id="confirmation-statement" class="masonry-container floating_set inner-views">
                                                 <div class="grid-sizer"></div>
                                                 <div class="accordion-panel">
                                                    <div class="box-division03">
                                                       <div class="accordion-heading" role="tab" id="headingOne">
                                                          <h3 class="card-title accordion-title">
                                                             <a class="accordion-msg">Important Information</a>
                                                          </h3>
                                                       </div>
                                                       <div id="collapse" class="panel-collapse">
                                                          <div class="basic-info-client1">
                                                             <div class="form-group row name_fields">
                                                                <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(18); ?></label>
                                                                <div class="col-sm-8">
                                                                   <input type="text" name="confirmed" id="confirmation_auth_code" placeholder="" value="<?php if(isset($client['crm_companies_house_authorisation_code']) && ($client['crm_companies_house_authorisation_code']!='') ){ echo $client['crm_companies_house_authorisation_code'];}?>" class="fields confirmation_auth_code" > <!-- readonly -->
                                                                </div>
                                                             </div>
                                                          </div>
                                                       </div>
                                                    </div>
                                                 </div>
                                                 <!-- accordion-panel -->
                                                 <div class="accordion-panel ">
                                                    <div class="box-division03">
                                                       <div class="accordion-heading" role="tab" id="headingOne">
                                                          <h3 class="card-title accordion-title">
                                                             <a class="accordion-msg">Confirmation Statement</a>
                                                          </h3>
                                                       </div>
                                                       <div id="collapse" class="panel-collapse">
                                                          <div class="basic-info-client1" id="box1">
                                                             <?php 
                                                                (isset(json_decode($client['conf_statement'])->reminder) && $client['conf_statement'] != '') ? $one =  json_decode($client['conf_statement'])->reminder : $one = '';
                                                                ($one!='') ? $one_cs = "" : $one_cs = "display:none;";
                                                                
                                                                ?>  
                                                             <div class="form-group row name_fields date_birth sorting_conf" id="<?php echo $this->Common_mdl->get_order_details(23); ?>">
                                                                <label class="col-sm-4 col-form-label">Next Statement Date</label>
                                                                <span class="hilight_due_date">*</span>
                                                                <div class="col-sm-8 edit-field-popup1">
                                                                   <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span><input type="text" name="confirmation_next_made_up_to" id="confirmation_next_made_up_to" placeholder="dd-mm-yyyy" value="<?php 
                                                                      if(isset($client['crm_confirmation_statement_date']) && ($client['crm_confirmation_statement_date']!='') ){ echo date("d-m-Y", strtotime($client['crm_confirmation_statement_date'])) ;}?>" class="fields edit_classname datepicker" >
                                                                   <?php if($client['status']==1){ ?>
                                                                   <div class="edit-button-confim">
                                                                      <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                                                   </div>
                                                                   <?php } ?>
                                                                </div>
                                                             </div>
                                                             <div class="form-group row name_fields date_birth sorting_conf" id="<?php echo $this->Common_mdl->get_order_details(24); ?>">
                                                                <label class="col-sm-4 col-form-label">Due By</label>
                                                                <div class="col-sm-8 edit-field-popup1">
                                                                   <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>  <input type="text" name="confirmation_next_due" id="confirmation_next_due" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_confirmation_statement_due_date']) && ($client['crm_confirmation_statement_due_date']!='') ){ echo date("d-m-Y", strtotime($client['crm_confirmation_statement_due_date']));}?>" class="fields edit_classname datepicker" >
                                                                   <?php if($client['status']==1){ ?>
                                                                   <div class="edit-button-confim">
                                                                      <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                                                   </div>
                                                                   <?php } ?>
                                                                </div>
                                                             </div>
                                                              <div class="form-group row name_fields date_birth sorting_conf" id="<?php echo $this->Common_mdl->get_order_details(195); ?>" <?php echo $this->Common_mdl->get_delete_details(195); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(195); ?></label>
                                                         <span class="hilight_due_date">*</span>
                                                         <div class="col-sm-8 edit-field-popup1">
                                                            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                            <input type="text" name="statement_last_made_up_to_date" id="statement_last_made_up_to_date" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_confirmation_statement_last_made_up_to_date'])){
                                                             echo date('d-m-Y',strtotime($client['crm_confirmation_statement_last_made_up_to_date'])); }  ?>" class="fields edit_classname datepicker" >
                                                            <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                                               <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                                            </div>
                                                         </div>
                                                      </div>
                                                          </div>
                                                       </div>
                                                    </div>
                                                 </div>
                                                 <!--- Conformation Reminder Section -->
                                                 <div class="accordion-panel enable_conf" style="<?php echo $one_cs;?>">
                                                    <div class="box-division03">
                                                       <div class="accordion-heading" role="tab" id="headingOne">
                                                          <h3 class="card-title accordion-title">
                                                             <a class="accordion-msg">Confirmation Reminders</a>
                                                          </h3>
                                                       </div>
                                                       <div id="collapse" class="panel-collapse">
                                                          <div class="basic-info-client1" id="box2">
                                                             <div class="form-group row name_fields date_birth radio_bts">
                                                                <label class="col-sm-4 col-form-label"><a name="add_custom_reminder_link" id="add_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/1" target="_blank">As per firm setting</a></label>
                                                                <div class="col-sm-8">
                                                                   <input type="hidden" name="confirmation_next_reminder_test" id="confirmation_next_reminder_test" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_confirmation_email_remainder']) && ($client['crm_confirmation_email_remainder']!='') ){ echo $client['crm_confirmation_email_remainder'];}?>" class="fields datepicker-13">
                                                                  <!--  <input type="checkbox" class="js-small f-right fields" name="confirmation_next_reminder_date" id="confirmation_next_reminder_date"  checked="checked" onchange="check_checkbox();"> -->
                                                                      <input type="checkbox" class="js-small f-right fields as_per_firm" name="confirmation_next_reminder" id="confirmation_next_reminder_date" <?php if(isset($client['crm_confirmation_email_remainder']) && ($client['crm_confirmation_email_remainder']!='') ){ echo "checked"; } ?> onchange="check_checkbox();">
                                                                </div>
                                                             </div>
                                                            <!--  <div class="form-group row radio_bts sorting_conf" id="<?php echo $this->Common_mdl->get_order_details(26); ?>">
                                                                <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                                                                <div class="col-sm-8">
                                                                   <input type="checkbox" class="js-small f-right fields" name="create_task_reminder" id="create_task_reminder" <?php if(isset($client['crm_confirmation_create_task_reminder']) && ($client['crm_confirmation_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> value='on' >
                                                                </div>
                                                             </div> -->
                                                             <?php
                                                              $cs_cus_reminder = (!empty($client['crm_confirmation_add_custom_reminder'])?$client['crm_confirmation_add_custom_reminder']:'');
                                                             ?>
                                                             <div class="form-group row radio_bts custom_remain add_custom_reminder_label sorting_conf" id="<?php echo $this->Common_mdl->get_order_details(27); ?>" >
                                                                <label class="col-sm-4 col-form-label" id="add_custom_reminder_label" name="add_custom_reminder_label">Add Custom Reminder</label>
                                                                <div class="col-sm-8 " id="add_custom_reminder">

                                                                   <input type="checkbox" class="js-small f-right fields cus_reminder" 
                                                                    name="add_custom_reminder" id="" data-id="confirm_cus" data-serid="1"
                                                                   value="<?php echo $cs_cus_reminder;?>" <?php if($cs_cus_reminder!=''){ ?>checked <?php } ?>>
                                                                   <!-- <textarea rows="3" placeholder="" name="add_custom_reminder" id="add_custom_reminder" class="fields"><?php if(isset($client['crm_confirmation_add_custom_reminder']) && ($client['crm_confirmation_add_custom_reminder']!='') ){ echo $client['crm_confirmation_add_custom_reminder'];}?></textarea> -->
                                                                   <!--  <a name="add_custom_reminder" id="add_custom_reminder" href="<?php echo base_url();?>user/Service_reminder_settings/4" target="_blank">Click to add Custom Reminder</a> -->
                                                                </div>
                                                             </div>
                                                             <?php $cus_temp = $this->db->query('select id,subject from  custom_service_reminder where client_id='.$client['id'].' and firm_id='.$_SESSION['firm_id'].' and service_id=1 ')->result_array();
                                                                if(isset($cus_temp)){
                                                                  foreach ($cus_temp as $key => $value) {
                                                                 ?>
                                                             <div class="form-group row name_fields" style="">
                                                                <label class="col-sm-4 col-form-label cus_reminder_subject_<?php echo $value['id']; ?>"><?php echo $value['subject']; ?></label>
                                                                <div class="col-sm-8">
                                                                   <a href="javascript:;" class="date-editop" data-toggle="modal" id="cus_reminder_<?php echo $value['id']; ?>">Edit</a>
                                                                </div>
                                                             </div>
                                                             <?php } } ?>
                                                          </div>
                                                       </div>
                                                    </div>
                                                 </div>
                                                 <!-- accordion-panel -->
                                                 <!-- invoice confirmation-->
                                                 
                                                 <!-- end invoice confirmation-->
                                                 <div class="accordion-panel">
                                                    <div class="box-division03">
                                                       <div class="accordion-heading" role="tab" id="headingOne">
                                                          <h3 class="card-title accordion-title">
                                                             <a class="accordion-msg"></a>
                                                          </h3>
                                                       </div>
                                                       <div id="collapse" class="panel-collapse">
                                                          <div class="basic-info-client1" id="box3">
                                                             <div class="form-group row sorting_conf" id="<?php echo $this->Common_mdl->get_order_details(19); ?>">
                                                                <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(19); ?></label>
                                                                <div class="col-sm-8">
                                                                   <textarea rows="3" placeholder="" name="confirmation_officers" id="confirmation_officers" class="fields"><?php if(isset($client['crm_confirmation_officers']) && ($client['crm_confirmation_officers']!='') ){ echo $client['crm_confirmation_officers'];}?></textarea>
                                                                </div>
                                                             </div>
                                                             <div class="form-group row sorting_conf" id="<?php echo $this->Common_mdl->get_order_details(20); ?>">
                                                                <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(20); ?></label>
                                                                <div class="col-sm-8">
                                                                   <textarea rows="3" placeholder="" name="share_capital" id="share_capital" class="fields"><?php if(isset($client['crm_share_capital']) && ($client['crm_share_capital']!='') ){ echo $client['crm_share_capital'];}?></textarea>
                                                                </div>
                                                             </div>
                                                             <div class="form-group row sorting_conf" id="<?php echo $this->Common_mdl->get_order_details(21); ?>">
                                                                <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(21); ?></label>
                                                                <div class="col-sm-8">
                                                                   <textarea rows="3" placeholder="" name="shareholders" id="shareholders" class="fields"><?php if(isset($client['crm_shareholders']) && ($client['crm_shareholders']!='') ){ echo $client['crm_shareholders'];}?></textarea>
                                                                </div>
                                                             </div>
                                                             <div class="form-group row sorting_conf" id="<?php echo $this->Common_mdl->get_order_details(22); ?>">
                                                                <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(22); ?></label>
                                                                <div class="col-sm-8">
                                                                   <textarea rows="3" placeholder="" name="people_with_significant_control" id="people_with_significant_control" class="fields"><?php if(isset($client['crm_people_with_significant_control']) && ($client['crm_people_with_significant_control']!='') ){ echo $client['crm_people_with_significant_control'];}?></textarea>
                                                                </div>
                                                             </div>
                                                          </div>
                                                       </div>
                                                    </div>
                                                 </div>
                                                 
                                              </div>

                                 </li>
                              <!-- confirm statement  tab end -->



                              <!-- account tab start -->
<?php 

$accService = ($tab_on['accounts']['tab']!==0) ? "display:block" : 'display:none' ;


?>

                          <li class="show-block" style="<?php echo $accService; ?>">
                              <a href="#" class="toggle">  Accounts</a>


<!--Account -->
               
                  <div id="accounts" class="masonry-container floating_set inner-views">
                     <div class="grid-sizer"></div>
                     <div class="accordion-panel">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">Important Information         </a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1" id="accounts_box">
                                 <div class="form-group row name_fields accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(91); ?>">
                                    <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(91); ?></label>
                                    <div class="col-sm-8">
                                       <input type="text" name="" id="accounts_auth_code" placeholder="" value="<?php if(isset($client['crm_companies_house_authorisation_code']) && ($client['crm_companies_house_authorisation_code']!='') ){ echo $client['crm_companies_house_authorisation_code'];}?>" class="fields accounts_auth_code" ><!-- remove readonly data -->
                                    </div>
                                 </div>
                                 <div class="form-group row name_fields accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(92); ?>">
                                    <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(92); ?></label>
                                    <div class="col-sm-8">
                                       <input type="number" name="accounts_utr_number" id="accounts_utr_number" placeholder="" value="<?php if(isset($client['crm_accounts_utr_number']) && ($client['crm_accounts_utr_number']!='') ){ echo $client['crm_accounts_utr_number'];}?>" class="fields">
                                    </div>
                                 </div>
                                 <div class="form-group row radio_bts accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(93); ?>">
                                    <label class="col-sm-4 col-form-label">Companies House Reminders</label>
                                    <div class="col-sm-8" id="che_ck">
                                       <input type="checkbox" class="js-small f-right fields" name="company_house_reminder" id="company_house_reminder" <?php if(isset($client['crm_companies_house_email_remainder']) && ($client['crm_companies_house_email_remainder']=='on') ){?> checked="checked"<?php } ?>>
                                       <!--  <button id="checked">Check</button> -->
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- accordion-panel -->
                     <div class="accordion-panel accounts_tab_section" >
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">Accounts</a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1" id="accounts_box1">
                                 <?php 
                                    (isset(json_decode($client['accounts'])->reminder) && $client['accounts'] != '') ? $two =  json_decode($client['accounts'])->reminder : $two = '';
                                    
                                    ($two !='') ? $two_acc = "" : $two_acc = "display:none;";
                                    
                                    ?>
                                 <div class="form-group row name_fields date_birth accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(94); ?>">
                                    <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(94); ?></label>
                                      <span class="hilight_due_date">*</span>
                                    <div class="col-sm-8 edit-field-popup1">
                                       <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>  <input type="text" name="accounts_next_made_up_to" id="accounts_next_made_up_to" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_hmrc_yearend']) && ($client['crm_hmrc_yearend']!='') ){ echo date("d-m-Y", strtotime($client['crm_hmrc_yearend']));}?>" class="fields edit_classname datepicker" >
                                       <?php if($client['status']==1){ ?>
                                       <div class="edit-button-confim">
                                          <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                       </div>
                                       <?php } ?>
                                    </div>
                                 </div>
                                  <div class="form-group row name_fields date_birth accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(194); ?>" <?php echo $this->Common_mdl->get_delete_details(194); ?>>
                                   <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(194); ?></label>
                                   <div class="col-sm-8 edit-field-popup1">
                                      <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span><input type="text" name="accounts_last_made_up_to" id="accounts_last_made_up_to" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_accounts_last_made_up_to_date'])){ echo date('d-m-Y',strtotime($client['crm_accounts_last_made_up_to_date'])); }  ?>" class="fields edit_classname datepicker" >
                                      <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                         <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                      </div>
                                   </div>
                                </div>
                                 <div class="form-group row name_fields date_birth accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(95); ?>">
                                    <label class="col-sm-4 col-form-label">Accounts Due Date - Companies House</label>
                                    <div class="col-sm-8 edit-field-popup1">
                                       <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>   <input type="text" name="accounts_next_due" id="accounts_next_due" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_ch_accounts_next_due']) && ($client['crm_ch_accounts_next_due']!='') ){ echo date("d-m-Y", strtotime($client['crm_ch_accounts_next_due']));}?>" class="fields edit_classname datepicker" >
                                       <?php if($client['status']==1){ ?>
                                       <div class="edit-button-confim">
                                          <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                       </div>
                                       <?php } ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                   
                     <!-- accordion-panel -->
                     <div class="accordion-panel enable_acc" style="<?php echo $two_acc?>">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">Accounts Reminders</a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1" id="accounts_box2">
                                 <div class="form-group row name_fields radio_bts">
                                    <label class="col-sm-4 col-form-label"><a id="accounts_custom_reminder_link" name="accounts_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/2" target="_blank">As per firm setting</a></label>
                                    <div class="col-sm-8">
                                       <!-- <input type="hidden" name="accounts_next_reminder_date" id="accounts_next_reminder_date" placeholder="" value="<?php if(isset($client['crm_accounts_next_reminder_date']) && ($client['crm_accounts_next_reminder_date']!='') ){ echo $client['crm_accounts_next_reminder_date'];}?>" class="fields datepicker"> -->
                                       <input type="checkbox" class="js-small f-right fields as_per_firm" name="accounts_next_reminder_date" id="accounts_next_reminder_date"  <?php if(isset($client['crm_accounts_next_reminder_date']) && ($client['crm_accounts_next_reminder_date']!='') ){ echo "checked"; } ?> onchange="check_checkbox1();">
                                    </div>
                                 </div>
                                <!--  <div class="form-group row radio_bts accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(97); ?>">
                                    <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" class="js-small f-right fields" name="accounts_create_task_reminder" id="accounts_create_task_reminder" <?php if(isset($client['crm_accounts_create_task_reminder']) && ($client['crm_accounts_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> value="on" >
                                    </div>
                                 </div> -->
                                 <?php $acc_cus_reminder = (!empty($client['crm_accounts_custom_reminder'])?$client['crm_accounts_custom_reminder']:'');?>
                                 <div class="form-group row radio_bts accounts_custom_reminder_label accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(98); ?>">
                                    <label id="accounts_custom_reminder_label" name="accounts_custom_reminder_label" class="col-sm-4 col-form-label">Add Custom Reminder</label>
                                    <div class="col-sm-8 " id="accounts_custom_reminder">

                                       <input type="checkbox" class="js-small f-right fields cus_reminder" name="accounts_custom_reminder" id="" data-id="accounts_cus" data-serid="2" value="<?php echo $acc_cus_reminder;?>" <?php if($acc_cus_reminder!=''){ ?>  checked <?php } ?> >

                                       <!-- <textarea rows="4" name="accounts_custom_reminder" id="accounts_custom_reminder" class="form-control fields"><?php if(isset($client['crm_accounts_custom_reminder']) && ($client['crm_accounts_custom_reminder']!='') ){ echo $client['crm_accounts_custom_reminder'];}?></textarea> -->
                                       <!-- <a name="accounts_custom_reminder" id="accounts_custom_reminder" href="<?php echo base_url();?>user/Service_reminder_settings/3" target="_blank">Click to add Custom Reminder</a> -->
                                    </div>
                                 </div>
                                 <?php $cus_temp = $this->db->query('select * from custom_service_reminder where client_id='.$client['id'].' and firm_id ='.$_SESSION['firm_id'].' and service_id=2')->result_array();
                                    if(isset($cus_temp)){
                                      foreach ($cus_temp as $key => $value) {
                                      
                                     ?>
                                 <div class="form-group row name_fields reminder_list" style="">
                                    <label class="col-sm-4 col-form-label cus_reminder_subject_<?php echo $value['id']; ?>"><?php echo $value['subject']; ?></label>
                                    <div class="col-sm-8">
                                       <a href="javascript:;" class="date-editop" data-toggle="modal" id="cus_reminder_<?php echo $value['id']; ?>">Edit</a>
                                    </div>
                                 </div>
                                 <?php } } ?>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- accordion-panel -->
                     <!-- accounts invoive -->
<?php 
      $taxtab = ( $tab_on['company_tax_return']['tab']!==0 ) ? "" :"display:none;";
?>                      
                     <!-- end accounts invoice -->
                     <div class="companytax" style="<?php echo $taxtab;?>">
                        <div class="accordion-panel">
                           <div class="box-division03">
                              <div class="accordion-heading" role="tab" id="headingOne">
                                 <h3 class="card-title accordion-title">
                                    <a class="accordion-msg">Tax Return </a>
                                 </h3>
                              </div>
                              <div id="collapse" class="panel-collapse">
                                 <div class="basic-info-client1" id="accounts_box3">
                                    <div class="form-group row name_fields date_birth accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(95); ?>">
                                       <label class="col-sm-4 col-form-label">
                                        <?php echo $this->Common_mdl->get_name_details(95); ?>
                                        </label>
                                       <span class="hilight_due_date">*</span>
                                       <div class="col-sm-8">
                                          <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>  <input type="text" name="accounts_due_date_hmrc" id="accounts_due_date_hmrc" placeholder="dd-mm-yyyy" value="<?php if($client['crm_ch_yearend']!=''){ echo date("F j, Y", strtotime("+1 years", strtotime($client['crm_ch_yearend']))); } 
                                             ?>" class="fields datepicker-13">
                                       </div>
                                    </div>
                                    <div class="form-group row name_fields date_birth accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(100); ?>">
                                       <label class="col-sm-4 col-form-label">Tax Payment Date to HMRC</label>
                                       <div class="col-sm-8">
                                          <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span> <input type="text" name="accounts_tax_date_hmrc" id="accounts_tax_date_hmrc" placeholder="dd-mm-yyyy" value="<?php if($client['crm_ch_accounts_next_due']!=''){ echo date("F j, Y", strtotime("+1 days", strtotime($client['crm_ch_accounts_next_due']))); } ?>" class="fields datepicker-13">
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <?php 
                          $taxrecom = ( $tab_on['company_tax_return']['reminder']!==0 ) ? "" :"display:none;";
                        ?>
                        <!-- accordion-panel -->
                        <div class="accordion-panel enable_tax" style="<?php echo $taxrecom;?>">
                           <div class="box-division03">
                              <div class="accordion-heading" role="tab" id="headingOne">
                                 <h3 class="card-title accordion-title">
                                    <a class="accordion-msg">Company Tax Reminders</a>
                                 </h3>
                              </div>
                              <div id="collapse" class="panel-collapse">
                                 <div class="basic-info-client1" id="accounts_box4">
                                    <div class="form-group row radio_bts name_fields">
                                       <label class="col-sm-4 col-form-label"><a id="company_custom_reminder_link" name="company_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/3" target="_blank">As per firm setting</a></label>
                                       <div class="col-sm-8">
                                          <!-- <input type="hidden" name="company_next_reminder_date" id="company_next_reminder_date" placeholder="" value="<?php if(isset($client['crm_company_next_reminder_date']) && ($client['crm_company_next_reminder_date']!='') ){ echo $client['crm_company_next_reminder_date'];}?>" class="fields datepicker"> -->

                                          <input type="checkbox" class="js-small f-right fields as_per_firm" name="company_next_reminder_date" id="company_next_reminder_date"   <?php if(isset($client['crm_company_next_reminder_date']) && ($client['crm_company_next_reminder_date']!='') ){ echo "checked"; } ?> onchange="check_checkbox2();">
                                       </div>
                                    </div>
                                   <!--  <div class="form-group row radio_bts accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(102); ?>">
                                       <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                                       <div class="col-sm-8">
                                          <input type="checkbox" class="js-small f-right fields" name="company_create_task_reminder" id="company_create_task_reminder" <?php if(isset($client['crm_company_create_task_reminder']) && ($client['crm_company_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> value="on" >
                                       </div>
                                    </div> -->
                                    <?php $com_ret_cus_reminder = (!empty($client['crm_company_custom_reminder'])?$client['crm_company_custom_reminder']:''); ?>

                                    <div class="form-group row radio_bts company_custom_reminder_label accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(103); ?>" >
                                       <label id="company_custom_reminder_label" name="company_custom_reminder_label" class="col-sm-4 col-form-label">Add Custom Reminder</label>
                                       <div class="col-sm-8" id="company_custom_reminder">
                                          <input type="checkbox" class="js-small f-right fields cus_reminder" name="company_custom_reminder" id="" data-id="company_cus" data-serid="3" value="<?php echo $com_ret_cus_reminder; ?>" <?php if($com_ret_cus_reminder!=''){ ?> checked <?php } ?> >

                                          <!-- <textarea rows="4" name="company_custom_reminder" id="company_custom_reminder" class="form-control fields"><?php if(isset($client['crm_company_custom_reminder']) && ($client['crm_company_custom_reminder']!='') ){ echo $client['crm_company_custom_reminder'];}?></textarea> -->
                                          <!-- <a name="company_custom_reminder" id="company_custom_reminder" href="<?php echo base_url();?>user/Service_reminder_settings/5" target="_blank">Click to add Custom Reminder</a> -->
                                       </div>
                                    </div>
                                    <?php $cus_temp = $this->db->query('select id,subject from  custom_service_reminder where client_id='.$client['id'].' and firm_id='.$_SESSION['firm_id'].' and service_id=3')->result_array();
                                       if(isset($cus_temp)){
                                         foreach ($cus_temp as $key => $value) {
                                         
                                        ?>
                                    <div class="form-group row name_fields" style="">
                                       <label class="col-sm-4 col-form-label cus_reminder_subject_<?php echo $value['id']; ?>"><?php echo $value['subject']; ?></label>
                                       <div class="col-sm-8">
                                          <a href="javascript:;" class="date-editop" data-toggle="modal" id="cus_reminder_<?php echo $value['id']; ?>">Edit</a>
                                       </div>
                                    </div>
                                    <?php } } ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- accordion-panel -->
                        <!-- invoice company tax-->
                        
                        <!-- company tax invoice-->
                     </div>
                   
                  </div>
               <!-- accounts -->
                          </li>

<!-- personal-tax tab start -->
<?php $PTRService = ($tab_on['personal_tax_return']['tab']!==0) ? 'display:block' : 'display:none'; ?>

                              <li class="show-block" style="<?php echo $PTRService ?>" >
                              <a href="#" class="toggle">  Personal Tax Return</a>
                              <!-- personal-tax-returns-->
               
                  <div id="personal-tax-returns" class="masonry-container floating_set inner-views">
                     <div class="grid-sizer"></div>
                     <div class="accordion-panel">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">Important Information         </a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1" id="personal_box">
                                 <div class="form-group row personal_sort" id="<?php echo $this->Common_mdl->get_order_details(29); ?>">
                                    <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(29); ?></label>
                                    <div class="col-sm-8">
                                       <input type="number" name="personal_utr_number" id="personal_utr_number" class="form-control fields" value="<?php if(isset($client['crm_personal_utr_number']) && ($client['crm_personal_utr_number']!='') ){ echo $client['crm_personal_utr_number'];}?>">
                                    </div>
                                 </div>
                                 <div class="form-group row personal_sort" id="<?php echo $this->Common_mdl->get_order_details(30); ?>">
                                    <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(30); ?></label>
                                    <div class="col-sm-8">
                                       <input type="number" name="ni_number" id="ni_number" class="form-control fields" value="<?php if(isset($client['crm_ni_number']) && ($client['crm_ni_number']!='') ){ echo $client['crm_ni_number'];}?>">
                                    </div>
                                 </div>
                                 <div class="form-group row radio_bts personal_sort" id="<?php echo $this->Common_mdl->get_order_details(31); ?>">
                                    <label class="col-sm-4 col-form-label">Property Income</label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" class="js-small f-right fields" name="property_income" id="property_income" <?php if(isset($client['crm_property_income']) && ($client['crm_property_income']=='on') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 <div class="form-group row radio_bts personal_sort" id="<?php echo $this->Common_mdl->get_order_details(32); ?>">
                                    <label class="col-sm-4 col-form-label">Additional Income</label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" class="js-small f-right fields" name="additional_income" id="additional_income" <?php if(isset($client['crm_additional_income']) && ($client['crm_additional_income']=='on') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>

                                 <?php
                                  $pre = ($client['crm_additional_income']=='on'?"block":'none');                                  
                                  ?>
                                  <div class="form-group row text-box1 personal_sort property_income_notes" id="<?php echo $this->Common_mdl->get_order_details(193); ?>" style="display:<?php echo $pre;?>">
                                                         <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(193); ?> </label>
                                                         <div class="col-sm-8">
                                                            <textarea rows="3" placeholder="" name="property_income_notes" id="property_income_notes" class="fields">                                             
                                                              <?php echo $client['property_income_notes']; ?>
                                                            </textarea>
                                                         </div>
                                  </div>

                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- accordion-panel -->
                     <div class="accordion-panel">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">Personal Tax Return       </a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1" id="personal_box1">
                                 <div class="form-group row name_fields date_birth personal_sort" id="<?php echo $this->Common_mdl->get_order_details(33); ?>">
                                    <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(33); ?>                                      
                                    </label>
                                    <span class="hilight_due_date">*</span>
                                    <div class="col-sm-8">
                                       <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>  <input type="text" name="personal_tax_return_date" id="personal_tax_return_date" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_personal_tax_return_date']) && ($client['crm_personal_tax_return_date']!='') ){ echo $client['crm_personal_tax_return_date'];}?>" class="datepicker-13 fields">
                                    </div>
                                 </div>
                                 <div class="form-group row name_fields date_birth personal_sort" id="<?php echo $this->Common_mdl->get_order_details(34); ?>">
                                    <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(34); ?></label>
                                    <div class="col-sm-8">
                                       <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>  <input type="text" name="personal_due_date_return" id="personal_due_date_return" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_personal_due_date_return']) && ($client['crm_personal_due_date_return']!='') ){ echo $client['crm_personal_due_date_return'];}?>" class="fields datepicker-13">
                                    </div>
                                 </div>
                                 <div class="form-group row name_fields date_birth personal_sort" id="<?php echo $this->Common_mdl->get_order_details(35); ?>">
                                    <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(35); ?></label>
                                    <div class="col-sm-8">
                                       <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>   <input type="text" name="personal_due_date_online" id="personal_due_date_online" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_personal_due_date_online']) && ($client['crm_personal_due_date_online']!='') ){ echo $client['crm_personal_due_date_online'];}?>" class="fields datepicker-13">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- accordion-panel -->
                     <?php
                        $three = ( $tab_on['personal_tax_return']['reminder']!==0 ) ? "" :"display:none;";
                     ?>
                     <div class="accordion-panel enable_pertax" style="<?php echo $three;?>">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">Personal tax Reminders</a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1" id="personal_box2">
                                 <div class="form-group row name_fields radio_bts">
                                    <label class="col-sm-4 col-form-label"><a id="personal_custom_reminder_link" name="personal_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/4" target="_blank">As per firm setting</a></label>
                                    <div class="col-sm-8">
                                       <!-- <input type="hidden" name="personal_next_reminder_date" id="personal_next_reminder_date" placeholder="" value="<?php if(isset($client['crm_personal_next_reminder_date']) && ($client['crm_personal_next_reminder_date']!='') ){ echo $client['crm_personal_next_reminder_date'];}?>" class="fields datepicker"> -->
                                       <input type="checkbox" class="js-small f-right fields as_per_firm" name="personal_next_reminder_date" id="Personal_next_reminder_date" <?php if(isset($client['crm_personal_next_reminder_date']) && ($client['crm_personal_next_reminder_date']!='') ){ echo "checked"; } ?> onchange="check_checkbox3();">
                                    </div>
                                 </div>
                                 <!-- <div class="form-group row radio_bts personal_sort" id="<?php echo $this->Common_mdl->get_order_details(37); ?>">
                                    <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" class="js-small f-right fields" name="personal_task_reminder" id="personal_task_reminder" <?php if(isset($client['crm_personal_task_reminder']) && ($client['crm_personal_task_reminder']=='on') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div> -->
                                 <?php $pers_tax_cus_reminder = (!empty($client['crm_personal_custom_reminder'])?$client['crm_personal_custom_reminder']:'');?>

                                 <div class="form-group row radio_bts personal_custom_reminder_label personal_sort" id="<?php echo $this->Common_mdl->get_order_details(38); ?>">
                                    <label id="personal_custom_reminder_label" name="personal_custom_reminder_label" class="col-sm-4 col-form-label">Add Custom Reminder</label>
                                    <div class="col-sm-8" id="personal_custom_reminder">
                                       <input type="checkbox" class="js-small f-right fields cus_reminder" name="personal_custom_reminder" id="" data-id="personal_cus" data-serid="4" value="<?php echo $pers_tax_cus_reminder; ?>" <?php if($pers_tax_cus_reminder!=''){ ?> checked <?php } ?> >
                                       <!-- <textarea rows="3" placeholder="" name="personal_custom_reminder" id="personal_custom_reminder" class="fields"><?php if(isset($client['crm_personal_custom_reminder']) && ($client['crm_personal_custom_reminder']!='') ){ echo $client['crm_personal_custom_reminder'];}?></textarea> -->
                                       <!-- <a name="personal_custom_reminder" id="personal_custom_reminder" href="<?php echo base_url();?>user/Service_reminder_settings/6" target="_blank">Click to add Custom Reminder</a> -->
                                    </div>
                                 </div>
                                 <?php $cus_temp = $this->db->query('select id,subject from  custom_service_reminder where client_id='.$client['id'].' and firm_id='.$_SESSION['firm_id'].' and service_id=4')->result_array();
                                    if(isset($cus_temp)){
                                      foreach ($cus_temp as $key => $value) {
                                      
                                     ?>
                                 <div class="form-group row name_fields" style="">
                                    <label class="col-sm-4 col-form-label cus_reminder_subject_<?php echo $value['id']; ?>"><?php echo $value['subject']; ?></label>
                                    <div class="col-sm-8">
                                       <a href="javascript:;" class="date-editop" data-toggle="modal" id="cus_reminder_<?php echo $value['id']; ?>">Edit</a>
                                    </div>
                                 </div>
                                 <?php } } ?>
                              </div>
                           </div>
                        </div>
                     </div>
                  
                  </div>              
               <!-- Personal Tax Return -->
                              </li>
<!-- personal-tax tab start -->

<?php

$payrollService ="display:none";

if($tab_on['payroll']['tab']!==0 || $tab_on['workplace']['tab']!==0 ||  $tab_on['cis']['tab']!==0 || $tab_on['cissub']['tab']!==0 || $tab_on['p11d']['tab']!==0 )
{
  $payrollService ="display:block";
}

?>

 <!-- payroll tab start -->
                           <li class="show-block" style="<?php echo $payrollService?>">
                           <a href="#" class="toggle">  Payroll</a>
 <!-- Pay Roll -->              
                  <div id="payroll" class="masonry-container floating_set inner-views">
                     <div class="grid-sizer"></div>
                     <div class="accordion-panel">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">Important Information         </a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1" id="payroll_box">
                                 <div class="form-group row name_fields payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(105); ?>">
                                    <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(105); ?></label>
                                    <div class="col-sm-8">
                                       <input type="number" name="payroll_acco_off_ref_no" id="payroll_acco_off_ref_no" placeholder="" value="<?php if(isset($client['crm_payroll_acco_off_ref_no']) && ($client['crm_payroll_acco_off_ref_no']!='') ){ echo $client['crm_payroll_acco_off_ref_no'];}?>" class="fields">
                                    </div>
                                 </div>
                                 <div class="form-group row name_fields payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(106); ?>">
                                    <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(106); ?></label>
                                    <div class="col-sm-8">
                                       <input type="number" name="paye_off_ref_no" id="paye_off_ref_no" placeholder="" value="<?php if(isset($client['crm_paye_off_ref_no']) && ($client['crm_paye_off_ref_no']!='') ){ echo $client['crm_paye_off_ref_no'];}?>" class="fields">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- accordion-panel -->
                     <div class="accordion-panel for_payroll_section">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">Payroll</a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1" id="payroll_box1">
                                 <div class="form-group row date_birth payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(107); ?>">
                                    <label class="col-sm-4 col-form-label">Payroll Registration Date</label>
                                    <div class="col-sm-8">
                                       <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                       <input type="text" name="payroll_reg_date" id="datepicker1" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_payroll_reg_date']) && ($client['crm_payroll_reg_date']!='') ){ echo $client['crm_payroll_reg_date'];}?>" class="fields datepicker-13">
                                    </div>
                                 </div>
                                 <div class="form-group row payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(108); ?>">
                                    <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(108); ?></label>
                                    <div class="col-sm-8">
                                       <input type="number" name="no_of_employees" id="no_of_employees" value="<?php if(isset($client['crm_no_of_employees']) && ($client['crm_no_of_employees']!='') ){ echo $client['crm_no_of_employees'];}?>" class="fields">
                                    </div>
                                 </div>
                                 <div class="form-group row  date_birth payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(109); ?>">
                                    <label class="col-sm-4 col-form-label ">First Pay Date</label>
                                    <div class="col-sm-8">
                                       <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                       <input class="fields datepicker-13" type="text" name="first_pay_date" id="datepicker2" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_first_pay_date']) && ($client['crm_first_pay_date']!='') ){ echo $client['crm_first_pay_date'];}?>"/>
                                    </div>
                                 </div>
                                 <div class="form-group row payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(110); ?>">
                                    <label class="col-sm-4 col-form-label">Payroll Run</label>
                                    <div class="col-sm-8">
                                       <select name="payroll_run" id="payroll_run" class="form-control fields">
                                          <option value="">--Select--</option>
                                          <option value="Monthly" <?php if(isset($client['crm_payroll_run']) && $client['crm_payroll_run']=='Monthly') {?> selected="selected"<?php } ?>>Monthly</option>
                                          <option value="Weekly" <?php if(isset($client['crm_payroll_run']) && $client['crm_payroll_run']=='Weekly') {?> selected="selected"<?php } ?>>Weekly</option>
                                          <option value="Forthnightly" <?php if(isset($client['crm_payroll_run']) && $client['crm_payroll_run']=='Forthnightly') {?> selected="selected"<?php } ?>>Forthnightly</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="form-group row  date_birth payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(111); ?>">
                                    <label class="col-sm-4 col-form-label">
                                      <?php echo $this->Common_mdl->get_name_details(111); ?>
                                    </label>
                                    <span class="hilight_due_date">*</span>
                                    <div class="col-sm-8">
                                       <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                       <input class="datepicker-13 fields" type="text" name="payroll_run_date" id="datepicker3" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_payroll_run_date']) && ($client['crm_payroll_run_date']!='') ){ echo $client['crm_payroll_run_date'];}?>"/>
                                    </div>
                                 </div>
                                 <div class="form-group row  date_birth payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(112); ?>">
                                    <label class="col-sm-4 col-form-label">RTI Due/Deadline Date</label>
                                    <div class="col-sm-8">
                                       <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                       <input class="fields datepicker-13" type="text" name="rti_deadline" id="datepicker4" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_rti_deadline']) && ($client['crm_rti_deadline']!='') ){ echo $client['crm_rti_deadline'];}?>"/>
                                    </div>
                                 </div>
                                 <div class="form-group row radio_bts payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(113); ?>">
                                    <label class="col-sm-4 col-form-label">Previous Year Requires</label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" class="js-small f-right fields" name="previous_year_require" id="previous_year_require" <?php if(isset($client['crm_previous_year_require']) && ($client['crm_previous_year_require']=='on') ){?> checked="checked"<?php } ?> data-id="preyear">
                                    </div>
                                 </div>
                                 <?php 
                                    (isset($client['crm_previous_year_require']) && $client['crm_previous_year_require'] == 'on') ? $preyear =  "block" : $preyear = 'none';
                                    
                                    
                                    ?>
                                 <div id="enable_preyear" style="display:<?php echo $preyear;?>;">
                                    <div class="form-group row name_fields">
                                       <label class="col-sm-4 col-form-label">If Yes</label>
                                       <div class="col-sm-8">
                                          <!-- <input type="text" name="payroll_if_yes" id="payroll_if_yes" placeholder="How many years" value="<?php if(isset($client['crm_payroll_if_yes']) && ($client['crm_payroll_if_yes']!='') ){ echo $client['crm_payroll_if_yes'];}?>" class="fields"> -->
                                          <select name="payroll_if_yes" id="payroll_if_yes" class="form-control fields">
                                             <option value="" selected="selected">--Select--</option>
                                             <option value="01" <?php if(isset($client['crm_payroll_if_yes']) && $client['crm_payroll_if_yes']=='01') {?> selected="selected"<?php } ?>>01</option>
                                             <option value="02" <?php if(isset($client['crm_payroll_if_yes']) && $client['crm_payroll_if_yes']=='02') {?> selected="selected"<?php } ?>>02</option>
                                             <option value="03" <?php if(isset($client['crm_payroll_if_yes']) && $client['crm_payroll_if_yes']=='03') {?> selected="selected"<?php } ?>>03</option>
                                             <option value="04" <?php if(isset($client['crm_payroll_if_yes']) && $client['crm_payroll_if_yes']=='04') {?> selected="selected"<?php } ?>>04</option>
                                             <option value="05" <?php if(isset($client['crm_payroll_if_yes']) && $client['crm_payroll_if_yes']=='05') {?> selected="selected"<?php } ?>>05</option>
                                             <option value="06" <?php if(isset($client['crm_payroll_if_yes']) && $client['crm_payroll_if_yes']=='06') {?> selected="selected"<?php } ?>>06</option>
                                             <option value="07" <?php if(isset($client['crm_payroll_if_yes']) && $client['crm_payroll_if_yes']=='07') {?> selected="selected"<?php } ?>>07</option>
                                             <option value="08" <?php if(isset($client['crm_payroll_if_yes']) && $client['crm_payroll_if_yes']=='08') {?> selected="selected"<?php } ?>>08</option>
                                             <option value="09" <?php if(isset($client['crm_payroll_if_yes']) && $client['crm_payroll_if_yes']=='09') {?> selected="selected"<?php } ?>>09</option>
                                             <option value="10" <?php if(isset($client['crm_payroll_if_yes']) && $client['crm_payroll_if_yes']=='10') {?> selected="selected"<?php } ?>>10</option>
                                             <option value="11" <?php if(isset($client['crm_payroll_if_yes']) && $client['crm_payroll_if_yes']=='11') {?> selected="selected"<?php } ?>>11</option>
                                             <option value="12" <?php if(isset($client['crm_payroll_if_yes']) && $client['crm_payroll_if_yes']=='12') {?> selected="selected"<?php } ?>>12</option>
                                             <option value="13" <?php if(isset($client['crm_payroll_if_yes']) && $client['crm_payroll_if_yes']=='13') {?> selected="selected"<?php } ?>>13</option>
                                             <option value="14" <?php if(isset($client['crm_payroll_if_yes']) && $client['crm_payroll_if_yes']=='14') {?> selected="selected"<?php } ?>>14</option>
                                             <option value="15" <?php if(isset($client['crm_payroll_if_yes']) && $client['crm_payroll_if_yes']=='15') {?> selected="selected"<?php } ?>>15</option>
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group row  date_birth payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(114); ?>">
                                    <label class="col-sm-4 col-form-label">PAYE Scheme Ceased</label>
                                    <div class="col-sm-8">
                                       <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                       <input class="form-control datepicker-13 fields" type="text" name="paye_scheme_ceased" id="datepicker5" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_paye_scheme_ceased']) && ($client['crm_paye_scheme_ceased']!='') ){ echo $client['crm_paye_scheme_ceased'];}?>"/>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- accordion-panel -->
                     <div class="accordion-panel enable_pay" style="<?php echo $four;?>">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">Payroll Reminders</a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1"  id="payroll_box2">
                                 <div class="form-group row  radio_bts date_birth">
                                    <label class="col-sm-4 col-form-label"><a id="payroll_add_custom_reminder_link" name="payroll_add_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/6" target="_blank">As per firm setting</a></label>
                                    <div class="col-sm-8">
                                       <!-- <input type="hidden" name="payroll_next_reminder_date" id="payroll_next_reminder_date" placeholder="How many years" value="<?php if(isset($client['crm_payroll_next_reminder_date']) && ($client['crm_payroll_next_reminder_date']!='') ){ echo $client['crm_payroll_next_reminder_date'];}?>" class="fields datepicker"> -->
                                       <input type="checkbox" class="js-small f-right fields as_per_firm" name="Payroll_next_reminder_date" id="Payroll_next_reminder_date" <?php if(isset($client['crm_payroll_next_reminder_date']) && ($client['crm_payroll_next_reminder_date']!='') ){ echo "checked"; } ?> onchange="check_checkbox4();">
                                    </div>
                                 </div>
                                <!--  <div class="form-group row radio_bts payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(116); ?>">
                                    <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" class="js-small f-right fields" name="payroll_create_task_reminder" id="payroll_create_task_reminder" <?php if(isset($client['crm_payroll_create_task_reminder']) && ($client['crm_payroll_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> value="on" >
                                    </div>
                                 </div> -->
                                 <?php $payroll_cus_reminder = (!empty($client['crm_payroll_add_custom_reminder'])?$client['crm_payroll_add_custom_reminder']:''); ?>
                                 <div class="form-group row radio_bts payroll_add_custom_reminder_label payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(117); ?>"  >
                                    <label id="payroll_add_custom_reminder_label" name="payroll_add_custom_reminder_label" class="col-sm-4 col-form-label">Add Custom Reminder</label>
                                    <div class="col-sm-8" id="payroll_add_custom_reminder">
                                       <input type="checkbox" class="js-small f-right fields cus_reminder" name="payroll_add_custom_reminder" id="" data-id="payroll_cus" data-serid="6" value="<?php echo $payroll_cus_reminder;?>" <?php if($payroll_cus_reminder!=''){ ?> checked <?php } ?> >

                                       <!--   <textarea rows="4" name="payroll_add_custom_reminder" id="payroll_add_custom_reminder" class="form-control fields"><?php if(isset($client['crm_payroll_add_custom_reminder']) && ($client['crm_payroll_add_custom_reminder']!='') ){ echo $client['crm_payroll_add_custom_reminder'];}?></textarea> -->
                                       <!-- <a name="payroll_add_custom_reminder" id="payroll_add_custom_reminder" href="<?php echo base_url();?>user/Service_reminder_settings/7" target="_blank">Click to add Custom Reminder</a> -->
                                    </div>
                                 </div>
                                 <?php $cus_temp = $this->db->query('select id,subject from  custom_service_reminder where client_id='.$client['id'].' and firm_id='.$_SESSION['firm_id'].' and service_id=6')->result_array();
                                    if(isset($cus_temp)){
                                      foreach ($cus_temp as $key => $value) {
                                      
                                     ?>
                                 <div class="form-group row name_fields" style="">
                                    <label class="col-sm-4 col-form-label cus_reminder_subject_<?php echo $value['id']; ?>"><?php echo $value['subject']; ?></label>
                                    <div class="col-sm-8">
                                       <a href="javascript:;" class="date-editop" data-toggle="modal" id="cus_reminder_<?php echo $value['id']; ?>">Edit</a>
                                    </div>
                                 </div>
                                 <?php } } ?>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- accordion-panel -->
                     <!-- payroll invoice -->
                     
                     <!-- payroll invocie -->
                     <div class='worktab' style="<?php echo $worktab;?>">
                        <div class="accordion-panel">
                           <div class="box-division03">
                              <div class="accordion-heading" role="tab" id="headingOne">
                                 <h3 class="card-title accordion-title">
                                    <a class="accordion-msg">WorkPlace Pension - AE       </a>
                                 </h3>
                              </div>
                              <div id="collapse" class="panel-collapse">
                                 <div class="basic-info-client1" id="workplace_sec">
                                    <div class="form-group row  date_birth workplace" id="<?php echo $this->Common_mdl->get_order_details(118); ?>">
                                       <label class="col-sm-4 col-form-label">Staging Date</label>
                                       <div class="col-sm-8">
                                          <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                          <input class="form-control datepicker-13 fields" type="text" name="staging_date" id="datepicker6" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_staging_date']) && ($client['crm_staging_date']!='') ){ echo $client['crm_staging_date'];}?>"/>
                                       </div>
                                    </div>
                                    <div class="form-group row workplace" id="<?php echo $this->Common_mdl->get_order_details(119); ?>">
                                       <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(119); ?></label>
                                       <div class="col-sm-8">
                                          <input type="text" name="pension_id" id="pension_id" value="<?php if(isset($client['crm_pension_id']) && ($client['crm_pension_id']!='') ){ echo $client['crm_pension_id'];}?>" class="fields">
                                       </div>
                                    </div>
                                    <div class="form-group row  date_birth workplace" id="<?php echo $this->Common_mdl->get_order_details(120); ?>">
                                       <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(120); ?></label>
                                                            <span class="hilight_due_date">*</span>
                                       <div class="col-sm-8">
                                          <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                          <input class="form-control datepicker-13 fields" type="text" name="pension_subm_due_date" id="datepicker7" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_pension_subm_due_date']) && ($client['crm_pension_subm_due_date']!='') ){ echo $client['crm_pension_subm_due_date'];}?>"/>
                                       </div>
                                    </div>
                                    <div class="form-group row  date_birth workplace" id="<?php echo $this->Common_mdl->get_order_details(121); ?>">
                                       <label class="col-sm-4 col-form-label">Defer/Postpone Upto</label>
                                       <div class="col-sm-8">
                                          <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                          <input class="form-control datepicker-13 fields" type="text" name="defer_post_upto" id="datepicker8" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_postponement_date']) && ($client['crm_postponement_date']!='') ){ echo $client['crm_postponement_date'];}?>"/>
                                       </div>
                                    </div>
                                    <div class="form-group row  date_birth">
                                       <label class="col-sm-4 col-form-label">The Pensions Regulator Opt Out Date</label>
                                       <div class="col-sm-8">
                                          <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                          <input class="form-control datepicker-13 fields" type="text" name="pension_regulator_date" id="datepicker9" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_the_pensions_regulator_opt_out_date']) && ($client['crm_the_pensions_regulator_opt_out_date']!='') ){ echo $client['crm_the_pensions_regulator_opt_out_date'];}?>"/>
                                       </div>
                                    </div>
                                    <div class="form-group row  date_birth workplace" id="<?php echo $this->Common_mdl->get_order_details(122); ?>">
                                       <label class="col-sm-4 col-form-label">Re-Enrolment Date</label>
                                       <div class="col-sm-8">
                                          <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                          <input class="form-control datepicker-13 fields" type="text" name="paye_re_enrolment_date" id="datepicker10" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_re_enrolment_date']) && ($client['crm_re_enrolment_date']!='') ){ echo $client['crm_re_enrolment_date'];}?>"/>
                                       </div>
                                    </div>
                                    <div class="form-group row  date_birth workplace" id="<?php echo $this->Common_mdl->get_order_details(124); ?>">
                                       <label class="col-sm-4 col-form-label">Declaration of Compliance Due</label>
                                       <div class="col-sm-8">
                                          <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                          <input class="form-control datepicker-13 fields" type="text" name="declaration_of_compliance_due_date" id="datepicker11" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_declaration_of_compliance_due_date']) && ($client['crm_declaration_of_compliance_due_date']!='') ){ echo $client['crm_declaration_of_compliance_due_date'];}?>"/>
                                       </div>
                                    </div>
                                    <div class="form-group row  date_birth">
                                       <label class="col-sm-4 col-form-label">Declaration of Compliance last filed</label>
                                       <div class="col-sm-8">
                                          <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                          <input class="form-control datepicker-13 fields" type="text" name="declaration_of_compliance_last_filed" id="datepicker12" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_declaration_of_compliance_submission']) && ($client['crm_declaration_of_compliance_submission']!='') ){ echo $client['crm_declaration_of_compliance_submission'];}?>"/>
                                       </div>
                                    </div>
                                    <div class="form-group row workplace" id="<?php echo $this->Common_mdl->get_order_details(125); ?>">
                                       <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(125); ?></label>
                                       <div class="col-sm-8">
                                          <input type="text" name="paye_pension_provider" id="paye_pension_provider" value="<?php if(isset($client['crm_paye_pension_provider']) && ($client['crm_paye_pension_provider']!='') ){ echo $client['crm_paye_pension_provider'];}?>" class="fields">
                                       </div>
                                    </div>
                                    <div class="form-group row workplace" id="<?php echo $this->Common_mdl->get_order_details(126); ?>">
                                       <label class="col-sm-4 col-form-label">Pension Provider User ID</label>
                                       <div class="col-sm-8">
                                          <input type="text" name="paye_pension_provider_userid" id="paye_pension_provider_userid" value="<?php if(isset($client['crm_pension_id']) && ($client['crm_pension_id']!='') ){ echo $client['crm_pension_id'];}?>" class="fields">
                                       </div>
                                    </div>
                                    <div class="form-group row workplace" id="<?php echo $this->Common_mdl->get_order_details(126); ?>">
                                       <label class="col-sm-4 col-form-label">Pension Provider Password</label>
                                       <div class="col-sm-8">
                                          <input type="password" name="paye_pension_provider_password" id="paye_pension_provider_password" value="<?php if(isset($client['crm_paye_pension_provider_password']) && ($client['crm_paye_pension_provider_password']!='') ){ echo $client['crm_paye_pension_provider_password'];}?>" class="fields">
                                       </div>
                                    </div>
                                    <div class="form-group row workplace" id="<?php echo $this->Common_mdl->get_order_details(127); ?>">
                                       <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(127); ?></label>
                                       <div class="col-sm-8">
                                          <input type="number" name="employer_contri_percentage" id="employer_contri_percentage" value="<?php if(isset($client['crm_employer_contri_percentage']) && ($client['crm_employer_contri_percentage']!='') ){ echo $client['crm_employer_contri_percentage'];}?>" class="fields">
                                       </div>
                                    </div>
                                    <div class="form-group row workplace" id="<?php echo $this->Common_mdl->get_order_details(128); ?>">
                                       <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(128); ?></label>
                                       <div class="col-sm-8">
                                          <input type="number" name="employee_contri_percentage" id="employee_contri_percentage" value="<?php if(isset($client['crm_employee_contri_percentage']) && ($client['crm_employee_contri_percentage']!='') ){ echo $client['crm_employee_contri_percentage'];}?>" class="fields">
                                       </div>
                                    </div>
                                    <div class="form-group row">
                                       <label class="col-sm-4 col-form-label">Other Notes</label>
                                       <div class="col-sm-8">
                                          <textarea rows="3" placeholder="" name="pension_notes" id="pension_notes" class="fields"><?php if(isset($client['crm_pension_notes']) && ($client['crm_pension_notes']!='') ){ echo $client['crm_pension_notes'];}?></textarea>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- accordion-panel -->
                        <div class="accordion-panel enable_work" style="<?php echo $five;?>">
                           <div class="box-division03">
                              <div class="accordion-heading" role="tab" id="headingOne">
                                 <h3 class="card-title accordion-title">
                                    <a class="accordion-msg">Pension Reminders</a>
                                 </h3>
                              </div>
                              <div id="collapse" class="panel-collapse">
                                 <div class="basic-info-client1" id="workplace_sec1">
                                    <div class="form-group row radio_bts date_birth">
                                       <label class="col-sm-4 col-form-label"><a id="pension_create_task_reminder_link" name="pension_create_task_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/7" target="_blank">As per firm setting</a></label>
                                       <div class="col-sm-8">
                                          <!-- <input type="hidden" name="pension_next_reminder_date" id="pension_next_reminder_date" placeholder="How many years" value="<?php if(isset($client['crm_pension_next_reminder_date']) && ($client['crm_pension_next_reminder_date']!='') ){ echo $client['crm_pension_next_reminder_date'];}?>" class="fields datepicker"> -->
                                          <input type="checkbox" class="js-small f-right fields as_per_firm" name="Pension_next_reminder_date" id="Pension_next_reminder_date"   <?php if(isset($client['crm_pension_next_reminder_date']) && ($client['crm_pension_next_reminder_date']!='') ){ echo "checked"; } ?> onchange="check_checkbox5();">
                                       </div>
                                    </div>
                                   <!--  <div class="form-group row radio_bts workplace" id="<?php echo $this->Common_mdl->get_order_details(130); ?>">
                                       <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                                       <div class="col-sm-8">
                                          <input type="checkbox" class="js-small f-right fields" name="pension_create_task_reminder" id="pension_create_task_reminder" <?php if(isset($client['crm_pension_create_task_reminder']) && ($client['crm_pension_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> value="on" >
                                       </div>
                                    </div> -->
                                    <?php $work_pension_cus_reminder = (!empty($client['crm_pension_add_custom_reminder'])?$client['crm_pension_add_custom_reminder']:''); ?>

                                    <div class="form-group row radio_bts pension_create_task_reminder_label workplace" id="<?php echo $this->Common_mdl->get_order_details(131); ?>">
                                       <label id="pension_create_task_reminder_label" name="pension_create_task_reminder_label" class="col-sm-4 col-form-label">Add Custom Reminder</label>
                                       <div class="col-sm-8" id="pension_add_custom_reminder">
                                          <input type="checkbox" class="js-small f-right fields cus_reminder" name="pension_add_custom_reminder" id="" data-id="pension_cus" data-serid="7" value="<?php echo $work_pension_cus_reminder;?>" <?php if($work_pension_cus_reminder!=''){ ?> checked <?php } ?> >
                                          <!-- <textarea rows="4" name="pension_add_custom_reminder" id="pension_add_custom_reminder" class="form-control fields"><?php if(isset($client['crm_pension_add_custom_reminder']) && ($client['crm_pension_add_custom_reminder']!='') ){ echo $client['crm_pension_add_custom_reminder'];}?></textarea> -->
                                          <!-- <a name="pension_add_custom_reminder" id="pension_add_custom_reminder" href="<?php echo base_url();?>user/Service_reminder_settings/8" target="_blank">Click to add Custom Reminder</a> -->
                                       </div>
                                    </div>
                                    <?php $cus_temp = $this->db->query('select id,subject from  custom_service_reminder where client_id='.$client['id'].' and firm_id='.$_SESSION['firm_id'].' and service_id=7')->result_array();
                                       if(isset($cus_temp)){
                                         foreach ($cus_temp as $key => $value) {
                                         
                                        ?>
                                    <div class="form-group row name_fields" style="">
                                       <label class="col-sm-4 col-form-label cus_reminder_subject_<?php echo $value['id']; ?>"><?php echo $value['subject']; ?></label>
                                       <div class="col-sm-8">
                                          <a href="javascript:;" class="date-editop" data-toggle="modal" id="cus_reminder_<?php echo $value['id']; ?>">Edit</a>
                                       </div>
                                    </div>
                                    <?php } } ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- accordion-panel -->
                     </div>
                     <!--worktab close-->
                     <!--  <div class="accordion-panel enable_pay" style="<?php echo $four;?>">
                        <div class="accordion-heading" role="tab" id="headingOne">
                           <h3 class="card-title accordion-title">
                           <a class="accordion-msg">Reminders</a></h3>
                        </div>
                        <div id="collapse" class="panel-collapse">
                           <div class="basic-info-client1">
                        <div class="form-group row  date_birth">
                        <label class="col-sm-4 col-form-label">Next Reminder Date</label>
                        <div class="col-sm-8"> -->
                     <!--  <input type="text" name="payroll_next_reminder_date" id="payroll_next_reminder_date" placeholder="How many years" value="<?php //if(isset($client['crm_payroll_next_reminder_date']) && ($client['crm_payroll_next_reminder_date']!='') ){ echo $client['crm_payroll_next_reminder_date'];}?>" class="fields datepicker">-->
                     <!-- <a id="Declration_add_custom_reminder_link" name="Declration_add_custom_reminder_link" target="_blank" href="<?php echo base_url(); ?>user/Service_reminder_settings/9">Declration next reminder date</a>
                        <input type="checkbox" class="js-small f-right fields" name="Declatrion_next_reminder_date" id="Declatrion_next_reminder_date"  checked="checked" onchange="check_checkbox11();"> -->
                     <!--lllll-->
                     <!--  </div>
                        </div> 
                        
                        <div class="form-group row radio_bts ">
                        <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                        <div class="col-sm-8">
                            <input type="checkbox" class="js-small f-right fields" name="Declration_create_task_reminder" id="Declration_create_task_reminder" <?php if(isset($client['crm_Declration_create_task_reminder']) && ($client['crm_Declration_create_task_reminder']=='on') ){?> checked="checked"<?php } ?>>
                         </div>
                         </div>
                        
                          <div class="form-group row radio_bts fivet" style="<?php echo $fivet;?>">
                        <label id="Declration_add_custom_reminder_label" name="Declration_add_custom_reminder_label" class="col-sm-4 col-form-label">Add Custom Reminder</label>
                        <div class="col-sm-8"> -->
                     <!-- <input type="checkbox" class="js-small f-right fields" name="payroll_add_custom_reminder" id="payroll_add_custom_reminder" <?php if(isset($client['crm_payroll_add_custom_reminder']) && ($client['crm_Declration_add_custom_reminder']=='on') ){?> checked="checked"<?php } ?>> -->
                     <!-- <textarea rows="4" name="Declration_add_custom_reminder" id="Declration_add_custom_reminder" class="form-control fields"><?php if(isset($client['crm_Declration_add_custom_reminder']) && ($client['crm_Declration_add_custom_reminder']!='') ){ echo $client['crm_Declration_add_custom_reminder'];}?></textarea> -->
                     <!-- <a name="Declration_add_custom_reminder" id="Declration_add_custom_reminder" href="<?php echo base_url();?>user/Service_reminder_settings/9" target="_blank">As per firm setting</a>
                        </div>
                        </div>
                                
                             </div>
                          </div>
                        </div> -->
                    
                     <div class="contratab" style="<?php echo $contratab;?>">
                        <div class="accordion-panel">
                           <div class="box-division03">
                              <div class="accordion-heading" role="tab" id="headingOne">
                                 <h3 class="card-title accordion-title">
                                    <a class="accordion-msg"> CIS Contractor/Sub Contractor</a>
                                 </h3>
                              </div>
                              <div id="collapse" class="panel-collapse">
                                 <div class="basic-info-client1">
                                    <div class="form-group row radio_bts name_fields">
                                       <label class="col-sm-4 col-form-label">C.I.S Contractor</label>
                                       <div class="col-sm-8">
                                          <input type="checkbox" class="js-small f-right fields" name="cis_contractor" id="cis_contractor" <?php if(isset($client['crm_cis_contractor']) && ($client['crm_cis_contractor']=='on') ){?> checked="checked"<?php } ?>>
                                       </div>
                                    </div>
                                    <div class="form-group row  date_birth">
                                       <label class="col-sm-4 col-form-label">Start Date</label>
                                       <div class="col-sm-8">
                                          <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                          <span class="hilight_due_date">*</span>
                                          <input class="form-control datepicker-13 fields" type="text" name="cis_contractor_start_date" id="datepicker14" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_cis_contractor_start_date']) && ($client['crm_cis_contractor_start_date']!='') ){ echo $client['crm_cis_contractor_start_date'];}?>"/>
                                       </div>
                                    </div>
                                    <div class="form-group row">
                                       <label class="col-sm-4 col-form-label">C.I.S Scheme</label>
                                       <div class="col-sm-8">
                                          <textarea rows="3" placeholder="" name="cis_scheme_notes" id="cis_scheme_notes" class="fields"><?php if(isset($client['crm_cis_scheme_notes']) && ($client['crm_cis_scheme_notes']!='') ){ echo $client['crm_cis_scheme_notes'];}?></textarea>
                                       </div>
                                    </div>
                                    <div class="form-group row name_fields radio_bts">
                                       <label class="col-sm-4 col-form-label">CIS Subcontractor</label>
                                       <div class="col-sm-8">
                                          <input type="checkbox" name="cis_subcontractor" id="cis_subcontractor" class="js-small f-right fields" <?php if(isset($client['crm_cis_subcontractor']) && ($client['crm_cis_subcontractor']=='on') ){?> checked="checked"<?php } ?>>
                                       </div>
                                    </div>
                                    <div class="form-group row  date_birth">
                                       <label class="col-sm-4 col-form-label">Start Date</label>
                                       <div class="col-sm-8">
                                          <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                          <span class="hilight_due_date">*</span>
                                          <input class="form-control datepicker-13 fields" type="text" name="cis_subcontractor_start_date" id="datepicker15" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_cis_subcontractor_start_date']) && ($client['crm_cis_subcontractor_start_date']!='') ){ echo $client['crm_cis_subcontractor_start_date'];}?>"/>
                                       </div>
                                    </div>
                                    <div class="form-group row">
                                       <label class="col-sm-4 col-form-label">C.I.S Scheme</label>
                                       <div class="col-sm-8">
                                          <textarea rows="3" placeholder="" name="cis_subcontractor_scheme_notes" id="cis_subcontractor_scheme_notes" class="fields"><?php if(isset($client['crm_cis_subcontractor_scheme_notes']) && ($client['crm_cis_subcontractor_scheme_notes']!='') ){ echo $client['crm_cis_subcontractor_scheme_notes'];}?></textarea>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- accordion-panel -->
                        <div class="accordion-panel enable_cis"  style="<?php echo $seven;?>">
                           <div class="box-division03">
                              <div class="accordion-heading" role="tab" id="headingOne">
                                 <h3 class="card-title accordion-title">
                                    <a class="accordion-msg">CIS Reminders</a>
                                 </h3>
                              </div>
                              <div id="collapse" class="panel-collapse">
                                 <div class="basic-info-client1">
                                    <div class="form-group row  date_birth radio_bts">
                                       <label class="col-sm-4 col-form-label"><a id="cis_add_custom_reminder_link" name="cis_add_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/8" target="_blank">As per firm setting</a></label>
                                       <div class="col-sm-8">
                                          <!--  <input type="hidden" name="cis_next_reminder_date" id="cis_next_reminder_date" placeholder="How many years" value="<?php if(isset($client['crm_cis_next_reminder_date']) && ($client['crm_cis_next_reminder_date']!='') ){ echo $client['crm_cis_next_reminder_date'];}?>" class="fields datepicker"> -->
                                          <input type="checkbox" class="js-small f-right fields as_per_firm" name="Cis_next_reminder_date" id="Cis_next_reminder_date" <?php if(isset($client['crm_cis_next_reminder_date']) && ($client['crm_cis_next_reminder_date']!='') ){ echo "checked"; } ?> onchange="check_checkbox6();">
                                          <!--ggggg-->
                                       </div>
                                    </div>
                                    <!-- <div class="form-group row radio_bts eightt" style="<?php //echo $eight;?>">
                                       <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                                       <div class="col-sm-8">
                                          <input type="checkbox" class="js-small f-right fields" name="cis_create_task_reminder" id="cis_create_task_reminder" <?php if(isset($client['crm_cis_create_task_reminder']) && ($client['crm_cis_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> value="on" >
                                       </div>
                                    </div> -->
                                    <?php $cis_cus_reminder = (!empty($client['crm_cis_add_custom_reminder'])?$client['crm_cis_add_custom_reminder']:''); ?>

                                    <div class="form-group row radio_bts cis_add_custom_reminder_label" >
                                       <label id="cis_add_custom_reminder_label" name="cis_add_custom_reminder_label" class="col-sm-4 col-form-label">Add Custom Reminder</label>
                                       <div class="col-sm-8" id="cis_add_custom_reminder">
                                          <input type="checkbox" class="js-small f-right fields cus_reminder" name="cis_add_custom_reminder" id="" data-id="cis_cus" data-serid="8" value="<?php echo $cis_cus_reminder;?>" <?php if($cis_cus_reminder!=''){ ?> checked <?php } ?> >
                                          <!-- <textarea rows="4" name="cis_add_custom_reminder" id="cis_add_custom_reminder" class="form-control fields"><?php if(isset($client['crm_cis_add_custom_reminder']) && ($client['crm_cis_add_custom_reminder']!='') ){ echo $client['crm_cis_add_custom_reminder'];}?></textarea> -->
                                          <!-- <a name="cis_add_custom_reminder" id="cis_add_custom_reminder" href="<?php echo base_url();?>user/Service_reminder_settings/10" target="_blank">Click to add Custom Reminder</a> -->
                                       </div>
                                    </div>
                                    <?php $cus_temp = $this->db->query('select id,subject from  custom_service_reminder where client_id='.$client['id'].' and firm_id='.$_SESSION['firm_id'].' and service_id=8')->result_array();
                                       if(isset($cus_temp)){
                                         foreach ($cus_temp as $key => $value) {
                                         
                                        ?>
                                    <div class="form-group row name_fields" style="">
                                       <label class="col-sm-4 col-form-label cus_reminder_subject_<?php echo $value['id']; ?>"><?php echo $value['subject']; ?></label>
                                       <div class="col-sm-8">
                                          <a href="javascript:;" class="date-editop" data-toggle="modal" id="cus_reminder_<?php echo $value['id']; ?>">Edit</a>
                                       </div>
                                    </div>
                                    <?php } } ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- accordion-panel -->
                     </div>
                     <!-- contratab close-->
                     <!-- cis invoice-->
                     
                     <!-- cis invoice-->
                     <div class="accordion-panel  enable_cissub" style="<?php echo $eight;?>">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">CIS SUB Reminders</a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1">
                                 <div class="form-group row  date_birth radio_bts">
                                    <label class="col-sm-4 col-form-label"><a id="cissub_add_custom_reminder_link" name="cissub_add_custom_reminder_link" target="_blank" href="<?php echo base_url(); ?>user/Service_reminder_settings/9">CIS-SUB next reminder date</a></label>
                                    <div class="col-sm-8">
                                       <!--  <input type="text" name="payroll_next_reminder_date" id="payroll_next_reminder_date" placeholder="How many years" value="<?php //if(isset($client['crm_payroll_next_reminder_date']) && ($client['crm_payroll_next_reminder_date']!='') ){ echo $client['crm_payroll_next_reminder_date'];}?>" class="fields datepicker">-->
                                       <input type="checkbox" class="js-small f-right fields as_per_firm" name="cissub_next_reminder_date" id="cissub_next_reminder_date" <?php if(isset($client['crm_cissub_next_reminder_date']) && ($client['crm_cissub_next_reminder_date']=='on')){ ?> checked="checked" <?php } ?> onchange="check_checkbox12();">
                                       <!--mmmmm-->
                                    </div>
                                 </div>
                                <!--  <div class="form-group row radio_bts ">
                                    <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" class="js-small f-right fields" name="cissub_create_task_reminder" id="cissub_create_task_reminder" <?php if(isset($client['crm_cissub_create_task_reminder']) && ($client['crm_cissub_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> value="on" >
                                    </div>
                                 </div> -->
                                 <?php $cis_sub_cus_reminder = (!empty($client['crm_cissub_add_custom_reminder'])?$client['crm_cissub_add_custom_reminder']:''); ?>

                                 <div class="form-group row radio_bts cissub_add_custom_reminder_label"  >
                                    <label id="cissub_add_custom_reminder_label" name="cissub_add_custom_reminder_label" class="col-sm-4 col-form-label">Add Custom Reminder</label>
                                    <div class="col-sm-8" id="cissub_add_custom_reminder">
                                       <input type="checkbox" class="js-small f-right fields cus_reminder" name="cissub_add_custom_reminder" id="" data-id="cissub_cus" data-serid="9" value="<?php echo $cis_sub_cus_reminder;?>" <?php if($cis_sub_cus_reminder!=''){ ?> checked <?php } ?> >
                                       <!--  <textarea rows="4" name="cissub_add_custom_reminder" id="cissub_add_custom_reminder" class="form-control fields"><?php if(isset($client['crm_cissub_add_custom_reminder']) && ($client['crm_cissub_add_custom_reminder']!='') ){ echo $client['crm_cissub_add_custom_reminder'];}?></textarea> -->
                                       <!-- <a name="cissub_add_custom_reminder" id="cissub_add_custom_reminder" href="<?php echo base_url();?>user/Service_reminder_settings/11" target="_blank">Click to add Custom Reminder</a> -->
                                    </div>
                                 </div>
                                 <?php $cus_temp = $this->db->query('select id,subject from  custom_service_reminder where client_id='.$client['id'].' and firm_id='.$_SESSION['firm_id'].' and service_id=9')->result_array();
                                    if(isset($cus_temp)){
                                      foreach ($cus_temp as $key => $value) {
                                      
                                     ?>
                                 <div class="form-group row name_fields" style="">
                                    <label class="col-sm-4 col-form-label cus_reminder_subject_<?php echo $value['id']; ?>"><?php echo $value['subject']; ?></label>
                                    <div class="col-sm-8">
                                       <a href="javascript:;" class="date-editop" data-toggle="modal" id="cus_reminder_<?php echo $value['id']; ?>">Edit</a>
                                    </div>
                                 </div>
                                 <?php } } ?>
                              </div>
                           </div>
                        </div>
                     </div>
                    
                     <div class="p11dtab" style="<?php echo $p11dtab?>">
                        <div class="accordion-panel">
                           <div class="box-division03">
                              <div class="accordion-heading" role="tab" id="headingOne">
                                 <h3 class="card-title accordion-title">
                                    <a class="accordion-msg">P11D</a>
                                 </h3>
                              </div>
                              <div id="collapse" class="panel-collapse">
                                 <div class="basic-info-client1" id="p11d_sec">
                                    <div class="form-group row  date_birth p11d" id="<?php echo $this->Common_mdl->get_order_details(132); ?>">
                                       <label class="col-sm-4 col-form-label">P11D Start Date</label>
                                       <div class="col-sm-8">
                                          <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                          <input class="form-control datepicker-13 fields" type="text" name="p11d_start_date" id="datepicker17" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_p11d_latest_action_date']) && ($client['crm_p11d_latest_action_date']!='') ){ echo $client['crm_p11d_latest_action_date'];}?>"/>
                                       </div>
                                    </div>
                                    <div class="form-group row name_fields p11d" id="<?php echo $this->Common_mdl->get_order_details(133); ?>">
                                       <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(133); ?></label>
                                       <div class="col-sm-8">
                                          <input type="number" name="p11d_todo" id="p11d_todo" placeholder="" value="<?php if(isset($client['crm_p11d_latest_action']) && ($client['crm_p11d_latest_action']!='') ){ echo $client['crm_p11d_latest_action'];}?>" class="fields">
                                       </div>
                                    </div>
                                    <div class="form-group row date_birth p11d" id="<?php echo $this->Common_mdl->get_order_details(134); ?>">
                                       <label class="col-sm-4 col-form-label">First Benefit Pay Date</label>
                                       <div class="col-sm-8">
                                          <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                          <input class="form-control datepicker-13 fields" type="text" name="p11d_first_benefit_date" id="datepicker18" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_latest_p11d_submitted']) && ($client['crm_latest_p11d_submitted']!='') ){ echo $client['crm_latest_p11d_submitted'];}?>"/>
                                       </div>
                                    </div>
                                    <div class="form-group row date_birth p11d" id="<?php echo $this->Common_mdl->get_order_details(135); ?>">
                                       <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(135); ?></label>
                                        <span class="hilight_due_date">*</span>
                                       <div class="col-sm-8">
                                          <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                          <input type="text" name="p11d_due_date" id="p11d_due_date" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_next_p11d_return_due']) && ($client['crm_next_p11d_return_due']!='') ){ echo $client['crm_next_p11d_return_due'];}?>" class="datepicker-13 fields">
                                       </div>
                                    </div>
                                    <div class="form-group row radio_bts p11d" id="<?php echo $this->Common_mdl->get_order_details(136); ?>">
                                       <label class="col-sm-4 col-form-label">Previous Year Requires</label>
                                       <div class="col-sm-8">
                                          <input type="checkbox" class="js-small f-right fields" name="p11d_previous_year_require" id="p11d_previous_year_require" <?php if(isset($client['crm_p11d_previous_year_require']) && ($client['crm_p11d_previous_year_require']=='on') ){?> checked="checked"<?php } ?>>
                                       </div>
                                    </div>
                                    <div class="form-group row name_fields p11d" id="<?php echo $this->Common_mdl->get_order_details(137); ?>">
                                       <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(137); ?></label>
                                       <div class="col-sm-8">
                                          <input type="text" name="p11d_payroll_if_yes" id="p11d_payroll_if_yes" placeholder="How many years" value="<?php if(isset($client['crm_p11d_payroll_if_yes']) && ($client['crm_p11d_payroll_if_yes']!='') ){ echo $client['crm_p11d_payroll_if_yes'];}?>" class="fields">
                                       </div>
                                    </div>
                                    <div class="form-group row  date_birth p11d" id="<?php echo $this->Common_mdl->get_order_details(138); ?>">
                                       <label class="col-sm-4 col-form-label">PAYE Scheme Ceased Date</label>
                                       <div class="col-sm-8">
                                          <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                          <input class="form-control datepicker-13 fields" type="text" name="p11d_paye_scheme_ceased" id="datepicker20" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_p11d_records_received']) && ($client['crm_p11d_records_received']!='') ){ echo $client['crm_p11d_records_received'];}?>"/>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- accordion-panel -->
                        <div class="accordion-panel enable_plld" style="<?php echo $nine;?>">
                           <div class="box-division03">
                              <div class="accordion-heading" role="tab" id="headingOne">
                                 <h3 class="card-title accordion-title">
                                    <a class="accordion-msg">P11D Reminders</a>
                                 </h3>
                              </div>
                              <div id="collapse" class="panel-collapse">
                                 <div class="basic-info-client1" id="p11d_sec1">
                                    <div class="form-group row  date_birth radio_bts">
                                       <label class="col-sm-4 col-form-label"><a id="p11d_add_custom_reminder_link" name="p11d_add_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/11" target="_blank">As per firm setting setting</a></label>
                                       <div class="col-sm-8">
                                          <!-- <input type="hidden" name="p11d_next_reminder_date" id="p11d_next_reminder_date" placeholder="" value="<?php if(isset($client['crm_p11d_records_received']) && ($client['crm_p11d_records_received']!='') ){ echo $client['crm_p11d_records_received'];}?>" class="fields datepicker"> -->
                                          <input type="checkbox" class="js-small f-right fields as_per_firm" name="P11d_next_reminder_date" id="P11d_next_reminder_date"  <?php if(isset($client['crm_p11d_next_reminder_date']) && ($client['crm_p11d_next_reminder_date']!='') ){ echo "checked"; } ?> onchange="check_checkbox7();">
                                          <!--hhhhh-->
                                       </div>
                                    </div>
                                    <!-- <div class="form-group row radio_bts ">
                                       <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                                       <div class="col-sm-8">
                                          <input type="checkbox" class="js-small f-right fields" name="p11d_create_task_reminder" id="p11d_create_task_reminder" <?php if(isset($client['crm_p11d_create_task_reminder']) && ($client['crm_p11d_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> value="on" >
                                       </div>
                                    </div> -->
                                    <?php $p11d_sub_cus_reminder = (!empty($client['crm_p11d_add_custom_reminder'])?$client['crm_p11d_add_custom_reminder']:''); ?>
                                    <div class="form-group row radio_bts p11d_add_custom_reminder_label"  >
                                       <label id="p11d_add_custom_reminder_label" name="p11d_add_custom_reminder_label" class="col-sm-4 col-form-label">Add Custom Reminder</label>
                                       <div class="col-sm-8" id="p11d_add_custom_reminder">
                                          <input type="checkbox" class="js-small f-right fields cus_reminder" name="p11d_add_custom_reminder" id="" data-id="p11d_cus" data-serid="11" value="<?php echo $p11d_sub_cus_reminder;?>" <?php if($p11d_sub_cus_reminder!=''){ ?> checked <?php } ?>  >
                                          <!-- <textarea rows="4" name="p11d_add_custom_reminder" id="p11d_add_custom_reminder" class="form-control fields"><?php if(isset($client['crm_p11d_add_custom_reminder']) && ($client['crm_p11d_add_custom_reminder']!='') ){ echo $client['crm_p11d_add_custom_reminder'];}?></textarea> -->
                                          <!-- <a name="p11d_add_custom_reminder" id="p11d_add_custom_reminder" href="<?php echo base_url();?>user/Service_reminder_settings/12" target="_blank">Click to add Custom Reminder</a> -->
                                       </div>
                                    </div>
                                    <?php $cus_temp = $this->db->query('select id,subject from  custom_service_reminder where client_id='.$client['id'].' and firm_id='.$_SESSION['firm_id'].' and service_id=11')->result_array();
                                       if(isset($cus_temp)){
                                         foreach ($cus_temp as $key => $value) {
                                         
                                        ?>
                                    <div class="form-group row name_fields" style="">
                                       <label class="col-sm-4 col-form-label cus_reminder_subject_<?php echo $value['id']; ?>"><?php echo $value['subject']; ?></label>
                                       <div class="col-sm-8">
                                          <a href="javascript:;" class="date-editop" data-toggle="modal" id="cus_reminder_<?php echo $value['id']; ?>">Edit</a>
                                       </div>
                                    </div>
                                    <?php } } ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- accordion-panel -->
                     </div>
                     <!-- p11dtab close -->
                    
                  
                  </div>
               
               <!-- Payroll -->

                           </li>
                           <!-- payroll tab end -->


 <!-- vat returns tab start -->

<?php

$vatService ="display:none";

if($tab_on['vat']['tab']!==0)
{
  $vatService ="display:block";
}

?>

                           <li class="show-block" style="<?php echo $vatService ?>">
                           <a href="#" class="toggle"> VAT Returns </a>

               
                  <div id="vat-Returns" class="masonry-container floating_set inner-views">
                     <div class="grid-sizer"></div>
                     <div class="accordion-panel">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">Important Information</a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1">
                                 <div class="form-group row name_fields">
                                    <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(40); ?></label>
                                    <div class="col-sm-8">
                                       <input type="number" name="vat_number_one" id="vat_number_one" placeholder="" value="<?php if(isset($client['crm_vat_number'])){ echo $client['crm_vat_number']; } ?>" class="fields">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- accordion-panel -->
                     <div class="accordion-panel">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">VAT</a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1" id="vat_box">
                                 <div class="form-group row  date_birth vat_sort" id="<?php echo $this->Common_mdl->get_order_details(41); ?>">
                                    <label class="col-sm-4 col-form-label">VAT Registration Date</label>
                                    <div class="col-sm-8">
                                       <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                       <input class="form-control datepicker-13 fields" type="text" name="vat_registration_date" id="datepicker21" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_vat_date_of_registration']) && ($client['crm_vat_date_of_registration']!='') ){ echo $client['crm_vat_date_of_registration'];}?>"/>
                                    </div>
                                 </div>
                                 <div class="form-group row vat_sort" id="<?php echo $this->Common_mdl->get_order_details(42); ?>">
                                    <label class="col-sm-4 col-form-label">VAT Frequency</label>
                                    <div class="col-sm-8">
                                       <select name="vat_frequency" id="vat_frequency" class="form-control fields">
                                          <option value="" selected="selected">--Select--</option>
                                          <option value="Monthly" <?php if(isset($client['crm_vat_frequency']) && $client['crm_vat_frequency']=='Monthly') {?> selected="selected"<?php } ?>>Monthly</option>
                                          <option value="quarterly" <?php if(isset($client['crm_vat_frequency']) && $client['crm_vat_frequency']=='Quarterly') {?> selected="selected"<?php } ?>>Quarterly</option>
                                          <option value="Yearly" <?php if(isset($client['crm_vat_frequency']) && $client['crm_vat_frequency']=='Yearly') {?> selected="selected"<?php } ?>>Yearly</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="form-group row help_icon date_birth vat_sort" id="<?php echo $this->Common_mdl->get_order_details(141); ?>">
                                    <label class="col-sm-4 col-form-label">VAT Quarter End Date</label>
                                    <div class="col-sm-8">
                                       <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                       <input class="form-control datepicker-13 fields" name="vat_quater_end_date" id="datepicker22" type="text" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_vat_quater_end_date']) && ($client['crm_vat_quater_end_date']!='') ){ echo $client['crm_vat_quater_end_date'];}?>"/>
                                    </div>
                                 </div>
                                 <div class="form-group row vat_sort" id="<?php echo $this->Common_mdl->get_order_details(142); ?>">
                                    <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(142); ?></label>
                                    <span class="hilight_due_date">*</span>
                                    <div class="col-sm-8">
                                       <select name="vat_quarters" id="vat_quarters" class="form-control fields">
                                          <option value="">--Select--</option>
                                          <option value="Jan/Apr/Jul/Oct" <?php if(isset($client['crm_vat_quarters']) && $client['crm_vat_quarters']=='Jan/Apr/Jul/Oct') {?> selected="selected"<?php } ?>>Jan/Apr/Jul/Oct</option>
                                          <option value="Feb/May/Aug/Nov" <?php if(isset($client['crm_vat_quarters']) && $client['crm_vat_quarters']=='Feb/May/Aug/Nov') {?> selected="selected"<?php } ?>>Feb/May/Aug/Nov</option>
                                          <option value="Mar/Jun/Sep/Dec" <?php if(isset($client['crm_vat_quarters']) && $client['crm_vat_quarters']=='Mar/Jun/Sep/Dec') {?> selected="selected"<?php } ?>>Mar/Jun/Sep/Dec</option>
                                          <option value="Monthly" <?php if(isset($client['crm_vat_quarters']) && $client['crm_vat_quarters']=='Monthly') {?> selected="selected"<?php } ?>>Monthly</option>
                                          <option value="Annual End Jan" <?php if(isset($client['crm_vat_quarters']) && $client['crm_vat_quarters']=='Annual End Jan') {?> selected="selected"<?php } ?>>Annual End Jan</option>
                                          <option value="Annual End Feb" <?php if(isset($client['crm_vat_quarters']) && $client['crm_vat_quarters']=='Annual End Feb') {?> selected="selected"<?php } ?>>Annual End Feb</option>
                                          <option value="Annual End Mar" <?php if(isset($client['crm_vat_quarters']) && $client['crm_vat_quarters']=='Monthly') {?> selected="selected"<?php } ?>>Annual End Mar</option>
                                          <option value="Annual End Apr" <?php if(isset($client['crm_vat_quarters']) && $client['crm_vat_quarters']=='Annual End Apr') {?> selected="selected"<?php } ?>>Annual End Apr</option>
                                          <option value="Annual End May" <?php if(isset($client['crm_vat_quarters']) && $client['crm_vat_quarters']=='Annual End May') {?> selected="selected"<?php } ?>>Annual End May</option>
                                          <option value="Annual End Jun" <?php if(isset($client['crm_vat_quarters']) && $client['crm_vat_quarters']=='Annual End Jun') {?> selected="selected"<?php } ?>>Annual End Jun</option>
                                          <option value="Annual End Jul" <?php if(isset($client['crm_vat_quarters']) && $client['crm_vat_quarters']=='Annual End Jul') {?> selected="selected"<?php } ?>>Annual End Jul</option>
                                          <option value="Annual End Aug" <?php if(isset($client['crm_vat_quarters']) && $client['crm_vat_quarters']=='Annual End Aug') {?> selected="selected"<?php } ?>>Annual End Aug</option>
                                          <option value="Annual End Sep" <?php if(isset($client['crm_vat_quarters']) && $client['crm_vat_quarters']=='Annual End Sep') {?> selected="selected"<?php } ?>>Annual End Sep</option>
                                          <option value="Annual End Oct" <?php if(isset($client['crm_vat_quarters']) && $client['crm_vat_quarters']=='Annual End Oct') {?> selected="selected"<?php } ?>>Annual End Oct</option>
                                          <option value="Annual End Nov" <?php if(isset($client['crm_vat_quarters']) && $client['crm_vat_quarters']=='Annual End Nov') {?> selected="selected"<?php } ?>>Annual End Nov</option>
                                          <option value="Annual End Dec" <?php if(isset($client['crm_vat_quarters']) && $client['crm_vat_quarters']=='Annual End Dec') {?> selected="selected"<?php } ?>>Annual End Dec</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="form-group row help_icon date_birth vat_sort" id="<?php echo $this->Common_mdl->get_order_details(143); ?>">
                                    <label class="col-sm-4 col-form-label">VAT Due Date</label>
                                    <div class="col-sm-8">
                                       <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                       <input class="form-control datepicker-13 fields" name="vat_due_date" id="datepicker24" type="text" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_vat_due_date']) && ($client['crm_vat_due_date']!='') ){ echo $client['crm_vat_due_date'];}?>"/>
                                    </div>
                                 </div>
                                 <div class="form-group row vat_sort" id="<?php echo $this->Common_mdl->get_order_details(144); ?>">
                                    <label class="col-sm-4 col-form-label">VAT Scheme</label>
                                    <div class="col-sm-8">
                                       <select name="vat_scheme" id="vat_scheme" class="form-control fields">
                                          <option value="">--Select--</option>
                                          <option value="Standard Rate" <?php if(isset($client['crm_vat_scheme']) && $client['crm_vat_scheme']=='Standard Rate') {?> selected="selected"<?php } ?>>Standard Rate</option>
                                          <option value="Flat Rate Scheme" <?php if(isset($client['crm_vat_scheme']) && $client['crm_vat_scheme']=='Flat Rate Scheme') {?> selected="selected"<?php } ?>>Flat Rate Scheme</option>
                                          <option value="Marginal Scheme" <?php if(isset($client['crm_vat_scheme']) && $client['crm_vat_scheme']=='Marginal Scheme') {?> selected="selected"<?php } ?>>Marginal Scheme</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="form-group row vat_sort" id="<?php echo $this->Common_mdl->get_order_details(43); ?>">
                                    <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(43); ?></label>
                                    <div class="col-sm-8">
                                       <input type="text" name="flat_rate_category" id="flat_rate_category" placeholder="" value="<?php if(isset($client['crm_flat_rate_category']) && ($client['crm_flat_rate_category']!='') ){ echo $client['crm_flat_rate_category'];}?>" class="fields">
                                    </div>
                                 </div>
                                 <div class="form-group row vat_sort" id="<?php echo $this->Common_mdl->get_order_details(44); ?>">
                                    <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(44); ?></label>
                                    <div class="col-sm-8">
                                       <input type="number" name="flat_rate_percentage" id="flat_rate_percentage" placeholder="" value="<?php if(isset($client['crm_flat_rate_percentage']) && ($client['crm_flat_rate_percentage']!='') ){ echo $client['crm_flat_rate_percentage'];}?>" class="fields">
                                    </div>
                                 </div>
                                 <div class="form-group row name_fields vat_sort radio_bts" id="<?php echo $this->Common_mdl->get_order_details(45); ?>">
                                    <label class="col-sm-4 col-form-label">Direct Debit with HMRC</label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" class="js-small f-right fields" name="direct_debit" id="direct_debit" <?php if(isset($client['crm_direct_debit']) && ($client['crm_direct_debit']=='on') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 <div class="form-group row name_fields vat_sort radio_bts" id="<?php echo $this->Common_mdl->get_order_details(46); ?>">
                                    <label class="col-sm-4 col-form-label">Annual Accounting Scheme</label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" class="js-small f-right fields" name="annual_accounting_scheme" id="annual_accounting_scheme" <?php if(isset($client['crm_annual_accounting_scheme']) && ($client['crm_annual_accounting_scheme']=='on') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 <div class="form-group row vat_sort" id="<?php echo $this->Common_mdl->get_order_details(47); ?>">
                                    <label class="col-sm-4 col-form-label">Box 5 Figure of Last Quarter</label>
                                    <div class="col-sm-8">
                                       <!-- <input type="text" name="box5_of_last_quarter_submitted" id="box5_of_last_quarter_submitted" value="<?php if(isset($client['crm_box5_of_last_quarter_submitted']) && ($client['crm_box5_of_last_quarter_submitted']!='') ){ echo $client['crm_box5_of_last_quarter_submitted'];}?>" class="fields"> -->
                                       <select name="box5_of_last_quarter_submitted" id="box5_of_last_quarter_submitted" class="form-control fields">
                                          <option value="">--Select--</option>
                                          <option value="01" <?php if(isset($client['crm_box5_of_last_quarter_submitted']) && $client['crm_box5_of_last_quarter_submitted']=='01') {?> selected="selected"<?php } ?>>01</option>
                                          <option value="02" <?php if(isset($client['crm_box5_of_last_quarter_submitted']) && $client['crm_box5_of_last_quarter_submitted']=='02') {?> selected="selected"<?php } ?>>02</option>
                                          <option value="03" <?php if(isset($client['crm_box5_of_last_quarter_submitted']) && $client['crm_box5_of_last_quarter_submitted']=='03') {?> selected="selected"<?php } ?>>03</option>
                                          <option value="04" <?php if(isset($client['crm_box5_of_last_quarter_submitted']) && $client['crm_box5_of_last_quarter_submitted']=='04') {?> selected="selected"<?php } ?>>04</option>
                                          <option value="05" <?php if(isset($client['crm_box5_of_last_quarter_submitted']) && $client['crm_box5_of_last_quarter_submitted']=='05') {?> selected="selected"<?php } ?>>05</option>
                                          <option value="06" <?php if(isset($client['crm_box5_of_last_quarter_submitted']) && $client['crm_box5_of_last_quarter_submitted']=='06') {?> selected="selected"<?php } ?>>06</option>
                                          <option value="07" <?php if(isset($client['crm_box5_of_last_quarter_submitted']) && $client['crm_box5_of_last_quarter_submitted']=='07') {?> selected="selected"<?php } ?>>07</option>
                                          <option value="08" <?php if(isset($client['crm_box5_of_last_quarter_submitted']) && $client['crm_box5_of_last_quarter_submitted']=='08') {?> selected="selected"<?php } ?>>08</option>
                                          <option value="09" <?php if(isset($client['crm_box5_of_last_quarter_submitted']) && $client['crm_box5_of_last_quarter_submitted']=='09') {?> selected="selected"<?php } ?>>09</option>
                                          <option value="10" <?php if(isset($client['crm_box5_of_last_quarter_submitted']) && $client['crm_box5_of_last_quarter_submitted']=='10') {?> selected="selected"<?php } ?>>10</option>
                                          <option value="11" <?php if(isset($client['crm_box5_of_last_quarter_submitted']) && $client['crm_box5_of_last_quarter_submitted']=='11') {?> selected="selected"<?php } ?>>11</option>
                                          <option value="12" <?php if(isset($client['crm_box5_of_last_quarter_submitted']) && $client['crm_box5_of_last_quarter_submitted']=='12') {?> selected="selected"<?php } ?>>12</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="form-group row vat_sort" id="<?php echo $this->Common_mdl->get_order_details(48); ?>">
                                    <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(48); ?></label>
                                    <div class="col-sm-8">
                                       <textarea rows="3" placeholder="" class="fields" name="vat_address" id="vat_address"><?php if(isset($client['crm_vat_address']) && ($client['crm_vat_address']!='') ){ echo $client['crm_vat_address'];}?></textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- accordion-panel -->
                     <div class="accordion-panel enable_vat" style="<?php echo $six;?>">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">VAT Reminders</a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1" id="vat_box1">
                                 <div class="form-group row  date_birth radio_bts">
                                    <label class="col-sm-4 col-form-label"><a id="vat_add_custom_reminder_link" name="vat_add_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/5" target="_blank">Tax Return reminder setting</a></label>
                                    <div class="col-sm-8">
                                       <!-- <input type="hidden" name="vat_next_reminder_date" id="vat_next_reminder_date" placeholder="" value="<?php if(isset($client['crm_vat_next_reminder_date']) && ($client['crm_vat_next_reminder_date']!='') ){ echo $client['crm_vat_next_reminder_date'];}?>" class="fields datepicker"> -->
                                       <input type="checkbox" class="js-small f-right fields as_per_firm" name="Vat_next_reminder_date" id="Vat_next_reminder_date"  <?php if(isset($client['crm_vat_next_reminder_date']) && ($client['crm_vat_next_reminder_date']=='on') ){?> checked="checked"<?php } ?> onchange="check_checkbox10();">
                                    </div>
                                 </div>
                                 <!-- <div class="form-group row radio_bts vat_sort" id="<?php echo $this->Common_mdl->get_order_details(145); ?>">
                                    <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" class="js-small f-right fields" name="vat_create_task_reminder" id="vat_create_task_reminder" <?php if(isset($client['crm_vat_create_task_reminder']) && ($client['crm_vat_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> value="on" >
                                    </div>
                                 </div> -->
                                 <?php $vat_cus_reminder = (!empty($client['crm_vat_add_custom_reminder'])?$client['crm_vat_add_custom_reminder']:'');?>

                                 <div class="form-group row radio_bts vat_add_custom_reminder_label  vat_sort" id="<?php echo $this->Common_mdl->get_order_details(146); ?>">
                                    <label id="vat_add_custom_reminder_label" name="vat_add_custom_reminder_label" class="col-sm-4 col-form-label">Add Custom Reminder</label>
                                    <div class="col-sm-8" id="vat_add_custom_reminder">
                                       <input type="checkbox" class="js-small f-right fields cus_reminder" name="vat_add_custom_reminder" id="" data-id="vat_cus" data-serid="5" value="<?php echo $vat_cus_reminder?>" <?php if($vat_cus_reminder!=''){ ?> checked <?php } ?>>
                                       <!-- <textarea rows="3" placeholder="" name="vat_add_custom_reminder" id="vat_add_custom_reminder" class="fields"><?php if(isset($client['crm_vat_add_custom_reminder']) && ($client['crm_vat_add_custom_reminder']!='') ){ echo $client['crm_vat_add_custom_reminder'];}?></textarea> -->
                                       <!-- <a name="vat_add_custom_reminder" id="vat_add_custom_reminder" href="<?php echo base_url();?>user/Service_reminder_settings/15" target="_blank">Click to add Custom Reminder</a> -->
                                    </div>
                                 </div>
                                 <?php $cus_temp = $this->db->query('select id,subject from  custom_service_reminder where client_id='.$client['id'].' and firm_id='.$_SESSION['firm_id'].' and service_id=5')->result_array();
                                    if(isset($cus_temp)){
                                      foreach ($cus_temp as $key => $value) {
                                      
                                     ?>
                                 <div class="form-group row name_fields" style="">
                                    <label class="col-sm-4 col-form-label cus_reminder_subject_<?php echo $value['id']; ?>"><?php echo $value['subject']; ?></label>
                                    <div class="col-sm-8">
                                       <a href="javascript:;" class="date-editop" data-toggle="modal" id="cus_reminder_<?php echo $value['id']; ?>">Edit</a>
                                    </div>
                                 </div>
                                 <?php } } ?>
                              </div>
                           </div>
                        </div>
                     </div>
                   
                  </div>
               
               <!-- VAT Returns -->
</li>
 <!-- vat returns tab end -->
<?php

$managementService ="display:none";

if($tab_on['bookkeep']['tab']!==0 || $tab_on['management']['tab']!==0 )
{
  $managementService ="display:block";
}

?>

<!-- management-a/c tab start -->
                           <li class="show-block" style="<?php echo $managementService?>">
                           <a href="#" class="toggle">  Management Accounts  </a>
  
              
              
                  <div id="management-account" class="masonry-container floating_set inner-views">
                     <div class="grid-sizer"></div>
                     <div class="bookkeeptab" style="<?php echo $bookkeeptab;?>">
                        <div class="accordion-panel">
                           <div class="box-division03">
                              <div class="accordion-heading" role="tab" id="headingOne">
                                 <h3 class="card-title accordion-title">
                                    <a class="accordion-msg">Bookkeeping</a>
                                 </h3>
                              </div>
                              <div id="collapse" class="panel-collapse">
                                 <div class="basic-info-client1" id="management_box1">
                                    <div class="form-group row management" id="<?php echo $this->Common_mdl->get_order_details(49); ?>">
                                       <label class="col-sm-4 col-form-label">Bookkeepeing to Done</label>
                                       <div class="col-sm-8">
                                          <select name="bookkeeping" id="bookkeeping" class="form-control fields">
                                             <option value="">--Select--</option>
                                             <option value="Weekly" <?php if(isset($client['crm_bookkeeping']) && $client['crm_bookkeeping']=='Weekly') {?> selected="selected"<?php } ?>>Weekly</option>
                                             <option value="Monthly" <?php if(isset($client['crm_bookkeeping']) && $client['crm_bookkeeping']=='Monthly') {?> selected="selected"<?php } ?>>Monthly</option>
                                             <option value="Quarterly" <?php if(isset($client['crm_bookkeeping']) && $client['crm_bookkeeping']=='Quarterly') {?> selected="selected"<?php } ?>>Quarterly</option>
                                             <option value="Annually" <?php if(isset($client['crm_bookkeeping']) && $client['crm_bookkeeping']=='Annually') {?> selected="selected"<?php } ?>>Annually</option>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="form-group row help_icon date_birth management" id="<?php echo $this->Common_mdl->get_order_details(50); ?>">
                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(50); ?></label>
                                                            <span class="hilight_due_date">*</span>
                                       <div class="col-sm-8">
                                          <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                          <input class="form-control datepicker-13 fields" name="next_booking_date" id="datepicker26" type="text" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_next_booking_date']) && ($client['crm_next_booking_date']!='') ){ echo $client['crm_next_booking_date'];}?>"/>
                                       </div>
                                    </div>
                                    <div class="form-group row management" id="<?php echo $this->Common_mdl->get_order_details(51); ?>">
                                       <label class="col-sm-4 col-form-label">Method of Bookkeeping</label>
                                       <div class="col-sm-8">
                                          <select name="method_bookkeeping" id="method_bookkeeping" class="form-control fields">
                                             <option value="">--Select--</option>
                                             <option value="Excel" <?php if(isset($client['crm_method_bookkeeping']) && $client['crm_method_bookkeeping']=='Excel') {?> selected="selected"<?php } ?>>Excel</option>
                                             <option value="Software" <?php if(isset($client['crm_method_bookkeeping']) && $client['crm_method_bookkeeping']=='Software') {?> selected="selected"<?php } ?>>Software</option>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="form-group row management" id="<?php echo $this->Common_mdl->get_order_details(52); ?>">
                                       <label class="col-sm-4 col-form-label">How Client will provide records</label>
                                       <div class="col-sm-8">
                                          <select name="client_provide_record" id="client_provide_record" class="form-control fields">
                                             <option value="">--Select--</option>
                                             <option value="Email/DropBox" <?php if(isset($client['crm_client_provide_record']) && $client['crm_client_provide_record']=='Email/DropBox') {?> selected="selected"<?php } ?>>Email/DropBox</option>
                                             <option value="Google/Online Portal" <?php if(isset($client['crm_client_provide_record']) && $client['crm_client_provide_record']=='Google/Online Portal') {?> selected="selected"<?php } ?>>Google/Online Portal</option>
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- accordion-panel -->
                        <div class="accordion-panel enable_book" style="<?php echo $ten;?>">
                           <div class="box-division03">
                              <div class="accordion-heading" role="tab" id="headingOne">
                                 <h3 class="card-title accordion-title">
                                    <a class="accordion-msg">Bookkeep Reminders</a>
                                 </h3>
                              </div>
                              <div id="collapse" class="panel-collapse">
                                 <div class="basic-info-client1" id="management_box2">
                                    <div class="form-group row  date_birth radio_bts">
                                       <label class="col-sm-4 col-form-label"><a id="bookkeep_add_custom_reminder_link" name="bookkeep_add_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/10" target="_blank">As per firm setting</a></label>
                                       <div class="col-sm-8">
                                          <!-- <input type="hidden" name="bookkeep_next_reminder_date" id="bookkeep_next_reminder_date" placeholder="" value="<?php if(isset($client['crm_bookkeep_next_reminder_date']) && ($client['crm_bookkeep_next_reminder_date']!='') ){ echo $client['crm_bookkeep_next_reminder_date'];}?>" class="fields datepicker"> -->
                                          <input type="checkbox" class="js-small f-right fields as_per_firm " name="bookkeep_next_reminder_date" id="bookkeep_next_reminder_date"  <?php if(isset($client['crm_bookkeep_next_reminder_date']) && ($client['crm_bookkeep_next_reminder_date']!='') ){ echo "checked"; } ?> onchange="check_checkbox8();">
                                          <!---iiiii-->
                                       </div>
                                    </div>
                                    <!-- <div class="form-group row radio_bts management" id="<?php echo $this->Common_mdl->get_order_details(147); ?>">
                                       <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                                       <div class="col-sm-8">
                                          <input type="checkbox" class="js-small f-right fields" name="bookkeep_create_task_reminder" id="bookkeep_create_task_reminder" <?php if(isset($client['crm_bookkeep_create_task_reminder']) && ($client['crm_bookkeep_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> value="on" >
                                       </div>
                                    </div> -->
                                    <?php $bookkeep_cus_reminder  = (!empty($client['crm_bookkeep_add_custom_reminder'])?$client['crm_bookkeep_add_custom_reminder']:'');?>
                                    <div class="form-group row radio_bts bookkeep_add_custom_reminder_label management" id="<?php echo $this->Common_mdl->get_order_details(148); ?>"  >
                                       <label id="bookkeep_add_custom_reminder_label" name="bookkeep_add_custom_reminder_label" class="col-sm-4 col-form-label">Add Custom Reminder</label>
                                       <div class="col-sm-8" id="bookkeep_add_custom_reminder">
                                          <input type="checkbox" class="js-small f-right fields cus_reminder" name="bookkeep_add_custom_reminder" id="" data-id="bookkeep_cus" data-serid="10" value="<?php echo $bookkeep_cus_reminder;?>" <?php if($bookkeep_cus_reminder!=''){ ?> checked <?php } ?>>
                                          <!-- <textarea rows="3" placeholder="" name="bookkeep_add_custom_reminder" id="bookkeep_add_custom_reminder" class="fields"><?php if(isset($client['crm_bookkeep_add_custom_reminder']) && ($client['crm_bookkeep_add_custom_reminder']!='') ){ echo $client['crm_bookkeep_add_custom_reminder'];}?></textarea> -->
                                          <!-- <a name="bookkeep_add_custom_reminder" id="bookkeep_add_custom_reminder" href="<?php echo base_url();?>user/Service_reminder_settings/13" target="_blank">Click to add Custom Reminder</a> -->
                                       </div>
                                    </div>
                                    <?php $cus_temp = $this->db->query('select id,subject from  custom_service_reminder where client_id='.$client['id'].' and firm_id='.$_SESSION['firm_id'].' and service_id=10')->result_array();
                                       if(isset($cus_temp)){
                                         foreach ($cus_temp as $key => $value) {
                                         
                                        ?>
                                    <div class="form-group row name_fields" style="">
                                       <label class="col-sm-4 col-form-label cus_reminder_subject_<?php echo $value['id']; ?>"><?php echo $value['subject']; ?></label>
                                       <div class="col-sm-8">
                                          <a href="javascript:;" class="date-editop" data-toggle="modal" id="cus_reminder_<?php echo $value['id']; ?>">Edit</a>
                                       </div>
                                    </div>
                                    <?php } } ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                        
                        <!-- accordion-panel -->
                     </div>
                     <!-- bookkeep tab close-->
                     <div class="accordion-panel for_management_account" style="<?php echo $managementtab;?>">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">Management Accounts</a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1" id="management_account">
                                 <div class="form-group row management" id="<?php echo $this->Common_mdl->get_order_details(53); ?>">
                                    <label class="col-sm-4 col-form-label">Management Accounts Frequency</label>
                                    <div class="col-sm-8">
                                       <select name="manage_acc_fre" id="manage_acc_fre" class="form-control fields">
                                          <option value="">--Select--</option>
                                          <option value="Weekly" <?php if(isset($client['crm_manage_acc_fre']) && $client['crm_manage_acc_fre']=='Weekly') {?> selected="selected"<?php } ?>>Weekly</option>
                                          <option value="Monthly" <?php if(isset($client['crm_manage_acc_fre']) && $client['crm_manage_acc_fre']=='Monthly') {?> selected="selected"<?php } ?>>Monthly</option>
                                          <option value="Quarterly" <?php if(isset($client['crm_manage_acc_fre']) && $client['crm_manage_acc_fre']=='Quarterly') {?> selected="selected"<?php } ?>>Quarterly</option>
                                          <option value="Annually" <?php if(isset($client['crm_manage_acc_fre']) && $client['crm_manage_acc_fre']=='Annually') {?> selected="selected"<?php } ?>>Annually</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="form-group row help_icon date_birth management" id="<?php echo $this->Common_mdl->get_order_details(54); ?>">
                                    <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(54); ?></label>
                                    <span class="hilight_due_date">*</span>
                                    <div class="col-sm-8">
                                       <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                       <input class="form-control datepicker-13 fields" name="next_manage_acc_date" id="datepicker28" type="text" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_next_manage_acc_date']) && ($client['crm_next_manage_acc_date']!='') ){ echo $client['crm_next_manage_acc_date'];}?>"/>
                                    </div>
                                 </div>
                                 <div class="form-group row management" id="<?php echo $this->Common_mdl->get_order_details(55); ?>">
                                    <label class="col-sm-4 col-form-label">Method of Bookkeeping</label>
                                    <div class="col-sm-8">
                                       <select name="manage_method_bookkeeping" id="manage_method_bookkeeping" class="form-control fields">
                                          <option value="">--Select--</option>
                                          <option value="Excel" <?php if(isset($client['crm_manage_method_bookkeeping']) && $client['crm_manage_method_bookkeeping']=='Excel') {?> selected="selected"<?php } ?>>Excel</option>
                                          <option value="Software" <?php if(isset($client['crm_manage_method_bookkeeping']) && $client['crm_manage_method_bookkeeping']=='Software') {?> selected="selected"<?php } ?>>Software</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="form-group row management" id="<?php echo $this->Common_mdl->get_order_details(56); ?>">
                                    <label class="col-sm-4 col-form-label">How Client will provide records</label>
                                    <div class="col-sm-8">
                                       <select name="manage_client_provide_record" id="manage_client_provide_record" class="form-control fields">
                                          <option value="">--Select--</option>
                                          <option value="Email/DropBox" <?php if(isset($client['crm_manage_client_provide_record']) && $client['crm_manage_client_provide_record']=='Email/DropBox') {?> selected="selected"<?php } ?>>Email/DropBox</option>
                                          <option value="Google/Online Portal" <?php if(isset($client['crm_manage_client_provide_record']) && $client['crm_manage_client_provide_record']=='Google/Online Portal') {?> selected="selected"<?php } ?>>Google/Online Portal</option>
                                       </select>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- accordion-panel -->
                     <div class="accordion-panel enable_management" style="<?php echo $eleven;?>">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">Management Reminders</a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1" id="management_account1">
                                 <div class="form-group row  date_birth radio_bts">
                                    <label class="col-sm-4 col-form-label"><a id="manage_add_custom_reminder_link" name="manage_add_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/12" target="_blank">As per firm setting</a></label>
                                    <div class="col-sm-8">
                                     <!--   <input type="hidden" name="manage_next_reminder_date" id="manage_next_reminder_date" placeholder="" value="<?php if(isset($client['crm_manage_next_reminder_date']) && ($client['crm_manage_next_reminder_date']!='') ){ echo $client['crm_manage_next_reminder_date'];}?>" class="fields datepicker-13"> -->
                                       <input type="checkbox" class="js-small f-right fields as_per_firm" name="manage_next_reminder_date" id="manage_next_reminder_date" <?php if(isset($client['crm_manage_next_reminder_date']) && ($client['crm_manage_next_reminder_date']!='') ){ echo "checked"; } ?> onchange="check_checkbox9();">
                                       <!--jjjjj-->
                                    </div>
                                 </div>
                                 <!-- <div class="form-group row radio_bts management" id="<?php echo $this->Common_mdl->get_order_details(147); ?>">
                                    <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" class="js-small f-right fields" name="manage_create_task_reminder" id="manage_create_task_reminder" <?php if(isset($client['crm_manage_create_task_reminder']) && ($client['crm_manage_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> value="on">
                                    </div>
                                 </div> -->
                                 <?php $management_cus_reminder = (!empty($client['crm_manage_add_custom_reminder'])?$client['crm_manage_add_custom_reminder']:''); ?>
                                 <div class="form-group row radio_bts manage_add_custom_reminder_label  management" id="<?php echo $this->Common_mdl->get_order_details(148); ?>">
                                    <label id="manage_add_custom_reminder_label" name="manage_add_custom_reminder_label" class="col-sm-4 col-form-label">Add Custom Reminder</label>
                                    <div class="col-sm-8" id="manage_add_custom_reminder">
                                       <input type="checkbox" class="js-small f-right fields cus_reminder" name="manage_add_custom_reminder" id="" data-id="manage_cus" data-serid="12" value="<?php echo $management_cus_reminder?>"  <?php if($management_cus_reminder!=''){ ?> checked <?php } ?> >
                                       <!-- <textarea rows="3" placeholder="" name="manage_add_custom_reminder" id="manage_add_custom_reminder" class="fields"><?php if(isset($client['crm_manage_add_custom_reminder']) && ($client['crm_manage_add_custom_reminder']!='') ){ echo $client['crm_manage_add_custom_reminder'];}?></textarea> -->
                                       <!-- <a name="manage_add_custom_reminder" id="manage_add_custom_reminder" href="<?php echo base_url();?>user/Service_reminder_settings/14" target="_blank">Click to add Custom Reminder</a> -->
                                    </div>
                                 </div>
                                 <?php $cus_temp = $this->db->query('select id,subject from  custom_service_reminder where client_id='.$client['id'].' and firm_id='.$_SESSION['firm_id'].' and service_id=12')->result_array();
                                    if(isset($cus_temp)){
                                      foreach ($cus_temp as $key => $value) {
                                      
                                     ?>
                                 <div class="form-group row name_fields" style="">
                                    <label class="col-sm-4 col-form-label cus_reminder_subject_<?php echo $value['id']; ?>"><?php echo $value['subject']; ?></label>
                                    <div class="col-sm-8">
                                       <a href="javascript:;" class="date-editop" data-toggle="modal" id="cus_reminder_<?php echo $value['id']; ?>">Edit</a>
                                    </div>
                                 </div>
                                 <?php } } ?>
                              </div>
                           </div>
                        </div>
                     </div>
                    
                     <!-- accordion-panel -->
                  </div>
               
               <!-- Management Accounts -->

                            </li>
<!-- management-a/c tab end -->
<?php

$investigateInsService ="display:none";

if($tab_on['investgate']['tab']!==0 || $tab_on['registered']['tab']!==0 || $tab_on['taxadvice']['tab']!==0 )
{
  $investigateInsService ="display:block";
}

?>
<!-- investigation tab start -->
                           <li class="show-block" style="<?php echo $investigateInsService ?>">
                              <a href="#" class="toggle"> Investigation Insurance</a>

                  <div  id="investigation-insurance" class="masonry-container floating_set inner-views">
                     <div class="grid-sizer"></div>
                     <div class="accordion-panel">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">Investigation Insurance       </a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1" id="invest-box">
                                 <div class="form-group row help_icon date_birth invest" id="<?php echo $this->Common_mdl->get_order_details(58); ?>">
                                    <label class="col-sm-4 col-form-label">Insurance Start Date</label>
                                    <div class="col-sm-8">
                                       <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                       <input class="form-control datepicker-13 fields" name="insurance_start_date" id="datepicker30" type="text" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_invesitgation_insurance']) && ($client['crm_invesitgation_insurance']!='') ){ echo $client['crm_invesitgation_insurance'];}?>"/>
                                    </div>
                                 </div>
                                 <div class="form-group row help_icon date_birth invest" id="<?php echo $this->Common_mdl->get_order_details(59); ?>">
                                        <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(59); ?></label>
                                                         <span class="hilight_due_date">*</span>                                    <div class="col-sm-8">
                                       <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                       <input class="form-control datepicker-13 fields" name="insurance_renew_date" id="datepicker31" type="text" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_insurance_renew_date']) && ($client['crm_insurance_renew_date']!='') ){ echo $client['crm_insurance_renew_date'];}?>"/>
                                    </div>
                                 </div>
                                 <div class="form-group row invest" id="<?php echo $this->Common_mdl->get_order_details(60); ?>">
                                    <label class="col-sm-4 col-form-label">Insurance Provider</label>
                                    <div class="col-sm-8">
                                       <select name="insurance_provider" id="insurance_provider" class="form-control fields">
                                          <option value="">--Select--</option>
                                          <option value="Excel" <?php if(isset($client['crm_insurance_provider']) && $client['crm_insurance_provider']=='Excel') {?> selected="selected"<?php } ?>>Excel</option>
                                          <option value="Software" <?php if(isset($client['crm_insurance_provider']) && $client['crm_insurance_provider']=='Software') {?> selected="selected"<?php } ?>>Software</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="form-group row invest" id="<?php echo $this->Common_mdl->get_order_details(140); ?>">
                                    <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(140); ?></label>
                                    <div class="col-sm-8">
                                       <textarea rows="3" placeholder="" name="claims_note" id="claims_note" class="fields"><?php if(isset($client['crm_claims_note']) && ($client['crm_claims_note']!='') ){ echo $client['crm_claims_note'];}?></textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- accordion-panel -->
                     <div class="accordion-panel enable_invest" style="<?php echo $twelve;?>">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">Investigation Insurance Reminders</a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1"  id="invest_box1">
                                 <div class="form-group row  date_birth radio_bts">
                                    <label class="col-sm-4 col-form-label"><a id="insurance_add_custom_reminder_link" name="insurance_add_custom_reminder_link" href="<?php echo base_url(); ?>user/Service_reminder_settings/13" target="_blank">As per firm setting</a></label>
                                    <div class="col-sm-8">
                                       <!-- <input type="hidden" name="insurance_next_reminder_date" id="insurance_next_reminder_date" placeholder="" value="<?php if(isset($client['crm_insurance_next_reminder_date']) && ($client['crm_insurance_next_reminder_date']!='') ){ echo $client['crm_insurance_next_reminder_date'];}?>" class="fields datepicker"> -->
                                       <input type="checkbox" class="js-small f-right fields as_per_firm" name="insurance_next_reminder_date" id="insurance_next_reminder_date"   <?php if(isset($client['crm_insurance_next_reminder_date']) && ($client['crm_insurance_next_reminder_date']!='') ){ echo "checked"; } ?> onchange="check_checkbox13();">
                                    </div>
                                 </div>
                                 <!-- <div class="form-group row radio_bts thirteent" style="<?php //echo $thirteent; ?>">
                                    <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" class="js-small f-right fields" name="insurance_create_task_reminder" id="insurance_create_task_reminder" <?php if(isset($client['crm_insurance_create_task_reminder']) && ($client['crm_insurance_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> value="on" >
                                    </div>
                                 </div> -->
                                 <?php $invs_ins_cus_reminder = (!empty($client['crm_insurance_add_custom_reminder'])?$client['crm_insurance_add_custom_reminder']:'');?>
                                 <div class="form-group row radio_bts insurance_add_custom_reminder_label">
                                    <label class="col-sm-4 col-form-label" name="insurance_add_custom_reminder_label" id="insurance_add_custom_reminder_label">Add Custom Reminder</label>
                                    <div class="col-sm-8" id="insurance_add_custom_reminder">
                                       <input type="checkbox" class="js-small f-right fields cus_reminder" name="insurance_add_custom_reminder" id="" data-id="insurance_cus" data-serid="13" value="<?php echo $invs_ins_cus_reminder; ?>" <?php if($invs_ins_cus_reminder!=''){ ?> checked <?php } ?>>
                                       <!-- <textarea rows="3" placeholder="" name="insurance_add_custom_reminder" id="insurance_add_custom_reminder" class="fields"><?php if(isset($client['crm_insurance_add_custom_reminder']) && ($client['crm_insurance_add_custom_reminder']!='') ){ echo $client['crm_insurance_add_custom_reminder'];}?></textarea> -->
                                       <!-- <a name="insurance_add_custom_reminder" id="insurance_add_custom_reminder" href="<?php echo base_url(); ?>user/Service_reminder_settings/9" target="_blank">Click to add Custom Reminder</a> -->
                                    </div>
                                 </div>
                                 <?php $cus_temp = $this->db->query('select id,subject from  custom_service_reminder where client_id='.$client['id'].' and firm_id='.$_SESSION['firm_id'].' and service_id=13')->result_array();
                                    if(isset($cus_temp)){
                                      foreach ($cus_temp as $key => $value) {
                                      
                                     ?>
                                 <div class="form-group row name_fields" style="">
                                    <label class="col-sm-4 col-form-label cus_reminder_subject_<?php echo $value['id']; ?>"><?php echo $value['subject']; ?></label>
                                    <div class="col-sm-8">
                                       <a href="javascript:;" class="date-editop" data-toggle="modal" id="cus_reminder_<?php echo $value['id']; ?>">Edit</a>
                                    </div>
                                 </div>
                                 <?php } } ?>
                              </div>
                           </div>
                        </div>
                     </div>
                    
                     <!-- accordion-panel -->
                     <div class="registeredtab" style="<?php echo $registeredtab;?>">
                        <div class="accordion-panel">
                           <div class="box-division03">
                              <div class="accordion-heading" role="tab" id="headingOne">
                                 <h3 class="card-title accordion-title">
                                    <a class="accordion-msg">Registered Office</a>
                                 </h3>
                              </div>
                              <div id="collapse" class="panel-collapse">
                                 <div class="basic-info-client1" id="invest_box2">
                                    <div class="form-group row help_icon date_birth invest" id="<?php echo $this->Common_mdl->get_order_details(62); ?>">
                                       <label class="col-sm-4 col-form-label">Registered Office Start Date</label>
                                       <div class="col-sm-8">
                                          <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                          <input class="form-control datepicker-13 fields" name="registered_start_date" id="datepicker33" type="text" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_registered_start_date']) && ($client['crm_registered_start_date']!='') ){ echo $client['crm_registered_start_date'];}?>"/>
                                       </div>
                                    </div>
                                    <div class="form-group row help_icon date_birth invest" id="<?php echo $this->Common_mdl->get_order_details(63); ?>">
                                       <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(63); ?></label>
                                                            <span class="hilight_due_date">*</span>
                                       <div class="col-sm-8">
                                          <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                          <input class="form-control datepicker-13 fields" name="registered_renew_date" id="datepicker34" type="text" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_registered_renew_date']) && ($client['crm_registered_renew_date']!='') ){ echo $client['crm_registered_renew_date'];}?>"/>
                                       </div>
                                    </div>
                                    <div class="form-group row invest" id="<?php echo $this->Common_mdl->get_order_details(64); ?>">
                                       <label class="col-sm-4 col-form-label">Registered office in Use</label>
                                       <div class="col-sm-8">
                                          <input type="text" name="registered_office_inuse" id="registered_office_inuse" placeholder="" value="<?php if(isset($client['crm_registered_office_inuse']) && ($client['crm_registered_office_inuse']!='') ){ echo $client['crm_registered_office_inuse'];}?>" class="fields">
                                       </div>
                                    </div>
                                    <div class="form-group row invest" id="<?php echo $this->Common_mdl->get_order_details(65); ?>">
                                       <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(65); ?></label>
                                       <div class="col-sm-8">
                                          <textarea rows="3" placeholder="" name="registered_claims_note" id="registered_claims_note" class="fields"><?php if(isset($client['crm_registered_claims_note']) && ($client['crm_registered_claims_note']!='') ){ echo $client['crm_registered_claims_note'];}?></textarea>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- accordion-panel -->
                        <div class="accordion-panel enable_reg" style="<?php echo $thirteen;?>">
                           <div class="box-division03">
                              <div class="accordion-heading" role="tab" id="headingOne">
                                 <h3 class="card-title accordion-title">
                                    <a class="accordion-msg">Registered Reminders</a>
                                 </h3>
                              </div>
                              <div id="collapse" class="panel-collapse">
                                 <div class="basic-info-client1">
                                    <div class="form-group row  date_birth radio_bts">
                                       <label class="col-sm-4 col-form-label"><a id="registered_add_custom_reminder_link" name="registered_add_custom_reminder_link" href="<?php echo base_url(); ?>user/Service_reminder_settings/14" target="_blank">As per firm setting</a></label>
                                       <div class="col-sm-8">
                                          <!--  <input type="hidden" name="registered_next_reminder_date" id="registered_next_reminder_date" placeholder="" value="<?php if(isset($client['crm_registered_next_reminder_date']) && ($client['crm_registered_next_reminder_date']!='') ){ echo $client['crm_registered_next_reminder_date'];}?>" class="fields datepicker"> -->
                                          <input type="checkbox" class="js-small f-right fields as_per_firm" name="registered_next_reminder_date" id="registered_next_reminder_date" <?php if(isset($client['crm_registered_next_reminder_date']) && ($client['crm_registered_next_reminder_date']!='') ){ echo "checked"; } ?> onchange="check_checkbox14();">
                                       </div>
                                    </div>
                                    <!-- <div class="form-group row radio_bts ">
                                       <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                                       <div class="col-sm-8">
                                          <input type="checkbox" class="js-small f-right fields" name="registered_create_task_reminder" id="registered_create_task_reminder" <?php if(isset($client['crm_registered_create_task_reminder'])  ){?> checked="checked"<?php } ?> value="on" >
                                       </div>
                                    </div> -->
                                    <?php $res_cus_reminder = (!empty($client['crm_registered_add_custom_reminder'])?$client['crm_registered_add_custom_reminder']:'');?>
                                    <div class="form-group row radio_bts registered_add_custom_reminder_label">
                                       <label class="col-sm-4 col-form-label" id="registered_add_custom_reminder_label" name="registered_add_custom_reminder_label">Add Custom Reminder</label>
                                       <div class="col-sm-8" id="registered_add_custom_reminder">
                                          <input type="checkbox" class="js-small f-right fields cus_reminder" name="registered_add_custom_reminder" id="" data-id="registered_cus" data-serid="14" value="<?php  echo $res_cus_reminder;?>" <?php if($res_cus_reminder!=''){ ?> checked <?php } ?>  >
                                          <!-- <textarea rows="3" placeholder="" name="registered_add_custom_reminder" id="registered_add_custom_reminder" class="fields"><?php if(isset($client['crm_registered_add_custom_reminder']) && ($client['crm_registered_add_custom_reminder']!='') ){ echo $client['crm_registered_add_custom_reminder'];}?></textarea> -->
                                          <!-- <a name="registered_add_custom_reminder" id="registered_add_custom_reminder" href="<?php echo base_url(); ?>user/Service_reminder_settings/9" target="_blank">Click to add Custom Reminder</a> -->
                                       </div>
                                    </div>
                                    <?php $cus_temp = $this->db->query('select id,subject from  custom_service_reminder where client_id='.$client['id'].' and firm_id='.$_SESSION['firm_id'].' and service_id=14')->result_array();
                                       if(isset($cus_temp)){
                                         foreach ($cus_temp as $key => $value) {
                                         
                                        ?>
                                    <div class="form-group row name_fields" style="">
                                       <label class="col-sm-4 col-form-label cus_reminder_subject_<?php echo $value['id']; ?>"><?php echo $value['subject']; ?></label>
                                       <div class="col-sm-8">
                                          <a href="javascript:;" class="date-editop" data-toggle="modal" id="cus_reminder_<?php echo $value['id']; ?>">Edit</a>
                                       </div>
                                    </div>
                                    <?php } } ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- accordion-panel -->
                       
                     </div>
                     <!-- registeredtab close-->
                     <div class="taxadvicetab" style="<?php echo $taxadvicetab;?>">
                        <div class="accordion-panel">
                           <div class="box-division03">
                              <div class="accordion-heading" role="tab" id="headingOne">
                                 <h3 class="card-title accordion-title">
                                    <a class="accordion-msg">Tax Advice/Investigation</a>
                                 </h3>
                              </div>
                              <div id="collapse" class="panel-collapse">
                                 <div class="basic-info-client1">
                                    <div class="form-group row help_icon date_birth">
                                       <label class="col-sm-4 col-form-label">Start Date</label>
                                       <div class="col-sm-8">
                                          <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                          <input class="form-control datepicker-13 fields" name="investigation_start_date" id="datepicker36" type="text" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_investigation_start_date']) && ($client['crm_investigation_start_date']!='') ){ echo $client['crm_investigation_start_date'];}?>"/>
                                       </div>
                                    </div>
                                    <div class="form-group row help_icon date_birth">
                                       <label class="col-sm-4 col-form-label">End Date</label>
                                       <span class="hilight_due_date">*</span>
                                       <div class="col-sm-8">
                                          <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                          <input class="form-control datepicker-13 fields" name="investigation_end_date" id="datepicker37" type="text" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_investigation_end_date']) && ($client['crm_investigation_end_date']!='') ){ echo $client['crm_investigation_end_date'];}?>"/>
                                       </div>
                                    </div>
                                    <div class="form-group row">
                                       <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(69); ?></label>
                                       <div class="col-sm-8">
                                          <textarea rows="3" placeholder="" name="investigation_note" id="investigation_note" class="fields"><?php if(isset($client['crm_investigation_note']) && ($client['crm_investigation_note']!='') ){ echo $client['crm_investigation_note'];}?></textarea>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- accordion-panel -->
                        <div class="accordion-panel enable_taxad" style="<?php echo $fourteen; ?>">
                           <div class="box-division03">
                              <div class="accordion-heading" role="tab" id="headingOne">
                                 <h3 class="card-title accordion-title">
                                    <a class="accordion-msg">Tax advice/investigation Reminders</a>
                                 </h3>
                              </div>
                              <div id="collapse" class="panel-collapse">
                                 <div class="basic-info-client1">
                                    <div class="form-group row  date_birth radio_bts">
                                       <label class="col-sm-4 col-form-label"><a id="investigation_add_custom_reminder_link" name="investigation_add_custom_reminder_link" href="<?php echo base_url(); ?>user/Service_reminder_settings/15" target="_blank">As per firm setting</a></label>
                                       <div class="col-sm-8">
                                          <!-- <input type="hidden" name="investigation_next_reminder_date" id="investigation_next_reminder_date" placeholder="" value="<?php if(isset($client['crm_investigation_next_reminder_date']) && ($client['crm_investigation_next_reminder_date']!='') ){ echo $client['crm_investigation_next_reminder_date'];}?>" class="fields datepicker"> -->
                                          <input type="checkbox" class="js-small f-right fields as_per_firm " name="investigation_next_reminder_date" id="investigation_next_reminder_date"  <?php if(isset($client['crm_investigation_next_reminder_date']) && ($client['crm_investigation_next_reminder_date']!='') ){ echo "checked"; } ?> onchange="check_checkbox15();">
                                       </div>
                                    </div>
                                    <!-- <div class="form-group row radio_bts ">
                                       <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                                       <div class="col-sm-8">
                                          <input type="checkbox" class="js-small f-right fields" name="investigation_create_task_reminder" id="investigation_create_task_reminder" <?php if(isset($client['crm_investigation_create_task_reminder']) && ($client['crm_investigation_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> value="on" >
                                       </div>
                                    </div> -->
                                    <?php $investigation_cus_reminder = (!empty($client['crm_investigation_add_custom_reminder'])?$client['crm_investigation_add_custom_reminder']:''); ?>
                                    <div class="form-group row radio_bts investigation_add_custom_reminder_label">
                                       <label class="col-sm-4 col-form-label" id="investigation_add_custom_reminder_label" name="investigation_add_custom_reminder_label">Add Custom Reminder</label>
                                       <div class="col-sm-8" id="investigation_add_custom_reminder">
                                          <input type="checkbox" class="js-small f-right fields cus_reminder" name="investigation_add_custom_reminder" id="" data-id="investigation_cus" data-serid="15" value="<?php echo $investigation_cus_reminder;?>" <?php if($investigation_cus_reminder!=''){ ?> checked <?php } ?> >
                                          <!--  <textarea rows="3" placeholder="" name="investigation_add_custom_reminder" id="investigation_add_custom_reminder" class="fields"><?php if(isset($client['crm_investigation_add_custom_reminder']) && ($client['crm_investigation_add_custom_reminder']!='') ){ echo $client['crm_investigation_add_custom_reminder'];}?></textarea> -->
                                          <!-- <a name="investigation_add_custom_reminder" id="investigation_add_custom_reminder" href="<?php echo base_url(); ?>user/Service_reminder_settings/9" target="_blank">Click to add Custom Reminder</a> -->
                                       </div>
                                    </div>
                                    <?php $cus_temp = $this->db->query('select id,subject from  custom_service_reminder where client_id='.$client['id'].' and firm_id='.$_SESSION['firm_id'].' and service_id=15')->result_array();
                                       if(isset($cus_temp)){
                                         foreach ($cus_temp as $key => $value) {
                                         
                                        ?>
                                    <div class="form-group row name_fields" style="">
                                       <label class="col-sm-4 col-form-label cus_reminder_subject_<?php echo $value['id']; ?>"><?php echo $value['subject']; ?></label>
                                       <div class="col-sm-8">
                                          <a href="javascript:;" class="date-editop" data-toggle="modal" id="cus_reminder_<?php echo $value['id']; ?>">Edit</a>
                                       </div>
                                    </div>
                                    <?php } } ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- accordion-panel -->
                     </div>
                     <!-- taxadvicetab-->
                     
                    
                     <!-- accordion-panel -->
                     <!-- <div class="floation_set text-right accordion_ups">
                        <input type="submit" class="add_acc_client" value="Save & Exit" id="submit"/>
                        </div> -->
                  </div>
            
               <!-- Investigation Insurance --> 
                              </li>
<!-- investigation tab end -->
  </ul>
                           </div>  
 <!-- import_tab close -->
  


               <!-- Filing Allocation -->
                                    <div id="filing-allocation" class="tab-pane fade ">
                                       
                                       <div class="masonry-container floating_set ">
                                          <div class="grid-sizer"></div>
                                          <!-- accordion-panel -->
                                          <div class="accordion-panel">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Filing Allocation</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1">
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Company Number</label>
                                                         <div class="col-sm-8">
                                                            <input type="text" class="fields" name="fa_cmyno" id="fa_cmyno" value="<?php if(isset($client_legalform['company_no'])){ echo $client_legalform['company_no']; } ?>">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts date_birth">
                                                         <label class="col-sm-4 col-form-label">Incorporation Date</label>
                                                         <div class="col-sm-8">
                                                            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                            <input class="form-control fields dob_picker" placeholder="dd-mm-yyyy" type="text" name="fa_incordate" id="fa_incordate" value="<?php if(isset($client_legalform['incorporation_date'])){ echo $client_legalform['incorporation_date']; } ?>"/>
                                                         </div>
                                                      </div>   
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Registered Address</label>
                                                         <div class="col-sm-8">
                                                            <textarea rows="3" placeholder="" name="fa_registadres" id="fa_registadres" class="fields"><?php if(isset($client_legalform['regist_address'])){ echo $client_legalform['regist_address']; } ?></textarea>
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Turn Over</label>
                                                         <div class="col-sm-8">
                                                            <input type="text" class="fields" name="fa_turnover" id="fa_turnover" value="<?php if(isset($client_legalform['turnover'])){ echo $client_legalform['turnover']; } ?>">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row assign_cus_type date_birth">
                                                         <label class="col-sm-4 col-form-label">Date Of Trading</label>
                                                         <div class="col-sm-8">
                                                            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                            <input class="form-control fields dob_picker" placeholder="dd-mm-yyyy" type="text" name="fa_dateoftrading" id="fa_dateoftrading" value="<?php if(isset($client_legalform['date_of_trading'])){ echo $client_legalform['date_of_trading']; } ?>"/>
                                                         </div>
                                                      </div>
                                                      <div class="form-group row name_fields spanassign" >
                                                         <label class="col-sm-4 col-form-label">SIC Code</label>
                                                         <div class="col-sm-8">
                                                            <input type="text" name="fa_siccode" id="fa_siccode" placeholder="" class="fields" value="<?php if(isset($client_legalform['sic_code'])){ echo $client_legalform['sic_code']; } ?>">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Nuture Of Business</label>
                                                         <div class="col-sm-8">
                                                            <input type="text" class="fields" name="fa_nutureofbus" id="fa_nutureofbus" value="<?php if(isset($client_legalform['nuture_of_business'])){ echo $client_legalform['nuture_of_business']; } ?>">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Company UTR</label>
                                                         <div class="col-sm-8">
                                                            <input type="text" class="fields" name="fa_cmyutr" id="fa_cmyutr" value="<?php if(isset($client_legalform['company_utr'])){ echo $client_legalform['company_utr']; } ?>">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Companies House Authorisation Code</label>
                                                         <div class="col-sm-8">
                                                            <input type="text" class="fields" name="fa_cmyhouse" id="fa_cmyhouse" value="<?php if(isset($client_legalform['company_house_auth_code'])){ echo $client_legalform['company_house_auth_code']; } ?>">
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- accordion-panel -->   
                                          
                                       </div>
                                    </div>
                                    <!-- Filing Allocation -->

                                 <!-- Business Details -->
                                    <div id="business-det" class="tab-pane fade ">
                                       <div class="masonry-container floating_set ">
                                          <div class="grid-sizer"></div>
                                          <!-- accordion-panel -->
                                          <div class="accordion-panel">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Business Details</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1">
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Trading As</label>
                                                         <div class="col-sm-8">
                                                            <input type="text" class="fields" name="bus_tradingas" id="bus_tradingas" value="<?php if(isset($client_legalform['trading_as'])){ echo $client_legalform['trading_as']; } ?>">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts date_birth">
                                                         <label class="col-sm-4 col-form-label">Commenced Trading</label>
                                                         <div class="col-sm-8">
                                                            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                            <input class="form-control fields dob_picker" placeholder="dd-mm-yyyy" type="text" name="bus_commencedtrading" id="bus_commencedtrading" value="<?php if(isset($client_legalform['commenced_trading'])){ echo $client_legalform['commenced_trading']; } ?>"/>
                                                         </div>
                                                      </div>   
                                                      <div class="form-group row radio_bts date_birth">
                                                         <label class="col-sm-4 col-form-label">Registered for SA</label>
                                                         <div class="col-sm-8">
                                                            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                            <input class="form-control fields dob_picker" placeholder="dd-mm-yyyy" type="text" name="bus_regist" id="bus_regist" value="<?php if(isset($client_legalform['register_sa'])){ echo $client_legalform['register_sa']; } ?>"/>
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Turn Over</label>
                                                         <div class="col-sm-8">
                                                            <input type="text" class="fields" name="bus_turnover" id="bus_turnover" value="<?php if(isset($client_legalform['bus_turnover'])){ echo $client_legalform['bus_turnover']; } ?>">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Nuture Of Business</label>
                                                         <div class="col-sm-8">
                                                            <input type="text" class="fields" name="bus_nutureofbus" id="bus_nutureofbus" value="<?php if(isset($client_legalform['bus_nuture_of_business'])){ echo $client_legalform['bus_nuture_of_business']; } ?>">
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- accordion-panel -->   
                                          
                                       </div>
                                    </div>











              
               
              
                 
               <!-- aml checks -->
               <div id="amlchecks" class="tab-pane fade">
                  <div class="space-required">
                    <div class="main-pane-border1" id="amlchecks_error">
                       <div class="alert-ss"></div>
                    </div>
                 </div>                  
                  <div class="masonry-container floating_set">
                     <div class="grid-sizer"></div>
                    
                     
                     <div class="accordion-panel">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">Anti Money Laundering Checks    </a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1">
                                 <div class="form-group row radio_bts ">
                                    <label class="col-sm-4 col-form-label">Client ID Verified</label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" class="js-small f-right fields" name="client_id_verified" id="client_id_verified" <?php if(isset($client['crm_assign_client_id_verified']) && ($client['crm_assign_client_id_verified']!=='') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 <div class="form-group row assign_cus_type client_id_verified_data " <?php if(isset($client['crm_assign_client_id_verified']) && ($client['crm_assign_client_id_verified']!==0) ){ }else{ ?> style="display: none;" <?php } ?> >
                                    <label class="col-sm-4 col-form-label">Type of ID Provided</label>
                                    <div class="col-sm-8">
                                     <div class="dropdown-sin-11 lead-form-st">
                                       <select name="type_of_id[]" id="person" class="form-control fields assign_cus" placeholder="Select" multiple="multiple">
                                          <option value="" disabled="disabled">--Select--</option>
                                          <?php
                                          $new_arr=array();
                                          $all_val=(isset($client['crm_assign_type_of_id']) && $client['crm_assign_type_of_id']!='')? explode(',',$client['crm_assign_type_of_id']):$new_arr;
                                           ?>
                                          <option value="Passport" <?php if(in_array('Passport',$all_val)) {?> selected="selected"<?php } ?>>Passport</option>
                                          <option value="Driving_License" <?php if(in_array('Driving_License',$all_val)) {?> selected="selected"<?php } ?>>Driving License</option>
                                          <option value="Other_Custom" <?php if(in_array('Other_Custom',$all_val)) {?> selected="selected"<?php } ?>>Other Custom</option>
                                       </select>
                                       </div>
                                    </div>
                                 </div>
                                <!-- 29-08-2018 for multiple image upload option -->
                                       <div class="form-group row" >
                                             <label class="col-sm-4 col-form-label">Attachment</label>
                                             <div class="col-sm-8">
											 <div class="custom_upload">
                                                <label for="proof_attach_file"></label><input type="file" name="proof_attach_file[]" id="proof_attach_file" multiple="multiple" >
												</div>
                                             </div>
                                      
                                       <div class="attach-files showtm f-right">
                                       <?php 
                                       if(isset($client['proof_attach_file'])){
                                          ?>
                                       <div class="jFiler-items jFiler-row">
                                           <ul class="jFiler-items-list jFiler-items-grid">
                                          <?php
                                          $ex_attach=array_filter(explode(',', $client['proof_attach_file']));
                                       foreach ($ex_attach as $attach_key => $attach_value) {
                                             $replace_val=str_replace(base_url(),'',$attach_value);
                                             $ext = explode(".", $replace_val);
                                           
                                             $res=$this->Common_mdl->geturl_image_or_not($ext[1]);
                                            // echo $res;
                                             if($res=='image'){
                                                ?>
                                             <li class="jFiler-item for_img_<?php echo $attach_key; ?>" data-jfiler-index="3" style="">
                                               <input type="hidden" name="already_upload_img[]" value="<?php echo $attach_value; ?>" >
                                                 <div class="jFiler-item-container">
                                                    <div class="jFiler-item-inner">
                                                       <div class="jFiler-item-thumb">
                                                          <div class="jFiler-item-status"></div>
                                                          <div class="jFiler-item-info">                                        
                                                                                         
                                                          </div>
                                                          <div class="jFiler-item-thumb-image"><img src="<?php echo $attach_value;?>" draggable="false"></div>
                                                       
                                                    </div>
                                                    <div class="jFiler-item-assets jFiler-row">
                                                          <ul class="list-inline pull-left">
                                                             <li>
                                                                <div class="jFiler-jProgressBar" style="display: none;">
                                                                   <div class="bar"></div>
                                                                </div>
                                                               
                                                             </li>
                                                          </ul>
                                                          <ul class="list-inline pull-right">
                                                             <li><a class="icon-jfi-trash jFiler-item-trash-action remove_file" data-id="<?php echo $attach_key ?>"></a></li>
                                                          </ul>
                                                       </div>
                                                 </div>
                                              </li>
                                                <?php
                                                } // if image
                                                else{ ?>
                                              <li class="jFiler-item jFiler-no-thumbnail for_img_<?php echo $attach_key; ?>" data-jfiler-index="2" style="">
                                               <input type="hidden" name="already_upload_img[]" value="<?php echo $attach_value; ?>" >
                                                 <div class="jFiler-item-container">
                                                    <div class="jFiler-item-inner">
                                                       <div class="jFiler-item-thumb">
                                                          <div class="jFiler-item-status"></div>
                                                          <a href="<?php echo $attach_value; ?>" target="_blank" ><div class="jFiler-item-info">                                                               </div></a>
                                                          <div class="jFiler-item-thumb-image"><span class="jFiler-icon-file f-file f-file-ext-odt" style="background-color: rgb(63, 79, 211);"><?php echo $attach_value; ?></span></div>
                                                       </div>
                                                       <div class="jFiler-item-assets jFiler-row">
                                                          <ul class="list-inline pull-left">
                                                             <li>
                                                                <div class="jFiler-jProgressBar" style="display: none;">
                                                                   <div class="bar"></div>
                                                                </div>
                                                               
                                                             </li>
                                                          </ul>
                                                          <ul class="list-inline pull-right">
                                                             <li><a class="icon-jfi-trash jFiler-item-trash-action remove_file" data-id="<?php echo $attach_key ?>" ></a></li>
                                                          </ul>
                                                       </div>
                                                    </div>
                                                 </div>
                                              </li>
                                             <?php } //else end

                                               } // foreach
                                               ?>
                                               </ul>
                                               </div>
                                               <?php
                                          }
                                       ?>
                                      </div>
                                       </div>
                                 <!-- en dof image upload for type of ids -->
                                 <div class="form-group row name_fields spanassign for_other_custom_choose" <?php if(in_array('Other_Custom',$all_val) && (isset($client['crm_assign_client_id_verified']) && ($client['crm_assign_client_id_verified']!==''))) { }else { ?> style="display:none" <?php } ?> >
                                    <label class="col-sm-4 col-form-label">Other Custom</label>
                                    <div class="col-sm-8">
                                       <input type="text" name="assign_other_custom" id="assign_other_custom" placeholder="" value="<?php if(isset($client['crm_assign_other_custom']) && ($client['crm_assign_other_custom']!='') ){ echo $client['crm_assign_other_custom'];}?>" class="fields">
                                    </div>
                                 </div>
                                 <div class="form-group row radio_bts ">
                                    <label class="col-sm-4 col-form-label">Proof of Address</label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" class="js-small f-right fields" name="proof_of_address" id="proof_of_address" <?php if(isset($client['crm_assign_proof_of_address']) && ($client['crm_assign_proof_of_address']!=='') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 <div class="form-group row radio_bts ">
                                    <label class="col-sm-4 col-form-label">Meeting with the Client</label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" class="js-small f-right fields" name="meeting_client" id="meeting_client" <?php if(isset($client['crm_assign_meeting_client']) && ($client['crm_assign_meeting_client']!=='') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                                         
                  </div>
               </div>
               <!-- end of aml checks -->
               <div id="other" class="tab-pane fade">
                 <div class="space-required ">
                    <div class="main-pane-border1" id="other_error">
                       <div class="alert-ss"></div>
                    </div>
                 </div>
                  <div class="masonry-container floating_set">
                     <div class="grid-sizer"></div>
                     <div class="accordion-panel">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">Other</a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1" id="others_section">
                                 <div class="form-group row radio_bts others_details" id="<?php echo $this->Common_mdl->get_order_details(78); ?>">
                                    <label class="col-sm-4 col-form-label">Previous Accounts</label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" class="js-small f-right fields" name="previous_account" id="previous_account" value="on" <?php if(isset($client['crm_previous_accountant']) && ($client['crm_previous_accountant']=='on') ){?> checked="checked"<?php } ?> data-id="preacc">
                                    </div>
                                 </div>
                                 <?php 
                                    (isset($client['crm_previous_accountant']) && $client['crm_previous_accountant'] == 'on') ? $pre =  "block" : $pre = 'none';
                                    
                                    
                                    ?>
                                 <div id="enable_preacc">
                                    <div class="form-group row name_fields preacc_content_toggle" style="display:<?php echo $pre;?>;">
                                       <label class="col-sm-4 col-form-label">Name of the Firm </label>
                                       <div class="col-sm-8">
                                          <input type="text" name="name_of_firm" id="name_of_firm" placeholder="" value="<?php if(isset($client['crm_other_name_of_firm']) && ($client['crm_other_name_of_firm']!='') ){ echo $client['crm_other_name_of_firm'];}?>" class="fields">
                                       </div>
                                    </div>
                                    <div class="form-group row preacc_content_toggle" style="display:<?php echo $pre;?>;">
                                       <label class="col-sm-4 col-form-label">Address </label>
                                       <div class="col-sm-8">
                                          <textarea rows="4" name="other_address" id="other_address" class="form-control fields"><?php if(isset($client['crm_other_address']) && ($client['crm_other_address']!='') ){ echo $client['crm_other_address'];}?></textarea>
                                       </div>
                                    </div>
                                 
                                 <!-- preacc close-->
                                 <div class="form-group row name_fields preacc_content_toggle others_details" id="<?php echo $this->Common_mdl->get_order_details(79); ?>" style="display:<?php echo $pre;?>;">
                                    <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(79); ?></label>
                                    <div class="col-sm-8">
                                       <!-- <input type="number" name="other_contact_no" id="other_contact_no" placeholder="" value="<?php if(isset($user['crm_phone_number']) && ($user['crm_phone_number']!='') ){ echo $user['crm_phone_number'];}?>" class="fields"> -->
                                       <input type="number" class="con-code" name="cun_code" name="cun_code" value="<?php if(isset($client['crm_other_contact_no']) && ($client['crm_other_contact_no']!='') ){ echo substr($client['crm_other_contact_no'], 0, 2);} ?>">
                                       <input type="number" name="pn_no_rec" id="pn_no_rec" class="fields" value="<?php if(isset($client['crm_other_contact_no']) && ($client['crm_other_contact_no']!='') ){ echo substr($client['crm_other_contact_no'], 2);} ?>">
                                    </div>
                                 </div>
                                 <div class="form-group row name_fieldsothers_details preacc_content_toggle" id="<?php echo $this->Common_mdl->get_order_details(80); ?>"  style="display:<?php echo $pre;?>;">
                                    <label class="col-sm-4 col-form-label">Email Address</label>
                                    <div class="col-sm-8">
                                       <input type="email" name="emailid" id="emailid" placeholder="" value="<?php if(isset($client['crm_other_email']) && ($client['crm_other_email']!='') ){ echo $client['crm_other_email'];}?>" class="fields">
                                    </div>
                                 </div>
                                 <div class="form-group row radio_bts others_details preacc_content_toggle" id="<?php echo $this->Common_mdl->get_order_details(81); ?>" style="display:<?php echo $pre;?>;">
                                    <label class="col-sm-4 col-form-label">Chase for Information</label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" class="js-small f-right fields" name="chase_for_info" data-id="chase_for_info" id="chase_for_info" <?php if(isset($client['crm_other_chase_for_info']) && ($client['crm_other_chase_for_info']=='on') ){?> checked="checked"<?php } ?> value="on">
                                    </div>
                                 </div>
                                 </div><!-- for previous account hide event -->
                                
                                 <div class="form-group row for_other_notes others_details" id="<?php echo $this->Common_mdl->get_order_details(82); ?>" <?php if(isset($client['crm_other_chase_for_info']) && ($client['crm_other_chase_for_info']=="on") ){ ?> <?php }else{ ?> style="display:none;" <?php } ?> >
                                    <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(82); ?></label>
                                    <div class="col-sm-8">
                                       <textarea rows="4" name="other_notes" id="other_notes" class="form-control fields"><?php if(isset($client['crm_other_notes']) && ($client['crm_other_notes']!='') ){ echo $client['crm_other_notes'];}?></textarea>
                                    </div>
                                 </div>
                                
                      
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- accordion-panel -->
                     <div class="accordion-panel">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">Additional Information - Internal Notes</a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1">
                                 <div class="form-group row ">
                                    <label class="col-sm-4 col-form-label">Notes</label>
                                    <div class="col-sm-8">
                                       <textarea rows="4" name="other_internal_notes" id="other_internal_notes" class="form-control fields"><?php if(isset($client['crm_other_internal_notes']) && ($client['crm_other_internal_notes']!='') ){ echo $client['crm_other_internal_notes'];}?></textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- accordion-panel -->
                       <!-- accordion-panel for client login-->
                       <?php 
                          $showLogin = "";
                          $showLoginchek = "";
                          if(!empty($user['username']) && !empty($user['confirm_password']))
                          {
                            $showLogin = "display:block";
                            $showLoginchek = "checked='checked'";
                          }                        
                       ?>
                     <div class="accordion-panel">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                  <a class="accordion-msg" style="display: inline-block;">Client Login
                                    <input type="checkbox" class="js-small f-right fields" name="show_login" id="show_login" data-id="show_login" <?php echo $showLoginchek;?>>
                                  </a>
                               </h3>
                           </div>
                           <div id="collapse" class="panel-collapse show_login" style="<?php echo $showLogin;?>">
                              <div class="basic-info-client1" id="other_details1">
                                    <!-- 30-08-2018 -->
                                 <div class="form-group row name_fields others_details" id="<?php echo $this->Common_mdl->get_order_details(88); ?>">
                                    <label class="col-sm-4 col-form-label">Username</label>
                                    <div class="col-sm-8">
                                       <input type="text" name="user_name" id="username" placeholder="" value="<?php if(isset($user['username']) && ($user['username']!='') ){ echo $user['username'];}?>" data-id="<?php echo $user['id'] ?>" class="fields user_name">
                                        <span class="v_err" style="color:red;display:none">User name already Exists</span>
                                    </div>
                                 </div>
                                 <div class="form-group row name_fields others_details" id="<?php echo $this->Common_mdl->get_order_details(89); ?>">
                                    <label class="col-sm-4 col-form-label">Password</label>
                                    <div class="col-sm-8">
                                       <input type="password" name="password" id="password" placeholder="" value="<?php if(isset($user['confirm_password']) && ($user['confirm_password']!='') ){ echo $user['confirm_password'];}?>" class="fields">
                                    </div>
                                 </div>
                                 <!-- end of 30-08-2018 -->
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- accordion-panel -->
                      <!-- accordion-panel --> 
                     <div class="accordion-panel">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">Client Source</a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1">
                                 <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Source</label>
                                    <div class="col-sm-8">
                                       <select name="source" id="source" class="form-control fields">
                                          <option value="" selected="selected">--Select--</option>
                                          <option value="Google" <?php if(isset($client['crm_assign_source']) && $client['crm_assign_source']=='Google') {?> selected="selected"<?php } ?>>Google</option>
                                          <option value="FB" <?php if(isset($client['crm_assign_source']) && $client['crm_assign_source']=='FB') {?> selected="selected"<?php } ?>>FB</option>
                                          <option value="Website" <?php if(isset($client['crm_assign_source']) && $client['crm_assign_source']=='Website') {?> selected="selected"<?php } ?>>Website</option>
                                          <option value="Existing Client" <?php if(isset($client['crm_assign_source']) && $client['crm_assign_source']=='Existing Client') {?> selected="selected"<?php } ?>>Existing Client</option>
                                       </select>
                                    </div>
                                 </div>
                              
                                 <div class="form-group row name_fields dropdown_source" <?php if(isset($client['crm_assign_source']) && $client['crm_assign_source']=='Existing Client') {?> <?php }else{ ?> style="display: none;" <?php } ?>>
                                    <label class="col-sm-4 col-form-label">Refered by </label>
                                    <div class="col-sm-8 ">
                                    <div class="dropdown-sin-3">
                                       <!--  <input type="text" name="refer_exist_client" id="refer_exist_client" placeholder="" value="" class="fields"> -->
                                       <select name="refered_by" id="refered_by" class="fields">
                                          <option value="">select</option>
                                          <?php foreach ($referby as $referbykey => $referbyvalue) {
                                             # code...
                                             ?>
                                          <option value="<?php echo $referbyvalue['id'];?>" <?php if(isset($client['crm_refered_by']) && $client['crm_refered_by']==$referbyvalue['id']) {?> selected="selected"<?php } ?>><?php echo $referbyvalue['crm_first_name'];?></option>
                                          <?php } ?>
                                       </select>
                                       </div>
                                    </div>
                                 </div>
                                 
                                 <?php
                                    $crm_assign_relationship_client=( isset($client['crm_assign_relationship_client']) ? $client['crm_assign_relationship_client'] : false);   
                                    $crm_assign_relationship=render_custom_fields_edit( 'relationship_client',$rel_id,$crm_assign_relationship_client);  
                                     if($crm_assign_relationship!='') {
                                      echo $crm_assign_relationship;
                                     } else {
                                    ?>
                                 <div class="form-group row name_fields">
                                    <label class="col-sm-4 col-form-label">Relationship to the Client</label>
                                    <div class="col-sm-8">
                                       <input type="text" name="relationship_client" id="relationship_client" placeholder="" value="<?php if(isset($client['crm_assign_relationship_client']) && ($client['crm_assign_relationship_client']!='') ){ echo $client['crm_assign_relationship_client'];}?>" class="fields">
                                    </div>
                                 </div>
                                 <?php } ?>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- accordion-panel -->
                     <!-- accordion-panel -->
                     <div class="accordion-panel" style="display: none;">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">Notes if any other</a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1">
                                 <div class="form-group row ">
                                    <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_order_details(90); ?></label>
                                    <div class="col-sm-8">
                                       <textarea rows="4" name="other_any_notes" id="other_any_notes" class="form-control fields"><?php if(isset($client['crm_other_any_notes']) && ($client['crm_other_any_notes']!='') ){ echo $client['crm_other_any_notes'];}?></textarea>
                                    </div>
                                 </div>
                                 <?php
                                   // echo render_custom_fields_one( 'other',$rel_id);  ?>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- accordion-panel -->
                  </div>
               </div>
               <!-- other -->
               <!-- referral tab section -->
               <div id="referral" class="tab-pane fade">
                  <div class="masonry-container floating_set">
                     <div class="grid-sizer"></div>
                     
                 
                     <!-- accordion-panel -->
                     <div class="accordion-panel">
                        <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">Invite Client</a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1" id="referals">
                                 <div class="form-group row radio_bts referal_details" id="<?php echo $this->Common_mdl->get_order_details(83); ?>">
                                    <label class="col-sm-4 col-form-label">Invite to use our system</label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" class="js-small f-right fields" name="invite_use" id="invite_use" <?php if(isset($client['crm_other_invite_use']) && ($client['crm_other_invite_use']!=='') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 <div class="nonediv" style="display: none;">
                                 <div class="form-group row radio_bts ">
                                    <label class="col-sm-4 col-form-label">crm</label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" class="js-small f-right fields" name="other_crm" id="other_crm" <?php if(isset($client['crm_other_crm']) && ($client['crm_other_crm']!=='') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 <div class="form-group row radio_bts ">
                                    <label class="col-sm-4 col-form-label">Proposal</label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" class="js-small f-right fields" name="other_proposal" id="other_proposal" <?php if(isset($client['crm_other_proposal']) && ($client['crm_other_proposal']!=='') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 <div class="form-group row radio_bts ">
                                    <label class="col-sm-4 col-form-label">Tasks</label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" class="js-small f-right fields" name="other_task" id="other_task" <?php if(isset($client['crm_other_task']) && ($client['crm_other_task']!=='') ){?> checked="checked"<?php } ?>>
                                    </div>
                                 </div>
                                 </div><!-- display none element -->
                                 <div class="form-group row name_fields referal_details" id="<?php echo $this->Common_mdl->get_order_details(87); ?>">
                                    <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(87); ?></label>
                                    <div class="col-sm-8">
                                       <input type="text" name="send_invit_link" id="send_invit_link" placeholder="" value="<?php if(isset($client['crm_other_send_invit_link']) && ($client['crm_other_send_invit_link']!='') ){ echo $client['crm_other_send_invit_link'];}?>" class="fields">
                                    </div>
                                 </div>
                          
                              </div>
                           </div>
                        </div>
                     </div>
                  
                  </div>
               </div>
               <!-- referral atb section -->
              
               <!-- confirmation-statement -->
               
             
               <div id="notes" class="tab-pane fade" style="display: none;">
                  <div class="accordion-panel">
                     <div class="box-division03">
                        <div class="accordion-heading" role="tab" id="headingOne">
                           <h3 class="card-title accordion-title">
                              <a class="accordion-msg">Additional Information - Internal Notes</a>
                           </h3>
                        </div>
                        <div id="collapse" class="panel-collapse">
                           <div class="basic-info-client1">
                              <div class="form-group row ">
                                 <label class="col-sm-4 col-form-label">Notes</label>
                                 <div class="col-sm-8">
                                    <textarea rows="4" name="notes_info" id="notes_info" class="form-control fields"><?php if(isset($client['crm_notes_info']) && ($client['crm_notes_info']!='') ){ echo $client['crm_notes_info'];}?></textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- accordion-panel -->
               </div>
               <!-- Notes tab close -->
               <div id="documents" class="tab-pane fade">
                  <div class="accordion-panel">
                     <div class="box-division03">
                        <div class="accordion-heading" role="tab" id="headingOne">
                           <h3 class="card-title accordion-title">
                              <a class="accordion-msg">Documents</a>
                           </h3>
                        </div>
                        <div id="collapse" class="panel-collapse">
                           <div class="basic-info-client1">
                              <div class="form-group row">
                                 <label class="col-sm-4 col-form-label">upload Document</label>
                                 <div class="col-sm-8">
                                    <input type="file" name="document" id="document" class="fields">
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- accordion-panel -->
               </div>
               <!-- documents tab close -->
               <div id="task" class="tab-pane fade" style="display: none;">
                  <div class="space-required">
                     <div class="all_user-section2 floating_set">
                        <div class="all-usera1 user-dashboard-section1">
                           <div class="client_section3 table-responsive ">
                              <table class="table client_table1 text-center display nowrap" id="alltask" cellspacing="0" width="100%">
                                 <thead>
                                    <tr class="text-uppercase">
                                       <th>
                                          <div class="checkbox-fade fade-in-primary">
                                             <label>
                                             <input type="checkbox"  id="bulkDelete"  />
                                             <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                             </label>
                                          </div>
                                       </th>
                                       <th>Timer</th>
                                       <th>Task Name</th>
                                       <!-- <th>CRM-Username</th> -->
                                       <th>Start Date</th>
                                       <th>Due Date</th>
                                       <th>Status</th>
                                       <th>Priority</th>
                                       <th>Tag</th>
                                       <th>Assignto</th>
                                       <th>Actions</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php foreach ($task_list as $key => $value) {
                                       if($value['worker']=='')
                                       {
                                       $value['worker'] = 0;
                                       }
                                       $staff=$this->db->query("SELECT * FROM staff_form WHERE user_id in (".$value['worker'].")")->result_array();
                                       if($value['task_status']=='notstarted')
                                       {
                                       $stat = 'Not Started';
                                       }else if($value['task_status']=='inprogress')
                                       {
                                       $stat = 'In Progress';
                                       }else if($value['task_status']=='awaiting')
                                       {
                                       $stat = 'Awaiting for a feedback';
                                       }else if($value['task_status']=='testing')
                                       {
                                       $stat = 'Testing';
                                       }else if($value['task_status']='complete')
                                       {
                                       $stat = 'Complete';
                                       }
                                       $exp_tag = explode(',', $value['tag']);
                                       
                                       ?>
                                    <tr id="<?php echo $value['id']; ?>">
                                       <td>
                                          <div class="checkbox-fade fade-in-primary">
                                             <label>
                                             <input type='checkbox'  class='deleteRow' value="<?php echo $value['id'];?>"  />
                                             <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                             </label>
                                          </div>
                                       </td>
                                       <td><?php echo date('Y-m-d H:i:s', $value['created_date']);?></td>
                                       <td><?php echo ucfirst($value['subject']);?></td>
                                       <td><?php echo $value['start_date'];?></td>
                                       <td><?php echo $value['end_date'];?></td>
                                       <td><?php echo $stat;?></td>
                                       <td>
                                          <?php echo $value['priority'];?>
                                       </td>
                                       <td>
                                          <?php  foreach ($exp_tag as $exp_tag_key => $exp_tag_value) {
                                             echo $tagName = $this->Common_mdl->getTag($exp_tag_value).' ';
                                             }?>
                                       </td>
                                       <td class="user_imgs">
                                          <?php
                                             foreach($staff as $key => $val){     
                                             ?>
                                          <img src="<?php echo base_url().'uploads/'.$val['profile'];?>" alt="img">
                                          <?php } ?>
                                       </td>
                                       <td>
                                          <p class="action_01">
                                             <?php if($role!='Staff'){?>  
                                             <a href="<?php echo base_url().'user/delete/'.$value['id'];?>" onclick="return confirm('Are you sure want to delete');"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>
                                             <?php } ?>
                                             <a href="<?php echo base_url().'user/update_task/'.$value['id'];?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                                             <!-- <a href="#"><i class="fa fa-inbox fa-6" aria-hidden="true"></i></a> -->
                                          </p>
                                       </td>
                                    </tr>
                                    <?php } ?>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- task tab tab close -->
               <div class="space-required">
                  <div class="comp_cre"> 
                     <span><?php if(isset($client['created_date'])){ echo 'Company Created'.' '.date('F j, Y, g:i',$client['created_date']); } ?></span>
                     <span><?php if(isset($user['CreatedTime'])){ echo ';Updated'.' '.date('F j, Y, g:i',$user['CreatedTime']); } ?></span>
                  </div>
               </div>
            </div>
            <!-- tabcontent -->
         </div>
         <div class="floation_set text-right accordion_ups">
            <input type="hidden" name="user_id" id="user_id" value="<?php if(isset($client['user_id']) && ($client['user_id']!='') ){ echo $client['user_id'];}?>" class="fields">
            <input type="hidden" name="client_id" id="client_id" value="<?php if(isset($user['client_id']) && ($user['client_id']!='') ){ echo $user['client_id'];}?>" class="fields">
            <input type="hidden" id="company_house" name="company_house" value="<?php if(isset($user['company_roles']) && ($user['company_roles']!='') ){ echo $user['company_roles'];}?>" class="fields">
            <!-- <input type="submit" class="add_acc_client" value="Save & Exit" id="submit"/> -->
         </div>
      </div>
      <!-- information-tab -->
	  </div>
   </form>
</section>
<!-- client-details-view -->
<!-- <?php  
   foreach ($contactRec as $contactRec_key => $contactRec_value) { 
   $id   = $contactRec_value['id']; 
      ?>
<div id="modalcontact<?php echo $id;?>" class="modal fade" role="dialog">
   <div class="modal-dialog">
      
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" style="text-align-last: center">contact Delete</h4>
         </div>
         <div class="modal-body">
            <p>Are you sure want to delete?</p>
         </div>
         <div class="modal-footer">
            <a href="#" class="delcontact" data-value="<?php echo $id;?>">Delete</a>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<?php  } ?> -->
<!--modal 2-->

<div class="modal fade" id="myAlert" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Alert</h4>
         </div>
         <div class="modal-body">
            <p>  
               Do you want to exit? 
            </p>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default no" data-dismiss="modal">No</button> 
            <button type="button" class="btn btn-default exit">Yes</button>        
         </div>
      </div>
   </div>
</div>
<div class="modal fade remind_popup_client" id="myReminders" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reminders</h4>
         </div>
         <!--   <form id="insert_form1" class="required-save client-firm-info1 validation" method="post" action="" enctype="multipart/form-data">   -->
         <input type="hidden" name="user_id" id="user_id" value="<?php if(isset($client['user_id']) && ($client['user_id']!='') ){ echo $client['user_id'];}?>" class="fields">
         <input type="hidden" name="client_id" id="client_id" value="<?php if(isset($user['client_id']) && ($user['client_id']!='') ){ echo $user['client_id'];}?>" class="fields">
         <input type="hidden" id="company_house" name="company_house" value="<?php if(isset($user['company_roles']) && ($user['company_roles']!='') ){ echo $user['company_roles'];}?>" class="fields">
         <input type="hidden" name="emailid" id="emailid" placeholder="" value="<?php if(isset($client['crm_email']) && ($client['crm_email']!='') ){ echo $client['crm_email'];}?>" class="fields">
         <input type="hidden" name="user_name" id="username" placeholder="" value="<?php if(isset($user['username']) && ($user['username']!='') ){ echo $user['username'];}?>" class="fields">
         <div class="modal-body">
            <div class="body-popup-content">
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default save" data-dismiss="modal">Close</button>        
         </div>
      </div>
      <!--   </form> -->
   </div>
</div>
<!-- modal-close -->
<!-- modal 2-->
<!-- new person add contact delete modal popup-->
<div id="modalnewperson" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" style="text-align-last: center">contact Delete</h4>
         </div>
         <div class="modal-body">
            <p>Are you sure want to delete?</p>
         </div>
         <div class="modal-footer">
            <a href="#" class="delnewperson" data-value="">Delete</a>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!--modal close-->
<div class="modal fade" id="add-reminder" role="dialog">
   <div class="modal-dialog modal-md">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close reminder_close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">add reminder</h4>
         </div>
         <div class="alert alert-success-re" style="display:none;"
            ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Remainder Added.</div>
         <div class="alert alert-danger-re" style="display:none;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Not Added.
         </div>
         <form action="" class="validation edit_field_form" method="post" accept-charset="utf-8" novalidate="novalidate">
            <div class="modal-body">
               <div class="modal-topsec">
                  <div class="send-invoice">
                     <div class="form-group">
                        <span class="invoice-notify">send</span>
                        <select name="due">
                           <option value="due_by">due by</option>
                           <option value="overdue_by">overdue by</option>
                        </select>
                     </div>
                     <div class="form-group">
                        <span class="days1">days</span>
                        <input type="text" class="due-days" name="days">
                          <span class="error_msg_days" style="color: red;display: none;">This Field is Required</span>
                     </div>
                  </div>
                  <div class="form-group">
                        <span class="days1">Email Template</span>
                        <select name="email_template" class="email_template">
                        <option value="">Select</option>
                          <?php foreach($reminder_templates as $key => $value1){ ?>              
                          <option value="<?php echo $value1['id']; ?>"><?php echo ucwords($value1['title']); ?></option>            
                          <?php } ?>
                        </select>

                        <span class="error_msg_email" style="color: red;display: none;">This Field is Required</span>
                  </div> 
                  <!-- <div class="insert-placeholder">
                      <span target="message" class="add_merge">{crm_ch_accounts_next_due}</span><br>
                        <span target="message" class="add_merge">{crm_accounts_custom_reminder}</span><br>
                     <div class="form-group">
                        <select name="placeholder" id="placeholder">
                           <option value="">--select placeholder--</option>
                           <option value="4">Confirmation</option>
                           <option value="3">Accounts</option>
                           <option value="5">Company tax return</option>
                           <option value="6">Personal tax return</option>
                           <option value="7">Payroll</option>
                           <option value="8">Workplace Pension</option>
                           <option value="10">CIS</option>
                           <option value="11">CIS SUB</option>
                           <option value="12">P11D</option>
                           <option value="13">Book keeping</option>
                           <option value="14">Management</option>
                           <option value="15">VAT</option>
                        </select>
                     </div>
                  </div> -->
                     <div class="append_placeholder">
                     </div>
               </div>
               <!--modal-topsec-->
               <div class="modal-bottomsec">
                  <div class="form-group">
                     <input type="hidden" name="service_id" id="service_id" value="">
                  </div>
                  <div class="form-group">
                  <input type="text" class="invoice-msg" name="subject" placeholder="Subject" value="">
                    <span class="error_msg_subject" style="color: red;display: none;">This Field is Required</span>
                    </div>
                  <div class="form-group">
                    <textarea name="message" id="editor2"></textarea>
                  <span class="error_msg_message" style="color: red;display: none;">This Field is Required</span></div>
               </div>
            </div>
            <div class="modal-footer">
               <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
               <!-- <button type="submit" class="reminder-delete">delete</button> -->
               <input type="hidden" name="client_id" value="<?php echo $client['id']; ?>">
               <input type="hidden" id="clicked_checkbox_name">
               <button type="submit" class="reminder-save">save</button>
            </div>
         </form>
      </div>
   </div>
</div>
<div class="modal fade" id="edit-reminder" role="dialog">
   <div class="modal-dialog modal-md">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">add reminder</h4>
         </div>
      
         <form action="" id="edit_cus_reminder_form" method="post" accept-charset="utf-8" novalidate="novalidate">
            <div class="modal-body">
               <div class="modal-topsec">
                  <div class="send-invoice">
                     <div class="form-group">
                        <span class="invoice-notify">send</span>
                        <select name="due">
                           <option value="due_by">due by</option>
                           <option value="overdue_by">overdue by</option>
                        </select>
                     </div>
                     <div class="form-group">
                        <span class="days1">days</span>
                        <input type="text" class="due-days" name="days">
                          <span class="error_msg_days" style="color: red;display: none;">This Field is Required</span>
                     </div>
                  </div>
                  <div class="form-group">
                        <span class="days1">Email Template</span>
                        <select name="email_template" class="email_template">
                          <option value="">Select</option>
                          <?php foreach($reminder_templates as $key => $value1){ ?>              
                          <option value="<?php echo $value1['id']; ?>"><?php echo ucwords($value1['title']); ?></option>
                          <?php } ?>
                        </select>

                        <span class="error_msg_email" style="color: red;display: none;">This Field is Required</span>
                  </div> 
                  <!-- <div class="insert-placeholder">
                      <span target="message" class="add_merge">{crm_ch_accounts_next_due}</span><br>
                        <span target="message" class="add_merge">{crm_accounts_custom_reminder}</span><br>
                     <div class="form-group">
                        <select name="placeholder" id="placeholder">
                           <option value="">--select placeholder--</option>
                           <option value="4">Confirmation</option>
                           <option value="3">Accounts</option>
                           <option value="5">Company tax return</option>
                           <option value="6">Personal tax return</option>
                           <option value="7">Payroll</option>
                           <option value="8">Workplace Pension</option>
                           <option value="10">CIS</option>
                           <option value="11">CIS SUB</option>
                           <option value="12">P11D</option>
                           <option value="13">Book keeping</option>
                           <option value="14">Management</option>
                           <option value="15">VAT</option>
                        </select>
                     </div>
                  </div> -->
                     <div class="append_placeholder">
                     </div>
               </div>
               <!--modal-topsec-->
               <div class="modal-bottomsec">
                  <div class="form-group">
                     <input type="hidden" name="service_id" id="service_id" value="">
                  </div>
                  <div class="form-group">
                  <input type="text" class="invoice-msg" name="subject" placeholder="Subject" value="">
                    <span class="error_msg_subject" style="color: red;display: none;">This Field is Required</span>
                  </div>
                  <div class="form-group"><textarea name="message" id="editor2"></textarea></div>
               </div>
            </div>
            <div class="modal-footer">
               <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
               <!-- <button type="submit" class="reminder-delete">delete</button> -->
               <input type="hidden" name="table_id">
               <button type="submit" class="reminder-save">save</button>
            </div>
         </form>
      </div>
   </div>
</div>
<div class="add_custom_fields_section" style="display: none;">
<?php   echo render_custom_fields_one( 'other',$rel_id,['firm_id'=>$_SESSION['firm_id']]); ?>
<?php   echo render_custom_fields_one( 'confirmation',$rel_id,['firm_id'=>$_SESSION['firm_id']]);  ?>
<?php   echo render_custom_fields_one( 'accounts',$rel_id,['firm_id'=>$_SESSION['firm_id']]);   ?>
<?php   echo render_custom_fields_one( 'personal_tax',$rel_id,['firm_id'=>$_SESSION['firm_id']]);  ?>
<?php   echo render_custom_fields_one( 'investigation',$rel_id,['firm_id'=>$_SESSION['firm_id']]); ?>
<?php   echo render_custom_fields_one( 'assign_to',$rel_id,['firm_id'=>$_SESSION['firm_id']]); ?>
<?php   echo render_custom_fields_one( 'payroll',$rel_id,['firm_id'=>$_SESSION['firm_id']]);  ?>
<?php   echo render_custom_fields_one( 'vat',$rel_id,['firm_id'=>$_SESSION['firm_id']]);  ?>
<?php   echo render_custom_fields_one( 'management',$rel_id,['firm_id'=>$_SESSION['firm_id']]);  ?>
<?php   echo render_custom_fields_one( 'customers',$rel_id,['firm_id'=>$_SESSION['firm_id']]);  ?>
</div>
<div class="modal fade all_layout_modal" id="company_house_contact" tabindex="-1" role="dialog">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h4 class="modal-title"> Companines House Contact</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  

                  <div class="main_contacts" style="display:none">
                   
                  </div>
               </div>
               <!-- modalbody -->
            </div>
         </div>
</div>
<!-- add reminder -->
<?php //$this->load->view('clients/edit_custom_reminder_view');?>
<?php $this->load->view('includes/footer');?>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/client_page.js"></script>
<script src="<?php echo base_url();?>assets/tree_select/comboTreePlugin.js"></script>
<script src="<?php echo base_url();?>assets/tree_select/icontains.js"></script>

<script>
 var tree_select = <?php echo json_encode( GetAssignees_SelectBox() ); ?>;
       $('#tree_select').comboTree({ 
          source : tree_select,
          isMultiple: true
       });

 $('.comboTreeItemTitle').click(function(){  
    var id = $(this).attr('data-id').split('_');
    var selected = [];
       $('.comboTreeItemTitle').each(function(){
          var id1 = $(this).attr('data-id');
          var id1 = id1.split('_');
             if($(this).find('input[type="checkbox"]').is(":checked"))
             {
                selected.push(id1[0]);
             }
          //console.log(id[1]+"=="+id1[1]);
          if(id[1]==id1[1])
          {
             $(this).toggleClass('disabled');
          }
       });
       $("#tree_select_values").val(selected.join());
       $(this).removeClass('disabled');
 });

var assignees = '<?php echo $assignees_node;?>';
if(assignees!='')
{
  assignees = assignees.split(',');
  $('.comboTreeItemTitle').each(function(){
     var id1 = $(this).attr('data-id');
     var id1 = id1.split('_');
     if(assignees.indexOf(id1[0])!=-1)
     {
      $(this).find('input[type="checkbox"]').trigger('click');
     }
  });
}
$('.hashtag').tagsinput({
        allowDuplicates: true
      });
      $('.hashtag').on('itemAdded', function(item, tag) {
         
         $('.hashtag').tagsinput('items');
       
     });
 $(document).ready(function() {

 	var check_value='<?php echo $client['blocked_status']; ?>';
 	//alert(check_value);
 	if(check_value==1){
 		$( ".services" ).each(function(  ) {
 		var oop=$(this).val();
 		if(oop=='on'){
      $(".LoadingImage").show();
       $(this).parent('.switching').find('.services').each(function() {   
        $(this).prop('checked', $(this).prop(''));        
        });
 			$(this).parent('.switching').find('.switchery').each(function() {
        $(this).trigger('click'); 
         $(this).trigger('change');
         $(this).addClass('disabled','disabled');
        });	
setTimeout(function(){  $(".LoadingImage").hide(); }, 3000); 		 
 		}
});
 	}

 });


  $(document).ready(function() {

  var check_reminder='<?php echo $client['reminder_block']; ?>';
 // alert(check_reminder);
  if(check_reminder==1){
    $( ".reminder" ).each(function(  ) {
    var oop=$(this).val();
    //alert(oop);
    if(oop=='on'){
      $(".LoadingImage").show();
       $(this).parent('.splwitch').find('.reminder').each(function() {   
        $(this).prop('checked', $(this).prop(''));        
        });
      $(this).parent('.splwitch').find('.switchery').each(function() {
        $(this).trigger('click'); 
         $(this).addClass("disabled");
         $(this).trigger('change');
         $(this).attr('disabled', 'disabled'); 
        }); 
setTimeout(function(){  $(".LoadingImage").hide(); }, 3000);     
    }
});
  }

 });


<?php if($client['blocked_status']=='0'){
	// echo "unblocked";
}else{
	/*echo "blocked";*/
} ?>



   $(document).on('change','.datepicker-13',function()
   {
     var date_val=$(this).val();
     var custom=$(this).attr('data-filed-custom');
       var fi_data_to = $(this).attr("data-fieldto");
              var fi_data_id  = $(this).attr("data-fieldid");
     if(typeof custom != "undefined"){
   
             $("[name^=custom_fields]").each(function() {
           var data_to = $(this).attr("data-fieldto");
              var data_id  = $(this).attr("data-fieldid");
   if((fi_data_id==data_id) && (fi_data_to==data_to) )
   {
     $(this).attr('value',date_val);
   }
           // Do stuff
         });
     }
     //$(this).attr('value','sdasd');
   
   });
      $( function() {
      /*  $( "#datepicker1,#datepicker2,#datepicker3,#datepicker4,#datepicker5,#datepicker6,#datepicker7,#datepicker8,#datepicker9,#datepicker10,#datepicker11,#datepicker12,#datepicker13,#datepicker14,#datepicker15,#datepicker16,#datepicker17,#datepicker18,#datepicker19,#datepicker20,#datepicker21,#datepicker22,#datepicker23,#datepicker24,#datepicker25,#datepicker26,#datepicker27,#datepicker28,#datepicker29,#datepicker30,#datepicker31,#datepicker32,#datepicker33,#datepicker34,#datepicker35,#datepicker36,#datepicker37,#datepicker38,#datepicker39,#letter_sign,#confirmation_next_reminder,#accounts_next_reminder_date,#personal_next_reminder_date,#datepicker,.datepicker,#personal_tax_return_date,#p11d_due_date").datepicker({ dateFormat: "yyyy-mm-dd" });*/
      // $(".datepicker").datepicker({dateFormat: 'd MM, y'});
      // } );
   
   
    /*  $(document).ready(function() {
       $(".reminder_close").click(function(){
         if ( $('.alert-success-re').css('display') == 'none' && $('.reminder_list').length == '0')
         {
            $("div.custom_remain .switchery").trigger('click');
         }      
       }); 
   });*/
   
   
   
      $('#auth_code').keyup(function () {
            $('.confirmation_auth_code').val($(this).val());
            $('.accounts_auth_code').val($(this).val());
        });
    });
</script>

<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script>   
 function validateEmail($email)
 {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return emailReg.test( $email );
 }

function phonenumber(inputtxt)
{
   console.log(inputtxt);

   var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;

         if(phoneno.test(inputtxt.value))
        {
            return true;
        }
        else
        {    
            return false;
        }
}
      var check_username = 
      {
         url:'<?php echo base_url();?>firm/check_username/',
         type : 'post',
         data:{
            <?php if(!empty($uri_seg)){?> id:function(){return $('#user_id').val();} <?php } ?>
            
         }   
    };
    var check_company_numper = {
      url:'<?php echo base_url();?>client/check_company_numberExist/',
      type : 'post',
       data:{
            <?php if(!empty($uri_seg)){?> id:function(){return $('#user_id').val();} <?php } ?>
         }
   };
</script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/add_client/client_validation_rules.js"></script>
<script type="text/javascript">
   $(document).ready(function(){
     
   jQuery.validator.addMethod("contact_no",function(inputtxt ,element)
    {
      return true;
       var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
       console.log(this.optional(element)+"inside contact_no");
      return this.optional(element) || phoneno.test(inputtxt);
           
    }, "Enter Valid Contact Number.");

    
   $("form#insert_form").on('submit',function(){
        
         $(document).find('input[name^="work_email"] , input[name^="main_email"]').each(function(){
            $(this).rules("add", 
                  {
                      email:true,
                      messages: {
                          email:"Enter Valid Email Address."
                      }
                  });
         });
         $(document).find('input[name^="landline"] ,input[name^="mobile_number"] ').each(function(){
            $(this).rules("add", 
                  {
                      required:false,
                      contact_no:true,
                                         
                  });
         });   
         $(document).find('input[name^="first_name"] ').each(function(){
            $(this).rules("add", 
                  {
                      required:true,
                      messages: {
                          required: "First Name is required.",
                      }
                  });
         }); 
         $("#insert_form").find("input[name='emailid']").rules("add", 
                  {  
                     required: {depends:check_previous_tab_open},
                     email:    {depends:check_previous_tab_open},                     
                     messages:{
                                 required:"Email Address For Previous Accounts.",
                                 email:"Please Enter A Valid Email Address In Previous Accounts."
                              },
                  });
            $("#insert_form").find("input[name='cun_code']").rules("add", 
                  {  
                     required: {depends:check_previous_tab_open},
                     digits:   {depends:check_previous_tab_open},
                     minlength: {param:2,depends:check_previous_tab_open},
                     maxlength:{param:2,depends:check_previous_tab_open},
                     messages:{
                                 required:"Enter Valide Country Code",
                              }
                  });
            $("#insert_form").find("input[name='pn_no_rec']").rules("add",
                  {
                     required:{depends:check_previous_tab_open},
                     contact_no:{depends:check_previous_tab_open},
                     messages:{
                                required:"Contact No Required.",
                              }
                  });
           
    });       

   $("#insert_form").validate({
   
          ignore: false,      
            errorPlacement: function(error, element)
            {
               var err=error.prop('outerHTML');     
                  console.log(error.attr('class'));

               if($("#required_information").has(element).length){                    
                   $('#required_information_error').append(err);
                   $('#required_information_error').find('.field-error').each(function(){
                     if($(this).html()!=''){
                        $(this).addClass('required-errors');
                     }
                   })                   
               }          
               if($("#basic_details").has(element).length){
                  $('#basic_details_error').append(err);
                    $('#basic_details_error').find('.field-error').each(function(){
                        if($(this).html()!=''){
                              $(this).addClass('required-errors');
                        }
                    })                     
               }
               if($("#main_Contact").has(element).length){
                   $('#main_Contact_error').append(err);
                   $('#main_Contact_error').find('.field-error').each(function(){
                     if($(this).html()!=''){
                        $(this).addClass('required-errors');
                     }
                   }); 
               }
               if($("#service").has(element).length){
                  $('#service_error').append(err);
                  $('#service_error').find('.field-error').each(function(){
                     if($(this).html()!=''){
                        $(this).addClass('required-errors');
                     }
                  });
               }
               if($("#assignto").has(element).length){
                  $('#assignto_error').append(err);
                   $('#assignto_error').find('.field-error').each(function(){
                        if($(this).html()!=''){
                            $(this).addClass('required-errors');
                        }
                   }) 
               }
               if($("#amlchecks").has(element).length){
                  $('#amlchecks_error').append(err);
                  $('#amlchecks_error').find('.field-error').each(function(){
                        if($(this).html()!=''){
                            $(this).addClass('required-errors');
                        }
                   }) 
               }
               if($("#other").has(element).length){
                if((element.attr("name")=="user_name" || element.attr("name")=="password") && $("#show_login").prop("checked")!=true)
              {
                $("#show_login").attr("checked","checked");
                $("#show_login").parent().find(".switchery").trigger("click");
              }

                  $('#other_error').append(err);
                  $('#other_error').find('.field-error').each(function(){
                        if($(this).html()!=''){
                            $(this).addClass('required-errors');
                        }
                   }) 
               }

               if($("#referral").has(element).length){
                  $('#referral_error').append(err);
                $('#referral_error').find('.field-error').each(function(){
                        if($(this).html()!=''){
                            $(this).addClass('required-errors');
                        }
                   }) 
               }               
                if($("#import_tab").has(element).length)
                {
                   $('#services_information_error').append(err);
                   $('#services_information_error').find('.field-error').addClass('required-errors');
                }
                if (element.attr("name") == "append_cnt" )
                {
                  $('.for_contact_validation').append(error);
                }                    
                else if(element.attr("name")=="make_primary_loop")
                {
                  $('.for_contact_validation').append(error)
                }
                else
                {
                  error.insertAfter(element);
                }    
            },
                            rules: Rules,
                            messages:Message,
                            errorElement: "span", 
                            errorClass: "field-error",                             
                             
                           submitHandler: function(form) {

                              /*for Remove cus reminder template*/
                               var Removed_CusReminder = [];
                               $('input[type="checkbox"].cus_reminder').not(':checked').each(function(){
                                if( $(this).val()!='on' && $(this).val()!='')
                                {
                                  Removed_CusReminder.push($(this).val());
                                }
                               });

                               var formData = new FormData($("#insert_form")[0]);
                                formData.append('removed_cus_reminder',Removed_CusReminder.join());
                               $(".LoadingImage").show();
                               $.ajax({
                                   url: '<?php echo base_url();?>client/updates_client/',
                                   dataType : 'json',
                                   type : 'POST',
                                   data : formData,
                                   contentType : false,
                                   processData : false,
                                   success: function(data) {
   
                                    var Contact_id = $('#user_id').val();
   
                                    contact_add(Contact_id);
                                    
                                   },
                                   error: function() {
                                       // $(".LoadingImage").hide();
                                      // alert('faileddd');
                                    }
                               });
                           
   
                               return false;
                           } ,
                            /* invalidHandler: function(e,validator) {
           for (var i=0;i<validator.errorList.length;i++){   
               $(validator.errorList[i].element).parents('.panel-collapse.collapse').collapse('show');
           }
       }*/
        invalidHandler: function(e, validator) {
              //console.log(validator.errorList);
              //console.log(jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id'));
              if(validator.errorList.length)
           $('#tabs a[href="#' + jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show');
    if(jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id')=='other'){
             
                    $(".other_tabs").find('.nav-link').trigger('click');
                 }
           }
   
                       });
   

      var start = new Date();
    start.setFullYear(start.getFullYear() - 70);
    var end = new Date();
    end.setFullYear(end.getFullYear());
          $(".date_picker_dob").datepicker({ dateFormat: 'dd-mm-yy', changeMonth: true,
        changeYear: true,yearRange: start.getFullYear() + ':' + end.getFullYear() }).val();
   
    $( ".datepicker" ).each(function( index ) {
   //console.log( index + ": " + $( this ).text() );
   $(this).removeClass('datepicker');
   $(this).addClass('datepicker-13');
   });
      function arr(name){
       var values = $("input[name='"+name+"[]']").map(function(){return $(this).val();}).get();
       return values;
      }
   
   
     function contact_add(id){
    $(".LoadingImage").show();
      var cnt = $("#append_cnt").val();
      var data={};
      var title=[];
      $("select[name=title]").each(function(){
       title.push($(this).val());
      }); 
      //data['title'] = arr('title');
      data['title'] = title;
      
      data['middle_name'] = arr('middle_name');
      data['last_name'] = arr('last_name');
      data['surname'] = arr('surname');
      data['preferred_name'] = arr('preferred_name');
      data['nationality'] = arr('nationality');
      data['psc'] = arr('psc');
      data['ni_number'] = arr('ni_number');
      //data['content_type'] = arr('content_type');
      data['address_line1'] = arr('address_line1');
      data['address_line2'] = arr('address_line2');
      data['town_city'] = arr('town_city');
      data['post_code'] = arr('post_code');
      //data['landline'] = arr('landline');
      data['date_of_birth'] = arr('date_of_birth');
      data['nature_of_control'] = arr('nature_of_control');
      //data['marital_status'] = arr('marital_status');
      data['utr_number'] = arr('utr_number');
      data['occupation'] = arr('occupation');
      data['appointed_on'] = arr('appointed_on');
      data['country_of_residence'] = arr('country_of_residence');
      data['other_custom'] = arr('other_custom');
      var selValue = $('input[name=make_primary]:checked').val(); 

      data['make_primary'] = selValue;
      data['make_primary_loop'] = arr('make_primary_loop');
      data['event'] = 'add';

      //data['work_email'] = arr('work_email');
      data['pre_landline'] = [];
      data['landline'] = [];
      data['work_email'] = [];
      data['client_contact_table_id'] = [];
      data['first_name'] = [];
      data['mobile'] = [];
      data['main_email'] =[]; 
         $('.make_a_primary').each(function(){

            
               var i = $(this).attr('id').split('-')[1];

            data['first_name'].push ( $(this).find('input[name="first_name['+i+']"]').val() ); 
            data['mobile'].push ( $(this).find('input[name="mobile_number['+i+']"]').val() ); 
            data['main_email'].push ( $(this).find('input[name="main_email['+i+']"]').val() ); 

            var temp = '';
            if( $(this).find('.contact_table_id').length )
            {
              temp = $(this).find('.contact_table_id').val();
            }
            data['client_contact_table_id'].push( temp );

            
            var temp = [];
            $('select[name^="pre_landline['+i+']"]').each(function(){
                temp.push( $(this).val() ); 
            });
            data['pre_landline'].push( temp );

            var temp = [];

            $('input[name^="landline['+i+']"]').each(function(){
               temp.push( $(this).val() ); 
               
            });
            data['landline'].push( temp );


            var temp = [];
        

            $('input[name^="work_email['+i+']"]').each(function(){
                temp.push( $(this).val() );  
            });
            data['work_email'].push( temp );

         });
          var usertype=[];
      $("select[name=shareholder]").each(function(){
       usertype.push($(this).val());
      }); 
      
       var marital=[];
      $("select[name=marital_status]").each(function(){
       marital.push($(this).val());
      }); 
      
      var contacttype=[];
      $("select[name=contact_type]").each(function(){
       contacttype.push($(this).val());
      }); 
      
      data['marital_status'] = marital;
      data['contact_type'] = contacttype;
      data['shareholder'] = usertype;
      data['client_id'] = id;
      data['event']='update';
      data['status']='<?php echo $user['status'];?>';
      data['cnt'] =cnt;
      $.ajax({
      url: '<?php echo base_url();?>client/update_client_contacts/',
      type : 'POST',
      data : data,
      success: function(data) {
        if(data)
        {
          $(".alert-success").show();
        }
      },
      });
      //});
   }
   
  /* function add_assignto(clientId)
   {
    var data = {};
   
   
           var team=[];
   $("select[name=team]").each(function(){
       team.push($(this).val());
   }); 
    data['team'] = team;
   
       data['allocation_holder'] = arr('allocation_holder');
       data['clientId'] = clientId;
       $.ajax({
        url: '<?php echo base_url();?>client/add_assignto/',
       type: "POST",
       data: data,
       success: function(data)  
       {
           add_responsibleuser(clientId);
       }
     });
   }
   
   function add_responsibleuser(clientId)
   {
    var data = {};
    // data['assign_managed'] = arr('assign_managed');
    //    data['manager_reviewer'] = arr('manager_reviewer');
   
           var assign_managed=[];
   $("select[name=assign_managed]").each(function(){
       assign_managed.push($(this).val());
   }); 
   
       var manager_reviewer=[];
   $("select[name=manager_reviewer]").each(function(){
       manager_reviewer.push($(this).val());
   }); 
   
    data['assign_managed'] = assign_managed;
       data['manager_reviewer'] = manager_reviewer;
   
       data['clientId'] = clientId;
       $.ajax({
        url: '<?php echo base_url();?>client/add_responsibleuser/',
       type: "POST",
       data: data,
       success: function(data)  
       {
        add_assigntodepart(clientId);
         //location.reload();  
        // window.location.href="<?php echo base_url().'user'?>"
       }
     });
   }
   
   function add_assigntodepart(clientId)
   {
    var data = {};
   
   
       var depart=[];
   $("select[name=depart]").each(function(){
       depart.push($(this).val());
   }); 
    data['depart'] = depart;
       data['allocation_holder'] = arr('allocation_holder_dept');
       data['clientId'] = clientId;
       $.ajax({
        url: '<?php echo base_url();?>client/add_assigntodepart/',
       type: "POST",
       data: data,
       success: function(data)  
       {
           add_responsiblemember(clientId);
       }
     });
   }
   
   function add_responsiblemember(clientId)
   {
    var data = {};
   //  data['assign_managed'] = arr('assign_managed');
   //     data['manager_reviewer'] = arr('manager_reviewer');
   
   //         var assign_managed_member=[];
   // $("select[name=assign_managed_member]").each(function(){
   //     assign_managed_member.push($(this).val());
   // }); 
   
       var manager_reviewer_member=[];
   $("select[name=manager_reviewer_member]").each(function(){
       manager_reviewer_member.push($(this).val());
   });
          var assign_managed_member=[];
   // $("select[name=assign_managed_member[]]").each(function(){
   //     assign_managed_member.push($(this).val());
   // });
   $(".assign_managed_member").each(function(){
       assign_managed_member.push($(this).val());
   });  
   
    //data['assign_managed_member'] = assign_managed_member;
    //data['assign_managed_member'] = arr('assign_managed_member');
    data['assign_managed_member']=assign_managed_member;
    data['manager_reviewer_member'] = manager_reviewer_member;
   // alert(JSON.stringify(data));
   // console.log(data);
       data['clientId'] = clientId;
       $.ajax({
        url: '<?php echo base_url();?>client/add_responsiblemember/',
       type: "POST",
       data: data,
       success: function(data)  
       {
         for_add_reminderintotask(clientId);
                  
                                                $(".LoadingImage").hide();
   
         //location.reload();  
         //window.location.href="<?php echo base_url().'user'?>";
          //$('.alert-danger').hide();
          setTimeout(function() { 
                                           $('.succs').html('<div class="modal-alertsuccess  alert alert-success succs"><div class="newupdate_alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><div class="pop-realted1"><div class="position-alert1">Your client was updated successfully.</div></div></div></div>');
                                              window.location.href="<?php echo base_url().'user'?>";
                                             }, 1000);  
         
   
       }
     });
   }*/
   
   function for_add_reminderintotask(id)
{
   var data={};
   data['user_id']=id;
     $.ajax({
                                   url: '<?php echo base_url();?>client/insert_remindertask/',
                                  
                                   type : 'POST',
                                   data :data ,
                                
                                   success: function(data) {

                                  
                                   },
                                   error: function() {
                                        //contact_add(data);
                                       // $(".LoadingImage").hide();
                                     //  alert('faileddd');
                                     }
                               });
   
}

    

   /*$(".user_name").keyup(function(){
   var val = $(this).val();
   var val_id=$(this).attr('data-id');
   $.ajax({
                                   url: '<?php echo base_url();?>user/check_username/',
                                   dataType : 'json',
                                   type : 'POST',
                                   data : {'val':val,'id':val_id},
                                   success: function(data) {
                                    //alert(data);
                                       if(data == '0'){
                                          //$(".v_err").html("User name already Exists");
                                          $(".sv_ex").attr("disabled", "disabled");
                                          $('.v_err').css('display','block');

                                       }else{
                                          $('.sv_ex').prop("disabled", false); // Element(s) are now enabled.
                                          $('.v_err').css('display','none');
                                       }
                                   }
                               });
});*/
    /*$("#insert_form1").validate({
   
          ignore: false,
      // errorClass: "error text-warning",
       //validClass: "success text-success",
       // highlight: function (element, errorClass) {
       //     //alert('em');
       //    // $(element).fadeOut(function () {
       //        // $(element).fadeIn();
       //     //});
       // },
                           rules: {
                           company_name: {required: true},
                          // company_number: {required: true},
                   
                           },
                           errorElement: "span", 
                           errorClass: "field-error",                             
                            messages: {
                             company_name: "Give company name",
                             //company_number: "Give company number",
                             
                            },
   
                           
                           submitHandler: function(form) {
                               var formData = new FormData($("#insert_form1")[0]);
   
                               $(".LoadingImage").show();
                               $.ajax({
                                   url: '<?php echo base_url();?>client/updates_client/',
                                   dataType : 'json',
                                   type : 'POST',
                                   data : formData,
                                   contentType : false,
                                   processData : false,
                                   success: function(data) {
   
                                   // location.reload();
                                     $("#myReminders .close").click();
                                 //  $("#myReminders").fadeOut();
   
                                  //  alert(data);
                                    var Contact_id = $('#user_id').val();
                                    
                                  //  contact_add(Contact_id);
                                    //add_assignto(Contact_id);
                                       if(data == '1'){
                                          // $('#insert_form')[0].reset();
                                           $('.alert-success').show();
                                           $('.alert-danger').hide();
                                       }
                                       else if(data == '0'){
                                          // alert('failed');
                                           $('.alert-danger').show();
                                           $('.alert-success').hide();
                                       }
                                   
                                  // $(".LoadingImage").hide();
                                   },
                                   error: function() {
                                      //  $(".LoadingImage").hide();
                                       alert('faileddd');}
                               });
   
                               return false;
                           } ,
       //                       invalidHandler: function(e,validator) {
       //     for (var i=0;i<validator.errorList.length;i++){   
       //         $(validator.errorList[i].element).parents('.panel-collapse.collapse').collapse('show');
       //     }
       // }
        invalidHandler: function(e, validator) {
              if(validator.errorList.length)
           $('#tabs a[href="#' + jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
   
           }
   
                       });*/
   
   });
</script>
<script type="text/javascript">
 
   $(document).ready(function(){
      $( ".datepicker-13" ).datepicker({
           dateFormat: 'dd-mm-yy',
          // minDate:0,
            changeMonth: true,
        changeYear: true,
                onSelect:function(selectedDate){
               //  alert($(this).attr('name'));
                     $("input[name="+$(this).attr('name')+"]").each(function() {
                        $(this).attr('value',selectedDate);
                       });
                    },
                 }
            );
  
            $(document).on( 'click', '.datepicker-13,.hasDatepicker', function() {
       $( ".datepicker-13,.hasDatepicker" ).datepicker({
           dateFormat: 'dd-mm-yy',
        //   minDate:0,
            changeMonth: true,
        changeYear: true,
                onSelect:function(selectedDate){
               //  alert($(this).attr('name'));
                     $("input[name="+$(this).attr('name')+"]").each(function() {
                        $(this).attr('value',selectedDate);
                       });
                    },
                 }
            );
   });
             });
   
            $( document ).ready(function() {
   $(".dob_picker").datepicker({ dateFormat: 'dd-mm-yy',
      //minDate:0,
       changeMonth: true,
        changeYear: true,  }).val();
   //$(document).on('change','.othercus',function(e){
   });
   
   $(document).on( 'click', '.datepicker,.hasDatepicker', function() {
   // $('.datepicker,.hasDatepicker').datepicker({ dateFormat: 'yy-mm-dd' });
   //alert('date picker is not working');
   });


   $(document).ready(function(){

     $( ".services" ).each(function(  ) {
                    if($(this).is(':checked')){               
                           $(this).parents('.switching').find('.switchery').addClass('default');
                     }
      });

       $( ".reminder" ).each(function(  ) {
          if($(this).is(':checked')){
                 $(this).parents('.splwitch').find('.switchery').addClass('default');  
          }

        });

        $( ".text" ).each(function(  ) {
              if($(this).is(':checked')){
                     $(this).parents('.textwitch').find('.switchery').addClass('default');   
                }
          });

         $( ".invoice" ).each(function(  ) {
              if($(this).is(':checked')){
                     $(this).parents('.invoicewitch').find('.switchery').addClass('default');      
              }
          });




   });
   
$(document).ready(function(){


   
     

   //   $('.checkall').parents('tr').find('th:not(:nth-child(2)) .js-small').next().addClass('disabled','disabled');
   // $(".checkall").change(function(){

   //    if($(this).is(':checked')){    
   //     $(this).parents('tr').find('th:not(:nth-child(2)) .js-small').next().removeClass('disabled');   
   //       $('td.switching .switchery').trigger('click'); 
   //       $(".services").prop('checked', $(this).prop('checked')); 
   //      // $("myReminders").hide();
   //    }else{
   //         $(this).parents('tr').find('th:not(:nth-child(2)) .js-small').next().addClass('disabled','disabled');
   //       $('td.switching .switchery').trigger('click'); 
   //       $(".services").prop('checked', $(this).prop(''));  
   //    }
   // });



   // $(".checkall_reminder").change(function(){
   
   //     //  alert('ok');
   //       if($(this).is(':checked')){  

         
   //          $('td.splwitch .switchery').trigger('click'); 
   //          $(".reminder").prop('checked', $(this).prop('checked')); 
   
   //         // $("myReminders").hide();
   //       }else{
         
   //          $('td.splwitch .switchery').trigger('click'); 
   //          $(".reminder").prop('checked', $(this).prop(''));  
   //       }
   //    });

      //           $(".checkall_text").change(function(){
   
      //  //  alert('ok');
      //    if($(this).is(':checked')){  

         
      //       $('td.textwitch .switchery').trigger('click'); 
      //       $(".text").prop('checked', $(this).prop('checked')); 
   
      //      // $("myReminders").hide();
      //    }else{
         
      //       $('td.textwitch .switchery').trigger('click'); 
      //       $(".text").prop('checked', $(this).prop(''));  
      //    }
      // });

      //             $(".checkall_notification").change(function(){
   
      //  //  alert('ok');
      //    if($(this).is(':checked')){  

         
      //       $('td.invoicewitch .switchery').trigger('click'); 
      //       $(".invoice").prop('checked', $(this).prop('checked')); 
   
      //      // $("myReminders").hide();
      //    }else{
         
      //       $('td.invoicewitch .switchery').trigger('click'); 
      //       $(".invoice").prop('checked', $(this).prop(''));  
      //    }
      // });


 });
   

   
   //$('.js-small').change(function() {
      /** 12-09-2018 **/
       $(document).on('change','.as_per_firm',function(){
        if($(this).prop('checked') == true)
         {
            $(this).closest('.panel-collapse').find('input[type="checkbox"]:checked.cus_reminder').trigger('click');                  
         }
       });

$(document).ready(function(){

   $('input.js-small.f-right.services').each(function(){
      $(this).addClass('rsptrspt');
      $(this).trigger('change');
     // $(this).removeClass('rsptrspt');
   // console.log('trigger change');
   });
    $('#myReminders').modal('hide');
});
   </script>

<script type="text/javascript" src="<?php echo base_url()?>assets/js/add_client/checkbox_toggleAction.js"></script>


   <script>
   
 $(document).ready(function(){

        $('.checkall').parents('tr').find("th input").each(function(){
          if( $(this).is(":checked") ) $(this).parent().find('.switchery').removeClass('disabled');     
        });
   

  //console.log(ch.length+"checkall");

   $('.contactupdate').click(function(){
    $(".LoadingImage").show();
   
   var Contact_id = $(this).data("id");
   /*var formData = new FormData($("#contact_info"+Contact_id));
   alert("#contact_info"+Contact_id);*/
   /*var datas = $("#contact_info"+Contact_id).serialize();
   alert(datas);
   */
   /*  var formElement = document.getElementById("#contact_info"+Contact_id);
        var formData = new FormData(formElement);*/
      // alert(formData);
   
      /* var rec = getFormData('contact_info'+Contact_id);
       alert(JSON.stringify(rec))*/
   //alert(Contact_id);
   var data = {};
   
   data['title'] = $("#title"+Contact_id).val();
   data['first_name'] = $("#first_name"+Contact_id).val();
   data['middle_name'] = $("#middle_name"+Contact_id).val();
   data['surname'] = $("#surname"+Contact_id).val();
   data['preferred_name'] = $("#preferred_name"+Contact_id).val();
   data['mobile'] = $("#mobile"+Contact_id).val();
   data['main_email'] = $("#main_email"+Contact_id).val();
   data['nationality'] = $("#nationality"+Contact_id).val();
   data['psc'] = $("#psc"+Contact_id).val();
   data['ni_number'] = $("#ni_number"+Contact_id).val();
   data['address_line1'] = $("#address_line1"+Contact_id).val();
   data['address_line2'] = $("#address_line2"+Contact_id).val();
   data['town_city'] = $("#town_city"+Contact_id).val();
   data['post_code'] = $("#post_code"+Contact_id).val();
   data['landline'] = $("#landline"+Contact_id).val();
   data['work_email'] = $("#work_email"+Contact_id).val();
   data['date_of_birth'] = $("#date_of_birth"+Contact_id).val();
   data['nature_of_control'] = $("#nature_of_control"+Contact_id).val();
   data['marital_status'] = $("#marital_status"+Contact_id).val();
   data['utr_number'] = $("#utr_number"+Contact_id).val();
   data['shareholder'] = $("#shareholder"+Contact_id).val();
   data['contact_type'] = $("#contact_type"+Contact_id).val();
   
   data['Contact_id'] = Contact_id;
   $.ajax({
   url: '<?php echo base_url();?>client/updates_clientcontact/',
    type: "POST",
    data: data,
    success: function(data)  
    {
      $('.succ').hide();
      $("#succ"+Contact_id).show();
   $(".LoadingImage").hide();
   
    }
   });
   //alert(JSON.stringify($("#title"+Contact_id)));
   /*$.ajax({
                                url: '<?php echo base_url();?>client/updates_clientcontact/',
                                dataType : 'json',
                                type : 'POST',
                                data : data,
                                contentType : false,
                                processData : false,
                                success: function(data) {
                                alert('ffff');
                                },
                                error: function() {
                                     $(".LoadingImage").hide();
                                    alert('failed');}
                            });*/
   });
   
   
   function getFormData(formId) {
    return $('#' + formId).serializeArray().reduce(function (obj, item) {
        var name = item.name,
            value = item.value;
   
        if (obj.hasOwnProperty(name)) {
            if (typeof obj[name] == "string") {
                obj[name] = [obj[name]];
                obj[name].push(value);
            } else {
                obj[name].push(value);
            }
        } else {
            obj[name] = value;
        }
        return obj;
    }, {});
   }
   
   
   /*$(".delcontact").on('click',function(e){
   e.preventDefault();
   id = $(this).data('value');
   //alert(id);
   $.ajax({
      type:"POST",
      url:"<?php echo base_url().'client/delete_contact/'?>",
      data:"id="+id,
      cache: false,  
      success:
      function(data)  
      {
   
      $(".remove"+id).remove();
      $('#modalcontact'+id).modal('hide');
      return false;
      
      }
    });
   return false;
   });*/
   // delete new person
   $(".delnewperson").on('click',function(e){
   e.preventDefault();
   //id = $(this).data('value');
   $(".removenew").remove();
   $('#modalnewperson').modal('hide');
   return false;
   });
   
   $("#select_responsible_type").change(function(){
   var rec = $(this).val();
   if(rec=='staff')
   {
   $('.responsible_user_tbl').show();
   $('.responsible_team_tbl').show();
   $('.responsible_department_tbl').show();
   //$('.responsible_member_tbl').show();
   
   }else if(rec=='team')
   {
   $('.responsible_team_tbl').show();
   $('.responsible_user_tbl').show();
   $('.responsible_department_tbl').show();
 //  $('.responsible_member_tbl').show();
   }else if(rec=='departments')
   {
   $('.responsible_department_tbl').show();
   $('.responsible_user_tbl').show();
   $('.responsible_team_tbl').show();
 //  $('.responsible_member_tbl').show();
   }else if(rec=='members')
   {
 //  $('.responsible_member_tbl').show();
   $('.responsible_user_tbl').show();
   $('.responsible_team_tbl').show();
   $('.responsible_department_tbl').show();
   
   }
   else{
      $('.responsible_user_tbl').hide();
      $('.responsible_team_tbl').hide();
      $('.responsible_department_tbl').hide();
    //  $('.responsible_member_tbl').hide();
   }
   });
   
   /* $("#select_responsible_type").change(function(){
   //alert($(this).val());
   var rec = $(this).val();
   if(rec=='staff')
   {
   $('.responsible_user_tbl').show();
   $('.responsible_team_tbl').hide();
   $('.responsible_department_tbl').show();
   $('.responsible_member_tbl').hide();
   
   }else if(rec=='team')
   {
   $('.responsible_team_tbl').show();
   $('.responsible_user_tbl').hide();
   $('.responsible_department_tbl').hide();
   $('.responsible_member_tbl').hide();
   }else if(rec=='departments')
   {
   $('.responsible_department_tbl').show();
   //$('.responsible_user_tbl').hide();
   $('.responsible_team_tbl').hide();
   $('.responsible_member_tbl').hide();
   }else if(rec=='members')
   {
   $('.responsible_member_tbl').show();
   $('.responsible_user_tbl').hide();
   $('.responsible_team_tbl').hide();
   $('.responsible_department_tbl').hide();
   
   }
   else{
   $('.responsible_user_tbl').hide();
   $('.responsible_team_tbl').hide();
   $('.responsible_department_tbl').hide();
   $('.responsible_member_tbl').hide();
   }
   });*/

    // $("#person").change(function(){
    //   var person = $(':selected',this).val();
 $(".person").click(function(){


    var companyNo=$("#company_number").val();
    var user_id = <?php echo $uri_seg; ?>
      
      $.ajax({
          url: '<?php echo base_url();?>client/selectcompany/'+user_id,
          type: 'post',
          dataType: 'JSON',
          data: { 'companyNo':companyNo},
          beforeSend : function(){$('.LoadingImage').show();},
          success: function( data ){
            $('#company_house_contact').find('.main_contacts').html(data.html).show().find('.view_contacts').hide();
            $('#company_house_contact').modal('show');
            $('.LoadingImage').hide();
         },
         error: function( errorThrown ){
            $('#company_house_contact').find('.main_contacts').html('Data Not found.').show();
            $('#company_house_contact').modal('show');
            $('.LoadingImage').hide();
         }

      });
   
      
 });   

  /* Add_New_Contact for us **/
  $("#Add_New_Contact").click(function(){

      $('.for_contact_validation').html('');
      $('#main_Contact_error').html('');
       var cnt = $("#append_cnt").val();
   
               if(cnt==''){
               cnt = 1;
              }else{
               cnt = parseInt(cnt)+1;
              }
           $("#append_cnt").val(cnt).trigger('keyup');
   
          $.ajax({
             url: '<?php echo base_url();?>client/new_contact/',
             type: 'post',
             
             data: {'cnt':cnt,'incre':'1'},

              beforeSend: function() {
                        $(".LoadingImage").show();
                      },
             success: function( data ){
   
                 $('.contact_form').append(data);

                     $(".LoadingImage").hide();
                   var start = new Date();
                   start.setFullYear(start.getFullYear() - 70);
                   var end = new Date();
                   end.setFullYear(end.getFullYear());
                 $(".date_picker_dob").datepicker({ dateFormat: 'dd-mm-yy',changeMonth: true,
        changeYear: true,yearRange: start.getFullYear() + ':' + end.getFullYear() }).val();
                
              
                 $('.make_a_primary').each(function(){
                     var countofdiv=$('.make_a_primary').length;
                     if(countofdiv>1)
                     {
                        var id=$(this).attr('id').split('-')[1];
                        $('.for_remove-'+id).remove();
                        $('.for_row_count-'+id).append('<div class="btn btn-danger remove for_remove-'+id+'"><a href="javascript:void(0)" class="contact_remove" id="remove-'+id+'">Remove</a></div>');
                     }
   
                  });
                 /** end of remove button **/
                   $(".date_picker").datepicker({ dateFormat: 'dd-mm-yy',
                      changeMonth: true,
                     changeYear: true, }).val();
               
   
                 },
                 error: function( errorThrown ){
                     console.log( errorThrown );
                 }
             });
    
  });

   // Other custom label
   $(document).on('change','.othercus',function(e){
   //$(".othercus").change(function(){
   var custom = $(':selected',this).val();
   var client_id = $('#user_id').val();
    //alert(custom);
    
   if(custom=='Other')
   {
   // $('.contact_type').next('span').show();
   $(this).closest('.contact_type').next('.spnMulti').show();
   } else {
   $('.contact_type').next('span').hide();
   }
   
   
   }); 
   
   //Assign Other custom label
   $(document).on('change','.assign_cus',function(e){
   //$(".othercus").change(function(){
   var custom = $(':selected',this).val();
   var client_id = $('#user_id').val();
    //alert(custom);
    
   if(custom=='Other_Custom')
   {
   // $('.contact_type').next('span').show();
   $(this).closest('.assign_cus_type').next('.spanassign').show();
   } else {
   $(this).closest('.assign_cus_type').next('.spanassign').hide();
   }
   
   
   }); 
   
   });
   
</script>
<script>
   /* $( "#searchCompany" ).autocomplete({
          source: function(request, response) {
              //console.info(request, 'request');
              //console.info(response, 'response');
   
              $.ajax({
                  //q: request.term,
                  url: "<?=site_url('client/SearchCompany')?>",
                  data: { term: $("#searchCompany").val()},
                  dataType: "json",
                  type: "POST",
                  success: function(data) {
                      //alert(JSON.stringify(data.searchrec));
                      //add(data.searchrec);
                      //console.log(data);
                      $(".ui-autocomplete").css('display','block');
                      response(data.searchrec);
                  }
              
              });
          },
          minLength: 2
      });*/
   /*$("#searchCompany").keyup(function(){
       var currentRequest = null;    
       var term = $(this).val();
       //var term = $(this).val().replace(/ +?/g, '');
       //var term = $(this).val($(this).val().replace(/ +?/g, ''));
       $("#searchresult").show();
       $('#selectcompany').hide();
        $(".LoadingImage").show();
       currentRequest = $.ajax({
          url: '<?php echo base_url();?>client/SearchCompany/',
          type: 'post',
          data: { 'term':term },
          beforeSend : function()    {           
          if(currentRequest != null) {
              currentRequest.abort();
          }
      },
          success: function( data ){
              $("#searchresult").html(data);
               $(".LoadingImage").hide();
              },
              error: function( errorThrown ){
                  console.log( errorThrown );
              }
          });
     });
   var xhr = null;
   function getCompanyRec(companyNo)
   {
      $(".LoadingImage").show();
       if( xhr != null ) {
                  xhr.abort();
                  xhr = null;
          }
       
      xhr = $.ajax({
          url: '<?php echo base_url();?>client/CompanyDetails/',
          type: 'post',
          data: { 'companyNo':companyNo },
          success: function( data ){
              //alert(data);
              $("#searchresult").hide();
              $('#companyprofile').show();
              $("#companyprofile").html(data);
              $(".LoadingImage").hide();
              
              },
              error: function( errorThrown ){
                  console.log( errorThrown );
              }
          });}
   
   
      function getCompanyView(companyNo)
   {
      //$('.modal-search').hide();
      $(".LoadingImage").show();
     var user_id= $("#user_id").val();
       if( xhr != null ) {
                  xhr.abort();
                  xhr = null;
          }
   
       
      xhr = $.ajax({
          url: '<?php echo base_url();?>client/selectcompany/',
          type: 'post',
          dataType: 'JSON',
          data: { 'companyNo':companyNo,'user_id': user_id},
          success: function( data ){
              //alert(data);
              $("#searchresult").hide();
              $('#companyprofile').hide();
              $('#selectcompany').show();
              $("#selectcompany").html(data.html);
              $('.main_contacts').html(data.html);
   // append form field value
   $("#company_house").val('1');
   $("#company_name").val(data.company_name);
   $("#company_name1").val(data.company_name);
   $("#company_number").val(data.company_number);
   $("#companynumber").val(data.company_number);
   $("#company_url").val(data.company_url);
   $("#company_url_anchor").attr("href",data.company_url);
   $("#officers_url").val(data.officers_url);
   $("#officers_url_anchor").attr("href",data.officers_url);
   $("#company_status").val(data.company_status);
   $("#company_type").val(data.company_type);
   //$("#company_sic").append(data.company_sic);
   $("#company_sic").val(data.company_sic);
   $("#sic_codes").val(data.sic_codes);
   
   $("#register_address").val(data.address1+"\n"+data.address2+"\n"+data.locality+"\n"+data.postal);
   $("#allocation_holder").val(data.allocation_holder);
   $("#date_of_creation").val(data.date_of_creation);
   $("#period_end_on").val(data.period_end_on);
   $("#next_made_up_to").val(data.next_made_up_to);
   $("#next_due").val(data.next_due);
   $("#confirm_next_made_up_to").val(data.confirm_next_made_up_to);
   $("#confirm_next_due").val(data.confirm_next_due);
   
   $("#tradingas").val(data.company_name);
   $("#business_details_nature_of_business").val(data.nature_business);
   $("#user_id").val(data.user_id);
   
   $(".LoadingImage").hide();
              
              },
              error: function( errorThrown ){
                  console.log( errorThrown );
              }
          });}
   */
    
   
   
   
   function formSubmit ( formID ) {
   
   $(".LoadingImage").show();
   
   
   //var formId = $(this).closest("form").attr('id');
   
   var formData = $("#contact_exist"+formID).serialize();
   
   var Contact_id = $('#user_id').val();
   
   var data = {};
   data['Contact_id'] = Contact_id;
   data['formdata'] = formData;
   $.ajax({
   url: '<?php echo base_url();?>client/insert_clientcontact/'+Contact_id,
    type: "POST",
    data: data,
    success: function(data)  
    {
      $('.succ').hide();
      $("#succ").show();
   $(".LoadingImage").hide();
   
    }
   });
   
   //});
   }
   
   function make_a_primary(id,clientId)
   {
   $(".LoadingImage").show();
   
   //alert(id);
   var data = {};
   data['contact_id'] = id;
   data['clientId'] = clientId;
   
   $.ajax({
   url: '<?php echo base_url();?>client/update_primary_contact/',
    type: "POST",
    data: data,
    success: function(data)  
    {
        $("#contactss").html(data);
          $(".LoadingImage").hide();
          $(".contactcc").hide();
   $('.make_a_primary').each(function(){
   var countofdiv=$('.make_a_primary').length;
   if(countofdiv>1){
   var id=$(this).attr('id').split('-')[1];
   $('.for_remove-'+id).remove();
 $('.for_row_count-'+id).append('<div class="btn btn-danger remove for_remove-'+id+'"><a href="javascript:void(0)" class="contact_remove" id="remove-'+id+'">Remove</a></div>');
}

});
         // location.reload();
   
      //alert('success');
    }
   });
   
   }
  
  


   $('.dropdown-sin-21').dropdown({
      limitCount: 5,
      input: '<input type="text" maxLength="20" placeholder="Search">'
    });

$(document).ready(function() {

   $('.dropdown-sin-2').dropdown({
  
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });
   $('.dropdown-sin-3').dropdown({
  
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });
      $('.dropdown-sin-4').dropdown({
  
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });

   $('.dropdown-sin-5').dropdown({
  
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });
   $('.dropdown-sin-11').dropdown({
     
        input: '<input type="text" maxLength="20" placeholder="Search">'
      });


});
  $(document).on('change','#source',function(){
         var value=$(this).val();
         if(value=='Existing Client')
         {
            $('.dropdown_source').css('display','');
            $('.textfor_source').css('display','none');
         }
         else
         {
            $('.dropdown_source').css('display','none');
            $('.textfor_source').css('display','');
         }
  });
</script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
   $(document).ready(function() {
   $("#alltask").dataTable({
         "iDisplayLength": 10,
      "scrollX": true,
      });
   });
</script>
<script type="text/javascript">
   /*function check_checkbox()
          {
   
                  if($('[name="confirmation_next_reminder_date"]:checked').length < 1)
                  {
                    $("#add_custom_reminder").fadeIn(2000);
                  $("#add_custom_reminder_label").fadeIn(2000);              
                  $("#add_custom_reminder_link").hide();
                  }else
                  {
                     $("#add_custom_reminder").hide();
                     $("#add_custom_reminder_label").hide();
                     $("#add_custom_reminder_link").fadeIn(2000);
                  }
   
    
         }
          function check_checkbox1()
          {
   
                  if($('[name="accounts_next_reminder_date"]:checked').length < 1)
                  {
                    $("#accounts_custom_reminder").fadeIn(2000);
                  $("#accounts_custom_reminder_label").fadeIn(2000);
                  $("#accounts_custom_reminder_link").hide();             
   
                  }else
                  {
                     $("#accounts_custom_reminder").hide();
                     $("#accounts_custom_reminder_label").hide();
                     $("#accounts_custom_reminder_link").fadeIn(2000);
                  }
   
    
         }
          function check_checkbox2()
          {
   
                  if($('[name="company_next_reminder_date"]:checked').length < 1)
                  {
                     //alert("fdg");
                    $("#company_custom_reminder").fadeIn(2000);
                  $("#company_custom_reminder_label").fadeIn(2000);
                  $("#company_custom_reminder_link").hide();              
   
                  }else
                  {
                     $("#company_custom_reminder").hide();
                     $("#company_custom_reminder_label").hide();
                     $("#company_custom_reminder_link").fadeIn(2000);
                  }
   
    
         }
         function check_checkbox3()
          {
   
                  if($('[name="Personal_next_reminder_date"]:checked').length < 1)
                  {
                    $("#personal_custom_reminder").fadeIn(2000);
                  $("#personal_custom_reminder_label").fadeIn(2000);
                  $("#personal_custom_reminder_link").hide();             
   
                  }else
                  {
                     $("#personal_custom_reminder").hide();
                     $("#personal_custom_reminder_label").hide();
                     $("#personal_custom_reminder_link").fadeIn(2000);
                  }
   
    
         }
         function check_checkbox4()
          {
   
                  if($('[name="Payroll_next_reminder_date"]:checked').length < 1)
                  {
                    $("#payroll_add_custom_reminder").fadeIn(2000);
                  $("#payroll_add_custom_reminder_label").fadeIn(2000);
                  $("#payroll_add_custom_reminder_link").hide();             
   
                  }else
                  {
                     $("#payroll_add_custom_reminder").hide();
                     $("#payroll_add_custom_reminder_label").hide();
                     $("#payroll_add_custom_reminder_link").fadeIn(2000);
                  }
   
    
         }
         function check_checkbox5()
          {
   
                  if($('[name="Pension_next_reminder_date"]:checked').length < 1)
                  {
                    $("#pension_add_custom_reminder").fadeIn(2000);
                  $("#pension_create_task_reminder_label").fadeIn(2000);
                  $("#pension_create_task_reminder_link").hide();            
   
                  }else
                  {
                     $("#pension_add_custom_reminder").hide();
                     $("#pension_create_task_reminder_label").hide();
                     $("#pension_create_task_reminder_link").fadeIn(2000);
                  }
   
    
         }
         function check_checkbox6()
          {
   
                  if($('[name="Cis_next_reminder_date"]:checked').length < 1)
                  {
                    $("#cis_add_custom_reminder").fadeIn(2000);
                  $("#cis_add_custom_reminder_label").fadeIn(2000);
                  $("#cis_add_custom_reminder_link").hide();              
   
                  }else
                  {
                     $("#cis_add_custom_reminder").hide();
                     $("#cis_add_custom_reminder_label").hide();
                     $("#cis_add_custom_reminder_link").fadeIn(2000);
                  }
   
    
         }
         function check_checkbox7()
          {
   
                  if($('[name="P11d_next_reminder_date"]:checked').length < 1)
                  {
                    $("#p11d_add_custom_reminder").fadeIn(2000);
                  $("#p11d_add_custom_reminder_label").fadeIn(2000);
                  $("#p11d_add_custom_reminder_link").hide();             
   
                  }else
                  {
                     $("#p11d_add_custom_reminder").hide();
                     $("#p11d_add_custom_reminder_label").hide();
                     $("#p11d_add_custom_reminder_link").fadeIn(2000);
                  }
   
    
         }
         function check_checkbox8()
          {
   
                  if($('[name="bookkeep_next_reminder_date"]:checked').length < 1)
                  {
                    $("#bookkeep_add_custom_reminder").fadeIn(2000);
                  $("#bookkeep_add_custom_reminder_label").fadeIn(2000);
                  $("#bookkeep_add_custom_reminder_link").hide();            
   
                  }else
                  {
                     $("#bookkeep_add_custom_reminder").hide();
                     $("#bookkeep_add_custom_reminder_label").hide();
                     $("#bookkeep_add_custom_reminder_link").fadeIn(2000);
                  }
   
    
         }
         function check_checkbox9()
          {
   
                  if($('[name="manage_next_reminder_date"]:checked').length < 1)
                  {
                    $("#manage_add_custom_reminder").fadeIn(2000);
                  $("#manage_add_custom_reminder_label").fadeIn(2000);
                  $("#manage_add_custom_reminder_link").hide();              
   
                  }else
                  {
                     $("#manage_add_custom_reminder").hide();
                     $("#manage_add_custom_reminder_label").hide();
                     $("#manage_add_custom_reminder_link").fadeIn(2000);
                  }
   
    
         }
         function check_checkbox10()
          {
   
                  if($('[name="Vat_next_reminder_date"]:checked').length < 1)
                  {
                    $("#vat_add_custom_reminder").fadeIn(2000);
                  $("#vat_add_custom_reminder_label").fadeIn(2000);
                  $("#vat_add_custom_reminder_link").hide();              
   
                  }else
                  {
                     $("#vat_add_custom_reminder").hide();
                     $("#vat_add_custom_reminder_label").hide();
                     $("#vat_add_custom_reminder_link").fadeIn(2000);
                  }
   
    
         }
         function check_checkbox11()
          {
   
                  if($('[name="Declatrion_next_reminder_date"]:checked').length < 1)
                  {
                    $("#Declration_add_custom_reminder").fadeIn(2000);
                  $("#Declration_add_custom_reminder_label").fadeIn(2000);
                  $("#Declration_add_custom_reminder_link").hide();             
   
                  }else
                  {
                     $("#Declration_add_custom_reminder").hide();
                     $("#Declration_add_custom_reminder_label").hide();
                     $("#Declration_add_custom_reminder_link").fadeIn(2000);
                  }
   
    
         }
         function check_checkbox12()
          {
   
                  if($('[name="cissub_next_reminder_date"]:checked').length < 1)
                  {
                    $("#cissub_add_custom_reminder").fadeIn(2000);
                  $("#cissub_add_custom_reminder_label").fadeIn(2000);
                  $("#cissub_add_custom_reminder_link").hide();              
   
                  }else
                  {
                     $("#cissub_add_custom_reminder").hide();
                     $("#cissub_add_custom_reminder_label").hide();
                     $("#cissub_add_custom_reminder_link").fadeIn(2000);
                  }
   
    
         }
   
         function check_checkbox13()
          {
   
                  if($('[name="insurance_next_reminder_date"]:checked').length < 1)
                  {
                    $("#insurance_add_custom_reminder").fadeIn(2000);
                  $("#insurance_add_custom_reminder_label").fadeIn(2000);
                  $("#insurance_add_custom_reminder_link").hide();              
   
                  }else
                  {
                     $("#insurance_add_custom_reminder").hide();
                     $("#insurance_add_custom_reminder_label").hide();
                     $("#insurance_add_custom_reminder_link").fadeIn(2000);
                  }
   
    
         }
         function check_checkbox14()
          {
   
                  if($('[name="registered_next_reminder_date"]:checked').length < 1)
                  {
                    $("#registered_add_custom_reminder").fadeIn(2000);
                  $("#registered_add_custom_reminder_label").fadeIn(2000);
                  $("#registered_add_custom_reminder_link").hide();              
   
                  }else
                  {
                     $("#registered_add_custom_reminder").hide();
                     $("#registered_add_custom_reminder_label").hide();
                     $("#registered_add_custom_reminder_link").fadeIn(2000);
                  }
   
    
         }
         
         function check_checkbox15()
          {
   
                  if($('[name="investigation_next_reminder_date"]:checked').length < 1)
                  {
                    $("#investigation_add_custom_reminder").fadeIn(2000);
                  $("#investigation_add_custom_reminder_label").fadeIn(2000);
                  $("#investigation_add_custom_reminder_link").hide();              
   
                  }else
                  {
                     $("#investigation_add_custom_reminder").hide();
                     $("#investigation_add_custom_reminder_label").hide();
                     $("#investigation_add_custom_reminder_link").fadeIn(2000);
                  }
   
    
         }*/
      
      
</script>
<script type="text/javascript">
   /* $( document ).ready(function() {
   
        $("#add_custom_reminder").hide();
        $("#add_custom_reminder_label").hide();
   
        $("#accounts_custom_reminder").hide();
        $("#accounts_custom_reminder_label").hide();
   
        $("#company_custom_reminder").hide();
        $("#company_custom_reminder_label").hide();
   
        $("#personal_custom_reminder").hide();
        $("#personal_custom_reminder_label").hide();
   
        $("#payroll_add_custom_reminder").hide();
        $("#payroll_add_custom_reminder_label").hide();
   
        $("#pension_add_custom_reminder").hide();
        $("#pension_create_task_reminder_label").hide();
   
        $("#cis_add_custom_reminder").hide();
        $("#cis_add_custom_reminder_label").hide();
   
        $("#p11d_add_custom_reminder").hide();
        $("#p11d_add_custom_reminder_label").hide();
   
        $("#bookkeep_add_custom_reminder").hide();
        $("#bookkeep_add_custom_reminder_label").hide();
   
        $("#manage_add_custom_reminder").hide();
        $("#manage_add_custom_reminder_label").hide();
   
        $("#vat_add_custom_reminder").hide();
        $("#vat_add_custom_reminder_label").hide();
   
         $("#Declration_add_custom_reminder").hide();
        $("#Declration_add_custom_reminder_label").hide();
   
         $("#cissub_add_custom_reminder").hide();
        $("#cissub_add_custom_reminder_label").hide();
   
        $("#insurance_add_custom_reminder").hide();
        $("#insurance_add_custom_reminder_label").hide();
   
        $("#registered_add_custom_reminder").hide();
        $("#registered_add_custom_reminder_label").hide();
   
        $("#investigation_add_custom_reminder").hide();
        $("#investigation_add_custom_reminder_label").hide();
   
       });*/
   
   
   
   $(document).on('keyup',".allocation_holder_cls",function(){
       var album_text = [];
   
   $("input[name='allocation_holder[]']").each(function() {
     var value = $(this).val();
     if (value) {
         album_text.push(value);
     }
   });
   if (album_text.length === 0) {
   $('.yk_pack_addrow_td').css('display','none');
   }
   
   else {
   $('.yk_pack_addrow_td').css('display','block');
   }
   });
   
   $(document).on('keyup',".allocation_holder_dept_cls",function(){
   var album_text = [];
   $("input[name='allocation_holder_dept[]']").each(function() {
     var value = $(this).val();
     if (value) {
         album_text.push(value);
     }
   });
   if (album_text.length === 0) {
   $('.yk_pack_addrow_tr').css('display','none');
   }
   
   else {
   $('.yk_pack_addrow_tr').css('display','block');
   }
   });
   
</script>

<script type="text/javascript">
   $(document).ready(function(){  
        $('#refer_exist_client').keyup(function(){  
         // alert('text');
             var query = $(this).val();  
             if(query != '')  
             {  
                  $.ajax({  
                       url:"<?php echo base_url();?>client/search_client/",  
                       method:"POST",  
                       data:{query:query},  
                       success:function(data)  
                       {  
                            $('#searchresult').fadeIn();  
                            $('#searchresult').html(data);  
                       }
                  });  
             }
             if(query=='')
             {
               $('#searchresult').fadeOut();
             }  
        });  
        $(document).on('click', '.client_name', function(){  
             $('#refer_exist_client').val($(this).text()); 
             //$('#user_id').val($(this).data('id')); 
             $('#searchresult').fadeOut();  
        });  
   });
</script>
<script type="text/javascript">
   $(document).ready(function(){  
        $(document).on('change','#placeholder',function(e){ 
         // alert('text');
             var query = $(this).val();  
             if(query != '')  
             {  
                  $.ajax({  
                       url:"<?php echo base_url();?>client/get_placeholder/",  
                       method:"POST",  
                       data:{query:query},  
                       success:function(data)  
                       {  
                            $('.append_placeholder').fadeIn();  
                            $('.append_placeholder').html(data); 
        $(".add_merge").on('click',function() { // bulk checked
        var target = $(this).attr('target');
        var name=$(this).html();
        
        $('[name='+target+']').append(name);
       
      }); 
                       }
                  });  
             }
             if(query=='')
             {
               $('.append_placeholder').fadeOut();
             }  
        }); 
   
   
   });
</script>
<script type="text/javascript">
   $(document).ready(function(){
  $(".reminder_close").click(function(){
         $('input[name="'+$('#clicked_checkbox_name').val()+'"]').trigger('click');
       }); 
  $(".email_template").change(function(){
    console.log('inside on chnage #email_template');
  var id=$(this).val();
   $.ajax({
           url: '<?php echo base_url();?>User/get_template',
           type : 'POST',
           data : {'id':id},                     
           beforeSend: function()
           {
               $(".LoadingImage").show();
           },
           success: function(msg)
           {
               $(".LoadingImage").hide();                   
               var json = JSON.parse(msg); 
               $("#add-reminder .invoice-msg").val(json['subject']);
               $("#add-reminder #editor2").html(json['body']);
            }
         });
  });
   $(".edit_field_form").submit(function () {

           // console.log($(".edit_field_form").find("input[name='service_id']").val());
      
            var clikedForm = $(this); // Select Form
            var user_id = $('#user_id').val();
           var days = clikedForm.find("input[name='days']").val().trim();
            var subject = clikedForm.find("input[name='subject']").val().trim();
            var body = clikedForm.find("textarea[name='message']").val().trim();
            if(days=='')
            {
               $('.error_msg_days').show();
            }
            else
            {
               $('.error_msg_days').hide();
            }
            if(subject=='')
            {
               $('.error_msg_subject').show();
            }
            else
            {  

               $('.error_msg_subject').hide();
            }
            if(body=='')
            {

               $('.error_msg_message').show();
            }
            else
            {

               $('.error_msg_message').hide();
            }
             if(days!='' && subject!='' && body!='')
            {
                  
               $.ajax({
               type: "POST",
               dataType:'json',
               url: '<?php echo base_url();?>client/AddCustom_ServiceReminder/',
               data: $(this).serialize()+"&user_id="+user_id,
               beforeSend:Show_LoadingImg,
               success: function (data) { 
                  // Inserting html into the result div
                  //console.log(data);
                  Hide_LoadingImg();
                  if(data.result != 0)
                  {
                     $('input[name="'+$("#clicked_checkbox_name").val()+'"]').val(data.result);
                     $('.alert-success-reminder').show();
                     setTimeout(function() { 
                        $('.alert-success-reminder').hide();
                        //$('#add-reminder').css('display','none');
                        $('#add-reminder').modal('hide');
                        $('.modal-backdrop.show').hide();
                     },1000);
                  }
             
               },
               error: function(jqXHR, text, error){
                  // Displaying if there are any errors
                     $('#result').html(error);           
              }
          });
            return false;
            }
            else
            {
                return false;
            }
           
           
      
          });
     $('a.date-editop[id^="cus_reminder"]').click(function(){
        var id = $(this).attr('id').split('_');
        $.ajax(
          {
            url:"<?php echo base_url()?>client/Get_CusReminder_Data/"+id[2],
            dataType :'json',
            beforeSend:Show_LoadingImg,
            success:function(data) 
            {
              Hide_LoadingImg();
              $('#edit-reminder').find("select[name='due']").val(data.due_type);
              $('#edit-reminder').find("input[name='days']").val(data.due_days);
              $('#edit-reminder').find("input[name='subject']").val(data.subject);
              $('#edit-reminder').find("input[name='message']").html(data.body);
              $('#edit-reminder').find("input[name='table_id']").val(data.id);
              $('#edit-reminder').modal('show');
            }
          }
          )
     });
     $("#edit_cus_reminder_form").validate(
     {
      rules:
      {
        days:{required:true},
        subject:{required:true},
        due:{required:true},
        message:{required:true}
      },
      submitHandler:function()
      {
        var Form = $('#edit_cus_reminder_form');
        var FormData = Form.serialize();
        $.ajax({
            url:"<?php echo base_url()?>client/Update_CusReminder",
            dataType : 'json',
            type : 'POST',
            data : FormData,
            beforeSend:Show_LoadingImg,
            success:function(res)
            {
              Hide_LoadingImg();
              if(res.result)
              { 
                //update subject
                var table_id = Form.find('input[name="table_id"]').val();
                $(".cus_reminder_subject_"+table_id).html( Form.find('input[name="subject"]').val() );


                $('.alert-success-reminder').find('.message').text('Reminder Updated Successfully');
                $('.alert-success-reminder').show();
                setTimeout(function() {$('.alert-success-reminder').hide();$('#edit-reminder').modal('hide');},1000);
              }
            }
        });
      }

     }
      )
   });
</script>
<div id="edit_confirmation1" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" style="text-align-last: center">Confirmation</h4>
         </div>
         <div class="modal-body">
            <p>Are you want edit this information</p>
         </div>
         <div class="modal-footer profileEdit">
            <input type="hidden" name="hidden">
            <a href="javascript:void();" class="clients-detailspop" id="acompany_name" data-dismiss="modal" class="delcontact">Yes</a>
            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
         </div>
      </div>
   </div>
</div>
<!-- <script type="text/javascript">
   $(document).ready(function() {
       var $grid11 = $('.masonry-container').masonry({
         itemSelector: '  .accordion-panel',
         percentPosition: true,
         columnWidth: '  .grid-sizer' 
       });
   
         $(document).on('click', '.switchery,.dropdown-main li', function(){

         setTimeout(function(){ 
            $grid11.masonry('reloadItems');
            $grid11.masonry('layout'); 
   
         }, 600);  
         });
   
   
        $(document).on( 'click', 'td.splwitch .switchery', function() {
            $('.body-popup-content .accordion-panel').css("transform", "");
        });
   
       $(document).on( 'click', '.switchery', function() {
          $('.accordion-panel').css("transform", "");
        });
       $(document).on( 'change', '#select_responsible_type', function() {
      $('.accordion-panel').css("transform", "");
      });
      $(document).on( 'click', '.addnewclient_pages1 nav li a', function() {
      $('.accordion-panel').css("transform", "");
      });
   
   });
   
   
</script> -->
<script type="text/javascript">
   $(function() {
              $('.click_code03').on('click', function( e ) {
            var text = $(this).parents('.edit-field-popup1').find('.edit_classname').attr('id');
            $('[name="hidden"]').val(text);
          });
   });
   
   $(document).ready(function () {
   
           $('.profileEdit a').click(function (elem) {
        $('#'+$('[name="hidden"]').val()).attr("readonly", false); 
            var id=$('[name="hidden"]').val();
         if('date_of_creation'==id || 'confirmation_next_made_up_to'== id || 'confirmation_next_due'==id || 'accounts_next_made_up_to'==id || 'accounts_next_due'==id ){
   
          $('#'+id).addClass('dob_picker');
          $(".dob_picker").datepicker({ dateFormat: 'dd-mm-yy',
            //minDate:0,
             changeMonth: true,
        changeYear: true, }).val();
       } 
        $('.modal').modal('hide');
           });
   });
   
</script>
<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.1.8/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/masonry/3.3.1/masonry.pkgd.min.js"></script> -->
<script>
   function initdatepicker(){
            // alert("hi");
          // $(".datepicker").datepicker({ dateFormat: 'dd-mm-yy' });
           var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));
   
           elem.forEach(function(html) {
               var switchery = new Switchery(html, {
                   color: '#1abc9c',
                   jackColor: '#fff',
                   size: 'small'
               });
           });
   }
    
   
   $(document).ready(function(){
   
   //   $(".edit_classname").datepicker();
   
   
   //     var $grid = $('.masonry-container').masonry({
   //     itemSelector: '.accordion-panel',
   //     percentPosition: true,
   //     columnWidth: '.grid-sizer'
   // });
   
   // $(document).on( 'click', 'ol .nav-item a,.dropdown-main li', function() {
   //   setTimeout(function(){ $grid.masonry('layout'); }, 500);
   // });
   
   // $(document).on( 'change', '.main-pane-border1', function() {
   //   setTimeout(function(){ $grid.masonry('layout'); }, 500);
   // });
   
   
   });
   //  $(document).on("click", "#myReminders .datepicker", function() {
   //    // alert('hi');
   //    $(".datepicker").datepicker("show");
   // });
   
   $(".save").click(function(e) {
   
   
   
   
    $('.body-popup-content input, select, textarea').each(function () {
         var id = $(this).attr('id');    
   
      //console.log(id+$(this).prop('type'));
   
      //input box
      if($(this).prop('type') == 'text')
         $('#'+id).val($(this).val());
      
        if($(this).prop('type') == 'number')
         $('#'+id).val($(this).val()); 
   
       if($(this).prop('type') == 'select-one')
         $('#'+id).val($(this).val());
   
       if($(this).prop('type') == 'textarea')
        // $('#'+id).val($(this).val());   
   
      // if($(this).prop('type') == 'checkbox')
      //    $('#'+id).val($(this).val());
      
      // if($(this).prop('type') == 'select-one')
      //    $('#'+id).val($(this).val());
   
   
      if($(this).prop('type') == 'checkbox'){
         var id = $(this).attr('id');
         
         if($(this).attr('data-switchery'))
         {
            if($(this).prop('checked') != $('#'+id).prop('checked'))
            {
               $('#'+id).next('.switchery').trigger('click');
            }
         }
         else
         {
            if($('#'+id).prop('checked','true')){
       //  alert(id);
         $('#'+id).prop('checked','true');
         $('#'+id).parents(".col-sm-8").addClass("switttt");
         //$('#'+id).find();
          //$('div.switttt .switchery').trigger('click'); 
      }
         }
   
   
      
   }
   
   
   
   
   // var elem = document.querySelector('#'+id);
   // var switchery = new Switchery(elem);
   
   // document.querySelector('.js-dynamic-disable').addEventListener('click', function() {
   //   switchery.disable();
   // });
   
   // document.querySelector('.js-dynamic-enable').addEventListener('click', function() {
   //   switchery.enable();
   // });
      
         });  
          
   
     
   
   function textarea(){
    $('.body-popup-content textarea').each(function () {
            var id = $(this).attr('id');
               $('#'+id).val($(this).val());  
            if($(this).attr('type')!='textarea' && $(this).attr('type')!='input'){
                selectbox();
            }else{
               input();
            }        
         });  
   }
   function selectbox(){  
       $('.body-popup-content select').each(function () {
            var id = $(this).attr('id');       
               $('#'+id).val($(this).val());  
               if($(this).attr('type')!='select' && $(this).attr('type')!='textarea'){
                input();
            }else{
               textarea();
            }           
         });
   }
   
     });
   
   
   $('.atleta').click(function(e) {
   
      //alert('ok');
      e.preventDefault();
      var h = $("a",this).attr('href');
   
        $("#myAlert").modal(); 
   
        $(".exit").click(function(){
            window.location.href = h;
      });
      
   });
   
   $('#checked').click(function(e) {
    e.preventDefault();
    $("#company_house_reminder").prop('checked','true');
   
    //$('div#che_ck .switchery').trigger('click'); 
   
   });
   
   
</script>
<!-- 30-06-2018 -->
<script type="text/javascript">
$(document).ready(function(){
   //alert('zz');
/*<?php if($cmy_role==1){ ?>
        $('.basic_details_tab').show();
   
         $('.basic_details_tab').addClass('bbs');
         $('.basic_deatils_id').addClass('active');
         $('#basic_details').addClass('active');
         $('.for_contact_tab').removeClass('bbs');
         $('.main_contacttab').removeClass('active');
         $('#main_Contact').removeClass('active');
   <?php } ?>*/
 });

 $(document).on('change', '.sel-legal-form', function() {
   //alert('zzz');
   var selectVal = $(this).find(':selected').val();
   // alert(selectVal);
   // company type value
   
   $('#company_type').val(selectVal);
   $('#company_type').prop('readonly', true);
   
   if((selectVal == "Private Limited company") || (selectVal == "Public Limited company") || (selectVal == "Limited Liability Partnership")) {

      $('.filing-alcat-tab').show();
      $('.business-info-tab').hide();
      // $('.business-info-tab').show();
      /** 16-08-2018 shown shown basic details tab **/
      $('.basic_details_tab').show();
      /** end of 16-08-2018 **/
   }
   else if((selectVal == "Partnership") || (selectVal == "Self Assessment")) {

      $('.business-info-tab').show();
      $('.filing-alcat-tab').hide();
      /** 16-08-2018 shown hide basic details tab **/
      $('.basic_details_tab').hide();
      /** end of 16-08-2018 **/
   }
   else
   {
      $('.business-info-tab').hide();
      $('.filing-alcat-tab').hide();
      /** 16-08-2018 shown hide basic details tab **/
      $('.basic_details_tab').hide();
      /** end of 16-08-2018 **/
   }
   

 });  
 $('.sel-legal-form').trigger('change');

</script>
<!-- end of 30-06-2018 -->
<!-- 05-07-2018  -->
<script type="text/javascript">
   /** 05-07-2018 for more contact remove option  **/
     $(document).on('click','.contact_remove',function(){
    var countno=$(this).attr('id').split('-')[1];
    var parent = $('.common_div_remove-'+countno);

       var action  = function ()
       {
         if( parent.find('.contact_table_id').length )
         {
            var id = parent.find('.contact_table_id').val();
            $.ajax(
               {
                  url:"<?php echo base_url();?>client/Client_Contact_Delete/"+id,
                  beforeSend:Show_LoadingImg,
                  success:function(res){ Hide_LoadingImg();}
               });
         }
         parent.remove();           
         var countofdiv = $('.make_a_primary').length;
         if(countofdiv == 1)
         {
            $('.contact_remove').css('display','none');
            $('.btn.btn-danger.remove').css('display','none');
         }
         $('#Confirmation_popup').modal('hide');
      };
    
      Conf_Confirm_Popup({'OkButton':{'Handler':action},'Info':'Do You Want Remove This Contact...!'}); 
      $('#Confirmation_popup').modal('show');
    });


 $(document).ready(function(){
$('.make_a_primary').each(function(){
   var countofdiv=$('.make_a_primary').length;
   if(countofdiv>1){
   var id=$(this).attr('id').split('-')[1];
   $('.for_remove-'+id).remove();
 $('.for_row_count-'+id).append('<div class="btn btn-danger remove for_remove-'+id+'"><a href="javascript:void(0)" class="contact_remove" id="remove-'+id+'">Remove</a></div>');
}

});
 });
 /** end of more cotact remove **/
 /** 29-08-2018 **/
   $(document).on('click','.remove_file',function(){
    $('#image_name').val('');
    //$('.extra_view_40').hide();
      var id=$(this).attr('data-id');
    $('.for_img_'+id).remove();
   });
 /** end of 29-08-2018 **/
</script>
<!--end of 05-07-2018 -->
<script type="text/javascript">
         /** 29-08-2018 **/
      
      $('.assign_cus_type').find('.dropdown-sin-11').on('click',function(){

      $(this).find('.dropdown-main > ul > li').click(function(){
           var custom=$(this).attr('data-value');
           var customclass=$(this).attr('class');
          if(custom=='Other_Custom')
         {   
            //if(customclass=='dropdown-option dropdown-chose'){
            if (customclass.indexOf('dropdown-chose') > -1){
                   $('.for_other_custom_choose').css('display','none');
            }
            else
            {
               $('.for_other_custom_choose').css('display','');
            }          
         }
         else{     
          }
      
        
         });
         $(this).find('.dropdown-clear-all').on('click',function(){
            $('.for_other_custom_choose').css('display','none');
         });
      });


$(document).ready(function () {
  //called when key is pressed in textbox
  $(".due-days").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
              return false;
    }
   });
});


/** 11-09-2018 **/
   $(document).on('keyup',"input",function(){
   var name=$(this).attr('name');
   var thisval=$(this).val();
   var type=$(this).attr('type');
    if(name == 'company_name' && $('#company_url_anchor').length )
      {
         $('#company_url_anchor').text(thisval);
      }
   var vallen=$("[name^='"+name+"']").length;
   //alert(vallen);
   if(vallen >1 && (type=="text" || type=="number")){
          $("[name^='"+name+"']").each(function() {
           $(this).val(thisval);
         });
       }


 });
   $(document).on('keyup',"textarea",function(){
   var name=$(this).attr('name');
   var thisval=$(this).val();
   var vallen=$("[name^='"+name+"']").length;
   console.log(thisval);
   console.log(name);
   if(vallen >1){
          $("[name^='"+name+"']").each(function() {
           $(this).val(thisval);
         });
       }
  
   });

/** end of 11-09-2018 **/

/** 14-09-2018 for client section **/
$(document).on('click','.make_primary_section',function(){
//alert('ok');
$('.make_primary_section').removeClass('active');
   $(this).addClass('active');

    $(".LoadingImage").show();
   $('.for_contact_validation').html('');

   var its_data_id=$(this).attr('data-id');
   $('#'+its_data_id).trigger('click');
   
   $('.make_primary_section').each(function(){
      $(this).html('<span>Make A Primary</span>');
   });
   $(this).html('<span style="color:red;">Primary Contact</span>');
 $(".LoadingImage").hide();
});


$( document ).ready(function() {
var mylist = $('#details').find('#important_details_section');
var listitems = $('#details').find('#important_details_section').children('.sorting').get();
listitems.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems, function(idx, itm) { 
    mylist.append(itm);
});
var mylist1 = $('#confirmation-statement').find('#box1');
var listitems1 = $('#confirmation-statement').find('#box1').children('.sorting_conf').get();
listitems1.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems1, function(idx, itm) {
    mylist1.append(itm);
})
var mylist2 = $('#confirmation-statement').find('#box2');
var listitems2 = $('#confirmation-statement').find('#box2').children('.sorting_conf').get();
listitems2.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems2, function(idx, itm) { 
    mylist2.append(itm);
})
var mylist3 = $('#confirmation-statement').find('#box3');
var listitems3 = $('#confirmation-statement').find('#box3').children('.sorting_conf').get();
listitems3.sort(function(a, b) { 
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems3, function(idx, itm) { 
    mylist3.append(itm);
})
var mylist4 = $('#personal-tax-returns').find('#personal_box');
var listitems4 = $('#personal-tax-returns').find('#personal_box').children('.personal_sort').get();
listitems4.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems4, function(idx, itm) { 
    mylist4.append(itm);
})
var mylist4 = $('#personal-tax-returns').find('#personal_box1');
var listitems4 = $('#personal-tax-returns').find('#personal_box1').children('.personal_sort').get();
listitems4.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems4, function(idx, itm) { 
    mylist4.append(itm);
})
var mylist4 = $('#personal-tax-returns').find('#personal_box2');
var listitems4 = $('#personal-tax-returns').find('#personal_box2').children('.personal_sort').get();
listitems4.sort(function(a, b) { 
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems4, function(idx, itm) {
    mylist4.append(itm);
})
var mylist5 = $('#vat-Returns').find('#vat_box');
var listitems5 = $('#vat-Returns').find('#vat_box').children('.vat_sort').get();
listitems5.sort(function(a, b) { 
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems5, function(idx, itm) {
  // console.log(idx);
    mylist5.append(itm);
})
var mylist6 = $('#vat-Returns').find('#vat_box1');
var listitems6 = $('#vat-Returns').find('#vat_box1').children('.vat_sort').get();
listitems6.sort(function(a, b) {  
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems6, function(idx, itm) { 
    mylist6.append(itm);
})
var mylist7 = $('#management-account').find('#management_account');
var listitems7 = $('#management-account').find('#management_account').children('.management').get();
listitems7.sort(function(a, b) {  
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems7, function(idx, itm) {
    mylist7.append(itm);
})
var mylist8 = $('#management-account').find('#management_account1');
var listitems8 = $('#management-account').find('#management_account1').children('.management').get();
listitems8.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems8, function(idx, itm) {
    mylist8.append(itm);
})
var mylist9 = $('#management-account').find('#management_box1');
var listitems9 = $('#management-account').find('#management_box1').children('.management').get();
listitems9.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems9, function(idx, itm) {
    mylist9.append(itm);
})
var mylist10 = $('#management-account').find('#management_box2');
var listitems10 = $('#management-account').find('#management_box2').children('.management').get();
listitems10.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems10, function(idx, itm) {
    mylist10.append(itm);
})

var mylist11 = $('#investigation-insurance').find('#invest-box');
var listitems11 = $('#investigation-insurance').find('#invest-box').children('.invest').get();
listitems11.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems11, function(idx, itm) {
    mylist11.append(itm);
})
var mylist13 = $('#investigation-insurance').find('#invest_box2');
var listitems13 = $('#investigation-insurance').find('#invest_box2').children('.invest').get();
listitems13.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems13, function(idx, itm) {
 //  console.log(idx);
    mylist13.append(itm);
})
var mylist12 = $('#other').find('#others_section');
var listitems12 = $('#other').find('#others_section').children('.others_details').get();
listitems12.sort(function(a, b) { 
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems12, function(idx, itm) {
  // console.log(idx);
    mylist12.append(itm);
})
var mylist14 = $('#other').find('#other_details1');
var listitems14 = $('#other').find('#other_details1').children('.others_details').get();
listitems14.sort(function(a, b) { 
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems14, function(idx, itm) {
 //  console.log(idx);
    mylist14.append(itm);
})
var mylist15 = $('#referral').find('#referals');
var listitems15 = $('#referral').find('#referals').children('.referal_details').get();
listitems15.sort(function(a, b) { 
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems15, function(idx, itm) {
  // console.log(idx);
    mylist15.append(itm);
})
/* Accounts */
var mylist16 = $('#accounts').find('#accounts_box');
var listitems16 = $('#accounts').find('#accounts_box').children('.accounts_sec').get();
listitems16.sort(function(a, b) { 
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems16, function(idx, itm) {
 //  console.log(idx);
    mylist16.append(itm);
})
var mylist17 = $('#accounts').find('#accounts_box1');
var listitems17 = $('#accounts').find('#accounts_box1').children('.accounts_sec').get();
listitems17.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems17, function(idx, itm) {
 //  console.log(idx);
    mylist17.append(itm);
})
var mylist18 = $('#accounts').find('#accounts_box2');
var listitems18 = $('#accounts').find('#accounts_box2').children('.accounts_sec').get();
listitems18.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems18, function(idx, itm) {
 //  console.log(idx);
    mylist18.append(itm);
})
var mylist19 = $('#accounts').find('#accounts_box3');
var listitems19 = $('#accounts').find('#accounts_box3').children('.accounts_sec').get();
listitems16.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems19, function(idx, itm) {
  // console.log(idx);
    mylist19.append(itm);
})

var mylist20 = $('#accounts').find('#accounts_box4');
var listitems20 = $('#accounts').find('#accounts_box4').children('.accounts_sec').get();
listitems20.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems20, function(idx, itm) {
  // console.log(idx);
    mylist20.append(itm);
})
var mylist21 = $('#payroll').find('#payroll_box');
var listitems21 = $('#payroll').find('#payroll_box').children('.payroll_sec').get();
listitems21.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems21, function(idx, itm) {
 //  console.log(idx);
    mylist21.append(itm);
})

var mylist22 = $('#payroll').find('#payroll_box1');
var listitems22 = $('#payroll').find('#payroll_box1').children('.payroll_sec').get();
listitems22.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems22, function(idx, itm) {
 //  console.log(idx);
    mylist22.append(itm);
})
var mylist23 = $('#payroll').find('#payroll_box2');
var listitems23 = $('#payroll').find('#payroll_box2').children('.payroll_sec').get();
listitems23.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems23, function(idx, itm) {
 //  console.log(idx);
    mylist23.append(itm);
})
var mylist24 = $('#payroll').find('#workplace_sec');
var listitems24 = $('#payroll').find('#workplace_sec').children('.workplace').get();
listitems24.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems24, function(idx, itm) {
  // console.log(idx);
    mylist24.append(itm);
})
var mylist25 = $('#payroll').find('#workplace_sec1');
var listitems25 = $('#payroll').find('#workplace_sec1').children('.workplace').get();
listitems25.sort(function(a, b) { 
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems25, function(idx, itm) {
//console.log(idx);
    mylist25.append(itm);
})
var mylist26 = $('#payroll').find('#p11d_sec');
var listitems26 = $('#payroll').find('#p11d_sec').children('.p11d').get();
listitems26.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems26, function(idx, itm) {
  // console.log(idx);
    mylist26.append(itm);
})


var listitems27 = $('#payroll').find('#p11d_sec1').children('.p11d').get();
listitems27.sort(function(a, b) {  
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems27, function(idx, itm) {
  // console.log(idx);
    mylist27.append(itm);
})
});



$( document ).ready(function() { 
$( "div.add_custom_fields_section div.form-group").each(function( index ) {
  $(this).addClass('name_fields');
  console.log( index + ": " + $( this ).attr('class')+ "ID :" +  $( this ).attr('id') );
  var goal=$( this ).attr('id');
  if ( $( this ).hasClass( "personal_sort" ) ) { 
        append_text_section('personal-tax-returns','personal_sort',index,goal);
   }

    if ( $( this ).hasClass( "sorting" ) ) { 
        append_text_section('details','sorting',index,goal);
   }

   if ( $( this ).hasClass( "accounts_sec" ) ) {          
         append_text_section('accounts','accounts_sec',index,goal); 
   }

   if ( $( this ).hasClass( "workplace" ) ) {  
         append_text_section('payroll','workplace',index,goal);    
   }

   if ( $( this ).hasClass( "p11d" ) ) {   
         append_text_section('payroll','p11d',index,goal);
     
   }

   if ( $( this ).hasClass( "payroll_sec" ) ) {        
          append_text_section('payroll','payroll_sec',index,goal);      
   }

   if ( $( this ).hasClass( "vat_sort" ) ) {       
         append_text_section('vat-Returns','vat_sort',index,goal);     
   }

   if ( $( this ).hasClass( "management" ) ) {        
         append_text_section('management-account','management',index,goal);      
   }

   if ( $( this ).hasClass( "invest" ) ) {        
         append_text_section('investigation-insurance','invest',index,goal);     
   }

   if ( $( this ).hasClass( "others_details" ) ) {        
        append_text_section('other','others_details',index,goal);   
   }

    if ( $( this ).hasClass( "sorting_conf" ) ) {        
        append_text_section('confirmation-statement','sorting_conf',index,goal);   
   }
});

   });



function append_text_section(parent,value,index,goal){
console.log(value);
         var myarray_parents=[];
         var myarray = [];
         $('#'+parent+' .'+value).each(function(){
            var condition=$(this).parents().attr('class');     
            if(condition!='add_custom_fields_section'){
                  myarray.push($(this).attr('id')); 
                  myarray_parents.push($(this).parents().attr('id'));                 
           }      
         });       
          var closest = null;  
              values = myarray;
              userNum = goal; //This is coming from user input
               smallestDiff = Math.abs(userNum - values[0]);
               closest = 0; //index of the current closest number
               for (i = 1; i < values.length; i++) {
                  currentDiff = Math.abs(userNum - values[i]);
                  if (currentDiff < smallestDiff) {
                     smallestDiff = currentDiff;
                     closest = i;
                  }
               }
               var search_value=values[closest]; 
               var position=jQuery.inArray( search_value, myarray );
               var id_value=myarray[position];                
               var parent_value=myarray_parents[position]; 
               var next_index=index +1;                     
               var consume=$("#"+parent_value).find("#"+id_value); 
               $("div.add_custom_fields_section  .form-group:nth-child(1)").insertAfter(consume);  
}


</script>
<span style="font-size:16px;">

<span style="font-family:arial,helvetica,sans-serif;">

 <!-- <script type="text/javascript">
 
$("#show_login").change(function(){
  if($(this).prop("checked")=="true")
  {
    $(".how_login").show();
  }
  else
  {
    $(".how_login").hide();
  }
});

</script> -->

<!-- <script type="text/javascript">
  $(document).ready(function(){
      $(document).on( 'click', '.Footer common-clienttab  input,.Footer common-clienttab  button', function() {
           setTimeout(function(){ $grid.masonry('layout'); }, 500);
      });
  });
</script> -->
</span></span>
<!-- /** end of 14-09-2018 **/ -->

<script type="text/javascript">



  $(document).ready(function(){

$("#show_login").change(function(){
  if($(this).prop("checked")=="true")
  {
    $(".how_login").show();
  }
  else
  {
    $(".how_login").hide();
  }
});

      
  $('.js-small').parent('.col-sm-8').addClass('addbts-radio');
   $('.accordion-views .toggle').click(function(e) {
$(this).next('.inner-views').slideToggle(300);
$(this).toggleClass('this-active');
});
    /*        $('.checkall').parents('tr').find("th input").each(function(){
  //$('.checkall').parents('tr').find("th input:checked").next().removeClass('disabled');
          //if($(this).attr('checked') == "checked" )
         console.log($(this).is(":checked") + $(this).parent().find('.switchery').attr('class') + "=====class under");
         $(this).parent().find('.switchery').removeClass('disabled');
        });*/
  










  
   $(document).on('click','.yk_pack_addrow_landline',function(e)
   {
 
  $(this).addClass('yyyyyy');
   
   e.preventDefault();
   var id=$(this).data('id');
   var i=1;
   var parent = $(this).parents('.pack_add_row_wrpr_landline');

   var len = parent.find('.yk_pack_addrow_landline').length;
    parent.append('<span class="primary-inner success ykpackrow_landline add-delete-work width_check" id="'+len+'"><div class="data_adds"><select name="pre_landline['+id+']['+len+']" id="pre_landline"><option value="mobile">Mobile</option><option value="work">Work</option><option value="home">Home</option><option value="main">Main</option><option value="workfax">Work Fax</option><option value="homefax">Home Fax</option></select><input type="text" class="text-info" name="landline['+id+']['+len+']" value=""></div><div class="text-right danger-make1"><a href="javascript:;" class="btn btn-danger yk_pack_delrow_landline" id="'+len+'"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true"></i></a></div>   <button type="button" class="btn btn-primary yk_pack_addrow_landline" data-id="'+id+'">Add Landline</button></span>');
   i++;
      // alert($('.yk_pack_addrow_landline').length);
    if(parent.find('.yk_pack_addrow_landline').length > 2){
     // alert('ok');
      $(this).parents('.primary-inner').addClass('check_width');

    }

   
   });
   
   /**************************************************
   landline REMOVE ROW 
   ***************************************************/  
   
     $(document).on('click','.yk_pack_delrow_landline',function(e)
   {
    
      var parent = $(this).parents('.pack_add_row_wrpr_landline');
      $(this).parents('.ykpackrow_landline').remove();

      //  alert($('.yk_pack_addrow_landline').length);
       if( parent.find('.yk_pack_addrow_landline').length == 1)
       {
       //  alert('true');
            parent.find('.yk_pack_addrow_landline').removeClass('yyyyyy');
       }
       else{
           //  alert('false');
                 parent.find('span:last-child .yk_pack_addrow_landline').removeClass('yyyyyy');
          }    //   $(this).parents('.remove_one_row').last('.width_check').addClass('class1');
          
      // alert($('.yk_pack_addrow_landline').length);

     //    $(this).parents('.remove_one_row').last('.ykpackrow_landline').find('.yk_pack_addrow_landline').removeClass('yyyyyy');
   });
  
     $(document).on('click','.yk_pack_addrow_email',function(e)
   {
     //alert('op');
       $(this).addClass('yyyyyy');
      e.preventDefault();
      var id=$(this).data('id');
      var parent = $(this).parents('.pack_add_row_wrpr_email');
      
      var len = parent.find('input[name^="work_email"]').length;
     
      parent.append('<span class="primary-inner success ykpackrow_email add-delete-work"><input type="email" class="text-info" name="work_email['+id+']['+len+']" value=""><div class="text-right danger-make1"><a href="javascript:;" class="btn btn-danger yk_pack_delrow_email"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true"></i></a></div> <button type="button" class="btn btn-primary yk_pack_addrow_email" data-id="'+id+'">Add Email</button></span>');
       // alert($('.yk_pack_addrow_email').length);
     if(parent.find('.yk_pack_addrow_email').length > 2)
     {
         $(this).parents('.primary-inner').addClass('check_width');
      }



   });
   

   
     
   $(document).on('click','.yk_pack_delrow_email',function(e)
   {
       var parent = $(this).closest('.pack_add_row_wrpr_email');
       $(this).parents('.ykpackrow_email').remove();

       if(parent.find('.yk_pack_addrow_email').length == 1)
       {
            parent.find('.yk_pack_addrow_email').removeClass('yyyyyy');
       }
       else
       {
       
         parent.find('span:last-child .yk_pack_addrow_email').removeClass('yyyyyy');
       }    
   });  
   
   

















  });

  
</script>