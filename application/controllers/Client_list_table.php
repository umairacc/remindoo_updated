<?php
  $column_helper = [
        'Client'=>'ClientColumn_Client' ,
        'Type'=>'ClientColumn_Type',
        'Active'=>'ClientColumn_Status',
        'staff_manager'=>'ClientColumn_staff_manager',
        'staff_managed' => 'ClientColumn_staff_managed',
        'team' => 'ClientColumn_team',
        'team_allocation' => 'ClientColumn_team_allocation',
        'department'=>'ClientColumn_department',
        'department_allocation'=>'ClientColumn_department_allocation',
        'member_manager'=> 'ClientColumn_member_manager',
        'member_managed' => 'ClientColumn_member_managed',
        'crm_company_status' => 'ClientColumn_client_AddtedFrom'
          ];

          $FirmId = 0;
?>




 <table class="table client_table1 text-center display nowrap printableArea" id="client_table" cellspacing="0" width="100%">
                                                    <thead>
                                                      <tr>
                                                        <th class="select_row_TH">
                                                           <label class="custom_checkbox1">
                                                              <input type="checkbox" id="select_all_user"><i></i>
                                                            </label>
                                                        </th>
                                                        <th>Crate Date</th>

                                                        <?php
                                                            foreach ($column_setting as $key => $colval) 
                                                            {
                                                            
                                                        ?>
                                                              <th class="<?php echo $colval['name'].'_TH';?> hasFilter" data-orderNo="<?php echo $colval['order_firm'.$FirmId];?>">
                                                                <?php echo $colval['cusname_firm'.$FirmId]?>
                                                                <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                                <div class="sortMask"></div>
                                                                <?php if($colval['name'] == "Active")
                                                                {
                                                                  echo  ClientColumn_Status_SelectBox();
                                                                } 
                                                                ?>
                                                             </th>
                                                        <?php
                                                            }
                                                        ?> 
                                                        <th class="action_TH">
                                                          Action
                                                        </th>
                                                        <th class="crm_legal_form__primary_search_TH" style="display: none;">
                                                        </th>
                                                        <th class="status_primary_search_TH" style="display: none;">
                                                        </th>
                                                        <th class="Created_date_primary_search_TH" style="display: none;">
                                                        </th>
                                                       
                                                      </tr>
                                                    </thead>
                                                    
                                                    <tbody>
                                                      <?php
foreach ($getallUser as $getallUserkey => $getallUservalue) 
           {

                     $client=$this->Common_mdl->select_record('client','user_id',$getallUservalue['id']);                    
                     $contact=$this->db->query('select * from client_contacts where client_id='.$getallUservalue['id'].' and make_primary=1')->row_array();
            ?>
                            <tr class="table_row" id="<?php echo $getallUservalue['id'];?>">
                                                 <td>
                                                   <label class="custom_checkbox1">
                                                        <input type="checkbox" class="user_checkbox" data-id="<?php echo $getallUservalue['id'];?>"><i></i>
                                                    </label>
                                                  </td>
                                                  <td><?php echo date('d-m-Y',trim($getallUservalue['CreatedTime']) ); ?></td>
                  <?php
                                       foreach ($column_setting as $key => $colval) 
                                       {
                                          if( isset( $column_helper[ $colval['name'] ] ) )
                                          {
                                            echo $column_helper[ $colval['name'] ]($getallUservalue,$client,$contact);
                                          }
                                          else if(preg_match("/^contact_/i", $colval['name']))
                                          {
                                            $column_name = trim( preg_replace("/^contact_/", "", $colval['name'] ) );
                                            $data = $contact[$column_name];

                                            echo "<td data-search='".$data."'>".$data."</td>";
                                          }
                                          else 
                                          {
                                            $data = $client[ $colval['name'] ];
                                            echo "<td data-search='".$data."'>". $data ."</td>";
                                          }
                                       }
                  ?>


                                    <td class="action_TD">
                                        <p class="action_01 sss">
                                           <a href="javascript:void(0)" data-toggle="modal" data-target="#deleteconfirmation" onclick="delete_user(this)" data-id="<?php echo $getallUservalue['id'];?>"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>
                                           <a href="<?php echo base_url().'Client/client_info/'.$getallUservalue['id'];?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#archiveconfirmation" onclick="archive_user(this)" data-id="<?php echo $getallUservalue['id'];?>">
                                          <i class="fa fa-archive archieve_click" aria-hidden="true"></i>
                                        </a>
                                        </p>
                                    </td>
                                    <td style="display: none;"><?php echo $client['crm_legal_form']; ?> </td>
                                    <td style="display: none;"><?php echo $getallUservalue['status']; ?> </td>
                                    <!-- D'nt move this its should stay last , for from-to search-->
                                    <td style="display: none;"><?php echo date('Ymd',trim($getallUservalue['CreatedTime']) );?></td>
                                   <!-- D'nt move this its should stay last , for from-to search-->
                         </tr>
            <?php
              }
            ?>
                                        </tbody>

</table>
<input type="hidden" class="rows_selected" id="select_user_count" >          