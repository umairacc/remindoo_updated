<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sub_Task extends CI_Controller {
public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";

  public function __construct(){
        parent::__construct();
        $this->load->model(array('Common_mdl','Security_model','Invoice_model','Task_invoice_model','Task_creating_model','Task_section_model'));
        $this->load->helper('download');
    }

 public function sub_task_get(){
  if($_SESSION["firm_id"]==258){
    $this->db->query('Update firm_column_settings SET visible_firm258=1 WHERE id=1 OR id=2');
  }
  $task_status=$this->db->query('SELECT * from task_status where is_superadmin_owned=1')->result_array();

    	$role = $this->Common_mdl->getRole($_SESSION['id']);
    	$sub_task_list=$this->db->query("SELECT * from add_new_task where id=".$_POST['id']."")->row_array();
    	$sub_task_id=array_filter(explode(',',$sub_task_list['sub_task_id']));  

       $progress_task_status=$this->db->query('SELECT * from progress_status  where firm_id IN (0,'.$_SESSION["firm_id"].') and status="0" ORDER BY id DESC')->result_array();

      $column_settings = Firm_column_settings('task_list');

      $column_order = array_fill_keys(json_decode($column_settings['order'],true), " ");

      $column_hidden = array_flip( json_decode($column_settings['hidden'],true) );

      
      


    	if(!empty($sub_task_id)){

    	for($i=0;$i<count($sub_task_id);$i++){
        $ROW =[];
    		$value=$this->db->query("SELECT * from add_new_task where id=".$sub_task_id[$i]." ")->row_array();    		            
        if(!empty($value)){

          $time=json_decode($value['counttimer']);
          $time_start_date =$value['time_start_date'];
          $time_end_date=$value['time_end_date'];
          $hours=0;
          $mins=0;
          $sec=0;
          $pause='';

          $sub_id=($value['sub_task_id']!='')?$value['sub_task_id']:"0"; 


              $priority_color='';
                                            if($value['priority']=='medium')
                                         {
                                          $priority_color = 'background: transparent';
                                          $priority_color_bg = 'color: #ff9900';
                                          $priority_color_bd = 'border: 2px solid #ff9900';
                                         }
                                         elseif($value['priority']=='high')
                                         {
                                          $priority_color = 'background: transparent';
                                          $priority_color_bg = 'color: #ff0066';
                                          $priority_color_bd = 'border: 2px solid #ff0066';
                                         }
                                         elseif($value['priority']=='super_urgent')
                                         {
                                          $priority_color = 'background: transparent';
                                          $priority_color_bg = 'color: #ff0000';
                                          $priority_color_bd = 'border: 2px solid #ff0000';
                                         }
                                         else
                                         {
                                          $priority_color = 'background: transparent';
                                          $priority_color_bg = 'color: #ffcc00';
                                          $priority_color_bd = 'border: 2px solid #ffcc00';

                                         }


          // if($_SESSION['role']==6)
          // {
          //     $individual_timer=$this->db->query("select * from individual_task_timer where user_id=".$_SESSION['id']." and task_id=".$value['id']." ")->row_array();
          // if(count($individual_timer)>0)
          // {
          //   if($individual_timer['time_start_pause']!=''){
          //     $res=explode(',',$individual_timer['time_start_pause']);
          //     //$res=array(1529919001,1530077528,1530080810,1530080817,1530080930,1530080933);
          //     $res1=array_chunk($res,2);
          //     $result_value=array();
          //     $pause='on';
          //     foreach($res1 as $rre_key => $rre_value)
          //     {
          //        $abc=$rre_value;
          //        if(count($abc)>1){
          //        if($abc[1]!='')
          //        {
          //           $ret_val=$this->calculate_test($abc[0],$abc[1]);
          //           array_push($result_value, $ret_val) ;
          //        }
          //        else
          //        {
          //         $pause='';
          //           $ret_val=$this->calculate_test($abc[0],time());
          //            array_push($result_value, $ret_val) ;
          //        }
          //       }
          //       else
          //       {
          //         $pause='';
          //           $ret_val=$this->calculate_test($abc[0],time());
          //            array_push($result_value, $ret_val) ;
          //       }
          //     }

          //     $time_tot=0;
          //      foreach ($result_value as $re_key => $re_value) {
          //         $time_tot+=time_to_sec($re_value) ;
          //      }
          //      $hr_min_sec=$this->sec_to_time($time_tot);
          //      $hr_explode=explode(':',$hr_min_sec);
          //      $hours=(int)$hr_explode[0];
          //      $min=(int)$hr_explode[1];
          //      $sec=(int)$hr_explode[2]; 
          //     }
          //     else
          //     {
          //       $hours=0;
          //       $min=0;
          //       $sec=0;
          //       $pause='on';
          //     }
          // }
          // else
          // {
          //     $hours=0;
          //     $min=0;
          //     $sec=0;
          //     $pause='on';
          // }
          // }
          // else
          // {
          /** non staff members shown timer **/
          //$individual_timer=$this->db->query("select * from individual_task_timer where task_id=".$value['id']." ")->result_array();
             if($_SESSION['user_type']=='FU')
                {
                    $individual_timer=$this->db->query("select * from individual_task_timer where task_id=".$value['id']." and  user_id=".$_SESSION['id']." ")->result_array();
                }
                else
                {
                    $individual_timer=$this->db->query("select * from individual_task_timer where task_id=".$value['id']."")->result_array();
                }

          $pause_val='on';
          $pause='on';
          $for_total_time=0;
          // echo $value['id'];
          // print_r($individual_timer);
          // die();
          if(count($individual_timer)>0){
          foreach ($individual_timer as $intime_key => $intime_value) {
          $its_time=$intime_value['time_start_pause'];
          $res=explode(',', $its_time);
            $res1=array_chunk($res,2);
          $result_value=array();
          //  $pause='on';
          foreach($res1 as $rre_key => $rre_value)
          {
             $abc=$rre_value;
             if(count($abc)>1){
             if($abc[1]!='')
             {
                $ret_val=$this->calculate_test($abc[0],$abc[1]);
                array_push($result_value, $ret_val) ;
             }
             else
             {
              $pause='';
              $pause_val='';
                $ret_val=$this->calculate_test($abc[0],time());
                 array_push($result_value, $ret_val) ;
             }
            }
            else
            {
              $pause='';
              $pause_val='';
                $ret_val=$this->calculate_test($abc[0],time());
                 array_push($result_value, $ret_val) ;
            }
          }
          // $time_tot=0;
           foreach ($result_value as $re_key => $re_value) {
              //$time_tot+=time_to_sec($re_value) ;
              $for_total_time+=$this->time_to_sec($re_value) ;
           }

          }
          //echo $for_total_time."val";
          $hr_min_sec=$this->sec_to_time($for_total_time);
           $hr_explode=explode(':',$hr_min_sec);
            $hours=(int)$hr_explode[0];
            $min=(int)$hr_explode[1];
           $sec=(int)$hr_explode[2];

          }
          else
          {
            $hours=0;
            $min=0;
            $sec=0;
            $pause='on';
          }
     // }

       //$isPasuse = ($pause==''?'time_pause1':'');

      $data[$i]='<input type="hidden" name="trhours_'.$value['id'].'" id="trhours_'.$value['id'].'" value='.$hours.' ><input type="hidden" name="trmin_'.$value['id'].'" id="trmin_'.$value['id'].'" value='.$min.' ><input type="hidden" name="trsec_'.$value['id'].'" id="trsec_'.$value['id'].'" value='.$sec.' ><input type="hidden" name="trmili_'.$value['id'].'" id="trmili_'.$value['id'].'" value="0" ><input type="hidden" name="trpause_'.$value['id'].'" id="trpause_'.$value['id'].'" value="'.$pause.'" >';

    		
		//error_reporting(0);                                   
		if($value['start_date']!='' && $value['end_date']!=''){    
		$start  = date_create(implode('-', array_reverse(explode('-', $value['start_date']))));
		$end  = date_create(implode('-', array_reverse(explode('-', $value['end_date']))));
		$diff    = date_diff ( $start, $end );
		$y =  $diff->y;
		$m =  $diff->m;
		$d =  $diff->d;
		$h =  $diff->h;
		$min =  $diff->i;
		$sec =  $diff->s;
		}else{
		$y =  0;
		$m =  0;
		$d =  0;
		$h =  0;
		$min = 0;
		$sec =  0;
		}

		                                  
		$d_rec = date('Y-m-d',strtotime($value['end_date']));  // this format is string comparable 
		
    $Ddays = dateDiffrents( date('Y-m-d') , $d_rec );

    if ( $Ddays < 0 )
    {                                     
      $d_val = abs( $Ddays ). " Days Over Due";
    }
    else if ( $Ddays === 0 )
    {
      $d_val = "Due Today";                                        
    }
    else 
    {
      $d_val = $Ddays." Days";
    }   

		if($value['worker']=='')
		{
		$value['worker'] = 0;
		}
		$staff=$this->db->query("SELECT * FROM staff_form WHERE user_id in (".$value['worker'].")")->result_array();
		$stat='';
		$percent = 0;
		if($value['task_status']=='1')
		{
		$percent = 0;
		$stat = 'Not Started';
		} if($value['task_status']=='2')
		{
		$percent = 25;
		$stat = 'In Progress';
		} if($value['task_status']=='3')
		{
		$percent = 50;
		$stat = 'Awaiting for a feedback';
		} if($value['task_status']=='testing')
		{
		$percent = 75;
		$stat = 'Testing';
		} if($value['task_status']=='5')
		{
		$percent = 100;
		$stat = 'Complete';
		}
		$exp_tag = explode(',', $value['tag']);
		$explode_worker=explode(',',$value['worker']);
		/** new 12-06-2018 **/
		$explode_team=explode(',',$value['team']);
		$explode_department=explode(',',$value['department']);

		$data[$i] .='<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;"><tr class="test_check subtask_check_'.$_POST['id'].'_'.$value['id'].'" parent-id='.$_POST['id'].'>';

        $checked ='';
        $strikeoutclass = '';
        if($value['task_status'] == '5' )
        {
          $checked = "checked='checked'";
          $strikeoutclass = "strikeout";
        }

        $ROW['select_row_TH'] ='<td class="select_row_TD"> <label class="custom_checkbox1"> <input type="checkbox" class="subTaskcheckbox" '.$checked.' data-alltask-id="'.$value['id'].'"> <i></i> </label> </div></td>'; 

          $time=json_decode($value['counttimer']);
                  $time_start_date =$value['time_start_date'];
                  $time_end_date=$value['time_end_date'];
                  $hours=0;
                  $mins=0;
                  $sec=0;
                  $pause='';
                  /** for task timer **/
                  if($_SESSION['role']==6)
                  {
                  $individual_timer=$this->db->query("select * from individual_task_timer where user_id=".$_SESSION['id']." and task_id=".$value['id']." ")->row_array();
                  if(count($individual_timer)>0)
                  {
                       if($individual_timer['time_start_pause']!=''){
                    $res=explode(',',$individual_timer['time_start_pause']);                  
                    $res1=array_chunk($res,2);
                    $result_value=array();
                    $pause='on';
                    foreach($res1 as $rre_key => $rre_value)
                    {
                       $abc=$rre_value;
                       if(count($abc)>1){
                       if($abc[1]!='')
                       {
                          $ret_val=calculate_test($abc[0],$abc[1]);
                          array_push($result_value, $ret_val) ;
                       }
                       else
                       {
                        $pause='';
                          $ret_val=calculate_test($abc[0],time());
                           array_push($result_value, $ret_val) ;
                       }
                      }
                      else
                      {
                        $pause='';
                          $ret_val=calculate_test($abc[0],time());
                           array_push($result_value, $ret_val) ;
                      }


                    }
                    $time_tot=0;
                     foreach ($result_value as $re_key => $re_value) {
                        $time_tot+=time_to_sec($re_value) ;
                     }
                     $hr_min_sec=$this->sec_to_time($time_tot);
                     $hr_explode=explode(':',$hr_min_sec);
                     $hours=(int)$hr_explode[0];
                     $min=(int)$hr_explode[1];
                     $sec=(int)$hr_explode[2];   
                    }
                    else
                    {
                      $hours=0;
                      $min=0;
                      $sec=0;
                    }
                  }
                  //echo $hours."--".$min."--".$sec;
                  }  
                  /** end of task timer **/
                  else {
              if($_SESSION['user_type']=='FU')
              {
                  $individual_timer=$this->db->query("select * from individual_task_timer where task_id=".$value['id']." and  user_id=".$_SESSION['id']." ")->result_array();
              }
              else
              {
                  $individual_timer=$this->db->query("select * from individual_task_timer where task_id=".$value['id']."")->result_array();
              }



                
                  $pause_val='on';
                  $pause='on';
                  $for_total_time=0;
                  if(count($individual_timer)>0){
                  foreach ($individual_timer as $intime_key => $intime_value) {
                  $its_time=$intime_value['time_start_pause'];
                  $res=explode(',', $its_time);
                    $res1=array_chunk($res,2);
                  $result_value=array();
                  //  $pause='on';
                  foreach($res1 as $rre_key => $rre_value)
                  {
                     $abc=$rre_value;
                     if(count($abc)>1){
                     if($abc[1]!='')
                     {
                        $ret_val=$this->calculate_test($abc[0],$abc[1]);
                        array_push($result_value, $ret_val) ;
                     }
                     else
                     {
                      $pause='';
                      $pause_val='';
                        $ret_val=$this->calculate_test($abc[0],time());
                         array_push($result_value, $ret_val) ;
                     }
                    }
                    else
                    {
                      $pause='';
                      $pause_val='';
                        $ret_val=$this->calculate_test($abc[0],time());
                         array_push($result_value, $ret_val) ;
                    }
                  }
                  // $time_tot=0;
                   foreach ($result_value as $re_key => $re_value) {
                      //$time_tot+=time_to_sec($re_value) ;
                      $for_total_time+=$this->time_to_sec($re_value) ;
                   }

                  }
                  //echo $for_total_time."val";
                  $hr_min_sec=$this->sec_to_time($for_total_time);
                   $hr_explode=explode(':',$hr_min_sec);
                    $hours=(int)$hr_explode[0];
                    $min=(int)$hr_explode[1];
                   $sec=(int)$hr_explode[2];

                  }
                  else
                  {
                    $hours=0;
                    $min=0;
                    $sec=0;
                    $pause='on';
                  }
                  }  
                   $isPasuse = ($pause==''?'time_pause1':''); 
                  $ROW['subtask_toggle_TH']='<td class="subtask_toggle_TD"></td>';

                  $time_start_date ="";

                  if($time_start_date!=""){ date('Y/m/d H:i:s',strtotime($time_start_date)); } 

                  $time_end_date="";

                  if($time_end_date!=""){ date('Y/m/d H:i:s',strtotime($time_end_date)); }

           $ROW['timer_TH']='<td  class="timer_TD"><div class="stopwatch" data-autostart="false" data-date="2018/06/23 15:37:25" data-id="'.$value['id'].'"  data-hour="'.$hours.'" data-min="'.$min.'" data-sec="'.$sec.'" data-mili="0" data-start="'.$time_start_date .'" data-end="'.$time_end_date.'" data-current="Start" data-subid="'.$sub_id.'" data-pauseon="'.$pause.'"><div class="time timer_'.$value['id'].'"> <span class="hours"></span> : <span class="minutes"></span> : <span class="seconds"></span>  </div> <div class="controls per_timerplay_'.$value['id'].'" id="'.$value['id'].'">'; 

              if($_SESSION['permission']['Task']['edit']==1 && $value['task_status']!="5"){
                $ROW['timer_TH'].='<button class="toggle for_timer_start_pause new_play_stop per_timerplay_'.$value['id'].' '.$isPasuse.' "  id="'.$value['id'].'" data-pausetext="||" data-resumetext=">"> > </button>';
              }

          $ROW['timer_TH'].='</td>';

            $ROW['subject_TH']= '<td  class="subject_TD subject_choose '.$strikeoutclass.'"><p class="Words_length"><a href="'.base_url().'user/task_details/'.$value['id'].'" target="_blank">'.ucfirst($value['subject']).'</a></p><p class="priority_div" style="' . $priority_color . ';' . $priority_color_bg . ';' . $priority_color_bd . '">'.
                 ucwords(str_replace("_", " ", $value['priority'])  ).'</p></td>';


            $ROW['startdate_TH'] ='<td  class="startdate_TD startdate_class">'.date('d-m-Y',strtotime($value['start_date'])).'</td>';

            if(!empty(strtotime($value["end_date"])))
            {
              if (isset($sub_task_list["related_to_services"]) && !empty($sub_task_list["related_to_services"]) && $sub_task_list["related_to_services"]) {
                  $services = explode("_", $sub_task_list["related_to_services"]);
                  if (isset($services[1]) && $services[0] == 'wor') {
                    $table         = 'work_flow';
                    $service_id    = $services[1];
                    $service_array = $work_flow;
                  } else {
                    $table         = 'service_lists';
                    $service_id    = (isset($services[1])) ? $services[1] : $services[0];
                    $service_array = $settings_val;
                  }
                  //$service_key = array_search($service_id, array_column($service_array, 'id'));
                  $Rservice    = $service_id; //$service_array[$service_key]["service_name"];
                } else {
                  $Rservice = 0;
                }

                $due_dates_data    = json_decode($this->calculate_due_dates($sub_task_list["id"], $Rservice, $sub_task_list["progress_status"]));
                $internal_due_date = !empty($due_dates_data[0]->internal_due_date) ? date('d-m-Y', strtotime($due_dates_data[0]->internal_due_date)) : '-';
                $internal_due_date = ($internal_due_date != '01-01-1970')          ? $internal_due_date                                              : '-';
               $ROW['duedate_TH'] ='<td  class="duedate_TD duedate_class">'.date('d-m-Y',strtotime($internal_due_date)).'</br> <span class="timer check_timer"><b>'.$d_val.'</b> </span> </td>';

               //$ROW['intduedate_TH'] ='<td  class="int_duedate_TD duedate_class">'.date('d-m-Y',strtotime($value['end_date'])).'</br> <span class="timer check_timer"><b>'.$d_val.'</b> </span> </td>';
            }
            else
            {
               $ROW['duedate_TH'] = '<td class="duedate_TD duedate_class"></td>';
               //$ROW['intduedate_TH'] = '<td class="int_duedate_TD duedate_class"></td>';
            }
           
              // $selectedStatus = ['notstarted'=>'','inprogress'=>'','awaiting'=>'','complete'=>'',''=>''];
              // $selectedStatus[ $value["task_status"] ] = 'selected="selected" ';
           

            $ROW['status_TH']= '<td  class="status_TD status_class">';

            $ROW['status_TH'].= '<select name=" task_status" id="task_status" class="task_status per_taskstatus_'.$value['id'].'" data-id="'.$value['id'].'">
            <option value="">Select Status</option>'; 
           // print_r($task_status);
                    foreach($task_status as $key => $status_val) { 
                       $selected_value='';
                    // echo "chk";
                     if( $status_val['id']==$value["task_status"] )
                     {
                      $selected_value='selected="selected" ';
                     }

                $ROW['status_TH'].=  '<option value="'.$status_val["id"].'"  '.$selected_value.'  >'. $status_val["status_name"] .'</option>';

                   } 


           $ROW['status_TH'].='</select></td>';



               $ROW['progress_TH']= '<td  class="progress_TD progress_'.$value['id'].'">';

            $ROW['progress_TH'].= '<select name="progress_status" id="progress_status" class="progress_status per_progress_'.$value['id'].'" data-id="'.$value['id'].'">
            <option value="">Select progress Status</option>'; 
           // print_r($task_status);
                    foreach($progress_task_status as $key => $status_val) { 
                       $selected_value='';
                    // echo "chk";
                     if( $status_val['id']==$value["progress_status"] )
                     {
                      $selected_value='selected="selected" ';
                     }

                $ROW['progress_TH'].=  '<option value="'.$status_val["id"].'"  '.$selected_value.'  >'. $status_val["status_name"] .'</option>';

                   } 


           $ROW['progress_TH'].='</select></td>';



              $selectedPriority = ['low'=>'','medium'=>'','high'=>'','super_urgent'=>'',''=>''];
              $selectedPriority[ $value["priority"] ] = 'selected="selected" ';

            $ROW['priority_TH']='<td  class="priority_TD priority_class"><select name="task_priority" id="task_priority" class="task_priority per_taskpriority_'.$value['id'].'" data-id="'.$value['id'].'">
            <option value="" '.$selectedPriority[''].'>By Priority</option>
            <option value="low" '.$selectedPriority['low'].'>Low</option>
            <option value="medium" '.$selectedPriority['medium'].'>Medium</option>
            <option value="high" '.$selectedPriority['high'].' >High</option>
            <option value="super_urgent" '.$selectedPriority['super_urgent'].'>Super Urgent</option>
            </select></td>';

            $tagName ='';
            foreach ($exp_tag as $exp_tag_key => $exp_tag_value) {
            	$tagName .= $this->Common_mdl->getTag($exp_tag_value).' ';
            }
            $ROW['tag_TH'] =  '<td  class="tag_TD tag_class">'.$tagName.'</td>';

            $username=array();

            $ROW['assignto_TH'] =' <td  class="assignto_TD assignto_class"><span class="task_'.$value['id'].'"> <span style="" class="task_'.$value['id'].'">';

             if($_SESSION['permission']['Task']['edit']=='1'){ 

             $ROW['assignto_TH'] .='<a href="javascript:;" data-toggle="modal" data-target="#adduser_'.$value['id'].'" class="adduser1 user_change per_assigne_'.$value['id'].'"><i class="fa fa-plus"></i></a>';

              }
             
             

                            $ex_val=$this->Common_mdl->get_module_user_data( 'TASK' ,$value['id'] );
                                $var_array=array();
                                 if(count($ex_val)>0)
                                         {
                                          foreach ($ex_val as $key => $value1) {
                                        
                                           $user_name = $this->Common_mdl->select_record('user','id',$value1);
                                            array_push($var_array, $user_name['crm_name']);
                                           }
                                          
                                         }


                             // $Assignee= implode(',',$var_array);


             //  foreach($explode_worker as $key => $val){  

                   /*$getUserProfilepic = $this->Common_mdl->getUserProfilepic($val);
                   $row10 .='<img src="'.$getUserProfilepic.'" alt="img">';*/

                 //    $getUserProfilename = $this->Common_mdl->getUserProfileName($val);
                 //     array_push($username,$getUserProfilename);
                 // }
                   // $row10 .='<a href="javascript:;" data-toggle="modal" data-target="#adduser_'.$value['id'].'" class="adduser1 per_assigne_'.$value['id'].'"><i class="fa fa-plus"></i></a></span></td>';// 
              $ROW['assignto_TH'] .=implode(',', $var_array).'</span></td>';
				
					

           
          $Rservice = '-';

            // if(isset($value['related_to_services']) && $value['related_to_services']!='')
            // {
            //   $related_to_services = $value['related_to_services'];
            // }


         if(isset($value['related_to_services']) && $value['related_to_services']!='')
          {
              $Rservice1 = $this->Common_mdl->select_record('service_lists','id',$value['related_to_services']);
            
              $Rservice=$Rservice1['service_name'];
            }
          
                     
          $ROW['services_TH'] = '<td  class="services_TD services_class">'.$Rservice.'</td>';

          $ROW['company_TH']= '<td  class="company_TD company_class">'.
        $this->Common_mdl->get_price('client','id',$value['company_name'],'crm_company_name').'</td>';
           
                 


                  $row12= '<td class="action_TD action_class"><p class="action_01 per_action_'.$value['id'].'">';  
                 if($role!='Staff'){    


/*
                 $row12.='<a href="#" onclick="return alltask_delete('.$value['id'].');"><i class="fa fa-trash fa-6 deleteAllTask" aria-hidden="true" ></i></a>';
*/
                 $row12.='<a href="javascript:;" data-toggle="modal" data-target="#deleteconfirmation" onclick="delete_task(this)" data-id="'.$value['id'].'"><i class="fa fa-trash fa-6 deleteAllTask" aria-hidden="true"></i></a>';

                 }
                 $row12.='<a href="'.base_url().'user/update_task/'.$value['id'].'"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>';
        if($value['task_status']=='archive'){ 
                 $row12.=' <a href="javascript:;" id="'.$value['id'].'" class="archieve_click" onclick="archieve_click(this)" data-status="unarchive" data-toggle="modal" data-target="#my_Modal"><i class="fa fa-archive archieve_click"  aria-hidden="true" title="unarchive"></i></a>';
                       }else{ 
                 $row12.='<a href="javascript:;" id="'.$value['id'].'" class="archieve_click" onclick="archieve_click(this)" data-status="archive" data-toggle="modal" data-target="#my_Modal"><i class="fa fa-archive archieve_click"  aria-hidden="true" title="archive"></i></a>';
                    } 
                  $row12.=' </p></td> ';

                  $ROW['action_TH'] = $row12;

                  $isBillable = ($value['billable']!=0?'Billable':'Non Billable'); 
                  $ROW['billable_TH'] ='<td class="billable_TD">'.$isBillable.'</td>';
                  
                 $remove_hidden = array_diff_key($ROW, $column_hidden);
                 $colum_modified = array_replace( $column_order , $remove_hidden);



                 $data[$i].=implode(' ', $colum_modified);


                $data[$i].='</tr></table>'; 
    	}
    

    }

    }else{
    	$data[0]='<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;"><tr><td colspan="13"  align="center">There is no subtasks</td></tr></table>';
    }

    	echo implode(' ',$data);

    }   



    // $role = $this->Common_mdl->getRole($_SESSION['id']);
   function convertToHoursMins($time, $format = '%02d:%02d') {
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
   }

   function calculate_test($a,$b){
$difference = $b-$a;

$second = 1;
$minute = 60*$second;
$hour   = 60*$minute;

$ans["hour"]   = floor(($difference)/$hour);
$ans["minute"] = floor((($difference)%$hour)/$minute);
$ans["second"] = floor(((($difference)%$hour)%$minute)/$second);
//echo  $ans["hour"] . " hours, "  . $ans["minute"] . " minutes, " . $ans["second"] . " seconds";

$test=$ans["hour"].":".$ans["minute"].":".$ans["second"];
return $test;
}
function time_to_sec($time) {
list($h, $m, $s) = explode (":", $time);
$seconds = 0;
$seconds += (intval($h) * 3600);
$seconds += (intval($m) * 60);
$seconds += (intval($s));
return $seconds;
}
function sec_to_time($sec) {
return sprintf('%02d:%02d:%02d', floor($sec / 3600), floor($sec / 60 % 60), floor($sec % 60));
}    

  public function calculate_due_dates($task_id, $serv_id, $status_id)
  {
    $this->Security_model->chk_login();
    $fid    = $_SESSION['firm_id'];
    $uid    = $_SESSION['id'];
    $sql_3  = "SELECT ANT.id,
                      ANT.start_date,
                      ANT.end_date,
                      ANT.task_status,
                      PSC.status_id,
                      PSC.created_at,
                      PSC.modified_at,
                      IDL.internal_deadline,
                      IDL.external_deadline
              FROM add_new_task AS ANT
              LEFT JOIN progress_status_change AS PSC
                     ON PSC.task_id = ANT.id  
                    AND PSC.status_id = $status_id            
              INNER JOIN internal_deadlines AS IDL
                      ON IDL.firm_id = $fid
                     AND IDL.user_id = $uid
                     AND IDL.task_status = PSC.status_id
                     AND IDL.service_id = $serv_id
              WHERE ANT.id = $task_id
              ORDER BY PSC.id DESC
              LIMIT 1";

    $data = $this->db->query($sql_3)->result_array();

    if (isset($data[0])) {
      $task_status = $data[0]['task_status'];

      if ($data[0]['internal_deadline'] > 0) {
        if (isset($data[0]['modified_at']) && $data[0]['modified_at'] != '') {
          $data[0]['internal_due_date'] = date('Y-m-d', strtotime('+' . $data[0]['internal_deadline'] . ' days', strtotime($data[0]['modified_at'])));
        } else {
          $data[0]['internal_due_date'] = '';
        }
      } else {
        if (isset($data[0]['end_date']) && $data[0]['end_date'] != '') {
          $data[0]['internal_due_date'] = date('Y-m-d', strtotime($data[0]['end_date']));
        } else {
          $data[0]['internal_due_date'] = '';
        }
      }

      if ($data[0]['external_deadline'] > 0) {
        if (isset($data[0]['modified_at']) && $data[0]['modified_at'] != '') {
          $data[0]['external_due_date'] = date('Y-m-d', strtotime('+' . $data[0]['external_deadline'] . ' days', strtotime($data[0]['modified_at'])));
        } else {
          $data[0]['external_due_date'] = '-';
        }
      } else {
        if (isset($data[0]['end_date']) && $data[0]['end_date'] != '') {
          $data[0]['external_due_date'] = date('Y-m-d', strtotime($data[0]['end_date']));
        } else {
          $data[0]['external_due_date'] = '-';
        }
      }

      $int_delay                    = round((time() - strtotime($data[0]['internal_due_date'])) / (60 * 60 * 24));
      $data[0]['internal_delay']    = ($int_delay > 0 && ($task_status != 'complete' && $task_status != 5)) ? $int_delay . 'Days Delay' : '';
      $ext_delay                    = 0;
      $data[0]['external_delay']    = ($ext_delay > 0 && ($task_status != 'complete' && $task_status != 5)) ? $ext_delay . 'Days Delay' : '';
    } else {
      if (isset($data[0]['end_date']) && $data[0]['end_date'] != '') {
        $data[0]['internal_due_date'] = date('Y-m-d', strtotime($data[0]['end_date']));
      } else {
        $data[0]['internal_due_date'] = '-';
      }

      if (isset($data[0]['end_date']) && $data[0]['end_date'] != '') {
        $data[0]['external_due_date'] = date('Y-m-d', strtotime($data[0]['end_date']));
      } else {
        $data[0]['external_due_date'] = '-';
      }
      $data[0]['internal_delay']    = '';
      $data[0]['external_delay']    = '';
    }

    if ($data[0]['external_due_date'] != '-') {
      $day_dif = round((strtotime($data[0]['external_due_date']) - strtotime($data[0]['internal_due_date'])) / (60 * 60 * 24));

      if ($day_dif < 0) {
        $data[0]['internal_due_date'] = $data[0]['external_due_date'];
        $int_delay                    = round((time() - strtotime($data[0]['internal_due_date'])) / (60 * 60 * 24));
        $data[0]['internal_delay']    = ($int_delay > 0 && ($task_status != 'complete' && $task_status != 5)) ? $int_delay . 'Days Delay' : '';
      }
    }
    return json_encode($data);
  }

}

?>
