<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web_to_lead extends CI_Controller {
public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Common_mdl','Security_model'));                   
    }    
    public function referral($code)
    {
        $query = "SELECT wlt.id,c.id as client_id 
                                    FROM client as c 
                                        INNER JOIN web_lead_template as wlt on wlt.firm_id = c.firm_id and wlt.default_template=1 
                                        WHERE crm_other_send_invit_link REGEXP '/".$code."$' 
                                ";
        $data = $this->db->query( $query )->row_array();
        if(!empty( $data ))
        {
            $this->web_lead_form( $data['id'] , $data['client_id'] );
        }
    }
       public function mailsettings()
    
    {   
        $this->load->library('email');
        $config['wrapchars'] = 76; // Character count to wrap at.
        $config['mailtype'] = 'html'; // text or html Type of mail. If you send HTML email you 
        $this->email->initialize($config);  
    }

    public function index($id=false)
    { 
        $this->Security_model->chk_login();
        $data['value']=$this->Common_mdl->select_record('web_lead_template','id',0);
        $data['Language'] = $this->Common_mdl->getallrecords('languages');
        $data['result'] = $this->db->query(" select * from web_lead_template where firm_id =".$_SESSION['firm_id']."")->result_array();

        $data['leads_status']=$this->Common_mdl->dynamic_status('leads_status');

        $data['source']=$this->Common_mdl->dynamic_status('source');
        $this->load->view('web_to_lead/form_builder',$data);
    }
    /** 02-04-2018 rspt **/
    public function save_form_data(){

        $get_id=$_POST['get_id'];
        $data['form_id'] = $_POST['form_id'];
        $i=0;
        /************************************************/
               foreach($_POST['formData'] as $fdkey => $fdvalue) {
                    $res=strtolower((isset($fdvalue['label'])?$fdvalue['label']:""));
                    $res=str_replace(' ','_',$res);
                   // echo $_POST['formData'][$fdkey]['name'];
                    $name=isset($_POST['formData'][$fdkey]['name'])?$_POST['formData'][$fdkey]['name']:"";
                    if($name!="" && $name=="text-input")
                    {

                    $_POST['formData'][$fdkey]['name']=$res.'-'.$i; // change lable value
                    }
                    if($name!="" && $name=="textarea"  )
                    {

                    $_POST['formData'][$fdkey]['name']=$res.'-'.$i; // change lable value
                    }
                    if($name!="" && $name=="file-input"  )
                    {
                    $_POST['formData'][$fdkey]['name']=$res.'-'.$i; // change lable value
                    }
                    if(($name!="" && $name=="radio-group") || ($name!="" && $name=="select") )
                    {
                    $_POST['formData'][$fdkey]['name']=$fdvalue['name'].'-'.$i; // change lable value
                    }
                $i++;
                }
            // echo "check";
            // exit;
        /************************************************/
        $data['form_json']=json_encode($_POST['formjson']);
        $data['form_fields'] = json_encode($_POST['formData']);

        // print_r($data);
        // exit;
        $result=$this->Common_mdl->update('web_lead_template',$data,'id',$get_id);

      //  print_r( $get_id);
    
                     if($result){

                                 echo 1;
                    }

        
    }
     public function web_leads_view(){
 
    $this->Security_model->chk_login();

    $data['result'] = $this->db->query(" select * from web_lead_template where  firm_id ='".$_SESSION['firm_id']."' ORDER BY id DESC")->result_array();

    $this->load->view('web_to_lead/web_leads_template_view',$data);
    }

    public function archive_update()
    {
      if(isset($_POST['id']))
      {
        $this->db->query("update web_lead_template set old_status = lead_status,lead_status=23 where id=$_POST[id]");
        echo $this->db->affected_rows();
      }
    }

    public function web_leads_delete(){   
            $checkbox=$_POST['checkbox']; 
            for($i=0;$i<count($checkbox);$i++){           
             $id=$checkbox[$i];  
             $delete=$this->Common_mdl->delete('web_lead_template','id',$id);
            }
            $this->session->set_flashdata('success', 'Record Deleted Successfully!'); 
            redirect('web_to_lead/web_leads_view');
    }

    public function web_leads_update_contents($id){
           $data['Language'] = $this->Common_mdl->getallrecords('languages');
        
          $data['leads_status']=$this->Common_mdl->dynamic_status('leads_status');

        $data['source']=$this->Common_mdl->dynamic_status('source');
            $data['value']=$this->Common_mdl->select_record('web_lead_template','id',$id);
            $sql_val=$this->db->query("SELECT * FROM firm_assignees where module_id = ". $id." AND module_name='WEB_LEADS'")->result_array();

     foreach ($sql_val as $key1 => $value1) {
     $assign_data[] =$value1['assignees'];
     }
     $data['assign_data']=$assign_data;
            $this->load->view('web_to_lead/form_builder',$data);
    }


    /** end 02-04-2018 **/
    /** rspt 03-04-2018 **/
    public function web_lead_form($value,$rdmnum=false)
    { 
      if(isset($_SESSION['user_type']))
      {
        $data['value']=$this->db->query("SELECT * FROM `web_lead_template`  where id='$value' and `default_template`!=1 and firm_id=".$_SESSION['firm_id']." ")->result_array();

        if(!empty($data['value'])) {
     
        $this->load->view('web_to_lead/form_render',$data);
        }
        else
        {
            redirect('web_to_lead/web_leads_view');
        }

      }
      else
      {
        $data['value']=$this->db->query("SELECT * FROM `web_lead_template`  where id='$value'   ")->result_array();

              if($rdmnum!=false)
        {
          $data['rdmnum']=$rdmnum;
        }
        // print_r($data);
        // exit;


        if(!empty($data['value'])) {
     
        $this->load->view('web_to_lead/form_render',$data);
        }
        else
        {
            redirect('user');
        }


      }
        
      

    }
   
    public function web_lead_custom_form_insert()
    {
        $qry_data=$this->Common_mdl->select_record('web_lead_template','form_id',$_POST['form_id']);
        $form_data=json_decode($qry_data['form_fields']);
        $data=array();
         foreach ($form_data as $fdkey => $fdvalue) {
                $res=strtolower($fdvalue->label);
                $res=str_replace(' ','_',$res);
                if($fdvalue->type!='file' && $fdvalue->type!='header' && $fdvalue->type!='paragraph')
                {
                  
                    $data[$res]=$_POST[$res];
                }
                if($fdvalue->type=='file')
                {
                     $image_one='';
                     $image= (isset($_FILES[$res]['name'])&&($_FILES[$res]['name']!=''))? $this->Common_mdl->do_upload($_FILES[$res], 'uploads/weblead') : $image_one ;
                  
                     $data[$res]=$image;
                }
               
         }
        /* echo "<br>test<br>";
         exit;*/
                $table='web_lead_'.$_POST['form_id'];
                $result = $this->Common_mdl->insert($table,$data);
                //echo $result;die;
                $this->session->set_flashdata('success', 'Record Inserted Successfully!'); 

                redirect('web_to_lead/web_lead_form/'.$_POST['form_id']);

    }

    function f_chk()
    { 
echo '<iframe src="http://remindoo.org/CRMTool/web_to_lead/web_leadform/Test" class="iframe-forms card" style="width:100%;height:100%;border:none;"></iframe>';
    }

    function web_leadform($value)
    {

        $this->Security_model->chk_login();
        $new_value = str_replace('%20', ' ', $value);

        $data['value']=$this->Common_mdl->select_record('web_lead_template','form_id',$new_value);
        $this->load->view('web_to_lead/new_form_render',$data); 
    }

    function web_lead_custom_forminsert()
    {

         $asssign_group=array();
        $new_array=array_keys($_POST);
        $file_array=array_keys($_FILES);
      
        // $old_array=array('name','position','email_address','phone','company','address','city','state','country','zip_code','description','website','form_id');
         $old_array=array('name','position','email','phone','company','address','city','state','country','zip_code','description','website','form_id','iframe');
        $merge_fields=array_diff($new_array,$old_array);

        $qry_data=$this->Common_mdl->select_record('web_lead_template','id',$_POST['form_id']);

          $sql_val=$this->db->query("SELECT * FROM firm_assignees where module_id = ".$_POST['form_id']." AND module_name='WEB_LEADS'")->result_array();

             foreach ($sql_val as $key1 => $value1) {
             $asssign_group[] =$value1['assignees'];
             }
       
                $form_data=json_decode($qry_data['form_fields']);
                $form_name = $qry_data['form_id'];
                $data=array();
    
                  //  echo $fdvalue->type."--".$fdvalue->label."<br>";
                    $data['name']=(isset($_POST['name']))? $_POST['name'] : '' ;
                    $data['position']=(isset($_POST['title']))? $_POST['title'] : '' ;
                    $data['email_address']=(isset($_POST['email']))? $_POST['email'] : '';
                    $data['phone']= (isset($_POST['phonenumber']))? $_POST['phonenumber'] : ''; 
                    $data['company']= (isset($_POST['company']))? $_POST['company'] : '';
                    $data['address']= (isset($_POST['address']))? $_POST['address'] : ''; 
                    $data['city']=(isset($_POST['city']))? $_POST['city'] : ''; 
                    $data['state']=(isset($_POST['state']))? $_POST['state'] : ''; 
                    $data['country']=(isset($_POST['country']))? $_POST['country'] : ''; 
                    $data['zip_code']=(isset($_POST['zip']))? $_POST['zip'] : ''; 
                    $data['description']=(isset($_POST['description']))? $_POST['description'] : '';
                    $data['website']=(isset($_POST['website']))? $_POST['website'] : ''; 
                    $data['createdTime'] = time();
                    $data['form_id'] = $_POST['form_id'];
                    $data['user_id'] = (isset($_SESSION['id']))? $_SESSION['id'] : '1' ;
                    $data['default_language'] = $qry_data['language'];
                    $data['lead_status'] = $qry_data['lead_status'];
                    $data['source'] = $qry_data['lead_source'];


                    $full_url = (isset($_SERVER['HTTP_REFERER']))?$_SERVER['HTTP_REFERER']:$_SERVER['REQUEST_URI'];

                    $full_url=explode("/",  $full_url);
                 

                    //exit;
                    if($_POST['rdmnum']!='')
                    {
                    	  $data['rndm_num'] = $_POST['rdmnum'];
                    }

                    // print_r($full_url);
                    // print_r($_POST);
                    // print_r($_GET);
                    // exit;
                   

                    $team=array();
                    $department=array();
                    $assign=array();
        
                    $data['firm_id'] = $qry_data['firm_id'];
                    $_SESSION['firm_id']= $qry_data['firm_id'];
                    $data['contact_date'] = date('Y-m-d');
                    $data['contact_today'] = 'on';

                   $result = $this->Common_mdl->insert('leads',$data);
               
                  $lead_id='';
                     if($result){
                        $image_one='';
                        $lead_id = $this->db->insert_id();

                        $this->Common_mdl->add_assignto(implode(",",$asssign_group),$lead_id,'LEADS');

                        $datas['lead_id'] = $lead_id;
                        $datas['user_id'] = (isset($_SESSION['id']))? $_SESSION['id'] : '1' ;
                        $datas['CreatedTime'] =time();
                        
                            $myObj = new stdClass();
                                foreach ($merge_fields as $key => $value) {
                                  $myObj->$value = $_POST[$value];

                                }
    
                        foreach ($file_array as $key => $value) {




                          $datas['attachments'] = (isset($_FILES[$value]['name'])&&($_FILES[$value]['name']!=''))? $this->Common_mdl->do_upload($_FILES[$value], 'uploads/leads/attachments') : $image_one ;

                            $myObj->$value =  $datas['attachments'] ;
                            if($datas['attachments']!=$image_one){
                        $this->Common_mdl->insert('leads_attachments',$datas);
                      }
                        }
                    
                        $myJSON = json_encode($myObj);
                        $custom_data['custom_fields']=$myJSON;
                        $this->Common_mdl->update('leads',$custom_data,'id',$lead_id);
            

                        $notify_to = $qry_data['notify_to'];
                        if($notify_to=='responsible_person' || $notify_to=='specific_staff')
                        {
                            $notify_id = $qry_data['notify_user_id']; 
                        }else{
                         $notify_id = $qry_data['notify_role_id']; 
                        }
                        $this->sent_notify_mail($notify_to,$notify_id);



                        $activity_datas['log'] = "New Lead Created";
                        $activity_datas['createdTime'] = time();
                        $activity_datas['module'] = 'Lead';
                        $activity_datas['module_id'] = $lead_id;
                        $this->Common_mdl->insert('activity_log',$activity_datas);


                  //  }
                }

              if($lead_id!='')
              {
                // if(isset($_POST['iframe']))
                // {
                   $this->web_lead_email($lead_id);
               // }
              }


                 $this->session->set_flashdata('success', 'Record Inserted Successfully!'); 
                if(isset($_POST['iframe']))
                {
                      //redirect('web_to_lead/web_lead_form/'.$qry_data['id']); 
                  echo 1;
                    //redirect('web_to_lead/web_leads_view');
                }
                else
                {
                     redirect('web_to_lead/web_leadform/'.$form_name); 
                }

              
         }
      
    /** end 04-04-208 **/


    public function add_form_info()
    {


        $form_rec_id = $_POST['form_rec_id'];
        $data['form_id'] = $_POST['form_name'];
        $data['language'] = $_POST['default_language'];
        // $data['lead_source'] = $_POST['source'];
        $data['lead_status'] = $_POST['lead_status'];
        
         if(is_array($_POST['source']))
        {
            $data['lead_source'] = implode(',',$_POST['source']);
        }
        else{
              $data['lead_source'] = $_POST['source'];
        }
       

      
        $data['createdTime'] = time();
        $data['create_by']=$_SESSION['id'];
        $data['firm_id'] = $_SESSION['firm_id'];
        if($form_rec_id==''){
        $result = $this->Common_mdl->insert('web_lead_template',$data);
        $id=$this->db->insert_id();
         $this->Common_mdl->add_assignto($_POST['assign_role'],$id,'WEB_LEADS');
         echo $id;
        }else{
            $result=$this->Common_mdl->update('web_lead_template',$data,'id',$form_rec_id);
             $this->Common_mdl->add_assignto($_POST['assign_role'],$form_rec_id,'WEB_LEADS',1);
            echo $form_rec_id;
        }
        

    }

    public function sent_notify_mail($notify_to,$notify_id)
    {
                        if($notify_to=='responsible_person' || $notify_to=='specific_staff')
                        {
                            $array[] = $notify_id; 
                            $get_user_mailid = $this->db->query("SELECT * FROM `user` WHERE `id` IN('".implode("','",$array)."')")->result_array();
                        }else{
                            $array[] = $notify_id;
                            $get_user_mailid = $this->db->query("SELECT * FROM `user` WHERE `role` IN('".implode("','",$array)."')")->result_array();
                        }

                      /*  echo '<pre>';
                        print_r($get_user_mailid);die;*/
                         foreach ($get_user_mailid as $g_key => $g_value) {
                                $to_mail = $g_value['crm_email_id'];
                                //$to_mail = "lakshmipriya.techleaf@gmail.com";
                                if($to_mail!='')
                                {
                                    $this->mailsettings();
                                    $content = $this->load->view('web_lead_notify.php', '', true);
                                    $this->load->library('email');
                                    $this->email->set_mailtype("html");
                                    $this->email->from('info@techleafsolutions.com');
                                    $this->email->to($to_mail);
                                    $this->email->subject("Web to lead");
                                    $this->email->message($content);
                                   /* echo '<pre>';
                                    print_r($content);die;*/
                                    $send = $this->email->send();
                                }
                            }

    }


    public function web_leadsDelete()
    {
      if(isset($_POST['id']))
      {
          $id = trim($_POST['id']);
          $this->db->query("DELETE FROM web_lead_template WHERE id in ($id)"); 
         echo  $this->db->affected_rows();
      }  
    }

    public function web_lead_email($lead_id=false){
                $get_leads_data=$this->Common_mdl->GetAllWithWhere('leads','id',$lead_id);
                 $get_lead_name=$get_leads_data[0]['name'];
                 $get_lead_id=$lead_id;
                 $type='web_to_leads';
                 $message="This Lead ".$get_lead_name." Assign to You " ;


                 $get_assigned=Get_Module_Assigees('LEADS',$lead_id);

                 if($get_assigned!='')
                 {
                  foreach ($get_assigned as $key => $value) {
                     $get_user_data=$this->Common_mdl->GetAllWithWhere('user','id',$value);
                     if(count($get_user_data)>0){
                       $it_name=$get_user_data[0]['crm_name'];
                     $it_email=$get_user_data[0]['crm_email_id'];

                      $this->Common_mdl->Send_Notification( 'Leads' , $type, $value , $message,'',$lead_id );

                      if($it_email!=''){
                       $this->weblead_email($get_lead_name,$lead_id,$it_name,$it_email,'assign','','');
                      }
                     }
                  }
               
                 }
   
    }

    public function weblead_email($lead_name,$lead_id,$name,$email_id,$field,$team,$depart)
    {

       $data2['username'] = $name;
      
        $email_content="This Lead ".$lead_name." Assign to You " ;
      
       
            $email_subject  = 'Weblead Form Notify';
            $data2['email_contents']  = '<a href='.base_url().'leads/leads_detailed_tab/'.$lead_id.' style="display: inline-block;text-align: center;color: #00aeef;text-decoration: none;font-family:;background: #00aeef;color: #fff;font-weight: 600;padding: 5px 10px;border-radius: 3px;">Click Here To view Lead</a><p>'.$email_content.'</p>';
           $data2['title']='New Lead Assigne to you';
           //$data2['link']=base_url();
            //email template
           $body = $this->load->view('remainder_email.php',$data2, TRUE);
            //redirect('login/email_format');
          
           $this->load->library('email');
           $this->email->set_mailtype("html");
            //$this->mailsettings();
            $this->email->from('info.techleaf@gmail.com');
         //   $this->email->to('shanmugapriya.techleaf@gmail.com');$email_id
            $this->email->to($email_id);
            $this->email->subject($email_subject);
            $this->email->message($body);
            $send = $this->email->send();
            if($send)
            {
             // echo "email send ";
            }
            else
            {
             // echo "email not send ";
            }
    }


}
?>
