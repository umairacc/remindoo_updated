<?php

class Taskcustomisation extends CI_Controller 
{
        public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
		public function __construct()
		{
			parent::__construct();
			$this->load->model(array('Common_mdl','Report_model','Security_model'));
			$this->load->library('Excel');
		}

	    public function Task_custom()
	    {
			if(empty($_POST['priority']))
			{
				$priority_type = "";
			}
			else
			{
				$priority = implode(',',$_POST['priority']);
				$priority_type = 'AND FIND_IN_SET(priority,"'.$priority.'")';
			}

			if(empty($_POST['status'])) 
			{				
				$status = 'AND task_status IN("notstarted","inprogress","awaiting","archive","complete")';
			}
			else
			{
				$poststatus = implode(',',$_POST['status']);
				$status = 'AND FIND_IN_SET(task_status,"'.$poststatus.'")';
			}
		

			if(!empty($_POST['filter']) && $_POST['filter'] != 'all')
			{					
				if($_POST['filter']=='week')
				{
					$current_date = date('Y-m-d');
					$week = date('W', strtotime($current_date));
					$year = date('Y', strtotime($current_date));

					$dto = new DateTime();
					$start_date = $dto->setISODate($year, $week, 0)->format('Y-m-d');
					$end_date = $dto->setISODate($year, $week, 6)->format('Y-m-d');
					$end_date = date('Y-m-d', strtotime($end_date . ' +1 day')); 
				}
                else if($_POST['filter']=='three_week')
                {
					$current_date = date('Y-m-d');
					$weeks = strtotime('- 3 week', strtotime( $current_date ));
					$start_date = date('Y-m-d', $weeks); 
					$addweeks = strtotime('+ 3 week', strtotime( $start_date ));
					$end_Week = date('Y-m-d', $addweeks); 
					$end_date = date('Y-m-d', strtotime($end_Week . ' +1 day'));
				}

				else if($_POST['filter']=='month')
				{
					$start_date = date("Y-m-01");
					$month_end = date("Y-m-t"); 
					$end_date = date('Y-m-d', strtotime($month_end . ' +1 day'));
				}

				if($_POST['filter']=='Three_month')
				{
					$end_date = date('Y-m-d',strtotime(date('Y-m-d').'+1 day'));
					$start_date = date('Y-m-d',strtotime('- 3 month', strtotime($end_date)));
				}	

				$filter = 'AND (start_date BETWEEN "'.$start_date.'" AND "'.$end_date.'" OR end_date BETWEEN "'.$start_date.'" AND "'.$end_date.'")';
		    }
		    else if(!empty($_POST['from_date']) && !empty($_POST['to_date']))
			{
				$start_date = $_POST['from_date'];
				$end_date = $_POST['to_date'];
				$filter = 'AND (start_date BETWEEN "'.$start_date.'" AND "'.$end_date.'" OR end_date BETWEEN "'.$start_date.'" AND "'.$end_date.'")';
			}
		    else
		    {
		    	$filter = '';
		    }			

			if(!empty($_POST['client']))
			{
                $rec = $this->Report_model->selectRecord('client','id', $_POST['client']);
                $client = 'AND user_id = "'.$rec['user_id'].'"';
			}
			else
			{
				$client = '';
			}	
			
			$Assigned_Task = Get_Assigned_Datas('TASK');
			$assigned_task = implode(',',$Assigned_Task);

			$data['records'] = $this->db->query('SELECT * from add_new_task WHERE FIND_IN_SET(id,"'.$assigned_task.'") AND firm_id = "'.$_SESSION['firm_id'].'" '.$priority_type.' '.$status.' '.$filter.' '.$client.' ORDER BY id ASC')->result_array();
					
		    $this->load->view('reports/task_customresult',$data);
	    }


} ?>
