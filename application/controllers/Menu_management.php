<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_management extends CI_Controller 
{
 	public $IconUploadPath = 'uploads/menu_icons/';

	public function __construct()
	{
		parent::__construct();
		$this->load->model( ['Common_mdl','Security_model','Loginchk_model'] );
		$this->load->helper('comman_helper');
	}
	public function Check_Login_User()
    {

      if( !empty( $_COOKIE['remindoo_logout'] ) && $_COOKIE['remindoo_logout']=="remindoo_superadmin" )
      {
        $this->Loginchk_model->superAdmin_LoginCheck();
      }
      else
      {
        $this->Security_model->chk_login();
      }

    }
	// public function index()
	// {	
	// 	$this->Check_Login_User();
	// 	if( $_SESSION['permission']['Menu_Management_Settings']['view'] != 1 )
	//     {
	//       $this->load->view('users/blank_page');
	//     }
	//     else 
	//     {
	// 		$data['modules'] = $this->db->query('select * from module_mangement')->result_array();
	// 		$data['menus'] = $this->getMenus();

	// 		$this->load->view("menu_management/custom_menu_view",$data);
	// 	}	
	// }	

	public function index(){
	
		$this->Check_Login_User();
		
	  	$result=array();

      if( $_SESSION['permission']['Menu_Management_Settings']['view'] != 1 )
      {
        $this->load->view('users/blank_page');
      }
      else 
      {
      $data['modules'] = $this->db->query('select * from module_mangement')->result_array();
      $data['menus'] = $this->db->query('select * from menu_manage WHERE firm_id="'.$_SESSION['firm_id'].'" ')->row_array();

      if(empty($data['menus']))
     {
     	$admin_data=$this->db->query('select `menu_details` from menu_manage WHERE firm_id="0" ')->row_array();

     	$admin_data['firm_id']=$_SESSION['firm_id'];
     	$this->db->insert('menu_manage',$admin_data);
     	 $data['menus'] = $this->db->query('select * from menu_manage WHERE firm_id="'.$_SESSION['firm_id'].'" ')->row_array();

     }


      $this->load->view('menu_management/custom_menu',$data);
     }

     }


	public function createMenu()
	{	
		$this->Check_Login_User();
		$fileName = $this->Common_mdl->do_upload( $_FILES['menu_icon'] , $this->IconUploadPath ); 

		if( !empty( $fileName ) )
		{       		      	
       		$data['icon_path'] = base_url().$this->IconUploadPath.$fileName;
       	}

		$data['parent_id'] = $_POST['parent_id'];
		$data['label'] = $_POST['menu_name'];
		if( $_POST['link_type'] == 'direct' )
		{
			$data['url'] = $_POST['menu_url'];
		}
		$data['firm_id'] = $_SESSION['firm_id'];
		echo  $this->Common_mdl->insert('menu_management',$data);
		 
	}

	public function editMenu( $id )
	{
	    $fileName = $this->Common_mdl->do_upload( $_FILES['emenu_icon'] , $this->IconUploadPath );

		if( !empty( $fileName ) )
		{
	       	$data['icon_path'] = base_url().$this->IconUploadPath.$fileName;
	    }
	    
       	$data['label'] = $_POST['emenu_name'];
       	if( $_POST['link_type'] == 'direct' )
		{
			$data['url'] = $_POST['emenu_url'];
		}
		else
		{
			$data['url'] = '';
		}
       	

       	echo $this->Common_mdl->update('menu_management' , $data , 'menu_management_id' , $id );
	}

	public function deleteMenu( $id )
	{
		$this->db->query("delete from menu_management where menu_management_id = $id or parent_id = $id");

		echo $this->db->affected_rows();
	}

	public function getEditData( $id )
	{
		
		$data = $this->db->query("select * from menu_management where menu_management_id = $id")->row_array();

		echo json_encode($data);
	}

	public function delop()
	{
		

		/*$this->load->view('includes/header1');
		$this->load->view('includes/footer');
*/

			$this->load->view("menu_management/custom_menu_view1");


		
		//echo htmlspecialchars("<ul>".getHeader( $PARENT_LI , $CHILD_UL , $CHILD_LI )."</ul>"); die;
		
		/*$data['modules'] = getMenu(  $PARENT_LI , $CHILD_UL , $CHILD_LI );
		$data['menus'] = menu_array();
		echo "<pre>";
		print_r($data['menus']);
		die;
		$this->load->view("menu_management/custom_menu_view1",$data);*/
	}

	public function getMenus()
	{
	 

	  $parent_menu = $this->db->query("SELECT * FROM `menu_management` where firm_id =".$_SESSION['firm_id']." AND `parent_id`=0 ORDER BY menu_management_id ASC")->result_array();

	  $menu_data = [];

	  foreach ($parent_menu as $datas)
	  {
	    $menu_data[] = array_merge( $datas , ['data_level' => 1 ] );                       

	    $menu_data = $this->subMenu( $datas['menu_management_id'] , $menu_data);
	  }

	  return $menu_data; 

	}


	public function subMenu( $id , $menu_data , $data_level=2 )
	{    
	  $child_menu = $this->db->query("SELECT * FROM `menu_management` where firm_id =".$_SESSION['firm_id']." AND `parent_id`=$id ORDER BY menu_management_id ASC")->result_array();

	  foreach ($child_menu as $datas)
	  {
	    $menu_data[] = array_merge( $datas , [ 'data_level' => $data_level ] );                       

	    $menu_data = $this->subMenu( $datas['menu_management_id'] , $menu_data , $data_level+1 );
	  }

	  return  $menu_data;
	}
	  public function menu_mangement(){
	
	  	$result=array();

      if( $_SESSION['permission']['Menu_Management_Settings']['view'] != 1 )
      {
        $this->load->view('users/blank_page');
      }
      else 
      {
      $data['modules'] = $this->db->query('select * from module_mangement')->result_array();
      $data['menus'] = $this->db->query('select * from menu_manage WHERE firm_id="'.$_SESSION['firm_id'].'" ')->row_array();

      if(empty($data['menus']))
     {
     	$admin_data=$this->db->query('select `menu_details` from menu_manage WHERE firm_id="0" ')->row_array();

     	$admin_data['firm_id']=$_SESSION['firm_id'];
     	$this->db->insert('menu_manage',$admin_data);
     	 $data['menus'] = $this->db->query('select * from menu_manage WHERE firm_id="'.$_SESSION['firm_id'].'" ')->row_array();

     }


      $this->load->view('menu_management/custom_menu',$data);
     }

     }

     public function menu_update(){
     	if(isset($_POST['menu_details']))
     	{
     	$data=array('menu_details'=>$_POST['menu_details']);

     	if($_POST['menu_id']!=1){
     	$this->db->where('id',$_POST['menu_id']);
     	}
     	
	  	$this->db->update('menu_manage',$data);
	  	}
	  	echo 1;

     }
     public function menu_icon_upload()
     {
  //    	 $fileName = $this->Common_mdl->do_upload( $_FILES['menu_icon'] , $this->IconUploadPath ); 

		// if( !empty( $fileName ) )
		// {       		      	
  //      		$data['icon_path'] = base_url().$this->IconUploadPath.$fileName;
  //      	}

     $photo = $this->Reduce_Size($_POST['reduce_image'], $this->IconUploadPath ); 
     $val=$this->IconUploadPath.$photo;
     echo $val;

     }
     public function Reduce_Size( $file, $path) { 

// $reduce_image = $file; 
 $unique_id = uniqid(); 
$uploads_path = $path; $file_type = substr($file, 5, strpos($file, ';')-5);
$ex_type = explode('/', $file_type);
 $img = str_replace('data:image/png;base64,', '', $file); 
 $img = str_replace(' ', '+', $img); $imgdata = base64_decode($img); 
 // $file = $uploads_path . time().'_'.rand(5,10).'.png'; 
 $file = $uploads_path . $unique_id .'.png';
  $up_image = file_put_contents($file, $imgdata); 
   if($up_image) { 
   	$data = $unique_id.'.png'; return $data; 
}
  }


}

?>