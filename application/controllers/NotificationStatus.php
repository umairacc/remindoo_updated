<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NotificationStatus extends CI_Controller{   
  public function __construct(){
    parent::__construct();    
    $this->load->model([
      'Service_Reminder_Model',
      'Service_model',
      'Task_section_model',
      'Common_mdl',
      'Security_model',
      'Invoice_model',
      'Task_invoice_model',
      'Task_creating_model'
    ]);
    $this->load->helper([
      'download',
      'template_decode'
    ]);
  }
  
  public function change_status($IsSend=null, $record_key=null){
    //$IsSend       = $_POST['IsSend'];
    $update_data  = [
      'smtp_response' => json_encode( $IsSend ),
      'status'        => ( ($IsSend['result'] == 1 || $IsSend['result'] == '1') ? 1 : 2 )
    ];
    //echo $this->db->update( 'email' , $update_data ,'id='.$emails_data['id'] );
    $sql    = "UPDATE email SET status  = ".$update_data['status'].", smtp_response = '".$update_data['smtp_response']."' WHERE id = ".$record_key.";";
    $result = $this->db->query($sql);
    echo $result;
    die;
  }
} ?>