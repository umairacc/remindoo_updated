<?php

use function GuzzleHttp\Promise\task;

defined('BASEPATH') or exit('No direct script access allowed');

class Task extends CI_Controller
{
  public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   *      http://example.com/index.php/welcome
   *  - or -
   *      http://example.com/index.php/welcome/index
   *  - or -
   * Since this controller is set as the default controller in
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see https://codeigniter.com/user_guide/general/urls.html
   */
  public function __construct()
  {    
    parent::__construct();
    // $this->load->model(array('Common_mdl','Security_model','Invoice_model','Task_invoice_model','Task_creating_model','Task_section_model'));

    $this->load->model(array('Service_model', 'Demo_model', 'Common_mdl', 'Security_model', 'Invoice_model', 'Task_invoice_model', 'Task_creating_model', 'Task_section_model', 'Loginchk_model', 'Service_Reminder_Model', 'FirmFields_model', 'Client_model'));

    $this->load->helper('download');
  }

  public function index()
  {    
    if ($_SESSION['permission']['Task']['view'] == 1) {

      $this->Security_model->chk_login();

      $get_this_qry       = $this->db->query("SELECT * FROM user WHERE id=" . $_SESSION['id'] ." ORDER BY id DESC LIMIT 1")->row_array();
      $created_id         = $get_this_qry['firm_admin_id'];
      $its_firm_admin_id  = $get_this_qry['firm_admin_id'];
      $it_session_id      = $_SESSION['id'];
      $res2               = array();
      $result             = array();
      
      array_push($result, $it_session_id);
      
      $reassign_firm_id = $its_firm_admin_id;
      $tot_val          = 1;
      $ques             = Get_Assigned_Datas('TASK');
      $result           = (!empty($ques)) ? implode(',', array_filter(array_unique($ques))) : '0';
      
      // $res=implode(',', $over_all_res);
      $sql_1              = "SELECT id,
                                    user_id,
                                    billable,
                                    public,
                                    subject,
                                    start_date,
                                    end_date,
                                    priority,
                                    related_to,
                                    company_name,
                                    sub_task_id,
                                    created_date,
                                    worker,
                                    team,
                                    department,
                                    manager,
                                    task_status,
                                    for_old_assign,
                                    create_by 
                            FROM add_new_task tl 
                            WHERE tl.id IN ($result)
                            ORDER BY id desc ";
      $data['task_list']  = ($result != 0) ? $this->db->query($sql_1)->result_array() : [];
      /*$sql_2              = "SELECT count(*) as task_count 
                            FROM add_new_task tl 
                            WHERE tl.id IN ($result)
                            ORDER BY id desc";
      $task_list          = $this->db->query($sql_2)->row_array();*/
      $task_list['task_count'] = count($data['task_list']);
      
      // if (!empty($result) && (($key = array_search($_SESSION['id'], $result)) !== false)) {
      // unset($result[$key]);
      // }
      //  $result=(!empty($result))?implode(',',array_filter(array_unique($result))):'';          
      // }
      $data['task_status']  = $this->db->query('SELECT * FROM task_status WHERE is_superadmin_owned=1')->result_array();
      $data['row_count']    = $task_list['task_count'];
      // $data['staff_form'] = $this->Common_mdl->GetAllWithWheretwo('user','role','6','firm_id',$_SESSION['id']);
      $data['staff_form']   = $this->Common_mdl->GetAllWithWheretwo('user', 'user_type', 'FU', 'firm_id', $_SESSION['firm_id']);

      /** end of permission **/
      $this->load->view('tasks/task_timeline', $data);
    } else {
      $this->load->view('users/blank_page');
    }
  }


  public function chat_page()
  {
    $this->Security_model->chk_login();
    $this->load->view('tasks/sample_page');
  }


  public function fetch_data()
  {

    $s = '';
    $s1 = '';
    $s2 = '';
    $a = '';
    $b = '';
    $c = '';
    $cond1 = '';
    $cond2 = '';
    $cond3 = '';
    $cond4 = '';
    if (isset($_SESSION['related_to']) && $_SESSION['related_to'] != '') {

      $a = 1;
      $cond1 = 'related_to ="' . $_SESSION['related_to'] . '"';
    }

    if (isset($_SESSION['task_status']) && $_SESSION['task_status'] != '') {
      if ($a == '1') {
        $s = 'and';
      } else {
        $s = '';
      }
      $b = 1;
      $cond2 = 'task_status ="' . $_SESSION['task_status'] . '"';
    }

    if (isset($_SESSION['company_name']) && $_SESSION['company_name'] != '') {
      if ($b == '1' || $a == '1') {
        $s1 = 'and';
      } else {
        $s1 = '';
      }
      $c = 1;
      $cond3 = 'company_name ="' . $_SESSION['company_name'] . '"';
    }

    if (isset($_SESSION['worker']) && $_SESSION['worker'] != '') {
      $this->session->set_flashdata('worker', $_POST['worker']);
      if ($c == '1' || $a == '1' || $b == '1') {
        $s2 = 'and';
      } else {
        $s2 = '';
      }
      $cond4 = 'worker in (' . $_SESSION['worker'] . ')';
    }

    if ((isset($_SESSION['related_to']) && $_SESSION['related_to'] != '') || (isset($_SESSION['task_status']) && $_SESSION['task_status'] != '') || (isset($_SESSION['worker']) && $_SESSION['worker'] != '') || (isset($_SESSION['company_name']) && $_SESSION['company_name'] != '')) {

      $sql =  'and' . ' ( ' . $cond1 . ' ' . $s . ' ' . $cond2 . ' ' . $s1 . ' ' . $cond3 . ' ' . $s2 . ' ' . $cond4 . ')';
      $row = 15;
    } else {
      $sql = '';
      $row = $_POST['row'];
    }




    $rowperpage = 15;

    $team_num = array();
    $for_cus_team = $this->db->query("select * from team_assign_staff where FIND_IN_SET('" . $_SESSION['id'] . "',staff_id)")->result_array();
    foreach ($for_cus_team as $cu_team_key => $cu_team_value) {
      array_push($team_num, $cu_team_value['team_id']);
    }
    if (!empty($team_num) && count($team_num) > 0) {
      $res_team_num = implode('|', $team_num);
    } else {
      $res_team_num = 0;
    }

    $department_num = array();
    $for_cus_department = $this->db->query("SELECT * FROM `department_assign_team` where CONCAT(',', `team_id`, ',') REGEXP ',($res_team_num),'")->result_array();
    foreach ($for_cus_department as $cu_dept_key => $cu_dept_value) {
      array_push($department_num, $cu_dept_value['depart_id']);
    }
    if (!empty($department_num) && count($department_num) > 0) {
      $res_dept_num = implode('|', $department_num);
    } else {
      $res_dept_num = 0;
    }
    /** end **/
    $sql_qry = '';
    $sql_qry_1 = '';
    //$sql_qry_2='';
    $it_session_id = '';
    $get_this_qry = $this->db->query("select * from user where id=" . $_SESSION['id'])->row_array();
    $created_id = $get_this_qry['firm_admin_id'];
    $its_firm_admin_id = $get_this_qry['firm_admin_id'];
    if ($_SESSION['role'] == 6) // staff
    {
      $it_session_id = $_SESSION['id'];
    } else if ($_SESSION['role'] == 5) //manager
    {
      $it_session_id = $created_id;
    } else if ($_SESSION['role'] == 4) //client
    {
      $it_session_id = $created_id;
    } else if ($_SESSION['role'] == 3) //director
    {
      $it_session_id = $created_id;
    } else if ($_SESSION['role'] == 2) // sub admin
    {
      $it_session_id = $created_id;
    } else {
      $it_session_id = $_SESSION['id'];
    }
    /** 07-07-2018 for permission **/
    if ($_SESSION['role'] == 6) {

      $task_list = $this->db->query("SELECT id,user_id,billable,public,subject,start_date,end_date,priority,related_to,company_name,sub_task_id,created_date,worker,team,department,manager,task_status,for_old_assign,create_by FROM add_new_task tl where tl.create_by=" . $it_session_id . "  
        UNION 
        SELECT * FROM add_new_task tls where tls.create_by!=" . $it_session_id . " and (FIND_IN_SET('" . $it_session_id . "',worker) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `department`, ',') REGEXP ',($res_team_num),') or FIND_IN_SET('" . $it_session_id . "',manager) ) " . $sql . " order by id desc limit " . $row . "," . $rowperpage . "")->result_array();
    } else if ($_SESSION['role'] == 4) {
      $result = array();
      array_push($result, $it_session_id);
      $reassign_firm_id = $its_firm_admin_id;
      $tot_val = 1;
      for ($z = 0; $z < $tot_val; $z++) {
        if ($reassign_firm_id == 0) {
          $tot_val = 0;
        } else {
          $get_this_qry = $this->db->query("select * from user where id=" . $reassign_firm_id)->row_array();
          if (!empty($get_this_qry)) {
            array_push($result, $get_this_qry['id']);
            $new_created_id = $get_this_qry['firm_admin_id'];
            $reassign_firm_id = $get_this_qry['firm_admin_id'];
          } else {
            $tot_val = 0;
          }
        }
      }
      if (!empty($result) && (($key = array_search($_SESSION['id'], $result)) !== false)) {
        unset($result[$key]);
      }
      $res = (!empty($result)) ? implode(',', array_filter(array_unique($result))) : '';
      $sql = $this->db->query("select * from client where user_id=" . $_SESSION['id'] . "")->row_array();
      echo "<pre>" . print_r($sql);
      die;
      $its_id = $sql['id'];
      $task_list = $this->db->query("SELECT id,user_id,billable,public,subject,start_date,end_date,priority,related_to,company_name,sub_task_id,created_date,worker,team,department,manager,task_status,for_old_assign,create_by FROM add_new_task tl where tl.company_name=" . $its_id . " " . $sql . " order by id desc limit " . $row . "," . $rowperpage . "")->result_array();
    } else if ($_SESSION['role'] == 5) {
      $result = array();
      array_push($result, $it_session_id);
      $reassign_firm_id = $its_firm_admin_id;
      $tot_val = 1;
      for ($z = 0; $z < $tot_val; $z++) {
        if ($reassign_firm_id == 0) {
          $tot_val = 0;
        } else {
          $get_this_qry = $this->db->query("select * from user where id=" . $reassign_firm_id)->row_array();
          if (!empty($get_this_qry)) {
            array_push($result, $get_this_qry['id']);
            $new_created_id = $get_this_qry['firm_admin_id'];
            $reassign_firm_id = $get_this_qry['firm_admin_id'];
          } else {
            $tot_val = 0;
          }
        }
      }
      if (!empty($result) && (($key = array_search($_SESSION['id'], $result)) !== false)) {
        unset($result[$key]);
      }
      $res = (!empty($result)) ? implode(',', array_filter(array_unique($result))) : '';
      $task_list = $this->db->query("SELECT id,user_id,billable,public,subject,start_date,end_date,priority,related_to,company_name,sub_task_id,created_date,worker,team,department,manager,task_status,for_old_assign,create_by FROM add_new_task tl where tl.create_by=" . $it_session_id . " 
        UNION
        SELECT * FROM add_new_task tls where tls.create_by!=" . $it_session_id . " and (FIND_IN_SET('" . $it_session_id . "',worker) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `department`, ',') REGEXP ',($res_team_num),') or FIND_IN_SET('" . $it_session_id . "',manager) ) " . $sql . "  order by id desc limit " . $row . "," . $rowperpage . "")->result_array();
    } else if ($_SESSION['role'] == 3 || $_SESSION['role'] == 2) {
      $result = array();
      $reassign_firm_id = $its_firm_admin_id;
      array_push($result, $it_session_id);
      $tot_val = 1;
      for ($z = 0; $z < $tot_val; $z++) {
        if ($reassign_firm_id == 0) {
          $tot_val = 0;
        } else {
          $get_this_qry = $this->db->query("select * from user where id=" . $reassign_firm_id)->row_array();
          if (!empty($get_this_qry)) {
            array_push($result, $get_this_qry['id']);
            $new_created_id = $get_this_qry['firm_admin_id'];
            $reassign_firm_id = $get_this_qry['firm_admin_id'];
          } else {
            $tot_val = 0;
          }
        }
      }
      $res = (!empty($result)) ? implode(',', array_filter(array_unique($result))) : '';
      $task_list = $this->db->query("SELECT id,user_id,billable,public,subject,start_date,end_date,priority,related_to,company_name,sub_task_id,created_date,worker,team,department,manager,task_status,for_old_assign,create_by  FROM add_new_task tl where tl.create_by in ($res)  " . $sql . " order by id desc limit " . $row . "," . $rowperpage . " ")->result_array();


      if (!empty($result) && (($key = array_search($_SESSION['id'], $result)) !== false)) {
        unset($result[$key]);
      }
      $res = (!empty($result)) ? implode(',', array_filter(array_unique($result))) : '';
    } else {
      $result = array();
      array_push($result, $it_session_id);
      $reassign_firm_id = $its_firm_admin_id;
      $tot_val = 1;
      for ($z = 0; $z < $tot_val; $z++) {
        if ($reassign_firm_id == 0) {
          $tot_val = 0;
        } else {
          $get_this_qry = $this->db->query("select * from user where id=" . $reassign_firm_id)->row_array();
          if (!empty($get_this_qry)) {
            array_push($result, $get_this_qry['id']);
            $new_created_id = $get_this_qry['firm_admin_id'];
            $reassign_firm_id = $get_this_qry['firm_admin_id'];
          } else {
            $tot_val = 0;
          }
        }
      }
      $res = (!empty($result)) ? implode(',', array_filter(array_unique($result))) : '';
      // $res=implode(',', $over_all_res);
      // echo "SELECT id,user_id,billable,public,subject,start_date,end_date,priority,related_to,company_name,sub_task_id,created_date,worker,team,department,manager,task_status,for_old_assign,create_by FROM add_new_task tl where tl.create_by in ($res) '(".$sql.")'  order by id desc limit ".$row.",".$rowperpage." ";


      $task_list = $this->db->query("SELECT id,user_id,billable,public,subject,start_date,end_date,priority,related_to,company_name,sub_task_id,created_date,worker,team,department,manager,task_status,for_old_assign,create_by FROM add_new_task tl where tl.create_by in ($res) " . $sql . "  order by id desc limit " . $row . "," . $rowperpage . " ")->result_array();


      if (!empty($result) && (($key = array_search($_SESSION['id'], $result)) !== false)) {
        unset($result[$key]);
      }
      $res = (!empty($result)) ? implode(',', array_filter(array_unique($result))) : '';
    }

    $html = '';

    foreach ($task_list as $task) {

      $html .= '<div class="cd-timeline-block ' . $task['id'] . '"><div class="cd-timeline-icon bg-primary"> <i class="icofont icofont-ui-file"></i> </div> <div class="cd-timeline-content card_main"> <div class="p-0"> <div class="btn-group dropdown-split-primary"> <button type="button" class="btn btn-primary search_word" >' . $task['task_status'] . '  </button> </div> </div><div class="p-20 search_word"> <h6>' . $task['subject'] . '</h6><div class="timeline-details"> <a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Start Date :';
      if (strtotime($task["start_date"]) != '') {

        $vv = date("d-m-Y", strtotime($task["start_date"]));
      } else {
        $vv = '';
      }


      $html .= '' . $vv . '</span> </a> <a href="#"> <i class="icofont icofont-ui-calendar"></i><span class="search_word">Due Date :';

      if (strtotime($task["end_date"]) != '') {

        $end = date("d-m-Y", strtotime($task['end_date']));
      } else {
        $end = '';
      }
      if ($task['related_to'] != 'leads' && is_numeric($task['company_name'])) {
        $value = $this->Common_mdl->getcompany_name($task["company_name"]);
      } else {
        $value = '-';
      }
      $html .= '' . $end . ' </span> </a><p class="m-t-0 search_word">Company Name : ' . $value . ' </p><p class="m-t-0 search_word">Staffs:';
      $explode_worker = explode(',', $task['worker']);
      $username = array();
      foreach ($explode_worker as $key => $val) {
        $getUserProfilename = $this->Common_mdl->getUserProfileName($val);
        array_push($username, $getUserProfilename);
      }
      $variable = array_filter($username);
      if (!empty($variable)) {
        $user_name = implode(',', $username);
      } else {
        $user_name = "Not Assigned";
      }
      $html .= '' . $user_name . '</p></div> </div></div></div>';
    }

    echo $html;
  }




  public function filter_data()
  {
    $s = '';
    $s1 = '';
    $s2 = '';
    $a = '';
    $b = '';
    $c = '';
    $cond1 = '';
    $cond2 = '';
    $cond3 = '';
    $cond4 = '';

    if (isset($_POST['related_to']) && $_POST['related_to'] != '') {
      $this->session->set_flashdata('related_to', $_POST['related_to']);
      $a = 1;
      $cond1 = 'related_to ="' . $_POST['related_to'] . '"';
    }

    if (isset($_POST['task_status']) && $_POST['task_status'] != '') {

      $this->session->set_flashdata('task_status', $_POST['task_status']);
      if ($a == '1') {
        $s = 'and';
      } else {
        $s = '';
      }
      $b = 1;
      $cond2 = 'task_status ="' . $_POST['task_status'] . '"';
    }

    if (isset($_POST['company_name']) && $_POST['company_name'] != '') {
      $this->session->set_flashdata('company_name', $_POST['company_name']);
      if ($b == '1' || $a == '1') {
        $s1 = 'and';
      } else {
        $s1 = '';
      }
      $c = 1;
      $cond3 = 'company_name ="' . $_POST['company_name'] . '"';
    }


    $result = array();
    array_push($result, $it_session_id);
    $reassign_firm_id = $its_firm_admin_id;
    $tot_val = 1;

    $sql = 0;
    $ques = Get_Assigned_Datas('TASK');




    $res = (!empty($ques)) ? implode(',', array_filter(array_unique($ques))) : '';

    if ($res != '') {
      $this->session->set_flashdata('worker', $_POST['worker']);
      if ($c == '1' || $a == '1' || $b == '1') {
        $s2 = 'and';
      } else {
        $s2 = '';
      }
      $cond4 = 'tl.id in (' . $res . ')';
    }



    $sql =  $cond1 . ' ' . $s . ' ' . $cond2 . ' ' . $s1 . ' ' . $cond3 . ' ' . $s2 . ' ' . $cond4;


    if (trim($sql) != '') {
      // echo 'y';
      $task_list = $this->db->query("SELECT id,user_id,billable,public,subject,start_date,end_date,priority,related_to,company_name,sub_task_id,created_date,worker,team,department,manager,task_status,for_old_assign,create_by FROM add_new_task tl where firm_id=" . $_SESSION['firm_id'] . " and company_name!='' and (" . $sql . ") order by id desc")->result_array();
    } else {
      // echo 'z';
      $task_list = array();
    }
    //  exit;

    if (!empty($result) && (($key = array_search($_SESSION['id'], $result)) !== false)) {
      unset($result[$key]);
    }
    $res = (!empty($result)) ? implode(',', array_filter(array_unique($result))) : '';


    if (isset($_POST['worker']) && $_POST['worker'] != '') {
      $this->session->set_flashdata('worker', $_POST['worker']);
      if ($c == '1' || $a == '1' || $b == '1') {
        $s2 = 'and';
      } else {
        $s2 = '';
      }
      $cond4 = 'worker in (' . $_POST['worker'] . ')';
    }


    //  }

    $over = array_filter($task_list);

    // print_r($over);
    // exit;

    $html = '';

    if (!empty($over)) {
      foreach ($task_list as $task) {

        $status_val = $this->Common_mdl->select_record('task_status', 'id', $task['task_status']);

        $html .= '<div class="cd-timeline-block ' . $task['id'] . '"><div class="cd-timeline-icon bg-primary"> <i class="icofont icofont-ui-file"></i> </div> <div class="cd-timeline-content card_main"> <div class="p-0"> <div class="btn-group dropdown-split-primary"> <button type="button" class="btn btn-primary search_word" >' . $status_val['status_name'] . '  </button> </div> </div><div class="p-20 search_word"><a href="' . base_url() . 'user/task_details/' . $task['id'] . '" target="_blank"> <h6 style="width: 78%;">' . $task['subject'] . '</h6></a><div class="timeline-details"> <a href="#"> <i class="icofont icofont-ui-calendar"></i><span class="search_word">Start Date :';
        if (strtotime($task["start_date"]) != '') {

          $vv = date("d-m-Y", strtotime($task["start_date"]));
        } else {
          $vv = '';
        }


        $html .= '' . $vv . '</span> </a> <a href="#"> <i class="icofont icofont-ui-calendar"></i><span class="search_word">Due Date :';

        if (strtotime($task["end_date"]) != '') {

          $end = date("d-m-Y", strtotime($task['end_date']));
        } else {
          $end = '';
        }
        if ($task['related_to'] != 'leads' && is_numeric($task['company_name'])) {
          $value = $this->Common_mdl->getcompany_name($task["company_name"]);
        } else {
          $value = '-';
        }

        $html .= '' . $end . ' </span> </a><p class="m-t-0 search_word">Company Name : ' . $value . ' </p><p class="m-t-0 search_word">Staffs:';
        // $explode_worker=explode(',', $res);


        // }
        $ex_val = $this->Common_mdl->get_module_user_data('TASK', $task['id']);
        $var_array = array();
        if (count($ex_val) > 0) {
          foreach ($ex_val as $key => $value1) {

            $user_name = $this->Common_mdl->select_record('user', 'id', $value1);
            array_push($var_array, $user_name['crm_name']);
          }
        }



        //  $variable=array_filter($username);
        if (!empty($var_array)) {
          $user_name = implode(',', $var_array);
        } else {
          $user_name = "Not Assigned";
        }
        $html .= '' . $user_name . '</p></div> </div></div></div>';
      }
    } else {
      $html .= '<div class="cd-timeline-block> No Data Found </div>';
    }
    echo $html;
  }
  public function addSubTask()
  {
    $ids  = json_decode($_POST['ids'], true);

    foreach ($ids as $id) {
      $data = current($this->Common_mdl->GetAllWithWhere("add_new_task", "id", $id));
      $get_data = $this->db->query('SELECT * FROM firm_assignees where module_name="TASK" and module_id=' . $id . '')->result_array();

      $Taskdata = [
        'subject' => $_POST['name'],
        'start_date' => $data['start_date'],
        'end_date' => $data['end_date'],
        'priority' => $data['priority'],
        'related_to' => 'sub_task',
        'worker' => $data['worker'],
        'company_name' => $data['company_name'],
        'task_status' => '1',
        'create_by' => $_SESSION['id'],
        'created_date' => time(),
        'firm_id' => $_SESSION['firm_id']
      ];
      $insertId = $this->Common_mdl->insert("add_new_task", $Taskdata);

      $log = $data['subject'] . " task was created by";
      $this->Common_mdl->Create_Log('TASK', $insertId, $log, $_SESSION['id']);

      $this->Common_mdl->task_setting($insertId);

      foreach ($get_data as $key => $value) {

        $this->Common_mdl->add_assignto($value['assignees'], $insertId, 'TASK');
        # code...
      }

      $sub_task_id = $insertId;

      if ($data['sub_task_id'] != '') {
        $sub_task_id = explode(',', $data['sub_task_id']);

        array_push($sub_task_id, $insertId);

        $sub_task_id = implode(',', $sub_task_id);
      }

      $this->Common_mdl->update("add_new_task", ['sub_task_id' => $sub_task_id], "id", $id);
    }
  }
  public function updateTask_column_setting()
  {
    $data['id'] = isset($_POST['id']) ? '1' : '0';
    $data['user_id'] = isset($_POST['user_id']) ? '1' : '0';
    $data['timer'] = isset($_POST['timer']) ? '1' : '0';
    $data['subject'] = isset($_POST['subject']) ? '1' : '0';
    $data['start_date'] = isset($_POST['start_date']) ? '1' : '0';
    $data['due_date'] = isset($_POST['due_date']) ? '1' : '0';
    $data['status'] = isset($_POST['status']) ? '1' : '0';
    $data['priority'] = isset($_POST['priority']) ? '1' : '0';
    $data['tag'] = isset($_POST['tag']) ? '1' : '0';
    $data['assignto'] = isset($_POST['assignto']) ? '1' : '0';
    $data['Due'] = isset($_POST['Due']) ? '1' : '0';
    $data['services'] = isset($_POST['services']) ? '1' : '0';
    $data['company_name'] = isset($_POST['company_name']) ? '1' : '0';
    $firm_id  = '0';
    foreach ($data as $k => $val) {
      $this->db->update('task_column_settings', ['visible' => $val], ["firm_id" => $firm_id, 'default_name' => $k]);
    }
    echo 1;
  }
  public function updateTask_columnOrder()
  {
    $i = 1;
    $data = $_POST['datas'];

    foreach ($data as $value) {
      $this->db->update('task_column_settings', ['order_no' => $i], ["firm_id" => 0, 'id' => $value]);


      $i++;
    }
    echo $this->db->affected_rows();
  }
  public function uplode_feilds()
  {


    ini_set('max_execution_time', 300);

    $text = '<?php 
                                                            if(isset($column_setting[0][\'crm_company_name1\'])&&($column_setting[0][\'crm_company_name1\']==\'1\')){?>            
                                                         <th>Name
                                                            <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                             </th>
                                                         <?php } 
                                                          if(isset($column_setting[0][\'crm_allocation_holder\'])&&($column_setting[0][\'crm_allocation_holder\']==\'1\')){?>             
                                                         <th>Allocation Holder <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />    <div class="sortMask"></div>
                                                          <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_company_number\'])&&($column_setting[0][\'crm_company_number\']==\'1\')){?>             
                                                         <th>Company No
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                           <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                           </th>
                                                         <?php } if(isset($column_setting[0][\'crm_company_url\'])&&($column_setting[0][\'crm_company_url\']==\'1\')){?>             
                                                         <th> Url (C)
                                                            <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                                </th>
                                                         <?php } if(isset($column_setting[0][\'crm_officers_url\'])&&($column_setting[0][\'crm_officers_url\']==\'1\')){?>             
                                                         <th> Url (O)
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />    
                                                         <div class="sortMask"></div>
                                                           <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                           </th>
                                                         <?php } if(isset($column_setting[0][\'crm_incorporation_date\'])&&($column_setting[0][\'crm_incorporation_date\']==\'1\')){?>             
                                                         <th>Incorp Dt
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                           </th>
                                                         <?php } if(isset($column_setting[0][\'crm_register_address\'])&&($column_setting[0][\'crm_register_address\']==\'1\')){?>             
                                                         <th>Reg Addr
                                                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                             <div class="sortMask"></div>
                                                              <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_company_type\'])&&($column_setting[0][\'crm_company_type\']==\'1\')){?> 
                                                         <th>Company Type 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                           <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                           </th>
                                                         <?php } if(isset($column_setting[0][\'crm_company_sic\'])&&($column_setting[0][\'crm_company_sic\']==\'1\')){?> 
                                                         <th>SIC  (C)
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                                                             <div class="sortMask"></div>
                                                              <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                          </th>

                                                         <?php } if(isset($column_setting[0][\'crm_company_utr\'])&&($column_setting[0][\'crm_company_utr\']==\'1\')){?> 
                                                         <th> UTR (C)
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_companies_house_authorisation_code\'])&&($column_setting[0][\'crm_companies_house_authorisation_code\']==\'1\')){?> 
                                                         <th>Auth Code 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                        </th>
                                                         <?php } if(isset($column_setting[0][\'crm_managed\'])&&($column_setting[0][\'crm_managed\']==\'1\')){?> 
                                                         <th>Managed 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                                                             <div class="sortMask"></div>
                                                              <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                              </th>
                                                         <?php } if(isset($column_setting[0][\'crm_tradingas\'])&&($column_setting[0][\'crm_tradingas\']==\'1\')){?> 
                                                         <th>Trading As 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                             <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                             </th>
                                                         <?php } if(isset($column_setting[0][\'crm_commenced_trading\'])&&($column_setting[0][\'crm_commenced_trading\']==\'1\')){?> 
                                                         <th>Commenced Trading 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                             <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                             </th>
                                                         <?php } if(isset($column_setting[0][\'crm_registered_for_sa\'])&&($column_setting[0][\'crm_registered_for_sa\']==\'1\')){?> 
                                                         <th>Registered for SA 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                                                             <div class="sortMask"></div>
                                                              <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                              </th>
                                                         <?php } if(isset($column_setting[0][\'crm_business_details_turnover\'])&&($column_setting[0][\'crm_business_details_turnover\']==\'1\')){?> 
                                                         <th>Business Turnover 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_business_details_nature_of_business\'])&&($column_setting[0][\'crm_business_details_nature_of_business\']==\'1\')){?> 
                                                         <th>Nature of business 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_person\'])&&($column_setting[0][\'crm_person\']==\'1\')){?> 
                                                         <th>Person 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                             <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                             </th>
                                                         <?php } if(isset($column_setting[0][\'crm_title\'])&&($column_setting[0][\'crm_title\']==\'1\')){?> 
                                                         <th>Title 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                           <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                           </th>
                                                         <?php } if(isset($column_setting[0][\'crm_first_name\'])&&($column_setting[0][\'crm_first_name\']==\'1\')){?> 
                                                         <th>First Name 
                                                            <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                             </th>
                                                         <?php } if(isset($column_setting[0][\'crm_middle_name\'])&&($column_setting[0][\'crm_middle_name\']==\'1\')){?> 
                                                         <th>Middle Name 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_last_name\'])&&($column_setting[0][\'crm_last_name\']==\'1\')){?> 
                                                         <th>Last Name 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_create_self_assessment_client\'])&&($column_setting[0][\'crm_create_self_assessment_client\']==\'1\')){?> 
                                                         <th>Create Self Assessment Client 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                           <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                           </th>
                                                         <?php } if(isset($column_setting[0][\'crm_client_does_self_assessment\'])&&($column_setting[0][\'crm_client_does_self_assessment\']==\'1\')){?> 
                                                         <th>Client Does Self Assessment 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_preferred_name\'])&&($column_setting[0][\'crm_preferred_name\']==\'1\')){?> 
                                                         <th>Perferred Name 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                           <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                           </th>

                                                         <?php } if(isset($column_setting[0][\'crm_date_of_birth\'])&&($column_setting[0][\'crm_date_of_birth\']==\'1\')){?> 
                                                         <th>DOB 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                           <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>                                                           
                                                         <?php }
                                                      
                                                          if(isset($column_setting[0][\'crm_postal_address\'])&&($column_setting[0][\'crm_postal_address\']==\'1\')){?> 
                                                         <th>Postal Address 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_telephone_number\'])&&($column_setting[0][\'crm_telephone_number\']==\'1\')){?> 
                                                         <th>Phone No
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                          </th>
                                                         <?php }                                                        
                                                         if(isset($column_setting[0][\'crm_ni_number\'])&&($column_setting[0][\'crm_ni_number\']==\'1\')){?>               
                                                         <th>NI Number 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                           <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                           </th>
                                                         <?php } if(isset($column_setting[0][\'crm_personal_utr_number\'])&&($column_setting[0][\'crm_personal_utr_number\']==\'1\')){?>               
                                                         <th> UTR (P)
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_terms_signed\'])&&($column_setting[0][\'crm_terms_signed\']==\'1\')){?>               
                                                         <th>Terms Signed
                                                            <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                             </th>
                                                         <?php } if(isset($column_setting[0][\'crm_photo_id_verified\'])&&($column_setting[0][\'crm_photo_id_verified\']==\'1\')){?>               
                                                         <th>Verified (PHOTO)
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_address_verified\'])&&($column_setting[0][\'crm_address_verified\']==\'1\')){?>               
                                                         <th>Verified (Addr)
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                           <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                           </th>
                                                         <?php } if(isset($column_setting[0][\'crm_previous\'])&&($column_setting[0][\'crm_previous\']==\'1\')){?>               
                                                         <th>Previous 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_current\'])&&($column_setting[0][\'crm_current\']==\'1\')){?>               
                                                         <th>Current 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                                                             <div class="sortMask"></div>
                                                             <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                             </th>
                                                         <?php } if(isset($column_setting[0][\'crm_ir_35_notes\'])&&($column_setting[0][\'crm_ir_35_notes\']==\'1\')){?>               
                                                         <th>Ir35 Notes 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_previous_accountant\'])&&($column_setting[0][\'crm_previous_accountant\']==\'1\')){?>               
                                                         <th>Previous Accountant 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_refered_by\'])&&($column_setting[0][\'crm_refered_by\']==\'1\')){?>               
                                                         <th>Refered By 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_allocated_office\'])&&($column_setting[0][\'crm_allocated_office\']==\'1\')){?>               
                                                         <th>Allocated Office
                                                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_inital_contact\'])&&($column_setting[0][\'crm_inital_contact\']==\'1\')){?>               
                                                         <th>Initial Contact 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                                                             <div class="sortMask"></div>
                                                             <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                             </th>
                                                         <?php } if(isset($column_setting[0][\'crm_quote_email_sent\'])&&($column_setting[0][\'crm_quote_email_sent\']==\'1\')){?>               
                                                         <th>Quote Email Sent 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                           <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                           </th>
                                                         <?php } if(isset($column_setting[0][\'crm_welcome_email_sent\'])&&($column_setting[0][\'crm_welcome_email_sent\']==\'1\')){?>               
                                                         <th>Welcome Email 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_internal_reference\'])&&($column_setting[0][\'crm_internal_reference\']==\'1\')){?>               
                                                         <th>Internal Reference 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_profession\'])&&($column_setting[0][\'crm_profession\']==\'1\')){?>               
                                                         <th>Profession 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                           <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                           </th>
                                                         <?php } if(isset($column_setting[0][\'crm_website\'])&&($column_setting[0][\'crm_website\']==\'1\')){?>               
                                                         <th>Website 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                                                             <div class="sortMask"></div>
                                                             <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                             </th>
                                                         <?php } if(isset($column_setting[0][\'crm_accounting_system\'])&&($column_setting[0][\'crm_accounting_system\']==\'1\')){?>               
                                                         <th>Accounting System 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_notes\'])&&($column_setting[0][\'crm_notes\']==\'1\')){?>               
                                                         <th>Notes
                                                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_other_urgent\'])&&($column_setting[0][\'crm_other_urgent\']==\'1\')){?>               
                                                         <th>OtherUrgent 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_accounts\'])&&($column_setting[0][\'crm_accounts\']==\'1\')){?>               
                                                         <th>Acct\'\'s 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                           <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                           </th>
                                                         <?php } if(isset($column_setting[0][\'crm_bookkeeping\'])&&($column_setting[0][\'crm_bookkeeping\']==\'1\')){?>               
                                                         <th>Bookkeeping 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                          </th>
                                                         <?php } if(isset($column_setting[0][\'crm_ct600_return\'])&&($column_setting[0][\'crm_ct600_return\']==\'1\')){?>               
                                                         <th>CT600 Return 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_payroll\'])&&($column_setting[0][\'crm_payroll\']==\'1\')){?>               
                                                         <th>Payroll 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                                                             <div class="sortMask"></div>
                                                             <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                             </th>
                                                         <?php } if(isset($column_setting[0][\'crm_auto_enrolment\'])&&($column_setting[0][\'crm_auto_enrolment\']==\'1\')){?>               
                                                         <th>Auto-Enrollement 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                           <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_vat_returns\'])&&($column_setting[0][\'crm_vat_returns\']==\'1\')){?>               
                                                         <th>VAT
                                                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />    
                                                          <div class="sortMask"></div>
                                                          <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                          </th>
                                                         <?php } if(isset($column_setting[0][\'crm_confirmation_statement\'])&&($column_setting[0][\'crm_confirmation_statement\']==\'1\')){?>               
                                                         <th>C S 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_cis\'])&&($column_setting[0][\'crm_cis\']==\'1\')){?>               
                                                         <th>CIS 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                           <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                           </th>
                                                         <?php } if(isset($column_setting[0][\'crm_p11d\'])&&($column_setting[0][\'crm_p11d\']==\'1\')){?>               
                                                         <th>P11D 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_invesitgation_insurance\'])&&($column_setting[0][\'crm_invesitgation_insurance\']==\'1\')){?>               
                                                         <th>Invest Insurance 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_services_registered_address\'])&&($column_setting[0][\'crm_services_registered_address\']==\'1\')){?>               
                                                         <th>Regd Addr
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                           <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                           </th>
                                                         <?php } if(isset($column_setting[0][\'crm_bill_payment\'])&&($column_setting[0][\'crm_bill_payment\']==\'1\')){?>               
                                                         <th>Bill Payment 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                           <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                           </th>
                                                         <?php } if(isset($column_setting[0][\'crm_advice\'])&&($column_setting[0][\'crm_advice\']==\'1\')){?>               
                                                         <th>Advice 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_monthly_charge\'])&&($column_setting[0][\'crm_monthly_charge\']==\'1\')){?>               
                                                         <th>Monthly Charge
                                                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                           <div class="sortMask"></div>
                                                           <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                           </th>
                                                         <?php } if(isset($column_setting[0][\'crm_annual_charge\'])&&($column_setting[0][\'crm_annual_charge\']==\'1\')){?>               
                                                         <th>Annual Charge 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_accounts_periodend\'])&&($column_setting[0][\'crm_accounts_periodend\']==\'1\')){?>               
                                                         <th>Acct\'s Periodend 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                          <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                          </th>
                                                         <?php } if(isset($column_setting[0][\'crm_ch_yearend\'])&&($column_setting[0][\'crm_ch_yearend\']==\'1\')){?>               
                                                         <th>CH Year End 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_hmrc_yearend\'])&&($column_setting[0][\'crm_hmrc_yearend\']==\'1\')){?>               
                                                         <th>HMRC Year End
                                                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                             <div class="sortMask"></div>
                                                               <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                               </th>
                                                         <?php } if(isset($column_setting[0][\'crm_ch_accounts_next_due\'])&&($column_setting[0][\'crm_ch_accounts_next_due\']==\'1\')){?>               
                                                         <th>CH Acct\'s Next Due 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                              <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                              </th>
                                                         <?php } if(isset($column_setting[0][\'crm_ct600_due\'])&&($column_setting[0][\'crm_ct600_due\']==\'1\')){?>               
                                                         <th>CT600 Due 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                             <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_companies_house_email_remainder\'])&&($column_setting[0][\'crm_companies_house_email_remainder\']==\'1\')){?>               
                                                         <th>CH Email Reminder
                                                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                            <div class="sortMask"></div>
                                                              <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_latest_action\'])&&($column_setting[0][\'crm_latest_action\']==\'1\')){?>               
                                                         <th>Latest Action 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                             <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_latest_action_date\'])&&($column_setting[0][\'crm_latest_action_date\']==\'1\')){?>               
                                                         <th>Latest Action Date 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                                                             <div class="sortMask"></div>
                                                               <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_records_received_date\'])&&($column_setting[0][\'crm_records_received_date\']==\'1\')){?>               
                                                         <th>Records Received 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                              <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_confirmation_statement_date\'])&&($column_setting[0][\'crm_confirmation_statement_date\']==\'1\')){?>               
                                                         <th>CS Dt 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                                                             <div class="sortMask"></div>
                                                               <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_confirmation_statement_due_date\'])&&($column_setting[0][\'crm_confirmation_statement_due_date\']==\'1\')){?>               
                                                         <th>CS Due 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                                                             <div class="sortMask"></div>
                                                               <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_confirmation_latest_action\'])&&($column_setting[0][\'crm_confirmation_latest_action\']==\'1\')){?>               
                                                         <th>CS Latest Action 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                                                             <div class="sortMask"></div>
                                                               <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_confirmation_latest_action_date\'])&&($column_setting[0][\'crm_confirmation_latest_action_date\']==\'1\')){?>               
                                                         <th>CS Latest Action Date 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_confirmation_records_received_date\'])&&($column_setting[0][\'crm_confirmation_records_received_date\']==\'1\')){?>               
                                                         <th>CS Records Received 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                              <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_confirmation_officers\'])&&($column_setting[0][\'crm_confirmation_officers\']==\'1\')){?>               
                                                         <th>CS Officers 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                             <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_share_capital\'])&&($column_setting[0][\'crm_share_capital\']==\'1\')){?>               
                                                         <th>CS Share Capital 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                                                             <div class="sortMask"></div>
                                                               <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_shareholders\'])&&($column_setting[0][\'crm_shareholders\']==\'1\')){?>               
                                                         <th>CS Shareholders 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                              <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_people_with_significant_control\'])&&($column_setting[0][\'crm_people_with_significant_control\']==\'1\')){?>               
                                                         <th>CS People with Significant Control 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                             <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_vat_quarters\'])&&($column_setting[0][\'crm_vat_quarters\']==\'1\')){?>               
                                                         <th>VAT Quarters 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                             <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_vat_quater_end_date\'])&&($column_setting[0][\'crm_vat_quater_end_date\']==\'1\')){?>               
                                                         <th>VAT Quarter End 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                                                             <div class="sortMask"></div>
                                                               <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_next_return_due_date\'])&&($column_setting[0][\'crm_next_return_due_date\']==\'1\')){?>               
                                                         <th>VAT Next Return Dt 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                             <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_vat_latest_action\'])&&($column_setting[0][\'crm_vat_latest_action\']==\'1\')){?>               
                                                         <th>VAT Latest Action 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_vat_latest_action_date\'])&&($column_setting[0][\'crm_vat_latest_action_date\']==\'1\')){?>               
                                                         <th>VAT Latest Action Date 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                             <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_vat_records_received_date\'])&&($column_setting[0][\'crm_vat_records_received_date\']==\'1\')){?>               
                                                         <th>VAT Records Received 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                             <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                             </th>
                                                         <?php } if(isset($column_setting[0][\'crm_vat_member_state\'])&&($column_setting[0][\'crm_vat_member_state\']==\'1\')){?>               
                                                         <th>VAT Member State 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                             <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                             </th>
                                                         <?php } if(isset($column_setting[0][\'crm_vat_number\'])&&($column_setting[0][\'crm_vat_number\']==\'1\')){?>               
                                                         <th>VAT No
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                             <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_vat_date_of_registration\'])&&($column_setting[0][\'crm_vat_date_of_registration\']==\'1\')){?>               
                                                         <th>VAT Dt of Registration 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                         <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_vat_effective_date\'])&&($column_setting[0][\'crm_vat_effective_date\']==\'1\')){?>               
                                                         <th>VAT Effective Date <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />    <div class="sortMask"></div></th>
                                                         <?php } if(isset($column_setting[0][\'crm_vat_estimated_turnover\'])&&($column_setting[0][\'crm_vat_estimated_turnover\']==\'1\')){?>               
                                                         <th>Estimated Turnover 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                           <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_transfer_of_going_concern\'])&&($column_setting[0][\'crm_transfer_of_going_concern\']==\'1\')){?>               
                                                         <th>Transfer of Going Concern 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                                                             <div class="sortMask"></div>
                                                             <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_involved_in_any_other_business\'])&&($column_setting[0][\'crm_involved_in_any_other_business\']==\'1\')){?>               
                                                         <th>Involved in Any Other Business
                                                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                             <div class="sortMask"></div>
                                                             <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_flat_rate\'])&&($column_setting[0][\'crm_flat_rate\']==\'1\')){?>               
                                                         <th>Flat Rate 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_direct_debit\'])&&($column_setting[0][\'crm_direct_debit\']==\'1\')){?>               
                                                         <th>Direct Debit
                                                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_annual_accounting_scheme\'])&&($column_setting[0][\'crm_annual_accounting_scheme\']==\'1\')){?>               
                                                         <th>Annual Accounting Scheme 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_flat_rate_category\'])&&($column_setting[0][\'crm_flat_rate_category\']==\'1\')){?>               
                                                         <th>Flat Rate Category 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                          <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_month_of_last_quarter_submitted\'])&&($column_setting[0][\'crm_month_of_last_quarter_submitted\']==\'1\')){?>               
                                                         <th>Month of Last Quarter Submitted 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_box5_of_last_quarter_submitted\'])&&($column_setting[0][\'crm_box5_of_last_quarter_submitted\']==\'1\')){?>               
                                                         <th>Box 5 of Last Quarter Submitted 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_vat_address\'])&&($column_setting[0][\'crm_vat_address\']==\'1\')){?>               
                                                         <th>VAT Addr
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                           <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_vat_notes\'])&&($column_setting[0][\'crm_vat_notes\']==\'1\')){?>               
                                                         <th>VAT Notes 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                           <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_employers_reference\'])&&($column_setting[0][\'crm_employers_reference\']==1)){?>               
                                                         <th>Employers Reference 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                           <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_accounts_office_reference\'])&&($column_setting[0][\'crm_accounts_office_reference\']==\'1\')){?>               
                                                         <th>Accounts Office Reference 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                           <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_years_required\'])&&($column_setting[0][\'crm_years_required\']==\'1\')){?>               
                                                         <th>Years Required 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                           <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_annual_or_monthly_submissions\'])&&($column_setting[0][\'crm_annual_or_monthly_submissions\']==\'1\')){?>               
                                                         <th>Annual/Monthly Submissions 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_irregular_montly_pay\'])&&($column_setting[0][\'crm_irregular_montly_pay\']==\'1\')){?>               
                                                         <th>Irregular Monthly Pay 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_nil_eps\'])&&($column_setting[0][\'crm_nil_eps\']==\'1\')){?>               
                                                         <th>Nil EPS 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                          <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_no_of_employees\'])&&($column_setting[0][\'crm_no_of_employees\']==\'1\')){?>               
                                                         <th>No of Employees 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                           <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_first_pay_date\'])&&($column_setting[0][\'crm_first_pay_date\']==\'1\')){?>               
                                                         <th>First Pay Date 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                          <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_rti_deadline\'])&&($column_setting[0][\'crm_rti_deadline\']==\'1\')){?>               
                                                         <th>RTI Deadline 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_paye_scheme_ceased\'])&&($column_setting[0][\'crm_paye_scheme_ceased\']==\'1\')){?>               
                                                         <th>PAYE Scheme Ceased
                                                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                                                              <div class="sortMask"></div>
                                                              <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_paye_latest_action\'])&&($column_setting[0][\'crm_paye_latest_action\']==\'1\')){?>               
                                                         <th>PAYE Latest Action 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                           <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_paye_latest_action_date\'])&&($column_setting[0][\'crm_paye_latest_action_date\']==\'1\')){?>               
                                                         <th>PAYE Latest Action Date 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_paye_records_received\'])&&($column_setting[0][\'crm_paye_records_received\']==\'1\')){?>               
                                                         <th>PAYE Records Received 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                                                             <div class="sortMask"></div>
                                                             <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_next_p11d_return_due\'])&&($column_setting[0][\'crm_next_p11d_return_due\']==\'1\')){?>               
                                                         <th>Next P11D Return Due 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                           <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_latest_p11d_submitted\'])&&($column_setting[0][\'crm_latest_p11d_submitted\']==\'1\')){?>               
                                                         <th>Latest P11D Submitted 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_p11d_latest_action\'])&&($column_setting[0][\'crm_p11d_latest_action\']==\'1\')){?>               
                                                         <th>P11D Latest Action 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                           <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_p11d_latest_action_date\'])&&($column_setting[0][\'crm_p11d_latest_action_date\']==\'1\')){?>               
                                                         <th>P11D Latest Action Date
                                                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_p11d_records_received\'])&&($column_setting[0][\'crm_p11d_records_received\']==\'1\')){?>               
                                                         <th>P11D Records Received
                                                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                           <div class="sortMask"></div>
                                                           </th>
                                                         <?php } if(isset($column_setting[0][\'crm_cis_contractor\'])&&($column_setting[0][\'crm_cis_contractor\']==\'1\')){?>               
                                                         <th>CIS Contractor 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_cis_subcontractor\'])&&($column_setting[0][\'crm_cis_subcontractor\']==\'1\')){?>               
                                                         <th>CIS Subcontractor 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_cis_deadline\'])&&($column_setting[0][\'crm_cis_deadline\']==\'1\')){?>               
                                                         <th>CIS Deadline 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_auto_enrolment_latest_action\'])&&($column_setting[0][\'crm_auto_enrolment_latest_action\']==\'1\')){?>               
                                                         <th>Auto-Enrollement Latest Action 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                           <div class="sortMask"></div>
                                                           <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                           </th>
                                                         <?php } if(isset($column_setting[0][\'crm_auto_enrolment_latest_action_date\'])&&($column_setting[0][\'crm_auto_enrolment_latest_action_date\']==\'1\')){?>               
                                                         <th>Auto-Enrollement Latest Action Date 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                          <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_auto_enrolment_records_received\'])&&($column_setting[0][\'crm_auto_enrolment_records_received\']==\'1\')){?>               
                                                         <th>Auto-Enrollement Record Received 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                          <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                          </th>
                                                         <?php } if(isset($column_setting[0][\'crm_auto_enrolment_staging\'])&&($column_setting[0][\'crm_auto_enrolment_staging\']==\'1\')){?>               
                                                         <th>Auto-Enrollement Staging
                                                           <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                          <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                          </th>
                                                         <?php } if(isset($column_setting[0][\'crm_postponement_date\'])&&($column_setting[0][\'crm_postponement_date\']==\'1\')){?>               
                                                         <th>Postponement Date <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />    <div class="sortMask"></div>
                                                           <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_the_pensions_regulator_opt_out_date\'])&&($column_setting[0][\'crm_the_pensions_regulator_opt_out_date\']==\'1\')){?>               
                                                         <th>The Pensions Regulator Opt Out Date 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_re_enrolment_date\'])&&($column_setting[0][\'crm_re_enrolment_date\']==\'1\')){?>               
                                                         <th>Re-Enrolment Date 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_pension_provider\'])&&($column_setting[0][\'crm_pension_provider\']==\'1\')){?>               
                                                         <th>Pension Provider 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                              <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_pension_id\'])&&($column_setting[0][\'crm_pension_id\']==\'1\')){?>               
                                                         <th>Pension ID 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                              <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_declaration_of_compliance_due_date\'])&&($column_setting[0][\'crm_declaration_of_compliance_due_date\']==\'1\')){?>               
                                                         <th>Declaration of Compliance Due 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_declaration_of_compliance_submission\'])&&($column_setting[0][\'crm_declaration_of_compliance_submission\']==\'1\')){?>               
                                                         <th>Declaration of Compliance Submission
                                                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                            <div class="sortMask"></div>
                                                              <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_pension_deadline\'])&&($column_setting[0][\'crm_pension_deadline\']==\'1\')){?>               
                                                         <th>Pension Deadline 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                                                             <div class="sortMask"></div>
                                                               <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_note\'])&&($column_setting[0][\'crm_note\']==\'1\')){?>               
                                                         <th>PAYE Notes 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                             <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_registration_fee_paid\'])&&($column_setting[0][\'crm_registration_fee_paid\']==\'1\')){?>               
                                                         <th>Registration Fee Paid 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />
                                                            <div class="sortMask"></div>
                                                              <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_64_8_registration\'])&&($column_setting[0][\'crm_64_8_registration\']==\'1\')){?>               
                                                         <th>64-8 Registration
                                                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                            <div class="sortMask"></div>
                                                              <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_packages\'])&&($column_setting[0][\'crm_packages\']==\'1\')){?>               
                                                         <th>Packages 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                              <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                              </th>
                                                         <?php } if(isset($column_setting[0][\'crm_gender\'])&&($column_setting[0][\'crm_gender\']==\'1\')){?>               
                                                         <th>Gender 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                             <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_letter_sign\'])&&($column_setting[0][\'crm_letter_sign\']==\'1\')){?> 
                                                         <th>Letter sign 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                             <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                             </th>
                                                         <?php } if(isset($column_setting[0][\'crm_business_website\'])&&($column_setting[0][\'crm_business_website\']==\'1\')){?>
                                                         <th>Business website 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                             <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                             </th>
                                                         <?php } if(isset($column_setting[0][\'crm_paye_ref_number\'])&&($column_setting[0][\'crm_paye_ref_number\']==\'1\')){?>
                                                         <th>Paye ref no 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                              <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                              </th>
                                                         <?php } if(isset($column_setting[0][\'select_responsible_type\'])&&($column_setting[0][\'select_responsible_type\']==\'1\')){?>
                                                         <th>Responsible type 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                              <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                              </th>
                                                         <?php } if(isset($column_setting[0][\'crm_assign_manager_reviewer\'])&&($column_setting[0][\'crm_assign_manager_reviewer\']==\'1\')){?>
                                                         <th>Manager reviewer 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                             <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                             </th>
                                                         <?php } if(isset($column_setting[0][\'crm_assign_managed\'])&&($column_setting[0][\'crm_assign_managed\']==\'1\')){?>
                                                         <th>Assign managed
                                                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                            <div class="sortMask"></div>
                                                              <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                              </th>
                                                         <?php } if(isset($column_setting[0][\'crm_assign_client_id_verified\'])&&($column_setting[0][\'crm_assign_client_id_verified\']==\'1\')){?>
                                                         <th>Client id verfied 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_assign_type_of_id\'])&&($column_setting[0][\'crm_assign_type_of_id\']==\'1\')){?>
                                                         <th>Assign type of id 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                              <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_assign_proof_of_address\'])&&($column_setting[0][\'crm_assign_proof_of_address\']==\'1\')){?>
                                                         <th>Assign proof address 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                             <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_assign_meeting_client\'])&&($column_setting[0][\'crm_assign_meeting_client\']==\'1\')){?>
                                                         <th>Assign meeting client 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_assign_source\'])&&($column_setting[0][\'crm_assign_source\']==\'1\')){?>
                                                         <th>Assign Source 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                              <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                              </th>
                                                         <?php } if(isset($column_setting[0][\'crm_assign_relationship_client\'])&&($column_setting[0][\'crm_assign_relationship_client\']==\'1\')){?>
                                                         <th>Assign relation client 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                              <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_assign_notes\'])&&($column_setting[0][\'crm_assign_notes\']==\'1\')){?>
                                                         <th>Assign notes 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                             <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                             </th>
                                                         <?php } if(isset($column_setting[0][\'crm_other_name_of_firm\'])&&($column_setting[0][\'crm_other_name_of_firm\']==\'1\')){?>
                                                         <th>Other name firm 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_other_address\'])&&($column_setting[0][\'crm_other_address\']==\'1\')){?>
                                                         <th>Other addr 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                             <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                             </th>
                                                         <?php } if(isset($column_setting[0][\'crm_other_contact_no\'])&&($column_setting[0][\'crm_other_contact_no\']==\'1\')){?>
                                                         <th>Other contact no 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                             <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_other_email\'])&&($column_setting[0][\'crm_other_email\']==\'1\')){?>
                                                         <th>Other email <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />    <div class="sortMask"></div></th>
                                                         <?php } if(isset($column_setting[0][\'crm_other_chase_for_info\'])&&($column_setting[0][\'crm_other_chase_for_info\']==\'1\')){?>
                                                         <th>other info 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                              <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_other_notes\'])&&($column_setting[0][\'crm_other_notes\']==\'1\')){?>
                                                         <th>other notes 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                              <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_other_internal_notes\'])&&($column_setting[0][\'crm_other_internal_notes\']==\'1\')){?>
                                                         <th>other internal notes 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                           <div class="sortMask"></div>
                                                             <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_other_invite_use\'])&&($column_setting[0][\'crm_other_invite_use\']==\'1\')){?>
                                                         <th>other invite use
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                            <div class="sortMask"></div>
                                                              <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_other_crm\'])&&($column_setting[0][\'crm_other_crm\']==\'1\')){?>
                                                         <th>other crm 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                                           <div class="sortMask"></div></th>
                                                         <?php } if(isset($column_setting[0][\'crm_other_proposal\'])&&($column_setting[0][\'crm_other_proposal\']==\'1\')){?>
                                                         <th>other proposal <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />    <div class="sortMask"></div>
                                                           <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_other_task\'])&&($column_setting[0][\'crm_other_task\']==\'1\')){?>
                                                         <th>other task 
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_other_send_invit_link\'])&&($column_setting[0][\'crm_other_send_invit_link\']==\'1\')){?>
                                                         <th>other invitlink
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_other_username\'])&&($column_setting[0][\'crm_other_username\']==\'1\')){?>
                                                         <th>other username
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_other_any_notes\'])&&($column_setting[0][\'crm_other_any_notes\']==\'1\')){?>
                                                         <th>other any notes
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_confirmation_auth_code\'])&&($column_setting[0][\'crm_confirmation_auth_code\']==\'1\')){?>
                                                         <th>Confimation auth code
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_accounts_auth_code\'])&&($column_setting[0][\'crm_accounts_auth_code\']==\'1\')){?>
                                                         <th>Acct\'s auth code
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_accounts_utr_number\'])&&($column_setting[0][\'crm_accounts_utr_number\']==\'1\')){?>
                                                         <th>Acct\'s utr
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_accounts_due_date_hmrc\'])&&($column_setting[0][\'crm_accounts_due_date_hmrc\']==\'1\')){?>
                                                         <th>Acct\'s due dt
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_accounts_tax_date_hmrc\'])&&($column_setting[0][\'crm_accounts_tax_date_hmrc\']==\'1\')){?>
                                                         <th>Acct\'s tax dt
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_accounts_notes\'])&&($column_setting[0][\'crm_accounts_notes\']==\'1\')){?>
                                                         <th>Acct\'s notes
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_property_income\'])&&($column_setting[0][\'crm_property_income\']==\'1\')){?>
                                                         <th>Property income
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_additional_income\'])&&($column_setting[0][\'crm_additional_income\']==\'1\')){?>
                                                         <th>Additional income
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_personal_tax_return_date\'])&&($column_setting[0][\'crm_personal_tax_return_date\']==\'1\')){?>
                                                         <th>Personal tax return dt
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_personal_due_date_return\'])&&($column_setting[0][\'crm_personal_due_date_return\']==\'1\')){?>
                                                         <th>Personal due dt return
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_personal_due_date_online\'])&&($column_setting[0][\'crm_personal_due_date_online\']==\'1\')){?>
                                                         <th>Personal due online
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_personal_notes\'])&&($column_setting[0][\'crm_personal_notes\']==\'1\')){?>
                                                         <th>Personal notes
                                                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_payroll_acco_off_ref_no\'])&&($column_setting[0][\'crm_payroll_acco_off_ref_no\']==\'1\')){?>
                                                         <th>payroll account ref no
                                                              <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                         </th>
                                                         <?php } if(isset($column_setting[0][\'crm_paye_off_ref_no\'])&&($column_setting[0][\'crm_paye_off_ref_no\']==\'1\')){?>
                                                         <th>paye ref no
                                                              <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_payroll_reg_date\'])&&($column_setting[0][\'crm_payroll_reg_date\']==\'1\')){?>
                                                         <th>payroll reg dt
                                                              <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_payroll_run\'])&&($column_setting[0][\'crm_payroll_run\']==\'1\')){?>
                                                         <th>payroll run 
                                                              <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_payroll_run_date\'])&&($column_setting[0][\'crm_payroll_run_date\']==\'1\')){?>
                                                         <th>payroll run dt   
                                                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_previous_year_require\'])&&($column_setting[0][\'crm_previous_year_require\']==\'1\')){?>
                                                         <th>previous year require 
                                                              <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_payroll_if_yes\'])&&($column_setting[0][\'crm_payroll_if_yes\']==\'1\')){?>
                                                         <th>payroll if yes
                                                              <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_staging_date\'])&&($column_setting[0][\'crm_staging_date\']==\'1\')){?>
                                                         <th>Staging dt
                                                              <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_pension_subm_due_date\'])&&($column_setting[0][\'crm_pension_subm_due_date\']==\'1\')){?>
                                                         <th>Pension sub due dt    <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                            </th>
                                                         <?php } if(isset($column_setting[0][\'crm_employer_contri_percentage\'])&&($column_setting[0][\'crm_employer_contri_percentage\']==\'1\')){?>
                                                         <th>Employer percentage      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_employee_contri_percentage\'])&&($column_setting[0][\'crm_employee_contri_percentage\']==\'1\')){?>
                                                         <th>Employee percentage      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_pension_notes\'])&&($column_setting[0][\'crm_pension_notes\']==\'1\')){?>
                                                         <th>Pension notes      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_cis_contractor_start_date\'])&&($column_setting[0][\'crm_cis_contractor_start_date\']==\'1\')){?>
                                                         <th>Cis start dt      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_cis_scheme_notes\'])&&($column_setting[0][\'crm_cis_scheme_notes\']==\'1\')){?>
                                                         <th>CIS scheme notes      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_cis_subcontractor_start_date\'])&&($column_setting[0][\'crm_cis_subcontractor_start_date\']==\'1\')){?>
                                                         <th>CIS sub start dt     <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_cis_subcontractor_scheme_notes\'])&&($column_setting[0][\'crm_cis_subcontractor_scheme_notes\']==\'1\')){?>
                                                         <th>CIS sub scheme notes      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_p11d_previous_year_require\'])&&($column_setting[0][\'crm_p11d_previous_year_require\']==\'1\')){?>
                                                         <th>p11d previous year require      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_p11d_payroll_if_yes\'])&&($column_setting[0][\'crm_p11d_payroll_if_yes\']==\'1\')){?>
                                                         <th>P11d payroll if yes      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_vat_frequency\'])&&($column_setting[0][\'crm_vat_frequency\']==\'1\')){?>
                                                         <th>Vat Frequency      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_vat_due_date\'])&&($column_setting[0][\'crm_vat_due_date\']==\'1\')){?>
                                                         <th>Vat due dt     <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_vat_scheme\'])&&($column_setting[0][\'crm_vat_scheme\']==\'1\')){?>
                                                          <th>Vat scheme      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_flat_rate_percentage\'])&&($column_setting[0][\'crm_flat_rate_percentage\']==\'1\')){?>
                                                         <th>Flat rate percentage      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_next_booking_date\'])&&($column_setting[0][\'crm_next_booking_date\']==\'1\')){?>
                                                         <th>Next booking dt      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_method_bookkeeping\'])&&($column_setting[0][\'crm_method_bookkeeping\']==\'1\')){?>
                                                         <th>Method bookkeeping      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_client_provide_record\'])&&($column_setting[0][\'crm_client_provide_record\']==\'1\')){?>
                                                         <th>client provide record      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_manage_acc_fre\'])&&($column_setting[0][\'crm_manage_acc_fre\']==\'1\')){?>
                                                         <th>Manage accounts frequency      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_next_manage_acc_date\'])&&($column_setting[0][\'crm_next_manage_acc_date\']==\'1\')){?>
                                                         <th>Next manage account dt     <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_manage_method_bookkeeping\'])&&($column_setting[0][\'crm_manage_method_bookkeeping\']==\'1\')){?>
                                                         <th>Method of bookkeeping      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_manage_client_provide_record\'])&&($column_setting[0][\'crm_manage_client_provide_record\']==\'1\')){?>
                                                         <th>client provide record      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_manage_notes\'])&&($column_setting[0][\'crm_manage_notes\']==\'1\')){?>
                                                         <th>Manage notes      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_insurance_renew_date\'])&&($column_setting[0][\'crm_insurance_renew_date\']==\'1\')){?>
                                                         <th>Insurance Renew Dt      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_insurance_provider\'])&&($column_setting[0][\'crm_insurance_provider\']==\'1\')){?>
                                                         <th>Insurance provider      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_claims_note\'])&&($column_setting[0][\'crm_claims_note\']==\'1\')){?>
                                                         <th>Claims notes      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_registered_start_date\'])&&($column_setting[0][\'crm_registered_start_date\']==\'1\')){?>
                                                         <th>Registered Start dt      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_registered_renew_date\'])&&($column_setting[0][\'crm_registered_renew_date\']==\'1\')){?>
                                                         <th>Registered renew dt      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_registered_office_inuse\'])&&($column_setting[0][\'crm_registered_office_inuse\']==\'1\')){?>
                                                         <th>Registered office inuse      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_registered_claims_note\'])&&($column_setting[0][\'crm_registered_claims_note\']==\'1\')){?>
                                                         <th>Registered claims note      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_investigation_start_date\'])&&($column_setting[0][\'crm_investigation_start_date\']==\'1\')){?>
                                                         <th>Investigation start dt     <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_investigation_end_date\'])&&($column_setting[0][\'crm_investigation_end_date\']==\'1\')){?>
                                                         <th>Investigation end date      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_investigation_note\'])&&($column_setting[0][\'crm_investigation_note\']==\'1\')){?>
                                                         <th>Investigation notes      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_tax_investigation_note\'])&&($column_setting[0][\'crm_tax_investigation_note\']==\'1\')){?>
                                                         <th>tax Investigation notes      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_assign_other_custom\'])&&($column_setting[0][\'crm_assign_other_custom\']==\'1\')){?>
                                                         <th>Assign other custom      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_notes_info\'])&&($column_setting[0][\'crm_notes_info\']==\'1\')){?>
                                                         <th>Notes info      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_registered_in\'])&&($column_setting[0][\'crm_registered_in\']==\'1\')){?>
                                                         <th>Registered In      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_address_line_one\'])&&($column_setting[0][\'crm_address_line_one\']==\'1\')){?>
                                                         <th>Address line1      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_address_line_two\'])&&($column_setting[0][\'crm_address_line_two\']==\'1\')){?>
                                                         <th>Address line2      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_address_line_three\'])&&($column_setting[0][\'crm_address_line_three\']==\'1\')){?>
                                                         <th>Address line3      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_town_city\'])&&($column_setting[0][\'crm_town_city\']==\'1\')){?>
                                                         <th>Town/city  <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'crm_post_code\'])&&($column_setting[0][\'crm_post_code\']==\'1\')){?>
                                                         <th>Post code   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } ?> 
                                                         <?php if(isset($column_setting[0][\'contact_title\'])&&($column_setting[0][\'contact_title\']==\'1\')){?>
                                                         <th>Title   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'contact_first_name\'])&&($column_setting[0][\'contact_first_name\']==\'1\')){?>
                                                         <th>First name   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'contact_middle_name\'])&&($column_setting[0][\'contact_middle_name\']==\'1\')){?>
                                                         <th>Middle name   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'contact_surname\'])&&($column_setting[0][\'contact_surname\']==\'1\')){?>
                                                         <th>Surname   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'contact_preferred_name\'])&&($column_setting[0][\'contact_preferred_name\']==\'1\')){?>
                                                         <th>Prefered name   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'contact_mobile\'])&&($column_setting[0][\'contact_mobile\']==\'1\')){?>
                                                         <th>Contact Mobile   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'contact_main_email\'])&&($column_setting[0][\'contact_main_email\']==\'1\')){?>
                                                         <th>Main Email   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'contact_nationality\'])&&($column_setting[0][\'contact_nationality\']==\'1\')){?>
                                                         <th>Nationality   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'contact_psc\'])&&($column_setting[0][\'contact_psc\']==\'1\')){?>
                                                         <th>Psc   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'contact_shareholder\'])&&($column_setting[0][\'contact_shareholder\']==\'1\')){?>
                                                         <th>Shareholder   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'contact_ni_number\'])&&($column_setting[0][\'contact_ni_number\']==\'1\')){?>
                                                         <th>Contact Ni Number   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'contact_country_of_residence\'])&&($column_setting[0][\'contact_country_of_residence\']==\'1\')){?>
                                                         <th>Country of residence   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'contact_type\'])&&($column_setting[0][\'contact_type\']==\'1\')){?>
                                                         <th>Contact type   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'contact_other_custom\'])&&($column_setting[0][\'contact_other_custom\']==\'1\')){?>
                                                         <th>Conact Other custom   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'contact_address_line1\'])&&($column_setting[0][\'contact_address_line1\']==\'1\')){?>
                                                         <th>Address line1   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'contact_address_line2\'])&&($column_setting[0][\'contact_address_line2\']==\'1\')){?>
                                                         <th>Address line2   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'contact_town_city\'])&&($column_setting[0][\'contact_town_city\']==\'1\')){?>
                                                         <th>Town/city   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'contact_post_code\'])&&($column_setting[0][\'contact_post_code\']==\'1\')){?>
                                                         <th>Post code   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'contact_landline\'])&&($column_setting[0][\'contact_landline\']==\'1\')){?>
                                                         <th>Landline   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'contact_work_email\'])&&($column_setting[0][\'contact_work_email\']==\'1\')){?>
                                                         <th>Work mail   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'contact_date_of_birth\'])&&($column_setting[0][\'contact_date_of_birth\']==\'1\')){?>
                                                         <th>Date of birth   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'contact_nature_of_control\'])&&($column_setting[0][\'contact_nature_of_control\']==\'1\')){?>
                                                         <th>Nature of control   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'contact_marital_status\'])&&($column_setting[0][\'contact_marital_status\']==\'1\')){?>
                                                         <th>Marital status   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'contact_utr_number\'])&&($column_setting[0][\'contact_utr_number\']==\'1\')){?>
                                                         <th>Contact Utr no   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'contact_occupation\'])&&($column_setting[0][\'contact_occupation\']==\'1\')){?>
                                                         <th>Occupation   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'contact_appointed_on\'])&&($column_setting[0][\'contact_appointed_on\']==\'1\')){?>
                                                         <th>Appointed on   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } ?>
                                                         <?php if(isset($column_setting[0][\'staff_manager\'])&&($column_setting[0][\'staff_manager\']==\'1\')){?>
                                                         <th>staff   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'staff_managed\'])&&($column_setting[0][\'staff_managed\']==\'1\')){?>
                                                         <th>Managed by   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'team\'])&&($column_setting[0][\'team\']==\'1\')){?>
                                                         <th>Team   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'team_allocation\'])&&($column_setting[0][\'team_allocation\']==\'1\')){?>
                                                         <th>Team allocation office   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'department\'])&&($column_setting[0][\'department\']==\'1\')){?>
                                                         <th>Department   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'department_allocation\'])&&($column_setting[0][\'department_allocation\']==\'1\')){?>
                                                         <th>Department allocation office   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'member_manager\'])&&($column_setting[0][\'member_manager\']==\'1\')){?>
                                                         <th>Member Manager   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <?php } if(isset($column_setting[0][\'member_managed\'])&&($column_setting[0][\'member_managed\']==\'1\')){?>
                                                         <th>Managed by   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                          <div class="sortMask"></div>
                                                            <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                                                         ';

    $arr = [['Client', 'Client'], ['Type', 'Type'], ['contact_main_email', 'Email'], ['contact_mobile', 'Phone'], ['Active', 'Active']];

    $i = 1;
    foreach ($arr as $key => $value) {
      $this->db->query("insert into firm_column_settings values ('','client_list','" . trim($value[0]) . "',1," . $i . ",'" . trim($value[1]) . "')");

      $i++;
    }


    $array = explode('</th>', $text);




    foreach ($array as $key => $str) {

      $len1 = strpos($str, '$column_setting[0][\'');

      $len1 += 20;

      $len2 = strpos($str, '\']', $len1);

      $index =  substr($str, $len1, $len2 - $len1);

      $len1 = strpos($str, '<th>');

      $len1 += 4;

      $len2 = strpos($str, '<img', $len1);

      $val = substr($str, $len1, $len2 - $len1);

      $this->db->query("insert into firm_column_settings values ('','client_list','" . trim($index) . "',1," . $i . ",'" . addslashes(trim($val)) . "')");

      $i++;
    }

    echo "<pre>";

    print_r($array);
    die;
  }
public function get_task_ajax_data()
{
  $filter_by                = isset($_REQUEST['filter_by']) ? $_REQUEST['filter_by'] : '';
  $search_flag              = 0;
  $res2                     = ($filter_by == '') ? Get_Assigned_Datas('TASK') : get_user_filtered_tasks('TASK', $filter_by);
  $res                      = (!empty($res2)) ? implode(',', array_filter(array_unique($res2))) : '-1';
  $data['ass_res']          = $res;
  $condition                = " AND tl.related_to != 'sub_task' AND  tl.task_status in (1,2,3) ";
  $data['status_condition'] = 0;
  $menu_status_val          = $this->input->post('menu_status_val');

  if ($menu_status_val) {
    $condition = " AND tl.task_status = " . $menu_status_val . " ";
  }

  if ($_SESSION["firm_id"] == 258) {
    $this->db->query('Update firm_column_settings SET visible_firm258=1 WHERE id=1 OR id=2');
  }

  /* Get Colorder from table */
  $key_val    = $this->Task_creating_model->Firm_column_settings('task_list');
  $sort_order = json_decode($key_val['sort_order'], true);
  // echo '<pre>';
  // print_r($sort_order);
  // echo $sort_order[$_POST['order'][0]['column']];
  // exit;
  $key_val    = json_decode($key_val['order'], true);
  $key_val    = array_map(function ($a) {
    return ($a - 1);
  }, $key_val);

  // Server Side Datatable functions //
  $totalRecord           = 0;
  $totalRecordwithFilter = 0;
  $count                 = 'count(tl.id) as allcount';
  $select                = "SELECT ";
  $select_val            = "tl.*,DATE(tl.start_date) AS order_start_date,cl.crm_company_name,cl.user_id as clientuid,cc.telephone_number,cc.mobile,cc.landline,cc.main_email,cc.id as contact_id, cl.id as client_id";
  $select_val           .= ", ( SELECT count(id) 
                                FROM task_comments 
                                WHERE task_id = tl.id
                                AND (viewed IS NULL OR viewed NOT IN('" . $_SESSION['id'] . "'))            
                            ) as msg_count";
  $from                  = " FROM add_new_task AS tl ";
  $join                  = " LEFT JOIN client AS cl ON cl.id=tl.company_name LEFT JOIN client_contacts AS cc ON cc.client_id=tl.user_id where tl.id in (" . $res . ") and tl.firm_id = " . $_SESSION['firm_id'] . " and cc.make_primary = 1 " . $condition;
  $q1                    = $select . $count . $from . $join; //Total row count
  $q1_with_join          = $select . $count . $from . $join;
  $q2                    = $select . $select_val . $from . $join;
  $draw                  = $_POST['draw'];
  $row_limit             = $_POST['start'];

  if (isset($_POST['colwise_filter_val']) && ($_POST['colwise_filter_val'] != '')) {
    $rowperpage = -1;
  } else {
    $rowperpage = $_POST['length']; // Rows display per page
  }
  $columnIndex = $_POST['order'][0]['column']; // Column index
  $columnName  = $_POST['columns'][$columnIndex]['data']; // Column name

  if ($rowperpage == -1) {
    $columnSortOrder  = $_POST['order'][0]['dir'];
  } else {
    $columnSortOrder = $_POST['order'][0]['dir'] . ' LIMIT ' . $rowperpage; // asc or desc
  }
  $searchValue = $_POST['search']['value']; // Search value

  if ($draw == 1) {
    $columnName = "tl.id";
    if ($rowperpage == -1) {
      $columnSortOrder  = "desc";
    } else {
      $columnSortOrder  = "desc LIMIT " . $rowperpage;
    }
  } else {
    if (in_array($sort_order[$columnIndex], array('subject', 'startdate', 'duedate', 'company', 'billable', 'int_duedate'))) {
      if ($sort_order[$columnIndex] == 'subject') $columnName = "tl.subject";
      if ($sort_order[$columnIndex] == 'startdate') $columnName = "str_to_date(trim(tl.start_date),'%Y-%m-%d')";
      if (($sort_order[$columnIndex] == 'duedate') || ($sort_order[$columnIndex] == 'int_duedate')) $columnName = "str_to_date(trim(tl.end_date),'%Y-%m-%d')";
      if ($sort_order[$columnIndex] == 'company') $columnName = "cl.crm_company_name";
      // $columnSortOrder  = "desc LIMIT " . $rowperpage;
    } else {
      $columnName = "tl.id";
      if ($rowperpage == -1) {
        $columnSortOrder  = "desc";
      } else {
        $columnSortOrder  = "desc LIMIT " . $rowperpage;
      }
    }
  }
  $searchByStatus     = $this->input->post('status_search');
  $top_status_search  = $this->input->post('top_status_search');
  $searchByPriority   = $this->input->post('priority_search');
  $searchByBillable   = $this->input->post('billable_search');
  $colwise_filter_val = $this->input->post('colwise_filter_val');

  if ($top_status_search != '') {
    $searchByStatus = array($top_status_search);
  }
  #searching
  $searchQuery = '';
  if ($searchByStatus != '' && !empty($searchByStatus)) {
    $searchQuery .= " AND tl.task_status in (" . implode(",", $searchByStatus) . ")";
  }
  if ($searchByPriority != '' && !empty($searchByPriority)) {
    $result       = "'" . implode("', '", $searchByPriority) . "'";
    $searchQuery .= " AND tl.priority in (" . $result . ")";
  }
  if ($searchByBillable != '' && !empty($searchByBillable)) {
    if (in_array('Non Billable', $searchByBillable)) {
      $searchByBillable[] = 0;
      $searchByBillable[] = "";
    }
    $result       = "'" . implode("', '", $searchByBillable) . "'";
    $searchQuery .= " AND tl.billable in (" . $result . ")";
  }
  if ($searchValue != '') {
    $searchQuery .= " AND subject LIKE '%" . $searchValue . "%'";
  }
  if ($colwise_filter_val != '') {
    $search_flag             = 1;
    $colwise_filter_vals     = explode(",", strval($colwise_filter_val));
    $result = $column_result = array();

    foreach ($colwise_filter_vals as $colwise_filter_key => $colwise_filter_value) {
      $column_result[] = explode("//", $colwise_filter_value);
    }
    $qry_progress_status = "";

    foreach ($column_result as $element) {
      if (isset($element[1])) {
        if ($element[1] == "subject_TH") {
          $searchQuery .= " AND subject LIKE '%" . $element[0] . "%'";
        } else if ($element[1] == "progress_TH") {
          if ($qry_progress_status == "") {
            $qry_progress_status .= " AND progress_status = (SELECT id FROM progress_status WHERE status_name = '" . trim($element[0]) . "')";
          } else {
            $qry_progress_status .= " OR progress_status = (SELECT id FROM progress_status WHERE status_name = '" . trim($element[0]) . "')";
          }
        }elseif ($element[1] == "company_TH") {
          $searchQuery .= " AND crm_company_name LIKE '%" . $element[0] . "%'";
        } else {
          $columnSortOrder  = "desc";
        }
      }
    }
    if ($qry_progress_status != "") {
      $searchQuery .= $qry_progress_status;
    }
  }
  ##Total number of records with filtering  

  $sql                   = $q1_with_join . $searchQuery;
  $totalRecordwithFilter = $this->db->query($sql)->row('allcount');
  $totalRecord           = $this->db->query($q1_with_join)->row('allcount');
  $final_data            = $search_array = $final_column_filter = array();

  if ($rowperpage == -1) {
    $offset_str  = "";
  } else {
    $offset_str  = " OFFSET " . $row_limit;
    //$offset_str  = " LIMIT " . $rowperpage . " OFFSET " . $row_limit;
  }
  $mod_columnName           = "msg_count";
  //$sql                    = $q2 . $searchQuery . " order by " . $columnName . " " . $columnSortOrder.$offset_str;
  $sql                      = $q2 . $searchQuery . " order by " . $mod_columnName . " " . $columnSortOrder . $offset_str;
  // echo '<br/><br/>'.$sql;
  // die;
  // echo '<br/> >>'.$sql;
  // echo '<br/> >>'.$q1_with_join;
  // die;

  $task_list                = $this->db->query($sql)->result_array();
  //print_r($task_list);
  //die;

  $data['task_list_count']  = count($task_list);
  $get_tk_id                = (!empty($task_list)) ? implode(",", array_column($task_list, 'id')) : '-1';
  $get_datalist             = array_column($task_list, 'task_status');
  $data['get_datalist']     = array_count_values($get_datalist);
  $timer_condition          = ($_SESSION['user_type'] == 'FU') ? "AND user_id=" . $_SESSION['id'] . "" : "";
  $individual_timer         = $this->db->query("SELECT `time_start_pause`,`task_id`,`user_id`,(CHAR_LENGTH(time_start_pause) - CHAR_LENGTH(REPLACE(time_start_pause, ',', '')) + 1) as total from individual_task_timer where task_id in ($get_tk_id) " . $timer_condition . " ORDER BY id ")->result_array();
  $overall_individual_timer = $individual_timer;
  $us_id                    = $_SESSION['id'];

  $user_count_result = array_filter($overall_individual_timer, function ($var) use ($us_id) {
    return $var['user_id'] == $us_id && $var['total'] % 2 !== 0;
  });
  $user_count_result = (count($user_count_result) > 0) ? current($user_count_result) : [];
  $user_count_result = (!empty($user_count_result)) ? $user_count_result['task_id'] : 0;

  $S_V = $this->db->query(
    "select  module_id,
            GROUP_CONCAT(assignees) as get_assignees 
    from firm_assignees 
    where module_name='TASK' 
    and module_id in (" . $get_tk_id . ")  
    group by module_id;  "
  )->result_array();

  $assign_data = [];
  foreach ($S_V as $key => $value) {
    $ex_vals                            = explode(",", $value['get_assignees']);
    $assign_data[$key]['get_assignees'] = $this->Task_creating_model->get_module_user_data($ex_vals);
    $assign_data[$key]['module_id']     = $value['module_id'];
  }
  $data['assign_data']  = $assign_data;
  $settings_val         = $this->db->query('select `id`,`service_name` from service_lists where id <16 ')->result_array();
  $work_flow            = $this->Service_model->get_firm_workflow();
  $task_status          = $this->db->query('SELECT `id`,`status_name` from task_status where is_superadmin_owned=1')->result_array();
  $progress_task_status = $this->Common_mdl->get_firm_progress();

  if ($data['status_condition'] == 0) {
    $result_status_array = $task_status;
  } else {
    $result_status_array = $this->Common_mdl->selectRecord('task_status', 'id', $data['status_condition']);
  }
  // foreach  for task list//
  foreach ($task_list as $key => $value) {

    $priority_color = '';

    if ($value["priority"] == 'medium') {
      $priority_color = 'background: transparent';
      $priority_color_bg = 'color: #ff9900';
      $priority_color_bd = 'border: 2px solid #ff9900';
    } elseif ($value["priority"] == 'high') {
      $priority_color = 'background: transparent';
      $priority_color_bg = 'color: #ff0066';
      $priority_color_bd = 'border: 2px solid #ff0066';
    } elseif ($value["priority"] == 'super_urgent') {
      $priority_color = 'background: transparent';
      $priority_color_bg = 'color: #ff0000';
      $priority_color_bd = 'border: 2px solid #ff0000';
    } elseif ($value["priority"] == 'low') {
      $priority_color = 'background: transparent';
      $priority_color_bg = 'color: #ffcc00';
      $priority_color_bd = 'border: 2px solid #ffcc00';
    } else {
      $priority_color = '';
      $priority_color_bg = '';
      $priority_color_bd = '';
    }

    if ($value["related_to"] == 'sub_task') {
      continue; //don't show subtask to admin,etc..
    }

    // error_reporting(0);                                   
    if ($value["start_date"] != "" && $value["end_date"] != "") {
      $start = date_create(implode('-', array_reverse(explode('-', $value["start_date"]))));
      $end   = date_create(implode('-', array_reverse(explode('-', $value["end_date"]))));
      $diff  = date_diff($start, $end);
      $y     =  $diff->y;
      $m     =  $diff->m;
      $d     =  $diff->d;
      $h     =  $diff->h;
      $min   =  $diff->i;
      $sec   =  $diff->s;
    } else {
      $y   =  0;
      $m   =  0;
      $d   =  0;
      $h   =  0;
      $min = 0;
      $sec =  0;
    }
    $d_rec = date('Y-m-d', strtotime($value["end_date"]));
    $Ddays = dateDiffrents(date('Y-m-d'), $d_rec);

    if ($Ddays < 0) {
      $d_val = abs($Ddays) . " Days Delay";
    } else if ($Ddays === 0) {
      $d_val = "Due Today";
    } else {
      $d_val = $Ddays . " Days";
    }

    if ($value["task_status"] == '1') {
      $percent = 0;
      $stat    = 'Not Started';
    }
    if ($value["task_status"] == '2') {
      $percent = 25;
      $stat    = 'In Progress';
    }
    if ($value["task_status"] == '3') {
      $percent = 50;
      $stat    = 'Awaiting for a feedback';
    }
    if ($value["task_status"] == 'testing') {
      $percent = 75;
      $stat    = 'Testing';
    }
    if ($value["task_status"] == '5') {
      $percent = 100;
      $stat    = '5';
    }
    if ($_SESSION["permission"]["Task"]["view"] != '1') {
      $view_style = "style='display:none'";
    }
    if ($_SESSION["permission"]["Task"]["edit"] != '1') {
      $edit_style = "style='display:none'";
      $edit_disable_style = "disable=diable";
    }
    if ($_SESSION["permission"]["Task"]["delete(oid)"] != '1') {
      $delete_style = "style='display:none'";
    }
    $sub_ico = "";
    if (!empty($value["sub_task_id"])) {
      $sub_ico = "details-control";
    }
    $r['first_td'] = '<input  type="hidden" class="key_val" id="key_val_' . $value["id"] . '" value=' . implode(",", $key_val) . '><input type="hidden" id="first_td_id_' . $value["id"] . '" class="first_td_id" name="first_td_id[]" value="' . $value["id"] . '"><input type="hidden" id="first_td_class_' . $value["id"] . '" name="first_td_class[]" class="first_td_class" value="subtask_toggle_TD  ' . $sub_ico . '">';
    $r['input_td'] = '<div style="" class="checkbox-fade fade-in-primary">
          <label class="custom_checkbox1">
          <input type="checkbox" id="checkbox-temp" class="alltask_checkbox" data-alltask-id="' . $value["id"] . '">
         <i></i>
          </label>
          </div>';

    $time             = json_decode($value["counttimer"]);
    $time_start_date  = $value["time_start_date"];
    $time_end_date    = $value["time_end_date"];
    $hours            = 0;
    $mins             = 0;
    $sec              = 0;
    $pause            = '';
    $individual_timer = array();

    foreach ($overall_individual_timer as $indi_key => $indi_value) {
      if ($value["id"] == $indi_value["task_id"]) {
        $individual_timer[] = $overall_individual_timer[$indi_key];
      }
    }
    $pause_val      = 'on';
    $pause          = 'on';
    $for_total_time = 0;

    if (count($individual_timer) > 0) {
      foreach ($individual_timer as $intime_key => $intime_value) {
        $its_time     = $intime_value["time_start_pause"];
        $res          = explode(',', $its_time);
        $res1         = array_chunk($res, 2);
        $result_value = array();

        foreach ($res1 as $rre_key => $rre_value) {
          $abc = $rre_value;
          if (count($abc) > 1) {
            if ($abc[1] != "") {
              $ret_val = $this->Common_mdl->calculate_test($abc[0], $abc[1]);
              array_push($result_value, $ret_val);
            } else {
              $pause     = '';
              $pause_val = '';
              $ret_val   = $this->Common_mdl->calculate_test($abc[0], time());
              array_push($result_value, $ret_val);
            }
          } else {
            $pause     = '';
            $pause_val = '';
            $ret_val   = $this->Common_mdl->calculate_test($abc[0], time());
            array_push($result_value, $ret_val);
          }
        }
        foreach ($result_value as $re_key => $re_value) {
          $for_total_time += $this->Common_mdl->time_to_sec($re_value);
        }
      }

      $hr_min_sec = $this->Common_mdl->sec_to_time($for_total_time);
      $hr_explode = explode(':', $hr_min_sec);
      $hours      = (int)$hr_explode[0];
      $min        = (int)$hr_explode[1];
      $sec        = (int)$hr_explode[2];
    } else {
      $hours = 0;
      $min   = 0;
      $sec   = 0;
      $pause = 'on';
    }
    $sub_task_id = $value["sub_task_id"];

    /*Company : Appy Codes ||| Coder: Amit Das||| Date : 16-06-2020*/
    if ($sub_task_id != '') {
      $sql           = "SELECT counttimer FROM add_new_task WHERE id IN (" . $sub_task_id . ")";
      $calc_time     = $this->db->query($sql)->result_array();
      $calctime      = array_column($calc_time, 'counttimer');
      $total_hours   = 0;
      $total_minutes = 0;
      $total_seconds = 0;

      foreach ($calctime as $timer) {
        $timerarray     = json_decode($timer);
        $total_hours   += $timerarray->hours;
        $total_minutes += $timerarray->minutes;
        $total_seconds += $timerarray->seconds;
      }
      $minutes_from_seconds = 0;
      $hours_from_minutes   = 0;

      if ($total_seconds > 59) {
        $minutes_from_seconds = $total_seconds / 60;
        $actual_seconds       = $total_seconds % 60;
      } else {
        $actual_seconds = $total_seconds;
      }

      if (($total_minutes + $minutes_from_seconds) > 59) {
        $hours_from_minutes = ($total_minutes + $minutes_from_seconds) / 60;
        $actual_minutes     = ($total_minutes + $minutes_from_seconds) % 60;
      } else {
        $actual_minutes = floor($total_minutes + $minutes_from_seconds);
      }

      $actual_hours =  floor($total_hours + $hours_from_minutes);
      $hours        = $actual_hours;
      $min          = $actual_minutes;
      $sec          = $actual_seconds;
    }
    $isPasuse                    = ($pause == "" ? "time_pause1" : "");
    $stop_sub_task_id            = ($value["sub_task_id"]) ? $value["sub_task_id"] : "0";
    $stop_time_start_date        = ($time_start_date != '') ? date('Y/m/d H:i:s', strtotime($time_start_date)) : "";
    $stop_time_end_date          = ($time_end_date != '') ? date('Y/m/d H:i:s', strtotime($time_end_date)) : "";
    $dropdown_filter['timer_TH'] = sprintf("%02d", $hours) . ' : ' . sprintf("%02d", $min) . ' : ' . sprintf("%02d", $sec);

    #Get subtask timers
    $userwiseTimers = $this->Task_creating_model->getSubtaskTimers($stop_sub_task_id);
    $tot_time_spent = 0;

    foreach ($userwiseTimers as $ut) {
      $tot_time_spent += $ut['total_time_spent'];
    }
    $total_time_spent_in_sec = $tot_time_spent;
    $tot_time_spent = $this->Task_creating_model->sec_to_time($tot_time_spent);
    $time_explode   = explode(':', $tot_time_spent);
    $hours          = $time_explode[0];
    $min            = $time_explode[1];
    $sec            = $time_explode[2];

    $r['stop_watch'] = '<input hidden name="th_attr" value="' . sprintf("%02d", $hours) . ' : ' . sprintf("%02d", $min) . ' : ' . sprintf("%02d", $sec) . '" >
                        <input hidden name="th_class" value="timer_TD timer_class">
                        <div  style="" 
                              class="stopwatch" 
                              data-autostart="false" 
                              data-date="2020/06/13 15:37:25" 
                              data-id="' . $value["id"] . '"  
                              data-subid="' . $stop_sub_task_id . '" 
                              data-hour="' . $hours . '" 
                              data-min="' . $min . '" 
                              data-sec="' . $sec . '" 
                              data-mili="0" 
                              data-start="' . $stop_time_start_date . '" 
                              data-end="' . $stop_time_end_date . '" 
                              data-current="Start" 
                              data-pauseon="' . $pause . '" >
                        <div class="time timer_' . $value["id"] . '">
                        <span class="hours">00</span>: 
                        <span class="minutes">00</span>: 
                        <span class="seconds">00</span>      
                        <!-- <span>' . $tot_time_spent . '</span>-->                    
                        </div>        
                        <div class="controls per_timerplay_' . $value["id"] . '" id="' . $value["id"] . '">';

    /* View the timer only task start by the itself or The task not started by anyone */

    $timer_style = $this->Task_creating_model->timer_validate($value["id"]);
    $timer_style = ($timer_style[0]['timer_style']) ? 'display:block' : 'display:none';
    /* View the timer only task start by the itself or The task not started by anyone */

    if ($_SESSION["permission"]["Task"]["edit"] == 1 && $value["task_status"] != "5" && empty($value["sub_task_id"])) {
      $r['stop_watch'] .= '<button class="toggle for_timer_start_pause new_play_stop per_timerplay_' . $value["id"] . ' ' . $isPasuse . ' " id="' . $value["id"] . '"  style="' . $timer_style . '"  data-pausetext="||" data-resumetext=">"> > </button>';
    }
    $r['stop_watch'] .= ' </div>
              </div>
              <div class="individual_timer_div">
                  <a href="javascript:;"  data-id="' . $value["id"] . '" class="individual_timer_btn timer_btn_view">View</a>
              </div>';

    if ($_SESSION["permission"]["Task"]["view"] == '1') {
      $subject_tag = base_url() . 'user/task_details/' . $value["id"] . '';
    } else {
      $subject_tag = "javascript:;";
    }

    #Test Block
    $sql        = "SELECT * FROM task_comments WHERE task_id = " . $value['id'] . " ORDER BY id DESC";
    $comments   = $this->db->query($sql)->result_array();
    $visitorArr = [];
    $userId     = $_SESSION['userId'];

    foreach ($comments as $tl) {
      $tmpArr = explode(',', $tl['viewed']);

      if (in_array($userId, $tmpArr)) {
        $visitorArr[$tl['task_id']][] = $tl['id'];
      }
    }

    //  . ' < -- > ' . ucfirst($new_subject[1]) .
    
   // ' . ucfirst($value["subject"]) . '
    $new_subject = @explode($value["crm_company_name"], $value["subject"]);
    $r['subject'] = '
      <input hidden name="th_attr" value="' . ucfirst($value["subject"]) . '" >
      <input hidden name="th_class" value="subject_TD">
      <a class="task-subject-title" href="' . $subject_tag . '" data-val="' . ucfirst($value["subject"]) . '" target="_blank">
      ' . ucfirst($value["subject"]) . '
      </a>
      <p class="priority_div" style="' . $priority_color . ';' . $priority_color_bg . ';' . $priority_color_bd . '">' . ucwords(str_replace("_", " ", $value["priority"])) . '</p>';

    if (!empty($comments) && empty($visitorArr)) {
      $msg_data_header = "<div class=notification-header>".'Notification'."</div>";
      $msg_data = "<div class=temp-notification>";        
      $i        = 1;
      foreach ($comments as $msg) {
        $msg_data .= "<div>                          
                        <div class=notification-read-wrapper>
                        <div class=notification-id-num>" . $i . "</div>
                        <div class=notification-read-msg>" . $msg['message'] . "</div>
                        </div>
                      </div>";
        $i++;
      }
      
      $msg_data .= "</div>";
      $msg_data .= "<a href='javascript:void(0)' class='mark_as_read' data-id='" . $value['id'] . "'>Mark as read</a>";
      $value['landline'] = str_replace('["','',$value['landline']);
      $value['landline'] = str_replace('"]','',$value['landline']);
      $r['subject'] .= '<div class="msg-wrapper">                            
                          <p class="priority_div unread-msg blink" style="background: #ff0066">                            
                            <a href="/user/task_details/' . $value['id'] . '">
                            <i class="fa fa-envelope-o" aria-hidden="true"></i>
                            </a>
                          </p>
                          <div class="msg-content">' .$msg_data_header . $msg_data . '</div>
                          </div>
                          <div class="phone-wrapper phone-class" data-toggle="modal" data-target="#popup_birthday_mail" data-email="'.$value['main_email'].'" data-phone="'.$value['telephone_number'].'" data-mobile="'.$value['mobile'].'" data-landline="'.$value['landline'].'" data-contact_id="'.$value['contact_id'].'" data-client_id="'.$value['client_id'].'" data-task-id="'.$value["id"].'">
                          <p class="priority_div unread-msg blink" style="background: #ed1c24">                            
                            <a>
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            </a>
                          </p>                            
                        </div>';
    }
    if (($value['sent_email_count'] != null) && ($value['email_count'] != null)) {
      $r['subject'] .= '<div class="msg-wrapper">
                        (' . $value['sent_email_count'] . ' <i class="fa fa-envelope-o" aria-hidden="true"></i> sent out of ' . $value['email_count'] . ')                         
                        </div>';
    }

    $search_array[$key][]            = ucfirst($value["subject"]);
    $dropdown_filter['subject_TH']   = ucfirst($value["subject"]);
    $startdate_TD                    = date('d-m-Y', strtotime($value["start_date"]));
    $r['start_date']                 = '<input hidden name="th_attr" value="' . $startdate_TD . '" ><input hidden name="th_class" value="startdate_TD startdate_class">' . $startdate_TD;
    $search_array[$key][]            = $startdate_TD;
    $dropdown_filter['startdate_TH'] = $startdate_TD;

    if (!empty(strtotime($value["end_date"]))) {
      if (isset($value["related_to_services"]) && !empty($value["related_to_services"]) && $value["related_to_services"]) {
        $services = explode("_", $value["related_to_services"]);
        if (isset($services[1]) && $services[0] == 'wor') {
          $table         = 'work_flow';
          $service_id    = $services[1];
          $service_array = $work_flow;
        } else {
          $table         = 'service_lists';
          $service_id    = (isset($services[1])) ? $services[1] : $services[0];
          $service_array = $settings_val;
        }
        //$service_key = array_search($service_id, array_column($service_array, 'id'));
        $Rservice    = $service_id; //$service_array[$service_key]["service_name"];
      } else {
        $Rservice = 0;
      }

      $due_dates_data    = json_decode($this->calculate_due_dates($value["id"], $Rservice, $value["progress_status"]));
      $internal_due_date = !empty($due_dates_data[0]->internal_due_date) ? date('d-m-Y', strtotime($due_dates_data[0]->internal_due_date)) : '-';
      $internal_due_date = ($internal_due_date != '01-01-1970')          ? $internal_due_date                                              : '-';
      $internal_delay    = !empty($due_dates_data[0]->internal_delay)    ? $due_dates_data[0]->internal_delay                              : '';
      $external_due_date = !empty($due_dates_data[0]->external_due_date) ? date('d-m-Y', strtotime($due_dates_data[0]->external_due_date)) : '-';
      $external_due_date = ($external_due_date != '01-01-1970')          ? $external_due_date                                              : '-';
      $external_delay    = !empty($due_dates_data[0]->external_delay)    ? $due_dates_data[0]->external_delay                              : '';
      $t_status          = trim($due_dates_data[0]->status);

      $r['start_d']                 = '
        <input hidden name="th_attr" value="' . date('d-m-Y', strtotime($value["end_date"])) . '" >
        <input hidden name="th_class" value="intduedate_TD intduedate_class">
        <div class="internal-due" style="">
          <span class="int-due-dt">' . $internal_due_date . '</span>';

      if ($t_status == 'complete' || $t_status == 5) {
        // Do nothing for now
      } else {
        $r['start_d'] .= '<span class="int-due-dt-delay timer check_timer"> <b>' . $internal_delay . ' </b> </span>';
      }

      $r['start_d'] .= '</div>';

      // $r['end_date'] =  '<span class="ext-due-title">External: </span>';

      $r['end_date'] =  '
      <input hidden name="th_attr" value="' . date('d-m-Y', strtotime($value["end_date"])) . '" >
        <input hidden name="th_class" value="duedate_TD duedate_class">
        <div class="ext-due" style="">
          <span class="ext-due-dt">' . $external_due_date . '</span>';

      if ($t_status == 'complete' || $t_status == 5) {
        // Do nothing for now
      } else {
        $r['end_date'] .= '<span class="ext-due-dt-delay timer check_timer"> <b>' . $external_delay . ' </b> </span>';
      }

      $r['end_date']   .= '</div>';

      //$r['int_end_date'] = 'test';
      // $r['end_date'] = $external_due_date;
      $search_array[$key][]          = date('d-m-Y', strtotime($value["end_date"]));
      $dropdown_filter['duedate_TH'] = date('d-m-Y', strtotime($value["end_date"]));


      // $r['start_d'] = $internal_due_date;
      $search_array[$key][]          = date('d-m-Y', strtotime($value["end_date"]));
      $dropdown_filter['int_duedate_TH'] = date('d-m-Y', strtotime($value["end_date"]));
    } else {
      $r['start_d'] = '';
      $r['end_date'] = '';
    }

    $status_val1                  = '';
    $result_key                   = array_search($value["task_status"], array_column($result_status_array, 'id'));
    $status_val1                  = $result_status_array[$result_key]["status_name"];
    $search_array[$key][]         = $status_val1;
    $dropdown_filter['status_TH'] = $status_val1;
    $r['task_select']             = '<input hidden name="th_attr" value="' . $status_val1 . '" ><input hidden name="th_class" value="status_class"><input hidden name="th_id" value="status_TD status_' . $value["id"] . '"><select name="task_status" id="task_status" class="task_status per_taskstatus_' . $value["id"] . '" data-id="' . $value["id"] . '" "' . $edit_disable_style . '">
            <option value="">Select Status</option>';

    foreach ($task_status as $status_key => $status_val) {
      $status_select     = ($value["task_status"] == $status_val["id"]) ? "selected" : "";
      $r['task_select'] .= '<option value="' . $status_val["id"] . '" ' . $status_select . ' >' . $status_val["status_name"] . '</option>';
    }
    $r['task_select']              .= ' </select>';
    $progress_status                = array_column($progress_task_status, 'status_name', 'id');
    $selected_status                = !empty($progress_status[$value["progress_status"]]) ? $progress_status[$value["progress_status"]] : '';
    $search_array[$key][]           = trim($selected_status);
    $dropdown_filter['progress_TH'] = trim($selected_status);


    if (isset($value["related_to_services"]) && !empty($value["related_to_services"]) && $value["related_to_services"]) {
      $services = explode("_", $value["related_to_services"]);
      if (isset($services[1]) && $services[0] == 'wor') {
        $table         = 'work_flow';
        $service_id    = $services[1];
        $service_array = $work_flow;
      } else {
        $table         = 'service_lists';
        $service_id    = (isset($services[1])) ? $services[1] : $services[0];
        $service_array = $settings_val;
      }
      //$service_key = array_search($service_id, array_column($service_array, 'id'));
      $Rservice    = $service_id; //$service_array[$service_key]["service_name"];
    } else {
      $Rservice = '0';
    }


    $r['progress_select']           = '<input hidden name="th_attr" value="' . trim($selected_status) . '" ><input hidden name="th_class" value="progress_TD progress_class">
    <select name="progress_status" id="progress_status" data-service="' . $Rservice . '" class="progress_status per_progress_"' . $value["id"] . '" data-id="' . $value["id"] . '" "' . $edit_disable_style . '">
            <option value="">Select Progress Status</option>';

    foreach ($progress_task_status as $progress_task_key => $status_val) {
      $progress_select       = ($value["progress_status"] == $status_val["id"]) ? "selected" : "";
      $r['progress_select'] .= '<option value="' . $status_val["id"] . '" ' . $progress_select . '>' . $status_val["status_name"] . '</option>';
    }
    $r['progress_select']          .= ' </select>';
    $value["priority"]              = strtolower($value["priority"]);
    $low_select                     = (isset($value["priority"]) && $value["priority"] == "low") ? "selected" : "";
    $med_select                     = (isset($value["priority"]) && $value["priority"] == "medium") ? "selected" : "";
    $high_select                    = (isset($value["priority"]) && $value["priority"] == "high") ? "selected" : "";
    $super_select                   = (isset($value["priority"]) && $value["priority"] == "super_urgent") ? "selected" : "";
    $search_array[$key][]           = $value["priority"];
    $dropdown_filter['priority_TH'] = $value["priority"];
    $r['priority_select']           = '<input hidden name="th_attr" value="' . strtolower($value["priority"]) . '" ><input hidden name="th_class" value="priority_TD priority_class"><div style="">
          <select name="task_priority" id="task_priority" class="task_priority per_taskpriority' . $value["id"] . '" data-id="' . $value["id"] . '" "' . $edit_disable_style . '">
          <option value="">By Priority</option>
          <option value="low" ' . $low_select . '>Low</option>
          <option value="medium" ' . $med_select . ' >Medium</option>
          <option value="high" ' . $high_select . '>High</option>
          <option value="super_urgent" ' . $super_select . '>Super Urgent</option>
          </select>
          </div>';
    $r['tag']                       = '<input hidden name="th_attr" value="' . trim($value["tag"]) . '" ><input hidden name="th_class" value="Tag_TD tag_class">' . trim($value["tag"]);
    $search_array[$key][]           = trim($value["tag"]);
    $dropdown_filter['tag_TH']      = trim($value["tag"]);
    $assign_user_key                = array_search($value["id"], array_column($assign_data, 'module_id'));
    $ex_val                         = $assign_data[$assign_user_key]["get_assignees"];
    $Assignee                       = $ex_val;
    $search_array[$key][]           = $Assignee;
    $dropdown_filter['assignto_TH'] = $Assignee;
    $r['assigness_val']             = '<input hidden name="th_attr" value="' . $Assignee . '" ><input hidden name="th_class" value="">
          <span style="" class="task_' . $value["id"] . '">';
    if ($_SESSION["permission"]["Task"]["edit"] == '1') {
      $r['assigness_val'] .= '<a href="javascript:;" data-toggle="modal" data-target="#adduser_' . $value["id"] . '" class="adduser1 user_change per_assigne_' . $value["id"] . '"><i class="fa fa-plus"></i></a>';
    }
    $r['assigness_val'] .= '<strong>' . $Assignee . '</strong>
          </span>';

    if (isset($value["related_to_services"]) && !empty($value["related_to_services"]) && $value["related_to_services"]) {
      $services = explode("_", $value["related_to_services"]);
      if (isset($services[1]) && $services[0] == 'wor') {
        $table         = 'work_flow';
        $service_id    = $services[1];
        $service_array = $work_flow;
      } else {
        $table         = 'service_lists';
        $service_id    = (isset($services[1])) ? $services[1] : $services[0];
        $service_array = $settings_val;
      }
      $service_key = array_search($service_id, array_column($service_array, 'id'));
      $Rservice    = $service_array[$service_key]["service_name"];
    } else {
      $Rservice = '-';
    }
    $r['R_service']                 = '<input hidden name="th_attr" value="' . $Rservice . '" ><input hidden name="th_class" value="">' . $Rservice;
    $search_array[$key][]           = $Rservice;
    $dropdown_filter['services_TH'] = $Rservice;
    $companyName                    = ($value["crm_company_name"] != "") ? $value["crm_company_name"] : '-';
    $r['c_name']                    = '<input hidden name="th_attr" value="' . $companyName . '" ><input hidden name="th_class" value="s company_class"><a class="task-company-name" href="' . base_url() . 'client/client_infomation/' . $value['clientuid'] . '" target="_blank">' . $companyName . '</a>';
    $search_array[$key][]           = $companyName;
    $dropdown_filter['company_TH']  = $companyName;
    $isBillable                     = ($value["billable"] == 'Billable' ? 'Billable' : 'Non Billable');
    $r["billable"]                  = '<input hidden name="th_attr" value="' . $isBillable . '" ><input hidden name="th_class" value="">' . $isBillable;
    $search_array[$key][]           = $isBillable;
    $dropdown_filter['billable_TH'] = $isBillable;
    $r["button_ref"]                = '<p class="action_01 per_action_' . $value["id"] . '">';
    
    $r["button_ref"]                .= '<div class="dropdown">
    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="dropdwn-action"></span><span class="dropdwn-action"></span><span class="dropdwn-action"></span></button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">';
    if ($_SESSION["permission"]["Task"]["edit"] == '1') {
      $r["button_ref"] .= '<div class="tool-tip-delete"><a class="dropdown-item" href="javascript:;" data-toggle="modal" data-target="#deleteconfirmation" onclick="delete_task(this)" data-id="' . $value["id"] . '" title="Delete"><i class="icofont icofont-ui-delete tooltip-delete deleteAllTask" aria-hidden="true"></i></a></div>';
    }
    if ($_SESSION["permission"]["Task"]["edit"] == '1') {
      $r["button_ref"] .= '<a class="dropdown-item" target="_blank" href="' . base_url() . 'user/update_task/' . $value["id"] . '" title="Edit"><i class="icofont icofont-edit" aria-hidden="true"></i></a>';
      if ($value["task_status"] == '4') {
        $r["button_ref"] .= '<a class="dropdown-item" href="javascript:;" id="' . $value["id"] . '" class="archieve_click" onclick="archieve_click(this)" data-status="unarchive" data-toggle="modal" data-target="#my_Modal" title="UnArchive">
              <i class="fa fa-file-archive-o archieve_click" aria-hidden="true"></i></a>';
      } else {
        $r["button_ref"] .= '<a class="dropdown-item" href="javascript:;" id="' . $value["id"] . '" class="archieve_click" onclick="archieve_click(this)" data-status="4" data-toggle="modal" data-target="#my_Modal" title="Archive">
        <i class="fa fa-file-archive-o archieve_click" aria-hidden="true"></i></a>';
      }
    }
    $r["button_ref"]                .= '</div></div>';


    
    

    $r["button_ref"] .= '
                                    </p><input type="hidden" name="trhours_' . $value["id"] . '" id="trhours_' . $value["id"] . '" value="' . $hours . '" >
                                    <input type="hidden" name="trmin_' . $value["id"] . '" id="trmin_' . $value["id"] . '" value="' . $min . '" >
                                    <input type="hidden" name="trsec_' . $value["id"] . '" id="trsec_' . $value["id"] . '" value="' . $sec . '" >
                                    <input type="hidden" name="trmili_' . $value["id"] . '" id="trmili_' . $value["id"] . '" value="0" >
                                    <input type="hidden" name="trpause_' . $value["id"] . '" id="trpause_' . $value["id"] . '" value="' . $pause . '" >';

    $estimated_time =  (float)$value['estimated_time'];

    if (!$estimated_time) {
      $related_to_service = $value['related_to_services'];

      if (strrpos($related_to_service, 'ser_') !== false) {
        $related_to_service_arr = explode('_', $related_to_service);
        $related_to_service = $related_to_service_arr[1];
      }

      $related_service = $this->Common_mdl->select_record('service_lists', 'id', $related_to_service);

      if (is_array($related_service) && count($related_service)) {
        $estimated_time = (float)$related_service['estimated_time'];
      }
    }

    if (!empty($total_time_spent_in_sec) && $estimated_time) {
      $estimated_time_in_sec = $estimated_time * 3600;
      $percentage_progress = (($total_time_spent_in_sec / $estimated_time_in_sec) * 100);
      $percentage_progress = round($percentage_progress, 2);
    } else {
      $percentage_progress = false;
    }

    if ($percentage_progress !== false) {
      if($percentage_progress > 100){
        $r['subject'] .= '
        <div class="progress" style="margin-top:10px">
          <div class="progress-bar progress-bar-striped progress-bar-per" role="progressbar" style="width: ' . $percentage_progress . '%" aria-valuenow="' . $percentage_progress . '" aria-valuemin="0" aria-valuemax="100">' . $percentage_progress . '%</div>
        </div>';
      } else if($percentage_progress > 75  && $percentage_progress <= 100 ){
        $r['subject'] .= '
        <div class="progress" style="margin-top:10px">
          <div class="progress-bar progress-bar-striped progress-bar-per-high" role="progressbar" style="width: ' . $percentage_progress . '%" aria-valuenow="' . $percentage_progress . '" aria-valuemin="0" aria-valuemax="100">' . $percentage_progress . '%</div>
        </div>';
      } else if( $percentage_progress <= 75 ){
        $r['subject'] .= '
        <div class="progress" style="margin-top:10px">
          <div class="progress-bar progress-bar-striped progress-bar-per-low" role="progressbar" style="width: ' . $percentage_progress . '%" aria-valuenow="' . $percentage_progress . '" aria-valuemin="0" aria-valuemax="100">' . $percentage_progress . '%</div>
        </div>';
      } else {
        $r['subject'] .= '
        <div class="progress" style="margin-top:10px">
          <div class="progress-bar progress-bar-striped" role="progressbar" style="width: ' . $percentage_progress . '%" aria-valuenow="' . $percentage_progress . '" aria-valuemin="0" aria-valuemax="100">' . $percentage_progress . '%</div>
        </div>';
      }
    }

    $default_array = array(
      $r['first_td'],
      $r['input_td'],
      $r['stop_watch'],
      $r['subject'],
      $r['start_date'],
      $r['end_date'],
      $r['task_select'],
      $r['progress_select'],
      $r['priority_select'],
      $r['tag'],
      $r['assigness_val'],
      $r['R_service'],
      $r['c_name'],
      $r["billable"],
      $r["button_ref"],
      $r['start_d'],
    );

    $final_datas          = [];
    $search_array[$key][] = $value['id'];

    // colwise filter//
    if ($colwise_filter_val != '') {
      $search_flag         = 1;
      $colwise_filter_vals = explode(",", strval($colwise_filter_val));
      $result = $column_result = array();

      foreach ($colwise_filter_vals as $colwise_filter_key => $colwise_filter_value) {
        $column_result[] = explode("//", $colwise_filter_value);
      }
      foreach ($column_result as $element) {
        if (isset($element[1])) {
          //$element[0]            = ($element[1] == 'priority_TH') ? strtolower($element[0]) : $element[0];
          if (($element[1] == 'priority_TH') || ($element[1] == 'progress_TH')) {
            $element[0] = strtolower($element[0]);
          } else {
            $element[0] = $element[0];
          }
          $result[$element[1]][] = $element[0];
        }
      }
      $res_count = 0;

      foreach ($result as $column_result_key => $column_result_value) {
        if (in_array($dropdown_filter[$column_result_key], $column_result_value) && $column_result_key != 'assignto_TH') {
          $res_count++;
        } else if ($column_result_key == 'progress_TH') {
          $res_count++;
        } else {
          $dropdown_filter[$column_result_key] = explode(",", $dropdown_filter[$column_result_key]);
          $get_intersect                       = array_intersect($column_result_value, $dropdown_filter[$column_result_key]);

          if (count($get_intersect) > 0) {
            $res_count++;
          }
        }
      }

      if (count($result) == $res_count) {
        array_push($final_column_filter, $value['id']);
      }
      if (!in_array($value['id'], $final_column_filter)) {
        continue;
        $default_array = array();
      }
    }
    //  end column wise filter //
    //  Datatable search  //
    if ($searchValue != "") {
      if (stripos(json_encode($search_array[$key]), $searchValue) !== false) {
        $search_flag = 1;
        $final_datas = $default_array;
      }
    } else {
      $final_datas = $default_array;
    }
    //  end Datatable search  //
    $new_array = [];

    if (!empty($final_datas)) {
      foreach ($key_val as $key => $key_val_value) {
        $new_array[$key] = $final_datas[$key_val_value];
      }
      // array_splice( $new_array, 11, 0, $r['start_d'] );
      $final_data[] = (!empty($new_array)) ? $new_array : $final_datas;
    }
  }

  // end foreach  for task list//
  $totalRecordwithFilter = ($search_flag == 0) ? $totalRecordwithFilter : count($final_data);

  if (isset($_POST['colwise_filter_val']) && ($_POST['colwise_filter_val'] != '')) {
    if(count($final_data) > $_POST['length'])
    {
      $final_data = array_slice($final_data, $_POST['start'], $_POST['length']);
    }
  }

  $output                = array(
    "draw"            => $draw,
    "recordsTotal"    => $totalRecord,
    "recordsFiltered" => $totalRecordwithFilter,
    "data"            => $final_data,
    "rowperpage"      => $_POST['length']
  );

  /*echo '<pre>';
  print_r($output);
  echo '</pre>';
  die;*/

  echo json_encode($output);
}

  public function get_list_kanban()
  {

    $limit = $_POST['limit'];
    $start = $_POST['start'];
    $status = $_POST['status'];
    $res = $_POST['res'];

    $data['leads_rec'] = $this->db->query("SELECT * FROM add_new_task AS tl where tl.id in ($res) and tl.firm_id = " . $_SESSION['firm_id'] . " and related_to!='service_reminder' and   tl.related_to != 'sub_task' and   tl.task_status = " . $status . " order by id desc LIMIT " . $start . ", " . $limit . "")->result_array();

    $holdproduct = $this->load->view('users/ajax_kanban', $data, true);
    if (count($data['leads_rec']) > 0) {
      echo json_encode(array("holdproduct" => $holdproduct));
    } else {
      echo 0;
    }
  }


  public function get_individual_timer()
  {
    $task_id           = $_POST['task_id'];
    $get_val           = array();
    $all_task_id       = array($task_id);
    $data['task_form'] = $this->Common_mdl->selectRecord('add_new_task', $task_id, 'id');
    $sub_task_id       = array_filter(explode(",", $data['task_form'][0]['sub_task_id']));
    $all_task_id       = (!empty($sub_task_id)) ? array_merge($all_task_id, $sub_task_id) : $all_task_id;

    if (!empty($all_task_id)) {
      $all_task_id = implode(",", $all_task_id);
      $get_val     = $this->Common_mdl->getall_wherein_records('individual_task_timer', 'task_id', $all_task_id);
      $get_val     = array_filter(array_column($get_val, 'user_id'));
    }
    $data['explode_worker'] = ($_SESSION['user_type'] == 'FU') ? array($_SESSION['id']) : $get_val;
    $holdproduct            = $this->load->view('users/ajax_individual_timer', $data, true);

    if (count($data['task_form']) > 0) {
      echo json_encode(array("holdproduct" => $holdproduct));
    } else {
      echo 0;
    }
  }

  public function get_hidden_value()
  {
    $key_val = $this->Task_creating_model->Firm_column_settings('task_list');
    echo $key_val['hidden'];
  }


  public function get_user_view_timer()
  {
    $task_list = $this->input->post('task_id');
    $key_val = $this->Task_creating_model->timer_validate($task_list);
    echo json_encode($key_val);
    exit;
  }

  public function billable_action()
  {

    if (isset($_POST['id'])) {

      $ids = json_decode($_POST['id'], true);

      foreach ($ids as $id) {
        $da1 = [];
        $dat1 = [];
        $update_data['billable'] = 'Billable';

        $da = $this->db->query("select * from add_new_task where id=$id")->row_array();

        $get_val = array_filter(explode(",", $da['sub_task_id']));

        if ($da['related_to'] != 'leads') {

          if (count($get_val) > 0) {

            // print_r( array_filter($get_val));exit;

            foreach ($get_val as $key => $value) {

              $sub_1 = $this->db->query("SELECT * FROM  `add_new_task` WHERE  id=" . $value . "")->row_array();

              $username = $this->Common_mdl->getUserProfileName($_SESSION['id']);

              $log = $sub_1['subject'] . " task changed as billable  by " . $username;
              if ($da['company_name'] != '' && is_numeric($da['company_name']) && $sub_1['related_to'] != 'leads') {
                $user_id = $this->Common_mdl->get_field_value('client', 'user_id', 'id', $da['company_name']);
                if ($user_id != '') {
                  $this->Common_mdl->Create_Log('client', $user_id, $log);
                }
              }
            }
            $this->db->where('id', $value)->update('add_new_task', $update_data);
          }

          $username = $this->Common_mdl->getUserProfileName($_SESSION['id']);

          $log = $da['subject'] . " task changed as billable  by " . $username;

          if ($da['company_name'] != '' && is_numeric($da['company_name']) && $da['related_to'] != 'leads') {
            $user_id = $this->Common_mdl->get_field_value('client', 'user_id', 'id', $da['company_name']);
            if ($user_id != '') {
              $this->Common_mdl->Create_Log('client', $user_id, $log);
            }
          }

          $this->db->where('id', $id)->update('add_new_task', $update_data);
        }
      }
    }
    echo $this->db->affected_rows();
  }

  public function has_viewed()
  {
    $task_id       = $_REQUEST['task_id'];
    $userId        = $_SESSION['id'];
    $viewedDetails = $this->db->query("SELECT id, viewed FROM task_comments WHERE task_id = $task_id")->result_array();

    foreach ($viewedDetails as $vd) {
      $id = $vd['id'];

      if ($vd['viewed'] == "") {
        $sql = "UPDATE task_comments SET viewed = '$userId' WHERE task_id = $task_id AND id = $id";
        $this->db->query($sql);
      } else {
        $details = explode(',', $vd['viewed']);
        if (!in_array($userId, $details)) {
          $sql = "UPDATE task_comments SET viewed = CONCAT(viewed, ',$userId') WHERE task_id = $task_id AND id = $id";
          $this->db->query($sql);
        }
      }
    }
    echo true;
  }

  public function task_initiated()
  {
    $task_id = $_REQUEST['task_id'];
    $status  = $_REQUEST['status'];
    $user_id = $_SESSION['id'];

    if ($status == '1') {
      $sql = "UPDATE firm_assignees 
              SET task_started = CONCAT(task_started, ':$user_id') 
              WHERE module_id = $task_id
              AND CONCAT(':', assignees, ':') REGEXP ':$user_id:'";
      echo $this->db->query($sql);
    } else {
      $sql     = "SELECT * 
                  FROM firm_assignees 
                  WHERE module_id = $task_id 
                  AND CONCAT(':', task_started, ':') REGEXP ':$user_id:'
                  AND CONCAT(':', assignees, ':') REGEXP ':$user_id:'";
      $details = $this->db->query($sql)->result_array();
      $arr     = [];

      foreach ($details as $dtl) {
        $arr = explode(':', $dtl['task_started']);

        foreach ($arr as $k => $v) {
          if ($v == $user_id || $v == "") {
            unset($arr[$k]);
          }
        }
      }
      $task_started = implode(':', $arr);
      $sql          = "UPDATE firm_assignees 
                      SET task_started = '$task_started' 
                      WHERE module_id = $task_id 
                      AND CONCAT(':', task_started, ':') REGEXP ':$user_id:'
                      AND CONCAT(':', assignees, ':') REGEXP ':$user_id:'";
      echo $this->db->query($sql);
    }
  }

  public function calculate_due_dates($task_id, $serv_id, $status_id)
  {
    $this->Security_model->chk_login();
    $fid    = $_SESSION['firm_id'];
    $uid    = $_SESSION['id'];
    $sql_3  = "SELECT ANT.id,
                      ANT.start_date,
                      ANT.end_date,
                      ANT.task_status,
                      PSC.status_id,
                      PSC.created_at,
                      PSC.modified_at,
                      IDL.internal_deadline,
                      IDL.external_deadline
              FROM add_new_task AS ANT
              LEFT JOIN progress_status_change AS PSC
                     ON PSC.task_id = ANT.id  
                    AND PSC.status_id = $status_id            
              INNER JOIN internal_deadlines AS IDL
                      ON IDL.firm_id = $fid
                     AND IDL.user_id = $uid
                     AND IDL.task_status = PSC.status_id
                     AND IDL.service_id = $serv_id
              WHERE ANT.id = $task_id
              ORDER BY PSC.id DESC
              LIMIT 1";

    $data = $this->db->query($sql_3)->result_array();

    if (isset($data[0])) {
      $task_status = $data[0]['task_status'];

      if ($data[0]['internal_deadline'] > 0) {
        if (isset($data[0]['modified_at']) && $data[0]['modified_at'] != '') {
          $data[0]['internal_due_date'] = date('Y-m-d', strtotime('+' . $data[0]['internal_deadline'] . ' days', strtotime($data[0]['modified_at'])));
        } else {
          $data[0]['internal_due_date'] = '';
        }
      } else {
        if (isset($data[0]['end_date']) && $data[0]['end_date'] != '') {
          $data[0]['internal_due_date'] = date('Y-m-d', strtotime($data[0]['end_date']));
        } else {
          $data[0]['internal_due_date'] = '';
        }
      }

      if ($data[0]['external_deadline'] > 0) {
        if (isset($data[0]['modified_at']) && $data[0]['modified_at'] != '') {
          $data[0]['external_due_date'] = date('Y-m-d', strtotime('+' . $data[0]['external_deadline'] . ' days', strtotime($data[0]['modified_at'])));
        } else {
          $data[0]['external_due_date'] = '-';
        }
      } else {
        if (isset($data[0]['end_date']) && $data[0]['end_date'] != '') {
          $data[0]['external_due_date'] = date('Y-m-d', strtotime($data[0]['end_date']));
        } else {
          $data[0]['external_due_date'] = '-';
        }
      }

      $int_delay                    = round((time() - strtotime($data[0]['internal_due_date'])) / (60 * 60 * 24));
      $data[0]['internal_delay']    = ($int_delay > 0 && ($task_status != 'complete' && $task_status != 5)) ? $int_delay . 'Days Delay' : '';
      $ext_delay                    = 0;
      $data[0]['external_delay']    = ($ext_delay > 0 && ($task_status != 'complete' && $task_status != 5)) ? $ext_delay . 'Days Delay' : '';
    } else {
      if (isset($data[0]['end_date']) && $data[0]['end_date'] != '') {
        $data[0]['internal_due_date'] = date('Y-m-d', strtotime($data[0]['end_date']));
      } else {
        $data[0]['internal_due_date'] = '-';
      }

      if (isset($data[0]['end_date']) && $data[0]['end_date'] != '') {
        $data[0]['external_due_date'] = date('Y-m-d', strtotime($data[0]['end_date']));
      } else {
        $data[0]['external_due_date'] = '-';
      }
      $data[0]['internal_delay']    = '';
      $data[0]['external_delay']    = '';
    }

    if ($data[0]['external_due_date'] != '-') {
      $day_dif = round((strtotime($data[0]['external_due_date']) - strtotime($data[0]['internal_due_date'])) / (60 * 60 * 24));

      if ($day_dif < 0) {
        $data[0]['internal_due_date'] = $data[0]['external_due_date'];
        $int_delay                    = round((time() - strtotime($data[0]['internal_due_date'])) / (60 * 60 * 24));
        $data[0]['internal_delay']    = ($int_delay > 0 && ($task_status != 'complete' && $task_status != 5)) ? $int_delay . 'Days Delay' : '';
      }
    }
    return json_encode($data);
  }
}
