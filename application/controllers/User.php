<?php

//declare(strict_types=1);
//error_reporting(-1);

defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
	public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 *      http://example.com/index.php/welcome
	 *  - or -
	 *      http://example.com/index.php/welcome/index
	 *  - or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('Service_model', 'Demo_model', 'Common_mdl', 'Security_model', 'Invoice_model', 'Task_invoice_model', 'Task_creating_model', 'Task_section_model', 'Loginchk_model', 'Service_Reminder_Model', 'FirmFields_model', 'Client_model'));
		$this->load->helper(['download', 'firm_columnSettings', 'comman', 'template_decode']);
	}
	public function Check_Login_User()
	{
		if (!empty($_COOKIE['remindoo_logout']) && $_COOKIE['remindoo_logout'] == "remindoo_superadmin") {
			$this->Loginchk_model->superAdmin_LoginCheck();
		} else {
			$this->Security_model->chk_login();
		}
	}
	public function meeting()
	{
		$this->Security_model->chk_login();
		$this->load->view("users/meeting");
	}
	public function design()
	{
		$this->load->view("users/design");
	}

	public function task_time_sheet($task_id)
	{
		$data["today_details"] = $this->db->query("select itt.user_id,itt.time_start_pause,u.crm_name,ant.subject from individual_task_timer itt left join user u on u.id=itt.user_id left join add_new_task ant on ant.id=$task_id where itt.task_id=$task_id")->result_array();
		$this->load->view("users/time_sheet", $data);
	}

	public function permission_restrict()
	{
		$this->load->view('users/blank_page');
	}

	public function index($assigned = null)
	{
		$this->Security_model->chk_login();

		if ($_SESSION['permission']['Client_Section']['view'] != 1) {
			$this->load->view('users/blank_page');
		} else {
			if (empty($this->uri->segment(2)) || ($assigned == 'assigned')) {

				$firm_id 				= $this->session->userdata('firm_id');
				$data['getallUser']     = $this->Client_model->Get_Assigned_client($firm_id);
				$data["email_temp"]     = $this->Common_mdl->getTemplates();
				$data['column_setting'] = $this->FirmFields_model->Get_FirmFields_Settings($firm_id);
				
				if($assigned == 'assigned'){
					$data['assigned_user'] = $_SESSION['userId'];
				}
				$this->load->view('clients/user_list', $data);
			} else if ($this->uri->segment(2) == 'task_list') {
				redirect('/user/task_list');
			} else {
				$this->load->view('users/blank_page');
			}
		}
	}

	function assigned() {
        $this->index('assigned');
    }

	public function ClientTable_ReorderUpdate()
	{
		$this->Security_model->chk_login();
		if (!empty($_POST['reorder']) && !empty($_POST['column_cnst'])) {
			$firm_id = $this->session->userdata('firm_id');
			$this->FirmFields_model->Update_ListPageOrder_FirmFields($firm_id);

			$data['getallUser'] 	= $this->Client_model->Get_Assigned_client($firm_id);
			$data['column_setting'] = $this->FirmFields_model->Get_FirmFields_Settings($firm_id);

			$this->load->view('clients/Client_list_table', $data);
		}
	}

	public function ClientTable_ColVisUpdate()
	{
		$this->Security_model->chk_login();

		if (!empty($_POST['Showhidden'])) {
			$firm_id = $this->session->userdata('firm_id');
			$this->FirmFields_model->Update_ListPageColVis_FirmFields($firm_id);

			$data['getallUser']     = $this->Client_model->Get_Assigned_client($firm_id);
			$data['column_setting'] = $this->FirmFields_model->Get_FirmFields_Settings($firm_id);

			$this->load->view('demo_check/Client_list_table', $data);
		}
	}

	public function archive_user()
	{
		if (isset($_POST['id']) && isset($_POST['status'])) {
			$ids = json_decode($_POST['id'], true);

			foreach ($ids as $id) {
				if ($_POST['status'] == "archive") {
					$this->db->query("update user set old_status=status,status=5 where id=$id and status!=5");
				} else if ($_POST['status'] == "unarchive") {
					$this->db->query("update user set status=@s:=status,status=old_status,old_status=@s where id=$id");
				}
			}
		}
		echo $this->db->affected_rows();
	}

	public function get_email_temp()
	{
		if ($_POST['id']) {
			//$EMAIL_TEMPLATE = $this->Common_mdl->getTemplates($_POST['id']);
			$EMAIL_TEMPLATE     = $this->db->get_where("email_templates", "id=" . $_POST['id'])->row_array();

			echo json_encode($EMAIL_TEMPLATE);
		}
	}

	/* User List Page Send Sms Section */
	public function send_mail_sms()
	{
		if (isset($_POST) && $_POST['status'] == "get") {
			$mail_ids = $this->db->query("select id,main_email,work_email from  client_contacts where make_primary=1 and client_id  IN ($_POST[ids]) and main_email!=''")->result_array();
			$arr 	  = array();
			foreach ($mail_ids as $v) {
				$arr[$v['id']] = $v['main_email'];
				if( !empty( $v['work_email'] ) ){
					$work_emails  = json_decode( $v['work_email'] , true );
					foreach($work_emails as $key => $work_email){
						if($work_email){
							$arr['work-'.$v['id'].'-'.$key] = $work_email;
						}
					}
				}
			}
			echo json_encode($arr);
		} else if (isset($_POST) &&  $_POST['status'] == "send") {
			if(isset($_POST['contact_id'])){
				$contact_id = $_POST['contact_id'];
				$query    	= "SELECT c.id,cc.main_email,work_email 
							   FROM client_contacts as cc 
							   INNER JOIN client as c on c.user_id=cc.client_id
							   WHERE cc.id=" . $contact_id;
				$client 	= $this->db->query($query)->row_array();
				$all_emails = [];

				if(!isset($_POST['acc_mail_id'])){
					$all_emails[] = $client["main_email"];
					if( !empty( $client['work_email'] ) )
					{
						$work_email = json_decode( $client['work_email'] , true );
						$all_emails = array_merge( $all_emails , $work_email );
					}	
					$refined_emails 		= [];
					$refined_emails 		= $all_emails;
					$welcome_email_template = WELCOME_EMAIL_TEMPLATE_ID;
					$email_temp 			= $this->db->query("SELECT * FROM `email_templates` WHERE `id` = ".$welcome_email_template)->row_array();
					$decode['subject'] 		= html_entity_decode($email_temp['subject']);
					$decode['body']    		= html_entity_decode($email_temp['body']);
				}
				else{
					$refined_emails    = [];
					$refined_emails[]  = $_POST['acc_mail_id'];
					$decode['subject'] = html_entity_decode($_POST['sub']);
					$decode['body']    = html_entity_decode($_POST['msg']);
				}
				$decoded        = Decode_ClientTable_Datas($client['id'], $decode);
				$send_to['to']	= $refined_emails;
				$send_to['cc']	= '';
				$send_to['bcc']	= '';
				$data = [
					'send_from'            =>  $_SESSION['firm_id'],
					'send_to'              =>  json_encode($send_to),
					'subject'              =>  $decoded['subject'],
					'content'              =>  $decoded['body'],
					'status'               =>  1,
					'relational_module'    =>  'client',
					'relational_module_id' =>  $client['id'],
					'created_at'           =>  date('Y-m-d H:i:s')
				];
				$this->db->insert( "email" , $data );
				
				$final_emails = [];
				if(is_array($refined_emails)){
					foreach($refined_emails as $r_emails){
						$final_emails['to'][] = $r_emails;
					}
				}else{
					$final_emails = $refined_emails;
				}
				$Issend = firm_settings_send_mail($_SESSION['firm_id'], $final_emails, $decoded['subject'], $decoded['body']);

				if ($Issend['result'] == 1) {
					$log 	 = strip_tags($decoded['subject']) . "- Email send";
					$user_id = $this->Common_mdl->get_field_value('client', 'user_id', 'id', $client['id']);
					$this->Common_mdl->Create_Log('client', $user_id, $log);
				}
			}
			else{
				$mail_ids 		 = explode(',', $_POST['mail_ids']);
				$mail_ids 		 = array_unique($mail_ids);
				$mail_id_new_arr = [];
				$custom_mail_ids = [];
				$lead_mail_ids 	 = [];
				$all_mails 		 = [];
				$bcc_all_mails   = [];

				if(isset($_POST['all_mails'])){
					$all_mails = $_POST['all_mails'];
				}
				if(isset($_POST['bcc_all_mails'])){
					$bcc_all_mails = $_POST['bcc_all_mails'];
				}
				foreach($mail_ids as $mail_key => $id){
					if ((strpos($id, 'work') === false)&&(strpos($id, 'cus') === false)&&(strpos($id, 'lead') === false)) {
						$mail_id_new_arr[] = $id;
					}else if(strpos($id, 'cus') !== false){
						$custom_mail_ids[] = $all_mails[$mail_key];
					}else if(strpos($id, 'lead') !== false){
						$lead_mail_ids[] = $all_mails[$mail_key];
					}			
				}
				$this->load->helper('mailer');

				if( $mail_id_new_arr ){
					foreach ($mail_id_new_arr as $mail_key => $va) {
						$query    	  = "SELECT c.id,cc.main_email,work_email FROM client_contacts as cc 
										 INNER JOIN client as c on c.user_id=cc.client_id
										 WHERE cc.id=" . $va;		
						$client 	  = $this->db->query($query)->row_array();
						$all_emails   = [];
						$all_emails[] = $client["main_email"];

						if( !empty( $client['work_email'] ) ){
							$work_email = json_decode( $client['work_email'] , true );
							$all_emails = array_merge( $all_emails , $work_email );
						}	
						$refined_emails = [];

						if($all_mails){
							foreach($all_emails as $email){
								if(in_array($email, $all_mails)){
									$refined_emails[] = $email;
								}
							}
							if($custom_mail_ids){
								$refined_emails  = array_merge( $refined_emails , $custom_mail_ids );
								$custom_mail_ids = [];
							}
						}else{
							$refined_emails = $all_emails;
						}
						$refined_bcc_emails = [];
						if($bcc_all_mails){
							foreach($bcc_all_mails as $email){
								$refined_bcc_emails[] = $email;
							}
						}else{
							$refined_bcc_emails = [];
						}
						$decode['subject'] 	= html_entity_decode($_POST['sub']);
						$decode['body']    	= html_entity_decode($_POST['msg']);
						$decoded           	= Decode_ClientTable_Datas($client['id'], $decode);
						$send_to['to']		= $refined_emails;
						$send_to['cc']		= '';
						$send_to['bcc']		= $refined_bcc_emails;
						$data = [
							'send_from'             =>  $_SESSION['firm_id'],
							'send_to'               =>  json_encode($send_to),
							'subject'               =>  $decoded['subject'],
							'content'               =>  $decoded['body'],
							'status'                =>  1,
							'relational_module'     =>  'client',
							'relational_module_id'  =>  $client['id'],
							'created_at'            =>  date('Y-m-d H:i:s')
						];		
						$this->db->insert( "email" , $data );						
						$final_emails = [];

						if(is_array($refined_emails)){
							foreach($refined_emails as $r_emails){
								$final_emails['to'][] = $r_emails;
							}
							foreach($refined_bcc_emails as $b_emails){
								$final_emails['bcc'][] = $b_emails;
							}
						}else{
							$final_emails = $refined_emails;
						}		
						$Issend = firm_settings_send_mail($_SESSION['firm_id'], $final_emails, $decoded['subject'], $decoded['body']);
		
						if ($Issend['result'] == 1) {
							$log 	 = strip_tags($decoded['subject']) . "- Email send";
							$user_id = $this->Common_mdl->get_field_value('client', 'user_id', 'id', $client['id']);
							$this->Common_mdl->Create_Log('client', $user_id, $log);
						}
					}
				}else if($lead_mail_ids){
					foreach($lead_mail_ids as $lead_mail){
						$refined_emails 	= [];
						$refined_bcc_emails = [];
						$refined_emails[] 	= $lead_mail;

						if($custom_mail_ids){
							foreach($custom_mail_ids as $email){
								$refined_emails[] = $email;
							}
						}
						if($bcc_all_mails){
							foreach($bcc_all_mails as $email){
								$refined_bcc_emails[] = $email;
							}
						}
						$query    			= "SELECT id FROM leads WHERE email_address='".$lead_mail."'";		
						$lead 				= $this->db->query($query)->row_array();
						$decode['subject'] 	= html_entity_decode($_POST['sub']);
						$decode['body']    	= html_entity_decode($_POST['msg']);
						$decoded           	= Decode_LeadTable_Datas($lead['id'], $decode);
						$send_to['to']		= $refined_emails;
						$send_to['cc']		= '';
						$send_to['bcc']		= $refined_bcc_emails;
						$data = [
							'send_from'            =>  $_SESSION['firm_id'],
							'send_to'              =>  json_encode($send_to),
							'subject'              =>  $decoded['subject'],
							'content'              =>  $decoded['body'],
							'status'               =>  1,
							'relational_module'    =>  'leads',
							'relational_module_id' =>  $lead['id'],
							'created_at'           =>  date('Y-m-d H:i:s')
						];
						$this->db->insert( "email" , $data );
						$final_emails = [];

						if(is_array($refined_emails)){
							foreach($refined_emails as $r_emails){
								$final_emails['to'][] = $r_emails;
							}
							foreach($refined_bcc_emails as $b_emails){
								$final_emails['bcc'][] = $b_emails;
							}
						}else{
							$final_emails = $refined_emails;
						}
						$Issend = firm_settings_send_mail($_SESSION['firm_id'], $final_emails, $decoded['subject'], $decoded['body']);

						if ($Issend['result'] == 1) {
							$log = strip_tags($decoded['subject']) . "- Email send";
						}
					}
				}
			}
			echo $Issend['result'];
		}
	}
	//sugan
	public function archive_update()
	{
		if (isset($_POST['id'])) {
			if ($_POST['status'] == '4') {
				$ids = json_decode($_POST['id'], true);
				foreach ($ids as $id) {
					$this->db->query("update add_new_task set task_old_status=task_status,task_status='" . $_POST['status'] . "' where id=$id");
					$this->task_statusChange($id, $_POST['status']);
				}
			} else {
				$ids = json_decode($_POST['id'], true);
				foreach ($ids as $id) {
					$select = $this->Common_mdl->select_record('add_new_task', 'id', $id);
					if ($select['task_old_status'] != '') {
						$data['task_status'] 	 = $select['task_old_status'];
						$data['task_old_status'] = '';
						
						$this->Common_mdl->update('add_new_task', $data, 'id', $id);
						$this->task_statusChange($id, $select['task_old_status']);						
					}
				}
			}
		}
		echo $this->db->affected_rows();
	}
	public function task_summary_data()
	{
		$this->Security_model->chk_login();
		//  $page_status=explode("/",$_SERVER['REQUEST_URI']);

		$full_url 		 = (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : $_SERVER['REQUEST_URI'];
		$full_url 		 = explode("/", $full_url);
		$full_url 		 = array_pop($full_url);
		$status   		 = ($full_url != '' && $full_url == 'task_list') ? '0' : $full_url;
		$ques 	  		 = Get_Assigned_Datas('TASK');
		$res 	  		 = (!empty($ques)) ? implode(',', array_filter(array_unique($ques))) : '-1';
		$data['ass_res'] = $res;
		$task_status_id  = array('1', '2', '3');
		$condition 		 = " AND tl.related_to != 'sub_task' AND  tl.task_status in (1,2,3) ";

		if ($status) {
			$status 		= ($status != '' && $status == 'archive_task') ? '4' : '5';
			$condition 		= " AND tl.task_status = " . $status . " ";
			$task_status_id = array($status);
		}
		$data['task_list'] 		 = [];
		$data['task_list_count'] = [];
		$data['get_datalist'] 	 = [];

		if ($res != '') {
			$data['task_list'] = $this->db->query("SELECT tl.id,tl.task_status FROM add_new_task AS tl 

                                               LEFT JOIN client AS cl ON cl.id=tl.company_name

                                                  where tl.id in ($res) and tl.firm_id = " . $_SESSION['firm_id'] . " " . $condition .  " order by tl.id desc")->result_array();
			$data['task_list_count'] = count($data['task_list']);
			$get_datalist = array_column($data['task_list'], 'task_status');
			$data['get_datalist'] = array_count_values($get_datalist);
		}
		$data['task_status'] = $this->db->query('SELECT `id`,`status_name` from task_status where id in (' . implode(",", $task_status_id) . ')')->result_array();

		$this->load->view('users/task_summary', $data);
	}
	public function statusChange()
	{
		$this->Security_model->chk_login();
		$firm_id = $this->session->userdata('firm_id');

		$id      = $this->input->post('rec_id');
		$status  = $this->input->post('status');
		$data    = array("status" => $status);
		$query   = $this->Common_mdl->update('user', $data, 'id', $id);
		$data    = array("crm_company_status" => $status);
		$query   = $this->Common_mdl->update('client', $data, 'user_id', $id);

		$getClientstatus = $this->Client_model->Get_Assigned_client($firm_id);

		$status = ['Active' => [1], 'Inactive' => [0, 2], 'Frozen' => [3], 'Archive' => [5]];

		$getClientstatus = array_column($getClientstatus, 'status');

		foreach ($status as $key => $value) {
			$status[$key] =  count(array_intersect($getClientstatus, $value));
		}

		$status['All'] = count($getClientstatus);

		echo json_encode($status);
	}
	public function deadline_management()
	{
		$this->Security_model->chk_login();
		$this->load->view('users/deadline-management');
	}
	public function passwrd_page()
	{
		$this->load->view('users/passwrd_page');
	}

	public function chat_page()
	{
		$this->load->view('users/chat_page');
	}
	public function settings_calender()
	{
		$this->load->view('users/settings_calender');
	}
	public function desk_dashboard()
	{
		$this->load->view('users/desk_dashboard');
	}
	public function deadline_calendar()
	{
		$this->load->view('users/deadline-calendar');
	}
	public function invoice()
	{
		$this->load->view('users/invoice_view');
	}
	public function proposal_template()
	{
		$this->load->view('users/proposal_parts');
	}
	public function print_page()
	{
		$this->load->view('users/print_page');
	}
	public function repeat_invoice()
	{
		$this->load->view('users/repeat-invoice');
	}

	public function leads()
	{
		$this->Security_model->chk_login();
		$data['getallUser'] = $this->db->query("SELECT * FROM user where role!='1' AND autosave_status!='1' order by id DESC")->result_array();
		$this->load->view('users/leads_view');
	}

	public function lead_details()
	{
		$this->Security_model->chk_login();
		$data['getallUser'] = $this->db->query("SELECT * FROM user where role!='1' AND autosave_status!='1' order by id DESC")->result_array();
		$this->load->view('users/lead_details_view');
	}

	public function task_summary()
	{
		$this->Security_model->chk_login();
		$data['getallUser'] = $this->db->query("SELECT * FROM user where role!='1' AND autosave_status!='1' order by id DESC")->result_array();
		$this->load->view('users/task_summary_view');
	}

	public function individual_task()
	{
		$this->Security_model->chk_login();
		$data['getallUser'] = $this->db->query("SELECT * FROM user where role!='1' AND autosave_status!='1' order by id DESC")->result_array();
		$this->load->view('users/individual_task_view');
	}

	public function section_setting()
	{
		$this->Security_model->chk_login();
		$data['getallUser'] = $this->db->query("SELECT * FROM user where role!='1' AND autosave_status!='1' order by id DESC")->result_array();
		$data['section_settings'] = $this->Common_mdl->select_record('section_settings', 'user_id', $_SESSION['id']);

		$this->load->view('users/section_setting_view', $data);
	}

	public function notification()
	{
		$this->Security_model->chk_login();

		$data['section_settings'] = $this->Common_mdl->select_record('section_settings', 'user_id', $_SESSION['id']);

		$this->load->view('users/notification_view', $data);
		unset($_SESSION['success_msg']);
	}

	public function import_csv()
	{
		$this->Security_model->chk_login();
		$client_limit = $this->Common_mdl->Check_Firm_Clients_limit($_SESSION['firm_id']);
		if ($_SESSION['permission']['Client_Section']['create'] != 1) {
			$this->load->view('users/blank_page');
		} else if (!($client_limit > 0 || $client_limit == 'unlimited')) {
			$this->load->view('users/client_limit_exit');
		} else {
			$data['getallUser'] = $this->db->query("SELECT * FROM user where role!='1' AND autosave_status!='1' order by id DESC")->result_array();
			$this->load->view('users/import_csv_view');
		}
	}

	public function admin_leads()
	{
		$this->Security_model->chk_login();
		$data['getallUser'] = $this->db->query("SELECT * FROM user where role!='1' AND autosave_status!='1' order by id DESC")->result_array();
		$this->load->view('users/admin_leads_view');
	}

	public function add_new_proposal()
	{
		$this->Security_model->chk_login();
		$data['getallUser'] = $this->db->query("SELECT * FROM user where role!='1' AND autosave_status!='1' order by id DESC")->result_array();
		$this->load->view('users/add_new_proposal_view');
	}

	public function settings()
	{
		$this->Security_model->chk_login();
		$data['getallUser'] = $this->db->query("SELECT * FROM user where role!='1' AND autosave_status!='1' order by id DESC")->result_array();
		$this->load->view('users/settings_view');
	}

	public function add_new_project()
	{
		$this->Security_model->chk_login();
		$data['getallUser'] = $this->db->query("SELECT * FROM user where role!='1' AND autosave_status!='1' order by id DESC")->result_array();
		$this->load->view('users/add_new_project_view');
	}

	public function addnewtask1()
	{
		$this->Security_model->chk_login();
		$data['getallUser'] = $this->db->query("SELECT * FROM user where role!='1' AND autosave_status!='1' order by id DESC")->result_array();
		$this->load->view('users/addnewtask1_view');
	}


	public function suspentChange()
	{
		$id = $this->input->post('rec_id');
		$status = $this->input->post('status');
		$data = array("frozen_freeze" => $status);
		$query = $this->Common_mdl->update('user', $data, 'id', $id);
		return true;
	}
	public function client_list()
	{

		$this->Security_model->chk_login();
		$data['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user', 'role', '1');
		$data['client_list'] = $this->Common_mdl->getallrecords('client');
		$data['column_setting'] = $this->Common_mdl->getallrecords('column_setting');
		$this->load->view('clients/client_list', $data);
	}
	public function delete($id)
	{
		$this->Common_mdl->delete('user', 'id', $id);
		$this->Common_mdl->delete('client', 'user_id', $id);
		$this->Common_mdl->delete('timeline_services_notes_added', 'user_id', $id);
		redirect('user');
	}
	public function t_delete($id)
	{
		$username = $this->Common_mdl->getUserProfileName($_SESSION['id']);
		$activity_datas['log'] = "Task was Deleted  by ";
		$activity_datas['createdTime'] = time();
		$activity_datas['module'] = 'Task';
		$activity_datas['sub_module'] = '';
		$activity_datas['user_id'] = $_SESSION['id'];
		$activity_datas['module_id'] = $id;
		$this->Common_mdl->insert('activity_log', $activity_datas);
		$this->Task_invoice_model->task_new_mail($id, 'for_delete');
		$this->Common_mdl->delete('add_new_task', 'id', $id);
		redirect('user/task_list');
	}

	public function multiple_delete()
	{

		$data_ids = $_POST['data_ids'];
		$data_id_array = explode(",", $data_ids);
		if (!empty($data_id_array)) {
			foreach ($data_id_array as $id) {
				$this->Common_mdl->delete('user', 'id', $id);
				$this->Common_mdl->delete('client', 'user_id', $id);
				$this->Common_mdl->delete('timeline_services_notes_added', 'user_id', $id);
			}
			echo  $data_ids;
		}
	}

	public function multipleActiveDelete()
	{
		if (isset($_POST['data_id'])) {
			$id = trim($_POST['data_id']);
			$this->db->query("DELETE FROM user WHERE id in ($id)");
			$this->db->query("DELETE FROM client WHERE user_id in ($id)");
			$this->db->query("DELETE FROM timeline_services_notes_added WHERE user_id in ($id)");
			echo $id;
		}
	}

	public function task_delete()
	{
		$data_ids = $_POST['data_ids'];
		$data_id_array = explode(",", $data_ids);
		if (!empty($data_id_array)) {
			foreach ($data_id_array as $id) {
				$this->Task_invoice_model->task_new_mail($id, 'for_delete');
				$this->Common_mdl->delete('add_new_task', 'id', $id);
				$username = $this->Common_mdl->getUserProfileName($_SESSION['id']);
				$activity_datas['log'] = "Task was Deleted  by ";
				$activity_datas['createdTime'] = time();
				$activity_datas['module'] = 'Task';
				$activity_datas['sub_module'] = '';
				$activity_datas['user_id'] = $_SESSION['id'];
				$activity_datas['module_id'] = $id;
				$this->Common_mdl->insert('activity_log', $activity_datas);
			}
			echo  $data_ids;
		}
	}

	public function column_setting()
	{
		$this->Security_model->chk_login();
		$data['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user', 'role', '1');
		$data['client'] = $this->db->query('select * from column_setting where id=1')->row_array();
		$this->load->view('users/column_setting_view', $data);
	}
	public function column_setting_new()
	{
		error_reporting(0);
		$this->Security_model->chk_login();
		$data['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user', 'role', '1');
		$data['client'] = $this->Common_mdl->GetAllWithWhere('column_setting_new', 'id', 1);
		$data['responsible_team'] = $this->Common_mdl->GetAllWithWhere('responsible_team', 'client_id', $user_id);
		$data['responsible_user'] = $this->Common_mdl->GetAllWithWhere('responsible_user', 'client_id', $user_id);
		$data['responsible_department'] = $this->Common_mdl->GetAllWithWhere('responsible_department', 'client_id', $user_id);
		$data['responsible_member'] = $this->Common_mdl->GetAllWithWhere('responsible_members', 'client_id', $user_id);
		$data['contactRec'] = $this->db->query("SELECT * FROM  `client_contacts` WHERE client_id =  '" . $user_id . "' ORDER BY make_primary DESC ")->result_array();
		$data['user'] = $this->Common_mdl->GetAllWithWhere('user', 'id', $user_id);
		$data['referby'] = $this->db->query("SELECT * FROM `client` where crm_first_name!='' and crm_first_name!='0' GROUP BY `crm_first_name`")->result_array();
		$data['teamlist'] = $this->Common_mdl->getallrecords('team');
		$data['deptlist'] = $this->Common_mdl->getallrecords('department');
		$data['staff_form'] = $this->Common_mdl->GetAllWithWhere('user', 'role', '6');
		$data['managed_by'] = $this->Common_mdl->GetAllWithWhere('user', 'role', '5');
		if ($user_id != '') {
			$data['task_list'] = $this->Common_mdl->GetAllWithWhere('add_new_task', 'user_id', $user_id);
		} else {
			$data['task_list'] = $this->Common_mdl->getallrecords('add_new_task');
		}
		$this->load->view('users/column_setting_new', $data);
	}
	public function task_column_settings()
	{
		// if($_SESSION['permission']['Task_Column_Settings'][0]['view']=='1'){
		//$data['task']=$this->Common_mdl->select_record('task_column_settings','user_id',1); 

		$data['columns'] = $this->db->query("SELECT * FROM `task_column_settings` where firm_id=0 and status=1 order by order_no ASC
")->result_array();


		$this->load->view('users/task_column_settings_view', $data);
		//   }else{
		//      $this->load->view('users/blank_page');
		// }

	}
	public function column_update()
	{
		$this->Security_model->chk_login();
		if (isset($_POST)) {
			$data1['user_id'] = (isset($_POST['user_id']) && ($_POST['user_id'] == '')) ? '0' : $_POST['user_id'];
			// required information
			$data1['company_name'] = (isset($_POST['company_name']) && ($_POST['company_name'] != '')) ? $_POST['company_name'] : '0';
			$data1['legal_form'] = (isset($_POST['legal_form']) && ($_POST['legal_form'] != '')) ? $_POST['legal_form'] : '0';
			$data1['allocation_holder'] = (isset($_POST['allocation_holder']) && ($_POST['allocation_holder'] != '')) ? $_POST['allocation_holder'] : '0';
			// main contact
			$data1['person'] = (isset($_POST['person']) && ($_POST['person'] != '')) ? $_POST['person'] : '0';
			$data1['title'] = (isset($_POST['title']) && ($_POST['title'] != '')) ? $_POST['title'] : '0';
			$data1['first_name'] = (isset($_POST['first_name']) && ($_POST['first_name'] != '')) ? $_POST['first_name'] : '0';
			$data1['middle_name'] = (isset($_POST['middle_name']) && ($_POST['middle_name'] != '')) ? $_POST['middle_name'] : '0';
			$data1['last_name'] = (isset($_POST['last_name']) && ($_POST['last_name'] != '')) ? $_POST['last_name'] : '0';
			$data1['create_self_assessment_client'] = (isset($_POST['create_self_assessment_client']) && ($_POST['last_name'] != '')) ? $_POST['create_self_assessment_client'] : '0';
			$data1['client_does_self_assessment'] = (isset($_POST['client_does_self_assessment']) && ($_POST['client_does_self_assessment'] != '')) ? $_POST['client_does_self_assessment'] : '0';
			$data1['preferred_name'] = (isset($_POST['name']) && ($_POST['name'] != '')) ? $_POST['name'] : '0';
			$data1['date_of_birth'] = (isset($_POST['dob']) && ($_POST['dob'] != '')) ? $_POST['dob'] : '0';
			$data1['email'] = (isset($_POST['email_id']) && ($_POST['email_id'] != '')) ? $_POST['email_id'] : '0';
			$data1['postal_address'] = (isset($_POST['postal_address']) && ($_POST['postal_address'] != '')) ? $_POST['postal_address'] : '0';
			$data1['telephone_number'] = (isset($_POST['telephone_number']) && ($_POST['telephone_number'] != '')) ? $_POST['telephone_number'] : '0';
			$data1['mobile_number'] = (isset($_POST['mobile_number']) && ($_POST['mobile_number'] != '')) ? $_POST['mobile_number'] : '0';
			$data1['ni_number'] = (isset($_POST['ni_number']) && ($_POST['ni_number'] != '')) ? $_POST['ni_number'] : '0';
			$data1['personal_utr_number'] = (isset($_POST['personal_utr_number']) && ($_POST['personal_utr_number'] != '')) ? $_POST['personal_utr_number'] : '0';
			$data1['terms_signed'] = (isset($_POST['terms_signed']) && ($_POST['terms_signed'] != '')) ? $_POST['terms_signed'] : '0';
			$data1['photo_id_verified'] = (isset($_POST['photo_id_verified']) && ($_POST['photo_id_verified'] != '')) ? $_POST['photo_id_verified'] : '0';
			$data1['address_verified'] = (isset($_POST['address_verified']) && ($_POST['address_verified'] != '')) ? $_POST['address_verified'] : '0';
			// income details
			$data1['previous'] = (isset($_POST['previous']) && ($_POST['previous'] != '')) ? $_POST['previous'] : '0';
			$data1['current'] = (isset($_POST['current']) && ($_POST['current'] != '')) ? $_POST['current'] : '0';
			$data1['ir_35_notes'] = (isset($_POST['ir_35_notes']) && ($_POST['ir_35_notes'] != '')) ? $_POST['ir_35_notes'] : '0';
			// other details
			$data1['previous_accountant'] = (isset($_POST['previous_accountant']) && ($_POST['previous_accountant'] != '')) ? $_POST['previous_accountant'] : '0';
			$data1['refered_by'] = (isset($_POST['refered_by']) && ($_POST['refered_by'] != '')) ? $_POST['refered_by'] : '0';
			$data1['allocated_office'] = (isset($_POST['allocated_office']) && ($_POST['allocated_office'] != '')) ? $_POST['allocated_office'] : '0';
			$data1['inital_contact'] = (isset($_POST['inital_contact']) && ($_POST['inital_contact'] != '')) ? $_POST['inital_contact'] : '0';
			$data1['quote_email_sent'] = (isset($_POST['quote_email_sent']) && ($_POST['quote_email_sent'] != '')) ? $_POST['quote_email_sent'] : '0';
			$data1['welcome_email_sent'] = (isset($_POST['welcome_email_sent']) && ($_POST['welcome_email_sent'] != '')) ? $_POST['welcome_email_sent'] : '0';
			$data1['internal_reference'] = (isset($_POST['internal_reference']) && ($_POST['internal_reference'] != '')) ? $_POST['internal_reference'] : '0';
			$data1['profession'] = (isset($_POST['profession']) && ($_POST['profession'] != '')) ? $_POST['profession'] : '0';
			$data1['website'] = (isset($_POST['website']) && ($_POST['website'] != '')) ? $_POST['website'] : '0';
			$data1['accounting_system'] = (isset($_POST['accounting_system']) && ($_POST['accounting_system'] != '')) ? $_POST['accounting_system'] : '0';
			$data1['notes'] = (isset($_POST['notes']) && ($_POST['notes'] != '')) ? $_POST['notes'] : '0';
			//serices required
			$data1['accounts'] = (isset($_POST['accounts']) && ($_POST['accounts'] != '')) ? $_POST['accounts'] : '0';
			$data1['bookkeeping'] = (isset($_POST['bookkeeping']) && ($_POST['bookkeeping'] != '')) ? $_POST['bookkeeping'] : '0';
			$data1['ct600_return'] = (isset($_POST['ct600_return']) && ($_POST['ct600_return'] != '')) ? $_POST['ct600_return'] : '0';
			$data1['payroll'] = (isset($_POST['payroll']) && ($_POST['payroll'] != '')) ? $_POST['payroll'] : '0';
			$data1['auto_enrolment'] = (isset($_POST['auto_enrolment']) && ($_POST['auto_enrolment'] != '')) ? $_POST['auto_enrolment'] : '0';
			$data1['vat_returns'] = (isset($_POST['vat_returns']) && ($_POST['vat_returns'] != '')) ? $_POST['vat_returns'] : '0';
			$data1['confirmation_statement'] = (isset($_POST['confirmation_statement']) && ($_POST['confirmation_statement'] != '')) ? $_POST['confirmation_statement'] : '0';
			$data1['cis'] = (isset($_POST['cis']) && ($_POST['cis'] != '')) ? $_POST['cis'] : '0';
			$data1['p11d'] = (isset($_POST['p11d']) && ($_POST['p11d'] != '')) ? $_POST['p11d'] : '0';
			$data1['invesitgation_insurance'] = (isset($_POST['invesitgation_insurance']) && ($_POST['invesitgation_insurance'] != '')) ? $_POST['invesitgation_insurance'] : '0';
			$data1['services_registered_address'] = (isset($_POST['services_registered_address']) && ($_POST['services_registered_address'] != '')) ? $_POST['services_registered_address'] : '0';
			$data1['bill_payment'] = (isset($_POST['bill_payment']) && ($_POST['bill_payment'] != '')) ? $_POST['bill_payment'] : '0';
			$data1['advice'] = (isset($_POST['advice']) && ($_POST['advice'] != '')) ? $_POST['advice'] : '0';
			$data1['monthly_charge'] = (isset($_POST['monthly_charge']) && ($_POST['monthly_charge'] != '')) ? $_POST['monthly_charge'] : '0';
			$data1['annual_charge'] = (isset($_POST['annual_charge']) && ($_POST['annual_charge'] != '')) ? $_POST['annual_charge'] : '0';

			// accounts and return details
			$data1['accounts_periodend'] = (isset($_POST['accounts_periodend']) && ($_POST['accounts_periodend'] != '')) ? $_POST['accounts_periodend'] : '0';
			$data1['ch_yearend'] = (isset($_POST['period_end_on']) && ($_POST['period_end_on'] != '')) ? $_POST['period_end_on'] : '0';
			$data1['hmrc_yearend'] = (isset($_POST['next_made_up_to']) && ($_POST['next_made_up_to'] != '')) ? $_POST['next_made_up_to'] : '0';
			$data1['ch_accounts_next_due'] = (isset($_POST['next_due']) && ($_POST['next_due'] != '')) ? $_POST['next_due'] : '0';
			$data1['ct600_due'] = (isset($_POST['ct600_due']) && ($_POST['ct600_due'] != '')) ? $_POST['ct600_due'] : '0';
			$data1['companies_house_email_remainder'] = (isset($_POST['companies_house_email_remainder']) && ($_POST['companies_house_email_remainder'] != '')) ? $_POST['companies_house_email_remainder'] : '0';
			$data1['latest_action'] = (isset($_POST['latest_action']) && ($_POST['latest_action'] != '')) ? $_POST['latest_action'] : '0';
			$data1['latest_action_date'] = (isset($_POST['latest_action_date']) && ($_POST['latest_action_date'] != '')) ? $_POST['latest_action_date'] : '0';
			$data1['records_received_date'] = (isset($_POST['records_received_date']) && ($_POST['records_received_date'] != '')) ? $_POST['records_received_date'] : '0';
			//confirmation statements
			$data1['confirmation_statement_date'] = (isset($_POST['confirm_next_made_up_to']) && ($_POST['confirm_next_made_up_to'] != '')) ? $_POST['confirm_next_made_up_to'] : '0';
			$data1['confirmation_statement_due_date'] = (isset($_POST['confirm_next_due']) && ($_POST['confirm_next_due'] != '')) ? $_POST['confirm_next_due'] : '0';
			$data1['confirmation_latest_action'] = (isset($_POST['confirmation_latest_action']) && ($_POST['confirmation_latest_action'] != '')) ? $_POST['confirmation_latest_action'] : '0';
			$data1['confirmation_latest_action_date'] = (isset($_POST['confirmation_latest_action_date']) && ($_POST['confirmation_latest_action_date'] != '')) ? $_POST['confirmation_latest_action_date'] : '0';
			$data1['confirmation_records_received_date'] = (isset($_POST['confirmation_records_received_date']) && ($_POST['confirmation_records_received_date'] != '')) ? $_POST['confirmation_records_received_date'] : '0';
			$data1['confirmation_officers'] = (isset($_POST['confirmation_officers']) && ($_POST['confirmation_officers'] != '')) ? $_POST['confirmation_officers'] : '0';
			$data1['share_capital'] = (isset($_POST['share_capital']) && ($_POST['share_capital'] != '')) ? $_POST['share_capital'] : '0';
			$data1['shareholders'] = (isset($_POST['shareholders']) && ($_POST['shareholders'] != '')) ? $_POST['shareholders'] : '0';
			$data1['people_with_significant_control'] = (isset($_POST['people_with_significant_control']) && ($_POST['people_with_significant_control'] != '')) ? $_POST['people_with_significant_control'] : '0';
			// vat details
			$data1['vat_quarters'] = (isset($_POST['vat_quarters']) && ($_POST['vat_quarters'] != '')) ? $_POST['vat_quarters'] : '0';
			$data1['vat_quater_end_date'] = (isset($_POST['vat_quater_end_date']) && ($_POST['vat_quater_end_date'] != '')) ? $_POST['vat_quater_end_date'] : '0';
			$data1['next_return_due_date'] = (isset($_POST['next_return_due_date']) && ($_POST['next_return_due_date'] != '')) ? $_POST['next_return_due_date'] : '0';
			$data1['vat_latest_action'] = (isset($_POST['vat_latest_action']) && ($_POST['vat_latest_action'] != '')) ? $_POST['vat_latest_action'] : '0';
			$data1['vat_latest_action_date'] = (isset($_POST['vat_latest_action_date']) && ($_POST['vat_latest_action_date'] != '')) ? $_POST['vat_latest_action_date'] : '0';
			$data1['vat_records_received_date'] = (isset($_POST['vat_records_received_date']) && ($_POST['vat_records_received_date'] != '')) ? $_POST['vat_records_received_date'] : '0';
			$data1['vat_member_state'] = (isset($_POST['vat_member_state']) && ($_POST['vat_member_state'] != '')) ? $_POST['vat_member_state'] : '0';
			$data1['vat_number'] = (isset($_POST['vat_number']) && ($_POST['vat_number'] != '')) ? $_POST['vat_number'] : '0';
			$data1['vat_date_of_registration'] = (isset($_POST['vat_date_of_registration']) && ($_POST['vat_date_of_registration'] != '')) ? $_POST['vat_date_of_registration'] : '0';
			$data1['vat_effective_date'] = (isset($_POST['vat_effective_date']) && ($_POST['vat_effective_date'] != '')) ? $_POST['vat_effective_date'] : '0';
			$data1['vat_estimated_turnover'] = (isset($_POST['vat_estimated_turnover']) && ($_POST['vat_estimated_turnover'] != '')) ? $_POST['vat_estimated_turnover'] : '0';
			$data1['transfer_of_going_concern'] = (isset($_POST['transfer_of_going_concern']) && ($_POST['transfer_of_going_concern'] != '')) ? $_POST['transfer_of_going_concern'] : '0';
			$data1['involved_in_any_other_business'] = (isset($_POST['involved_in_any_other_business']) && ($_POST['involved_in_any_other_business'] != '')) ? $_POST['involved_in_any_other_business'] : '0';
			$data1['flat_rate'] = (isset($_POST['flat_rate']) && ($_POST['flat_rate'] != '')) ? $_POST['flat_rate'] : '0';
			$data1['direct_debit'] = (isset($_POST['direct_debit']) && ($_POST['direct_debit'] != '')) ? $_POST['direct_debit'] : '0';
			$data1['annual_accounting_scheme'] = (isset($_POST['annual_accounting_scheme']) && ($_POST['annual_accounting_scheme'] != '')) ? $_POST['annual_accounting_scheme'] : '0';
			$data1['flat_rate_category'] = (isset($_POST['flat_rate_category']) && ($_POST['flat_rate_category'] != '')) ? $_POST['flat_rate_category'] : '0';
			$data1['month_of_last_quarter_submitted'] = (isset($_POST['month_of_last_quarter_submitted']) && ($_POST['month_of_last_quarter_submitted'] != '')) ? $_POST['month_of_last_quarter_submitted'] : '0';
			$data1['box5_of_last_quarter_submitted'] = (isset($_POST['box5_of_last_quarter_submitted']) && ($_POST['box5_of_last_quarter_submitted'] != '')) ? $_POST['box5_of_last_quarter_submitted'] : '0';
			$data1['vat_address'] = (isset($_POST['vat_address']) && ($_POST['vat_address'] != '')) ? $_POST['vat_address'] : '0';
			$data1['vat_notes'] = (isset($_POST['vat_notes']) && ($_POST['vat_notes'] != '')) ? $_POST['vat_notes'] : '0';
			//paye details
			$data1['employers_reference'] = (isset($_POST['employers_reference']) && ($_POST['employers_reference'] != '')) ? $_POST['employers_reference'] : '0';
			$data1['accounts_office_reference'] = (isset($_POST['accounts_office_reference']) && ($_POST['accounts_office_reference'] != '')) ? $_POST['accounts_office_reference'] : '0';
			$data1['years_required'] = (isset($_POST['years_required']) && ($_POST['years_required'] != '')) ? $_POST['years_required'] : '0';
			$data1['annual_or_monthly_submissions'] = (isset($_POST['annual_or_monthly_submissions']) && ($_POST['annual_or_monthly_submissions'] != '')) ? $_POST['annual_or_monthly_submissions'] : '0';
			$data1['irregular_montly_pay'] = (isset($_POST['irregular_montly_pay']) && ($_POST['irregular_montly_pay'] != '')) ? $_POST['irregular_montly_pay'] : '0';
			$data1['nil_eps'] = (isset($_POST['nil_eps']) && ($_POST['nil_eps'] != '')) ? $_POST['nil_eps'] : '0';
			$data1['no_of_employees'] = (isset($_POST['no_of_employees']) && ($_POST['no_of_employees'] != '')) ? $_POST['vat_number'] : '0';
			$data1['first_pay_date'] = (isset($_POST['first_pay_date']) && ($_POST['first_pay_date'] != '')) ? $_POST['first_pay_date'] : '0';
			$data1['rti_deadline'] = (isset($_POST['rti_deadline']) && ($_POST['rti_deadline'] != '')) ? $_POST['rti_deadline'] : '0';
			$data1['paye_scheme_ceased'] = (isset($_POST['paye_scheme_ceased']) && ($_POST['paye_scheme_ceased'] != '')) ? $_POST['paye_scheme_ceased'] : '0';
			$data1['paye_latest_action'] = (isset($_POST['paye_latest_action']) && ($_POST['paye_latest_action'] != '')) ? $_POST['paye_latest_action'] : '0';
			$data1['paye_latest_action_date'] = (isset($_POST['paye_latest_action_date']) && ($_POST['paye_latest_action_date'] != '')) ? $_POST['paye_latest_action_date'] : '0';
			$data1['paye_records_received'] = (isset($_POST['paye_records_received']) && ($_POST['paye_records_received'] != '')) ? $_POST['paye_records_received'] : '0';
			$data1['next_p11d_return_due'] = (isset($_POST['next_p11d_return_due']) && ($_POST['next_p11d_return_due'] != '')) ? $_POST['next_p11d_return_due'] : '0';
			$data1['latest_p11d_submitted'] = (isset($_POST['latest_p11d_submitted']) && ($_POST['latest_p11d_submitted'] != '')) ? $_POST['latest_p11d_submitted'] : '0';
			$data1['p11d_latest_action'] = (isset($_POST['p11d_latest_action']) && ($_POST['p11d_latest_action'] != '')) ? $_POST['p11d_latest_action'] : '0';
			$data1['p11d_latest_action_date'] = (isset($_POST['p11d_latest_action_date']) && ($_POST['p11d_latest_action_date'] != '')) ? $_POST['p11d_latest_action_date'] : '0';
			$data1['p11d_records_received'] = (isset($_POST['p11d_records_received']) && ($_POST['p11d_records_received'] != '')) ? $_POST['p11d_records_received'] : '0';
			$data1['cis_contractor'] = (isset($_POST['cis_contractor']) && ($_POST['cis_contractor'] != '')) ? $_POST['cis_contractor'] : '0';
			$data1['cis_subcontractor'] = (isset($_POST['cis_subcontractor']) && ($_POST['cis_subcontractor'] != '')) ? $_POST['cis_subcontractor'] : '0';
			$data1['cis_deadline'] = (isset($_POST['cis_deadline']) && ($_POST['cis_deadline'] != '')) ? $_POST['cis_deadline'] : '0';
			$data1['auto_enrolment_latest_action'] = (isset($_POST['auto_enrolment_latest_action']) && ($_POST['auto_enrolment_latest_action'] != '')) ? $_POST['auto_enrolment_latest_action'] : '0';
			$data1['auto_enrolment_latest_action_date'] = (isset($_POST['auto_enrolment_latest_action_date']) && ($_POST['auto_enrolment_latest_action_date'] != '')) ? $_POST['auto_enrolment_latest_action_date'] : '0';
			$data1['auto_enrolment_records_received'] = (isset($_POST['auto_enrolment_records_received']) && ($_POST['auto_enrolment_records_received'] != '')) ? $_POST['auto_enrolment_records_received'] : '0';
			$data1['auto_enrolment_staging'] = (isset($_POST['auto_enrolment_staging']) && ($_POST['auto_enrolment_staging'] != '')) ? $_POST['auto_enrolment_staging'] : '0';
			$data1['postponement_date'] = (isset($_POST['postponement_date']) && ($_POST['postponement_date'] != '')) ? $_POST['postponement_date'] : '0';
			$data1['the_pensions_regulator_opt_out_date'] = (isset($_POST['the_pensions_regulator_opt_out_date']) && ($_POST['the_pensions_regulator_opt_out_date'] != '')) ? $_POST['the_pensions_regulator_opt_out_date'] : '0';
			$data1['re_enrolment_date'] = (isset($_POST['re_enrolment_date']) && ($_POST['re_enrolment_date'] != '')) ? $_POST['re_enrolment_date'] : '0';
			$data1['pension_provider'] = (isset($_POST['pension_provider']) && ($_POST['pension_provider'] != '')) ? $_POST['pension_provider'] : '0';
			$data1['paye_re_enrolment_date'] = (isset($_POST['paye_re_enrolment_date']) && ($_POST['paye_re_enrolment_date'] != '')) ? $_POST['paye_re_enrolment_date'] : '0';
			$data1['paye_pension_provider'] = (isset($_POST['paye_pension_provider']) && ($_POST['paye_pension_provider'] != '')) ? $_POST['paye_pension_provider'] : '0';
			$data1['pension_id'] = (isset($_POST['pension_id']) && ($_POST['pension_id'] != '')) ? $_POST['pension_id'] : '0';
			$data1['declaration_of_compliance_due_date'] = (isset($_POST['declaration_of_compliance_due_date']) && ($_POST['declaration_of_compliance_due_date'] != '')) ? $_POST['declaration_of_compliance_due_date'] : '0';
			$data1['declaration_of_compliance_submission'] = (isset($_POST['declaration_of_compliance_submission']) && ($_POST['declaration_of_compliance_submission'] != '')) ? $_POST['declaration_of_compliance_submission'] : '0';
			$data1['pension_deadline'] = (isset($_POST['pension_deadline']) && ($_POST['pension_deadline'] != '')) ? $_POST['pension_deadline'] : '0';
			$data1['note'] = (isset($_POST['note']) && ($_POST['note'] != '')) ? $_POST['note'] : '0';
			//registration
			$data1['registration_fee_paid'] = (isset($_POST['registration_fee_paid']) && ($_POST['crm_registration_fee_paid'] != '')) ? $_POST['registration_fee_paid'] : '0';
			$data1['64_8_registration'] = (isset($_POST['64_8_registration']) && ($_POST['64_8_registration'] != '')) ? $_POST['64_8_registration'] : '0';
			// company details
			$data1['company_number'] = (isset($_POST['company_number']) && ($_POST['company_number'] != '')) ? $_POST['company_number'] : '0';
			$data1['company_url'] = (isset($_POST['company_url']) && ($_POST['company_url'] != '')) ? $_POST['company_url'] : '0';
			$data1['officers_url'] = (isset($_POST['officers_url']) && ($_POST['officers_url'] != '')) ? $_POST['officers_url'] : '0';
			$data1['company_name1'] = (isset($_POST['company_name1']) && ($_POST['company_name1'] != '')) ? $_POST['company_name1'] : '0';
			$data1['register_address'] = (isset($_POST['register_address']) && ($_POST['register_address'] != '')) ? $_POST['register_address'] : '0';
			$data1['company_status'] = (isset($_POST['company_status']) && ($_POST['company_status'] != '')) ? $_POST['company_status'] : '0';
			$data1['company_type'] = (isset($_POST['company_type']) && ($_POST['company_type'] != '')) ? $_POST['company_type'] : '0';
			$data1['company_sic'] = (isset($_POST['company_sic']) && ($_POST['company_sic'] != '')) ? $_POST['company_sic'] : '0';
			$data1['sic_codes'] = (isset($_POST['sic_codes']) && ($_POST['sic_codes'] != '')) ? $_POST['sic_codes'] : '0';
			$data1['companies_house_authorisation_code'] = (isset($_POST['companies_house_authorisation_code']) && ($_POST['companies_house_authorisation_code'] != '')) ? $_POST['companies_house_authorisation_code'] : '0';
			$data1['company_utr'] = (isset($_POST['company_utr']) && ($_POST['company_utr'] != '')) ? $_POST['company_utr'] : '0';
			$data1['incorporation_date'] = (isset($_POST['date_of_creation']) && ($_POST['date_of_creation'] != '')) ? $_POST['date_of_creation'] : '0';
			$data1['managed'] = (isset($_POST['managed']) && ($_POST['managed'] != '')) ? $_POST['managed'] : '0';
			//Business details
			$data1['tradingas'] = (isset($_POST['tradingas']) && ($_POST['tradingas'] != '')) ? $_POST['tradingas'] : '0';
			$data1['commenced_trading'] = (isset($_POST['commenced_trading']) && ($_POST['commenced_trading'] != '')) ? $_POST['commenced_trading'] : '0';
			$data1['registered_for_sa'] = (isset($_POST['registered_for_sa']) && ($_POST['registered_for_sa'] != '')) ? $_POST['registered_for_sa'] : '0';
			$data1['business_details_turnover'] = (isset($_POST['business_details_turnover']) && ($_POST['business_details_turnover'] != '')) ? $_POST['business_details_turnover'] : '0';
			$data1['business_details_nature_of_business'] = (isset($_POST['business_details_nature_of_business']) && ($_POST['business_details_nature_of_business'] != '')) ? $_POST['business_details_nature_of_business'] : '0';

			$data1['packages'] = (isset($_POST['packages']) && ($_POST['packages'] != '')) ? $_POST['packages'] : '0';
			$data1['gender'] = (isset($_POST['gender']) && ($_POST['gender'] != '')) ? $_POST['gender'] : '0';
			$data1['profile_image'] = (isset($_POST['profile_image']) && ($_POST['profile_image'] != '')) ? $_POST['profile_image'] : '0';

			$data1['created_date'] = time();

			$update = $this->Common_mdl->update('column_setting', $data1, 'id', '1');
			echo $update;
		} else {
			echo '0';
		}
	}
	public function add_test()
	{
		$this->Security_model->chk_login();
		$data['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user', 'role', '1');
		$this->load->view('staffs/add_test_view', $data);
	}

	public function import()
	{
		$this->load->view('users/import_client');
	}
	public function upload_file()
	{

		$csvMimes = array('application/vnd.ms-excel', 'text/plain', 'text/csv', 'text/tsv');
		if (!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'], $csvMimes)) {

			if (is_uploaded_file($_FILES['file']['tmp_name'])) {

				$csvFile = fopen($_FILES['file']['tmp_name'], 'r');
				fgetcsv($csvFile);

				while (($line = fgetcsv($csvFile)) !== FALSE) {

					$var = rand(1, 10);

					$inser_data = $this->db->insert("user", array("crm_name" => $line[23], "crm_last_name" => $line[25], "username" => $line[28], "password" => md5($var), "confirm_password" => $var, "crm_email_id" => $line[30], "crm_phone_number" => $line[32], "crm_date_of_birth" => $line[29], "crm_gender" => $line[148], "crm_street1" => $line[149], "crm_street2" => $line[150], "crm_country" => $line[151], "crm_state" => $line[152], "crm_city" => $line[153], "crm_postal_code" => $line[31], "crm_packages" => $line[154], "crm_firm_name" => $line[1], "crm_firm_status" => $line[10], "crm_profile_pic" => $line[155], "role" => $line[156], "status" => 1, "frozen_freeze" => $line[158], "client_id" => rand(1, 5), "company_roles" => 2, "CreatedTime" => time(), "autosave_status" => 0, "firm_admin_id" => $_SESSION['id']));
					if ($inser_data) {
						$in = $this->db->insert_id();


						$insert_client = $this->db->insert("client", array("user_id" => $in, "crm_company_name" => $line[1], "crm_legal_form" => $line[2], "crm_allocation_holder" => $line[3], "crm_company_number" => $line[4], "crm_company_url" => $line[5], "crm_officers_url" => $line[6], "crm_company_name1" => $line[7], "crm_incorporation_date" => $line[8], "crm_register_address" => $line[9], "crm_company_status" => $line[10], "crm_company_type" => $line[11], "crm_company_sic" => $line[12], "crm_company_utr" => $line[13], "crm_companies_house_authorisation_code" => $line[14], "crm_managed" => $line[15], "crm_tradingas" => $line[16], "crm_commenced_trading" => $line[17], "crm_registered_for_sa" => $line[18], "crm_business_details_turnover" => $line[19], "crm_business_details_nature_of_business" => $line[20], "crm_person" => $line[21], "crm_title" => $line[22], "crm_first_name" => $line[23], "crm_middle_name" => $line[24], "crm_last_name" => $line[25], "crm_create_self_assessment_client" => $line[26], "crm_client_does_self_assessment" => $line[27], "crm_preferred_name" => $line[28], "crm_date_of_birth" => $line[29], "crm_email" => $line[30], "crm_postal_address" => $line[31], "crm_telephone_number" => $line[32], "crm_mobile_number" => $line[33], "crm_ni_number" => $line[34], "crm_personal_utr_number" => $line[35], "crm_terms_signed" => $line[36], "crm_photo_id_verified" => $line[37], "crm_address_verified" => $line[38], "crm_previous" => $line[39], "crm_current" => $line[40], "crm_ir_35_notes" => $line[41], "crm_previous_accountant" => $line[42], "crm_refered_by" => $line[43], "crm_allocated_office" => $line[44], "crm_inital_contact" => $line[45], "crm_quote_email_sent" => $line[46], "crm_welcome_email_sent" => $line[47], "crm_internal_reference" => $line[48], "crm_profession" => $line[49], "crm_website" => $line[50], "crm_accounting_system" => $line[51], "crm_notes" => $line[52], "crm_accounts" => $line[53], "crm_bookkeeping" => $line[54], "crm_ct600_return" => $line[55], "crm_payroll" => $line[56], "crm_auto_enrolment" => $line[57], "crm_vat_returns" => $line[58], "crm_confirmation_statement" => $line[59], "crm_cis" => $line[60], "crm_p11d" => $line[61], "crm_invesitgation_insurance" => $line[62], "crm_services_registered_address" => $line[63], "crm_bill_payment" => $line[64], "crm_advice" => $line[65], "crm_bill_payment" => $line[66], "crm_annual_charge" => $line[67], "crm_accounts_periodend" => $line[68], "crm_ch_yearend" => $line[69], "crm_hmrc_yearend" => $line[70], "crm_ch_accounts_next_due" => $line[71], "crm_ct600_due" => $line[72], "crm_companies_house_email_remainder" => $line[73], "crm_latest_action" => $line[74], "crm_latest_action_date" => $line[75], "crm_records_received_date" => $line[76], "crm_confirmation_statement_date" => $line[77], "crm_confirmation_statement_due_date" => $line[78], "crm_confirmation_latest_action" => $line[79], "crm_confirmation_latest_action_date" => $line[80], "crm_confirmation_records_received_date" => $line[81], "crm_confirmation_officers" => $line[82], "crm_share_capital" => $line[83], "crm_shareholders" => $line[84], "crm_people_with_significant_control" => $line[85], "crm_vat_quarters" => $line[86], "crm_vat_quater_end_date" => $line[87], "crm_next_return_due_date" => $line[88], "crm_vat_latest_action" => $line[89], "crm_vat_latest_action_date" => $line[90], "crm_vat_records_received_date" => $line[91], "crm_vat_member_state" => $line[92], "crm_vat_number" => $line[93], "crm_vat_date_of_registration" => $line[94], "crm_vat_effective_date" => $line[95], "crm_vat_estimated_turnover" => $line[96], "crm_transfer_of_going_concern" => $line[97], "crm_involved_in_any_other_business" => $line[98], "crm_flat_rate" => $line[99], "crm_direct_debit" => $line[100], "crm_annual_accounting_scheme" => $line[101], "crm_flat_rate_category" => $line[102], "crm_month_of_last_quarter_submitted" => $line[103], "crm_box5_of_last_quarter_submitted" => $line[104], "crm_vat_address" => $line[105], "crm_vat_notes" => $line[106], "crm_employers_reference" => $line[107], "crm_accounts_office_reference" => $line[108], "crm_years_required" => $line[109], "crm_annual_or_monthly_submissions" => $line[110], "crm_irregular_montly_pay" => $line[111], "crm_nil_eps" => $line[112], "crm_no_of_employees" => $line[113], "crm_first_pay_date" => $line[114], "crm_rti_deadline" => $line[115], "crm_paye_scheme_ceased" => $line[116], "crm_paye_latest_action" => $line[117], "crm_paye_latest_action_date" => $line[118], "crm_paye_records_received" => $line[119], "crm_next_p11d_return_due" => $line[120], "crm_latest_p11d_submitted" => $line[121], "crm_p11d_latest_action" => $line[122], "crm_p11d_latest_action_date" => $line[123], "crm_p11d_records_received" => $line[124], "crm_cis_contractor" => $line[125], "crm_cis_subcontractor" => $line[126], "crm_cis_deadline" => $line[127], "crm_auto_enrolment_latest_action" => $line[128], "crm_auto_enrolment_latest_action_date" => $line[129], "crm_auto_enrolment_records_received" => $line[130], "crm_auto_enrolment_staging" => $line[131], "crm_postponement_date" => $line[132], "crm_the_pensions_regulator_opt_out_date" => $line[133], "crm_re_enrolment_date" => $line[134], "crm_pension_provider" => $line[135], "crm_paye_re_enrolment_date" => $line[136], "crm_paye_pension_provider" => $line[137], "crm_pension_id" => $line[138], "crm_declaration_of_compliance_due_date" => $line[139], "crm_declaration_of_compliance_submission" => $line[140], "crm_pension_deadline" => $line[141], "crm_note" => $line[142], "crm_registration_fee_paid" => $line[143], "crm_64_8_registration" => $line[144], "status" => 2, "created_date" => time()));
					}
					$count[] = $this->db->affected_rows($insert_client);
				}

				fclose($csvFile);
				$c = (!empty($count) ? count($count) : '0');
				echo $c;
			} else {
				echo '0';
			}
		} else {
			echo '0';
		}
	}

	public function exportCSV()
	{
		// file name
		$filename = 'users_' . date('Ymd') . '.csv';

		header("Content-Description: File Transfer");
		header("Content-Disposition: attachment; filename=$filename");
		header("Content-Type: application/csv; ");
		// get data
		$usersData = $this->Common_mdl->getUserDetails();
		// file creation
		$file = fopen('php://output', 'w');
		$header = array("Client_ID", "company_name", "telephone_number", "email", "website", "date_of_birth");
		fputcsv($file, $header);
		foreach ($usersData as $key => $line) {
			fputcsv($file, $line);
		}

		fclose($file);
		exit;
	}

	function downloadFile()
	{
		$yourFile = "sampeCSVformat.csv";
		$file = @fopen($yourFile, "r");

		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename=Sample_CSV_Format.csv');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($yourFile));
		while (!feof($file)) {
			print(@fread($file, 1024 * 8));
			ob_flush();
			flush();
		}
	}

	public function view_staff($user_id = false)
	{
		$this->Security_model->chk_login();
		$data['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user', 'role', '1');
		$data['staff'] = $this->Common_mdl->GetAllWithWhere('user', 'id', $user_id);
		$data['staff_form'] = $this->Common_mdl->GetAllWithWhere('staff_form', 'user_id', $user_id);
		$this->load->view('users/staff_view', $data);
	}

	public function new_task($user_id = false)
	{
		if ($_SESSION['permission']['Task']['create'] == '1') {
			$data['timeline_task_section'] = $user_id;
			$data['noti_task_section'] = false;
			$this->Security_model->chk_login();
			$data['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user', 'role', '1');
			//  $data['staff'] = $this->Common_mdl->GetAllWithWhere('user','id',$user_id);
			//   $data['staff_form'] = $this->Common_mdl->GetAllWithWheretwo('user','role','6','firm_admin_id',$_SESSION['id']);
			//  $data['roles']=$this->db->query('SELECT * FROM `Role` where id not in(1,2,4)')->result_array();
			$data['tag'] = $this->Common_mdl->getallrecords('Tag');
			$data['task_status'] = $this->db->query('SELECT * from task_status where is_superadmin_owned=1')->result_array();
			// $data['manager'] = $this->Common_mdl->GetAllWithWheretwo('user','role','5','firm_admin_id',$_SESSION['id']);
			$this->load->view('users/new_task_view', $data);
		} else {
			$this->load->view('users/blank_page');
		}
	}

	public function new_tasks($noti_id = false)
	{
		if ($_SESSION['permission']['Task']['create'] == '1') {
			$data['timeline_task_section'] = false;
			$data['noti_task_section'] = $noti_id;
			$this->Security_model->chk_login();
			$data['task_status'] = $this->db->query('SELECT * from task_status where is_superadmin_owned=1')->result_array();
			$data['tag'] = $this->Common_mdl->getallrecords('Tag');
			$this->load->view('users/new_task_view', $data);
		} else {
			$this->load->view('users/blank_page');
		}
	}

	public function get_company_assign()
	{

		// $member = array( 'assignee' => array() , 'manager' => array() );
		//   if(isset($_POST['id']))
		//   {
		//     $company = $this->db->query("select user_id from client where id= ".$_POST['id']." ")->row_array();
		//     $user = $this->db->query("select * from responsible_user where client_id = ".$company['user_id']." ")->result_array();

		//     foreach($user as $arr) {
		//       array_push($member['assignee'],$arr['manager_reviewer']);
		//       array_push($member['manager'],$arr['assign_managed']);

		//     }
		//     $team = $this->db->query("select * from responsible_team where client_id = ".$company['user_id']." ")->result_array();
		//     foreach($team as $arr) array_push($member['assignee'],"tm_".$arr['team']);
		//     $dept = $this->db->query("select * from  responsible_department where client_id = ".$company['user_id']." ")->result_array();
		//     foreach($dept as $arr) array_push($member['assignee'],"de_".$arr['depart']);

		//     echo json_encode($member);
		$data['asssign_group'] = array();

		// print_r($_POST);
		$sql_val1 = $this->db->query("SELECT user_id FROM client WHERE id=" . $_POST['id'] . " ")->result_array();

		$id = (isset($sql_val1[0]['user_id'])) ? $_POST['id'] : '0';

		if (isset($_POST['id']) && isset($_POST['task_name'])) {

			$sql_val = $this->db->query("SELECT * FROM firm_assignees where module_id ='" . $id . "'  AND module_name='" . $_POST['task_name'] . "'")->result_array();

			//  print_r($_POST['id']);

			foreach ($sql_val as $key1 => $value1) {
				$data['asssign_group'][] = $value1['assignees'];
			}


			echo json_encode($data);
		}
	}
	public function update_task($task_id = false)
	{
		$this->Security_model->chk_login();
		// if($_SESSION['permission']['Task'][0]['edit']=='1'){
		$data['task_form'] = $this->Common_mdl->GetAllWithWheretwo('add_new_task', 'id', $task_id, 'firm_id', $_SESSION['firm_id']);
		if (!empty($data['task_form']) && $_SESSION['permission']['Task']['edit'] == '1') {
			$data['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user', 'role', '1');
			$data['staff_form'] = $this->Common_mdl->GetAllWithWheretwo('user', 'role', '6', 'firm_admin_id', $_SESSION['id']);
			$data['manager'] = $this->Common_mdl->GetAllWithWheretwo('user', 'role', '5', 'firm_admin_id', $_SESSION['id']);
			$data['task_form'] = $this->Common_mdl->GetAllWithWhere('add_new_task', 'id', $task_id);

			// $data['roles']=$this->db->query('SELECT * FROM `Role` where id not in(1,2,4)')->result_array();



			$sql_val = $this->db->query("SELECT * FROM firm_assignees where module_id = " . $task_id . " AND module_name='TASK'")->result_array();

			foreach ($sql_val as $key1 => $value1) {
				$data['asssign_group'][] = $value1['assignees'];
			}

			# code...

			$data['task_status'] = $this->db->query('SELECT * from task_status where is_superadmin_owned=1')->result_array();
			$data['tag'] = $this->Common_mdl->getallrecords('Tag');


			$this->load->view('users/update_task_view', $data);
		} else {
			redirect('user/task_list');
		}
	}

	public function save_info_chaser_to_timeline(){
		$decode['body']  = html_entity_decode($_POST['msg']);
		$decoded         = Decode_ClientTable_Datas($_POST['client_id'], $decode);
		$msg             = $decoded['body'];
		return $this->Common_mdl->Create_Log('Task', $_POST['task_id'], $msg, $_SESSION['userId']);
	}

	public function save_lead_notes_to_timeline(){
		return $this->Common_mdl->Create_Log('Lead', $_POST['lead_id'], $_POST['msg'], $_SESSION['userId']);
	}

	public function task_details($task_id = false){
		$data['task_form'] = $this->Common_mdl->GetAllWithWheretwo('add_new_task', 'id', $task_id, 'firm_id', $_SESSION['firm_id']);
		
		if (!empty($data['task_form']) &&   $_SESSION['permission']['Task']['view'] == 1) {
			$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

			$this->session->set_userdata('mail_url', $actual_link);
			$this->Security_model->chk_login();
			$this->session->unset_userdata($_SESSION['mail_url']);
			
			$data['userid']             = $this->session->userdata('id');
			$data['getallUser']         = $this->Common_mdl->GetAllWithExceptField('user', 'role', '1');
			$data['staff_form']         = $this->Common_mdl->GetAllWithWhere('user', 'role', '6');
			$data['staffs']             = $this->Common_mdl->GetAllWithWheretwo('user', 'role', '6', 'firm_admin_id', $_SESSION['id']);
			$data['manager']            = $this->Common_mdl->GetAllWithWheretwo('user', 'role', '5', 'firm_admin_id', $_SESSION['id']);
			$data['roles']              = $this->db->query('SELECT * FROM `Role` where id not in(1,2,4)')->result_array();
			$data['tag']                = $this->Common_mdl->getallrecords('Tag');
			$data['activity_log']       = $this->db->query("select * from activity_log where module='Task' and module_id=" . $task_id . " order by id desc")->result_array();
			$data['assign_task_status'] = $this->Common_mdl->dynamic_status('assign_task_status');
			$data['task_status']        = $this->db->query('SELECT * from task_status where is_superadmin_owned=1')->result_array();
			$data['progress_status']    = $this->Common_mdl->get_firm_progress();
			$userId                     = $_SESSION['userId'];
			$viewedDetails              = $this->db->query("SELECT id, viewed FROM task_comments WHERE task_id = $task_id")->result_array();
						
			foreach($viewedDetails as $vd){
				$id = $vd['id'];
				
				if($vd['viewed'] == ""){
					$sql = "UPDATE task_comments SET viewed = '$userId' WHERE task_id = $task_id AND id = $id";
					$this->db->query($sql);											
				}else{
					$details = explode(',',$vd['viewed']);									
					if(!in_array($userId, $details)){
						$sql = "UPDATE task_comments SET viewed = CONCAT(viewed, ',$userId') WHERE task_id = $task_id AND id = $id";
						$this->db->query($sql);		
					}
				}
			}						
			$this->load->view('users/task_details', $data);
		} else {
			redirect('user/task_list');
		}
	}

	public function downloadAttach($filename = null)
	{
		$data = file_get_contents(base_url('/uploads/' . $filename));
		force_download($filename, $data);
	}

	public function Change_TaskStatus()
	{
		$selectValue = $_POST['selectValue'];
		$task_id = $_POST['task_id'];
		$sql = $this->Common_mdl->UpdateStatus('add_new_task', 'id', $task_id, 'task_status', $selectValue);

		$query = $this->Common_mdl->select_record('add_new_task', 'id', $task_id);
		$sub_task_id = explode(',', $query['sub_task_id']);
		for ($i = 0; $i < count($sub_task_id); $i++) {
			$sql = $this->Common_mdl->UpdateStatus('add_new_task', 'id', $sub_task_id[$i], 'task_status', $selectValue);
		}
		$username = $this->Common_mdl->getUserProfileName($_SESSION['id']);
		$activity_datas['log'] = "Task Was updated  by ";
		$activity_datas['createdTime'] = time();
		$activity_datas['module'] = 'Task';
		$activity_datas['sub_module'] = '';
		$activity_datas['user_id'] = $_SESSION['id'];
		$activity_datas['module_id'] = $_POST['task_id'];
		$this->Common_mdl->insert('activity_log', $activity_datas);
		if ($selectValue == '5') {
			// echo "hai";
			$res_table = $this->db->query("select * from reminder_setting where FIND_IN_SET(" . $task_id . ",custom_task_created) ")->result_array();
			if (count($res_table) > 0) {
				$invoice_id = $this->Task_invoice_model->task_billable_createInvoice($task_id);
			}
		}
		if ($sql) {
			echo '1';
		} else {
			echo '0';
		}
	}

	public function task_AddComments()
	{
		$cmts = $_POST['cmt_val'];
		$Ses_ID = $_POST['Ses_ID'];
		$Ses_name = $_POST['Ses_name'];
		$data = array(
			'user_id' => $Ses_ID,
			'name' => $Ses_name,
			'message' => $cmts,
			'status' => 'Active',
			'created_at' => time()
		);
		$insert_data = $this->db->insert('task_comments', $data);
		if ($insert_data) {
			$ins = $this->db->insert_id();
			echo '1';
		} else {
			echo '0';
		}
	}

	public function search_companies()
	{
		$this->Security_model->chk_login();
		if (isset($_POST["query"])) {
			$output = '';

			$query = $this->db->query("SELECT * FROM client WHERE crm_company_name LIKE '%" . $_POST["query"] . "%' AND autosave_status=0");
			$results = $query->result_array();
			$output .= '<ul class="list-unstyled">';
			if (count($results) > 0) {
				foreach ($results as $key => $val) {
					$output .= '<li data-id="' . $val['user_id'] . '">' . $val["crm_company_name"] . '</li>';
				}
			} else {
				$output .= '<b>Data Not Found</b>';
			}
			$output .= '</ul>';
			echo $output;
		}
	}

	// public function add_assignto($assignee,$id,$update=false)
	// {

	//   if($assignee!='' && $id!='' )
	//   {
	//     if($update==1)
	//       {
	//         $this->db->where('module_name','TASK');
	//         $this->db->where('module_id',$id);
	//         $this->db->delete('firm_assignees');
	//     }
	//       foreach (explode(",",$assignee) as $key => $value)
	//     {

	//       $data = ['firm_id'=>$_SESSION['firm_id'],'module_name'=>'TASK','module_id'=>$id,'assignees'=>$value ];

	//         $this->db->insert('firm_assignees',$data);

	//     }
	//   }
	//   return true;
	// }



	public function add_new_task()
	{
		$this->Security_model->chk_login();
		$datass['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user', 'role', '1');
		if (isset($_POST) && $_POST['company_name'] != '') {
			$client_id = $this->Common_mdl->select_record("client", "id", $_POST['user_id']);
			$data['user_id'] = $client_id['user_id'];
			
			$data['public'] = (isset($_POST['public']) && ($_POST['public'] != '')) ? $_POST['public'] : '0';
			$data['billable'] = (isset($_POST['billable']) && ($_POST['billable'] != '')) ? $_POST['billable'] : '0';
			$data['subject'] = (isset($_POST['subject']) && ($_POST['subject'] != '')) ? $_POST['subject'] : '0';
			$data['start_date'] = (isset($_POST['startdate']) && ($_POST['startdate'] != '')) ? date('Y-m-d', strtotime($_POST['startdate'])) : '0';
			$data['end_date'] = (isset($_POST['enddate']) && ($_POST['enddate'] != '')) ? date('Y-m-d', strtotime($_POST['enddate'])) : '0';
			$data['priority'] = (isset($_POST['priority']) && ($_POST['priority'] != '')) ? $_POST['priority'] : '0';
			$data['related_to'] = 'tasks';
			$data['related_to_services'] = (!empty($_POST['related_to_services'])) ? $_POST['related_to_services'] : '0';
			$data['estimated_time'] = $_POST['estimated_time'] ?? 0;
			$data['project_id'] = (isset($_POST['r_project']) && ($_POST['r_project'] != '')) ? $_POST['r_project'] : '0';
			$data['company_name'] = $_POST['company_name'];
			$data['timeline_task_id'] = (isset($_POST['timeline_task_id'])) ? $_POST['timeline_task_id'] : '0';
			$data['firm_id'] = (isset($_SESSION['firm_id'])) ? $_SESSION['firm_id'] : '0';


			$data['tag'] = (isset($_POST['tag']) && ($_POST['tag'] != '')) ? implode(',', $_POST['tag']) : '0';

			$data['specific_time'] = (isset($_POST['specific_time']) && ($_POST['specific_time'] != '')) ? $_POST['specific_time'] : '0';
			$data['description'] = (isset($_POST['description_value']) && ($_POST['description_value'] != '')) ? $_POST['description_value'] : '0';
			$data['customize'] = (isset($_POST['customize']) && ($_POST['customize'] != '')) ? $_POST['customize'] : '1';
			$data['selectuser'] = (isset($_POST['selectuser']) && ($_POST['selectuser'] != '')) ? implode(',', $_POST['selectuser']) : '0';
			$data['task_status'] = (isset($_POST['task_status']) && ($_POST['task_status'] != '')) ? $_POST['task_status'] : '0';
			$data['created_date'] = time();
			$attach_files = '';
			if (!empty($_FILES['attach_file']['name'])) {
				/** for multiple **/
				$files = $_FILES;
				$cpt = count($_FILES['attach_file']['name']);
				for ($i = 0; $i < $cpt; $i++) {
					/** end of multiple **/
					$_FILES['attach_file']['name'] = $files['attach_file']['name'][$i];
					$_FILES['attach_file']['type'] = $files['attach_file']['type'][$i];
					$_FILES['attach_file']['tmp_name'] = $files['attach_file']['tmp_name'][$i];
					$_FILES['attach_file']['error'] = $files['attach_file']['error'][$i];
					$_FILES['attach_file']['size'] = $files['attach_file']['size'][$i];
					$uploadPath = 'uploads/';
					$config['upload_path'] = $uploadPath;
					$config['allowed_types'] = '*';
					$config['max_size'] = '0';
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if ($this->upload->do_upload('attach_file')) {
						$fileData = $this->upload->data();
						$attach_files = $attach_files . "," . base_url() . 'uploads/' . $fileData['file_name'];
					}
				}
			} else {
				$attach_files = '';
			}
			if ($attach_files != '') {
				$data['attach_file'] = implode(',', array_filter(explode(',', $attach_files)));
			} else {
				$data['attach_file'] = '';
			}
			/** newly added rs **/
			$data['create_by'] = $_SESSION['id'];
			/** for reminder popup data 18-06-2018 **/
			$data['recuring_sus_msg'] = $_POST['recuring_sus_msg'];
			$data['reminder_specific_time'] = $_POST['specific_time'];
			$data['reminder_specific_timepic'] = $_POST['specific_timepic'];
			$data['reminder_chk_box'] = $_POST['for_reminder_chk_box'];
			$data['reminder_msg'] = $_POST['reminder_msg'];
			// $data['every_date']=(isset($_POST['every_date'])&& ($_POST['every_date']!=''))?$_POST['every_date']:'0';
			/** end of reminder popup data **/
			/** 23-06-2018 **/
			$data['sche_start_date'] = (isset($_POST['sche_startdate']) && ($_POST['sche_startdate'] != '')) ? $_POST['sche_startdate'] : '';
			$data['sche_send_time'] = (isset($_POST['sche_send_time']) && ($_POST['sche_send_time'] != '')) ? $_POST['sche_send_time'] : '';
			$data['sche_repeats'] = (isset($_POST['repeats']) && ($_POST['repeats'] != '')) ? $_POST['repeats'] : '';
			$data['sche_every_week'] = (isset($_POST['every']) && ($_POST['every'] != '')) ? $_POST['every'] : '';
			$data['sche_on'] = (isset($_POST['days']) && ($_POST['days'] != '')) ? implode(',', $_POST['days']) : '';
			$data['sche_ends'] = (isset($_POST['end']) && ($_POST['end'] != '')) ? $_POST['end'] : '';
			$data['sche_after_msg'] = (isset($_POST['message']) && ($_POST['message'] != '')) ? $_POST['message'] : '';
			$data['sche_on_date'] = (isset($_POST['sche_on_date']) && ($_POST['sche_on_date'] != '')) ? date("Y-m-d", strtotime($_POST['sche_on_date'])) : '';


			if (!empty($_POST['sche_startdate'])) {
				$recurring_start_date = date_create_from_format('d-m-Y h:i a', $_POST['sche_startdate'] . " " . $_POST['sche_send_time']);
				$recurring_start_date = date_format($recurring_start_date, 'U');

				while ($recurring_start_date < strtotime('NOW')) {
					$recurring_start_date = strtotime("+" . trim($_POST['every']) . " " . trim($_POST['repeats']) . "s", $recurring_start_date);
				}
				$data['recuring_start_time'] = date('Y-m-d', $recurring_start_date);
			}
			/** end of 23-03-2018 **/
			// print_r($_POST['related_to_services']);
			//   print_r($data);
			//   exit;
			$insert_data = $this->db->insert('add_new_task', $data);
			$in = $this->db->insert_id();
			$this->Common_mdl->add_assignto($_POST['assign_role'], $in, 'TASK');
			$task_ids = $in;
			$this->Common_mdl->task_setting($in);
			/** 16-10-2018 services subtasks**/

			if (!empty($_POST['related_to_services'])) {
				// $this->Task_section_model->get_services_options_insert($_POST['related_to_services'],$in); 
				$this->Task_section_model->get_services_options_insert($in);
				$content1 = 'With Sub Task';
			} else {
				$content1 = '';
			}
			/** end of 16-10-2018 **/
			$username = $this->Common_mdl->getUserProfileName($_SESSION['id']);
			$activity_datas['log'] = "Task Was Created " . $content1 . ' by ';
			$activity_datas['createdTime'] = time();
			$activity_datas['module'] = 'Task';
			$activity_datas['sub_module'] = '';
			$activity_datas['user_id'] = $_SESSION['id'];
			$activity_datas['module_id'] = $task_ids;

			$this->Common_mdl->insert('activity_log', $activity_datas);
			//echo $task_ids;
			/*******************************************************/
			$task_data = $this->db->query("select * from add_new_task where id=" . $in)->row_array();
			$reminder_specific_time = date('Y-m-d', strtotime($task_data['reminder_specific_time']));
			$reminder_specific_timepic = $task_data['reminder_specific_timepic'];
			//nobody,1hour,2hour,4hour,5hour,8hour,tomorrow_morning,tomorrow_afternoon,1day,2day,4day,6day,8day,1week,2week,4week,6week,8week,1month,2month,4month,6month,8month
			$reminder_chk_box = $task_data['reminder_chk_box'];
			$time = strtotime($reminder_specific_timepic);
			$round = 5 * 60;
			$rounded = round($time / $round) * $round;
			$reminder_specific_timepic = date("h:i A", $rounded);
			if ($reminder_chk_box != '') {

				$if_no = array("no" => "nobody");
				$if_hour = array("+1 hour" => "1hour", "+2 hour" => "2hour", "+4 hour" => "4hour", "+5 hour" => "5hour", "+8 hour" => "8hour");
				$if_to = array("10:00 AM" => "tomorrow_morning", "01:00 PM" => "tomorrow_afternoon");
				$if_day = array("+1 days" => "1day", "+2 days" => "2day", "+4 days" => "4day", "+6 days" => "6day", "+8 days" => "8day");
				$if_week = array("+1 week" => "1week", "+2 week" => "2week", "+4 week" => "4week", "+6 week" => "6week", "8week");
				$if_month = array("+1 months" => "1month", "+2 months" => "2month", "+4 months" => "4month", "+6 months" => "6month", "+8 months" => "8month");

				$new_array = array("no" => "nobody", "+1 hour" => "1hour", "+2 hour" => "2hour", "+4 hour" => "4hour", "+5 hour" => "5hour", "+8 hour" => "8hour", "tomorrow_morning", "tomorrow_afternoon", "+1 days" => "1day", "+2 days" => "2day", "+4 days" => "4day", "+6 days" => "6day", "+8 days" => "8day", "+1 week" => "1week", "+2 week" => "2week", "+4 week" => "4week", "+6 week" => "6week", "8week", "+1 months" => "1month", "+2 months" => "2month", "+4 months" => "4month", "+6 months" => "6month", "+8 months" => "8month");

				$ex_val = explode(',', $reminder_chk_box);

				$for_new_array = array();

				$zzx = '+1 hour';
				// echo date("h:i:A", strtotime(''.$reminder_specific_timepic.' '.$zzx));
				foreach ($ex_val as $re_key => $re_value) {

					$if_no_arr = array_search($re_value, $if_no);
					if ($if_no_arr !== false) {
						// echo $if_no[$if_no_arr];
						// echo "<br>";
					}
					$if_hour_arr = array_search($re_value, $if_hour);
					if ($if_hour_arr != false) {

						$its_time = date("h:i A", strtotime('' . $reminder_specific_timepic . ' ' . $if_hour_arr));

						//  $for_new_array[$re_value]=$reminder_specific_time."//".$its_time;
						$for_new_array[$re_value] = strtotime($reminder_specific_time . " " . $its_time);

						//  echo $for_new_array[$re_value];

					}
					$if_to_arr = array_search($re_value, $if_to);
					if ($if_to_arr != false) {

						$its_date = date("Y-m-d", strtotime('' . $reminder_specific_time . ' +1 days'));

						//$for_new_array[$re_value]=$its_date."//".$if_to_arr;
						$for_new_array[$re_value] = strtotime($its_date . " " . $if_to_arr);
					}
					$if_day_arr = array_search($re_value, $if_day);
					if ($if_day_arr != false) {

						$its_date = date("Y-m-d", strtotime('' . $reminder_specific_time . ' ' . $if_day_arr));
						//$for_new_array[$re_value]=$its_date."//".$reminder_specific_timepic;
						$for_new_array[$re_value] = strtotime($its_date . " " . $reminder_specific_timepic);
					}
					$if_week_arr = array_search($re_value, $if_week);
					if ($if_week_arr != false) {

						$its_date = date("Y-m-d", strtotime('' . $reminder_specific_time . ' ' . $if_week_arr));
						//$for_new_array[$re_value]=$its_date."//".$reminder_specific_timepic;
						$for_new_array[$re_value] = strtotime($its_date . " " . $reminder_specific_timepic);
					}
					$if_month_arr = array_search($re_value, $if_month);
					if ($if_month_arr != false) {

						$its_date = date("Y-m-d", strtotime('' . $reminder_specific_time . ' ' . $if_month_arr));
						//$for_new_array[$re_value]=$its_date."//".$reminder_specific_timepic;
						$for_new_array[$re_value] = strtotime($its_date . " " . $reminder_specific_timepic);
					}
				}

				//$for_reminder_chk_box=json_encode($for_new_array);
				$for_reminder_chk_box = implode(",", array_values($for_new_array));
				//  print_r($for_reminder_chk_box);
				// exit;
				$data['for_reminder_chk_box'] = $for_reminder_chk_box;
				// echo $in;
				$this->Common_mdl->update('add_new_task', $data, 'id', $in);
			}
			/*****************************************************/
			$invoice_id = '';
			if ((isset($_POST['billable']) && ($_POST['billable'] != '')) && $_POST['task_status'] == '5') {
				$invoice_id = $this->Task_invoice_model->task_billable_createInvoice($in);
				if ($invoice_id != '') {
					$datas['invoice_id'] = $invoice_id;
					$result =  $this->Common_mdl->update('add_new_task', $datas, 'id', $in);
				}
			}

			$this->Task_invoice_model->task_new_mail($in, 'for_add');


			if ($insert_data) {
				/** 20-10-2018 for Assignee person **/

				echo $task_ids;
				/** end of 20-10-2018 **/
			} else {
				echo '0';
			}
		} else {
			echo '0';
		}
	}

	public function update_new_task()
	{

		// print_r($_POST);
		// exit;

		$this->Security_model->chk_login();

		//  $this->add_assignto($_POST['assign_role'],$_POST['task_id'],1);

		$task_datas = $this->Common_mdl->select_record('add_new_task', 'id', $_POST['task_id']);

		$prev_company_name = $task_datas['company_name'];
		$for_recurring_time = $task_datas['recuring_start_time'];
		$datass['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user', 'role', '1');

		if (isset($_POST) && $_POST['task_id'] != '') {

			$image_one = (isset($_POST['image_name']) && ($_POST['image_name'] != '')) ? $_POST['image_name'] : '';

			$attach_files = '';
			if (!empty($_FILES['attach_file']['name'])) {
				/** for multiple **/
				$files = $_FILES;
				$cpt = count($_FILES['attach_file']['name']);
				for ($i = 0; $i < $cpt; $i++) {
					/** end of multiple **/
					$_FILES['attach_file']['name'] = $files['attach_file']['name'][$i];
					$_FILES['attach_file']['type'] = $files['attach_file']['type'][$i];
					$_FILES['attach_file']['tmp_name'] = $files['attach_file']['tmp_name'][$i];
					$_FILES['attach_file']['error'] = $files['attach_file']['error'][$i];
					$_FILES['attach_file']['size'] = $files['attach_file']['size'][$i];
					$uploadPath = 'uploads/';
					$config['upload_path'] = $uploadPath;
					$config['allowed_types'] = '*';
					$config['max_size'] = '0';
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if ($this->upload->do_upload('attach_file')) {
						$fileData = $this->upload->data();
						//  $uploadData[$i]['file_name'] = base_url().'attachment/'.$fileData['file_name'];
						//$data['attach_file']= base_url().'uploads/'.$fileData['file_name'];
						$attach_files = $attach_files . "," . base_url() . 'uploads/' . $fileData['file_name'];
					}
				}
			}
			$ex_img = (isset($_POST['already_upload_img']) && $_POST['already_upload_img'] != '') ? $_POST['already_upload_img'] : '';
			if (!empty($ex_img)) {
				$attach_files = $attach_files . "," . implode(',', $ex_img);
			}
			if ($attach_files != '') {
				$data['attach_file'] = implode(',', array_filter(explode(',', $attach_files)));
			} else {
				$data['attach_file'] = '';
			}

			// $data['user_id']=$_POST['user_id'];
			$client_id = $this->Common_mdl->select_record("client", "id", $_POST['user_id']);
			$data['user_id'] = $client_id['user_id'];
			$data['public'] = (isset($_POST['public']) && ($_POST['public'] != '')) ? $_POST['public'] : '0';
			$data['billable'] = (isset($_POST['billable']) && ($_POST['billable'] != '')) ? $_POST['billable'] : '0';
			$data['subject'] = (isset($_POST['subject']) && ($_POST['subject'] != '')) ? $_POST['subject'] : '0';
			$data['start_date'] = (isset($_POST['startdate']) && ($_POST['startdate'] != '')) ? date('Y-m-d', strtotime($_POST['startdate'])) : '0';
			$data['end_date'] = (isset($_POST['enddate']) && ($_POST['enddate'] != '')) ? date('Y-m-d', strtotime($_POST['enddate'])) : '0';
			$data['priority'] = (isset($_POST['priority']) && ($_POST['priority'] != '')) ? $_POST['priority'] : '0';
			// $data['related_to']='tasks';        
			$data['related_to_services'] = (!empty($_POST['related_to_services'])) ? $_POST['related_to_services'] : '0';
			$data['estimated_time'] = $_POST['estimated_time'] ?? 0;
			$data['project_id'] = (isset($_POST['r_project']) && ($_POST['r_project'] != '')) ? $_POST['r_project'] : '0';
			$data['company_name'] = (isset($_POST['company_name']) && $_POST['company_name'] != '') ? $_POST['company_name'] : $task_datas['company_name'];
			$team = array();
			$department = array();
			$assign = array();
			//$data['assigned'] = implode(',',$_POST['assigned']);
			/** for assign team and department  14-06-2018 **/

			$this->Common_mdl->add_assignto($_POST['assign_role'], $_POST['task_id'], 'TASK', 1);
			if (isset($_POST['worker'])) {
				foreach ($_POST['worker'] as $key => $value) {
					$ex_val = explode('_', $value);
					if (count($ex_val) > 1) {
						if ($ex_val[0] == 'tm') {
							array_push($team, $ex_val[1]);
						}
						if ($ex_val[0] == 'de') {
							array_push($department, $ex_val[1]);
						}
					} else {
						array_push($assign, $ex_val[0]);
					}
				}

				$data['worker'] = implode(',', $assign);
				$data['team'] = implode(',', $team);
				$data['department'] = implode(',', $department);
			}
			/** end of 14-06-2018 **/

			// $data['manager']=(isset($_POST['manager']) && is_array($_POST['manager']) )? implode(',',$_POST['manager']):'';

			//$data['tag']=(isset($_POST['tag'])&&($_POST['tag']!=''))? implode(',',$_POST['tag']):'0';

			$data['tag'] = (!empty($_POST['tag'])) ? $_POST['tag'] : '';

			$data['manager'] = (isset($_POST['manager']) && ($_POST['manager'] != '')) ? $_POST['manager'] : '0';
			$data['sche_start_date'] = (isset($_POST['sche_startdate']) && ($_POST['sche_startdate'] != '')) ? $_POST['sche_startdate'] : '';
			$data['sche_send_time'] = (isset($_POST['sche_send_time']) && ($_POST['sche_send_time'] != '')) ? $_POST['sche_send_time'] : '';
			$data['sche_repeats'] = (isset($_POST['repeats']) && ($_POST['repeats'] != '')) ? $_POST['repeats'] : '';
			$data['sche_every_week'] = (isset($_POST['every']) && ($_POST['every'] != '')) ? $_POST['every'] : '';
			$data['sche_on'] = (isset($_POST['days']) && ($_POST['days'] != '')) ? implode(',', $_POST['days']) : '';
			$data['sche_ends'] = (isset($_POST['end']) && ($_POST['end'] != '')) ? $_POST['end'] : '';
			$data['sche_after_msg'] = (isset($_POST['message']) && ($_POST['message'] != '')) ? $_POST['message'] : '0';
			$data['sche_on_date'] = (isset($_POST['sche_on_date']) && ($_POST['sche_on_date'] != '')) ? date("Y-m-d", strtotime($_POST['sche_on_date'])) : '';
			$data['specific_time'] = (isset($_POST['specific_time']) && ($_POST['specific_time'] != '')) ? $_POST['specific_time'] : '0';
			$data['description'] = (isset($_POST['description_value']) && ($_POST['description_value'] != '')) ? $_POST['description_value'] : '0';
			/*as Per client requirement*/

			/*$data['customize']=(isset($_POST['customize'])&&($_POST['customize']!=''))? $_POST['customize']:'0';
             $data['selectuser']=(isset($_POST['selectuser'])&&($_POST['selectuser']!=''))? implode(',',$_POST['selectuser']):'0';*/

			/*as Per client requirement*/

			$data['task_status'] = (isset($_POST['task_status']) && ($_POST['task_status'] != '')) ? $_POST['task_status'] : '0';
			// $data['created_date']=time();
			$data['updated_date'] = time();
			// $data['attach_file']=$image;
			/** for newly added **/
			/** for reminder popup data 18-06-2018 **/
			$data['recuring_sus_msg'] = $_POST['recuring_sus_msg'];
			$data['reminder_specific_time'] = $_POST['specific_time'];
			$data['reminder_specific_timepic'] = $_POST['specific_timepic'];
			$data['reminder_chk_box'] = $_POST['for_reminder_chk_box'];
			$data['reminder_msg'] = $_POST['reminder_msg'];
			//$data['every_date']=(isset($_POST['every_date'])&& ($_POST['every_date']!=''))?$_POST['every_date']:'0';
			/** end of reminder popup data **/
			/** for 23-06-2018 recurring task **/
			/* $recurring_start_date=$_POST['for_recuring_start'];// if value is changed all values reset
            $recurring_repeats=$_POST['recurring_repeats'];
            $recurring_every=$_POST['recurring_every'];
            //$recurring_every_date=$_POST['recurring_every_date'];
            $recurring_end=$_POST['recurring_end'];
            $recurring_msg=$_POST['recurring_msg'];
            $recurring_end_date=$_POST['recurring_end_date'];

            if($recurring_start_date=="changed")
            {
              $data['recuring_start_time']='';
              $for_recurring_time='';
              $data['recuring_on_off']='';
              $data['recuring_times']='';
              $data['recuring_task_count']=0;
            }
            else{
               if(($recurring_repeats!=$_POST['repeats']))
               {
                  $data['recuring_start_time']='';
                  $for_recurring_time='';
                  $data['recuring_on_off']='';
                  $data['recuring_times']='';
                  $data['recuring_task_count']=0;
               }
                if(($recurring_every!=$_POST['every']))
               {
                  $data['recuring_start_time']='';
                  $for_recurring_time='';
                  $data['recuring_on_off']='';
                  $data['recuring_times']='';
                  $data['recuring_task_count']=0;
               }              
              if(isset($_POST['end'])){
              if($recurring_end!=$_POST['end'])
              {
                  $data['recuring_on_off']='';
              }
              else
              {
                  if($recurring_end=='after')
                  {
                      if($recurring_msg!=$_POST['message'])
                      {
                          $data['recuring_on_off']='';
                      }
                  }
                  else if($recurring_end="on")
                  {
                      if($recurring_end_date!= date("Y-m-d", strtotime($_POST['sche_on_date'])))
                      {
                          $data['recuring_on_off']='';
                      }
                  }
              }
             }
            }

if($_POST['repeats']=='day')
{
    $date1=date_create(date('Y-m-d'));
    $date2=date_create(date('Y-m-d',strtotime($_POST['sche_startdate'])));
    
    $diff=date_diff($date1,$date2);
    $zz=$diff->format("%R%a");
    $diffence=$_POST['every'];
 
    if((int)$zz<0) //today is greater then start date
    {
      //  echo 'one';
        if($for_recurring_time==''){
        
        $for_end_date= date('Y-m-d',strtotime(date("Y-m-d",strtotime($_POST['sche_startdate']))." +".$diffence." days"));
       // echo $for_end_date;
        $data['recuring_start_time']=$for_end_date;
         }
    }
    else if((int)$zz==0) //check start today 
    {
        // echo 'two';
              $dateTime = new DateTime('now', new DateTimeZone(date_default_timezone_get()));
              $now_time= $dateTime->format("h:i A"); 
              $time = strtotime($now_time);
    $round = 5*60;
    $rounded = round($time / $round) * $round;
    $f_time=date("h:i A", $rounded);
              $test= $_POST['sche_send_time'];
                         $time = strtotime($test);
    $round = 5*60;
    $rounded = round($time / $round) * $round;
    $s_time=date("h:i A", $rounded);
  //  echo $f_time."--".$s_time;
             if($f_time>$s_time) //if send today 
              {
                if($for_recurring_time==''){
                   $for_end_date= date('Y-m-d',strtotime(date("Y-m-d",strtotime($_POST['sche_startdate']))." +".$diffence." days"));
                       echo $for_end_date;
                   $data['recuring_start_time']=$for_end_date;
                 }
              }
    }
    else
    {
      //  echo "thr";
    }
}*/

			$manager = (isset($_POST['manager']) && is_array($_POST['manager'])) ?  $_POST['manager'] : explode(",", $task_datas['manager']);
			// $manager = (is_array($manager) ? $manager : array() );

			if ($task_datas['for_old_assign'] != '') {
				$old_assign = json_decode($task_datas['for_old_assign'], TRUE);

				foreach ($old_assign as $k =>  $part) {
					if (is_array($part) && count($part) >= 1) {

						$diff = array_diff($$k, array_filter($part));
						$for_old_values[$k] = (count($diff) ? array_merge($part, $diff) : $part);
					} else {
						$for_old_values[$k] = $part;
					}
				}

				// print_r($for_old_values);

			} else {

				$for_old_values = array();
				$for_old_values['assign'] = $assign;
				$for_old_values['team'] = $team;
				$for_old_values['department'] = $department;
				$for_old_values['manager'] = $manager;
			}
			$data['for_old_assign'] = json_encode($for_old_values);

			//$result =  $this->Common_mdl->update('add_new_task',$for_us_datas,'id',$in);

			$update_data = $this->Common_mdl->update('add_new_task', $data, 'id', $_POST['task_id']);


			$new_data = $this->db->query("select * from add_new_task where id=" . $_POST['task_id'])->row_array();


			$find_diff = array_fill_keys(['sche_start_date', 'sche_send_time', 'sche_repeats', 'sche_every_week', 'sche_ends', 'sche_after_msg', 'sche_on_date'], 0);

			$old_data = array_intersect_key($task_datas, $find_diff);
			$new_data = array_intersect_key($new_data, $find_diff);
			$data_varies = array_diff($new_data, $old_data);

			if ($new_data['sche_start_date'] != '' && count($data_varies)) {
				$recurring_start_date = date_create_from_format('d-m-Y h:i a', $new_data['sche_start_date'] . " " . $new_data['sche_send_time']);
				$recurring_start_date = date_format($recurring_start_date, 'U');

				while ($recurring_start_date < strtotime('NOW')) {
					$recurring_start_date = strtotime("+" . $new_data['sche_every_week'] . " " . trim($new_data['sche_repeats']) . "s", $recurring_start_date);
				}
				$recuring_start['recuring_start_time'] = date('Y-m-d', $recurring_start_date);
				$this->Common_mdl->update('add_new_task', $recuring_start, 'id', $_POST['task_id']);
			}

			//MAINTAIN add remove members date
			$assign_parm = ['assign' => $assign, 'team' => $team, 'department' => $department];
			$this->update_task_assignDetails($assign_parm, $_POST['task_id']);
			//MAINTAIN add remove members date

			$username = $this->Common_mdl->getUserProfileName($_SESSION['id']);
			$activity_datas['log'] = "Task Was Updated  by ";
			$activity_datas['createdTime'] = time();
			$activity_datas['module'] = 'Task';
			$activity_datas['sub_module'] = '';
			$activity_datas['user_id'] = $_SESSION['id'];
			$activity_datas['module_id'] = $_POST['task_id'];

			$this->Common_mdl->insert('activity_log', $activity_datas);

			// include exclude steps in main task
			if ($task_datas['related_to'] != 'sub_task' && $_POST['related_to_services'] != '' && $_POST['inc_exc_ids'] != "nochanges") {
				$this->inc_exc_service_steps($_POST['task_id']);
			}
			// include exclude steps in main task                  


			/*******************************************************/
			$task_data = $this->db->query("select * from add_new_task where id=" . $_POST['task_id'])->row_array();

			$reminder_specific_time = date('Y-m-d', strtotime($task_data['reminder_specific_time']));
			$reminder_specific_timepic = $task_data['reminder_specific_timepic'];
			//nobody,1hour,2hour,4hour,5hour,8hour,tomorrow_morning,tomorrow_afternoon,1day,2day,4day,6day,8day,1week,2week,4week,6week,8week,1month,2month,4month,6month,8month

			$reminder_chk_box = $task_data['reminder_chk_box'];
			$time = strtotime($reminder_specific_timepic);
			$round = 5 * 60;
			$rounded = round($time / $round) * $round;
			$reminder_specific_timepic = date("h:i A", $rounded);
			// echo $reminder_specific_time."<br>";
			// echo $reminder_specific_timepic."<br>";
			// echo $reminder_chk_box;
			if ($reminder_chk_box != '') {
				$if_no = array("no" => "nobody");
				$if_hour = array("+1 hour" => "1hour", "+2 hour" => "2hour", "+4 hour" => "4hour", "+5 hour" => "5hour", "+8 hour" => "8hour");
				$if_to = array("10:00 AM" => "tomorrow_morning", "01:00 PM" => "tomorrow_afternoon");
				$if_day = array("+1 days" => "1day", "+2 days" => "2day", "+4 days" => "4day", "+6 days" => "6day", "+8 days" => "8day");
				$if_week = array("+1 week" => "1week", "+2 week" => "2week", "+4 week" => "4week", "+6 week" => "6week", "8week");
				$if_month = array("+1 months" => "1month", "+2 months" => "2month", "+4 months" => "4month", "+6 months" => "6month", "+8 months" => "8month");

				$new_array = array("no" => "nobody", "+1 hour" => "1hour", "+2 hour" => "2hour", "+4 hour" => "4hour", "+5 hour" => "5hour", "+8 hour" => "8hour", "tomorrow_morning", "tomorrow_afternoon", "+1 days" => "1day", "+2 days" => "2day", "+4 days" => "4day", "+6 days" => "6day", "+8 days" => "8day", "+1 week" => "1week", "+2 week" => "2week", "+4 week" => "4week", "+6 week" => "6week", "8week", "+1 months" => "1month", "+2 months" => "2month", "+4 months" => "4month", "+6 months" => "6month", "+8 months" => "8month");

				$ex_val = explode(',', $reminder_chk_box);
				$for_new_array = array();
				$zzx = '+1 hour';
				// echo date("h:i:A", strtotime(''.$reminder_specific_timepic.' '.$zzx));
				foreach ($ex_val as $re_key => $re_value) {

					//    $if_no_arr = array_search($re_value, $if_no);
					//     if ($if_no_arr !== false) {
					//      // echo $if_no[$if_no_arr];
					//      // echo "<br>";
					//     }
					//    $if_hour_arr = array_search($re_value, $if_hour);
					//     if($if_hour_arr != false)
					//     {

					//   $its_time= date("h:i A", strtotime(''.$reminder_specific_timepic.' '.$if_hour_arr));
					//   $for_new_array[$re_value]=$reminder_specific_time."//".$its_time;
					// //  echo $for_new_array[$re_value];

					//     }
					//    $if_to_arr = array_search($re_value, $if_to);
					//     if($if_to_arr != false)
					//     {

					//     $its_date= date("Y-m-d", strtotime(''.$reminder_specific_time.' +1 days'));

					//   $for_new_array[$re_value]=$its_date."//".$if_to_arr;

					//     }
					//    $if_day_arr = array_search($re_value, $if_day);
					//     if($if_day_arr != false)
					//     {

					//   $its_date= date("Y-m-d", strtotime(''.$reminder_specific_time.' '.$if_day_arr));
					//   $for_new_array[$re_value]=$its_date."//".$reminder_specific_timepic;

					//     }
					//    $if_week_arr = array_search($re_value, $if_week);
					//     if($if_week_arr != false)
					//     {

					//      $its_date= date("Y-m-d", strtotime(''.$reminder_specific_time.' '.$if_week_arr));
					//   $for_new_array[$re_value]=$its_date."//".$reminder_specific_timepic;

					//     }
					//    $if_month_arr = array_search($re_value, $if_month);
					//     if($if_month_arr != false)
					//     {

					//      $its_date= date("Y-m-d", strtotime(''.$reminder_specific_time.' '.$if_month_arr));
					//   $for_new_array[$re_value]=$its_date."//".$reminder_specific_timepic;

					//     }  

					$if_no_arr = array_search($re_value, $if_no);
					if ($if_no_arr !== false) {
						// echo $if_no[$if_no_arr];
						// echo "<br>";
					}
					$if_hour_arr = array_search($re_value, $if_hour);
					if ($if_hour_arr != false) {

						$its_time = date("h:i A", strtotime('' . $reminder_specific_timepic . ' ' . $if_hour_arr));

						//  $for_new_array[$re_value]=$reminder_specific_time."//".$its_time;
						$for_new_array[$re_value] = strtotime($reminder_specific_time . " " . $its_time);

						//  echo $for_new_array[$re_value];

					}
					$if_to_arr = array_search($re_value, $if_to);
					if ($if_to_arr != false) {

						$its_date = date("Y-m-d", strtotime('' . $reminder_specific_time . ' +1 days'));

						//$for_new_array[$re_value]=$its_date."//".$if_to_arr;
						$for_new_array[$re_value] = strtotime($its_date . " " . $if_to_arr);
					}
					$if_day_arr = array_search($re_value, $if_day);
					if ($if_day_arr != false) {

						$its_date = date("Y-m-d", strtotime('' . $reminder_specific_time . ' ' . $if_day_arr));
						//$for_new_array[$re_value]=$its_date."//".$reminder_specific_timepic;
						$for_new_array[$re_value] = strtotime($its_date . " " . $reminder_specific_timepic);
					}
					$if_week_arr = array_search($re_value, $if_week);
					if ($if_week_arr != false) {

						$its_date = date("Y-m-d", strtotime('' . $reminder_specific_time . ' ' . $if_week_arr));
						//$for_new_array[$re_value]=$its_date."//".$reminder_specific_timepic;
						$for_new_array[$re_value] = strtotime($its_date . " " . $reminder_specific_timepic);
					}
					$if_month_arr = array_search($re_value, $if_month);
					if ($if_month_arr != false) {

						$its_date = date("Y-m-d", strtotime('' . $reminder_specific_time . ' ' . $if_month_arr));
						//$for_new_array[$re_value]=$its_date."//".$reminder_specific_timepic;
						$for_new_array[$re_value] = strtotime($its_date . " " . $reminder_specific_timepic);
					}
				}
				$for_reminder_chk_box = implode(",", $for_new_array);
				$data2['for_reminder_chk_box'] = $for_reminder_chk_box;
				$this->Common_mdl->update('add_new_task', $data2, 'id', $_POST['task_id']);
			}
			/*****************************************************/

			/** for task edit option invoice update **/
			$get_task_data = $this->Common_mdl->GetAllWithWhere('add_new_task', 'id', $_POST['task_id']);
			$changed_company_name = $get_task_data[0]['company_name'];
			$res_cmpny = '';
			if ($changed_company_name == $prev_company_name) {
				$res_cmpny = 'equ';
			} else {
				$res_cmpny = 'not';
			}
			$invoice_id = $get_task_data[0]['invoice_id'];
			//echo $invoice_id."---";
			$in = $_POST['task_id'];
			if ($invoice_id != '') {

				if ((isset($_POST['billable']) && ($_POST['billable'] != '')) && $_POST['task_status'] == '5') {
					$invoice_id = $this->Task_invoice_model->task_billable_createInvoice($in, $res_cmpny);
				}
			} else {
				$invoice_id = '';
				if ((isset($_POST['billable']) && ($_POST['billable'] != '')) && $_POST['task_status'] == '5') {
					$invoice_id = $this->Task_invoice_model->task_billable_createInvoice($in, $res_cmpny);
					if ($invoice_id != '') {
						$for_us_datas['invoice_id'] = $invoice_id;
						$data['invoice_id'] = $invoice_id;
						$result =  $this->Common_mdl->update('add_new_task', $for_us_datas, 'id', $in);
					}
				}
			}

			if ($_POST['task_status'] == '5') {
				$res_table = $this->db->query("select * from reminder_setting where  FIND_IN_SET(" . $in . ",custom_task_created)")->result_array();
				if (count($res_table) > 0) {
					$invoice_id = $this->Task_invoice_model->task_billable_createInvoice($in);
				}
			}
			/** end of invoice update **/

			/** task send mail notification **/
			/** new task send a mail to a assignee **/
			$this->Task_invoice_model->task_new_mail($in, 'for_edit');



			$task_datas2 = $this->Common_mdl->select_record('add_new_task', 'id', $_POST['task_id']);

			if ($task_datas2['sub_task_id'] != '') {

				$sub_task = explode(',', $task_datas2['sub_task_id']);

				for ($i = 0; $i < count($sub_task); $i++) {


					unset($data['subject']);
					$update_datas = $this->Common_mdl->update('add_new_task', $data, 'id', $sub_task[$i]);

					//MAINTAIN add remove members date
					$this->update_task_assignDetails($assign_parm, $sub_task[$i]);
					//MAINTAIN add remove members date

					$username = $this->Common_mdl->getUserProfileName($_SESSION['id']);
					$activity_datas['log'] = "Subtask Was Updated  by " . $username;
					$activity_datas['createdTime'] = time();
					$activity_datas['module'] = 'Task';
					$activity_datas['sub_module'] = '';
					$activity_datas['user_id'] = $_SESSION['id'];
					$activity_datas['module_id'] = $_POST['task_id'];

					$this->Common_mdl->insert('activity_log', $activity_datas);
				}
			}
			/** end of mail notification **/
			/* $usersid = $this->session->userdata('id');
                 $datas['log'] = "Update a Task details or status";
                 $datas['createdTime'] = time();
                 $datas['module'] = 'Task';
                 $datas['user_id'] = $usersid;
                 $this->Common_mdl->insert('activity_log',$datas);*/

			$data1 = array(
				'task_id' => $_POST['task_id'],
				'created_at' => time()
			);
			if ($update_data) {
				$in = $this->db->insert_id();

				$query = $this->Common_mdl->getallrecords('task_revisions');
				if (!empty($query)) {
					$in_sql = $this->Common_mdl->insert('task_revisions', $data1);
				} else {
					$inn_sql = $this->Common_mdl->insert('task_revisions', $data1);
				}

				echo '1';
			} else {
				echo '0';
			}
		} else {
			echo '0';
		}
	}


	public function inc_exc_service_steps($task_id)
	{

		$task_data = $this->db->query("select * from add_new_task where id = " . $task_id . " ")->row_array();

		$sub_task_id = (!empty($task_data['sub_task_id']) ?  explode(',', $task_data['sub_task_id']) : []);

		if (!empty($task_data['sub_task_id'])) {
			$ex_steps = $this->db->query("select services_main_id,id from add_new_task where id IN (" . $task_data['sub_task_id'] . ") ")->result_array();
			$ex_step = array_column($ex_steps, 'services_main_id', 'id');
		} else {
			$ex_step = [];
		}

		$Ch_step = (!empty($_POST['inc_exc_ids']) ? explode(',', $_POST['inc_exc_ids']) : []);

		$imp_s = array_diff($Ch_step,  $ex_step);

		$exp_s = array_diff($ex_step, $Ch_step);

		if (count($imp_s)) {
			foreach ($imp_s as $id) {
				$main_steps = $this->db->query("select * from service_steps where id = " . $id)->row_array();
				/*$details = $this->db->query("select * from service_step_details where id = ".$id." ")->row_array();                    
                    $task_detail= [
                    'related_to_services'=>$_POST['related_to_services'],
                    's_date'=>$_POST['startdate'],
                    'e_date'=>$_POST['enddate'],
                    'subject'=>$_POST['related_to_services']."-".$main_steps['title'],
                    'assignee'=>$assignee,
                    'old_assign' => $old_assign
                    ];   */
				$sub_task_id[] = $this->Task_section_model->task_creation($task_data['id'], $main_steps['title'], $id, '');
			}
		}

		if (count($exp_s)) {
			foreach ($exp_s as $id => $ex_steps_id) {

				$this->db->query("delete from add_new_task where id = " . $id);

				$sub_task_id = array_diff($sub_task_id, [$id]);

				if ($this->db->affected_rows()) {
					$username = $this->Common_mdl->getUserProfileName($_SESSION['id']);
					$activity_datas['log'] = "Subtask Was removed  by ";
					$activity_datas['createdTime'] = time();
					$activity_datas['module'] = 'Task';
					$activity_datas['sub_module'] = '';
					$activity_datas['user_id'] = $_SESSION['id'];
					$activity_datas['module_id'] = $id;
					$this->Common_mdl->insert('activity_log', $activity_datas);
				}
			}
		}

		$sub_task_id = implode(',', $sub_task_id);

		$this->db->query("update add_new_task set sub_task_id = '" . $sub_task_id . "' where id=" . $task_data['id'] . " ");
	}

	public function update_task_assignDetails(array $assign, $task_id)
	{
		/** for update assigne data maintaine added and removed users 20-10-2018 **/
		$it_assign = $assign['assign'];
		$it_team = $assign['team'];
		$it_department = $assign['department'];
		//$get_prev_assigne_data=$this->Task_section_model->get_assigne_data('staff',$task_id);
		/** for staff assignee **/
		$get_prev_assigne_data = $this->db->query("select * from task_assignee_add_remove_list where task_id=" . $task_id . " and staff_team_department='staff' ")->result_array();

		$get_prev_assigne = array();
		$get_prev_ids = array();
		$get_added_removed_date = array();
		$get_prev_status = array();
		$get_prev_working_hrs = array();
		if (count($get_prev_assigne_data) > 0) {
			foreach ($get_prev_assigne_data as $prev_key => $prev_value) {
				array_push($get_prev_assigne, $prev_value['assignee_ids']);
				array_push($get_prev_ids, $prev_value['id']);
				array_push($get_added_removed_date, $prev_value['added_removed_date']);
				array_push($get_prev_status, $prev_value['status']);
				array_push($get_prev_working_hrs, $prev_value['removed_workers_timing']);
			}
		}
		if (count($get_prev_assigne_data) > 0) {
			$ex_assign = $get_prev_assigne;
			$ex_result = array_diff($ex_assign, $it_assign); //find remove person
			if (count($ex_result) > 0) {
				foreach ($ex_result as $ex_key => $ex_value) {
					$its_key = array_search($ex_value, $ex_assign);
					if ($get_prev_status[$its_key] != 'removed') {
						$add_remove_list = array();
						/** if it removed added a overall timing **/
						$get_individual_timer_data = $this->db->query("select * from individual_task_timer where user_id=" . $get_prev_assigne[$ex_key] . " and task_id=" . $task_id . " ")->row_array();
						$res_timing = $this->Task_section_model->calculate_timing_status_removed($get_individual_timer_data['time_start_pause']);
						// echo $res."jhdfhgf gjhggjhgh";
						/** end of overall timing **/
						if ($get_prev_working_hrs[$its_key] != '') {
							$add_remove_list['removed_workers_timing'] = $get_prev_working_hrs[$its_key] . "," . $res_timing;
						} else {
							$add_remove_list['removed_workers_timing'] = $res_timing;
						}
						$add_remove_list['status'] = 'removed';
						$add_remove_list['added_removed_date'] = $get_added_removed_date[$its_key] . "," . time();
						$this->Common_mdl->update('task_assignee_add_remove_list', $add_remove_list, 'id', $get_prev_ids[$ex_key]);
					}
				}
			}
		}
		foreach ($it_assign as $itas_key => $itas_value) {
			$get_count = $this->db->query("SELECT count(*) as it_count,GROUP_CONCAT(id) as dbid,added_removed_date,status FROM `task_assignee_add_remove_list` where assignee_ids=" . $itas_value . " and staff_team_department='staff' and task_id=" . $task_id . " ")->row_array();
			if ($get_count['it_count'] == 0) {
				$add_remove_list = array();
				$add_remove_list['task_id'] = $task_id;
				$add_remove_list['assignee_ids'] = $itas_value;
				$add_remove_list['staff_team_department'] = 'staff';
				$add_remove_list['status'] = 'active';
				$add_remove_list['removed_workers_timing'] = '';
				$add_remove_list['added_removed_date'] = time();
				$add_remove_list['created_by'] = $_SESSION['id'];
				$add_remove_list['created_time'] = time();
				$this->Common_mdl->insert('task_assignee_add_remove_list', $add_remove_list);
			} else {
				$it_id = $get_count['dbid'];
				if ($get_count['status'] != 'active') {
					$add_remove_list = array();
					$add_remove_list['status'] = 'active';
					$add_remove_list['added_removed_date'] = $get_count['added_removed_date'] . "," . time();
					$this->Common_mdl->update('task_assignee_add_remove_list', $add_remove_list, 'id', $it_id);
				}
			}
		}
		/** for staff assigne **/
		/** for TEAM **/
		$get_prev_assigne_data = $this->db->query("select * from task_assignee_add_remove_list where task_id=" . $task_id . " and staff_team_department='team' ")->result_array();
		$get_prev_assigne = array();
		$get_prev_ids = array();
		$get_added_removed_date = array();
		$get_prev_status = array();
		if (count($get_prev_assigne_data) > 0) {
			foreach ($get_prev_assigne_data as $prev_key => $prev_value) {
				array_push($get_prev_assigne, $prev_value['assignee_ids']);
				array_push($get_prev_ids, $prev_value['id']);
				array_push($get_added_removed_date, $prev_value['added_removed_date']);
				array_push($get_prev_status, $prev_value['status']);
			}
		}
		if (count($get_prev_assigne_data) > 0) {
			$ex_assign = $get_prev_assigne;
			$ex_result = array_diff($ex_assign, $it_team);
			if (count($ex_result) > 0) {
				foreach ($ex_result as $ex_key => $ex_value) {
					$its_key = array_search($ex_value, $ex_assign);
					if ($get_prev_status[$its_key] != 'removed') {
						$add_remove_list = array();
						$add_remove_list['status'] = 'removed';
						$add_remove_list['added_removed_date'] = $get_added_removed_date[$its_key] . "," . time();
						$this->Common_mdl->update('task_assignee_add_remove_list', $add_remove_list, 'id', $get_prev_ids[$ex_key]);
					}
				}
			}
		}
		foreach ($it_team as $itas_key => $itas_value) {
			$get_count = $this->db->query("SELECT count(*) as it_count,GROUP_CONCAT(id) as dbid,added_removed_date,status FROM `task_assignee_add_remove_list` where assignee_ids=" . $itas_value . " and staff_team_department='team' and task_id=" . $task_id . " ")->row_array();
			if ($get_count['it_count'] == 0) {
				$add_remove_list = array();
				$add_remove_list['task_id'] = $task_id;
				$add_remove_list['assignee_ids'] = $itas_value;
				$add_remove_list['staff_team_department'] = 'team';
				$add_remove_list['status'] = 'active';
				$add_remove_list['removed_workers_timing'] = '';
				$add_remove_list['added_removed_date'] = time();
				$add_remove_list['created_by'] = $_SESSION['id'];
				$add_remove_list['created_time'] = time();
				$this->Common_mdl->insert('task_assignee_add_remove_list', $add_remove_list);
			} else {
				$it_id = $get_count['dbid'];
				if ($get_count['status'] != 'active') {
					$add_remove_list = array();
					$add_remove_list['status'] = 'active';
					$add_remove_list['added_removed_date'] = $get_count['added_removed_date'] . "," . time();
					$this->Common_mdl->update('task_assignee_add_remove_list', $add_remove_list, 'id', $it_id);
				}
			}
		}
		/** for TEAM **/
		/** for Department **/
		$get_prev_assigne_data = $this->db->query("select * from task_assignee_add_remove_list where task_id=" . $task_id . " and staff_team_department='department' ")->result_array();
		$get_prev_assigne = array();
		$get_prev_ids = array();
		$get_added_removed_date = array();
		$get_prev_status = array();
		if (count($get_prev_assigne_data) > 0) {
			foreach ($get_prev_assigne_data as $prev_key => $prev_value) {
				array_push($get_prev_assigne, $prev_value['assignee_ids']);
				array_push($get_prev_ids, $prev_value['id']);
				array_push($get_added_removed_date, $prev_value['added_removed_date']);
				array_push($get_prev_status, $prev_value['status']);
			}
		}
		if (count($get_prev_assigne_data) > 0) {
			$ex_assign = $get_prev_assigne;
			$ex_result = array_diff($ex_assign, $it_department);
			if (count($ex_result) > 0) {
				foreach ($ex_result as $ex_key => $ex_value) {
					$its_key = array_search($ex_value, $ex_assign);
					if ($get_prev_status[$its_key] != 'removed') {
						$add_remove_list = array();
						$add_remove_list['status'] = 'removed';
						$add_remove_list['added_removed_date'] = $get_added_removed_date[$its_key] . "," . time();
						$this->Common_mdl->update('task_assignee_add_remove_list', $add_remove_list, 'id', $get_prev_ids[$ex_key]);
					}
				}
			}
		}
		foreach ($it_department as $itas_key => $itas_value) {
			$get_count = $this->db->query("SELECT count(*) as it_count,GROUP_CONCAT(id) as dbid,added_removed_date,status FROM `task_assignee_add_remove_list` where assignee_ids=" . $itas_value . " and staff_team_department='department' and task_id=" . $task_id . " ")->row_array();
			if ($get_count['it_count'] == 0) {
				$add_remove_list = array();
				$add_remove_list['task_id'] = $task_id;
				$add_remove_list['assignee_ids'] = $itas_value;
				$add_remove_list['staff_team_department'] = 'department';
				$add_remove_list['status'] = 'active';
				$add_remove_list['removed_workers_timing'] = '';
				$add_remove_list['added_removed_date'] = time();
				$add_remove_list['created_by'] = $_SESSION['id'];
				$add_remove_list['created_time'] = time();
				$this->Common_mdl->insert('task_assignee_add_remove_list', $add_remove_list);
			} else {
				$it_id = $get_count['dbid'];
				if ($get_count['status'] != 'active') {
					$add_remove_list = array();
					$add_remove_list['status'] = 'active';
					$add_remove_list['added_removed_date'] = $get_count['added_removed_date'] . "," . time();
					$this->Common_mdl->update('task_assignee_add_remove_list', $add_remove_list, 'id', $it_id);
				}
			}
		}
		/** for Department **/
		/** end of 20-10-2018 **/
	}
	public function admin_setting($user_id = false)
	{
		$this->Security_model->chk_login();

		if ($_SESSION['permission']['Admin_Settings']['view'] != 1) {
			$this->load->view('users/blank_page');
		} else {
			$data['currency']   = $this->db->query('select * from  crm_currency')->result_array();
			$data['admin_set']  = $this->db->query('select * from admin_setting where firm_id=' . $_SESSION['firm_id'])->row_array();
			$data['user_data']  = $this->db->query("select * from user where user_type='FA' and firm_id=" . $_SESSION['firm_id'])->row_array();
			$data['task_status'] = $this->Common_mdl->get_firm_progress();
			$data['countries']  = $this->Common_mdl->getallrecords('countries');
			$data['smtp_settings']  = $this->Common_mdl->select_record('mail_protocol_settings', 'firm_id', $_SESSION['firm_id']);
			$this->load->view('users/admin_profileSetting_view', $data);
		}
	}

	public function update_firm_admin_settings()
	{
		$this->Security_model->chk_login();
		$res['response'] = 0;

		if (!class_exists('Firm_setting_mdl')) $this->load->model('Firm_setting_mdl');

		if (!empty($this->input->post('company_name'))) {
			$res['response'] = $this->Firm_setting_mdl->update_admin_settings();
		}
		echo json_encode($res);
	}
	public function send_to_review()
	{
		$task_id = $_POST['task_id'];
		$role = $_POST['role'];
		$member = $_SESSION['id'];
		$break = 0;
		$ch = $this->db->query("select * from assign_member where assign_to_roll=$role")->result_array();
		if (count($ch) <= 0) return;

		do {
			if ($break == 300) break;

			$data = $this->db->query("select * from assign_member where CONCAT(',',`members_id`,',') REGEXP ',($member),'")->result_array();
			//echo "<pre>";print_r($data);die;
			$t = '';
			foreach ($data as $v) {
				if ($v['assign_to_roll'] == $role) {
					$t .= ($t == '') ? $v['assign_to_id'] : "," . $v['assign_to_id'];
				} else {
					$member .= ($member == '' ? $v['assign_to_id'] : "|" . $v['assign_to_id']);
				}
			}
			$break++;
		} while ($t == '');

		$this->db->insert('Manager_notification', ['manager_id' => $t, 'user_id' => $_SESSION['id'], 'task_id' => $task_id, 'status' => 0]);
		echo $this->db->affected_rows();
	}
	public function find_underlevel_users($role, $user_id)
	{
		do {
			if ($break == 1000) break;

			$data = $this->db->query("select * from assign_member where assign_to_id IN ($user_id)")->result_array();
			//echo "<pre>";print_r($data);die;
			$t = '';
			$atr = '';
			foreach ($data as $v) {
				if ($role == ($v['assign_to_roll'] + 1)) {
					$t .= ($t == '') ? $v['members_id'] : "," . $v['members_id'];
				} else {
					$atr .= ($atr == '' ? $v['members_id'] : "," . $v['members_id']);
				}
			}
			$user_id = $atr;
			$break++;
		} while ($t == '');

		return $this->db->query("select * from user where id IN ($t)")->result_array();
	}

	// all task list

	public function task_list($status = false){
		// echo '<pre>';
		// print_r($_REQUEST);
		// echo '</pre>';
		// die("Current Status -> " . $status );
		$this->Security_model->chk_login();

		$data['task_list'] = $assign_data = array();

		if ($_SESSION['permission']['Task']['view'] == 1){
			$task_status_id           = array('1', '2', '3');
			$condition                = " AND tl.related_to != 'sub_task' AND  tl.task_status in (1,2,3) ";
			$data['status_condition'] = 0;

			if ($status){
				$status                   = ($status != '' && $status == 'archive_task') ? '4' : '5';
				$condition                = " AND tl.task_status = " . $status . " ";
				$task_status_id           = array($status);
				$data['status_condition'] = $status;				
			}
			$sql                          = "SELECT tl.id,
													tl.task_status,
													( SELECT count(id) 
												FROM task_comments 
												WHERE task_id = tl.id
												AND (viewed IS NULL OR viewed NOT IN('4100'))            
											) as msg_count
											FROM add_new_task AS tl 
											LEFT JOIN client AS cl 
													ON cl.id=tl.company_name 
											LEFT JOIN client_contacts AS cc ON cc.client_id=tl.user_id
											WHERE tl.firm_id = " . $_SESSION['firm_id'] . "  AND cc.make_primary = 1 " . $condition .  " 
											ORDER BY tl.id DESC";
			$data['task_list']            = $this->db->query($sql)->result_array();
			$data['task_list_count']      = count($data['task_list']);
			// echo $data['task_list_count'];
			// exit;
			$get_datalist                 = array_column($data['task_list'], 'task_status');
			$data['get_datalist']         = array_count_values($get_datalist);
			$data['task_status']          = $this->db->query('
												SELECT 	id,
														status_name 
												FROM task_status 
												WHERE id in (' . implode(",", $task_status_id) . ')'
											)->result_array();
			$data['assign_task_status']   = $this->Common_mdl->dynamic_status('assign_task_status');
			$FIRM_ID 					  = $_SESSION['firm_id'];			
			$data['user_list']            = $this->db->query("
												SELECT OT.id,
													   OT.parent_id,
													   U.crm_name 
												FROM organisation_tree OT
												INNER JOIN user U
														ON U.id = OT.user_id
														AND U.user_type !='FC' 
														AND U.user_type !='SA' 
														AND U.status IN('1')
												WHERE OT.firm_id = $FIRM_ID"
											)->result_array();
			
			$userArr                      = [];
			$nestedUserArr                = [];

			foreach($data['user_list'] as $ul){				
				$userArr[$ul['id']]                         = $ul['crm_name'];
				$nestedUserArr[$ul['parent_id']][$ul['id']] = $ul['crm_name'];
			}
			$data['user_list']            = $userArr;
			$data['nested_user_list']     = $nestedUserArr;
			$data['progress_task_status'] = $this->Common_mdl->get_firm_progress();
			//$data['my_active_tasks']      = $this->my_active_tasks($_SESSION['id']);
			$data['status'] = $status;
		
			$this->load->view('users/task_list_view', $data);			
		} else {
			$this->load->view('users/blank_page');
		}
	}

	public function kill_timers(){
		$this->Security_model->chk_login();
		$user_id = $_SESSION['id'];
		$this_timer = $_REQUEST['this_timer'];
		$sql = "SELECT 	ITT.id,
						ITT.user_id,
						ITT.task_id,
						(CHAR_LENGTH(ITT.time_start_pause) - CHAR_LENGTH(REPLACE(ITT.time_start_pause, ',', '')) + 1) AS TotalValue,
						ANT.firm_id,
						ANT.subject,
						ANT.company_name,
						F.crm_company_name
				FROM individual_task_timer AS ITT
				INNER JOIN add_new_task AS ANT
				ON ANT.id = ITT.task_id
				INNER JOIN firm AS F
				ON F.firm_id=ANT.firm_id
				WHERE ITT.user_id = $user_id
				HAVING TotalValue % 2 = 1";
		
		$data = $this->db->query($sql)->result_array();
		$task_id = [];

		foreach($data as $key=>$val){
			$task_id[] = $val['task_id'];			
		}
		$task_ids = trim(implode(',',$task_id));
		
		if(!empty($task_ids)){
			$sql = "UPDATE individual_task_timer 
					SET time_start_pause=concat(time_start_pause, ',',UNIX_TIMESTAMP())
					WHERE task_id IN($task_ids)
					AND task_id != $this_timer
					AND user_id=$user_id";
		
			echo $this->db->query($sql);
		}else{
			echo 0;
		}
	}

	public function my_active_tasks(){
		$this->Security_model->chk_login();
		$user_id = $_SESSION['id'];
		//$this_timer = $_REQUEST['this_timer'];
		$sql = "SELECT 	ITT.id,
						ITT.user_id,
						ITT.task_id,
						(CHAR_LENGTH(ITT.time_start_pause) - CHAR_LENGTH(REPLACE(ITT.time_start_pause, ',', '')) + 1) AS TotalValue,
						ANT.firm_id,
						ANT.subject,
						ANT.company_name,
						F.crm_company_name
				FROM individual_task_timer AS ITT
				INNER JOIN add_new_task AS ANT
				ON ANT.id = ITT.task_id
				INNER JOIN firm AS F
				ON F.firm_id=ANT.firm_id
				WHERE ITT.user_id = $user_id
				-- AND ITT.task_id != $this_timer
				HAVING TotalValue % 2 = 1";
		
		$data = $this->db->query($sql)->result_array();
		echo json_encode($data);
	}

	public function long_running_tasks(){
		$this->Security_model->chk_login();

		$user_id 	= $_SESSION['id'];		
		/*$sql 	 	= "	SELECT 	ITT.id,
								ITT.user_id,
								ITT.task_id,
								(CHAR_LENGTH(ITT.time_start_pause) - CHAR_LENGTH(REPLACE(ITT.time_start_pause, ',', '')) + 1) AS TotalValue,
								SUBSTRING_INDEX(ITT.time_start_pause, ',', -1) AS start_time,
								UNIX_TIMESTAMP() - SUBSTRING_INDEX(ITT.time_start_pause, ',', -1) RunningFrom
					   	FROM individual_task_timer AS ITT
					   	WHERE ITT.user_id = $user_id
					   	HAVING TotalValue % 2 = 1
					   	AND RunningFrom >= 1080
					   	ORDER BY IIT.id DESC
					   	LIMIT 10";*/
		$sql 		= " SELECT task_id
						FROM active_timers 
						WHERE start_time <= UNIX_TIMESTAMP(TIMESTAMP(NOW() - INTERVAL 18 MINUTE))
						AND status = 1
						AND user_id = $user_id";
		$data 		= $this->db->query($sql)->result_array();
		$result_arr = [];

		foreach($data as $res){
			$task_id 				 = $res['task_id'];
			$sql 	 				 = "SELECT 	ANT.firm_id, ANT.subject, ANT.company_name, F.crm_company_name
										FROM add_new_task AS ANT
										INNER JOIN firm AS F ON ANT.firm_id = F.firm_id
										WHERE ANT.id = $task_id";
			$task_data 				 = $this->db->query($sql)->row_array();
			$res['firm_id'] 		 = $task_data['firm_id'];
			$res['subject'] 		 = $task_data['subject'];
			$res['company_name'] 	 = $task_data['company_name'];
			$res['crm_company_name'] = $task_data['crm_company_name'];
			$result_arr[] 			 = $res;
		}
		echo json_encode($result_arr);
	}

	public function kill_long_timers(){
		$this->Security_model->chk_login();
		$user_id = $_SESSION['id'];
		$sql = "SELECT 	ITT.id,
						ITT.user_id,
						ITT.task_id,
						(CHAR_LENGTH(ITT.time_start_pause) - CHAR_LENGTH(REPLACE(ITT.time_start_pause, ',', '')) + 1) AS TotalValue,
						SUBSTRING_INDEX(ITT.time_start_pause, ',', -1) AS start_time,
						UNIX_TIMESTAMP() - SUBSTRING_INDEX(ITT.time_start_pause, ',', -1) RunningFrom,
						ANT.firm_id,
						ANT.subject,
						ANT.company_name,
						F.crm_company_name
				FROM individual_task_timer AS ITT
				INNER JOIN add_new_task AS ANT
				ON ANT.id = ITT.task_id
				INNER JOIN firm AS F
				ON F.firm_id=ANT.firm_id
				WHERE ITT.user_id = $user_id
				HAVING TotalValue % 2 = 1
				AND RunningFrom >= 10800";

		$lrt = $this->db->query($sql)->result_array();
		$task_id = [];
		
		foreach($lrt as $v){
			$task_id[] = $v['task_id'];
		}
		$task_ids = trim(implode(',',$task_id));
		
		if(!empty($task_ids)){
			$sql = "UPDATE individual_task_timer 
					SET time_start_pause=concat(time_start_pause, ',',UNIX_TIMESTAMP())
					WHERE task_id IN($task_ids)
					AND user_id=$user_id";
		
			echo $this->db->query($sql);
		}else{
			echo 0;
		}
	}

	public function memper_active()
	{
		$this->Security_model->chk_login();

		if ($_SESSION['role'] <= 2) {

			$list = $this->db->query("select id from user where firm_admin_id=$_SESSION[id]")->result_array();
			$list = implode(',', array_column($list, 'id'));
			$track = $this->db->query("select itt.*,u.crm_name,ant.subject from individual_task_timer as itt left join user u on u.id=itt.user_id left join add_new_task ant on ant.id=itt.task_id where itt.user_id IN ($list)")->result_array();
			$live_work = array();

			foreach ($track as $k => $v) {
				if (count(explode(',', $v['time_start_pause'])) % 2) $live_work[] = $v;
			}
			$this->load->view("users/live_team_view", ['live_worker' => $live_work]);
		}
	}

	public function ajax_list()
	{
		$date = $this->input->post('today');

		if ($date != '' && $date == 'today') {
			$today = date("d-m-Y");
			$posts = $this->Common_mdl->getallrecords('add_new_task');
		} else {
			$posts = $this->Common_mdl->getallrecords('add_new_task');
		}
		$data = array();
		//$no = $this->input->post('start');
		foreach ($posts as $post) {
			$staff = $this->db->query("SELECT * FROM staff_form WHERE user_id in (" . $post['worker'] . ")")->result_array();
			// $no++;
			$row   = array();
			$row[] = '<input type="checkbox"  class="deleteRow" value="' . $post['id'] . '"/>';
			$row[] = date('Y-m-d H:i:s', $post['created_date']);
			$row[] = ucfirst($post['subject']);
			$row[] = $post['start_date'];
			$row[] = $post['end_date'];
			$row[] = $post['task_status'];
			$row[] = $post['priority'];

			foreach ($staff as $key => $val) {
				$rows = '<img class="user_imgs" src="' . base_url() . 'uploads/' . $val['profile'] . '" alt="img">';
			}
			$row[] = $rows;
			$row[] = '<p class="action_01">    
						<a href="' . base_url() . 'user/delete/' . $post['id'] . '" onclick="return confirm("Are you sure want to delete");"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>
						<a href="' . base_url() . 'user/update_task/' . $post['id'] . '"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>                                
					  </p>';
			$data[] = $row;
		}

		$output = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => count($posts),
			//"recordsFiltered" => $this->Datamodel->count_filtered($from,$to),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	public function proposal()
	{
		$this->load->view('users/proposal');
	}

	public function getRoleName($id)
	{
		$getrole = $this->Common_mdl->GetAllWithExceptField('Role', 'id', $id);
		return $getrole['role'];
	}

	public function firm_field()
	{
		error_reporting(0);
		$this->Security_model->chk_login();
		$data['edit_fields'] = $this->Common_mdl->getallrecords('tblcustomfields');
		$this->load->view('users/firm-field-settings', $data);
	}

	public function firm_field_check($show_old = 0)
	{
		//error_reporting(0);
		$this->Check_Login_User();
		if ($_SESSION['permission']['Firm_Fields_Settings']['view'] != 1) {
			$this->load->view('users/blank_page');
		} else {
			$firm_id 				  = $_SESSION['firm_id'];
			$data['mandatory_fields'] =  $this->FirmFields_model->GetMandatory_Feilds($firm_id);
			$Dynamic_service          =  $this->FirmFields_model->GetServiceSections($firm_id);
			$data['dynamic_service']  =  array_column($Dynamic_service, 'service_name', 'section');
			$data['section_data']     =  $this->FirmFields_model->GetSectionData($firm_id);

			$this->load->view('users/firm-field', $data);
		}
	}
	public function UpdateDefult_FirmField()
	{
		$this->Check_Login_User();
		$return['result'] = 0;
		if (!empty($_POST['id'])) {
			$return['result'] = $this->FirmFields_model->Update_FirmField($this->session->userdata('firm_id'));
		}
		echo json_encode($return);
	}
	public function firm_custom_fields_action()
	{
		$this->Check_Login_User();
		if (!empty($_POST['id'])) {
			$result = $this->FirmFields_model->Update_FirmField($this->session->userdata('firm_id'));
			echo $result;
		} else {
			$_POST['firm_id'] = $_SESSION['firm_id'];
			$id = $this->Common_mdl->add($this->input->post());
			if ($id) {
				echo '1';
			} else {
				echo '0';
			}
		}
	}
	public function custom_firmField_info()
	{
		if (!empty($_POST['id'])) {
			$data = $this->db->query("SELECT a.id,a.field_propriety,a.label,b.type,b.options,b.required FROM client_fields_management AS a INNER JOIN tblcustomfields AS b ON b.id=a.field_propriety WHERE a.id=" . $_POST['id'])->row_array();
			echo json_encode($data);
		}
	}
	public function update_firm_field_order()
	{
		$this->Check_Login_User();

		if (!empty($_POST['datas'])) {
			$this->FirmFields_model->UpdateOrder_FirmFields($this->session->userdata('firm_id'));
		}
		echo 1;
	}


	public function delete_firm_field()
	{
		$this->Check_Login_User();
		$return = 0;
		if (!empty($_POST['id'])) {
			$return = $this->FirmFields_model->Delete_FirmFields($this->session->userdata('firm_id'));
		}
		echo $return;


		/*  if ($id == '') {
            $title = _l('add_new', _l('custom_field_lowercase'));
        } else {
            $data['custom_field'] = $this->Common_mdl->get($id);
            $title                = _l('edit', _l('custom_field_lowercase'));
        }
        $data['pdf_fields']           = $this->pdf_fields;
        $data['client_portal_fields'] = $this->client_portal_fields;
        $data['client_editable_fields'] = $this->client_editable_fields;
        $data['title']                = $title;*/
		// $this->load->view('admin/custom_fields/customfield', $data);
		//$this->load->view('users/new_field');
	}
	// edit fields










	//sankar - 29-01-2018

	public function managed_person_list()
	{
		//$sql = 'SELECT * FROM Managed_by_person';
		$data['details'] = $this->Common_mdl->getallrecords('Managed_by_person');
		$this->load->view('users/managed_person_list', $data);
	}

	public function managed_delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('Managed_by_person');
		redirect(base_url('user/managed_person_list'));
	}

	public function managed_edit($id)
	{
		$data['product'] = $this->db->get_where('Managed_by_person', array('id' => $id))->row_array();

		$this->load->view('users/managed_person_edit', $data);
	}


	/**
	 * Update Data from this method.
	 *
	 * @return Response
	 */
	public function managed_update($id)
	{
		$products = new ProductsModel;
		$products->update_product($id);
		redirect(base_url('products'));
	}

	public function managed_person()
	{

		$data = array();
		$userData = array();

		if ($this->input->post('person') != '') {

			$this->form_validation->set_rules('name', 'Name', 'required');


			$userData = array(
				'name' => $this->input->post('name'),
				'createdTime' => time()

			);

			if ($this->form_validation->run() == true) {
				$insert = $this->db->insert('Managed_by_person', $userData);
				if ($insert != '') {

					echo "<script type='text/javascript'>alert('data added');</script>";
					redirect('user/managed_person');
				} else {

					echo "<script type='text/javascript'>alert('Problem Occured,Try again');</script>";
				}
			}
		}

		$data['user'] = $userData;

		$this->load->view('users/managed_person', $data);
	}

	public function emailreminder_cron()
	{

		$queries = $this->Common_mdl->get_price('admin_setting', 'id', '1', 'service_reminder');
		if ($queries == '1') {


			$admin = $this->Common_mdl->GetAllWithWhere('admin_setting', 'user_id', '1');
			$curr_time = date("h:i A", time());
			if ($admin[0]['client_reminder_time'] == '10:30 PM') {

				echo "accounts reminders";
				$records = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_ch_accounts_next_due!="0" AND crm_email!="0" AND DATE(crm_ch_accounts_next_due) > DATE_FORMAT(NOW(),"%Y-%m-%d") ')->result_array();
				//$accounts=$this->db->query('SELECT * FROM reminder_setting WHERE service_type="accounts" and reminder_block=0')->result_array();


				foreach ($records as $key => $value) {

					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_ch_accounts_next_due']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');


					$old_record[] = $value;

					$query = $this->db->query('select * from user where id="' . $value['user_id'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];
					$accounts = $this->db->query('SELECT * FROM reminder_setting WHERE service_type="accounts" and user_id=' . $value['user_id'] . '')->row_array();


					$message = $accounts['message'];
					$a = array(
						':: Client Name::' => $query['crm_name'],
						':: Service Name::' => 'Accounts',
						':: service Date::' => $value['crm_ch_yearend'],
						':: service Due Date::' => $value['crm_ch_accounts_next_due'],
						'::Client Username::' => $query['username'],
						'::Client Company::' => $value['crm_company_name'],
						' :: Date ::' => date('Y-m-d')
					);

					$email_content = strtr($message, $a);
					$email_content_sms = strtr($message, $a);
					echo $value['crm_email'] . '<br>';
					echo $diff . '<br>';

					if (($query['username'] != '') && ($diff == $accounts['days'])) {

						$this->send_sms($to_number, $email_content_sms);
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = $accvalue['subject'];
						$data2['email_contents']  = '<a href=' . base_url() . ' style="display: inline-block;text-align: center;color: #00aeef;text-decoration: none;font-family:;background: #00aeef;color: #fff;font-weight: 600;padding: 5px 10px;border-radius: 3px;">Click Here</a><p>' . $email_content . '</p>';
						$data2['title'] = 'Accounts Statement Remainder Mail';
						//email template
						$body = $this->load->view('remainder_email.php', $data2, TRUE);
						//redirect('login/email_format');
						//echo $email_content;
						//die;

						$contact_data = $this->Common_mdl->GetAllWithWheretwo('client_contacts', 'client_id', $id, 'make_primary', 1);
						$this->load->library('email');
						$this->email->set_mailtype("html");
						//$this->mailsettings();
						$this->email->from('info.techleaf@gmail.com');
						$this->email->to($contact_data[0]['main_email']);
						$this->email->subject($email_subject);
						$this->email->message($body);
						$send = $this->email->send();
						if ($send) {
							echo 'Accounts email remainder send';
							echo "<br>";

							$this->session->set_flashdata('success', "Kindly Check Your Mail Id");
						} else {
							echo 'Accounts email remainder not send';
							echo '<br>';
							$this->session->set_flashdata('warning', "Mail Not sent");
						}
					}

					// }//foreach close

				} // foreach close

				//exit;

				// confirmation reminders
				$confirmation = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_confirmation_statement_due_date!="0" AND crm_email!="0" AND DATE(crm_confirmation_statement_due_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") and reminder_block=0')->result_array();
				//$confirmation_re=$this->db->query('SELECT * FROM reminder_setting WHERE service_type="confirmation"')->result_array();

				foreach ($confirmation as $key => $value) {

					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_confirmation_statement_due_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');

					$old_record[] = $value;

					$query = $this->db->query('select * from user where id="' . $value['user_id'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];

					//$a  = array('{crm_confirmation_statement_date}'=>$value['crm_confirmation_statement_date'],'{crm_confirmation_statement_due_date}'=>$value['crm_confirmation_statement_due_date'],'{crm_confirmation_add_custom_reminder}'=>$value['crm_confirmation_add_custom_reminder']);
					//foreach ($confirmation_re as $acckey => $accvalue) {

					$confirmation_re = $this->db->query('SELECT * FROM reminder_setting WHERE service_type="confirmation" and user_id=' . $value['user_id'] . '')->row_array();
					$message = $confirmation_re['message'];
					$a = array(
						':: Client Name::' => $query['crm_name'],
						':: Service Name::' => 'Accounts',
						':: service Date::' => $value['crm_ch_yearend'],
						':: service Due Date::' => $value['crm_ch_accounts_next_due'],
						'::Client Username::' => $query['username'],
						'::Client Company::' => $value['crm_company_name'],
						' :: Date ::' => date('Y-m-d')
					);

					$email_content = strtr($message, $a);
					$email_content_sms = strtr($message, $a);


					if (($query['username'] != '') && ($diff == $confirmation_re['days'])) {
						$this->send_sms($to_number, $email_content_sms);
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For Confirmation';
						$data2['email_contents']  = '<a href=' . base_url() . ' style="display: inline-block;text-align: center;color: #00aeef;text-decoration: none;font-family:;background: #00aeef;color: #fff;font-weight: 600;padding: 5px 10px;border-radius: 3px;">Click Here</a><p>' . $email_content . '</p>';
						$data2['title'] = 'Confirmation Statement Remainder Mail';
						//email template
						$body = $this->load->view('remainder_email.php', $data2, TRUE);

						$contact_data = $this->Common_mdl->GetAllWithWheretwo('client_contacts', 'client_id', $id, 'make_primary', 1);
						//redirect('login/email_format');
						//echo $email_content;
						//die;
						$this->load->library('email');
						$this->email->set_mailtype("html");
						//$this->mailsettings();
						$this->email->from('info.techleaf@gmail.com');
						$this->email->to($contact_data[0]['main_email']);
						$this->email->subject($email_subject);
						$this->email->message($body);
						$send = $this->email->send();
						if ($send) {
							echo 'confirmation email remainder send';
							echo '<br>';

							$this->session->set_flashdata('success', "Kindly Check Your Mail Id");
						} else {
							echo 'confirmation email remainder not send';
							echo '<br>';
							$this->session->set_flashdata('warning', "Mail Not sent");
						}
					}

					//}// foreach close
				} // foreach close

				// Tax Return  -- Accounts Due Date - HMRC


				$tax_return = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_accounts_due_date_hmrc) > DATE_FORMAT(NOW(),"%Y-%m-%d") and reminder_block=0')->result_array();
				//$taxreturn_re=$this->db->query('SELECT * FROM reminder_setting WHERE service_type="tax_return"')->result_array();

				foreach ($tax_return as $key => $value) {

					(isset(json_decode($value['company_tax_return'])->reminder) && $value['company_tax_return'] != '') ? $jsnAcc =  json_decode($value['company_tax_return'])->reminder : $jsnAcc = '';



					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_accounts_due_date_hmrc']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');

					$old_record[] = $value;

					$query = $this->db->query('select * from user where crm_email_id="' . $value['user_id'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];

					// $a  = array('{crm_ch_yearend}'=>$value['crm_ch_yearend'],'{crm_accounts_due_date_hmrc}'=>$value['crm_accounts_due_date_hmrc'],'{crm_accounts_custom_reminder}'=>$value['crm_accounts_custom_reminder']);
					// foreach ($taxreturn_re as $acckey => $accvalue) {
					$taxreturn_re = $this->db->query('SELECT * FROM reminder_setting WHERE service_type="tax_return" and user_id=' . $value['user_id'] . '')->row_array();

					$message = $taxreturn_re['message'];

					$a = array(
						':: Client Name::' => $query['crm_name'],
						':: Service Name::' => 'Accounts',
						':: service Date::' => $value['crm_ch_yearend'],
						':: service Due Date::' => $value['crm_ch_accounts_next_due'],
						'::Client Username::' => $query['username'],
						'::Client Company::' => $value['crm_company_name'],
						' :: Date ::' => date('Y-m-d')
					);

					$email_content = strtr($message, $a);
					$email_content_sms = strtr($message, $a);
					// $email_content1    = $accvalue['message'];
					// $email_content  = strtr($email_content1,$a);

					if (($query['username'] != '') && ($diff == $taxreturn_re['days']) && $jsnAcc == 'on') {
						$this->send_sms($to_number, $email_content_sms);
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For Tax Return';
						$data2['email_contents']  = '<a href=' . base_url() . ' style="display: inline-block;text-align: center;color: #00aeef;text-decoration: none;font-family:;background: #00aeef;color: #fff;font-weight: 600;padding: 5px 10px;border-radius: 3px;">Click Here</a><p>' . $email_content . '</p>';
						$data2['title'] = 'Accounts Tax Return Remainder Mail';
						//email template
						$body = $this->load->view('remainder_email.php', $data2, TRUE);
						$contact_data = $this->Common_mdl->GetAllWithWheretwo('client_contacts', 'client_id', $id, 'make_primary', 1);
						//redirect('login/email_format');
						//echo $email_content;
						//die;
						$this->load->library('email');
						$this->email->set_mailtype("html");
						//$this->mailsettings();
						$this->email->from('info.techleaf@gmail.com');
						$this->email->to($contach_data[0]['main_email']);
						$this->email->subject($email_subject);
						$this->email->message($body);
						$send = $this->email->send();
						if ($send) {
							echo 'Tax Return email remainder send';
							echo '<br>';

							$this->session->set_flashdata('success', "Kindly Check Your Mail Id");
						} else {
							echo 'Tax Return email remainder not send';
							echo '<br>';
							$this->session->set_flashdata('warning', "Mail Not sent");
						}
					}
					//} // foreach close
				} // foreach close

				// Personal Tax Return  -- Tax Return Date
				$personal_tax_return = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_personal_tax_return_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") and reminder_block=0')->result_array();
				//$personaltax_re=$this->db->query('SELECT * FROM reminder_setting WHERE service_type="personal_tax"')->result_array();

				foreach ($personal_tax_return as $key => $value) {


					(isset(json_decode($value['personal_tax_return'])->reminder) && $value['personal_tax_return'] != '') ? $jsnpertax =  json_decode($value['personal_tax_return'])->reminder : $jsnpertax = '';



					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_personal_tax_return_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					/*echo $diff;
    echo '<br>';
    echo $jsnpertax;
    exit;*/

					$old_record[] = $value;

					$query = $this->db->query('select * from user where id="' . $value['user_id'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];

					// $a  = array('{crm_personal_tax_return_date}'=>$value['crm_personal_tax_return_date'],'{crm_personal_custom_reminder}'=>$value['crm_personal_custom_reminder']);
					// foreach ($personaltax_re as $acckey => $accvalue) {

					$personaltax_re = $this->db->query('SELECT * FROM reminder_setting WHERE service_type="personal_tax" and user_id=' . $value['user_id'] . '')->row_array();

					$message = $personaltax_re['message'];

					$a = array(
						':: Client Name::' => $query['crm_name'],
						':: Service Name::' => 'Accounts',
						':: service Date::' => $value['crm_ch_yearend'],
						':: service Due Date::' => $value['crm_ch_accounts_next_due'],
						'::Client Username::' => $query['username'],
						'::Client Company::' => $value['crm_company_name'],
						' :: Date ::' => date('Y-m-d')
					);

					$email_content = strtr($message, $a);
					$email_content_sms = strtr($message, $a);

					if (($query['username'] != '') && ($diff == $personaltax_re['days']) && $jsnpertax == 'on') {
						$this->send_sms($to_number, $email_content_sms);
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For Personal Tax Return';
						$data2['email_contents']  = '<a href=' . base_url() . ' style="display: inline-block;text-align: center;color: #00aeef;text-decoration: none;font-family:;background: #00aeef;color: #fff;font-weight: 600;padding: 5px 10px;border-radius: 3px;">Click Here</a><p>' . $email_content . '</p>';
						$data2['title'] = 'Personal Tax Return Remainder Mail';
						$body = $this->load->view('remainder_email.php', $data2, TRUE);

						$contact_data = $this->Common_mdl->GetAllWithWheretwo('client_contacts', 'client_id', $id, 'make_primary', 1);
						//redirect('login/email_format');
						//echo $email_content;
						//die;
						$this->load->library('email');
						$this->email->set_mailtype("html");
						//$this->mailsettings();
						$this->email->from('info.techleaf@gmail.com');
						$this->email->to($conatct_data[0]['main_email']);
						$this->email->subject($email_subject);
						$this->email->message($body);
						$send = $this->email->send();
						if ($send) {
							echo 'Personal Tax Return email remainder send';
							echo '<br>';

							$this->session->set_flashdata('success', "Kindly Check Your Mail Id");
						} else {
							echo 'Personal Tax Return email remainder not send';
							echo '<br>';
							$this->session->set_flashdata('warning', "Mail Not sent");
						}
					}

					//   } // foreach close

				} // foreach close

				// payroll --Payroll Run Date

				$Payroll = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_payroll_run_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") and reminder_block=0')->result_array();
				//$payroll_re=$this->db->query('SELECT * FROM reminder_setting WHERE service_type="payroll"')->result_array();

				foreach ($Payroll as $key => $value) {


					(isset(json_decode($value['payroll'])->reminder) && $value['payroll'] != '') ? $jsnpay =  json_decode($value['payroll'])->reminder : $jsnpay = '';



					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_payroll_run_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					/*echo $diff;
    echo '<br>';
    echo $jsnpertax;
    exit;*/

					$old_record[] = $value;

					$query = $this->db->query('select * from user where id="' . $value['user_id'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];

					$payroll_re = $this->db->query('SELECT * FROM reminder_setting WHERE service_type="payroll" and user_id=' . $value['user_id'] . '')->row_array();
					$message = $payroll_re['message'];

					$a = array(
						':: Client Name::' => $query['crm_name'],
						':: Service Name::' => 'Accounts',
						':: service Date::' => $value['crm_ch_yearend'],
						':: service Due Date::' => $value['crm_ch_accounts_next_due'],
						'::Client Username::' => $query['username'],
						'::Client Company::' => $value['crm_company_name'],
						' :: Date ::' => date('Y-m-d')
					);

					$email_content = strtr($message, $a);
					$email_content_sms = strtr($message, $a);
					if (($query['username'] != '') && ($diff == $payroll_re['days']) && $jsnpay == 'on') {
						$this->send_sms($to_number, $email_content_sms);
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For Payroll Run Date';
						$data2['email_contents']  = '<a href=' . base_url() . ' style="display: inline-block;text-align: center;color: #00aeef;text-decoration: none;font-family:;background: #00aeef;color: #fff;font-weight: 600;padding: 5px 10px;border-radius: 3px;">Click Here</a><p>' . $email_content . '</p>';
						$data2['title'] = 'Payroll Run Date Remainder Mail';
						$body = $this->load->view('remainder_email.php', $data2, TRUE);
						$contact_data = $this->Common_mdl->GetAllWithWheretwo('client_contacts', 'client_id', $id, 'make_primary', 1);
						$this->load->library('email');
						$this->email->set_mailtype("html");
						//$this->mailsettings();
						$this->email->from('info.techleaf@gmail.com');
						$this->email->to($contact_data[0]['main_email']);
						$this->email->subject($email_subject);
						$this->email->message($body);
						$send = $this->email->send();
						if ($send) {
							echo 'Payroll Run Date email remainder send';
							echo '<br>';

							$this->session->set_flashdata('success', "Kindly Check Your Mail Id");
						} else {
							echo 'Payroll Run Date email remainder not send';
							echo '<br>';
							$this->session->set_flashdata('warning', "Mail Not sent");
						}
					}
				} // foreach close

				//WorkPlace Pension-AE -- Pension Submission Due date
				$Pension_ae = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_pension_subm_due_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") and reminder_block=0')->result_array();
				//$pension_re=$this->db->query('SELECT * FROM reminder_setting WHERE service_type="pension"')->result_array();

				foreach ($Pension_ae as $key => $value) {


					(isset(json_decode($value['workplace'])->reminder) && $value['workplace'] != '') ? $jsnwork =  json_decode($value['workplace'])->reminder : $jsnwork = '';



					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_pension_subm_due_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');


					$old_record[] = $value;

					$query = $this->db->query('select * from user where id="' . $value['user_id'] . '"')->row_array();

					$to_number = $query['crm_phone_number'];
					$pension_re = $this->db->query('SELECT * FROM reminder_setting WHERE service_type="pension" and user_id=' . $value['user_id'] . '')->result_array();

					$message = $pension_re['message'];

					$a = array(
						':: Client Name::' => $query['crm_name'],
						':: Service Name::' => 'Accounts',
						':: service Date::' => $value['crm_ch_yearend'],
						':: service Due Date::' => $value['crm_ch_accounts_next_due'],
						'::Client Username::' => $query['username'],
						'::Client Company::' => $value['crm_company_name'],
						' :: Date ::' => date('Y-m-d')
					);

					$email_content = strtr($message, $a);
					$email_content_sms = strtr($message, $a);

					if (($query['username'] != '') && ($diff == $pension_re['days']) && $jsnwork == 'on') {
						$this->send_sms($to_number, $email_content_sms);
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For Pension Submission Due date';
						$data2['email_contents']  = '<a href=' . base_url() . ' style="display: inline-block;text-align: center;color: #00aeef;text-decoration: none;font-family:;background: #00aeef;color: #fff;font-weight: 600;padding: 5px 10px;border-radius: 3px;">Click Here</a><p>' . $email_content . '</p>';
						$data2['title'] = 'Pension Submission Due Date Remainder Mail';

						$contact_data = $this->Common_mdl->GetAllWithWheretwo('client_contacts', 'client_id', $id, 'make_primary', 1);
						//$data2['link']=base_url();
						//email template
						$body = $this->load->view('remainder_email.php', $data2, TRUE);
						//redirect('login/email_format');
						//echo $email_content;
						//die;
						$this->load->library('email');
						$this->email->set_mailtype("html");
						//$this->mailsettings();
						$this->email->from('info.techleaf@gmail.com');
						$this->email->to($contact_data[0]['main_email']);
						$this->email->subject($email_subject);
						$this->email->message($body);
						$send = $this->email->send();
						if ($send) {
							echo 'Pension Submission Due date email remainder send';
							echo '<br>';

							$this->session->set_flashdata('success', "Kindly Check Your Mail Id");
						} else {
							echo 'Pension Submission Due date email remainder not send';
							echo '<br>';
							$this->session->set_flashdata('warning', "Mail Not sent");
						}
					}
					//} // foreach close
				} // foreach close

				//WorkPlace Pension-AE --Declaration of Compliance Due
				$Pension_dec = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_declaration_of_compliance_due_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") and reminder_block=0')->result_array();
				//$declaration_re=$this->db->query('SELECT * FROM reminder_setting WHERE service_type="declaration"')->result_array();

				foreach ($Pension_dec as $key => $value) {


					(isset(json_decode($value['workplace'])->reminder) && $value['workplace'] != '') ? $jsnworkde =  json_decode($value['workplace'])->reminder : $jsnworkde = '';



					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_declaration_of_compliance_due_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					/*echo $diff;
    echo '<br>';
    echo $jsnpertax;
    exit;*/

					$old_record[] = $value;

					$query = $this->db->query('select * from user where id="' . $value['user_id'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];

					// $a  = array('{crm_declaration_of_compliance_due_date}'=>$value['crm_declaration_of_compliance_due_date'],'{crm_pension_add_custom_reminder}'=>$value['crm_pension_add_custom_reminder']);
					// foreach ($declaration_re as $acckey => $accvalue) {
					$declaration_re = $this->db->query('SELECT * FROM reminder_setting WHERE service_type="declaration" and user_id=' . $value['user_id'] . '')->row_array();

					$message = $declaration_re['message'];

					$a = array(
						':: Client Name::' => $query['crm_name'],
						':: Service Name::' => 'Accounts',
						':: service Date::' => $value['crm_ch_yearend'],
						':: service Due Date::' => $value['crm_ch_accounts_next_due'],
						'::Client Username::' => $query['username'],
						'::Client Company::' => $value['crm_company_name'],
						' :: Date ::' => date('Y-m-d')
					);

					$email_content = strtr($message, $a);
					$email_content_sms = strtr($message, $a);
					// $email_content1    = $accvalue['message'];
					// $email_content  = strtr($email_content1,$a);

					if (($query['username'] != '') && ($diff == $declaration_re['days']) && $jsnworkde == 'on') {
						$this->send_sms($to_number, $email_content_sms);
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For Declaration of Compliance Due Date';
						$data2['email_contents']  = '<a href=' . base_url() . ' style="display: inline-block;text-align: center;color: #00aeef;text-decoration: none;font-family:;background: #00aeef;color: #fff;font-weight: 600;padding: 5px 10px;border-radius: 3px;">Click Here</a><p>' . $email_content . '</p>';
						$data2['title'] = 'Declaration of Compliance Due Date Remainder Mail';

						$contact_data = $this->Common_mdl->GetAllWithWheretwo('client_contacts', 'client_id', $id, 'make_primary', 1);
						//$data2['link']=base_url();
						//email template
						$body = $this->load->view('remainder_email.php', $data2, TRUE);
						//redirect('login/email_format');
						//echo $email_content;
						//die;
						$this->load->library('email');
						$this->email->set_mailtype("html");
						//$this->mailsettings();
						$this->email->from('info.techleaf@gmail.com');
						$this->email->to($contact_data[0]['main_email']);
						$this->email->subject($email_subject);
						$this->email->message($body);
						$send = $this->email->send();
						if ($send) {
							echo 'Declaration of Compliance Due date email remainder send';
							echo '<br>';

							$this->session->set_flashdata('success', "Kindly Check Your Mail Id");
						} else {
							echo 'Declaration of Compliance Due date email remainder not send';
							echo '<br>';
							$this->session->set_flashdata('warning', "Mail Not sent");
						}
					}
					//} // foreach close
				} // foreach close

				// CIS Contractor --  Start Date

				$cis_contractor = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_cis_contractor_start_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") and reminder_block=0')->result_array();
				//$cis_re=$this->db->query('SELECT * FROM reminder_setting WHERE service_type="cis"')->result_array();

				foreach ($cis_contractor as $key => $value) {


					(isset(json_decode($value['cis'])->reminder) && $value['cis'] != '') ? $jsncontra =  json_decode($value['cis'])->reminder : $jsncontra = '';



					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_cis_contractor_start_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					/*echo $diff;
    echo '<br>';
    echo $jsnpertax;
    exit;*/

					$old_record[] = $value;

					$query = $this->db->query('select * from user where id="' . $value['user_id'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];

					// $a  = array('{crm_cis_contractor_start_date}'=>$value['crm_cis_contractor_start_date'],'{crm_cis_add_custom_reminder}'=>$value['crm_cis_add_custom_reminder']);
					// foreach ($cis_re as $acckey => $accvalue) {

					$cis_re = $this->db->query('SELECT * FROM reminder_setting WHERE service_type="cis" and user_id=' . $value['user_id'] . '')->row_array();

					$message = $cis_re['message'];

					$a = array(
						':: Client Name::' => $query['crm_name'],
						':: Service Name::' => 'Accounts',
						':: service Date::' => $value['crm_ch_yearend'],
						':: service Due Date::' => $value['crm_ch_accounts_next_due'],
						'::Client Username::' => $query['username'],
						'::Client Company::' => $value['crm_company_name'],
						' :: Date ::' => date('Y-m-d')
					);

					$email_content = strtr($message, $a);
					$email_content_sms = strtr($message, $a);

					if (($query['username'] != '') && ($diff == $cis_re['days']) && $jsncontra == 'on') {
						$this->send_sms($to_number, $email_content_sms);
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For C.I.S Contractor Start Date';
						$data2['email_contents']  = '<a href=' . base_url() . ' style="display: inline-block;text-align: center;color: #00aeef;text-decoration: none;font-family:;background: #00aeef;color: #fff;font-weight: 600;padding: 5px 10px;border-radius: 3px;">Click Here</a><p>' . $email_content . '</p>';
						$data2['title'] = 'C.I.S Contractor Start Date Remainder Mail';

						$contact_data = $this->Common_mdl->GetAllWithWheretwo('client_contacts', 'client_id', $id, 'make_primary', 1);
						//$data2['link']=base_url();
						//email template
						$body = $this->load->view('remainder_email.php', $data2, TRUE);
						//redirect('login/email_format');
						//echo $email_content;
						//die;
						$this->load->library('email');
						$this->email->set_mailtype("html");
						//$this->mailsettings();
						$this->email->from('info.techleaf@gmail.com');
						$this->email->to($contact_data[0]['main_email']);
						$this->email->subject($email_subject);
						$this->email->message($body);
						$send = $this->email->send();
						if ($send) {
							echo 'C.I.S Contractor Start Date email remainder send';
							echo '<br>';

							$this->session->set_flashdata('success', "Kindly Check Your Mail Id");
						} else {
							echo 'C.I.S Contractor Start Date email remainder not send';
							echo '<br>';
							$this->session->set_flashdata('warning', "Mail Not sent");
						}
					}
					//} // foreach close
				} // foreach close
				// CIS Sub Contractor --  Start Date

				$cis_subcontractor = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_cis_subcontractor_start_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") and reminder_block=0')->result_array();
				//$cis_sub_re=$this->db->query('SELECT * FROM reminder_setting WHERE service_type="cis_sub"')->result_array();

				foreach ($cis_subcontractor as $key => $value) {


					(isset(json_decode($value['cissub'])->reminder) && $value['cissub'] != '') ? $jsncontrasub =  json_decode($value['cissub'])->reminder : $jsncontrasub = '';



					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_cis_subcontractor_start_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					/*echo $diff;
    echo '<br>';
    echo $jsnpertax;
    exit;*/

					$old_record[] = $value;

					$query = $this->db->query('select * from user where id="' . $value['user_id'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];

					// $a  = array('{crm_cis_subcontractor_start_date}'=>$value['crm_cis_subcontractor_start_date'],'{crm_cis_add_custom_reminder}'=>$value['crm_cis_add_custom_reminder']);
					// foreach ($cis_sub_re as $acckey => $accvalue) {
					$cis_sub_re = $this->db->query('SELECT * FROM reminder_setting WHERE service_type="cis_sub" and user_id=' . $value['user_id'] . '')->row_array();

					$message = $cis_sub_re['message'];

					$a = array(
						':: Client Name::' => $query['crm_name'],
						':: Service Name::' => 'Accounts',
						':: service Date::' => $value['crm_ch_yearend'],
						':: service Due Date::' => $value['crm_ch_accounts_next_due'],
						'::Client Username::' => $query['username'],
						'::Client Company::' => $value['crm_company_name'],
						' :: Date ::' => date('Y-m-d')
					);

					$email_content = strtr($message, $a);
					$email_content_sms = strtr($message, $a);

					if (($query['username'] != '') && ($diff == $cis_sub_re['days']) && $jsncontrasub == 'on') {
						$this->send_sms($to_number, $email_content_sms);
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For C.I.S Subcontractor Start Date';
						$data2['email_contents']  = '<a href=' . base_url() . ' style="display: inline-block;text-align: center;color: #00aeef;text-decoration: none;font-family:;background: #00aeef;color: #fff;font-weight: 600;padding: 5px 10px;border-radius: 3px;">Click Here</a><p>' . $email_content . '</p>';
						$data2['title'] = 'C.I.S Subcontractor Start Date Remainder Mail';
						$contact_data = $this->Common_mdl->GetAllWithWheretwo('client_contacts', 'client_id', $id, 'make_primary', 1);
						//$data2['link']=base_url();
						//email template
						$body = $this->load->view('remainder_email.php', $data2, TRUE);
						//redirect('login/email_format');
						//echo $email_content;
						//die;
						$this->load->library('email');
						$this->email->set_mailtype("html");
						//$this->mailsettings();
						$this->email->from('info.techleaf@gmail.com');
						$this->email->to($contact_data[0]['main_email']);
						$this->email->subject($email_subject);
						$this->email->message($body);
						$send = $this->email->send();
						if ($send) {
							echo 'C.I.S Subcontractor Start Date email remainder send';
							echo '<br>';

							$this->session->set_flashdata('success', "Kindly Check Your Mail Id");
						} else {
							echo 'C.I.S Subcontractor Start Date email remainder not send';
							echo '<br>';
							$this->session->set_flashdata('warning', "Mail Not sent");
						}
					}
					//} // foreach close
				} // foreach close

				// P11D -- P11D Due date

				$p11d = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_next_p11d_return_due) > DATE_FORMAT(NOW(),"%Y-%m-%d") and reminder_block=0')->result_array();
				//$p11d_re=$this->db->query('SELECT * FROM reminder_setting WHERE service_type="p11d"')->result_array();

				foreach ($p11d as $key => $value) {


					(isset(json_decode($value['p11d'])->reminder) && $value['p11d'] != '') ? $jsnp11d =  json_decode($value['p11d'])->reminder : $jsnp11d = '';



					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_next_p11d_return_due']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					/*echo $diff;
    echo '<br>';
    echo $jsnpertax;
    exit;*/

					$old_record[] = $value;

					$query = $this->db->query('select * from user where id="' . $value['user_id'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];

					// $a  = array('{crm_next_p11d_return_due}'=>$value['crm_next_p11d_return_due'],'{crm_p11d_add_custom_reminder}'=>$value['crm_p11d_add_custom_reminder']);
					// foreach ($p11d_re as $acckey => $accvalue) {

					$p11d_re = $this->db->query('SELECT * FROM reminder_setting WHERE service_type="p11d" and user_id=' . $value['user_id'] . '')->row_array();

					$message = $p11d_re['message'];

					$a = array(
						':: Client Name::' => $query['crm_name'],
						':: Service Name::' => 'Accounts',
						':: service Date::' => $value['crm_ch_yearend'],
						':: service Due Date::' => $value['crm_ch_accounts_next_due'],
						'::Client Username::' => $query['username'],
						'::Client Company::' => $value['crm_company_name'],
						' :: Date ::' => date('Y-m-d')
					);

					$email_content = strtr($message, $a);
					$email_content_sms = strtr($message, $a);

					if (($query['username'] != '') && ($diff == $p11d_re['days']) && $jsnp11d == 'on') {

						$this->send_sms($to_number, $email_content_sms);
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For P11D Due Date';
						$data2['email_contents']  = '<a href=' . base_url() . ' style="display: inline-block;text-align: center;color: #00aeef;text-decoration: none;font-family:;background: #00aeef;color: #fff;font-weight: 600;padding: 5px 10px;border-radius: 3px;">Click Here</a><p>' . $email_content . '</p>';
						$data2['title'] = 'P11D Due Date Remainder Mail';
						$contact_data = $this->Common_mdl->GetAllWithWheretwo('client_contacts', 'client_id', $id, 'make_primary', 1);
						//$data2['link']=base_url();
						//email template
						$body = $this->load->view('remainder_email.php', $data2, TRUE);
						//redirect('login/email_format');
						//echo $email_content;
						//die;
						$this->load->library('email');
						$this->email->set_mailtype("html");
						//$this->mailsettings();
						$this->email->from('info.techleaf@gmail.com');
						$this->email->to($contact_data[0]['main_email']);
						$this->email->subject($email_subject);
						$this->email->message($body);
						$send = $this->email->send();
						if ($send) {
							echo 'P11D Due Date email remainder send';
							echo '<br>';

							$this->session->set_flashdata('success', "Kindly Check Your Mail Id");
						} else {
							echo 'P11D Due Date email remainder not send';
							echo '<br>';
							$this->session->set_flashdata('warning', "Mail Not sent");
						}
					}
					//} // foreach close
				} // foreach close

				// Bookkeeping --  Next Bookkeeping Date

				$Bookkeeping = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_next_booking_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") and reminder_block=0')->result_array();
				//$bookkeep_re=$this->db->query('SELECT * FROM reminder_setting WHERE service_type="bookkeep"')->result_array();

				foreach ($Bookkeeping as $key => $value) {


					(isset(json_decode($value['bookkeep'])->reminder) && $value['bookkeep'] != '') ? $jsnbookkeep =  json_decode($value['bookkeep'])->reminder : $jsnbookkeep = '';



					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_next_booking_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					/*echo $diff;
    echo '<br>';
    echo $jsnpertax;
    exit;*/

					$old_record[] = $value;

					$query = $this->db->query('select * from user where id="' . $value['user_id'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];

					// $a  = array('{crm_next_booking_date}'=>$value['crm_next_booking_date'],'{crm_bookkeep_add_custom_reminder}'=>$value['crm_bookkeep_add_custom_reminder']);
					// foreach ($bookkeep_re as $acckey => $accvalue) {

					// $email_content1    = $accvalue['message'];
					// $email_content  = strtr($email_content1,$a);

					$bookkeep_re = $this->db->query('SELECT * FROM reminder_setting WHERE service_type="bookkeep" and user_id=' . $value['user_id'] . '')->row_array();


					$message = $bookkeep_re['message'];

					$a = array(
						':: Client Name::' => $query['crm_name'],
						':: Service Name::' => 'Accounts',
						':: service Date::' => $value['crm_ch_yearend'],
						':: service Due Date::' => $value['crm_ch_accounts_next_due'],
						'::Client Username::' => $query['username'],
						'::Client Company::' => $value['crm_company_name'],
						' :: Date ::' => date('Y-m-d')
					);

					$email_content = strtr($message, $a);
					$email_content_sms = strtr($message, $a);

					if (($query['username'] != '') && ($diff == $bookkeep_re['days']) && $jsnbookkeep == 'on') {
						$this->send_sms($to_number, $email_content_sms);
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For Next Bookkeeping Date';
						$data2['email_contents']  = '<a href=' . base_url() . ' style="display: inline-block;text-align: center;color: #00aeef;text-decoration: none;font-family:;background: #00aeef;color: #fff;font-weight: 600;padding: 5px 10px;border-radius: 3px;">Click Here</a><p>' . $email_content . '</p>';
						$data2['title'] = 'Next Bookkeeping Date Remainder Mail';
						$contact_data = $this->Common_mdl->GetAllWithWheretwo('client_contacts', 'client_id', $id, 'make_primary', 1);
						//$data2['link']=base_url();
						//email template
						$body = $this->load->view('remainder_email.php', $data2, TRUE);
						//redirect('login/email_format');
						//echo $email_content;
						//die;
						$this->load->library('email');
						$this->email->set_mailtype("html");
						//$this->mailsettings();
						$this->email->from('info.techleaf@gmail.com');
						$this->email->to($contact_data[0]['main_email']);
						$this->email->subject($email_subject);
						$this->email->message($body);
						$send = $this->email->send();
						if ($send) {
							echo 'Next Bookkeeping Date email remainder send';
							echo '<br>';

							$this->session->set_flashdata('success', "Kindly Check Your Mail Id");
						} else {
							echo 'Next Bookkeeping Date email remainder not send';
							echo '<br>';
							$this->session->set_flashdata('warning', "Mail Not sent");
						}
					}
					//} // foreach close
				} // foreach close

				// Management Accounts --  Next Management Accounts Due date

				$Management = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" AND DATE(crm_next_manage_acc_date) > DATE_FORMAT(NOW(),"%Y-%m-%d") and reminder_block=0')->result_array();
				//$management_re=$this->db->query('SELECT * FROM reminder_setting WHERE service_type="management"')->result_array();

				foreach ($Management as $key => $value) {


					(isset(json_decode($value['management'])->reminder) && $value['management'] != '') ? $jsnmanagement =  json_decode($value['management'])->reminder : $jsnmanagement = '';



					$today = date('Y-m-d');
					$datetime1 = new DateTime($today);
					$datetime2 = new DateTime($value['crm_next_manage_acc_date']);
					$interval = $datetime1->diff($datetime2);
					$diff = $interval->format('%a');
					$today_time = date('g:i a');
					/*echo $diff;
    echo '<br>';
    echo $jsnpertax;
    exit;*/

					$old_record[] = $value;
					$query = $this->db->query('select * from user where id="' . $value['user_id'] . '"')->row_array();
					$to_number = $query['crm_phone_number'];
					$management_re = $this->db->query('SELECT * FROM reminder_setting WHERE service_type="management" and user_id=' . $value['user_id'] . '')->row_array();
					$message = $management_re['message'];
					$a = array(
						':: Client Name::' => $query['crm_name'],
						':: Service Name::' => 'Accounts',
						':: service Date::' => $value['crm_ch_yearend'],
						':: service Due Date::' => $value['crm_ch_accounts_next_due'],
						'::Client Username::' => $query['username'],
						'::Client Company::' => $value['crm_company_name'],
						' :: Date ::' => date('Y-m-d')
					);
					$email_content = strtr($message, $a);
					$email_content_sms = strtr($message, $a);
					if (($query['username'] != '') && ($diff == $management_re['days']) && $jsnmanagement == 'on') {
						$this->send_sms($to_number, $email_content_sms);
						$data2['username'] = $query['username'];
						$user_id = $query['id'];
						$password = $query['password'];
						$encrypt_id = base64_encode($user_id);
						$email_subject  = 'Email Remainder For Next Management Accounts Due Date';
						$data2['email_contents']  = '<a href=' . base_url() . ' style="display: inline-block;text-align: center;color: #00aeef;text-decoration: none;font-family:;background: #00aeef;color: #fff;font-weight: 600;padding: 5px 10px;border-radius: 3px;">Click Here</a><p>' . $email_content . '</p>';
						$data2['title'] = 'Next Management Accounts Due Date Remainder Mail';
						$contact_data = $this->Common_mdl->GetAllWithWheretwo('client_contacts', 'client_id', $id, 'make_primary', 1);
						$body = $this->load->view('remainder_email.php', $data2, TRUE);
						$this->load->library('email');
						$this->email->set_mailtype("html");
						$this->email->from('info.techleaf@gmail.com');
						$this->email->to($contact_data[0]['main_email']);
						$this->email->subject($email_subject);
						$this->email->message($body);
						$send = $this->email->send();
						if ($send) {
							echo 'Next Management Accounts Due Date email remainder send';
							echo '<br>';

							$this->session->set_flashdata('success', "Kindly Check Your Mail Id");
						} else {
							echo 'Next Management Accounts Due Date email remainder not send';
							echo '<br>';
							$this->session->set_flashdata('warning', "Mail Not sent");
						}
					}
				} // foreach close
				// VAT Quaters --  VAT Quarters
				$vat_quarters = $this->db->query('SELECT * FROM client WHERE autosave_status=0 AND crm_email!="0" and reminder_block=0')->result_array();
				//$vat_re=$this->db->query('SELECT * FROM reminder_setting WHERE service_type="vat"')->result_array();
				foreach ($vat_quarters as $vat_quarterskey => $vat_quartersval) {

					(isset(json_decode($vat_quartersval['vat'])->reminder) && $vat_quartersval['vat'] != '') ? $vatmanagement =  json_decode($vat_quartersval['vat'])->reminder : $vatmanagement = '';

					if ($vatmanagement == 'on') {
						$crm_vat_quarters = $vat_quartersval['crm_vat_quarters'];
						if ($crm_vat_quarters != '0') {
							if ($crm_vat_quarters == 'Annual End Jan') {
								$month = '01';
							} elseif ($crm_vat_quarters == 'Annual End Feb') {
								$month = '02';
							} elseif ($crm_vat_quarters == 'Annual End Mar') {
								$month = '03';
							} elseif ($crm_vat_quarters == 'Annual End Apr') {
								$month = '04';
							} elseif ($crm_vat_quarters == 'Annual End May') {
								$month = '05';
							} elseif ($crm_vat_quarters == 'Annual End Jun') {
								$month = '06';
							} elseif ($crm_vat_quarters == 'Annual End Jul') {
								$month = '07';
							} elseif ($crm_vat_quarters == 'Annual End Aug') {
								$month = '08';
							} elseif ($crm_vat_quarters == 'Annual End Sep') {
								$month = '09';
							} elseif ($crm_vat_quarters == 'Annual End Oct') {
								$month = '10';
							} elseif ($crm_vat_quarters == 'Annual End Nov') {
								$month = '11';
							} elseif ($crm_vat_quarters == 'Annual End Dec') {
								$month = '12';
							} elseif ($crm_vat_quarters == 'Jan/Apr/Jul/Oct') {
								$month = '01_04_07_10';
							} elseif ($crm_vat_quarters == 'Feb/May/Aug/Nov') {
								$month = '02_05_08_11';
							} elseif ($crm_vat_quarters == 'Mar/Jun/Sep/Dec') {
								$month = '03_06_09_12';
							} elseif ($crm_vat_quarters == 'Monthly') {
								$month = '01_02_03_04_05_06_07_08_09_10_11_12';
							}
							$array = explode('_', $month);
							foreach ($array as $key => $value) {
								$y = date('Y');
								$e_date = date('01-' . $value . '-' . $y);
								//echo $e_date;
								$c_date = date('d-m-Y');
								if ($c_date == $e_date) {
									$query = $this->db->query('select * from user where id="' . $vat_quartersval['user_id'] . '"')->row_array();

									$to_number = $query['crm_phone_number'];

									$vat_re = $this->db->query('SELECT * FROM reminder_setting WHERE service_type="vat" and user_id=' . $vat_quartersval['user_id'] . '')->row_array();

									$message = $vat_re['message'];

									$a = array(
										':: Client Name::' => $query['crm_name'],
										':: Service Name::' => 'Accounts',
										':: service Date::' => $value['crm_ch_yearend'],
										':: service Due Date::' => $value['crm_ch_accounts_next_due'],
										'::Client Username::' => $query['username'],
										'::Client Company::' => $value['crm_company_name'],
										' :: Date ::' => date('Y-m-d')
									);

									$email_content = strtr($message, $a);
									$email_content_sms = strtr($message, $a);


									// $a  = array('{crm_vat_quarters}'=>$crm_vat_quarters,'{crm_vat_add_custom_reminder}'=>$vat_quartersval['crm_vat_add_custom_reminder']);
									// foreach ($vat_re as $acckey => $accvalue) {

									//     $email_content1    = $accvalue['message'];
									//     $email_content  = strtr($email_content1,$a);

									if (($query['username'] != '')) {
										$this->send_sms($to_number, $email_content_sms);
										$data2['username'] = $query['username'];
										$user_id = $query['id'];
										$password = $query['password'];
										$encrypt_id = base64_encode($user_id);
										$email_subject  = 'Email Remainder For VAT Quaters Due Date';
										$data2['email_contents']  = '<a href=' . base_url() . ' style="display: inline-block;text-align: center;color: #00aeef;text-decoration: none;font-family:;background: #00aeef;color: #fff;font-weight: 600;padding: 5px 10px;border-radius: 3px;">Click Here</a><p>' . $email_content . '</p>';
										$data2['title'] = 'Next VAT Quaters Due Date Remainder Mail';
										$contact_data = $this->Common_mdl->GetAllWithWheretwo('client_contacts', 'client_id', $id, 'make_primary', 1);
										//$data2['link']=base_url();
										//email template
										$body = $this->load->view('remainder_email.php', $data2, TRUE);
										//redirect('login/email_format');

										$this->load->library('email');
										$this->email->set_mailtype("html");
										//$this->mailsettings();
										$this->email->from('info.techleaf@gmail.com');
										$this->email->to($contact_data[0]['main_email']);
										$this->email->subject($email_subject);
										$this->email->message($body);
										$send = $this->email->send();
										/*  print_r($body);
            die;*/
										if ($send) {
											echo 'Next VAT Quaters Accounts Due Date email remainder send';
											echo '<br>';

											$this->session->set_flashdata('success', "Kindly Check Your Mail Id");
										} else {
											echo 'Next VAT Quaters Due Date email remainder not send';
											echo '<br>';
											$this->session->set_flashdata('warning', "Mail Not sent");
										}
									}

									// }//close foreach
								}
							}
						}
					}
				}

				$da['user_id'] = 2;
				$da['crm_name'] = 'test';
				$da['crm_currency'] = '₨';
				$da['crm_profile_image'] = '';
				$da['logo_image'] = '';
				$da['created_date'] = time();
				$this->Common_mdl->insert('admin_setting', $da);
			} //close if
		}
	} // function close  

	public function generate_invoice()
	{
		$this->load->view('users/generate-invoice');
	}

	public function home_invoice()
	{
		$this->load->view('users/home-invoice');
	}



	public function get_projects()
	{
		//$this->db->select('id');
		//$this->db->select('project_name');
		//$ret=$this->db->get('projects')->result_array();
		$ret = $this->Common_mdl->getallrecords('projects');
		echo json_encode($ret);
	}


	public function test()
	{
		$diff = new DateTime("2019-04-05");
		$date = $diff->format('Y-m-d');
		echo $date;
	}
	public function new_field()
	{
		$this->load->view('users/new_field');
	}
	public function field($id = '')
	{

		if ($this->input->post()) {
			if ($id == '') {
				$id = $this->Common_mdl->add($this->input->post());
				if ($id) {
					$this->session->set_flashdata('success', "New Field Added");
					//redirect('User/field/' . $id);
					redirect('User/new_field');
				}
			} else {
				//  $success = $this->Common_mdl->update($this->input->post(), $id);
				$success = $this->Common_mdl->update('tblcustomfields', $this->input->post(), 'id', $id);
				if (is_array($success) && isset($success['cant_change_option_custom_field'])) {
					set_alert('warning', _l('cf_option_in_use'));
				} elseif ($success === true) {
					set_alert('success', _l('updated_successfully', _l('custom_field')));
				}
				$this->session->set_flashdata('success', 'Custom Fields Updated Successfully');
				redirect('user/custom_fields');
			}
		}
		/*  if ($id == '') {
            $title = _l('add_new', _l('custom_field_lowercase'));
        } else {
            $data['custom_field'] = $this->Common_mdl->get($id);
            $title                = _l('edit', _l('custom_field_lowercase'));
        }
        $data['pdf_fields']           = $this->pdf_fields;
        $data['client_portal_fields'] = $this->client_portal_fields;
        $data['client_editable_fields'] = $this->client_editable_fields;
        $data['title']                = $title;*/
		// $this->load->view('admin/custom_fields/customfield', $data);
		$this->load->view('users/new_field');
	}

	// staff   
	public function staff_list()
	{

		$this->Security_model->chk_login();
		if ($_SESSION['permission']['User_and_Management']['view'] != 1) {
			$this->load->view('users/blank_page');
		} else {
			//$data['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user','role','1');
			$data['staff_list'] = $this->db->query("select * from staff_form where firm_id=" . $_SESSION['firm_id'])->result_array();
			//firm enabled service
			$services = $this->Common_mdl->get_FirmEnabled_Services();
			$data['services'] = [];
			if (!empty($services)) {
				$services = implode(',', array_filter(array_keys($services)));

				$data['services'] = $this->db->get_where("service_lists", "id IN(" . $services . ")")->result_array();
			}

			$data['work_flow'] = $this->Service_model->get_firm_workflow();

			$this->load->view('staffs/staff_list_view', $data);
		}
	}
	public function archive_staff($id)
	{
		$this->db->query("update staff_form set old_status=status,status=3 where id=$id");
		echo $this->db->affected_rows();
	}
	public function staffstatusChange()
	{
		$Ids = explode(',', $_POST['ids']);
		$Ids = implode(',', $Ids);

		if ($_POST['status'] == 'unarchive') {
			$this->db->query("update user set status=@s:=status,status=old_status,old_status=@s where id IN(" . $Ids . ") and status=3");
			$this->db->query("update staff_form set status=@s:=status,status=old_status,old_status=@s where user_id IN(" . $Ids . ") and status=3");
		} else {
			$IsArchiveCon = '';

			if ($_POST['status'] == 3) {
				$IsArchiveCon = " and status!=3 ";
			}
			$this->db->query("update user set old_status=status,status=" . $_POST['status'] . " where id IN (" . $Ids . ") " . $IsArchiveCon . " and  firm_id=" . $_SESSION['firm_id']);
			$this->db->query("update staff_form set old_status=status,status=" . $_POST['status'] . " where user_id IN (" . $Ids . ") " . $IsArchiveCon . " and  firm_id=" . $_SESSION['firm_id']);
		}
		echo $this->db->affected_rows();
	}




	public function multiple_staff_delete()
	{

		$data_ids = $_POST['data_ids'];
		$data_id_array = explode(",", $data_ids);
		if (!empty($data_id_array)) {
			foreach ($data_id_array as $id) {
				$this->Common_mdl->delete('user', 'id', $id);
				$this->Common_mdl->delete('staff_form', 'user_id', $id);
			}
			echo  $data_ids;
		}
	}


	public  function sendMail()
	{

		$mailids = $this->db->query("select crm_email,id,crm_other_username from client where crm_email!='' and crm_email!='0'")->result_array();



		$msg = $this->input->post('editor4');




		foreach ($mailids as $address) {

			$data2['username'] = $address['crm_other_username'];
			$data2['email_contents']  = $msg;
			$data2['title'] = 'Remindoo.org';
			$this->load->library('email');
			$this->email->set_mailtype("html");
			$body = $this->load->view('remainder_email.php', $data2, TRUE);
			$this->email->from('info.techleaf@gmail.com');
			$this->email->to($address['crm_email']);
			$this->email->subject('Remindoo.org');
			$this->email->message($body);
			$this->email->send();
			$inser_data = $this->db->insert("email_storage", array("id" => $address['id'], "msg" => $msg, "client" => $address['crm_email']));
		}
		//echo "<script>alert('E-mail send sucessfully');</script>";
		if ($inser_data == 1) {
			$this->session->set_flashdata('flashmessage', 'This is a message.');
			redirect('User');
		} else {
			$this->session->set_flashdata('flashmessage_error', 'This is a message.');
			redirect('User');
		}
	}

	public function fetch_user()
	{
		$sort = str_replace("%40", '@', ($this->input->post('control')));

		$data['comments'] = $this->db->query("select id,username,crm_email_id from user where username!='' and username!='0' and  crm_email_id!='' and crm_email_id!='0' and id='$sort' and username!='admin'")->result();
		echo json_encode($data);
		//$this->load->view('level/assign',$data);
	}
	// user list feedback

	// user list feedback
	public  function feedMail()
	{

		$msg = $this->input->post('feed_msg');


		if ($_SESSION['user_type'] == "FA" || $_SESSION['user_type'] == "FU") {
			$table          = "super_admin_details";
			$field_name     = "id";
			$field_value    = 1;
			$EMAIL_TEMPLATE = $this->Common_mdl->getTemplates(25);
		} else {
			$table          = "admin_setting";
			$field_name     = "firm_id";
			$field_value    = $_SESSION['firm_id'];
			$EMAIL_TEMPLATE = $this->Common_mdl->getTemplates(26);
		}
		$EMAIL_TEMPLATE   = end($EMAIL_TEMPLATE);


		$subject  = remove_undecode_tokens(strip_tags($EMAIL_TEMPLATE['subject']));
		$body     =  remove_undecode_tokens(strtr($EMAIL_TEMPLATE['body'], ['::Message::' => $msg]));



		$admin = $this->Common_mdl->select_record($table, $field_name, $field_value);
		$to    = !empty($admin['company_email']) ?   $admin['company_email'] : $admin['email_id'];
		$from  = !empty($_SESSION['crm_email_id']) ? $_SESSION['crm_email_id'] : 'info@remindoo.org';


		$inser_data = $this->db->insert("feedback", array("userid" => $_SESSION['userId'], "feedback" => $msg, "username" => $_SESSION['userName'], "emailid" => $from));
		if ($inser_data == 1) {

			firm_settings_send_mail($_SESSION['firm_id'], $to, $subject, $body);

			$this->session->set_flashdata('server_response', 'Feedback Successfully Send...!');
			redirect($_SERVER['HTTP_REFERER']);
		} else {
			$this->session->set_flashdata('server_response', 'Some thing goes wrong...!');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	/* Coded By Shashmethah*/
	// reminder setting
	public function reminder_settings()
	{
		/*$reminder_setting = $this->db->query("SELECT * FROM reminder_setting WHERE custom_reminder=0 AND firm_id IN('".$_SESSION['firm_id']."','0') ORDER BY service_id ASC")->result_array();
        //service_id BETWEEN '0' AND '16' AND
        if($_SESSION['firm_id'] != 0)
        {
           $record = $this->db->query("SELECT super_admin_owned FROM reminder_setting WHERE firm_id = '".$_SESSION['firm_id']."' ORDER BY service_id ASC")->result_array();
           //service_id BETWEEN '0' AND '16' AND
           foreach ($reminder_setting as $key => $value) 
           {
              foreach ($record as $value1) 
              {
                 if($value['id'] == $value1['super_admin_owned'])
                 {
                    unset($reminder_setting[$key]);
                 }
              }
           }
        }*/

		$query = "SELECT * 
                  FROM `reminder_setting` 
                  WHERE custom_reminder=0 
                  AND firm_id IN (0," . $_SESSION['firm_id'] . ")                  
                  AND id NOT IN ( SELECT super_admin_owned from reminder_setting WHERE firm_id=" . $_SESSION['firm_id'] . " ) 
                  ORDER BY service_type ASC,due ASC,days ASC";

		$reminder_setting = $this->db->query($query)->result_array();

		$data['reminder_setting'] = $reminder_setting;
		$data['reminder_templates'] = $this->Common_mdl->getReminderTemplates();

		return $data;
	}


	/*Reminder email Content */

	public function email_content_get($id = false)
	{
		$email_temp = $this->Common_mdl->GetAllWithWhere('tblemailtemplates', 'id', $id);

		$exp_contect = explode(',', $email_temp[0]['placeholder']);
		$data['content'] = '';

		foreach ($exp_contect as $key => $val) {
			$data['content'] .= '<span target="message" class="add_merge">' . $val . '</span><br>';
		}

		$data['subject'] = $email_temp[0]['subject'];
		echo json_encode($data);
	}
	public function add_reminder($id)
	{
		if ($_SESSION['firm_id'] == '0' || $_COOKIE['remindoo_logout'] == 'remindoo_superadmin') {
			$this->Loginchk_model->superAdmin_LoginCheck();
		} else {
			$this->Security_model->chk_login();
		}

		if (isset($_POST)) {
			$service_type = $this->Common_mdl->get_price('service_lists', 'id', $id, 'services_subnames');
			$data['service_id'] = $id;
			$data['service_type'] = $service_type;
			$data['name'] = (isset($_POST['name']) && ($_POST['name'] != '')) ? $_POST['name'] : '';
			$data['subject'] = (isset($_POST['subject']) && ($_POST['subject'] != '')) ? $_POST['subject'] : '';
			$data['message'] = (isset($_POST['message']) && ($_POST['message'] != '')) ? $_POST['message'] : '';
			$data['fromname'] = (isset($_POST['fromname']) && ($_POST['fromname'] != '')) ? $_POST['fromname'] : '';
			$data['due'] = (isset($_POST['due']) && ($_POST['due'] != '')) ? $_POST['due'] : '';
			$data['days'] = (isset($_POST['days']) && ($_POST['days'] != '')) ? $_POST['days'] : '';
			$data['status'] = 1;
			$data['created_time'] = time();
			$data['firm_id'] = $_SESSION['firm_id'];
			$data['headline_color'] = $_POST['headline_color'];

			$insert = $this->Common_mdl->insert('reminder_setting', $data);

			$this->Service_Reminder_Model->Add_NewServiceReminder_Cron($insert);

			if ($insert == true) {
				$_SESSION['msg'] = "Remainder added successfully!";
			} else {
				$_SESSION['msg'] = "Process failed!";
			}
		}

		echo '<script type="text/javascript">window.close();</script>';

		//redirect('user/Service_reminder_settings/');
	}

	public function edit_reminder()
	{
		if ($_SESSION['firm_id'] == '0' || $_COOKIE['remindoo_logout'] == 'remindoo_superadmin') {
			$this->Loginchk_model->superAdmin_LoginCheck();
		} else {
			$this->Security_model->chk_login();
		}

		if (isset($_POST)) {
			$id = $_POST['id'];
			$service_id = $_POST['service_id'];

			$record = $this->db->query('SELECT * FROM reminder_setting WHERE id = "' . $id . '"')->row_array();

			if ($record['firm_id'] == $_SESSION['firm_id']) {
				$data['subject'] = (isset($_POST['subject']) && ($_POST['subject'] != '')) ? $_POST['subject'] : '';
				$data['message'] = (isset($_POST['message']) && ($_POST['message'] != '')) ? $_POST['message'] : '';
				$data['due'] = (isset($_POST['due']) && ($_POST['due'] != '')) ? $_POST['due'] : '';
				$data['days'] = (isset($_POST['days']) && ($_POST['days'] != '')) ? $_POST['days'] : '';
				$data['status'] = 1;
				$data['headline_color'] = $_POST['headline_color'];
				$data['updated_time'] = time();

				$update = $this->Common_mdl->update('reminder_setting', $data, 'id', $id);
			} else {
				$service_type = $this->Common_mdl->get_price('service_lists', 'id', $service_id, 'services_subnames');

				$data['service_id'] = $service_id;
				$data['service_type'] = $service_type;
				$data['name'] = (isset($_POST['name']) && ($_POST['name'] != '')) ? $_POST['name'] : '';
				$data['subject'] = (isset($_POST['subject']) && ($_POST['subject'] != '')) ? $_POST['subject'] : '';
				$data['message'] = (isset($_POST['message']) && ($_POST['message'] != '')) ? $_POST['message'] : '';
				$data['fromname'] = (isset($_POST['fromname']) && ($_POST['fromname'] != '')) ? $_POST['fromname'] : '';
				$data['due'] = (isset($_POST['due']) && ($_POST['due'] != '')) ? $_POST['due'] : '';
				$data['days'] = (isset($_POST['days']) && ($_POST['days'] != '')) ? $_POST['days'] : '';
				$data['status'] = 1;
				$data['created_time'] = time();
				$data['firm_id'] = $_SESSION['firm_id'];
				$data['super_admin_owned'] = $id;
				$data['headline_color'] = $_POST['headline_color'];

				$update = $insert =  $this->Common_mdl->insert('reminder_setting', $data);
				$this->Service_Reminder_Model->Shift_ServiceReminders_Cron($insert);
				$id = $insert;
			}

			$this->Service_Reminder_Model->Edit_ExistServiceReminder_Cron($id);

			if ($update == true) {
				$_SESSION['msg'] = "Remainder edited successfully!";
			} else {
				$_SESSION['msg'] = "Process failed!";
			}
		}

		echo '<script type="text/javascript">window.close();</script>';

		//redirect('user/Service_reminder_settings/');
	}

	public function delete_reminder()
	{
		$delete = $this->Common_mdl->delete('reminder_setting', 'id', $_POST['record_id']);
		$this->Service_Reminder_Model->Delete_ServiceReminder_Cron($_POST['record_id']);

		if ($delete == true) {
			$_SESSION['msg'] = "Reminder Deleted Successfully!";
		} else {
			$_SESSION['msg'] = "Process failed!";
		}

		echo '1';
	}
	/* Coded By Shashmethah*/
	public function reminder_set_delete($id)
	{

		$email_temp = $this->Common_mdl->GetAllWithWhere('reminder_setting', 'id', $id);
		$this->Common_mdl->delete('reminder_setting', 'id', $id);

		redirect('user/reminder_settings/' . $email_temp[0]['service_id']);
	}


	public function send_sms($to, $msg)
	{
		// $to = "9566945538";
		$url = 'https://rest.nexmo.com/sms/json?' . http_build_query([
			'api_key' => "226a930f",
			'api_secret' => "56231124c516302f",
			'to' => "+" . $to,
			'from' => "NEXMO",
			'text' => $msg
		]);

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		echo '<pre>';
		print_r($response);
		//return true;
	}

	public function get_persona_enquiry(){
		$persona_inquiry_id = $_POST['persona_inquiry_id'];
		$url = 'https://withpersona.com/api/v1/inquiries/'.$persona_inquiry_id;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Authorization: Bearer '.PERSONA_API_KEY, 'Persona-Version: 2023-01-05'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$result = curl_exec($ch);
		$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		$res = json_decode($result,true);
		if($http_status != 200 ){
			echo 0;
			exit;
		}
		else{
			echo json_encode($res['data']['attributes']);
			exit;
		}
	}

	public function send_persona_enquiry()
	{
		$contact_id = $_POST['contact_id'];
		$query    	= "SELECT c.id client_id,cc.* 
							   FROM client_contacts as cc 
							   INNER JOIN client as c on c.user_id=cc.client_id
							   WHERE cc.id=" . $contact_id;
		$client 	= $this->db->query($query)->row_array();
		$ch = curl_init( 'https://withpersona.com/api/v1/inquiries' );
		# Setup request to send json via POST.
		$data['attributes']['fields']['name-first'] = $client['first_name'];
		$data['attributes']['fields']['name-last'] = $client['last_name'];
		$data['attributes']['fields']['birthdate'] = $client['date_of_birth'];
		$data['attributes']['fields']['address-street-1'] = $client['address_line1'];
		$data['attributes']['fields']['address-city'] = $client['town_city'];
		$data['attributes']['fields']['address-subdivision'] = $client['address_line2'];
		$data['attributes']['fields']['address-postal-code'] = $client['post_code'];
		$data['attributes']['fields']['address-country-code'] = $client['post_code'];
		$data['attributes']['fields']['email-address'] = $client['main_email'];
		$data['attributes']['inquiry-template-id'] = PERSONA_TEMPLATE_ID;
		$payload = json_encode( array( "data"=> $data ) );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json', 'Authorization: Bearer '.PERSONA_API_KEY, 'Persona-Version: 2023-01-05'));
		# Return response instead of printing.
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		# Send request.
		$result = curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		# Print response.
		$res = json_decode($result,true);
		if($httpcode != 201 ){
			echo 0;
			exit;
		}
		else{
			$inquiry_id = $res['data']['id'];
			$this->db->update('client_contacts', ["persona_inquiry_id" => $inquiry_id], "id = " . $client['id']);
			echo '<pre>';
			print_r($res);
			exit;
		}
	}



	// custom fields
	public function custom_fields()
	{

		$this->Security_model->chk_login();
		$data['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user', 'role', '1');
		$data['custom_list'] = $this->Common_mdl->getallrecords('tblcustomfields');

		$this->load->view('users/custom_fields_view', $data);
	}
	public function custom_delete($id)
	{
		$this->Common_mdl->delete('tblcustomfields', 'id', $id);
		$this->Common_mdl->delete('tblcustomfieldsvalues', 'fieldid', $id);
		redirect('user/custom_fields');
	}
	public function custom_field_statusChange()
	{
		$id = $this->input->post('rec_id');
		$status = $this->input->post('status');
		$data = array("active" => $status);
		$query = $this->Common_mdl->update('tblcustomfields', $data, 'id', $id);
		return true;
	}
	public function edit_custom_fields($id)
	{
		$this->Security_model->chk_login();
		$data['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user', 'role', '1');

		$data['custom_fields'] = $this->Common_mdl->GetAllWithWhere('tblcustomfields', 'id', $id);

		$this->load->view('users/new_field', $data);
	}


	//sankar 23/2/2018

	public function taskstatus_filter()
	{
		$id = $this->input->post('filterstatus');
		$stat = $this->Common_mdl->GetAllWithWhere_order_desc('add_new_task', 'task_status', $id, 'id');
		echo json_encode($stat);
	}

	public function user_status_filter()
	{
		$status = $this->input->post('filterstatus');
		$dateb = $this->input->post('time');
		$dateb1 = $this->input->post('time1');
		$prior = $this->input->post('priority');
		$data['table'] = $_POST['table'];

		$table = $this->input->post('table');

		if ($table == 'alluser') {
			$status = $this->input->post('filterstatus');
		} else if ($table == 'newactives') {
			$status = 1;
		} else if ($table == 'newinactives') {
			$status = 2;
		} else if ($table == 'frozens') {
			$status = 3;
		} else {
			$status = 5;
		}

		if ($status == 2) {
			$state = 0;
		} else {
			$state = $status;
		}

		//if($dateb==''){ $dateb=0; }
		if ($dateb == '') {

			$dateb = '2018-01-01';
		} else {
			$dateb = date('Y-m-d', strtotime($dateb));
		}
		if ($dateb1 == '') {

			$dateb1 = date('Y-m-d');
		} else {
			$dateb1 = date('Y-m-d', strtotime($dateb1));
		}
		$it_session_id = '';
		$get_this_qry = $this->db->query("select * from user where id=" . $_SESSION['id'])->row_array();
		$created_id = $get_this_qry['firm_admin_id'];
		$its_firm_admin_id = $get_this_qry['firm_admin_id'];
		if ($_SESSION['role'] == 6) // staff
		{
			$it_session_id = $_SESSION['id'];
		} else if ($_SESSION['role'] == 5) //manager
		{
			$it_session_id = $created_id;
		} else if ($_SESSION['role'] == 4) //client
		{
			$it_session_id = $created_id;
		} else if ($_SESSION['role'] == 3) //director
		{
			$it_session_id = $created_id;
		} else if ($_SESSION['role'] == 2) // sub admin
		{
			$it_session_id = $created_id;
		} else {
			$it_session_id = $_SESSION['id'];
		}
		$result = array();
		array_push($result, $it_session_id);
		$reassign_firm_id = $its_firm_admin_id;
		$tot_val = 1;
		for ($z = 0; $z < $tot_val; $z++) {
			if ($reassign_firm_id == 0) {
				$tot_val = 0;
			} else {
				$get_this_qry = $this->db->query("select * from user where id=" . $reassign_firm_id)->row_array();
				if (!empty($get_this_qry)) {
					array_push($result, $get_this_qry['id']);
					$new_created_id = $get_this_qry['firm_admin_id'];
					$reassign_firm_id = $get_this_qry['firm_admin_id'];
				} else {
					$tot_val = 0;
				}
			}
		}
		$res = (!empty($result)) ? implode(',', array_filter(array_unique($result))) : '';
		$sql_qry = '';
		$sql_qry1 = '';
		if ($_SESSION['role'] == 6 || $_SESSION['role'] == 5) {
			$array_client_id = array();
			if ($_SESSION['role'] == 6) {

				$team_num = array();
				$for_cus_team = $this->db->query("select * from team_assign_staff where FIND_IN_SET('" . $_SESSION['id'] . "',staff_id)")->result_array();
				foreach ($for_cus_team as $cu_team_key => $cu_team_value) {
					array_push($team_num, $cu_team_value['team_id']);
				}
				if (!empty($team_num) && count($team_num) > 0) {
					$res_team_num = implode('|', $team_num);
					$res_team_num1 = implode(',', $team_num);
				} else {
					$res_team_num = '0';
					$res_team_num1 = '0';
				}

				$department_num = array();
				$for_cus_department = $this->db->query("SELECT * FROM `department_assign_team` where CONCAT(',', `team_id`, ',') REGEXP ',($res_team_num),'")->result_array();
				foreach ($for_cus_department as $cu_dept_key => $cu_dept_value) {
					array_push($department_num, $cu_dept_value['depart_id']);
				}
				if (!empty($department_num) && count($department_num) > 0) {
					$res_dept_num = implode('|', $department_num);
					$res_dept_num1 = implode(',', $department_num);
				} else {
					$res_dept_num = '0';
					$res_dept_num1 = '0';
				}

				/* responsible team **/

				$responsible_team = $this->db->query("select * from responsible_team where team in (" . $res_team_num1 . ") and team!='' ")->result_array();
				if (!empty($responsible_team)) {
					foreach ($responsible_team as $retm_key => $retm_value) {
						array_push($array_client_id, $retm_value['client_id']);
					}
				}
				/** responsible department **/
				$responsible_depart = $this->db->query("select * from responsible_department where depart in (" . $res_dept_num1 . ") and depart!='' ")->result_array();
				if (!empty($responsible_depart)) {
					foreach ($responsible_depart as $redekey => $rede_value) {
						array_push($array_client_id, $rede_value['client_id']);
					}
				}
				/** responsible user **/
				$responsible_staff = $this->db->query("select * from responsible_user where manager_reviewer=" . $_SESSION['id'] . " ")->result_array();
				if (!empty($responsible_staff)) {
					foreach ($responsible_staff as $rest_key => $rest_value) {
						array_push($array_client_id, $rest_value['client_id']);
					}
				}
			}
			if ($_SESSION['role'] == 5) {
				$responsible_management = $this->db->query("select * from responsible_user where assign_managed=" . $_SESSION['id'] . " ")->result_array();
				if (!empty($responsible_management)) {
					foreach ($responsible_management as $rest_key => $rest_value) {
						array_push($array_client_id, $rest_value['client_id']);
					}
				}
			}
			/** 03-08-2018 rs **/
			$get_login_user = $this->db->query("select * from user where firm_admin_id=" . $_SESSION['id'] . " and role='4' AND autosave_status!='1' ")->result_array();
			if (!empty($get_login_user)) {
				foreach ($get_login_user as $lg_key => $lg_value) {
					array_push($array_client_id, $lg_value['id']);
				}
			}
			/** 03-08-2018 **/
			if (!empty($array_client_id)) {
				$result_var = implode(',', array_unique($array_client_id));
			} else {
				$result_var = '0';
			}
			$sql_qry = " and (id in (" . $result_var . ") ) and role='4' AND autosave_status!='1' ";
			$sql_qry1 = " and (t1.id in (" . $result_var . ") ) and t1.role='4' AND t1.autosave_status!='1' ";
		} else if ($_SESSION['role'] == 4) { // client
			$sql_qry = " and id=" . $_SESSION['id'] . " and role='4' AND autosave_status!='1' ";
			$sql_qry1 = " and t1.id=" . $_SESSION['id'] . " and t1.role='4' AND t1.autosave_status!='1' ";
		} else {
			$sql_qry = " and firm_admin_id in ($res) and role='4' AND autosave_status!='1'";
			$sql_qry1 = " and t1.firm_admin_id in ($res) and t1.role='4' AND t1.autosave_status!='1'";
		}


		if ($dateb != 0) {
			//   $status = $this->input->post('filterstatus');
			// $dateb = $this->input->post('time');

			// $start_date=date('Y-m-d 06:30:00', strtotime($dateb));
			// $end_date=date('Y-m-d 25:30:00', strtotime($dateb));

			// list($date, $time) = explode(' ',  $start_date);
			// list($year, $month, $day) = explode('-', $date);
			// list($hour, $minute, $second) = explode(':', $time);
			// $first = mktime($hour, $minute, $second, $month, $day, $year);

			// list($date, $time) = explode(' ',  $end_date);
			// list($year, $month, $day) = explode('-', $date);
			// list($hour, $minute, $second) = explode(':', $time);
			// $second = mktime($hour, $minute, $second, $month, $day, $year);
			// ---------------------------------------------------------------------------------------
			if ($prior != '0') {
				if ($status != 0) {

					$data['getallUser'] = $this->db->query("SELECT t1.* FROM user t1 INNER JOIN client t2 ON t1.id =t2.user_id WHERE from_unixtime(t1.CreatedTime) BETWEEN '" . $dateb . "' AND '" . $dateb1 . "' and t1.crm_name!='' and t2.crm_legal_form='" . $prior . "' and t1.status = '" . $state . "' and from_unixtime(t1.CreatedTime) BETWEEN '" . $dateb . "' AND '" . $dateb1 . "' and t1.crm_name!='' and t2.crm_legal_form='" . $prior . "' and t1.status = '" . $state . "' and t1.role=4 " . $sql_qry1 . "  order by t1.id DESC")->result_array();
				} else {

					$data['getallUser'] = $this->db->query("SELECT t1.* FROM user t1 INNER JOIN client t2 ON t1.id =t2.user_id WHERE from_unixtime(t1.CreatedTime) BETWEEN '" . $dateb . "' AND '" . $dateb1 . "' and t1.crm_name!='' and t2.crm_legal_form='" . $prior . "' and t1.role=4 " . $sql_qry1 . "  order by t1.id DESC")->result_array();
				}
			}
			if ($prior == '0') {

				if ($status != 0) {

					$data['getallUser'] = $this->db->query("SELECT * FROM user where from_unixtime(CreatedTime) BETWEEN '" . $dateb . "' AND '" . $dateb1 . "' and status = '" . $state . "' and crm_name!='' and from_unixtime(CreatedTime) BETWEEN '" . $dateb . "' AND '" . $dateb1 . "' and status = '" . $state . "' and crm_name!='' and role=4 " . $sql_qry . " order by id DESC")->result_array();
				} else {
					$data['getallUser'] = $this->db->query("SELECT * FROM user where crm_name!='' and from_unixtime(CreatedTime) BETWEEN '" . $dateb . "' AND '" . $dateb1 . "' and role=4 " . $sql_qry . " order by id DESC")->result_array();
				}
			}
			// ------------------------------------------------------------------------------------      
			$data['column_setting'] = $this->Common_mdl->getallrecords('column_setting_new');
			$data['client'] = $this->db->query('select * from column_setting_new where id=1')->row_array();
			$this->load->view('users/filter_user', $data);
		} else {

			//  $status = $this->input->post('filterstatus');
			$prior = $this->input->post('priority');
			// ------------------------------------------------------------------------------------
			if ($prior != '0') {
				if ($status != 0) {

					$data['getallUser'] = $this->db->query("SELECT t1.* FROM user t1 INNER JOIN client t2 ON t1.id =t2.user_id WHERE t1.crm_name!='' and t2.crm_legal_form='" . $prior . "' and t1.status = '" . $state . "' amd t1.crm_name!='' and t2.crm_legal_form='" . $prior . "' and t1.status = '" . $state . "' and t1.role=4 " . $sql_qry1 . " order by t1.id DESC")->result_array();
				} else {

					$data['getallUser'] = $this->db->query("SELECT t1.* FROM user t1 INNER JOIN client t2 ON t1.id =t2.user_id where t2.crm_legal_form = '" . $prior . "' and t1.crm_name!='' and t1.role=4 " . $sql_qry1 . " order by id DESC")->result_array();
				}
			}
			if ($prior == '0') {

				if ($status != 0) {

					$data['getallUser'] = $this->db->query("SELECT * FROM user where status = '" . $state . "' and crm_name!='' and role=4 " . $sql_qry . " order by id DESC")->result_array();
				} else {
					$data['getallUser'] = $this->db->query("SELECT * FROM user where crm_name!='' and role=4 " . $sql_qry . " order by id DESC")->result_array();
				}
			}
			// -------------------------------------------------------------------------------
			$data['column_setting'] = $this->Common_mdl->getallrecords('column_setting_new');
			$data['client'] = $this->db->query('select * from column_setting_new where id=1')->row_array();
			$this->load->view('users/filter_user', $data);
		}
	}
	public function priority_status()
	{

		$status = $this->input->post('filterstatus');
		$dateb = $this->input->post('time');
		$dateb1 = $this->input->post('time1');
		$prior = $this->input->post('priority');
		$data['table'] = $_POST['table'];
		//if($dateb==''){ $dateb=0; }


		$table = $this->input->post('table');

		if ($table == 'alluser') {
			$status = $this->input->post('filterstatus');
		} else if ($table == 'newactives') {
			$status = 1;
		} else if ($table == 'newinactives') {
			$status = 2;
		} else if ($table == 'frozens') {
			$status = 3;
		} else {
			$status = 5;
		}

		if ($dateb == '') {

			$dateb = '2018-01-01';
		} else {
			$dateb = date('Y-m-d', strtotime($dateb));
		}
		if ($dateb1 == '') {

			$dateb1 = date('Y-m-d');
		} else {
			$dateb1 = date('Y-m-d', strtotime($dateb1));
		}

		if ($status == 2) {
			$state = 0;
		} else {
			$state = $status;
		}
		//if($status==''){ $status=0; }
		$it_session_id = '';
		$get_this_qry = $this->db->query("select * from user where id=" . $_SESSION['id'])->row_array();
		$created_id = $get_this_qry['firm_admin_id'];
		$its_firm_admin_id = $get_this_qry['firm_admin_id'];
		if ($_SESSION['role'] == 6) // staff
		{
			$it_session_id = $_SESSION['id'];
		} else if ($_SESSION['role'] == 5) //manager
		{
			$it_session_id = $created_id;
		} else if ($_SESSION['role'] == 4) //client
		{
			$it_session_id = $created_id;
		} else if ($_SESSION['role'] == 3) //director
		{
			$it_session_id = $created_id;
		} else if ($_SESSION['role'] == 2) // sub admin
		{
			$it_session_id = $created_id;
		} else {
			$it_session_id = $_SESSION['id'];
		}
		$result = array();
		array_push($result, $it_session_id);
		$reassign_firm_id = $its_firm_admin_id;
		$tot_val = 1;
		for ($z = 0; $z < $tot_val; $z++) {
			if ($reassign_firm_id == 0) {
				$tot_val = 0;
			} else {
				$get_this_qry = $this->db->query("select * from user where id=" . $reassign_firm_id)->row_array();
				if (!empty($get_this_qry)) {
					array_push($result, $get_this_qry['id']);
					$new_created_id = $get_this_qry['firm_admin_id'];
					$reassign_firm_id = $get_this_qry['firm_admin_id'];
				} else {
					$tot_val = 0;
				}
			}
		}
		$res = (!empty($result)) ? implode(',', array_filter(array_unique($result))) : '';
		$sql_qry = '';
		$sql_qry1 = '';
		if ($_SESSION['role'] == 6 || $_SESSION['role'] == 5) {
			$array_client_id = array();
			if ($_SESSION['role'] == 6) {

				$team_num = array();
				$for_cus_team = $this->db->query("select * from team_assign_staff where FIND_IN_SET('" . $_SESSION['id'] . "',staff_id)")->result_array();
				foreach ($for_cus_team as $cu_team_key => $cu_team_value) {
					array_push($team_num, $cu_team_value['team_id']);
				}
				if (!empty($team_num) && count($team_num) > 0) {
					$res_team_num = implode('|', $team_num);
					$res_team_num1 = implode(',', $team_num);
				} else {
					$res_team_num = '0';
					$res_team_num1 = '0';
				}

				$department_num = array();
				$for_cus_department = $this->db->query("SELECT * FROM `department_assign_team` where CONCAT(',', `team_id`, ',') REGEXP ',($res_team_num),'")->result_array();
				foreach ($for_cus_department as $cu_dept_key => $cu_dept_value) {
					array_push($department_num, $cu_dept_value['depart_id']);
				}
				if (!empty($department_num) && count($department_num) > 0) {
					$res_dept_num = implode('|', $department_num);
					$res_dept_num1 = implode(',', $department_num);
				} else {
					$res_dept_num = '0';
					$res_dept_num1 = '0';
				}

				/* responsible team **/

				$responsible_team = $this->db->query("select * from responsible_team where team in (" . $res_team_num1 . ") and team!='' ")->result_array();
				if (!empty($responsible_team)) {
					foreach ($responsible_team as $retm_key => $retm_value) {
						array_push($array_client_id, $retm_value['client_id']);
					}
				}
				/** responsible department **/
				$responsible_depart = $this->db->query("select * from responsible_department where depart in (" . $res_dept_num1 . ") and depart!='' ")->result_array();
				if (!empty($responsible_depart)) {
					foreach ($responsible_depart as $redekey => $rede_value) {
						array_push($array_client_id, $rede_value['client_id']);
					}
				}
				/** responsible user **/
				$responsible_staff = $this->db->query("select * from responsible_user where manager_reviewer=" . $_SESSION['id'] . " ")->result_array();
				if (!empty($responsible_staff)) {
					foreach ($responsible_staff as $rest_key => $rest_value) {
						array_push($array_client_id, $rest_value['client_id']);
					}
				}
			}
			if ($_SESSION['role'] == 5) {
				$responsible_management = $this->db->query("select * from responsible_user where assign_managed=" . $_SESSION['id'] . " ")->result_array();
				if (!empty($responsible_management)) {
					foreach ($responsible_management as $rest_key => $rest_value) {
						array_push($array_client_id, $rest_value['client_id']);
					}
				}
			}
			/** 03-08-2018 rs **/
			$get_login_user = $this->db->query("select * from user where firm_admin_id=" . $_SESSION['id'] . " and role='4' AND autosave_status!='1' ")->result_array();
			if (!empty($get_login_user)) {
				foreach ($get_login_user as $lg_key => $lg_value) {
					array_push($array_client_id, $lg_value['id']);
				}
			}
			/** 03-08-2018 **/
			if (!empty($array_client_id)) {
				$result_var = implode(',', array_unique($array_client_id));
			} else {
				$result_var = '0';
			}
			$sql_qry = " and (id in (" . $result_var . ") ) and role='4' AND autosave_status!='1'  ";
			$sql_qry1 = " and (t1.id in (" . $result_var . ") ) and t1.role='4' AND t1.autosave_status!='1' ";
		} else if ($_SESSION['role'] == 4) { // client
			$sql_qry = " and id=" . $_SESSION['id'] . " and role='4' AND autosave_status!='1' ";
			$sql_qry1 = " and t1.id=" . $_SESSION['id'] . " and t1.role='4' AND t1.autosave_status!='1' ";
		} else {
			$sql_qry = " and firm_admin_id in ($res) and role='4' AND autosave_status!='1' ";
			$sql_qry1 = " and t1.firm_admin_id in ($res) and t1.role='4' AND t1.autosave_status!='1' ";
		}

		if ($dateb != 0) {
			// $status = $this->input->post('filterstatus');
			// $dateb = $this->input->post('time');

			// $start_date=date('Y-m-d 06:30:00', strtotime($dateb));
			// $end_date=date('Y-m-d 25:30:00', strtotime($dateb));

			// list($date, $time) = explode(' ',  $start_date);
			// list($year, $month, $day) = explode('-', $date);
			// list($hour, $minute, $second) = explode(':', $time);
			// $first = mktime($hour, $minute, $second, $month, $day, $year);

			// list($date, $time) = explode(' ',  $end_date);
			// list($year, $month, $day) = explode('-', $date);
			// list($hour, $minute, $second) = explode(':', $time);
			// $second = mktime($hour, $minute, $second, $month, $day, $year);
			// ----------------------------------------------------------------------------------------
			if ($status != 0) {

				if ($prior != '0') {
					$data['getallUser'] = $this->db->query("SELECT t1.* FROM user t1 INNER JOIN client t2 ON t1.id =t2.user_id WHERE from_unixtime(t1.CreatedTime) BETWEEN '" . $dateb . "' AND '" . $dateb1 . "' and t1.crm_name!='' and t2.crm_legal_form='" . $prior . "' and t1.status = '" . $status . "' AND  from_unixtime(t1.CreatedTime) BETWEEN '" . $dateb . "' AND '" . $dateb1 . "' and t1.crm_name!='' and t2.crm_legal_form='" . $prior . "' and t1.status = '" . $state . "' " . $sql_qry1 . " order by t1.id DESC")->result_array();
				} else {
					$data['getallUser'] = $this->db->query("SELECT t1.* FROM user t1 INNER JOIN client t2 ON t1.id =t2.user_id WHERE from_unixtime(t1.CreatedTime) BETWEEN '" . $dateb . "' AND '" . $dateb1 . "' and t1.crm_name!='' and t1.status = '" . $status . "' AND  from_unixtime(t1.CreatedTime) BETWEEN '" . $dateb . "' AND '" . $dateb1 . "' and t1.crm_name!='' and t1.status = '" . $state . "' " . $sql_qry1 . " order by t1.id DESC")->result_array();
				}
			} else {
				if ($prior != '0') {
					$data['getallUser'] = $this->db->query("SELECT t1.* FROM user t1 INNER JOIN client t2 ON t1.id =t2.user_id WHERE from_unixtime(t1.CreatedTime) BETWEEN '" . $dateb . "' AND '" . $dateb1 . "' and t1.crm_name!='' and t2.crm_legal_form='" . $prior . "' " . $sql_qry1 . " order by t1.id DESC")->result_array();
				} else {
					$data['getallUser'] = $this->db->query("SELECT * FROM user where  from_unixtime(t1.CreatedTime) BETWEEN '" . $dateb . "' AND '" . $dateb1 . "' and crm_name!='' " . $sql_qry . "  order by id DESC")->result_array();
				}
			}
			// --------------------------------------------------------------------------------------
			$data['column_setting'] = $this->Common_mdl->getallrecords('column_setting_new');
			$data['client'] = $this->db->query('select * from column_setting_new where id=1')->row_array();
			$this->load->view('users/filter_user', $data);
		} else {
			$status = $this->input->post('filterstatus');
			$prior = $this->input->post('priority');
			// ---------------------------------------------------------------------------------------            
			if ($status != 0) {
				if ($prior != '0') {

					$data['getallUser'] = $this->db->query("SELECT t1.* FROM user t1 INNER JOIN client t2 ON t1.id =t2.user_id WHERE t1.crm_name!='' and t2.crm_legal_form='" . $prior . "' and t1.status = '" . $status . "' and t1.crm_name!='' and t2.crm_legal_form='" . $prior . "' and t1.status = '" . $state . "' " . $sql_qry1 . " order by t1.id DESC")->result_array();
				} else {

					$data['getallUser'] = $this->db->query("SELECT t1.* FROM user t1 INNER JOIN client t2 ON t1.id =t2.user_id WHERE t1.crm_name!='' and t1.status = '" . $status . "' and t1.crm_name!='' and t1.status = '" . $state . "' " . $sql_qry1 . " order by t1.id DESC")->result_array();
				}
			} else {
				if ($prior != '0') {
					$data['getallUser'] = $this->db->query("SELECT t1.* FROM user t1 INNER JOIN client t2 ON t1.id =t2.user_id WHERE t1.crm_name!='' and t2.crm_legal_form='" . $prior . "' " . $sql_qry1 . " order by t1.id DESC")->result_array();
				} else {
					$data['getallUser'] = $this->db->query("SELECT * FROM user WHERE crm_name!='' " . $sql_qry . " order by id DESC")->result_array();
				}
			}
			// ------------------------------------------------------------------------------
			$data['column_setting'] = $this->Common_mdl->getallrecords('column_setting_new');
			$data['client'] = $this->db->query('select * from column_setting_new where id=1')->row_array();
			$this->load->view('users/filter_user', $data);
		}
	}




	public function date_time_filter()
	{
		$user_select_date = $this->input->post('selected_date');

		$table = $this->input->post('table');

		if ($table == 'alluser') {
			$status = $this->input->post('filterstatus');
		} else if ($table == 'newactives') {
			$status = 1;
		} else if ($table == 'newinactives') {
			$status = 2;
		} else if ($table == 'frozens') {
			$status = 3;
		} else {
			$status = 5;
		}

		$data['table'] = $_POST['table'];

		if ($user_select_date == '') {
			$user_select_date = '2018-01-01';
		} else {
			$user_select_date = date('Y-m-d', strtotime($this->input->post('selected_date')));
		}
		/** 01-08-2018 **/
		if (isset($_POST['selected_end_date']) && $_POST['selected_end_date'] != '') {
			$user_select_end_date = date('Y-m-d', strtotime($_POST['selected_end_date']));
		} else {
			$user_select_end_date = date('Y-m-d');
		}
		/** 01-08-2018 **/
		//   $status = $this->input->post('status');
		$priority = $this->input->post('priority');

		if ($status == 2) {
			$state = 0;
		} else {
			$state = $status;
		}

		$it_session_id = '';
		$get_this_qry = $this->db->query("select * from user where id=" . $_SESSION['id'])->row_array();
		$created_id = $get_this_qry['firm_admin_id'];
		$its_firm_admin_id = $get_this_qry['firm_admin_id'];
		if ($_SESSION['role'] == 6) // staff
		{
			$it_session_id = $_SESSION['id'];
		} else if ($_SESSION['role'] == 5) //manager
		{
			$it_session_id = $created_id;
		} else if ($_SESSION['role'] == 4) //client
		{
			$it_session_id = $created_id;
		} else if ($_SESSION['role'] == 3) //director
		{
			$it_session_id = $created_id;
		} else if ($_SESSION['role'] == 2) // sub admin
		{
			$it_session_id = $created_id;
		} else {
			$it_session_id = $_SESSION['id'];
		}
		$result = array();
		array_push($result, $it_session_id);
		$reassign_firm_id = $its_firm_admin_id;
		$tot_val = 1;
		for ($z = 0; $z < $tot_val; $z++) {
			if ($reassign_firm_id == 0) {
				$tot_val = 0;
			} else {
				$get_this_qry = $this->db->query("select * from user where id=" . $reassign_firm_id)->row_array();
				if (!empty($get_this_qry)) {
					array_push($result, $get_this_qry['id']);
					$new_created_id = $get_this_qry['firm_admin_id'];
					$reassign_firm_id = $get_this_qry['firm_admin_id'];
				} else {
					$tot_val = 0;
				}
			}
		}
		$res = (!empty($result)) ? implode(',', array_filter(array_unique($result))) : '';
		$sql_qry = '';
		$sql_qry1 = '';
		if ($_SESSION['role'] == 6 || $_SESSION['role'] == 5) {
			$array_client_id = array();
			if ($_SESSION['role'] == 6) {

				$team_num = array();
				$for_cus_team = $this->db->query("select * from team_assign_staff where FIND_IN_SET('" . $_SESSION['id'] . "',staff_id)")->result_array();
				foreach ($for_cus_team as $cu_team_key => $cu_team_value) {
					array_push($team_num, $cu_team_value['team_id']);
				}
				if (!empty($team_num) && count($team_num) > 0) {
					$res_team_num = implode('|', $team_num);
					$res_team_num1 = implode(',', $team_num);
				} else {
					$res_team_num = '0';
					$res_team_num1 = '0';
				}

				$department_num = array();
				$for_cus_department = $this->db->query("SELECT * FROM `department_assign_team` where CONCAT(',', `team_id`, ',') REGEXP ',($res_team_num),'")->result_array();
				foreach ($for_cus_department as $cu_dept_key => $cu_dept_value) {
					array_push($department_num, $cu_dept_value['depart_id']);
				}
				if (!empty($department_num) && count($department_num) > 0) {
					$res_dept_num = implode('|', $department_num);
					$res_dept_num1 = implode(',', $department_num);
				} else {
					$res_dept_num = '0';
					$res_dept_num1 = '0';
				}

				/* responsible team **/

				$responsible_team = $this->db->query("select * from responsible_team where team in (" . $res_team_num1 . ") and team!='' ")->result_array();
				if (!empty($responsible_team)) {
					foreach ($responsible_team as $retm_key => $retm_value) {
						array_push($array_client_id, $retm_value['client_id']);
					}
				}
				/** responsible department **/
				$responsible_depart = $this->db->query("select * from responsible_department where depart in (" . $res_dept_num1 . ") and depart!='' ")->result_array();
				if (!empty($responsible_depart)) {
					foreach ($responsible_depart as $redekey => $rede_value) {
						array_push($array_client_id, $rede_value['client_id']);
					}
				}
				/** responsible user **/
				$responsible_staff = $this->db->query("select * from responsible_user where manager_reviewer=" . $_SESSION['id'] . " ")->result_array();
				if (!empty($responsible_staff)) {
					foreach ($responsible_staff as $rest_key => $rest_value) {
						array_push($array_client_id, $rest_value['client_id']);
					}
				}
			}
			if ($_SESSION['role'] == 5) {
				$responsible_management = $this->db->query("select * from responsible_user where assign_managed=" . $_SESSION['id'] . " ")->result_array();
				if (!empty($responsible_management)) {
					foreach ($responsible_management as $rest_key => $rest_value) {
						array_push($array_client_id, $rest_value['client_id']);
					}
				}
			}
			/** 03-08-2018 rs **/
			$get_login_user = $this->db->query("select * from user where firm_admin_id=" . $_SESSION['id'] . " and role='4' AND autosave_status!='1' ")->result_array();
			if (!empty($get_login_user)) {
				foreach ($get_login_user as $lg_key => $lg_value) {
					array_push($array_client_id, $lg_value['id']);
				}
			}
			/** 03-08-2018 **/
			if (!empty($array_client_id)) {
				$result_var = implode(',', array_unique($array_client_id));
			} else {
				$result_var = '0';
			}
			$sql_qry = " and (id in (" . $result_var . ") ) and role='4' AND autosave_status!='1' ";
			$sql_qry1 = " and (t1.id in (" . $result_var . ") ) and t1.role='4' AND t1.autosave_status!='1' ";
		} else if ($_SESSION['role'] == 4) { // client
			$sql_qry = " and id=" . $_SESSION['id'] . " and role='4' AND autosave_status!='1' ";
			$sql_qry1 = " and t1.id=" . $_SESSION['id'] . " and t1.role='4' AND t1.autosave_status!='1' ";
		} else {
			$sql_qry = " and firm_admin_id in ($res) and role='4' AND autosave_status!='1'";
			$sql_qry1 = " and t1.firm_admin_id in ($res) and t1.role='4' AND t1.autosave_status!='1' ";
		}


		// echo $priority;
		// echo '<br>';
		// echo $status;die;

		$start_date = date('Y-m-d 06:30:00', strtotime($user_select_date));
		$end_date = date('Y-m-d 25:30:00', strtotime($user_select_date));

		list($date, $time) = explode(' ',  $start_date);
		list($year, $month, $day) = explode('-', $date);
		list($hour, $minute, $second) = explode(':', $time);
		$first = mktime($hour, $minute, $second, $month, $day, $year);

		list($date, $time) = explode(' ',  $end_date);
		list($year, $month, $day) = explode('-', $date);
		list($hour, $minute, $second) = explode(':', $time);
		$second = mktime($hour, $minute, $second, $month, $day, $year);
		// ------------------------------------------------------------------------------------

		if ($status == 0) {
			if ($priority != '0') {
				//  $data['getallUser']=$this->db->query("SELECT t1.* FROM user t1 INNER JOIN client t2 ON t1.id =t2.user_id where t2.crm_legal_form='".$priority."' and t1.CreatedTime between '".$first."' and '".$second."' and t1.crm_name!='' ".$sql_qry1." order by t1.id DESC")->result_array();
				$data['getallUser'] = $this->db->query("SELECT t1.* FROM user t1 INNER JOIN client t2 ON t1.id =t2.user_id where t2.crm_legal_form='" . $priority . "' and from_unixtime(t1.CreatedTime) BETWEEN '" . $user_select_date . "' AND '" . $user_select_end_date . "' and t1.crm_name!='' " . $sql_qry1 . " order by t1.id DESC")->result_array();
			} else {
				if ($user_select_date == 0) {
					$data['getallUser'] = $this->db->query("SELECT * FROM user where crm_name!='' " . $sql_qry . " order by id DESC")->result_array();
				} else {
					$data['getallUser'] = $this->db->query("SELECT * FROM user where from_unixtime(CreatedTime) BETWEEN '" . $user_select_date . "' AND '" . $user_select_end_date . "' and crm_name!='' " . $sql_qry . " order by id DESC")->result_array();
				}
			}
		} else {

			if ($priority != '0') {
				if ($user_select_date == 0) {
					$data['getallUser'] = $this->db->query("SELECT t1.* FROM user t1 INNER JOIN client t2 ON t1.id =t2.user_id where t2.crm_legal_form='" . $priority . "' and t1.status='" . $status . "'  and  t1.crm_name!='' or t2.crm_legal_form='" . $priority . "' and t1.status='" . $state . "'  and  t1.crm_name!='' " . $sql_qry1 . " order by t1.id DESC")->result_array();
				} else {
					$data['getallUser'] = $this->db->query("SELECT t1.* FROM user t1 INNER JOIN client t2 ON t1.id =t2.user_id where t2.crm_legal_form='" . $priority . "' and t1.status='" . $status . "' and from_unixtime(t1.CreatedTime) BETWEEN '" . $user_select_date . "' AND '" . $user_select_end_date . "' and t1.crm_name!='' or t2.crm_legal_form='" . $priority . "' and t1.status='" . $state . "' and t1.CreatedTime between '" . $first . "' and '" . $second . "' and t1.crm_name!='' " . $sql_qry1 . " order by t1.id DESC")->result_array();
				}
			} else {
				$data['getallUser'] = $this->db->query("SELECT * FROM user where status='" . $status . "' and CreatedTime between '" . $first . "' and '" . $second . "' and crm_name!='' or status='" . $state . "' and from_unixtime(CreatedTime) BETWEEN '" . $user_select_date . "' AND '" . $user_select_end_date . "' and crm_name!='' " . $sql_qry . " order by id DESC")->result_array();
			}
		}
		// -------------------------------------------------------------------------------
		$data['column_setting'] = $this->Common_mdl->getallrecords('column_setting_new');
		$data['client'] = $this->db->query('select * from column_setting_new where id=1')->row_array();
		$this->load->view('users/filter_user', $data);
	}


	public function user_exportCSV()
	{

		$status = $_GET['stati'];
		$priority = $_GET['pe'];

		if (isset($_GET['de'])) {
			$user_select_date = $_GET['de'];
		}

		if (isset($_GET['de7'])) {
			$user_select_date = $_GET['de7'];
		}


		if ($status == 2) {
			$state = 0;
		} else {
			$state = $status;
		}

		// echo $status;
		// echo '<br>';
		// echo $user_select_date;
		// echo '<br>';
		// echo $priority;die;

		if (isset($_GET['de'])) {
			$filename = 'users_' . date('Ymd') . '.csv';
		}
		if (isset($_GET['de7'])) {
			$filename = 'users_' . date('Ymd') . '.xlsx';
		}

		header("Content-Description: File Transfer");
		header("Content-Disposition: attachment; filename=$filename");
		header("Content-Type: application/csv; ");

		if ($user_select_date != 0) {


			$start_date = date('Y-m-d 06:30:00', strtotime($user_select_date));
			$end_date = date('Y-m-d 25:30:00', strtotime($user_select_date));

			list($date, $time) = explode(' ',  $start_date);
			list($year, $month, $day) = explode('-', $date);
			list($hour, $minute, $second) = explode(':', $time);
			$first = mktime($hour, $minute, $second, $month, $day, $year);

			list($date, $time) = explode(' ',  $end_date);
			list($year, $month, $day) = explode('-', $date);
			list($hour, $minute, $second) = explode(':', $time);
			$second = mktime($hour, $minute, $second, $month, $day, $year);



			// --------------------------------------------------------------------------------              
			//withdate  query start
			if ($status == 0) {
				// ==========================
				if ($priority == '0') {
					$data['refer_data'] = $this->db->query("SELECT t1.id,t1.crm_name,t1.status,t1.role,t2.crm_company_name,t2.crm_company_status,t2.status as client_status,t2.crm_legal_form FROM user
t1 INNER JOIN client t2 ON t1.id =t2.user_id WHERE t1.CreatedTime between '" . $first . "' and '" . $second . "' and t1.crm_name!='' order by t1.id DESC")->result_array();
				} else {
					$data['refer_data'] = $this->db->query("SELECT t1.id,t1.crm_name,t1.status,t1.role,t2.crm_company_name,t2.crm_company_status,t2.status as client_status,t2.crm_legal_form FROM user
t1 INNER JOIN client t2 ON t1.id =t2.user_id WHERE t1.CreatedTime between '" . $first . "' and '" . $second . "' and t1.crm_name!='' and t2.crm_legal_form='" . $priority . "' order by t1.id DESC")->result_array();
				}
				// ==========================


			}
			// **********************
			else {
				// ==========================
				if ($priority == '0') {
					$data['refer_data'] = $this->db->query("SELECT t1.id,t1.crm_name,t1.status,t1.role,t2.crm_company_name,t2.crm_company_status,t2.status as client_status,t2.crm_legal_form FROM user
t1 INNER JOIN client t2 ON t1.id =t2.user_id WHERE t1.CreatedTime between '" . $first . "' and '" . $second . "' and t1.crm_name!='' and t1.status='" . $status . "' or t1.CreatedTime between '" . $first . "' and '" . $second . "' and t1.crm_name!='' and t1.status='" . $state . "' order by t1.id DESC")->result_array();
				} else {
					$data['refer_data'] = $this->db->query("SELECT t1.id,t1.crm_name,t1.status,t1.role,t2.crm_company_name,t2.crm_company_status,t2.status as client_status,t2.crm_legal_form FROM user
t1 INNER JOIN client t2 ON t1.id =t2.user_id WHERE t1.CreatedTime between '" . $first . "' and '" . $second . "' and t1.crm_name!='' and t1.status='" . $status . "' or t1.CreatedTime between '" . $first . "' and '" . $second . "' and t1.crm_name!='' and t1.status='" . $state . "' and t2.crm_legal_form='" . $priority . "' order by t1.id DESC")->result_array();
				}
				// ============================ 

			}
			// --------------------------------------------------------------------------------          
			//query end


		}   //end if 

		else {
			$status = $_GET['stati'];
			$priority = $_GET['pe'];

			// echo $status;
			// echo '<br>';

			// echo $priority;die;



			// -------------------------------------------------------------------------------------------
			// without date query start
			if ($status == 0) {
				// ==========================
				if ($priority == '0') {
					$data['refer_data'] = $this->db->query("SELECT t1.id,t1.crm_name,t1.status,t1.role,t2.crm_company_name,t2.crm_company_status,t2.status as client_status,t2.crm_legal_form FROM user
t1 INNER JOIN client t2 ON t1.id =t2.user_id WHERE t1.crm_name!='' order by t1.id DESC")->result_array();
				} else {
					$data['refer_data'] = $this->db->query("SELECT t1.id,t1.crm_name,t1.status,t1.role,t2.crm_company_name,t2.crm_company_status,t2.status as client_status,t2.crm_legal_form FROM user
t1 INNER JOIN client t2 ON t1.id =t2.user_id WHERE t1.crm_name!='' and t2.crm_legal_form='" . $priority . "' order by t1.id DESC")->result_array();
				}
				// ========================
			}
			// ************************************         
			else {
				// ==========================
				if ($priority == '0') {
					$data['refer_data'] = $this->db->query("SELECT t1.id,t1.crm_name,t1.status,t1.role,t2.crm_company_name,t2.crm_company_status,t2.status as client_status,t2.crm_legal_form FROM user
t1 INNER JOIN client t2 ON t1.id =t2.user_id WHERE t1.crm_name!='' and t1.status='" . $status . "' or t1.crm_name!='' and t1.status='" . $state . "' order by t1.id DESC")->result_array();
				} else {
					$data['refer_data'] = $this->db->query("SELECT t1.id,t1.crm_name,t1.status,t1.role,t2.crm_company_name,t2.crm_company_status,t2.status as client_status,t2.crm_legal_form FROM user
t1 INNER JOIN client t2 ON t1.id =t2.user_id WHERE t1.crm_name!='' and t1.status='" . $status . "' and t2.crm_legal_form='" . $priority . "' or t1.crm_name!='' and t1.status='" . $state . "' and t2.crm_legal_form='" . $priority . "' order by t1.id DESC")->result_array();
				}
				// ============================      
			}
			// ***************************

			//query end

			// ------------------------------------------------------------------------------------------               


		}  //end else

		$file = fopen('php://output', 'w');
		$header = array("S.NO", "Name", "Active", "Usertype", "Companyname", "CompanyStatus", "Status", "LegalForm");
		fputcsv($file, $header);
		$i = 0;

		foreach ($data['refer_data'] as $key => $line) {

			if ($line['id'] != 0) {
				$i++;
				$line['id'] = $i;
			}

			if ($line['status'] == 1) {
				$line['status'] = "Active";
			}
			if ($line['status'] == 2) {
				$line['status'] = "In-active";
			}
			if ($line['status'] == 3) {
				$line['status'] = "Frozen";
			}

			if ($line['role'] == 1) {
				$line['role'] = "Super Admin";
			}
			if ($line['role'] == 2) {
				$line['role'] = "Sub Admin";
			}
			if ($line['role'] == 3) {
				$line['role'] = "Director";
			}
			if ($line['role'] == 4) {
				$line['role'] = "Client";
			}
			if ($line['role'] == 5) {
				$line['role'] = "Manager";
			}
			if ($line['role'] == 6) {
				$line['role'] = "Staff";
			}

			if ($line['client_status'] == 0) {
				$line['client_status'] = "Client";
			}
			if ($line['client_status'] == 1) {
				$line['client_status'] = "Company House";
			}
			if ($line['client_status'] == 2) {
				$line['client_status'] = "Import";
			}

			fputcsv($file, $line);
		}


		fclose($file);
		exit;
	}

	// pdf 




	function pdf()
	{

		$status = $_GET['stati'];
		$priority = $_GET['pe'];


		if (isset($_GET['de'])) {
			$user_select_date = $_GET['de'];
		}

		if (isset($_GET['de7'])) {
			$user_select_date = $_GET['de7'];
		}

		if ($status == 2) {
			$state = 0;
		} else {
			$state = $status;
		}

		// echo $user_select_date;
		// echo '<br>'   ;
		// echo $status;
		// echo '<br>'   ;
		// echo $priority;die;


		if ($user_select_date != 0) {

			$user_select_date = $_GET['de'];
			$user = $_GET['stati'];

			$start_date = date('Y-m-d 06:30:00', strtotime($user_select_date));
			$end_date = date('Y-m-d 25:30:00', strtotime($user_select_date));

			list($date, $time) = explode(' ',  $start_date);
			list($year, $month, $day) = explode('-', $date);
			list($hour, $minute, $second) = explode(':', $time);
			$first = mktime($hour, $minute, $second, $month, $day, $year);

			list($date, $time) = explode(' ',  $end_date);
			list($year, $month, $day) = explode('-', $date);
			list($hour, $minute, $second) = explode(':', $time);
			$second = mktime($hour, $minute, $second, $month, $day, $year);



			// withdate query
			// -----------------------------------------------------------------------------------------

			if ($status == 0) {
				// =========================================================================================
				if ($priority == '0') {
					$data['refer_data'] = $this->db->query("SELECT t1.id,t1.crm_name,t1.status,t1.role,t2.crm_company_name,t2.crm_company_status,t2.status as client_status,t2.crm_legal_form FROM user
t1 INNER JOIN client t2 ON t1.id =t2.user_id WHERE t1.CreatedTime between '" . $first . "' and '" . $second . "' and t1.crm_name!='' order by t1.id DESC")->result_array();
				} else {
					$data['refer_data'] = $this->db->query("SELECT t1.id,t1.crm_name,t1.status,t1.role,t2.crm_company_name,t2.crm_company_status,t2.status as client_status,t2.crm_legal_form FROM user
t1 INNER JOIN client t2 ON t1.id =t2.user_id WHERE t1.CreatedTime between '" . $first . "' and '" . $second . "' and t1.crm_name!='' and t2.crm_legal_form='" . $priority . "' order by t1.id DESC")->result_array();
				}
				// ===========================================================================================
			}
			// *******************************************************************************************
			else {
				// ===========================================================================================
				if ($priority == '0') {
					$data['refer_data'] = $this->db->query("SELECT t1.id,t1.crm_name,t1.status,t1.role,t2.crm_company_name,t2.crm_company_status,t2.status as client_status,t2.crm_legal_form FROM user
t1 INNER JOIN client t2 ON t1.id =t2.user_id WHERE t1.CreatedTime between '" . $first . "' and '" . $second . "' and t1.crm_name!='' and t1.status='" . $status . "' or t1.CreatedTime between '" . $first . "' and '" . $second . "' and t1.crm_name!='' and t1.status='" . $state . "' order by t1.id DESC")->result_array();
				} else {
					$data['refer_data'] = $this->db->query("SELECT t1.id,t1.crm_name,t1.status,t1.role,t2.crm_company_name,t2.crm_company_status,t2.status as client_status,t2.crm_legal_form FROM user
t1 INNER JOIN client t2 ON t1.id =t2.user_id WHERE t1.CreatedTime between '" . $first . "' and '" . $second . "' and t1.crm_name!='' and t1.status='" . $status . "' and t2.crm_legal_form='" . $priority . "' or t1.CreatedTime between '" . $first . "' and '" . $second . "' and t1.crm_name!='' and t1.status='" . $state . "' and t2.crm_legal_form='" . $priority . "' order by t1.id DESC")->result_array();
				}
				// ==========================================================================================

			}
			// *****************************************************************************************
			// ------------------------------------------------------------------------------------------

		} else {
			$status = $_GET['stati'];
			$priority = $_GET['pe'];



			//without date query
			// -----------------------------------------------------------------------------------     
			if ($status == 0) {
				// ===================================================================================
				if ($priority == '0') {
					$data['refer_data'] = $this->db->query("SELECT t1.id,t1.crm_name,t1.status,t1.role,t2.crm_company_name,t2.crm_company_status,t2.status as client_status,t2.crm_legal_form FROM user
t1 INNER JOIN client t2 ON t1.id =t2.user_id WHERE t1.crm_name!='' order by t1.id DESC")->result_array();
				} else {
					$data['refer_data'] = $this->db->query("SELECT t1.id,t1.crm_name,t1.status,t1.role,t2.crm_company_name,t2.crm_company_status,t2.status as client_status,t2.crm_legal_form FROM user
t1 INNER JOIN client t2 ON t1.id =t2.user_id WHERE t1.crm_name!='' and t2.crm_legal_form='" . $priority . "' order by t1.id DESC")->result_array();
				}
				//==========================================================================================
			}
			// *****************************************************************************************
			else {
				// =========================================================================================
				if ($priority == '0') {
					$data['refer_data'] = $this->db->query("SELECT t1.id,t1.crm_name,t1.status,t1.role,t2.crm_company_name,t2.crm_company_status,t2.status as client_status,t2.crm_legal_form FROM user
t1 INNER JOIN client t2 ON t1.id =t2.user_id WHERE t1.crm_name!='' and t1.status='" . $status . "' or  t1.crm_name!='' and t1.status='" . $state . "' order by t1.id DESC")->result_array();
				} else {
					$data['refer_data'] = $this->db->query("SELECT t1.id,t1.crm_name,t1.status,t1.role,t2.crm_company_name,t2.crm_company_status,t2.status as client_status,t2.crm_legal_form FROM user
t1 INNER JOIN client t2 ON t1.id =t2.user_id WHERE t1.crm_name!='' and t1.status='" . $status . "' and t2.crm_legal_form='" . $priority . "' or t1.crm_name!='' and t1.status='" . $state . "' and t2.crm_legal_form='" . $priority . "' order by t1.id DESC")->result_array();
				}
				// ==========================================================================================
			}
			// ******************************************************************************************
			// ------------------------------------------------------------------------------------------


		}

		$data['title'] = "Users List";

		$data['column_setting'] = $this->db->query("select * from  firm_column_settings where table_name ='client_list' and visible_firm" . $_SESSION['firmId'] . "=1 order by order_firm" . $_SESSION['firmId'] . " ASC")->result_array();

		$data['getallUser'] = $data['refer_data'];

		$this->load->library('pdf');
		$customPaper = array(0, 0, 1000, 1000);
		$this->pdf->set_paper($customPaper);
		$this->pdf->load_view('users/pdf_view', $data);
		$this->pdf->render();
		$this->pdf->stream("User_list.pdf");
	}


	public function  pdf_download($id)
	{

		$data['records'] = $this->Common_mdl->select_record('proposals', 'id', $id);
		$user_id = $data['records']['user_id'];
		// $data['signature']=$this->db->query('select * from pdf_signatures where user_id='.$user_id.' Order By id Desc Limit 1')->row_array();
		$proposal_name = $data['records']['proposal_name'];

		$this->load->library('pdf');
		$customPaper = array(0, 0, 1000, 1000);
		$this->pdf->set_paper($customPaper);
		$this->pdf->load_view('users/test_pdf', $data);
		$this->pdf->render();
		$this->pdf->stream("User_list123.pdf");
	}

	public function firm_dashboard()
	{
		$this->Security_model->chk_login();
		$id = $this->session->userdata('admin_id');
		$data['user_details'] = $this->Common_mdl->select_record('user', 'id', $id);
		if (isset($_POST['date'])) {
			$month = '';
			$year = '';
		} else {
			$month = date('m');
			$year = date('Y');
		}

		$data['start_end_date'] = $this->start_end_date('01-' . $month . '-' . $year);

		$exp_date = explode(',', $data['start_end_date']);

		$start = $exp_date[0];
		$end = $exp_date[1];

		$data['start_month'] = $this->month_cnt($start);
		$data['end_month'] = $this->month_cnt($end);

		$data['total_count_month'] = $this->weeks($month, $year);


		$assigned_task = $this->Common_mdl->getallrecords('add_new_task');
		$user_id = $this->session->userdata('id');
		$high = 0;
		$low = 0;
		$medium = 0;
		$super_urgent = 0;
		foreach ($assigned_task as $task_key => $task_value) {
			$workers = $task_value['worker'];
			$ex_worker = explode(',', $workers);
			if (in_array($user_id, $ex_worker)) {
				$task_status = $task_value['priority'];
				if ($task_status == 'high') {
					$high++;
				} else if ($task_status == 'low') {
					$low++;
				} else if ($task_status == 'medium') {
					$medium++;
				} else if ($task_status == 'super_urgent') {
					$super_urgent++;
				}
			}
		}
		$data['high'] = $high;
		$data['low'] = $low;
		$data['medium'] = $medium;
		$data['super_urgent'] = $super_urgent;

		//$data['user_activity']=$this->Common_mdl->GetAllWithWhere('activity_log','user_id',$id);
		$data['user_activity'] = $this->Common_mdl->orderby('activity_log', 'user_id', $id, 'desc');
		//echo $this->db->last_query();die;
		/* for($i=1; $i<=$data['total_count_month'];$i++)
       {
        echo $i;
       }die;
*/
		$data['task_list'] = $this->Common_mdl->getallrecords('add_new_task');
		echo "<pre>";
		print_r($data['task_list']);
		$this->load->view('users/firm_dashboard', $data);
	}
	function firm_setting_insert()
	{
		$this->Security_model->chk_login();
		if (isset($_POST)) {

			if (!empty($_POST['confirm'])) {

				$data1['conf_statement'] = json_encode($_POST['confirm']);
			} else {
				$data1['conf_statement'] = '';
			}


			if (!empty($_POST['accounts'])) {

				$data1['accounts'] = json_encode($_POST['accounts']);
			} else {
				$data1['accounts'] = '';
			}


			if (!empty($_POST['companytax'])) {

				$data1['company_tax_return'] = json_encode($_POST['companytax']);
			} else {

				$data1['company_tax_return'] = '';
			}


			if (!empty($_POST['personaltax'])) {

				$data1['personal_tax_return'] = json_encode($_POST['personaltax']);
			} else {
				$data1['personal_tax_return'] = '';
			}

			if (!empty($_POST['payroll'])) {

				$data1['payroll'] = json_encode($_POST['payroll']);
			} else {
				$data1['payroll'] = '';
			}


			if (!empty($_POST['workplace'])) {

				$data1['workplace'] = json_encode($_POST['workplace']);
			} else {
				$data1['workplace'] = '';
			}


			if (!empty($_POST['vat'])) {

				$data1['vat'] = json_encode($_POST['vat']);
			} else {
				$data1['vat'] = '';
			}



			if (!empty($_POST['cis'])) {
				$data1['cis'] = json_encode($_POST['cis']);
			} else {
				$data1['cis'] = '';
			}

			if (!empty($_POST['cissub'])) {

				$data1['cissub'] = json_encode($_POST['cissub']);
			} else {
				$data1['cissub'] = '';
			}



			if (!empty($_POST['p11d'])) {

				$data1['p11d'] = json_encode($_POST['p11d']);
			} else {
				$data1['p11d'] = '';
			}


			if (!empty($_POST['bookkeep'])) {

				$data1['bookkeep'] = json_encode($_POST['bookkeep']);
			} else {
				$data1['bookkeep'] = '';
			}


			if (!empty($_POST['management'])) {

				$data1['management'] = json_encode($_POST['management']);
			} else {
				$data1['management'] = '';
			}


			if (!empty($_POST['investgate'])) {

				$data1['investgate'] = json_encode($_POST['investgate']);
			} else {
				$data1['investgate'] = '';
			}

			if (!empty($_POST['registered'])) {

				$data1['registered'] = json_encode($_POST['registered']);
			} else {
				$data1['registered'] = '';
			}

			if (!empty($_POST['taxadvice'])) {

				$data1['taxadvice'] = json_encode($_POST['taxadvice']);
			} else {
				$data1['taxadvice'] = '';
			}
			if (!empty($_POST['taxinvest'])) {

				$data1['taxinvest'] = json_encode($_POST['taxinvest']);
			} else {
				$data1['taxinvest'] = '';
			}
			$data1['company_name'] = (isset($_POST['company_name']) && ($_POST['company_name'] != '')) ? $_POST['company_name'] : '';
			$data1['user_id'] = $_POST['user_id'];
			$data1['created_time'] = time();
			$this->Common_mdl->delete('firm_dashboard_setting', 'user_id', $_POST['user_id']);
			$inser_data = $this->db->insert('firm_dashboard_setting', $data1);
			if ($inser_data == 1) {
				$this->session->set_flashdata('success', '<div class="alert alert-success" style="display:block;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Your Setting was Added successfully.</div>');
				redirect('user/firm_dashboard');
			} else {
				$this->session->set_flashdata('success', '<div class="alert alert-danger" style="display:block;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Please fill the required fields.</div>');
				redirect('user/firm_dashboard');
			}
		}
	}
	function weeks($month, $year)
	{
		$num_of_days = date("t", mktime(0, 0, 0, $month, 1, $year));
		$lastday = date("t", mktime(0, 0, 0, $month, 1, $year));
		$no_of_weeks = 0;
		$count_weeks = 0;
		while ($no_of_weeks < $lastday) {
			$no_of_weeks += 7;
			$count_weeks++;
		}
		return $count_weeks;
	}

	function month_cnt($date)
	{
		$week_num =  date('W', strtotime($date));
		return $week_num;
	}

	function start_end_date($date)
	{
		$search_form = date('Y-m-01', strtotime($date));
		$search_to =  date('Y-m-t', strtotime($date));
		return $search_form . ',' . $search_to;
	}



	public function html()
	{
		$status = $_GET['stati'];
		$priority = $_GET['pe'];


		if (isset($_GET['de'])) {
			$user_select_date = $_GET['de'];
		}

		if (isset($_GET['de7'])) {
			$user_select_date = $_GET['de7'];
		}

		if ($status == 2) {
			$state = 0;
		} else {
			$state = $status;
		}

		// echo $user_select_date;
		// echo '<br>'   ;
		// echo $status;
		// echo '<br>'   ;
		// echo $priority;die;


		if ($user_select_date != 0) {

			$user_select_date = $_GET['de'];
			$user = $_GET['stati'];

			$start_date = date('Y-m-d 06:30:00', strtotime($user_select_date));
			$end_date = date('Y-m-d 25:30:00', strtotime($user_select_date));

			list($date, $time) = explode(' ',  $start_date);
			list($year, $month, $day) = explode('-', $date);
			list($hour, $minute, $second) = explode(':', $time);
			$first = mktime($hour, $minute, $second, $month, $day, $year);

			list($date, $time) = explode(' ',  $end_date);
			list($year, $month, $day) = explode('-', $date);
			list($hour, $minute, $second) = explode(':', $time);
			$second = mktime($hour, $minute, $second, $month, $day, $year);



			// withdate query
			// -----------------------------------------------------------------------------------------

			if ($status == 0) {
				// =========================================================================================
				if ($priority == '0') {
					$data['refer_data'] = $this->db->query("SELECT t1.id,t1.crm_name,t1.status,t1.role,t2.crm_company_name,t2.crm_company_status,t2.status as client_status,t2.crm_legal_form FROM user
t1 INNER JOIN client t2 ON t1.id =t2.user_id WHERE t1.CreatedTime between '" . $first . "' and '" . $second . "' and t1.crm_name!='' order by t1.id DESC")->result_array();
				} else {
					$data['refer_data'] = $this->db->query("SELECT t1.id,t1.crm_name,t1.status,t1.role,t2.crm_company_name,t2.crm_company_status,t2.status as client_status,t2.crm_legal_form FROM user
t1 INNER JOIN client t2 ON t1.id =t2.user_id WHERE t1.CreatedTime between '" . $first . "' and '" . $second . "' and t1.crm_name!='' and t2.crm_legal_form='" . $priority . "' order by t1.id DESC")->result_array();
				}
				// ===========================================================================================
			}
			// *******************************************************************************************
			else {
				// ===========================================================================================
				if ($priority == '0') {
					$data['refer_data'] = $this->db->query("SELECT t1.id,t1.crm_name,t1.status,t1.role,t2.crm_company_name,t2.crm_company_status,t2.status as client_status,t2.crm_legal_form FROM user
t1 INNER JOIN client t2 ON t1.id =t2.user_id WHERE t1.CreatedTime between '" . $first . "' and '" . $second . "' and t1.crm_name!='' and t1.status='" . $status . "' or t1.CreatedTime between '" . $first . "' and '" . $second . "' and t1.crm_name!='' and t1.status='" . $state . "' order by t1.id DESC")->result_array();
				} else {
					$data['refer_data'] = $this->db->query("SELECT t1.id,t1.crm_name,t1.status,t1.role,t2.crm_company_name,t2.crm_company_status,t2.status as client_status,t2.crm_legal_form FROM user
t1 INNER JOIN client t2 ON t1.id =t2.user_id WHERE t1.CreatedTime between '" . $first . "' and '" . $second . "' and t1.crm_name!='' and t1.status='" . $status . "' and t2.crm_legal_form='" . $priority . "' or t1.CreatedTime between '" . $first . "' and '" . $second . "' and t1.crm_name!='' and t1.status='" . $state . "' and t2.crm_legal_form='" . $priority . "' order by t1.id DESC")->result_array();
				}
				// ==========================================================================================

			}
			// *****************************************************************************************
			// ------------------------------------------------------------------------------------------

		} else {
			$status = $_GET['stati'];
			$priority = $_GET['pe'];



			//without date query
			// -----------------------------------------------------------------------------------     
			if ($status == 0) {
				// ===================================================================================
				if ($priority == '0') {
					$data['refer_data'] = $this->db->query("SELECT t1.id,t1.crm_name,t1.status,t1.role,t2.crm_company_name,t2.crm_company_status,t2.status as client_status,t2.crm_legal_form FROM user
t1 INNER JOIN client t2 ON t1.id =t2.user_id WHERE t1.crm_name!='' order by t1.id DESC")->result_array();
				} else {
					$data['refer_data'] = $this->db->query("SELECT t1.id,t1.crm_name,t1.status,t1.role,t2.crm_company_name,t2.crm_company_status,t2.status as client_status,t2.crm_legal_form FROM user
t1 INNER JOIN client t2 ON t1.id =t2.user_id WHERE t1.crm_name!='' and t2.crm_legal_form='" . $priority . "' order by t1.id DESC")->result_array();
				}
				//==========================================================================================
			}
			// *****************************************************************************************
			else {
				// =========================================================================================
				if ($priority == '0') {
					$data['refer_data'] = $this->db->query("SELECT t1.id,t1.crm_name,t1.status,t1.role,t2.crm_company_name,t2.crm_company_status,t2.status as client_status,t2.crm_legal_form FROM user
t1 INNER JOIN client t2 ON t1.id =t2.user_id WHERE t1.crm_name!='' and t1.status='" . $status . "' or  t1.crm_name!='' and t1.status='" . $state . "' order by t1.id DESC")->result_array();
				} else {
					$data['refer_data'] = $this->db->query("SELECT t1.id,t1.crm_name,t1.status,t1.role,t2.crm_company_name,t2.crm_company_status,t2.status as client_status,t2.crm_legal_form FROM user
t1 INNER JOIN client t2 ON t1.id =t2.user_id WHERE t1.crm_name!='' and t1.status='" . $status . "' and t2.crm_legal_form='" . $priority . "' or t1.crm_name!='' and t1.status='" . $state . "' and t2.crm_legal_form='" . $priority . "' order by t1.id DESC")->result_array();
				}
				// ==========================================================================================
			}
			// ******************************************************************************************
			// ------------------------------------------------------------------------------------------


		}
		$data['column_setting'] = $this->Common_mdl->getallrecords('column_setting_new');
		$data['getallUser'] = $data['refer_data'];

		//print_r ( $data ); die;

		$this->load->view("users/print_page", $data);
	}

	public function update_column_setting_new()
	{

		//
		//echo "<pre>";print_r($_POST);die;

		if (isset($_POST)) {



			$data1['user_id'] = $_POST['user_id'];
			// required information

			if (!empty($_POST['confirm']))
				$data1['conf_statement'] = json_encode($_POST['confirm']);
			else
				$data1['conf_statement'] = '';

			if (!empty($_POST['accounts']))
				$data1['accounts'] = json_encode($_POST['accounts']);
			else
				$data1['accounts'] = '';

			if (!empty($_POST['companytax']))
				$data1['company_tax_return'] = json_encode($_POST['companytax']);
			else
				$data1['company_tax_return'] = '';
			if (!empty($_POST['personaltax']))
				$data1['personal_tax_return'] = json_encode($_POST['personaltax']);
			else
				$data1['personal_tax_return'] = '';

			if (!empty($_POST['payroll']))
				$data1['payroll'] = json_encode($_POST['payroll']);
			else
				$data1['payroll'] = '';

			if (!empty($_POST['workplace']))
				$data1['workplace'] = json_encode($_POST['workplace']);
			else
				$data1['workplace'] = '';

			if (!empty($_POST['vat']))
				$data1['vat'] = json_encode($_POST['vat']);
			else
				$data1['vat'] = '';

			if (!empty($_POST['cis']))
				$data1['cis'] = json_encode($_POST['cis']);
			else
				$data1['cis'] = '';

			if (!empty($_POST['cissub']))
				$data1['cissub'] = json_encode($_POST['cissub']);
			else
				$data1['cissub'] = '';

			if (!empty($_POST['p11d']))
				$data1['p11d'] = json_encode($_POST['p11d']);
			else
				$data1['p11d'] = '';


			if (!empty($_POST['bookkeep']))
				$data1['bookkeep'] = json_encode($_POST['bookkeep']);
			else
				$data1['bookkeep'] = '';


			if (!empty($_POST['management']))
				$data1['management'] = json_encode($_POST['management']);
			else
				$data1['management'] = '';


			if (!empty($_POST['investgate']))
				$data1['investgate'] = json_encode($_POST['investgate']);
			else
				$data1['investgate'] = '';

			if (!empty($_POST['registered']))
				$data1['registered'] = json_encode($_POST['registered']);
			else
				$data1['registered'] = '';

			if (!empty($_POST['taxadvice']))
				$data1['taxadvice'] = json_encode($_POST['taxadvice']);
			else
				$data1['taxadvice'] = '';

			if (!empty($_POST['taxinvest']))
				$data1['taxinvest'] = json_encode($_POST['taxinvest']);
			else
				$data1['taxinvest'] = '';


			$data1['crm_company_name'] = (isset($_POST['company_name']) && ($_POST['company_name'] != '')) ? $_POST['company_name'] : '';
			$data1['crm_company_number'] = (isset($_POST['company_number']) && ($_POST['company_number'] != '')) ? trim($_POST['company_number']) : '';
			$data1['crm_incorporation_date'] = (isset($_POST['date_of_creation']) && ($_POST['date_of_creation'] != '')) ? $_POST['date_of_creation'] : '';
			$data1['crm_registered_in'] = (isset($_POST['registered_in']) && ($_POST['registered_in'] != '')) ? $_POST['registered_in'] : '';
			$data1['crm_address_line_one'] = (isset($_POST['address_line_one']) && ($_POST['address_line_one'] != '')) ? $_POST['address_line_one'] : '';
			$data1['crm_address_line_two'] = (isset($_POST['address_line_two']) && ($_POST['address_line_two'] != '')) ? $_POST['address_line_two'] : '';
			$data1['crm_address_line_three'] = (isset($_POST['address_line_three']) && ($_POST['address_line_three'] != '')) ? $_POST['address_line_three'] : '';
			$data1['crm_town_city'] = (isset($_POST['crm_town_city']) && ($_POST['crm_town_city'] != '')) ? $_POST['crm_town_city'] : '';
			$data1['crm_post_code'] = (isset($_POST['crm_post_code']) && ($_POST['crm_post_code'] != '')) ? $_POST['crm_post_code'] : '';
			$data1['crm_company_status'] = (isset($_POST['company_status']) && ($_POST['company_status'] != '')) ? $_POST['company_status'] : '';
			$data1['crm_company_type'] = (isset($_POST['company_type']) && ($_POST['company_type'] != '')) ? $_POST['company_type'] : '';
			$data1['crm_company_sic'] = (isset($_POST['company_sic']) && ($_POST['company_sic'] != '')) ? $_POST['company_sic'] : '';

			$data1['crm_company_name1'] = (isset($_POST['company_name1']) && ($_POST['company_name1'] != '')) ? $_POST['company_name1'] : '';
			$data1['crm_legal_form'] = (isset($_POST['legal_form']) && ($_POST['legal_form'] != '')) ? $_POST['legal_form'] : '';
			$data1['crm_allocation_holder'] = (isset($_POST['allocation_holder']) && ($_POST['allocation_holder'] != '')) ? $_POST['allocation_holder'] : '';



			$data1['crm_accounting_system'] = (isset($_POST['accounting_system']) && ($_POST['accounting_system'] != '')) ? $_POST['accounting_system'] : '';
			$data1['crm_companies_house_authorisation_code'] = (isset($_POST['auth_code']) && ($_POST['auth_code'] != '')) ? $_POST['auth_code'] : '';
			$data1['crm_company_utr'] = (isset($_POST['crm_company_utr']) && ($_POST['crm_company_utr'] != '')) ? $_POST['crm_company_utr'] : '';
			$data1['crm_accounts_office_reference'] = (isset($_POST['acc_ref_number']) && ($_POST['acc_ref_number'] != '')) ? $_POST['acc_ref_number'] : '';

			$data1['crm_vat_number'] = (isset($_POST['vat_number']) && ($_POST['vat_number'] != '')) ? $_POST['vat_number'] : '';




			//$data1['status']=(isset($_POST['company_house'])&&($_POST['company_house']!=''))? $_POST['company_house'] : 0;


			// client information
			$data1['crm_letter_sign'] = (isset($_POST['letter_sign']) && ($_POST['letter_sign'] != '')) ? $_POST['letter_sign'] : 0;
			$data1['crm_business_website'] = (isset($_POST['business_website']) && ($_POST['business_website'] != '')) ? $_POST['business_website'] : '';
			$data1['crm_paye_ref_number'] = (isset($_POST['paye_ref_number']) && ($_POST['paye_ref_number'] != '')) ? $_POST['paye_ref_number'] : 0;

			//Assign to
			$data1['staff_manager'] = (isset($_POST['staff_manager']) && ($_POST['staff_manager'] != '')) ? $_POST['staff_manager'] : '';
			$data1['staff_managed'] = (isset($_POST['staff_managed']) && ($_POST['staff_managed'] != '')) ? $_POST['staff_managed'] : '';
			$data1['team'] = (isset($_POST['team']) && ($_POST['team'] != '')) ? $_POST['team'] : '';
			$data1['team_allocation'] = (isset($_POST['team_allocation']) && ($_POST['team_allocation'] != '')) ? $_POST['team_allocation'] : '';
			$data1['department'] = (isset($_POST['department']) && ($_POST['department'] != '')) ? $_POST['department'] : '';
			$data1['department_allocation'] = (isset($_POST['department_allocation']) && ($_POST['department_allocation'] != '')) ? $_POST['department_allocation'] : '';
			$data1['member_manager'] = (isset($_POST['member_manager']) && ($_POST['member_manager'] != '')) ? $_POST['member_manager'] : '';
			$data1['member_managed'] = (isset($_POST['member_managed']) && ($_POST['member_managed'] != '')) ? $_POST['member_managed'] : '';


			$data1['crm_assign_client_id_verified'] = (isset($_POST['client_id_verified']) && ($_POST['client_id_verified'] != '')) ? $_POST['client_id_verified'] : '';
			$data1['crm_assign_type_of_id'] = (isset($_POST['type_of_id']) && ($_POST['type_of_id'] != '')) ? $_POST['type_of_id'] : '';
			$data1['crm_assign_proof_of_address'] = (isset($_POST['proof_of_address']) && ($_POST['proof_of_address'] != '')) ? $_POST['proof_of_address'] : '';
			$data1['crm_assign_meeting_client'] = (isset($_POST['meeting_client']) && ($_POST['meeting_client'] != '')) ? $_POST['meeting_client'] : '';
			$data1['crm_assign_source'] = (isset($_POST['source']) && ($_POST['source'] != '')) ? $_POST['source'] : '';
			$data1['crm_refered_by'] = (isset($_POST['refer_exist_client']) && ($_POST['refer_exist_client'] != '')) ? $_POST['refer_exist_client'] : '';
			$data1['crm_assign_relationship_client'] = (isset($_POST['relationship_client']) && ($_POST['relationship_client'] != '')) ? $_POST['relationship_client'] : '';
			$data1['crm_assign_notes'] = (isset($_POST['assign_notes']) && ($_POST['assign_notes'] != '')) ? $_POST['assign_notes'] : '';

			// other
			$data1['crm_previous_accountant'] = (isset($_POST['previous_account']) && ($_POST['previous_account'] != '')) ? $_POST['previous_account'] : '';
			$data1['crm_other_name_of_firm'] = (isset($_POST['name_of_firm']) && ($_POST['name_of_firm'] != '')) ? $_POST['name_of_firm'] : '';
			$data1['crm_other_address'] = (isset($_POST['other_address']) && ($_POST['other_address'] != '')) ? $_POST['other_address'] : '';
			$data1['crm_other_contact_no'] = (isset($_POST['pn_no_rec']) && ($_POST['pn_no_rec'] != '')) ? $_POST['cun_code'] . $_POST['pn_no_rec'] : '';
			$data1['crm_email'] = (isset($_POST['emailid']) && ($_POST['emailid'] != '')) ? $_POST['emailid'] : '';
			$data1['crm_other_chase_for_info'] = (isset($_POST['chase_for_info']) && ($_POST['chase_for_info'] != '')) ? $_POST['chase_for_info'] : '';
			$data1['crm_other_notes'] = (isset($_POST['other_notes']) && ($_POST['other_notes'] != '')) ? $_POST['other_notes'] : '';
			$data1['crm_other_internal_notes'] = (isset($_POST['other_internal_notes']) && ($_POST['other_internal_notes'] != '')) ? $_POST['other_internal_notes'] : '';
			$data1['crm_other_invite_use'] = (isset($_POST['invite_use']) && ($_POST['invite_use'] != '')) ? $_POST['invite_use'] : '';
			$data1['crm_other_crm'] = (isset($_POST['other_crm']) && ($_POST['other_crm'] != '')) ? $_POST['other_crm'] : '';
			$data1['crm_other_proposal'] = (isset($_POST['other_proposal']) && ($_POST['other_proposal'] != '')) ? $_POST['other_proposal'] : '';
			$data1['crm_other_task'] = (isset($_POST['other_task']) && ($_POST['other_task'] != '')) ? $_POST['other_task'] : '';
			$data1['crm_other_send_invit_link'] = (isset($_POST['send_invit_link']) && ($_POST['send_invit_link'] != '')) ? $_POST['send_invit_link'] : '';
			$data1['crm_other_username'] = (isset($_POST['user_name']) && ($_POST['user_name'] != '')) ? $_POST['user_name'] : '';
			$data1['crm_other_password'] = (isset($_POST['password']) && ($_POST['password'] != '')) ? $_POST['password'] : '';
			$data1['crm_other_any_notes'] = (isset($_POST['other_any_notes']) && ($_POST['other_any_notes'] != '')) ? $_POST['other_any_notes'] : '';

			// confirmation
			$data1['crm_confirmation_auth_code'] = (isset($_POST['confirmation_auth_code']) && ($_POST['confirmation_auth_code'] != '')) ? $_POST['confirmation_auth_code'] : '';
			$data1['crm_confirmation_statement_date'] = (isset($_POST['confirmation_next_made_up_to']) && ($_POST['confirmation_next_made_up_to'] != '')) ? $_POST['confirmation_next_made_up_to'] : '';
			$data1['crm_confirmation_statement_due_date'] = (isset($_POST['confirmation_next_due']) && ($_POST['confirmation_next_due'] != '')) ? $_POST['confirmation_next_due'] : '';
			$data1['crm_confirmation_email_remainder'] = (isset($_POST['confirmation_next_reminder']) && ($_POST['confirmation_next_reminder'] != '')) ? $_POST['confirmation_next_reminder'] : '';
			$data1['crm_confirmation_create_task_reminder'] = (isset($_POST['create_task_reminder']) && ($_POST['create_task_reminder'] != '')) ? $_POST['create_task_reminder'] : '';
			$data1['crm_confirmation_add_custom_reminder'] = (isset($_POST['add_custom_reminder']) && ($_POST['add_custom_reminder'] != '')) ? $_POST['add_custom_reminder'] : '';
			$data1['crm_confirmation_officers'] = (isset($_POST['confirmation_officers']) && ($_POST['confirmation_officers'] != '')) ? $_POST['confirmation_officers'] : '';
			$data1['crm_share_capital'] = (isset($_POST['share_capital']) && ($_POST['share_capital'] != '')) ? $_POST['share_capital'] : '';
			$data1['crm_shareholders'] = (isset($_POST['shareholders']) && ($_POST['shareholders'] != '')) ? $_POST['shareholders'] : '';
			$data1['crm_people_with_significant_control'] = (isset($_POST['people_with_significant_control']) && ($_POST['people_with_significant_control'] != '')) ? $_POST['people_with_significant_control'] : '';
			$data1['crm_confirmation_notes'] = (isset($_POST['confirmation_notes']) && ($_POST['confirmation_notes'] != '')) ? $_POST['confirmation_notes'] : '';

			// accounts

			$data1['crm_accounts_auth_code'] = (isset($_POST['accounts_auth_code']) && ($_POST['accounts_auth_code'] != '')) ? $_POST['accounts_auth_code'] : '';
			$data1['crm_accounts_utr_number'] = (isset($_POST['accounts_utr_number']) && ($_POST['accounts_utr_number'] != '')) ? $_POST['accounts_utr_number'] : '';
			$data1['crm_companies_house_email_remainder'] = (isset($_POST['company_house_reminder']) && ($_POST['company_house_reminder'] != '')) ? $_POST['company_house_reminder'] : '';
			$data1['crm_ch_yearend'] = (isset($_POST['accounts_next_made_up_to']) && ($_POST['accounts_next_made_up_to'] != '')) ? $_POST['accounts_next_made_up_to'] : '';
			$data1['crm_ch_accounts_next_due'] = (isset($_POST['accounts_next_due']) && ($_POST['accounts_next_due'] != '')) ? $_POST['accounts_next_due'] : '';
			$data1['crm_accounts_next_reminder_date'] = (isset($_POST['accounts_next_reminder_date']) && ($_POST['accounts_next_reminder_date'] != '')) ? $_POST['accounts_next_reminder_date'] : '';
			$data1['crm_accounts_create_task_reminder'] = (isset($_POST['accounts_create_task_reminder']) && ($_POST['accounts_create_task_reminder'] != '')) ? $_POST['accounts_create_task_reminder'] : '';
			$data1['crm_accounts_custom_reminder'] = (isset($_POST['accounts_custom_reminder']) && ($_POST['accounts_custom_reminder'] != '')) ? $_POST['accounts_custom_reminder'] : '';
			$data1['crm_accounts_due_date_hmrc'] = (isset($_POST['accounts_due_date_hmrc']) && ($_POST['accounts_due_date_hmrc'] != '')) ? $_POST['accounts_due_date_hmrc'] : '';
			$data1['crm_accounts_tax_date_hmrc'] = (isset($_POST['accounts_tax_date_hmrc']) && ($_POST['accounts_tax_date_hmrc'] != '')) ? $_POST['accounts_tax_date_hmrc'] : '';
			$data1['crm_company_next_reminder_date'] = (isset($_POST['company_next_reminder_date']) && ($_POST['company_next_reminder_date'] != '')) ? $_POST['company_next_reminder_date'] : '';
			$data1['crm_company_create_task_reminder'] = (isset($_POST['company_create_task_reminder']) && ($_POST['company_create_task_reminder'] != '')) ? $_POST['company_create_task_reminder'] : '';
			$data1['crm_company_custom_reminder'] = (isset($_POST['company_custom_reminder']) && ($_POST['company_custom_reminder'] != '')) ? $_POST['company_custom_reminder'] : '';
			$data1['crm_accounts_notes'] = (isset($_POST['accounts_notes']) && ($_POST['accounts_notes'] != '')) ? $_POST['accounts_notes'] : '';

			// personal tax return

			$data1['crm_personal_utr_number'] = (isset($_POST['personal_utr_number']) && ($_POST['personal_utr_number'] != '')) ? $_POST['personal_utr_number'] : '';
			$data1['crm_ni_number'] = (isset($_POST['ni_number']) && ($_POST['ni_number'] != '')) ? $_POST['ni_number'] : '';
			$data1['crm_property_income'] = (isset($_POST['property_income']) && ($_POST['property_income'] != '')) ? $_POST['property_income'] : '';
			$data1['crm_additional_income'] = (isset($_POST['additional_income']) && ($_POST['additional_income'] != '')) ? $_POST['additional_income'] : '';
			$data1['crm_personal_tax_return_date'] = (isset($_POST['personal_tax_return_date']) && ($_POST['personal_tax_return_date'] != '')) ? $_POST['personal_tax_return_date'] : '';
			$data1['crm_personal_due_date_return'] = (isset($_POST['personal_due_date_return']) && ($_POST['personal_due_date_return'] != '')) ? $_POST['personal_due_date_return'] : '';
			$data1['crm_personal_due_date_online'] = (isset($_POST['personal_due_date_online']) && ($_POST['personal_due_date_online'] != '')) ? $_POST['personal_due_date_online'] : '';
			$data1['crm_personal_next_reminder_date'] = (isset($_POST['personal_next_reminder_date']) && ($_POST['personal_next_reminder_date'] != '')) ? $_POST['personal_next_reminder_date'] : '';
			$data1['crm_personal_task_reminder'] = (isset($_POST['personal_task_reminder']) && ($_POST['personal_task_reminder'] != '')) ? $_POST['personal_task_reminder'] : '';
			$data1['crm_personal_custom_reminder'] = (isset($_POST['personal_custom_reminder']) && ($_POST['personal_custom_reminder'] != '')) ? $_POST['personal_custom_reminder'] : '';
			$data1['crm_personal_notes'] = (isset($_POST['personal_notes']) && ($_POST['personal_notes'] != '')) ? $_POST['personal_notes'] : '';

			// payroll
			$data1['crm_payroll_acco_off_ref_no'] = (isset($_POST['payroll_acco_off_ref_no']) && ($_POST['payroll_acco_off_ref_no'] != '')) ? $_POST['payroll_acco_off_ref_no'] : '';
			$data1['crm_paye_off_ref_no'] = (isset($_POST['paye_off_ref_no']) && ($_POST['paye_off_ref_no'] != '')) ? $_POST['paye_off_ref_no'] : '';
			$data1['crm_payroll_reg_date'] = (isset($_POST['payroll_reg_date']) && ($_POST['payroll_reg_date'] != '')) ? $_POST['payroll_reg_date'] : '';
			$data1['crm_no_of_employees'] = (isset($_POST['no_of_employees']) && ($_POST['no_of_employees'] != '')) ? $_POST['no_of_employees'] : '';
			$data1['crm_first_pay_date'] = (isset($_POST['first_pay_date']) && ($_POST['first_pay_date'] != '')) ? $_POST['first_pay_date'] : '';
			$data1['crm_payroll_run'] = (isset($_POST['payroll_run']) && ($_POST['payroll_run'] != '')) ? $_POST['payroll_run'] : '';
			$data1['crm_payroll_run_date'] = (isset($_POST['payroll_run_date']) && ($_POST['payroll_run_date'] != '')) ? $_POST['payroll_run_date'] : '';
			$data1['crm_rti_deadline'] = (isset($_POST['rti_deadline']) && ($_POST['rti_deadline'] != '')) ? $_POST['rti_deadline'] : '';
			$data1['crm_previous_year_require'] = (isset($_POST['previous_year_require']) && ($_POST['previous_year_require'] != '')) ? $_POST['previous_year_require'] : '';
			$data1['crm_payroll_if_yes'] = (isset($_POST['payroll_if_yes']) && ($_POST['payroll_if_yes'] != '')) ? $_POST['payroll_if_yes'] : '';
			$data1['crm_paye_scheme_ceased'] = (isset($_POST['paye_scheme_ceased']) && ($_POST['paye_scheme_ceased'] != '')) ? $_POST['paye_scheme_ceased'] : '';
			$data1['crm_payroll_next_reminder_date'] = (isset($_POST['payroll_next_reminder_date']) && ($_POST['payroll_next_reminder_date'] != '')) ? $_POST['payroll_next_reminder_date'] : '';
			$data1['crm_payroll_create_task_reminder'] = (isset($_POST['payroll_create_task_reminder']) && ($_POST['payroll_create_task_reminder'] != '')) ? $_POST['payroll_create_task_reminder'] : '';
			$data1['crm_payroll_add_custom_reminder'] = (isset($_POST['payroll_add_custom_reminder']) && ($_POST['payroll_add_custom_reminder'] != '')) ? $_POST['payroll_add_custom_reminder'] : '';
			$data1['crm_staging_date'] = (isset($_POST['staging_date']) && ($_POST['staging_date'] != '')) ? $_POST['staging_date'] : '';
			$data1['crm_pension_id'] = (isset($_POST['pension_id']) && ($_POST['pension_id'] != '')) ? $_POST['pension_id'] : '';
			$data1['crm_pension_subm_due_date'] = (isset($_POST['pension_subm_due_date']) && ($_POST['pension_subm_due_date'] != '')) ? $_POST['pension_subm_due_date'] : '';
			$data1['crm_postponement_date'] = (isset($_POST['defer_post_upto']) && ($_POST['defer_post_upto'] != '')) ? $_POST['defer_post_upto'] : '';
			$data1['crm_the_pensions_regulator_opt_out_date'] = (isset($_POST['pension_regulator_date']) && ($_POST['pension_regulator_date'] != '')) ? $_POST['pension_regulator_date'] : '';
			$data1['crm_re_enrolment_date'] = (isset($_POST['paye_re_enrolment_date']) && ($_POST['paye_re_enrolment_date'] != '')) ? $_POST['paye_re_enrolment_date'] : '';
			$data1['crm_declaration_of_compliance_due_date  '] = (isset($_POST['declaration_of_compliance_due_date']) && ($_POST['declaration_of_compliance_due_date'] != '')) ? $_POST['declaration_of_compliance_due_date'] : '';
			$data1['crm_declaration_of_compliance_submission  '] = (isset($_POST['declaration_of_compliance_last_filed']) && ($_POST['declaration_of_compliance_last_filed'] != '')) ? $_POST['declaration_of_compliance_last_filed'] : '';
			$data1['crm_paye_pension_provider'] = (isset($_POST['paye_pension_provider']) && ($_POST['paye_pension_provider'] != '')) ? $_POST['paye_pension_provider'] : '';
			$data1['crm_pension_id'] = (isset($_POST['paye_pension_provider_userid']) && ($_POST['paye_pension_provider_userid'] != '')) ? $_POST['paye_pension_provider_userid'] : '';
			$data1['crm_paye_pension_provider_password'] = (isset($_POST['paye_pension_provider_password']) && ($_POST['paye_pension_provider_password'] != '')) ? $_POST['paye_pension_provider_password'] : '';
			$data1['crm_employer_contri_percentage'] = (isset($_POST['employer_contri_percentage']) && ($_POST['employer_contri_percentage'] != '')) ? $_POST['employer_contri_percentage'] : '';
			$data1['crm_employee_contri_percentage'] = (isset($_POST['employee_contri_percentage']) && ($_POST['employee_contri_percentage'] != '')) ? $_POST['employee_contri_percentage'] : '';

			$data1['crm_pension_notes'] = (isset($_POST['pension_notes']) && ($_POST['pension_notes'] != '')) ? $_POST['pension_notes'] : '';
			$data1['crm_pension_next_reminder_date'] = (isset($_POST['pension_next_reminder_date']) && ($_POST['pension_next_reminder_date'] != '')) ? $_POST['pension_next_reminder_date'] : '';
			$data1['crm_pension_create_task_reminder'] = (isset($_POST['pension_create_task_reminder']) && ($_POST['pension_create_task_reminder'] != '')) ? $_POST['pension_create_task_reminder'] : '';
			$data1['crm_pension_add_custom_reminder'] = (isset($_POST['pension_add_custom_reminder']) && ($_POST['pension_add_custom_reminder'] != '')) ? $_POST['pension_add_custom_reminder'] : '';
			$data1['crm_cis_contractor'] = (isset($_POST['cis_contractor']) && ($_POST['cis_contractor'] != '')) ? $_POST['cis_contractor'] : '';
			$data1['crm_cis_contractor_start_date'] = (isset($_POST['cis_contractor_start_date']) && ($_POST['cis_contractor_start_date'] != '')) ? $_POST['cis_contractor_start_date'] : '';
			$data1['crm_cis_scheme_notes'] = (isset($_POST['cis_scheme_notes']) && ($_POST['cis_scheme_notes'] != '')) ? $_POST['cis_scheme_notes'] : '';
			$data1['crm_cis_subcontractor'] = (isset($_POST['cis_subcontractor']) && ($_POST['cis_subcontractor'] != '')) ? $_POST['cis_subcontractor'] : '';
			$data1['crm_cis_subcontractor_start_date'] = (isset($_POST['cis_subcontractor_start_date']) && ($_POST['cis_subcontractor_start_date'] != '')) ? $_POST['cis_subcontractor_start_date'] : '';
			$data1['crm_cis_subcontractor_scheme_notes'] = (isset($_POST['cis_subcontractor_scheme_notes']) && ($_POST['cis_subcontractor_scheme_notes'] != '')) ? $_POST['cis_subcontractor_scheme_notes'] : '';
			$data1['crm_cis_next_reminder_date'] = (isset($_POST['cis_next_reminder_date']) && ($_POST['cis_next_reminder_date'] != '')) ? $_POST['cis_next_reminder_date'] : '';
			$data1['crm_cis_create_task_reminder'] = (isset($_POST['cis_create_task_reminder']) && ($_POST['cis_create_task_reminder'] != '')) ? $_POST['cis_create_task_reminder'] : '';
			$data1['crm_cis_add_custom_reminder'] = (isset($_POST['cis_add_custom_reminder']) && ($_POST['cis_add_custom_reminder'] != '')) ? $_POST['cis_add_custom_reminder'] : '';
			$data1['crm_p11d_latest_action_date'] = (isset($_POST['p11d_start_date']) && ($_POST['p11d_start_date'] != '')) ? $_POST['p11d_start_date'] : '';
			$data1['crm_p11d_latest_action'] = (isset($_POST['p11d_todo']) && ($_POST['p11d_todo'] != '')) ? $_POST['p11d_todo'] : '';
			$data1['crm_latest_p11d_submitted'] = (isset($_POST['p11d_first_benefit_date']) && ($_POST['p11d_first_benefit_date'] != '')) ? $_POST['p11d_first_benefit_date'] : '';
			$data1['crm_next_p11d_return_due'] = (isset($_POST['p11d_due_date']) && ($_POST['p11d_due_date'] != '')) ? $_POST['p11d_due_date'] : '';
			$data1['crm_p11d_previous_year_require'] = (isset($_POST['p11d_previous_year_require']) && ($_POST['p11d_previous_year_require'] != '')) ? $_POST['p11d_previous_year_require'] : '';
			$data1['crm_p11d_payroll_if_yes'] = (isset($_POST['p11d_payroll_if_yes']) && ($_POST['p11d_payroll_if_yes'] != '')) ? $_POST['p11d_payroll_if_yes'] : '';
			$data1['crm_p11d_records_received'] = (isset($_POST['p11d_paye_scheme_ceased']) && ($_POST['p11d_paye_scheme_ceased'] != '')) ? $_POST['p11d_paye_scheme_ceased'] : '';
			$data1['crm_p11d_next_reminder_date'] = (isset($_POST['p11d_next_reminder_date']) && ($_POST['p11d_next_reminder_date'] != '')) ? $_POST['p11d_next_reminder_date'] : '';
			$data1['crm_p11d_create_task_reminder'] = (isset($_POST['p11d_create_task_reminder']) && ($_POST['p11d_create_task_reminder'] != '')) ? $_POST['p11d_create_task_reminder'] : '';
			$data1['crm_p11d_add_custom_reminder'] = (isset($_POST['p11d_add_custom_reminder']) && ($_POST['p11d_add_custom_reminder'] != '')) ? $_POST['p11d_add_custom_reminder'] : '';
			$data1['crm_p11d_notes'] = (isset($_POST['p11d_notes']) && ($_POST['p11d_notes'] != '')) ? $_POST['p11d_notes'] : '';

			//vat return

			$data1['crm_vat_number_one'] = (isset($_POST['vat_number_one']) && ($_POST['vat_number_one'] != '')) ? $_POST['vat_number_one'] : '';
			$data1['crm_vat_date_of_registration'] = (isset($_POST['vat_registration_date']) && ($_POST['vat_registration_date'] != '')) ? $_POST['vat_registration_date'] : '';
			$data1['crm_vat_frequency'] = (isset($_POST['vat_frequency']) && ($_POST['vat_frequency'] != '')) ? $_POST['vat_frequency'] : '';
			$data1['crm_vat_quater_end_date'] = (isset($_POST['vat_quater_end_date']) && ($_POST['vat_quater_end_date'] != '')) ? $_POST['vat_quater_end_date'] : '';
			$data1['crm_vat_quarters'] = (isset($_POST['vat_quarters']) && ($_POST['vat_quarters'] != '')) ? $_POST['vat_quarters'] : '0';
			$data1['crm_vat_due_date'] = (isset($_POST['vat_due_date']) && ($_POST['vat_due_date'] != '')) ? $_POST['vat_due_date'] : '';
			$data1['crm_vat_scheme'] = (isset($_POST['vat_scheme']) && ($_POST['vat_scheme'] != '')) ? $_POST['vat_scheme'] : '';
			$data1['crm_flat_rate_category'] = (isset($_POST['flat_rate_category']) && ($_POST['flat_rate_category'] != '')) ? $_POST['flat_rate_category'] : '';
			$data1['crm_flat_rate_percentage'] = (isset($_POST['flat_rate_percentage']) && ($_POST['flat_rate_percentage'] != '')) ? $_POST['flat_rate_percentage'] : '';
			$data1['crm_direct_debit'] = (isset($_POST['direct_debit']) && ($_POST['direct_debit'] != '')) ? $_POST['direct_debit'] : '';
			$data1['crm_annual_accounting_scheme'] = (isset($_POST['annual_accounting_scheme']) && ($_POST['annual_accounting_scheme'] != '')) ? $_POST['annual_accounting_scheme'] : '';
			$data1['crm_box5_of_last_quarter_submitted'] = (isset($_POST['box5_of_last_quarter_submitted']) && ($_POST['box5_of_last_quarter_submitted'] != '')) ? $_POST['box5_of_last_quarter_submitted'] : '';
			$data1['crm_vat_address'] = (isset($_POST['vat_address']) && ($_POST['vat_address'] != '')) ? $_POST['vat_address'] : '';
			$data1['crm_vat_next_reminder_date'] = (isset($_POST['vat_next_reminder_date']) && ($_POST['vat_next_reminder_date'] != '')) ? $_POST['vat_next_reminder_date'] : '';
			$data1['crm_vat_create_task_reminder'] = (isset($_POST['vat_create_task_reminder']) && ($_POST['vat_create_task_reminder'] != '')) ? $_POST['vat_create_task_reminder'] : '';
			$data1['crm_vat_add_custom_reminder'] = (isset($_POST['vat_add_custom_reminder']) && ($_POST['vat_add_custom_reminder'] != '')) ? $_POST['vat_add_custom_reminder'] : '';
			$data1['crm_vat_notes'] = (isset($_POST['vat_notes']) && ($_POST['vat_notes'] != '')) ? $_POST['vat_notes'] : '';

			//management accounts
			$data1['crm_bookkeeping'] = (isset($_POST['bookkeeping']) && ($_POST['bookkeeping'] != '')) ? $_POST['bookkeeping'] : '';
			$data1['crm_next_booking_date'] = (isset($_POST['next_booking_date']) && ($_POST['next_booking_date'] != '')) ? $_POST['next_booking_date'] : '';
			$data1['crm_method_bookkeeping'] = (isset($_POST['method_bookkeeping']) && ($_POST['method_bookkeeping'] != '')) ? $_POST['method_bookkeeping'] : '';
			$data1['crm_client_provide_record'] = (isset($_POST['client_provide_record']) && ($_POST['client_provide_record'] != '')) ? $_POST['client_provide_record'] : '';
			$data1['crm_bookkeep_next_reminder_date'] = (isset($_POST['bookkeep_next_reminder_date']) && ($_POST['bookkeep_next_reminder_date'] != '')) ? $_POST['bookkeep_next_reminder_date'] : '';
			$data1['crm_bookkeep_create_task_reminder'] = (isset($_POST['bookkeep_create_task_reminder']) && ($_POST['bookkeep_create_task_reminder'] != '')) ? $_POST['bookkeep_create_task_reminder'] : '';
			$data1['crm_bookkeep_add_custom_reminder'] = (isset($_POST['bookkeep_add_custom_reminder']) && ($_POST['bookkeep_add_custom_reminder'] != '')) ? $_POST['bookkeep_add_custom_reminder'] : '';
			$data1['crm_manage_acc_fre'] = (isset($_POST['manage_acc_fre']) && ($_POST['manage_acc_fre'] != '')) ? $_POST['manage_acc_fre'] : '';
			$data1['crm_next_manage_acc_date'] = (isset($_POST['next_manage_acc_date']) && ($_POST['next_manage_acc_date'] != '')) ? $_POST['next_manage_acc_date'] : '';
			$data1['crm_manage_method_bookkeeping'] = (isset($_POST['manage_method_bookkeeping']) && ($_POST['manage_method_bookkeeping'] != '')) ? $_POST['manage_method_bookkeeping'] : '';
			$data1['crm_manage_client_provide_record'] = (isset($_POST['manage_client_provide_record']) && ($_POST['manage_client_provide_record'] != '')) ? $_POST['manage_client_provide_record'] : '';
			$data1['crm_manage_next_reminder_date'] = (isset($_POST['manage_next_reminder_date']) && ($_POST['manage_next_reminder_date'] != '')) ? $_POST['manage_next_reminder_date'] : '';
			$data1['crm_manage_create_task_reminder'] = (isset($_POST['manage_create_task_reminder']) && ($_POST['manage_create_task_reminder'] != '')) ? $_POST['manage_create_task_reminder'] : '';
			$data1['crm_manage_add_custom_reminder'] = (isset($_POST['manage_add_custom_reminder']) && ($_POST['manage_add_custom_reminder'] != '')) ? $_POST['manage_add_custom_reminder'] : '';
			$data1['crm_manage_notes'] = (isset($_POST['manage_notes']) && ($_POST['manage_notes'] != '')) ? $_POST['manage_notes'] : '';

			//Investigation Insurance

			$data1['crm_invesitgation_insurance'] = (isset($_POST['insurance_start_date']) && ($_POST['insurance_start_date'] != '')) ? $_POST['insurance_start_date'] : '';
			$data1['crm_insurance_renew_date'] = (isset($_POST['insurance_renew_date']) && ($_POST['insurance_renew_date'] != '')) ? $_POST['insurance_renew_date'] : '';
			$data1['crm_insurance_provider'] = (isset($_POST['insurance_provider']) && ($_POST['insurance_provider'] != '')) ? $_POST['insurance_provider'] : '';
			$data1['crm_claims_note'] = (isset($_POST['claims_note']) && ($_POST['claims_note'] != '')) ? $_POST['claims_note'] : '';
			$data1['crm_insurance_next_reminder_date'] = (isset($_POST['insurance_next_reminder_date']) && ($_POST['insurance_next_reminder_date'] != '')) ? $_POST['insurance_next_reminder_date'] : '';
			$data1['crm_insurance_create_task_reminder'] = (isset($_POST['insurance_create_task_reminder']) && ($_POST['insurance_create_task_reminder'] != '')) ? $_POST['insurance_create_task_reminder'] : '';
			$data1['crm_insurance_add_custom_reminder'] = (isset($_POST['insurance_add_custom_reminder']) && ($_POST['insurance_add_custom_reminder'] != '')) ? $_POST['insurance_add_custom_reminder'] : '';
			$data1['crm_registered_start_date'] = (isset($_POST['registered_start_date']) && ($_POST['registered_start_date'] != '')) ? $_POST['registered_start_date'] : '';
			$data1['crm_registered_renew_date'] = (isset($_POST['registered_renew_date']) && ($_POST['registered_renew_date'] != '')) ? $_POST['registered_renew_date'] : '';
			$data1['crm_registered_office_inuse'] = (isset($_POST['registered_office_inuse']) && ($_POST['registered_office_inuse'] != '')) ? $_POST['registered_office_inuse'] : '';
			$data1['crm_registered_claims_note'] = (isset($_POST['registered_claims_note']) && ($_POST['registered_claims_note'] != '')) ? $_POST['registered_claims_note'] : '';
			$data1['crm_registered_next_reminder_date'] = (isset($_POST['registered_next_reminder_date']) && ($_POST['registered_next_reminder_date'] != '')) ? $_POST['registered_next_reminder_date'] : '';
			$data1['crm_registered_create_task_reminder'] = (isset($_POST['registered_create_task_reminder']) && ($_POST['registered_create_task_reminder'] != '')) ? $_POST['registered_create_task_reminder'] : '';
			$data1['crm_registered_add_custom_reminder'] = (isset($_POST['registered_add_custom_reminder']) && ($_POST['registered_add_custom_reminder'] != '')) ? $_POST['registered_add_custom_reminder'] : '';
			$data1['crm_investigation_start_date'] = (isset($_POST['investigation_start_date']) && ($_POST['investigation_start_date'] != '')) ? $_POST['investigation_start_date'] : '';
			$data1['crm_investigation_end_date'] = (isset($_POST['investigation_end_date']) && ($_POST['investigation_end_date'] != '')) ? $_POST['investigation_end_date'] : '';
			$data1['crm_investigation_note'] = (isset($_POST['investigation_note']) && ($_POST['investigation_note'] != '')) ? $_POST['investigation_note'] : '';
			$data1['crm_investigation_next_reminder_date'] = (isset($_POST['investigation_next_reminder_date']) && ($_POST['investigation_next_reminder_date'] != '')) ? $_POST['investigation_next_reminder_date'] : '';
			$data1['crm_investigation_create_task_reminder'] = (isset($_POST['investigation_create_task_reminder']) && ($_POST['investigation_create_task_reminder'] != '')) ? $_POST['investigation_create_task_reminder'] : '';
			$data1['crm_investigation_add_custom_reminder'] = (isset($_POST['investigation_add_custom_reminder']) && ($_POST['investigation_add_custom_reminder'] != '')) ? $_POST['investigation_add_custom_reminder'] : '';
			$data1['crm_tax_investigation_note'] = (isset($_POST['tax_investigation_note']) && ($_POST['tax_investigation_note'] != '')) ? $_POST['tax_investigation_note'] : '';
			$data1['select_responsible_type'] = (isset($_POST['select_responsible_type']) && ($_POST['select_responsible_type'] != '')) ? $_POST['select_responsible_type'] : '';

			// notes tab
			$data1['crm_notes_info'] = (isset($_POST['notes_info']) && ($_POST['notes_info'] != '')) ? $_POST['notes_info'] : '';
			// documents tab
			$data1['crm_documents'] = (isset($_FILES['document']['name']) && ($_FILES['document']['name'] != '')) ? $this->Common_mdl->do_upload($_FILES['document'], 'uploads') : '';

			//primary contact
			$data1['contact_title'] = (isset($_POST['contact_title']) && ($_POST['contact_title'] != '')) ? $_POST['company_name1'] : '';
			$data1['contact_first_name'] = (isset($_POST['contact_first_name']) && ($_POST['contact_first_name'] != '')) ? $_POST['contact_first_name'] : '';
			$data1['contact_middle_name'] = (isset($_POST['contact_middle_name']) && ($_POST['contact_middle_name'] != '')) ? $_POST['contact_middle_name'] : '';
			$data1['contact_surname'] = (isset($_POST['contact_surname']) && ($_POST['contact_surname'] != '')) ? $_POST['contact_surname'] : '';
			$data1['contact_preferred_name'] = (isset($_POST['contact_preferred_name']) && ($_POST['contact_preferred_name'] != '')) ? $_POST['contact_preferred_name'] : '';
			$data1['contact_mobile'] = (isset($_POST['contact_mobile']) && ($_POST['contact_mobile'] != '')) ? $_POST['contact_mobile'] : '';
			$data1['contact_main_email'] = (isset($_POST['contact_main_email']) && ($_POST['contact_main_email'] != '')) ? $_POST['contact_main_email'] : '';
			$data1['contact_nationality'] = (isset($_POST['contact_nationality']) && ($_POST['contact_nationality'] != '')) ? $_POST['contact_nationality'] : '';
			$data1['contact_psc'] = (isset($_POST['contact_psc']) && ($_POST['contact_psc'] != '')) ? $_POST['contact_psc'] : '';
			$data1['contact_shareholder'] = (isset($_POST['contact_shareholder']) && ($_POST['contact_shareholder'] != '')) ? $_POST['contact_shareholder'] : '';
			$data1['contact_ni_number'] = (isset($_POST['contact_ni_number']) && ($_POST['contact_ni_number'] != '')) ? $_POST['contact_ni_number'] : '';
			$data1['contact_country_of_residence'] = (isset($_POST['contact_country_of_residence']) && ($_POST['contact_country_of_residence'] != '')) ? $_POST['contact_country_of_residence'] : '';
			$data1['contact_type'] = (isset($_POST['contact_type']) && ($_POST['contact_type'] != '')) ? $_POST['contact_type'] : '';
			$data1['contact_other_custom'] = (isset($_POST['contact_other_custom']) && ($_POST['contact_other_custom'] != '')) ? $_POST['contact_other_custom'] : '';
			$data1['contact_address_line1'] = (isset($_POST['contact_address_line1']) && ($_POST['contact_address_line1'] != '')) ? $_POST['contact_address_line1'] : '';
			$data1['contact_address_line2'] = (isset($_POST['contact_address_line2']) && ($_POST['contact_address_line2'] != '')) ? $_POST['contact_address_line2'] : '';
			$data1['contact_town_city'] = (isset($_POST['contact_town_city']) && ($_POST['contact_town_city'] != '')) ? $_POST['contact_town_city'] : '';
			$data1['contact_post_code'] = (isset($_POST['contact_post_code']) && ($_POST['contact_post_code'] != '')) ? $_POST['contact_post_code'] : '';
			$data1['contact_landline'] = (isset($_POST['contact_landline']) && ($_POST['contact_landline'] != '')) ? $_POST['contact_landline'] : '';
			$data1['contact_work_email'] = (isset($_POST['contact_work_email']) && ($_POST['contact_work_email'] != '')) ? $_POST['contact_work_email'] : '';
			$data1['contact_date_of_birth'] = (isset($_POST['contact_date_of_birth']) && ($_POST['contact_date_of_birth'] != '')) ? $_POST['contact_date_of_birth'] : '';
			$data1['contact_nature_of_control'] = (isset($_POST['contact_nature_of_control']) && ($_POST['contact_nature_of_control'] != '')) ? $_POST['contact_nature_of_control'] : '';
			$data1['contact_marital_status'] = (isset($_POST['contact_marital_status']) && ($_POST['contact_marital_status'] != '')) ? $_POST['contact_marital_status'] : '';
			$data1['contact_utr_number'] = (isset($_POST['contact_utr_number']) && ($_POST['contact_utr_number'] != '')) ? $_POST['contact_utr_number'] : '';
			$data1['contact_occupation'] = (isset($_POST['contact_occupation']) && ($_POST['contact_occupation'] != '')) ? $_POST['contact_occupation'] : '';
			$data1['contact_appointed_on'] = (isset($_POST['contact_appointed_on']) && ($_POST['contact_appointed_on'] != '')) ? $_POST['contact_appointed_on'] : '';




			$data1['created_date'] = time();
			$data1['autosave_status'] = 0;

			$data1['status'] = 1;

			//$updates_client=$this->db->insert('column_setting_new',$data1);
			$updates_client = $this->Common_mdl->update('column_setting_new', $data1, 'id', 1);
			if ($updates_client == 1) {
				$this->session->set_flashdata('success', '<div class="alert alert-success" style="display:block;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Your Setting was Updated successfully.</div>');
				redirect('user/column_setting_new');
			} else {
				$this->session->set_flashdata('success', '<div class="alert alert-danger" style="display:block;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Try again.</div>');
				redirect('user/column_setting_new');
			}
		}
	} //fuction close

	public function member()
	{
		$department = $this->Common_mdl->GetAllWithWhere('responsible_department', 'client_id', 747);
		//$this->db->query('select * from responsible_department where client_id='.$getallUservalue['id'].' and make_primary=1')->row_array();
		echo "<pre>";
		print_r($department);

		foreach ($department as $key => $value) {
			$dept[] = $value['depart'];
		}
		print_r($dept);
		if (count(array_filter($dept)) == count($dept)) {
			echo implode(',', $dept);
		}
	}


	public function update_assignees()
	{

		if (isset($_POST['assign_role'])) {
			// print_r($_POST);
			// exit;
			$reason = array();

			//$handle_user_name = $this->Common_mdl->select_record('user','id',$_SESSION['id']);

			$task_datas1 = $this->Common_mdl->select_specific_field_record('add_new_task', 'id', $_POST['task_id'], 'sub_task_id');
			$message = 0;

			if (isset($_POST['assign_task_status']) && $_POST['description'] != '') {

				$reason = $this->Common_mdl->select_specific_field_record('assign_task_status', 'id', $_POST['assign_task_status'], 'status_name');
				$message = $_POST['description'] . ' for ' . $reason['status_name'];
			}

			$this->Common_mdl->add_assignto($_POST['assign_role'], $_POST['task_id'], 'TASK', 1, $message);
			if ($task_datas1['sub_task_id'] != '') {

				$sub_task = explode(',', $task_datas1['sub_task_id']);
				for ($i = 0; $i < count($sub_task); $i++) {

					$this->Common_mdl->add_assignto($_POST['assign_role'], $sub_task[$i], 'TASK', 1, $message);
				}
			}

			$ex_val = $this->Common_mdl->get_module_user_data('TASK', $_POST['task_id']);

			//   print_r($ex_val);exit;

			$i = 1;
			$var_array = array();
			//   print_r($ex_val);
			if (count($ex_val) > 0) {
				foreach ($ex_val as $key => $value) {

					$user_name = $this->Common_mdl->select_specific_field_record('user', 'id', $value, 'crm_name');

					//$reason = $this->Common_mdl->select_record('assign_task_status','id',$_POST['assign_task_status']);
					// $type='Task Assign';
					// $messages=($message!=0)?$message:'';

					array_push($var_array, $user_name['crm_name']);
				}
			}


			//$username=$this->Common_mdl->getUserProfileName($_SESSION['id']); 

			$activity_datas['log'] = "Task  Assignee Was Changed  by ";
			$activity_datas['createdTime'] = time();
			$activity_datas['module'] = 'Task';
			$activity_datas['sub_module'] = '';
			$activity_datas['user_id'] = $_SESSION['id'];
			$activity_datas['module_id'] = $_POST['task_id'];

			$this->Common_mdl->insert('activity_log', $activity_datas);





			$img = '';

			// $img .='<a href="javascript:;" data-toggle="modal" data-target="#adduser_'.$_POST['task_id'].'" class="adduser1"><i class="fa fa-plus"></i></a>';
			// $img .=implode(",",$var_array);
			// echo '<strong>'.$img.'</strong>';

			$img .= ' <a href="javascript:;" data-toggle="modal" data-target="#adduser_' . $_POST['task_id'] . '" class="adduser1 user_change per_assigne_' . $_POST['task_id'] . '"><i class="fa fa-plus"></i></a>';

			$img .= implode(",", $var_array);
			echo '<strong>' . $img . '</strong>';
		}
	}

	public function update_assignees1()
	{
		if ($_POST['task_id'] != '') {
			foreach (explode(",", $_POST['task_id']) as $key => $value) {
				$this->Common_mdl->add_assignto($_POST['assign_role'], $value, 'TASK', 1);
				# code...
			}
		}
		echo 1;
	}

	public function update_assignees2()
	{
		$data['worker'] = (isset($_POST['worker']) && ($_POST['worker'] != '')) ? implode(',', $_POST['worker']) : '0';
		$update_data = $this->Common_mdl->update('add_new_task', $data, 'id', $_POST['task2_id']);
		$img = '';
		if (isset($_POST['worker']) && !empty($_POST['worker'])) {
			foreach ($_POST['worker'] as $key => $value) {
				$getUserProfilepic = $this->Common_mdl->getUserProfilepic($value);
				$img .= '<img src=' . $getUserProfilepic . '>';
			}
		}
		$img .= '<a href="javascript:;" data-toggle="modal" data-target="#adduser2_' . $_POST['task2_id'] . '" class="adduser1"><i class="fa fa-plus"></i></a>';
		echo $img;
	}

	public function update_assignees3()
	{
		$data['worker'] = (isset($_POST['worker']) && ($_POST['worker'] != '')) ? implode(',', $_POST['worker']) : '0';
		$update_data = $this->Common_mdl->update('add_new_task', $data, 'id', $_POST['task3_id']);
		$img = '';
		if (isset($_POST['worker']) && !empty($_POST['worker'])) {
			foreach ($_POST['worker'] as $key => $value) {
				$getUserProfilepic = $this->Common_mdl->getUserProfilepic($value);
				$img .= '<img src=' . $getUserProfilepic . '>';
			}
		}
		$img .= '<a href="javascript:;" data-toggle="modal" data-target="#adduser3_' . $_POST['task3_id'] . '" class="adduser1"><i class="fa fa-plus"></i></a>';
		echo $img;
	}

	public function task_statusChange($task_id = false, $status = false)
	{
		if ($task_id != false) {
			$id = $task_id;
			$status = $status;
		} else {
			$id = $this->input->post('rec_id');
			$status = $this->input->post('status');
		}
		$val = array();
		$check_val = $this->Common_mdl->select_record('add_new_task', 'id', $id);
		$this->Common_mdl->update('add_new_task', array("task_status" => $status), 'id', $id);

		if ($status == '5') {
			if ($check_val['related_to'] != 'leads' && $check_val['related_to'] != 'sub_task' && $check_val['company_name'] != '') {

				//   $query = $this->Common_mdl->update('add_new_task', array("task_status" => $status), 'id', $id);

				$res_table = $this->db->query("select * from reminder_setting where FIND_IN_SET(" . $id . ",custom_task_created)")->result_array();

				if (count($res_table) >= 0) {
					$invoice_id = $this->Task_invoice_model->task_billable_createInvoice($id);
				}

				$username = $this->Common_mdl->getUserProfileName($_SESSION['id']);
				$activity_datas['log'] = "Task Revision send by ";
				$activity_datas['createdTime'] = time();
				$activity_datas['module'] = 'Task';
				$activity_datas['sub_module'] = '';
				$activity_datas['user_id'] = $_SESSION['id'];
				$activity_datas['module_id'] = $id;
				$this->Common_mdl->insert('activity_log', $activity_datas);
			}

			/* Show the timeline with induvidual user timer */
			$final_array = array();
			$new_explode_worker = array();
			$explode_worker = array();

			$all_task_id = array($id);


			/* This explode_worker get currently  assigned user in  this task */
			$explode_worker = Get_Module_Assigees('TASK',  $id);

			$sub_task_id = array_filter(explode(",", $check_val['sub_task_id']));
			$all_task_id = (!empty($sub_task_id)) ? array_merge($all_task_id, $sub_task_id) : $all_task_id;

			if (!empty($all_task_id)) {

				$all_task_id = implode(",", $all_task_id);

				$get_val = $this->Common_mdl->getall_wherein_records('individual_task_timer', 'task_id', $all_task_id);

				$get_val = array_filter(array_column($get_val, 'user_id'));
			}
			$new_explode_worker = $get_val;

			/* This explode_worker who are all assigned user in this task */
			$explode_worker = array_unique(array_merge($explode_worker, $get_val));

			// print_r($explode_worker);
			if (count($explode_worker) > 0) {
				$user_id = implode(",", $explode_worker);
				$get_user = $this->Common_mdl->getall_wherein_records('user', 'id', $user_id);
				foreach ($explode_worker as $s_key => $s_val) {
					$sub_task_id = array_filter(explode(",", $check_val['sub_task_id']));
					if (count($sub_task_id) > 0) {
						$res = $id . ',' . $check_val['sub_task_id'];
						$data['task_list'] = $this->Common_mdl->getall_wherein_records('add_new_task', 'id', $res);
						$dat_val = $this->Task_creating_model->maintask_timer($data['task_list'], $s_val);
					} else {
						$dat_val = $this->Task_creating_model->maintask_timer(array($check_val), $s_val, 0, 1);
					}
					$dat_val = explode(",", $dat_val[0]);
					unset($dat_val[4]);
					$sub_time = implode(":", $dat_val);
					$hr_explode = explode(':', $sub_time);
					$hours = (int)$hr_explode[0];
					$min = (int)$hr_explode[1];
					$sec = (int)$hr_explode[2];
					if ($hours + $min + $sec > 0) {
						$assign_user_key = array_search($s_val, array_column($get_user, 'id'));
						$user_name = $get_user[$assign_user_key]["crm_name"];
						$final_array[] = $user_name . "-" . sprintf("%02d", $hours)  . ": " . sprintf("%02d", $min)  . ": " . sprintf("%02d", $sec);
					}
				}
				if (count($final_array) > 0) {
					$final_array = implode(",", $final_array);
					$log = 'Task Performance <br>' . $final_array . ' generated by ';
					$activity_datas['log'] = $log;
					$activity_datas['createdTime'] = time();
					$activity_datas['module'] = 'Task';
					$activity_datas['sub_module'] = '';
					$activity_datas['user_id'] = $_SESSION['id'];
					$activity_datas['module_id'] = $id;
					// echo '<pre>'; print_r($activity_datas); echo '</pre>';
					$this->Common_mdl->insert('activity_log', $activity_datas);
				}
			}
		}
		$new_status_val = $this->Common_mdl->select_record('task_status', 'id', $status);
		$old_status_val = $this->Common_mdl->select_record('task_status', 'id', $check_val['task_status']);
		$username = $this->Common_mdl->getUserProfileName($_SESSION['id']);
		if (isset($old_status_val['status_name'])) {
			$activity_datas['log'] = "Task Status change from " . $old_status_val['status_name'] . " to " . $new_status_val['status_name'] . " Was updated  by ";
		} else {
			$activity_datas['log'] = "Task Status " . $new_status_val['status_name'] . " Was updated  by ";
		}
		$activity_datas['createdTime'] = time();
		$activity_datas['module'] = 'Task';
		$activity_datas['sub_module'] = '';
		$activity_datas['user_id'] = $_SESSION['id'];
		$activity_datas['module_id'] = $id;
		// echo '<pre>'; print_r($activity_datas); echo '</pre>';
		$this->Common_mdl->insert('activity_log', $activity_datas);
		$this->db->update("add_new_task", ["task_status" => $status], "id=$id");


		// echo $this->db->affected_rows();
		$Message = '{TASK_SUBJECT} Task Status Has Been Changed';
		//  $this->Common_mdl->Task_Members_Notify($id, 'Task_Status_Change', $Message);
		echo $this->db->affected_rows();
	}
	public function manager_review_response()
	{
		if (isset($_POST["mn_id"]) && isset($_POST["res"])) {
			$da = $this->db->query("select task_id from Manager_notification where id=$_POST[mn_id]")->row_array();
			$tid = $da['task_id'];

			if ($_POST["res"] == 1) {
				$this->db->update("Manager_notification", ['review_status' => 1, "message" => $_POST['notes']], "id=$_POST[mn_id]");

				/*$da1=$this->db->query("select worker from add_new_task where id=$tid")->result_array();
        $da2=$this->db->query("select count(task_id) as tot from Manager_notification where status=0 and task_id=$tid and  review_status=1")->result_array();
          if(count(explode(",",$da1[0]['worker']))==$da2[0]['tot'])
          {*/
				$this->db->update("add_new_task", ["task_status" => '5'], "id=$tid");

				$username = $this->Common_mdl->getUserProfileName($_SESSION['id']);
				$activity_datas['log'] = "Task  Was Accepted  by ";
				$activity_datas['createdTime'] = time();
				$activity_datas['module'] = 'Task';
				$activity_datas['sub_module'] = '';
				$activity_datas['user_id'] = $_SESSION['id'];
				$activity_datas['module_id'] = $tid;
				$this->Common_mdl->insert('activity_log', $activity_datas);


				$res_table = $this->db->query("select * from reminder_setting where FIND_IN_SET(" . $tid . ",custom_task_created) ")->result_array();
				if (count($res_table) > 0) {
					$invoice_id = $this->Task_invoice_model->task_billable_createInvoice($task_id);
				}
				return true;
			} else if ($_POST['res'] == 0) {


				$username = $this->Common_mdl->getUserProfileName($_SESSION['id']);
				$activity_datas['log'] = "Task  Was Declined  by ";
				$activity_datas['createdTime'] = time();
				$activity_datas['module'] = 'Task';
				$activity_datas['sub_module'] = '';
				$activity_datas['user_id'] = $_SESSION['id'];
				$activity_datas['module_id'] = $tid;
				$this->Common_mdl->insert('activity_log', $activity_datas);


				$this->db->update("Manager_notification", ['review_status' => 2, "message" => $_POST['notes']], "id=$_POST[mn_id]");
				/*$data=$this->db->query("select * from Manager_notification where id=$_POST[mn_id]")->result_array();
        $data=current($data);
        $this->db->insert(["task_id"=>$data["task_id"],"manager_id"=>$data["manager_id"],"review_status"=>2],"message"=>$_POST['notes'],"status"]);*/
				echo $this->db->affected_rows();
			}
		}
	}
	public function rejected_tsk_seen_status($id)
	{
		$this->db->update("Manager_notification", ["overall_status" => 1], "id=$id");
		echo $this->db->affected_rows();
	}
	/** task priority change **/
	public function task_priorityChange()
	{
		$id = $this->input->post('rec_id');
		$priority = $this->input->post('priority');
		$check_val = $this->Common_mdl->select_record('add_new_task', 'id', $id);
		$data = array("priority" => $priority);
		$query = $this->Common_mdl->update('add_new_task', $data, 'id', $id);
		$username = $this->Common_mdl->getUserProfileName($_SESSION['id']);




		//$activity_datas['log'] = "Task Priority Was updated  by ";
		$activity_datas['log'] = "Task Priority change from " . $check_val['priority'] . " to " . $priority . " Was updated  by ";
		$activity_datas['createdTime'] = time();
		$activity_datas['module'] = 'Task';
		$activity_datas['sub_module'] = '';
		$activity_datas['user_id'] = $_SESSION['id'];
		$activity_datas['module_id'] = $id;

		$this->Common_mdl->insert('activity_log', $activity_datas);
		return true;
	}
	/** end of priority **/

	public function check_username()
	{
		$username = $this->input->post('val');
		if (isset($_POST['id'])) {
			//$sql = $this->Common_mdl->GetAllWithWhere('user','userName',$username);
			$sql = $this->db->query("select * from user where userName='" . $username . "' and id!=" . $_POST['id'])->result_array();
		} else {
			$sql = $this->Common_mdl->GetAllWithWhere('user', 'userName', $username);
		}
		//$sql = $this->Common_mdl->GetAllWithWhere('user','userName',$username);
		if (!empty($sql)) {
			echo '0';
		} else {
			echo '1';
		}
	}

	public function filter_task()
	{
		/** for 28-06-2018  customize permission**/
		$team_num = array();
		$for_cus_team = $this->db->query("select * from team_assign_staff where FIND_IN_SET('" . $_SESSION['id'] . "',staff_id)")->result_array();
		foreach ($for_cus_team as $cu_team_key => $cu_team_value) {
			array_push($team_num, $cu_team_value['team_id']);
		}
		if (!empty($team_num) && count($team_num) > 0) {
			$res_team_num = implode('|', $team_num);
		} else {
			$res_team_num = 0;
		}





		$department_num = array();
		$for_cus_department = $this->db->query("SELECT * FROM `department_assign_team` where CONCAT(',', `team_id`, ',') REGEXP ',($res_team_num),'")->result_array();
		foreach ($for_cus_department as $cu_dept_key => $cu_dept_value) {
			array_push($department_num, $cu_dept_value['depart_id']);
		}
		if (!empty($department_num) && count($department_num) > 0) {
			$res_dept_num = implode('|', $department_num);
		} else {
			$res_dept_num = 0;
		}

		$status = $this->input->post('status');
		$priority = $this->input->post('priority');
		$worker = $this->input->post('worker');
		$_SESSION['assignee'] = $worker;

		$assign = '';
		$team = '';
		$depart = '';
		if (isset($_POST['worker'])) {

			$assignee = $_POST['worker'];
			$_SESSION['assignee'] = $assignee;
			$value = $_POST['worker'];
			$ex_val = explode('_', $value);
			if (count($ex_val) > 1) {
				if ($ex_val[0] == 'tm') {
					$team = $ex_val[1];
				}
				if ($ex_val[0] == 'de') {
					$depart = $ex_val[1];
				}
			} else {
				$assign = $ex_val[0];
			}
		}

		//  $sql = 'select * from add_new_task where create_by='.$_SESSION['id'].' ';
		$qry_sql = '';
		$qry_sql2 = '';
		if ($status != '' && $priority == '') {
			$qry_sql = " and tl.task_status='" . $status . "'";
			$qry_sql2 = " and tls.task_status='" . $status . "'";


			if ($assign != '') {

				$qry_sql .= " and tl.worker in('" . $assign . "')";
				$qry_sql2 .= " and tls.worker in('" . $assign . "')";
			}


			if ($team != '') {

				$qry_sql .= " and tl.team in('" . $team . "')";
				$qry_sql2 .= " and tls.team in('" . $team . "')";
			}


			if ($depart != '') {

				$qry_sql .= " and tl.department in('" . $depart . "')";
				$qry_sql2 .= " and tls.department in('" . $depart . "')";
			}



			//              $data['task_list']=$this->db->query("SELECT * FROM add_new_task tl where tl.create_by=".$_SESSION['id']." and tl.task_status='".$status."'
			// UNION
			// SELECT * FROM add_new_task tls where tls.create_by!=".$_SESSION['id']." and (FIND_IN_SET('".$_SESSION['id']."',worker) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `department`, ',') REGEXP ',($res_team_num),') or FIND_IN_SET('".$_SESSION['id']."',manager) )  and tls.task_status='".$status."' ")->result_array();
			//   $sql .= ' and task_status="'.$status.'"';

		} else if ($status == '' && $priority != '') {
			$qry_sql = " and tl.priority='" . $priority . "'";
			$qry_sql2 = " and tls.priority='" . $priority . "'";


			if ($assign != '') {

				$qry_sql .= " and tl.worker in(" . $assign . ")";
				$qry_sql2 .= " and tls.worker in(" . $assign . ")";
			}


			if ($team != '') {

				$qry_sql .= " and tl.team in(" . $team . ")";
				$qry_sql2 .= " and tls.team in(" . $team . ")";
			}


			if ($depart != '') {

				$qry_sql .= " and tl.department in(" . $depart . ")";
				$qry_sql2 .= " and tls.department in(" . $depart . ")";
			}


			//                $data['task_list']=$this->db->query("SELECT * FROM add_new_task tl where tl.create_by=".$_SESSION['id']." and tl.priority='".$priority."'
			// UNION
			// SELECT * FROM add_new_task tls where tls.create_by!=".$_SESSION['id']." and (FIND_IN_SET('".$_SESSION['id']."',worker) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `department`, ',') REGEXP ',($res_team_num),') or FIND_IN_SET('".$_SESSION['id']."',manager) )  and tls.priority='".$priority."' ")->result_array();
			//  $sql .= ' and priority="'.$priority.'"';


		} else if ($status != '' && $priority != '') {
			$qry_sql = " and tl.task_status='" . $status . "' and tl.priority='" . $priority . "'";
			$qry_sql2 = " and tls.task_status='" . $status . "' and tls.priority='" . $priority . "'";



			if ($assign != '') {

				$qry_sql .= " and tl.worker in(" . $assign . ")";
				$qry_sql2 .= " and tls.worker in(" . $assign . ")";
			}


			if ($team != '') {

				$qry_sql .= " and tl.team in(" . $team . ")";
				$qry_sql2 .= " and tls.team in(" . $team . ")";
			}


			if ($depart != '') {

				$qry_sql .= " and tl.department in(" . $depart . ")";
				$qry_sql2 .= " and tls.department in(" . $depart . ")";
			}



			//              $data['task_list']=$this->db->query("SELECT * FROM add_new_task tl where tl.create_by=".$_SESSION['id']." and tl.task_status='".$status."' and tl.priority='".$priority."'
			// UNION
			// SELECT * FROM add_new_task tls where tls.create_by!=".$_SESSION['id']." and (FIND_IN_SET('".$_SESSION['id']."',worker) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `department`, ',') REGEXP ',($res_team_num),') or FIND_IN_SET('".$_SESSION['id']."',manager) )  and tls.task_status='".$status."' and tls.priority='".$priority."' ")->result_array();

		} else {

			if ($assign != '') {

				$qry_sql = " and tl.worker in(" . $assign . ")";
				$qry_sql2 = " and tls.worker in(" . $assign . ")";
			}


			if ($team != '') {

				$qry_sql = " and tl.team in(" . $team . ")";
				$qry_sql2 = " and tls.team in(" . $team . ")";
			}


			if ($depart != '') {

				$qry_sql = " and tl.department in(" . $depart . ")";
				$qry_sql2 = " and tls.department in(" . $depart . ")";
			}

			//           $data['task_list']=$this->db->query("SELECT * FROM add_new_task tl where tl.create_by=".$_SESSION['id']." 
			// UNION
			// SELECT * FROM add_new_task tls where tls.create_by!=".$_SESSION['id']." and (FIND_IN_SET('".$_SESSION['id']."',worker) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `department`, ',') REGEXP ',($res_team_num),') or FIND_IN_SET('".$_SESSION['id']."',manager) )   ")->result_array();   

		}
		/** 17-07-2018 for permission **/
		$it_session_id = '';
		$get_this_qry = $this->db->query("select * from user where id=" . $_SESSION['id'])->row_array();
		$created_id = $get_this_qry['firm_admin_id'];
		$its_firm_admin_id = $get_this_qry['firm_admin_id'];
		if ($_SESSION['role'] == 6) // staff
		{
			$it_session_id = $_SESSION['id'];
		} else if ($_SESSION['role'] == 5) //manager
		{
			$it_session_id = $created_id;
		} else if ($_SESSION['role'] == 4) //client
		{
			$it_session_id = $created_id;
		} else if ($_SESSION['role'] == 3) //director
		{
			$it_session_id = $created_id;
		} else if ($_SESSION['role'] == 2) // sub admin
		{
			$it_session_id = $created_id;
		} else {
			$it_session_id = $_SESSION['id'];
		}
		if ($_SESSION['role'] == 6) {
			$data['task_list'] = $this->db->query("SELECT * FROM add_new_task tl where tl.create_by=" . $it_session_id . " " . $qry_sql . "
UNION 
SELECT * FROM add_new_task tls where tls.create_by!=" . $it_session_id . " and (FIND_IN_SET('" . $it_session_id . "',worker) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `department`, ',') REGEXP ',($res_team_num),') or FIND_IN_SET('" . $it_session_id . "',manager) ) " . $qry_sql2 . " order by id desc ")->result_array();

			$data['custom_permission'] = $this->db->query("SELECT * FROM add_new_task tls where tls.create_by!=" . $_SESSION['id'] . " and (FIND_IN_SET('" . $_SESSION['id'] . "',worker) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `department`, ',') REGEXP ',($res_team_num),') or FIND_IN_SET('" . $_SESSION['id'] . "',manager) ) order by id desc  ")->result_array();

			/** for customize **/
			$data['for_user_edit_permission'] = $this->db->query("SELECT * FROM add_new_task tls where tls.create_by!=" . $_SESSION['id'] . " and (FIND_IN_SET('" . $_SESSION['id'] . "',worker) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `department`, ',') REGEXP ',($res_team_num),') or FIND_IN_SET('" . $_SESSION['id'] . "',manager) ) and (FIND_IN_SET('" . $_SESSION['role'] . "',selectuser) ) and tls.customize=1 order by id desc ")->result_array();
		} else if ($_SESSION['role'] == 4) {
			$result = array();
			array_push($result, $it_session_id);
			$reassign_firm_id = $its_firm_admin_id;
			$tot_val = 1;
			for ($z = 0; $z < $tot_val; $z++) {
				if ($reassign_firm_id == 0) {
					$tot_val = 0;
				} else {
					$get_this_qry = $this->db->query("select * from user where id=" . $reassign_firm_id)->row_array();
					if (!empty($get_this_qry)) {
						array_push($result, $get_this_qry['id']);
						$new_created_id = $get_this_qry['firm_admin_id'];
						$reassign_firm_id = $get_this_qry['firm_admin_id'];
					} else {
						$tot_val = 0;
					}
				}
			}
			if (!empty($result) && (($key = array_search($_SESSION['id'], $result)) !== false)) {
				unset($result[$key]);
			}
			$res = (!empty($result)) ? implode(',', array_filter(array_unique($result))) : '';
			$sql = $this->db->query("select * from client where user_id=" . $_SESSION['id'] . " ")->row_array();
			$its_id = $sql['id'];
			$data['task_list'] = $this->db->query("SELECT * FROM add_new_task tl where tl.company_name=" . $its_id . " " . $qry_sql . " order by id desc ")->result_array();

			$data['custom_permission'] = $this->db->query("SELECT * FROM add_new_task tls where tls.create_by in ($res) order by id desc ")->result_array();

			/** for customize **/
			$data['for_user_edit_permission'] = $this->db->query("SELECT * FROM add_new_task tls where tls.create_by in ($res) and (FIND_IN_SET('" . $_SESSION['role'] . "',selectuser) ) and tls.customize=1 order by id desc ")->result_array();
		} else if ($_SESSION['role'] == 5) {
			$result = array();
			array_push($result, $it_session_id);
			$reassign_firm_id = $its_firm_admin_id;
			$tot_val = 1;
			for ($z = 0; $z < $tot_val; $z++) {
				if ($reassign_firm_id == 0) {
					$tot_val = 0;
				} else {
					$get_this_qry = $this->db->query("select * from user where id=" . $reassign_firm_id)->row_array();
					if (!empty($get_this_qry)) {
						array_push($result, $get_this_qry['id']);
						$new_created_id = $get_this_qry['firm_admin_id'];
						$reassign_firm_id = $get_this_qry['firm_admin_id'];
					} else {
						$tot_val = 0;
					}
				}
			}
			if (!empty($result) && (($key = array_search($_SESSION['id'], $result)) !== false)) {
				unset($result[$key]);
			}
			$res = (!empty($result)) ? implode(',', array_filter(array_unique($result))) : '';

			$data['task_list'] = $this->db->query("SELECT * FROM add_new_task tl where tl.create_by=" . $it_session_id . " " . $qry_sql . "
UNION
SELECT * FROM add_new_task tls where tls.create_by!=" . $it_session_id . " and (FIND_IN_SET('" . $it_session_id . "',worker) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `department`, ',') REGEXP ',($res_team_num),') or FIND_IN_SET('" . $it_session_id . "',manager) ) " . $qry_sql2 . " order by id desc ")->result_array();

			$data['custom_permission'] = $this->db->query("SELECT * FROM add_new_task tls where tls.create_by!=" . $_SESSION['id'] . " and (FIND_IN_SET('" . $_SESSION['id'] . "',worker) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `department`, ',') REGEXP ',($res_team_num),') or FIND_IN_SET('" . $_SESSION['id'] . "',manager) ) order by id desc ")->result_array();

			//  /** for customize **/
			$data['for_user_edit_permission'] = $this->db->query("SELECT * FROM add_new_task tls where tls.create_by!=" . $_SESSION['id'] . " and (FIND_IN_SET('" . $_SESSION['id'] . "',worker) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `department`, ',') REGEXP ',($res_team_num),') or FIND_IN_SET('" . $_SESSION['id'] . "',manager) ) and (FIND_IN_SET('" . $_SESSION['role'] . "',selectuser) ) and tls.customize=1 order by id desc ")->result_array();
		} else if ($_SESSION['role'] == 3 || $_SESSION['role'] == 2) {
			$result = array();
			$reassign_firm_id = $its_firm_admin_id;
			// if($its_firm_admin_id==0)
			// {
			array_push($result, $it_session_id);
			// }
			// else
			// {
			$tot_val = 1;
			for ($z = 0; $z < $tot_val; $z++) {
				if ($reassign_firm_id == 0) {
					$tot_val = 0;
				} else {
					$get_this_qry = $this->db->query("select * from user where id=" . $reassign_firm_id)->row_array();
					if (!empty($get_this_qry)) {
						array_push($result, $get_this_qry['id']);
						$new_created_id = $get_this_qry['firm_admin_id'];
						$reassign_firm_id = $get_this_qry['firm_admin_id'];
					} else {
						$tot_val = 0;
					}
				}
			}
			// }
			$res = (!empty($result)) ? implode(',', array_filter(array_unique($result))) : '';
			// $res=implode(',', $over_all_res);
			$data['task_list'] = $this->db->query("SELECT * FROM add_new_task tl where tl.create_by in ($res) " . $qry_sql . " order by id desc ")->result_array();

			if (!empty($result) && (($key = array_search($_SESSION['id'], $result)) !== false)) {
				unset($result[$key]);
			}
			$res = (!empty($result)) ? implode(',', array_filter(array_unique($result))) : '';

			$data['custom_permission'] = $this->db->query("SELECT * FROM add_new_task tls where tls.create_by in ($res) order by id desc ")->result_array();

			/** for customize **/
			$data['for_user_edit_permission'] = $this->db->query("SELECT * FROM add_new_task tls where tls.create_by in ($res) and (FIND_IN_SET('" . $_SESSION['role'] . "',selectuser) ) and tls.customize=1 order by id desc ")->result_array();
		} else {
			$result = array();
			array_push($result, $it_session_id);
			$reassign_firm_id = $its_firm_admin_id;
			$tot_val = 1;
			for ($z = 0; $z < $tot_val; $z++) {
				if ($reassign_firm_id == 0) {
					$tot_val = 0;
				} else {
					$get_this_qry = $this->db->query("select * from user where id=" . $reassign_firm_id)->row_array();
					if (!empty($get_this_qry)) {
						array_push($result, $get_this_qry['id']);
						$new_created_id = $get_this_qry['firm_admin_id'];
						$reassign_firm_id = $get_this_qry['firm_admin_id'];
					} else {
						$tot_val = 0;
					}
				}
			}
			$res = (!empty($result)) ? implode(',', array_filter(array_unique($result))) : '';
			// $res=implode(',', $over_all_res);
			$data['task_list'] = $this->db->query("SELECT * FROM add_new_task tl where tl.create_by in ($res) " . $qry_sql . " order by id desc ")->result_array();

			if (!empty($result) && (($key = array_search($_SESSION['id'], $result)) !== false)) {
				unset($result[$key]);
			}
			$res = (!empty($result)) ? implode(',', array_filter(array_unique($result))) : '';

			$data['custom_permission'] = array();

			$data['for_user_edit_permission'] = array();
			// $data['leads']=$this->db->query("SELECT * FROM leads li where li.user_id=".$it_session_id." ")->result_array();
		}
		/** end of permission **/

		// $data['custom_permission']=$this->db->query("SELECT * FROM add_new_task tls where tls.create_by!=".$_SESSION['id']." and (FIND_IN_SET('".$_SESSION['id']."',worker) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `department`, ',') REGEXP ',($res_team_num),') or FIND_IN_SET('".$_SESSION['id']."',manager) )   ")->result_array();

		// $data['for_user_edit_permission']=$this->db->query("SELECT * FROM add_new_task tls where tls.create_by!=".$_SESSION['id']." and (FIND_IN_SET('".$_SESSION['id']."',worker) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `department`, ',') REGEXP ',($res_team_num),') or FIND_IN_SET('".$_SESSION['id']."',manager) ) and (FIND_IN_SET('".$_SESSION['role']."',selectuser) ) and tls.customize=1  ")->result_array();

		//  $data['task_list'] = $this->db->query($sql)->result_array();
		/*echo '<pre>';
        print_r($data['task_list']);die;*/
		$data['staffs'] = $this->Common_mdl->GetAllWithWheretwo('user', 'role', '6', 'firm_admin_id', $_SESSION['id']);

		$this->load->view('users/task_result', $data);
	}

	public function task_excel()
	{
		$ques = Get_Assigned_Datas('TASK');

		$res = (!empty($ques)) ? implode(',', array_filter(array_unique($ques))) : '-1';



		$filename = 'tasks_' . date('Ymd') . '.csv';

		$status = $_GET['status'];
		$priority = $_GET['priority'];
		$sql = 'select id,subject,start_date,end_date,task_status,priority,related_to from add_new_task';

		// if($status!='' && $priority=='')
		// {
		//     $sql .= ' where task_status="'.$status.'"';
		// }else if($status=='' && $priority!='')
		// {
		//     $sql .= ' where priority="'.$priority.'"';
		// }else if($status!='' && $priority!='')
		// {
		//     $sql .= ' where task_status="'.$status.'" and priority="'.$priority.'"';
		// }

		//  $sql = 'select * from add_new_task';
		if ($status != '' && $priority == '') {
			$sql .= ' where task_status="' . $status . '" and firm_id="' . $_SESSION['firm_id'] . '" and id in (' . $res . ') ';
		} else if ($status == '' && $priority != '') {
			$sql .= ' where priority="' . $priority . '" and firm_id="' . $_SESSION['firm_id'] . '" and id in (' . $res . ') ';
		} else if ($status != '' && $priority != '') {
			$sql .= ' where task_status="' . $status . '" and priority="' . $priority . '" and firm_id="' . $_SESSION['firm_id'] . '" and id in (' . $res . ') ';
		} else {
			$sql .= ' where firm_id="' . $_SESSION['firm_id'] . '" and id in (' . $res . ') ';
		}

		$data['task_list'] = $this->db->query($sql)->result_array();

		header("Content-Description: File Transfer");
		header("Content-Disposition: attachment; filename=$filename");
		header("Content-Type: application/csv; ");


		$file = fopen('php://output', 'w');
		$header = array("S.NO", "Task Name", "Start Date", "Due Date", "Status", "Priority", "Task Type");
		fputcsv($file, $header);
		$i = 0;

		foreach ($data['task_list'] as $key => $line) {

			if ($line['id']) {
				$i++;
				$line['id'] = $i;
			}



			if ($line['task_status'] == '1') {
				$line['task_status'] = 'Not Started';
			} else if ($line['task_status'] == '2') {
				$line['task_status'] = 'In Progress';
			} else if ($line['task_status'] == '3') {
				$line['task_status'] = 'Awaiting for a feedback';
			} else if ($line['task_status'] == '4') {
				$line['task_status'] = 'Archive';
			} else if ($line['task_status'] == '5') {
				$line['task_status'] = 'Completed';
			}

			fputcsv($file, $line);
		}


		fclose($file);
		exit;
	}
	public function task_pdf()
	{

		$ques = Get_Assigned_Datas('TASK');

		$res = (!empty($ques)) ? implode(',', array_filter(array_unique($ques))) : '-1';

		// $data['ass_res']=$res;
		$status = $_GET['status'];
		$priority = $_GET['priority'];
		// print_r($_GET);
		$sql = 'select * from add_new_task';
		if ($status != '' && $priority == '') {
			$sql .= ' where task_status="' . $status . '" and firm_id="' . $_SESSION['firm_id'] . '" and id in (' . $res . ') ';
		} else if ($status == '' && $priority != '') {
			$sql .= ' where priority="' . $priority . '" and firm_id="' . $_SESSION['firm_id'] . '" and id in (' . $res . ') ';
		} else if ($status != '' && $priority != '') {
			$sql .= ' where task_status="' . $status . '" and priority="' . $priority . '" and firm_id="' . $_SESSION['firm_id'] . '" and id in (' . $res . ') ';
		} else {
			$sql .= ' where firm_id="' . $_SESSION['firm_id'] . '" and id in (' . $res . ') ';
		}

		$data['task_list'] = $this->db->query($sql)->result_array();

		//  $html= $this->load->view('users/task_pdf', $data);

		//   $this->load->library('Pdf');
		//   $dompdf = new Pdf();
		//   $dompdf->set_paper("A4");
		//   $dompdf->load_html($html);
		//   $dompdf->render();
		//   $dompdf->stream("task.pdf"); 


		$this->load->library('Pdf');
		$dompdf = new Pdf();

		//general options for pdf
		$dompdf->set_option('isHtml5ParserEnabled', true);
		$dompdf->set_option('defaultMediaType', 'all');
		$dompdf->set_option('isFontSubsettingEnabled', true);
		$dompdf->set_option('isRemoteEnabled', true);


		$dompdf->set_paper("A4");
		// load the html content

		$html = $this->load->view('users/task_pdf', $data, true);
		// echo $html;
		// exit;
		// $html .= $this->load->view('proposal/new_test_pdf',$data,true);    
		$dompdf->load_html($html);
		$dompdf->render();
		$dompdf->stream("task.pdf");
	}

	public function task_html()
	{

		$ques = Get_Assigned_Datas('TASK');

		$res = (!empty($ques)) ? implode(',', array_filter(array_unique($ques))) : '-1';

		$status = $_GET['status'];
		$priority = $_GET['priority'];
		$sql = 'select * from add_new_task';

		if ($status != '' && $priority == '') {
			$sql .= ' where task_status="' . $status . '" and firm_id="' . $_SESSION['firm_id'] . '" and id in (' . $res . ') ';
		} else if ($status == '' && $priority != '') {
			$sql .= ' where priority="' . $priority . '" and firm_id="' . $_SESSION['firm_id'] . '" and id in (' . $res . ') ';
		} else if ($status != '' && $priority != '') {
			$sql .= ' where task_status="' . $status . '" and priority="' . $priority . '" and firm_id="' . $_SESSION['firm_id'] . '" and id in (' . $res . ') ';
		} else {
			$sql .= ' where firm_id="' . $_SESSION['firm_id'] . '" and id in (' . $res . ') ';
		}

		$data['task_list'] = $this->db->query($sql)->result_array();
		$this->load->view("users/task_print", $data);
	}

	public function test_statusChange()
	{
		$id = $this->input->post('rec_id');
		$status = $this->input->post('status');
		$data = array("test_status" => $status);
		$query = $this->Common_mdl->update('add_new_task', $data, 'id', $id);
		return true;
	}

	public function update_timer()
	{
		$id = $this->input->post('id');
		$timer = $this->input->post('time');
		$ex_t = explode('.', $timer);
		$ts_ex = explode(':', $ex_t[0]);
		$ar_cnt = count($ts_ex);
		if ($ar_cnt == 1) {
			$sec = $ts_ex[0];
			$min = 00;
			$hr = 00;
		} else if ($ar_cnt == 2) {
			$sec = $ts_ex[1];
			$min = $ts_ex[0];
			$hr = 00;
		} else if ($ar_cnt == 3) {
			$sec = $ts_ex[2];
			$min = $ts_ex[1];
			$hr = $ts_ex[0];
		}
		$tot_min = ($hr * 60) + ($min) + ($sec / 60);
		$time_details = $this->Common_mdl->select_record('add_new_task', 'id', $id);
		if ($time_details['timer_status'] != '') {
			$tot_min = $time_details['timer_status'] + $tot_min;
		}
		$data = array("timer_status" => $tot_min);
		$query = $this->Common_mdl->update('add_new_task', $data, 'id', $id);
		echo $tot_min;
		//return true; 
	}

	/*function minutes($time){
$time = explode(':', $time);
return ($time[0]*60) + ($time[1]) + ($time[2]/60);
}*/


	function convertToHoursMins($time, $format = '%02d:%02d')
	{
		if ($time < 1) {
			return;
		}
		$hours = floor($time / 60);
		$minutes = ($time % 60);
		return sprintf($format, $hours, $minutes);
	}

	public function get_task_status_data()
	{
		$this->Security_model->chk_login();
		$task_status = $_POST['task_status'];
		/** for 28-06-2018  customize permission**/
		$team_num = array();
		$for_cus_team = $this->db->query("select * from team_assign_staff where FIND_IN_SET('" . $_SESSION['id'] . "',staff_id)")->result_array();
		foreach ($for_cus_team as $cu_team_key => $cu_team_value) {
			array_push($team_num, $cu_team_value['team_id']);
		}
		if (!empty($team_num) && count($team_num) > 0) {
			$res_team_num = implode('|', $team_num);
		} else {
			$res_team_num = 0;
		}

		$department_num = array();
		$for_cus_department = $this->db->query("SELECT * FROM `department_assign_team` where CONCAT(',', `team_id`, ',') REGEXP ',($res_team_num),'")->result_array();
		foreach ($for_cus_department as $cu_dept_key => $cu_dept_value) {
			array_push($department_num, $cu_dept_value['depart_id']);
		}
		if (!empty($department_num) && count($department_num) > 0) {
			$res_dept_num = implode('|', $department_num);
		} else {
			$res_dept_num = 0;
		}



		$data['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user', 'role', '1');
		/** 17-07-2018 rspt **/
		$it_session_id = '';
		$get_this_qry = $this->db->query("select * from user where id=" . $_SESSION['id'])->row_array();
		$created_id = $get_this_qry['firm_admin_id'];
		$its_firm_admin_id = $get_this_qry['firm_admin_id'];
		if ($_SESSION['role'] == 6) // staff
		{
			$it_session_id = $_SESSION['id'];
		} else if ($_SESSION['role'] == 5) //manager
		{
			$it_session_id = $created_id;
		} else if ($_SESSION['role'] == 4) //client
		{
			$it_session_id = $created_id;
		} else if ($_SESSION['role'] == 3) //director
		{
			$it_session_id = $created_id;
		} else if ($_SESSION['role'] == 2) // sub admin
		{
			$it_session_id = $created_id;
		} else {
			$it_session_id = $_SESSION['id'];
		}
		/** end of 17-07-2018 **/
		$qry_sql = '';
		$qry_sql2 = '';
		if ($task_status != '') {
			$qry_sql = " and tl.task_status='" . $task_status . "' ";
			$qry_sql2 = " and tls.task_status='" . $task_status . "'";
		}


		if ($_SESSION['role'] == 6) {
			$data['review_select'] = $this->db->query("select a.id,a.highest_role from roles_section as a left join roles_section b on b.id >a.id and b.id <= a.highest_role where a.id=$_SESSION[role]")->result_array();

			if (isset($_POST['sub_status'])) {
				$qry_sql = " and tl.id IN (" . $_POST['task_ids'] . ") ";
				$qry_sql2 = " and tls.id IN (" . $_POST['task_ids'] . ") ";
			}

			$data['task_list'] = $this->db->query("SELECT tl.*,mn.review_status FROM add_new_task tl left join Manager_notification mn on mn.task_id=tl.id and mn.user_id=$_SESSION[id] where tl.create_by=" . $it_session_id . "  " . $qry_sql . "
UNION 
SELECT tls.*,mn.review_status FROM add_new_task tls left join Manager_notification mn on mn.task_id=tls.id and mn.user_id=$_SESSION[id] where tls.create_by!=" . $it_session_id . " and (FIND_IN_SET('" . $it_session_id . "',worker) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `department`, ',') REGEXP ',($res_team_num),') or FIND_IN_SET('" . $it_session_id . "',manager) ) " . $qry_sql2)->result_array();

			$data['custom_permission'] = $this->db->query("SELECT * FROM add_new_task tls where tls.create_by!=" . $_SESSION['id'] . " and (FIND_IN_SET('" . $_SESSION['id'] . "',worker) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `department`, ',') REGEXP ',($res_team_num),') or FIND_IN_SET('" . $_SESSION['id'] . "',manager) ) order by id desc  ")->result_array();

			/** for customize **/
			$data['for_user_edit_permission'] = $this->db->query("SELECT * FROM add_new_task tls where tls.create_by!=" . $_SESSION['id'] . " and (FIND_IN_SET('" . $_SESSION['id'] . "',worker) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `department`, ',') REGEXP ',($res_team_num),') or FIND_IN_SET('" . $_SESSION['id'] . "',manager) ) and (FIND_IN_SET('" . $_SESSION['role'] . "',selectuser) ) and tls.customize=1 order by id desc ")->result_array();
		} else if ($_SESSION['role'] == 4) {
			$result = array();
			array_push($result, $it_session_id);
			$reassign_firm_id = $its_firm_admin_id;
			$tot_val = 1;
			for ($z = 0; $z < $tot_val; $z++) {
				if ($reassign_firm_id == 0) {
					$tot_val = 0;
				} else {
					$get_this_qry = $this->db->query("select * from user where id=" . $reassign_firm_id)->row_array();
					if (!empty($get_this_qry)) {
						array_push($result, $get_this_qry['id']);
						$new_created_id = $get_this_qry['firm_admin_id'];
						$reassign_firm_id = $get_this_qry['firm_admin_id'];
					} else {
						$tot_val = 0;
					}
				}
			}
			if (!empty($result) && (($key = array_search($_SESSION['id'], $result)) !== false)) {
				unset($result[$key]);
			}
			$res = (!empty($result)) ? implode(',', array_filter(array_unique($result))) : '';
			$sql = $this->db->query("select * from client where user_id=" . $_SESSION['id'] . " ")->row_array();
			$its_id = $sql['id'];
			$data['task_list'] = $this->db->query("SELECT * FROM add_new_task tl where tl.company_name=" . $its_id . " " . $qry_sql . " order by id desc ")->result_array();

			$data['custom_permission'] = $this->db->query("SELECT * FROM add_new_task tls where tls.create_by in ($res) order by id desc ")->result_array();

			/** for customize **/
			$data['for_user_edit_permission'] = $this->db->query("SELECT * FROM add_new_task tls where tls.create_by in ($res) and (FIND_IN_SET('" . $_SESSION['role'] . "',selectuser) ) and tls.customize=1 order by id desc ")->result_array();
		} else if ($_SESSION['role'] == 5) {
			$result = array();
			array_push($result, $it_session_id);
			$reassign_firm_id = $its_firm_admin_id;
			$tot_val = 1;
			for ($z = 0; $z < $tot_val; $z++) {
				if ($reassign_firm_id == 0) {
					$tot_val = 0;
				} else {
					$get_this_qry = $this->db->query("select * from user where id=" . $reassign_firm_id)->row_array();
					if (!empty($get_this_qry)) {
						array_push($result, $get_this_qry['id']);
						$new_created_id = $get_this_qry['firm_admin_id'];
						$reassign_firm_id = $get_this_qry['firm_admin_id'];
					} else {
						$tot_val = 0;
					}
				}
			}
			if (!empty($result) && (($key = array_search($_SESSION['id'], $result)) !== false)) {
				unset($result[$key]);
			}
			$res = (!empty($result)) ? implode(',', array_filter(array_unique($result))) : '';

			$data['task_list'] = $this->db->query("SELECT * FROM add_new_task tl where tl.create_by=" . $it_session_id . " " . $qry_sql . "
UNION
SELECT * FROM add_new_task tls where tls.create_by!=" . $it_session_id . " and (FIND_IN_SET('" . $it_session_id . "',worker) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `department`, ',') REGEXP ',($res_team_num),') or FIND_IN_SET('" . $it_session_id . "',manager) ) " . $qry_sql2 . " order by id desc ")->result_array();

			$data['custom_permission'] = $this->db->query("SELECT * FROM add_new_task tls where tls.create_by!=" . $_SESSION['id'] . " and (FIND_IN_SET('" . $_SESSION['id'] . "',worker) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `department`, ',') REGEXP ',($res_team_num),') or FIND_IN_SET('" . $_SESSION['id'] . "',manager) ) order by id desc ")->result_array();

			//  /** for customize **/
			$data['for_user_edit_permission'] = $this->db->query("SELECT * FROM add_new_task tls where tls.create_by!=" . $_SESSION['id'] . " and (FIND_IN_SET('" . $_SESSION['id'] . "',worker) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `department`, ',') REGEXP ',($res_team_num),') or FIND_IN_SET('" . $_SESSION['id'] . "',manager) ) and (FIND_IN_SET('" . $_SESSION['role'] . "',selectuser) ) and tls.customize=1 order by id desc ")->result_array();
		} else if ($_SESSION['role'] == 3 || $_SESSION['role'] == 2) {
			$result = array();
			$reassign_firm_id = $its_firm_admin_id;
			// if($its_firm_admin_id==0)
			// {
			array_push($result, $it_session_id);
			// }
			// else
			// {
			$tot_val = 1;
			for ($z = 0; $z < $tot_val; $z++) {
				if ($reassign_firm_id == 0) {
					$tot_val = 0;
				} else {
					$get_this_qry = $this->db->query("select * from user where id=" . $reassign_firm_id)->row_array();
					if (!empty($get_this_qry)) {
						array_push($result, $get_this_qry['id']);
						$new_created_id = $get_this_qry['firm_admin_id'];
						$reassign_firm_id = $get_this_qry['firm_admin_id'];
					} else {
						$tot_val = 0;
					}
				}
			}
			// }
			$res = (!empty($result)) ? implode(',', array_filter(array_unique($result))) : '-1';
			// $res=implode(',', $over_all_res);
			$data['task_list'] = $this->db->query("SELECT * FROM add_new_task tl where tl.create_by in ($res) " . $qry_sql . " order by id desc ")->result_array();

			if (!empty($result) && (($key = array_search($_SESSION['id'], $result)) !== false)) {
				unset($result[$key]);
			}
			$res = (!empty($result)) ? implode(',', array_filter(array_unique($result))) : '-1';

			$data['custom_permission'] = $this->db->query("SELECT * FROM add_new_task tls where tls.create_by in ($res) order by id desc ")->result_array();

			/** for customize **/
			$data['for_user_edit_permission'] = $this->db->query("SELECT * FROM add_new_task tls where tls.create_by in ($res) and (FIND_IN_SET('" . $_SESSION['role'] . "',selectuser) ) and tls.customize=1 order by id desc ")->result_array();
		} else {
			$result = array();
			array_push($result, $it_session_id);
			$reassign_firm_id = $its_firm_admin_id;
			$tot_val = 1;
			for ($z = 0; $z < $tot_val; $z++) {
				if ($reassign_firm_id == 0) {
					$tot_val = 0;
				} else {
					$get_this_qry = $this->db->query("select * from user where id=" . $reassign_firm_id)->row_array();
					if (!empty($get_this_qry)) {
						array_push($result, $get_this_qry['id']);
						$new_created_id = $get_this_qry['firm_admin_id'];
						$reassign_firm_id = $get_this_qry['firm_admin_id'];
					} else {
						$tot_val = 0;
					}
				}
			}
			$res = (!empty($result)) ? implode(',', array_filter(array_unique($result))) : '';
			// $res=implode(',', $over_all_res);
			$data['task_list'] = $this->db->query("SELECT * FROM add_new_task tl where tl.create_by in ($res) " . $qry_sql . " order by id desc ")->result_array();

			if (!empty($result) && (($key = array_search($_SESSION['id'], $result)) !== false)) {
				unset($result[$key]);
			}
			$res = (!empty($result)) ? implode(',', array_filter(array_unique($result))) : '-1';

			$data['custom_permission'] = array();

			$data['for_user_edit_permission'] = array();
			// $data['leads']=$this->db->query("SELECT * FROM leads li where li.user_id=".$it_session_id." ")->result_array();
		}
		/** end of permission **/


		// $data['custom_permission']=$this->db->query("SELECT * FROM add_new_task tls where tls.create_by!=".$_SESSION['id']." and (FIND_IN_SET('".$_SESSION['id']."',worker) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `department`, ',') REGEXP ',($res_team_num),') or FIND_IN_SET('".$_SESSION['id']."',manager) )  ")->result_array();

		// $data['for_user_edit_permission']=$this->db->query("SELECT * FROM add_new_task tls where tls.create_by!=".$_SESSION['id']." and (FIND_IN_SET('".$_SESSION['id']."',worker) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `department`, ',') REGEXP ',($res_team_num),') or FIND_IN_SET('".$_SESSION['id']."',manager) ) and FIND_IN_SET('".$_SESSION['role']."',selectuser) and tls.customize=1  ")->result_array();

		$data['staffs'] = $this->Common_mdl->GetAllWithWheretwo('user', 'role', '6', 'firm_admin_id', $_SESSION['id']);
		$data['task_status'] = $task_status;



		//print_r($data);die;
		//SELECT * FROM add_new_task tls where tls.create_by!=".$_SESSION['id']." and (FIND_IN_SET('".$_SESSION['id']."',worker) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `department`, ',') REGEXP ',($res_team_num),') or FIND_IN_SET('".$_SESSION['id']."',manager) ) and  tls.task_status='notstarted'
		// echo "<pre>";print_r($data['task_list']);die;
		$this->load->view('users/task_table_status_change', $data);
	}

	public function tasks_delete()
	{
		$a = $_POST['id'];
		$id_values = array_unique($a);
		print_r($id_values);
		for ($i = 0; $i < count($id_values); $i++) {
			$value = $id_values[$i];
			$this->Task_invoice_model->task_new_mail($value, 'for_delete');
			$delete = $this->Common_mdl->delete('add_new_task', 'id', $value);
		}
	}

	public function alltasks_delete()
	{
		// print_r($_POST);
		// exit;
		if (isset($_POST['id'])) {
			$ids = json_decode($_POST['id'], true);
			foreach ($ids as $id) {
				$da1 = $dat1 = array();
				$da = $this->db->query("select * from add_new_task where id=$id")->row_array();
				$da1 = $this->db->query("SELECT * FROM  `add_new_task` WHERE FIND_IN_SET( " . $id . ", sub_task_id )")->row_array();


				if (!empty($da1)) {
					// print_r($da1);
					//   exit;
					$get_val = explode(",", $da1['sub_task_id']);

					if (($key = array_search($id, $get_val)) !== false) {
						unset($get_val[$key]);
					}
					$dat1['sub_task_id'] = implode(",", $get_val);
					$this->Common_mdl->update('add_new_task', $dat1, 'id', $da1['id']);
				}

				if ($da['sub_task_id'] != '') {


					foreach ($da['sub_task'] as $key => $value) {


						$sub_1 = $this->db->query("SELECT * FROM  `add_new_task` WHERE  $id=" . $value . "")->row_array();
						$username = $this->Common_mdl->getUserProfileName($_SESSION['id']);
						$log = $sub_1['subject'] . " task was deleted by " . $username;
						if ($da['company_name'] != '' && is_numeric($da['company_name']) && $sub_1['related_to'] != 'leads') {
							$user_id = $this->Common_mdl->get_field_value('client', 'user_id', 'id', $da['company_name']);
							if ($user_id != '') {
								$this->Common_mdl->Create_Log('client', $user_id, $log);
							}
						}
						$this->Common_mdl->assignees_delete('TASK', $value);
					}
					$this->db->query("DELETE FROM add_new_task WHERE id IN (" . $da['sub_task_id'] . ")");
				}
				$this->Task_invoice_model->task_new_mail($id, 'for_delete');

				// exit;


				$username = $this->Common_mdl->getUserProfileName($_SESSION['id']);
				$log = $da['subject'] . " task was deleted by " . $username;
				if ($da['company_name'] != '' && is_numeric($da['company_name']) && $da['related_to'] != 'leads') {
					$user_id = $this->Common_mdl->get_field_value('client', 'user_id', 'id', $da['company_name']);
					if ($user_id != '') {
						$this->Common_mdl->Create_Log('client', $user_id, $log);
					}
				}


				$this->db->query("DELETE FROM add_new_task WHERE id=$id");
				$this->Common_mdl->assignees_delete('TASK', $id);



				//  $activity_datas['log'] = "Task Delete  by ";
				// $activity_datas['createdTime'] = time();
				// $activity_datas['module'] = 'Client';
				// $activity_datas['sub_module']='';
				// $activity_datas['user_id'] = $_SESSION['id'];
				// $activity_datas['module_id'] = $id;
				// $this->Common_mdl->insert('activity_log',$activity_datas);



			}
		}
		echo $this->db->affected_rows();
	}

	public function alltasks_assign()
	{

		// exit;

		$task_id_sug = explode(',', $_POST['task_id']);
		$staff_id = implode(',', $_POST['staff_id']);
		$id_values = count($task_id_sug);
		$worker = array();
		for ($i = 0; $i < count($task_id_sug); $i++) {

			$query = $this->db->query("select `worker`,`team`,`department` from add_new_task where id='" . $task_id_sug[$i] . "'")->row_array();


			$assign = explode(',', $query['worker']);
			$team = explode(',', $query['team']);
			$department = explode(',', $query['department']);

			if (isset($_POST['staff_id'])) {
				foreach ($_POST['staff_id'] as $key => $value) {
					$ex_val = explode('_', $value);
					if (count($ex_val) > 1) {
						if ($ex_val[0] == 'tm') {
							array_push($team, $ex_val[1]);
						}
						if ($ex_val[0] == 'de') {
							array_push($department, $ex_val[1]);
						}
					} else {
						array_push($assign, $ex_val[0]);
					}
				}

				$data['worker'] = implode(',', array_filter(array_unique($assign)));
				$data['team'] = implode(',', array_filter(array_unique($team)));
				$data['department'] = implode(',', array_filter(array_unique($department)));
			}





			//  echo $task_id_sug[$i];
			//  exit;

			$update_data = $this->Common_mdl->update('add_new_task', $data, 'id', $task_id_sug[$i]);

			$task_datas1 = $this->Common_mdl->select_record('add_new_task', 'id', $task_id_sug[$i]);

			if ($task_datas1['sub_task_id'] != '') {

				$sub_task = explode(',', $task_datas1['sub_task_id']);


				for ($j = 0; $j < count($sub_task); $j++) {
					$update_datas = $this->Common_mdl->update('add_new_task', $data, 'id', $sub_task[$j]);
				}
			}
			/** new invoice **/
			$for_old_values = array();
			$in = $task_id_sug[$i];
			$tsk_rec = $this->Invoice_model->selectRecord('add_new_task', 'id', $task_id_sug[$i]);
			$invoice_id = $tsk_rec['invoice_id'];
			$billable = $tsk_rec['billable'];
			$task_status = $tsk_rec['task_status'];
			if ($invoice_id != '') {

				if ((isset($billable) && ($billable != '')) && $task_status == '5') {
					$invoice_id = $this->Task_invoice_model->task_billable_createInvoice($in);
				}
			} else {
				$invoice_id = '';
				if ((isset($billable) && ($billable != '')) && $task_status == '5') {
					$invoice_id = $this->Task_invoice_model->task_billable_createInvoice($in);
					if ($invoice_id != '') {
						$for_us_datas['invoice_id'] = $invoice_id;
						$result =  $this->Common_mdl->update('add_new_task', $for_us_datas, 'id', $in);
					}
				}
			}
			/** end of invoice **/
			/** for task mail send to assign **/
			$this->Task_invoice_model->task_new_mail($task_id_sug[$i], 'for_edit');

			$for_old_values['assign'] = $assign;
			$for_old_values['team'] = $team;
			$for_old_values['department'] = $department;
			$for_old_values['manager'] = explode(',', $tsk_rec['manager']);


			$task_datas1 = $this->Common_mdl->select_record('add_new_task', 'id', $task_id_sug[$i]);






			if ($task_datas1['for_old_assign'] != '') {
				$old_assign = json_decode($task_datas1['for_old_assign'], TRUE);

				$manager = (is_array($old_assign['manager']) ? $old_assign['manager'] : array());

				foreach ($old_assign as $k =>  $part) {
					if (is_array($part) && count($part) >= 1) {

						$diff = array_diff($$k, array_filter($part));
						$for_old_values[$k] = (count($diff) ? array_merge($part, $diff) : $part);
					} else {
						$for_old_values[$k] = $part;
					}
				}

				// print_r($for_old_values);

			} else {
				$manager = explode(',', $task_datas1['manager']);

				$for_old_values = array();
				$for_old_values['assign'] = $assign;
				$for_old_values['team'] = $team;
				$for_old_values['department'] = $department;
				$for_old_values['manager'] = (is_array($manager)  ? $manager : array());
			}

			$for_us_datas['for_old_assign'] = json_encode($for_old_values);
			$result =  $this->Common_mdl->update('add_new_task', $for_us_datas, 'id', $task_id_sug[$i]);

			$assign_parm = ['assign' => $assign, 'team' => $team, 'department' => $department];
			$this->update_task_assignDetails($assign_parm, $task_id_sug[$i]);

			if ($task_datas1['sub_task_id'] != '') {

				$sub_task = explode(',', $task_datas1['sub_task_id']);
				for ($k = 0; $k < count($sub_task); $k++) {
					$update_datas = $this->Common_mdl->update('add_new_task', $for_us_datas, 'id', $sub_task[$k]);
					$this->update_task_assignDetails($assign_parm, $sub_task[$i]);
				}
				// $sql=$this->db->query("select * from add_new_task where id in (".$task_datas['sub_task_id'].") ")->result_array();

			}




			//echo $task_id_sug[$i];
			/** end of task mail send **/
			// $update=$this->db->query("UPDATE `add_new_task` SET `worker`='".$worker."' where id='".$task_id_sug[$i]."'");
			$worker = array();
		}
		$data['status'] = 1;
		echo json_encode($data);
	}

	/** 19-06-2018 for task list kanban**/
	public function task_list_kanban($userId = false)
	{

		$this->Security_model->chk_login();

		$ques = Get_Assigned_Datas('TASK');


		$res = (!empty($ques)) ? implode(',', array_filter(array_unique($ques))) : '-1';
		$data['ass_res'] = $res;
		$data['task_list'] = $this->db->query("SELECT * FROM add_new_task AS tl where tl.id in ($res) and tl.firm_id = " . $_SESSION['firm_id'] . " and related_to!='service_reminder' and   tl.related_to != 'sub_task'  order by id desc
")->result_array();


		// echo "<pre>";
		// print_r($data['task_list']);
		// exit;

		$data['custom_permission'] = array();

		$data['for_user_edit_permission'] = array();

		$data['tag'] = $this->Common_mdl->getallrecords('Tag');

		$data['user_list'] = $this->db->query("select * from user where firm_id=" . $_SESSION['firm_id'] . " and user_type !='FC' and user_type !='SA'")->result_array();
		$data['task_status'] = $this->db->query('SELECT * from task_status where is_superadmin_owned=1')->result_array();
		if ($_SESSION['permission']['Leads']['edit'] == '1') {
			$this->load->view('users/task_list_view_kanban', $data);
		} else {
			redirect('user/task_list');
		}
	}

	/** for update kanban data **/
	public function update_task_status_kanban()
	{

		$task_id = $_POST['task_id'];
		$task_status = $_POST['status'];
		$this->task_statusChange($task_id, $task_status);
	}
	public function task_countdown_update()
	{
		//'task_id':task_id,'hours':hours,'minutes':minutes,'seconds':seconds,'milliseconds':milliseconds
		$task_id = $_POST['task_id'];
		$hours = $_POST['hours'];
		$minutes = $_POST['minutes'];
		$seconds = $_POST['seconds'];
		$milliseconds = $_POST['milliseconds'];
		$pause = '';
		$val = (isset($_POST['pause'])) ? $_POST['pause'] : '';

		$new_array = array();
		$new_array['hours'] = $hours;
		$new_array['minutes'] = $minutes;
		$new_array['seconds'] = $seconds;
		$new_array['milliseconds'] = $_POST['milliseconds'];
		$data['counttimer'] = json_encode($new_array);
		$data['pause'] = $val;

		$this->Common_mdl->update('add_new_task', $data, 'id', $task_id);
	}

	public function task_countdown_update_start()
	{
		$task_id = $_POST['task_id'];
		$start = time();
		$res = $this->db->query('select * from add_new_task where id=' . $task_id)->result_array();
		if ($res[0]['time_start_date'] == '') {
			$data['time_start_date'] = $start;
			$this->Common_mdl->update('add_new_task', $data, 'id', $task_id);
		}


		echo $start;
	}

	/*Company : Appy Codes ||| Coder: Amit Das||| Date : 16-06-2020*/
	public function update_the_parent_timer()
	{
		$task_id = $_POST['parent_id'];
		$sub_task_id = $this->db->query("SELECT sub_task_id FROM add_new_task WHERE id = " . $task_id . "")->result_array();
		$sub_task_id = array_column($sub_task_id, 'sub_task_id');
		if (count($sub_task_id) > 0) {
			$sql = "SELECT counttimer FROM add_new_task WHERE id IN (" . $sub_task_id[0] . ")";
			$calc_time = $this->db->query($sql)->result_array();
			$calctime = array_column($calc_time, 'counttimer');
			$total_hours = 0;
			$total_minutes = 0;
			$total_seconds = 0;
			foreach ($calctime as $timer) {
				$timerarray = json_decode($timer);
				$total_hours += $timerarray->hours;
				$total_minutes += $timerarray->minutes;
				$total_seconds += $timerarray->seconds;
			}

			$minutes_from_seconds = 0;
			$hours_from_minutes = 0;

			if ($total_seconds > 59) {
				$minutes_from_seconds = $total_seconds / 60;
				$actual_seconds = $total_seconds % 60;
			} else {
				$actual_seconds = $total_seconds;
			}

			if (($total_minutes + $minutes_from_seconds) > 59) {
				$hours_from_minutes = ($total_minutes + $minutes_from_seconds) / 60;
				$actual_minutes = ($total_minutes + $minutes_from_seconds) % 60;
			} else {
				$actual_minutes = floor($total_minutes + $minutes_from_seconds);
			}

			$actual_hours =  floor($total_hours + $hours_from_minutes);

			$hours = $actual_hours;
			$min = $actual_minutes;
			$sec = $actual_seconds;
			$data_time = ['success' => true, 'hours' => $hours, 'min' => $min, 'sec' => $sec];
			// echo $actual_hours . ":" . $actual_minutes . ":" . $actual_seconds;
			// exit;
		} else {
			$data_time = ['success' => false];
		}
		echo json_encode($data_time);
	}

	public function task_timer_start_pause()
	{
		$task_id = $_POST['task_id'];
		$time = time();
		$res = $this->db->query('select * from add_new_task where id=' . $task_id)->result_array();
		$time_start_pause = '';

		if ($res[0]['time_start_pause'] != '') {
			$time_start_pause = $res[0]['time_start_pause'] . "," . $time;
		} else {
			$time_start_pause = $time;
		}

		$data['time_start_pause'] = $time_start_pause;
		$data['task_status'] = "2";
		$this->Common_mdl->update('add_new_task', $data, 'id', $task_id);

		echo true;
	}

	/** for individual task timer 07-08-2018 **/
	public function task_timer_start_pause_individual()
	{
		$task_id = $_POST['task_id'];
		$user_id = $_SESSION['id'];
		$time 	 = time();
		$sql 	 = 'SELECT * FROM individual_task_timer WHERE task_id=' . $task_id . ' AND user_id=' . $user_id . ' ';
		$res 	 = $this->db->query($sql)->result_array();
		
		if (count($res) > 0) {
			$time_start_pause 		  = '';
			$time_start_pause 		  = ($res[0]['time_start_pause'] != '') ? $res[0]['time_start_pause'] . "," . $time : $time;
			$data['time_start_pause'] = $time_start_pause;
			$this->Common_mdl->update('individual_task_timer', $data, 'id', $res[0]['id']);

			if (isset($_POST['change_id'])) {
				$sql = 'SELECT * FROM individual_task_timer WHERE task_id=' . $task_id . ' ';
				$res = $this->db->query($sql)->result_array();

				if (count($res) > 0) {
					$time_start_pause = '';
					$arr 			  = count(explode(",", $res[0]['time_start_pause']));
					
					if ($res[0]['time_start_pause'] != '' && $arr % 2 != 0) {
						$time_start_pause 		   = $res[0]['time_start_pause'] . "," . $time;
						$data1['time_start_pause'] = $time_start_pause;
						$this->Common_mdl->update('individual_task_timer', $data1, 'id', $res[0]['id']);
					}
				}
				$this->task_statusChange($task_id, $_POST['status']);
			}
		} else {
			if (isset($_POST['change_id'])) {
				$sql = 'SELECT * FROM individual_task_timer WHERE task_id=' . $task_id . ' ';
				$res = $this->db->query($sql)->result_array();

				if (count($res) > 0) {
					$time_start_pause = '';
					$arr 			  = count(explode(",", $res[0]['time_start_pause']));
					
					if ($res[0]['time_start_pause'] != '' && $arr % 2 != 0) {
						$time_start_pause 		  = $res[0]['time_start_pause'] . "," . $time;
						$data['time_start_pause'] = $time_start_pause;
						$this->Common_mdl->update('individual_task_timer', $data, 'id', $res[0]['id']);
					}
				}
				$this->task_statusChange($task_id, $_POST['status']);
			}
			$data['user_id'] 		  = $user_id;
			$data['task_id'] 		  = $task_id;
			$data['time_start_pause'] = $time;
			$data['created_time'] 	  = $time;
			$this->Common_mdl->insert('individual_task_timer', $data);
		}
		
		if ($_POST['timer'] == 'start') {
			$this->session->set_userdata(array('timer' => time(), 'totalSeconds' => $_POST['totalSeconds'], 'timer_task_id' => $_POST['task_id']));
		} else {
			$this->session->unset_userdata('timer');
			$this->session->unset_userdata('totalSeconds');
			$this->session->unset_userdata('timer_task_id');
		}

		####### Added this to simplify the process to identify active timers to get long running tasks #######
		$sql 		   	  = "SELECT id FROM active_timers WHERE user_id = $user_id AND task_id = $task_id AND status = 1 ORDER BY id DESC LIMIT 1";
		$active_timers 	  = $this->db->query($sql)->result_array();
		$time_start_pause = $time;

		if(!empty($active_timers)){	
			$id  = $active_timers[0]['id'];		
			$sql = "UPDATE active_timers SET end_time = $time_start_pause, status = 0 WHERE id = $id";			
		}else{
			$sql = "INSERT INTO active_timers (user_id, task_id, start_time) VALUES ($user_id, $task_id, $time_start_pause)";						
		}		
		$this->db->query($sql);	
		####### /Added this to simplify the process to identify active timers to get long running tasks #######

		echo true;
	}
	/** end of individual task timer  **/

	/* 07-07-2018 update users **/
	/* public function update_select_users()
    {
        $task_id=$_POST['id'];
        $selectuser=$_POST['select_users'];
        $data['selectuser']=$selectuser;
        $this->Common_mdl->update('add_new_task',$data,'id',$task_id);
        $username=$this->Common_mdl->getUserProfileName($_SESSION['id']); 
        $activity_datas['log'] = "Task Select User Was updated  by " .$username;
        $activity_datas['createdTime'] = time();
        $activity_datas['module'] = 'Task';
        $activity_datas['sub_module']='';
        $activity_datas['user_id'] = $_SESSION['id'];
        $activity_datas['module_id'] = $task_id;

        $this->Common_mdl->insert('activity_log',$activity_datas);
    }*/
	/** end of users **/
	public function task_details_assigne_get($task_id)
	{
		$data['task_form'] = $this->Common_mdl->GetAllWithWhere('add_new_task', 'id', $task_id);
		//  print_r($data);
		$this->load->view('users/taskdetails_assign_list', $data);
	}


	public function online_registeration($id)
	{
		$records = $this->Common_mdl->select_record('proposals', 'id', $id);
		$data['proposal'] = $records;

		$this->load->view('proposal/online_registeration', $data);
	}

	public function multipleActiveArchive()
	{
		$userid = explode(',', $_POST['data_id']);
		for ($i = 0; $i < count($userid); $i++) {
			$query = $this->Common_mdl->select_record('user', 'id', $userid[$i]);
			$old_status = $query['status'];
			$data = array('status' => '5', 'old_status' => $old_status);
			$this->Common_mdl->update('user', $data, 'id', $userid[$i]);
		}
		echo 1;
	}

	public function alltasks_archive()
	{

		$userid = explode(',', $_POST['data_ids']);

		echo '<pre>';
		print_r($userid);

		for ($i = 0; $i < count($userid); $i++) {
			$query = $this->Common_mdl->select_record('add_new_task', 'id', $userid[$i]);
			echo '<pre>';
			print_r($query);

			$old_status = $query['task_status'];
			$data = array('task_status' => 'archive', 'task_old_status' => $old_status);
			$this->Common_mdl->update('user', $data, 'id', $userid[$i]);
		}
		echo 1;
	}

	public function archive_task($id)
	{
		$this->session->set_flashdata('sta_tus', 'archive');
		redirect('user/new_task/' . $id);
	}

	public function update_archive()
	{
		$id = $_POST['id'];
		$data = array('status' => 1);
		$this->Common_mdl->update('timeline_services_notes_added', $data, 'id', $id);
		echo 1;
	}

	public function dashboard_settings()
	{
		$this->Security_model->chk_login();
		$data['settings'] = $this->Common_mdl->select_record('settings', 'user_id', '1');
		$this->load->view('users/dashboard_settings', $data);
	}

	/* Coded By Shashmethah*/
	public function Service_reminder_settings($service_id = false)
	{
		error_reporting(E_ERROR);

		if ($_SESSION['firm_id'] == '0' || $_COOKIE['remindoo_logout'] == 'remindoo_superadmin') {
			$this->Loginchk_model->superAdmin_LoginCheck();
		} else {
			$this->Security_model->chk_login();
		}

		if ($_SESSION['permission']['Reminder_Settings']['view'] == '1') {
			$data = $this->reminder_settings();

			$this->load->view('users/remindersettings', $data);
			unset($_SESSION['msg']);
		} else {
			$this->load->view('users/blank_page');
		}
	}

	public function reminder($service_id, $id = false)
	{
		$data = $this->reminder_settings();
		$placeholder = $this->Common_mdl->GetAllWithWhere('tblemailtemplates', 'id', $service_id);

		if ($id != false) {
			$data['record'] = $this->db->query('SELECT * FROM reminder_setting WHERE id = "' . $id . '"')->row_array();
		} else if ($service_id < 17) {
			$data['subject'] = $placeholder[0]['subject'];
		}

		$exp_contect = explode(',', $placeholder[0]['placeholder']);

		foreach ($exp_contect as $val) {
			$data['content'] .= '<span target="message_' . $id . '" class="add_merge">' . $val . '</span><br>';
		}

		$data['service_id'] = $service_id;

		$this->load->view('users/reminder', $data);
	}

	public function get_template()
	{
		$template = array();
		$id = $_POST['id'];
		$template = $this->Common_mdl->select_record('email_templates', 'id', $id);

		$template['subject'] = html_entity_decode($template['subject']);
		$template['body'] = html_entity_decode($template['body']);

		echo json_encode($template);
	}
	/* Coded By Shashmethah*/

	public function get_subtask_count()
	{
		$count = 0;
		$check_array1 = $this->Common_mdl->select_record("add_new_task", "id", $_POST['task_id']);

		$check_array = $this->db->query("SELECT `task_status` FROM add_new_task where id  in (" . $check_array1['sub_task_id'] . ")")->result_array();
		if (count($check_array > 0)) {
			$filter_val = array_column($check_array, 'task_status');

			$result = array_diff($filter_val, array('5'));

			// print_r($result);
			if (count($result) > 0) {
				$count++;
			}
		}
		echo $count;
	}

	public function manager_notify()
	{
		foreach ($_POST['task_id'] as $key => $value) {

			$check_array1 = $this->Common_mdl->select_record("user", "id", $_SESSION['userId']);

			$Message = $value . '///' . $check_array1['crm_name'] . ' /// {TASK_SUBJECT} review by ' . $check_array1['crm_name'];

			$this->Common_mdl->Task_Members_Notify($value, 'Task_Review', $Message);
			$this->task_statusChange($value, 'awaiting');
		}

		echo 0;
	}


	public function assign_status_change()
	{


		$data['status_name'] = $_POST['assignuser_staff'];
		$data['firm_id'] = $_SESSION['firm_id'];

		if ($_POST['type'] == 1) {
			$this->db->insert('assign_task_status', $data);
		}

		if ($_POST['type'] == 3 && $_POST['type'] != '' && $_POST['assignuser_staffid'] != '') {
			// $this->db->insert('assign_task_status',$data);
			$this->db->where('id', $_POST['assignuser_staffid']);
			$this->db->delete('assign_task_status');
		}

		if ($_POST['type'] == 2 && $_POST['type'] != '' && $_POST['assignuser_staffid'] != '') {
			$this->db->where('id', $_POST['assignuser_staffid']);
			$this->db->update('assign_task_status', $data);
		}


		$assign_task_status = $this->Common_mdl->dynamic_status('assign_task_status');

		$datas = '<select name="assign_task_status" class="form-control" id="assign_task_status_' . $_POST["task_id"] . '">
                   <option value="">select</option>';
		foreach ($assign_task_status as $key => $value1) {
			if ($value1["id"] != '') {

				$datas .= '  <option value="' . $value1["id"] . '">' . $value1["status_name"] . '</option>';
			}
		}
		$datas .= ' </select> <a href="javascript:;" data-toggle="modal" data-target="#assignuser_' . $_POST["task_id"] . '" class="assignuser_change "><i class="fa fa-plus"></i></a>';
		echo $datas;
	}

	public function main_task_timer(){
		$ques 			 = Get_Assigned_Datas('TASK');
		$res 			 = (!empty($ques)) ? implode(',', array_filter(array_unique($ques))) : '-1';
		$data['ass_res'] = $res;

		if ($res != '') {
			/*$sql = 	"SELECT * 
					FROM add_new_task AS tl 
					WHERE tl.id IN ($res) 
					AND tl.firm_id = " . $_SESSION['firm_id'] . " 
					ORDER BY id desc LIMIT 1";

			$data['task_list'] 	= $this->db->query($sql)->result_array();*/

			$resArr1 = explode(',', $res);			
			$resArr2 = [];
			$resArr3 = [];
			$limit   = 10;			
			$i       = 0;
			$j       = 0;

			foreach($resArr1 as $ra1){
				if($i < $limit){
					$resArr2[$j][] = $ra1;						 
				}else{
					$i = 0;
					$j++;
				}
				$i++;
			}			
			$taskListArr = [];

			foreach($resArr2 as $ra2){
				$resArr3[] = implode(',', $ra2);
			}
			
			foreach($resArr3 as $ra3){
				$sql = 	"SELECT * 
						FROM add_new_task AS tl 
						WHERE tl.id IN ($ra3) 
						AND tl.firm_id = " . $_SESSION['firm_id'] . " 
						ORDER BY id desc";

				$result = $this->db->query($sql)->result_array();

				foreach($result as $r){
					array_push($taskListArr, $r);
				}				
			}		
			$data['task_list'] 	= 	$taskListArr;
		} else {
			$data['task_list']  = '';
		}		
		$dat_val = $this->Task_creating_model->maintask_timer($data['task_list']);		
	}

	public function update_notification()
	{

		$id = $_POST['id'];
		if ($id != '') {
			$data['status'] = 4;
			$this->db->where('id', $id);
			$this->db->update('notification_management', $data);
		}
		//$_SESSION['user_id']

	}
	public function get_assigness(){
		$data['asssign_group'] = array();
		if (isset($_POST['id']) && isset($_POST['task_name'])) {
			$sql                   = "SELECT * 
									  FROM firm_assignees 
									  WHERE module_id =" . $_POST['id'] . " 
									  AND module_name='" . $_POST['task_name'] . "'";
			$sql_val               = $this->db->query($sql)->result_array();
			$data['asssign_group'] = array_column($sql_val, 'assignees');
			$data['assign_check']  = $this->Common_mdl->assign_check($_POST['id']);
			
			echo json_encode($data);
		}
	}

	public function progress_statusChange()
	{

		$id = $_POST['rec_id'];

		if ($id != '') {
			$check_val = $this->Common_mdl->select_record('add_new_task', 'id', $id);

			$data['progress_status'] = $_POST['status'];
			$this->db->where('id', $id);
			$this->db->update('add_new_task', $data);

			if ($check_val['related_to'] == 'service_reminder') {

				/*$send_val=$_POST['status'];
        $this->Service_Reminder_Model->Reminder_Status_Change($check_val['user_id'],$check_val['related_to_services'],$send_val,$check_val['created_date']);*/
				$this->Service_Reminder_Model->Reminder_Status_Change($id);
			}


			$new_status_val = $this->Common_mdl->select_record('progress_status', 'id', $_POST['status']);
			$old_status_val = $this->Common_mdl->select_record('progress_status', 'id', $check_val['progress_status']);
			$username = $this->Common_mdl->getUserProfileName($_SESSION['id']);

			if (isset($old_status_val['status_name'])) {
				$log = "Progress Status change from " . $old_status_val['status_name'] . " to " . $new_status_val['status_name'] . " Was updated  by ";
			} else {
				$log = "Progress Status " . $new_status_val['status_name'] . " Was updated  by ";
			}
			$this->Common_mdl->Create_Log('Task', $id, $log, $_SESSION['id']);
		}
		echo 1;
		//$_SESSION['user_id']

	}



	// public function delete_user()
	// {
	//   if(isset($_POST['id']))
	//   {
	//    $ids=json_decode($_POST['id'],true);
	//     foreach ($ids as $id)
	//      {
	//         $this->Common_mdl->delete('user','id',$id);
	//         $this->Common_mdl->delete('client','user_id',$id);
	//          /** for schedule delete option **/
	//         $this->Common_mdl->delete('timeline_services_notes_added','user_id',$id);
	//      } 
	//   }
	//   echo $this->db->affected_rows();
	// }


}
