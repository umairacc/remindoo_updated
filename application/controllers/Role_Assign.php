<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role_Assign extends CI_Controller {
public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";

	public function __construct()
  {
    parent::__construct();
    $this->load->model(array('User_management','Common_mdl','Security_model','Invoice_model','Task_invoice_model','Task_creating_model','Task_section_model'));
    $this->load->helper('download');
  }
  /*Coded BY Ajith*/
  /*========================================================*/
	public function index()
  {
		$this->Security_model->chk_login();
    if( $_SESSION['permission']['User_and_Management']['view'] != 1 )
    {
      $this->load->view('users/blank_page');
    }
		else 
    {
      $data['roles'] = $this->User_management->Firm_Roles_List( $_SESSION['firm_id'] );
      $this->load->view('roles/role_list',$data);
    } 
	}

  public function add_role()
  {
   $this->Security_model->chk_login();
   if( $_SESSION['permission']['User_and_Management']['create'] != 1 )
    {
      $this->load->view('users/blank_page');
    }
    else 
    {
      $data['roles_tags'] = GetAssignees_SelectBox_Content();
      $this->load->view('roles/add_roles',$data);
    } 
  }
  public function save_role()
  { 
    $this->Security_model->chk_login();
    if( !empty( $this->input->post('parent_id') ) && !empty( $this->input->post('role') ) )
    {       
      echo $this->User_management->Insert_Role();
    }
  }
  public function edit_role($id)
  {
    $this->Security_model->chk_login();
   if( $_SESSION['permission']['User_and_Management']['edit'] != 1 )
    {
      $this->load->view('users/blank_page');
    }
    else 
    {
      $edit_role = $this->Common_mdl->GetAllWithWheretwo('roles_section' , 'firm_id' , $_SESSION['firm_id'] ,'id', $id );
      
      $data['edit_role'] = end( $edit_role );
      $data['roles_tags'] = GetAssignees_SelectBox_Content( $data['edit_role']['parent_id'] , $data['edit_role']['id'] );
      
      $this->load->view('roles/role_edit',$data);
    }  
  }
  public function save_edited_role()
  { 
    $this->Security_model->chk_login();

    if( !empty( $this->input->post('role_id') ) )
    {
      echo $this->User_management->Update_Role( $this->input->post('role_id') );
    }
  }
  public function DeleteRole()
  {
    if( !empty( $this->input->post( 'id' ) ) )
    {
      echo $this->User_management->Hierarchy_RoleDelete( $this->input->post( 'id' ) );
    }
  }
  /*========================================================*/

	public function roles_add(){

		$data['parent_id']=$_POST['parent_id'];
		$data['role']=str_replace(' ', '_',$_POST['role']);
    $data['firm_id']=$_SESSION['firm_admin_id'];
		$roles['role']=str_replace(' ', '_',$_POST['role']);
		if(isset($_POST['id']) && $_POST['id']!=''){
			$this->Common_mdl->update('roles_section',$data,'id',$_POST['id']);			
		}else{
			$query=$this->Common_mdl->insert('roles_section',$data);
			$roles['id']=$query;
      $roles['firm_id']=$_SESSION['firm_admin_id'];
			$this->Common_mdl->insert('Role',$roles);
      $data=array('role'=>$roles['role'],'firm_id'=>$_SESSION['firm_admin_id'],'role_id'=>$query);
    //  echo "ALTER TABLE `role_permission` ADD ".$roles['role']." VARCHAR NOT NULL";
      $value=$this->Common_mdl->insert('role_permission1',$data);
      //$ccc=$this->db->query("ALTER TABLE `role_permission` ADD ".$roles['role']." varchar(255) not null");
      echo $value;
		}
		redirect('Role_Assign');
	}

	public function reassign_member(){
		$id=$_POST['id'];
		$record=$this->Common_mdl->select_record('roles_section','id',$id);	
		$data['role']=$record['role'];
		$data['id']=$record['id'];
		echo json_encode($data);		
	}

	public function reassign(){
		 $data['roles']=$this->db->query('select * from Role where id not in(1,2,4)')->result_array();
		//$data['roles']=$this->Common_mdl->getall('roles_section');
		$this->load->view('roles/reassign',$data);
	}

	public function director_get(){
		$id=$_POST['id'];
		$director = $this->db->query( "SELECT * FROM user WHERE  (role=".$id.") and firm_admin_id=".$_SESSION['id']."  ")->result_array();

		 $res='';
      $res.='<div class="dropdown-sin-2"><select name="director" id="director"  placeholder="Select"><option value="">Select Director</option>';
      foreach ($director as $key => $value) {
        $sel='';
        
       $res.='<option value='.$value['id'].'>'.$value['username'].'</option>';
      }
      $res.='</select></div>';
       echo $res;
	}
		public function manager_get(){
		$id=$_POST['id'];
		$director = $this->db->query( "SELECT * FROM user WHERE  (role=".$id.") and firm_admin_id=".$_SESSION['id']."  ")->result_array();

		 $res='';
      $res.='<div class="dropdown-sin-2"><select name="manager" id="manager"  placeholder="Select"><option value="">Select Manager</option>';
      foreach ($director as $key => $value) {
        $sel='';
        
       $res.='<option value='.$value['id'].'>'.$value['username'].'</option>';
      }
      $res.='</select></div>';
       echo $res;
	}

	public function get_directorwise_manager(){
       $this->Security_model->chk_login();
       $get_director=$_POST['director'];

       $assign_director=$this->Common_mdl->select_record('assign_director','director',$get_director);
        $sql = $this->db->query("SELECT GROUP_CONCAT(  `manager` SEPARATOR  ',' ) AS  `manager` , GROUP_CONCAT(  `director` SEPARATOR  ',' ) AS  `director` FROM assign_director where id NOT IN( '" .$assign_director['id']."' )")->row_array();
  
       $manager =  explode(',', $sql['manager']);
      //  $director = explode(',',$sql['director']);
        
      //  $data['manager'] = $this->db->query( "SELECT * FROM user WHERE role=5 and crm_name!='' and firm_admin_id=".$_SESSION['id']." ")->result_array();
        $custom_manager = $this->db->query( "SELECT * FROM user WHERE id NOT IN ( '" . implode( "','", $manager ) . "' ) and role=5 and crm_name!='' and firm_admin_id=".$_SESSION['id']." ")->result_array();
        
       // $custom_director = $this->db->query( "SELECT * FROM user WHERE id NOT IN ( '" .  implode( "','", $director ) . "' ) and (role='3') and firm_admin_id=".$_SESSION['id']."  ")->result_array();
         $db_manager_ids=explode(',',$assign_director['manager']);
         $res='';
      $res.='<div class="dropdown-sin-3"><select name="manager[]" id="manager" multiple placeholder="Select Manager">';
      foreach ($custom_manager as $key => $value) {
        $sel='';
        if(isset($value['id']) && in_array( $value['id'] ,$db_manager_ids )) { $sel="selected"; }
       $res.='<option value='.$value['id'].' '.$sel.' >'.$value['username'].'</option>';
      }
      $res.='</select></div>';
       echo $res;
            
    }


	public function get_managerwise_users(){
		$this->Security_model->chk_login();
		$get_manager=$_POST['manager'];
		$assign_manager=$this->Common_mdl->select_record('assign_manager','manager',$get_manager);
		$sql = $this->db->query("SELECT GROUP_CONCAT(  `manager` SEPARATOR  ',' ) AS  `manager` , GROUP_CONCAT(  `staff` SEPARATOR  ',' ) AS  `staff` FROM assign_manager where manager NOT IN( '" .$get_manager."' )")->row_array();

		//  $manager =  explode(',', $sql['manager']);
		$staff = explode(',',$sql['staff']);

		// $data['manager'] = $this->db->query( "SELECT * FROM user WHERE id NOT IN ( '" . implode( "','", $manager ) . "' ) and role=5 and crm_name!='' and firm_admin_id=".$_SESSION['id']." ")->result_array();
//echo "SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) from roles_section where parent_id=".$get_manager."";
		$under_manager=$this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) as id from roles_section where parent_id='5'")->row_array();

		$id = explode(',',$under_manager['id']);

		$user_detailes = $this->db->query( "SELECT * FROM user WHERE id NOT IN ( '" .  implode( "','", $staff ) . "' ) and role IN ( '" .   implode( "','",$id) . "') and firm_admin_id=".$_SESSION['id']." ")->result_array(); 
		// only dispaly staff
		$db_staff_ids=explode(',',$assign_manager['staff']);
		$res='';
		$res.='<label>Select Staff</label><div class="dropdown-sin-3"><select name="staff[]" id="staff" multiple placeholder="Select">';
		foreach ($user_detailes as $key => $value) {
		$sel='';
		if(isset($value['id']) && in_array( $value['id'] ,$db_staff_ids )) { $sel="selected"; }
		$res.='<option value='.$value['id'].' '.$sel.' >'.$value['username'].'</option>';
		}
		$res.='</select></div>';
		echo $res;
	}

  public function assign_member()
    {
        $this->Security_model->chk_login();
        //$data['manager'] = $_POST['catagory1'];
        $data['members_id'] = implode(',',$_POST['catagory2']);
        $get_db_manager=$this->Common_mdl->select_record('assign_member','assign_to_id',$_POST['catagory1']);  
      if(count($get_db_manager) > 0)
      {
        $res=$this->Common_mdl->update('assign_member',$data,'assign_member_id',$get_db_manager['assign_member_id']);
      }
      else
      {
        $role=current($this->db->query("select role from user where id=$_POST[catagory1]")->result_array());
       $data['assign_to_roll']=$role['role']; 
       $data['assign_to_id']=$_POST['catagory1']; 
       $data['create_by']=$_SESSION['id'];
       $res=$this->Common_mdl->insert('assign_member',$data); 
      }
        $this->session->set_flashdata('success', 'Updated Successfully');
       echo 1;
    }
	///ajith

	public function get_members($selected_user)
	{

       $this->Security_model->chk_login();
       $selected_user_assigned='';
       $already_assigned="";
       $selected_role=current($this->db->query("select * from user where id=$selected_user")->result_array());
       $assigned_member=$this->db->query("select * from assign_member where assign_to_roll=$selected_role[role]")->result_array();
       foreach ($assigned_member as $key => $value) 
       {
       		if($value['assign_to_id']==$selected_user)$selected_user_assigned=$value['members_id'];
       		else $already_assigned.=($already_assigned==""?$value['members_id']:",".$value['members_id']);
       }
		  $under_selected_role=$this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) as id from roles_section where parent_id=$selected_role[role]")->row_array();
      $avoid=(($already_assigned=="" || ($selected_role['role']+1)==6) ?'':"id NOT IN (".$already_assigned.") and");
      $members=$this->db->query( "SELECT * FROM user WHERE ".$avoid." role IN (".$under_selected_role['id'].") and crm_name!='' and firm_admin_id=".$_SESSION['id']." ")->result_array();

		  $db_manager_ids=explode(',',$selected_user_assigned);        
      $res='<div class="dropdown-sin-3"><select name="catagory2[]" id="catagory2" multiple placeholder="Select ">';
      foreach ($members as $key => $value) 
      {
      $sel='';
    	if(isset($value['id']) && in_array( $value['id'] ,$db_manager_ids )) { $sel="selected"; }
    	$res.='<option value='.$value['id'].' '.$sel.' >'.$value['username'].'</option>';
      }
      $res.='</select></div>';
      echo $res;		       		   
            
    }
    public function get_select_role_user($role_id)
    {
    	$n=current($this->db->query("select * from roles_section where id=$role_id")->result_array());
    	$u=$this->db->query("select * from user where role=$role_id and crm_name!='' and firm_admin_id=".$_SESSION['id']." ")->result_array();
    	$sel='<div class="dropdown-sin-2"><select name="catagory1" id="catagory1" placeholder="Select '.$n['role'].'">';
    	foreach ($u as $v) 
    	{
    		$sel.='<option value='.$v['id'].'>'.$v['username'].'</option>';
    	}
    	$sel.='</select></div>';
    	echo $sel;
    }
///ajith
	public function under_details_check(){
		$id=$_POST['id'];	
		$under_manager=$this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) as id from roles_section where parent_id=".$id."")->row_array();	
		$id = array_filter(explode(',',$under_manager['id']));
		if(!empty($id) && count($id) > 0){
			echo 1;
		}else{
			echo 0;
		}
	}

	public function update_assign_stafftomanager()
    {
        $this->Security_model->chk_login();
        $data['manager'] = $_POST['manager'];
        $data['staff'] = implode(',',$_POST['staff']);
        $get_db_manager=$this->Common_mdl->select_record('assign_manager','manager',$_POST['manager']);  
      if(count($get_db_manager) > 0)
      {
        $res=$this->Common_mdl->update('assign_manager',$data,'id',$get_db_manager['id']);
      }
      else
      {
       $data['created_time'] = time();
       $data['create_by']=$_SESSION['id'];
       $res=$this->Common_mdl->insert('assign_manager',$data); 
      }
        $this->session->set_flashdata('success', 'Assigne Staff Updated Successfully');
       echo 1;
    }


      public function update_assign_managertodirector()
    {
        $this->Security_model->chk_login();
        $data['director'] = $_POST['director'];
        $data['manager'] = implode(',',$_POST['manager']);
        $get_db_director=$this->Common_mdl->select_record('assign_director','director',$_POST['director']);
        if(count($get_db_director) > 0)
        {
           $res=$this->Common_mdl->update('assign_director',$data,'id',$get_db_director['id']);
        }
        else
        {
           $data['created_time'] = time();
           $data['create_by']=$_SESSION['id'];
           $res=$this->Common_mdl->insert('assign_director',$data);
        }
        $this->session->set_flashdata('success', 'Assigne Manager Updated Successfully');
       echo 1;
       
    }

    public function permission(){      
     // echo "<pre>";
     //  print_r($_POST);
     //  exit;

      if(isset($_POST['parent_id']) && $_POST['parent_id']!='')
      {
         $data['parent_id'] = $_POST['parent_id'];
      }

    $data['role']=str_replace(' ', '_',$_POST['role']);
    $data['firm_id']=$_SESSION['firm_admin_id'];
    $roles['role']=str_replace(' ', '_',$_POST['role']);
    if(isset($_POST['role_id']) && $_POST['role_id']!='')
    {
      $this->Common_mdl->update('roles_section',$data,'id',$_POST['role_id']);     
    }
    else
    {
      $query=$this->Common_mdl->insert('roles_section',$data);
      $roles['id']=$query;
      $roles['firm_id']=$_SESSION['firm_admin_id'];
      $_POST['role_id']=$this->Common_mdl->insert('Role',$roles);
      $data=array('role'=>$roles['role'],'firm_id'=>$_SESSION['firm_admin_id'],'role_id'=>$query);
      //  echo "ALTER TABLE `role_permission` ADD ".$roles['role']." VARCHAR NOT NULL";
      $value1=$this->Common_mdl->insert('role_permission1',$data);
    }
   // print_r($_POST['role_id']);
   //     exit;
      //$ccc=$this->db->query("ALTER TABLE `role_permission` ADD ".$roles['role']." varchar(255) not null");
     // echo $value;

      
      $value=array();
      $view_all=array();
            if(isset($_POST['parent_id']) && $_POST['parent_id']!=''){

         unset($data['parent_id']);
      }
        for($i=0;$i<count($_POST['settings_section']);$i++){    

           $view=isset($_POST['bulkview'][$_POST['settings_section'][$i].'-view']) ? '1' : '0';  

           array_push($value,$view);    

           $create=isset($_POST['bulkcreates'][$_POST['settings_section'][$i].'-create']) ? '1' : '0';

           array_push($value,$create);          

           $edit=isset($_POST['bulkedit'][$_POST['settings_section'][$i].'-edit']) ? '1' : '0';

           array_push($value,$edit);     

           $delete=isset($_POST['bulkdelete'][$_POST['settings_section'][$i].'-delete']) ? '1' : '0';

           array_push($value,$delete);

           $data[$_POST['settings_section'][$i]]=implode(',',$value);  

           $view=isset($_POST['optionsall']['viewall_'.$_POST['settings_section'][$i]]) ? '1' : '0';  

           array_push($view_all,$view);            

           $create=isset($_POST['optionsassigned']['viewassigned_'.$_POST['settings_section'][$i]]) ? '1' : '0';

           array_push($view_all,$create);             

           $edit=isset($_POST['optioncreated']['viewcreated_'.$_POST['settings_section'][$i]]) ? '1' : '0';

           array_push($view_all,$edit);             

           $delete=isset($_POST['optionsown']['viewown_'.$_POST['settings_section'][$i]]) ? '1' : '0';

           array_push($view_all,$delete);       

           $data['viewall_'.$_POST['settings_section'][$i]]=implode(',',$view_all);    

          $this->Common_mdl->update('role_permission1',$data,'role_id',$_POST['role_id']);

          $value=array();
          $view_all=array();
      }
     $this->Common_mdl->role_identify();      
     redirect('Role_Assign');
    }

    public function permission_content(){     
      $data['role']=$_POST['role'];
      $data['parent_id']=$_POST['parent_id'];

      if($_POST['role_id']!='')
      {
       $data['role_id']=$_POST['role_id'];
      }else{
         $data['role_id']='';

      }

      $this->load->view('roles/permisson_page',$data);
    }


  
    


}

?>
