<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Document extends CI_Controller
{
	public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 *      http://example.com/index.php/welcome
	 *  - or -
	 *      http://example.com/index.php/welcome/index
	 *  - or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('Common_mdl', 'Security_model'));
	}

	public function index()
	{
		$this->Security_model->chk_login();
		$array_client_id = array();

		if ($_SESSION['role'] == 4) {

			$data['documents'] = $this->Common_mdl->GetAllWithWhere('document_record', 'create_by', $_SESSION['id']);
		} else if ($_SESSION['role'] == 3) {
			//Director created Documents

			//****
			$director_document = $this->Common_mdl->GetAllWithWhere('document_record', 'create_by', $_SESSION['id']);

			$manager_details = $this->db->query("select * from user where  firm_admin_id =" . $_SESSION['id'] . " and role='5'")->result_array();

			if (count($manager_details) > 0) {
				$managers = array();
				foreach ($manager_details as $manager) {
					array_push($managers, $manager['id']);
				}
				$man_doc = implode(',', $managers);
			} else {
				$man_doc = 0;
			}

			//Director Assign Manager

			$dir_manager_details = $this->db->query("select * from assign_director where director='" . $_SESSION['id'] . "'")->row_array();

			if (count($dir_manager_details) > 0) {
				$director_manager = $dir_manager_details['manager'];
			} else {
				$director_manager = 0;
			}

			//****
			//Director Under Manager created Documents
			$manager_document = $this->db->query("select * from document_record where ( create_by in (" . $man_doc . ")  or create_by in (" . $director_manager . ")  )")->result_array();


			//Manager Under Staff Details

			$staff_details = $this->db->query("select * from user where (firm_admin_id in (" . $man_doc . ") or firm_admin_id in (" . $director_manager . ") ) and role='6'")->result_array();

			if (count($staff_details) > 0) {
				$staffs_details = array();
				foreach ($staff_details as $staff) {
					array_push($staffs_details, $staff['id']);
				}

				$stf_doc = implode(',', $staffs_details);
			} else {
				$stf_doc = 0;
			}

			//Manager assign staff

			$man_staff_details = $this->db->query("select * from assign_manager where manager in (" . $man_doc . ") ")->row_array();

			if (count($man_staff_details) > 0) {

				$manager_staff = $man_staff_details['staff'];
			} else {
				$manager_staff = 0;
			}

			//****
			$staff_document = $this->db->query("select * from document_record where (create_by in (" . $stf_doc . ") or create_by in (" . $manager_staff . ") )")->result_array();

			$team_num = array();
			$for_cus_team = $this->db->query("select * from team_assign_staff where (staff_id in ('" . $stf_doc . "') or staff_id in ('" . $manager_staff . "') )")->result_array();
			foreach ($for_cus_team as $cu_team_key => $cu_team_value) {
				array_push($team_num, $cu_team_value['team_id']);
			}

			if (!empty($team_num) && count($team_num) > 0) {
				$res_team_num = implode('|', $team_num);
				$res_team_num1 = implode(',', $team_num);
			} else {
				$res_team_num = '0';
				$res_team_num1 = '0';
			}
			$department_num = array();
			$for_cus_department = $this->db->query("SELECT * FROM `department_assign_team` where CONCAT(',', `team_id`, ',') REGEXP ',($res_team_num),'")->result_array();
			foreach ($for_cus_department as $cu_dept_key => $cu_dept_value) {
				array_push($department_num, $cu_dept_value['depart_id']);
			}
			if (!empty($department_num) && count($department_num) > 0) {
				$res_dept_num = implode('|', $department_num);
				$res_dept_num1 = implode(',', $department_num);
			} else {
				$res_dept_num = '0';
				$res_dept_num1 = '0';
			}

			/* responsible team **/

			$responsible_team = $this->db->query("select * from responsible_team where team in (" . $res_team_num1 . ") and team!='' ")->result_array();
			if (!empty($responsible_team)) {
				foreach ($responsible_team as $retm_key => $retm_value) {
					array_push($array_client_id, $retm_value['client_id']);
				}
			}
			/** responsible department **/
			$responsible_depart = $this->db->query("select * from responsible_department where depart in (" . $res_dept_num1 . ") and depart!='' ")->result_array();
			if (!empty($responsible_depart)) {
				foreach ($responsible_depart as $redekey => $rede_value) {
					array_push($array_client_id, $rede_value['client_id']);
				}
			}
			/** responsible user **/
			$responsible_staff = $this->db->query("select * from responsible_user where (manager_reviewer in  (" . $man_doc . ") or  manager_reviewer in (" . $director_manager . ") or manager_reviewer in (" . $stf_doc . ") or manager_reviewer in (" . $manager_staff . ") ) ")->result_array();
			if (!empty($responsible_staff)) {
				foreach ($responsible_staff as $rest_key => $rest_value) {
					array_push($array_client_id, $rest_value['client_id']);
				}
			}


			$responsible_management = $this->db->query("select * from responsible_user where (assign_managed in (" . $man_doc . ") or  assign_managed in (" . $director_manager . ") or assign_managed in (" . $stf_doc . ") or assign_managed in (" . $manager_staff . ")) ")->result_array();
			if (!empty($responsible_management)) {
				foreach ($responsible_management as $rest_key => $rest_value) {
					array_push($array_client_id, $rest_value['client_id']);
				}
			}

			$get_login_user = $this->db->query("select * from user where firm_admin_id=" . $_SESSION['id'] . " and role='4' AND autosave_status!='1' ")->result_array();
			if (!empty($get_login_user)) {
				foreach ($get_login_user as $lg_key => $lg_value) {
					array_push($array_client_id, $lg_value['id']);
				}
			}
			/** 06-08-2018 **/
			if (!empty($array_client_id)) {
				$result_var = implode(',', array_unique($array_client_id));
			} else {
				$result_var = '0';
			}

			//echo $result_var."-----abcabc";

			//****
			$client_document = $this->db->query("SELECT * FROM document_record where ( create_by in (" . $result_var . ") ) order by id DESC")->result_array();


			$data['documents'] = array_merge($client_document, $staff_document, $director_document, $manager_document);
		} else if ($_SESSION['role'] == 5) {
			//Director created Documents
			$manager_document = $this->Common_mdl->GetAllWithWhere('document_record', 'create_by', $_SESSION['id']);
			//Manager Under Staff Details
			$staff_details = $this->db->query("select * from user where firm_admin_id='" . $_SESSION['id'] . "'  and role='6'")->result_array();


			if (count($staff_details) > 0) {
				$staffs_details = array();
				foreach ($staff_details as $staff) {
					array_push($staffs_details, $staff['id']);
				}
				$stf_doc = implode(',', $staffs_details);
			} else {
				$stf_doc = 0;
			}
			//Manager assign staff
			$man_staff_details = $this->db->query("select * from assign_manager where manager = '" . $_SESSION['id'] . "'")->row_array();
			if (count($man_staff_details) > 0) {
				$manager_staff_implode = explode(',', $man_staff_details['staff']);
				$manager_staff = $man_staff_details['staff'];
			} else {
				$manager_staff = 0;
			}


			$staff_document = $this->db->query("select * from document_record where (create_by in (" . $stf_doc . ") or create_by in (" . $manager_staff . ") )")->result_array();

			$team_num = array();
			$for_cus_team = $this->db->query("select * from team_assign_staff where (staff_id in ('" . $stf_doc . "') or staff_id in ('" . $manager_staff . "') )")->result_array();
			foreach ($for_cus_team as $cu_team_key => $cu_team_value) {
				array_push($team_num, $cu_team_value['team_id']);
			}

			if (!empty($team_num) && count($team_num) > 0) {
				$res_team_num = implode('|', $team_num);
				$res_team_num1 = implode(',', $team_num);
			} else {
				$res_team_num = '0';
				$res_team_num1 = '0';
			}
			$department_num = array();
			$for_cus_department = $this->db->query("SELECT * FROM `department_assign_team` where CONCAT(',', `team_id`, ',') REGEXP ',($res_team_num),'")->result_array();
			foreach ($for_cus_department as $cu_dept_key => $cu_dept_value) {
				array_push($department_num, $cu_dept_value['depart_id']);
			}
			if (!empty($department_num) && count($department_num) > 0) {
				$res_dept_num = implode('|', $department_num);
				$res_dept_num1 = implode(',', $department_num);
			} else {
				$res_dept_num = '0';
				$res_dept_num1 = '0';
			}

			/* responsible team **/

			$responsible_team = $this->db->query("select * from responsible_team where team in (" . $res_team_num1 . ") and team!='' ")->result_array();
			if (!empty($responsible_team)) {
				foreach ($responsible_team as $retm_key => $retm_value) {
					array_push($array_client_id, $retm_value['client_id']);
				}
			}
			/** responsible department **/
			$responsible_depart = $this->db->query("select * from responsible_department where depart in (" . $res_dept_num1 . ") and depart!='' ")->result_array();
			if (!empty($responsible_depart)) {
				foreach ($responsible_depart as $redekey => $rede_value) {
					array_push($array_client_id, $rede_value['client_id']);
				}
			}
			/** responsible user **/
			$responsible_staff = $this->db->query("select * from responsible_user where ( manager_reviewer in (" . $stf_doc . ") or manager_reviewer in (" . $manager_staff . ") ) ")->result_array();
			if (!empty($responsible_staff)) {
				foreach ($responsible_staff as $rest_key => $rest_value) {
					array_push($array_client_id, $rest_value['client_id']);
				}
			}

			$responsible_management = $this->db->query("select * from responsible_user where ( assign_managed in (" . $stf_doc . ") or assign_managed in (" . $manager_staff . ")) ")->result_array();
			if (!empty($responsible_management)) {
				foreach ($responsible_management as $rest_key => $rest_value) {
					array_push($array_client_id, $rest_value['client_id']);
				}
			}

			$get_login_user = $this->db->query("select * from user where firm_admin_id=" . $_SESSION['id'] . " and role='4' AND autosave_status!='1' ")->result_array();
			if (!empty($get_login_user)) {
				foreach ($get_login_user as $lg_key => $lg_value) {
					array_push($array_client_id, $lg_value['id']);
				}
			}
			/** 06-08-2018 **/
			if (!empty($array_client_id)) {
				$result_var = implode(',', array_unique($array_client_id));
			} else {
				$result_var = '0';
			}
			//echo $result_var."-----abcabc";
			$client_document = $this->db->query("SELECT * FROM document_record where ( create_by in (" . $result_var . ") ) order by id DESC")->result_array();

			$data['documents'] = array_merge($client_document, $staff_document, $manager_document);
		} else if ($_SESSION['role'] == 6) {


			$staff_document = $this->Common_mdl->GetAllWithWhere('document_record', 'create_by', $_SESSION['id']);
			$team_num = array();
			$for_cus_team = $this->db->query("select * from team_assign_staff where FIND_IN_SET('" . $_SESSION['id'] . "',staff_id)")->result_array();
			foreach ($for_cus_team as $cu_team_key => $cu_team_value) {
				array_push($team_num, $cu_team_value['team_id']);
			}

			if (!empty($team_num) && count($team_num) > 0) {
				$res_team_num = implode('|', $team_num);
				$res_team_num1 = implode(',', $team_num);
			} else {
				$res_team_num = '0';
				$res_team_num1 = '0';
			}
			$department_num = array();
			$for_cus_department = $this->db->query("SELECT * FROM `department_assign_team` where CONCAT(',', `team_id`, ',') REGEXP ',($res_team_num),'")->result_array();
			foreach ($for_cus_department as $cu_dept_key => $cu_dept_value) {
				array_push($department_num, $cu_dept_value['depart_id']);
			}
			if (!empty($department_num) && count($department_num) > 0) {
				$res_dept_num = implode('|', $department_num);
				$res_dept_num1 = implode(',', $department_num);
			} else {
				$res_dept_num = '0';
				$res_dept_num1 = '0';
			}

			/* responsible team **/

			$responsible_team = $this->db->query("select * from responsible_team where team in (" . $res_team_num1 . ") and team!='' ")->result_array();
			if (!empty($responsible_team)) {
				foreach ($responsible_team as $retm_key => $retm_value) {
					array_push($array_client_id, $retm_value['client_id']);
				}
			}
			/** responsible department **/
			$responsible_depart = $this->db->query("select * from responsible_department where depart in (" . $res_dept_num1 . ") and depart!='' ")->result_array();
			if (!empty($responsible_depart)) {
				foreach ($responsible_depart as $redekey => $rede_value) {
					array_push($array_client_id, $rede_value['client_id']);
				}
			}
			/** responsible user **/
			$responsible_staff = $this->db->query("select * from responsible_user where manager_reviewer='" . $_SESSION['id'] . "'")->result_array();
			if (!empty($responsible_staff)) {
				foreach ($responsible_staff as $rest_key => $rest_value) {
					array_push($array_client_id, $rest_value['client_id']);
				}
			}

			$responsible_management = $this->db->query("select * from responsible_user where  assign_managed='" . $_SESSION['id'] . "'")->result_array();
			if (!empty($responsible_management)) {
				foreach ($responsible_management as $rest_key => $rest_value) {
					array_push($array_client_id, $rest_value['client_id']);
				}
			}

			$get_login_user = $this->db->query("select * from user where firm_admin_id=" . $_SESSION['id'] . " and role='4' AND autosave_status!='1' ")->result_array();
			if (!empty($get_login_user)) {
				foreach ($get_login_user as $lg_key => $lg_value) {
					array_push($array_client_id, $lg_value['id']);
				}
			}
			/** 06-08-2018 **/
			if (!empty($array_client_id)) {
				$result_var = implode(',', array_unique($array_client_id));
			} else {
				$result_var = '0';
			}
			
			$client_document = $this->db->query("SELECT * FROM document_record where ( create_by in (" . $result_var . ") ) order by id DESC")->result_array();

			$data['documents'] = array_merge($client_document, $staff_document);
		} else if ($_SESSION['id'] == '1' || $_SESSION['id'] == '2') {
			$data['documents'] = $this->Common_mdl->getallrecords('document_record');
		}

		$this->load->view('Document/document_list', $data);
	}
	public function archive_document($id)
	{
		$this->db->query("update document_record set status=1 where id=$id");
		echo $this->db->affected_rows();
	}
	public function document_add()
	{
		$this->load->view('Document/document_add_view');
	}
	public function add_document_data()
	{
		//        //  $data['document_file']=$this->Common_mdl->do_upload($_FILES['document_file'], 'documents');

		if (!empty($_FILES['document_file']['name'])) {
			//$filesCount = count($_FILES['userFiles']['name']);
			//for($i = 0; $i < $filesCount; $i++){
			$_FILES['document_file']['name'] = $_FILES['document_file']['name'];
			$_FILES['document_file']['type'] = $_FILES['document_file']['type'];
			$_FILES['document_file']['tmp_name'] = $_FILES['document_file']['tmp_name'];
			$_FILES['document_file']['error'] = $_FILES['document_file']['error'];
			$_FILES['document_file']['size'] = $_FILES['document_file']['size'];
			$uploadPath = 'documents/';
			$config['upload_path'] = $uploadPath;
			$config['allowed_types'] = '*';
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ($this->upload->do_upload('document_file')) {
				$fileData = $this->upload->data();
				//  $uploadData[$i]['file_name'] = base_url().'attachment/'.$fileData['file_name'];
				$new_image_name = base_url() . 'documents/' . $fileData['file_name'];
			}
		}

		$firm_details = $this->db->query("select firm_admin_id from  user where id='" . $_SESSION['id'] . "'")->row_array();

		$firm_id = $firm_details['firm_admin_id'];

		$respon_user = $this->db->query("select * from responsible_user where client_id='" . $_SESSION['id'] . "'")->result_array();
		//   echo '<pre>';
		//   print_r($respon_user);
		$assigned_user_reviewer = array();
		$assigned_user_maneged = array();
		foreach ($respon_user as $user) {
			array_push($assigned_user_reviewer, $user['manager_reviewer']);
			array_push($assigned_user_maneged, $user['assign_managed']);
		}
		//   print_r($assigned_user_reviewer);
		//  print_r($assigned_user_maneged);

		$responsible_members = $this->db->query("select * from responsible_members where client_id='" . $_SESSION['id'] . "'")->result_array();
		//  print_r($responsible_members);
		$assigned_member_reviewer = array();
		$assigned_member_maneged = array();
		foreach ($responsible_members as $member) {
			array_push($assigned_member_reviewer, $member['manager_reviewer']);
			array_push($assigned_member_maneged, $member['assign_managed']);
		}
		//  print_r($assigned_member_reviewer);
		// print_r($assigned_member_maneged);

		$responsible_team = $this->db->query("select * from responsible_team where client_id='" . $_SESSION['id'] . "'")->result_array();
		// print_r($responsible_team);
		$assigned_team = array();
		foreach ($responsible_team as $team) {
			array_push($assigned_team, $team['team']);
		}
		//  print_r($assigned_team);

		$responsible_dept = $this->db->query("select * from responsible_department where client_id='" . $_SESSION['id'] . "'")->result_array();
		//   print_r($responsible_dept);
		$assigned_dept = array();
		foreach ($responsible_dept as $dept) {
			array_push($assigned_dept, $dept['depart']);
		}

		// print_r($assigned_dept);
		$data['document_file'] = $new_image_name;
		$data['document_name'] = $_POST['document_name'];
		$data['document_category'] = $_POST['document_category'];
		$data['create_by'] = $_SESSION['id'];
		$data['document_type'] = $_FILES['document_file']['type'];
		$data['firm_admin_id'] = $firm_id;
		$data['assigned_team'] = implode(',', $assigned_team);
		$data['assigned_user_reviewer'] = implode(',', $assigned_user_reviewer);
		$data['assigned_user_maneged'] = implode(',', $assigned_user_maneged);
		$data['assigned_dept'] = implode(',', $assigned_dept);
		$data['assgined_member_reviewed'] = implode(',', $assigned_member_reviewer);
		$data['assign_member_managed'] = implode(',', $assigned_member_maneged);
		$data['createTime'] = time();

		$insert_data = $this->db->insert('document_record', $data);
		//  $this->session->set_flashdata('success',"Document Data Created Successfully!!!");
		// redirect('Document');
		$data['documents'] = $this->Common_mdl->GetAllWithWhere('document_record', 'create_by', $_SESSION['id']);
		$this->load->view('Document/document_list_table', $data);
	}

	public function document_list_delete($id)
	{
		$this->Common_mdl->delete('document_record', 'id', $id);
		$data['documents'] = $this->Common_mdl->GetAllWithWhere('document_record
', 'create_by', $_SESSION['id']);
		$this->load->view('Document/document_list_table', $data);
	}
	public function edit_document($id)
	{
		$data['documents'] = $this->Common_mdl->GetAllWithWhere('document_record
', 'id', $id);
		$this->load->view('Document/document_edit_view', $data);
	}

	public function update_document_data($id)
	{
		if (!empty($_FILES['document_file']['name'])) {
			//$filesCount = count($_FILES['userFiles']['name']);
			//for($i = 0; $i < $filesCount; $i++){
			$_FILES['document_file']['name'] = $_FILES['document_file']['name'];
			$_FILES['document_file']['type'] = $_FILES['document_file']['type'];
			$_FILES['document_file']['tmp_name'] = $_FILES['document_file']['tmp_name'];
			$_FILES['document_file']['error'] = $_FILES['document_file']['error'];
			$_FILES['document_file']['size'] = $_FILES['document_file']['size'];
			$uploadPath = 'documents/';
			$config['upload_path'] = $uploadPath;
			$config['allowed_types'] = '*';
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ($this->upload->do_upload('document_file')) {
				$fileData = $this->upload->data();
				//  $uploadData[$i]['file_name'] = base_url().'attachment/'.$fileData['file_name'];
				$new_image_name = base_url() . 'documents/' . $fileData['file_name'];
			}
		} else {
			$new_image_name = $_POST['old_doc_file'];
		}

		$data['document_file'] = $new_image_name;
		$data['document_name'] = $_POST['document_name'];
		$data['document_category'] = $_POST['document_category'];
		$data['create_by'] = $_SESSION['id'];
		$data['createTime'] = time();
		$this->Common_mdl->update('document_record', $data, 'id', $id);
		// $this->session->set_flashdata('success',"Document Data Updated Successfully!!!");
		//  redirect('Document');
		$data['documents'] = $this->Common_mdl->GetAllWithWhere('document_record
', 'create_by', $_SESSION['id']);
		$this->load->view('Document/document_list_table', $data);
	}

	public function document_edit_poup()
	{
		$data['documents'] = $this->Common_mdl->GetAllWithWhere('document_record
', 'create_by', $_SESSION['id']);
		$this->load->view('Document/document_edit_popup', $data);
	}

	/** 04-08-2018 **/
	public function delete_bulk_document()
	{
		if (isset($_POST['data_id'])) {
			$id = trim($_POST['data_id']);
			$sql = $this->db->query("DELETE FROM document_record WHERE id in ($id)");
			echo $id;
		}
	}

	/** 04-08-2018 **/
}
