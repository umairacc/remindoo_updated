<?php
//error_reporting(E_STRICT);
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
  public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";

  public function __construct(){
    parent::__construct();
    @session_start();
    $this->load->model(array('Common_mdl','Security_model','Invoice_model','Proposal_model','Report_model'));
    $this->load->helper(['comman']);
  }

  public function index($user_id=false){  
    $ret_val = $this->Security_model->chk_login();
	  
    if($ret_val){
      $this->adminDashboard($user_id);  
    }             
  }

  public function adminDashboard($user_id){     
    $get_datalist= array();
    
    if($_SESSION['permission']['Dashboard']['view']=='1'){                           
      $Assigned_Client                = Get_Assigned_Datas('CLIENT'); 
      $assigned_client                = (!empty($Assigned_Client))? implode(',',$Assigned_Client):'0';
      $Assigned_Leads                 = Get_Assigned_Datas('LEADS');
      $assigned_leads                 = (!empty($Assigned_Leads))? implode(',',$Assigned_Leads):'0';
      $Assigned_Task                  = Get_Assigned_Datas('TASK');
      $assigned_task                  = (!empty($Assigned_Task))? implode(',',$Assigned_Task):'0';
      $select_fields                  = 'user_id';
      $cli_arr                        = $this ->Common_mdl->getall_wherein_records_improved('client','id',$assigned_client,$select_fields);
      $arr                            = (!empty($cli_arr))? array_column($cli_arr,'user_id'):['0'];
      $data['client_userid']          = implode(',',$arr);
      $data['client_records']         = $cli_arr;
      $data['assigned_client']        = $assigned_client;
      $data['assigned_task']          = $assigned_task;
      $data['getCompany']             = $this->db->query("SELECT id FROM client WHERE (FIND_IN_SET(user_id,'".implode(',',$arr)."') AND firm_id = '".$_SESSION['firm_id']."') and crm_company_name !='' ORDER BY id DESC")->result_array();
      $cond_arr                       = array();
      $cnt_arr                        = $this->Common_mdl->getall_wherein_records_improved_count('leads','id',$assigned_leads,$cond_arr);
      $data['total_leads']            = $cnt_arr[0]['cnt'];
      $data['todolist']               = $this->db->query("SELECT id,todo_name from `todos_list` WHERE user_id ='".$_SESSION['id']."'order by order_number Asc")->result_array();   
      $cnt_arr                        = $this->db->query('SELECT count(id) As cnt FROM `add_new_task` WHERE related_to != "sub_task" AND firm_id = "'.$_SESSION['firm_id'].'" AND  id in ("'.$assigned_task.'") AND task_status in (4,5)')->result_array();
      $data['closed_tasks']           = $cnt_arr[0]['cnt'];
      $cond_arr                       = array('autosave_status'=>'<>1','status'=>'=1');
      $cnt_arr                        = $this->Common_mdl->getall_wherein_records_improved_count('user','id',implode(',',$arr),$cond_arr);
      $data['active_client']          = $cnt_arr[0]['cnt'];
      $cond_arr                       = array('autosave_status'=>'<>1','status'=>' in (2,0)');
      $cnt_arr                        = $this->Common_mdl->getall_wherein_records_improved_count('user','id',implode(',',$arr),$cond_arr);
      $data['frozen_client']          = $cnt_arr[0]['cnt'];
      $cond_arr                       = array('autosave_status'=>'<>1','status'=>'=3');
      $cnt_arr                        = $this->Common_mdl->getall_wherein_records_improved_count('user','id',implode(',',$arr),$cond_arr);
      $data['inactive_client']        = $cnt_arr[0]['cnt'];
      $last_month_start               = date("Y-m-01");
      $last_month_end                 = date("Y-m-t"); 
      $last_month_end                 = date('Y-m-d', strtotime($last_month_end . ' +1 day'));   
      $data_proposal                  = $this->Proposal_model->proposal_details_date('',$last_month_start,$last_month_end); 
      $propsal_status                 = array_count_values(array_filter(array_column($data_proposal, 'status')));
      $data['indiscussion_proposal']  = isset($propsal_status['in discussion'])?$propsal_status['in discussion']:'0';
      $data['accept_proposal']        = isset($propsal_status['accepted'])?$propsal_status['accepted']:'0';
      $data['sent_proposal']          = isset($propsal_status['sent'])?$propsal_status['sent']:'0';
      $data['viewed_proposal']        = isset($propsal_status['opened'])?$propsal_status['opened']:'0';
      $data['declined_proposal']      = isset($propsal_status['declined'])?$propsal_status['declined']:'0';
      $data['archive_proposal']       = isset($propsal_status['archive'])?$propsal_status['archive']:'0';
      $data['draft_proposal']         = isset($propsal_status['draft'])?$propsal_status['draft']:'0';

      # Due to start popup
      $user_id                        = $_SESSION['id'];
      $data['due_to_start']           = get_user_filtered_tasks_7days('TASK', $user_id);
      $usr                            = $this->Common_mdl->select_record('user', 'id', $user_id);
      $data['username']               = $usr['crm_name']; 
      # /Due to start popup

      $this->load->view('firm_dashboard/desk_dashboard',$data); 
      unset($_SESSION['success_msg']);
    }else{
      $this->load->view('users/blank_page');
    } 
  }
 
    public function staffDashboard()
    {
         $id = $this->session->userdata('id');
       $data['user_details']=$this->Common_mdl->select_record('user','id',$id);
       //$data['create_clients_list']=$this->Common_mdl->GetAllWithWhere('user','firm_admin_id',$id);
       //$data['create_clients_list']=$this->db->query("select * from user where firm_admin_id='".$id."' and role = '6' ")->result_array();
       $data['create_clients_list']=$this->db->query("select * from user where firm_admin_id='".$id."' and crm_name != '' ")->result_array();
       if(isset($_POST['date']))
       {
        $month = '';
        $year = '';
       }else{
        $month = date('m');
        $year = date('Y');
       }   


       $data['start_end_date'] = $this->start_end_date('01-'.$month.'-'.$year);

       $exp_date = explode(',', $data['start_end_date']);

       $start = $exp_date[0];
       $end = $exp_date[1];

       $data['start_month'] = $this->month_cnt($start);
       $data['end_month'] = $this->month_cnt($end);

       $data['total_count_month'] = $this->weeks($month,$year);

               
      $assigned_task=$this->Common_mdl->getallrecords('add_new_task');
      $user_id = $this->session->userdata('id');
      $high = 0;
      $low = 0;
      $medium = 0;
      $super_urgent = 0;
      $tot_tim[] = '';
      foreach ($assigned_task as $task_key => $task_value) {
        $workers = $task_value['worker'];
        $ex_worker = explode(',', $workers);
        if(in_array($user_id, $ex_worker))
        {
          $tot_tim[] = $task_value['timer_status'];
          $task_status = $task_value['priority'];
          if($task_status=='high'){
            $high++;
          }
          else if($task_status=='low'){
            $low++;
          }
          else if($task_status=='medium'){
            $medium++;
          }
          else if($task_status=='super_urgent')
          {
            $super_urgent++;
          }
        }
      }

  /** for 09-08-2018 task timer **/
  $individual_timer=$this->db->query("select * from individual_task_timer where user_id=".$_SESSION['id']." ")->result_array();
$pause_val='on';
$pause='on';
$for_total_time=0;
if(count($individual_timer)>0){
foreach ($individual_timer as $intime_key => $intime_value) {
     $task_id=$intime_value['task_id'];
  $task_data=$this->db->query("select * from add_new_task where id=".$task_id." ")->row_array();
  if(!empty($task_data)){
  $its_time=$intime_value['time_start_pause'];
  $res=explode(',', $its_time);
    $res1=array_chunk($res,2);
  $result_value=array();
//  $pause='on';
  foreach($res1 as $rre_key => $rre_value)
  {
     $abc=$rre_value;
     if(count($abc)>1){
     if($abc[1]!='')
     {
        $ret_val=$this->calculate_test($abc[0],$abc[1]);
        array_push($result_value, $ret_val) ;
     }
     else
     {
      $pause='';
      $pause_val='';
        $ret_val=$this->calculate_test($abc[0],time());
         array_push($result_value, $ret_val) ;
     }
    }
    else
    {
      $pause='';
      $pause_val='';
        $ret_val=$this->calculate_test($abc[0],time());
         array_push($result_value, $ret_val) ;
    }
  }
  // $time_tot=0;
   foreach ($result_value as $re_key => $re_value) {
      //$time_tot+=time_to_sec($re_value) ;
      $for_total_time+=$this->time_to_sec($re_value) ;
   }

} // task check
} // for
} // if

    //  $tot_time = array_sum($tot_tim);
      $tot_time=$for_total_time;
      $task_cnt = count($tot_tim);
      $data['high'] = $high;
      $data['low'] = $low;
      $data['medium'] = $medium;
      $data['super_urgent'] = $super_urgent;
      $data['tot_tim'] = $tot_time;
      $data['task_cnt'] = $task_cnt;

       //$data['user_activity']=$this->Common_mdl->GetAllWithWhere('activity_log','user_id',$id);
       $data['user_activity']=$this->Common_mdl->orderby('activity_log','user_id',$id,'desc');
      
//echo $this->db->last_query();die;
      /* for($i=1; $i<=$data['total_count_month'];$i++)
       {
        echo $i;
       }die;
*/
        $data['task_list']=$this->Common_mdl->getallrecords('add_new_task');
        //$data['getCompany']=$this->db->query('select * from user where firm_admin_id = '.$id.' and role = 4')->result_array();
        $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = ".$id." and role = 4 and crm_name !=''")->row_array();

        $us_id =  explode(',', $sql['us_id']);





        $data['getCompany'] = $this->db->query( "SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and reminder_block=0")->result_array();
        /*  echo '<pre>';
          print_r($data['getCompany']);die;*/
        $con_stat = 'on';
        $acc_stat = 'on';
        $c_tax_stat = 'on';
        $p_tax_stat = 'on';
        $payroll_stat = 'on';
        $workplace_stat = 'on';
        $vat_stat = 'on';
        $cis_stat = 'on';
        $cissub_stat = 'on';
        $p11d_stat = 'on';
        $bookkeep_stat = 'on';
        $management_stat = 'on';
        $investgate_stat = 'on';
        $registered_stat = 'on';
        $taxadvice_stat = 'on';
        $taxinvest_stat = 'on';

          $ids = array();
        foreach ($data['getCompany'] as $getCompanykey => $getCompanyvalue) {
          $con = (isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnCst =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnCst = '';

          $acc = (isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnAcc =  json_decode($getCompanyvalue['accounts'])->tab : $jsnAcc = '';

          $c_tax = (isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsntax =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsntax = '';

          $p_tax = (isset(json_decode($getCompanyvalue['personal_tax_return'])->tab) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnp_tax =  json_decode($getCompanyvalue['personal_tax_return'])->tab : $jsnp_tax = '';

          $payroll = (isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpay =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpay = '';

          $workplace = (isset(json_decode($getCompanyvalue['workplace'])->tab) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->tab : $jsnworkplace = '';

          $vat = (isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = '';

          $cis = (isset(json_decode($getCompanyvalue['cis'])->tab) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->tab : $jsncis = '';

          $cissub = (isset(json_decode($getCompanyvalue['cissub'])->tab) && $getCompanyvalue['cissub'] != '') ? $jsncis_sub =  json_decode($getCompanyvalue['cissub'])->tab : $jsncis_sub = '';

          $p11d = (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = '';

          $bookkeep = (isset(json_decode($getCompanyvalue['bookkeep'])->tab) && $getCompanyvalue['bookkeep'] != '') ? $jsnbk =  json_decode($getCompanyvalue['bookkeep'])->tab : $jsnbk = '';

          $management = (isset(json_decode($getCompanyvalue['management'])->tab) && $getCompanyvalue['management'] != '') ? $jsnmgnt =  json_decode($getCompanyvalue['management'])->tab : $jsnmgnt = '';

          $investgate = (isset(json_decode($getCompanyvalue['investgate'])->tab) && $getCompanyvalue['investgate'] != '') ? $jsninvest =  json_decode($getCompanyvalue['investgate'])->tab : $jsninvest = '';

          $registered = (isset(json_decode($getCompanyvalue['registered'])->tab) && $getCompanyvalue['registered'] != '') ? $jsnreg =  json_decode($getCompanyvalue['registered'])->tab : $jsnreg = '';

          $taxadvice =(isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxad =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxad = '';

          $taxinvest = (isset(json_decode($getCompanyvalue['taxinvest'])->tab) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->tab : $jsntaxinvest = '';


          if($con==$con_stat && $acc==$acc_stat && $c_tax==$c_tax_stat && $p_tax==$p_tax_stat && $payroll==$payroll_stat && $workplace==$workplace_stat && $vat==$vat_stat && $cis==$cis_stat && $cissub==$cissub_stat && $p11d==$p11d_stat && $bookkeep==$bookkeep_stat && $management==$management_stat && $investgate==$investgate_stat && $registered==$registered_stat && $taxadvice==$taxadvice_stat && $taxinvest==$taxinvest_stat)
          {
            $ids[] = $getCompanyvalue['id'];
          }
        }

             $data['getCompany'] = $this->db->query( "SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and id in (  '" . implode( "','", $ids ) . "' )")->result_array();


  $data['getCompanyss'] = $this->db->query( "SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !=''")->result_array();

   $data['getCompany'] = $this->db->query( "SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' ")->result_array();
  $user_id=$this->session->userdata['userId'];
  $data['custom_form']=$this->Common_mdl->select_record('staff_custom_form','user_id',$user_id);


       $us_id = $this->session->userdata('id');
      // echo $id;
        // $user_id = $this->session->userdata('id');
        // $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = ".$id." and role = 4 and crm_name !=''")->row_array();
        //  echo $this->db->last_query();
        // $us_id =  explode(',', $sql['us_id']);
        $current_date=date("Y-m-d");
        $time=date("Y-m-d");
        $data['today'] = $this->db->query( "SELECT * FROM client WHERE user_id ='".$us_id."' and crm_company_name !='' and ((crm_confirmation_statement_due_date= '".$current_date."') or (crm_ch_accounts_next_due = '".$current_date."') or (crm_accounts_tax_date_hmrc = '".$current_date."' ) or (crm_personal_due_date_return = '".$current_date."' ) or (crm_vat_due_date = '".$current_date."' ) or (crm_rti_deadline = '".$current_date."')  or (crm_pension_subm_due_date = '".$current_date."') or (crm_next_p11d_return_due = '".$current_date."')  or (crm_next_manage_acc_date = '".$current_date."') or (crm_next_booking_date = '".$current_date."' ) or (crm_insurance_renew_date = '".$current_date."')  or (crm_registered_renew_date = '".$current_date."' ) or (crm_investigation_end_date = '".$current_date."' )) and reminder_block=0")->result_array();

      //  echo $this->db->last_query();
        $oneweek_end_date = date('Y-m-d', strtotime($time . ' +1 week'));
          //echo $end_date;
        $data['oneweek'] = $this->db->query( "SELECT * FROM client WHERE user_id ='".$us_id."' and crm_company_name !='' and ((crm_confirmation_statement_due_date BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_ch_accounts_next_due BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_personal_due_date_return BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_vat_due_date BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_rti_deadline BETWEEN '".$current_date."' AND '".$oneweek_end_date."' )  or (crm_pension_subm_due_date BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_next_p11d_return_due BETWEEN '".$current_date."' AND '".$oneweek_end_date."' )  or (crm_next_manage_acc_date BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_next_booking_date BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_insurance_renew_date BETWEEN '".$current_date."' AND '".$oneweek_end_date."' )  or (crm_registered_renew_date BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_investigation_end_date BETWEEN '".$current_date."' AND '".$oneweek_end_date."' )) and reminder_block=0")->result_array();
        $onemonth_end_date = date('Y-m-d', strtotime($time . ' +1 month'));
        $data['onemonth'] = $this->db->query( "SELECT * FROM client WHERE user_id ='".$us_id."' and crm_company_name !='' and ((crm_confirmation_statement_due_date BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_ch_accounts_next_due BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_personal_due_date_return BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_vat_due_date BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_rti_deadline BETWEEN '".$current_date."' AND '".$onemonth_end_date."' )  or (crm_pension_subm_due_date BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_next_p11d_return_due BETWEEN '".$current_date."' AND '".$onemonth_end_date."' )  or (crm_next_manage_acc_date BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_next_booking_date BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_insurance_renew_date BETWEEN '".$current_date."' AND '".$onemonth_end_date."' )  or (crm_registered_renew_date BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_investigation_end_date BETWEEN '".$current_date."' AND '".$onemonth_end_date."' )) and reminder_block=0 ")->result_array();
        $oneyear_end_date = date('Y-m-d', strtotime($time . ' +1 year'));
        $data['oneyear'] = $this->db->query( "SELECT * FROM client WHERE user_id ='".$us_id."' and crm_company_name !='' and ((crm_confirmation_statement_due_date BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_ch_accounts_next_due BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_personal_due_date_return BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_vat_due_date BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_rti_deadline BETWEEN '".$current_date."' AND '".$oneyear_end_date."' )  or (crm_pension_subm_due_date BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_next_p11d_return_due BETWEEN '".$current_date."' AND '".$oneyear_end_date."' )  or (crm_next_manage_acc_date BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_next_booking_date BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_insurance_renew_date BETWEEN '".$current_date."' AND '".$oneyear_end_date."' )  or (crm_registered_renew_date BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_investigation_end_date BETWEEN '".$current_date."' AND '".$oneyear_end_date."' )) and reminder_block=0 ")->result_array();
      

        $last_month_start = date("Y-m-01");
        $last_mon_end = date("Y-m-t");  

         $data['confirmation'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_confirmation_statement_due_date between "'.$last_month_start.'" AND "'.$last_mon_end.'")) and reminder_block=0')->row_array();
        $data['account'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_ch_accounts_next_due between "'.$last_month_start.'" AND "'.$last_mon_end.'")) and reminder_block=0')->row_array();
        $data['company_tax'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_accounts_tax_date_hmrc between "'.$last_month_start.'" AND "'.$last_mon_end.'")) and reminder_block=0')->row_array();
        $data['personal_due'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id ='.$us_id.' and crm_company_name !="" and ((crm_personal_due_date_return between "'.$last_month_start.'" AND "'.$last_mon_end.'")) and reminder_block=0')->row_array();
        $data['vat'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_vat_due_date between "'.$last_month_start.'" AND "'.$last_mon_end.'")) and reminder_block=0')->row_array();
        $data['payroll'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_rti_deadline between "'.$last_month_start.'" AND "'.$last_mon_end.'")) and reminder_block=0')->row_array();
        $data['pension'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_pension_subm_due_date between "'.$last_month_start.'" AND "'.$last_mon_end.'")) and reminder_block=0')->row_array();
        $data['plld'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_next_p11d_return_due between "'.$last_month_start.'" AND "'.$last_mon_end.'")) and reminder_block=0')->row_array();
        $data['management'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_next_manage_acc_date between "'.$last_month_start.'" AND "'.$last_mon_end.'")) and reminder_block=0')->row_array();
        $data['booking'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_next_booking_date between "'.$last_month_start.'" AND "'.$last_mon_end.'")) and reminder_block=0')->row_array();
        $data['insurance'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_insurance_renew_date between "'.$last_month_start.'" AND "'.$last_mon_end.'")) and reminder_block=0')->row_array();
        $data['registeration'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_registered_renew_date between "'.$last_month_start.'" AND "'.$last_mon_end.'")) and reminder_block=0')->row_array();
        $data['investigation'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_investigation_end_date between "'.$last_month_start.'" AND "'.$last_mon_end.'")) and reminder_block=0')->row_array(); 

        $data['todo_list']=$this->db->query("select * from `todos_list` where status='active'  and user_id='".$_SESSION['id']."' order by order_number Asc")->result_array();

        $data['todo_completedlist']=$this->db->query("select * from `todos_list` where status='completed'  and user_id='".$_SESSION['id']."' order by order_number Asc")->result_array();

        $data['todolist']=$this->db->query("select * from `todos_list` order by order_number Asc")->result_array();

        $last_month_start = date("Y-m-d");
        $last_month_end = date("Y-m-t"); 



            $data['task_list']=$this->db->query("SELECT * FROM `add_new_task` WHERE  create_by ='".$_SESSION['id']."' order by id desc Limit 0,5")->result_array(); 
       // }
        


        $last_month_start = date("Y-m-01");
        $last_mon_end = date("Y-m-t");  
         
            $data['tasks']=$this->db->query('select count(*) as task_count from add_new_task
            where create_by='.$_SESSION['id'].'')->row_array();
            $data['closed_tasks']=$this->db->query('select count(*) as task_count from add_new_task where create_by='.$_SESSION['id'].' AND task_status="5"')->row_array();
            $data['pending_tasks']=$this->db->query('select count(*) as task_count from add_new_task where create_by='.$_SESSION['id'].' AND task_status="2"')->row_array();
            $data['cancelled_tasks']=$this->db->query('select count(*) as task_count from add_new_task where create_by='.$_SESSION['id'].' AND task_status="1"')->row_array();                
       // }



       $this->load->view('staffs/staff_profile_update',$data); 
    }

    function start_end_date($date)
    {
       $search_form = date('Y-m-01',strtotime($date));
       $search_to =  date('Y-m-t',strtotime($date));
       return $search_form.','.$search_to;
    }

    function month_cnt($date)
    {
      $week_num =  date('W', strtotime($date));
      return $week_num;
    }

    function weeks($month, $year)
    {
        $num_of_days = date("t", mktime(0,0,0,$month,1,$year)); 
        $lastday = date("t", mktime(0, 0, 0, $month, 1, $year)); 
        $no_of_weeks = 0; 
        $count_weeks = 0; 
        while($no_of_weeks < $lastday){ 
            $no_of_weeks += 7; 
            $count_weeks++; 
        } 
    return $count_weeks;
   }

  function calculate_test($a,$b)
  {
    $difference = $b-$a;

    $second = 1;
    $minute = 60*$second;
    $hour   = 60*$minute;

    $ans["hour"]   = floor(($difference)/$hour);
    $ans["minute"] = floor((($difference)%$hour)/$minute);
    $ans["second"] = floor(((($difference)%$hour)%$minute)/$second);
    //echo  $ans["hour"] . " hours, "  . $ans["minute"] . " minutes, " . $ans["second"] . " seconds";

    $test=$ans["hour"].":".$ans["minute"].":".$ans["second"];
    return $test;
  }

  function time_to_sec($time) 
  {
    list($h, $m, $s) = explode (":", $time);
    $seconds = 0;
    $seconds += (intval($h) * 3600);
    $seconds += (intval($m) * 60);
    $seconds += (intval($s));
    return $seconds;
    }

    function get_filter($person,$condtion) {
    return $person['autosave_status'] != 1 && $person['status'] == $condtion;
    }

    public function task_class_value()
    {


          // print_r($_POST);
          // exit;
          // $data['tab_name']=='task';
          // $Assigned_Task = Get_Assigned_Datas('TASK');
         // $assigned_task = implode(',',$Assigned_Task);
         //print_r($_POST['assigned_task']);
          $new =[];

          $assigned_task=json_decode($_POST['assigned_task'],true);

          $assigned_task_filter = $_POST['assigned_task_filter'];

          if($assigned_task_filter!='none'){
            $today_date = date('Y-m-d');
            $task_due_date = date ( 'Y-m-d' , strtotime ( $today_date . '+'. $assigned_task_filter .' days' ));
          }

          // print_r($assigned_task);
          if($assigned_task_filter=='none')
          {
            $sql_query = 'SELECT * FROM `add_new_task` WHERE   related_to != "sub_task" AND firm_id = "'.$_SESSION['firm_id'].'"  AND  id in ('.$assigned_task.') ORDER BY end_date DESC'; 
          }else if($assigned_task_filter=='1'){
            $sql_query = 'SELECT * FROM `add_new_task` WHERE   related_to != "sub_task" AND end_date="'.$today_date.'" AND firm_id = "'.$_SESSION['firm_id'].'" AND  id in ('.$assigned_task.')';
          }else{
            $sql_query = 'SELECT * FROM `add_new_task` WHERE   related_to != "sub_task" AND end_date>="'.$today_date.'" AND end_date<="'.$task_due_date.'" AND firm_id = "'.$_SESSION['firm_id'].'" AND  id in ('.$assigned_task.') ORDER BY end_date DESC';
          }

          $get_datalists=$this->db->query($sql_query)->result_array();
           
         
          // exit;
        //  print_r($get_datalists);
        //  exit;

          $get_datalist=array_filter(array_column($get_datalists, 'task_status'),'is_numeric');
          $get_datalist=array_count_values($get_datalist);
      
          $data['tasks']['task_count']=array_sum($get_datalist);
          $data['closed_tasks']['task_count']=isset($get_datalist[5])?$get_datalist[5]:'0';
          $data['pending_tasks']['task_count']=isset($get_datalist[2])?$get_datalist[2]:'0';
          $data['cancelled_tasks']['task_count']=isset($get_datalist[1])?$get_datalist[1]:'0';
        $new = array_filter($get_datalists, function ($var) {
          return ($var['task_status'] == '2');
            });
            $data['task_list']=array_values($new);
         //   print_r($data['task_list']);

          // foreach ($data['task_list'] as $key => $value) {

          //   if($key > 9){
          //     unset($data['task_list'][$key]);
          //   }
          //   # code...
          // }
         // print_r($data['task_list']);exit;


        //  $data['task_list']=$this->db->query('SELECT * FROM `add_new_task` WHERE task_status="1" AND related_to != "sub_task" AND firm_id = "'.$_SESSION['firm_id'].'" AND FIND_IN_SET(id,"'.$assigned_task.'") ORDER BY id desc Limit 0,15')->result_array();  


          $task_view['task_class_details']=$this->load->view('firm_dashboard/ajax_tab_details_view',$data,TRUE);
          echo json_encode($task_view);
    }

    public function todolist_class_value()
    {

          $data['todo_list']=$this->db->query("SELECT * from `todos_list` where status='active'  and user_id='".$_SESSION['id']."' order by order_number DESC LIMIT 5")->result_array();

          $data['todo_completedlist']=$this->db->query("SELECT * from `todos_list` where status='completed'  and user_id='".$_SESSION['id']."' order by order_number DESC LIMIT 5")->result_array();
          $data['todolist']=json_decode($_POST['todolist'],true);  

          $task_view['task_class_details']=$this->load->view('firm_dashboard/ajax_tab_to_do_list_view',$data,TRUE);
          echo json_encode($task_view);
    }


    public function client_class_value()
    {
           // $data['tab_name']=='Client';
           //  $Assigned_Client = Get_Assigned_Datas('CLIENT'); 

            $assigned_client = $_POST['assigned_client'];


           //  $cli_arr=$this ->Common_mdl->getall_wherein_records('client','id',$assigned_client);

           //  $arr=array_column($cli_arr,'user_id');

            $clients_user_ids =$_POST['client_userid'];
            //print_r($clients_user_ids);


            $data['invoice']=$this->Invoice_model->selectInvoiceByType('','','','client_email', $clients_user_ids);  
         //  $data['invoice']=$invoice;

           $invoice_status=array_count_values(array_filter(array_column( $data['invoice'], 'payment_status')));




                  
            $data['approved_invoice']=isset($invoice_status['approve'])?$invoice_status['approve']:'0';
            $data['cancel_invoice'] =isset($invoice_status['decline'])?$invoice_status['decline']:'0';
            $data['unpaid_invoice'] =isset($invoice_status['pending'])?$invoice_status['pending']:'0';
          



            $last_month_start = date("Y-m-01");
            $last_month_end = date("Y-m-t"); 
            $last_month_end = date('Y-m-d', strtotime($last_month_end . ' +1 day'));   

            $data['client']=$this->db->query('SELECT user.id,user.status  FROM user LEFT JOIN client ON user.id = client.user_id where user.user_type = "FC" AND user.autosave_status!="1" and user.crm_name!="" and user.firm_id = "'.$_SESSION['firm_id'].'"  AND FIND_IN_SET(client.id,"'.$assigned_client.'") ORDER BY user.id desc')->result_array();

            
            $client_status=array_count_values(array_filter(array_column( $data['client'], 'status'),'is_numeric'));
            $inactive_client=0;

            // print_r(array_column( $data['client'], 'status'));
            // print_r($client_status);


            if(isset($client_status['0']) && isset($client_status['2']))
            {
               $inactive_client=$client_status['0']+$client_status['2'];

            }else if(isset($client_status['0']) && !isset($client_status['2']))
            {
                $inactive_client=$client_status['0'];
            }
            else if((!isset($client_status['0']) && isset($client_status['2'])))
            {
              $inactive_client=$client_status['2'];
            }

            $data['active_client']=isset($client_status['1'])?$client_status['1']:'0';
            $data['inactive_client'] = $inactive_client;
            $data['frozen_client'] =isset($client_status['3'])?$client_status['3']:'0';
          




            // print_r($data_client);
            // exit;

            // $data['inactive_client']=$this->db->query('SELECT count(*) as client_count FROM user LEFT JOIN client ON user.id = client.user_id where user.user_type = "FC" AND user.autosave_status!="1" and user.crm_name!="" and user.firm_id = "'.$_SESSION['firm_id'].'" and (user.status="2" or user.status="0") AND FIND_IN_SET(client.id,"'.$assigned_client.'") ORDER BY user.id DESC')->row_array();

            // $data['frozen_client']=$this->db->query('SELECT count(*) as client_count FROM user LEFT JOIN client ON user.id = client.user_id where user.user_type = "FC" AND user.autosave_status!="1" and user.crm_name!="" and user.firm_id = "'.$_SESSION['firm_id'].'" and user.status="3" AND FIND_IN_SET(client.id,"'.$assigned_client.'") ORDER BY user.id DESC')->row_array();

              $results=$this->db->query('SELECT user.id,user.status  FROM user LEFT JOIN client ON user.id = client.user_id where user.user_type = "FC" AND user.autosave_status!="1" and user.crm_name!="" and user.firm_id = "'.$_SESSION['firm_id'].'"  AND FIND_IN_SET(client.id,"'.$assigned_client.'") and from_unixtime(user.CreatedTime) BETWEEN "'.$last_month_start.'" AND "'.$last_month_end.'" ORDER BY user.id desc')->result_array();

               $client_date_status=array_count_values(array_filter(array_column(  $results, 'status'),'is_numeric'));
            $inactive_clients=0;

            if(isset($client_date_status['0']) && isset($client_date_status['2']))
            {
               $inactive_clients=$client_date_status['0']+$client_date_status['2'];

            }else if(isset($client_date_status['0']) && !isset($client_date_status['2']))
            {
                $inactive_clients=$client_date_status['0'];
            }
            else if((!isset($client_date_status['0']) && isset($client_date_status['2'])))
            {
              $inactive_clients=$client_date_status['2'];
            }




            $data['new_client']=count($results);
            $data['active'] = isset($client_date_status['1'])?$client_date_status['1']:'0';
            $data['inactive'] = $inactive_clients;
            $data['frozen'] = isset($client_date_status['3'])?$client_date_status['3']:'0';    

             


          $client_view['client_class_details']=$this->load->view('firm_dashboard/ajax_client_tab_details_view',$data,TRUE);
          echo json_encode($client_view);
    }
        public function  deadline_class_value()
        {
           //$this->output->enable_profiler(TRUE);
         //   $starttime = microtime(true);
            // $Assigned_Client = Get_Assigned_Datas('CLIENT'); 
            // $assigned_client = implode(',',$Assigned_Client);


            //  $cli_arr=$this ->Common_mdl->getall_wherein_records('client','id',$assigned_client);

            //  $arr=array_filter(array_column($cli_arr,'user_id'));

            $clients_user_ids =$_POST['client_userid'];

                  $con = date('Y-m-d')." AND ".date('Y-m-d', strtotime(date('Y-m-d') . ' +1 year'));
 $data['oneyear'] = $this->db->query( "SELECT * FROM client WHERE (firm_id='".$_SESSION['firm_id']."') and FIND_IN_SET('user_id','".$clients_user_ids."') and ((crm_confirmation_statement_due_date BETWEEN ".$con." ) or (crm_ch_accounts_next_due BETWEEN ".$con." ) or (crm_accounts_tax_date_hmrc BETWEEN ".$con." ) or (crm_personal_due_date_return BETWEEN ".$con." ) or (crm_vat_due_date BETWEEN ".$con." ) or (crm_rti_deadline BETWEEN ".$con." )  or (crm_pension_subm_due_date BETWEEN ".$con." ) or (crm_next_p11d_return_due BETWEEN ".$con." )  or (crm_next_manage_acc_date BETWEEN ".$con." ) or (crm_next_booking_date BETWEEN ".$con." ) or (crm_insurance_renew_date BETWEEN ".$con." )  or (crm_registered_renew_date BETWEEN ".$con." ) or (crm_investigation_end_date BETWEEN ".$con." )) ORDER BY id DESC")->result_array();            
            

              
           $deadline_view['deadline_class_details']=$this->load->view('firm_dashboard/ajax_deadline_tab_details_view',$data,TRUE);


         //    $endtime = microtime(true);
           //print_r($task_view );
         // //  exit;
        //  $task_view['deadline_class_details']=array();
          echo json_encode($deadline_view);
    }


           public function  reminder_class_value()
        {

            $clients_user_ids =$_POST['client_userid'];
            // $Assigned_Client = Get_Assigned_Datas('CLIENT'); 
            // $assigned_client = implode(',',$Assigned_Client);

            // $cli_arr=$this ->Common_mdl->getall_wherein_records('client','id',$assigned_client);

            // $arr=array_filter(array_column($cli_arr,'user_id'));


            $getCompany = $this->db->query("SELECT * FROM client WHERE (FIND_IN_SET(user_id,'".$clients_user_ids."') AND firm_id = '".$_SESSION['firm_id']."') and crm_company_name !='' ORDER BY id DESC")->result_array();
      
              $data['service_list'] = $this->Common_mdl->getServicelist();  
              $data['service_charges'] = $this->Common_mdl->getClientsServiceCharge($getCompany); 

           $reminder_view['reminder_class_details']=
        $this->load->view('Deadline_manager/reminder_invoice_table',$data,TRUE);

          echo json_encode($reminder_view);
    }





  }

?>
