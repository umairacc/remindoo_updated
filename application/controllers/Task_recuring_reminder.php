<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Task_recuring_reminder extends CI_Controller {
public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct(){
        parent::__construct();
         $this->load->model(array('Common_mdl','Security_model','Invoice_model','Task_invoice_model','Task_recuring_model'));
        $this->load->helper('download');
    }

     public function index()
    {
      //$this->Task_recuring_model->for_recuring_sample(1650);

      $delete_firm=array();
      
      $Current_Time = date('h:i a');

      $Check_End = " ( `sche_ends`= 'noend' OR ( `sche_ends`= 'on' AND sche_on_date >= ".date('Y-m-d')." ) OR (`sche_ends`= 'after' AND `sche_after_msg` > `recuring_task_count` ) )";  
      $query = 'select * from add_new_task where recuring_start_time!="" AND sche_send_time = "'.$Current_Time.'" AND '.$Check_End;
      echo $query;
      $delete_firm=$this->Common_mdl->Get_Deleted_FirmsIds();
    	$data = $this->db->query( $query )->result_array();
    	foreach ( $data as $key => $value )
      {
        echo "<br/>Delete firm == ".$delete_firm;

        if(!in_array($value['firm_id'], $delete_firm)){
        $start = date_create_from_format('Y-m-d' , $value['recuring_start_time'] ); 

        if( ($value['sche_repeats ']='day' || $value['sche_repeats ']='month' || $value['sche_repeats ']='year') && $value['recuring_start_time'] == date('Y-m-d') )
        {
          $this->Task_recuring_model->recuring_sample_task( $value['id'] );

          $data['recuring_start_time'] = date("Y-m-d", strtotime("+".trim($value['sche_every_week'])." ".trim($value['sche_repeats'])."s") );

          $this->db->update( 'add_new_task',$datas,"id = ".$value['id']);
        }        
        else if($value['sche_repeats']='week' && date_format($start , 'w') == date('w') )
        {          
          $this->Task_recuring_model->Recurring_Task_WeekDaysWise( $value );
        }
        //$this->Task_recuring_model->for_recuring_sample($value['id']);
    	}
    }

  }
    
    public function reminder()
    { 

      $delete_firm=array();
       $delete_firm=$this->Common_mdl->Get_Deleted_FirmsIds();

      $date=date('Y-m-d h:i A') ;   
      $dateTime = new DateTime($date); 
      $time=$dateTime->format('U'); 


        $test=$this->db->query('select * from add_new_task where for_reminder_chk_box!="" and FIND_IN_SET("'.$time.'", for_reminder_chk_box) ')->result_array();

        //print_r($test);
        // exit;

        foreach ($test as $key => $value)
        {
           if(!in_array($value['firm_id'], $delete_firm)){
        //$this->Task_recuring_model->for_reminder_sample($value['id']);
          echo "send";
          $this->Task_invoice_model->task_new_mail($value['id'],'for_reminder');
          }
        }

        /** reminder for calendar reminder **/
           // $this->calendar_notifications();
        /** end of calendar reminder 28-09-2018 **/
    }
    /** calendar reminder functions 28-09-2018 **/
/** 27-09-2018 for calendar section notification  **/
 public function calendar_notifications()
 {
        $db_val=$this->db->query("select * from calender_event_data where create_by=".$_SESSION['id']."  and FIND_IN_SET('email',notification_status)  ")->result_array();
        if(count($db_val)>0)
        {
            foreach ($db_val as $db_key => $db_value) {
                    if(date('Y-m-d',strtotime($db_value['from_date'])) == date('Y-m-d'))
                    {
                        $notification_status=explode(',',$db_value['notification_status']);
                        $notification_timing=explode(',',$db_value['notification_timing']);
                        $notification_notes=explode(',',$db_value['notification_notes']);
                        if(count($notification_status)>0)
                        {
                            foreach ($notification_status as $notsta_key => $notsta_value) {
                                
                                if($notsta_value=='email')
                                {
                                    if(isset($notification_timing[$notsta_key]) && isset($notification_notes[$notsta_key]))
                                    {
                                         $notify_time=isset($notification_timing[$notsta_key])?$notification_timing[$notsta_key]:'';
                                         $notify_notes=isset($notification_notes[$notsta_key])?$notification_notes[$notsta_key]:'';
                                        if(isset($db_value['from_date']) && isset($db_value['from_time']) && $db_value['from_time']!='')
                                        {
                                              $it_time=$db_value['from_time'];
                                              $time = strtotime($it_time);
                                              $round = 5*60;
                                              $rounded = round($time / $round) * $round;
                                              $it_time=date("h:i A", $rounded);
                                              /** for get current date time **/
                                              $dateTime = new DateTime('now', new DateTimeZone(date_default_timezone_get()));   
                                              $zz= $dateTime->format("h:i A"); 
                                            /** end of current date time **/
                                            if($notify_notes!='' && $notify_time!='')
                                            {
                                                if($notify_notes=='hours')
                                                {
                                                    $timestamp = strtotime($it_time) - 60*60*$notify_time;
                                                    $time = date('h:i A', $timestamp);
                                                    if($zz==$time)
                                                    {
                                                        //echo "mail";
                                                        $mail_data['title']=$db_value['event_title'];
                                                    $mail_data['notification_description']=$db_value['notification_description'];
                                                    $mail_data['from_date']=$db_value['from_date']." ".$db_value['from_time'];
                                                    $mail_data['to_date']=$db_value['to_date']." ".$db_value['to_time'];
                                                    $mail_data['add_location']=$db_value['add_location'];
                                                    $body = $this->load->view('calendar/calendar_notify_empty_mail_content.php', $mail_data, TRUE);

    if($db_value['notification_mail']!=''){
        $email_ids=explode(',',$db_value['notification_mail']);
        foreach ($email_ids as $email_key => $email_value) {
          

          firm_settings_send_mail( $_SESSION['firm_id'] , $email_value ,'Calendar Reminder' , $body );

         
        }
      }
                                                    }
                                                }
                                                else
                                                {
                                                   $endTime = strtotime("-".$notify_time." minutes", strtotime('12:09 am'));
                                                    $time = date('h:i A', $endTime);
                                                    if($zz==$time)
                                                    {
                                                        //echo "mail another";
                                                        $mail_data['title']=$db_value['event_title'];
                                                    $mail_data['notification_description']=$db_value['notification_description'];
                                                    $mail_data['from_date']=$db_value['from_date']." ".$db_value['from_time'];
                                                    $mail_data['to_date']=$db_value['to_date']." ".$db_value['to_time'];
                                                    $mail_data['add_location']=$db_value['add_location'];
                                                    $body = $this->load->view('calendar/calendar_notify_empty_mail_content.php', $mail_data, TRUE);

    if($db_value['notification_mail']!=''){
        $email_ids=explode(',',$db_value['notification_mail']);
        foreach ($email_ids as $email_key => $email_value) {
          firm_settings_send_mail( $_SESSION['firm_id'] , $email_value ,'Calendar Reminder' , $body );
        }
      }
                                                    }

                                                }

                                            }
                                            
                                        }
                                        else if(isset($db_value['from_date']))
                                        {
                                              $it_time='10:00 am';
                                              $time = strtotime($it_time);
                                              $round = 5*60;
                                              $rounded = round($time / $round) * $round;
                                              $it_time=date("h:i A", $rounded);
                                              /** for get current date time **/
                                              $dateTime = new DateTime('now', new DateTimeZone(date_default_timezone_get()));   
                                              $zz= $dateTime->format("h:i A"); 
                                            /** end of current date time **/
                                            //echo $notify_notes."---";
                                            if($notify_notes!='' && $notify_time!='')
                                            {
                                                if($notify_notes=='hours')
                                                {
                                                    $timestamp = strtotime($it_time) - 60*60*$notify_time;
                                                    $time = date('h:i A', $timestamp);
                                                    //echo $zz."==".$time."<br>"; 
                                                    if($zz==$time)
                                                    {
                                                        //echo "mail";
                                                        $mail_data['title']=$db_value['event_title'];
                                                    $mail_data['notification_description']=$db_value['notification_description'];
                                                    $mail_data['from_date']=$db_value['from_date']." ".$db_value['from_time'];
                                                    $mail_data['to_date']=$db_value['to_date']." ".$db_value['to_time'];
                                                    $mail_data['add_location']=$db_value['add_location'];
                                                    $body = $this->load->view('calendar/calendar_notify_empty_mail_content.php', $mail_data, TRUE);

    if($db_value['notification_mail']!=''){
        $email_ids=explode(',',$db_value['notification_mail']);
        foreach ($email_ids as $email_key => $email_value) {
          firm_settings_send_mail( $_SESSION['firm_id'] , $email_value ,'Calendar Reminder' , $body );
        }
      }
                                                    }
                                                }
                                                else
                                                {
                                                   $endTime = strtotime("-".$notify_time." minutes", strtotime($it_time));
                                                    $time = date('h:i A', $endTime);
                                                  //  echo $zz."==".$time."<br>"; 
                                                    if($zz==$time)
                                                    {
                                                        //echo "mail another";
                                                    $mail_data['title']=$db_value['event_title'];
                                                    $mail_data['notification_description']=$db_value['notification_description'];
                                                    $mail_data['from_date']=$db_value['from_date']." ".$db_value['from_time'];
                                                    $mail_data['to_date']=$db_value['to_date']." ".$db_value['to_time'];
                                                    $mail_data['add_location']=$db_value['add_location'];
                                                    $body = $this->load->view('calendar/calendar_notify_empty_mail_content.php', $mail_data, TRUE);

    if($db_value['notification_mail']!=''){
        $email_ids=explode(',',$db_value['notification_mail']);
        foreach ($email_ids as $email_key => $email_value) {
         firm_settings_send_mail( $_SESSION['firm_id'] , $email_value ,'Calendar Reminder' , $body );
        }
      }
                                                    }

                                                }

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }   
        }
  }
/** end of 27-09-2018 **/
    /** end of reminder **/

public function test_cron()
{
  $this->db->query("INSERT INTO `test_records`( `value`) VALUES ('testing')");
}   
   
}
