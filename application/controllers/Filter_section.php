<?php

class Filter_section extends CI_Controller 
{
        public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
	  public function __construct()
        {
            parent::__construct();
            $this->load->model(array('Common_mdl','Report_model','Proposal_model','Invoice_model'));
            $this->load->library('Excel');
	  }

        public function getFilterDatas($start,$end,$client)
        {
            $data['client']=$this->Report_model->client_list(); 
            $data['Public']=$this->Report_model->details_list('Public Limited company',$start,$end,$client);
            $data['limited']=$this->Report_model->details_list('Limited Liability Partnership',$start,$end,$client);
            $data['Private']=$this->Report_model->details_list('Private Limited company',$start,$end,$client);  
            $data['Partnership']=$this->Report_model->details_list('Partnership',$start,$end,$client);
            $data['self']=$this->Report_model->details_list('Self Assessment',$start,$end,$client);
            $data['Trust']=$this->Report_model->details_list('Trust',$start,$end,$client);
            $data['Charity']=$this->Report_model->details_list('Charity',$start,$end,$client);
            $data['Other']=$this->Report_model->details_list('Other',$start,$end,$client);

            $data['total_tasks']=$this->Report_model->task_count($start,$end,$client);
            $data['notstarted_tasks']=$this->Report_model->task_detailsWithdate('notstarted',$start,$end,$client);
            $data['completed_tasks']=$this->Report_model->task_detailsWithdate('complete',$start,$end,$client);
            $data['inprogress_tasks']=$this->Report_model->task_detailsWithdate('inprogress',$start,$end,$client);
            $data['awaiting_tasks']=$this->Report_model->task_detailsWithdate('awaiting',$start,$end,$client);
            $data['archive_tasks']=$this->Report_model->task_detailsWithdate('archive',$start,$end,$client); 
            $data['low_tasks']=$this->Report_model->task_pridetails('low',$start,$end,$client);
            $data['high_tasks']=$this->Report_model->task_pridetails('high',$start,$end,$client);
            $data['medium_tasks']=$this->Report_model->task_pridetails('medium',$start,$end,$client);
            $data['super_urgent_tasks']=$this->Report_model->task_pridetails('super_urgent',$start,$end,$client);  
            $data['task_list_this_month']=$this->Report_model->task_this_month('add_new_task',$start,$end,$client);
            $data['completed_task_count']=$this->Report_model->completed_task_count($start,$end,$client);

            $data['proposal_history']=$this->Proposal_model->proposal_details_date('',$start,$end,$client);
            $data['accept_proposal']=$this->Proposal_model->proposal_details_date('accepted',$start,$end,$client);
            $data['indiscussion_proposal']=$this->Proposal_model->proposal_details_date('in discussion',$start,$end,$client);
            $data['sent_proposal']=$this->Proposal_model->proposal_details_date('sent',$start,$end,$client);
            $data['viewed_proposal']=$this->Proposal_model->proposal_details_date('opened',$start,$end,$client);
            $data['declined_proposal']=$this->Proposal_model->proposal_details_date('declined',$start,$end,$client);
            $data['archive_proposal']=$this->Proposal_model->proposal_details_date('archive',$start,$end,$client);
            $data['draft_proposal']=$this->Proposal_model->proposal_details_date('draft',$start,$end,$client);

            $data['conf_statement']=$this->Report_model->detailsWithDate('conf_statement','',$start,$end,$client);
            $data['accounts']=$this->Report_model->detailsWithDate('accounts','',$start,$end,$client);
            $data['company_tax_return']=$this->Report_model->detailsWithDate('company_tax_return','',$start,$end,$client);
            $data['personal_tax_return']=$this->Report_model->detailsWithDate('personal_tax_return','',$start,$end,$client);
            $data['payroll']=$this->Report_model->detailsWithDate('payroll','',$start,$end,$client);
            $data['workplace']=$this->Report_model->detailsWithDate('workplace','',$start,$end,$client);
            $data['vat']=$this->Report_model->detailsWithDate('vat','',$start,$end,$client);
            $data['cis']=$this->Report_model->detailsWithDate('cis','',$start,$end,$client);
            $data['cissub']=$this->Report_model->detailsWithDate('cissub','',$start,$end,$client);
            $data['p11d']=$this->Report_model->detailsWithDate('p11d','',$start,$end,$client);
            $data['bookkeep']=$this->Report_model->detailsWithDate('bookkeep','',$start,$end,$client);
            $data['management']=$this->Report_model->detailsWithDate('management','',$start,$end,$client);
            $data['investgate']=$this->Report_model->detailsWithDate('investgate','',$start,$end,$client);
            $data['registered']=$this->Report_model->detailsWithDate('registered','',$start,$end,$client);
            $data['taxadvice']=$this->Report_model->detailsWithDate('taxadvice','',$start,$end,$client);
            $data['taxinvest']=$this->Report_model->detailsWithDate('taxinvest','',$start,$end,$client);

            $data['de_conf_statement']=$this->Report_model->deadline_detailsWithDate('crm_confirmation_statement_due_date','conf_statement',$start,$end,$client);
            $data['de_accounts']=$this->Report_model->deadline_detailsWithDate('crm_ch_accounts_next_due','accounts',$start,$end,$client);
            $data['de_company_tax_return']=$this->Report_model->deadline_detailsWithDate('crm_accounts_tax_date_hmrc','company_tax_return',$start,$end,$client);
            $data['de_personal_tax_return']=$this->Report_model->deadline_detailsWithDate('crm_personal_due_date_return','personal_tax_return',$start,$end,$client);
            $data['de_payroll']=$this->Report_model->deadline_detailsWithDate('crm_rti_deadline','payroll',$start,$end,$client);
            $data['de_workplace']=$this->Report_model->deadline_detailsWithDate('crm_pension_subm_due_date','workplace',$start,$end,$client);
            $data['de_vat']=$this->Report_model->deadline_detailsWithDate('crm_vat_due_date','vat',$start,$end,$client);
            $data['de_cis']=$this->Report_model->deadline_detailsWithDate('','cis',$start,$end,$client);
            $data['de_cissub']=$this->Report_model->deadline_detailsWithDate('','cissub',$start,$end,$client);
            $data['de_p11d']=$this->Report_model->deadline_detailsWithDate('crm_next_p11d_return_due','p11d',$start,$end,$client);
            $data['de_bookkeep']=$this->Report_model->deadline_detailsWithDate('crm_next_booking_date','bookkeep',$start,$end,$client);
            $data['de_management']=$this->Report_model->deadline_detailsWithDate('crm_next_manage_acc_date','management',$start,$end,$client);
            $data['de_investgate']=$this->Report_model->deadline_detailsWithDate('crm_insurance_renew_date','investgate',$start,$end,$client);
            $data['de_registered']=$this->Report_model->deadline_detailsWithDate('crm_registered_renew_date','registered',$start,$end,$client);
            $data['de_taxadvice']=$this->Report_model->deadline_detailsWithDate('crm_investigation_end_date','taxadvice',$start,$end,$client);
            $data['de_taxinvest']=$this->Report_model->deadline_detailsWithDate('crm_investigation_end_date','taxinvest',$start,$end,$client);

            $data['leads_status']=$this->Common_mdl->getallrecords('leads_status');
            $data['leads_history']=$this->Report_model->leads_history_section($start,$end,$client);
            $data['overall_leads']=$this->Report_model->overallleads_count($start,$end);

            $data['users_count']=$this->Report_model->users_count($start,$end);
            $data['active_user_count']=$this->Report_model->activeusers_count($start,$end);
            $data['inactive_user_count']=$this->Report_model->inactiveusers_count($start,$end);
            $data['frozen_user_count']=$this->Report_model->frozenusers_count($start,$end);  
            
            $data['expired_invoice']=$this->Report_model->selectInvoiceByType('amount_details','payment_status','pending', 'client_email',$start,$end,$client); 
            $data['approved_invoice']=$this->Report_model->selectInvoiceByType('amount_details','payment_status','approve','client_email', $start,$end,$client);
            $data['cancel_invoice']=$this->Report_model->selectInvoiceByType('amount_details','payment_status','decline', 'client_email', $start,$end,$client);
            
            return $data;
        }

	  public function date_wise_filter()
        { 
    		$date = isset($_POST['date'])?$_POST['date']:"";
    		$client = isset($_POST['client'])?$_POST['client']:"";
    		   
            $data = $this->getFilterDatas($date,$date,$client);
		$this->load->view('reports/filter_reports',$data);
	  }


        public function all_filter()
        {
            $data['client']=$this->Report_model->client_list(); 
            $data['Public']=$this->Report_model->details_list('Public Limited company');
            $data['limited']=$this->Report_model->details_list('Limited Liability Partnership');
            $data['Private']=$this->Report_model->details_list('Private Limited company');  
            $data['Partnership']=$this->Report_model->details_list('Partnership');
            $data['self']=$this->Report_model->details_list('Self Assessment');
            $data['Trust']=$this->Report_model->details_list('Trust');
            $data['Charity']=$this->Report_model->details_list('Charity');
            $data['Other']=$this->Report_model->details_list('Other');

            $data['total_tasks']=$this->Report_model->task_count();
            $data['notstarted_tasks']=$this->Report_model->task_details('notstarted');
            $data['completed_tasks']=$this->Report_model->task_details('complete');
            $data['inprogress_tasks']=$this->Report_model->task_details('inprogress');
            $data['awaiting_tasks']=$this->Report_model->task_details('awaiting');
            $data['archive_tasks']=$this->Report_model->task_details('archive'); 
            $data['low_tasks']=$this->Report_model->task_pridetails('low');
            $data['high_tasks']=$this->Report_model->task_pridetails('high');
            $data['medium_tasks']=$this->Report_model->task_pridetails('medium');
            $data['super_urgent_tasks']=$this->Report_model->task_pridetails('super_urgent');            
            $first_day = date("Y-m-01");
            $end_day = date("Y-m-t");   
            $end_day = date('Y-m-d', strtotime($end_day . ' +1 day'));
            $data['task_list_this_month']=$this->Report_model->task_this_month('add_new_task',$first_day,$end_day);
            $data['completed_task_count']=$this->Report_model->completed_task_count();

            $data['proposal_history']=$this->Proposal_model->proposal_details_date('',$first_day,$end_day);
            $data['accept_proposal']=$this->Proposal_model->proposal_details('accepted');
            $data['indiscussion_proposal']=$this->Proposal_model->proposal_details('in discussion');
            $data['sent_proposal']=$this->Proposal_model->proposal_details('sent');
            $data['viewed_proposal']=$this->Proposal_model->proposal_details('opened');
            $data['declined_proposal']=$this->Proposal_model->proposal_details('declined');
            $data['archive_proposal']=$this->Proposal_model->proposal_details('archive');
            $data['draft_proposal']=$this->Proposal_model->proposal_details('draft');

            $data['conf_statement']=$this->Report_model->service_count('conf_statement');
            $data['accounts']=$this->Report_model->service_count('accounts');
            $data['company_tax_return']=$this->Report_model->service_count('company_tax_return');
            $data['personal_tax_return']=$this->Report_model->service_count('personal_tax_return');
            $data['payroll']=$this->Report_model->service_count('payroll');
            $data['workplace']=$this->Report_model->service_count('workplace');
            $data['vat']=$this->Report_model->service_count('vat');
            $data['cis']=$this->Report_model->service_count('cis');
            $data['cissub']=$this->Report_model->service_count('cissub');
            $data['p11d']=$this->Report_model->service_count('p11d');
            $data['bookkeep']=$this->Report_model->service_count('bookkeep');
            $data['management']=$this->Report_model->service_count('management');
            $data['investgate']=$this->Report_model->service_count('investgate');
            $data['registered']=$this->Report_model->service_count('registered');
            $data['taxadvice']=$this->Report_model->service_count('taxadvice');
            $data['taxinvest']=$this->Report_model->service_count('taxinvest');

            $data['de_conf_statement']=$this->Report_model->dead_count('crm_confirmation_statement_due_date','conf_statement');
            $data['de_accounts']=$this->Report_model->dead_count('crm_ch_accounts_next_due','accounts');
            $data['de_company_tax_return']=$this->Report_model->dead_count('crm_accounts_tax_date_hmrc','company_tax_return');
            $data['de_personal_tax_return']=$this->Report_model->dead_count('crm_personal_due_date_return','personal_tax_return');
            $data['de_payroll']=$this->Report_model->dead_count('crm_rti_deadline','payroll');
            $data['de_workplace']=$this->Report_model->dead_count('crm_pension_subm_due_date','workplace');
            $data['de_vat']=$this->Report_model->dead_count('crm_vat_due_date','vat');
            $data['de_cis']=$this->Report_model->dead_count('','cis');
            $data['de_cissub']=$this->Report_model->dead_count('','cissub');
            $data['de_p11d']=$this->Report_model->dead_count('crm_next_p11d_return_due','p11d');
            $data['de_bookkeep']=$this->Report_model->dead_count('crm_next_booking_date','bookkeep');
            $data['de_management']=$this->Report_model->dead_count('crm_next_manage_acc_date','management');
            $data['de_investgate']=$this->Report_model->dead_count('crm_insurance_renew_date','investgate');
            $data['de_registered']=$this->Report_model->dead_count('crm_registered_renew_date','registered');
            $data['de_taxadvice']=$this->Report_model->dead_count('crm_investigation_end_date','taxadvice');
            $data['de_taxinvest']=$this->Report_model->dead_count('crm_investigation_end_date','taxinvest');
            
            $data['leads_status']=$this->Common_mdl->getallrecords('leads_status');
            $data['leads_history']=$this->Report_model->leads_history_section();
            $data['overall_leads']=$this->Report_model->overallleads_count();

            $data['users_count']=$this->Report_model->users_count();
            $data['active_user_count']=$this->Report_model->activeusers_count();
            $data['inactive_user_count']=$this->Report_model->inactiveusers_count();
            $data['frozen_user_count']=$this->Report_model->frozenusers_count();                        

            $clients_user_ids = $this->Report_model->getClientsUserIds();
            $data['approved_invoice'] = $this->Invoice_model->selectInvoiceByType('amount_details','payment_status','approve','client_email', $clients_user_ids);
            $data['cancel_invoice'] = $this->Invoice_model->selectInvoiceByType('amount_details','payment_status','decline', 'client_email', $clients_user_ids);           
            $data['expired_invoice'] = $this->Invoice_model->selectInvoiceByType('amount_details','payment_status','pending', 'client_email', $clients_user_ids); //pending invoice

            $data['pending_invoice_month']=$this->Report_model->selectInvoiceByType('amount_details','payment_status','pending', 'client_email',$first_day,$end_day); 
            $data['approved_invoice_month']=$this->Report_model->selectInvoiceByType('amount_details','payment_status','approve','client_email', $first_day,$end_day);
            $data['cancel_invoice_month']=$this->Report_model->selectInvoiceByType('amount_details','payment_status','decline', 'client_email', $first_day,$end_day);
            
            $this->load->view('reports/filter_all',$data);
        }

        public function week_filter()
        {
           $current_date = date('Y-m-d');
           $week = date('W', strtotime($current_date));
           $year = date('Y', strtotime($current_date));

           $dto = new DateTime();
           $start_date = $dto->setISODate($year, $week, 0)->format('Y-m-d');
           $end_date = $dto->setISODate($year, $week, 6)->format('Y-m-d');
           $end_date = date('Y-m-d', strtotime($end_date . ' +1 day')); 

           $data = $this->getFilterDatas($start_date,$end_date,"");
           $this->load->view('reports/filter_reports',$data);
        }

        public function threeweek_filter()
        {
            $current_date = date('Y-m-d');
            $weeks = strtotime('- 3 week', strtotime( $current_date ));
            $start_Week = date('Y-m-d', $weeks); 
            $addweeks = strtotime('+ 3 week', strtotime( $start_Week ));
            $end_Week = date('Y-m-d', $addweeks); 
            $end_Week = date('Y-m-d', strtotime($end_Week . ' +1 day'));

            $data = $this->getFilterDatas($start_Week,$end_Week,"");
            $this->load->view('reports/filter_reports',$data);
        }

        public function month_filter()
        {
            $month_start = date("Y-m-01");
            $month_end = date("Y-m-t"); 
            $month_end = date('Y-m-d', strtotime($month_end . ' +1 day'));

            $data = $this->getFilterDatas($month_start,$month_end,"");
            $this->load->view('reports/filter_reports',$data);
        }


        public function threemonth_filter()
        {
            $curr_date = date('Y-m-d',strtotime(date('Y-m-d').'+1 day'));
            $start_month = date('Y-m-d',strtotime('- 3 month', strtotime($curr_date))); 

            $data = $this->getFilterDatas($start_month,$curr_date,"");
            $this->load->view('reports/filter_reports',$data);
        }


        public function user_filter(){

            $client=$_POST['client'];
            $user=$_POST['user'];
            $filter=$_POST['filter'];
            $date=$_POST['date'];

            if($date!='0'){

            }else{

            }


            $data['Public']=$this->Report_model->details_list('Public Limited company');
            $data['limited']=$this->Report_model->details_list('Limited Liability Partnership');
            $data['Private']=$this->Report_model->details_list('Private Limited company');  
            $data['Partnership']=$this->Report_model->details_list('Partnership');
            $data['self']=$this->Report_model->details_list('Self Assessment');
            $data['Trust']=$this->Report_model->details_list('Trust');
            $data['Charity']=$this->Report_model->details_list('Charity');
            $data['Other']=$this->Report_model->details_list('Other');
            $data['notstarted_tasks']=$this->Report_model->task_details('notstarted');
            $data['completed_tasks']=$this->Report_model->task_details('complete');
            $data['inprogress_tasks']=$this->Report_model->task_details('inprogress');
            $data['awaiting_tasks']=$this->Report_model->task_details('awaiting');
            $data['testing_tasks']=$this->Report_model->task_details('testing');
            $data['low_tasks']=$this->Report_model->task_pridetails('low');
            $data['high_tasks']=$this->Report_model->task_pridetails('high');
            $data['medium_tasks']=$this->Report_model->task_pridetails('medium');
            $data['super_urgent_tasks']=$this->Report_model->task_pridetails('super_urgent'); 
            $first_day=date("Y-m-01");
            $end_day=date("Y-m-t");      
            $data['task_list_this_month']=$this->Report_model->task_this_month('add_new_task',$first_day,$end_day);
            $data['proposal_history']=$this->Report_model->proposal_details_section();
            $data['accept_proposal']=$this->Report_model->proposal_details('accepted');
            $data['indiscussion_proposal']=$this->Report_model->proposal_details('in discussion');
            $data['sent_proposal']=$this->Report_model->proposal_details('sent');
            $data['viewed_proposal']=$this->Report_model->proposal_details('opened');
            $data['declined_proposal']=$this->Report_model->proposal_details('declined');
            $data['archive_proposal']=$this->Report_model->proposal_details('archive');
            $data['draft_proposal']=$this->Report_model->proposal_details('draft');
            $data['conf_statement']=$this->Report_model->service_count('conf_statement');
            $data['accounts']=$this->Report_model->service_count('accounts');
            $data['company_tax_return']=$this->Report_model->service_count('company_tax_return');
            $data['personal_tax_return']=$this->Report_model->service_count('personal_tax_return');
            $data['payroll']=$this->Report_model->service_count('payroll');
            $data['workplace']=$this->Report_model->service_count('workplace');
            $data['vat']=$this->Report_model->service_count('vat');
            $data['cis']=$this->Report_model->service_count('cis');
            $data['cissub']=$this->Report_model->service_count('cissub');
            $data['p11d']=$this->Report_model->service_count('p11d');
            $data['bookkeep']=$this->Report_model->service_count('bookkeep');
            $data['management']=$this->Report_model->service_count('management');
            $data['investgate']=$this->Report_model->service_count('investgate');
            $data['registered']=$this->Report_model->service_count('registered');
            $data['taxadvice']=$this->Report_model->service_count('taxadvice');
            $data['taxinvest']=$this->Report_model->service_count('taxinvest');
            $data['de_conf_statement']=$this->Report_model->dead_count('crm_confirmation_statement_due_date','conf_statement');
            $data['de_accounts']=$this->Report_model->dead_count('crm_ch_accounts_next_due','accounts');
            $data['de_company_tax_return']=$this->Report_model->dead_count('crm_accounts_tax_date_hmrc','company_tax_return');
            $data['de_personal_tax_return']=$this->Report_model->dead_count('crm_personal_due_date_return','personal_tax_return');
            $data['de_payroll']=$this->Report_model->dead_count('crm_rti_deadline','payroll');
            $data['de_workplace']=$this->Report_model->dead_count('crm_pension_subm_due_date','workplace');
            $data['de_vat']=$this->Report_model->dead_count('crm_vat_due_date','vat');
            $data['de_cis']=$this->Report_model->dead_count('','cis');
            $data['de_cissub']=$this->Report_model->dead_count('','cissub');
            $data['de_p11d']=$this->Report_model->dead_count('crm_next_p11d_return_due','p11d');
            $data['de_bookkeep']=$this->Report_model->dead_count('crm_next_booking_date','bookkeep');
            $data['de_management']=$this->Report_model->dead_count('crm_next_manage_acc_date','management');
            $data['de_investgate']=$this->Report_model->dead_count('crm_insurance_renew_date','investgate');
            $data['de_registered']=$this->Report_model->dead_count('crm_registered_renew_date','registered');
            $data['de_taxadvice']=$this->Report_model->dead_count('crm_investigation_end_date','taxadvice');
            $data['de_taxinvest']=$this->Report_model->dead_count('crm_investigation_end_date','taxinvest');
            $leads_status=$this->Common_mdl->getallrecords('leads_status');
            foreach($leads_status as $status){
                $status_name=$status['status_name'];
                $data[$status_name]=$this->Report_model->lead_details($status['id']);
            }           
            $data['leads_status']=$this->Common_mdl->getallrecords('leads_status');

            $data['leads_history']=$this->Report_model->leads_history_section();
            $data['users_count']=$this->Report_model->users_count();
            $data['active_user_count']=$this->Report_model->activeusers_count();
            $data['inactive_user_count']=$this->Report_model->inactiveusers_count();
            $data['frozen_user_count']=$this->Report_model->frozenusers_count();
            $data['completed_task_count']=$this->Report_model->completed_task_count();
            $data['overall_leads']=$this->Report_model->overallleads_count();
            $data['approved_invoice']=$this->Report_model->oth_count('approve');
            $data['cancel_invoice']=$this->Report_model->oth_count('decline');
            $data['expired_invoice']=$this->Report_model->exp_count();
            $data['staff_form'] = $this->Common_mdl->GetAllWithWheretwo('user','role','6','firm_admin_id',$_SESSION['id']);
            $data['team'] = $this->db->query("select * from team where create_by=".$_SESSION['id']." ")->result_array();
            $data['department']=$this->db->query("select * from department_permission where create_by=".$_SESSION['id']." ")->result_array();
            $this->load->view('reports/filter_success',$data);
        }



          public function client_filter(){



            $data['Public']=$this->Report_model->details_list('Public Limited company');
            $data['limited']=$this->Report_model->details_list('Limited Liability Partnership');
            $data['Private']=$this->Report_model->details_list('Private Limited company');  
            $data['Partnership']=$this->Report_model->details_list('Partnership');
            $data['self']=$this->Report_model->details_list('Self Assessment');
            $data['Trust']=$this->Report_model->details_list('Trust');
            $data['Charity']=$this->Report_model->details_list('Charity');
            $data['Other']=$this->Report_model->details_list('Other');
            $data['notstarted_tasks']=$this->Report_model->task_details('notstarted');
            $data['completed_tasks']=$this->Report_model->task_details('complete');
            $data['inprogress_tasks']=$this->Report_model->task_details('inprogress');
            $data['awaiting_tasks']=$this->Report_model->task_details('awaiting');
            $data['testing_tasks']=$this->Report_model->task_details('testing');
            $data['low_tasks']=$this->Report_model->task_pridetails('low');
            $data['high_tasks']=$this->Report_model->task_pridetails('high');
            $data['medium_tasks']=$this->Report_model->task_pridetails('medium');
            $data['super_urgent_tasks']=$this->Report_model->task_pridetails('super_urgent'); 
            $first_day=date("Y-m-01");
            $end_day=date("Y-m-t");      
            $data['task_list_this_month']=$this->Report_model->task_this_month('add_new_task',$first_day,$end_day);
            $data['proposal_history']=$this->Report_model->proposal_details_section();
            $data['accept_proposal']=$this->Report_model->proposal_details('accepted');
            $data['indiscussion_proposal']=$this->Report_model->proposal_details('in discussion');
            $data['sent_proposal']=$this->Report_model->proposal_details('sent');
            $data['viewed_proposal']=$this->Report_model->proposal_details('opened');
            $data['declined_proposal']=$this->Report_model->proposal_details('declined');
            $data['archive_proposal']=$this->Report_model->proposal_details('archive');
            $data['draft_proposal']=$this->Report_model->proposal_details('draft');
            $data['conf_statement']=$this->Report_model->service_count('conf_statement');
            $data['accounts']=$this->Report_model->service_count('accounts');
            $data['company_tax_return']=$this->Report_model->service_count('company_tax_return');
            $data['personal_tax_return']=$this->Report_model->service_count('personal_tax_return');
            $data['payroll']=$this->Report_model->service_count('payroll');
            $data['workplace']=$this->Report_model->service_count('workplace');
            $data['vat']=$this->Report_model->service_count('vat');
            $data['cis']=$this->Report_model->service_count('cis');
            $data['cissub']=$this->Report_model->service_count('cissub');
            $data['p11d']=$this->Report_model->service_count('p11d');
            $data['bookkeep']=$this->Report_model->service_count('bookkeep');
            $data['management']=$this->Report_model->service_count('management');
            $data['investgate']=$this->Report_model->service_count('investgate');
            $data['registered']=$this->Report_model->service_count('registered');
            $data['taxadvice']=$this->Report_model->service_count('taxadvice');
            $data['taxinvest']=$this->Report_model->service_count('taxinvest');
            $data['de_conf_statement']=$this->Report_model->dead_count('crm_confirmation_statement_due_date','conf_statement');
            $data['de_accounts']=$this->Report_model->dead_count('crm_ch_accounts_next_due','accounts');
            $data['de_company_tax_return']=$this->Report_model->dead_count('crm_accounts_tax_date_hmrc','company_tax_return');
            $data['de_personal_tax_return']=$this->Report_model->dead_count('crm_personal_due_date_return','personal_tax_return');
            $data['de_payroll']=$this->Report_model->dead_count('crm_rti_deadline','payroll');
            $data['de_workplace']=$this->Report_model->dead_count('crm_pension_subm_due_date','workplace');
            $data['de_vat']=$this->Report_model->dead_count('crm_vat_due_date','vat');
            $data['de_cis']=$this->Report_model->dead_count('','cis');
            $data['de_cissub']=$this->Report_model->dead_count('','cissub');
            $data['de_p11d']=$this->Report_model->dead_count('crm_next_p11d_return_due','p11d');
            $data['de_bookkeep']=$this->Report_model->dead_count('crm_next_booking_date','bookkeep');
            $data['de_management']=$this->Report_model->dead_count('crm_next_manage_acc_date','management');
            $data['de_investgate']=$this->Report_model->dead_count('crm_insurance_renew_date','investgate');
            $data['de_registered']=$this->Report_model->dead_count('crm_registered_renew_date','registered');
            $data['de_taxadvice']=$this->Report_model->dead_count('crm_investigation_end_date','taxadvice');
            $data['de_taxinvest']=$this->Report_model->dead_count('crm_investigation_end_date','taxinvest');
            $leads_status=$this->Common_mdl->getallrecords('leads_status');
            foreach($leads_status as $status){
                $status_name=$status['status_name'];
                $data[$status_name]=$this->Report_model->lead_details($status['id']);
            }           
            $data['leads_status']=$this->Common_mdl->getallrecords('leads_status');

            $data['leads_history']=$this->Report_model->leads_history_section();
            $data['users_count']=$this->Report_model->users_count();
            $data['active_user_count']=$this->Report_model->activeusers_count();
            $data['inactive_user_count']=$this->Report_model->inactiveusers_count();
            $data['frozen_user_count']=$this->Report_model->frozenusers_count();
            $data['completed_task_count']=$this->Report_model->completed_task_count();
            $data['overall_leads']=$this->Report_model->overallleads_count();
            $data['approved_invoice']=$this->Report_model->oth_count('approve');
            $data['cancel_invoice']=$this->Report_model->oth_count('decline');
            $data['expired_invoice']=$this->Report_model->exp_count();
            $data['staff_form'] = $this->Common_mdl->GetAllWithWheretwo('user','role','6','firm_admin_id',$_SESSION['id']);
            $data['team'] = $this->db->query("select * from team where create_by=".$_SESSION['id']." ")->result_array();
            $data['department']=$this->db->query("select * from department_permission where create_by=".$_SESSION['id']." ")->result_array();
            $this->load->view('reports/filter_success',$data);
        }


} ?>
