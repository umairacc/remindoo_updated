<!DOCTYPE html>
<html>
   <head>
      <style>
         #pdf {
         font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
         border-collapse: collapse;
         width: 100%;
         }
         #pdf td, #pdf th {
         border: 1px solid #ddd;
         padding: 8px;
         }
         #pdf tr:nth-child(even){background-color: #f2f2f2;}
         #pdf td
         {
         font-size: 13px;
         }
         #pdf th {
         padding-top: 12px;
         padding-bottom: 12px;
         text-align: left;
         background-color: #9E9E9E;
         color: white;
         font-size: 14px !important;
         white-space: nowrap;
         }
         p{
         color: #D7CCC8;
         }
         caption{
         color: #555;
         font-size: 22px;
         margin-bottom: 20px;
         }
      </style>
   </head>
   <body>
      <table class="table client_table1 text-center display nowrap printableArea" id="alluser" cellspacing="0" cellpadding="10" width="100%" style="border-collapse: collapse;">
         <thead>
            <tr class="text-uppercase">
               <th style="text-align:left;border: 1px solid #ddd;">
                  S.no
               </th>
               <?php if(isset($column_setting[0]['profile_image'])&&($column_setting[0]['profile_image']==1)){?>
               <th style="text-align:left;border: 1px solid #ddd;">Profile</th>
               <?php } ?>      
               <th style="text-align:left;border: 1px solid #ddd;">Name</th>
               <th style="text-align:left;border: 1px solid #ddd;">Updates</th>
               <th style="text-align:left;border: 1px solid #ddd;">Active</th>
               <th style="text-align:left;border: 1px solid #ddd;">CompanyStatus</th>
               <th style="text-align:left;border: 1px solid #ddd;">User Type</th>
               <th style="text-align:left;border: 1px solid #ddd;">Status</th>
               <th style="text-align:left;border: 1px solid #ddd;" <?php echo (isset($column_setting[0]['crm_company_name'])&&($column_setting[0]['crm_company_name']=='1')) ? '' : 'style="display:none;"'; ?>>Company Name</th>
               <?php 
                  if(isset($column_setting[0]['crm_company_name1'])&&($column_setting[0]['crm_company_name1']=='1')){?>             
               <th style="text-align:left;border: 1px solid #ddd;">Name</th>
               <?php } if(isset($column_setting[0]['crm_legal_form'])&&($column_setting[0]['crm_legal_form']=='1')){?>             
               <th style="text-align:left;border: 1px solid #ddd;">Legal Form</th>
               <?php } if(isset($column_setting[0]['crm_allocation_holder'])&&($column_setting[0]['crm_allocation_holder']=='1')){?>             
               <th style="text-align:left;border: 1px solid #ddd;">Allocation Holder</th>
               <?php } if(isset($column_setting[0]['crm_company_number'])&&($column_setting[0]['crm_company_number']=='1')){?>             
               <th style="text-align:left;border: 1px solid #ddd;">Company Number</th>
               <?php } if(isset($column_setting[0]['crm_company_url'])&&($column_setting[0]['crm_company_url']=='1')){?>             
               <th style="text-align:left;border: 1px solid #ddd;">Company Url</th>
               <?php } if(isset($column_setting[0]['crm_officers_url'])&&($column_setting[0]['crm_officers_url']=='1')){?>             
               <th style="text-align:left;border: 1px solid #ddd;">Officers Url</th>
               <?php } if(isset($column_setting[0]['crm_incorporation_date'])&&($column_setting[0]['crm_incorporation_date']=='1')){?>             
               <th style="text-align:left;border: 1px solid #ddd;">Incorporation Date</th>
               <?php } if(isset($column_setting[0]['crm_register_address'])&&($column_setting[0]['crm_register_address']=='1')){?>             
               <th style="text-align:left;border: 1px solid #ddd;">Register Address</th>
               <?php } if(isset($column_setting[0]['crm_company_type'])&&($column_setting[0]['crm_company_type']=='1')){?> 
               <th style="text-align:left;border: 1px solid #ddd;">Company Type</th>
               <?php } if(isset($column_setting[0]['crm_company_sic'])&&($column_setting[0]['crm_company_sic']=='1')){?> 
               <th style="text-align:left;border: 1px solid #ddd;">Company SIC</th>
               <?php } if(isset($column_setting[0]['crm_company_utr'])&&($column_setting[0]['crm_company_utr']=='1')){?> 
               <th style="text-align:left;border: 1px solid #ddd;">Company UTR</th>
               <?php } if(isset($column_setting[0]['crm_companies_house_authorisation_code'])&&($column_setting[0]['crm_companies_house_authorisation_code']=='1')){?> 
               <th style="text-align:left;border: 1px solid #ddd;">Authentication Code</th>
               <?php } if(isset($column_setting[0]['crm_managed'])&&($column_setting[0]['crm_managed']=='1')){?> 
               <th style="text-align:left;border: 1px solid #ddd;">Managed</th>
               <?php } if(isset($column_setting[0]['crm_tradingas'])&&($column_setting[0]['crm_tradingas']=='1')){?> 
               <th style="text-align:left;border: 1px solid #ddd;">Trading As</th>
               <?php } if(isset($column_setting[0]['crm_commenced_trading'])&&($column_setting[0]['crm_commenced_trading']=='1')){?> 
               <th style="text-align:left;border: 1px solid #ddd;">Commenced Trading</th>
               <?php } if(isset($column_setting[0]['crm_registered_for_sa'])&&($column_setting[0]['crm_registered_for_sa']=='1')){?> 
               <th style="text-align:left;border: 1px solid #ddd;">Registered for SA</th>
               <?php } if(isset($column_setting[0]['crm_business_details_turnover'])&&($column_setting[0]['crm_business_details_turnover']=='1')){?> 
               <th style="text-align:left;border: 1px solid #ddd;">Business Turnover</th>
               <?php } if(isset($column_setting[0]['crm_business_details_nature_of_business'])&&($column_setting[0]['crm_business_details_nature_of_business']=='1')){?> 
               <th style="text-align:left;border: 1px solid #ddd;">Nature of business</th>
               <?php } if(isset($column_setting[0]['crm_person'])&&($column_setting[0]['crm_person']=='1')){?> 
               <th style="text-align:left;border: 1px solid #ddd;">Person</th>
               <?php } if(isset($column_setting[0]['crm_title'])&&($column_setting[0]['crm_title']=='1')){?> 
               <th>Title</th>
               <?php } if(isset($column_setting[0]['crm_first_name'])&&($column_setting[0]['crm_first_name']=='1')){?> 
               <th style="text-align:left;border: 1px solid #ddd;">First Name</th>
               <?php } if(isset($column_setting[0]['crm_middle_name'])&&($column_setting[0]['crm_middle_name']=='1')){?> 
               <th style="text-align:left;border: 1px solid #ddd;">Middle Name</th>
               <?php } if(isset($column_setting[0]['crm_last_name'])&&($column_setting[0]['crm_last_name']=='1')){?> 
               <th style="text-align:left;border: 1px solid #ddd;">Last Name</th>
               <?php } if(isset($column_setting[0]['crm_create_self_assessment_client'])&&($column_setting[0]['crm_create_self_assessment_client']=='1')){?> 
               <th style="text-align:left;border: 1px solid #ddd;">Create Self Assessment Client</th>
               <?php } if(isset($column_setting[0]['crm_client_does_self_assessment'])&&($column_setting[0]['crm_client_does_self_assessment']=='1')){?> 
               <th style="text-align:left;border: 1px solid #ddd;">Client Does Self Assessment</th>
               <?php } if(isset($column_setting[0]['crm_preferred_name'])&&($column_setting[0]['crm_preferred_name']=='1')){?> 
               <th style="text-align:left;border: 1px solid #ddd;">Perferred Name</th>
               <?php } if(isset($column_setting[0]['crm_date_of_birth'])&&($column_setting[0]['crm_date_of_birth']=='1')){?> 
               <th style="text-align:left;border: 1px solid #ddd;">DOB</th>
               <?php } if(isset($column_setting[0]['crm_email'])&&($column_setting[0]['crm_email']=='1')){?> 
               <th style="text-align:left;border: 1px solid #ddd;">Email</th>
               <?php } if(isset($column_setting[0]['crm_postal_address'])&&($column_setting[0]['crm_postal_address']=='1')){?> 
               <th style="text-align:left;border: 1px solid #ddd;">Postal Address</th>
               <?php } if(isset($column_setting[0]['crm_telephone_number'])&&($column_setting[0]['crm_telephone_number']=='1')){?> 
               <th style="text-align:left;border: 1px solid #ddd;">Telephone Number</th>
               <?php } if(isset($column_setting[0]['crm_mobile_number'])&&($column_setting[0]['crm_mobile_number']=='1')){?>   
               <th style="text-align:left;border: 1px solid #ddd;">Mobile Number</th>
               <?php } if(isset($column_setting[0]['crm_ni_number'])&&($column_setting[0]['crm_ni_number']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">NI Number</th>
               <?php } if(isset($column_setting[0]['crm_personal_utr_number'])&&($column_setting[0]['crm_personal_utr_number']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Personal UTR Number</th>
               <?php } if(isset($column_setting[0]['crm_terms_signed'])&&($column_setting[0]['crm_terms_signed']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Terms Signed</th>
               <?php } if(isset($column_setting[0]['crm_photo_id_verified'])&&($column_setting[0]['crm_photo_id_verified']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Photoid Verified</th>
               <?php } if(isset($column_setting[0]['crm_address_verified'])&&($column_setting[0]['crm_address_verified']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Address Verified</th>
               <?php } if(isset($column_setting[0]['crm_previous'])&&($column_setting[0]['crm_previous']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Previous</th>
               <?php } if(isset($column_setting[0]['crm_current'])&&($column_setting[0]['crm_current']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Current</th>
               <?php } if(isset($column_setting[0]['crm_ir_35_notes'])&&($column_setting[0]['crm_ir_35_notes']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Ir35 Notes</th>
               <?php } if(isset($column_setting[0]['crm_previous_accountant'])&&($column_setting[0]['crm_previous_accountant']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Previous Accountant</th>
               <?php } if(isset($column_setting[0]['crm_refered_by'])&&($column_setting[0]['crm_refered_by']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Refered By</th>
               <?php } if(isset($column_setting[0]['crm_allocated_office'])&&($column_setting[0]['crm_allocated_office']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Allocated Office</th>
               <?php } if(isset($column_setting[0]['crm_inital_contact'])&&($column_setting[0]['crm_inital_contact']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Initial Contact</th>
               <?php } if(isset($column_setting[0]['crm_quote_email_sent'])&&($column_setting[0]['crm_quote_email_sent']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Quote Email Sent</th>
               <?php } if(isset($column_setting[0]['crm_welcome_email_sent'])&&($column_setting[0]['crm_welcome_email_sent']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Welcome Email</th>
               <?php } if(isset($column_setting[0]['crm_internal_reference'])&&($column_setting[0]['crm_internal_reference']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Internal Reference</th>
               <?php } if(isset($column_setting[0]['crm_profession'])&&($column_setting[0]['crm_profession']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Profession</th>
               <?php } if(isset($column_setting[0]['crm_website'])&&($column_setting[0]['crm_website']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Website</th>
               <?php } if(isset($column_setting[0]['crm_accounting_system'])&&($column_setting[0]['crm_accounting_system']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Accounting System</th>
               <?php } if(isset($column_setting[0]['crm_notes'])&&($column_setting[0]['crm_notes']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Notes</th>
               <?php } if(isset($column_setting[0]['crm_other_urgent'])&&($column_setting[0]['crm_other_urgent']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">OtherUrgent</th>
               <?php } if(isset($column_setting[0]['crm_accounts'])&&($column_setting[0]['crm_accounts']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Accounts</th>
               <?php } if(isset($column_setting[0]['crm_bookkeeping'])&&($column_setting[0]['crm_bookkeeping']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Bookkeeping</th>
               <?php } if(isset($column_setting[0]['crm_ct600_return'])&&($column_setting[0]['crm_ct600_return']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">CT600 Return</th>
               <?php } if(isset($column_setting[0]['crm_payroll'])&&($column_setting[0]['crm_payroll']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Payroll</th>
               <?php } if(isset($column_setting[0]['crm_auto_enrolment'])&&($column_setting[0]['crm_auto_enrolment']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Auto-Enrollement</th>
               <?php } if(isset($column_setting[0]['crm_vat_returns'])&&($column_setting[0]['crm_vat_returns']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">VAT Returns</th>
               <?php } if(isset($column_setting[0]['crm_confirmation_statement'])&&($column_setting[0]['crm_confirmation_statement']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Confirmation Statement</th>
               <?php } if(isset($column_setting[0]['crm_cis'])&&($column_setting[0]['crm_cis']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">CIS</th>
               <?php } if(isset($column_setting[0]['crm_p11d'])&&($column_setting[0]['crm_p11d']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">P11D</th>
               <?php } if(isset($column_setting[0]['crm_invesitgation_insurance'])&&($column_setting[0]['crm_invesitgation_insurance']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Investigation Insurance</th>
               <?php } if(isset($column_setting[0]['crm_services_registered_address'])&&($column_setting[0]['crm_services_registered_address']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Registered Address</th>
               <?php } if(isset($column_setting[0]['crm_bill_payment'])&&($column_setting[0]['crm_bill_payment']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Bill Payment</th>
               <?php } if(isset($column_setting[0]['crm_advice'])&&($column_setting[0]['crm_advice']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Advice</th>
               <?php } if(isset($column_setting[0]['crm_monthly_charge'])&&($column_setting[0]['crm_monthly_charge']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Monthly Charge</th>
               <?php } if(isset($column_setting[0]['crm_annual_charge'])&&($column_setting[0]['crm_annual_charge']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Annual Charge</th>
               <?php } if(isset($column_setting[0]['crm_accounts_periodend'])&&($column_setting[0]['crm_accounts_periodend']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Accounts Periodend</th>
               <?php } if(isset($column_setting[0]['crm_ch_yearend'])&&($column_setting[0]['crm_ch_yearend']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">CH Year End</th>
               <?php } if(isset($column_setting[0]['crm_hmrc_yearend'])&&($column_setting[0]['crm_hmrc_yearend']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">HMRC Year End</th>
               <?php } if(isset($column_setting[0]['crm_ch_accounts_next_due'])&&($column_setting[0]['crm_ch_accounts_next_due']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">CH Accounts Next Due</th>
               <?php } if(isset($column_setting[0]['crm_ct600_due'])&&($column_setting[0]['crm_ct600_due']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">CT600 Due</th>
               <?php } if(isset($column_setting[0]['crm_companies_house_email_remainder'])&&($column_setting[0]['crm_companies_house_email_remainder']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">CH Email Reminder</th>
               <?php } if(isset($column_setting[0]['crm_latest_action'])&&($column_setting[0]['crm_latest_action']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Latest Action</th>
               <?php } if(isset($column_setting[0]['crm_latest_action_date'])&&($column_setting[0]['crm_latest_action_date']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Latest Action Date</th>
               <?php } if(isset($column_setting[0]['crm_records_received_date'])&&($column_setting[0]['crm_records_received_date']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Records Received</th>
               <?php } if(isset($column_setting[0]['crm_confirmation_statement_date'])&&($column_setting[0]['crm_confirmation_statement_date']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Confirmation Statement Date</th>
               <?php } if(isset($column_setting[0]['crm_confirmation_statement_due_date'])&&($column_setting[0]['crm_confirmation_statement_due_date']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Confirmation Statement Due</th>
               <?php } if(isset($column_setting[0]['crm_confirmation_latest_action'])&&($column_setting[0]['crm_confirmation_latest_action']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">CS Latest Action</th>
               <?php } if(isset($column_setting[0]['crm_confirmation_latest_action_date'])&&($column_setting[0]['crm_confirmation_latest_action_date']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">CS Latest Action Date</th>
               <?php } if(isset($column_setting[0]['crm_confirmation_records_received_date'])&&($column_setting[0]['crm_confirmation_records_received_date']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">CS Records Received</th>
               <?php } if(isset($column_setting[0]['crm_confirmation_officers'])&&($column_setting[0]['crm_confirmation_officers']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">CS Officers</th>
               <?php } if(isset($column_setting[0]['crm_share_capital'])&&($column_setting[0]['crm_share_capital']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">CS Share Capital</th>
               <?php } if(isset($column_setting[0]['crm_shareholders'])&&($column_setting[0]['crm_shareholders']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">CS Shareholders</th>
               <?php } if(isset($column_setting[0]['crm_people_with_significant_control'])&&($column_setting[0]['crm_people_with_significant_control']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">CS People with Significant Control</th>
               <?php } if(isset($column_setting[0]['crm_vat_quarters'])&&($column_setting[0]['crm_vat_quarters']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">VAT Quarters</th>
               <?php } if(isset($column_setting[0]['crm_vat_quater_end_date'])&&($column_setting[0]['crm_vat_quater_end_date']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">VAT Quarter End</th>
               <?php } if(isset($column_setting[0]['crm_next_return_due_date'])&&($column_setting[0]['crm_next_return_due_date']=='1')){?>               
               <th  style="text-align:left;border: 1px solid #ddd;">VAT Next Return Date</th>
               <?php } if(isset($column_setting[0]['crm_vat_latest_action'])&&($column_setting[0]['crm_vat_latest_action']=='1')){?>               
               <th  style="text-align:left;border: 1px solid #ddd;">VAT Latest Action</th>
               <?php } if(isset($column_setting[0]['crm_vat_latest_action_date'])&&($column_setting[0]['crm_vat_latest_action_date']=='1')){?>               
               <th  style="text-align:left;border: 1px solid #ddd;">VAT Latest Action Date</th>
               <?php } if(isset($column_setting[0]['crm_vat_records_received_date'])&&($column_setting[0]['crm_vat_records_received_date']=='1')){?>               
               <th  style="text-align:left;border: 1px solid #ddd;">VAT Records Received</th>
               <?php } if(isset($column_setting[0]['crm_vat_member_state'])&&($column_setting[0]['crm_vat_member_state']=='1')){?>               
               <th  style="text-align:left;border: 1px solid #ddd;">VAT Member State</th>
               <?php } if(isset($column_setting[0]['crm_vat_number'])&&($column_setting[0]['crm_vat_number']=='1')){?>               
               <th  style="text-align:left;border: 1px solid #ddd;">VAT Number</th>
               <?php } if(isset($column_setting[0]['crm_vat_date_of_registration'])&&($column_setting[0]['crm_vat_date_of_registration']=='1')){?>               
               <th  style="text-align:left;border: 1px solid #ddd;">VAT Date of Registration</th>
               <?php } if(isset($column_setting[0]['crm_vat_effective_date'])&&($column_setting[0]['crm_vat_effective_date']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">VAT Effective Date</th>
               <?php } if(isset($column_setting[0]['crm_vat_estimated_turnover'])&&($column_setting[0]['crm_vat_estimated_turnover']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Estimated Turnover</th>
               <?php } if(isset($column_setting[0]['crm_transfer_of_going_concern'])&&($column_setting[0]['crm_transfer_of_going_concern']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Transfer of Going Concern</th>
               <?php } if(isset($column_setting[0]['crm_involved_in_any_other_business'])&&($column_setting[0]['crm_involved_in_any_other_business']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Involved in Any Other Business</th>
               <?php } if(isset($column_setting[0]['crm_flat_rate'])&&($column_setting[0]['crm_flat_rate']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Flat Rate</th>
               <?php } if(isset($column_setting[0]['crm_direct_debit'])&&($column_setting[0]['crm_direct_debit']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Direct Debit</th>
               <?php } if(isset($column_setting[0]['crm_annual_accounting_scheme'])&&($column_setting[0]['crm_annual_accounting_scheme']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Annual Accounting Scheme</th>
               <?php } if(isset($column_setting[0]['crm_flat_rate_category'])&&($column_setting[0]['crm_flat_rate_category']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Flat Rate Category</th>
               <?php } if(isset($column_setting[0]['crm_month_of_last_quarter_submitted'])&&($column_setting[0]['crm_month_of_last_quarter_submitted']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Month of Last Quarter Submitted</th>
               <?php } if(isset($column_setting[0]['crm_box5_of_last_quarter_submitted'])&&($column_setting[0]['crm_box5_of_last_quarter_submitted']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Box 5 of Last Quarter Submitted</th>
               <?php } if(isset($column_setting[0]['crm_vat_address'])&&($column_setting[0]['crm_vat_address']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">VAT Address</th>
               <?php } if(isset($column_setting[0]['crm_vat_notes'])&&($column_setting[0]['crm_vat_notes']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">VAT Notes</th>
               <?php } if(isset($column_setting[0]['crm_employers_reference'])&&($column_setting[0]['crm_employers_reference']==1)){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Employers Reference</th>
               <?php } if(isset($column_setting[0]['crm_accounts_office_reference'])&&($column_setting[0]['crm_accounts_office_reference']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Accounts Office Reference</th>
               <?php } if(isset($column_setting[0]['crm_years_required'])&&($column_setting[0]['crm_years_required']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Years Required</th>
               <?php } if(isset($column_setting[0]['crm_annual_or_monthly_submissions'])&&($column_setting[0]['crm_annual_or_monthly_submissions']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;"Annual/Monthly Submissions</th>
               <?php } if(isset($column_setting[0]['crm_irregular_montly_pay'])&&($column_setting[0]['crm_irregular_montly_pay']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Irregular Monthly Pay</th>
               <?php } if(isset($column_setting[0]['crm_nil_eps'])&&($column_setting[0]['crm_nil_eps']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Nil EPS</th>
               <?php } if(isset($column_setting[0]['crm_no_of_employees'])&&($column_setting[0]['crm_no_of_employees']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Number of Employees</th>
               <?php } if(isset($column_setting[0]['crm_first_pay_date'])&&($column_setting[0]['crm_first_pay_date']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">First Pay Date</th>
               <?php } if(isset($column_setting[0]['crm_rti_deadline'])&&($column_setting[0]['crm_rti_deadline']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">RTI Deadline</th>
               <?php } if(isset($column_setting[0]['crm_paye_scheme_ceased'])&&($column_setting[0]['crm_paye_scheme_ceased']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">PAYE Scheme Ceased</th>
               <?php } if(isset($column_setting[0]['crm_paye_latest_action'])&&($column_setting[0]['crm_paye_latest_action']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">PAYE Latest Action</th>
               <?php } if(isset($column_setting[0]['crm_paye_latest_action_date'])&&($column_setting[0]['crm_paye_latest_action_date']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">PAYE Latest Action Date</th>
               <?php } if(isset($column_setting[0]['crm_paye_records_received'])&&($column_setting[0]['crm_paye_records_received']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">PAYE Records Received</th>
               <?php } if(isset($column_setting[0]['crm_next_p11d_return_due'])&&($column_setting[0]['crm_next_p11d_return_due']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Next P11D Return Due</th>
               <?php } if(isset($column_setting[0]['crm_latest_p11d_submitted'])&&($column_setting[0]['crm_latest_p11d_submitted']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Latest P11D Submitted</th>
               <?php } if(isset($column_setting[0]['crm_p11d_latest_action'])&&($column_setting[0]['crm_p11d_latest_action']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">P11D Latest Action</th>
               <?php } if(isset($column_setting[0]['crm_p11d_latest_action_date'])&&($column_setting[0]['crm_p11d_latest_action_date']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">P11D Latest Action Date</th>
               <?php } if(isset($column_setting[0]['crm_p11d_records_received'])&&($column_setting[0]['crm_p11d_records_received']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">P11D Records Received</th>
               <?php } if(isset($column_setting[0]['crm_cis_contractor'])&&($column_setting[0]['crm_cis_contractor']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">CIS Contractor</th>
               <?php } if(isset($column_setting[0]['crm_cis_subcontractor'])&&($column_setting[0]['crm_cis_subcontractor']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">CIS Subcontractor</th>
               <?php } if(isset($column_setting[0]['crm_cis_deadline'])&&($column_setting[0]['crm_cis_deadline']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">CIS Deadline</th>
               <?php } if(isset($column_setting[0]['crm_auto_enrolment_latest_action'])&&($column_setting[0]['crm_auto_enrolment_latest_action']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Auto-Enrollement Latest Action</th>
               <?php } if(isset($column_setting[0]['crm_auto_enrolment_latest_action_date'])&&($column_setting[0]['crm_auto_enrolment_latest_action_date']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Auto-Enrollement Latest Action Date</th>
               <?php } if(isset($column_setting[0]['crm_auto_enrolment_records_received'])&&($column_setting[0]['crm_auto_enrolment_records_received']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Auto-Enrollement Record Received</th>
               <?php } if(isset($column_setting[0]['crm_auto_enrolment_staging'])&&($column_setting[0]['crm_auto_enrolment_staging']=='1')){?>               
               <th>Auto-Enrollement Staging</th>
               <?php } if(isset($column_setting[0]['crm_postponement_date'])&&($column_setting[0]['crm_postponement_date']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Postponement Date</th>
               <?php } if(isset($column_setting[0]['crm_the_pensions_regulator_opt_out_date'])&&($column_setting[0]['crm_the_pensions_regulator_opt_out_date']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">The Pensions Regulator Opt Out Date</th>
               <?php } if(isset($column_setting[0]['crm_re_enrolment_date'])&&($column_setting[0]['crm_re_enrolment_date']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Re-Enrolment Date</th>
               <?php } if(isset($column_setting[0]['crm_pension_provider'])&&($column_setting[0]['crm_pension_provider']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Pension Provider</th>
               <?php } if(isset($column_setting[0]['crm_pension_id'])&&($column_setting[0]['crm_pension_id']=='1')){?>               
               <th>Pension ID</th>
               <?php } if(isset($column_setting[0]['crm_declaration_of_compliance_due_date'])&&($column_setting[0]['crm_declaration_of_compliance_due_date']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Declaration of Compliance Due</th>
               <?php } if(isset($column_setting[0]['crm_declaration_of_compliance_submission'])&&($column_setting[0]['crm_declaration_of_compliance_submission']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Declaration of Compliance Submission</th>
               <?php } if(isset($column_setting[0]['crm_pension_deadline'])&&($column_setting[0]['crm_pension_deadline']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Pension Deadline</th>
               <?php } if(isset($column_setting[0]['crm_note'])&&($column_setting[0]['crm_note']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">PAYE Notes</th>
               <?php } if(isset($column_setting[0]['crm_registration_fee_paid'])&&($column_setting[0]['crm_registration_fee_paid']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Registration Fee Paid</th>
               <?php } if(isset($column_setting[0]['crm_64_8_registration'])&&($column_setting[0]['crm_64_8_registration']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">64-8 Registration</th>
               <?php } if(isset($column_setting[0]['crm_packages'])&&($column_setting[0]['crm_packages']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Packages</th>
               <?php } if(isset($column_setting[0]['crm_gender'])&&($column_setting[0]['crm_gender']=='1')){?>               
               <th style="text-align:left;border: 1px solid #ddd;">Gender</th>
               <?php } if(isset($column_setting[0]['crm_letter_sign'])&&($column_setting[0]['crm_letter_sign']=='1')){?> 
               <th style="text-align:left;border: 1px solid #ddd;">Letter sign</th>
               <?php } if(isset($column_setting[0]['crm_business_website'])&&($column_setting[0]['crm_business_website']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Business website</th>
               <?php } if(isset($column_setting[0]['crm_paye_ref_number'])&&($column_setting[0]['crm_paye_ref_number']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Paye ref no</th>
               <?php } if(isset($column_setting[0]['select_responsible_type'])&&($column_setting[0]['select_responsible_type']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Responsible type</th>
               <?php } if(isset($column_setting[0]['crm_assign_manager_reviewer'])&&($column_setting[0]['crm_assign_manager_reviewer']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Manager reviewer</th>
               <?php } if(isset($column_setting[0]['crm_assign_managed'])&&($column_setting[0]['crm_assign_managed']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Assign managed</th>
               <?php } if(isset($column_setting[0]['crm_assign_client_id_verified'])&&($column_setting[0]['crm_assign_client_id_verified']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Client id verfied</th>
               <?php } if(isset($column_setting[0]['crm_assign_type_of_id'])&&($column_setting[0]['crm_assign_type_of_id']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Assign type of id</th>
               <?php } if(isset($column_setting[0]['crm_assign_proof_of_address'])&&($column_setting[0]['crm_assign_proof_of_address']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Assign proof address</th>
               <?php } if(isset($column_setting[0]['crm_assign_meeting_client'])&&($column_setting[0]['crm_assign_meeting_client']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Assign meeting client</th>
               <?php } if(isset($column_setting[0]['crm_assign_source'])&&($column_setting[0]['crm_assign_source']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Assign Source</th>
               <?php } if(isset($column_setting[0]['crm_assign_relationship_client'])&&($column_setting[0]['crm_assign_relationship_client']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Assign relation client</th>
               <?php } if(isset($column_setting[0]['crm_assign_notes'])&&($column_setting[0]['crm_assign_notes']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Assign notes</th>
               <?php } if(isset($column_setting[0]['crm_other_name_of_firm'])&&($column_setting[0]['crm_other_name_of_firm']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Other name firm</th>
               <?php } if(isset($column_setting[0]['crm_other_address'])&&($column_setting[0]['crm_other_address']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Other address</th>
               <?php } if(isset($column_setting[0]['crm_other_contact_no'])&&($column_setting[0]['crm_other_contact_no']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Other contact no</th>
               <?php } if(isset($column_setting[0]['crm_other_email'])&&($column_setting[0]['crm_other_email']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Other email</th>
               <?php } if(isset($column_setting[0]['crm_other_chase_for_info'])&&($column_setting[0]['crm_other_chase_for_info']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">other info</th>
               <?php } if(isset($column_setting[0]['crm_other_notes'])&&($column_setting[0]['crm_other_notes']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">other notes</th>
               <?php } if(isset($column_setting[0]['crm_other_internal_notes'])&&($column_setting[0]['crm_other_internal_notes']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">other internal notes</th>
               <?php } if(isset($column_setting[0]['crm_other_invite_use'])&&($column_setting[0]['crm_other_invite_use']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">other invite use</th>
               <?php } if(isset($column_setting[0]['crm_other_crm'])&&($column_setting[0]['crm_other_crm']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">other crm</th>
               <?php } if(isset($column_setting[0]['crm_other_proposal'])&&($column_setting[0]['crm_other_proposal']=='1')){?>
               <th>other proposal</th>
               <?php } if(isset($column_setting[0]['crm_other_task'])&&($column_setting[0]['crm_other_task']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">other task</th>
               <?php } if(isset($column_setting[0]['crm_other_send_invit_link'])&&($column_setting[0]['crm_other_send_invit_link']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">other invitlink</th>
               <?php } if(isset($column_setting[0]['crm_other_username'])&&($column_setting[0]['crm_other_username']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">other username</th>
               <?php } if(isset($column_setting[0]['crm_other_any_notes'])&&($column_setting[0]['crm_other_any_notes']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">other any notes</th>
               <?php } if(isset($column_setting[0]['crm_confirmation_auth_code'])&&($column_setting[0]['crm_confirmation_auth_code']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Confimation auth code</th>
               <?php } if(isset($column_setting[0]['crm_accounts_auth_code'])&&($column_setting[0]['crm_accounts_auth_code']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Accounts auth code</th>
               <?php } if(isset($column_setting[0]['crm_accounts_utr_number'])&&($column_setting[0]['crm_accounts_utr_number']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Accounts utr no</th>
               <?php } if(isset($column_setting[0]['crm_accounts_due_date_hmrc'])&&($column_setting[0]['crm_accounts_due_date_hmrc']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Accounts due date</th>
               <?php } if(isset($column_setting[0]['crm_accounts_tax_date_hmrc'])&&($column_setting[0]['crm_accounts_tax_date_hmrc']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Accounts tax date</th>
               <?php } if(isset($column_setting[0]['crm_accounts_notes'])&&($column_setting[0]['crm_accounts_notes']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Accounts notes</th>
               <?php } if(isset($column_setting[0]['crm_property_income'])&&($column_setting[0]['crm_property_income']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Property income</th>
               <?php } if(isset($column_setting[0]['crm_additional_income'])&&($column_setting[0]['crm_additional_income']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Additional income</th>
               <?php } if(isset($column_setting[0]['crm_personal_tax_return_date'])&&($column_setting[0]['crm_personal_tax_return_date']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Personal tax return date</th>
               <?php } if(isset($column_setting[0]['crm_personal_due_date_return'])&&($column_setting[0]['crm_personal_due_date_return']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;"> Personal duedate return</th>
               <?php } if(isset($column_setting[0]['crm_personal_due_date_online'])&&($column_setting[0]['crm_personal_due_date_online']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Personal due online</th>
               <?php } if(isset($column_setting[0]['crm_personal_notes'])&&($column_setting[0]['crm_personal_notes']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Personal notes</th>
               <?php } if(isset($column_setting[0]['crm_payroll_acco_off_ref_no'])&&($column_setting[0]['crm_payroll_acco_off_ref_no']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">payroll account ref no</th>
               <?php } if(isset($column_setting[0]['crm_paye_off_ref_no'])&&($column_setting[0]['crm_paye_off_ref_no']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">paye ref no</th>
               <?php } if(isset($column_setting[0]['crm_payroll_reg_date'])&&($column_setting[0]['crm_payroll_reg_date']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">payroll reg date</th>
               <?php } if(isset($column_setting[0]['crm_payroll_run'])&&($column_setting[0]['crm_payroll_run']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">payroll run</th>
               <?php } if(isset($column_setting[0]['crm_payroll_run_date'])&&($column_setting[0]['crm_payroll_run_date']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">payroll run date</th>
               <?php } if(isset($column_setting[0]['crm_previous_year_require'])&&($column_setting[0]['crm_previous_year_require']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">previous year require</th>
               <?php } if(isset($column_setting[0]['crm_payroll_if_yes'])&&($column_setting[0]['crm_payroll_if_yes']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">payroll if yes</th>
               <?php } if(isset($column_setting[0]['crm_staging_date'])&&($column_setting[0]['crm_staging_date']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Staging date</th>
               <?php } if(isset($column_setting[0]['crm_pension_subm_due_date'])&&($column_setting[0]['crm_pension_subm_due_date']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Pension sub duedate</th>
               <?php } if(isset($column_setting[0]['crm_employer_contri_percentage'])&&($column_setting[0]['crm_employer_contri_percentage']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Employer percentage</th>
               <?php } if(isset($column_setting[0]['crm_employee_contri_percentage'])&&($column_setting[0]['crm_employee_contri_percentage']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Employee percentage</th>
               <?php } if(isset($column_setting[0]['crm_pension_notes'])&&($column_setting[0]['crm_pension_notes']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Pension notes</th>
               <?php } if(isset($column_setting[0]['crm_cis_contractor_start_date'])&&($column_setting[0]['crm_cis_contractor_start_date']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Cis start date</th>
               <?php } if(isset($column_setting[0]['crm_cis_scheme_notes'])&&($column_setting[0]['crm_cis_scheme_notes']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">CIS scheme notes</th>
               <?php } if(isset($column_setting[0]['crm_cis_subcontractor_start_date'])&&($column_setting[0]['crm_cis_subcontractor_start_date']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">CIS sub start date</th>
               <?php } if(isset($column_setting[0]['crm_cis_subcontractor_scheme_notes'])&&($column_setting[0]['crm_cis_subcontractor_scheme_notes']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">CIS sub scheme notes</th>
               <?php } if(isset($column_setting[0]['crm_p11d_previous_year_require'])&&($column_setting[0]['crm_p11d_previous_year_require']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">p11d previous year require</th>
               <?php } if(isset($column_setting[0]['crm_p11d_payroll_if_yes'])&&($column_setting[0]['crm_p11d_payroll_if_yes']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">P11d payroll if yes</th>
               <?php } if(isset($column_setting[0]['crm_vat_frequency'])&&($column_setting[0]['crm_vat_frequency']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Vat Frequency</th>
               <?php } if(isset($column_setting[0]['crm_vat_due_date'])&&($column_setting[0]['crm_vat_due_date']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Vat due date</th>
               <?php } if(isset($column_setting[0]['crm_vat_scheme'])&&($column_setting[0]['crm_vat_scheme']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Vat scheme</th>
               <?php } if(isset($column_setting[0]['crm_flat_rate_percentage'])&&($column_setting[0]['crm_flat_rate_percentage']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Flat rate percentage</th>
               <?php } if(isset($column_setting[0]['crm_next_booking_date'])&&($column_setting[0]['crm_next_booking_date']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Next booking date</th>
               <?php } if(isset($column_setting[0]['crm_method_bookkeeping'])&&($column_setting[0]['crm_method_bookkeeping']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Method bookkeeping</th>
               <?php } if(isset($column_setting[0]['crm_client_provide_record'])&&($column_setting[0]['crm_client_provide_record']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">client provide record</th>
               <?php } if(isset($column_setting[0]['crm_manage_acc_fre'])&&($column_setting[0]['crm_manage_acc_fre']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Manage accounts frequency</th>
               <?php } if(isset($column_setting[0]['crm_next_manage_acc_date'])&&($column_setting[0]['crm_next_manage_acc_date']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Next manage account date</th>
               <?php } if(isset($column_setting[0]['crm_manage_method_bookkeeping'])&&($column_setting[0]['crm_manage_method_bookkeeping']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Method of bookkeeping</th>
               <?php } if(isset($column_setting[0]['crm_manage_client_provide_record'])&&($column_setting[0]['crm_manage_client_provide_record']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">client provide record</th>
               <?php } if(isset($column_setting[0]['crm_manage_notes'])&&($column_setting[0]['crm_manage_notes']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Manage notes</th>
               <?php } if(isset($column_setting[0]['crm_insurance_renew_date'])&&($column_setting[0]['crm_insurance_renew_date']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Insurance Renew Date</th>
               <?php } if(isset($column_setting[0]['crm_insurance_provider'])&&($column_setting[0]['crm_insurance_provider']=='1')){?>
               <th>Insurance provider</th>
               <?php } if(isset($column_setting[0]['crm_claims_note'])&&($column_setting[0]['crm_claims_note']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Claims notes</th>
               <?php } if(isset($column_setting[0]['crm_registered_start_date'])&&($column_setting[0]['crm_registered_start_date']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Registered Start date</th>
               <?php } if(isset($column_setting[0]['crm_registered_renew_date'])&&($column_setting[0]['crm_registered_renew_date']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Registered renew date</th>
               <?php } if(isset($column_setting[0]['crm_registered_office_inuse'])&&($column_setting[0]['crm_registered_office_inuse']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Registered office inuse</th>
               <?php } if(isset($column_setting[0]['crm_registered_claims_note'])&&($column_setting[0]['crm_registered_claims_note']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Registered claims note</th>
               <?php } if(isset($column_setting[0]['crm_investigation_start_date'])&&($column_setting[0]['crm_investigation_start_date']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Investigation start date</th>
               <?php } if(isset($column_setting[0]['crm_investigation_end_date'])&&($column_setting[0]['crm_investigation_end_date']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Investigation end date</th>
               <?php } if(isset($column_setting[0]['crm_investigation_note'])&&($column_setting[0]['crm_investigation_note']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Investigation notes</th>
               <?php } if(isset($column_setting[0]['crm_tax_investigation_note'])&&($column_setting[0]['crm_tax_investigation_note']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">tax Investigation notes</th>
               <?php } if(isset($column_setting[0]['crm_assign_other_custom'])&&($column_setting[0]['crm_assign_other_custom']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Assign other custom</th>
               <?php } if(isset($column_setting[0]['crm_notes_info'])&&($column_setting[0]['crm_notes_info']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Notes info</th>
               <?php } if(isset($column_setting[0]['crm_registered_in'])&&($column_setting[0]['crm_registered_in']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Registered In</th>
               <?php } if(isset($column_setting[0]['crm_address_line_one'])&&($column_setting[0]['crm_address_line_one']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Address line1</th>
               <?php } if(isset($column_setting[0]['crm_address_line_two'])&&($column_setting[0]['crm_address_line_two']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Address line2</th>
               <?php } if(isset($column_setting[0]['crm_address_line_three'])&&($column_setting[0]['crm_address_line_three']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Address line3</th>
               <?php } if(isset($column_setting[0]['crm_town_city'])&&($column_setting[0]['crm_town_city']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Town/city</th>
               <?php } if(isset($column_setting[0]['crm_post_code'])&&($column_setting[0]['crm_post_code']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Post code</th>
               <?php } ?>  
               <?php if(isset($column_setting[0]['contact_title'])&&($column_setting[0]['contact_title']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Title</th>
               <?php } if(isset($column_setting[0]['contact_first_name'])&&($column_setting[0]['contact_first_name']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">First name</th>
               <?php } if(isset($column_setting[0]['contact_middle_name'])&&($column_setting[0]['contact_middle_name']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Middle name</th>
               <?php } if(isset($column_setting[0]['contact_surname'])&&($column_setting[0]['contact_surname']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Surname</th>
               <?php } if(isset($column_setting[0]['contact_preferred_name'])&&($column_setting[0]['contact_preferred_name']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Prefered name</th>
               <?php } if(isset($column_setting[0]['contact_mobile'])&&($column_setting[0]['contact_mobile']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Contact Mobile</th>
               <?php } if(isset($column_setting[0]['contact_main_email'])&&($column_setting[0]['contact_main_email']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Main Email</th>
               <?php } if(isset($column_setting[0]['contact_nationality'])&&($column_setting[0]['contact_nationality']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Nationality</th>
               <?php } if(isset($column_setting[0]['contact_psc'])&&($column_setting[0]['contact_psc']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Psc</th>
               <?php } if(isset($column_setting[0]['contact_shareholder'])&&($column_setting[0]['contact_shareholder']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Shareholder</th>
               <?php } if(isset($column_setting[0]['contact_ni_number'])&&($column_setting[0]['contact_ni_number']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Contact Ni Number</th>
               <?php } if(isset($column_setting[0]['contact_country_of_residence'])&&($column_setting[0]['contact_country_of_residence']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Country of residence</th>
               <?php } if(isset($column_setting[0]['contact_type'])&&($column_setting[0]['contact_type']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Contact type</th>
               <?php } if(isset($column_setting[0]['contact_other_custom'])&&($column_setting[0]['contact_other_custom']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Conact Other custom</th>
               <?php } if(isset($column_setting[0]['contact_address_line1'])&&($column_setting[0]['contact_address_line1']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Address line1</th>
               <?php } if(isset($column_setting[0]['contact_address_line2'])&&($column_setting[0]['contact_address_line2']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Address line2</th>
               <?php } if(isset($column_setting[0]['contact_town_city'])&&($column_setting[0]['contact_town_city']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Town/city</th>
               <?php } if(isset($column_setting[0]['contact_post_code'])&&($column_setting[0]['contact_post_code']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Post code</th>
               <?php } if(isset($column_setting[0]['contact_landline'])&&($column_setting[0]['contact_landline']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Landline</th>
               <?php } if(isset($column_setting[0]['contact_work_email'])&&($column_setting[0]['contact_work_email']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Work mail</th>
               <?php } if(isset($column_setting[0]['contact_date_of_birth'])&&($column_setting[0]['contact_date_of_birth']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Date of birth</th>
               <?php } if(isset($column_setting[0]['contact_nature_of_control'])&&($column_setting[0]['contact_nature_of_control']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Nature of control</th>
               <?php } if(isset($column_setting[0]['contact_marital_status'])&&($column_setting[0]['contact_marital_status']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Marital status</th>
               <?php } if(isset($column_setting[0]['contact_utr_number'])&&($column_setting[0]['contact_utr_number']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Contact Utr no</th>
               <?php } if(isset($column_setting[0]['contact_occupation'])&&($column_setting[0]['contact_occupation']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Occupation</th>
               <?php } if(isset($column_setting[0]['contact_appointed_on'])&&($column_setting[0]['contact_appointed_on']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Appointed on</th>
               <?php } ?>
               <?php if(isset($column_setting[0]['staff_manager'])&&($column_setting[0]['staff_manager']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">staff</th>
               <?php } if(isset($column_setting[0]['staff_managed'])&&($column_setting[0]['staff_managed']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Managed by</th>
               <?php } if(isset($column_setting[0]['team'])&&($column_setting[0]['team']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Team</th>
               <?php } if(isset($column_setting[0]['team_allocation'])&&($column_setting[0]['team_allocation']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Team allocation office</th>
               <?php } if(isset($column_setting[0]['department'])&&($column_setting[0]['department']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Department</th>
               <?php } if(isset($column_setting[0]['department_allocation'])&&($column_setting[0]['department_allocation']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Department allocation office</th>
               <?php } if(isset($column_setting[0]['member_manager'])&&($column_setting[0]['member_manager']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Member Manager</th>
               <?php } if(isset($column_setting[0]['member_managed'])&&($column_setting[0]['member_managed']=='1')){?>
               <th style="text-align:left;border: 1px solid #ddd;">Managed by</th>
               <?php } ?>
            </tr>
         </thead>
         <tbody>
            <?php 
               $s = 1;
               foreach ($getallUser as $getallUserkey => $getallUservalue) {
                 $update=$this->Common_mdl->select_record('update_client','user_id',$getallUservalue['id']);
                 $company_status=$this->Common_mdl->select_record('client','user_id',$getallUservalue['id']);
               
                 $contact=$this->db->query('select * from client_contacts where client_id='.$getallUservalue['id'].' and make_primary=1')->row_array();
               
               $staff=$this->Common_mdl->GetAllWithWhere('responsible_user','client_id',$getallUservalue['id']);
               $team=$this->Common_mdl->GetAllWithWhere('responsible_team','client_id',$getallUservalue['id']);
               $department=$this->Common_mdl->GetAllWithWhere('responsible_department','client_id',$getallUservalue['id']);
               $member=$this->Common_mdl->GetAllWithWhere('responsible_members','client_id',$getallUservalue['id']);
               
                     if($getallUservalue['role']=='6'){
                       //$role = 'Staff';
                       $edit=base_url().'user/view_staff/'.$getallUservalue['id'];
                       $house='Manual';
                     }elseif($getallUservalue['role']=='4' && $company_status['status']==0 ){
                         //$role = 'Client';
                         $edit=base_url().'Client/client_info/'.$getallUservalue['id'];
                         $house='Manual';
                     }elseif($getallUservalue['role']=='4' && $company_status['status']==1){
                         //$role = 'Client';
                         $edit=base_url().'Client/client_info/'.$getallUservalue['id'];
                         $house='Company House';
                     }elseif($getallUservalue['role']=='4' && $company_status['status']==2){
                         //$role = 'Client';
                         $edit=base_url().'Client/client_info/'.$getallUservalue['id'];
                         $house='Import';
                     }else{
                       $house ='';
                       $role = '';
                       $edit='#';
                     }
                     if($getallUservalue['status']=='1'){
                         $status_id = 'checkboxid1';  
                         $chked = 'selected';
                         $status_val = '1';
                         $active='Active';
                     }elseif($getallUservalue['status']=='2' || $getallUservalue['status']=='0' ){
                         $status_id = 'checkboxid2';  
                         $chked = 'selected';
                         $status_val = '2';
                         $active='Inactive';
                     } elseif($getallUservalue['status']=='3'){
                       $status_id = 'checkboxid3';  
                         $chked = 'selected';
                         $status_val = '3';
                         $active='Frozen';
                     }
               
                     
               
                      $role=$this->Common_mdl->select_record('Role','id',$getallUservalue['role']);
               
                 ?>
            <tr id="<?php echo $getallUservalue["id"]; ?>">
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $s;?></td>
               <?php if(isset($column_setting[0]['profile_image'])&&($column_setting[0]['profile_image']==1)){?>
               <td style="text-align:left;border: 1px solid #ddd;" class="user_imgs"><img src="<?php echo base_url().'/uploads/'.$getallUservalue['crm_profile_pic'];?>" alt="img"></td>
               <?php } ?>
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo ucfirst($getallUservalue['crm_name']);?></td>
               <td style="text-align:left;border: 1px solid #ddd;">
                  <?php echo ($update['counts']!='')? ($update['counts']):'0';?>  new
               </td>
               <td style="text-align:left;border: 1px solid #ddd;">
                  <?php if($getallUservalue['status']=='1'){ echo "Active"; } elseif($getallUservalue['status']=='0'){ echo "Inactive"; }elseif($getallUservalue['status']=='3'){ echo "Frozen"; }elseif($getallUservalue['status']=='5'){ echo 'Archive'; }else{ echo '-'; } ?>
               </td>
               <td style="text-align:left;border: 1px solid #ddd;" id="innac"><?php echo ucfirst($company_status['crm_company_status']);?></td>
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $role['role'];?></td>
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $house;?></td>
               <td style="text-align:left;border: 1px solid #ddd;" <?php echo (isset($column_setting[0]['crm_company_name'])&&($column_setting[0]['crm_company_name']=='1')) ? '' : 'style="display:none;"'; ?>><?php echo $company_status['crm_company_name'];?></td>
               <?php 
                  if(isset($column_setting[0]['crm_company_name1'])&&($column_setting[0]['crm_company_name1']=='1')){?>             
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_company_name1'];?></td>
               <?php } if(isset($column_setting[0]['crm_legal_form'])&&($column_setting[0]['crm_legal_form']=='1')){?>             
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_legal_form'];?></td>
               <?php } if(isset($column_setting[0]['crm_allocation_holder'])&&($column_setting[0]['crm_allocation_holder']=='1')){?>             
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_allocation_holder'];?></td>
               <?php } if(isset($column_setting[0]['crm_company_number'])&&($column_setting[0]['crm_company_number']=='1')){?>             
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_company_number'];?></td>
               <?php } if(isset($column_setting[0]['crm_company_url'])&&($column_setting[0]['crm_company_url']=='1')){?>             
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_company_url'];?></td>
               <?php } if(isset($column_setting[0]['crm_officers_url'])&&($column_setting[0]['crm_officers_url']=='1')){?>             
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_officers_url'];?></td>
               <?php } if(isset($column_setting[0]['crm_incorporation_date'])&&($column_setting[0]['crm_incorporation_date']=='1')){?>             
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_incorporation_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_register_address'])&&($column_setting[0]['crm_register_address']=='1')){?>             
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_register_address'];?></td>
               <?php } if(isset($column_setting[0]['crm_company_type'])&&($column_setting[0]['crm_company_type']=='1')){?> 
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_company_type'];?></td>
               <?php } if(isset($column_setting[0]['crm_company_sic'])&&($column_setting[0]['crm_company_sic']=='1')){?> 
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_company_sic'];?></td>
               <?php } if(isset($column_setting[0]['crm_company_utr'])&&($column_setting[0]['crm_company_utr']=='1')){?> 
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_company_utr'];?></td>
               <?php } if(isset($column_setting[0]['crm_companies_house_authorisation_code'])&&($column_setting[0]['crm_companies_house_authorisation_code']=='1')){?> 
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_companies_house_authorisation_code'];?></td>
               <?php } if(isset($column_setting[0]['crm_managed'])&&($column_setting[0]['crm_managed']=='1')){?> 
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_managed'];?></td>
               <?php } if(isset($column_setting[0]['crm_tradingas'])&&($column_setting[0]['crm_tradingas']=='1')){?> 
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_tradingas'];?></td>
               <?php } if(isset($column_setting[0]['crm_commenced_trading'])&&($column_setting[0]['crm_commenced_trading']=='1')){?> 
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_commenced_trading'];?></td>
               <?php } if(isset($column_setting[0]['crm_registered_for_sa'])&&($column_setting[0]['crm_registered_for_sa']=='1')){?> 
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_registered_for_sa'];?></td>
               <?php } if(isset($column_setting[0]['crm_business_details_turnover'])&&($column_setting[0]['crm_business_details_turnover']=='1')){?> 
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_business_details_turnover'];?></td>
               <?php } if(isset($column_setting[0]['crm_business_details_nature_of_business'])&&($column_setting[0]['crm_business_details_nature_of_business']=='1')){?> 
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_business_details_nature_of_business'];?></td>
               <?php } if(isset($column_setting[0]['crm_person'])&&($column_setting[0]['crm_person']=='1')){?> 
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_person'];?></td>
               <?php } if(isset($column_setting[0]['crm_title'])&&($column_setting[0]['crm_title']=='1')){?> 
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_title'];?></td>
               <?php } if(isset($column_setting[0]['crm_first_name'])&&($column_setting[0]['crm_first_name']=='1')){?> 
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_first_name'];?></td>
               <?php } if(isset($column_setting[0]['crm_middle_name'])&&($column_setting[0]['crm_middle_name']=='1')){?> 
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_middle_name'];?></td>
               <?php } if(isset($column_setting[0]['crm_last_name'])&&($column_setting[0]['crm_last_name']=='1')){?> 
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_last_name'];?></td>
               <?php } if(isset($column_setting[0]['crm_create_self_assessment_client'])&&($column_setting[0]['crm_create_self_assessment_client']=='1')){?> 
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_create_self_assessment_client'];?></td>
               <?php } if(isset($column_setting[0]['crm_client_does_self_assessment'])&&($column_setting[0]['crm_client_does_self_assessment']=='1')){?> 
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_client_does_self_assessment'];?></td>
               <?php } if(isset($column_setting[0]['crm_preferred_name'])&&($column_setting[0]['crm_preferred_name']=='1')){?> 
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_preferred_name'];?></td>
               <?php } if(isset($column_setting[0]['crm_date_of_birth'])&&($column_setting[0]['crm_date_of_birth']=='1')){?> 
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_date_of_birth'];?></td>
               <?php } if(isset($column_setting[0]['crm_email'])&&($column_setting[0]['crm_email']=='1')){?> 
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_email'];?></td>
               <?php } if(isset($column_setting[0]['crm_postal_address'])&&($column_setting[0]['crm_postal_address']=='1')){?> 
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_postal_address'];?></td>
               <?php } if(isset($column_setting[0]['crm_telephone_number'])&&($column_setting[0]['crm_telephone_number']=='1')){?> 
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_telephone_number'];?></td>
               <?php } if(isset($column_setting[0]['crm_mobile_number'])&&($column_setting[0]['crm_mobile_number']=='1')){?>   
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_mobile_number'];?></td>
               <?php } if(isset($column_setting[0]['crm_ni_number'])&&($column_setting[0]['crm_ni_number']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_ni_number'];?></td>
               <?php } if(isset($column_setting[0]['crm_personal_utr_number'])&&($column_setting[0]['crm_personal_utr_number']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_personal_utr_number'];?></td>
               <?php } if(isset($column_setting[0]['crm_terms_signed'])&&($column_setting[0]['crm_terms_signed']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_terms_signed'];?></td>
               <?php } if(isset($column_setting[0]['crm_photo_id_verified'])&&($column_setting[0]['crm_photo_id_verified']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_photo_id_verified'];?></td>
               <?php } if(isset($column_setting[0]['crm_address_verified'])&&($column_setting[0]['crm_address_verified']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_address_verified'];?></td>
               <?php } if(isset($column_setting[0]['crm_previous'])&&($column_setting[0]['crm_previous']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_previous'];?></td>
               <?php } if(isset($column_setting[0]['crm_current'])&&($column_setting[0]['crm_current']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_current'];?></td>
               <?php } if(isset($column_setting[0]['crm_ir_35_notes'])&&($column_setting[0]['crm_ir_35_notes']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_ir_35_notes'];?></td>
               <?php } if(isset($column_setting[0]['crm_previous_accountant'])&&($column_setting[0]['crm_previous_accountant']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_previous_accountant'];?></td>
               <?php } if(isset($column_setting[0]['crm_refered_by'])&&($column_setting[0]['crm_refered_by']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_refered_by'];?></td>
               <?php } if(isset($column_setting[0]['crm_allocated_office'])&&($column_setting[0]['crm_allocated_office']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_allocated_office'];?></td>
               <?php } if(isset($column_setting[0]['crm_inital_contact'])&&($column_setting[0]['crm_inital_contact']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_inital_contact'];?></td>
               <?php } if(isset($column_setting[0]['crm_quote_email_sent'])&&($column_setting[0]['crm_quote_email_sent']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_quote_email_sent'];?></td>
               <?php } if(isset($column_setting[0]['crm_welcome_email_sent'])&&($column_setting[0]['crm_welcome_email_sent']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_welcome_email_sent'];?></td>
               <?php } if(isset($column_setting[0]['crm_internal_reference'])&&($column_setting[0]['crm_internal_reference']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_internal_reference'];?></td>
               <?php } if(isset($column_setting[0]['crm_profession'])&&($column_setting[0]['crm_profession']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_profession'];?></td>
               <?php } if(isset($column_setting[0]['crm_website'])&&($column_setting[0]['crm_website']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_website'];?></td>
               <?php } if(isset($column_setting[0]['crm_accounting_system'])&&($column_setting[0]['crm_accounting_system']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_accounting_system'];?></td>
               <?php } if(isset($column_setting[0]['crm_notes'])&&($column_setting[0]['crm_notes']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_notes'];?></td>
               <?php } if(isset($column_setting[0]['crm_other_urgent'])&&($column_setting[0]['crm_other_urgent']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_other_urgent'];?></td>
               <?php } if(isset($column_setting[0]['crm_accounts'])&&($column_setting[0]['crm_accounts']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_accounts'];?></td>
               <?php } if(isset($column_setting[0]['crm_bookkeeping'])&&($column_setting[0]['crm_bookkeeping']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_bookkeeping'];?></td>
               <?php } if(isset($column_setting[0]['crm_ct600_return'])&&($column_setting[0]['crm_ct600_return']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_ct600_return'];?></td>
               <?php } if(isset($column_setting[0]['crm_payroll'])&&($column_setting[0]['crm_payroll']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_payroll'];?></td>
               <?php } if(isset($column_setting[0]['crm_auto_enrolment'])&&($column_setting[0]['crm_auto_enrolment']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_auto_enrolment'];?></td>
               <?php } if(isset($column_setting[0]['crm_vat_returns'])&&($column_setting[0]['crm_vat_returns']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_vat_returns'];?></td>
               <?php } if(isset($column_setting[0]['crm_confirmation_statement'])&&($column_setting[0]['crm_confirmation_statement']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_confirmation_statement'];?></td>
               <?php } if(isset($column_setting[0]['crm_cis'])&&($column_setting[0]['crm_cis']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_cis'];?></td>
               <?php } if(isset($column_setting[0]['crm_p11d'])&&($column_setting[0]['crm_p11d']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_p11d'];?></td>
               <?php } if(isset($column_setting[0]['crm_invesitgation_insurance'])&&($column_setting[0]['crm_invesitgation_insurance']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_invesitgation_insurance'];?></td>
               <?php } if(isset($column_setting[0]['crm_services_registered_address'])&&($column_setting[0]['crm_services_registered_address']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_services_registered_address'];?></td>
               <?php } if(isset($column_setting[0]['crm_bill_payment'])&&($column_setting[0]['crm_bill_payment']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_bill_payment'];?></td>
               <?php } if(isset($column_setting[0]['crm_advice'])&&($column_setting[0]['crm_advice']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_advice'];?></td>
               <?php } if(isset($column_setting[0]['crm_monthly_charge'])&&($column_setting[0]['crm_monthly_charge']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_monthly_charge'];?></td>
               <?php } if(isset($column_setting[0]['crm_annual_charge'])&&($column_setting[0]['crm_annual_charge']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_annual_charge'];?></td>
               <?php } if(isset($column_setting[0]['crm_accounts_periodend'])&&($column_setting[0]['crm_accounts_periodend']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_accounts_periodend'];?></td>
               <?php } if(isset($column_setting[0]['crm_ch_yearend'])&&($column_setting[0]['crm_ch_yearend']=='1')){?>               
               <td> style="text-align:left;border: 1px solid #ddd;"<?php echo $company_status['crm_ch_yearend'];?></td>
               <?php } if(isset($column_setting[0]['crm_hmrc_yearend'])&&($column_setting[0]['crm_hmrc_yearend']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_hmrc_yearend'];?></td>
               <?php } if(isset($column_setting[0]['crm_ch_accounts_next_due'])&&($column_setting[0]['crm_ch_accounts_next_due']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_ch_accounts_next_due'];?></td>
               <?php } if(isset($column_setting[0]['crm_ct600_due'])&&($column_setting[0]['crm_ct600_due']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_ct600_due'];?></td>
               <?php } if(isset($column_setting[0]['crm_companies_house_email_remainder'])&&($column_setting[0]['crm_companies_house_email_remainder']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_companies_house_email_remainder'];?></td>
               <?php } if(isset($column_setting[0]['crm_latest_action'])&&($column_setting[0]['crm_latest_action']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_latest_action'];?></td>
               <?php } if(isset($column_setting[0]['crm_latest_action_date'])&&($column_setting[0]['crm_latest_action_date']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_latest_action_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_records_received_date'])&&($column_setting[0]['crm_records_received_date']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_records_received_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_confirmation_statement_date'])&&($column_setting[0]['crm_confirmation_statement_date']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_confirmation_statement_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_confirmation_statement_due_date'])&&($column_setting[0]['crm_confirmation_statement_due_date']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_confirmation_statement_due_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_confirmation_latest_action'])&&($column_setting[0]['crm_confirmation_latest_action']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_confirmation_latest_action'];?></td>
               <?php } if(isset($column_setting[0]['crm_confirmation_latest_action_date'])&&($column_setting[0]['crm_confirmation_latest_action_date']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_confirmation_latest_action_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_confirmation_records_received_date'])&&($column_setting[0]['crm_confirmation_records_received_date']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_confirmation_records_received_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_confirmation_officers'])&&($column_setting[0]['crm_confirmation_officers']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_confirmation_officers'];?></td>
               <?php } if(isset($column_setting[0]['crm_share_capital'])&&($column_setting[0]['crm_share_capital']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_share_capital'];?></td>
               <?php } if(isset($column_setting[0]['crm_shareholders'])&&($column_setting[0]['crm_shareholders']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_shareholders'];?></td>
               <?php } if(isset($column_setting[0]['crm_people_with_significant_control'])&&($column_setting[0]['crm_people_with_significant_control']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_people_with_significant_control'];?></td>
               <?php } if(isset($column_setting[0]['crm_vat_quarters'])&&($column_setting[0]['crm_vat_quarters']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_vat_quarters'];?></td>
               <?php } if(isset($column_setting[0]['crm_vat_quater_end_date'])&&($column_setting[0]['crm_vat_quater_end_date']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_vat_quater_end_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_next_return_due_date'])&&($column_setting[0]['crm_next_return_due_date']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_next_return_due_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_vat_latest_action'])&&($column_setting[0]['crm_vat_latest_action']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_vat_latest_action'];?></td>
               <?php } if(isset($column_setting[0]['crm_vat_latest_action_date'])&&($column_setting[0]['crm_vat_latest_action_date']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_vat_latest_action_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_vat_records_received_date'])&&($column_setting[0]['crm_vat_records_received_date']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_vat_records_received_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_vat_member_state'])&&($column_setting[0]['crm_vat_member_state']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_vat_member_state'];?></td>
               <?php } if(isset($column_setting[0]['crm_vat_number'])&&($column_setting[0]['crm_vat_number']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_vat_number'];?></td>
               <?php } if(isset($column_setting[0]['crm_vat_date_of_registration'])&&($column_setting[0]['crm_vat_date_of_registration']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_vat_date_of_registration'];?></td>
               <?php } if(isset($column_setting[0]['crm_vat_effective_date'])&&($column_setting[0]['crm_vat_effective_date']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_vat_effective_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_vat_estimated_turnover'])&&($column_setting[0]['crm_vat_estimated_turnover']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_vat_estimated_turnover'];?></td>
               <?php } if(isset($column_setting[0]['crm_transfer_of_going_concern'])&&($column_setting[0]['crm_transfer_of_going_concern']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_transfer_of_going_concern'];?></td>
               <?php } if(isset($column_setting[0]['crm_involved_in_any_other_business'])&&($column_setting[0]['crm_involved_in_any_other_business']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_involved_in_any_other_business'];?></td>
               <?php } if(isset($column_setting[0]['crm_flat_rate'])&&($column_setting[0]['crm_flat_rate']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_flat_rate'];?></td>
               <?php } if(isset($column_setting[0]['crm_direct_debit'])&&($column_setting[0]['crm_direct_debit']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_direct_debit'];?></td>
               <?php } if(isset($column_setting[0]['crm_annual_accounting_scheme'])&&($column_setting[0]['crm_annual_accounting_scheme']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_annual_accounting_scheme'];?></td>
               <?php } if(isset($column_setting[0]['crm_flat_rate_category'])&&($column_setting[0]['crm_flat_rate_category']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_flat_rate_category'];?></td>
               <?php } if(isset($column_setting[0]['crm_month_of_last_quarter_submitted'])&&($column_setting[0]['crm_month_of_last_quarter_submitted']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_month_of_last_quarter_submitted'];?></td>
               <?php } if(isset($column_setting[0]['crm_box5_of_last_quarter_submitted'])&&($column_setting[0]['crm_box5_of_last_quarter_submitted']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_box5_of_last_quarter_submitted'];?></td>
               <?php } if(isset($column_setting[0]['crm_vat_address'])&&($column_setting[0]['crm_vat_address']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_vat_address'];?></td>
               <?php } if(isset($column_setting[0]['crm_vat_notes'])&&($column_setting[0]['crm_vat_notes']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_vat_notes'];?></td>
               <?php } if(isset($column_setting[0]['crm_employers_reference'])&&($column_setting[0]['crm_employers_reference']==1)){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_employers_reference'];?></td>
               <?php } if(isset($column_setting[0]['crm_accounts_office_reference'])&&($column_setting[0]['crm_accounts_office_reference']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_accounts_office_reference'];?></td>
               <?php } if(isset($column_setting[0]['crm_years_required'])&&($column_setting[0]['crm_years_required']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_years_required'];?></td>
               <?php } if(isset($column_setting[0]['crm_annual_or_monthly_submissions'])&&($column_setting[0]['crm_annual_or_monthly_submissions']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_annual_or_monthly_submissions'];?></td>
               <?php } if(isset($column_setting[0]['crm_irregular_montly_pay'])&&($column_setting[0]['crm_irregular_montly_pay']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_irregular_montly_pay'];?></td>
               <?php } if(isset($column_setting[0]['crm_nil_eps'])&&($column_setting[0]['crm_nil_eps']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_nil_eps'];?></td>
               <?php } if(isset($column_setting[0]['crm_no_of_employees'])&&($column_setting[0]['crm_no_of_employees']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_no_of_employees'];?></td>
               <?php } if(isset($column_setting[0]['crm_first_pay_date'])&&($column_setting[0]['crm_first_pay_date']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_first_pay_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_rti_deadline'])&&($column_setting[0]['crm_rti_deadline']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_rti_deadline'];?></td>
               <?php } if(isset($column_setting[0]['crm_paye_scheme_ceased'])&&($column_setting[0]['crm_paye_scheme_ceased']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_paye_scheme_ceased'];?></td>
               <?php } if(isset($column_setting[0]['crm_paye_latest_action'])&&($column_setting[0]['crm_paye_latest_action']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_paye_latest_action'];?></td>
               <?php } if(isset($column_setting[0]['crm_paye_latest_action_date'])&&($column_setting[0]['crm_paye_latest_action_date']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_paye_latest_action_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_paye_records_received'])&&($column_setting[0]['crm_paye_records_received']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_paye_records_received'];?></td>
               <?php } if(isset($column_setting[0]['crm_next_p11d_return_due'])&&($column_setting[0]['crm_next_p11d_return_due']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_next_p11d_return_due'];?></td>
               <?php } if(isset($column_setting[0]['crm_latest_p11d_submitted'])&&($column_setting[0]['crm_latest_p11d_submitted']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_latest_p11d_submitted'];?></td>
               <?php } if(isset($column_setting[0]['crm_p11d_latest_action'])&&($column_setting[0]['crm_p11d_latest_action']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_p11d_latest_action'];?></td>
               <?php } if(isset($column_setting[0]['crm_p11d_latest_action_date'])&&($column_setting[0]['crm_p11d_latest_action_date']=='1')){?>               
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_p11d_latest_action_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_p11d_records_received'])&&($column_setting[0]['crm_p11d_records_received']=='1')){?>               
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_p11d_records_received'];?></td>
               <?php } if(isset($column_setting[0]['crm_cis_contractor'])&&($column_setting[0]['crm_cis_contractor']=='1')){?>               
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_cis_contractor'];?></td>
               <?php } if(isset($column_setting[0]['crm_cis_subcontractor'])&&($column_setting[0]['crm_cis_subcontractor']=='1')){?>               
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_cis_subcontractor'];?></td>
               <?php } if(isset($column_setting[0]['crm_cis_deadline'])&&($column_setting[0]['crm_cis_deadline']=='1')){?>               
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_cis_deadline'];?></td>
               <?php } if(isset($column_setting[0]['crm_auto_enrolment_latest_action'])&&($column_setting[0]['crm_auto_enrolment_latest_action']=='1')){?>               
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_auto_enrolment_latest_action'];?></td>
               <?php } if(isset($column_setting[0]['crm_auto_enrolment_latest_action_date'])&&($column_setting[0]['crm_auto_enrolment_latest_action_date']=='1')){?>               
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_auto_enrolment_latest_action_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_auto_enrolment_records_received'])&&($column_setting[0]['crm_auto_enrolment_records_received']=='1')){?>               
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_auto_enrolment_records_received'];?></td>
               <?php } if(isset($column_setting[0]['crm_auto_enrolment_staging'])&&($column_setting[0]['crm_auto_enrolment_staging']=='1')){?>               
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_auto_enrolment_staging'];?></td>
               <?php } if(isset($column_setting[0]['crm_postponement_date'])&&($column_setting[0]['crm_postponement_date']=='1')){?>               
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_postponement_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_the_pensions_regulator_opt_out_date'])&&($column_setting[0]['crm_the_pensions_regulator_opt_out_date']=='1')){?>               
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_the_pensions_regulator_opt_out_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_re_enrolment_date'])&&($column_setting[0]['crm_re_enrolment_date']=='1')){?>               
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_re_enrolment_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_pension_provider'])&&($column_setting[0]['crm_pension_provider']=='1')){?>               
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_pension_provider'];?></td>
               <?php } if(isset($column_setting[0]['crm_pension_id'])&&($column_setting[0]['crm_pension_id']=='1')){?>               
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_pension_id'];?></td>
               <?php } if(isset($column_setting[0]['crm_declaration_of_compliance_due_date'])&&($column_setting[0]['crm_declaration_of_compliance_due_date']=='1')){?>               
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_declaration_of_compliance_due_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_declaration_of_compliance_submission'])&&($column_setting[0]['crm_declaration_of_compliance_submission']=='1')){?>               
               <td style="text-align:left;border: 1px solid #ddd;"> <?php echo $company_status['crm_declaration_of_compliance_submission'];?></td>
               <?php } if(isset($column_setting[0]['crm_pension_deadline'])&&($column_setting[0]['crm_pension_deadline']=='1')){?>               
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_pension_deadline'];?></td>
               <?php } if(isset($column_setting[0]['crm_note'])&&($column_setting[0]['crm_note']=='1')){?>               
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_note'];?></td>
               <?php } if(isset($column_setting[0]['crm_registration_fee_paid'])&&($column_setting[0]['crm_registration_fee_paid']=='1')){?>               
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_registration_fee_paid'];?></td>
               <?php } if(isset($column_setting[0]['crm_64_8_registration'])&&($column_setting[0]['crm_64_8_registration']=='1')){?>               
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_64_8_registration'];?></td>
               <?php } if(isset($column_setting[0]['crm_packages'])&&($column_setting[0]['crm_packages']=='1')){?>               
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_packages'];?></td>
               <?php } if(isset($column_setting[0]['crm_gender'])&&($column_setting[0]['crm_gender']=='1')){?>               
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_gender'];?></td>
               <?php } if(isset($column_setting[0]['crm_letter_sign'])&&($column_setting[0]['crm_letter_sign']=='1')){?> 
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_letter_sign'];?></td>
               <?php } if(isset($column_setting[0]['crm_business_website'])&&($column_setting[0]['crm_business_website']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_business_website'];?></td>
               <?php } if(isset($column_setting[0]['crm_paye_ref_number'])&&($column_setting[0]['crm_paye_ref_number']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_paye_ref_number'];?></td>
               <?php } if(isset($column_setting[0]['select_responsible_type'])&&($column_setting[0]['select_responsible_type']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['select_responsible_type'];?></td>
               <?php } if(isset($column_setting[0]['crm_assign_manager_reviewer'])&&($column_setting[0]['crm_assign_manager_reviewer']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_assign_manager_reviewer'];?></td>
               <?php } if(isset($column_setting[0]['crm_assign_managed'])&&($column_setting[0]['crm_assign_managed']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_assign_managed'];?></td>
               <?php } if(isset($column_setting[0]['crm_assign_client_id_verified'])&&($column_setting[0]['crm_assign_client_id_verified']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_assign_client_id_verified'];?></td>
               <?php } if(isset($column_setting[0]['crm_assign_type_of_id'])&&($column_setting[0]['crm_assign_type_of_id']=='1')){?>
               <td style="text-align:left;border: 1px solid #ddd;"> <?php echo $company_status['crm_assign_type_of_id'];?></td>
               <?php } if(isset($column_setting[0]['crm_assign_proof_of_address'])&&($column_setting[0]['crm_assign_proof_of_address']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_assign_proof_of_address'];?></td>
               <?php } if(isset($column_setting[0]['crm_assign_meeting_client'])&&($column_setting[0]['crm_assign_meeting_client']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_assign_meeting_client'];?></td>
               <?php } if(isset($column_setting[0]['crm_assign_source'])&&($column_setting[0]['crm_assign_source']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_assign_source'];?></td>
               <?php } if(isset($column_setting[0]['crm_assign_relationship_client'])&&($column_setting[0]['crm_assign_relationship_client']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_assign_relationship_client'];?></td>
               <?php } if(isset($column_setting[0]['crm_assign_notes'])&&($column_setting[0]['crm_assign_notes']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_assign_notes'];?></td>
               <?php } if(isset($column_setting[0]['crm_other_name_of_firm'])&&($column_setting[0]['crm_other_name_of_firm']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_other_name_of_firm'];?></td>
               <?php } if(isset($column_setting[0]['crm_other_address'])&&($column_setting[0]['crm_other_address']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_other_address'];?></td>
               <?php } if(isset($column_setting[0]['crm_other_contact_no'])&&($column_setting[0]['crm_other_contact_no']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_other_contact_no'];?></td>
               <?php } if(isset($column_setting[0]['crm_other_email'])&&($column_setting[0]['crm_other_email']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_other_email'];?></td>
               <?php } if(isset($column_setting[0]['crm_other_chase_for_info'])&&($column_setting[0]['crm_other_chase_for_info']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_other_chase_for_info'];?></td>
               <?php } if(isset($column_setting[0]['crm_other_notes'])&&($column_setting[0]['crm_other_notes']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_other_notes'];?></td>
               <?php } if(isset($column_setting[0]['crm_other_internal_notes'])&&($column_setting[0]['crm_other_internal_notes']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_other_internal_notes'];?></td>
               <?php } if(isset($column_setting[0]['crm_other_invite_use'])&&($column_setting[0]['crm_other_invite_use']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_other_invite_use'];?></td>
               <?php } if(isset($column_setting[0]['crm_other_crm'])&&($column_setting[0]['crm_other_crm']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_other_crm'];?></td>
               <?php } if(isset($column_setting[0]['crm_other_proposal'])&&($column_setting[0]['crm_other_proposal']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_other_proposal'];?></td>
               <?php } if(isset($column_setting[0]['crm_other_task'])&&($column_setting[0]['crm_other_task']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_other_task'];?></td>
               <?php } if(isset($column_setting[0]['crm_other_send_invit_link'])&&($column_setting[0]['crm_other_send_invit_link']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_other_send_invit_link'];?></td>
               <?php } if(isset($column_setting[0]['crm_other_username'])&&($column_setting[0]['crm_other_username']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_other_username'];?></td>
               <?php } if(isset($column_setting[0]['crm_other_any_notes'])&&($column_setting[0]['crm_other_any_notes']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_other_any_notes'];?></td>
               <?php } if(isset($column_setting[0]['crm_confirmation_auth_code'])&&($column_setting[0]['crm_confirmation_auth_code']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_confirmation_auth_code'];?></td>
               <?php } if(isset($column_setting[0]['crm_accounts_auth_code'])&&($column_setting[0]['crm_accounts_auth_code']=='1')){?>
               <td style="text-align:left;border: 1px solid #ddd;"> <?php echo $company_status['crm_accounts_auth_code'];?></td>
               <?php } if(isset($column_setting[0]['crm_accounts_utr_number'])&&($column_setting[0]['crm_accounts_utr_number']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_accounts_utr_number'];?></td>
               <?php } if(isset($column_setting[0]['crm_accounts_due_date_hmrc'])&&($column_setting[0]['crm_accounts_due_date_hmrc']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_accounts_due_date_hmrc'];?></td>
               <?php } if(isset($column_setting[0]['crm_accounts_tax_date_hmrc'])&&($column_setting[0]['crm_accounts_tax_date_hmrc']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_accounts_tax_date_hmrc'];?></td>
               <?php } if(isset($column_setting[0]['crm_accounts_notes'])&&($column_setting[0]['crm_accounts_notes']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_accounts_notes'];?></td>
               <?php } if(isset($column_setting[0]['crm_property_income'])&&($column_setting[0]['crm_property_income']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_property_income'];?></td>
               <?php } if(isset($column_setting[0]['crm_additional_income'])&&($column_setting[0]['crm_additional_income']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_additional_income'];?></td>
               <?php } if(isset($column_setting[0]['crm_personal_tax_return_date'])&&($column_setting[0]['crm_personal_tax_return_date']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_personal_tax_return_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_personal_due_date_return'])&&($column_setting[0]['crm_personal_due_date_return']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_personal_due_date_return'];?></td>
               <?php } if(isset($column_setting[0]['crm_personal_due_date_online'])&&($column_setting[0]['crm_personal_due_date_online']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_personal_due_date_online'];?></td>
               <?php } if(isset($column_setting[0]['crm_personal_notes'])&&($column_setting[0]['crm_personal_notes']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_personal_notes'];?></td>
               <?php } if(isset($column_setting[0]['crm_payroll_acco_off_ref_no'])&&($column_setting[0]['crm_payroll_acco_off_ref_no']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_payroll_acco_off_ref_no'];?></td>
               <?php } if(isset($column_setting[0]['crm_paye_off_ref_no'])&&($column_setting[0]['crm_paye_off_ref_no']=='1')){?>
               <td style="text-align:left;border: 1px solid #ddd;"> <?php echo $company_status['crm_paye_off_ref_no'];?></td>
               <?php } if(isset($column_setting[0]['crm_payroll_reg_date'])&&($column_setting[0]['crm_payroll_reg_date']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_payroll_reg_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_payroll_run'])&&($column_setting[0]['crm_payroll_run']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_payroll_run'];?></td>
               <?php } if(isset($column_setting[0]['crm_payroll_run_date'])&&($column_setting[0]['crm_payroll_run_date']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_payroll_run_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_previous_year_require'])&&($column_setting[0]['crm_previous_year_require']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_previous_year_require'];?></td>
               <?php } if(isset($column_setting[0]['crm_payroll_if_yes'])&&($column_setting[0]['crm_payroll_if_yes']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_payroll_if_yes'];?></td>
               <?php } if(isset($column_setting[0]['crm_staging_date'])&&($column_setting[0]['crm_staging_date']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_staging_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_pension_subm_due_date'])&&($column_setting[0]['crm_pension_subm_due_date']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_pension_subm_due_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_employer_contri_percentage'])&&($column_setting[0]['crm_employer_contri_percentage']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_employer_contri_percentage'];?></td>
               <?php } if(isset($column_setting[0]['crm_employee_contri_percentage'])&&($column_setting[0]['crm_employee_contri_percentage']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_employee_contri_percentage'];?></td>
               <?php } if(isset($column_setting[0]['crm_pension_notes'])&&($column_setting[0]['crm_pension_notes']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_pension_notes'];?></td>
               <?php } if(isset($column_setting[0]['crm_cis_contractor_start_date'])&&($column_setting[0]['crm_cis_contractor_start_date']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_cis_contractor_start_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_cis_scheme_notes'])&&($column_setting[0]['crm_cis_scheme_notes']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_cis_scheme_notes'];?></td>
               <?php } if(isset($column_setting[0]['crm_cis_subcontractor_start_date'])&&($column_setting[0]['crm_cis_subcontractor_start_date']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_cis_subcontractor_start_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_cis_subcontractor_scheme_notes'])&&($column_setting[0]['crm_cis_subcontractor_scheme_notes']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_cis_subcontractor_scheme_notes'];?></td>
               <?php } if(isset($column_setting[0]['crm_p11d_previous_year_require'])&&($column_setting[0]['crm_p11d_previous_year_require']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_p11d_previous_year_require'];?></td>
               <?php } if(isset($column_setting[0]['crm_p11d_payroll_if_yes'])&&($column_setting[0]['crm_p11d_payroll_if_yes']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_p11d_payroll_if_yes'];?></td>
               <?php } if(isset($column_setting[0]['crm_vat_frequency'])&&($column_setting[0]['crm_vat_frequency']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_vat_frequency'];?></td>
               <?php } if(isset($column_setting[0]['crm_vat_due_date'])&&($column_setting[0]['crm_vat_due_date']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_vat_due_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_vat_scheme'])&&($column_setting[0]['crm_vat_scheme']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_vat_scheme'];?></td>
               <?php } if(isset($column_setting[0]['crm_flat_rate_percentage'])&&($column_setting[0]['crm_flat_rate_percentage']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_flat_rate_percentage'];?></td>
               <?php } if(isset($column_setting[0]['crm_next_booking_date'])&&($column_setting[0]['crm_next_booking_date']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_next_booking_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_method_bookkeeping'])&&($column_setting[0]['crm_method_bookkeeping']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_method_bookkeeping'];?></td>
               <?php } if(isset($column_setting[0]['crm_client_provide_record'])&&($column_setting[0]['crm_client_provide_record']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_client_provide_record'];?></td>
               <?php } if(isset($column_setting[0]['crm_manage_acc_fre'])&&($column_setting[0]['crm_manage_acc_fre']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_manage_acc_fre'];?></td>
               <?php } if(isset($column_setting[0]['crm_next_manage_acc_date'])&&($column_setting[0]['crm_next_manage_acc_date']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_next_manage_acc_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_manage_method_bookkeeping'])&&($column_setting[0]['crm_manage_method_bookkeeping']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_manage_method_bookkeeping'];?></td>
               <?php } if(isset($column_setting[0]['crm_manage_client_provide_record'])&&($column_setting[0]['crm_manage_client_provide_record']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_manage_client_provide_record'];?></td>
               <?php } if(isset($column_setting[0]['crm_manage_notes'])&&($column_setting[0]['crm_manage_notes']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_manage_notes'];?></td>
               <?php } if(isset($column_setting[0]['crm_insurance_renew_date'])&&($column_setting[0]['crm_insurance_renew_date']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_insurance_renew_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_insurance_provider'])&&($column_setting[0]['crm_insurance_provider']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_insurance_provider'];?></td>
               <?php } if(isset($column_setting[0]['crm_claims_note'])&&($column_setting[0]['crm_claims_note']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_claims_note'];?></td>
               <?php } if(isset($column_setting[0]['crm_registered_start_date'])&&($column_setting[0]['crm_registered_start_date']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_registered_start_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_registered_renew_date'])&&($column_setting[0]['crm_registered_renew_date']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_registered_renew_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_registered_office_inuse'])&&($column_setting[0]['crm_registered_office_inuse']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_registered_office_inuse'];?></td>
               <?php } if(isset($column_setting[0]['crm_registered_claims_note'])&&($column_setting[0]['crm_registered_claims_note']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_registered_claims_note'];?></td>
               <?php } if(isset($column_setting[0]['crm_investigation_start_date'])&&($column_setting[0]['crm_investigation_start_date']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_investigation_start_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_investigation_end_date'])&&($column_setting[0]['crm_investigation_end_date']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_investigation_end_date'];?></td>
               <?php } if(isset($column_setting[0]['crm_investigation_note'])&&($column_setting[0]['crm_investigation_note']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_investigation_note'];?></td>
               <?php } if(isset($column_setting[0]['crm_tax_investigation_note'])&&($column_setting[0]['crm_tax_investigation_note']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_tax_investigation_note'];?></td>
               <?php } if(isset($column_setting[0]['crm_assign_other_custom'])&&($column_setting[0]['crm_assign_other_custom']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_assign_other_custom'];?></td>
               <?php } if(isset($column_setting[0]['crm_notes_info'])&&($column_setting[0]['crm_notes_info']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_notes_info'];?></td>
               <?php } if(isset($column_setting[0]['crm_registered_in'])&&($column_setting[0]['crm_registered_in']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_registered_in'];?></td>
               <?php } if(isset($column_setting[0]['crm_address_line_one'])&&($column_setting[0]['crm_address_line_one']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_address_line_one'];?></td>
               <?php } if(isset($column_setting[0]['crm_address_line_two'])&&($column_setting[0]['crm_address_line_two']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_address_line_two'];?></td>
               <?php } if(isset($column_setting[0]['crm_address_line_three'])&&($column_setting[0]['crm_address_line_three']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_address_line_three'];?></td>
               <?php } if(isset($column_setting[0]['crm_town_city'])&&($column_setting[0]['crm_town_city']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_town_city'];?></td>
               <?php } if(isset($column_setting[0]['crm_post_code'])&&($column_setting[0]['crm_post_code']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $company_status['crm_post_code'];?></td>
               <?php } ?>
               <?php if(isset($column_setting[0]['contact_title'])&&($column_setting[0]['contact_title']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $contact['title'];?></td>
               <?php } if(isset($column_setting[0]['contact_first_name'])&&($column_setting[0]['contact_first_name']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $contact['first_name'];?></td>
               <?php } if(isset($column_setting[0]['contact_middle_name'])&&($column_setting[0]['contact_middle_name']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $contact['middle_name'];?></td>
               <?php } if(isset($column_setting[0]['contact_surname'])&&($column_setting[0]['contact_surname']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $contact['surname'];?></td>
               <?php } if(isset($column_setting[0]['contact_preferred_name'])&&($column_setting[0]['contact_preferred_name']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $contact['preferred_name'];?></td>
               <?php } if(isset($column_setting[0]['contact_mobile'])&&($column_setting[0]['contact_mobile']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $contact['mobile'];?></td>
               <?php } if(isset($column_setting[0]['contact_main_email'])&&($column_setting[0]['contact_main_email']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $contact['main_email'];?></td>
               <?php } if(isset($column_setting[0]['contact_nationality'])&&($column_setting[0]['contact_nationality']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $contact['nationality'];?></td>
               <?php } if(isset($column_setting[0]['contact_psc'])&&($column_setting[0]['contact_psc']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $contact['psc'];?></td>
               <?php } if(isset($column_setting[0]['contact_shareholder'])&&($column_setting[0]['contact_shareholder']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $contact['shareholder'];?></td>
               <?php } if(isset($column_setting[0]['contact_ni_number'])&&($column_setting[0]['contact_ni_number']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $contact['ni_number'];?></td>
               <?php } if(isset($column_setting[0]['contact_country_of_residence'])&&($column_setting[0]['contact_country_of_residence']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $contact['country_of_residence'];?></td>
               <?php } if(isset($column_setting[0]['contact_type'])&&($column_setting[0]['contact_type']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $contact['contact_type'];?></td>
               <?php } if(isset($column_setting[0]['contact_other_custom'])&&($column_setting[0]['contact_other_custom']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $contact['other_custom'];?></td>
               <?php } if(isset($column_setting[0]['contact_address_line1'])&&($column_setting[0]['contact_address_line1']=='1')){?>
               <td  style="text-align:left;border: 1px solid #ddd;"><?php echo $contact['address_line1'];?></td>
               <?php } if(isset($column_setting[0]['contact_address_line2'])&&($column_setting[0]['contact_address_line2']=='1')){?>
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $contact['address_line2'];?></td>
               <?php } if(isset($column_setting[0]['contact_town_city'])&&($column_setting[0]['contact_town_city']=='1')){?>
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $contact['town_city'];?></td>
               <?php } if(isset($column_setting[0]['contact_post_code'])&&($column_setting[0]['contact_post_code']=='1')){?>
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $contact['post_code'];?></td>
               <?php } if(isset($column_setting[0]['contact_landline'])&&($column_setting[0]['contact_landline']=='1')){?>
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $contact['landline'];?></td>
               <?php } if(isset($column_setting[0]['contact_work_email'])&&($column_setting[0]['contact_work_email']=='1')){?>
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $contact['work_email'];?></td>
               <?php } if(isset($column_setting[0]['contact_date_of_birth'])&&($column_setting[0]['contact_date_of_birth']=='1')){?>
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $contact['date_of_birth'];?></td>
               <?php } if(isset($column_setting[0]['contact_nature_of_control'])&&($column_setting[0]['contact_nature_of_control']=='1')){?>
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $contact['nature_of_control'];?></td>
               <?php } if(isset($column_setting[0]['contact_marital_status'])&&($column_setting[0]['contact_marital_status']=='1')){?>
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $contact['marital_status'];?></td>
               <?php } if(isset($column_setting[0]['contact_utr_number'])&&($column_setting[0]['contact_utr_number']=='1')){?>
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $contact['utr_number'];?></td>
               <?php } if(isset($column_setting[0]['contact_occupation'])&&($column_setting[0]['contact_occupation']=='1')){?>
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $contact['occupation'];?></td>
               <?php } if(isset($column_setting[0]['contact_appointed_on'])&&($column_setting[0]['contact_appointed_on']=='1')){?>
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $contact['appointed_on'];?></td>
               <?php } ?>
               <?php if(isset($column_setting[0]['staff_manager'])&&($column_setting[0]['staff_manager']=='1')){
                  if(is_array($staff) && count($staff) > 0)
                  {
                    
                  
                    
                  foreach ($staff as $key => $value) {
                  
                  
                  $staffs[]=$value['manager_reviewer'];
                  
                  /*foreach($value as $key => $vaal){
                    
                  
                  }*/
                  
                  }
                  
                  //print_r($staffs);
                  
                  }
                  
                  if(!empty($staffs)) {
                  
                  
                  $staff1=implode(',', $staffs);
                  $staffs=array();
                  $staff_form = $this->db->query('select first_name from staff_form where user_id in ('.$staff1.')')->result_array();
                  /* echo "<pre>";
                    print_r($staff_form);
                    echo "</pre>";*/
                  foreach ($staff_form as $key => $value) {
                    $val[]=$value['first_name'];
                  }
                  if(!empty($val)){
                  $staff2=implode(',', $val);
                  $val=array();
                  } else {
                    $staff2='';
                  }
                  
                  } else{
                    $staff2='';
                  }
                                                        ?>
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $staff2; ?></td>
               <?php } if(isset($column_setting[0]['staff_managed'])&&($column_setting[0]['staff_managed']=='1')){
                  if(is_array($staff) && count($staff) > 0)
                  {
                  foreach ($staff as $key => $value) {
                  $staffs[]=$value['assign_managed'];
                  }
                  }
                  if(!empty($staffs)) {
                  $staff2=implode(',', $staffs);
                  $staffs=array();
                  $managed = $this->db->query('select crm_name from user where id in ('.$staff2.') and role=5')->result_array();
                  foreach ($managed as $key => $value) {
                    $val[]=$value['crm_name'];
                  }
                  if(!empty($val)){
                  $staff4=implode(',', $val);
                  $val=array();
                  } else {
                    $staff4='';
                  }
                  } else{
                    $staff4='';
                  }
                  
                                                        ?>
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $staff4; ?></td>
               <?php } if(isset($column_setting[0]['team'])&&($column_setting[0]['team']=='1')){
                  if(is_array($team) && count($team) > 0)
                  {
                  foreach ($team as $key => $value) {
                  $teams[]=$value['team'];
                  }
                  }
                  
                  if(!empty($teams)) {
                  $team1=implode(',', $teams);
                  $teams=array();
                  $teamed = $this->db->query('select team from team where id in ('.$team1.')')->result_array();
                  foreach ($teamed as $key => $value) {
                    $val[]=$value['team'];
                  }
                  if(!empty($val)){
                  $team2=implode(',', $val);
                  $val=array();
                  } else {
                    $team2='';
                  }
                  } else{
                    $team2='';
                  }
                                                        ?>
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $team2; ?></td>
               <?php } if(isset($column_setting[0]['team_allocation'])&&($column_setting[0]['team_allocation']=='1')){
                  if(is_array($team) && count($team) > 0)
                  {
                  foreach ($team as $key => $value) {
                  $teams[]=$value['allocation_holder'];
                  }
                  }
                  
                  if(!empty($teams)) {
                  $team3=implode(',', $teams);
                  $teams=array();
                  } else {
                    $team3='';
                  }
                                                        ?>
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $team3; ?></td>
               <?php } if(isset($column_setting[0]['department'])&&($column_setting[0]['department']=='1')){
                  if(is_array($department) && count($department) > 0)
                  {   
                                                     
                  foreach ($department as $key => $value) {
                  $departs[]=$value['depart'];
                  }
                  }
                  
                  if(!empty($departs)) {
                  
                  $depart1=implode(',', $departs);
                  $departs=array();
                  
                  
                  } else{
                  $depart1='';
                  }
                                                        ?>
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $depart1; ?></td>
               <?php } if(isset($column_setting[0]['department_allocation'])&&($column_setting[0]['department_allocation']=='1')){
                  if(is_array($department) && count($department) > 0)
                  {
                  
                  foreach ($department as $key => $value) {
                  $departs[]=$value['allocation_holder'];
                  }
                  }
                  
                  if(!empty($departs)) {
                  $depart3=implode(',', $departs);
                  $departs=array();
                  } else {
                    $depart3='';
                  }
                                                        ?>
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $depart3; ?></td>
               <?php } if(isset($column_setting[0]['member_manager'])&&($column_setting[0]['member_manager']=='1')){
                  if(is_array($member) && count($member) > 0)
                  {                                        
                  foreach ($member as $key => $value) {
                  $members[]=$value['manager_reviewer'];
                  }
                  }
                  
                  if(!empty($members)) {
                  $member1=implode(',', $members);
                  $members=array();
                  } else{
                  $member1='';
                  }
                                                        ?>
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $member1;?></td>
               <?php } if(isset($column_setting[0]['member_managed'])&&($column_setting[0]['member_managed']=='1')){
                  if(is_array($member) && count($member) > 0)
                  { 
                  foreach ($member as $key => $value) {
                  $members[]=$value['assign_managed'];
                  }
                  }
                  if(!empty($members)) {
                  $member2=implode(',', $members);
                  $members=array();
                  } else {
                  $member2='';
                  }
                                                        ?>
               <td style="text-align:left;border: 1px solid #ddd;"><?php echo $member2;?></td>
               <?php } ?>
            </tr>
            <?php $s++; } ?>
         </tbody>
      </table>
   </body>
</html>