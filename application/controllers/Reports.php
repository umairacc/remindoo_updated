<?php
error_reporting(E_STRICT);
class Reports extends CI_Controller 
{
        public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
		public function __construct()
        {
            parent::__construct();
            $this->load->model(array('Common_mdl','Report_model','Security_model','Proposal_model','Invoice_model'));
            // $this->load->library('Excel');
            $this->load->helper('comman');
		}
        public function index()
        { 
          
          
          $this->Security_model->chk_login();
          
          if($_SESSION['permission']['Reports']['view'] == '1')
          {
            $data['client']=$this->Report_model->client_list(); 
            $data['Public']=$this->Report_model->details_list('Public Limited company');
            $data['limited']=$this->Report_model->details_list('Limited Liability Partnership');
            $data['Private']=$this->Report_model->details_list('Private Limited company');  
            $data['Partnership']=$this->Report_model->details_list('Partnership');
            $data['self']=$this->Report_model->details_list('Self Assessment');
            $data['Trust']=$this->Report_model->details_list('Trust');
            $data['Charity']=$this->Report_model->details_list('Charity');
            $data['Other']=$this->Report_model->details_list('Other');
            $data['total_tasks']=$this->Report_model->task_count();
            $data['notstarted_tasks']=$this->Report_model->task_details('notstarted');
            $data['completed_tasks']=$this->Report_model->task_details('complete');
            $data['inprogress_tasks']=$this->Report_model->task_details('inprogress');
            $data['awaiting_tasks']=$this->Report_model->task_details('awaiting');
            $data['archive_tasks']=$this->Report_model->task_details('archive'); 
            $data['low_tasks']=$this->Report_model->task_pridetails('low');
            $data['high_tasks']=$this->Report_model->task_pridetails('high');
            $data['medium_tasks']=$this->Report_model->task_pridetails('medium');
            $data['super_urgent_tasks']=$this->Report_model->task_pridetails('super_urgent');            
            $first_day = date("Y-m-01");
            $end_day = date("Y-m-t");   
            $end_day = date('Y-m-d', strtotime($end_day . ' +1 day'));
            $data['task_list_this_month']=$this->Report_model->task_this_month('add_new_task',$first_day,$end_day);
            $data['completed_task_count']=$this->Report_model->completed_task_count();
            
            $data['proposal_history']=$this->Proposal_model->proposal_details_date('',$first_day,$end_day);
            $data['accept_proposal']=$this->Proposal_model->proposal_details('accepted');
            $data['indiscussion_proposal']=$this->Proposal_model->proposal_details('in discussion');
            $data['sent_proposal']=$this->Proposal_model->proposal_details('sent');
            $data['viewed_proposal']=$this->Proposal_model->proposal_details('opened');
            $data['declined_proposal']=$this->Proposal_model->proposal_details('declined');
            $data['archive_proposal']=$this->Proposal_model->proposal_details('archive');
            $data['draft_proposal']=$this->Proposal_model->proposal_details('draft');
            
            $other_services = $this->db->query('SELECT * FROM service_lists WHERE id > 16')->result_array();               
            
            $columns = array('1'=> 'conf_statement','2' =>'accounts','3'=>'company_tax_return','4'=>'personal_tax_return','5' => 'vat','6' => 'payroll','7' => 'workplace', '8' => 'cis', '9' => 'cissub','10'=>'bookkeep','11' => 'p11d','12' => 'management','13' => 'investgate','14'=>'registered','15'=>'taxinvest','16' => 'taxadvice');
            
            foreach($other_services as $key => $value) 
            { 
              $columns[$value['id']] = $value['services_subnames'];
            } 
            
            $services = $this->Common_mdl->getServicelist();
            
            foreach($services as $key => $value) 
            {
              $data['services_count'][$value['service_name']]['id'] = $value['id'];
              $data['services_count'][$value['service_name']]['count'] = $this->Report_model->service_count($columns[$value['id']]);
            }               
            
            $columns_check = array('1'=> 'crm_confirmation_statement_due_date','2' =>'crm_ch_accounts_next_due','3'=>'crm_accounts_tax_date_hmrc','4'=>'crm_personal_due_date_return','5' => 'crm_vat_due_date','6' => 'crm_rti_deadline','7' => 'crm_pension_subm_due_date','8' =>'crm_registered_renew_date','9' =>'','10'=>'crm_next_booking_date','11' => 'crm_next_p11d_return_due','12' => 'crm_next_manage_acc_date','13' => 'crm_insurance_renew_date','14'=>'crm_registered_renew_date','15'=>'crm_investigation_end_date','16' => 'crm_investigation_end_date');                  
            
            foreach($other_services as $key => $value) 
            {
              $columns_check[$value['id']] = 'crm_'.$value['services_subnames'].'_statement_date';
            }
            
            // die("Reportsssss module is currently under progress");
            foreach($services as $key => $value) 
            {
              $data['deadlines_count'][$value['service_name']]['id'] = $value['id'];
              $data['deadlines_count'][$value['service_name']]['count'] = $this->Report_model->dead_count($columns_check[$value['id']],$columns[$value['id']]);
            }
                
                $data['leads_status']=$this->Common_mdl->getallrecords('leads_status');
                $data['leads_history']=$this->Report_model->leads_history_section();
                $data['overall_leads']=$this->Report_model->overallleads_count();

                $data['users_count']=$this->Report_model->users_count();
                $data['active_user_count']=$this->Report_model->activeusers_count();
                $data['inactive_user_count']=$this->Report_model->inactiveusers_count();
                $data['frozen_user_count']=$this->Report_model->frozenusers_count();                        

                $clients_user_ids = $this->Report_model->getClientsUserIds();
                $data['approved_invoice'] = $this->Invoice_model->selectInvoiceByType('amount_details','payment_status','approve','client_email', $clients_user_ids);
                $data['cancel_invoice'] = $this->Invoice_model->selectInvoiceByType('amount_details','payment_status','decline', 'client_email', $clients_user_ids);           
                $data['expired_invoice'] = $this->Invoice_model->selectInvoiceByType('amount_details','payment_status','pending', 'client_email', $clients_user_ids); //pending invoice

                $data['pending_invoice_month']=$this->Report_model->selectInvoiceByType('amount_details','payment_status','pending', 'client_email',$first_day,$end_day); 
                $data['approved_invoice_month']=$this->Report_model->selectInvoiceByType('amount_details','payment_status','approve','client_email', $first_day,$end_day);
                $data['cancel_invoice_month']=$this->Report_model->selectInvoiceByType('amount_details','payment_status','decline', 'client_email', $first_day,$end_day);

                $this->load->view('reports/reports_view',$data);
            }
            else
            {
                $this->load->view('users/blank_page');
            }
        }

        public function user_performation()
        {          
            $this->Security_model->chk_login();

            if($_SESSION['permission']['Reports']['view'] == '1')
            {
                $data['schedule_records'] = $this->db->query('SELECT * from reports_schedule where user_id='.$_SESSION['id'].' and reports_type="user_performance"')->row_array();
                $data['tasks'] = $this->Report_model->getTasks();
                $data['firm_users'] = $this->Report_model->getFirmUsers();
                
                $arr = array();     
                $i = 0;

                foreach($data['tasks'] as $key => $value) 
                { 
                   $task_assignees = Get_Module_Assigees('TASK',$value['id']);
                   $assignees_name = array();
                   $duration = array();             

                   foreach($data['firm_users'] as $key1 => $value1) 
                   {
                      foreach($task_assignees as $key2 => $value2)
                      { 
                         if($value1['id'] == $value2)
                         { 
                             $assignees_name[] = ucwords($this->Common_mdl->getUserProfileName($value2));
                             $time = $this->Common_mdl->get_working_hours($value['id'],$value2);

                             if($time>0)
                             {
                               $duration[$value2] = $time;
                             }  
                         }    
                      }  
                   } 
                
                   if(count($assignees_name)>0)
                   {
                      $arr[$i]['id'] = $value['id'];
                      $arr[$i]['subject'] = $value['subject'];                 
                      $arr[$i]['status'] = $value['task_status'];
                      $arr[$i]['staff_name'] = implode(',', array_unique($assignees_name));
                      $arr[$i]['hours_worked'] = round(array_sum($duration),2);
                      $i++; 
                   }                   
                   
                }              
                    
                $data['tasks_list'] = $arr;          
                $data['leads']=$this->Report_model->getLeads();

                $data['groups'] = $this->db->query("SELECT * from `Groups` where user_id='".$_SESSION['id']."'")->result_array();  
                $data['getallUser'] = $this->Report_model->getClients();
                $data['column_setting']=$this->Common_mdl->getallrecords('column_setting_new');       

                $this->load->view('reports/performance_dashboard',$data);    
            }
            else
            {
                $this->load->view('users/blank_page');
            }    
        }

        public function pdf_download(){  
            if(isset($_POST['image']))
            {
              $this->session->set_userdata('image',$_POST['image']);            
            }                
        }
        public function pdf_download_sess(){  
            if(isset($_POST['image_sess']))
            {
              $this->session->set_userdata('image_sess',$_POST['image_sess']);            
            }                
        }

        public function task_chart(){
               if(isset($_POST['chart_image']))
            {
                $this->session->set_userdata('chart_image',$_POST['chart_image']);            
            }  
        }
        public function task_chart_sess(){
               if(isset($_POST['chart_image_sess']))
            {
                $this->session->set_userdata('chart_image_sess',$_POST['chart_image_sess']);            
            }  
        }
        public function chart(){          
            $data['records']=$this->Report_model->client_custom_list();  

            $this->load->view('reports/client_customresult',$data);                       
            // $this->load->library('Tc');
            // $pdf = new Tc('P', 'mm', 'A4', true, 'UTF-8', false); 
            // $pdf->AddPage('L','A4');
            // $html = $this->load->view('reports/test_pdf',$data,true);      
            // $pdf->SetTitle('Client_list');
            // $pdf->SetHeaderMargin(30);
            // $pdf->SetTopMargin(20);
            // $pdf->setFooterMargin(20);
            // $pdf->SetAutoPageBreak(true, 20);
            // $pdf->SetAuthor('Author');
            // $pdf->SetDisplayMode('real', 'default');
            // $pdf->WriteHTML($html);          
            // $pdf->Output('Client_list.pdf', 'D'); 
        }
        public function chart_sess(){            
                $legal_form=$_SESSION['client_legal_form'];
                $status=$_SESSION['client_status'];
                $last_Year_start=$_SESSION['client_last_Year_start'];
                $last_month_end=$_SESSION['client_last_month_end'];
                if($legal_form=='all'){
                    if($status=='all'){                    
                        $data['client']=$this->Report_model->client_list1($legal_form,'or',$last_Year_start,$last_month_end,'or',$status);  
                    }else{                 
                        $data['client']=$this->Report_model->client_list1($legal_form,'or',$last_Year_start,$last_month_end,'and',$status);  
                    } 
                }else{
                    if($status=='all'){                    
                        $data['client']=$this->Report_model->client_list1($legal_form,'and',$last_Year_start,$last_month_end,'or',$status);  
                    }else{                 
                        $data['client']=$this->Report_model->client_list1($legal_form,'and',$last_Year_start,$last_month_end,'and',$status);  
                    } 
                }                          
            $this->load->library('Tc');
            $pdf = new Tc('P', 'mm', 'A4', true, 'UTF-8', false); 
            $pdf->AddPage('L','A4');
            if($_SESSION['client_legal_form']!='all'){
            $html = $this->load->view('reports/client_report',$data,true);  
            }else{
             $html = $this->load->view('reports/test_pdf',$data,true);      
            }    
            $pdf->SetTitle('Client_list');
            $pdf->SetHeaderMargin(30);
            $pdf->SetTopMargin(20);
            $pdf->setFooterMargin(20);
            $pdf->SetAutoPageBreak(true, 20);
            $pdf->SetAuthor('Author');
            $pdf->SetDisplayMode('real', 'default');
            $pdf->WriteHTML($html);          
            $pdf->Output('Client_list.pdf', 'D'); 
        }

        public function Client_customization(){
            $last_Year_start=$_POST['fromdate'];
            $last_month_end=$_POST['todate'];
            $legal_form=$_POST['legal_form'];
            $status=$_POST['status'];
            if($_POST['fromdate']==''){
                $last_Year_start = date('Y-01-01');                         
            }
            if($_POST['todate']==''){
                $last_month_end  = date('Y-12-31');
            }
            $this->session->set_userdata('client_last_Year_start',$last_Year_start);
            $this->session->set_userdata('client_last_month_end',$last_month_end);
            $this->session->set_userdata('client_legal_form',$legal_form);
            $this->session->set_userdata('client_status',$status);
            if($legal_form!='all'){
                if($status=='all'){  
                if($legal_form=='Public Limited company')  {               
                    $data['Public']=$this->Report_model->details_list1('Public Limited company',$last_Year_start,$last_month_end,'or',$status);
                     $data['others']=$this->Report_model->details_list2('Public Limited company',$last_Year_start,$last_month_end,'or',$status);
                }
                if($legal_form=='Limited Liability Partnership')  {    
                $data['limited']=$this->Report_model->details_list1('Limited Liability Partnership',$last_Year_start,$last_month_end,'or',$status );
                 $data['others']=$this->Report_model->details_list2('Limited Liability Partnership',$last_Year_start,$last_month_end,'or',$status );
                }
                if($legal_form=='Private Limited company')  {    
                $data['Private']=$this->Report_model->details_list1('Private Limited company',$last_Year_start,$last_month_end,'or',$status); 
                 $data['others']=$this->Report_model->details_list2('Private Limited company',$last_Year_start,$last_month_end,'or',$status); 
                }
                if($legal_form=='Partnership')  {    
                $data['Partnership']=$this->Report_model->details_list1('Partnership',$last_Year_start,$last_month_end,'or',$status);
                  $data['others']=$this->Report_model->details_list2('Partnership',$last_Year_start,$last_month_end,'or',$status);
                }
                if($legal_form=='Self Assessment')  {    
                $data['self']=$this->Report_model->details_list1('Self Assessment',$last_Year_start,$last_month_end,'or',$status);
                $data['others']=$this->Report_model->details_list2('Self Assessment',$last_Year_start,$last_month_end,'or',$status);
                }
                if($legal_form=='Trust')  {    
                $data['Trust']=$this->Report_model->details_list1('Trust',$last_Year_start,$last_month_end,'or',$status);
                $data['others']=$this->Report_model->details_list2('Trust',$last_Year_start,$last_month_end,'or',$status);
                }
                if($legal_form=='Charity')  {    
                $data['Charity']=$this->Report_model->details_list1('Charity',$last_Year_start,$last_month_end,'or',$status);
                $data['others']=$this->Report_model->details_list2('Charity',$last_Year_start,$last_month_end,'or',$status);
                }
                if($legal_form=='Other')  {    
                $data['Other']=$this->Report_model->details_list1('Other',$last_Year_start,$last_month_end,'or',$status); 
                $data['others']=$this->Report_model->details_list2('Other',$last_Year_start,$last_month_end,'or',$status); 
                }
            }else{                 
               if($legal_form=='Public Limited company')  {               
                    $data['Public']=$this->Report_model->details_list1('Public Limited company',$last_Year_start,$last_month_end,'and',$status);
                     $data['others']=$this->Report_model->details_list2('Public Limited company',$last_Year_start,$last_month_end,'and',$status);
                }
                if($legal_form=='Limited Liability Partnership')  {    
                $data['limited']=$this->Report_model->details_list1('Limited Liability Partnership',$last_Year_start,$last_month_end,'and',$status );
                 $data['limited']=$this->Report_model->details_list2('Limited Liability Partnership',$last_Year_start,$last_month_end,'and',$status );
                }
                if($legal_form=='Private Limited company')  {    
                $data['Private']=$this->Report_model->details_list1('Private Limited company',$last_Year_start,$last_month_end,'and',$status); 
                $data['others']=$this->Report_model->details_list2('Private Limited company',$last_Year_start,$last_month_end,'and',$status); 
                }
                if($legal_form=='Partnership')  {    
                $data['Partnership']=$this->Report_model->details_list1('Partnership',$last_Year_start,$last_month_end,'and',$status);
                 $data['others']=$this->Report_model->details_list2('Partnership',$last_Year_start,$last_month_end,'and',$status);
                }
                if($legal_form=='Self Assessment')  {    
                $data['self']=$this->Report_model->details_list1('Self Assessment',$last_Year_start,$last_month_end,'and',$status);
                 $data['others']=$this->Report_model->details_list2('Self Assessment',$last_Year_start,$last_month_end,'and',$status);
                }
                if($legal_form=='Trust')  {    
                $data['Trust']=$this->Report_model->details_list1('Trust',$last_Year_start,$last_month_end,'and',$status);
                 $data['others']=$this->Report_model->details_list2('Trust',$last_Year_start,$last_month_end,'and',$status);
                }
                if($legal_form=='Charity')  {    
                $data['Charity']=$this->Report_model->details_list1('Charity',$last_Year_start,$last_month_end,'and',$status);
                $data['others']=$this->Report_model->details_list2('Charity',$last_Year_start,$last_month_end,'and',$status);
                }
                if($legal_form=='Other')  {    
                $data['Other']=$this->Report_model->details_list1('Other',$last_Year_start,$last_month_end,'and',$status); 
                 $data['others']=$this->Report_model->details_list2('Other',$last_Year_start,$last_month_end,'and',$status); 
                }
            }                                
                $this->load->view('reports/legal_form_success',$data);   
            }else{
               // $this->session->unset_userdata('image');                  
                $status='1';
                echo $status;
            }
        }
        public function excel_download(){   

            $filename = 'client_'.date('Ymd').'.xls';           
            $data['client']=$this->Report_model->client_list(); 
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/csv; "); 
            $file = fopen('php://output', 'w'); 
            $header = array("S.NO","Name","Updates","Active","CompanyStatus","User Type","Status");
            fputcsv($file, $header);
            $i=0;  
              foreach ($data['client'] as $key=>$line){
                $user_id=$line['user_id'];
            if($line['user_id']){ $i++; $line['user_id']=$i; }    
            $update=$this->Common_mdl->select_record('update_client','user_id',$user_id);
            $company_status=$this->Common_mdl->select_record('client','user_id',$user_id);
            $getallUservalue=$this->Common_mdl->select_record('user','id',$user_id);
            $role=$this->Common_mdl->select_record('Role','id',$getallUservalue['role']);
            $line['crm_name']=$getallUservalue['crm_name'];
            if($update['counts']!=''){
               $line['updates']= $update['counts']; 
            } else{
               $line['updates']='0';
            }
            if($getallUservalue['status']=='1'){ 
                 $line['status']="Active"; }
                  elseif($getallUservalue['status']=='0'){ 
                     $line['status']="Inactive"; 
                 }elseif($getallUservalue['status']=='3'){
                   $line['status']="Frozen"; 
               }else{ 
                 $line['status']='-';
            }

            $line['crm_company_status']=$company_status['crm_company_status'];
            $line['role']=$role['role'];

            if($getallUservalue['role']=='6'){
            $line['house']='Manual';
            }elseif($getallUservalue['role']=='4' && $company_status['status']==0 ){
             $line['house']='Manual';
            }elseif($getallUservalue['role']=='4' && $company_status['status']==1){
             $line['house']='Company House';
            }elseif($getallUservalue['role']=='4' && $company_status['status']==2){
             $line['house']='Import';
            }else{
                $line['house'] ='';
            }
           
              fputcsv($file,$line);
            }                       
            fclose($file);
            exit;     
           // $this->load->view('reports/client_excel',$data);
        }
        public function excel_download_sess(){            
                $legal_form=$_SESSION['client_legal_form'];
                $status=$_SESSION['client_status'];
                $last_Year_start=$_SESSION['client_last_Year_start'];
                $last_month_end=$_SESSION['client_last_month_end'];
                if($legal_form=='all'){
                    if($status=='all'){                    
                        $data['client']=$this->Report_model->client_list1($legal_form,'or',$last_Year_start,$last_month_end,'or',$status);  
                    }else{                 
                        $data['client']=$this->Report_model->client_list1($legal_form,'or',$last_Year_start,$last_month_end,'and',$status);  
                    } 
                }else{
                $data['client']=$this->Report_model->client_list1($legal_form,'and',$last_Year_start,$last_month_end,'or',$status);  
                }
            $filename = 'client_'.date('Ymd').'.xls';       
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/csv; "); 
            $file = fopen('php://output', 'w'); 
            $header = array("S.NO","Name","Updates","Active","CompanyStatus","User Type","Status");
            fputcsv($file, $header);
            $i=0;  
              foreach ($data['client'] as $key=>$line){
                $user_id=$line['user_id'];
            if($line['user_id']){ $i++; $line['user_id']=$i; }    
            $update=$this->Common_mdl->select_record('update_client','user_id',$user_id);
            $company_status=$this->Common_mdl->select_record('client','user_id',$user_id);
            $getallUservalue=$this->Common_mdl->select_record('user','id',$user_id);
            $role=$this->Common_mdl->select_record('Role','id',$getallUservalue['role']);
            $line['crm_name']=$getallUservalue['crm_name'];
            if($update['counts']!=''){
               $line['updates']= $update['counts']; 
            } else{
               $line['updates']='0';
            }
            if($getallUservalue['status']=='1'){ 
                 $line['status']="Active"; }
                  elseif($getallUservalue['status']=='0'){ 
                     $line['status']="Inactive"; 
                 }elseif($getallUservalue['status']=='3'){
                   $line['status']="Frozen"; 
               }else{ 
                 $line['status']='-';
            }

            $line['crm_company_status']=$company_status['crm_company_status'];
            $line['role']=$role['role'];

            if($getallUservalue['role']=='6'){
            $line['house']='Manual';
            }elseif($getallUservalue['role']=='4' && $company_status['status']==0 ){
             $line['house']='Manual';
            }elseif($getallUservalue['role']=='4' && $company_status['status']==1){
             $line['house']='Company House';
            }elseif($getallUservalue['role']=='4' && $company_status['status']==2){
             $line['house']='Import';
            }else{
                $line['house'] ='';
            }
           
              fputcsv($file,$line);
                    }                       
                    fclose($file);
                    exit;  
           // $this->load->view('reports/client_excel',$data);
        }
         public function task_pdf_download(){                             
            $data['task_list']=$this->Report_model->task_list();                         
            $this->load->library('Tc');
            $pdf = new Tc('P', 'mm', 'A4', true, 'UTF-8', false); 
            $pdf->AddPage('L','A4');
            $html = $this->load->view('reports/task_pdf',$data,true);      
            $pdf->SetTitle('Task list');
            $pdf->SetHeaderMargin(30);
            $pdf->SetTopMargin(20);
            $pdf->setFooterMargin(20);
            $pdf->SetAutoPageBreak(true, 20);
            $pdf->SetAuthor('Author');
            $pdf->SetDisplayMode('real', 'default');
            $pdf->WriteHTML($html);          
            $pdf->Output('Task_list.pdf', 'D'); 
        }

         public function task_pdf_download_sess(){                          
                $priority=$_SESSION['task_priority'];
                $status=$_SESSION['task_status'];
                $last_Year_start=$_SESSION['task_last_Year_start'];
                $last_month_end=$_SESSION['task_last_month_end'];
                if($status=='all'){
                    if($priority=='all'){                    
                        $data['task_list']=$this->Report_model->task_list1($status,'or',$last_Year_start,$last_month_end,'or',$priority);  
                    }else{                 
                        $data['task_list']=$this->Report_model->task_list1($status,'or',$last_Year_start,$last_month_end,'and',$priority);  
                    } 
                }else{
                    if($priority=='all'){                    
                        $data['task_list']=$this->Report_model->task_list1($status,'and',$last_Year_start,$last_month_end,'or',$priority);  
                    }else{                 
                        $data['task_list']=$this->Report_model->task_list1($status,'and',$last_Year_start,$last_month_end,'and',$priority);  
                    }                 
                }                           
            $this->load->library('Tc');
            $pdf = new Tc('P', 'mm', 'A4', true, 'UTF-8', false); 
            $pdf->AddPage('L','A4');
            if($_SESSION['task_status']!='all'){
            $html = $this->load->view('reports/task_report',$data,true); 
            }else{  
               $html = $this->load->view('reports/task_pdf',$data,true);  
            }     
            $pdf->SetTitle('Task list');
            $pdf->SetHeaderMargin(30);
            $pdf->SetTopMargin(20);
            $pdf->setFooterMargin(20);
            $pdf->SetAutoPageBreak(true, 20);
            $pdf->SetAuthor('Author');
            $pdf->SetDisplayMode('real', 'default');
            $pdf->WriteHTML($html);          
            $pdf->Output('Task_list.pdf', 'D'); 
        }

        public function exceltask_download(){                             
            $data['task_list']=$this->Report_model->task_list();      
             $filename = 'task_'.date('Ymd').'.xls';       
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/csv; "); 
            $file = fopen('php://output', 'w'); 
            $header = array("S.NO","Task Name","Start Date","Due Date","Status","Assignto");
            fputcsv($file, $header);
            $i=0;  
            $team_array=array();
            $dept_array=array();
            $assignto=array();
              foreach ($data['task_list'] as $key=>$line){ 

                  if($line['id']){ $i++; $line['id']=$i; }
                    if(isset($line['worker'])){
                     $explode_worker=explode(',',$line['worker']);
                        foreach($explode_worker as $key => $val){     
                        $getUserProfilepic = $this->Common_mdl->getUserProfileName($val);
                        array_push($assignto,$getUserProfilepic);
                        }                      
                    }
                    if(isset($value['team'])){
                     $explode_team=explode(',',$line['team']);
                     for($v=0;$v<count($explode_team);$v++){
                        $team = $this->db->query("select * from team where id=".$explode_team[$v]." ")->row_array();
                        array_push($team_array,$team['team']);
                     } 
                    }
                    if(isset($value['department'])){
                     $explode_department=explode(',',$line['department']);
                     for($z=0;$z<count($explode_department);$z++){
                       $department=$this->db->query("select * from department_permission where id=".$explode_department[$z]." ")->row_array();
                       array_push($dept_array,$department['new_dept']);
                     }                    
                    }
                    $line['worker']= implode(',',$assignto);
                    $line['team']= implode(',',$team_array);
                    $line['department']= implode(',',$dept_array);
                   fputcsv($file,$line);
                   $assignto=array(); 
                   $team_array=array();
                   $dept_array=array();
                    } 
                                         
                    fclose($file);
                    exit;          
           // $this->load->view('reports/task_excel',$data);               
        }

        public function exceltask_download_sess(){                       
                $priority=$_SESSION['task_priority'];
                $status=$_SESSION['task_status'];
                $last_Year_start=$_SESSION['task_last_Year_start'];
                $last_month_end=$_SESSION['task_last_month_end'];
                if($status=='all'){
                    if($priority=='all'){                    
                        $data['task_list']=$this->Report_model->task_list1($status,'or',$last_Year_start,$last_month_end,'or',$priority);  
                    }else{                 
                        $data['task_list']=$this->Report_model->task_list1($status,'or',$last_Year_start,$last_month_end,'and',$priority);  
                    } 
                }else{
                    if($priority=='all'){                    
                        $data['task_list']=$this->Report_model->task_list1($status,'and',$last_Year_start,$last_month_end,'or',$priority);  
                    }else{                 
                        $data['task_list']=$this->Report_model->task_list1($status,'and',$last_Year_start,$last_month_end,'and',$priority);  
                    }                 
                } 

            $filename = 'task_'.date('Ymd').'.xls';       
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/csv; "); 
            $file = fopen('php://output', 'w'); 
            $header = array("S.NO","Task Name","Start Date","Due Date","Status","Assignto");
            fputcsv($file, $header);
            $i=0;  
            $team_array=array();
            $dept_array=array();
            $assignto=array();
              foreach ($data['task_list'] as $key=>$line){   
                    $user_id=$line['id'];
                  if($line['id']){ $i++; $line['id']=$i; }
                    if(isset($line['worker'])){
                     $explode_worker=explode(',',$line['worker']);
                        foreach($explode_worker as $key => $val){     
                        $getUserProfilepic = $this->Common_mdl->getUserProfileName($val);
                        array_push($assignto,$getUserProfilepic);
                        }
                       // $assign_list=implode(',',$assignto);
                    }
                    if(isset($value['team'])){
                     $explode_team=explode(',',$line['team']);
                     for($v=0;$v<count($explode_team);$v++){
                        $team = $this->db->query("select * from team where id=".$explode_team[$v]." ")->row_array();
                        array_push($team_array,$team['team']);
                     } 
                    }
                    if(isset($value['department'])){
                     $explode_department=explode(',',$line['department']);
                     for($z=0;$z<count($explode_department);$z++){
                       $department=$this->db->query("select * from department_permission where id=".$explode_department[$z]." ")->row_array();
                       array_push($dept_array,$department['new_dept']);
                     }                    
                    }
                    $line['worker']= implode(',',$assignto);
                    $line['team']= implode(',',$team_array);
                    $line['department']= implode(',',$dept_array);
                   fputcsv($file,$line);
                   $assignto=array(); 
                   $team_array=array();
                   $dept_array=array();
                    } 
                                         
                    fclose($file);
                    exit;         
            //$this->load->view('reports/task_excel',$data);               
        }
         public function Task_customization(){
            $last_Year_start=$_POST['fromdate'];
            $last_month_end=$_POST['todate'];
            $priority=$_POST['priority'];
            $status=$_POST['status'];
            if($_POST['fromdate']==''){
                $last_Year_start = date('Y-01-01');                         
            }
            if($_POST['todate']==''){
                $last_month_end  = date('Y-12-31');
            }
            $this->session->set_userdata('task_last_Year_start',$last_Year_start);
            $this->session->set_userdata('task_last_month_end',$last_month_end);
            $this->session->set_userdata('task_priority',$_POST['priority']);
            $this->session->set_userdata('task_status',$_POST['status']);
            if($status!='all'){
                if($priority=='all'){  
                    if($status=='notstarted'){
                    $data['notstarted_tasks']=$this->Report_model->task_details_date('notstarted','and',$last_Year_start,$last_month_end,'or',$priority);
                     $data['others']=$this->Report_model->task_details_date1('notstarted','and',$last_Year_start,$last_month_end,'or',$priority);
                    }
                    if($status=='complete'){
                    $data['completed_tasks']=$this->Report_model->task_details_date('complete','and',$last_Year_start,$last_month_end,'or',$priority);
                    $data['others']=$this->Report_model->task_details_date1('complete','and',$last_Year_start,$last_month_end,'or',$priority);
                    }
                    if($status=='inprogress'){
                    $data['inprogress_tasks']=$this->Report_model->task_details_date('inprogress','and',$last_Year_start,$last_month_end,'or',$priority);
                     $data['others']=$this->Report_model->task_details_date1('inprogress','and',$last_Year_start,$last_month_end,'or',$priority);
                   
                    }
                    if($status=='awaiting'){
                    $data['awaiting_tasks']=$this->Report_model->task_details_date('awaiting','and',$last_Year_start,$last_month_end,'or',$priority);
                    $data['others']=$this->Report_model->task_details_date1('awaiting','and',$last_Year_start,$last_month_end,'or',$priority);
                    }
                    if($status=='testing'){
                    $data['testing_tasks']=$this->Report_model->task_details_date('testing','and',$last_Year_start,$last_month_end,'or',$priority);  
                     $data['others']=$this->Report_model->task_details_date1('testing','and',$last_Year_start,$last_month_end,'or',$priority); 
                    }         
            }else{                 
                   if($status=='notstarted'){
                    $data['notstarted_tasks']=$this->Report_model->task_details_date('notstarted','and',$last_Year_start,$last_month_end,'and',$priority);
                    $data['others']=$this->Report_model->task_details_date1('notstarted','and',$last_Year_start,$last_month_end,'and',$priority);
                    }
                    if($status=='complete'){
                    $data['completed_tasks']=$this->Report_model->task_details_date('complete','and',$last_Year_start,$last_month_end,'and',$priority);
                    $data['others']=$this->Report_model->task_details_date1('complete','and',$last_Year_start,$last_month_end,'and',$priority);
                   
                    }
                    if($status=='inprogress'){
                    $data['inprogress_tasks']=$this->Report_model->task_details_date('inprogress','and',$last_Year_start,$last_month_end,'and',$priority);
                     $data['others']=$this->Report_model->task_details_date1('inprogress','and',$last_Year_start,$last_month_end,'and',$priority);
                    }
                    if($status=='awaiting'){
                    $data['awaiting_tasks']=$this->Report_model->task_details_date('awaiting','and',$last_Year_start,$last_month_end,'and',$priority);
                     $data['others']=$this->Report_model->task_details_date1('awaiting','and',$last_Year_start,$last_month_end,'and',$priority);
                    }
                    if($status=='testing'){
                    $data['testing_tasks']=$this->Report_model->task_details_date('testing','and',$last_Year_start,$last_month_end,'and',$priority); 
                    $data['others']=$this->Report_model->task_details_date1('testing','and',$last_Year_start,$last_month_end,'and',$priority);  
                    }      
            }                                
                $this->load->view('reports/task_success',$data);   
            }else{  
                $status='1';
                echo $status;
            }
        }

        public function deadline_sess(){
            $status=$_SESSION['deadline_type'];
            $last_Year_start=$_SESSION['deadline_last_Year_start'];
            $last_month_end=$_SESSION['deadline_last_month_end'];
            $company_type=$_SESSION['company_type'];
          //  echo $status;

            if($status!=''){
              //  echo "not";
               // exit;
                    if($company_type!=''){
                    if($status=='conf_statement'){
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_confirmation_statement_due_date','conf_statement','and',$last_Year_start,$last_month_end,$company_type,'and');
                    }
                    if($status=='accounts'){
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_ch_accounts_next_due','accounts','and',$last_Year_start,$last_month_end,$company_type,'and');
                    }
                    if($status=='company_tax_return'){
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_accounts_tax_date_hmrc','company_tax_return','and',$last_Year_start,$last_month_end,$company_type,'and');
                    }
                    if($status=='personal_tax_return'){
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_personal_due_date_return','personal_tax_return','and',$last_Year_start,$last_month_end,$company_type,'and');
                    }
                    if($status=='payroll'){
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_rti_deadline','payroll','and',$last_Year_start,$last_month_end,$company_type,'and');
                    }
                    if($status=='workplace'){
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_pension_subm_due_date','workplace','and',$last_Year_start,$last_month_end,$company_type,'and');
                    }
                    if($status=='vat'){
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_vat_due_date','vat','and',$last_Year_start,$last_month_end,$company_type,'and');
                    }
                    if($status=='cis'){
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('','cis','and',$last_Year_start,$last_month_end,$company_type,'and');
                    }
                    if($status=='cissub'){
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('','cissub','and',$last_Year_start,$last_month_end,$company_type,'and');
                    }
                    if($status=='p11d'){
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_next_p11d_return_due','p11d','and',$last_Year_start,$last_month_end,$company_type,'and');
                    }
                    if($status=='bookkeep'){
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_next_booking_date','bookkeep','and',$last_Year_start,$last_month_end,$company_type,'and');
                    }
                    if($status=='management'){
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_next_manage_acc_date','management','and',$last_Year_start,$last_month_end,$company_type,'and');
                    }
                    if($status=='investgate'){
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_insurance_renew_date','investgate','and',$last_Year_start,$last_month_end,$company_type,'and');
                    }
                    if($status=='registered'){
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_registered_renew_date','registered','and',$last_Year_start,$last_month_end,$company_type,'and');
                    }
                    if($status=='taxadvice'){
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_investigation_end_date','taxadvice','and',$last_Year_start,$last_month_end,$company_type,'and');
                    }
                    if($status=='taxinvest'){
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_investigation_end_date','taxinvest','and',$last_Year_start,$last_month_end,$company_type,'and');
                    }
                    }else{
                        if($status=='conf_statement'){
                        $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_confirmation_statement_due_date','conf_statement','and',$last_Year_start,$last_month_end,$company_type,'or');
                        }
                        if($status=='accounts'){
                        $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_ch_accounts_next_due','accounts','and',$last_Year_start,$last_month_end,$company_type,'or');
                        }
                        if($status=='company_tax_return'){
                        $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_accounts_tax_date_hmrc','company_tax_return','and',$last_Year_start,$last_month_end,$company_type,'or');
                        }
                        if($status=='personal_tax_return'){
                        $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_personal_due_date_return','personal_tax_return','and',$last_Year_start,$last_month_end,$company_type,'or');
                        }
                        if($status=='payroll'){
                        $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_rti_deadline','payroll','and',$last_Year_start,$last_month_end,$company_type,'or');
                        }
                        if($status=='workplace'){
                        $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_pension_subm_due_date','workplace','and',$last_Year_start,$last_month_end,$company_type,'or');
                        }
                        if($status=='vat'){
                        $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_vat_due_date','vat','and',$last_Year_start,$last_month_end,$company_type,'or');
                        }
                        if($status=='cis'){
                        $data['getCompany_today']=$this->Report_model->deadline_records_new('','cis','and',$last_Year_start,$last_month_end,$company_type,'or');
                        }
                        if($status=='cissub'){
                        $data['getCompany_today']=$this->Report_model->deadline_records_new('','cissub','and',$last_Year_start,$last_month_end,$company_type,'or');
                        }
                        if($status=='p11d'){
                        $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_next_p11d_return_due','p11d','and',$last_Year_start,$last_month_end,$company_type,'or');
                        }
                        if($status=='bookkeep'){
                        $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_next_booking_date','bookkeep','and',$last_Year_start,$last_month_end,$company_type,'or');
                        }
                        if($status=='management'){
                        $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_next_manage_acc_date','management','and',$last_Year_start,$last_month_end,$company_type,'or');
                        }
                        if($status=='investgate'){
                        $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_insurance_renew_date','investgate','and',$last_Year_start,$last_month_end,$company_type,'or');
                        }
                        if($status=='registered'){
                        $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_registered_renew_date','registered','and',$last_Year_start,$last_month_end,$company_type,'or');
                        }
                        if($status=='taxadvice'){
                        $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_investigation_end_date','taxadvice','and',$last_Year_start,$last_month_end,$company_type,'or');
                        }
                        if($status=='taxinvest'){
                        $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_investigation_end_date','taxinvest','and',$last_Year_start,$last_month_end,$company_type,'or');
                        }  
                     }             
                }else{
                  //  echo "empty";
                   // exit;
                if($company_type!=''){
                   
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_confirmation_statement_due_date','conf_statement','or',$last_Year_start,$last_month_end,$company_type,'and');                  
                  
                }else{
               
                $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_confirmation_statement_due_date','conf_statement','or',$last_Year_start,$last_month_end,$company_type,'or');
              
            } 
           }
            $this->load->library('Tc');
            $pdf = new Tc('P', 'mm', 'A3', true, 'UTF-8', false); 
            $pdf->AddPage('P','A3');
           if($_SESSION['deadline_type']!=''){
                 $html = $this->load->view('reports/deadline_report',$data,true);    
           }else{
             $html = $this->load->view('reports/deadline_pdf',$data,true);    
           }             
            $pdf->SetTitle('Deadline list');
            $pdf->SetHeaderMargin(30);
            $pdf->SetTopMargin(20);
            $pdf->setFooterMargin(20);
            $pdf->SetAutoPageBreak(true,30);
            $pdf->SetAuthor('Author');
            $pdf->SetDisplayMode('real', 'default');
            $pdf->WriteHTML($html);          
            $pdf->Output('Deadline_list.pdf', 'D');  
        }

        public function exceldeadline_download_sess(){
            $status=$_SESSION['deadline_type'];
            $last_Year_start=$_SESSION['deadline_last_Year_start'];
            $last_month_end=$_SESSION['deadline_last_month_end'];
            $company_type=$_SESSION['company_type'];       
             if($status!=''){             
                    if($company_type!=''){
                    if($status=='conf_statement'){
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_confirmation_statement_due_date','conf_statement','and',$last_Year_start,$last_month_end,$company_type,'and');
                    }
                    if($status=='accounts'){
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_ch_accounts_next_due','accounts','and',$last_Year_start,$last_month_end,$company_type,'and');
                    }
                    if($status=='company_tax_return'){
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_accounts_tax_date_hmrc','company_tax_return','and',$last_Year_start,$last_month_end,$company_type,'and');
                    }
                    if($status=='personal_tax_return'){
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_personal_due_date_return','personal_tax_return','and',$last_Year_start,$last_month_end,$company_type,'and');
                    }
                    if($status=='payroll'){
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_rti_deadline','payroll','and',$last_Year_start,$last_month_end,$company_type,'and');
                    }
                    if($status=='workplace'){
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_pension_subm_due_date','workplace','and',$last_Year_start,$last_month_end,$company_type,'and');
                    }
                    if($status=='vat'){
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_vat_due_date','vat','and',$last_Year_start,$last_month_end,$company_type,'and');
                    }
                    if($status=='cis'){
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('','cis','and',$last_Year_start,$last_month_end,$company_type,'and');
                    }
                    if($status=='cissub'){
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('','cissub','and',$last_Year_start,$last_month_end,$company_type,'and');
                    }
                    if($status=='p11d'){
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_next_p11d_return_due','p11d','and',$last_Year_start,$last_month_end,$company_type,'and');
                    }
                    if($status=='bookkeep'){
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_next_booking_date','bookkeep','and',$last_Year_start,$last_month_end,$company_type,'and');
                    }
                    if($status=='management'){
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_next_manage_acc_date','management','and',$last_Year_start,$last_month_end,$company_type,'and');
                    }
                    if($status=='investgate'){
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_insurance_renew_date','investgate','and',$last_Year_start,$last_month_end,$company_type,'and');
                    }
                    if($status=='registered'){
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_registered_renew_date','registered','and',$last_Year_start,$last_month_end,$company_type,'and');
                    }
                    if($status=='taxadvice'){
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_investigation_end_date','taxadvice','and',$last_Year_start,$last_month_end,$company_type,'and');
                    }
                    if($status=='taxinvest'){
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_investigation_end_date','taxinvest','and',$last_Year_start,$last_month_end,$company_type,'and');
                    }
                    }else{
                        if($status=='conf_statement'){
                        $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_confirmation_statement_due_date','conf_statement','and',$last_Year_start,$last_month_end,$company_type,'or');
                        }
                        if($status=='accounts'){
                        $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_ch_accounts_next_due','accounts','and',$last_Year_start,$last_month_end,$company_type,'or');
                        }
                        if($status=='company_tax_return'){
                        $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_accounts_tax_date_hmrc','company_tax_return','and',$last_Year_start,$last_month_end,$company_type,'or');
                        }
                        if($status=='personal_tax_return'){
                        $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_personal_due_date_return','personal_tax_return','and',$last_Year_start,$last_month_end,$company_type,'or');
                        }
                        if($status=='payroll'){
                        $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_rti_deadline','payroll','and',$last_Year_start,$last_month_end,$company_type,'or');
                        }
                        if($status=='workplace'){
                        $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_pension_subm_due_date','workplace','and',$last_Year_start,$last_month_end,$company_type,'or');
                        }
                        if($status=='vat'){
                        $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_vat_due_date','vat','and',$last_Year_start,$last_month_end,$company_type,'or');
                        }
                        if($status=='cis'){
                        $data['getCompany_today']=$this->Report_model->deadline_records_new('','cis','and',$last_Year_start,$last_month_end,$company_type,'or');
                        }
                        if($status=='cissub'){
                        $data['getCompany_today']=$this->Report_model->deadline_records_new('','cissub','and',$last_Year_start,$last_month_end,$company_type,'or');
                        }
                        if($status=='p11d'){
                        $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_next_p11d_return_due','p11d','and',$last_Year_start,$last_month_end,$company_type,'or');
                        }
                        if($status=='bookkeep'){
                        $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_next_booking_date','bookkeep','and',$last_Year_start,$last_month_end,$company_type,'or');
                        }
                        if($status=='management'){
                        $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_next_manage_acc_date','management','and',$last_Year_start,$last_month_end,$company_type,'or');
                        }
                        if($status=='investgate'){
                        $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_insurance_renew_date','investgate','and',$last_Year_start,$last_month_end,$company_type,'or');
                        }
                        if($status=='registered'){
                        $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_registered_renew_date','registered','and',$last_Year_start,$last_month_end,$company_type,'or');
                        }
                        if($status=='taxadvice'){
                        $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_investigation_end_date','taxadvice','and',$last_Year_start,$last_month_end,$company_type,'or');
                        }
                        if($status=='taxinvest'){
                        $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_investigation_end_date','taxinvest','and',$last_Year_start,$last_month_end,$company_type,'or');
                        }  
                     }             
                }else{                 
                if($company_type!=''){                   
                    $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_confirmation_statement_due_date','conf_statement','or',$last_Year_start,$last_month_end,$company_type,'and');
                }else{
                $data['getCompany_today']=$this->Report_model->deadline_records_new('crm_confirmation_statement_due_date','conf_statement','or',$last_Year_start,$last_month_end,$company_type,'or');
              
            } 
           }
            if($_SESSION['deadline_type']!=''){
              $filename = 'deadline_'.date('Ymd').'.xls';       
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/csv; "); 
            $file = fopen('php://output', 'w'); 
            $header = array("Client Name","Client Type","Company Name","Deadline Type","Date");
            fputcsv($file, $header);
            $i=0;  
            foreach ($data['getCompany_today'] as $getCompanykey => $getCompanyvalue) {  
              $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
              $status=$_SESSION['deadline_type'];
              if($status=='conf_statement'){
               if($getCompanyvalue['crm_confirmation_statement_due_date']!=''){
              $value['client_name']=$getusername['crm_name'];
              $value['client_type']=$getCompanyvalue['crm_legal_form'];
              $value['company_name']=$getCompanyvalue['crm_company_name'];
              $value['deadline_type']='Confirmation statement Due Date';
              if(strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) !=''){
              $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])); 
              }else{
              $value['date']='';
              }
              fputcsv($file,$value);
              // confi
              } 
              }
          if($status=='accounts'){
          if($getCompanyvalue['crm_ch_accounts_next_due']!=''){ 
            $value['client_name']=$getusername['crm_name'];
            $value['client_type']=$getCompanyvalue['crm_legal_form'];
            $value['company_name']=$getCompanyvalue['crm_company_name'];
            $value['deadline_type']='Accounts Due Date';
            if(strtotime($getCompanyvalue['crm_ch_accounts_next_due']) !=''){
             $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due']));
            }else{
             $value['date']='';
            } 
            fputcsv($file,$value);
          // account -->
          } 
          }
          if($status=='company_tax_return'){ 
          if($getCompanyvalue['crm_accounts_tax_date_hmrc']!=''){
            $value['client_name']=$value['client_name']=$getusername['crm_name'];
            $value['client_type']=$getCompanyvalue['crm_legal_form'];
            $value['company_name']=$getCompanyvalue['crm_company_name'];
            $value['deadline_type']='Company Tax Return Due Date';
            if(strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) !=''){
              $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])); 
            }else{
              $value['date']='';
            }
            fputcsv($file,$value);
          //company tax 
          } 
          }
          if($status=='personal_tax_return'){
          if($getCompanyvalue['crm_personal_due_date_return']!=''){
            $value['client_name']=$getusername['crm_name'];
            $value['client_type']=$getCompanyvalue['crm_legal_form'];
            $value['company_name']=$getCompanyvalue['crm_company_name'];
            $value['deadline_type']='Personal Tax Return Due Date';
            if(strtotime($getCompanyvalue['crm_personal_due_date_return']) !=''){
               $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])); 
            }else{
               $value['date']='';
            }
            fputcsv($file,$value);
                      //crm personal tax 
          } 
          }
          if($status=='vat'){
          if($getCompanyvalue['crm_vat_due_date']!=''){ 
            $value['client_name']=$getusername['crm_name'];
            $value['client_type']=$getCompanyvalue['crm_legal_form'];
            $value['company_name']=$getCompanyvalue['crm_company_name'];
            $value['deadline_type']='VAT Due Date';
            if(strtotime($getCompanyvalue['crm_vat_due_date']) !=''){
             $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date']));
            }else{
             $value['date']=''; 
            }    
            fputcsv($file,$value);
            // vat 
          } 
          }
          if($status=='payroll'){
          if($getCompanyvalue['crm_rti_deadline']!=''){ 
            $value['client_name']=$getusername['crm_name'];
            $value['client_type']=$getCompanyvalue['crm_legal_form'];
            $value['company_name']=$getCompanyvalue['crm_company_name'];
            $value['deadline_type']='Payroll Due Date';
            if(strtotime($getCompanyvalue['crm_rti_deadline']) !=''){
              $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline']));
            }else{
              $value['date']='';
            } 
            fputcsv($file,$value);
          // payrol
          } 
          }
          if($status=='workplace'){
          if($getCompanyvalue['crm_pension_subm_due_date']!=''){
            $value['client_name']=$getusername['crm_name'];
            $value['client_type']=$getCompanyvalue['crm_legal_form'];
            $value['company_name']=$getCompanyvalue['crm_company_name'];
            $value['deadline_type']='WorkPlace Pension - AE Due Date';
            if(strtotime($getCompanyvalue['crm_pension_subm_due_date']) !=''){
              $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date']));
            } else{
              $value['date']='';
            } 
            fputcsv($file,$value);  
          //workplace 
          } 
          }
          if($status=='cis'){
          $value['client_name']=$getusername['crm_name'];
          $value['client_type']= $getCompanyvalue['crm_legal_form'];
          $value['company_name']=$getCompanyvalue['crm_company_name'];
          $value['deadline_type']='CIS - Contractor Due Date';
          $value['date']='';
          fputcsv($file,$value);
          }
          if($status=='cissub'){
          $value['client_name']=$getusername['crm_name'];
          $value['client_type']= $getCompanyvalue['crm_legal_form'];
          $value['company_name']=$getCompanyvalue['crm_company_name'];
          $value['deadline_type']='CIS - Sub Contractor Due Date';
          $value['date']='';
          fputcsv($file,$value);
          // CIS - Sub Contractor
          }
          if($status=='p11d'){ 
           if($getCompanyvalue['crm_next_p11d_return_due']!=''){ 
              $value['client_name']=$getusername['crm_name'];
              $value['client_type']=$getCompanyvalue['crm_legal_form'];
              $value['company_name']=$getCompanyvalue['crm_company_name'];
              $value['deadline_type']='P11D Due Date';
              if(strtotime($getCompanyvalue['crm_next_p11d_return_due']) !=''){ 
                 $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due']));
              }else{
                 $value['date']='';
              } 
              fputcsv($file,$value);
         // p11d 
           } 
           }
           if($status=='management'){
          if($getCompanyvalue['crm_next_manage_acc_date']!=''){ 
          $value['client_name']= $getusername['crm_name'];
          $value['client_type']= $getCompanyvalue['crm_legal_form'];
          $value['company_name']= $getCompanyvalue['crm_company_name'];
          $value['deadline_type']='Management Accounts Due Date';
            if(strtotime($getCompanyvalue['crm_next_manage_acc_date']) !=''){
               $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date']));
            }else{
               $value['date']='';
            } 
            fputcsv($file,$value);
          // manage account 
          } 
          }
          if($status=='bookkeep'){
          if($getCompanyvalue['crm_next_booking_date']!=''){ 
          $value['client_name']=$getusername['crm_name'];
          $value['client_type']=$getCompanyvalue['crm_legal_form'];
          $value['company_name']=$getCompanyvalue['crm_company_name'];
          $value['deadline_type']='Bookkeeping Due Date';
          if(strtotime($getCompanyvalue['crm_next_booking_date']) !=''){
          $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date']));
          }else{
          $value['date']='';
          } 
          fputcsv($file,$value);
          // booking
          } 
          }
          if($status=='investgate'){
          if($getCompanyvalue['crm_insurance_renew_date']!=''){
          $value['client_name']=$getusername['crm_name'];
          $value['client_type']=$getCompanyvalue['crm_legal_form'];
          $value['company_name']= $getCompanyvalue['crm_company_name'];
          $value['deadline_type']='Investigation Insurance Due Date';
            if(strtotime($getCompanyvalue['crm_insurance_renew_date']) !=''){
             $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date']));
            } else{
              $value['date']=''; 
            }
            fputcsv($file,$value);
          }
          }
          if($status=='registered'){
          if($getCompanyvalue['crm_registered_renew_date']!=''){ 
            $value['client_name']=$getusername['crm_name'];
            $value['client_type']=$getCompanyvalue['crm_legal_form'];
            $value['company_name']=$getCompanyvalue['crm_company_name'];
            $value['deadline_type']='Registered Address Due Date';
            if(strtotime($getCompanyvalue['crm_registered_renew_date']) !='') {
            $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date']));
            } else{
            $value['date']='';
            } 
            fputcsv($file,$value);
            // registed 
          } 
          }
          if($status=='taxadvice'){
          if($getCompanyvalue['crm_investigation_end_date']!=''){
             $value['client_name']=$getusername['crm_name'];
             $value['client_type']=$getCompanyvalue['crm_legal_form'];
             $value['company_name']=$getCompanyvalue['crm_company_name'];
             $value['deadline_type']='Tax Advice Due Date';
             if(strtotime($getCompanyvalue['crm_investigation_end_date'])!=''){
             $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date']));
             }else{
              $value['date']='';
             }  
            // tax advice 
             fputcsv($file,$value);
           } 
           }
           if($status=='taxinvest'){ 
          if($getCompanyvalue['crm_investigation_end_date']!=''){
            $value['client_name']=$getusername['crm_name'];
            $value['client_type']=$getCompanyvalue['crm_legal_form'];
            $value['company_name']=$getCompanyvalue['crm_company_name'];
            $value['deadline_type']='Tax Investigation Due Date';
            if(strtotime($getCompanyvalue['crm_investigation_end_date'])!=''){
            $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date']));
            }else{
            $value['date']='';
            }
          // tax inve  
          fputcsv($file,$value);           
          } 
          }
          
      }
      fclose($file);
      exit;  
            }else{
              $filename = 'deadline_'.date('Ymd').'.xls';       
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/csv; "); 
            $file = fopen('php://output', 'w'); 
            $header = array("Client Name","Client Type","Company Name","Deadline Type","Date");
            fputcsv($file, $header);
            $i=0;  
            foreach ($data['getCompany_today'] as $getCompanykey => $getCompanyvalue) {  
              $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
               if($getCompanyvalue['crm_confirmation_statement_due_date']!=''){
              $value['client_name']=$getusername['crm_name'];
              $value['client_type']=$getCompanyvalue['crm_legal_form'];
              $value['company_name']=$getCompanyvalue['crm_company_name'];
              $value['deadline_type']='Confirmation statement Due Date';
              if(strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) !=''){
              $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])); 
              }else{
              $value['date']='';
              }
              fputcsv($file,$value);
              // confi
              } 

          if($getCompanyvalue['crm_ch_accounts_next_due']!=''){ 
            $value['client_name']=$getusername['crm_name'];
            $value['client_type']=$getCompanyvalue['crm_legal_form'];
            $value['company_name']=$getCompanyvalue['crm_company_name'];
            $value['deadline_type']='Accounts Due Date';
            if(strtotime($getCompanyvalue['crm_ch_accounts_next_due']) !=''){
             $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due']));
            }else{
             $value['date']='';
            } 
            fputcsv($file,$value);
          // account -->
          } 
          if($getCompanyvalue['crm_accounts_tax_date_hmrc']!=''){
            $value['client_name']=$value['client_name']=$getusername['crm_name'];
            $value['client_type']=$getCompanyvalue['crm_legal_form'];
            $value['company_name']=$getCompanyvalue['crm_company_name'];
            $value['deadline_type']='Company Tax Return Due Date';
            if(strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) !=''){
              $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])); 
            }else{
              $value['date']='';
            }
            fputcsv($file,$value);
          //company tax 
          } 
          if($getCompanyvalue['crm_personal_due_date_return']!=''){
            $value['client_name']=$getusername['crm_name'];
            $value['client_type']=$getCompanyvalue['crm_legal_form'];
            $value['company_name']=$getCompanyvalue['crm_company_name'];
            $value['deadline_type']='Personal Tax Return Due Date';
            if(strtotime($getCompanyvalue['crm_personal_due_date_return']) !=''){
               $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])); 
            }else{
               $value['date']='';
            }
            fputcsv($file,$value);
                      //crm personal tax 
          } 

          if($getCompanyvalue['crm_vat_due_date']!=''){ 
            $value['client_name']=$getusername['crm_name'];
            $value['client_type']=$getCompanyvalue['crm_legal_form'];
            $value['company_name']=$getCompanyvalue['crm_company_name'];
            $value['deadline_type']='VAT Due Date';
            if(strtotime($getCompanyvalue['crm_vat_due_date']) !=''){
             $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date']));
            }else{
             $value['date']=''; 
            }    
            fputcsv($file,$value);
            // vat 
          } 
          if($getCompanyvalue['crm_rti_deadline']!=''){ 
            $value['client_name']=$getusername['crm_name'];
            $value['client_type']=$getCompanyvalue['crm_legal_form'];
            $value['company_name']=$getCompanyvalue['crm_company_name'];
            $value['deadline_type']='Payroll Due Date';
            if(strtotime($getCompanyvalue['crm_rti_deadline']) !=''){
              $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline']));
            }else{
              $value['date']='';
            } 
            fputcsv($file,$value);
          // payrol
          } 

          if($getCompanyvalue['crm_pension_subm_due_date']!=''){
            $value['client_name']=$getusername['crm_name'];
            $value['client_type']=$getCompanyvalue['crm_legal_form'];
            $value['company_name']=$getCompanyvalue['crm_company_name'];
            $value['deadline_type']='WorkPlace Pension - AE Due Date';
            if(strtotime($getCompanyvalue['crm_pension_subm_due_date']) !=''){
              $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date']));
            } else{
              $value['date']='';
            } 
            fputcsv($file,$value);  
          //workplace 
          } 

          $value['client_name']=$getusername['crm_name'];
          $value['client_type']= $getCompanyvalue['crm_legal_form'];
          $value['company_name']=$getCompanyvalue['crm_company_name'];
          $value['deadline_type']='CIS - Contractor Due Date';
          $value['date']='';
          fputcsv($file,$value);

          $value['client_name']=$getusername['crm_name'];
          $value['client_type']= $getCompanyvalue['crm_legal_form'];
          $value['company_name']=$getCompanyvalue['crm_company_name'];
          $value['deadline_type']='CIS - Sub Contractor Due Date';
          $value['date']='';
          fputcsv($file,$value);
          // CIS - Sub Contractor

           if($getCompanyvalue['crm_next_p11d_return_due']!=''){ 
              $value['client_name']=$getusername['crm_name'];
              $value['client_type']=$getCompanyvalue['crm_legal_form'];
              $value['company_name']=$getCompanyvalue['crm_company_name'];
              $value['deadline_type']='P11D Due Date';
              if(strtotime($getCompanyvalue['crm_next_p11d_return_due']) !=''){ 
                 $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due']));
              }else{
                 $value['date']='';
              } 
              fputcsv($file,$value);
         // p11d 
           } 
          if($getCompanyvalue['crm_next_manage_acc_date']!=''){ 
          $value['client_name']= $getusername['crm_name'];
          $value['client_type']= $getCompanyvalue['crm_legal_form'];
          $value['company_name']= $getCompanyvalue['crm_company_name'];
          $value['deadline_type']='Management Accounts Due Date';
            if(strtotime($getCompanyvalue['crm_next_manage_acc_date']) !=''){
               $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date']));
            }else{
               $value['date']='';
            } 
            fputcsv($file,$value);
          // manage account 
          } 
          if($getCompanyvalue['crm_next_booking_date']!=''){ 
          $value['client_name']=$getusername['crm_name'];
          $value['client_type']=$getCompanyvalue['crm_legal_form'];
          $value['company_name']=$getCompanyvalue['crm_company_name'];
          $value['deadline_type']='Bookkeeping Due Date';
          if(strtotime($getCompanyvalue['crm_next_booking_date']) !=''){
          $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date']));
          }else{
          $value['date']='';
          } 
          fputcsv($file,$value);
          // booking
          } 
          if($getCompanyvalue['crm_insurance_renew_date']!=''){
          $value['client_name']=$getusername['crm_name'];
          $value['client_type']=$getCompanyvalue['crm_legal_form'];
          $value['company_name']= $getCompanyvalue['crm_company_name'];
          $value['deadline_type']='Investigation Insurance Due Date';
            if(strtotime($getCompanyvalue['crm_insurance_renew_date']) !=''){
             $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date']));
            } else{
              $value['date']=''; 
            }
            fputcsv($file,$value);
          }

          if($getCompanyvalue['crm_registered_renew_date']!=''){ 
            $value['client_name']=$getusername['crm_name'];
            $value['client_type']=$getCompanyvalue['crm_legal_form'];
            $value['company_name']=$getCompanyvalue['crm_company_name'];
            $value['deadline_type']='Registered Address Due Date';
            if(strtotime($getCompanyvalue['crm_registered_renew_date']) !='') {
            $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date']));
            } else{
            $value['date']='';
            } 
            fputcsv($file,$value);
            // registed 
          } 
          if($getCompanyvalue['crm_investigation_end_date']!=''){
             $value['client_name']=$getusername['crm_name'];
             $value['client_type']=$getCompanyvalue['crm_legal_form'];
             $value['company_name']=$getCompanyvalue['crm_company_name'];
             $value['deadline_type']='Tax Advice Due Date';
             if(strtotime($getCompanyvalue['crm_investigation_end_date'])!=''){
             $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date']));
             }else{
              $value['date']='';
             }  
            // tax advice 
             fputcsv($file,$value);
           } 
          if($getCompanyvalue['crm_investigation_end_date']!=''){
            $value['client_name']=$getusername['crm_name'];
            $value['client_type']=$getCompanyvalue['crm_legal_form'];
            $value['company_name']=$getCompanyvalue['crm_company_name'];
            $value['deadline_type']='Tax Investigation Due Date';
            if(strtotime($getCompanyvalue['crm_investigation_end_date'])!=''){
            $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date']));
            }else{
            $value['date']='';
            }
          // tax inve  
          fputcsv($file,$value);           
          } 
          
      }
      fclose($file);
      exit;  
            }
        }


        

      public function performance_customisation()
      {
         $this->Security_model->chk_login();   
      
         if($_SESSION['permission']['Reports']['view'] == '1')
         {
             $data['client']=$this->Report_model->client_list(); 
             $data['firm_users'] = $this->Report_model->getFirmUsers();
                          
             $this->load->view('reports/Performance_customisation',$data);
         }
         else
         {
             $this->load->view('users/blank_page');
         }
      }



} ?>
