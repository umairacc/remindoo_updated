<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Firm extends CI_Controller {
 //public $companieshouseAPIkey = "eHgKR3qG51_yHpL0-VSiM97QK-H6Z7fYiienSlzv";
 public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
 public $Firm_Status =  [
                          'all' => ['label'=>'All','count'=>0],
                          0 => ['label'=>'Non-Activeted','count'=>0],
                          1 => ['label'=>'Active','count'=>0],
                          2 => ['label'=>'Frozen','count'=>0],
                          3 => ['label'=>'Archive','count'=>0] //3 also check in firm_list page staticly
                        ];

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Common_mdl','Loginchk_model','Task_invoice_model','Task_creating_model'));
    }
    public function index()
    {

      $this->Loginchk_model->superAdmin_LoginCheck();
            
      $data['firm_admin'] = $this->db->query("select u.* from user u inner join firm f on f.user_id = u.id and f.is_deleted !=1 where user_type = 'FA' ")->result_array();

      if(count( $data['firm_admin'] ))
      {        
        $FirmAdmin_Ids = implode( ',' , array_column( $data['firm_admin'] ,'id' ) );

        $data['firm'] = $this->db->query("select * from firm where user_id  IN (".$FirmAdmin_Ids.")")->result_array();
      }    
      $this->Update_Firm_StatusCount_var();      
      $data['Firms_Status'] = $this->Firm_Status;

      $this->load->view('Firm/firm_list' , $data);
    }
    public function add_firm($user_id=false)
    {
        //this page contains many unnecessary code reson this copy from add client module

        $this->Loginchk_model->superAdmin_LoginCheck();
        // $data['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user','role','1');
       
        $data['firm'] = current ( $this->Common_mdl->GetAllWithWhere('firm','user_id',$user_id) );  
       
        $firm_admin_id =  $_SESSION['id'];

          if($user_id!='')
          {
              $datas['status'] = '1';
              $user['company_roles'] = '1';
              $erc = $this->db->query("UPDATE `firm` SET `status` = 1  WHERE `user_id` = $user_id ");     
              $this->Common_mdl->update('user',$user,'id',$user_id);
              $data['contactRec']= $this->db->query("SELECT * FROM  `firm_contacts` WHERE user_id =  '".$user_id."' ORDER BY make_primary DESC ")->result_array();
          }
          else
          {
              $data['contactRec'] = array();
          }
          $data['user'] = $this->Common_mdl->GetAllWithWhere('user','id',$user_id); 
         /** whose refer the firm **/
            $query1 = $this->db->query("SELECT * FROM `user` where role=2 AND autosave_status!='1' and crm_name!='' and firm_admin_id=".$_SESSION['id']." and status=1 order by id DESC");
            $results1 = $query1->result_array();
            $res=array();
            if(count($results1)>0)
            {
              foreach ($results1 as $key => $value) 
              {
                   array_push($res, $value['id']);
              }  
            }
            if(!empty($res))
            {
              $im_val=implode(',',$res);           
              $data['referby'] = $this->db->query("SELECT * FROM firm WHERE autosave_status=0 and user_id in (".$im_val.") order by firm_id desc ")->result_array();         
            }
           else
           {
             $data['referby']=array();
           }
          /** end of 21-08-2018 **/
       // $data['currency_set'] = $this->Common_mdl->GetAllWithWhere('admin_setting','user_id',$_SESSION['id']);        
        //$data['teamlist']= $this->Common_mdl->getallrecords('team');
        //$data['deptlist']= $this->Common_mdl->getallrecords('department');     
       // $data['staff_form'] = $this->db->query("select * from user where role = 6 and firm_admin_id=".$firm_admin_id." ")->result_array();

     //   $data['managed_by'] = $this->db->query("select * from user where role in(5,3) and firm_admin_id=".$firm_admin_id." ")->result_array();
       // $data['new_department'] = $this->db->query( "SELECT * FROM department_permission WHERE new_dept!=''")->result_array();

        if($user_id!='')
        {
            $data['user_ids'] = $user_id;
        }
        else
        {
            $data['user_ids'] = '';
        }

       // $data['template_details'] = $this->db->query("select * from proposal_proposaltemplate where user_id='".$_SESSION['id']."' and reminder_heading=1")->result_array();
      //  $data['client_hashtag'] = $this->Common_mdl->GetAllWithWhere('client_hashtag','user_id',$_SESSION['id']);

        $this->load->view('Firm/addnewFirm',$data);    
    }
  public function ChangeFirm_Status()
  {
    $Returndata = []; 

    if( $_POST['ids'] !='' && $_POST['status'] !='' )
    {
      $Ids = implode(',' , $_POST['ids']);

      if( $_POST['status'] == 'unarchive')
      {
         $this->db->query("update firm set status=@s:=status,status=old_status,old_status=@s where frim_id IN(".$Ids.") and old_status!=3");
      }
      else
      {
        $IsArchive = $IsArchiveCon ='';

        if( $_POST['status'] == 3 )
        {
          $IsArchive = "old_status=status,";
          $IsArchiveCon = "and status!=3";
        }
        $this->db->query("update firm set ".$IsArchive."status=".$_POST['status']." where firm_id IN (".$Ids.") ".$IsArchiveCon." "); 
      }
          
      $Returndata['result'] = $this->db->affected_rows();      
      $this->Update_Firm_StatusCount_var();
      $Returndata['status'] = $this->Firm_Status;

      echo json_encode( $Returndata );
    }
  }
  public function Update_Firm_StatusCount_var()
  {
    $Firm_Status = $this->Firm_Status; 

    $status_count = $this->db->query("select count(*) as num , status from firm where is_deleted!=1  GROUP by status")->result_array();  
      $All_count = 0;    
      foreach ( $status_count as $key => $value )
      {
        $Firm_Status[ $value['status'] ]['count'] = $value['num'];
        $All_count += $value['num'];
      }

      $Firm_Status['all']['count'] = $All_count;

      $this->Firm_Status = $Firm_Status;
  }


  public function login_us_firm($id,$admin_id)
  {
    session_destroy(); 
    $query=$this->db->query("select * from user where id=".$id."")->row_array(); 
    $data['username']=$query['username'];
    $data['password']=$query['confirm_password'];
    $_SESSION['last_login_id']=$admin_id;
    $this->load->view('Firm/firm_login',$data);
  }
  public function Update_firm_column_settings_order()
  {
    $firm=0;
    $i=1;
    $table = $_POST['table_name'];
    foreach ($_POST['reorder'] as $column) 
    {

      $this->db->update('firm_column_settings',["order_firm".$firm => $i],"table_name='".$table."' and name='".trim( $column )."'");
      $i++;
      echo $this->db->affected_rows();
    }
  }
  public function Update_firm_column_settings_visibility()
  {
    $firm=0;
    $table = $_POST['table_name'];
    $column = $_POST['column'];
    $state = $_POST['state'];
    $this->db->update('firm_column_settings',["visible_firm".$firm => $state],"table_name='".$table."' and name='".trim( $column )."'");
    echo $this->db->affected_rows();
  }

    public function insert_firm()
    {           
        $this->Loginchk_model->superAdmin_LoginCheck();        
        //$datas['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user','role','1');  

         if(!isset($_POST['user_ids']) || $_POST['user_ids']=='')
         {
         if($_POST['company_name']!='')
         {
            $var=rand(1,10);            
            $image_one = (isset($_POST['image_name'])&&($_POST['image_name']!=''))? $_POST['image_name'] : '825898.jpg';
            $image= (isset($_FILES['profile_image']['name'])&&($_FILES['profile_image']['name']!=''))? $this->Common_mdl->do_upload($_FILES['profile_image'], 'uploads') : $image_one ;
            $data['crm_name']= $_POST['company_name'];
            $data['username']= $_POST['user_name'];
            $data['crm_email_id']= $_POST['emailid'];
            $data['crm_phone_number']=$_POST['cun_code'].$_POST['pn_no_rec'];
            $data['password']=(isset($_POST['password'])&&($_POST['password']!=''))? md5($_POST['password']) : md5($var);
            $data['confirm_password']=(isset($_POST['confirm_password'])&&($_POST['confirm_password']!=''))? $_POST['confirm_password'] : $var;
            $data['role']=2;
            $data['status']=1;
            $data['company_roles']=(isset($_POST['company_house'])&&($_POST['company_house']!=''))? $_POST['company_house'] : 0;
            $data['CreatedTime']=time();
            $data['client_id']=(isset($_POST['client_id'])&&($_POST['client_id']!=''))? $_POST['client_id'] : rand(1,5);
           /* $data['crm_packages']=(isset($_POST['packages'])&&($_POST['packages']!=''))? $_POST['packages'] : '';
            $data['crm_gender']=(isset($_POST['gender'])&&($_POST['gender']!=''))? $_POST['gender'] : '';
*/            $data['crm_profile_pic']=$image;
            $data['autosave_status']=0;
            $data['firm_admin_id'] = $_SESSION['id'];  
            $data['firm']=1;
           $updates_user=(isset($_POST['user_id'])&&($_POST['user_id']=='0'|| $_POST['user_id']=='') ) ? $this->db->insert( 'user', $data ) : $this->Common_mdl->update('user',$data,'id',$_POST['user_id']);
           $insert_data= $updates_user;

            if($_POST['user_id']=='')
            {
                $_POST['user_id'] = $this->db->insert_id();
            }
              if($_POST['user_id']){

                $in=$this->db->insert_id();
                $data1['user_id']=(isset($_POST['user_id'])&&($_POST['user_id']==''))? $in : $_POST['user_id'];
                $_POST['pn_no_rec'] =$_POST['cun_code'].$_POST['pn_no_rec'];
              //  $this->send_sms($_POST['pn_no_rec']);
               // $data1['user_id']= $_SESSION['id'];
                //requried information
               $data1['crm_company_name']=(isset($_POST['company_name1'])&&($_POST['company_name1']!=''))? $_POST['company_name1']:'';
               $data1['crm_legal_form']=(isset($_POST['legal_form'])&&($_POST['legal_form']!=''))? $_POST['legal_form']:'';
               $data1['crm_allocation_holder']=(isset($_POST['allocation_holders'])&&($_POST['allocation_holders']!=''))? $_POST['allocation_holders']:'';
                //basic deatils
               $data1['crm_company_number']=(isset($_POST['company_number'])&&($_POST['company_number']!=''))? $_POST['company_number']:'';
               $data1['crm_company_url']=(isset($_POST['company_url'])&&($_POST['company_url']!=''))? $_POST['company_url']:'';
               $data1['crm_officers_url']=(isset($_POST['officers_url'])&&($_POST['officers_url']!=''))? $_POST['officers_url']:'';
               $data1['crm_company_name1']=(isset($_POST['company_name'])&&($_POST['company_name']!=''))? $_POST['company_name']:'';
               $data1['crm_register_address']=(isset($_POST['register_address'])&&($_POST['register_address']!=''))? $_POST['register_address']:'';
               $data1['crm_company_status']=(isset($_POST['company_status'])&&($_POST['company_status']!=''))? $_POST['company_status']:'';
               $data1['crm_company_type']=(isset($_POST['company_type'])&&($_POST['company_type']!=''))? $_POST['company_type']:'';
               $data1['crm_company_sic']=(isset($_POST['company_sic'])&&($_POST['company_sic']!=''))? $_POST['company_sic']:'';
               $data1['crm_sic_codes']=(isset($_POST['sic_codes'])&&($_POST['sic_codes']!=''))? $_POST['sic_codes']:'';
               $data1['crm_company_utr']=(isset($_POST['company_utr'])&&($_POST['company_utr']!=''))? $_POST['company_utr']:'';
               $data1['crm_incorporation_date']=(isset($_POST['date_of_creation'])&&($_POST['date_of_creation']!=''))? $_POST['date_of_creation']:'';
               $data1['crm_registered_in']=(isset($_POST['registered_in'])&&($_POST['registered_in']!=''))? $_POST['registered_in']:'';
               $data1['crm_address_line_one']=(isset($_POST['address_line_one'])&&($_POST['address_line_one']!=''))? $_POST['address_line_one']:'';
               $data1['crm_address_line_two']=(isset($_POST['address_line_two'])&&($_POST['address_line_two']!=''))? $_POST['address_line_two']:'';
               $data1['crm_address_line_three']=(isset($_POST['address_line_three'])&&($_POST['address_line_three']!=''))? $_POST['address_line_three']:'';
               $data1['crm_town_city']=(isset($_POST['crm_town_city'])&&($_POST['crm_town_city']!=''))? $_POST['crm_town_city']:'';
               $data1['crm_post_code']=(isset($_POST['crm_post_code'])&&($_POST['crm_post_code']!=''))? $_POST['crm_post_code']:'';
               $data1['crm_hashtag']=(isset($_POST['client_hashtag'])&&($_POST['client_hashtag']!=''))? $_POST['client_hashtag']:'';
               $data1['crm_accounts_office_reference']=(isset($_POST['accounts_office_reference'])&&($_POST['accounts_office_reference']!=''))? $_POST['accounts_office_reference']:'';
               $data1['crm_vat_number']=(isset($_POST['vat_number'])&&($_POST['vat_number']!=''))? $_POST['vat_number']:'';
              // client information
               $data1['crm_letter_sign']=(isset($_POST['engagement_letter'])&&($_POST['engagement_letter']!=''))? $_POST['engagement_letter'] : '';
               $data1['crm_business_website']=(isset($_POST['business_website'])&&($_POST['business_website']!=''))? $_POST['business_website'] : '';
               $data1['crm_accounting_system']=(isset($_POST['accounting_system_inuse'])&&($_POST['accounting_system_inuse']!=''))? $_POST['accounting_system_inuse'] : '';
               $data1['crm_paye_ref_number']=(isset($_POST['paye_ref_number'])&&($_POST['paye_ref_number']!=''))? $_POST['paye_ref_number'] : '';
               $data1['crm_assign_client_id_verified']=(isset($_POST['client_id_verified'])&&($_POST['client_id_verified']!=''))? $_POST['client_id_verified'] : '';
               $data1['crm_assign_type_of_id']=(isset($_POST['type_of_id'])&&($_POST['type_of_id']!=''))? implode(',',$_POST['type_of_id']) : '';
          /** 29-08-2018 proof attch **/
                $proof_attach_file='';
                if(!empty($_FILES['proof_attach_file']['name'])){
                      /** for multiple **/
                      $files = $_FILES;
                      $cpt = count($_FILES['proof_attach_file']['name']);
                      for($i=0; $i<$cpt; $i++)
                      {           
                      /** end of multiple **/
                          $_FILES['proof_attach_file']['name']= $files['proof_attach_file']['name'][$i];
                          $_FILES['proof_attach_file']['type']= $files['proof_attach_file']['type'][$i];
                          $_FILES['proof_attach_file']['tmp_name']= $files['proof_attach_file']['tmp_name'][$i];
                          $_FILES['proof_attach_file']['error']= $files['proof_attach_file']['error'][$i];
                          $_FILES['proof_attach_file']['size']= $files['proof_attach_file']['size'][$i]; 
                          $uploadPath = 'uploads/client_proof/';
                          $config['upload_path'] = $uploadPath;
                          $config['allowed_types'] = '*';    
                          $config['max_size']='0';      
                          $this->load->library('upload', $config);
                          $this->upload->initialize($config);
                          if($this->upload->do_upload('proof_attach_file')){
                                 $fileData = $this->upload->data();
                                 $proof_attach_file=$proof_attach_file.",".base_url().'uploads/client_proof/'.$fileData['file_name'];
                          }
                      }
                }
              $data1['proof_attach_file']=$proof_attach_file;
              $data1['crm_assign_other_custom']=(isset($_POST['assign_other_custom'])&&($_POST['assign_other_custom']!=''))? $_POST['assign_other_custom'] : '';
              $data1['crm_assign_proof_of_address']=(isset($_POST['proof_of_address'])&&($_POST['proof_of_address']!=''))? $_POST['proof_of_address'] : '';
              $data1['crm_assign_meeting_client']=(isset($_POST['meeting_client'])&&($_POST['meeting_client']!=''))? $_POST['meeting_client'] : '';
              $data1['crm_assign_source']=(isset($_POST['source'])&&($_POST['source']!=''))? $_POST['source'] : '';
              $data1['crm_refered_by']=(isset($_POST['refered_by'])&&($_POST['refered_by']!=''))? $_POST['refered_by'] : '';
              $data1['crm_assign_relationship_client']=(isset($_POST['relationship_client'])&&($_POST['relationship_client']!=''))? $_POST['relationship_client'] : '';
              $data1['crm_assign_notes']=(isset($_POST['assign_notes'])&&($_POST['assign_notes']!=''))? $_POST['assign_notes'] : '';
              // other
              $data1['crm_previous_accountant']=(isset($_POST['previous_account'])&&($_POST['previous_account']!=''))? $_POST['previous_account'] : '';
              $data1['crm_other_name_of_firm']=(isset($_POST['name_of_firm'])&&($_POST['name_of_firm']!=''))? $_POST['name_of_firm'] : '';
              $data1['crm_other_address']=(isset($_POST['other_address'])&&($_POST['other_address']!=''))? $_POST['other_address'] : '';
              $data1['crm_other_contact_no']=(isset($_POST['pn_no_rec'])&&($_POST['pn_no_rec']!=''))? $_POST['cun_code'].$_POST['pn_no_rec'] : '';
              $data1['crm_email']=(isset($_POST['emailid'])&&($_POST['emailid']!=''))? $_POST['emailid'] : '';
              $data1['crm_other_chase_for_info']=(isset($_POST['chase_for_info'])&&($_POST['chase_for_info']!=''))? $_POST['chase_for_info'] : '';
              $data1['crm_other_notes']=(isset($_POST['other_notes'])&&($_POST['other_notes']!=''))? $_POST['other_notes'] : '';
              $data1['crm_other_internal_notes']=(isset($_POST['other_internal_notes'])&&($_POST['other_internal_notes']!=''))? $_POST['other_internal_notes'] : '';
              $data1['crm_other_invite_use']=(isset($_POST['invite_use'])&&($_POST['invite_use']!=''))? $_POST['invite_use'] : '';
              $data1['crm_other_crm']=(isset($_POST['other_crm'])&&($_POST['other_crm']!=''))? $_POST['other_crm'] : '';
              $data1['crm_other_proposal']=(isset($_POST['other_proposal'])&&($_POST['other_proposal']!=''))? $_POST['other_proposal'] : '';
              $data1['crm_other_task']=(isset($_POST['other_task'])&&($_POST['other_task']!=''))? $_POST['other_task'] : '';
              $data1['crm_other_send_invit_link']=(isset($_POST['send_invit_link'])&&($_POST['send_invit_link']!=''))? $_POST['send_invit_link'] : '';
              $data1['crm_other_username']=(isset($_POST['user_name'])&&($_POST['user_name']!=''))? $_POST['user_name'] : '';
              $data1['crm_other_password']=(isset($_POST['password'])&&($_POST['password']!=''))? $_POST['password'] : '';
              $data1['crm_other_any_notes']=(isset($_POST['other_any_notes'])&&($_POST['other_any_notes']!=''))? $_POST['other_any_notes'] : '';
              $data1['select_responsible_type']=(isset($_POST['select_responsible_type'])&&($_POST['select_responsible_type']!=''))? $_POST['select_responsible_type'] : '';
              // notes tab
              /** activity log for notes **/
             /* $data1['crm_notes_info']=(isset($_POST['notes_info'])&&($_POST['notes_info']!=''))? $_POST['notes_info'] : '';
              if(isset($_POST['notes_info'])&&($_POST['notes_info']!='')){

                      $activity_datas['log'] = "Notes Added ---".$_POST['notes_info'];
                      $activity_datas['createdTime'] = time();
                      $activity_datas['module'] = 'Client';
                      $activity_datas['sub_module']='Client Notes';
                      $activity_datas['user_id'] = $_SESSION['admin_id'];
                      $activity_datas['module_id'] = (isset($_POST['user_id'])&&($_POST['user_id']==''))? $in : $_POST['user_id'];
                      $this->Common_mdl->insert('activity_log',$activity_datas);
              }*/
              /** end of activity log for notes **/
              // documents tab
              $data1['crm_documents']=(isset($_FILES['document']['name'])&&($_FILES['document']['name']!=''))? $this->Common_mdl->do_upload($_FILES['document'], 'uploads') : '';
              $data1['created_date']=time();
              $data1['autosave_status']=0;
              //busness tab that only on if u choose selfassesment
              $datalegal['user_id']=$_POST['user_id'];
              $datalegal['company_no'] = isset($_POST['fa_cmyno']) ? $_POST['fa_cmyno'] : "";  
              $datalegal['incorporation_date'] = isset($_POST['fa_incordate']) ? $_POST['fa_incordate'] : "";  
              $datalegal['regist_address'] = isset($_POST['fa_registadres']) ? $_POST['fa_registadres'] : "";  
              $datalegal['turnover'] = isset($_POST['fa_turnover']) ? $_POST['fa_turnover'] : "";  
              $datalegal['date_of_trading'] = isset($_POST['fa_dateoftrading']) ? $_POST['fa_dateoftrading'] : ""; 
              $datalegal['sic_code'] = isset($_POST['fa_siccode']) ? $_POST['fa_siccode'] : "";  
              $datalegal['nuture_of_business'] = isset($_POST['fa_nutureofbus']) ? $_POST['fa_nutureofbus'] : "";  
              $datalegal['company_utr'] = isset($_POST['fa_cmyutr']) ? $_POST['fa_cmyutr'] : "";  
              $datalegal['company_house_auth_code'] = isset($_POST['fa_cmyhouse']) ? $_POST['fa_cmyhouse'] : "";  
              $datalegal['trading_as'] = isset($_POST['bus_tradingas']) ? $_POST['bus_tradingas'] : "";  
              $datalegal['commenced_trading'] = isset($_POST['bus_commencedtrading']) ? $_POST['bus_commencedtrading'] : "";  
              $datalegal['register_sa'] = isset($_POST['bus_regist']) ? $_POST['bus_regist'] : "";  
              $datalegal['bus_turnover'] = isset($_POST['bus_turnover']) ? $_POST['bus_turnover'] : "";  
              $datalegal['bus_nuture_of_business'] = isset($_POST['bus_nutureofbus']) ? $_POST['bus_nutureofbus'] : "";  
              //busness tab that only on if u choose selfassesment

              $c_id = $_POST['user_id'];
              $sql_c = $this->db->query("select * from firm where user_id = $c_id")->row_array();

              if(!empty($sql_c))
              {
                $updates_client =  $this->Common_mdl->update('firm',$data1,'user_id',$_POST['user_id']);
              }
              else
              {
                  $updates_client =  $this->db->insert( 'firm', $data1 );
          
                  $id=$this->db->insert_id();

                  $legalFrom = $this->db->insert( 'client_legalform', $datalegal );
              }

              /** 06-07-2018 rs for auto cretaed task**/
              /*$add_reminder_ids=  isset($_POST['add_reminder_ids']) ? $_POST['add_reminder_ids'] : "";    
              if($add_reminder_ids!='')
              {
                $ex_reminder_ids=explode(',', $add_reminder_ids);
                foreach ($ex_reminder_ids as $reid_key => $reid_value) {
                  $re_data['user_id']=$_POST['user_id'];
                 $update_rem_ids =  $this->Common_mdl->update('reminder_setting',$re_data,'id',$reid_value);
                }
              }*/

              if($updates_client && isset($_POST['chase_for_info'])=='on')
              {
                  $query = $this->db->query('select * from user where id="'.$_POST['user_id'].'"')->row_array();
                          $data2['username'] = $query['username'];
                          $user_id = $query['id'];
                          $password = $query['password'];
                          $encrypt_id = base64_encode($user_id);
                          $email_subject  = 'Email For Others';
                          $data2['email_contents']  = '<a href='.base_url().' style="display: inline-block;text-align: center;color: #00aeef;text-decoration: none;font-family:;background: #00aeef;color: #fff;font-weight: 600;padding: 5px 10px;border-radius: 3px;">Click Here</a><p>Notes :'.$_POST['other_notes'].'</p>';
                         $data2['title']='Others Chase for Information Mail';
                          //email template
                            $body = $this->load->view('remainder_email.php',$data2, TRUE);
                            $this->load->library('email');
                            $this->email->set_mailtype("html");
                            $this->email->from('info@remindoo.org');
                            $this->email->to($query['crm_email_id']);
                            $this->email->subject($email_subject);
                            $this->email->message($body);
                            $send = $this->email->send();
              }
              $email = $this->input->post('emailid');
              $query = $this->db->query('select * from user where crm_email_id="'.$_POST['emailid'].'"')->row_array();
              if($query['username']!='')
              { 
                  $data2['username'] = $query['username'];
                  $user_id = $query['id'];
                  $password = $query['password'];
                  $encrypt_id = base64_encode($user_id);
                  $email_subject  = 'Welcome Email';
                  $data2['email_content']  = '<a href='.base_url().'login/resetPwd/'.$encrypt_id.' style="display: inline-block;text-align: center;color: #00aeef;text-decoration: none;font-family:;background: #00aeef;color: #fff;font-weight: 600;padding: 5px 10px;border-radius: 3px;">Click Here</a> for Reset Your CRM Account Password';
                  $this->session->set_flashdata('email',$email);
                  //email template
                  $body = $this->load->view('forgot_email_format.php',$data2, TRUE);          
                  $this->mailsettings();
                  $this->email->from('info@techleafsolutions.com');
                  $this->email->to($email);
                  $this->email->subject($email_subject);
                  $this->email->message($body);
                  $send = $this->email->send(); 
              }
                 echo $in;
                      
            }
            else
              {
                echo '0'; //usre id not preset
              }
        }
            if(isset($_POST['chase_for_info']) && $_POST['chase_for_info']!='')
            {
              $this->Task_creating_model->client_create_task( $in );
            }
          }
          elseif ($_POST['user_ids']!='' && isset($_POST['user_ids'])) {
          //  echo 'fffff';die;
          $this->updates_client($_POST['user_ids']);


          } else { echo '0';}
          
  }
  public function AddFirmContacts ()
  {
   if(isset($_POST['title']))
   {
    $user_id = $_POST['user_id'];
    /*$this->db->where('client_id',$client_id);
    $this->db->delete('client_contacts');*/
   $i=1;
    $data = array();
   foreach ($_POST['title'] as $key => $details)
   {
      
      /* $data['title'] = $_POST['title'][$key];
       $data['first_name'] = $_POST['first_name'][$key];
       $data['middle_name'] = $_POST['middle_name'][$key];*/
      if(isset($_POST['title']) &&  !empty(array_filter($_POST['title']))) {  $data['title'] =  $_POST['title'][$key];}else{ $data['title'] = ''; }
      if(isset($_POST['first_name']) &&  !empty(array_filter($_POST['first_name']))) {  $data['first_name'] =  $_POST['first_name'][$key];}else{ $data['first_name'] = ''; }
      if(isset($_POST['middle_name']) &&  !empty(array_filter($_POST['middle_name']))) {  $data['middle_name'] =  $_POST['middle_name'][$key];}else{ $data['middle_name'] = ''; }
     // echo json_encode(array_filter($_POST['last_name']))."//l name";
      if(isset($_POST['last_name']) &&  !empty(array_filter($_POST['last_name']))) {  $data['last_name'] =  $_POST['last_name'][$key];}else{ $data['last_name'] = ''; }
      if(isset($_POST['surname']) &&  !empty(array_filter($_POST['surname']))) {  $data['surname'] =  $_POST['surname'][$key];}else{ $data['surname'] = ''; }
      if(isset($_POST['preferred_name']) && !empty(array_filter($_POST['preferred_name']))) {  $data['preferred_name'] =  $_POST['preferred_name'][$key];}else{ $data['preferred_name'] = ''; }
      if(isset($_POST['mobile']) &&  !empty(array_filter($_POST['mobile']))) {  $data['mobile'] =  $_POST['mobile'][$key];}else{ $data['mobile'] = ''; }
      if(isset($_POST['main_email']) &&  !empty(array_filter($_POST['main_email']))) {  $data['main_email'] =  $_POST['main_email'][$key];}else{ $data['main_email'] = ''; }

      if(isset($_POST['work_email'.$i])) {  $data['work_email'] =  json_encode($_POST['work_email'.$i]);}else{ $data['work_email'] = ''; }

      if(isset($_POST['nationality'])) {  $data['nationality'] =  $_POST['nationality'][$key];}else{ $data['nationality'] = ''; }
      if(isset($_POST['psc'])) {  $data['psc'] =  $_POST['psc'][$key];}else{ $data['psc'] = ''; }
      if(isset($_POST['shareholder'])) {  $data['shareholder'] =  $_POST['shareholder'][$key];}else{ $data['shareholder'] = ''; }
      if(isset($_POST['ni_number'])) {  $data['ni_number'] =  $_POST['ni_number'][$key];}else{ $data['ni_number'] = ''; }
      if(isset($_POST['content_type'])) {  $data['content_type'] =  $_POST['content_type'][$key];}else{ $data['content_type'] = ''; }
      if(isset($_POST['address_line1'])) {  $data['address_line1'] =  $_POST['address_line1'][$key];}else{ $data['address_line1'] = ''; }
      if(isset($_POST['address_line2'])) {  $data['address_line2'] =  $_POST['address_line2'][$key];}else{ $data['address_line2'] = ''; }
      if(isset($_POST['town_city'])) {  $data['town_city'] =  $_POST['town_city'][$key];}else{ $data['town_city'] = ''; }
      if(isset($_POST['post_code'])) {  $data['post_code'] =  $_POST['post_code'][$key];}else{ $data['post_code'] = ''; }

      if(isset($_POST['pre_landline'.$i])) {  $data['pre_landline'] =  json_encode($_POST['pre_landline'.$i]);}else{ $data['pre_landline'] = ''; }

      if(isset($_POST['landline'.$i])) {  $data['landline'] =  json_encode($_POST['landline'.$i]);}else{ $data['landline'] = ''; }

      if(isset($_POST['date_of_birth'])) {  $data['date_of_birth'] =  $_POST['date_of_birth'][$key];}else{ $data['date_of_birth'] = ''; }
      if(isset($_POST['nature_of_control'])) {  $data['nature_of_control'] =  $_POST['nature_of_control'][$key];}else{ $data['nature_of_control'] = ''; }
      if(isset($_POST['utr_number'])) {  $data['utr_number'] =  $_POST['utr_number'][$key];}else{ $data['utr_number'] = ''; }
      if(isset($_POST['marital_status'])) {  $data['marital_status'] =  $_POST['marital_status'][$key];}else{ $data['marital_status'] = ''; }  if(isset($_POST['contact_type'])) {  $data['contact_type'] =  $_POST['contact_type'][$key];}else{ $data['contact_type'] = ''; }
      //ddddddddddddd
      /*if(isset($_POST['make_primary'])) {  $data['make_primary'] =  $_POST['make_primary'][$key];}else{ $data['make_primary'] = '0'; }*/
      $mk = (isset($_POST['make_primary']))?$_POST['make_primary']:'0';
     // $mk = $_POST['make_primary'] - 1;
    //  $re_key=$key+1;
     
      $make_primary_loop=(isset($_POST['make_primary_loop']))?$_POST['make_primary_loop'][$key]:'0';
      echo $mk."--".$make_primary_loop."<br>";
      if($mk == $make_primary_loop){
        $data['make_primary'] = '1';
      }else{
        $data['make_primary'] = '0';
      }

      if(isset($_POST['user_id'])) {  $data['user_id'] =  $_POST['user_id'];}else{ $data['user_id'] = ''; }

      if(isset($_POST['occupation'])) {  $data['occupation'] =  $_POST['occupation'][$key];}else{ $data['occupation'] = ''; }

      if(isset($_POST['appointed_on'])) {  $data['appointed_on'] =  $_POST['appointed_on'][$key];}else{ $data['appointed_on'] = ''; }
      
      if(isset($_POST['country_of_residence'])) {  $data['country_of_residence'] =  $_POST['country_of_residence'][$key];}else{ $data['country_of_residence'] = ''; }

      if(isset($_POST['other_custom'])) {  $data['other_custom'] =  $_POST['other_custom'][$key];}else{ $data['other_custom'] = ''; }


       /*$data['preferred_name'] = $_POST['preferred_name'][$key];
       $data['mobile'] = $_POST['mobile'][$key];
       $data['main_email'] = $_POST['main_email'][$key];
       $data['nationality'] = $_POST['nationality'][$key];
       $data['psc'] = $_POST['psc'][$key];
       $data['ni_number'] = $_POST['ni_number'];
       $data['address_line1'] = $_POST['address_line1'][$key];
       $data['address_line2'] = $_POST['address_line2'][$key];
       $data['town_city'] = $_POST['town_city'][$key];
       $data['post_code'] = $_POST['post_code'][$key];
       $data['landline'] = $_POST['landline'][$key];
       $data['work_email'] = $_POST['work_email'][$key];
       $data['date_of_birth'] = $_POST['date_of_birth'][$key];
       $data['nature_of_control'] = $_POST['nature_of_control'][$key];
       $data['utr_number'] = $_POST['utr_number'][$key];
       $data['create_self_assessment_client'] = $_POST['create_self_assessment_client'][$key];
       $data['client_does_self_assessment'] = $_POST['client_does_self_assessment'][$key];
       $data['photo_id_verified'] = $_POST['photo_id_verified'][$key];
       $data['address_verified'] = $_POST['address_verified'][$key];
       $data['terms_signed'] = $_POST['terms_signed'][$key];*/

     // print_r($data);
       $this->Common_mdl->insert('firm_contacts',$data);



       //echo $this->db->last_query();
      /* if($this->Common_mdl->insert('client_contacts',$data))
       {
        //echo $this->db->last_query();
        return true;
       }else{
        return false;
       }*/
       $i++;
   }


}
   //exit;
           return true;


  }

     public function mailsettings()
    {   
        $this->load->library('email');
        $config['wrapchars'] = 76; // Character count to wrap at.
        $config['mailtype'] = 'html'; // text or html Type of mail. If you send HTML email you must send it as a complete web page. Make sure you don't have any relative links or relative image paths otherwise they will not work.
        //$config['charset'] = 'utf-8'; // Character set (utf-8, iso-8859-1, etc.).
        $this->email->initialize($config);  
    }

    public function user()
    {
        $this->Loginchk_model->superAdmin_LoginCheck();        
        $data['getallUser']=$this->db->query("SELECT * FROM user where firm='1' AND role='2' AND autosave_status!='1' and  firm_admin_id='".$_SESSION['id']."' order by id DESC")->result_array();
        $data['column_setting']=$this->Common_mdl->getallrecords('column_setting_new');
        $data['client'] =$this->db->query('select * from column_setting_new where id=1')->row_array();
        $data['feedback'] =$this->db->query("select id,username,crm_email_id from user where username!='' and username!='0' and  crm_email_id!='' and crm_email_id!='0' "  )->result();
        $data["email_temp"]=$this->db->query("select * from proposal_proposaltemplate where user_id=".$_SESSION['id']." and type='add_email_template'")->result_array(); 
        $this->load->view('users/user_list',$data);
    }



    function Firm_Permission($id){
   //   echo $id;
     // echo $_SESSION['role_name'];
    //  echo $_SESSION['firm_admin_id'];

      $query1=$this->db->query("INSERT INTO `roles_section`(`parent_id`, `role`, `firm_id`) VALUES ('0','Admin','".$id."')");

      $role_id=$this->db->insert_id();

      $query=$this->db->query("INSERT INTO `Role`( `id`,`firm_id`, `role`) VALUES ('".$role_id."','".$id."','Admin')");

     $data=array('role'=>$role_id);
     $update=$this->Common_mdl->update('user',$data,'id',$id);

      $query2=$this->db->query("INSERT INTO `role_permission1` ( `firm_id`, `role_id`, `role`, `Dashboard`, `Task`, `Leads`, `Webtolead`, `Proposal_Dashboad`, `Create_Proposal`, `Catalog`, `Template`, `Setting`, `Client_Section`, `Add_Client`, `Add_From_Company_House`, `Import_Client`, `Services_Timeline`, `Deadline_manager`, `Reports`, `Admin_Settings`, `Firm_Settings`, `Reminder_Settings`, `Team_and_Management`, `Staff_Custom_Firmlist`, `Tickets`, `Chat`, `Documents`, `Invoices`, `Give_Feedback`, `Task_Column_Settings`, `Client_Column_Settings`, `Firm_Tickets`, `Support_Tickets`, `My_Profile`, `Section_Settings`, `Notification_Settings`, `Pricelist_Settings`, `Services`, `Email_Template`, `Import_Task`, `Task_Sub_Task`, `Task_Timer`, `Import_leads`, `Leads_source`, `Lead_Status`) SELECT '".$id."', '".$role_id."','Admin', `Dashboard`, `Task`, `Leads`, `Webtolead`, `Proposal_Dashboad`, `Create_Proposal`, `Catalog`, `Template`, `Setting`, `Client_Section`, `Add_Client`, `Add_From_Company_House`, `Import_Client`, `Services_Timeline`, `Deadline_manager`, `Reports`, `Admin_Settings`, `Firm_Settings`, `Reminder_Settings`, `Team_and_Management`, `Staff_Custom_Firmlist`, `Tickets`, `Chat`, `Documents`, `Invoices`, `Give_Feedback`, `Task_Column_Settings`, `Client_Column_Settings`, `Firm_Tickets`, `Support_Tickets`, `My_Profile`, `Section_Settings`, `Notification_Settings`, `Pricelist_Settings`, `Services`, `Email_Template`, `Import_Task`, `Task_Sub_Task`, `Task_Timer`, `Import_leads`, `Leads_source`, `Lead_Status` FROM `role_permission1` where role='".$_SESSION['role_name']."' and firm_id='".$_SESSION['firm_admin_id']."'");

    }

    public function updates_client($user_ids = false)
    {
         $this->Loginchk_model->superAdmin_LoginCheck();     

      // print_r($_POST);
      // exit;
        if($user_ids!='')
        {
            $_POST['user_id'] = $user_ids;
            $var=rand(1,10);
            $image_one=(isset($_POST['image_name'])&&($_POST['image_name']!=''))? $_POST['image_name'] : '825898.jpg';
            $image= (isset($_FILES['profile_image']['name'])&&($_FILES['profile_image']['name']!=''))? $this->Common_mdl->do_upload($_FILES['profile_image'], 'uploads') : $image_one ;
            $data['crm_name']=$_POST['company_name'];
            $data['username']=$_POST['user_name'];
            $data['crm_email_id']=$_POST['emailid'];
            $data['crm_phone_number']=(isset($_POST['pn_no_rec'])&&($_POST['pn_no_rec']!=''))? $_POST['cun_code'].$_POST['pn_no_rec'] : '';
            $data['password']=(isset($_POST['password'])&&($_POST['password']!=''))? md5($_POST['password']) : md5($var);
            $data['company_roles']=(isset($_POST['company_house'])&&($_POST['company_house']!=''))? $_POST['company_house'] : 0;
            $data['CreatedTime']=time();
            $data['client_id']=(isset($_POST['client_id'])&&($_POST['client_id']!=''))? $_POST['client_id'] : rand(1,5);
            /** 09-08-2018 **/
            $data['status']=1;
            /** end of 09-08-2018 **/
            // print_r($data);
            // exit;
            $updates_user= $this->Common_mdl->update('user',$data,'id',$_POST['user_id']);
          //  print_r($updates_user);

           // exit;

            /** welcome mail function **/
            //     $mail_data['name']=$_POST['user_name'];
            //     $mail_data['user_name']=$_POST['user_name'];
            //     $mail_data['password']=(isset($_POST['password'])&&($_POST['password']!=''))? $_POST['password'] : $var;
            //   $body = $this->load->view('email/welcome-email-mail.php', $mail_data, TRUE);
            //   $this->load->library('email');
            //   $this->email->set_mailtype('html');
            //   $this->email->from('info@remindoo.org');
            //   $this->email->to($_POST['emailid']);
            // //  $this->email->to('shanmugapriya.techleaf@gmail.com');
            //   $this->email->subject('Welcome To Remindoo');
            //   $this->email->message($body);
            //   $send = $this->email->send();

            /** end of welcome mail function **/


//requried information
$datak['crm_company_name1']=(isset($_POST['company_name1'])&&($_POST['company_name1']!=''))? $_POST['company_name1']:'';
$datak['crm_legal_form']=(isset($_POST['legal_form'])&&($_POST['legal_form']!=''))? $_POST['legal_form']:'';
$datak['crm_allocation_holder']=(isset($_POST['allocation_holders'])&&($_POST['allocation_holders']!=''))? $_POST['allocation_holders']:'';
// basic details
$datak['crm_company_name']=(isset($_POST['company_name'])&&($_POST['company_name']!=''))? $_POST['company_name']:'';
$datak['crm_company_number']=(isset($_POST['company_number'])&&($_POST['company_number']!=''))? $_POST['company_number']:'';
$datak['crm_incorporation_date']=(isset($_POST['date_of_creation'])&&($_POST['date_of_creation']!=''))? $_POST['date_of_creation']:'';
$datak['crm_registered_in']=(isset($_POST['registered_in'])&&($_POST['registered_in']!=''))? $_POST['registered_in']:'';
$datak['crm_address_line_one']=(isset($_POST['address_line_one'])&&($_POST['address_line_one']!=''))? $_POST['address_line_one']:'';
$datak['crm_address_line_two']=(isset($_POST['address_line_two'])&&($_POST['address_line_two']!=''))? $_POST['address_line_two']:'';
$datak['crm_address_line_three']=(isset($_POST['address_line_three'])&&($_POST['address_line_three']!=''))? $_POST['address_line_three']:'';
 $datak['crm_letter_sign']=(isset($_POST['engagement_letter'])&&($_POST['engagement_letter']!=''))? $_POST['engagement_letter'] : '';

$datak['crm_assign_client_id_verified']=(isset($_POST['client_id_verified'])&&($_POST['client_id_verified']!=''))? $_POST['client_id_verified'] : '';
$datak['crm_assign_type_of_id']=(isset($_POST['type_of_id'])&&($_POST['type_of_id']!=''))? implode(',',$_POST['type_of_id']) : '';
/** 29-08-2018 proof attch **/
               $proof_attach_file='';
             if(!empty($_FILES['proof_attach_file']['name'])){
              /** for multiple **/
          $files = $_FILES;
    $cpt = count($_FILES['proof_attach_file']['name']);
    for($i=0; $i<$cpt; $i++)
    {           
              /** end of multiple **/
              $_FILES['proof_attach_file']['name']= $files['proof_attach_file']['name'][$i];
        $_FILES['proof_attach_file']['type']= $files['proof_attach_file']['type'][$i];
        $_FILES['proof_attach_file']['tmp_name']= $files['proof_attach_file']['tmp_name'][$i];
        $_FILES['proof_attach_file']['error']= $files['proof_attach_file']['error'][$i];
        $_FILES['proof_attach_file']['size']= $files['proof_attach_file']['size'][$i]; 
               $uploadPath = 'uploads/client_proof/';
               $config['upload_path'] = $uploadPath;
               $config['allowed_types'] = '*';    
               $config['max_size']='0';      
               $this->load->library('upload', $config);
               $this->upload->initialize($config);
               if($this->upload->do_upload('proof_attach_file')){
                   $fileData = $this->upload->data();
                 //  $uploadData[$i]['file_name'] = base_url().'attachment/'.$fileData['file_name'];
                    //$data['attach_file']= base_url().'uploads/'.$fileData['file_name'];
                   $proof_attach_file=$proof_attach_file.",".base_url().'uploads/client_proof/'.$fileData['file_name'];
               }
             }
           }
  /** alredy uploaded images **/
    $ex_img=(isset($_POST['already_upload_img']) && $_POST['already_upload_img']!='')?$_POST['already_upload_img']:'';
    //echo implode(',',$ex_img);
           if(!empty($ex_img))
           {
            $proof_attach_file=$proof_attach_file.",".implode(',', $ex_img);
           }
if($proof_attach_file!='')
{
  $datak['proof_attach_file']=implode(',', array_filter(explode(',', $proof_attach_file)));
}
else
{
  $datak['proof_attach_file']='';
}

  /** end of uploaded images **/
//$data1['proof_attach_file']=$proof_attach_file;
/** end of 29-08-2018 **/
/** 30-08-2018 **/
$datak['crm_assign_other_custom']=(isset($_POST['assign_other_custom'])&&($_POST['assign_other_custom']!=''))? $_POST['assign_other_custom'] : '';
/** end of 30-08-2018 **/
$datak['crm_assign_proof_of_address']=(isset($_POST['proof_of_address'])&&($_POST['proof_of_address']!=''))? $_POST['proof_of_address'] : '';
$datak['crm_assign_meeting_client']=(isset($_POST['meeting_client'])&&($_POST['meeting_client']!=''))? $_POST['meeting_client'] : '';
$datak['crm_assign_source']=(isset($_POST['source'])&&($_POST['source']!=''))? $_POST['source'] : '';
/** 30-06-2018 **/
// $data1['crm_refered_by']=(isset($_POST['refer_exist_client'])&&($_POST['refer_exist_client']!=''))? $_POST['refer_exist_client'] : '';
$datak['crm_refered_by']=(isset($_POST['refered_by'])&&($_POST['refered_by']!=''))? $_POST['refered_by'] : '';
/** 30-06-2018 **/
$datak['crm_assign_relationship_client']=(isset($_POST['relationship_client'])&&($_POST['relationship_client']!=''))? $_POST['relationship_client'] : '';
$data1['crm_assign_notes']=(isset($_POST['assign_notes'])&&($_POST['assign_notes']!=''))? $_POST['assign_notes'] : '';
// other
$datak['crm_previous_accountant']=(isset($_POST['previous_account'])&&($_POST['previous_account']!=''))? $_POST['previous_account'] : '';
$datak['crm_other_name_of_firm']=(isset($_POST['name_of_firm'])&&($_POST['name_of_firm']!=''))? $_POST['name_of_firm'] : '';
$datak['crm_other_address']=(isset($_POST['other_address'])&&($_POST['other_address']!=''))? $_POST['other_address'] : '';
$datak['crm_other_contact_no']=(isset($_POST['pn_no_rec'])&&($_POST['pn_no_rec']!=''))? $_POST['cun_code'].$_POST['pn_no_rec'] : '';
$datak['crm_email']=(isset($_POST['emailid'])&&($_POST['emailid']!=''))? $_POST['emailid'] : '';
$datak['crm_other_chase_for_info']=(isset($_POST['chase_for_info'])&&($_POST['chase_for_info']!=''))? $_POST['chase_for_info'] : '';

/** task creation chase for information 30-08-2018 **/

/** end of 30-08-2018 **/

$datak['crm_other_notes']=(isset($_POST['other_notes'])&&($_POST['other_notes']!=''))? $_POST['other_notes'] : '';

$datak['crm_other_internal_notes']=(isset($_POST['other_internal_notes'])&&($_POST['other_internal_notes']!=''))? $_POST['other_internal_notes'] : '';
$datak['crm_other_invite_use']=(isset($_POST['invite_use'])&&($_POST['invite_use']!=''))? $_POST['invite_use'] : '';
$datak['crm_other_crm']=(isset($_POST['other_crm'])&&($_POST['other_crm']!=''))? $_POST['other_crm'] : '';
$datak['crm_other_proposal']=(isset($_POST['other_proposal'])&&($_POST['other_proposal']!=''))? $_POST['other_proposal'] : '';
$datak['crm_other_task']=(isset($_POST['other_task'])&&($_POST['other_task']!=''))? $_POST['other_task'] : '';
$datak['crm_other_send_invit_link']=(isset($_POST['send_invit_link'])&&($_POST['send_invit_link']!=''))? $_POST['send_invit_link'] : '';
$datak['crm_other_username']=(isset($_POST['user_name'])&&($_POST['user_name']!=''))? $_POST['user_name'] : '';
$datak['crm_other_password']=(isset($_POST['password'])&&($_POST['password']!=''))? $_POST['password'] : '';
$datak['crm_other_any_notes']=(isset($_POST['other_any_notes'])&&($_POST['other_any_notes']!=''))? $_POST['other_any_notes'] : '';
// confirmation

$datak['select_responsible_type']=(isset($_POST['select_responsible_type'])&&($_POST['select_responsible_type']!=''))? $_POST['select_responsible_type'] : '';


   $updates_user= $this->Common_mdl->update('firm',$datak,'user_id',$_POST['user_id']);

        }
        
        $datas['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user','role','1');
    /** for client our check for mail ervices  **/       
      $re_conf_statement='';$re_accounts='';$re_company_tax_return='';$re_personal_tax_return='';
      $re_payroll='';$re_workplace='';$re_vat='';$re_cis='';$re_cissub='';$re_p11d='';$re_bookkeep='';
      $re_management='';$re_investgate='';$re_registered='';$re_taxadvice='';$re_taxinvest='';

      $tot_rec=$this->db->query("select * from client where user_id=".$_POST['user_id'])->row_array();
      
      //  if( !empty($tot_rec['conf_statement'] ))
      // $before_data1['conf_statement'] = (isset(json_decode($tot_rec['conf_statement'])->tab))?json_decode($tot_rec['conf_statement'])->tab:'';
      // else
      // $before_data1['conf_statement'] = ''; 

      // if( !empty($tot_rec['accounts'] ))
      // $before_data1['accounts'] = (isset(json_decode($tot_rec['accounts'])->tab))?json_decode($tot_rec['accounts'])->tab:'';
      // else
      // $before_data1['accounts'] = '';
      // if( !empty($tot_rec['company_tax_return'] ))
      // $before_data1['company_tax_return'] = (isset(json_decode($tot_rec['company_tax_return'])->tab))?json_decode($tot_rec['company_tax_return'])->tab:'';
      // else
      // $before_data1['company_tax_return'] = '';
      // if( !empty($tot_rec['personal_tax_return'] ))
      // $before_data1['personal_tax_return'] = (isset(json_decode($tot_rec['personal_tax_return'])->tab))?json_decode($tot_rec['personal_tax_return'])->tab:'';
      // else
      // $before_data1['personal_tax_return'] = '';

      // if( !empty($tot_rec['payroll'] ))
      // $before_data1['payroll'] = (isset(json_decode($tot_rec['payroll'])->tab))?json_decode($tot_rec['payroll'])->tab:'';
      // else
      // $before_data1['payroll'] = '';

      // if( !empty($tot_rec['workplace'] ))
      // $before_data1['workplace'] = (isset(json_decode($tot_rec['workplace'])->tab))?json_decode($tot_rec['workplace'])->tab:'';
      // else
      // $before_data1['workplace'] = '';

      // if( !empty($tot_rec['vat'] ))
      // $before_data1['vat'] = (isset(json_decode($tot_rec['vat'])->tab))?json_decode($tot_rec['vat'])->tab:'';
      // else
      // $before_data1['vat'] = '';

      // if( !empty($tot_rec['cis'] ))
      // $before_data1['cis'] = (isset(json_decode($tot_rec['cis'])->tab))?json_decode($tot_rec['cis'])->tab:'';
      // else
      // $before_data1['cis'] = '';

      // if( !empty($tot_rec['cissub'] ))
      // $before_data1['cissub'] = (isset(json_decode($tot_rec['cissub'])->tab))?json_decode($tot_rec['cissub'])->tab:'';
      // else
      // $before_data1['cissub'] = '';

      // if( !empty($tot_rec['p11d'] ))
      // $before_data1['p11d'] = (isset(json_decode($tot_rec['p11d'])->tab))?json_decode($tot_rec['p11d'])->tab:'';
      // else
      // $before_data1['p11d'] = '';


      // if( !empty($tot_rec['bookkeep'] ))
      // $before_data1['bookkeep'] =  (isset(json_decode($tot_rec['bookkeep'])->tab))?json_decode($tot_rec['bookkeep'])->tab:'';
      // else
      // $before_data1['bookkeep'] = '';
      // if( !empty($tot_rec['management'] ))
      // $before_data1['management'] =  (isset(json_decode($tot_rec['management'])->tab))?json_decode($tot_rec['management'])->tab:'';
      // else
      // $before_data1['management'] = '';
      // if( !empty($tot_rec['investgate'] ))
      // $before_data1['investgate'] =  (isset(json_decode($tot_rec['investgate'])->tab))?json_decode($tot_rec['investgate'])->tab:'';
      // else
      // $before_data1['investgate'] = '';
      // if( !empty($tot_rec['registered'] ))
      // $before_data1['registered'] =  (isset(json_decode($tot_rec['registered'])->tab))?json_decode($tot_rec['registered'])->tab:'';
      // else
      // $before_data1['registered'] = '';
      // if( !empty($tot_rec['taxadvice'] ))
      // $before_data1['taxadvice'] =  (isset(json_decode($tot_rec['taxadvice'])->tab))?json_decode($tot_rec['taxadvice'])->tab:'';
      // else
      // $before_data1['taxadvice'] = '';
      // if( !empty($tot_rec['taxinvest'] ))
      // $before_data1['taxinvest'] =  (isset(json_decode($tot_rec['taxinvest'])->tab))?json_decode($tot_rec['taxinvest'])->tab:'';
      // else
      // $before_data1['taxinvest'] = '';

    /** end of services mail chk 09-07-2018 **/

if(isset($_POST['user_id'])&&$_POST['user_id']!=''){
      // $data2['crm_name']=$_POST['user_name'];
      // $data2['username']=$_POST['user_name'];
      // $data2['crm_email_id']=$_POST['emailid'];
      // $data2['crm_phone_number']=(isset($_POST['pn_no_rec'])&&($_POST['pn_no_rec']!=''))? $_POST['cun_code'].$_POST['pn_no_rec'] : '';
      // $data2['password']=(isset($_POST['password'])&&($_POST['password']!=''))? md5($_POST['password']) : '';
      // $data2['confirm_password']=(isset($_POST['password'])&&($_POST['password']!=''))? ($_POST['password']) : '';
      // $data2['company_roles']=(isset($_POST['company_house'])&&($_POST['company_house']!=''))? $_POST['company_house'] : 0;
      // $data2['CreatedTime']=time();
      // $this->Common_mdl->update('user',$data2,'id',$_POST['user_id']);
    /** welcome mail function **/
     $get_user_status=$this->Common_mdl->get_field_value('user','status','id',$_POST['user_id']);
     if($get_user_status==4){
        $var=rand(1,10);
        $mail_data['name']=$_POST['user_name'];
        $mail_data['user_name']=$_POST['user_name'];
        $password=(isset($_POST['password'])&&($_POST['password']!=''))? $_POST['password'] : $var;
        /* 18.09.2018 */

          $proposal_content=$this->Common_mdl->select_record('proposal_proposaltemplate','title','welcome_mail');

          $sender_details=$this->db->query('select company_name,crm_name from admin_setting where user_id='.$_SESSION['id'].'')->row_array();

          $sender_company=$sender_details['company_name'];
          $sender_name=$sender_details['crm_name'];
          $body1=$proposal_content['body'];
          $subject=$proposal_content['subject'];
        //  $random_string=$this->generateRandomString();
          //$link=base_url().'Proposal_page/step_proposal/'.$random_string.'---'.$id;
        //  echo $link;
           $a1  =   array('::Client Name::'=>$user_data['crm_name'],
            '::Client Company::'=>$tot_rec['crm_company_name'],
            '::ClientPhoneno::'=> $tot_rec['crm_mobile_number'],
            '::ClientWebsite::'=>$tot_rec['crm_company_url'],
            '::Client Username::'=>$_POST['user_name'],
            '::Client Password::'=> $password,         
            ':: Sender Name::'=>$sender_company,
            ':: Sender Company::'=>$sender_name,       
            '::Date::'=>date("Y-m-d h:i:s"));

          $mail_data['body']  =   strtr($body1,$a1); 
          $subject  =   strtr($subject,$a1);
        //  $random_string=$this->generateRandomString();
          $body = $this->load->view('email/welcome-email-mail.php', $mail_data, TRUE);
      /* 18.09.2018 */


     // $body = $this->load->view('email/welcome-email-mail.php', $mail_data, TRUE);
      $this->load->library('email');
      $this->email->set_mailtype('html');
      $this->email->from('info@remindoo.org');
      $this->email->to($_POST['emailid']);
    //  $this->email->to('shanmugapriya.techleaf@gmail.com');
      $this->email->subject(preg_replace('/ {2,}/', ' ', str_replace('&nbsp;', ' ', strip_tags($subject))));
      $this->email->message($body);
      $send = $this->email->send();

       $change_status['status']=1;
      $change_client_status['crm_company_status']='active';
      $this->Common_mdl->update('user',$change_status,'id',$_POST['user_id']);
      $this->Common_mdl->update('client',$change_client_status,'user_id',$_POST['user_id']);

      }
     
    /** end of welcome mail function **/
      $data1['user_id']=$_POST['user_id'];
      // required information
      // if( !empty($_POST['confirm'] ))
      // $data1['conf_statement'] = json_encode($_POST['confirm']);
      // else
      // $data1['conf_statement'] = ''; 

      // if( !empty($_POST['accounts'] ))
      // $data1['accounts'] = json_encode($_POST['accounts']);
      // else
      // $data1['accounts'] = '';
      // if( !empty($_POST['companytax'] ))
      // $data1['company_tax_return'] = json_encode($_POST['companytax']);
      // else
      // $data1['company_tax_return'] = '';
      // if( !empty($_POST['personaltax'] ))
      // $data1['personal_tax_return'] = json_encode($_POST['personaltax']);
      // else
      // $data1['personal_tax_return'] = '';

      // if( !empty($_POST['payroll'] ))
      // $data1['payroll'] = json_encode($_POST['payroll']);
      // else
      // $data1['payroll'] = '';

      // if( !empty($_POST['workplace'] ))
      // $data1['workplace'] = json_encode($_POST['workplace']);
      // else
      // $data1['workplace'] = '';

      // if( !empty($_POST['vat'] ))
      // $data1['vat'] = json_encode($_POST['vat']);
      // else
      // $data1['vat'] = '';

      // if( !empty($_POST['cis'] ))
      // $data1['cis'] = json_encode($_POST['cis']);
      // else
      // $data1['cis'] = '';

      // if( !empty($_POST['cissub'] ))
      // $data1['cissub'] = json_encode($_POST['cissub']);
      // else
      // $data1['cissub'] = '';

      // if( !empty($_POST['p11d'] ))
      // $data1['p11d'] = json_encode($_POST['p11d']);
      // else
      // $data1['p11d'] = '';


      // if( !empty($_POST['bookkeep'] ))
      // $data1['bookkeep'] = json_encode($_POST['bookkeep']);
      // else
      // $data1['bookkeep'] = '';
      // if( !empty($_POST['management'] ))
      // $data1['management'] = json_encode($_POST['management']);
      // else
      // $data1['management'] = '';
      // if( !empty($_POST['investgate'] ))
      // $data1['investgate'] = json_encode($_POST['investgate']);
      // else
      // $data1['investgate'] = '';
      // if( !empty($_POST['registered'] ))
      // $data1['registered'] = json_encode($_POST['registered']);
      // else
      // $data1['registered'] = '';
      // if( !empty($_POST['taxadvice'] ))
      // $data1['taxadvice'] = json_encode($_POST['taxadvice']);
      // else
      // $data1['taxadvice'] = '';
      // if( !empty($_POST['taxinvest'] ))
      // $data1['taxinvest'] = json_encode($_POST['taxinvest']);
      // else
      // $data1['taxinvest'] = '';

//requried information
$data1['crm_company_name1']=(isset($_POST['company_name1'])&&($_POST['company_name1']!=''))? $_POST['company_name1']:'';
$data1['crm_legal_form']=(isset($_POST['legal_form'])&&($_POST['legal_form']!=''))? $_POST['legal_form']:'';
$data1['crm_allocation_holder']=(isset($_POST['allocation_holders'])&&($_POST['allocation_holders']!=''))? $_POST['allocation_holders']:'';
// basic details
$data1['crm_company_name']=(isset($_POST['company_name'])&&($_POST['company_name']!=''))? $_POST['company_name']:'';
$data1['crm_company_number']=(isset($_POST['company_number'])&&($_POST['company_number']!=''))? $_POST['company_number']:'';
$data1['crm_incorporation_date']=(isset($_POST['date_of_creation'])&&($_POST['date_of_creation']!=''))? $_POST['date_of_creation']:'';
$data1['crm_registered_in']=(isset($_POST['registered_in'])&&($_POST['registered_in']!=''))? $_POST['registered_in']:'';
$data1['crm_address_line_one']=(isset($_POST['address_line_one'])&&($_POST['address_line_one']!=''))? $_POST['address_line_one']:'';
$data1['crm_address_line_two']=(isset($_POST['address_line_two'])&&($_POST['address_line_two']!=''))? $_POST['address_line_two']:'';
$data1['crm_address_line_three']=(isset($_POST['address_line_three'])&&($_POST['address_line_three']!=''))? $_POST['address_line_three']:'';
$data1['crm_town_city']=(isset($_POST['crm_town_city'])&&($_POST['crm_town_city']!=''))? $_POST['crm_town_city']:'';
$data1['crm_post_code']=(isset($_POST['crm_post_code'])&&($_POST['crm_post_code']!=''))? $_POST['crm_post_code']:'';
$data1['crm_company_status']=(isset($_POST['company_status'])&&($_POST['company_status']!=''))? $_POST['company_status']:'';
$data1['crm_company_type']=(isset($_POST['company_type'])&&($_POST['company_type']!=''))? $_POST['company_type']:'';
$data1['crm_company_sic']=(isset($_POST['company_sic'])&&($_POST['company_sic']!=''))? $_POST['company_sic']:'';
$data1['crm_accounting_system']=(isset($_POST['accounting_system'])&&($_POST['accounting_system']!=''))? $_POST['accounting_system']:'';
$data1['crm_companies_house_authorisation_code']=(isset($_POST['auth_code'])&&($_POST['auth_code']!=''))? $_POST['auth_code']:'';
$data1['crm_company_utr']=(isset($_POST['crm_company_utr'])&&($_POST['crm_company_utr']!=''))? $_POST['crm_company_utr']:'';
$data1['crm_accounts_office_reference']=(isset($_POST['acc_ref_number'])&&($_POST['acc_ref_number']!=''))? $_POST['acc_ref_number']:'';
$data1['crm_vat_number']=(isset($_POST['vat_number'])&&($_POST['vat_number']!=''))? $_POST['vat_number']:'';
//$data1['status']=(isset($_POST['company_house'])&&($_POST['company_house']!=''))? $_POST['company_house'] : 0;
// client information
$data1['crm_letter_sign']=(isset($_POST['letter_sign'])&&($_POST['letter_sign']!=''))? $_POST['letter_sign'] : '';
$data1['crm_business_website']=(isset($_POST['business_website'])&&($_POST['business_website']!=''))? $_POST['business_website'] : '';
$data1['crm_paye_ref_number']=(isset($_POST['paye_ref_number'])&&($_POST['paye_ref_number']!=''))? $_POST['paye_ref_number'] : '';
//Assign to
//$data1['crm_assign_team']=(isset($_POST['team'])&&($_POST['team']!=''))? $_POST['team'] : '0';
//$data1['crm_allocation_holder']=(isset($_POST['allocation_holder'])&&($_POST['allocation_holder']!=''))? $_POST['team'] : '0';
//$data1['crm_assign_manager_reviewer']=(isset($_POST['manager_reviewer'])&&($_POST['manager_reviewer']!=''))? $_POST['manager_reviewer'] : '0';
//$data1['crm_assign_managed']=(isset($_POST['managed'])&&($_POST['managed']!=''))? $_POST['managed'] : '0';
/*
$data1['crm_assign_team'] = (isset($_POST['team']) && !empty($_POST['team']))? implode(',', $_POST['team']):''; 
$data1['crm_allocation_holder'] = (isset($_POST['allocation_holder']) && !empty($_POST['allocation_holder']))? implode(' ^ ', $_POST['allocation_holder']):'';*/ 
//$data1['crm_allocation_holder']=(isset($_POST['allocation_holder'])&&($_POST['allocation_holder']!=''))? $_POST['team'] : '0';
$data1['crm_assign_client_id_verified']=(isset($_POST['client_id_verified'])&&($_POST['client_id_verified']!=''))? $_POST['client_id_verified'] : '';
$data1['crm_assign_type_of_id']=(isset($_POST['type_of_id'])&&($_POST['type_of_id']!=''))? implode(',',$_POST['type_of_id']) : '';
/** 29-08-2018 proof attch **/
               $proof_attach_file='';
             if(!empty($_FILES['proof_attach_file']['name'])){
              /** for multiple **/
          $files = $_FILES;
    $cpt = count($_FILES['proof_attach_file']['name']);
    for($i=0; $i<$cpt; $i++)
    {           
              /** end of multiple **/
              $_FILES['proof_attach_file']['name']= $files['proof_attach_file']['name'][$i];
        $_FILES['proof_attach_file']['type']= $files['proof_attach_file']['type'][$i];
        $_FILES['proof_attach_file']['tmp_name']= $files['proof_attach_file']['tmp_name'][$i];
        $_FILES['proof_attach_file']['error']= $files['proof_attach_file']['error'][$i];
        $_FILES['proof_attach_file']['size']= $files['proof_attach_file']['size'][$i]; 
               $uploadPath = 'uploads/client_proof/';
               $config['upload_path'] = $uploadPath;
               $config['allowed_types'] = '*';    
               $config['max_size']='0';      
               $this->load->library('upload', $config);
               $this->upload->initialize($config);
               if($this->upload->do_upload('proof_attach_file')){
                   $fileData = $this->upload->data();
                 //  $uploadData[$i]['file_name'] = base_url().'attachment/'.$fileData['file_name'];
                    //$data['attach_file']= base_url().'uploads/'.$fileData['file_name'];
                   $proof_attach_file=$proof_attach_file.",".base_url().'uploads/client_proof/'.$fileData['file_name'];
               }
             }
           }
  /** alredy uploaded images **/
    $ex_img=(isset($_POST['already_upload_img']) && $_POST['already_upload_img']!='')?$_POST['already_upload_img']:'';
    //echo implode(',',$ex_img);
           if(!empty($ex_img))
           {
            $proof_attach_file=$proof_attach_file.",".implode(',', $ex_img);
           }
if($proof_attach_file!='')
{
  $data1['proof_attach_file']=implode(',', array_filter(explode(',', $proof_attach_file)));
}
else
{
  $data1['proof_attach_file']='';
}

  /** end of uploaded images **/
//$data1['proof_attach_file']=$proof_attach_file;
/** end of 29-08-2018 **/
/** 30-08-2018 **/
$data1['crm_assign_other_custom']=(isset($_POST['assign_other_custom'])&&($_POST['assign_other_custom']!=''))? $_POST['assign_other_custom'] : '';
/** end of 30-08-2018 **/
$data1['crm_assign_proof_of_address']=(isset($_POST['proof_of_address'])&&($_POST['proof_of_address']!=''))? $_POST['proof_of_address'] : '';
$data1['crm_assign_meeting_client']=(isset($_POST['meeting_client'])&&($_POST['meeting_client']!=''))? $_POST['meeting_client'] : '';
$data1['crm_assign_source']=(isset($_POST['source'])&&($_POST['source']!=''))? $_POST['source'] : '';
/** 30-06-2018 **/
// $data1['crm_refered_by']=(isset($_POST['refer_exist_client'])&&($_POST['refer_exist_client']!=''))? $_POST['refer_exist_client'] : '';
$data1['crm_refered_by']=(isset($_POST['refered_by'])&&($_POST['refered_by']!=''))? $_POST['refered_by'] : '';
/** 30-06-2018 **/
$data1['crm_assign_relationship_client']=(isset($_POST['relationship_client'])&&($_POST['relationship_client']!=''))? $_POST['relationship_client'] : '';
$data1['crm_assign_notes']=(isset($_POST['assign_notes'])&&($_POST['assign_notes']!=''))? $_POST['assign_notes'] : '';
// other
$data1['crm_previous_accountant']=(isset($_POST['previous_account'])&&($_POST['previous_account']!=''))? $_POST['previous_account'] : '';
$data1['crm_other_name_of_firm']=(isset($_POST['name_of_firm'])&&($_POST['name_of_firm']!=''))? $_POST['name_of_firm'] : '';
$data1['crm_other_address']=(isset($_POST['other_address'])&&($_POST['other_address']!=''))? $_POST['other_address'] : '';
$data1['crm_other_contact_no']=(isset($_POST['pn_no_rec'])&&($_POST['pn_no_rec']!=''))? $_POST['cun_code'].$_POST['pn_no_rec'] : '';
$data1['crm_email']=(isset($_POST['emailid'])&&($_POST['emailid']!=''))? $_POST['emailid'] : '';
$data1['crm_other_chase_for_info']=(isset($_POST['chase_for_info'])&&($_POST['chase_for_info']!=''))? $_POST['chase_for_info'] : '';

/** task creation chase for information 30-08-2018 **/

/** end of 30-08-2018 **/

$data1['crm_other_notes']=(isset($_POST['other_notes'])&&($_POST['other_notes']!=''))? $_POST['other_notes'] : '';

$data1['crm_other_internal_notes']=(isset($_POST['other_internal_notes'])&&($_POST['other_internal_notes']!=''))? $_POST['other_internal_notes'] : '';
$data1['crm_other_invite_use']=(isset($_POST['invite_use'])&&($_POST['invite_use']!=''))? $_POST['invite_use'] : '';
$data1['crm_other_crm']=(isset($_POST['other_crm'])&&($_POST['other_crm']!=''))? $_POST['other_crm'] : '';
$data1['crm_other_proposal']=(isset($_POST['other_proposal'])&&($_POST['other_proposal']!=''))? $_POST['other_proposal'] : '';
$data1['crm_other_task']=(isset($_POST['other_task'])&&($_POST['other_task']!=''))? $_POST['other_task'] : '';
$data1['crm_other_send_invit_link']=(isset($_POST['send_invit_link'])&&($_POST['send_invit_link']!=''))? $_POST['send_invit_link'] : '';
$data1['crm_other_username']=(isset($_POST['user_name'])&&($_POST['user_name']!=''))? $_POST['user_name'] : '';
$data1['crm_other_password']=(isset($_POST['password'])&&($_POST['password']!=''))? $_POST['password'] : '';
$data1['crm_other_any_notes']=(isset($_POST['other_any_notes'])&&($_POST['other_any_notes']!=''))? $_POST['other_any_notes'] : '';
// confirmation

$data1['select_responsible_type']=(isset($_POST['select_responsible_type'])&&($_POST['select_responsible_type']!=''))? $_POST['select_responsible_type'] : '';
// notes tab
$data1['crm_notes_info']=(isset($_POST['notes_info'])&&($_POST['notes_info']!=''))? $_POST['notes_info'] : '';
/** activity log for notes **/
if(isset($_POST['notes_info'])&&($_POST['notes_info']!='')){
  
            $activity_datas['log'] = "Notes Updated ---".$_POST['notes_info'];
            $activity_datas['createdTime'] = time();
            $activity_datas['module'] = 'Client';
            $activity_datas['sub_module']='Client Notes';
            $activity_datas['user_id'] = $_SESSION['admin_id'];
            $activity_datas['module_id'] = (isset($_POST['user_id'])&&($_POST['user_id']==''))? $in : $_POST['user_id'];

            $this->Common_mdl->insert('activity_log',$activity_datas);
  }
/** end of activity log for notes **/
// documents tab
$data1['crm_documents']=(isset($_FILES['document']['name'])&&($_FILES['document']['name']!=''))? $this->Common_mdl->do_upload($_FILES['document'], 'uploads') : '';
//$data1['created_date']=time();
$data1['autosave_status']=0;
$dds = $_POST['user_id'];
$dd = $this->db->query("select * from client where user_id = $dds")->row_array();
$data1['status'] = $dd['status'];

//echo "hai";
$updates_client=$this->Common_mdl->updatewithaffectedrow_count('client',$data1,'user_id',$_POST['user_id']);
//echo $updates_client;


$data_inv['created_date']=time();
$this->Common_mdl->update('client_invoice',$data_inv,'user_id',$_POST['user_id']);

/* ******  */
$datalegal['user_id']=$_POST['user_id'];
$datalegal['company_no'] = isset($_POST['fa_cmyno']) ? $_POST['fa_cmyno'] : "";  
$datalegal['incorporation_date'] = isset($_POST['fa_incordate']) ? $_POST['fa_incordate'] : "";  
$datalegal['regist_address'] = isset($_POST['fa_registadres']) ? $_POST['fa_registadres'] : "";  
$datalegal['turnover'] = isset($_POST['fa_turnover']) ? $_POST['fa_turnover'] : "";  
$datalegal['date_of_trading'] = isset($_POST['fa_dateoftrading']) ? $_POST['fa_dateoftrading'] : ""; 
$datalegal['sic_code'] = isset($_POST['fa_siccode']) ? $_POST['fa_siccode'] : "";  
$datalegal['nuture_of_business'] = isset($_POST['fa_nutureofbus']) ? $_POST['fa_nutureofbus'] : "";  
$datalegal['company_utr'] = isset($_POST['fa_cmyutr']) ? $_POST['fa_cmyutr'] : "";  
$datalegal['company_house_auth_code'] = isset($_POST['fa_cmyhouse']) ? $_POST['fa_cmyhouse'] : "";  

$datalegal['trading_as'] = isset($_POST['bus_tradingas']) ? $_POST['bus_tradingas'] : "";  
$datalegal['commenced_trading'] = isset($_POST['bus_commencedtrading']) ? $_POST['bus_commencedtrading'] : "";  
$datalegal['register_sa'] = isset($_POST['bus_regist']) ? $_POST['bus_regist'] : "";  
$datalegal['bus_turnover'] = isset($_POST['bus_turnover']) ? $_POST['bus_turnover'] : "";  
$datalegal['bus_nuture_of_business'] = isset($_POST['bus_nutureofbus']) ? $_POST['bus_nutureofbus'] : "";  
//print_r($datalegal);
$legalFrom = $this->db->update( 'client_legalform', $datalegal, 'user_id', $_POST['user_id']);
/* ****** */


if($updates_client && isset($_POST['chase_for_info'])=='on'){
    $query = $this->db->query('select * from user where id="'.$_POST['user_id'].'"')->row_array();
    $data2['username'] = $query['username'];
            $user_id = $query['id'];
            $password = $query['password'];
            $encrypt_id = base64_encode($user_id);
            $email_subject  = 'Email For Others';
            $data2['email_content']  = '<a href='.base_url().' style="display: inline-block;text-align: center;color: #00aeef;text-decoration: none;font-family:;background: #00aeef;color: #fff;font-weight: 600;padding: 5px 10px;border-radius: 3px;">Click Here</a><p>Notes :'.$_POST['other_notes'].'</p>';
           $data2['title']='Others Chase for Information Mail';
            //email template
           // $body = $this->load->view('remainder_email.php',$data2, TRUE);
           // $this->load->library('email');
           // $this->email->set_mailtype("html");
           //  $this->email->from('info@remindoo.org');
           //  $this->email->to($query['crm_email_id']);
           //  $this->email->subject($email_subject);
           //  $this->email->message($body);
           //  $send = $this->email->send();
          /* if($send){
                echo 'welcome email remainder send';
            
            }else{
                echo 'welcome email remainder not send';
               
            }*/
}

if($updates_client){
    $success = $this->Common_mdl->update_field($this->input->post(), $_POST['user_id']);


}

echo $_POST['user_id'];

}
else { echo '0';}
       
    }




//Company house function Start//
public function selectcompany()
{
   $this->session->set_userdata('c_status', '1');

//https://api.companieshouse.gov.uk/company/{company_number}

    $companyNo = $_POST['companyNo'];
    $user_id = (isset($_POST['user_id'])&&($_POST['user_id']!=''))? $_POST['user_id'] : '';
// first api
        $curl = curl_init();
        $items_per_page = '10';
        //curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk/company/05755918/exemptions'); 

curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk/company/'.$companyNo.'/officers?items_per_page=10&register_type=directors&register_view=false'); 
//curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk/company/'.$companyNo.'/officers?items_per_page=10&register_type=directors&register_view=false'); 

        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET'); 
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_USERPWD,$this->companieshouseAPIkey);
        $response = curl_exec($curl);
        
        if($errno = curl_errno($curl)) {
        $error_message = curl_strerror($errno);
        echo "cURL error ({$errno}):\n {$error_message}";
        } else {
        curl_close($curl);
        $data = json_decode($response);
        $array_rec = $this->object_2_array($data);
      /* echo '<pre>';
        print_r($array_rec);
      //  die; */

// second api
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk/company/'.$companyNo.'/'); 

//https://beta.companieshouse.gov.uk/company/06335955

        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET'); 
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_USERPWD,$this->companieshouseAPIkey);
        $response1 = curl_exec($curl);
    // echo '<pre>';
    //     print_r($response1);
    //     die;
        if($errno = curl_errno($curl)) {
        $error_message = curl_strerror($errno);
        echo "cURL error ({$errno}):\n {$error_message}";
        }
        curl_close($curl);
        $data1 = json_decode($response1);

        $array_rec1 = $this->object_2_array($data1);  

        // echo "<pre>";
        // print_r($array_rec1);
        //echo $array_rec1['links']['persons_with_significant_control'];die;
      /*  if(isset($array_rec1['links']['persons_with_significant_control']))
        {
          $psc_details = $this->getPSC($array_rec1['links']['persons_with_significant_control']);
        }else{
          $psc_details = "No";
        }
        echo '<pre>';
        print_r($psc_details);die;*/
        $address1= (isset($array_rec1['registered_office_address']['address_line_1'])&&($array_rec1['registered_office_address']['address_line_1']!=''))? $array_rec1['registered_office_address']['address_line_1']:''; 
        
        $address2= (isset($array_rec1['registered_office_address']['address_line_2'])&&($array_rec1['registered_office_address']['address_line_2']!=''))? $array_rec1['registered_office_address']['address_line_2']:''; 
        $address3= (isset($array_rec1['registered_office_address']['address_line_3'])&&($array_rec1['registered_office_address']['address_line_3']!=''))? $array_rec1['registered_office_address']['address_line_3']:''; 
        $registered_in=(isset($array_rec1['jurisdiction'])&&($array_rec1['jurisdiction']!=''))? $array_rec1['jurisdiction']:''; 
        $crm_town_city=(isset($array_rec1['registered_office_address']['locality'])&&($array_rec1['registered_office_address']['locality']!=''))? $array_rec1['registered_office_address']['locality']:'';
        $crm_post_code=(isset($array_rec1['registered_office_address']['postal_code'])&&($array_rec1['registered_office_address']['postal_code']!=''))? $array_rec1['registered_office_address']['postal_code']:'';
        
        $period_end_on= (isset($array_rec1['accounts']['next_accounts']['period_end_on'])&&($array_rec1['accounts']['next_accounts']['period_end_on']!=''))? $array_rec1['accounts']['next_accounts']['period_end_on']:''; 
        
        $next_made_up_to= (isset($array_rec1['accounts']['next_made_up_to'])&&($array_rec1['accounts']['next_made_up_to']!=''))? $array_rec1['accounts']['next_made_up_to']:''; 
        
        $next_due= (isset($array_rec1['accounts']['next_due'])&&($array_rec1['accounts']['next_due']!=''))? $array_rec1['accounts']['next_due']:'';
         
        $confirmation_next_made_up_to= (isset($array_rec1['confirmation_statement']['next_made_up_to'])&&($array_rec1['confirmation_statement']['next_made_up_to']!=''))? $array_rec1['confirmation_statement']['next_made_up_to']:'';
        
        $confirmation_next_due= (isset($array_rec1['confirmation_statement']['next_due'])&&($array_rec1['confirmation_statement']['next_due']!=''))? $array_rec1['confirmation_statement']['next_due']:'';


        if(!empty($array_rec1['links'])){
              $company_url= 'https://beta.companieshouse.gov.uk'.$array_rec1['links']['self'];
              $officers_url=(isset($array_rec1['links']['officers']))?'https://beta.companieshouse.gov.uk'.$array_rec1['links']['officers']:''; 
        }
        else
        {
           $company_url= '';
              $officers_url='';
        }
        // $company_url= 'https://beta.companieshouse.gov.uk'.$array_rec1['links']['self'];
        // $officers_url='https://beta.companieshouse.gov.uk'.$array_rec1['links']['officers'];
     if(isset($array_rec1['sic_codes']) && $array_rec1['sic_codes']!='') {  

       /* foreach($array_rec1['sic_codes'] as $key => $val){
$code=$this->db->query('SELECT * FROM sic_code WHERE COL1="'.$val.'"')->row_array();
$sic_code[]='<option value='.$code['COL1'].'>'.$code['COL1'].'-'.$code['COL2'].'</option>';

}*/
$code=$this->db->query('SELECT * FROM sic_code WHERE COL1="'.$array_rec1['sic_codes'][0].'"')->row_array();
$sic_code= $code['COL1'].'-'.$code['COL2'];
$codess=$this->db->query('SELECT * FROM sic_code WHERE COL1="'.$array_rec1['sic_codes'][0].'"')->row_array();
$sic_codes=$array_rec1['sic_codes'];
$nature_business=$codess['COL1'].'-'.$codess['COL2'];
} else {
    //$sic_code[]='<option value="select">select</option>';
    $sic_code='';
    $sic_codes='';
    $nature_business='';
}
    //echo '<pre>';
       // print_r($array_rec['items']);
       //echo  $array_rec1['sic_codes'];
       // die;
        $content = '';
        if(!empty($array_rec1['company_name'])){
        $content .='<h3>'.$array_rec1['company_name'].'</h3>';
        }

        $array = array('First','Second','Third','Fourth','Fifth','Sixth','Seventh','Eight','Nineth','Tenth');


        $i=0;

        if(!empty($array_rec['items'])){
          // echo "<pre>";
          // print_r($array_rec['items']);
          // echo "</pre>";
          $k=1;
          $c1='';
        foreach ($array_rec['items'] as $key => $value) {

            if($k==1){ $c1='Use as Primary Contact'; }else{ $c1='Use as Secondary Contact'; }
// if($value['officer_role'] == 'director' && !isset($value['resigned_on'])) {
if($value['officer_role'] !='' && !isset($value['resigned_on'])) {          
$dateofbirth=(isset($value['date_of_birth']['year'])&&$value['date_of_birth']['year']!='') ? $value['date_of_birth']['year'] : '';
$month=(isset($value['date_of_birth']['month'])&&$value['date_of_birth']['month']!='') ? date("F", mktime(0, 0, 0, $value['date_of_birth']['month'], 10)) : '';
$concat=$dateofbirth.' '.$month;

/** 21-08-2018 **/
if($user_id!='')
{
  $get_db_user_contact=$this->db->query('select * from firm_contacts where user_id='.$user_id.' and contact_count="'.$array[$i].'" ')->result_array();  
//  echo 'select * from client_contacts where client_id='.$user_id.' and contact_count="'.$array[$i].'" ';

  if(count($get_db_user_contact)==0)
  {
    $content .='
    <div class="trading_tables"> 
    <div class="trading_rate">
    <p>
    <span><strong>'.$value['name'].'</strong></span>
    <span>Born '.$dateofbirth.'</span>
    <span class="fields" id="usecontact'.$array[$i].'"><a href="#" onclick="return getmaincontact('."'".$companyNo."'".','."'".$value['links']['officer']['appointments']."'".','."'".$array[$i]."'".','."'".$concat."'".');">'.$c1.'</a></span>
    </p> 
    </div>
    </div>';
  }
  else
  {
       $content .='
    <div class="trading_tables"> 
    <div class="trading_rate">
    <p>
    <span><strong>'.$value['name'].'</strong></span>
    <span>Born '.$dateofbirth.'</span>
    <span class="fields" id="usecontact'.$array[$i].'"><span class="succ_contact"><a href="#">Added</a></span></span>
    </p> 
    </div>
    </div>';
  }
}
else{
/**end of 21-08-2018 **/ 
  $content .='
  <div class="trading_tables"> 
  <div class="trading_rate">
  <p>
  <span><strong>'.$value['name'].'</strong></span>
  <span>Born '.$dateofbirth.'</span>
  <span class="fields" id="usecontact'.$array[$i].'"><a href="#" onclick="return getmaincontact('."'".$companyNo."'".','."'".$value['links']['officer']['appointments']."'".','."'".$array[$i]."'".','."'".$concat."'".');">'.$c1.'</a></span>
  </p> 
  </div>
  </div>';
 
 }
  $i++;
}
$k++;
}
}


//echo json_encode($datas);

//echo $content;
//if($_SESSION['roleId']=='1'){

   //  $role=2;

//}else{
    $role=4;
//}

$user_value=array(


            'role'=> '',
            'status'=>0,
            //'autosave_status'=>1,
            'autosave_status'=>0,// rs 30-06-2018
            'company_roles'=>1,
            'CreatedTime'=>time(),
            'user_type'=>'FA',
    );
// if($_SESSION['roleId']=='1'){
// $user_value['firm']=1;
// }

 $updates_user=(isset($_POST['user_id'])&&($_POST['user_id']==''))? $this->db->insert( 'user', $user_value ) : $this->Common_mdl->update('user',$user_value,'id',$_POST['user_id']);
$insert_data= $updates_user;
$idd = $_POST['user_id'];
if($idd=='')
{
    $idd = $this->db->insert_id();
}


$rec = $this->db->query('select * from user order by id desc limit 0,1')->row_array();
//$content .='<a href='.base_url().'client/client_info/'.$rec['id'].' class="view_contacts">Back</a>';
//$content .='<span class="view_contacts" onclick="return backto_company();">Back</span>';
//$content .='<span class="view_contacts" onclick="return company_model_close();">Exit</span>';
$content .='<span class="view_contacts" onclick="return addmorecontact('.$idd.');">Next</span>';
  $cmpny_type='';
if(!empty($array_rec1['type'])){
   if($array_rec1['type']=='ltd')
   {
      $cmpny_type='Private Limited company';
   }
   else if($array_rec1['type']=='plc')
   {
    $cmpny_type='Public Limited company';
   }
   else if($array_rec1['type']=='llp')
   {
    $cmpny_type='Limited Liability Partnership';
   }
   else
   {
    $cmpny_type=$array_rec1['type'];
   }
 }

if($updates_user){
//$in=$this->db->insert_id();
$in=$idd;
/*print_r($address2);
exit;*/
 
$client_value=array('crm_company_name' => (!empty($array_rec1['company_name']))?$array_rec1['company_name']:'',
    'crm_allocation_holder'=>'wagqs',
        'crm_company_number'=>$companyNo,
    'crm_register_address'=>(!empty($array_rec1['registered_office_address']))?$array_rec1['registered_office_address']['address_line_1'].$address2.$array_rec1['registered_office_address']['locality'].$array_rec1['registered_office_address']['postal_code']:'',
    'crm_company_url'=>$company_url,
    'crm_officers_url'=>$officers_url,
    'crm_company_status'=>(!empty($array_rec1['company_status']))?$array_rec1['company_status']:'',
   // 'crm_company_type'=>$array_rec1['type'],
    'crm_company_type'=>$cmpny_type,
    'crm_incorporation_date'=>(!empty($array_rec1['date_of_creation']))?$array_rec1['date_of_creation']:'',
        
    'created_date'=>time(),
    'user_id'=>(isset($_POST['user_id'])&&($_POST['user_id']==''))? $in : $_POST['user_id'],
    //'autosave_status'=>1,
    'autosave_status'=>0,//rs 30-06-2018

                    "crm_company_sic"=> $sic_code,
                    
                    //"crm_register_address"=>$array_rec1['registered_office_address']['address_line_1'],
                    
                    'crm_incorporation_date'=>(!empty($array_rec1['date_of_creation']))?date("d-m-Y", strtotime($array_rec1['date_of_creation'])):'',
                    //'crm_accounts_due_date_hmrc'=>date("F j, Y", strtotime("+1 years", strtotime($next_made_up_to))),
                    "crm_allocation_holder" => '',
                    'crm_registered_in'=>$registered_in,
                    'crm_address_line_one'=>$address1,
                    'crm_address_line_two'=>$address2,
                    'crm_address_line_three'=>$address3,
                    'crm_town_city'=>$crm_town_city,
                    'crm_post_code'=>$crm_post_code,
                    'crm_previous_accountant'=>(isset($_POST['previous_account'])&&($_POST['previous_account']!=''))? $_POST['previous_account'] : '',
                    'crm_assign_other_custom'=>(isset($_POST['assign_other_custom'])&&($_POST['assign_other_custom']!=''))? $_POST['assign_other_custom'] : '',
/** end of 30-08-2018 **/
'crm_assign_proof_of_address'=>(isset($_POST['proof_of_address'])&&($_POST['proof_of_address']!='')) ? $_POST['proof_of_address'] : '',
'crm_assign_meeting_client'=>(isset($_POST['meeting_client'])&&($_POST['meeting_client']!=''))? $_POST['meeting_client'] : '',
'crm_assign_source'=>(isset($_POST['source'])&&($_POST['source']!=''))? $_POST['source'] : '',
/** 30-06-2018 **/
// $data1['crm_refered_by']=(isset($_POST['refer_exist_client'])&&($_POST['refer_exist_client']!=''))? $_POST['refer_exist_client'] : '';
'crm_refered_by'=>(isset($_POST['refered_by'])&&($_POST['refered_by']!=''))? $_POST['refered_by'] : '',
/** 30-06-2018 **/
'crm_assign_relationship_client'=>(isset($_POST['relationship_client'])&&($_POST['relationship_client']!=''))? $_POST['relationship_client'] : '',
'crm_assign_notes'=>(isset($_POST['assign_notes'])&&($_POST['assign_notes']!=''))? $_POST['assign_notes'] : '',
// other
'crm_previous_accountant'=>(isset($_POST['previous_account'])&&($_POST['previous_account']!=''))? $_POST['previous_account'] : '',
'crm_other_name_of_firm'=>(isset($_POST['name_of_firm'])&&($_POST['name_of_firm']!=''))? $_POST['name_of_firm'] : '',
'crm_other_address'=>(isset($_POST['other_address'])&&($_POST['other_address']!=''))? $_POST['other_address'] : '',
'crm_other_contact_no'=>(isset($_POST['pn_no_rec'])&&($_POST['pn_no_rec']!=''))? $_POST['cun_code'].$_POST['pn_no_rec'] : '',
'crm_email'=>(isset($_POST['emailid'])&&($_POST['emailid']!=''))? $_POST['emailid'] : '',
'crm_other_chase_for_info'=>(isset($_POST['chase_for_info'])&&($_POST['chase_for_info']!=''))? $_POST['chase_for_info'] : '',

/** task creation chase for information 30-08-2018 **/

/** end of 30-08-2018 **/

'crm_other_notes'=>(isset($_POST['other_notes'])&&($_POST['other_notes']!=''))? $_POST['other_notes'] : '',

'crm_other_internal_notes'=>(isset($_POST['other_internal_notes'])&&($_POST['other_internal_notes']!=''))? $_POST['other_internal_notes'] : '',
'crm_other_invite_use'=>(isset($_POST['invite_use'])&&($_POST['invite_use']!=''))? $_POST['invite_use'] : '',
'crm_other_crm'=>(isset($_POST['other_crm'])&&($_POST['other_crm']!=''))? $_POST['other_crm'] : '',
'crm_other_proposal'=>(isset($_POST['other_proposal'])&&($_POST['other_proposal']!=''))? $_POST['other_proposal'] : '',
'crm_other_task'=>(isset($_POST['other_task'])&&($_POST['other_task']!=''))? $_POST['other_task'] : '',
'crm_other_send_invit_link'=>(isset($_POST['send_invit_link'])&&($_POST['send_invit_link']!=''))? $_POST['send_invit_link'] : '',
'crm_other_username'=>(isset($_POST['user_name'])&&($_POST['user_name']!=''))? $_POST['user_name'] : '',
'crm_other_password'=>(isset($_POST['password'])&&($_POST['password']!=''))? $_POST['password'] : '',
'crm_other_any_notes'=>(isset($_POST['other_any_notes'])&&($_POST['other_any_notes']!=''))? $_POST['other_any_notes'] : '',
// confirmation

'select_responsible_type'=>(isset($_POST['select_responsible_type'])&&($_POST['select_responsible_type']!=''))? $_POST['select_responsible_type'] : ''
// notes tab
// 'crm_notes_info'=>(isset($_POST['notes_info'])&&($_POST['notes_info']!=''))? $_POST['notes_info'] : ''
    );

$updates_client=(isset($_POST['user_id'])&&($_POST['user_id']==''))? $this->db->insert( 'firm', $client_value ) : $this->Common_mdl->update('firm',$client_value,'user_id',$_POST['user_id']);
//echo $this->db->last_query();die;
}
if((!empty($array_rec1)) && $array_rec1['registered_office_address']['postal_code']=='')
{
    $array_rec1['registered_office_address']['postal_code'] ='';
}
$return_arr = array( "html" => $content,
                    "company_name" => (!empty($array_rec1['company_name']))?$array_rec1['company_name']:'',
                    "company_number" => $companyNo,
                    "company_url"=>$company_url,
                    "officers_url"=>$officers_url,
                    "company_status"=>(!empty($array_rec1['company_status']))?$array_rec1['company_status']:'',
                   // "company_type"=>$array_rec1['type'],
                    "company_type"=>$cmpny_type,
                    "company_sic"=> $sic_code,
                    'sic_codes'=>$sic_codes,
                    'nature_business'=>$nature_business,
                    /*"company_sic"=>,
                    "auth_code"=>,
                    "company_utr"=>,*/
                    "address1"=>(!empty($array_rec1['registered_office_address']))?$array_rec1['registered_office_address']['address_line_1']:'',
                    "address2"=>$address2,
                    "locality"=>(!empty($array_rec1['registered_office_address']))?$array_rec1['registered_office_address']['locality']:'',
                    "postal"=>(!empty($array_rec1['registered_office_address']))?$array_rec1['registered_office_address']['postal_code']:'',
                    'date_of_creation'=>(!empty($array_rec1['date_of_creation']))?date("d-m-Y", strtotime($array_rec1['date_of_creation'])):'',
                    'period_end_on'=>date("d-m-Y", strtotime($period_end_on)),
                    'next_made_up_to'=>date("d-m-Y", strtotime($next_made_up_to)),
                    'next_due'=>date("d-m-Y", strtotime($next_due)),
                    'accounts_due_date_hmrc'=>date("F j, Y", strtotime("+1 years", strtotime($next_made_up_to))),
                    'accounts_tax_date_hmrc'=>date("F j, Y", strtotime("+1 days", strtotime($next_due))),
                    'confirm_next_made_up_to'=>date("d-m-Y", strtotime($confirmation_next_made_up_to)),
                    'confirm_next_due'=>date("d-m-Y", strtotime($confirmation_next_due)),
                    "allocation_holder" => '',
                    'user_id'=> (isset($_POST['user_id'])&&($_POST['user_id']==''))? $in : $_POST['user_id'], 
                    );

echo json_encode($return_arr);
  }      
}


public function object_2_array($result)
{
    $array = array();
    if(!empty($result)){
    foreach ($result as $key=>$value)
    {

       # if $value is an array then
        if (is_array($value))
        {
            #you are feeding an array to object_2_array function it could potentially be a perpetual loop.
            $array[$key]=$this->object_2_array($value);
        }

       # if $value is not an array then (it also includes objects)
        else
        {
       # if $value is an object then
        if (is_object($value))
        {
            $array[$key]=$this->object_2_array($value);
        } else {

            $array[$key]=$value;
}
        }
    }
  }
    return $array;
}


public function maincontact()
{


    $companyNo = $_POST['companyNo'];
    $appointments=$_POST['appointments'];
    $cnt=$_POST['cnt'];
    $count_house=(isset($_POST['count']))?$_POST['count']:'';
    $birth=(isset($_POST['birth'])&&($_POST['birth']!=''))? $_POST['birth']:'';
// first api
        $curl = curl_init();
        $items_per_page = '10';
  

//curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk/company/'.$companyNo.'/officers?items_per_page=10&register_type=directors&register_view=false');
curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk'.$appointments);
 
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET'); 
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_USERPWD,$this->companieshouseAPIkey);
        $response = curl_exec($curl);

        
        if($errno = curl_errno($curl)) {
        $error_message = curl_strerror($errno);
        echo "cURL error ({$errno}):\n {$error_message}";
        } else {
        curl_close($curl);
        $data = json_decode($response);
        $array_rec = $this->object_2_array($data);
/*echo '<pre>';
        print_r($array_rec);
        die;*/
// second api
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk/company/'.$companyNo.'/persons-with-significant-control/'); 

        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET'); 
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_USERPWD,$this->companieshouseAPIkey);
        $response1 = curl_exec($curl);
        
        if($errno = curl_errno($curl)) {
        $error_message = curl_strerror($errno);
        echo "cURL error ({$errno}):\n {$error_message}";
        }
        curl_close($curl);
        $data1 = json_decode($response1);
        $array_rec1 = $this->object_2_array($data1); 
               
      
if(isset($array_rec1['items'])) {
 foreach ($array_rec1['items'] as $key => $value) {
   $nature_of_control=$value['natures_of_control']; 
   $psc='yes';   

}      
 
$implode_nature=implode(',', $nature_of_control);
} else{
  $implode_nature='';
  $psc='';
}        
foreach ($array_rec['items'] as $key => $value) {
        
if($value['appointed_to']['company_number'] == $companyNo) {      
    $content=$value;
} else{
    $content=$value; 
}
}        //echo json_encode($datas);
$title= (isset($content['name_elements']['title'])&&($content['name_elements']['title']!=''))? $content['name_elements']['title']:'';
$fore_name= (isset($content['name_elements']['forename'])&&($content['name_elements']['forename']!=''))? $content['name_elements']['forename']:'';
$otherfore_name= (isset($content['name_elements']['other_forenames'])&&($content['name_elements']['other_forenames']!=''))? $content['name_elements']['other_forenames']:'';
$sur_name= (isset($content['name_elements']['surname'])&&($content['name_elements']['surname']!=''))? $content['name_elements']['surname']:'';
$address_line_1= (isset($content['address']['address_line_1'])&&($content['address']['address_line_1']!=''))? $content['address']['address_line_1']:'';
$address_line_2= (isset($content['address']['address_line_2'])&&($content['address']['address_line_2']!=''))? $content['address']['address_line_2']:'';
$premises= (isset($content['address']['premises'])&&($content['address']['premises']!=''))? $content['address']['premises']:'';
$region= (isset($content['address']['region'])&&($content['address']['region']!=''))? $content['address']['region']:'';
$country= (isset($content['address']['country'])&&($content['address']['country']!=''))? $content['address']['country']:'';
$locality= (isset($content['address']['locality'])&&($content['address']['locality']!=''))? $content['address']['locality']:'';
$postal_code= (isset($content['address']['postal_code'])&&($content['address']['postal_code']!=''))? $content['address']['postal_code']:'';
$nationality= (isset($content['nationality'])&&($content['nationality']!=''))? $content['nationality']:'';
$occupation= (isset($content['occupation'])&&($content['occupation']!=''))? $content['occupation']:'';
$appointed_on= (isset($content['appointed_on'])&&($content['appointed_on']!=''))? $content['appointed_on']:'';
$country_of_residence= (isset($content['country_of_residence'])&&($content['country_of_residence']!=''))? $content['country_of_residence']:'';
// if($_SESSION['roleId']=='1'){
// $role=2;
// }else{
    $role=4;
//}
$user_value=array(
            'role'=> '',
            'status'=>0,
            //'autosave_status'=>1,
            'autosave_status'=>0, //rs 30-06-2018
            'company_roles'=>1,
            'CreatedTime'=>time(),
            'user_type'=>'FA',
    );

 $updates_user=(isset($_POST['user_id'])&&($_POST['user_id']==''))? $this->db->insert( 'user', $user_value ) : $this->Common_mdl->update('user',$user_value,'id',$_POST['user_id']);
 //$client_autoId = $this->db->last_id();
//echo $this->db->last_query();

$insert_data= $updates_user;
$in=$this->db->insert_id();
if($insert_data){

//echo $in;die;
$data_c = array();
$data = array();
$data_c['user_id'] = $_POST['user_id'];
$data_c['title'] = $title;
$data_c['first_name'] = $fore_name;
$data_c['surname'] = $sur_name;
$data_c['preferred_name'] = $otherfore_name;
$data_c['address_line1'] = $address_line_1;
$data_c['address_line2'] = $address_line_2;
$data_c['premises'] = $premises;
$data_c['region'] = $region;
$data_c['country'] = $country;
$data_c['locality'] = $locality;
$data_c['post_code'] = $postal_code;
$data_c['nationality'] = $nationality;
$data_c['occupation'] = $occupation;
$data_c['appointed_on'] = $appointed_on;
$data_c['country_of_residence'] = $country_of_residence;
$data_c['psc'] = $psc;
$data_c['date_of_birth']=$birth;
$data_c['nature_of_control'] = $implode_nature;

$data_c['created_date'] = time();
/** 21-08-2018 **/
$data_c['contact_count']=$count_house;

if($count_house=='First'){
  $data_c['make_primary ']='1';
}
/** end of 21-08-2018 **/

$result = $this->Common_mdl->insert('firm_contacts',$data_c);
   

$data['cnt'] = $cnt;
$data['birth']=$birth;
$data['incre']=$_POST['incre'];
$data['user_id'] = $_POST['user_id'];
$data['title'] = $title;
$data['first_name'] = $fore_name;
$data['surname'] = $sur_name;
$data['preferred_name'] = $otherfore_name;
$data['address_line1'] = $address_line_1;
$data['address_line2'] = $address_line_2;
$data['premises'] = $premises;
$data['region'] = $region;
$data['country'] = $country;
$data['locality'] = $locality;
$data['post_code'] = $postal_code;
$data['nationality'] = $nationality;
$data['occupation'] = $occupation;
$data['appointed_on'] = $appointed_on;
$data['country_of_residence'] = $country_of_residence;
$data['psc'] = $psc;
$data['nature_of_control'] = $implode_nature;
$data['contact_from']='company_house';



$client_value=array('crm_first_name' => $fore_name,
    'crm_middle_name'=>$otherfore_name,
    'crm_last_name'=>$sur_name,
    'status'=>1,
    'created_date'=>time(),
    'user_id'=>(isset($_POST['user_id'])&&($_POST['user_id']==''))? $in : $_POST['user_id'],
   // 'autosave_status'=>1
    'autosave_status'=>0,//rs 30-06-2018
    );
$updates_client=(isset($_POST['user_id'])&&($_POST['user_id']==''))? $this->db->insert( 'client', $client_value ) : $this->Common_mdl->update('client',$client_value,'user_id',$_POST['user_id']);

//echo $this->db->last_query();die;
}
        $datas['rec'] = $this->Common_mdl->GetAllWithWhere('firm_contacts','user_id',$_POST['user_id']);
    //echo $this->db->last_query();die;
    $this->load->view('clients/contact_form',$data);           
}
}

//Company house function End//


//Add  //


    public function firm_information($user_id=false)
      {
          error_reporting(0);
          $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
          $this->session->set_userdata('mail_url', $actual_link);
          $this->Loginchk_model->superAdmin_LoginCheck();   
          $data['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user','role','1');
          $data['client']=$this->Common_mdl->GetAllWithWhere('firm','user_id',$user_id);
          $data['responsible_team']=$this->Common_mdl->GetAllWithWhere('responsible_team','client_id',$user_id);
          $data['responsible_user']=$this->Common_mdl->GetAllWithWhere('responsible_user','client_id',$user_id);
          $data['responsible_department']=$this->Common_mdl->GetAllWithWhere('responsible_department','client_id',$user_id);
          $data['responsible_member']=$this->Common_mdl->GetAllWithWhere('responsible_members','client_id',$user_id);
          //$data['contactRec']=$this->Common_mdl->GetAllWithWhere('client_contacts','client_id',$user_id);
          $data['contactRec']=$this->db->query("SELECT * FROM  `firm_contacts` WHERE user_id =  '".$user_id."' ORDER BY make_primary DESC ")->result_array();
          $data['user']=$this->Common_mdl->GetAllWithWhere('user','id',$user_id);

          $data['staff_form'] = $this->Common_mdl->GetAllWithWheretwo('user','role','6','firm_admin_id',$_SESSION['id']);
         // $data['referby'] = $this->db->query("SELECT * FROM `client` where crm_first_name!='' and crm_first_name!='0' GROUP BY `crm_first_name`")->result_array();
          /** 21-08-2018 **/
  $query1=$this->db->query("SELECT * FROM `user` where role=4 AND autosave_status!='1' and crm_name!='' and firm_admin_id=".$_SESSION['id']." and status=1 order by id DESC");
            $results1 = $query1->result_array();
            $res=array();
            if(count($results1)>0){
              foreach ($results1 as $key => $value) {
                   array_push($res, $value['id']);
                }  
            }
            if(!empty($res)){
              $im_val=implode(',',$res);
              //echo $im_val."abc abc";
          $data['referby']=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id in (".$im_val.") order by id desc ")->result_array();    
           //$results = $query->result_array(); 
           }
           else
           {
             $data['referby']=array();
           }
          /** end of 21-08-2018 **/
          $data['teamlist']=$this->Common_mdl->getallrecords('team');
          $data['deptlist']=$this->Common_mdl->getallrecords('department');
          // $data['staff_form'] = $this->Common_mdl->GetAllWithWhere('user','role','6');
          // $data['managed_by'] = $this->Common_mdl->GetAllWithWhere('user','role','5');
           $data['staff_form'] =$this->db->query("select * from user where role in(5,6,3) and firm_admin_id=".$_SESSION['id']." ")->result_array();
        $data['managed_by'] = $this->db->query("select * from user where role in(5,3) and firm_admin_id=".$_SESSION['id']." ")->result_array();
          if($user_id!=''){
             $query=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id='".$user_id."' order by id desc ")->result_array();
             $res=array();
                foreach ($query as $key => $value) {
                     array_push($res, $value['id']);
                  }  
                  $im_val=implode(',',$res); 
                  $query_res=$this->db->query("SELECT * FROM add_new_task WHERE company_name in ('".$im_val."') order by id desc "); 
          // $data['task_list']=$this->Common_mdl->GetAllWithWhere('add_new_task','user_id',$user_id);
          $data['task_list']=$query_res->result_array();
          }else{
          $data['task_list']=$this->Common_mdl->getallrecords('add_new_task');
          }
          
          if($user_id!=''){

            // $query=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id='".$user_id."' order by id desc ")->result_array();
            //  $res=array();
            //     foreach ($query as $key => $value) {
            //          array_push($res, $value['id']);
            //       }  
            //       $im_val=implode(',',$res); 
            //       $query=$this->db->query("SELECT * FROM proposals WHERE company_id in ('".$im_val."') order by id desc "); 
             $query=$this->db->query("SELECT * FROM proposals WHERE company_id=".$user_id."  order by id desc ");      
           $data['proposals']=$query->result_array();
          }else{
          $data['proposals']=$this->Common_mdl->getallrecords('proposals');
          }

          /** rspt for leads **/
          if($user_id!=''){
             $query=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id='".$user_id."' order by id desc ")->result_array();
             $res=array();
                                foreach ($query as $key => $value) {
                                     array_push($res, $value['id']);
                                  }  
                                  $im_val=implode(',',$res); 
                                   $query=$this->db->query("SELECT * FROM leads WHERE company in ('".$im_val."') order by id desc ");  
                                  // echo "SELECT * FROM leads WHERE user_id in ('".$im_val."') order by id desc";
          $data['leads']=$query->result_array();
          }else{
          $data['leads']=$this->Common_mdl->getallrecords('leads');
          }

          /** rspt 20-08-2018 for invoice view **/
           if($user_id!=''){
          $data['invoice_details'] = $this->Invoice_model->selectAllRecord('Invoice_details', 'client_email', $user_id);
           }

           if($user_id!=''){
          $data['reminder_details'] = $this->Invoice_model->selectAllRecord('client_reminder', 'user_id', $user_id);
           }


          /** end of 20-08-2018 **/

          /** end opf leads **/
  
  //print_r($data['staff_form']);die;
          //first api
          $curl = curl_init();
          $items_per_page = '10';
       
  curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk/company/'.$data['client'][0]['crm_company_number'].'/'); 
  
          curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET'); 
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($curl, CURLOPT_HEADER, false);
          curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($curl, CURLOPT_USERPWD,$this->companieshouseAPIkey);
          $response = curl_exec($curl);
         /* echo '<pre>';
          print_r($response);
          die;*/
          if($errno = curl_errno($curl)) {
          $error_message = curl_strerror($errno);
          echo "cURL error ({$errno}):\n {$error_message}";
          } else {
          curl_close($curl);
          $data_rec = json_decode($response);
           $array_rec = $this->object_2_array($data_rec);
           $data['company']=$array_rec;
  
  // second api
          $curl = curl_init();
       
  curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk/company/'.$data['client'][0]['crm_company_number'].'/officers?items_per_page=10&register_type=directors&register_view=false'); 
  
          curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET'); 
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($curl, CURLOPT_HEADER, false);
          curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($curl, CURLOPT_USERPWD,$this->companieshouseAPIkey);
          $response1 = curl_exec($curl);
  
          
          if($errno = curl_errno($curl)) {
          $error_message = curl_strerror($errno);
          echo "cURL error ({$errno}):\n {$error_message}";
          }
          curl_close($curl);
          $data1 = json_decode($response1);
          $array_rec1 = $this->object_2_array($data1);   
    /*echo '<pre>';
          print_r($array_rec1);
         // die; */ 
  
         
  $data['officers']=$array_rec1['items'];
    
  foreach($array_rec1['items'] as $key => $value){ 
  if($value['officer_role'] == 'director') {
  
  // third api
          $curl = curl_init();
          $items_per_page = '10';
    
  curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk'.$value['links']['officer']['appointments']);
   
          curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET'); 
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($curl, CURLOPT_HEADER, false);
          curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($curl, CURLOPT_USERPWD,$this->companieshouseAPIkey);
          $response2 = curl_exec($curl);
  
          
          if($errno = curl_errno($curl)) {
          $error_message = curl_strerror($errno);
          echo "cURL error ({$errno}):\n {$error_message}";
          } 
          curl_close($curl);
          $data2 = json_decode($response2);
          //$data['array_rec2'][]= $this->object_2_array($data2);
          $array_rec2= $this->object_2_array($data2);
          /*echo "<pre>";
          print_r($array_rec2['items']);die; */
  
            foreach ($array_rec2['items'] as $key => $val) {
          
  if((isset($val['appointed_to']['company_number']) == isset($data['client'][0]['crm_company_number'])) &&  (isset($val['appointed_on']) == isset($value['appointed_on']))  ) {
            
  $data['content'][]=$val;
  } /*else{
     $data['content'][]=$value; 
  }*/
  }
          //echo json_encode($datas);
          //echo $content;
  }
  } 
  
  // fourth filling history api
          $curl = curl_init();
       
  curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk/company/'.$data['client'][0]['crm_company_number'].'/filing-history'); 
  
          curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET'); 
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($curl, CURLOPT_HEADER, false);
          curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($curl, CURLOPT_USERPWD,$this->companieshouseAPIkey);
          $response3 = curl_exec($curl);
  
          
          if($errno = curl_errno($curl)) {
          $error_message = curl_strerror($errno);
          echo "cURL error ({$errno}):\n {$error_message}";
          }
          curl_close($curl);
          $data3 = json_decode($response3);
          $data['array_rec3'] = $this->object_2_array($data3);  
    }  //else close  
        $data['feedback'] =$this->db->query("select id,username,crm_email_id from user where username!='' and username!='0' and  crm_email_id!='' and crm_email_id!='0' "  )->result();  
          $this->load->view('Firm/firm_information',$data);
      }



public function firm_info($user_id=false)
      {
          error_reporting(0);
          $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
          $this->session->set_userdata('mail_url', $actual_link);
          $this->Loginchk_model->superAdmin_LoginCheck();   
          $data['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user','role','1');
          $data['client']=$this->Common_mdl->GetAllWithWhere('firm','user_id',$user_id);
          $data['responsible_team']=$this->Common_mdl->GetAllWithWhere('responsible_team','client_id',$user_id);
          $data['responsible_user']=$this->Common_mdl->GetAllWithWhere('responsible_user','client_id',$user_id);
          $data['responsible_department']=$this->Common_mdl->GetAllWithWhere('responsible_department','client_id',$user_id);
          $data['responsible_member']=$this->Common_mdl->GetAllWithWhere('responsible_members','client_id',$user_id);
          //$data['contactRec']=$this->Common_mdl->GetAllWithWhere('client_contacts','client_id',$user_id);
          $data['contactRec']=$this->db->query("SELECT * FROM  `firm_contacts` WHERE user_id =  '".$user_id."' ORDER BY make_primary DESC ")->result_array();
          $data['user']=$this->Common_mdl->GetAllWithWhere('user','id',$user_id);

          $data['staff_form'] = $this->Common_mdl->GetAllWithWheretwo('user','role','6','firm_admin_id',$_SESSION['id']);
         // $data['referby'] = $this->db->query("SELECT * FROM `client` where crm_first_name!='' and crm_first_name!='0' GROUP BY `crm_first_name`")->result_array();
          /** 21-08-2018 **/
  $query1=$this->db->query("SELECT * FROM `user` where role=4 AND autosave_status!='1' and crm_name!='' and firm_admin_id=".$_SESSION['id']." and status=1 order by id DESC");
            $results1 = $query1->result_array();
            $res=array();
            if(count($results1)>0){
              foreach ($results1 as $key => $value) {
                   array_push($res, $value['id']);
                }  
            }
            if(!empty($res)){
              $im_val=implode(',',$res);
              //echo $im_val."abc abc";
          $data['referby']=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id in (".$im_val.") order by id desc ")->result_array();    
           //$results = $query->result_array(); 
           }
           else
           {
             $data['referby']=array();
           }
          /** end of 21-08-2018 **/
          $data['teamlist']=$this->Common_mdl->getallrecords('team');
          $data['deptlist']=$this->Common_mdl->getallrecords('department');
          // $data['staff_form'] = $this->Common_mdl->GetAllWithWhere('user','role','6');
          // $data['managed_by'] = $this->Common_mdl->GetAllWithWhere('user','role','5');
           $data['staff_form'] =$this->db->query("select * from user where role in(5,6,3) and firm_admin_id=".$_SESSION['id']." ")->result_array();
        $data['managed_by'] = $this->db->query("select * from user where role in(5,3) and firm_admin_id=".$_SESSION['id']." ")->result_array();
          if($user_id!=''){
             $query=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id='".$user_id."' order by id desc ")->result_array();
             $res=array();
                foreach ($query as $key => $value) {
                     array_push($res, $value['id']);
                  }  
                  $im_val=implode(',',$res); 
                  $query_res=$this->db->query("SELECT * FROM add_new_task WHERE company_name in ('".$im_val."') order by id desc "); 
          // $data['task_list']=$this->Common_mdl->GetAllWithWhere('add_new_task','user_id',$user_id);
          $data['task_list']=$query_res->result_array();
          }else{
          $data['task_list']=$this->Common_mdl->getallrecords('add_new_task');
          }
          
          if($user_id!=''){

            // $query=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id='".$user_id."' order by id desc ")->result_array();
            //  $res=array();
            //     foreach ($query as $key => $value) {
            //          array_push($res, $value['id']);
            //       }  
            //       $im_val=implode(',',$res); 
            //       $query=$this->db->query("SELECT * FROM proposals WHERE company_id in ('".$im_val."') order by id desc "); 
             $query=$this->db->query("SELECT * FROM proposals WHERE company_id=".$user_id."  order by id desc ");      
           $data['proposals']=$query->result_array();
          }else{
          $data['proposals']=$this->Common_mdl->getallrecords('proposals');
          }

          /** rspt for leads **/
          if($user_id!=''){
             $query=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id='".$user_id."' order by id desc ")->result_array();
             $res=array();
                                foreach ($query as $key => $value) {
                                     array_push($res, $value['id']);
                                  }  
                                  $im_val=implode(',',$res); 
                                   $query=$this->db->query("SELECT * FROM leads WHERE company in ('".$im_val."') order by id desc ");  
                                  // echo "SELECT * FROM leads WHERE user_id in ('".$im_val."') order by id desc";
          $data['leads']=$query->result_array();
          }else{
          $data['leads']=$this->Common_mdl->getallrecords('leads');
          }

          /** rspt 20-08-2018 for invoice view **/
           if($user_id!=''){
          $data['invoice_details'] = $this->Invoice_model->selectAllRecord('Invoice_details', 'client_email', $user_id);
           }

           if($user_id!=''){
          $data['reminder_details'] = $this->Invoice_model->selectAllRecord('client_reminder', 'user_id', $user_id);
           }


          /** end of 20-08-2018 **/

          /** end opf leads **/
  
  //print_r($data['staff_form']);die;
          //first api
          $curl = curl_init();
          $items_per_page = '10';
       
  curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk/company/'.$data['client'][0]['crm_company_number'].'/'); 
  
          curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET'); 
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($curl, CURLOPT_HEADER, false);
          curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($curl, CURLOPT_USERPWD,$this->companieshouseAPIkey);
          $response = curl_exec($curl);
         /* echo '<pre>';
          print_r($response);
          die;*/
          if($errno = curl_errno($curl)) {
          $error_message = curl_strerror($errno);
          echo "cURL error ({$errno}):\n {$error_message}";
          } else {
          curl_close($curl);
          $data_rec = json_decode($response);
           $array_rec = $this->object_2_array($data_rec);
           $data['company']=$array_rec;
  
  // second api
          $curl = curl_init();
       
  curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk/company/'.$data['client'][0]['crm_company_number'].'/officers?items_per_page=10&register_type=directors&register_view=false'); 
  
          curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET'); 
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($curl, CURLOPT_HEADER, false);
          curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($curl, CURLOPT_USERPWD,$this->companieshouseAPIkey);
          $response1 = curl_exec($curl);
  
          
          if($errno = curl_errno($curl)) {
          $error_message = curl_strerror($errno);
          echo "cURL error ({$errno}):\n {$error_message}";
          }
          curl_close($curl);
          $data1 = json_decode($response1);
          $array_rec1 = $this->object_2_array($data1);   
    /*echo '<pre>';
          print_r($array_rec1);
         // die; */ 
  
         
  $data['officers']=$array_rec1['items'];
    
  foreach($array_rec1['items'] as $key => $value){ 
  if($value['officer_role'] == 'director') {
  
  // third api
          $curl = curl_init();
          $items_per_page = '10';
    
  curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk'.$value['links']['officer']['appointments']);
   
          curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET'); 
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($curl, CURLOPT_HEADER, false);
          curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($curl, CURLOPT_USERPWD,$this->companieshouseAPIkey);
          $response2 = curl_exec($curl);
  
          
          if($errno = curl_errno($curl)) {
          $error_message = curl_strerror($errno);
          echo "cURL error ({$errno}):\n {$error_message}";
          } 
          curl_close($curl);
          $data2 = json_decode($response2);
          //$data['array_rec2'][]= $this->object_2_array($data2);
          $array_rec2= $this->object_2_array($data2);
          /*echo "<pre>";
          print_r($array_rec2['items']);die; */
  
            foreach ($array_rec2['items'] as $key => $val) {
          
  if((isset($val['appointed_to']['company_number']) == isset($data['client'][0]['crm_company_number'])) &&  (isset($val['appointed_on']) == isset($value['appointed_on']))  ) {
            
  $data['content'][]=$val;
  } /*else{
     $data['content'][]=$value; 
  }*/
  }
          //echo json_encode($datas);
          //echo $content;
  }
  } 
  
  // fourth filling history api
          $curl = curl_init();
       
  curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk/company/'.$data['client'][0]['crm_company_number'].'/filing-history'); 
  
          curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET'); 
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($curl, CURLOPT_HEADER, false);
          curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($curl, CURLOPT_USERPWD,$this->companieshouseAPIkey);
          $response3 = curl_exec($curl);
  
          
          if($errno = curl_errno($curl)) {
          $error_message = curl_strerror($errno);
          echo "cURL error ({$errno}):\n {$error_message}";
          }
          curl_close($curl);
          $data3 = json_decode($response3);
          $data['array_rec3'] = $this->object_2_array($data3);  
    }  //else close  
        $data['feedback'] =$this->db->query("select id,username,crm_email_id from user where username!='' and username!='0' and  crm_email_id!='' and crm_email_id!='0' "  )->result();  
          $this->load->view('Firm/firm_info',$data);
      }

// //
}
?>