<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Google_share extends CI_Controller {
public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
    public function __construct()
    {
        parent::__construct();
        //session_start();
    }

    public function index(){

		$url_array = explode('?', 'http://'.$_SERVER ['HTTP_HOST'].$_SERVER['REQUEST_URI']);
		$data['url'] = $url_array[0];
		//$url = $url_array[0];

    	require_once(APPPATH.'libraries/google-api-php-client/src/Google_Client.php');
    	require_once(APPPATH.'libraries/google-api-php-client/src/contrib/Google_DriveService.php');
    	$client = new Google_Client();
		$client->setClientId('4558830130-n3ief9qkufjv2qq0hfkbkai31a96e1g4.apps.googleusercontent.com');
		$client->setClientSecret('Pz8TIxlZ5sGQqt7qF3vAW45S');
		$client->setRedirectUri($data['url']);
		$client->setScopes(array('https://www.googleapis.com/auth/drive'));
		if (isset($_GET['code'])) {
		    $_SESSION['accessToken'] = $client->authenticate($_GET['code']);
		    header('location:'.$data['url']);exit;
		} elseif (!isset($_SESSION['accessToken'])) {
		    $client->authenticate();
		}

		$record=$this->db->query('select pdf_file from proposals where id="43"')->row_array();
		$files_name=$record['pdf_file'];

		$files= array();
		$dir = dir('uploads/proposal_pdf');
		while ($file = $dir->read()) {
		    if ($file != '.' && $file != '..' && $file == $files_name) {
		        $files[] = $file;		        
		    }
		    $data['files']=$files;
		}
		$dir->close();
		if (!empty($_POST)) {
		    $client->setAccessToken($_SESSION['accessToken']);
		    $service = new Google_DriveService($client);
		    $finfo = finfo_open(FILEINFO_MIME_TYPE);
		    $file = new Google_DriveFile();
		    foreach ($files as $file_name) {
		        $file_path = 'uploads/proposal_pdf/'.$file_name;
		        $mime_type = finfo_file($finfo, $file_path);
		        $file->setTitle($file_name);
		        $file->setDescription('This is a '.$mime_type.' document');
		        $file->setMimeType($mime_type);
		        $service->files->insert(
		            $file,
		            array(
		                'data' => file_get_contents($file_path),
		                'mimeType' => $mime_type
		            )
		        );
		    }
		    finfo_close($finfo);
		    header('location:'.$data['url']);exit;
		}
		$this->load->view('proposal/google_share',$data);

    }

} ?>
