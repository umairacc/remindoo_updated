<?php
class Report extends CI_Controller 
{
    public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
		public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Common_mdl','Report_model','Security_model','Proposal_model','Invoice_model'));
        $this->load->library('Excel');
        $this->load->helper('comman');
		}
        public function index()
        {
	        redirect('reports');
        }

        public function proposal_chart(){
        	if(isset($_POST['proposal_image']))
            {            	
              $this->session->set_userdata('proposal_image',$_POST['proposal_image']);                   
            }  
        }
         public function proposal_chart_sess(){
          if(isset($_POST['proposal_image_sess']))
            {             
              $this->session->set_userdata('proposal_image_sess',$_POST['proposal_image_sess']);                   
            }  
        }

        public function proposalpdf_download(){        	                
            $data['proposal_list']=$this->Report_model->proposal_list();
            $this->load->library('Tc');
            $pdf = new Tc('P', 'mm', 'A4', true, 'UTF-8', false); 
            $pdf->AddPage('P','A4');
            $html = $this->load->view('reports/proposal_pdf',$data,true);      
            $pdf->SetTitle('Proposal list');
            $pdf->SetHeaderMargin(30);
            $pdf->SetTopMargin(20);
            $pdf->setFooterMargin(20);
            $pdf->SetAutoPageBreak(true, 20);
            $pdf->SetAuthor('Author');
            $pdf->SetDisplayMode('real', 'default');
            $pdf->WriteHTML($html);          
            $pdf->Output('Proposal_list.pdf', 'D'); 
        }

        public function report_dashboard(){ 

          $this->load->view('reports/report_dashboard');
        }

        public function proposalpdf_download_sess(){          
          $status=$_SESSION['proposal_status'];
          $last_Year_start=$_SESSION['proposal_last_Year_start'];
            $last_month_end=$_SESSION['proposal_last_month_end'];
              if($status=='all'){                 
                  $data['proposal_list']=$this->Report_model->proposal_searching1($status,'or',$last_Year_start,$last_month_end);
              }else{                  
                    $data['proposal_list']=$this->Report_model->proposal_searching1($status,'and',$last_Year_start,$last_month_end);                    
              }
          $this->load->library('Tc');
          $pdf = new Tc('P', 'mm', 'A4', true, 'UTF-8', false); 
          $pdf->AddPage('P','A4');
          if($_SESSION['status']!='all'){
          $html = $this->load->view('reports/proposal_report',$data,true);   
          }else{
             $html = $this->load->view('reports/proposal_pdf',$data,true); 
          }   
          $pdf->SetTitle('Proposal list');
          $pdf->SetHeaderMargin(30);
          $pdf->SetTopMargin(20);
          $pdf->setFooterMargin(20);
          $pdf->SetAutoPageBreak(true,20);
          $pdf->SetAuthor('Author');
          $pdf->SetDisplayMode('real', 'default');
          $pdf->WriteHTML($html);          
          $pdf->Output('Proposal_list.pdf', 'D'); 
      }

        public function excelproposal_download(){   
            $filename = 'proposal_'.date('Ymd').'.xls';          	                 
            $data['proposal_list']=$this->Report_model->proposal_list();    
             header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/csv; "); 
            $file = fopen('php://output', 'w'); 
            $header = array("S.NO","Proposal No","Proposal Name","Proposal Amount","Status","Sent on");
            fputcsv($file, $header);
            $i=0;  
             foreach ($data['proposal_list'] as $key=>$line){
               if($line['id']){ $i++; $line['id']=$i; }  
                 fputcsv($file,$line);  
             }
              fclose($file);
              exit; 
           // $this->load->view('reports/proposal_excel',$data);     
        }

         public function excelproposal_download_sess(){
          $filename = 'proposal_'.date('Ymd').'.xls';                      
          $status=$_SESSION['proposal_status'];
          $last_Year_start=$_SESSION['proposal_last_Year_start'];
          $last_month_end=$_SESSION['proposal_last_month_end'];
          if($status=='all'){
              $data['proposal_list']=$this->Report_model->proposal_searching1($status,'or',$last_Year_start,$last_month_end);
          }else{
                $data['proposal_list']=$this->Report_model->proposal_searching1($status,'and',$last_Year_start,$last_month_end);
          } 
           header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/csv; "); 
            $file = fopen('php://output', 'w'); 
            $header = array("S.NO","Proposal No","Proposal Name","Proposal Amount","Status","Sent on");
            fputcsv($file, $header);
            $i=0;  
             foreach ($data['proposal_list'] as $key=>$line){
               if($line['id']){ $i++; $line['id']=$i; }  
                 fputcsv($file,$line);  
             }
              fclose($file);
              exit; 
          //$this->load->view('reports/proposal_excel',$data);     
        }
        public function Proposal_customization(){
        	$last_Year_start=$_POST['fromdate'];
            $last_month_end=$_POST['todate'];          
            $status=$_POST['status'];
            if($_POST['fromdate']==''){
                $last_Year_start = date('Y-01-01');                         
            }
            if($_POST['todate']==''){
                $last_month_end  = date('Y-12-31');
            }
            $this->session->set_userdata('proposal_last_Year_start',$last_Year_start);
            $this->session->set_userdata('proposal_last_month_end',$last_month_end);        
            $this->session->set_userdata('proposal_status',$_POST['status']);
            if($status!='all'){
            	if($status=='accepted'){
					$data['accept_proposal']=$this->Report_model->proposal_searching('accepted','and',$last_Year_start,$last_month_end);
          $data['others']=$this->Report_model->proposal_searching_new('accepted','and',$last_Year_start,$last_month_end);					
			    }
			    if($status=='sent'){					
					$data['sent_proposal']=$this->Report_model->proposal_searching('sent','and',$last_Year_start,$last_month_end);
           $data['others']=$this->Report_model->proposal_searching_new('sent','and',$last_Year_start,$last_month_end);					
			    }			    
			    if($status=='opened'){					
					$data['viewed_proposal']=$this->Report_model->proposal_searching('opened','and',$last_Year_start,$last_month_end);
          $data['others']=$this->Report_model->proposal_searching_new('opened','and',$last_Year_start,$last_month_end);	
			    }
			    if($status=='declined'){					
					$data['declined_proposal']=$this->Report_model->proposal_searching('declined','and',$last_Year_start,$last_month_end);
          $data['others']=$this->Report_model->proposal_searching_new('declined','and',$last_Year_start,$last_month_end);					
			    }
			    if($status=='in discussion'){					
					$data['indiscussion_proposal']=$this->Report_model->proposal_searching('in discussion','and',$last_Year_start,$last_month_end);
          $data['others']=$this->Report_model->proposal_searching_new('in discussion','and',$last_Year_start,$last_month_end);					
			    }
			    if($status=='archive'){					
					$data['archive_proposal']=$this->Report_model->proposal_searching('archive','and',$last_Year_start,$last_month_end); 
          $data['others']=$this->Report_model->proposal_searching_new('archive','and',$last_Year_start,$last_month_end); 				
			    }
			     if($status=='draft'){					
					$data['draft_proposal']=$this->Report_model->proposal_searching('draft','and',$last_Year_start,$last_month_end);
          $data['others']=$this->Report_model->proposal_searching_new('draft','and',$last_Year_start,$last_month_end);
			    }			   
                $this->load->view('reports/proposal_success',$data);   
            }else{ 
                $status='1';
                echo $status;
            }
        }

        public function lead_chart(){
        	if(isset($_POST['lead_image']))
            {            	
              $this->session->set_userdata('lead_image',$_POST['lead_image']);                   
            }  
        }

        public function lead_chart_sess(){
          if(isset($_POST['lead_image_sess']))
            {             
              $this->session->set_userdata('lead_image_sess',$_POST['lead_image_sess']);                   
            }  
        }

        public function leadspdf_download(){        	                
            $data['lead_list']=$this->Report_model->lead_list();             
            $this->load->library('Tc');
            $pdf = new Tc('P', 'mm', 'A4', true, 'UTF-8', false); 
            $pdf->AddPage('L','A4');
            $html = $this->load->view('reports/lead_pdf',$data,true);      
            $pdf->SetTitle('Lead list');
            $pdf->SetHeaderMargin(30);
            $pdf->SetTopMargin(20);
            $pdf->setFooterMargin(20);
            $pdf->SetAutoPageBreak(true,20);
            $pdf->SetAuthor('Author');
            $pdf->SetDisplayMode('real', 'default');
            $pdf->WriteHTML($html);          
            $pdf->Output('lead_list.pdf', 'D'); 
        }
         public function leadspdf_download_sess(){
           
            $status=$_SESSION['leads_status'];
            $last_Year_start=$_SESSION['leads_last_Year_start'];
            $last_month_end=$_SESSION['leads_last_month_end'];
            if($status=='all'){                 
              $data['lead_list']=$this->Report_model->leads_details1($status,'or',$last_Year_start,$last_month_end);
            }else{                  
              $data['lead_list']=$this->Report_model->leads_details1($status,'and',$last_Year_start,$last_month_end);                    
            }
            $this->load->library('Tc');
            $pdf = new Tc('P', 'mm', 'A4', true, 'UTF-8', false); 
            $pdf->AddPage('L','A4');

           if($_SESSION['leads_status']!='all'){
              $html = $this->load->view('reports/lead_report',$data,true);      
           }else{
             $html = $this->load->view('reports/lead_pdf',$data,true);    
           }                  
            $pdf->SetTitle('Lead list');
            $pdf->SetHeaderMargin(30);
            $pdf->SetTopMargin(20);
            $pdf->setFooterMargin(20);
            $pdf->SetAutoPageBreak(true,20);
            $pdf->SetAuthor('Author');
            $pdf->SetDisplayMode('real', 'default');
            $pdf->WriteHTML($html);          
            $pdf->Output('lead_list.pdf', 'D'); 
        }

        public function excelleads_download(){        	                
            $data['lead_list']=$this->Report_model->lead_list();  
            $filename = 'leads_'.date('Ymd').'.xls';       
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/csv; "); 
            $file = fopen('php://output', 'w'); 
            $header = array("S.NO","Lead Name","Company Name","Email","Assigned","Status");
            fputcsv($file, $header);
            $i=0;  
            foreach ($data['lead_list'] as $key=>$line){                  
              if($line['id']){ $i++; $line['id']=$i; }
              $getUserProfilepic=array();
              $ex_assign=explode(',',$line['assigned']);
              foreach ($ex_assign as $ex_key => $ex_value) {
              $getUserProfilepic[] = $this->Common_mdl->getUserProfileName($ex_value);
              }
              if(is_numeric($line['company']))
              {
                $client_query=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and id='".$line['company']."' order by id desc ")->result_array();
              if(count($client_query)>0)
              {
                 $line['company']=$client_query[0]['crm_company_name'];
              }
              else
              {
                 $line['company']='-';
              }
              }
              else
              {
                 $line['company']=$line['company'];
              }              
              $line['assigned']=implode(',',$getUserProfilepic);
              $line['lead_status']=$this->Report_model->lead_status($line['lead_status']);
              fputcsv($file,$line);
              $getUserProfilepic=array();              
            }
            fclose($file);
            exit;
           //  $this->load->view('reports/lead_excel',$data);
        }

         public function excelleads_download_sess(){
          $status=$_SESSION['leads_status'];
          $last_Year_start=$_SESSION['leads_last_Year_start'];
          $last_month_end=$_SESSION['leads_last_month_end'];
          if($status=='all'){                 
              $data['lead_list']=$this->Report_model->leads_details1($status,'or',$last_Year_start,$last_month_end);
          }else{                  
              $data['lead_list']=$this->Report_model->leads_details1($status,'and',$last_Year_start,$last_month_end);                    
          }
           $filename = 'leads_'.date('Ymd').'.xls';       
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/csv; "); 
            $file = fopen('php://output', 'w'); 
            $header = array("S.NO","Lead Name","Company Name","Email","Assigned","Status");
            fputcsv($file, $header);
            $i=0;  
            foreach ($data['lead_list'] as $key=>$line){                  
              if($line['id']){ $i++; $line['id']=$i; }
              $getUserProfilepic=array();
              $ex_assign=explode(',',$line['assigned']);
              foreach ($ex_assign as $ex_key => $ex_value) {
              $getUserProfilepic[] = $this->Common_mdl->getUserProfileName($ex_value);
              }
              if(is_numeric($line['company']))
              {
                $client_query=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and id='".$line['company']."' order by id desc ")->result_array();
              if(count($client_query)>0)
              {
                 $line['company']=$client_query[0]['crm_company_name'];
              }
              else
              {
                 $line['company']='-';
              }
              }
              else
              {
                 $line['company']=$line['company'];
              }              
              $line['assigned']=implode(',',$getUserProfilepic);
              $line['lead_status']=$this->Report_model->lead_status($line['lead_status']);
              fputcsv($file,$line);
              $getUserProfilepic=array();             
            }
            fclose($file);
            exit;          
          //$this->load->view('reports/lead_excel',$data);
        }

        public function Leads_customization(){
        	$last_Year_start=$_POST['fromdate'];
            $last_month_end=$_POST['todate'];          
            echo $lead_status=$_POST['status'];
            if($_POST['fromdate']==''){
                $last_Year_start = date('Y-01-01');                         
            }
            if($_POST['todate']==''){
                $last_month_end  = date('Y-12-31');
            }
            $this->session->set_userdata('leads_last_Year_start',$last_Year_start);
            $this->session->set_userdata('leads_last_month_end',$last_month_end);        
            $this->session->set_userdata('leads_status',$_POST['status']);
            if($lead_status=='all'){
            	$status='1';
                echo $status;
            }else{
            	 $data['leads_status']=$this->Common_mdl->getallrecords('leads_status');
					$leads_status=$this->Common_mdl->getallrecords('leads_status');
					foreach($leads_status as $status){
					$status_id=$status['id'];					
					$status_name=$status['status_name'];
					if($lead_status==$status_id){						
					$data[$status_name]=$this->Report_model->leads_details($status_id,$last_Year_start,$last_month_end);						
					}else{
						$data['others']=$this->Report_model->leads_details_new($status_id,$last_Year_start,$last_month_end);  
					}
			   }			  
			   $this->load->view('reports/leads_success',$data);
            }

        }

        public function services(){
         $data['service_details']=$this->Report_model->service_details();
          $this->load->library('Tc');
            $pdf = new Tc('P', 'mm', 'A4', true, 'UTF-8', false); 
            $pdf->AddPage('L','A4');
            $html = $this->load->view('reports/service_pdf',$data,true);      
            $pdf->SetTitle('Service list');
            $pdf->SetHeaderMargin(30);
            $pdf->SetTopMargin(20);
            $pdf->setFooterMargin(20);
            $pdf->SetAutoPageBreak(true,20);
            $pdf->SetAuthor('Author');
            $pdf->SetDisplayMode('real', 'default');
            $pdf->WriteHTML($html);          
            $pdf->Output('service_list.pdf', 'D');            
        }
        public function service_chart(){  
            if(isset($_POST['service_image']))
            {
              $this->session->set_userdata('service_image',$_POST['service_image']);          
            }                
        }

         public function service_chart_sess(){  
            if(isset($_POST['service_image_sess']))
            {
              $this->session->set_userdata('service_image_sess',$_POST['service_image_sess']);          
            }                
        }
        public function excelservice_download(){
         $data['service_details']=$this->Report_model->service_details();
          $filename = 'service_'.date('Ymd').'.xls';       
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/csv; "); 
            $file = fopen('php://output', 'w'); 
            $header = array("S.NO","Company Name","Services","Service Due Dates");
            fputcsv($file, $header);
            $i=0; 
            $s = 1;
            $service_array=array();
            $servicedue_date_array=array();
            foreach ($data['service_details'] as $service) {
              
              //conf_statement
              (isset(json_decode($service['conf_statement'])->tab) && $service['conf_statement'] != '') ? $jsnvat =  json_decode($service['conf_statement'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $conf_statement = "On" : $conf_statement = "Off";
              //accounts
              (isset(json_decode($service['accounts'])->tab) && $service['accounts'] != '') ? $jsnvat =  json_decode($service['accounts'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $accounts = "On" : $accounts = "Off";
              //company_tax_return
              (isset(json_decode($service['company_tax_return'])->tab) && $service['company_tax_return'] != '') ? $jsnvat =  json_decode($service['company_tax_return'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $company_tax_return = "On" : $company_tax_return = "Off";
              //personal_tax_return
              (isset(json_decode($service['personal_tax_return'])->tab) && $service['personal_tax_return'] != '') ? $jsnvat =  json_decode($service['personal_tax_return'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $personal_tax_return = "On" : $personal_tax_return = "Off";
              //payroll
              (isset(json_decode($service['payroll'])->tab) && $service['payroll'] != '') ? $jsnvat =  json_decode($service['payroll'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $payroll = "On" : $payroll = "Off";
              //workplace
              (isset(json_decode($service['workplace'])->tab) && $service['workplace'] != '') ? $jsnvat =  json_decode($service['workplace'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $workplace = "On" : $workplace = "Off";
              //vat
              (isset(json_decode($service['vat'])->tab) && $service['vat'] != '') ? $jsnvat =  json_decode($service['vat'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $vat = "On" : $vat = "Off";
              //cis
              (isset(json_decode($service['cis'])->tab) && $service['cis'] != '') ? $jsnvat =  json_decode($service['cis'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $cis = "On" : $cis = "Off";
              //cissub
              (isset(json_decode($service['cissub'])->tab) && $service['cissub'] != '') ? $jsnvat =  json_decode($service['cissub'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $cissub = "On" : $cissub = "Off";
              //p11d
              (isset(json_decode($service['p11d'])->tab) && $service['p11d'] != '') ? $jsnvat =  json_decode($service['p11d'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $p11d = "On" : $p11d = "Off";
              //bookkeep
              (isset(json_decode($service['bookkeep'])->tab) && $service['bookkeep'] != '') ? $jsnvat =  json_decode($service['bookkeep'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $bookkeep = "On" : $bookkeep = "Off";
              //management
              (isset(json_decode($service['management'])->tab) && $service['management'] != '') ? $jsnvat =  json_decode($service['management'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $management = "On" : $management = "Off";
              //investgate
              (isset(json_decode($service['investgate'])->tab) && $service['investgate'] != '') ? $jsnvat =  json_decode($service['investgate'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $investgate = "On" : $investgate = "Off";
              //registered
              (isset(json_decode($service['registered'])->tab) && $service['registered'] != '') ? $jsnvat =  json_decode($service['registered'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $registered = "On" : $registered = "Off";
              //taxadvice
              (isset(json_decode($service['taxadvice'])->tab) && $service['taxadvice'] != '') ? $jsnvat =  json_decode($service['taxadvice'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $taxadvice = "On" : $taxadvice = "Off";
              //taxinvest
              (isset(json_decode($service['taxinvest'])->tab) && $service['taxinvest'] != '') ? $jsnvat =  json_decode($service['taxinvest'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $taxinvest = "On" : $taxinvest = "Off"; 
                if($conf_statement=='On'){ 
                array_push($service_array,'Confirmation Statements');               
                $confirmation_due_date=$service['crm_confirmation_statement_due_date'];
                if($confirmation_due_date!=''){
                $confirmation_due_date=$service['crm_confirmation_statement_due_date'];
                }else{
                $confirmation_due_date=$service['crm_confirmation_statement_due_date'];
                }
                array_push($servicedue_date_array,$confirmation_due_date);
                } 
                if($accounts=='On'){ 
                array_push($service_array,'Accounts');
                $accounts_due_date=$service['crm_ch_accounts_next_due'];
                if($accounts_due_date!=''){
                $accounts_due_date=$service['crm_ch_accounts_next_due'];
                }else{
                $accounts_due_date='-';
                }
                array_push($servicedue_date_array,$accounts_due_date);
                } 
                if($company_tax_return=='On'){ 
                array_push($service_array,'Company Tax Return');
                $company_tax_due_date=$service['crm_accounts_tax_date_hmrc']; 
                if($company_tax_due_date!=''){
                $company_tax_due_date=$service['crm_accounts_tax_date_hmrc']; 
                }else{
                $company_tax_due_date='-'; 
                }
                array_push($servicedue_date_array,$company_tax_due_date);
                } 
                if($personal_tax_return=='On'){ 
                array_push($service_array,'Personal Tax Return');
                $personal_tax_due_date=$service['crm_personal_due_date_return']; 
                if($personal_tax_due_date!=''){
                $personal_tax_due_date=$service['crm_personal_due_date_return']; 
                }else{
                $personal_tax_due_date='-'; 
                }
                array_push($servicedue_date_array,$personal_tax_due_date);
                } 
                if($vat=='On'){ 
                array_push($service_array,'VAT Returns');
                $vat_due_date=$service['crm_vat_due_date']; 
                if($vat_due_date!=''){
                $vat_due_date=$service['crm_vat_due_date']; 
                }else{
                $vat_due_date='-';  
                }
                array_push($servicedue_date_array,$vat_due_date);
                } 
                if($payroll=='On'){ 
                array_push($service_array,'Payroll');
                $payroll_due_date=$service['crm_rti_deadline']; 
                if($payroll_due_date!=''){
                $payroll_due_date=$service['crm_rti_deadline']; 
                }else{
                $payroll_due_date='-';  
                }
                array_push($servicedue_date_array,$payroll_due_date);
                } 
                if($workplace=='On'){ 
                array_push($service_array,'WorkPlace Pension - AE');
                $work_place_due_date=$service['crm_pension_subm_due_date']; 
                if($work_place_due_date!=''){
                $work_place_due_date=$service['crm_pension_subm_due_date']; 
                }else{
                $work_place_due_date='-';   
                }
                array_push($servicedue_date_array,$work_place_due_date);
                } 
                if($cis=='On'){ 
                array_push($service_array,'CIS - Contractor');
                $cis_due_date='-'; 
                array_push($servicedue_date_array,$cis_due_date);
                } 
                if($cissub=='On'){ 
                array_push($service_array,'CIS - Sub Contractor');
                $cis_sub_due_date='-'; 
                array_push($servicedue_date_array,$cis_sub_due_date);
                } 
                if($p11d=='On'){ 
                array_push($service_array,'P11D');
                $p11d_due_date=$service['crm_next_p11d_return_due']; 
                if($p11d_due_date!=''){
                $p11d_due_date=$service['crm_next_p11d_return_due']; 
                }else{
                $p11d_due_date='-';   
                }
                array_push($servicedue_date_array,$p11d_due_date);
                } 
                if($management=='On'){ 
                array_push($service_array,'Management Accounts');
                $management_due_date=$service['crm_next_manage_acc_date']; 
                if($management_due_date!=''){
                $management_due_date=$service['crm_next_manage_acc_date']; 
                }else{
                $management_due_date='-';   
                }
                array_push($servicedue_date_array,$management_due_date);
                } 
                if($bookkeep=='On'){ 
                array_push($service_array,'Bookkeeping');
                $bookkeep_due_date=$service['crm_next_booking_date']; 
                if($bookkeep_due_date!=''){
                $bookkeep_due_date=$service['crm_next_booking_date']; 
                }else{
                $bookkeep_due_date='-';   
                }
                array_push($servicedue_date_array,$bookkeep_due_date);
                } 
                if($investgate=='On'){ 
                array_push($service_array,'Investigation Insurance');
                $investigate_due_date=$service['crm_insurance_renew_date']; 
                if($investigate_due_date!=''){
                $investigate_due_date=$service['crm_insurance_renew_date']; 
                }else{
                $investigate_due_date='-'; 
                }
                array_push($servicedue_date_array,$investigate_due_date);
                } 
                if($registered=='On'){ 
                array_push($service_array,'Registered Address');
                $registered_due_date=$service['crm_registered_renew_date']; 
                if($registered_due_date!=''){
                $registered_due_date=$service['crm_registered_renew_date'];
                }else{
                $registered_due_date='-';
                }
                array_push($servicedue_date_array,$registered_due_date);
                } 
                if($taxadvice=='On'){ 
                array_push($service_array,'Tax Advice');
                $taxadvice_due_date=$service['crm_investigation_end_date']; 
                if($taxadvice_due_date!=''){
                $taxadvice_due_date=$service['crm_investigation_end_date']; 
                }else{
                $taxadvice_due_date='-';  
                }
                array_push($servicedue_date_array,$taxadvice_due_date);
                } 
                if($taxinvest=='On'){ 
                array_push($service_array,'Tax Investigation');
                $taxinvest_due_date=$service['crm_investigation_end_date']; 
                if($taxinvest_due_date!=''){
                $taxinvest_due_date=$service['crm_investigation_end_date']; 
                }else{
                $taxinvest_due_date='-'; 
                }
                array_push($servicedue_date_array,$taxinvest_due_date);
                }
                if($service['id']){ $i++; $value['id']=$i; }
                $value['crm_company_name']=$service['crm_company_name'];
                 for($i=0;$i<count($service_array);$i++){ 
                $value['services']=$service_array[$i];
                $value['serviceduedate']=$servicedue_date_array[$i];
                 fputcsv($file,$value);
                } 
               $service_array=array();
               $servicedue_date_array=array();  
            }            
             fclose($file);
             exit;  

       //  $this->load->view('reports/service_excel',$data); 
        }

         public function services_sess(){
          if($_SESSION['service_last_Year_start']){
          $status=$_SESSION['service'];
          $last_Year_start=$_SESSION['service_last_Year_start'];
          $last_month_end=$_SESSION['service_last_month_end'];
          if($status=='all'){
           $data['service_details']=$this->Report_model->service_details_list($last_Year_start,$last_month_end);
          }else{
            $data['service_details']=$this->Report_model->service_details_list1($status,$last_Year_start,$last_month_end);
          }
        }
          $this->load->library('Tc');
            $pdf = new Tc('P', 'mm', 'A4', true, 'UTF-8', false); 
            $pdf->AddPage('L','A4');

           if($_SESSION['service']!='all'){
                  $html = $this->load->view('reports/service_report',$data,true);     
           }else{
             $html = $this->load->view('reports/service_pdf',$data,true);    
           }             
            $pdf->SetTitle('Service list');
            $pdf->SetHeaderMargin(30);
            $pdf->SetTopMargin(20);
            $pdf->setFooterMargin(20);
            $pdf->SetAutoPageBreak(true,20);
            $pdf->SetAuthor('Author');
            $pdf->SetDisplayMode('real', 'default');
            $pdf->WriteHTML($html);          
            $pdf->Output('service_list.pdf', 'D');            
        }

         public function excelservice_download_sess(){
          if($_SESSION['service_last_Year_start']){
            $status=$_SESSION['service'];
            $last_Year_start=$_SESSION['service_last_Year_start'];
            $last_month_end=$_SESSION['service_last_month_end'];
            if($status=='all'){
            $data['service_details']=$this->Report_model->service_details_list($last_Year_start,$last_month_end);
          }else{
            $data['service_details']=$this->Report_model->service_details_list1($status,$last_Year_start,$last_month_end);
          }
       }
       if($_SESSION['service']!=''){
            $filename = 'service_'.date('Ymd').'.xls';       
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/csv; "); 
            $file = fopen('php://output', 'w'); 
            $header = array("S.NO","Company Name","Services","Service Due Dates");
            fputcsv($file, $header);
            $i=0; 
            $s = 1;
            $service_array=array();
            $servicedue_date_array=array();
            foreach ($data['service_details'] as $service) {
              
              //conf_statement
              (isset(json_decode($service['conf_statement'])->tab) && $service['conf_statement'] != '') ? $jsnvat =  json_decode($service['conf_statement'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $conf_statement = "On" : $conf_statement = "Off";
              //accounts
              (isset(json_decode($service['accounts'])->tab) && $service['accounts'] != '') ? $jsnvat =  json_decode($service['accounts'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $accounts = "On" : $accounts = "Off";
              //company_tax_return
              (isset(json_decode($service['company_tax_return'])->tab) && $service['company_tax_return'] != '') ? $jsnvat =  json_decode($service['company_tax_return'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $company_tax_return = "On" : $company_tax_return = "Off";
              //personal_tax_return
              (isset(json_decode($service['personal_tax_return'])->tab) && $service['personal_tax_return'] != '') ? $jsnvat =  json_decode($service['personal_tax_return'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $personal_tax_return = "On" : $personal_tax_return = "Off";
              //payroll
              (isset(json_decode($service['payroll'])->tab) && $service['payroll'] != '') ? $jsnvat =  json_decode($service['payroll'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $payroll = "On" : $payroll = "Off";
              //workplace
              (isset(json_decode($service['workplace'])->tab) && $service['workplace'] != '') ? $jsnvat =  json_decode($service['workplace'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $workplace = "On" : $workplace = "Off";
              //vat
              (isset(json_decode($service['vat'])->tab) && $service['vat'] != '') ? $jsnvat =  json_decode($service['vat'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $vat = "On" : $vat = "Off";
              //cis
              (isset(json_decode($service['cis'])->tab) && $service['cis'] != '') ? $jsnvat =  json_decode($service['cis'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $cis = "On" : $cis = "Off";
              //cissub
              (isset(json_decode($service['cissub'])->tab) && $service['cissub'] != '') ? $jsnvat =  json_decode($service['cissub'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $cissub = "On" : $cissub = "Off";
              //p11d
              (isset(json_decode($service['p11d'])->tab) && $service['p11d'] != '') ? $jsnvat =  json_decode($service['p11d'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $p11d = "On" : $p11d = "Off";
              //bookkeep
              (isset(json_decode($service['bookkeep'])->tab) && $service['bookkeep'] != '') ? $jsnvat =  json_decode($service['bookkeep'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $bookkeep = "On" : $bookkeep = "Off";
              //management
              (isset(json_decode($service['management'])->tab) && $service['management'] != '') ? $jsnvat =  json_decode($service['management'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $management = "On" : $management = "Off";
              //investgate
              (isset(json_decode($service['investgate'])->tab) && $service['investgate'] != '') ? $jsnvat =  json_decode($service['investgate'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $investgate = "On" : $investgate = "Off";
              //registered
              (isset(json_decode($service['registered'])->tab) && $service['registered'] != '') ? $jsnvat =  json_decode($service['registered'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $registered = "On" : $registered = "Off";
              //taxadvice
              (isset(json_decode($service['taxadvice'])->tab) && $service['taxadvice'] != '') ? $jsnvat =  json_decode($service['taxadvice'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $taxadvice = "On" : $taxadvice = "Off";
              //taxinvest
              (isset(json_decode($service['taxinvest'])->tab) && $service['taxinvest'] != '') ? $jsnvat =  json_decode($service['taxinvest'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $taxinvest = "On" : $taxinvest = "Off";
              $status=$_SESSION['service'];
                if($status=='conf_statement'){ 
                if($conf_statement=='On'){ 
                array_push($service_array,'Confirmation Statements');               
                $confirmation_due_date=$service['crm_confirmation_statement_due_date'];
                if($confirmation_due_date!=''){
                $confirmation_due_date=$service['crm_confirmation_statement_due_date'];
                }else{
                $confirmation_due_date=$service['crm_confirmation_statement_due_date'];
                }
                array_push($servicedue_date_array,$confirmation_due_date);
                } 
                }
                if($status=='accounts'){
                if($accounts=='On'){ 
                array_push($service_array,'Accounts');
                $accounts_due_date=$service['crm_ch_accounts_next_due'];
                if($accounts_due_date!=''){
                $accounts_due_date=$service['crm_ch_accounts_next_due'];
                }else{
                $accounts_due_date='-';
                }
                array_push($servicedue_date_array,$accounts_due_date);
                } 
                }
                if($status=='company_tax_return'){
                if($company_tax_return=='On'){ 
                array_push($service_array,'Company Tax Return');
                $company_tax_due_date=$service['crm_accounts_tax_date_hmrc']; 
                if($company_tax_due_date!=''){
                $company_tax_due_date=$service['crm_accounts_tax_date_hmrc']; 
                }else{
                $company_tax_due_date='-'; 
                }
                array_push($servicedue_date_array,$company_tax_due_date);
                } 
                }
                if($status=='personal_tax_return'){
                if($personal_tax_return=='On'){ 
                array_push($service_array,'Personal Tax Return');
                $personal_tax_due_date=$service['crm_personal_due_date_return']; 
                if($personal_tax_due_date!=''){
                $personal_tax_due_date=$service['crm_personal_due_date_return']; 
                }else{
                $personal_tax_due_date='-'; 
                }
                array_push($servicedue_date_array,$personal_tax_due_date);
                }
                } 
                if($status=='vat'){
                if($vat=='On'){ 
                array_push($service_array,'VAT Returns');
                $vat_due_date=$service['crm_vat_due_date']; 
                if($vat_due_date!=''){
                $vat_due_date=$service['crm_vat_due_date']; 
                }else{
                $vat_due_date='-';  
                }
                array_push($servicedue_date_array,$vat_due_date);
                } 
                }
                if($status=='payroll'){
                if($payroll=='On'){ 
                array_push($service_array,'Payroll');
                $payroll_due_date=$service['crm_rti_deadline']; 
                if($payroll_due_date!=''){
                $payroll_due_date=$service['crm_rti_deadline']; 
                }else{
                $payroll_due_date='-';  
                }
                array_push($servicedue_date_array,$payroll_due_date);
                } 
                }
                if($status=='workplace'){
                if($workplace=='On'){ 
                array_push($service_array,'WorkPlace Pension - AE');
                $work_place_due_date=$service['crm_pension_subm_due_date']; 
                if($work_place_due_date!=''){
                $work_place_due_date=$service['crm_pension_subm_due_date']; 
                }else{
                $work_place_due_date='-';   
                }
                array_push($servicedue_date_array,$work_place_due_date);
                } 
                }
                if($status=='cis'){
                if($cis=='On'){ 
                array_push($service_array,'CIS - Contractor');
                $cis_due_date='-'; 
                array_push($servicedue_date_array,$cis_due_date);
                } 
                }
                if($status=='cissub'){
                if($cissub=='On'){ 
                array_push($service_array,'CIS - Sub Contractor');
                $cis_sub_due_date='-'; 
                array_push($servicedue_date_array,$cis_sub_due_date);
                } 
                }
                if($status=='p11d'){
                if($p11d=='On'){ 
                array_push($service_array,'P11D');
                $p11d_due_date=$service['crm_next_p11d_return_due']; 
                if($p11d_due_date!=''){
                $p11d_due_date=$service['crm_next_p11d_return_due']; 
                }else{
                $p11d_due_date='-';   
                }
                array_push($servicedue_date_array,$p11d_due_date);
                } 
                }
                if($status=='management'){
                if($management=='On'){ 
                array_push($service_array,'Management Accounts');
                $management_due_date=$service['crm_next_manage_acc_date']; 
                if($management_due_date!=''){
                $management_due_date=$service['crm_next_manage_acc_date']; 
                }else{
                $management_due_date='-';   
                }
                array_push($servicedue_date_array,$management_due_date);
                }
                }
                if($status=='bookkeep'){ 
                if($bookkeep=='On'){ 
                array_push($service_array,'Bookkeeping');
                $bookkeep_due_date=$service['crm_next_booking_date']; 
                if($bookkeep_due_date!=''){
                $bookkeep_due_date=$service['crm_next_booking_date']; 
                }else{
                $bookkeep_due_date='-';   
                }
                array_push($servicedue_date_array,$bookkeep_due_date);
                }
                }
                if($status=='investgate'){  
                if($investgate=='On'){ 
                array_push($service_array,'Investigation Insurance');
                $investigate_due_date=$service['crm_insurance_renew_date']; 
                if($investigate_due_date!=''){
                $investigate_due_date=$service['crm_insurance_renew_date']; 
                }else{
                $investigate_due_date='-'; 
                }
                array_push($servicedue_date_array,$investigate_due_date);
                } 
                }
                if($status=='registered'){
                if($registered=='On'){ 
                array_push($service_array,'Registered Address');
                $registered_due_date=$service['crm_registered_renew_date']; 
                if($registered_due_date!=''){
                $registered_due_date=$service['crm_registered_renew_date'];
                }else{
                $registered_due_date='-';
                }
                array_push($servicedue_date_array,$registered_due_date);
                } 
                }
                if($status=='taxadvice'){
                if($taxadvice=='On'){ 
                array_push($service_array,'Tax Advice');
                $taxadvice_due_date=$service['crm_investigation_end_date']; 
                if($taxadvice_due_date!=''){
                $taxadvice_due_date=$service['crm_investigation_end_date']; 
                }else{
                $taxadvice_due_date='-';  
                }
                array_push($servicedue_date_array,$taxadvice_due_date);
                } 
                }
                if($status=='taxinvest'){
                if($taxinvest=='On'){ 
                array_push($service_array,'Tax Investigation');
                $taxinvest_due_date=$service['crm_investigation_end_date']; 
                if($taxinvest_due_date!=''){
                $taxinvest_due_date=$service['crm_investigation_end_date']; 
                }else{
                $taxinvest_due_date='-'; 
                }
                array_push($servicedue_date_array,$taxinvest_due_date);
                }
                }
                if($service['id']){ $i++; $value['id']=$i; }
                $value['crm_company_name']=$service['crm_company_name'];
                 for($i=0;$i<count($service_array);$i++){ 
                $value['services']=$service_array[$i];
                $value['serviceduedate']=$servicedue_date_array[$i];
                 fputcsv($file,$value);
                } 
               $service_array=array();
               $servicedue_date_array=array();  
            }            
             fclose($file);
             exit;  
         //$this->load->view('reports/service_report_excel',$data); 
       }else{
         $filename = 'service_'.date('Ymd').'.xls';       
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/csv; "); 
            $file = fopen('php://output', 'w'); 
            $header = array("S.NO","Company Name","Services","Service Due Dates");
            fputcsv($file, $header);
            $i=0; 
            $s = 1;
            $service_array=array();
            $servicedue_date_array=array();
            foreach ($data['service_details'] as $service) {
              
              //conf_statement
              (isset(json_decode($service['conf_statement'])->tab) && $service['conf_statement'] != '') ? $jsnvat =  json_decode($service['conf_statement'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $conf_statement = "On" : $conf_statement = "Off";
              //accounts
              (isset(json_decode($service['accounts'])->tab) && $service['accounts'] != '') ? $jsnvat =  json_decode($service['accounts'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $accounts = "On" : $accounts = "Off";
              //company_tax_return
              (isset(json_decode($service['company_tax_return'])->tab) && $service['company_tax_return'] != '') ? $jsnvat =  json_decode($service['company_tax_return'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $company_tax_return = "On" : $company_tax_return = "Off";
              //personal_tax_return
              (isset(json_decode($service['personal_tax_return'])->tab) && $service['personal_tax_return'] != '') ? $jsnvat =  json_decode($service['personal_tax_return'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $personal_tax_return = "On" : $personal_tax_return = "Off";
              //payroll
              (isset(json_decode($service['payroll'])->tab) && $service['payroll'] != '') ? $jsnvat =  json_decode($service['payroll'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $payroll = "On" : $payroll = "Off";
              //workplace
              (isset(json_decode($service['workplace'])->tab) && $service['workplace'] != '') ? $jsnvat =  json_decode($service['workplace'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $workplace = "On" : $workplace = "Off";
              //vat
              (isset(json_decode($service['vat'])->tab) && $service['vat'] != '') ? $jsnvat =  json_decode($service['vat'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $vat = "On" : $vat = "Off";
              //cis
              (isset(json_decode($service['cis'])->tab) && $service['cis'] != '') ? $jsnvat =  json_decode($service['cis'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $cis = "On" : $cis = "Off";
              //cissub
              (isset(json_decode($service['cissub'])->tab) && $service['cissub'] != '') ? $jsnvat =  json_decode($service['cissub'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $cissub = "On" : $cissub = "Off";
              //p11d
              (isset(json_decode($service['p11d'])->tab) && $service['p11d'] != '') ? $jsnvat =  json_decode($service['p11d'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $p11d = "On" : $p11d = "Off";
              //bookkeep
              (isset(json_decode($service['bookkeep'])->tab) && $service['bookkeep'] != '') ? $jsnvat =  json_decode($service['bookkeep'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $bookkeep = "On" : $bookkeep = "Off";
              //management
              (isset(json_decode($service['management'])->tab) && $service['management'] != '') ? $jsnvat =  json_decode($service['management'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $management = "On" : $management = "Off";
              //investgate
              (isset(json_decode($service['investgate'])->tab) && $service['investgate'] != '') ? $jsnvat =  json_decode($service['investgate'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $investgate = "On" : $investgate = "Off";
              //registered
              (isset(json_decode($service['registered'])->tab) && $service['registered'] != '') ? $jsnvat =  json_decode($service['registered'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $registered = "On" : $registered = "Off";
              //taxadvice
              (isset(json_decode($service['taxadvice'])->tab) && $service['taxadvice'] != '') ? $jsnvat =  json_decode($service['taxadvice'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $taxadvice = "On" : $taxadvice = "Off";
              //taxinvest
              (isset(json_decode($service['taxinvest'])->tab) && $service['taxinvest'] != '') ? $jsnvat =  json_decode($service['taxinvest'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $taxinvest = "On" : $taxinvest = "Off"; 
                if($conf_statement=='On'){ 
                array_push($service_array,'Confirmation Statements');               
                $confirmation_due_date=$service['crm_confirmation_statement_due_date'];
                if($confirmation_due_date!=''){
                $confirmation_due_date=$service['crm_confirmation_statement_due_date'];
                }else{
                $confirmation_due_date=$service['crm_confirmation_statement_due_date'];
                }
                array_push($servicedue_date_array,$confirmation_due_date);
                } 
                if($accounts=='On'){ 
                array_push($service_array,'Accounts');
                $accounts_due_date=$service['crm_ch_accounts_next_due'];
                if($accounts_due_date!=''){
                $accounts_due_date=$service['crm_ch_accounts_next_due'];
                }else{
                $accounts_due_date='-';
                }
                array_push($servicedue_date_array,$accounts_due_date);
                } 
                if($company_tax_return=='On'){ 
                array_push($service_array,'Company Tax Return');
                $company_tax_due_date=$service['crm_accounts_tax_date_hmrc']; 
                if($company_tax_due_date!=''){
                $company_tax_due_date=$service['crm_accounts_tax_date_hmrc']; 
                }else{
                $company_tax_due_date='-'; 
                }
                array_push($servicedue_date_array,$company_tax_due_date);
                } 
                if($personal_tax_return=='On'){ 
                array_push($service_array,'Personal Tax Return');
                $personal_tax_due_date=$service['crm_personal_due_date_return']; 
                if($personal_tax_due_date!=''){
                $personal_tax_due_date=$service['crm_personal_due_date_return']; 
                }else{
                $personal_tax_due_date='-'; 
                }
                array_push($servicedue_date_array,$personal_tax_due_date);
                } 
                if($vat=='On'){ 
                array_push($service_array,'VAT Returns');
                $vat_due_date=$service['crm_vat_due_date']; 
                if($vat_due_date!=''){
                $vat_due_date=$service['crm_vat_due_date']; 
                }else{
                $vat_due_date='-';  
                }
                array_push($servicedue_date_array,$vat_due_date);
                } 
                if($payroll=='On'){ 
                array_push($service_array,'Payroll');
                $payroll_due_date=$service['crm_rti_deadline']; 
                if($payroll_due_date!=''){
                $payroll_due_date=$service['crm_rti_deadline']; 
                }else{
                $payroll_due_date='-';  
                }
                array_push($servicedue_date_array,$payroll_due_date);
                } 
                if($workplace=='On'){ 
                array_push($service_array,'WorkPlace Pension - AE');
                $work_place_due_date=$service['crm_pension_subm_due_date']; 
                if($work_place_due_date!=''){
                $work_place_due_date=$service['crm_pension_subm_due_date']; 
                }else{
                $work_place_due_date='-';   
                }
                array_push($servicedue_date_array,$work_place_due_date);
                } 
                if($cis=='On'){ 
                array_push($service_array,'CIS - Contractor');
                $cis_due_date='-'; 
                array_push($servicedue_date_array,$cis_due_date);
                } 
                if($cissub=='On'){ 
                array_push($service_array,'CIS - Sub Contractor');
                $cis_sub_due_date='-'; 
                array_push($servicedue_date_array,$cis_sub_due_date);
                } 
                if($p11d=='On'){ 
                array_push($service_array,'P11D');
                $p11d_due_date=$service['crm_next_p11d_return_due']; 
                if($p11d_due_date!=''){
                $p11d_due_date=$service['crm_next_p11d_return_due']; 
                }else{
                $p11d_due_date='-';   
                }
                array_push($servicedue_date_array,$p11d_due_date);
                } 
                if($management=='On'){ 
                array_push($service_array,'Management Accounts');
                $management_due_date=$service['crm_next_manage_acc_date']; 
                if($management_due_date!=''){
                $management_due_date=$service['crm_next_manage_acc_date']; 
                }else{
                $management_due_date='-';   
                }
                array_push($servicedue_date_array,$management_due_date);
                } 
                if($bookkeep=='On'){ 
                array_push($service_array,'Bookkeeping');
                $bookkeep_due_date=$service['crm_next_booking_date']; 
                if($bookkeep_due_date!=''){
                $bookkeep_due_date=$service['crm_next_booking_date']; 
                }else{
                $bookkeep_due_date='-';   
                }
                array_push($servicedue_date_array,$bookkeep_due_date);
                } 
                if($investgate=='On'){ 
                array_push($service_array,'Investigation Insurance');
                $investigate_due_date=$service['crm_insurance_renew_date']; 
                if($investigate_due_date!=''){
                $investigate_due_date=$service['crm_insurance_renew_date']; 
                }else{
                $investigate_due_date='-'; 
                }
                array_push($servicedue_date_array,$investigate_due_date);
                } 
                if($registered=='On'){ 
                array_push($service_array,'Registered Address');
                $registered_due_date=$service['crm_registered_renew_date']; 
                if($registered_due_date!=''){
                $registered_due_date=$service['crm_registered_renew_date'];
                }else{
                $registered_due_date='-';
                }
                array_push($servicedue_date_array,$registered_due_date);
                } 
                if($taxadvice=='On'){ 
                array_push($service_array,'Tax Advice');
                $taxadvice_due_date=$service['crm_investigation_end_date']; 
                if($taxadvice_due_date!=''){
                $taxadvice_due_date=$service['crm_investigation_end_date']; 
                }else{
                $taxadvice_due_date='-';  
                }
                array_push($servicedue_date_array,$taxadvice_due_date);
                } 
                if($taxinvest=='On'){ 
                array_push($service_array,'Tax Investigation');
                $taxinvest_due_date=$service['crm_investigation_end_date']; 
                if($taxinvest_due_date!=''){
                $taxinvest_due_date=$service['crm_investigation_end_date']; 
                }else{
                $taxinvest_due_date='-'; 
                }
                array_push($servicedue_date_array,$taxinvest_due_date);
                }
                if($service['id']){ $i++; $value['id']=$i; }
                $value['crm_company_name']=$service['crm_company_name'];
                 for($i=0;$i<count($service_array);$i++){ 
                $value['services']=$service_array[$i];
                $value['serviceduedate']=$servicedue_date_array[$i];
                 fputcsv($file,$value);
                } 
               $service_array=array();
               $servicedue_date_array=array();  
            }            
             fclose($file);
             exit;  
         //$this->load->view('reports/service_excel',$data); 
       }

        }

        public function service_customization(){
            $last_Year_start=$_POST['fromdate'];
            $last_month_end=$_POST['todate'];           
            $status=$_POST['status'];
            if($_POST['fromdate']==''){
                $last_Year_start = date('Y-01-01');                         
            }
            if($_POST['todate']==''){
                $last_month_end  = date('Y-12-31');
            }
            $this->session->set_userdata('service_last_Year_start',$last_Year_start);
            $this->session->set_userdata('service_last_month_end',$last_month_end);          
            $this->session->set_userdata('service',$status);
            if($status!='all'){
              if($status=='conf_statement'){
                $data['conf_statement']=$this->Report_model->service_count_1('conf_statement',$last_Year_start, $last_month_end);
                $data['others']=$this->Report_model->service_count_2('conf_statement',$last_Year_start, $last_month_end);
              }
              if($status=='accounts'){
                 $data['accounts']=$this->Report_model->service_count_1('accounts',$last_Year_start, $last_month_end);
                 $data['others']=$this->Report_model->service_count_2('accounts',$last_Year_start, $last_month_end);
              }
              if($status=='company_tax_return'){
                $data['company_tax_return']=$this->Report_model->service_count_1('company_tax_return',$last_Year_start, $last_month_end);
                 $data['others']=$this->Report_model->service_count_2('company_tax_return',$last_Year_start, $last_month_end);
              }
              if($status=='personal_tax_return'){
                $data['personal_tax_return']=$this->Report_model->service_count_1('personal_tax_return',$last_Year_start, $last_month_end);
                 $data['others']=$this->Report_model->service_count_2('personal_tax_return',$last_Year_start, $last_month_end);
              }
              if($status=='payroll'){
                 $data['payroll']=$this->Report_model->service_count_1('payroll',$last_Year_start, $last_month_end);
                 $data['others']=$this->Report_model->service_count_2('payroll',$last_Year_start, $last_month_end);
              }
              if($status=='workplace'){
                 $data['workplace']=$this->Report_model->service_count_1('workplace',$last_Year_start, $last_month_end);
                   $data['others']=$this->Report_model->service_count_2('workplace',$last_Year_start, $last_month_end);
              }
              if($status=='vat'){
                 $data['vat']=$this->Report_model->service_count_1('vat',$last_Year_start, $last_month_end);
                 $data['others']=$this->Report_model->service_count_2('vat',$last_Year_start, $last_month_end);
              }
              if($status=='cis'){
                $data['cis']=$this->Report_model->service_count_1('cis',$last_Year_start, $last_month_end);
                 $data['others']=$this->Report_model->service_count_2('cis',$last_Year_start, $last_month_end);
              }
              if($status=='cissub'){
                 $data['cissub']=$this->Report_model->service_count_1('cissub',$last_Year_start, $last_month_end);
                 $data['others']=$this->Report_model->service_count_2('cissub',$last_Year_start, $last_month_end);
              }
              if($status=='p11d'){
                 $data['p11d']=$this->Report_model->service_count_1('p11d',$last_Year_start, $last_month_end);
                 $data['others']=$this->Report_model->service_count_2('p11d',$last_Year_start, $last_month_end);
              }
              if($status=='bookkeep'){
                $data['bookkeep']=$this->Report_model->service_count_1('bookkeep',$last_Year_start, $last_month_end);
                 $data['others']=$this->Report_model->service_count_2('bookkeep',$last_Year_start, $last_month_end); 
              }
               if($status=='management'){
                 $data['management']=$this->Report_model->service_count_1('management',$last_Year_start, $last_month_end);
                   $data['others']=$this->Report_model->service_count_2('management',$last_Year_start, $last_month_end);
              }
               if($status=='investgate'){
                 $data['investgate']=$this->Report_model->service_count_1('investgate',$last_Year_start, $last_month_end);
                   $data['others']=$this->Report_model->service_count_2('investgate',$last_Year_start, $last_month_end);  
              }
               if($status=='registered'){
                 $data['registered']=$this->Report_model->service_count_1('registered',$last_Year_start, $last_month_end);
                 $data['others']=$this->Report_model->service_count_2('registered',$last_Year_start, $last_month_end); 
              }
               if($status=='taxadvice'){
                 $data['taxadvice']=$this->Report_model->service_count_2('taxadvice',$last_Year_start, $last_month_end);
                   $data['others']=$this->Report_model->service_count_2('taxadvice',$last_Year_start, $last_month_end); 
              }
               if($status=='taxinvest'){
                 $data['taxinvest']=$this->Report_model->service_count_1('taxinvest',$last_Year_start, $last_month_end);
                  $data['others']=$this->Report_model->service_count_2('taxinvest',$last_Year_start, $last_month_end);
              }
            $this->load->view('reports/service_success',$data);
            }else{
              $status='1';
              echo $status;
            }
        }

        public function deadline(){
          $data['getCompany_today']=$this->Report_model->deadline_records();
          $this->load->library('Tc');
            $pdf = new Tc('P', 'mm', 'A3', true, 'UTF-8', false); 
            $pdf->AddPage('P','A3');
            $html = $this->load->view('reports/deadline_pdf',$data,true);      
            $pdf->SetTitle('Deadline list');
            $pdf->SetHeaderMargin(30);
            $pdf->SetTopMargin(20);
            $pdf->setFooterMargin(20);
            $pdf->SetAutoPageBreak(true,30);
            $pdf->SetAuthor('Author');
            $pdf->SetDisplayMode('real', 'default');
            $pdf->WriteHTML($html);          
            $pdf->Output('Deadline_list.pdf', 'D'); 
        }

        public function deadline_chart(){
           if(isset($_POST['deadline_image']))
            {
              $this->session->set_userdata('deadline_image',$_POST['deadline_image']);            
            } 
        }

         public function deadline_chart_sess(){
           if(isset($_POST['deadline_image_sess']))
            {
              $this->session->set_userdata('deadline_image_sess',$_POST['deadline_image_sess']);            
            } 
        }

        public function exceldeadline_download(){
           $data['getCompany_today']=$this->Report_model->deadline_records();
            $filename = 'deadline_'.date('Ymd').'.xls';       
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/csv; "); 
            $file = fopen('php://output', 'w'); 
            $header = array("Client Name","Client Type","Company Name","Deadline Type","Date");
            fputcsv($file, $header);
            $i=0;  
            foreach ($data['getCompany_today'] as $getCompanykey => $getCompanyvalue) {  
              $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
               if($getCompanyvalue['crm_confirmation_statement_due_date']!=''){
              $value['client_name']=$getusername['crm_name'];
              $value['client_type']=$getCompanyvalue['crm_legal_form'];
              $value['company_name']=$getCompanyvalue['crm_company_name'];
              $value['deadline_type']='Confirmation statement Due Date';
              if(strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) !=''){
              $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])); 
              }else{
              $value['date']='';
              }
              fputcsv($file,$value);
              // confi
              } 

          if($getCompanyvalue['crm_ch_accounts_next_due']!=''){ 
            $value['client_name']=$getusername['crm_name'];
            $value['client_type']=$getCompanyvalue['crm_legal_form'];
            $value['company_name']=$getCompanyvalue['crm_company_name'];
            $value['deadline_type']='Accounts Due Date';
            if(strtotime($getCompanyvalue['crm_ch_accounts_next_due']) !=''){
             $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due']));
            }else{
             $value['date']='';
            } 
            fputcsv($file,$value);
          // account -->
          } 
          if($getCompanyvalue['crm_accounts_tax_date_hmrc']!=''){
            $value['client_name']=$value['client_name']=$getusername['crm_name'];
            $value['client_type']=$getCompanyvalue['crm_legal_form'];
            $value['company_name']=$getCompanyvalue['crm_company_name'];
            $value['deadline_type']='Company Tax Return Due Date';
            if(strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) !=''){
              $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])); 
            }else{
              $value['date']='';
            }
            fputcsv($file,$value);
          //company tax 
          } 
          if($getCompanyvalue['crm_personal_due_date_return']!=''){
            $value['client_name']=$getusername['crm_name'];
            $value['client_type']=$getCompanyvalue['crm_legal_form'];
            $value['company_name']=$getCompanyvalue['crm_company_name'];
            $value['deadline_type']='Personal Tax Return Due Date';
            if(strtotime($getCompanyvalue['crm_personal_due_date_return']) !=''){
               $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])); 
            }else{
               $value['date']='';
            }
            fputcsv($file,$value);
                      //crm personal tax 
          } 

          if($getCompanyvalue['crm_vat_due_date']!=''){ 
            $value['client_name']=$getusername['crm_name'];
            $value['client_type']=$getCompanyvalue['crm_legal_form'];
            $value['company_name']=$getCompanyvalue['crm_company_name'];
            $value['deadline_type']='VAT Due Date';
            if(strtotime($getCompanyvalue['crm_vat_due_date']) !=''){
             $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date']));
            }else{
             $value['date']=''; 
            }    
            fputcsv($file,$value);
            // vat 
          } 
          if($getCompanyvalue['crm_rti_deadline']!=''){ 
            $value['client_name']=$getusername['crm_name'];
            $value['client_type']=$getCompanyvalue['crm_legal_form'];
            $value['company_name']=$getCompanyvalue['crm_company_name'];
            $value['deadline_type']='Payroll Due Date';
            if(strtotime($getCompanyvalue['crm_rti_deadline']) !=''){
              $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline']));
            }else{
              $value['date']='';
            } 
            fputcsv($file,$value);
          // payrol
          } 

          if($getCompanyvalue['crm_pension_subm_due_date']!=''){
            $value['client_name']=$getusername['crm_name'];
            $value['client_type']=$getCompanyvalue['crm_legal_form'];
            $value['company_name']=$getCompanyvalue['crm_company_name'];
            $value['deadline_type']='WorkPlace Pension - AE Due Date';
            if(strtotime($getCompanyvalue['crm_pension_subm_due_date']) !=''){
              $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date']));
            } else{
              $value['date']='';
            } 
            fputcsv($file,$value);  
          //workplace 
          } 

          $value['client_name']=$getusername['crm_name'];
          $value['client_type']= $getCompanyvalue['crm_legal_form'];
          $value['company_name']=$getCompanyvalue['crm_company_name'];
          $value['deadline_type']='CIS - Contractor Due Date';
          $value['date']='';
          fputcsv($file,$value);

          $value['client_name']=$getusername['crm_name'];
          $value['client_type']= $getCompanyvalue['crm_legal_form'];
          $value['company_name']=$getCompanyvalue['crm_company_name'];
          $value['deadline_type']='CIS - Sub Contractor Due Date';
          $value['date']='';
          fputcsv($file,$value);
          // CIS - Sub Contractor

           if($getCompanyvalue['crm_next_p11d_return_due']!=''){ 
              $value['client_name']=$getusername['crm_name'];
              $value['client_type']=$getCompanyvalue['crm_legal_form'];
              $value['company_name']=$getCompanyvalue['crm_company_name'];
              $value['deadline_type']='P11D Due Date';
              if(strtotime($getCompanyvalue['crm_next_p11d_return_due']) !=''){ 
                 $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due']));
              }else{
                 $value['date']='';
              } 
              fputcsv($file,$value);
         // p11d 
           } 
          if($getCompanyvalue['crm_next_manage_acc_date']!=''){ 
          $value['client_name']= $getusername['crm_name'];
          $value['client_type']= $getCompanyvalue['crm_legal_form'];
          $value['company_name']= $getCompanyvalue['crm_company_name'];
          $value['deadline_type']='Management Accounts Due Date';
            if(strtotime($getCompanyvalue['crm_next_manage_acc_date']) !=''){
               $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date']));
            }else{
               $value['date']='';
            } 
            fputcsv($file,$value);
          // manage account 
          } 
          if($getCompanyvalue['crm_next_booking_date']!=''){ 
          $value['client_name']=$getusername['crm_name'];
          $value['client_type']=$getCompanyvalue['crm_legal_form'];
          $value['company_name']=$getCompanyvalue['crm_company_name'];
          $value['deadline_type']='Bookkeeping Due Date';
          if(strtotime($getCompanyvalue['crm_next_booking_date']) !=''){
          $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date']));
          }else{
          $value['date']='';
          } 
          fputcsv($file,$value);
          // booking
          } 
          if($getCompanyvalue['crm_insurance_renew_date']!=''){
          $value['client_name']=$getusername['crm_name'];
          $value['client_type']=$getCompanyvalue['crm_legal_form'];
          $value['company_name']= $getCompanyvalue['crm_company_name'];
          $value['deadline_type']='Investigation Insurance Due Date';
            if(strtotime($getCompanyvalue['crm_insurance_renew_date']) !=''){
             $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date']));
            } else{
              $value['date']=''; 
            }
            fputcsv($file,$value);
          }

          if($getCompanyvalue['crm_registered_renew_date']!=''){ 
            $value['client_name']=$getusername['crm_name'];
            $value['client_type']=$getCompanyvalue['crm_legal_form'];
            $value['company_name']=$getCompanyvalue['crm_company_name'];
            $value['deadline_type']='Registered Address Due Date';
            if(strtotime($getCompanyvalue['crm_registered_renew_date']) !='') {
            $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date']));
            } else{
            $value['date']='';
            } 
            fputcsv($file,$value);
            // registed 
          } 
          if($getCompanyvalue['crm_investigation_end_date']!=''){
             $value['client_name']=$getusername['crm_name'];
             $value['client_type']=$getCompanyvalue['crm_legal_form'];
             $value['company_name']=$getCompanyvalue['crm_company_name'];
             $value['deadline_type']='Tax Advice Due Date';
             if(strtotime($getCompanyvalue['crm_investigation_end_date'])!=''){
             $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date']));
             }else{
              $value['date']='';
             }  
            // tax advice 
             fputcsv($file,$value);
           } 
          if($getCompanyvalue['crm_investigation_end_date']!=''){
            $value['client_name']=$getusername['crm_name'];
            $value['client_type']=$getCompanyvalue['crm_legal_form'];
            $value['company_name']=$getCompanyvalue['crm_company_name'];
            $value['deadline_type']='Tax Investigation Due Date';
            if(strtotime($getCompanyvalue['crm_investigation_end_date'])!=''){
            $value['date']=date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date']));
            }else{
            $value['date']='';
            }
          // tax inve  
          fputcsv($file,$value);           
          } 
          
      }
      fclose($file);
      exit;   
           //$this->load->view('reports/deadline_excel',$data);  
        }

        public function deadline_customization(){
            $last_Year_start=$_POST['fromdate'];
            $last_month_end=$_POST['todate'];           
            $company_type=$_POST['company_type'];
            $deadline_type=$_POST['deadline_type'];
            if($_POST['fromdate']==''){
                $last_Year_start = date('Y-01-01');                         
            }
            if($_POST['todate']==''){
                $last_month_end  = date('Y-12-31');
            }
            $this->session->set_userdata('deadline_last_Year_start',$last_Year_start);
            $this->session->set_userdata('deadline_last_month_end',$last_month_end);         
            $this->session->set_userdata('company_type',$company_type);
            $this->session->set_userdata('deadline_type',$deadline_type);
            if($deadline_type!=''){
              if($company_type!=''){
                  if($deadline_type=='conf_statement'){
                      $data['de_conf_statement']=$this->Report_model->dead_counts('crm_confirmation_statement_due_date','conf_statement',$last_Year_start,$last_month_end,'and',$company_type);
                       $data['de_others']=$this->Report_model->dead_count_new('crm_confirmation_statement_due_date','conf_statement',$last_Year_start,$last_month_end,'and',$company_type);
                  }
                  if($deadline_type=='accounts'){
                     $data['de_accounts']=$this->Report_model->dead_counts('crm_ch_accounts_next_due','accounts',$last_Year_start,$last_month_end,'and',$company_type);
                      $data['de_others']=$this->Report_model->dead_count_new('crm_ch_accounts_next_due','accounts',$last_Year_start,$last_month_end,'and',$company_type);  
                  }
                  if($deadline_type=='company_tax_return'){
                   $data['de_company_tax_return']=$this->Report_model->dead_counts('crm_accounts_tax_date_hmrc','company_tax_return',$last_Year_start,$last_month_end,'and',$company_type); 
                     $data['de_others']=$this->Report_model->dead_count_new('crm_accounts_tax_date_hmrc','company_tax_return',$last_Year_start,$last_month_end,'and',$company_type); 
                  }
                  if($deadline_type=='personal_tax_return'){
                   $data['de_personal_tax_return']=$this->Report_model->dead_counts('crm_personal_due_date_return','personal_tax_return',$last_Year_start,$last_month_end,'and',$company_type);
                    $data['de_others']=$this->Report_model->dead_count_new('crm_personal_due_date_return','personal_tax_return',$last_Year_start,$last_month_end,'and',$company_type);
                  }
                  if($deadline_type=='payroll'){
                     $data['de_payroll']=$this->Report_model->dead_counts('crm_rti_deadline','payroll',$last_Year_start,$last_month_end,'and',$company_type);
                      $data['de_others']=$this->Report_model->dead_count_new('crm_rti_deadline','payroll',$last_Year_start,$last_month_end,'and',$company_type);
                  }
                  if($deadline_type=='workplace'){
                     $data['de_workplace']=$this->Report_model->dead_counts('crm_pension_subm_due_date','workplace',$last_Year_start,$last_month_end,'and',$company_type);
                      $data['de_others']=$this->Report_model->dead_count_new('crm_pension_subm_due_date','workplace',$last_Year_start,$last_month_end,'and',$company_type);
                  }
                  if($deadline_type=='vat'){
                     $data['de_vat']=$this->Report_model->dead_counts('crm_vat_due_date','vat',$last_Year_start,$last_month_end,'and',$company_type);
                     $data['de_others']=$this->Report_model->dead_count_new('crm_vat_due_date','vat',$last_Year_start,$last_month_end,'and',$company_type);
                  }
                  if($deadline_type=='cis'){
                    $data['de_cis']=$this->Report_model->dead_counts('','cis',$last_Year_start,$last_month_end,'and',$company_type);
                     $data['de_others']=$this->Report_model->dead_count_new('','cis',$last_Year_start,$last_month_end,'and',$company_type);
                  }
                  if($deadline_type=='cissub'){
                     $data['de_cissub']=$this->Report_model->dead_counts('','cissub',$last_Year_start,$last_month_end,'and',$company_type);
                     $data['de_others']=$this->Report_model->dead_count_new('','cissub',$last_Year_start,$last_month_end,'and',$company_type);
                  }
                  if($deadline_type=='p11d'){
                     $data['de_p11d']=$this->Report_model->dead_counts('crm_next_p11d_return_due','p11d',$last_Year_start,$last_month_end,'and',$company_type);
                      $data['de_others']=$this->Report_model->dead_count_new('crm_next_p11d_return_due','p11d',$last_Year_start,$last_month_end,'and',$company_type);
                  }
                  if($deadline_type=='bookkeep'){
                   $data['de_bookkeep']=$this->Report_model->dead_counts('crm_next_booking_date','bookkeep',$last_Year_start,$last_month_end,'and',$company_type);
                   $data['de_others']=$this->Report_model->dead_count_new('crm_next_booking_date','bookkeep',$last_Year_start,$last_month_end,'and',$company_type);
                  }
                  if($deadline_type=='management'){
                    $data['de_management']=$this->Report_model->dead_counts('crm_next_manage_acc_date','management',$last_Year_start,$last_month_end,'and',$company_type);
                     $data['de_others']=$this->Report_model->dead_count_new('crm_next_manage_acc_date','management',$last_Year_start,$last_month_end,'and',$company_type);
                  }
                  if($deadline_type=='investgate'){
                     $data['de_investgate']=$this->Report_model->dead_counts('crm_insurance_renew_date','investgate',$last_Year_start,$last_month_end,'and',$company_type);
                       $data['de_others']=$this->Report_model->dead_count_new('crm_insurance_renew_date','investgate',$last_Year_start,$last_month_end,'and',$company_type);
                  }
                  if($deadline_type=='registered'){
                     $data['de_registered']=$this->Report_model->dead_counts('crm_registered_renew_date','registered',$last_Year_start,$last_month_end,'and',$company_type);
                      $data['de_others']=$this->Report_model->dead_count_new('crm_registered_renew_date','registered',$last_Year_start,$last_month_end,'and',$company_type);
                  }
                  if($deadline_type=='taxadvice'){
                    $data['de_taxadvice']=$this->Report_model->dead_counts('crm_investigation_end_date','taxadvice','and',$company_type);
                      $data['de_others']=$this->Report_model->dead_count_new('crm_investigation_end_date','taxadvice',$last_Year_start,$last_month_end,'and',$company_type);
                  }
                  if($deadline_type=='taxinvest'){
                    $data['de_taxinvest']=$this->Report_model->dead_counts('crm_investigation_end_date','taxinvest','and',$company_type);
                     $data['de_others']=$this->Report_model->dead_count_new('crm_investigation_end_date','taxinvest',$last_Year_start,$last_month_end,'and',$company_type);  
                  }
                /* Company And */
              }else{
                /* Company or */
                if($deadline_type=='conf_statement'){
                    $data['de_conf_statement']=$this->Report_model->dead_counts('crm_confirmation_statement_due_date','conf_statement',$last_Year_start,$last_month_end,'or',$company_type);
                    $data['de_others']=$this->Report_model->dead_count_new('crm_confirmation_statement_due_date','conf_statement',$last_Year_start,$last_month_end,'or',$company_type);
                }
                if($deadline_type=='accounts'){
                   $data['de_accounts']=$this->Report_model->dead_counts('crm_ch_accounts_next_due','accounts',$last_Year_start,$last_month_end,'or',$company_type);
                    $data['de_others']=$this->Report_model->dead_count_new('crm_ch_accounts_next_due','accounts',$last_Year_start,$last_month_end,'or',$company_type);  
                }
                if($deadline_type=='company_tax_return'){
                 $data['de_company_tax_return']=$this->Report_model->dead_counts('crm_accounts_tax_date_hmrc','company_tax_return',$last_Year_start,$last_month_end,'or',$company_type); 
                   $data['de_others']=$this->Report_model->dead_count_new('crm_accounts_tax_date_hmrc','company_tax_return',$last_Year_start,$last_month_end,'or',$company_type); 
                }
                if($deadline_type=='personal_tax_return'){
                 $data['de_personal_tax_return']=$this->Report_model->dead_counts('crm_personal_due_date_return','personal_tax_return',$last_Year_start,$last_month_end,'or',$company_type);
                  $data['de_others']=$this->Report_model->dead_count_new('crm_personal_due_date_return','personal_tax_return',$last_Year_start,$last_month_end,'or',$company_type);
                }
                if($deadline_type=='payroll'){
                   $data['de_payroll']=$this->Report_model->dead_counts('crm_rti_deadline','payroll',$last_Year_start,$last_month_end,'or',$company_type);
                    $data['de_others']=$this->Report_model->dead_count_new('crm_rti_deadline','payroll',$last_Year_start,$last_month_end,'or',$company_type);
                }
                if($deadline_type=='workplace'){
                   $data['de_workplace']=$this->Report_model->dead_counts('crm_pension_subm_due_date','workplace',$last_Year_start,$last_month_end,'or',$company_type);
                    $data['de_others']=$this->Report_model->dead_count_new('crm_pension_subm_due_date','workplace',$last_Year_start,$last_month_end,'or',$company_type);
                }
                if($deadline_type=='vat'){
                   $data['de_vat']=$this->Report_model->dead_counts('crm_vat_due_date','vat',$last_Year_start,$last_month_end,'or',$company_type);
                   $data['de_others']=$this->Report_model->dead_count_new('crm_vat_due_date','vat',$last_Year_start,$last_month_end,'or',$company_type);
                }
                if($deadline_type=='cis'){
                  $data['de_cis']=$this->Report_model->dead_counts('','cis',$last_Year_start,$last_month_end,'or',$company_type);
                   $data['de_others']=$this->Report_model->dead_count_new('','cis',$last_Year_start,$last_month_end,'or',$company_type);
                }
                if($deadline_type=='cissub'){
                   $data['de_cissub']=$this->Report_model->dead_counts('','cissub',$last_Year_start,$last_month_end,'or',$company_type);
                   $data['de_others']=$this->Report_model->dead_count_new('','cissub',$last_Year_start,$last_month_end,'or',$company_type);
                }
                if($deadline_type=='p11d'){
                   $data['de_p11d']=$this->Report_model->dead_counts('crm_next_p11d_return_due','p11d',$last_Year_start,$last_month_end,'or',$company_type);
                    $data['de_others']=$this->Report_model->dead_count_new('crm_next_p11d_return_due','p11d',$last_Year_start,$last_month_end,'or',$company_type);
                }
                if($deadline_type=='bookkeep'){
                 $data['de_bookkeep']=$this->Report_model->dead_counts('crm_next_booking_date','bookkeep',$last_Year_start,$last_month_end,'or',$company_type);
                 $data['de_others']=$this->Report_model->dead_count_new('crm_next_booking_date','bookkeep',$last_Year_start,$last_month_end,'or',$company_type);
                }
                if($deadline_type=='management'){
                  $data['de_management']=$this->Report_model->dead_counts('crm_next_manage_acc_date','management',$last_Year_start,$last_month_end,'or',$company_type);
                   $data['de_others']=$this->Report_model->dead_count_new('crm_next_manage_acc_date','management',$last_Year_start,$last_month_end,'or',$company_type);
                }
                if($deadline_type=='investgate'){
                   $data['de_investgate']=$this->Report_model->dead_counts('crm_insurance_renew_date','investgate',$last_Year_start,$last_month_end,'or',$company_type);
                     $data['de_others']=$this->Report_model->dead_count_new('crm_insurance_renew_date','investgate',$last_Year_start,$last_month_end,'or',$company_type);
                }
                if($deadline_type=='registered'){
                   $data['de_registered']=$this->Report_model->dead_counts('crm_registered_renew_date','registered',$last_Year_start,$last_month_end,'or',$company_type);
                    $data['de_others']=$this->Report_model->dead_count_new('crm_registered_renew_date','registered',$last_Year_start,$last_month_end,'or',$company_type);
                }
                if($deadline_type=='taxadvice'){
                  $data['de_taxadvice']=$this->Report_model->dead_counts('crm_investigation_end_date','taxadvice',$last_Year_start,$last_month_end,'or',$company_type);
                    $data['de_others']=$this->Report_model->dead_count_new('crm_investigation_end_date','taxadvice',$last_Year_start,$last_month_end,'or',$company_type);
                }
                if($deadline_type=='taxinvest'){
                  $data['de_taxinvest']=$this->Report_model->dead_counts('crm_investigation_end_date','taxinvest',$last_Year_start,$last_month_end,'or',$company_type);
                   $data['de_others']=$this->Report_model->dead_count_new('crm_investigation_end_date','taxinvest',$last_Year_start,$last_month_end,'or',$company_type); 
                }
              }
              $this->load->view('reports/deadline_success',$data);
            }else{
              $status='1';
              echo $status;              
            }

        }

        public function client_filter(){

        $last_Year_start = date("Y-m-01");
        $last_month_end = date("Y-m-t");        

        $data['Private']=$this->Common_mdl->ClientDetailsByLegalForm('Private Limited company',$last_Year_start,$last_month_end); 

        $data['Public']=$this->Common_mdl->ClientDetailsByLegalForm('Public Limited company',$last_Year_start,$last_month_end);

        $data['limited']=$this->Common_mdl->ClientDetailsByLegalForm('Limited Liability Partnership',$last_Year_start,$last_month_end);

        $data['Partnership']=$this->Common_mdl->ClientDetailsByLegalForm('Partnership',$last_Year_start,$last_month_end);

        $data['self']=$this->Common_mdl->ClientDetailsByLegalForm('Self Assessment',$last_Year_start,$last_month_end);

        $data['Trust']=$this->Common_mdl->ClientDetailsByLegalForm('Trust',$last_Year_start,$last_month_end);

        $data['Charity']=$this->Common_mdl->ClientDetailsByLegalForm('Charity',$last_Year_start,$last_month_end);

        $data['Other']=$this->Common_mdl->ClientDetailsByLegalForm('Other',$last_Year_start,$last_month_end);
        $this->load->view('reports/legal_form_success',$data);
        //echo json_encode($data);
   
        }


         public function client_filter_year(){
        $last_Year_start = date("Y-01-01");
        $last_month_end = date("Y-12-31");        

        $data['Private']=$this->Common_mdl->ClientDetailsByLegalForm('Private Limited company',$last_Year_start,$last_month_end); 

        $data['Public']=$this->Common_mdl->ClientDetailsByLegalForm('Public Limited company',$last_Year_start,$last_month_end);

        $data['limited']=$this->Common_mdl->ClientDetailsByLegalForm('Limited Liability Partnership',$last_Year_start,$last_month_end);

        $data['Partnership']=$this->Common_mdl->ClientDetailsByLegalForm('Partnership',$last_Year_start,$last_month_end);

        $data['self']=$this->Common_mdl->ClientDetailsByLegalForm('Self Assessment',$last_Year_start,$last_month_end);

        $data['Trust']=$this->Common_mdl->ClientDetailsByLegalForm('Trust',$last_Year_start,$last_month_end);

        $data['Charity']=$this->Common_mdl->ClientDetailsByLegalForm('Charity',$last_Year_start,$last_month_end);

        $data['Other']=$this->Common_mdl->ClientDetailsByLegalForm('Other',$last_Year_start,$last_month_end);
        $this->load->view('reports/legal_form_success',$data);
        //echo json_encode($data);
    }

    public function Client_customisation()
    {  
        $this->Security_model->chk_login();   

        if($_SESSION['permission']['Reports']['view'] == '1')
        {
          $data['client']=$this->Report_model->client_list();            
          
          $this->load->view('reports/Client_customisation',$data);
        }
        else
        {
          $this->load->view('users/blank_page');
        }
    }

     public function Task_customisation()
     {
        $this->Security_model->chk_login();
        
        if($_SESSION['permission']['Reports']['view'] == '1')
        {
          $data['client']=$this->Report_model->client_list(); 
          
          $this->load->view('reports/Task_customisation',$data);
        }
        else
        {
          $this->load->view('users/blank_page');
        }
     }

     public function Proposal_customisation()
     {
        $this->Security_model->chk_login();

        if($_SESSION['permission']['Reports']['view'] == '1')
        {
          $data['client']=$this->Report_model->client_list(); 

          $this->load->view('reports/Proposal_customisation',$data);
        }
        else
        {
          $this->load->view('users/blank_page');
        }
     } 

     public function Leads_customisation()
     {
        $this->Security_model->chk_login();

        if($_SESSION['permission']['Reports']['view'] == '1')
        {
          $data['client']=$this->Report_model->client_list(); 
          $data['leads_status']=$this->Common_mdl->getallrecords('leads_status');

          $this->load->view('reports/Leads_customisation',$data);
        }
        else
        {
          $this->load->view('users/blank_page');
        }

     }

     public function Service_customisation()
     {
        $this->Security_model->chk_login();
     
        if($_SESSION['permission']['Reports']['view'] == '1')
        {
          $data['client']=$this->Report_model->client_list(); 
          
          $this->load->view('reports/Service_customisation',$data);
        }
        else
        {
          $this->load->view('users/blank_page');
        }
     }

     public function Deadline_customisation()
     {
        $this->Security_model->chk_login();
      
        if($_SESSION['permission']['Reports']['view'] == '1')
        {
          $data['client']=$this->Report_model->client_list();

          $this->load->view('reports/Deadline_customisation',$data);
        }
        else
        {
          $this->load->view('users/blank_page');
        }
     }

     public function Invoice_customisation()
     {
        $this->Security_model->chk_login();
        
        if($_SESSION['permission']['Reports']['view'] == '1')
        {
          $data['client']=$this->Report_model->client_list();

          $this->load->view('reports/Invoice_customisation',$data);
        }
        else
        {
          $this->load->view('users/blank_page');
        }
     }


     public function Client()
     {  
         $this->Security_model->chk_login();

         if($_SESSION['permission']['Reports']['view'] == '1')
         {
            $data['schedule_records'] = $this->db->query('SELECT * from reports_schedule where user_id="'.$_SESSION['id'].'" and reports_type="client"')->row_array();
            $data['records'] = $this->Report_model->client_list(); 
            $data['groups'] = $this->db->query("SELECT * from `Groups` where user_id='".$_SESSION['id']."'")->result_array();  
            $data['getallUser'] = $this->Report_model->getClients();

            $this->load->view('reports/report_dashboard',$data);
         }
         else
         {
            $this->load->view('users/blank_page');
         }
     }

     public function Task()
     {
         $this->Security_model->chk_login();

         if($_SESSION['permission']['Reports']['view'] == '1')
         {
             $data['schedule_records'] = $this->db->query('SELECT * from reports_schedule where user_id="'.$_SESSION['id'].'" and reports_type="task"')->row_array();
             $data['records'] = $this->Report_model->getTasks(); 
             $data['groups'] = $this->db->query("SELECT * from `Groups` where user_id='".$_SESSION['id']."'")->result_array();  
             $data['getallUser'] = $this->Report_model->getClients();

             $this->load->view('reports/task_dashboard',$data);
         }
         else
         {
             $this->load->view('users/blank_page');
         }
     }

     public function Proposal()
     {
         $this->Security_model->chk_login();

         if($_SESSION['permission']['Reports']['view'] == '1')
         {
             $data['schedule_records'] = $this->db->query('SELECT * from reports_schedule where user_id="'.$_SESSION['id'].'" and reports_type="proposal"')->row_array();
             $data['records'] = $this->Proposal_model->proposals(); 
             $data['groups'] = $this->db->query("SELECT * from `Groups` where user_id='".$_SESSION['id']."'")->result_array();  
             $data['getallUser'] = $this->Report_model->getClients();
             
             $this->load->view('reports/proposal_dashboard',$data); 
         }
         else
         {
             $this->load->view('users/blank_page');
         }
     }

     public function Leads()
     {
         $this->Security_model->chk_login();
         
         if($_SESSION['permission']['Reports']['view'] == '1')
         {
             $data['schedule_records'] = $this->db->query('SELECT * from reports_schedule where user_id="'.$_SESSION['id'].'" and reports_type="leads"')->row_array();
             $data['records'] = $this->Report_model->getLeads(); 
             $data['groups'] = $this->db->query("SELECT * from `Groups` where user_id='".$_SESSION['id']."'")->result_array();  
             $data['getallUser'] = $this->Report_model->getClients();
             $data['leads_status']=$this->Common_mdl->getallrecords('leads_status');

             $this->load->view('reports/Leads_dashboard',$data);   
         }
         else
         {
             $this->load->view('users/blank_page');
         }       
     }

     public function Service()
     {
        $this->Security_model->chk_login();
        
        if($_SESSION['permission']['Reports']['view'] == '1')
        {
            $data['schedule_records'] = $this->db->query('SELECT * from reports_schedule where user_id="'.$_SESSION['id'].'" and reports_type="service"')->row_array();
            $data['records'] = $this->Report_model->getServices(); 
            $data['groups'] = $this->db->query("SELECT * from `Groups` where user_id='".$_SESSION['id']."'")->result_array();  
            $data['getallUser'] = $this->Report_model->getClients();    
            
            $this->load->view('reports/Service_dashboard',$data);       
        }
        else
        {
            $this->load->view('users/blank_page');
        } 
     }

     public function Dead_line()
     {
        $this->Security_model->chk_login();
      
        if($_SESSION['permission']['Reports']['view'] == '1')
        {
            $data['schedule_records'] = $this->db->query('SELECT * from reports_schedule where user_id="'.$_SESSION['id'].'" and reports_type="deadline"')->row_array();
            $data['records'] = $this->Report_model->getServices();
            $data['groups'] = $this->db->query("SELECT * from `Groups` where user_id='".$_SESSION['id']."'")->result_array();  
            $data['getallUser'] = $this->Report_model->getClients();  

            $this->load->view('reports/Deadline_dashboard',$data);   
        }
        else
        {
            $this->load->view('users/blank_page');
        } 
    }

    public function Invoice()
    {
        $this->Security_model->chk_login();

        if($_SESSION['permission']['Reports']['view'] == '1')
        {
            $data['schedule_records'] = $this->db->query('SELECT * from reports_schedule where user_id="'.$_SESSION['id'].'" and reports_type="invoice"')->row_array();       
            $data['groups'] = $this->db->query("SELECT * from `Groups` where user_id='".$_SESSION['id']."'")->result_array();  
            $data['getallUser'] = $this->Report_model->getClients(); 
            
            $clients_user_ids = $this->Report_model->getClientsUserIds();
            $data['records'] = $this->Invoice_model->selectInvoiceByType('','','','client_email', $clients_user_ids);
            
            $Assigned_Leads = Get_Assigned_Datas('LEADS');
            $assigned_leads = implode(',', $Assigned_Leads);

            $data['draft'] = $this->db->query('SELECT * from invoices where status = "draft" and FIND_IN_SET(lead_id,"'.$assigned_leads.'")')->result_array();
         
            $this->load->view('reports/Invoice_dashboard',$data);
        }
        else
        {
            $this->load->view('users/blank_page');
        } 
    }

    public function Summary()
    {
        $this->Security_model->chk_login();

        if($_SESSION['permission']['Reports']['view'] == '1')
        {
            $data['schedule_records'] = $this->db->query('SELECT * from reports_schedule where user_id="'.$_SESSION['id'].'" and reports_type="summary"')->row_array();       
            $data['groups'] = $this->db->query("SELECT * from `Groups` where user_id='".$_SESSION['id']."'")->result_array();  
            $data['getallUser'] = $this->Report_model->getClients();       

            $data['firm_users'] = $this->Report_model->getFirmUsers();      
            $data['completed_tasks'] = $this->Report_model->getCompletedTasks();   
            $data['lead_details'] = $this->Report_model->getLeads();  
            $data['leads_status'] = $this->Common_mdl->getallrecords('leads_status');  
            $data['column_setting'] = $this->Common_mdl->getallrecords('column_setting_new');
            
            $this->load->view('reports/Summary_dashboard',$data);      
        }
        else
        {
            $this->load->view('users/blank_page');
        }           
    }

    public function SummaryCustomisation()
    {
       $this->Security_model->chk_login();   
       
       if($_SESSION['permission']['Reports']['view'] == '1')
       {
           $data['client']=$this->Report_model->client_list();  
           $this->load->view('reports/Summary_customisation',$data);  
       }
       else
       {
           $this->load->view('users/blank_page');
       } 
    }

    public function schdule_insert()
    { 
       $startTime = date("Y-m-d");

       if($_POST['repeats']=='week')
       {      
         $schedule_start_date=date('Y-m-d',strtotime('+'.$_POST['every_week'].' week',strtotime($startTime)));
       }
       else if($_POST['repeats']=='day')
       {
         $schedule_start_date=date('Y-m-d',strtotime('+'.$_POST['every_week'].' day',strtotime($startTime)));
       }
       else if($_POST['repeats']=='month')
       {
         $schedule_start_date=date('Y-m-d',strtotime('+'.$_POST['every_week'].' month',strtotime($startTime)));
       }
       else
       {
         $schedule_start_date=date('Y-m-d',strtotime('+'.$_POST['every_week'].' year',strtotime($startTime)));
       }

       $timestamp = strtotime($_POST['monthly_date']);
       $day = date('d', $timestamp);

       $data=array(
        'repeats'=>$_POST['repeats'],
        'every_week'=>$_POST['every_week'],
        'days'=>$_POST['days'],
        'ends'=>$_POST['ends'],
        'message'=>$_POST['message'],
        'schedule_on_date'=>date('Y-m-d',strtotime($_POST['schedule_on_date'])),
        'users'=>(isset($_POST['users'])&&!empty($_POST['users']))?$_POST['users']:"",
        'groups'=>(isset($_POST['groups'])&&!empty($_POST['groups']))?$_POST['groups']:"",
        'emails'=>trim($_POST['email'], ","),
        'reports_type'=>$_POST['reports_type'],
        'monthly_date'=>$day,
        'user_id'=>$_SESSION['id'],'schedule_start_date'=>$schedule_start_date,'schedule_times'=>$_POST['message']);

        $insert=$this->Common_mdl->insert('reports_schedule',$data);      
        echo $insert;      
    }
    /* 01.09.2018 */
     public function reports_schedule(){
      $client=$this->db->query('select * from reports_schedule')->result_array();
      foreach ($client as $key => $value) {
        $this->Report_model->Client_reports($value['id']);
      }

      // $task=$this->db->query('select * from reports_schedule where reports_type="task"')->result_array();     
      //   foreach ($task as $key => $value) {
      //     $this->Report_model->Client_reports($value['id']);
      //   }


    }

    public function task_schedule(){

    }

   public function schdule_update()
   {
      $startTime = date("Y-m-d");

      if($_POST['repeats']=='week'){
         $schedule_start_date=date('Y-m-d',strtotime('+'.$_POST['every_week'].' week',strtotime($startTime)));

      }else if($_POST['repeats']=='day'){
         $schedule_start_date=date('Y-m-d',strtotime('+'.$_POST['every_week'].' day',strtotime($startTime)));
      }else if($_POST['repeats']=='month'){
         $schedule_start_date=date('Y-m-d',strtotime('+'.$_POST['every_week'].' month',strtotime($startTime)));
      }else{
         $schedule_start_date=date('Y-m-d',strtotime('+'.$_POST['every_week'].' year',strtotime($startTime)));
      }

      $id=$_POST['id'];    
      $timestamp = strtotime($_POST['monthly_date']);
      $day = date('d', $timestamp);

      $data=array(
        'repeats'=>$_POST['repeats'],
        'every_week'=>$_POST['every_week'],
        'days'=>$_POST['days'],
        'ends'=>$_POST['ends'],
        'message'=>$_POST['message'],
        'schedule_on_date'=>date('Y-m-d',strtotime($_POST['schedule_on_date'])),
        'users'=>$_POST['users'],
        'groups'=>$_POST['groups'],
        'emails'=>trim($_POST['email'], ","),
        'reports_type'=>$_POST['reports_type'],
        'monthly_date'=>$day,
        'user_id'=>$_SESSION['id'],
        'schedule_start_date'=>$schedule_start_date,
        'schedule_times'=>$_POST['message']);

      $insert=$this->Common_mdl->update('reports_schedule',$data,'id',$id);

      $in=1;
      echo $in;      
    }



    public function groups(){

       $this->Security_model->chk_login();
        //$data['getallUser'] = $this->Common_mdl->GetAllWithExceptFieldOrder('user','role','1','DESC');

           
         //  $data['leads']=$this->Common_mdl->select_record('leads','id',$id);
        if($_SESSION['roleId']==1)
        {
            $login_user_id=$_SESSION['userId'];
     

        $data['getallUser']=$this->db->query("SELECT * FROM user where role='4' AND autosave_status!='1' and crm_name!='' and firm_admin_id=".$login_user_id." order by id DESC")->result_array();

         $user_staff_data=$this->db->query("SELECT * FROM user where role='6' AND autosave_status!='1' and crm_name!='' and firm_admin_id=".$login_user_id." order by id DESC")->result_array();
        
        $leads_data=$this->db->query("SELECT * FROM leads where user_id=".$login_user_id." order by id DESC")->result_array();
        $taskdata=$this->db->query("SELECT * FROM add_new_task where create_by=".$login_user_id." order by id DESC")->result_array();
$assign='';$worker='';
$user_staff=array();
                foreach ($leads_data as $leads_key => $leads_value) {
                    if($leads_value['assigned']!=''){
                        $assign.=$leads_value['assigned'].",";
                    }

                }
                foreach ($taskdata as $task_key => $task_value) {
                    if($task_value['worker']!=''){
                        $worker.=$task_value['worker'].",";
                    }
                }
                foreach ($user_staff_data as $user_staff_key => $user_staff_value) {
                    //user_staff
                    array_push($user_staff,$user_staff_value['id']);
                   
                }
                $leadstask=array_values(array_unique(array_merge(array_filter(explode(',',$assign)),array_filter(explode(',', $worker)))));
                $newarray=array_values(array_unique(array_merge(array_filter($leadstask),array_filter($user_staff))));

                if(!empty($newarray)){
                $data['getallstaff']=$this->db->query("SELECT * FROM user where role='6' AND autosave_status!='1' and crm_name!='' and id in (".implode(',',$newarray).") order by id DESC")->result_array();
                }

               // echo "SELECT * FROM user where role='6' AND autosave_status!='1' and crm_name!='' and id in (".implode(',',$newarray).") order by id DESC";

        }
        if($_SESSION['roleId']==4)
        {
            $login_user_id=$_SESSION['userId'];
            $client_data=$this->db->query("SELECT * FROM user where role='4' AND autosave_status!='1' and crm_name!='' and id=".$login_user_id." order by id DESC")->result_array();
           
             $firm_id=$client_data[0]['firm_admin_id'];
         
            $data['getalladmin']=$this->db->query("SELECT * FROM user where crm_name!='' and id=".$firm_id." order by id DESC")->result_array();

            /** extra added for staff shown **/
        $data['getallUser']=$this->db->query("SELECT * FROM user where role='4' AND autosave_status!='1' and crm_name!='' and firm_admin_id=".$login_user_id." order by id DESC")->result_array();

         $user_staff_data=$this->db->query("SELECT * FROM user where role='6' AND autosave_status!='1' and crm_name!='' and firm_admin_id=".$login_user_id." order by id DESC")->result_array();
        
        $leads_data=$this->db->query("SELECT * FROM leads where user_id=".$login_user_id." order by id DESC")->result_array();
        $taskdata=$this->db->query("SELECT * FROM add_new_task where create_by=".$login_user_id." order by id DESC")->result_array();
        $assign='';$worker='';
        $user_staff=array();
                foreach ($leads_data as $leads_key => $leads_value) {
                    if($leads_value['assigned']!=''){
                        $assign.=$leads_value['assigned'].",";
                    }

                }
                foreach ($taskdata as $task_key => $task_value) {
                    if($task_value['worker']!=''){
                        $worker.=$task_value['worker'].",";
                    }
                }
                foreach ($user_staff_data as $user_staff_key => $user_staff_value) {
                    //user_staff
                    array_push($user_staff,$user_staff_value['id']);
                   
                }
                $leadstask=array_values(array_unique(array_merge(array_filter(explode(',',$assign)),array_filter(explode(',', $worker)))));
                $newarray=array_values(array_unique(array_merge(array_filter($leadstask),array_filter($user_staff))));

                if(!empty($newarray)){
                $data['getallstaff']=$this->db->query("SELECT * FROM user where role='6' AND autosave_status!='1' and crm_name!='' and id in (".implode(',',$newarray).") order by id DESC")->result_array();
                /** for staff end **/
                }
                else
                {
                    $data['getallstaff']='';
                }

        }

        if($_SESSION['roleId']==6)
        {
            $login_user_id=$_SESSION['userId'];
            $client_data=$this->db->query("SELECT * FROM user where role='6' AND autosave_status!='1' and crm_name!='' and id=".$login_user_id." order by id DESC")->result_array();
           
             $firm_id=$client_data[0]['firm_admin_id'];
         
            $data['getalladmin']=$this->db->query("SELECT * FROM user where crm_name!='' and id=".$firm_id." order by id DESC")->result_array();

            $leads_data=$this->db->query("SELECT * FROM leads where find_in_set('".$login_user_id."',assigned)  order by id DESC")->result_array();
            $taskdata=$this->db->query("SELECT * FROM add_new_task where find_in_set('".$login_user_id."',worker) order by id DESC")->result_array();
            $for_assigned=array();
            foreach ($leads_data as $leads_key => $leads_value) {
               array_push($for_assigned, $leads_value['user_id']);
            }
            foreach ($taskdata as $task_key => $task_value) {
               array_push($for_assigned, $task_value['create_by']);
            }
            $newarray=array_values(array_unique($for_assigned));
            if(!empty($newarray)){
                $data['getallUser']=$this->db->query("SELECT * FROM user where id in (".implode(',',$newarray).")  order by id DESC")->result_array();
            }
            else
            {
                $data['getallUser']='';
            }

        }
        $data['column_setting']=$this->Common_mdl->getallrecords('column_setting_new');
        $data['client'] =$this->db->query('select * from column_setting_new where id=1')->row_array();
        $data['feedback'] =$this->db->query("select id,username,crm_email_id from user where username!='' and username!='0' and  crm_email_id!='' and crm_email_id!='0' "  )->result();

        $data['chat_result']=$this->db->query("select * from `Groups` where user_id='".$_SESSION['id']."'")->result_array();

         $this->load->view('reports/group',$data); 

    }

    public function group_insert()
    {      
        $datas=array('user_id'=>$_SESSION['id'],'groupname'=>$_POST['group_name'],'users'=>implode(',',$_POST['our_group_data']));
        $insert=$this->Common_mdl->insert('Groups',$datas);

        $groups_list=$this->db->query("SELECT * from `Groups` where user_id='".$_SESSION['id']."' order by id desc")->result_array();

        $data['content']='<select name="repeats" class="form-control repeats schd-reports" id="groups" data-date="" data-day="" data-month="">';

        foreach($groups_list as $group)
        {        
           $data['content'].=' <option value="'.$group['id'].'">'.$group['groupname'].'</option>';
        }

        $data['content'].=' </select>';
        echo json_encode($data);
    }


    public function edit_group()
    {
       $data=array('groupname'=>$_POST['group_name'],'users'=>implode(',',$_POST['our_group_data']));
       $insert=$this->Common_mdl->update('Groups',$data,'id',$_POST['id']);
       redirect('report/groups');
    }

    public function delete_group($id)
    {
       $insert=$this->Common_mdl->delete('Groups','id',$id);
       redirect('report/groups');      
    }




   




 } ?>
