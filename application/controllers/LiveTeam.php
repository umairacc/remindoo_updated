<?php

class LiveTeam extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model(array('Common_mdl', 'Security_model', 'Task_creating_model'));		
		$this->load->helper(['template_decode', 'firm_columnSettings']);		
			
		$this->prerequisities 	= $this->getPrerequisities();		
		$this->wpd 			  	= $this->prerequisities['wpd'];
		$this->dtwk			  	= $this->prerequisities['dtwk'];
		$this->dlwk			  	= $this->prerequisities['dlwk'];
		$this->tm			  	= $this->prerequisities['tm'];
		$this->lm			  	= $this->prerequisities['lm'];
		$this->fid  	 	  	= $this->prerequisities['fid'];	
		$this->today 			= $this->prerequisities['today'];
		$this->yesterday 		= $this->prerequisities['yesterday'];
		$this->this_week_start  = $this->prerequisities['this_week_start'];	
		$this->this_week_end 	= $this->prerequisities['this_week_end'];
		$this->last_week_start  = $this->prerequisities['last_week_start'];
		$this->last_week_end 	= $this->prerequisities['last_week_end'];
		$this->this_month_start = $this->prerequisities['this_month_start'];
		$this->this_month_end	= $this->prerequisities['this_month_end'];
		$this->last_month_start = $this->prerequisities['last_month_start'];
		$this->last_month_end   = $this->prerequisities['last_month_end'];
		$this->users 	 	  	= $this->get_firm_users($this->fid, ['FA', 'FU']);		
	}	

	public function index(){
		//$this->syncHistoricalTimelogs();
		$this->load->view('LiveTeam/live_team');	
	}

	public function getPrerequisities(){
		$wpd       = 7.5 * 60 * 60; 										// Work time per day in seconds
		$twt       = $wpd; 													// Total work time		
		$fdlm      = new DateTime("first day of last month");				// Get first date of last month
		$thisMonth = $this->working_days(date('Y-m-d', time()));			// Get week wise working days of current month		
		$lastMonth = $this->working_days($fdlm->format('Y-m-d'));	    	// Get week wise working days of last month
		$today     = date('Y-m-d', time());									// Current date
		$yesterday = date('Y-m-d', strtotime("-1 days")); 					// Yesterday's date
		$tdobj     = new DateTime($today); 										
		$ldobj     = new DateTime($yesterday);	
		$tweek     = $tdobj->format("W");									// Week number of this week (Year wise)
		$lweek     = $ldobj->format("W"); 									// Week number of last week (Year wise)
		$dtwk      = isset($thisMonth[$tweek]) ? $thisMonth[$tweek] : 7;	// Working days in current week
		$dlwk      = isset($thisMonth[$lweek]) ? $thisMonth[$lweek] : 7;	// Working days in last week
		$tm        = !empty($thisMonth) ? array_sum($thisMonth) : 30; 		// Working days in current month
		$lm        = !empty($lastMonth) ? array_sum($lastMonth) : 30; 		// Working days in last month

		return [
			'wpd'  				=> $wpd,
			'dtwk' 				=> $dtwk,
			'dlwk' 				=> $dlwk,
			'tm'   				=> $tm,
			'lm'   				=> $lm,
			'fid'  	 	  		=> $_SESSION['firm_id'],	
			'today' 			=> date('Y-m-d', time()),
			'yesterday' 		=> date('Y-m-d', strtotime("-1 day", time())),
			'this_week_start'  	=> date("Y-m-d", strtotime("last sunday")),	
			'this_week_end' 	=> date("Y-m-d", strtotime("this saturday")),
			'last_week_start'  	=> date("Y-m-d", strtotime("-7 day", strtotime("last sunday"))),
			'last_week_end' 	=> date("Y-m-d", strtotime("last saturday")),
			'this_month_start' 	=> date('Y-m-01', strtotime('this month')),
			'this_month_end'	=> date('Y-m-t', strtotime('this month')),
			'last_month_start' 	=> date('Y-m-01', strtotime('last month')),
			'last_month_end'   	=> date('Y-m-t', strtotime('last month'))	
		];
	}

	public function get_live_team(){		
		unset($_SESSION['team_data']);

		$user_time_logs 	   = $this->get_user_time_logs($this->users, 90); // We are fetching records for last 90 days		
		$merged_data 		   = $this->mergeLiveTeamData($this->users, $this->user_data, $user_time_logs);
		$live_team_activity    = $this->liveTeamActivity($merged_data);	
		$_SESSION['team_data'] = json_encode($live_team_activity);
		
		return $live_team_activity;
	}

	public function mergeLiveTeamData($users, $user_data, $user_time_logs){
		$res 		   = [];
		$staff_data    = [];
		$task_data 	   = [];
		$customer_data = [];
		
		foreach($user_time_logs as $task_id=>$task){
			foreach($task as $t){
				# Task Data
				$task_data[$task_id] = !isset($task_data[$task_id]) ? $this->get_task_data($task_id) : $task_data[$task_id];
				$service_name 		 = $task_data[$task_id]['subject'];			
				$created_at   		 = $t['created_time'];
				$start_time 		 = $t['start_time'];
				$end_time 			 = $t['end_time'];
				$chargeable   		 = $task_data[$task_id]['billable'];	
				$is_active 	  		 = $t['is_active'];
				
				# Staff Data
				$staff_id   		   = $t['user_id'];
				$staff_data[$staff_id] = !isset($staff_data[$staff_id]) ? $this->get_user_data($staff_id) : $staff_data[$staff_id];
				$staff_name 		   = $staff_data[$staff_id]['crm_name'];
				
				# Customer Data			
				$customer_id   				 = $task_data[$task_id]['user_id'];
				$customer_data[$customer_id] = !isset($customer_data[$customer_id]) ? $this->get_user_data($customer_id) : $customer_data[$customer_id];			
				$customer_name 				 = $customer_data[$customer_id]['crm_name'];

				# Merged Data
				$res[] = [
					'STAFF_ID'		=> $staff_id,
					'STAFF_NAME'	=> $staff_name,		
					'CUSTOMER_ID'	=> $customer_id,
					'CUSTOMER_NAME'	=> $customer_name,
					'SERVICE_ID'	=> $task_id,						
					'START_TIME' 	=> $start_time,
					'END_TIME' 		=> $end_time,
					'CREATED_AT'	=> $created_at,
					'TASK_ID'		=> $task_id,
					'SERVICE_NAME'	=> $service_name,
					'CHARGEABLE'	=> $chargeable,
					'STATUS'		=> $is_active
				];
			}
		}
		return $res;
	}

	public function get_firm_users($firm_id=0, $type=[]){
		$sql  = "SELECT GROUP_CONCAT(DISTINCT id) AS IDS FROM user WHERE 1 ";
		$sql .= ($firm_id) ? " AND firm_id =  $firm_id "	: "";			
		$res  = $this->db->query($sql)->result_array();	
		return !empty($res) ? $res[0]['IDS'] : '';
	}

	public function get_user_time_logs($ids, $days){
		$sql  = "SELECT user_id, 
						task_id,
						start_time,
						IF(end_time IS NULL, UNIX_TIMESTAMP(NOW()), end_time) AS end_time, 						
						UNIX_TIMESTAMP(created) AS created_time, 
						status AS is_active
				FROM active_timers
				WHERE user_id IN($ids)
				  AND DAY(created) >= (DAY(current_date())-$days) 
				  AND YEAR(created) = YEAR(current_date())
				GROUP BY user_id, task_id, start_time
				ORDER BY is_active DESC, created DESC";	
				
		$logs = $this->db->query($sql)->result_array();
		$res  = [];

		if(!empty($logs)){
			foreach($logs as $log) { $res[$log['task_id']][] = $log; }
		}	
		return $res;
	}

	public function get_task_data($id=0){
		$res = [];
		if($id){
			$sql       = "SELECT id, user_id, subject, billable FROM add_new_task WHERE id = $id LIMIT 1";
			$task_data = $this->db->query($sql)->result_array();			

			if(!empty($task_data)){
				foreach($task_data as $task){
					$res = [
						'user_id'  => $task['user_id'],
						'subject'  => $task['subject'],
						'billable' => $task['billable']
					];
				}
			}	
		}
		return $res;	
	}

	public function get_user_data($id=0){
		$res = [];	
		if($id){
			$sql  = "SELECT id, crm_name FROM user WHERE id = $id LIMIT 1";
			$user = $this->db->query($sql)->result_array($sql);	
			$res  = !empty($user) ? $user[0] : [];			
		}
		return $res;
	}

	public function liveTeamActivity($data=[]){
		$timers = [];
		
		if(!empty($data)){
			foreach($data as $row){	
				$start_time = $row['START_TIME'];;
				$end_time 	= $row['END_TIME'];
				$for_date  	= date('Y-m-d', $row['CREATED_AT']);

				if($start_time != ''){
					$time_seconds = $this->Task_creating_model->calculate_diff($start_time, $end_time);
					$time_seconds = $this->Task_creating_model->time_to_sec($time_seconds);						
					$timer 		  = [
						'FOR_DATE' 		=> $for_date,
						'TIMER_STARTED' => $start_time,
						'TASK_ID'		=> $row['TASK_ID'],
						'STAFF_ID'		=> $row['STAFF_ID'],
						'STAFF_NAME'    => addslashes($row['STAFF_NAME']),
						'TIME_SPENT'    => isset($timers[$row['STAFF_ID']][$row['TASK_ID'].'_'.$for_date]['TIME_SPENT']) ? ($timers[$row['STAFF_ID']][$row['TASK_ID'].'_'.$for_date]['TIME_SPENT'] + $time_seconds) : $time_seconds,
						'CUSTOMER_NAME' => '<a href="/client/client_infomation/'.$row['CUSTOMER_ID'].'" target="_blank">'.addslashes($row['CUSTOMER_NAME']).'</a>',
						'SERVICE_NAME'  => '<a href="/user/task_details/'.$row['TASK_ID'].'" target="_blank">'.addslashes($row['SERVICE_NAME']).'</a>',
						'CHARGEABLE'    => $row['CHARGEABLE'],
						'CREATED_AT'    => $row['CREATED_AT'],
						'TIMER_STATUS'  => $row['STATUS'],
					];
					$timers[] = $timer;
				}
			}
		}
		//return $timers;		
		return $this->beutifyLiveTeamData($timers);
	}

	public function beutifyLiveTeamData($data=[]){
		$res 	   = [];	
		$team_data = [];	
		if(!empty($data)){
			foreach($data as $row){	
				$time_spent   = isset($res[$row['FOR_DATE']][$row['STAFF_ID']][$row['TASK_ID']]['TIME_SPENT']) ? $res[$row['FOR_DATE']][$row['STAFF_ID']][$row['TASK_ID']]['TIME_SPENT'] + $row['TIME_SPENT'] : $row['TIME_SPENT'];
				$timer_status = (isset($res[$row['FOR_DATE']][$row['STAFF_ID']][$row['TASK_ID']]['TIMER_STATUS']) && $res[$row['FOR_DATE']][$row['STAFF_ID']][$row['TASK_ID']]['TIMER_STATUS'] == 1) ? 1 : $row['TIMER_STATUS'];
				
				$res[$row['FOR_DATE']][$row['STAFF_ID']][$row['TASK_ID']] = [
					'FOR_DATE' 		=> $row['FOR_DATE'],
					'TIMER_STARTED' => $row['TIMER_STARTED'],
					'TASK_ID'		=> $row['TASK_ID'],
					'STAFF_ID'		=> $row['STAFF_ID'],
					'STAFF_NAME'    => $row['STAFF_NAME'],
					'TIME_SPENT'    => $time_spent,
					'CUSTOMER_NAME' => $row['CUSTOMER_NAME'],
					'SERVICE_NAME'  => $row['SERVICE_NAME'],
					'CHARGEABLE'    => $row['CHARGEABLE'],
					'CREATED_AT'    => $row['CREATED_AT'],
					'TIMER_STATUS'  => $timer_status
				]; 
			}
			foreach($res as $r1){
				foreach($r1 as $r2){
					foreach($r2 as $r3){
						$team_data[] = $r3;
					}
				}				
			}
		}
		array_multisort(array_column($team_data, 'TIMER_STATUS'), SORT_DESC, SORT_NATURAL|SORT_FLAG_CASE, $team_data);
		return $team_data;
	}

	public function get_live_team_report(){	
		$team = $this->get_live_team();			
		$this->load->view('LiveTeam/live_team_report_view', ['data' => $team]);		
	}
	
	public function get_performance_analytics(){
		$data 	   	  = json_decode($_SESSION['team_data']);
		$analytics 	  = [];		
		$task_details = [];
		
		if(!empty($data)){		
			foreach($data as $row){
				$row->FOR_DATE = date('Y-m-d', strtotime($row->FOR_DATE));					
				# For today's records
				if($row->FOR_DATE == $this->today){		
					$time_spent = (isset($analytics['today'][$row->STAFF_ID]['TIME_SPENT'])) ? $analytics['today'][$row->STAFF_ID]['TIME_SPENT'] + $row->TIME_SPENT : $row->TIME_SPENT;
					
					$task_details['today'][$row->STAFF_ID][] = [
						'ID'          => $row->TASK_ID,
						'CLIENT_NAME' => str_replace("'", "|", $row->CUSTOMER_NAME),
						'SUBJECT'     => str_replace("'", "|", $row->SERVICE_NAME),
						'TIME_SPENT'  => $this->Task_creating_model->sec_to_time($row->TIME_SPENT)					
					];

					$analytics['today'][$row->STAFF_ID] = [
						'STAFF_ID'   => $row->STAFF_ID,
						'STAFF_NAME' => $row->STAFF_NAME,
						'TIME_SPENT' => $time_spent,
						'TASK'       => ['today' => $task_details['today'][$row->STAFF_ID]]
					];
				}
				# For yesterday's records
				if($row->FOR_DATE == $this->yesterday){					
					$time_spent = (isset($analytics['yesterday'][$row->STAFF_ID]['TIME_SPENT'])) ? $analytics['yesterday'][$row->STAFF_ID]['TIME_SPENT'] + $row->TIME_SPENT : $row->TIME_SPENT;
					
					$task_details['yesterday'][$row->STAFF_ID][] = [
						'ID'          => $row->TASK_ID,
						'CLIENT_NAME' => str_replace("'", "|", $row->CUSTOMER_NAME),
						'SUBJECT'     => str_replace("'", "|", $row->SERVICE_NAME),
						'TIME_SPENT'  => $this->Task_creating_model->sec_to_time($row->TIME_SPENT)					
					];

					$analytics['yesterday'][$row->STAFF_ID] = [
						'STAFF_ID'   => $row->STAFF_ID,
						'STAFF_NAME' => $row->STAFF_NAME,
						'TIME_SPENT' => $time_spent,
						'TASK'       => ['yesterday' => $task_details['yesterday'][$row->STAFF_ID]]
					];
				}
				# For this week's records
				if($row->FOR_DATE >= $this->this_week_start && $row->FOR_DATE <= $this->this_week_end){															
					$time_spent = (isset($analytics['this_week'][$row->STAFF_ID]['TIME_SPENT'])) ? $analytics['this_week'][$row->STAFF_ID]['TIME_SPENT'] + $row->TIME_SPENT : $row->TIME_SPENT;
					
					$task_details['this_week'][$row->STAFF_ID][] = [
						'ID'          => $row->TASK_ID,
						'CLIENT_NAME' => str_replace("'", "|", $row->CUSTOMER_NAME),
						'SUBJECT'     => str_replace("'", "|", $row->SERVICE_NAME),
						'TIME_SPENT'  => $this->Task_creating_model->sec_to_time($row->TIME_SPENT)					
					];

					$analytics['this_week'][$row->STAFF_ID] = [
						'STAFF_ID'   => $row->STAFF_ID,
						'STAFF_NAME' => $row->STAFF_NAME,
						'TIME_SPENT' => $time_spent,
						'TASK'       => ['this_week' => $task_details['this_week'][$row->STAFF_ID]]
					];
				}
				# For last week's records
				if($row->FOR_DATE >= $this->last_week_start && $row->FOR_DATE <= $this->last_week_end){															
					$time_spent = (isset($analytics['last_week'][$row->STAFF_ID]['TIME_SPENT'])) ? $analytics['last_week'][$row->STAFF_ID]['TIME_SPENT'] + $row->TIME_SPENT : $row->TIME_SPENT;
					
					$task_details['last_week'][$row->STAFF_ID][] = [
						'ID'          => $row->TASK_ID,
						'CLIENT_NAME' => str_replace("'", "|", $row->CUSTOMER_NAME),
						'SUBJECT'     => str_replace("'", "|", $row->SERVICE_NAME),
						'TIME_SPENT'  => $this->Task_creating_model->sec_to_time($row->TIME_SPENT)					
					];

					$analytics['last_week'][$row->STAFF_ID] = [
						'STAFF_ID'   => $row->STAFF_ID,
						'STAFF_NAME' => $row->STAFF_NAME,
						'TIME_SPENT' => $time_spent,
						'TASK'       => ['last_week' => $task_details['last_week'][$row->STAFF_ID]]
					];
				}
				# For this month's records					
				if($row->FOR_DATE >= $this->this_month_start && $row->FOR_DATE <= $this->this_month_end){															
					$time_spent = (isset($analytics['this_month'][$row->STAFF_ID]['TIME_SPENT'])) ? $analytics['this_month'][$row->STAFF_ID]['TIME_SPENT'] + $row->TIME_SPENT : $row->TIME_SPENT;
					
					$task_details['this_month'][$row->STAFF_ID][] = [
						'ID'          => $row->TASK_ID,
						'CLIENT_NAME' => str_replace("'", "|", $row->CUSTOMER_NAME),
						'SUBJECT'     => str_replace("'", "|", $row->SERVICE_NAME),
						'TIME_SPENT'  => $this->Task_creating_model->sec_to_time($row->TIME_SPENT)					
					];

					$analytics['this_month'][$row->STAFF_ID] = [
						'STAFF_ID'   => $row->STAFF_ID,
						'STAFF_NAME' => $row->STAFF_NAME,
						'TIME_SPENT' => $time_spent,
						'TASK'       => ['this_month' => $task_details['this_month'][$row->STAFF_ID]]
					];
				}
				# For last month's records					
				if($row->FOR_DATE >= $this->last_month_start && $row->FOR_DATE <= $this->last_month_end){															
					$time_spent = (isset($analytics['last_month'][$row->STAFF_ID]['TIME_SPENT'])) ? $analytics['last_month'][$row->STAFF_ID]['TIME_SPENT'] + $row->TIME_SPENT : $row->TIME_SPENT;
					
					$task_details['last_month'][$row->STAFF_ID][] = [
						'ID'          => $row->TASK_ID,
						'CLIENT_NAME' => str_replace("'", "|", $row->CUSTOMER_NAME),
						'SUBJECT'     => str_replace("'", "|", $row->SERVICE_NAME),
						'TIME_SPENT'  => $this->Task_creating_model->sec_to_time($row->TIME_SPENT)					
					];

					$analytics['last_month'][$row->STAFF_ID] = [
						'STAFF_ID'   => $row->STAFF_ID,
						'STAFF_NAME' => $row->STAFF_NAME,
						'TIME_SPENT' => $time_spent,
						'TASK'       => ['last_month' => $task_details['last_month'][$row->STAFF_ID]]
					];
				}				
			}					
			$analytics = $this->add_time_spent_and_contribution($analytics);			
		}		
		$this->load->view('LiveTeam/performance_analytics', ['data' => $analytics]);		
	}

	public function add_time_spent_and_contribution($analytics=[]){
		if(!empty($analytics)){
			$mod_analytics = [];
			
			foreach($analytics as $day=>$rec){
				foreach($rec as $row){		
					$time_spent = $row['TIME_SPENT'];			
					
					if($day == 'today')		 	  { $contribution = round($time_spent / $this->wpd * 100); 				   }		
					else if($day == 'yesterday')  { $contribution = round($time_spent / $this->wpd * 100); 				   }		
					else if($day == 'this_week')  { $contribution = round($time_spent / ($this->wpd * $this->dtwk) * 100); }		
					else if($day == 'last_week')  { $contribution = round($time_spent / ($this->wpd * $this->dlwk) * 100); }		
					else if($day == 'this_month') { $contribution = round($time_spent / ($this->wpd * $this->tm) * 100);   }		
					else if($day == 'last_month') { $contribution = round($time_spent / ($this->wpd * $this->lm) * 100);   }	
					
					$mod_analytics[$day][$row['STAFF_ID']] = [
						'STAFF_ID' 	   => $row['STAFF_ID'],
						'STAFF_NAME'   => $row['STAFF_NAME'],
						'TIME_SPENT'   => $this->Task_creating_model->sec_to_time($time_spent), 
						'CONTRIBUTION' => $contribution,
						'TASK' 		   => json_encode($row['TASK'][$day])
					];
				}					
			}
			$analytics = $mod_analytics;
		}
		return $analytics;
	}	

	public function working_days($date){		
		$fdate  = date('Y-m-01', strtotime($date));
		$ldate  = date('Y-m-t', strtotime($date));    
		$fday   = date('d', strtotime($fdate));        
		$lday   = date('d', strtotime($ldate));
		$month  = date('m', strtotime($fdate));    
		$year   = date('Y', strtotime($fdate));    
		$result = [];

		for($i=$fday; $i<=$lday; $i++){
			$dt    = $year.'-'.$month.'-'.$i;
			$date  = strtotime($dt);
			$date  = date("l", $date);
			$dl    = strtolower($date);
			$excpt = ['saturday', 'sunday'];        
			$dobj  = new DateTime($dt);
			$week  = $dobj->format("W");
	
			if(!in_array($dl, $excpt)){
				$result[$week] = isset($result[$week]) ? $result[$week]+1 : 1;  
			}        
		}    
		return $result;
	}	

	public function syncHistoricalTimelogs($days=90){
		# Clean the table
		$sql  = "TRUNCATE TABLE active_timers";
		$this->db->query($sql);

		# Collect historical data
		$sql  = "SELECT id,
						user_id,
						task_id,
						GROUP_CONCAT(time_start_pause) AS time_start_pause
				FROM individual_task_timer
				/*WHERE created_time >= UNIX_TIMESTAMP(DATE(NOW() - INTERVAL $days DAY))*/
				GROUP BY user_id, task_id";
		$logs = $this->db->query($sql)->result_array();
		
		# If historical data has been found, sync
		if(!empty($logs)){
			foreach($logs as $log){				
				$user_id 	  = $log['user_id'];
				$task_id 	  = $log['task_id'];
				$timer_chunks = array_chunk(explode(',', $log['time_start_pause']), 2);				
				
				foreach($timer_chunks as $timer){					
					$start_time = isset($timer[0])  ? $timer[0] : '';
					$end_time 	= isset($timer[1])  ? $timer[1] : NULL;
					$status 	= !isset($timer[1]) ? 1 		: 0;	
					
					if($start_time != ''){
						if($end_time == ''){
							$sql = "INSERT INTO active_timers (user_id, task_id, start_time, created, status) VALUES ($user_id, $task_id, $start_time, '".DATE('Y-m-d H:i:s', $start_time)."', $status)";						
						}else{
							$sql = "INSERT INTO active_timers (user_id, task_id, start_time, end_time, created, status) VALUES ($user_id, $task_id, $start_time, $end_time, '".DATE('Y-m-d H:i:s', $start_time)."', $status)";								
						}					
						$this->db->query($sql);	
					}		
				}			
			}			
		}
	}
} ?>