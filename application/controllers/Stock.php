<?php

	//session_start(); //we need to start session in order to access it through CI

	class Stock extends CI_Controller 
		{ 
			public function __construct()
				{  
		           parent::__construct();  
		           
		           // Load form helper library
					$this->load->helper('form');

					// Load form validation library
					$this->load->library('form_validation');

					// Load session library
					$this->load->library('session');

					// Load database
					
					$this->load->database();
					$this->load->model('Stock_model');
					
					date_default_timezone_set("UTC");
	            } 

			public function index()  
	      		{  	         
				
				    $data['category']=$this->Stock_model->get_category();
				    $data['supplier']=$this->Stock_model->get_supplier();
				    $data['records']=$this->Stock_model->getdata();
				    $this->load->view("stock_details",$data);
	      		}


	      	public function stock_information()  
	      		{  	



	    //   			$from=$this->input->post('start_from');
					// $to=$this->input->post('End_To'); 

					  $from_date=$this->input->post('start_from');
					 $to_date=$this->input->post('End_To'); 

			

					 $id=$this->input->post('catch_value');

			if(!empty($from_date && !empty($to_date)))
					 {

         	   //$date1 = DateTime::createFromFormat('m/d/Y', $from_date );
				 $from=strtotime($from_date);


				   //$date2 = DateTime::createFromFormat('m/d/Y', $to_date );
				 $to=strtotime($to_date);

					 	$purchase_data=$this->Stock_model->show_purchase($from,$to,$id);
					 	
					 	$sale_data=$this->Stock_model->show_sale($from,$to,$id);

					 	if(!empty($purchase_data))
					 	{
				foreach ($purchase_data as $address)
						{

			 	$data1[]=$this->Stock_model->fetch_supplier($address['invoice_number']);
					 }
					}

					if(!empty($sale_data))
					 	{
				foreach ($sale_data as $address)
						{

			 	$data2[]=$this->Stock_model->fetch_customer($address['invoiceNo']);
					 }
					}

					if(!empty($purchase_data))
					 	{
					 $data['purchase_data']=array_replace_recursive($purchase_data,$data1);
					}
					if(!empty($sale_data))
					 	{
					$data['sales_data']=array_replace_recursive($sale_data,$data2);
				}
					if(!empty($data))
				{

					$this->load->view('stock_info',$data);
				}
				else
				{
					$this->load->view('stock_info');

				}
				
			
					 }

				else
				{
					$purchase_data=$this->Stock_model->show_purchase_individual($id);
					$sale_data=$this->Stock_model->show_sale_individual($id);



					if(!empty($purchase_data))
					 	{
				foreach ($purchase_data as $address)
						{

			 	$data1[]=$this->Stock_model->fetch_supplier($address['invoice_number']);
					 }
					}

					if(!empty($sale_data))
					 	{
				foreach ($sale_data as $address)
						{

			 	$data2[]=$this->Stock_model->fetch_customer($address['invoiceNo']);
					 }
					}

					if(!empty($purchase_data))
					 	{
					 $data['purchase_data']=array_replace_recursive($purchase_data,$data1);
					 
					
					}
					if(!empty($sale_data))
					 	{
					$data['sales_data']=array_replace_recursive($sale_data,$data2);
				}
			
				if(!empty($data))
				{
					
					$this->load->view('stock_info',$data);
				}
				else
				{
					$this->load->view('stock_info');

				}
				
			

				} 


	 			} 




	 		// public function check_notifi()  
	   //    		{  	         
				

				//     $data['products']=$this->Stock_model->notifi_data();
				   
				//     echo json_encode($data);
	 		// 	} 


	 		public function set_noti()  
	      		{  	         
				

				    $this->Stock_model->set_noti();

				  $data['products']=$this->db->query("select * from category_store where onhand < 10 AND status=0 order by pcode asc")->result_array();
				   
				    echo json_encode($data);
	 			} 
	 			

	      		
		 //      		public function fromtofilter()  
	  //     			{  	
	  // 		// 		$from=$this->input->post('datepicker');
			// 		// $to=$this->input->post('datepicker_1'); 

	  //     				$from='03/01/2018';
	  //     				$to='03/06/2018';


			// 	$datas=$this->Stock_model->date_filter($from,$to);
			// 	$data=$this->Stock_model->date_filter1($from,$to);
				
			
			// 	if(!empty($datas) && !empty($data))
			// 	{
			// 	foreach ($datas as $address)
			// 	{

			// 	$data1[]=$this->Stock_model->from_to_filter($address['product_code'],$from,$to);
			// 	$data2[]=$this->Stock_model->from_to_filter1($address['product_code'],$from,$to);
			// 	$data3[]=$this->Stock_model->getonhand($address['product_code']);
			// 				}
			// 	$data4=$this->Stock_model->getallproductdetails();	
				
							
			// 		$product['product_records'] = array_replace_recursive( $data4,$data1,$data2,$data3);

			// 	$hold=$this->load->view('stock_table',$product,true);
		  		
			// 	echo json_encode(array( 'holdproduct' => $hold));
	  //     			}	


	  //     	else if(empty($data) && !empty($datas) )
	  //     		{
	  //     		foreach ($datas as $address)
			// 	{

	  //     		$data1[]=$this->Stock_model->from_to_filter($address['product_code'],$from,$to);
			// 	$data3[]=$this->Stock_model->getonhand($address['product_code']);
	  //     		}
	  //     		$data4=$this->Stock_model->getallproductdetails();	

	  //     		foreach($data4 as $key=>$value)
			// 	{

	  //     		  	if(isset($data1[$value['product_code']]))
   //     				 $value[$key]['purchase'] = $data1[$value['product_code']];
   //     				else

   // 					  $value[$key]['purchase'] =0;
			// }
	  //     		echo "<pre>";
			// 	print_r($data4);

			// 	// $product=array_merge_recursive($data4,$data1,$data3);
			// 	// print_r($product);
			// 	echo "</pre>";
			// 	exit;
	      			
			// 	foreach($data4 as $key=>$value)
			// 	{
			// 		foreach($data1 as $key1=>$value1)
			// 	{
				
			// 		if($value['product_code']==$data1[$key1]['product_code'])
			// 		{
			// 			$data4[$key]['purchase']=$data1[$key1]['product_quan'];
			// 		}
			// 		}
					 
			// 	}
				
				

			// 	foreach($data4 as $key=>$value)
			// 	{
			// 	foreach($data3 as $key1=>$value1)
			// 	{
			// 		if($value['product_code']==$data3[$key1]['pcode'])
			// 		{
			// 			$data4[$key]['onhand']=$data3[$key1]['onhand'];
			// 		}
			// 		}
					 
			// 	}

				

				
			// 	$product['product_details']=$data4;
			
			// 	$hold=$this->load->view('stock_table1',$product,true);
		  		
			// 	echo json_encode(array( 'holdproduct' => $hold));
			// 	}


	  //     		else if(!empty($data) && empty($datas) )
	  //     		{
	  //     		foreach ($data as $address)
			// 	{

	  //     		$data2[]=$this->Stock_model->from_to_filter1($address['isbn'],$from,$to);
			// $data3[]=$this->Stock_model->getonhand($address['isbn']);
	  //     		}
	  //     		$data4=$this->Stock_model->getallproductdetails();	
	      		
							
	      		
	      			
			// 	foreach($data4 as $key=>$value)
			// 	{
			// 		foreach($data2 as $key1=>$value1)
			// 		{
				
			// 		if($value['product_code']==$data2[$key1]['isbn'])
			// 		{
			// 			$data4[$key]['sale']=$data2[$key1]['quantity'];
			// 		}
					
			// 		}
					
					
			// 	}
					
				
				

			// 	foreach($data4 as $key=>$value)
			// 	{
			// 	foreach($data3 as $key1=>$value1)
			// 		{
			// 		if($value['product_code']==$data3[$key1]['pcode'])
			// 		{
			// 			$data4[$key]['onhand']=$data3[$key1]['onhand'];
			// 		}
					
			// 		}
					 
			// 	}
				
				

				
			// 	$product['product_details']=$data4;
			
			// 	$hold=$this->load->view('stock_table1',$product,true);
		  		
			// 	echo json_encode(array( 'holdproduct' => $hold));
			// 	}
			// 	else
			// 	{
			// 		echo json_encode(array( 'holdproduct' =>'<th></th><th></th><th></th><th>No Data Available</th><th></th><th></th>'));
			// 	}
				      		
	  //     		}  
	      		
	      		public function outputCSV() 
	      			{
    			
				$this->Stock_model->csv();
				redirect('Stock');
    				}
    				
    				
    // 			public function company_filter()  
	   //    			{  	         
				
				// $companyname=$this->input->post('company_sort');
	   //    		//$companyname="Techleaf Supplier";

				// $datas=$this->Stock_model->purchase_invoice_records($companyname);

				// if(!empty($datas))
				// {

				// foreach ($datas as $address)
				// 	{

				// 	$datas=$this->Stock_model->product_details($address['invoice_number']);

					
   
				// 	}


				// 	foreach ($datas as $address)
				// 	{

				// 	 $data1[]=$this->Stock_model->company_records($address['product_code'],$address['product_name']);
				// 	  $data2[]=$this->Stock_model->company_records1($address['product_code'],$address['product_name']);
				// 	  $data3[]=$this->Stock_model->getonhand($address['product_code']);
						 
				// 	 }
				// 	 	$data4=$this->Stock_model->getallproductdetails();	
				// 	 	$product['product_records'] = array_replace_recursive( $data4,$data1,$data2,$data3);
					 	
				

				// 	$hold=$this->load->view('stock_table',$product,true);
				// 	echo json_encode(array( 'holdproduct' => $hold));
				// 	}
				// else
				// {
				// 	echo json_encode(array( 'holdproduct' =>'<th></th><th></th><th></th><th>No Data Available</th><th></th><th></th>'));
				// }
				
	   //    		}  
	      			
	   //    		public function category_filter()  
	   //    		{  	         
				
				// $category_id=$this->input->post('category_sort');

				// $datas=$this->Stock_model->get_productdata($category_id);

				// if(!empty($datas))
				// {
				// foreach ($datas as $address)
				// 	{

			 //      	 $data1[]=$this->Stock_model->category_records($address['product_code']);

			 //      	 $data2[]=$this->Stock_model->category_records1($address['product_code']);
			 //      	  $data3[]=$this->Stock_model->getonhand($address['product_code']);


				// 	}

				// 	$data4=$this->Stock_model->getallproductdetails();	
				// 	 	$product['product_records'] = array_replace_recursive( $data4,$data1,$data2,$data3);
					
				// $hold=$this->load->view('stock_table',$product,true);

		  	
				// echo json_encode(array( 'holdproduct' => $hold));
				// }
				// else
				// {
				// 	echo json_encode(array( 'holdproduct' =>'<th></th><th></th><th></th><th>No Data Available</th><th></th><th></th>'));
				// }
	   //    		}
	      			
	      			
	      			
			public function outputExcel() 
	      			{
    			
			$filename = 'stock_details.xlsx';   
			header("Content-Description: File Transfer");
                	header("Content-Disposition: attachment; filename=$filename");
                	header("Content-Type: application/csv; "); 

              		
     			$data['refer_data']=$this->db->query("SELECT pcode,pname,quantity,shipped,onhand  FROM category_store")->result_array();


                   	 $file = fopen('php://output', 'w');        
      		 	 $header = array("Product Code","Product Name","Received","Shipped","OnHand");
                	 fputcsv($file, $header);
                	 $i=0;

        		 foreach ($data['refer_data'] as $key=>$line)
        			{

           			

                     	fputcsv($file,$line);
                    		}

                       
	                    fclose($file);
	                    exit;
        
    				}
    				
    			
    // 			public function date_filter()  
	   //    			{  	         
				
				// $date=$this->input->post('datepicker2');
			 //   	$data['product_records']=$this->Stock_model->datewise_getdata($date);
				// if(!empty($data))
				// {

				// $hold=$this->load->view('stock_table',$data,true);
		  		
				// echo json_encode(array( 'holdproduct' => $hold));
	   //    			} 
	   //    		else
				// {
				// echo json_encode(array( 'holdproduct' =>'<th></th><th></th><th></th><th>No Data Available</th><th></th><th></th>'));
				// }
	   //    			} 		      		
	      		public function pdf1()

	      			{
	      			$data['records']=$this->Stock_model->getdata();
	      			$this->load->library('m_pdf');
				$pdf = $this->m_pdf->load();
					
           		   	 $html = $this->load->view('pdf_view', $data, true);
            			$this->load->view('pdf_view');
			        $pdfFilePath ="mypdfName-".time()."-download.pdf";
			        $pdf = $this->m_pdf->load();
			       	$stylesheet = file_get_contents('css/table.css');
			        $pdf->WriteHTML($stylesheet,1);
			        $pdf->WriteHTML($html,2);
			        $pdf->Output($pdfFilePath, "D");
			        redirect('Stock');
	      			}


	      		
	      		
	      			      		
	      		


	     	 public function common_filter()  
	      		{  	
	     //  		
	  				 $from_date=$this->input->post('datepicker');
					 $to_date=$this->input->post('datepicker_1'); 

	
				
				 $from= strtotime($from_date);
				// echo $from;
				 $to= strtotime($to_date);


					 $category_id=$this->input->post('category_sort');
					 $companyname=$this->input->post('company_sort');


//Start All Sorting

			if(!empty($from) && !empty($to) && !empty($category_id)&& !empty($companyname))
					 {


				$company_datas=$this->Stock_model->common_purchase($companyname,$category_id);


				if(!empty($company_datas))
				{
				foreach ($company_datas as $address)
				{

				$from_data[]=$this->Stock_model->common_date_filter($from,$to,$address['product_code']);
				
				$to_data[]=$this->Stock_model->common_date_filter1($from,$to,$address['product_code']);
				 $data3[]=$this->Stock_model->getonhand($address['product_code']);

				}
				$data4=$this->Stock_model->getallproductdetails();

			foreach($data4 as $key=>$value)
				{
					foreach($to_data as $key1=>$value1)
				{
				
					if($value['product_code']==$to_data[$key1]['isbn'])
					{
						$data4[$key]['sale']=$to_data[$key1]['quantity'];
					}
					}
					 
				}
				foreach($data4 as $key=>$value)
				{
				foreach($data3 as $key1=>$value1)
					{
					if($value['product_code']==$data3[$key1]['pcode'])
					{
						$data4[$key]['onhand']=$data3[$key1]['onhand'];
					}
					
					}
					 
				}
				foreach($data4 as $key=>$value)
				{
				foreach($from_data as $key1=>$value1)
					{
					if($value['product_code']==$from_data[$key1]['product_code'])
					{
						$data4[$key]['purchase']=$from_data[$key1]['product_quan'];
					}
					
					}
					 
				}

				 $product['product_details'] = $data4;

				 	$hold=$this->load->view('stock_table1',$product,true);
					echo json_encode(array( 'holdproduct' => $hold));


				
				}
				else
				{
						echo json_encode(array( 'holdproduct' =>'<th></th><th></th><th></th><th>No Data Available</th><th></th><th></th>'));
				}
				}
// END All  Sorting

// Start  date & category sort
				elseif(!empty($from) && !empty($to) && !empty($category_id) && empty($companyname) )
				{

					
					$datas=$this->Stock_model->get_productdata($category_id);

					if(!empty($datas))
				{
				foreach ($datas as $address)
				{

				$from_data[]=$this->Stock_model->common_date_filter($from,$to,$address['product_code']);
				
				$to_data[]=$this->Stock_model->common_date_filter1($from,$to,$address['product_code']);
				 $data3[]=$this->Stock_model->getonhand($address['product_code']);

				}
				$data4=$this->Stock_model->getallproductdetails();

			foreach($data4 as $key=>$value)
				{
					foreach($to_data as $key1=>$value1)
				{
				
					if($value['product_code']==$to_data[$key1]['isbn'])
					{
						$data4[$key]['sale']=$to_data[$key1]['quantity'];
					}
					}
					 
				}
				foreach($data4 as $key=>$value)
				{
				foreach($data3 as $key1=>$value1)
					{
					if($value['product_code']==$data3[$key1]['pcode'])
					{
						$data4[$key]['onhand']=$data3[$key1]['onhand'];
					}
					
					}
					 
				}
				foreach($data4 as $key=>$value)
				{
				foreach($from_data as $key1=>$value1)
					{
					if($value['product_code']==$from_data[$key1]['product_code'])
					{
						$data4[$key]['purchase']=$from_data[$key1]['product_quan'];
					}
					
					}
					 
				}

				 $product['product_details'] = $data4;

				 	$hold=$this->load->view('stock_table1',$product,true);
				 	 
					echo json_encode(array( 'holdproduct' => $hold));

				}
				else
				{
						echo json_encode(array( 'holdproduct' =>'<th></th><th></th><th></th><th>No Data Available</th><th></th><th></th>'));
				}
			     		
	      		}
//End Date and category sort
	      		//Start Date and company sort
	      		elseif(!empty($from) && !empty($to)  && !empty($companyname)&& empty($category_id))
	      		{
	      			$datas=$this->Stock_model->fetch_company_records($companyname);
	      				if(!empty($datas))
				{
				foreach ($datas as $address)
				{

				$from_data[]=$this->Stock_model->common_date_filter($from,$to,$address['product_code']);
				
				$to_data[]=$this->Stock_model->common_date_filter1($from,$to,$address['product_code']);
				 $data3[]=$this->Stock_model->getonhand($address['product_code']);

				}
				$data4=$this->Stock_model->getallproductdetails();

			foreach($data4 as $key=>$value)
				{
					foreach($to_data as $key1=>$value1)
				{
				
					if($value['product_code']==$to_data[$key1]['isbn'])
					{
						$data4[$key]['sale']=$to_data[$key1]['quantity'];
					}
					}
					 
				}
				foreach($data4 as $key=>$value)
				{
				foreach($data3 as $key1=>$value1)
					{
					if($value['product_code']==$data3[$key1]['pcode'])
					{
						$data4[$key]['onhand']=$data3[$key1]['onhand'];
					}
					
					}
					 
				}
				foreach($data4 as $key=>$value)
				{
				foreach($from_data as $key1=>$value1)
					{
					if($value['product_code']==$from_data[$key1]['product_code'])
					{
						$data4[$key]['purchase']=$from_data[$key1]['product_quan'];
					}
					
					}
					 
				}

				 $product['product_details'] = $data4;

				 	$hold=$this->load->view('stock_table1',$product,true);
				 	
					echo json_encode(array( 'holdproduct' => $hold));

				}
				else
				{
						echo json_encode(array( 'holdproduct' =>'<th></th><th></th><th></th><th>No Data Available</th><th></th><th></th>'));
				}


	      		}
//End Date and company sort
	      		////Start category and company sort
	      		elseif(!empty($companyname) && !empty($category_id) && empty($from) && empty($to) ) 
	      		{
	      			$datas=$this->Stock_model->common_purchase($companyname,$category_id);


	      			if(!empty($datas))
	      			{

	      			foreach ($datas as $address)
					{

					 $data1[]=$this->Stock_model->company_records($address['product_code'],$address['product_name']);
					  $data2[]=$this->Stock_model->company_records1($address['product_code'],$address['product_name']);
					  $data3[]=$this->Stock_model->getonhand($address['product_code']);
						 
					 }


					 	$data4=$this->Stock_model->getallproductdetails();	
					 	foreach($data4 as $key=>$value)
				{
					foreach($data2 as $key1=>$value1)
				{
				
					if($value['product_code']==$data2[$key1]['isbn'])
					{
						$data4[$key]['sale']=$data2[$key1]['quantity'];
					}
					}
					 
				}
				foreach($data4 as $key=>$value)
				{
				foreach($data3 as $key1=>$value1)
					{
					if($value['product_code']==$data3[$key1]['pcode'])
					{
						$data4[$key]['onhand']=$data3[$key1]['onhand'];
					}
					
					}
					 
				}
				foreach($data4 as $key=>$value)
				{
				foreach($data1 as $key1=>$value1)
					{
					if($value['product_code']==$data1[$key1]['product_code'])
					{
						$data4[$key]['purchase']=$data1[$key1]['product_quan'];
					}
					
					}
					 
				}
						 $product['product_details'] = $data4;
				 	$hold=$this->load->view('stock_table1',$product,true);
				 	
					echo json_encode(array( 'holdproduct' => $hold));

				}
				else
				{
						echo json_encode(array( 'holdproduct' =>'<th></th><th></th><th></th><th>No Data Available</th><th></th><th></th>'));
				}

	      		} 
//End Category and company sort
	      		//Start company sort
	      		elseif(!empty($companyname) && empty($category_id) && empty($from) && empty($to) )
	      		{
	      			$datas=$this->Stock_model->fetch_company_records($companyname);
	      			if(!empty($datas))
	      			{

	      			foreach ($datas as $address)
					{

					 $data1[]=$this->Stock_model->company_records($address['product_code'],$address['product_name']);
					  $data2[]=$this->Stock_model->company_records1($address['product_code'],$address['product_name']);
					  $data3[]=$this->Stock_model->getonhand($address['product_code']);
						 
					 }


					 	$data4=$this->Stock_model->getallproductdetails();	
					 	foreach($data4 as $key=>$value)
						{
					foreach($data2 as $key1=>$value1)
						{
				
					if($value['product_code']==$data2[$key1]['isbn'])
					{
						$data4[$key]['sale']=$data2[$key1]['quantity'];
					}
					}
					 
				}
				foreach($data4 as $key=>$value)
				{
				foreach($data3 as $key1=>$value1)
					{
					if($value['product_code']==$data3[$key1]['pcode'])
					{
						$data4[$key]['onhand']=$data3[$key1]['onhand'];
					}
					
					}
					 
				}
				foreach($data4 as $key=>$value)
				{
				foreach($data1 as $key1=>$value1)
					{
					if($value['product_code']==$data1[$key1]['product_code'])
					{
						$data4[$key]['purchase']=$data1[$key1]['product_quan'];
					}
					
					}
					 
				}
						 $product['product_details'] = $data4;

				 	$hold=$this->load->view('stock_table1',$product,true);
				 	
					echo json_encode(array( 'holdproduct' => $hold));

				}
				else
				{
						echo json_encode(array( 'holdproduct' =>'<th></th><th></th><th></th><th>No Data Available</th><th></th><th></th>'));
				}


	      		}
//End company sort
	      		//Start Category sort
	      	elseif(!empty($category_id) && empty($from) && empty($to) && empty($companyname))
	      	{
	      		$category_id=$this->input->post('category_sort');

				$datas=$this->Stock_model->get_productdata($category_id);

				if(!empty($datas))
				{
				foreach ($datas as $address)
					{

			      	 $data1[]=$this->Stock_model->category_records($address['product_code']);

			      	 $data2[]=$this->Stock_model->category_records1($address['product_code']);
			      	  $data3[]=$this->Stock_model->getonhand($address['product_code']);


					}

					$data4=$this->Stock_model->getallproductdetails();	
						foreach($data4 as $key=>$value)
						{
					foreach($data2 as $key1=>$value1)
						{
				
					if($value['product_code']==$data2[$key1]['isbn'])
					{
						$data4[$key]['sale']=$data2[$key1]['quantity'];
					}
					}
					 
				}
				foreach($data4 as $key=>$value)
				{
				foreach($data3 as $key1=>$value1)
					{
					if($value['product_code']==$data3[$key1]['pcode'])
					{
						$data4[$key]['onhand']=$data3[$key1]['onhand'];
					}
					
					}
					 
				}
				foreach($data4 as $key=>$value)
				{
				foreach($data1 as $key1=>$value1)
					{
					if($value['product_code']==$data1[$key1]['product_code'])
					{
						$data4[$key]['purchase']=$data1[$key1]['product_quan'];
					}
					
					}
					 
				}
				 $product['product_details'] = $data4;
				 	$hold=$this->load->view('stock_table1',$product,true);
				 	
					echo json_encode(array( 'holdproduct' => $hold));

				}
				else
				{
						echo json_encode(array( 'holdproduct' =>'<th></th><th></th><th></th><th>No Data Available</th><th></th><th></th>'));
				}

	      		}
//End Category sort
	      			//start From - to  sort

	      		elseif(!empty($from) && !empty($to) && empty($category_id) &&  empty($companyname))
	      		{
	      			$datas=$this->Stock_model->date_filter($from,$to);
				$data=$this->Stock_model->date_filter1($from,$to);



				
			
				if(!empty($datas) && !empty($data))
				{
				foreach ($datas as $address)
				{

				$data1[]=$this->Stock_model->from_to_filter($address['product_code'],$from,$to);
				//$data2[]=$this->Stock_model->from_to_filter1($address['product_code'],$from,$to);
				$data3[]=$this->Stock_model->getonhand($address['product_code']);
							}
				foreach ($data as $address)
				{

				//$data1[]=$this->Stock_model->from_to_filter($address['product_code'],$from,$to);
				$data2[]=$this->Stock_model->from_to_filter1($address['isbn'],$from,$to);
				$data5[]=$this->Stock_model->getonhand($address['isbn']);
							}
				$data4=$this->Stock_model->getallproductdetails();	

				$purchase_product = array_replace_recursive($data1,$data3);
				$sale_product = array_replace_recursive($data2,$data5);
				

				foreach($data4 as $key=>$value)
				{
				foreach($purchase_product as $key1=>$value1)
					{
					if($value['product_code']==$purchase_product[$key1]['product_code'])
					{
						$data4[$key]['product_quan']=$purchase_product[$key1]['product_quan'];
						$data4[$key]['onhand']=$purchase_product[$key1]['onhand'];
					}
					
					}
					 
				}

					foreach($data4 as $key=>$value)
				{
				foreach($sale_product as $key1=>$value1)
					{
					if($value['product_code']==$sale_product[$key1]['isbn'])
					{
						$data4[$key]['quantity']=$sale_product[$key1]['quantity'];
						$data4[$key]['onhand']=$sale_product[$key1]['onhand'];
					}
					
					}
					 
				}

					foreach ($data4 as $key => $value){
					    if (empty($data4[$key]['onhand'])) {
					        unset($data4[$key]);
					    }
					}
  					

							
				$product['product_records'] = $data4;


					// print_r($data4);
					 
					//  // print_r($data3);
					//  // print_r($data5);
					//  exit;
					// // // print_r($data4);
					// // exit;

				$hold=$this->load->view('stock_table',$product,true);
		  		
				echo json_encode(array( 'holdproduct' => $hold));
	      			}	


	      	else if(empty($data) && !empty($datas) )
	      		{
	      		foreach ($datas as $address)
				{

	      		$data1[]=$this->Stock_model->from_to_filter($address['product_code'],$from,$to);
				$data3[]=$this->Stock_model->getonhand($address['product_code']);
	      		}
	      		$data4=$this->Stock_model->getallproductdetails();	
				
	      		
	      			
				foreach($data4 as $key=>$value)
				{
					foreach($data1 as $key1=>$value1)
				{
				
					if($value['product_code']==$data1[$key1]['product_code'])
					{
						$data4[$key]['purchase']=$data1[$key1]['product_quan'];
					}
					}
					 
				}
				
				

				foreach($data4 as $key=>$value)
				{
				foreach($data3 as $key1=>$value1)
				{
					if($value['product_code']==$data3[$key1]['pcode'])
					{
						$data4[$key]['onhand']=$data3[$key1]['onhand'];
					}
					}
					 
				}

				

				
				$product['product_details']=$data4;
			
				$hold=$this->load->view('stock_table1',$product,true);
		  		
				echo json_encode(array( 'holdproduct' => $hold));
				}


	      		else if(!empty($data) && empty($datas) )
	      		{
	      		foreach ($data as $address)
				{

	      		$data2[]=$this->Stock_model->from_to_filter1($address['isbn'],$from,$to);
			$data3[]=$this->Stock_model->getonhand($address['isbn']);
	      		}
	      		$data4=$this->Stock_model->getallproductdetails();	
	      		
							
	      		
	      			
				foreach($data4 as $key=>$value)
				{
					foreach($data2 as $key1=>$value1)
					{
				
					if($value['product_code']==$data2[$key1]['isbn'])
					{
						$data4[$key]['sale']=$data2[$key1]['quantity'];
					}
					
					}
					
					
				}
					
			
				foreach($data4 as $key=>$value)
				{
				foreach($data3 as $key1=>$value1)
					{
					if($value['product_code']==$data3[$key1]['pcode'])
					{
						$data4[$key]['onhand']=$data3[$key1]['onhand'];
					}
					
					}
					 
				}
				
				
				$product['product_details']=$data4;
			
				$hold=$this->load->view('stock_table1',$product,true);
		  		
				echo json_encode(array( 'holdproduct' => $hold));
				}
				else
				{
					echo json_encode(array( 'holdproduct' =>'<th></th><th></th><th></th><th>No Data Available</th><th></th><th></th>'));
				}
	      		}
//End from-to sort
	      else
	      {
	      	echo 0;

	      	}

	      	}	
	      		
	      			      		
	    }
?>