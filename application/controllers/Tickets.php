<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tickets extends CI_Controller {
public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
	public function __construct() {
		parent::__construct();
		$this->load->model(array('Common_mdl','Security_model','Loginchk_model'));
		 $this->load->helper(array("cookie"));
	}

	public function index()
	{
	//print_r($_COOKIE);die;

		if(isset($_COOKIE['remindoo_logout']) && $_COOKIE['remindoo_logout']=='remindoo_superadmin') 
        {
           $this->Loginchk_model->superAdmin_LoginCheck();
        }
        else if($_SESSION['user_type']=='FC')
        {
        	$this->Security_model->check_login();
        }
        else
        {
           $this->Security_model->chk_login();
        } 
		// $this->Security_model->chk_login();
		
		/*if($id = $this->session->userdata('admin_id'))
		{*/
			//$data['tickets'] = $this->Common_mdl->select_record('user', 'id', $_SESSION['id']);	
			//print_r($data); exit();
			$data['modules']= $this->db->query('select * from module_mangement')->result_array();
			if($_SESSION['permission']['Tickets']['view']==1){
				$this->load->view('tickets/tickets_list', $data);	
			}
			else
			{
		           $this->load->view('users/blank_page',$data); 
		        }
							
		/*}*/		
		
	}

	public function addTicket()
	{		
      /*$User_name = $_POST['userName'];
		$User_email = $_POST['emailID'];
		
		*/
		if(isset($_COOKIE['remindoo_logout']) && $_COOKIE['remindoo_logout']=='remindoo_superadmin') 
        {
           $this->Loginchk_model->superAdmin_LoginCheck();
        }
         else if($_SESSION['user_type']=='FC')
        {
        	$this->Security_model->check_login();
        }
        else
        {
           $this->Security_model->chk_login();
        } 

		//$this->Security_model->chk_login();
		
		$subject = $_POST['subject'];
		$help_topic = $_POST['help_topic'];
		$message = $_POST['message'];
		$email = $_POST['email'];

		$resprsn_name = $_POST['cname']; //display name name of resperson
		$company = $_POST['trouble_company'];		
		
		$module = isset($_POST['module']) ? $_POST['module'] : ''; 
		$phone =isset($_POST['phone']) ? $_POST['phone'] : ''; 
		//$dept = isset($_POST['dept']) ? $_POST['dept'] : '';
		$onprogress = 'Open';
		$priority = $_POST['priority'];
		$ticket_no =  "TKT".mt_rand(100000, 999999);

		if($_SESSION['user_type'] == 'FA')
		{
			$active = 'Admin';
		}
		else
		{
			$active = 'Client';
		}
		   // print_r($status); exit();		
			$file1 = $this->Common_mdl->do_upload_option($_FILES['file1'],'uploads/tickets');
	
			$file2 = $this->Common_mdl->do_upload_option($_FILES['file2'],'uploads/tickets');	
		
		$data = array(	'ticket_no' => $ticket_no,
			 			'user_id' => $_SESSION['id'],
						'subject' => $subject,
						'help_topic' => $help_topic,
						'responsible_person' => $_POST['responsible_person'],
						'username' => $resprsn_name,
						'company' => $company,
						'email' => $email, 
						'module' => $module,
						'phone' => $phone,
						//'dept' => $dept,
						'firm_id'=>$_SESSION['firm_id'],
						'message' => $message,
						'file1' => $file1,
						'file2' => $file2,
						'Active' => $active,
						'status' => $onprogress,
						'priority' => $priority,
						'created_at' => time()
				);
		//print_r($data); exit();
		$result = $this->Common_mdl->insert('tickets', $data);


		if(isset($result) && !empty($_POST['email']))
		{  
			
				$data1['username'] = $_POST['cname'];
				$data1['email'] = $_POST['email'];
				$data1['subject'] = $_POST['subject'];
				$data1['company'] = $_POST['trouble_company'];
	            $data1['message'] = $_POST['message'];
	           // $data1['department'] = $_POST['dept'];
	            $data1['priority'] = $_POST['priority'];
	            $data1['ticket_no'] = $ticket_no;

				$body = $this->load->view('tickets/ticket_remainder_email.php', $data1, TRUE);
				$this->load->library('email');
				$this->email->set_mailtype('html');
				$this->email->from('info@remindoo.org');
				$this->email->to($_POST['email']);
				$this->email->subject('New Ticket Notification.');
				$this->email->message($body);
				if(isset($file1) && $file1!=''){
					$file1=FCPATH.'uploads/tickets/'.$file1;
					//$this->email->attach(FCPATH.'attachment/'.$attaching_image[$i]);
					//echo $file1;
				   $this->email->attach($file1);
			    }
			    if(isset($file2) && $file2!=''){
			    	$file2=FCPATH.'uploads/tickets/'.$file2;
					//echo $file2;
				   //exit;
			    	$this->email->attach($file2);
			    }
				
				echo $this->email->send();
		}
		
	}

	public function EditTickets()
	{
		if(isset($_COOKIE['remindoo_logout']) && $_COOKIE['remindoo_logout']=='remindoo_superadmin') 
        {
           $this->Loginchk_model->superAdmin_LoginCheck();
        }
         else if($_SESSION['user_type']=='FC')
        {
        	$this->Security_model->check_login();
        }
        else
        {
           $this->Security_model->chk_login();
        } 
		// $this->Security_model->chk_login();
	
		$Ses_id = $_POST['Sesid_id'];
		$spt_id = $_POST['edit_id'];
		$subject = $_POST['subject'];
		$help_topic = $_POST['help_topic'];
		$username = $_POST['cname'];
		$company = $_POST['company'];
		$email = $_POST['email'];
		$module = $_POST['module'];
		$phone = $_POST['phone'];
		//$dept = $_POST['dept'];
        $onprogress = $_POST['on_progress'];
		$message = $_POST['message'];
		$priority = $_POST['priority'];
		if($_FILES['file1']['name'])
		{
			$file1 = $this->Common_mdl->do_upload_option($_FILES['file1'],'uploads/tickets');				
		} 
		else 
		{
			$file1 = $_POST['file1_edit'];			
		}
		
		if($_FILES['file2']['name'])
		{
			$file2 = $this->Common_mdl->do_upload_option($_FILES['file2'],'uploads/tickets');	
		} 
		else
		{
			$file2 = $_POST['file2_edit'];
		}
	
		$data = array(
						// 'user_id' => $Ses_id,
						'subject' => $subject,
						'help_topic' => $help_topic,
						'username' => $username,
						'company' => $company,
						'email' => $email,
						'module' => $module,
						'phone' => $phone,
						//'dept' => $dept,
						'message' => $message,
						'file1' => $file1,
						'file2' => $file2,
						/*'Active' => 'Admin',*/
						'priority' => $priority,
						'status' => $onprogress,
						'updated_at' => time()
				);
		
		$result = $this->Common_mdl->updateFields('tickets', 'id', $spt_id, $data);
		echo $this->db->affected_rows();	
		
	}

	public function DeleteSupportTicket($id, $newID = false)
	{
		// $sql = $this->Common_mdl->deleteFields('tickets', 'id', $id, 'user_id', $newID);
		$sql = $this->Common_mdl->deleteFields('tickets', 'id', $id);
		if(isset($sql))
		{
			redirect('Tickets/index');
		 // print($sql);  exit();
		}
	}

	public function EditClientTickets()
	{
		$User_name = $_POST['userName'];
		$User_email = $_POST['emailID'];

		$SesID = $_POST['session_id'];
		$ctID = $_POST['editClient_id'];
		$subject = $_POST['subject'];
		$help_topic = $_POST['help_topic'];
		$uname = $_POST['cname'];
		$company = $_POST['company'];
		$email = $_POST['email'];
		$module = $_POST['module'];
		$phone = $_POST['phone'];
		$dept = $_POST['dept'];
        $onprogress = $_POST['on_progress'];
		$message = $_POST['message'];
		
		if($_FILES['file1']['name'])
		{
			$file1 = $this->Common_mdl->do_upload_option($_FILES['file1'],'uploads/tickets');	
		} else {
			$file1 = $_POST['file11_edit'];
		}
		
		if($_FILES['file2']['name'])
		{
			$file2 = $this->Common_mdl->do_upload_option($_FILES['file2'],'uploads/tickets');	
		} else {
			$file2 = $_POST['file22_edit'];
		}	

		$data = array('id' => $ctID,
						// 'user_id' => $SesID,
						'subject' => $subject,
						'help_topic' => $help_topic,
						'username' => $uname,
						'company' => $company,
						'email' => $email,
						'module' => $module,
						'phone' => $phone,
						'dept' => $dept,
						'message' => $message,
						'file1' => $file1,
						'file2' => $file2,
						'Active' => 'Client',
						'status' => $onprogress,
						'updated_at' => time()
				);
		// print_r($data); exit();
		$result = $this->Common_mdl->updateFields('tickets', 'id', $ctID, $data);
		if(isset($result))
		{
			$data1['userName'] = $_POST['userName'];
			$data1['UserEmail'] = $_POST['emailID'];
			$data1['username'] = $_POST['cname'];
			$data1['email'] = $_POST['email'];
			$data1['subject'] = $_POST['subject'];
			$data1['company'] = $_POST['company'];
            $data1['message'] = $_POST['message'];

            // $data1['status'] = $_POST['on_progress'];             
			$body = $this->load->view('tickets/ticket_remainder_email.php', $data1, TRUE);
			$this->load->library('email');
			$this->email->set_mailtype('html');
			$this->email->from('info@remindoo.org');
			$this->email->to($_POST['emailID']);
			$this->email->subject($_POST['subject']);
			$this->email->message($body);
			$send = $this->email->send();

			$this->session->set_flashdata('successCtEdit', '<div class="modal-alertsuccess alert alert-success">
                       <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                       <div class="pop-realted1">
                        <div class="position-alert1">
                        <strong>Success!</strong> Client Ticket have been Updated Successfully.</div>
                        </div></div>');	

			redirect('Tickets/index');
		}
	}

	public function DeleteClientTicket($id, $newID = false)
	{
		// $sql = $this->Common_mdl->deleteFields('tickets', 'id', $id, 'user_id', $newID);
		$sql = $this->Common_mdl->deleteFields('tickets', 'id', $id);
		if(isset($sql))
		{
			redirect('Tickets/index');
		 // print($sql);  exit();
		}
	}

	public function get_modulename()
	{
		// print_r($_POST);
		// exit;
		// $sql = $this->Common_mdl->deleteFields('tickets', 'id', $id, 'user_id', $newID);
		$sql = $this->Common_mdl->select_record('module_table', 'id', $_POST['stModule']);
		// print_r($sql);
		// exit;

		echo json_encode($sql['module']);
		
	}



	
	public function StatusCheck()
	{
		$stId = $_POST['stId'];
		//$IDSes = $_POST['IDSes'];
		$selectValue = $_POST['selectValue'];
		$stUname = $_POST['stUname'];
		$stEmail = $_POST['stEmail'];
		$stSub = $_POST['stSub'];
		$stCmy = $_POST['stCmy'];
		$stTicketUname = $_POST['stTicketUname'];
		$stTicketEmail = $_POST['stTicketEmail'];
 		
 		$old_status=$this->db->query("select * from tickets where id=".$stId." ")->row_array();

		$sql = $this->Common_mdl->UpdateStatus('tickets', 'id', $stId, 'status', $selectValue);

		if($selectValue=="Archive" && $old_status['status']!="Archive")
		{
		if($sql)$this->db->query("update tickets set old_status='".$old_status['status']."' where id=".$stId." ");
		}


		/*if(isset($sql))
		{
			$data2['username'] = $stUname;
			$data2['email'] = $stEmail;
			$data2['subject'] = $stSub;
			$data2['company'] = $stCmy;
            $data2['status'] = $selectValue;
			$body = $this->load->view('tickets/ticket_remainder_email.php', $data2, TRUE);
			$this->load->library('email');
			$this->email->set_mailtype('html');
			$this->email->from($_POST['stTicketEmail']);
			$this->email->to($_POST['stEmail']);
			$this->email->subject($_POST['stSub']);
			$this->email->message($body);
			$send = $this->email->send();
		}*/
	}

	public function Status_change()
	{ 

		if(isset($_COOKIE['remindoo_logout']) && $_COOKIE['remindoo_logout']=='remindoo_superadmin') 
        {
           $this->Loginchk_model->superAdmin_LoginCheck();
        }
         else if($_SESSION['user_type']=='FC')
        {
        	$this->Security_model->check_login();
        }
        else
        {
           $this->Security_model->chk_login();
        } 

		//$this->Security_model->chk_login();

		$ctId = $_POST['ctId'];
		//$IDSes = $_POST['IDSes'];
		$selectVal = $_POST['selectVal'];
		$ids = explode(",", $ctId);
		foreach ($ids as $tid) 
		{				
	 		$old_status=$this->db->query("select * from tickets where id=".$tid." ")->row_array();
			
			$sql1 = $this->Common_mdl->UpdateStatus('tickets', 'id', $tid, 'status', $selectVal);
			
			if($selectVal=="Archive" && $old_status['status']!="Archive")
			{
				if($sql1)$this->db->query("update tickets set old_status='".$old_status['status']."' where id=".$tid." ");
			}
			if($sql1)
			{
				$this->StatusChange_notification($old_status,$selectVal);
			}	
		}
		
		
	}
	public function StatusChange_notification($old_ticket,$status)
	{
		// print_r($old_ticket);
		// exit;
			if($old_ticket['Active'] == 'Admin' )
			{
					$genrater_details =  $this->db->query("select crm_email_id as mail from user where id = ".$old_ticket['user_id']." ")->row_array();
			
			}
			else
			{
					//$genrater_details =  $this->db->query("select main_email as mail from client_contacts where client_id = ".$old_ticket['user_id']." and make_primary=1 ")->row_array();

				$genrater_details =  $this->db->query("select crm_email_id as mail from user where id = ".$old_ticket['user_id']." ")->row_array();
			
			}


			if(!empty($genrater_details['mail']))
			{
				$data2['username'] = $this->Common_mdl->getUserProfileName($old_ticket['user_id']);			
	            $data2['status'] = $status;
	            $data2['ticket_no'] = $old_ticket['ticket_no'];

				$body = $this->load->view('tickets/statusChange_notification', $data2, TRUE);
				$this->load->library('email');
				$this->email->set_mailtype('html');
				$this->email->from('info@remindoo.org');
				$this->email->to($genrater_details['mail']);
				$this->email->subject('Ticket Notification');
				$this->email->message($body);
				$send = $this->email->send();
				return $send;
			}

		//echo $genrater_details['mail'];	
		
	}
	public function CheckClientDet()
	{
		$crmName = $_POST['selectClientVal'];
		$data = $this->db->query("SELECT * FROM client WHERE crm_other_username='$crmName'")->row_array();
		if($data != '')
		//{	print_r($data);
			echo json_encode($data);
		//	return true;
			// $this->load->view("tickets_list", $data);
		
	}
	public function status_counts($ticket_type)
	{
		if($ticket_type == 'Admin')
		{
			$data=$this->db->query("select status from tickets WHERE user_id= ".$_SESSION['id']." ")->result_array();
			$data = array_column($data,'status');
			echo json_encode( array_count_values($data) );
		}
		else
		{
			$data=$this->db->query("select status from tickets WHERE responsible_person= ".$_SESSION['id']." ")->result_array(); 
			$data = array_column($data,'status');
			echo json_encode( array_count_values($data) );
		}

	}

	public function deleteTickets()
    {
    	if(isset($_COOKIE['remindoo_logout']) && $_COOKIE['remindoo_logout']=='remindoo_superadmin') 
        {
           $this->Loginchk_model->superAdmin_LoginCheck();
        }
         else if($_SESSION['user_type']=='FC')
        {
        	$this->Security_model->check_login();
        }
        else
        {
           $this->Security_model->chk_login();
        } 
		//$this->Security_model->chk_login();

      if(isset($_POST['data_ids']))
      {
          $id = trim($_POST['data_ids']);
          $sql = $this->db->query("DELETE FROM tickets WHERE id in ($id)");

          		$username=$this->Common_mdl->getUserProfileName($_SESSION['id']); 
                $activity_datas['log'] = "Tickets Delted by " .$username;
                $activity_datas['createdTime'] = time();
                $activity_datas['module'] = 'Ticket';
                $activity_datas['sub_module']='';
                $activity_datas['user_id'] = $_SESSION['id'];
                $activity_datas['module_id'] = $id;
                $this->Common_mdl->insert('activity_log',$activity_datas);

          echo $this->db->affected_rows();
      }  
    }
    public function status_filter()
    {
    	if(isset($_COOKIE['remindoo_logout']) && $_COOKIE['remindoo_logout']=='remindoo_superadmin') 
        {
           $this->Loginchk_model->superAdmin_LoginCheck();
        }
         else if($_SESSION['user_type']=='FC')
        {
        	$this->Security_model->check_login();
        }
        else
        {
           $this->Security_model->chk_login();
        } 
    	//$this->Security_model->chk_login();

    	
    	if(isset($_POST['active']))
    	{

    		   		
    		if($_POST['active']=="Admin")
    		{
    			$data['tickets']=$this->db->query("select * from tickets WHERE user_id= ".$_SESSION['id']." ")->result_array(); 
    			$response = $this->load->view("tickets/status_filter",$data,TRUE);
    		}
    		else
    		{
    			$data['tickets']=$this->db->query("select * from tickets WHERE (responsible_person= ".$_SESSION['id'].")  ")->result_array();     			
    			$response = $this->load->view("tickets/status_filter",$data,TRUE);
    		}

    		echo $response;
    	}

    }
public function unarchive()
{	
	if(isset($_COOKIE['remindoo_logout']) && $_COOKIE['remindoo_logout']=='remindoo_superadmin') 
        {
           $this->Loginchk_model->superAdmin_LoginCheck();
        }
         else if($_SESSION['user_type']=='FC')
        {
        	$this->Security_model->check_login();
        }
        else
        {
           $this->Security_model->chk_login();
        } 
	//	$this->Security_model->chk_login();
			
	if(!empty($_POST['ids']))
	{
		$tickets = $this->db->query("select * from tickets where id IN (".$_POST['ids'].") ")->result_array();
		
		
		$returnData = array();
		foreach ($tickets as  $datas) 
		{ 


			if($datas['status']=="Archive" && $datas['old_status'] <> "Archive")
			{
				
				$this->db->query("update tickets set status='".$datas['old_status']."',old_status='Archive' where id =".$datas['id']." ");
				if($this->db->affected_rows())
				{
					$returnData[$datas['id']] = $datas['old_status'];
					$this->StatusChange_notification($datas,$datas['old_status']);	
				}
				
			}

		}
		echo json_encode($returnData);
	}	
}


}	
