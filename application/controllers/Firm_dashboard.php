<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Firm_dashboard extends CI_Controller {
public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
    public function __construct(){
        parent::__construct();
        $this->load->model(array('Common_mdl','Security_model','Invoice_model','Proposal_model','Service_Reminder_Model'));
        $this->load->helper(['comman']);
    }

    public function get_all_services(){
      return $this->db->query('select * from service_lists')->result_array();
    }

    public function get_user_tasks($user_task_ids_str){
      return $this->db->query("SELECT id,related_to_services,task_status FROM add_new_task where id in (".$user_task_ids_str.") and firm_id = ".$_SESSION['firm_id']."  AND related_to != 'sub_task' AND related_to_services != '' AND related_to_services NOT LIKE '%wor%' AND task_status in (1,2,3)")->result_array();
    }

    public function get_all_tasks_counts($clause){
      return $this->db->query("SELECT count(*) as count FROM add_new_task where ".$clause." firm_id = ".$_SESSION['firm_id']."  AND related_to != 'sub_task' AND related_to_services != '' AND task_status in (1,2,3)")->result_array();
    }

    public function get_all_clients_counts($clause){
      return $this->db->query("SELECT count(*) as count FROM client where ".$clause." firm_id = ".$_SESSION['firm_id']."  ")->result_array();
    }

    public function get_all_leads_counts($clause){
      $leads_status = $this->db->query("SELECT id FROM `leads_status` where status_name='customer'")->row_array();
      return $this->db->query("SELECT count(*) as count FROM leads where ".$clause." firm_id = ".$_SESSION['firm_id']."  AND lead_status != '".$leads_status['id']."' AND lead_status!=''")->result_array();
    }

    public function task_counts(){
      $user_task_ids_arr = get_user_filtered_tasks('TASK');
      $user_task_ids_str = (!empty($user_task_ids_arr)) ? implode(',', array_filter(array_unique($user_task_ids_arr))) : '-1';
      $user_tasks = $this->get_user_tasks($user_task_ids_str);
      echo json_encode($user_tasks);
      exit;
    }

    public function get_all_firm_task_ids($clause){
      $all_task_ids = $this->db->query("SELECT id,sub_task_id FROM add_new_task where ".$clause." firm_id = ".$_SESSION['firm_id']."  AND related_to != 'sub_task'  AND related_to_services != '' AND task_status in (1,2,3)")->result_array();
      $all_task_ids_str = '';
      $count_task_ids = count($all_task_ids);
      foreach($all_task_ids as $key => $task_id){
        if($task_id['sub_task_id'] != ''){
          // $all_task_ids_str .=$task_id['sub_task_id'].', ';
          
        }
        if(($key+1) == $count_task_ids){
          $all_task_ids_str .= $task_id['id'];
        }
        else{
          $all_task_ids_str .= $task_id['id'].', ';
        }
      }
      return $all_task_ids_str;
    }

    public function get_total_timer($task_ids_str){
      $start_pause_results = $this->db->query("SELECT * FROM individual_task_timer where task_id in (".$task_ids_str.") ")->result_array(); 
      $total_time = 0;
      foreach($start_pause_results as $result){
        $time_stamp_str = $result['time_start_pause'];
        $task_id = $result['task_id'];
        $time_stamp_str_arr = explode(',',$time_stamp_str);
        $time_stamp_str_arr = array_reverse($time_stamp_str_arr);
        $prev_stamp = 0;
        foreach($time_stamp_str_arr as $key => $time_stamp){
          if($key == 0){
            $prev_stamp = $time_stamp;
          }
          else{
            $time_diff = $prev_stamp - $time_stamp;
            $prev_stamp = $time_stamp;
            // echo '<br/>'.$time_diff.'=='.$task_id;
            // Remove large logs > 10 hours
            // ise existing fn
            // try with non-wqa user

            $total_time += $time_diff < 36000 ? $time_diff : 0 ;
          }
        }
      }
      // $time_mins = $total_time/60;
      return $total_time;
    }

    public function header_pie_charts_counts(){
      $total_tasks_count_arr = $this->get_all_tasks_counts('');
      $user_task_ids = get_user_filtered_tasks('TASK');
      $user_task_ids_str = (!empty($user_task_ids)) ? implode(',', array_filter(array_unique($user_task_ids))) : '-1';
      $user_total_tasks_count_arr = $this->get_all_tasks_counts("id in (".$user_task_ids_str.") and");
      $total_clients_count_arr = $this->get_all_clients_counts('');
      $user_client_ids = get_user_filtered_tasks('CLIENT');
      $user_client_ids_str = (!empty($user_client_ids)) ? implode(',', array_filter(array_unique($user_client_ids))) : '-1';
      $user_total_clients_count_arr = $this->get_all_clients_counts("id in (".$user_client_ids_str.") and");
      $total_leads_count_arr = $this->get_all_leads_counts('');
      $user_lead_ids = get_user_filtered_tasks('LEADS');
      $user_lead_ids_str = (!empty($user_lead_ids)) ? implode(',', array_filter(array_unique($user_lead_ids))) : '-1';
      $user_total_leads_count_arr = $this->get_all_leads_counts("id in (".$user_lead_ids_str.") and");
      $all_firm_task_ids = $this->get_all_firm_task_ids('');
      $all_user_firm_task_ids = $this->get_all_firm_task_ids("id in (".$user_task_ids_str.") and");
      $total_time = $this->get_total_timer($all_firm_task_ids);
      $total_user_time = $this->get_total_timer($all_user_firm_task_ids);
      $result['total_tasks'] = $total_tasks_count_arr[0]['count'];
      $result['user_total_tasks'] = $user_total_tasks_count_arr[0]['count'];
      $result['total_clients'] = $total_clients_count_arr[0]['count'];
      $result['user_total_clients'] = $user_total_clients_count_arr[0]['count'];
      $result['total_leads'] = $total_leads_count_arr[0]['count'];
      $result['user_total_leads'] = $user_total_leads_count_arr[0]['count'];
      $result['total_time'] = $total_time;
      $result['total_user_time'] = $total_user_time;
      $result['user_name'] = $_SESSION['crm_name'];
      echo json_encode($result);
      die();
    }

    public function index($user_id=false)
    {      
        $this->Security_model->chk_login();

        $user_id = $_SESSION['id'];
        $all_services = $this->get_all_services();
        $data['all_services'] = $all_services;
        $data['user_tasks'] = $user_tasks;
        if($_SESSION['permission']['Dashboard']['view']=='1'){

        $last_month_start = date("Y-m-01");
        $last_month_end = date("Y-m-t"); 
        $last_month_end = date('Y-m-d', strtotime($last_month_end . ' +1 day'));  

        if($_SESSION['role']=='6'){

             $data['tasks']=$this->db->query('select count(*) as task_count from add_new_task where FIND_IN_SET("'.$_SESSION['id'].'",worker)')->row_array();
            $data['closed_tasks']=$this->db->query('select count(*) as task_count from add_new_task where FIND_IN_SET("'.$_SESSION['id'].'",worker) AND task_status="complete"')->row_array();
            $data['pending_tasks']=$this->db->query('select count(*) as task_count from add_new_task where FIND_IN_SET("'.$_SESSION['id'].'",worker) AND task_status="inprogress"')->row_array();
            $data['cancelled_tasks']=$this->db->query('select count(*) as task_count from add_new_task where FIND_IN_SET("'.$_SESSION['id'].'",worker) AND task_status="notstarted"')->row_array(); 
        }else if($_SESSION['role']=='4'){

            $id_get=$this->db->query("select id from client where user_id='".$_SESSION['id']."'")->row_array();            
            $data['tasks']=$this->db->query('select count(*) as task_count from add_new_task where company_name='.$id_get['id'].'')->row_array();

            $data['closed_tasks']=$this->db->query('select count(*) as task_count from add_new_task where company_name='.$id_get['id'].' AND task_status="complete"')->row_array();
            $data['pending_tasks']=$this->db->query('select count(*) as task_count from add_new_task where company_name='.$id_get['id'].' AND task_status="inprogress"')->row_array();
            $data['cancelled_tasks']=$this->db->query('select count(*) as task_count from add_new_task where company_name='.$id_get['id'].' AND task_status="notstarted"')->row_array();
        }else{            
            $data['tasks']=$this->db->query('select count(*) as task_count from add_new_task
            where create_by='.$_SESSION['id'].'')->row_array();
            $data['closed_tasks']=$this->db->query('select count(*) as task_count from add_new_task where create_by='.$_SESSION['id'].' AND task_status="5"')->row_array();
            $data['pending_tasks']=$this->db->query('select count(*) as task_count from add_new_task where create_by='.$_SESSION['id'].' AND task_status="2"')->row_array();
            $data['cancelled_tasks']=$this->db->query('select count(*) as task_count from add_new_task where create_by='.$_SESSION['id'].' AND task_status="1"')->row_array();                
        }

        $data['client']= $this->db->query('SELECT count(*) as client_count FROM user where role="4" AND autosave_status!="1" and crm_name!="" and firm_admin_id="'.$_SESSION['id'].'" order by id DESC')->row_array();

         if($_SESSION['role']=='6'){

             $data['completed_tasks']=$this->db->query('select count(*) as task_count from add_new_task where task_status="complete" and  FIND_IN_SET("'.$_SESSION['id'].'",worker)')->row_array();  

        }else if($_SESSION['role']=='4'){

            $id_get=$this->db->query("select id from client where user_id='".$_SESSION['id']."'")->row_array();  
            $data['completed_tasks']=$this->db->query('select count(*) as task_count from add_new_task where task_status="complete" and company_name="'.$id_get['id'].'"')->row_array();

        }else{

            $data['completed_tasks']=$this->db->query('select count(*) as task_count from add_new_task where task_status="5" and create_by="'.$_SESSION['id'].'"')->row_array();
        }
        // $data['active_client']=$this->db->query('select count(*) as client_count from user where status="1"')->row_array();
       
        /** for count rspt **/
        $id = $this->session->userdata('admin_id');
        $user_id = $this->session->userdata('id');
        $query=$this->db->query("SELECT * FROM client WHERE firm_admin_id=".$id." order by id desc ")->result_array();
        $res=array();
        foreach ($query as $key => $value) {
            array_push($res, $value['id']);
        }  
        $im_val=implode(',',$res); 
        $querys=$this->db->query("SELECT * FROM leads where user_id=".$_SESSION['id']." order by id desc "); 
                                
        $data['total_leads']=$querys->result_array();     
      
        $data['active_client']=$this->db->query('SELECT count(*) as client_count FROM user where role="4" AND autosave_status!="1" and crm_name!="" and firm_admin_id="'.$_SESSION['id'].'" and status="1" order by id DESC')->row_array();

        $data['inactive_client']=$this->db->query('SELECT count(*) as client_count FROM user where role="4" AND autosave_status!="1" and crm_name!="" and firm_admin_id="'.$_SESSION['id'].'" and (status="2" or status="0") order by id DESC')->row_array();
        /** end of rspt **/
        $data['nonactive_client']=$this->db->query('SELECT count(*) as client_count FROM user where role="4" AND autosave_status!="1" and crm_name!="" and firm_admin_id="'.$_SESSION['id'].'" and (status="2" or status="0") order by id DESC')->row_array();

        $data['archive_client']=$this->db->query('SELECT count(*) as client_count FROM user where role="4" AND autosave_status!="1" and crm_name!="" and firm_admin_id="'.$_SESSION['id'].'" and status="3"  order by id DESC')->row_array();

        $data['new_client']=$this->Common_mdl->ClientDetailsByMonth($last_month_start,$last_month_end);
       // echo $this->db->last_query();
        $data['todo_list']=$this->db->query("select * from `todos_list` where status='active'  and user_id='".$_SESSION['id']."' order by order_number Asc")->result_array();
        $data['todo_completedlist']=$this->db->query("select * from `todos_list` where status='completed'  and user_id='".$_SESSION['id']."' order by order_number Asc")->result_array();
        $data['todolist']=$this->db->query("select * from `todos_list` order by order_number Asc")->result_array();
        $last_month_start = date("Y-m-d");
        $last_month_end = date("Y-m-t"); 
//`end_date` between '".$last_month_start."' and  '".$last_month_end."' and priority='high'   AND
        if($_SESSION['role']=='6'){

            $data['task_list']=$this->db->query("SELECT * FROM `add_new_task` WHERE  FIND_IN_SET('".$_SESSION['id']."',worker)  order by id desc Limit 0,5")->result_array();

        }else if($_SESSION['role']=='4'){

            $id_get=$this->db->query("select id from client where user_id='".$_SESSION['id']."'")->row_array();
           // echo "SELECT * FROM `add_new_task` WHERE  id='".$id_get['id']."'  order by id desc Limit 0,5";
            $data['task_list']=$this->db->query("SELECT * FROM `add_new_task` WHERE  company_name='".$id_get['id']."'  order by id desc Limit 0,5")->result_array();

        }else{

            $data['task_list']=$this->db->query("SELECT * FROM `add_new_task` WHERE  create_by ='".$_SESSION['id']."' order by id desc Limit 0,5")->result_array(); 
        }
        

        $id = $this->session->userdata('admin_id');
        $user_id = $this->session->userdata('id');
        $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = ".$id." and role = 4 and crm_name !=''")->row_array();
        $us_id =  explode(',', $sql['us_id']);
        $current_date=date("Y-m-d");
        $time=date("Y-m-d");
        $data['today'] = $this->db->query( "SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and ((crm_confirmation_statement_due_date= '".$current_date."') or (crm_ch_accounts_next_due = '".$current_date."') or (crm_accounts_tax_date_hmrc = '".$current_date."' ) or (crm_personal_due_date_return = '".$current_date."' ) or (crm_vat_due_date = '".$current_date."' ) or (crm_rti_deadline = '".$current_date."')  or (crm_pension_subm_due_date = '".$current_date."') or (crm_next_p11d_return_due = '".$current_date."')  or (crm_next_manage_acc_date = '".$current_date."') or (crm_next_booking_date = '".$current_date."' ) or (crm_insurance_renew_date = '".$current_date."')  or (crm_registered_renew_date = '".$current_date."' ) or (crm_investigation_end_date = '".$current_date."' )) ")->result_array();
        $oneweek_end_date = date('Y-m-d', strtotime($time . ' +1 week'));
          //echo $end_date;
        $data['oneweek'] = $this->db->query( "SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and ((crm_confirmation_statement_due_date BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_ch_accounts_next_due BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_personal_due_date_return BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_vat_due_date BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_rti_deadline BETWEEN '".$current_date."' AND '".$oneweek_end_date."' )  or (crm_pension_subm_due_date BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_next_p11d_return_due BETWEEN '".$current_date."' AND '".$oneweek_end_date."' )  or (crm_next_manage_acc_date BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_next_booking_date BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_insurance_renew_date BETWEEN '".$current_date."' AND '".$oneweek_end_date."' )  or (crm_registered_renew_date BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_investigation_end_date BETWEEN '".$current_date."' AND '".$oneweek_end_date."' )) ")->result_array();
        $onemonth_end_date = date('Y-m-d', strtotime($time . ' +1 month'));
        $data['onemonth'] = $this->db->query( "SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and ((crm_confirmation_statement_due_date BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_ch_accounts_next_due BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_personal_due_date_return BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_vat_due_date BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_rti_deadline BETWEEN '".$current_date."' AND '".$onemonth_end_date."' )  or (crm_pension_subm_due_date BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_next_p11d_return_due BETWEEN '".$current_date."' AND '".$onemonth_end_date."' )  or (crm_next_manage_acc_date BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_next_booking_date BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_insurance_renew_date BETWEEN '".$current_date."' AND '".$onemonth_end_date."' )  or (crm_registered_renew_date BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_investigation_end_date BETWEEN '".$current_date."' AND '".$onemonth_end_date."' )) ")->result_array();
        $oneyear_end_date = date('Y-m-d', strtotime($time . ' +1 year'));

        $data['oneyear'] = $this->db->query( "SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and ((crm_confirmation_statement_due_date BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_ch_accounts_next_due BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_personal_due_date_return BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_vat_due_date BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_rti_deadline BETWEEN '".$current_date."' AND '".$oneyear_end_date."' )  or (crm_pension_subm_due_date BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_next_p11d_return_due BETWEEN '".$current_date."' AND '".$oneyear_end_date."' )  or (crm_next_manage_acc_date BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_next_booking_date BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_insurance_renew_date BETWEEN '".$current_date."' AND '".$oneyear_end_date."' )  or (crm_registered_renew_date BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_investigation_end_date BETWEEN '".$current_date."' AND '".$oneyear_end_date."' )) ")->result_array();

          $last_Year_start = date("Y-01-01");
          $last_month_end = date("Y-12-31");

        $data['active']=$this->db->query('SELECT count(*) as client_count FROM user where role="4" AND autosave_status!="1" and crm_name!="" and firm_admin_id="'.$_SESSION['id'].'" and status="1"  and from_unixtime(CreatedTime) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'" order by id DESC')->row_array();


        $data['inactive']=$this->db->query('SELECT count(*) as client_count FROM user where role="4" AND autosave_status!="1" and crm_name!="" and firm_admin_id="'.$_SESSION['id'].'" and (status="2" or status="0")  and from_unixtime(CreatedTime) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'" order by id DESC')->row_array();


        $data['frozen']=$this->db->query('SELECT count(*) as client_count FROM user where role="4" AND autosave_status!="1" and crm_name!="" and firm_admin_id="'.$_SESSION['id'].'" and status="3"  and from_unixtime(CreatedTime) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"  order by id DESC')->row_array();

        $last_month_start = date("Y-m-01");
        $last_mon_end = date("Y-m-t");  

        $data['invoice']=$this->db->query('select count(*) as invoice_count from Invoice_details' )->row_array();       
        $data['approved_invoice']=$this->db->query('select count(*) as invoice_count from amount_details where payment_status="approve"' )->row_array();
        $data['cancel_invoice']=$this->db->query('select count(*) as invoice_count from amount_details where payment_status="decline" ' )->row_array();
        $data['expired_invoice']=$this->db->query('select count(*) as invoice_count from Invoice_details where invoice_duedate="Expired"' )->row_array();


        $count=explode(',', $sql['us_id']);
        if($count['0']!=''){
        $data['confirmation'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id IN ('. implode( ',', $us_id ) .') and crm_company_name !="" and ((crm_confirmation_statement_due_date between "'.$last_month_start.'" AND "'.$last_mon_end.'"))')->row_array(); 
        $data['account'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id IN ('. implode( ',', $us_id ) .') and crm_company_name !="" and crm_ch_accounts_next_due!="" and ((crm_ch_accounts_next_due between "'.$last_month_start.'" AND "'.$last_mon_end.'"))')->row_array();
        $data['company_tax'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id IN ('. implode( ',', $us_id ) .') and crm_company_name !="" and ((crm_accounts_tax_date_hmrc between "'.$last_month_start.'" AND "'.$last_mon_end.'"))')->row_array();
        $data['personal_due'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id IN ('. implode( ',', $us_id ) .') and crm_company_name !="" and ((crm_personal_due_date_return between "'.$last_month_start.'" AND "'.$last_mon_end.'"))')->row_array();

// echo 'SELECT count(*) as deadline_count FROM client WHERE user_id IN ('. implode( ',', $us_id ) .') and crm_company_name !="" and ((crm_vat_due_date between "'.$last_month_start.'" AND "'.$last_mon_end.'"))';

        $data['vat_record'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id IN ('. implode( ',', $us_id ) .') and crm_company_name !="" and ((crm_vat_due_date between "'.$last_month_start.'" AND "'.$last_mon_end.'"))')->row_array();

        $data['payroll'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id IN ('. implode( ',', $us_id ) .') and crm_company_name !="" and ((crm_rti_deadline between "'.$last_month_start.'" AND "'.$last_mon_end.'"))')->row_array();
        $data['pension'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id IN ('. implode( ',', $us_id ) .') and crm_company_name !="" and ((crm_pension_subm_due_date between "'.$last_month_start.'" AND "'.$last_mon_end.'"))')->row_array();
        $data['plld'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id IN ('. implode( ',', $us_id ) .') and crm_company_name !="" and ((crm_next_p11d_return_due between "'.$last_month_start.'" AND "'.$last_mon_end.'"))')->row_array();
        $data['management_record'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id IN ('. implode( ',', $us_id ) .') and crm_company_name !="" and ((crm_next_manage_acc_date between "'.$last_month_start.'" AND "'.$last_mon_end.'"))')->row_array();
        $data['booking'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id IN ('. implode( ',', $us_id ) .') and crm_company_name !="" and ((crm_next_booking_date between "'.$last_month_start.'" AND "'.$last_mon_end.'"))')->row_array();
        $data['insurance'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id IN ('. implode( ',', $us_id ) .') and crm_company_name !="" and ((crm_insurance_renew_date between "'.$last_month_start.'" AND "'.$last_mon_end.'"))')->row_array();
        $data['registeration'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id IN ('. implode( ',', $us_id ) .') and crm_company_name !="" and ((crm_registered_renew_date between "'.$last_month_start.'" AND "'.$last_mon_end.'"))')->row_array();
        $data['investigation'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id IN ('. implode( ',', $us_id ) .') and crm_company_name !="" and ((crm_investigation_end_date between "'.$last_month_start.'" AND "'.$last_mon_end.'"))')->row_array();

    }else{
            $data['confirmation'] =0;
            $data['company_tax']=0;
            $data['personal_due'] =0;
            $data['account'] =0;
            $data['vat'] =0;
            $data['payroll'] =0;
            $data['pension']=0;
            $data['plld']=0;
            $data['management']=0;
            $data['booking']=0;
            $data['insurance']=0;
            $data['registeration']=0;
            $data['investigation']=0;
    }


        $last_month_start = date("Y-m-01");
        $last_mon_end = date("Y-m-t"); 
        if($_SESSION['role']=='1'){


              $data['accept_proposal']=$this->db->query('select count(*) as proposal_count from proposals
            where status="accepted" and  created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"')->row_array();
            $data['indiscussion_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="in discussion" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"')->row_array();
            $data['sent_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="sent"  and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
            $data['viewed_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="opened"  and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
            $data['declined_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="declined" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
            $data['archive_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="archive" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
             $data['draft_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="draft"  and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();   

          
        }else if($_SESSION['role']=='4'){

             $data['accept_proposal']=$this->db->query('select count(*) as proposal_count from proposals
            where status="accepted" and company_id="'.$_SESSION['id'].'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"')->row_array();
            $data['indiscussion_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="in discussion" and company_id="'.$_SESSION['id'].'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"')->row_array();
            $data['sent_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="sent"  and company_id="'.$_SESSION['id'].'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
            $data['viewed_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="opened" and company_id="'.$_SESSION['id'].'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
            $data['declined_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="declined" and company_id="'.$_SESSION['id'].'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
            $data['archive_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="archive" and company_id="'.$_SESSION['id'].'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
             $data['draft_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="draft" and company_id="'.$_SESSION['id'].'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();


        }else{

            $data['accept_proposal']=$this->db->query('select count(*) as proposal_count from proposals
            where status="accepted" and user_id="'.$_SESSION['id'].'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"')->row_array();
            $data['indiscussion_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="in discussion" and user_id="'.$_SESSION['id'].'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"')->row_array();
            $data['sent_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="sent"  and user_id="'.$_SESSION['id'].'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
            $data['viewed_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="opened" and user_id="'.$_SESSION['id'].'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
            $data['declined_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="declined" and user_id="'.$_SESSION['id'].'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
            $data['archive_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="archive" and user_id="'.$_SESSION['id'].'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
             $data['draft_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="draft" and user_id="'.$_SESSION['id'].'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
          
        }

         if($_SESSION['role']==4)
        {
             $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where  role = 4 and crm_name !='' and (firm_admin_id = ".$id." or id=".$id.")  ")->row_array();

        $us_id =  explode(',', $sql['us_id']);

        if(count($us_id)>0){

        }else{
            $us_id=0;
        }
         
        }
        else
        {
          $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = ".$id." and role = 4 and crm_name !=''")->row_array();

        $us_id =  explode(',', $sql['us_id']);

        if(count($us_id)>0){

        }else{
            $us_id=0;
        }

        }

        $data['getCompany'] = $this->db->query( "SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !=''")->result_array();

        $this->load->view('firm_dashboard/desk_dashboard',$data); 
      }else{
        $this->load->view('users/blank_page');
      }
    }


    public function todolist_add()
    {
       $todo_name = $_POST['todo_name'];
       $user_id = $this->session->userdata['userId']; 
       $status = 'active';
       $created_time=time();
       $maximum_id = $this->db->query("SELECT max(id) as max_id FROM todos_list")->row_array();
       $order_number = $maximum_id['max_id']+1;
       $datas = array('todo_name'=>$todo_name,'user_id'=>$user_id,'status'=>$status,'created_time'=>$created_time,'order_number'=>$order_number,'firm_id'=>$_SESSION['firm_id']);
       $insert = $this->Common_mdl->insert('todos_list',$datas);
      
       $data = $this->getTodoContent();
       $data['status'] = $insert;
       echo json_encode($data);
    }

    public function todolist_edit()
    {
        $id = $_POST['id'];
        $todo_name = $_POST['todo_name'];
        $data = array('todo_name'=>$todo_name);
        $update = $this->Common_mdl->update('todos_list',$data,'id',$id);             
        
        $data = $this->getTodoContent();
        $data['status'] = $update;
        echo json_encode($data);          
    }

    public function todos_status()
    {
         $id = $_POST['id'];
         $statsus = $this->db->query("select status from todos_list where id=".$id."")->row_array();
         $status = $statsus['status'];

         if($status == 'active')
         {
            $status = 'completed';
         }
         else
         {
            $status = 'active';
         }
        $data=array('status'=>$status);
        $update=$this->Common_mdl->update('todos_list',$data,'id',$id);
        
        $data = $this->getTodoContent();
        $data['status'] = $update;
        echo json_encode($data);  
    }    

    public function getTodoContent()
    {
        $todo_active = $this->db->query("SELECT * from `todos_list` where status='active' and user_id='".$_SESSION['id']."' order by order_number DESC LIMIT 5")->result_array();
        $data['content'] = '';

        foreach ($todo_active as $todo) 
        {
            $data['content'].='<div class="to-do-list card-sub ui-sortable-handle" id="'.$todo['id'].'">
                   <div class="checkbox-fade fade-in-primary">
                      <label class="check-task">
                      <input type="checkbox" value="" id="'.$todo['id'].'" onclick="todo_status(this)">
                      <span class="cr">
                      <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                      </span>
                      <span>'.$todo['todo_name'].'</span> 
                      </label>
                   </div>
                   <div class="f-right">
                     <a href="javascript:;" data-toggle="modal" data-target="#EditTodo_'.$todo['id'].'" class=""><i class="icofont icofont-ui-edit"></i></a>
                      <a href="javascript:;" data-toggle="modal" data-target="#DeleteTodo_'.$todo['id'].'" class=""><i class="icofont icofont-ui-delete"></i></a>
                   </div>
                </div>';           
         }

         $todo_completed = $this->db->query("SELECT * from `todos_list` where status='completed'  and user_id='".$_SESSION['id']."' order by order_number DESC LIMIT 5")->result_array();
         $data['completed_content']='';
         
         foreach ($todo_completed as $todo) 
         {
             $data['completed_content'].='<div class="to-do-list card-sub ui-sortable-handle" id="'.$todo['id'].'">
                    <div class="checkbox-fade fade-in-primary">
                       <label class="check-task done-task">
                       <input type="checkbox" value="" id="'.$todo['id'].'" onclick="todo_status(this)" checked="checked">
                       <span class="cr">
                       <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                       </span>
                       <span>'.$todo['todo_name'].'</span> 
                       </label>
                    </div>
                    <div class="f-right">
                      <a href="#" data-toggle="modal" data-target="#EditTodo_'.$todo['id'].'" class=""><i class="icofont icofont-ui-edit"></i></a>
                       <a href="javascript:;" data-toggle="modal" data-target="#DeleteTodo_'.$todo['id'].'" class=""><i class="icofont icofont-ui-delete"></i></a>
                    </div>
                 </div>';
             
          }

          $todo_all = $this->db->query("SELECT * from `todos_list` WHERE user_id ='".$_SESSION['id']."' order by order_number Asc")->result_array(); 
          $data['all_edit_delete_modals'] = "";
          $data['view_all'] = "";

          foreach ($todo_all as $todo) 
          { 
              $data['all_edit_delete_modals'] .= '<div class="modal fade edittodo_view" id="EditTodo_'.$todo['id'].'" role="dialog">
                   <div class="modal-dialog">
                      <!-- Modal content-->
                      <div class="modal-content">
                         <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Todos</h4>
                         </div>
                         <div class="modal-body">
                            <input type="hidden" name="id" id="todo_id" value="'.$todo['id'].'">
                            <input type="text" name="todo_name" class="todo_name" value="'.$todo['todo_name'].'"><span id="er_todoname_'.$todo['id'].'"></span>
                         </div>
                         <div class="modal-footer">
                            <button type="button" class="btn btn-default save" id="'.$todo['id'].'">Save</button>
                            <button type="button" class="btn btn-default close" data-dismiss="modal">Close</button>
                         </div>
                      </div>
                   </div>
                </div>
                <div class="modal fade" id="DeleteTodo_'.$todo['id'].'" role="dialog">
                   <div class="modal-dialog">
                      <!-- Modal content-->
                      <div class="modal-content">
                         <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Delete todos</h4>
                         </div>
                         <div class="modal-body">
                            <p>Do you want to Delete Todo(s)?</p>
                         </div>
                         <div class="modal-footer">
                            <button type="button" class="btn btn-default delete" id="'.$todo['id'].'">Yes</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                         </div>
                      </div>
                   </div>
                </div>';

               if($todo['status'] == 'completed')
               { 
                  $class = 'done-task'; 
                  $checked = 'checked=checked';
               }
               else
               { 
                  $class = ""; 
                  $checked = "";
               }

               $data['view_all'] .= '<div class="to-do-list sortable-moves">
                                      <div class="to-do-list card-sub" id="1">
                                         <div class="top-disk01">
                                            <label class="custom_checkbox1 check-task '.$class.'">
                                            <input type="checkbox" value="" id="'.$todo['id'].'" onclick="todo_status(this)" '.$checked.'> <i></i></label>
                                            <span>'.ucwords($todo['todo_name']).'</span> 
                                         </div>
                                         <div class="f-right">
                                            <a href="javascript:;" data-toggle="modal" data-target="#EditTodo_'.$todo['id'].'" class=""><i class="icofont icofont-ui-edit"></i></a>
                                            <a href="javascript:;" data-toggle="modal" data-target="#DeleteTodo_'.$todo['id'].'" class=""><i class="icofont icofont-ui-delete"></i></a>
                                         </div>
                                      </div>
                                   </div>';
         }

         return $data;       
    }
    
    public function todolist_delete()
    {
        $id=$_POST['id'];        
        $delete=$this->Common_mdl->delete('todos_list','id',$id);
        echo $delete;
    }

    public function Check_Distinct()
    { 
        $table = 'todos_list';
        $field = 'todo_name';
        $value = $_POST['todoname'];
        $id = !empty($_POST['id'])?$_POST['id']:"";
        
        $result = $this->Common_mdl->check_distinct($table,$field,$value,$id);

        echo $result['count'];
    }

    public function client_filter()
    {        
        $last_month_start = date("Y-m-01");
        $last_month_end = date("Y-m-t"); 
        $last_month_end = date('Y-m-d', strtotime($last_month_end . ' +1 day'));        

        $data['active'] = $this->Common_mdl->ClientDetailsByMonth($last_month_start,$last_month_end,'AND user.status="1"');
        $data['inactive'] = $this->Common_mdl->ClientDetailsByMonth($last_month_start,$last_month_end,'AND (user.status="2" or user.status="0")');
        $data['frozen'] = $this->Common_mdl->ClientDetailsByMonth($last_month_start,$last_month_end,'AND user.status="3"');     

        $this->load->view('firm_dashboard/chart_success',$data);       
    }


    public function client_filter_year()
    {
        $last_Year_start = date("Y-01-01");
        $last_month_end = date("Y-12-31");        

        $data['Private']=$this->Common_mdl->ClientDetailsByLegalForm('Private Limited company',$last_Year_start,$last_month_end); 
        $data['Public']=$this->Common_mdl->ClientDetailsByLegalForm('Public Limited company',$last_Year_start,$last_month_end);
        $data['limited']=$this->Common_mdl->ClientDetailsByLegalForm('Limited Liability Partnership',$last_Year_start,$last_month_end);
        $data['Partnership']=$this->Common_mdl->ClientDetailsByLegalForm('Partnership',$last_Year_start,$last_month_end);
        $data['self']=$this->Common_mdl->ClientDetailsByLegalForm('Self Assessment',$last_Year_start,$last_month_end);
        $data['Trust']=$this->Common_mdl->ClientDetailsByLegalForm('Trust',$last_Year_start,$last_month_end);
        $data['Charity']=$this->Common_mdl->ClientDetailsByLegalForm('Charity',$last_Year_start,$last_month_end);
        $data['Other']=$this->Common_mdl->ClientDetailsByLegalForm('Other',$last_Year_start,$last_month_end);
        
        $this->load->view('firm_dashboard/year_chart_clients',$data);        
    }

    public function deadline_filter_year()
    {
        $year_start = date("Y-01-01");
        $year_end = date("Y-12-31"); 

        $Assigned_Client = Get_Assigned_Datas('CLIENT');

        foreach ($Assigned_Client as $key => $value) 
        {
            $rec = $this->Invoice_model->selectRecord('client','id', $value);
            
            if(!empty($rec['user_id']))
            {
              $arr[] = $rec['user_id'];
            }  
        }

        if(isset($arr) && count($arr)>0)
        {
          $clients_user_ids = implode(',', $arr);
        }
        else
        {
           $clients_user_ids = "";
        }

        $data = $this->Common_mdl->getClientsDeadlinesCount($clients_user_ids,$year_start,$year_end);
      
        $this->load->view('firm_dashboard/deadline_chart',$data);
    }

    public function deadline_filter_month()
    {
        $month_start = date("Y-m-01");
        $month_end = date("Y-m-t"); 
        $month_end = date('Y-m-d', strtotime($month_end . ' +1 day'));

        $Assigned_Client = Get_Assigned_Datas('CLIENT');

        foreach ($Assigned_Client as $key => $value) 
        {
            $rec = $this->Invoice_model->selectRecord('client','id', $value);
            
            if(!empty($rec['user_id']))
            {
              $arr[] = $rec['user_id'];
            }  
        }
        
        if(isset($arr) && count($arr)>0)
        {
          $clients_user_ids = implode(',', $arr);
        }
        else
        {
           $clients_user_ids = "";
        }

        $data = $this->Common_mdl->getClientsDeadlinesCount($clients_user_ids,$month_start,$month_end);
      
        $this->load->view('firm_dashboard/deadline_chart',$data);
    }

    public function filter_proposal_year()
    {
        $year_start = date("Y-01-01");
        $year_end = date("Y-12-31");  
        
        $data['accept_proposal'] = $this->Proposal_model->proposal_details_date('accepted',$year_start,$year_end); 
        $data['indiscussion_proposal'] = $this->Proposal_model->proposal_details_date('in discussion',$year_start,$year_end);
        $data['sent_proposal'] = $this->Proposal_model->proposal_details_date('sent',$year_start,$year_end);
        $data['viewed_proposal'] = $this->Proposal_model->proposal_details_date('opened',$year_start,$year_end);
        $data['declined_proposal'] = $this->Proposal_model->proposal_details_date('declined',$year_start,$year_end);
        $data['archive_proposal'] = $this->Proposal_model->proposal_details_date('archive',$year_start,$year_end);
        $data['draft_proposal'] = $this->Proposal_model->proposal_details_date('draft',$year_start,$year_end);

        $this->load->view('firm_dashboard/proposal_chart',$data);        
    }

    public function filter_proposal_month()
    {
        $month_start = date("Y-m-01");
        $month_end = date("Y-m-t");  
        $month_end = date('Y-m-d', strtotime($month_end . ' +1 day')); 

        $data['accept_proposal'] = $this->Proposal_model->proposal_details_date('accepted',$month_start,$month_end); 
        $data['indiscussion_proposal'] = $this->Proposal_model->proposal_details_date('in discussion',$month_start,$month_end);
        $data['sent_proposal'] = $this->Proposal_model->proposal_details_date('sent',$month_start,$month_end);
        $data['viewed_proposal'] = $this->Proposal_model->proposal_details_date('opened',$month_start,$month_end);
        $data['declined_proposal'] = $this->Proposal_model->proposal_details_date('declined',$month_start,$month_end);
        $data['archive_proposal'] = $this->Proposal_model->proposal_details_date('archive',$month_start,$month_end);
        $data['draft_proposal'] = $this->Proposal_model->proposal_details_date('draft',$month_start,$month_end);

        $this->load->view('firm_dashboard/proposal_chart',$data);
    }

    public function update_type_todolist(){
        $all_val = array_filter($_POST['datas']);          
       $a=array();
       foreach ($all_val as $key => $value) {
            $rec = implode(',', $value);
            array_push($a,$value); 
        }       
       $j=1;
         for($i=0;$i<count($a[0]);$i++) {       
             $data=array('order_number'=>$j);
             $update=$this->Common_mdl->update('todos_list',$data,'id',$a[0][$i]);
             $j++;
         }     
    }

    public function Active_client(){
      $this->session->set_flashdata('firm_seen','active');
      redirect('user');
    }
    public function All_client(){
      $this->session->set_flashdata('firm_seen','alluser');
      redirect('user');
    }
    public function Inactive_client(){
      $this->session->set_flashdata('firm_seen','inactive');
      redirect('user');
    }
    public function Frozen_client(){
      $this->session->set_flashdata('firm_seen','frozen');
      redirect('user');
    }
    public function Complete(){
      $this->session->set_flashdata('firm_seen','5');
      redirect('user/task_list');
    }

    public function inprogress(){
      $this->session->set_flashdata('firm_seen','2');
      redirect('user/task_list');
    }

    public function notstarted(){
      $this->session->set_flashdata('firm_seen','1');
      redirect('user/task_list');
    }

     public function awaited(){
      $this->session->set_flashdata('firm_seen','awaited');
      redirect('user/task_list');
    }

    public function archive(){
      $this->session->set_flashdata('firm_seen','archive');
      redirect('user/task_list');
    }

    public function draft(){
      $this->session->set_flashdata('firm_seen','draft');
      redirect('proposal/');
    }

    public function sent(){
      $this->session->set_flashdata('firm_seen','sent');
      redirect('proposal/');
    }

     public function indiscussion(){
      $this->session->set_flashdata('firm_seen','indiscussion');
      redirect('proposal/');
    }

    public function view(){
      $this->session->set_flashdata('firm_seen','view');
      redirect('proposal/');
    }

     public function accept(){
      $this->session->set_flashdata('firm_seen','accept');
      redirect('proposal/');
    }

    public function decline(){
      $this->session->set_flashdata('firm_seen','decline');
      redirect('proposal/');
    }

    public function archieve(){
      $this->session->set_flashdata('firm_seen','archieve');
      redirect('proposal_page/proposal_template');
    }

    public function service($id)
    {
       $rec = $this->Common_mdl->select_record('service_lists','id',$id);

       $this->session->set_flashdata('firm_seen',$rec['service_name']);
       redirect('Deadline_manager/');
    }

    public function vat(){
      $this->session->set_flashdata('firm_seen','VAT');
      redirect('Deadline_manager/');
    }

    public function accounts(){
      $this->session->set_flashdata('firm_seen','Accounts');
      redirect('Deadline_manager/');
    }

    public function company_tax(){
      $this->session->set_flashdata('firm_seen','Company Tax Return');
      redirect('Deadline_manager/');
    }

    public function personal_tax(){
      $this->session->set_flashdata('firm_seen','Personal Tax Return');
      redirect('Deadline_manager/');
    }

      public function payroll(){
      $this->session->set_flashdata('firm_seen','Payroll');
      redirect('Deadline_manager/');
    }

      public function confirm_statement(){
      $this->session->set_flashdata('firm_seen','Confirmation statement');
      redirect('Deadline_manager/');
    }

     public function workplace(){
      $this->session->set_flashdata('firm_seen','WorkPlace Pension - AE');
      redirect('Deadline_manager/');
    }

      public function cis(){
      $this->session->set_flashdata('firm_seen','CIS - Contractor');
      redirect('Deadline_manager/');
    }

      public function cis_sub(){
      $this->session->set_flashdata('firm_seen','CIS - Sub Contractor');
      redirect('Deadline_manager/');
    }

      public function p11d(){
      $this->session->set_flashdata('firm_seen','P11D');
      redirect('Deadline_manager/');
    }

      public function bookkeeping(){
      $this->session->set_flashdata('firm_seen','Bookkeeping');
      redirect('Deadline_manager/');
    }

      public function management(){
      $this->session->set_flashdata('firm_seen','Management Accounts');
      redirect('Deadline_manager/');
    }

     public function investication(){
      $this->session->set_flashdata('firm_seen','Investigation Insurance');
      redirect('Deadline_manager/');
    }

     public function register(){
      $this->session->set_flashdata('firm_seen','Registered Address');
      redirect('Deadline_manager/');
    }

    public function tax(){
        $this->session->set_flashdata('firm_seen','Tax Investigation/Tax Advice');
        redirect('Deadline_manager/');
    }

    public function tax_inves(){
      $this->session->set_flashdata('firm_seen','Tax Investigation');
        redirect('Deadline_manager/');  
    }

     public function low(){
      $this->session->set_flashdata('firm_seen','low');
      redirect('user/task_list');
    }

     public function high(){
      $this->session->set_flashdata('firm_seen','hign');
      redirect('user/task_list');
    }

     public function medium(){
      $this->session->set_flashdata('firm_seen','medium');
      redirect('user/task_list');
    }

     public function super_urgent(){
      $this->session->set_flashdata('firm_seen','super_urgent');
      redirect('user/task_list');
    }

    public function private_function(){
      $this->session->set_flashdata('firm_seen','private_limited');
      redirect('user/');
    }

    public function public_function(){
         $this->session->set_flashdata('firm_seen','public_limited');
      redirect('user/');
    }

    public function limited_function(){
       $this->session->set_flashdata('firm_seen','limited_liablity');
       redirect('user/');
    }

    public function partnership(){
        $this->session->set_flashdata('firm_seen','partnership');
       redirect('user/');
    }

    public function self_assesment(){
        $this->session->set_flashdata('firm_seen','self');
       redirect('user/');
    }

     public function trust(){
        $this->session->set_flashdata('firm_seen','trust');
       redirect('user/');
    }

    public function charity(){
       $this->session->set_flashdata('firm_seen','charity');
       redirect('user/');
    }

    public function other(){
        $this->session->set_flashdata('firm_seen','other');
       redirect('user/');
    }




    

    


}
?>
