<?php
class Proposalcustomisation extends CI_Controller 
{
        public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
		public function __construct()
		{
			parent::__construct();
			$this->load->model(array('Common_mdl','Report_model','Security_model'));
			$this->load->library('Excel');
			$this->load->helper('comman');
		}

		public function custom()
		{			
			if(empty($_POST['status'])) 
			{				
				$status = "";
			}
			else
			{
				$poststatus = implode(',',$_POST['status']);
				$status = 'AND FIND_IN_SET(status,"'.$poststatus.'")';
			}
			
			if(!empty($_POST['filter']) && $_POST['filter'] != 'all')
				{					
					if($_POST['filter']=='week')
					{
						$current_date = date('Y-m-d');
						$week = date('W', strtotime($current_date));
						$year = date('Y', strtotime($current_date));

						$dto = new DateTime();
						$start_date = $dto->setISODate($year, $week, 0)->format('Y-m-d');
						$end_date = $dto->setISODate($year, $week, 6)->format('Y-m-d');
						$end_date = date('Y-m-d', strtotime($end_date . ' +1 day')); 
					}
	                else if($_POST['filter']=='three_week')
	                {
						$current_date = date('Y-m-d');
						$weeks = strtotime('- 3 week', strtotime( $current_date ));
						$start_date = date('Y-m-d', $weeks); 
						$addweeks = strtotime('+ 3 week', strtotime( $start_date ));
						$end_Week = date('Y-m-d', $addweeks); 
						$end_date = date('Y-m-d', strtotime($end_Week . ' +1 day'));
					}

					else if($_POST['filter']=='month')
					{
						$start_date = date("Y-m-01");
						$month_end = date("Y-m-t"); 
						$end_date = date('Y-m-d', strtotime($month_end . ' +1 day'));
					}

					if($_POST['filter']=='Three_month')
					{
						$end_date = date('Y-m-d',strtotime(date('Y-m-d').'+1 day'));
						$start_date = date('Y-m-d',strtotime('- 3 month', strtotime($end_date)));
					}	

					$filter = 'AND created_at BETWEEN "'.$start_date.'" AND "'.$end_date.'"';
			    }
			    else if(!empty($_POST['from_date']) && !empty($_POST['to_date']))
				{
					$start_date = $_POST['from_date'];
					$end_date = $_POST['to_date'];
					$filter = 'AND created_at BETWEEN "'.$start_date.'" AND "'.$end_date.'"';
				}
			    else
			    {
			    	$filter = '';
			    }			   

				if(!empty($_POST['client']))
				{
	                $clients_id = $_POST['client'];

	                $rec = $this->Report_model->selectRecord('client','id', $clients_id);    

	                $client = 'AND FIND_IN_SET(company_id,"'.$rec['user_id'].'")';
				}
                else
                {
                	$Assigned_Client = Get_Assigned_Datas('CLIENT'); 
                	$Assigned_Leads = Get_Assigned_Datas('LEADS');
                	
                	foreach ($Assigned_Client as $key => $value) 
                	{
                	    $rec = $this->Report_model->selectRecord('client','id', $value);
                	    
                	    if(!empty($rec['user_id']))
                	    {
                	      $arr[] = $rec['user_id'];
                	    }  
                	}
                	
                	if(isset($arr) && count($arr)>0)
                	{             
                	   $clients_user_ids = implode(',', $arr);
                	}
                	else
                	{
                	   $clients_user_ids = "";
                	}  
                	
                	$leads_ids = implode(',', $Assigned_Leads);	

                	$client = 'AND (FIND_IN_SET(company_id,"'.$clients_user_ids.'") OR FIND_IN_SET(lead_id,"'.$leads_ids.'"))';
                }
					
				$data['records'] = $this->db->query('SELECT * FROM proposals WHERE firm_id = "'.$_SESSION['firm_id'].'" '.$client.' '.$status.' '.$filter.' ORDER BY id ASC')->result_array();	

				$this->load->view('reports/proposal_customresult',$data);
		}

} ?>
