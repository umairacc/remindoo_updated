<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Assign extends CI_Controller {
public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct(){
        parent::__construct();
        $this->load->model(array('Common_mdl','Security_model'));
    }

    public function index()
    {
        $this->Security_model->chk_login();
        //   $data['team']=$this->Common_mdl->GetAllWithWhere('team','create_by',$_SESSION['id']);
        // $this->load->view('team/team',$data);
    }
    public function assign_manager(){
      $this->Security_model->chk_login();
      $data['assign_manager']=$this->db->query("select * from assign_manager where create_by=".$_SESSION['id']." ")->result_array();
      $this->load->view('assign/assign_manager',$data);
    }


    public function archive_assign_manager($id)
   {
    $this->db->query("update assign_manager set status=1 where id=$id");
    echo $this->db->affected_rows();
   }
    public function assign_staff_to_manager()
    {
       /* $this->Security_model->chk_login();
        $data['assign_manager']=$this->Common_mdl->getallrecords('assign_manager');
        $sql = $this->db->query("SELECT GROUP_CONCAT(  `manager` SEPARATOR  ',' ) AS  `manager` , GROUP_CONCAT(  `staff` SEPARATOR  ',' ) AS  `staff` FROM assign_manager")->row_array();
         $manager =  explode(',', $sql['manager']);
         $staff = explode(',',$sql['staff']);
        
     
        $data['manager'] = $this->db->query( "SELECT * FROM user WHERE id NOT IN ( '" . implode( "','", $manager ) . "' ) and role=5 and crm_name!='' and firm_admin_id=".$_SESSION['id']." ")->result_array();
        
      //  $data['staff'] = $this->db->query( "SELECT * FROM user WHERE id NOT IN ( '" .  implode( "','", $staff ) . "' ) and (role='6' or role='5') and firm_admin_id=".$_SESSION['id']."  ")->result_array(); // for old

          $data['staff'] = $this->db->query( "SELECT * FROM user WHERE id NOT IN ( '" .  implode( "','", $staff ) . "' ) and (role='6') and firm_admin_id=".$_SESSION['id']."  ")->result_array();
       
        $this->load->view('assign/assign_staff_to_manager',$data);*/
       $this->Security_model->chk_login();
       $data['assign_manager']=$this->Common_mdl->getallrecords('assign_manager');
       $sql = $this->db->query("SELECT GROUP_CONCAT(  `manager` SEPARATOR  ',' ) AS  `manager` , GROUP_CONCAT(  `staff` SEPARATOR  ',' ) AS  `staff` FROM assign_manager ")->row_array();
  
        $manager =  explode(',', $sql['manager']);
        $staff = explode(',',$sql['staff']);

        $data['manager'] = $this->db->query( "SELECT * FROM user WHERE  role=5 and crm_name!='' and firm_admin_id=".$_SESSION['id']." ")->result_array();
        $data['staff'] = $this->db->query( "SELECT * FROM user WHERE id NOT IN ( '" .  implode( "','", $staff ) . "' ) and (role='6') and firm_admin_id=".$_SESSION['id']." ")->result_array(); // only dispaly staff
       
       $this->load->view('assign/update_assign_staff_to_manager',$data);
    }

    public function update_assign_staff_to_manager($id){
       $this->Security_model->chk_login();
       $data['assign_manager']=$this->Common_mdl->select_record('assign_manager','id',$id);
        $sql = $this->db->query("SELECT GROUP_CONCAT(  `manager` SEPARATOR  ',' ) AS  `manager` , GROUP_CONCAT(  `staff` SEPARATOR  ',' ) AS  `staff` FROM assign_manager where id NOT IN( '" .$id."' )")->row_array();
  
        $manager =  explode(',', $sql['manager']);
        $staff = explode(',',$sql['staff']);

         $data['manager'] = $this->db->query( "SELECT * FROM user WHERE  role=5 and crm_name!='' and firm_admin_id=".$_SESSION['id']." ")->result_array();
      //  $data['manager'] = $this->db->query( "SELECT * FROM user WHERE id NOT IN ( '" . implode( "','", $manager ) . "' ) and role=5 and crm_name!='' and firm_admin_id=".$_SESSION['id']." ")->result_array();

        $data['staff'] = $this->db->query( "SELECT * FROM user WHERE id NOT IN ( '" .  implode( "','", $staff ) . "' ) and (role='6') and firm_admin_id=".$_SESSION['id']." ")->result_array(); // only dispaly staff
       
       $this->load->view('assign/update_assign_staff_to_manager',$data);
    }
/** 10-08-2018 for edit option manager wise staff **/
    public function get_managerwise_users(){
       $this->Security_model->chk_login();
       $get_manager=$_POST['manager'];
       $assign_manager=$this->Common_mdl->select_record('assign_manager','manager',$get_manager);
       $sql = $this->db->query("SELECT GROUP_CONCAT(  `manager` SEPARATOR  ',' ) AS  `manager` , GROUP_CONCAT(  `staff` SEPARATOR  ',' ) AS  `staff` FROM assign_manager where manager NOT IN( '" .$get_manager."' )")->row_array();
  
      //  $manager =  explode(',', $sql['manager']);
        $staff = explode(',',$sql['staff']);
        
       // $data['manager'] = $this->db->query( "SELECT * FROM user WHERE id NOT IN ( '" . implode( "','", $manager ) . "' ) and role=5 and crm_name!='' and firm_admin_id=".$_SESSION['id']." ")->result_array();

        $user_detailes = $this->db->query( "SELECT * FROM user WHERE id NOT IN ( '" .  implode( "','", $staff ) . "' ) and (role='6') and firm_admin_id=".$_SESSION['id']." ")->result_array(); // only dispaly staff
$db_staff_ids=explode(',',$assign_manager['staff']);
          $res='';
      $res.='<label>Select Staff</label><div class="dropdown-sin-2"><select name="staff[]" id="staff" multiple placeholder="Select">';
      foreach ($user_detailes as $key => $value) {
        $sel='';
        if(isset($value['id']) && in_array( $value['id'] ,$db_staff_ids )) { $sel="selected"; }
       $res.='<option value='.$value['id'].' '.$sel.' >'.$value['username'].'</option>';
      }
      $res.='</select></div>';
       echo $res;
    }
/** end of  10-08-2018 **/
    public function assign_stafftomanager()
    {
        $this->Security_model->chk_login();
        $data['manager'] = $_POST['manager'];
        $data['staff'] = implode(',',$_POST['staff']);
        $data['created_time'] = time();
        $data['create_by']=$_SESSION['id'];
        $res=$this->Common_mdl->insert('assign_manager',$data);
        $this->session->set_flashdata('success', 'Assigne Staff added Successfully');
        redirect('assign/assign_manager');
       
    }
    public function update_assign_stafftomanager($id)
    {
        $this->Security_model->chk_login();
        $data['manager'] = $_POST['manager'];
        $data['staff'] = implode(',',$_POST['staff']);
     //   $data['created_time'] = time();
      //  $data['create_by']=$_SESSION['id'];
      $get_db_manager=$this->Common_mdl->select_record('assign_manager','manager',$_POST['manager']);  
      if(count($get_db_manager) > 0)
      {
        $res=$this->Common_mdl->update('assign_manager',$data,'id',$get_db_manager['id']);
      }
      else
      {
       $data['created_time'] = time();
       $data['create_by']=$_SESSION['id'];
       $res=$this->Common_mdl->insert('assign_manager',$data); 
      }
        $this->session->set_flashdata('success', 'Assigne Staff Updated Successfully');
       // redirect('assign/update_assign_staff_to_manager/'.$id);
         redirect('assign/assign_manager');
       
    }

    public function delete($id)
    {
        $this->Security_model->chk_login();
        $this->Common_mdl->delete('assign_manager','id',$id);
        $this->session->set_flashdata('success', 'Manager Assigne Deleted Successfully');
        redirect('assign/assign_manager');
    }

    public function Managerdelete()
    {
      if(isset($_POST['data_ids']))
      {
          $id = trim($_POST['data_ids']);
          $sql = $this->db->query("DELETE FROM assign_manager WHERE id in ($id)");
          echo $id;
      }  
    }
  
  /** director **/
    public function assign_director(){
      $this->Security_model->chk_login();
      $data['assign_director']=$this->db->query("select * from assign_director where create_by=".$_SESSION['id']." ")->result_array();
      $this->load->view('assign/assign_director',$data);
    }
    public function archive_assign_director($id)
   {
    $this->db->query("update assign_director set status=1 where id=$id");
    echo $this->db->affected_rows();
   }
    public function assign_manager_to_director()
    {
        // $this->Security_model->chk_login();
        // $data['assign_director']=$this->Common_mdl->getallrecords('assign_director');
        // $sql = $this->db->query("SELECT GROUP_CONCAT(  `director` SEPARATOR  ',' ) AS  `director` , GROUP_CONCAT(  `manager` SEPARATOR  ',' ) AS  `manager` FROM assign_director")->row_array();
        //  $manager =  explode(',', $sql['manager']);
        //  $director = explode(',',$sql['director']);
        
     
        // $data['manager'] = $this->db->query( "SELECT * FROM user WHERE id NOT IN ( '" . implode( "','", $manager ) . "' ) and role=5 and crm_name!='' and firm_admin_id=".$_SESSION['id']." ")->result_array();
        
        // $data['director'] = $this->db->query( "SELECT * FROM user WHERE id NOT IN ( '" .  implode( "','", $director ) . "' ) and (role='3') and firm_admin_id=".$_SESSION['id']."  ")->result_array();
       
        // $this->load->view('assign/assign_manager_to_director',$data);
       $this->Security_model->chk_login();
       $data['assign_director']=$this->Common_mdl->getallrecords('assign_director');
        $sql = $this->db->query("SELECT GROUP_CONCAT(  `manager` SEPARATOR  ',' ) AS  `manager` , GROUP_CONCAT(  `director` SEPARATOR  ',' ) AS  `director` FROM assign_director ")->row_array();
  
        $manager =  explode(',', $sql['manager']);
        $director = explode(',',$sql['director']);

         $data['director'] = $this->db->query( "SELECT * FROM user WHERE  (role='3') and firm_admin_id=".$_SESSION['id']."  ")->result_array();
        //$data['director'] = $this->db->query( "SELECT * FROM user WHERE id NOT IN ( '" .  implode( "','", $director ) . "' ) and (role='3') and firm_admin_id=".$_SESSION['id']."  ")->result_array();

        $data['manager'] = $this->db->query( "SELECT * FROM user WHERE id NOT IN ( '" . implode( "','", $manager ) . "' ) and role=5 and crm_name!='' and firm_admin_id=".$_SESSION['id']." ")->result_array();
        
       
       
       $this->load->view('assign/update_assign_manager_to_director',$data);
    }

    public function update_assign_manager_to_director($id=false){
       $this->Security_model->chk_login();
       $data['assign_director']=$this->Common_mdl->select_record('assign_director','id',$id);
        $sql = $this->db->query("SELECT GROUP_CONCAT(  `manager` SEPARATOR  ',' ) AS  `manager` , GROUP_CONCAT(  `director` SEPARATOR  ',' ) AS  `director` FROM assign_director where id NOT IN( '" .$id."' )")->row_array();
  
        $manager =  explode(',', $sql['manager']);
        $director = explode(',',$sql['director']);

         $data['director'] = $this->db->query( "SELECT * FROM user WHERE  (role='3') and firm_admin_id=".$_SESSION['id']."  ")->result_array();
        //$data['director'] = $this->db->query( "SELECT * FROM user WHERE id NOT IN ( '" .  implode( "','", $director ) . "' ) and (role='3') and firm_admin_id=".$_SESSION['id']."  ")->result_array();

        $data['manager'] = $this->db->query( "SELECT * FROM user WHERE id NOT IN ( '" . implode( "','", $manager ) . "' ) and role=5 and crm_name!='' and firm_admin_id=".$_SESSION['id']." ")->result_array();
        
       
       
       $this->load->view('assign/update_assign_manager_to_director',$data);
    }
    public function get_directorwise_manager(){
       $this->Security_model->chk_login();
       $get_director=$_POST['director'];

       $assign_director=$this->Common_mdl->select_record('assign_director','director',$get_director);
        $sql = $this->db->query("SELECT GROUP_CONCAT(  `manager` SEPARATOR  ',' ) AS  `manager` , GROUP_CONCAT(  `director` SEPARATOR  ',' ) AS  `director` FROM assign_director where id NOT IN( '" .$assign_director['id']."' )")->row_array();
  
       $manager =  explode(',', $sql['manager']);
      //  $director = explode(',',$sql['director']);
        
      //  $data['manager'] = $this->db->query( "SELECT * FROM user WHERE role=5 and crm_name!='' and firm_admin_id=".$_SESSION['id']." ")->result_array();
        $custom_manager = $this->db->query( "SELECT * FROM user WHERE id NOT IN ( '" . implode( "','", $manager ) . "' ) and role=5 and crm_name!='' and firm_admin_id=".$_SESSION['id']." ")->result_array();
        
       // $custom_director = $this->db->query( "SELECT * FROM user WHERE id NOT IN ( '" .  implode( "','", $director ) . "' ) and (role='3') and firm_admin_id=".$_SESSION['id']."  ")->result_array();
         $db_manager_ids=explode(',',$assign_director['manager']);
         $res='';
      $res.='<label>Select Staff</label><div class="dropdown-sin-2"><select name="manager[]" id="manager" multiple placeholder="Select">';
      foreach ($custom_manager as $key => $value) {
        $sel='';
        if(isset($value['id']) && in_array( $value['id'] ,$db_manager_ids )) { $sel="selected"; }
       $res.='<option value='.$value['id'].' '.$sel.' >'.$value['username'].'</option>';
      }
      $res.='</select></div>';
       echo $res;
            
    }

    public function assign_managertodirector()
    {
        $this->Security_model->chk_login();
        $data['director'] = $_POST['director'];
        $data['manager'] = implode(',',$_POST['manager']);
        $data['created_time'] = time();
        $data['create_by']=$_SESSION['id'];
        $res=$this->Common_mdl->insert('assign_director',$data);
        $this->session->set_flashdata('success', 'Assigne Manager added Successfully');
        redirect('assign/assign_director');
       
    }
    public function update_assign_managertodirector($id)
    {
        $this->Security_model->chk_login();
        $data['director'] = $_POST['director'];
        $data['manager'] = implode(',',$_POST['manager']);
     //   $data['created_time'] = time();
      //  $data['create_by']=$_SESSION['id'];
        $get_db_director=$this->Common_mdl->select_record('assign_director','director',$_POST['director']);
        if(count($get_db_director) > 0)
        {
           $res=$this->Common_mdl->update('assign_director',$data,'id',$get_db_director['id']);
        }
        else
        {
           $data['created_time'] = time();
           $data['create_by']=$_SESSION['id'];
           $res=$this->Common_mdl->insert('assign_director',$data);
        }
        //$res=$this->Common_mdl->update('assign_director',$data,'id',$id);
        $this->session->set_flashdata('success', 'Assigne Manager Updated Successfully');
       // redirect('assign/update_assign_manager_to_director/'.$id);
         redirect('assign/assign_director');
       
    }

    public function director_assign_delete($id)
    {
        $this->Security_model->chk_login();
        $this->Common_mdl->delete('assign_director','id',$id);
        $this->session->set_flashdata('success', 'Manager Assigne Deleted Successfully');
        redirect('assign/assign_director');
    }

    public function directordelete()
    {
      if(isset($_POST['data_ids']))
      {
          $id = trim($_POST['data_ids']);
          $sql = $this->db->query("DELETE FROM assign_director WHERE id in ($id)");
          echo $id;
      }  
    }

    public function bulkArchive_manager(){
      
   $id_value=explode(',',$_POST['data_ids']);
    for($i=0;$i<count($id_value);$i++){
      $data=array('status'=>1);
        $this->Common_mdl->update('assign_manager',$data,'id',$id_value[$i]);
    }
    echo 1;
    }

     public function bulkArchive_director(){      
      $id_value=explode(',',$_POST['data_ids']);
      for($i=0;$i<count($id_value);$i++){
        $data=array('status'=>1);
        $this->Common_mdl->update('assign_director',$data,'id',$id_value[$i]);
      }
    echo 1;
    }
 
   
}
?>
