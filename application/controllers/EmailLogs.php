<?php

class EmailLogs extends CI_Controller{
	function __construct(){
		parent::__construct();
		require_once(APPPATH.'controllers/AwsMailer.php');
		$this->load->model(array('Common_mdl', 'Security_model', 'Service_Reminder_Model'));
		$this->load->helper(['template_decode', 'firm_columnSettings']);		
	}	

	public function index(){
		$this->load->view('email_logs/email_logs');		
	}

	public function QueuedReminders(){
		$this->load->view('email_logs/queued_reminders');
	}

	public function get_email_logs(){
		$limit  = $_GET['jtPageSize'];
		$offset = $_GET['jtStartIndex'];
		$order  = isset($_GET['jtSorting']) ? $_GET['jtSorting'] : "id DESC";
		$output = '';
		$sql    = "SELECT count(id) CNT FROM email";
		$data   = $this->db->query($sql)->result_array();
		$cnt    = $data[0]['CNT'];
		$data   = [];
		// $sql   	= "SELECT * FROM email WHERE `send_from` LIKE '258' ";	 	
		$sql   	= "SELECT * FROM email ";	
		$and_flag = 0; 	
		if(isset($_POST['type']) && ($_POST['type'] != ''))
	 	{
	 		$and_flag = 1;
	 		$sql   .= "WHERE relational_module = '".$_POST['type']."'";
	 	}
	 	if(isset($_POST['from_date']) && ($_POST['from_date'] != '') && isset($_POST['to_date']) && ($_POST['to_date'] != ''))
	 	{
	 		if($and_flag == 1)
	 		{
	 			$sql   .= " AND ";
	 		}
	 		else
	 		{
	 			$and_flag = 1;
	 			$sql   .= ' WHERE ';
	 		}
	 		$from_date = date('Y-m-d', strtotime($_POST['from_date']));
	 		$to_date = date('Y-m-d', strtotime($_POST['to_date']));
	 		$sql   .= "(DATE(created_at) BETWEEN '".$from_date."' AND '".$to_date."')";
	 	}
	 	if(isset($_POST['status']) && ($_POST['status'] != ''))
	 	{
	 		if($and_flag == 1)
	 		{
	 			$sql   .= " AND ";
	 		}
	 		else
	 		{
	 			$and_flag = 1;
	 			$sql   .= ' WHERE ';
	 		}
	 		$sql   .= "status = '".$_POST['status']."'";
	 	}
	 	$sql   .= ($order != "") ? " ORDER BY $order" : "";	 
		$sql   .= ($offset != "" && $limit != "") ? " LIMIT $offset, $limit" : "";
		$data   = $this->db->query($sql)->result_array();

		//Add all records to an array
		$rows   = [];
		foreach($data as $el){
			// Open email content in modal
			if($el['status'] == 0)
			{
				$el['checkbox'] = '<input type="checkbox" email-id="'.$el['id'].'" class="email-check">';
			}
			else
			{
				$el['checkbox'] = '';
			}
			
			$el['email_body']   = "<a class='view-mail-body' href='javascript:void(0)' data-id='".$el['id']."' data-email-body='".base64_encode($el['content'])."' data-toggle='modal' data-target='#emailBody'>
										<i class='fa fa-eye' aria-hidden='true'></i>
								   </a>"; 		    
 		    
 		    #Resend email
 		    $el['resend_email'] = "<a class = 'resend-email' 
 		    						  href = 'javascript:void(0)' 
 		    						  data-id='".$el['id']."'
 		    						  data-email-to='".$el['send_to']."' 
 		    						  data-email-subject='".$el['subject']."'
 		    						  data-email-body='".base64_encode($el['content'])."'>
										<i class='fa fa-paper-plane' aria-hidden='true'></i>
								   </a>";

			#Add custom items to the list			
			$smtp_response = json_decode($el['smtp_response'], true);
			if (isset($smtp_response['result']) && $smtp_response['result'] != ""){
				$smtp_response_result = $smtp_response['result'];
			}else{
				$smtp_response_result = "";
			}

			if (isset($smtp_response['message']) && $smtp_response['message'] != ""){
				$smtp_response_message = $smtp_response['message'];
			}else{
				$smtp_response_message = "";
			}

			$smtp_response_output = "Result: ".$smtp_response_result."<br/><span class='smtpmessage'>Message: ".$smtp_response_message."</span>";
			$el['smtp_response']  = $smtp_response_output;
			$status               = $el['status'];
			$status_output        = "Queued";

			if ($status == 1){
				$status_output = 'Sent';
			}else if ($status == 2){
				$status_output = 'Failed';
			}else{
				$status_output = 'Queued';
			}

			$el['status'] = $el['status']." - ".$status_output;
			$sendto       = json_decode($el['send_to'], true);

			if (is_array($sendto)){
				if (isset($sendto['to'])){
					$to = (is_array($sendto['to'])) ? implode(", ", $sendto['to']): $sendto['to'];  					
				}
				if (isset($sendto['cc'])){
					$cc = (is_array($sendto['cc'])) ? implode(", ", $sendto['cc']): $sendto['cc'];  					
				}
				$el['send_to'] = "To: ".$to."<br/>"."Cc: ".$cc;
			}		
			unset($el['content']);	
 		    $rows[] = $el;
		}
		 
		//Return result to jTable
		$jTableResult            		  = [];		
		$jTableResult['Result']  		  = "OK";
		$jTableResult['TotalRecordCount'] = $cnt;
		$jTableResult['Records'] 		  = $rows;		
		
		echo json_encode($jTableResult);	
	}

	public function get_queued_reminders(){
		$limit  = $_GET['jtPageSize'];
		$offset = $_GET['jtStartIndex'];
		$order  = isset($_GET['jtSorting']) ? $_GET['jtSorting'] : "id DESC";
		$output = '';
		$sql    = "SELECT count(id) CNT FROM service_reminder_cron_data";
		$data   = $this->db->query($sql)->result_array();
		$cnt    = $data[0]['CNT'];
		$data   = [];
		// $sql   	= "SELECT * FROM email WHERE `send_from` LIKE '258' ";	 	
		$sql   	= "SELECT a.id,b.service_name,c.crm_company_name,a.client_contacts_id,a.template_type,a.template_id,a.status,a.frequency, a.date,a.client_id  FROM `service_reminder_cron_data` AS a INNER JOIN service_lists AS b on b.id = a.service_id INNER JOIN client AS c on c.id = a.client_id ";	
		$and_flag = 0; 	
		if(isset($_POST['type']) && ($_POST['type'] != ''))
	 	{
	 		$and_flag = 1;
	 		$sql   .= "WHERE relational_module = '".$_POST['type']."'";
	 	}
	 	if(isset($_POST['from_date']) && ($_POST['from_date'] != '') && isset($_POST['to_date']) && ($_POST['to_date'] != ''))
	 	{
	 		if($and_flag == 1)
	 		{
	 			$sql   .= " AND ";
	 		}
	 		else
	 		{
	 			$and_flag = 1;
	 			$sql   .= ' WHERE ';
	 		}
	 		$sql   .= "UNIX_TIMESTAMP(STR_TO_DATE(  `date` ,'%d-%m-%Y' ) ) > UNIX_TIMESTAMP(STR_TO_DATE(  '".$_POST['from_date']."' ,'%d-%m-%Y' )) AND UNIX_TIMESTAMP(STR_TO_DATE(  `date` ,'%d-%m-%Y' ) ) <= UNIX_TIMESTAMP(STR_TO_DATE(  '".$_POST['to_date']."' ,'%d-%m-%Y' ))";
	 	}
	 	if(isset($_POST['status']) && ($_POST['status'] != ''))
	 	{
	 		if($and_flag == 1)
	 		{
	 			$sql   .= " AND ";
	 		}
	 		else
	 		{
	 			$and_flag = 1;
	 			$sql   .= ' WHERE ';
	 		}
	 		$sql   .= "a.status = '".$_POST['status']."'";
	 	}
	 	$sql   .= ($order != "") ? " ORDER BY $order" : "";	 
		$sql   .= ($offset != "" && $limit != "") ? " LIMIT $offset, $limit" : "";
		$data   = $this->db->query($sql)->result_array();
		//Add all records to an array
		$rows   = [];
		foreach($data as $el){
			// Open email content in modal
			if($el['status'] == 0)
			{
				$el['checkbox'] = '<input type="checkbox" email-id="'.$el['id'].'" class="email-check">';
			}
			else
			{
				$el['checkbox'] = '';
			}

			if( $el['template_type'] !='0' )
			{
				$Template = $this->db->query("SELECT subject,body,headline_color FROM custom_service_reminder WHERE id=".$el['template_id'])->row_array();
			}
			else
			{
				$Template = $this->db->query("SELECT subject,message as body,headline_color  FROM reminder_setting WHERE id=".$el['template_id'])->row_array();
			}	    
 		    $Content['subject'] = $Template['subject'];
 		    $Content['body']    = $Template['body'];
 		    if( ! function_exists('Decode_ClientTable_Datas') )
			$this->load->helper('template_decode');
			$client_contacts_id = 	$el['client_contacts_id'];
			$client_id   		=	$el['client_id'];
			$DATA = Decode_ClientTable_Datas( $client_id , $Content , $client_contacts_id );

			$el['subject'] = $DATA['subject'];
 		    #Resend email
 		    $el['resend_email'] = '<button type="button" class="btn-success btn-sm send_mail_task" id="mark_sent_task" data-email="0" reminder_id="">Mark Sent & Create Task <i class="fa fa-paper-plane" aria-hidden="true"></i></button>
				<button type="button" class="btn-success btn-sm send_mail_task" id="send_task" data-email="1" reminder_id="'.$el['id'].'">Send & Create Task <i class="fa fa-paper-plane" aria-hidden="true"></i></button>';

			#Add custom items to the list			
			$smtp_response = json_decode($el['smtp_response'], true);
			if (isset($smtp_response['result']) && $smtp_response['result'] != ""){
				$smtp_response_result = $smtp_response['result'];
			}else{
				$smtp_response_result = "";
			}

			if (isset($smtp_response['message']) && $smtp_response['message'] != ""){
				$smtp_response_message = $smtp_response['message'];
			}else{
				$smtp_response_message = "";
			}

			$status               = $el['status'];
			$status_output        = "Queued";

			if ($status == 1){
				$status_output = 'Sent';
			}else if ($status == 2){
				$status_output = 'Stopped';
			}else if (($status == 3)||($status == 4)){
				$status_output = 'Not Deliverable';
			}else{
				$status_output = 'Queued';
			}

			$el['status'] = $el['status']." - ".$status_output;
						
 		    $rows[] = $el;
		}
		 
		//Return result to jTable
		$jTableResult            		  = [];		
		$jTableResult['Result']  		  = "OK";
		$jTableResult['TotalRecordCount'] = $cnt;
		$jTableResult['Records'] 		  = $rows;		
		
		echo json_encode($jTableResult);	
	}

	public function delete_email_logs(){
		//Delete record from database
		$record_id = $_POST['id'];
		$sql       = "DELETE FROM email WHERE id = $record_id";
		
		$this->db->query($sql);		
		//Return result to jTable
		$jTableResult           = [];
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}

	public function update_email_logs(){
		//Update record in database
		$sql    = "UPDATE email 
					  SET send_to = '".$_POST['send_to']."', 
					  	  subject = '".$_POST['subject']."',
					  	  status  = ".$_POST['status']."
				   WHERE id = ".$_POST['id'].";";
		$result = $this->db->query($sql);
		 
		//Return result to jTable
		$jTableResult           = [];
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}	

	public function send_checked_emails()
	{
		if(isset($_POST['email_ids']))
		{
			$ids = $_POST['email_ids'];
			$reminders = [];
			foreach ($ids as $id) {
				$sql   	= "SELECT * FROM email WHERE id = ".$id;
				$data   = $this->db->query($sql)->row_array();	
				if(isset($data['status']) && ($data['status'] == 0) && ($data['relational_module'] == 'service_reminder'))
				{
					$sql   	= "SELECT * FROM service_reminder_cron_data WHERE id = ".$data['relational_module_id'];
					$reminder   = $this->db->query($sql)->row_array();
					if(isset($reminder['status']))
					{
						$reminder['email_id'] = $id;
						$reminders[] = $reminder;
					}
				}
			}
			$this->firm_exc_sr_log .= "Today service reminders\n".json_encode( $reminders )."\n";

			$return['result'] = 0;

			foreach ( $reminders as $key => $reminder_data )
			{
				
				$query = "SELECT FS.* 
	              FROM admin_setting as FS 
	              INNER JOIN firm as F 
	                      ON F.firm_id=FS.firm_id 
	                     AND F.is_deleted!=1 
	              WHERE FS.firm_id!=".$reminder_data['firm_id'];

	            $firm_Settings = $this->db->query( $query )->result_array();

	            $Firm_Data = [];

	            if(isset($firm_Settings))
	            {
	            	$Firm_Data = $firm_Settings[0];
	            }



				$check_task_query_status=1;



				if( $reminder_data['status']==0 && $check_task_query_status==1)
				{
					$this->firm_exc_sr_log .= "inside reminder progress condtion\n";

					$client_service_contact_cond  = [
													'client_id' 		=> 	$reminder_data['client_id'] ,
													'service_id'		=>	$reminder_data['service_id'],
													'client_contacts_id'=>	$reminder_data['client_contacts_id']
												];

					$frequency_data = $this->db->where( $client_service_contact_cond )
											   ->get( "client_service_frequency_due" )
											   ->row_array();
				
					if( $frequency_data['is_create_task'] == 0  )
					{	
						$this->firm_exc_sr_log .= "TASK not created yet\n";
						$this->Service_Reminder_Model->Create_ServiceTask( $reminder_data );
						$this->firm_exc_sr_log .= "Task created\n";
					}

					$query = "SELECT c.user_id,c.id,u.status 
								FROM client as c  
									INNER JOIN user as u 
										ON u.id=c.user_id 
									WHERE c.id=".$reminder_data['client_id'];

					$client_data = $this->db->query( $query )->row_array();

					$this->firm_exc_sr_log .= "client data".json_encode( $client_data )."\n";

					$Reminder_SendAble_Status = [];

					if(isset($Firm_Data['reminder_send_status']))
					{
						$Reminder_SendAble_Status		=	json_decode( $Firm_Data['reminder_send_status'] , true );
					}

					if( $reminder_data['template_id'] !='' && in_array( $client_data['status'] , $Reminder_SendAble_Status ) )
					{

						
						$this->firm_exc_sr_log .= "Inside reminder creatable client status\n";
						
						$this->db->select("main_email,work_email");

						if( $reminder_data['client_contacts_id'] != 0 )
							$this->db->where( "id=".$reminder_data['client_contacts_id'] );
						else 
							$this->db->where( "client_id=".$client_data['user_id']." AND make_primary=1" );

						$contact_details = $this->db->get("client_contacts")->row_array();

						$contact_details['mails'][] 	=	$contact_details['main_email']; 

						if( !empty( $contact_details['work_email'] ) )
						{
							$work_email            	   = json_decode( $contact_details['work_email'] , true );

							$contact_details['mails']  = array_merge( $contact_details['mails'] , $work_email );
						}

						if( !empty( $contact_details['mails'] ) && $Firm_Data['service_reminder'] == 1 )
						{				
							$client 				= [ 
														'client_id' 			=>	$reminder_data['client_id'],
														'client_contacts_id'	=>	$reminder_data['client_contacts_id']
													];

							$aws_mailer = new AwsMailer();
							$emails = $this->db
					                  ->where( "id=".$reminder_data['email_id'] )
					                  ->limit(50)
					                  ->get( "email" )
					                  ->result_array();

							
							
	                  		foreach ($emails as $key => $emails_data ){

								$emailData    = [
								'email_to'      => $emails_data['send_to'],
								'email_subject' => $emails_data['subject'],
								'email_body'    => $emails_data['content']            
								];
					          
								$IsSend       = $aws_mailer->prepare_email_to_send($emailData, false);
								$update_data  = [
								'smtp_response' => json_encode( $IsSend ),
								'status'        => ($IsSend['result'] == 1 || $IsSend['result'] == '1') ? 1 : 2
								];

								if($this->db->update( 'email' , $update_data ,'id='.$emails_data['id'] ))
								{
									$emails_data['status'] = 1;
								}


								if( $emails_data['relational_module']  == 'service_reminder' ){
									$this->Service_Reminder_Model->update_reminder_status( $emails_data );            
								}

						      }
							$return['result'] = 1;
						}
						else
						{
							$this->firm_exc_sr_log .= "client mail empty or service reminder off in firm settings\n";
						}		
					}
					else
					{
						$this->firm_exc_sr_log .= "client status differ from serminder sendable status\n";
						$this->db->update( 'service_reminder_cron_data' , [ 'status'=> 3 ] , "id=".$reminder_data['id'] );
					}
				}
				else
				{
					$this->firm_exc_sr_log .= "inside reminder stop condtion\n";
				}

				if( $reminder_data['create_records'] == 1 )
				{
					$this->firm_exc_sr_log 	.= "inside Create recordes condition\n";

					$this->Service_Reminder_Model->Setup_Next_Frequency_Data( $reminder_data['client_id'] , $reminder_data['service_id'] , $reminder_data['client_contacts_id'] );

					$this->firm_exc_sr_log 	.= "Setup next frequncy reminders\n";
				}

				$this->firm_exc_sr_log .= $key." Reminder Execution End\n";
			}

		}
		echo json_encode($return);
	}
} ?>