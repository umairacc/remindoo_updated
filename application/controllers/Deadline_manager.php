<?php
error_reporting(E_STRICT);
defined('BASEPATH') or exit('No direct script access allowed');

class Deadline_manager extends CI_Controller
{

  public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   *      http://example.com/index.php/welcome
   *  - or -
   *      http://example.com/index.php/welcome/index
   *  - or -
   * Since this controller is set as the default controller in
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see https://codeigniter.com/user_guide/general/urls.html
   */
  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('Common_mdl', 'Security_model', 'Invoice_model', 'Service_Reminder_Model'));
    $this->load->helper(['comman']);
  }

  /* Modified By Shashmethah*/

  public function index()
  {
    $this->Security_model->chk_login();

    if ($_SESSION['permission']['Deadline_manager']['view'] == '1') {
      $data = $this->getRecordsByType('', '', '', 'due');
      $id   = $_SESSION['id'];

      if (isset($_POST['date'])) {
        $month = '';
        $year  = '';
      } else {
        $month = date('m');
        $year  = date('Y');
      }

      $data['start_end_date']     = $this->start_end_date('01-' . $month . '-' . $year);
      $exp_date                   = explode(',', $data['start_end_date']);
      $start                      = $exp_date[0];
      $end                        = $exp_date[1];
      $data['start_month']        = $this->month_cnt($start);
      $data['end_month']          = $this->month_cnt($end);
      $data['total_count_month']  = $this->weeks($month, $year);
      $Assigned_Task              = Get_Assigned_Datas('TASK');
      $assigned_task              = implode(',', $Assigned_Task);
      /*$sql                        = 'SELECT * 
                                    FROM `add_new_task` 
                                    WHERE firm_id = "' . $_SESSION['firm_id'] . '" 
                                    AND FIND_IN_SET(id,"' . $assigned_task . '") 
                                    ORDER BY id desc 
                                    Limit 0,5';*/                                    
      $sql                        = "SELECT * 
                                    FROM add_new_task 
                                    WHERE firm_id = ".$_SESSION['firm_id']." 
                                    AND id IN($assigned_task) 
                                    ORDER BY id desc 
                                    Limit 0,5";
      $data['task_list']          = $this->db->query($sql)->result_array();      
      $high                       = 0;
      $low                        = 0;
      $medium                     = 0;
      $super_urgent               = 0;
      $tot_tim[]                  = '';

      foreach ($data['task_list'] as $task_key => $task_value) {
        $tot_tim[]    = $task_value['timer_status'];
        $task_status  = $task_value['priority'];

        if ($task_status == 'high') {
          $high++;
        } else if ($task_status == 'low') {
          $low++;
        } else if ($task_status == 'medium') {
          $medium++;
        } else if ($task_status == 'super_urgent') {
          $super_urgent++;
        }
      }

      $tot_time               = array_sum($tot_tim);
      $task_cnt               = count($tot_tim);
      $data['high']           = $high;
      $data['low']            = $low;
      $data['medium']         = $medium;
      $data['super_urgent']   = $super_urgent;
      $data['tot_tim']        = $tot_time;
      $data['task_cnt']       = $task_cnt;
      $data['user_activity']  = $this->Common_mdl->orderby('activity_log', 'user_id', $id, 'desc');
      $data['custom_form']    = $this->Common_mdl->select_record('staff_custom_form', 'user_id', $id);
      $data['firm_currency']  = $this->Common_mdl->get_price('admin_setting', 'firm_id', $_SESSION['firm_id'], 'crm_currency');      

      $this->load->view('Deadline_manager/deadline_info_new', $data);
      unset($_SESSION['success_msg']);
    } else {
      $this->load->view('users/blank_page');
    }
  }

  public function company_search_deadline_info()
  {
    $company = $_POST['company'];
    $company_type = $_POST['company_type'];
    $deadline_type = $_POST['deadline_type'];
    $due_val = $_POST['due_val'];

    $data = $this->getRecordsByType($company, $company_type, $deadline_type, $due_val);

    $data['due_val'] = $due_val;
    $data['deadline_type'] = $deadline_type;
    $this->load->view('Deadline_manager/deadline_info_company_search_new', $data);
  }

  public function getRecordsByType($company, $company_type, $deadline_type, $due_val)
  {
    if ($company_type != '') {
      $cond = 'and crm_legal_form="' . $company_type . '"';
    } else {
      $cond = '';
    }

    if (empty($due_val)) {
      $data['due_val'] = 'due';
    } else {
      $data['due_val'] = $due_val;
    }

    if (!empty($company)) {
      $company = 'and `crm_company_name` like "%' . $company . '%"';
    }

    $id                   = $_SESSION['id'];
    $data['user_details'] = $this->Common_mdl->select_record('user', 'id', $id);
    $Assigned_Client      = Get_Assigned_Datas('CLIENT');
    $assigned_client      = implode(',', $Assigned_Client);

    foreach ($Assigned_Client as $key => $value) {
      $rec = $this->Invoice_model->selectRecord('client', 'id', $value);

      if (!empty($rec['user_id'])) {
        $arr[] = $rec['user_id'];
      }
    }

    $clients_user_ids             = implode(',', $arr);
    $data['create_clients_list']  = $this->db->query("SELECT * from user where (firm_id='" . $_SESSION['firm_id'] . "' AND user_type = 'FC' AND FIND_IN_SET(id,'" . $clients_user_ids . "')) and crm_name != '' ORDER BY id DESC")->result_array();
    $data['getCompany']           = $this->db->query("SELECT * FROM client WHERE (firm_id='" . $_SESSION['firm_id'] . "') and crm_company_name !='' AND FIND_IN_SET(id,'" . $assigned_client . "') ORDER BY id DESC")->result_array();
    $current_date                 = date("Y-m-d");
    $time                         = date("Y-m-d");
    $service_cond                 = $this->Common_mdl->getServicedue($current_date);
    $data['getCompany_today']     = $this->db->query('SELECT * FROM client WHERE (firm_id="' . $_SESSION['firm_id'] . '") AND FIND_IN_SET(id,"' . $assigned_client . '") ' . $company . ' ' . $cond . ' ' . $service_cond . ' ORDER BY id DESC')->result_array();

    if ($due_val == 'over_due') {
      $oneweek_end_date = date('Y-m-d', strtotime($time . ' +1 week'));
      $service_cond = $this->Common_mdl->getServicedue($current_date, $oneweek_end_date);
    } else {
      $oneweek_end_date = date('Y-m-d', strtotime($time . ' -1 week'));
      $service_cond = $this->Common_mdl->getServicedue($oneweek_end_date, $current_date);
    }

    $data['getCompany_oneweek'] = $this->db->query('SELECT * FROM client WHERE (firm_id="' . $_SESSION['firm_id'] . '") AND FIND_IN_SET(id,"' . $assigned_client . '") ' . $company . ' ' . $cond . ' ' . $service_cond . ' ORDER BY id DESC')->result_array();

    if ($due_val == 'over_due') {
      $onemonth_end_date = date('Y-m-d', strtotime($time . ' +1 month'));
      $service_cond = $this->Common_mdl->getServicedue($current_date, $onemonth_end_date);
    } else {
      $onemonth_end_date = date('Y-m-d', strtotime($time . ' -1 month'));
      $service_cond = $this->Common_mdl->getServicedue($onemonth_end_date, $current_date);
    }

    $data['getCompany_onemonth'] = $this->db->query('SELECT * FROM client WHERE (firm_id="' . $_SESSION['firm_id'] . '") AND FIND_IN_SET(id,"' . $assigned_client . '") ' . $company . ' ' . $cond . ' ' . $service_cond . ' ORDER BY id DESC')->result_array();

    if ($due_val == 'over_due') {
      $oneyear_end_date = date('Y-m-d', strtotime($time . ' +1 year'));
      $service_cond = $this->Common_mdl->getServicedue($current_date, $oneyear_end_date);
    } else {
      $oneyear_end_date = date('Y-m-d', strtotime($time . ' -1 year'));
      $service_cond = $this->Common_mdl->getServicedue($oneyear_end_date, $current_date);
    }

    $data['getCompany_oneyear'] = $this->db->query('SELECT * FROM client WHERE (firm_id="' . $_SESSION['firm_id'] . '") AND FIND_IN_SET(id,"' . $assigned_client . '") ' . $company . ' ' . $cond . ' ' . $service_cond . ' ORDER BY id DESC')->result_array();

    return $data;
  }

  public function companysearch()
  {
    $com_num = $_POST['com_num'];
    $legal_form = $_POST['legal_form'];

    if (!empty($_POST['values']) && !empty($_POST['fields'])) {
      $for_values = explode(',', $_POST['values']);
      $for_fields = explode(',', $_POST['fields']);
    } else {
      $for_values = "";
      $for_fields = "";
    }

    $qy = $this->getStatusBoard_Query($com_num, $legal_form, $for_values, $for_fields);
    $getCompany = $this->db->query($qy)->result_array();

    $data['service_charges'] = $this->Common_mdl->getClientsServiceCharge($getCompany);
    $data['service_list'] = $this->Common_mdl->getServicelist();

    $this->load->view('Deadline_manager/filter_rec', $data);
  }

  public function getStatusBoard_Query($com_num, $legal_form, $for_values, $for_fields)
  {
    $ids = array();

    $Assigned_Client = Get_Assigned_Datas('CLIENT');
    $assigned_client = implode(',', $Assigned_Client);

    $other_services = $this->db->query('SELECT * FROM service_lists WHERE id>16')->result_array();

    if (!empty($for_values) && !empty($for_fields)) {
      $getCompany = $this->db->query("SELECT * FROM client WHERE (firm_id='" . $_SESSION['firm_id'] . "' AND FIND_IN_SET(id,'" . $assigned_client . "')) and crm_company_name !=''")->result_array();

      foreach ($getCompany as $getCompanykey => $getCompanyvalue) {
        if ($getCompanyvalue['blocked_status'] == 0) {
          $conf_statement = (isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnCst =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnCst = 'off';

          $accounts = (isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnAcc =  json_decode($getCompanyvalue['accounts'])->tab : $jsnAcc = 'off';

          $company_tax_return = (isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsntax =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsntax = 'off';

          $personal_tax_return = (isset(json_decode($getCompanyvalue['personal_tax_return'])->tab) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnp_tax =  json_decode($getCompanyvalue['personal_tax_return'])->tab : $jsnp_tax = 'off';

          $payroll = (isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpay =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpay = 'off';

          $workplace = (isset(json_decode($getCompanyvalue['workplace'])->tab) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->tab : $jsnworkplace = 'off';

          $vat = (isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = 'off';

          $cis = (isset(json_decode($getCompanyvalue['cis'])->tab) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->tab : $jsncis = 'off';

          $cissub = (isset(json_decode($getCompanyvalue['cissub'])->tab) && $getCompanyvalue['cissub'] != '') ? $jsncis_sub =  json_decode($getCompanyvalue['cissub'])->tab : $jsncis_sub = 'off';

          $p11d = (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = 'off';

          $bookkeep = (isset(json_decode($getCompanyvalue['bookkeep'])->tab) && $getCompanyvalue['bookkeep'] != '') ? $jsnbk =  json_decode($getCompanyvalue['bookkeep'])->tab : $jsnbk = 'off';

          $management = (isset(json_decode($getCompanyvalue['management'])->tab) && $getCompanyvalue['management'] != '') ? $jsnmgnt =  json_decode($getCompanyvalue['management'])->tab : $jsnmgnt = 'off';

          $investgate = (isset(json_decode($getCompanyvalue['investgate'])->tab) && $getCompanyvalue['investgate'] != '') ? $jsninvest =  json_decode($getCompanyvalue['investgate'])->tab : $jsninvest = 'off';

          $registered = (isset(json_decode($getCompanyvalue['registered'])->tab) && $getCompanyvalue['registered'] != '') ? $jsnreg =  json_decode($getCompanyvalue['registered'])->tab : $jsnreg = 'off';

          $taxadvice = (isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxad =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxad = 'off';

          $taxinvest = (isset(json_decode($getCompanyvalue['taxinvest'])->tab) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->tab : $jsntaxinvest = 'off';


          $new_val = array();

          foreach (array_combine($for_fields, $for_values) as $fields => $v_id) {
            if ($v_id == 'true') {
              $s_val = 'on';
            } else {
              $s_val = 'off';
            }
            if ($fields == 'conf_statement') {
              $f_val = $conf_statement;
            } elseif ($fields == 'accounts') {
              $f_val = $accounts;
            } elseif ($fields == 'company_tax_return') {
              $f_val = $company_tax_return;
            } elseif ($fields == 'personal_tax_return') {
              $f_val = $personal_tax_return;
            } elseif ($fields == 'payroll') {
              $f_val = $payroll;
            } elseif ($fields == 'workplace') {
              $f_val = $workplace;
            } elseif ($fields == 'vat') {
              $f_val = $vat;
            } elseif ($fields == 'cis') {
              $f_val = $cis;
            } elseif ($fields == 'cissub') {
              $f_val = $cissub;
            } elseif ($fields == 'p11d') {
              $f_val = $p11d;
            } elseif ($fields == 'bookkeep') {
              $f_val = $bookkeep;
            } elseif ($fields == 'management') {
              $f_val = $management;
            } elseif ($fields == 'investgate') {
              $f_val = $investgate;
            } elseif ($fields == 'registered') {
              $f_val = $registered;
            } elseif ($fields == 'taxadvice') {
              $f_val = $taxadvice;
            } elseif ($fields == 'taxinvest') {
              $f_val = $taxinvest;
            } else if (in_array($fields, array_column($other_services, 'services_subnames'))) {
              $f_val = (isset(json_decode($getCompanyvalue[$fields])->tab) && $getCompanyvalue[$fields] != '') ? json_decode($getCompanyvalue[$fields])->tab : 'off';
            } else {
              $f_val = '';
            }

            if ($v_id == 'true') {
              if ($f_val == $s_val) {
                array_push($new_val, 'on');
              } else {
                array_push($new_val, 'off');
              }
            } else {
              array_push($new_val, 'off');
            }
          }

          if (!in_array('off', $new_val)) {
            $ids[] = $getCompanyvalue['id'];
          }
        }
      }

      $qy = "SELECT * FROM client WHERE (firm_id='" . $_SESSION['firm_id'] . "' AND FIND_IN_SET(id,'" . implode(",", $ids) . "')) and crm_company_name !=''";
    } else {
      $qy = "SELECT * FROM client WHERE (firm_id='" . $_SESSION['firm_id'] . "' AND FIND_IN_SET(id,'" . $assigned_client . "')) and crm_company_name !=''";
    }

    if ($com_num != '' && $legal_form != '') {
      $qy .= "and crm_company_name like '%$com_num%' and crm_legal_form = '$legal_form'";
    } elseif ($com_num != '' && $legal_form == '') {
      $qy .= "and crm_company_name like '%$com_num%' ";
    } elseif ($com_num == '' && $legal_form != '') {
      $qy .= "and crm_legal_form = '$legal_form' ";
    }

    return $qy . " ORDER BY id DESC";
  }

  function pdf_new()
  {
    $company = $_GET['company'];
    $company_type = $_GET['company_type'];
    $deadline_type = $_GET['deadline_type'];
    $due_val = $_GET['due_val'];

    $data = $this->getRecordsByType($company, $company_type, $deadline_type, $due_val);

    $data['due_val'] = $due_val;
    $data['deadline_type'] = $deadline_type;

    $this->load->library('pdf');
    $customPaper = array(0, 0, 1000, 1000);
    $this->pdf->set_paper($customPaper);
    $this->pdf->load_view('Deadline_manager/pdf_view_new_filter', $data);
    $this->pdf->render();
    $this->pdf->stream("Deadline_" . date('dmY') . ".pdf");
  }

  function html_new()
  {
    $company = $_GET['company'];
    $company_type = $_GET['company_type'];
    $deadline_type = $_GET['deadline_type'];
    $due_val = $_GET['due_val'];

    $data = $this->getRecordsByType($company, $company_type, $deadline_type, $due_val);

    $data['due_val'] = $due_val;
    $data['deadline_type'] = $deadline_type;

    $this->load->view('Deadline_manager/deadline_print_page_new', $data);
  }

  public function deadline_exportCSV_new()
  {
    $company = $_GET['company'];
    $company_type = $_GET['company_type'];
    $deadline_type = $_GET['deadline_type'];
    $due_val = $_GET['due_val'];

    $data = $this->getRecordsByType($company, $company_type, $deadline_type, $due_val);

    $data['due_val'] = $due_val;
    $data['deadline_type'] = $deadline_type;
    header("Content-Description: File Transfer");

    $filename = 'deadline_' . date('Ymd') . '.csv';
    header("Content-Disposition: attachment; filename=" . $filename . "");
    header("Content-Type: application/csv; ");

    $new_array = array();
    $new_array_week = array();
    $new_array_month = array();
    $new_array_year = array();


    $file = fopen('php://output', 'w');
    $header = array("Today");
    fputcsv($file, $header);
    $header = array("Client Name", "Client Type", "Company Name", "Deadline Type", "Date");
    fputcsv($file, $header);
    $i = 0;

    $current_date = date("Y-m-d");
    $other_services = $this->db->query('SELECT * FROM service_lists WHERE id>16')->result_array();

    foreach ($data['getCompany_today'] as $getCompanykey => $getCompanyvalue) {
      $getusername = $this->Common_mdl->select_record('user', 'id', $getCompanyvalue['user_id']);

      if (($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date']))) && ($deadline_type == 'Confirmation statement' || $deadline_type == '')) {

        $new_array[$getCompanykey][1]['crm_name'] = $getusername['crm_name'];
        $new_array[$getCompanykey][1]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array[$getCompanykey][1]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array[$getCompanykey][1]['crm_deadline_types'] = "Confirmation statement Due Date";
        $new_array[$getCompanykey][1]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) : '');

        fputcsv($file, $new_array[$getCompanykey][1]);
      }

      if (($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due']))) && ($deadline_type == 'Accounts' || $deadline_type == '')) {

        $new_array[$getCompanykey][2]['crm_name'] = $getusername['crm_name'];
        $new_array[$getCompanykey][2]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array[$getCompanykey][2]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array[$getCompanykey][2]['crm_deadline_types'] = "Accounts Due Date";

        $new_array[$getCompanykey][2]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) : '');


        fputcsv($file, $new_array[$getCompanykey][2]);
      }

      if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && ($deadline_type == 'Company Tax Return' || $deadline_type == '')) {

        $new_array[$getCompanykey][3]['crm_name'] = $getusername['crm_name'];
        $new_array[$getCompanykey][3]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];
        $new_array[$getCompanykey][3]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array[$getCompanykey][3]['crm_deadline_types'] = "Company Tax Return Due Date";
        $new_array[$getCompanykey][3]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) : '');


        fputcsv($file, $new_array[$getCompanykey][3]);
      }

      if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) && ($deadline_type == 'Personal Tax Return' || $deadline_type == '')) {

        $new_array[$getCompanykey][4]['crm_name'] = $getusername['crm_name'];
        $new_array[$getCompanykey][4]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array[$getCompanykey][4]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array[$getCompanykey][4]['crm_deadline_types'] = "Personal Tax Return Due Date";
        $new_array[$getCompanykey][4]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_personal_due_date_return']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) : '');


        fputcsv($file, $new_array[$getCompanykey][4]);
      }

      if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && ($deadline_type == 'VAT' || $deadline_type == '')) {

        $new_array[$getCompanykey][5]['crm_name'] = $getusername['crm_name'];
        $new_array[$getCompanykey][5]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array[$getCompanykey][5]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array[$getCompanykey][5]['crm_deadline_types'] = "VAT Due Date";
        $new_array[$getCompanykey][5]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_vat_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) : '');


        fputcsv($file, $new_array[$getCompanykey][5]);
      }

      if (($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline']))) && ($deadline_type == 'Payroll' || $deadline_type == '')) {

        $new_array[$getCompanykey][6]['crm_name'] = $getusername['crm_name'];
        $new_array[$getCompanykey][6]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array[$getCompanykey][6]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array[$getCompanykey][6]['crm_deadline_types'] = "Payroll Due Date";
        $new_array[$getCompanykey][6]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_rti_deadline']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) : '');


        fputcsv($file, $new_array[$getCompanykey][6]);
      }

      if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && ($deadline_type == 'WorkPlace Pension - AE' || $deadline_type == '')) {

        $new_array[$getCompanykey][7]['crm_name'] = $getusername['crm_name'];
        $new_array[$getCompanykey][7]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array[$getCompanykey][7]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array[$getCompanykey][7]['crm_deadline_types'] = "WorkPlace Pension - AE Due Date";
        $new_array[$getCompanykey][7]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_pension_subm_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) : '');


        fputcsv($file, $new_array[$getCompanykey][7]);
      }

      if ($deadline_type == 'CIS - Contractor' || $deadline_type == '') {

        $new_array[$getCompanykey][8]['crm_name'] = $getusername['crm_name'];
        $new_array[$getCompanykey][8]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array[$getCompanykey][8]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array[$getCompanykey][8]['crm_deadline_types'] = "CIS - Contractor Due Date";
        $new_array[$getCompanykey][8]['crm_deadline_date'] = "-";
        fputcsv($file, $new_array[$getCompanykey][8]);
      }
      if ($deadline_type == 'CIS - Sub Contractor' || $deadline_type == '') {

        $new_array[$getCompanykey][9]['crm_name'] = $getusername['crm_name'];
        $new_array[$getCompanykey][9]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array[$getCompanykey][9]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array[$getCompanykey][9]['crm_deadline_types'] = "CIS - Sub Contractor Due Date";
        $new_array[$getCompanykey][9]['crm_deadline_date'] = "-";
        fputcsv($file, $new_array[$getCompanykey][9]);
      }
      if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && ($deadline_type == 'P11D' || $deadline_type == '')) {

        $new_array[$getCompanykey][10]['crm_name'] = $getusername['crm_name'];
        $new_array[$getCompanykey][10]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array[$getCompanykey][10]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array[$getCompanykey][10]['crm_deadline_types'] = "P11D Due Date";
        $new_array[$getCompanykey][10]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_next_p11d_return_due']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) : '');


        fputcsv($file, $new_array[$getCompanykey][10]);
      }


      if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && ($deadline_type == 'Management Accounts' || $deadline_type == '')) {

        $new_array[$getCompanykey][11]['crm_name'] = $getusername['crm_name'];
        $new_array[$getCompanykey][11]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array[$getCompanykey][11]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array[$getCompanykey][11]['crm_deadline_types'] = "Management Accounts Due Date";
        $new_array[$getCompanykey][11]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_next_manage_acc_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) : '');


        fputcsv($file, $new_array[$getCompanykey][11]);
      }

      if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && ($deadline_type == 'Bookkeeping' || $deadline_type == '')) {

        $new_array[$getCompanykey][12]['crm_name'] = $getusername['crm_name'];
        $new_array[$getCompanykey][12]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array[$getCompanykey][12]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array[$getCompanykey][12]['crm_deadline_types'] = "Bookkeeping Due Date";
        $new_array[$getCompanykey][12]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_next_booking_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) : '');


        fputcsv($file, $new_array[$getCompanykey][12]);
      }

      if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && ($deadline_type == 'Investigation Insurance' || $deadline_type == '')) {

        $new_array[$getCompanykey][13]['crm_name'] = $getusername['crm_name'];
        $new_array[$getCompanykey][13]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array[$getCompanykey][13]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array[$getCompanykey][13]['crm_deadline_types'] = "Investigation Insurance Due Date";
        $new_array[$getCompanykey][13]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_insurance_renew_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) : '');


        fputcsv($file, $new_array[$getCompanykey][13]);
      }

      if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && ($deadline_type == 'Registered Address' || $deadline_type == '')) {

        $new_array[$getCompanykey][14]['crm_name'] = $getusername['crm_name'];
        $new_array[$getCompanykey][14]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array[$getCompanykey][14]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array[$getCompanykey][14]['crm_deadline_types'] = "Registered Address Due Date";
        $new_array[$getCompanykey][14]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_registered_renew_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) : '');


        fputcsv($file, $new_array[$getCompanykey][14]);
      }

      if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && ($deadline_type == 'Tax Advice' || $deadline_type == '')) {

        $new_array[$getCompanykey][15]['crm_name'] = $getusername['crm_name'];
        $new_array[$getCompanykey][15]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array[$getCompanykey][15]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array[$getCompanykey][15]['crm_deadline_types'] = "Tax Advice Due Date";
        $new_array[$getCompanykey][15]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_investigation_end_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) : '');

        fputcsv($file, $new_array[$getCompanykey][15]);
      }

      if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && ($deadline_type == 'Tax Investigation' || $deadline_type == '')) {

        $new_array[$getCompanykey][16]['crm_name'] = $getusername['crm_name'];
        $new_array[$getCompanykey][16]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array[$getCompanykey][16]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array[$getCompanykey][16]['crm_deadline_types'] = "Tax Investigation Due Date";
        $new_array[$getCompanykey][16]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_investigation_end_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) : '');


        fputcsv($file, $new_array[$getCompanykey][16]);
      }

      $count = '17';

      foreach ($other_services as $key => $value) {
        if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])) && ($deadline_type == $value['service_name'] || $deadline_type == '')) {

          $new_array[$getCompanykey][$count]['crm_name'] = $getusername['crm_name'];
          $new_array[$getCompanykey][$count]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];
          $new_array[$getCompanykey][$count]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
          $new_array[$getCompanykey][$count]['crm_deadline_types'] = "" . $value['service_name'] . " Due Date";
          $new_array[$getCompanykey][$count]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])) : '');
          fputcsv($file, $new_array[$getCompanykey][$count]);
          $count++;
        }
      }
    }

    /*********************************/
    /** for week **/

    $header = array("This Week");
    fputcsv($file, $header);
    $header = array("Client Name", "Client Type", "Company Name", "Deadline Type", "Date");
    fputcsv($file, $header);
    $i = 0;

    $time = date('Y-m-d');

    if ($due_val == 'over_due') {
      $oneweek_end_date = date('Y-m-d', strtotime($time . ' +1 week'));
    } else {
      $oneweek_end_date = date('Y-m-d', strtotime($time . ' -1 week'));
    }

    $current_date = date('Y-m-d');

    foreach ($data['getCompany_oneweek'] as $getCompanykey => $getCompanyvalue) {
      $getusername = $this->Common_mdl->select_record('user', 'id', $getCompanyvalue['user_id']);

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) && $oneweek_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])));
          break;
      }

      if (($zz) && ($deadline_type == 'Confirmation statement' || $deadline_type == '')) {

        $new_array_week[$getCompanykey][1]['crm_name'] = $getusername['crm_name'];
        $new_array_week[$getCompanykey][1]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_week[$getCompanykey][1]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_week[$getCompanykey][1]['crm_deadline_types'] = "Confirmation statement Due Date";
        $new_array_week[$getCompanykey][1]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) : '');

        fputcsv($file, $new_array_week[$getCompanykey][1]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) && $oneweek_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])));
          break;
      }

      if (($zz) && ($deadline_type == 'Accounts' || $deadline_type == '')) {

        $new_array_week[$getCompanykey][2]['crm_name'] = $getusername['crm_name'];
        $new_array_week[$getCompanykey][2]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_week[$getCompanykey][2]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_week[$getCompanykey][2]['crm_deadline_types'] = "Accounts Due Date";

        $new_array_week[$getCompanykey][2]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) : '');


        fputcsv($file, $new_array_week[$getCompanykey][2]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $oneweek_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])));
          break;
      }

      if (($zz) && ($deadline_type == 'Company Tax Return' || $deadline_type == '')) {

        $new_array_week[$getCompanykey][3]['crm_name'] = $getusername['crm_name'];
        $new_array_week[$getCompanykey][3]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];
        $new_array_week[$getCompanykey][3]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_week[$getCompanykey][3]['crm_deadline_types'] = "Company Tax Return Due Date";
        $new_array_week[$getCompanykey][3]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) : '');


        fputcsv($file, $new_array_week[$getCompanykey][3]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) && $oneweek_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])));
          break;
      }

      if (($zz) && ($deadline_type == 'Personal Tax Return' || $deadline_type == '')) {

        $new_array_week[$getCompanykey][4]['crm_name'] = $getusername['crm_name'];
        $new_array_week[$getCompanykey][4]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_week[$getCompanykey][4]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_week[$getCompanykey][4]['crm_deadline_types'] = "Personal Tax Return Due Date";
        $new_array_week[$getCompanykey][4]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_personal_due_date_return']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) : '');


        fputcsv($file, $new_array_week[$getCompanykey][4]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && $oneweek_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])));
          break;
      }

      if (($zz) && ($deadline_type == 'VAT' || $deadline_type == '')) {

        $new_array_week[$getCompanykey][5]['crm_name'] = $getusername['crm_name'];
        $new_array_week[$getCompanykey][5]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_week[$getCompanykey][5]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_week[$getCompanykey][5]['crm_deadline_types'] = "VAT Due Date";
        $new_array_week[$getCompanykey][5]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_vat_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) : '');


        fputcsv($file, $new_array_week[$getCompanykey][5]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) && $oneweek_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])));
          break;
      }

      if (($zz) && $deadline_type == 'Payroll' || $deadline_type == '') {

        $new_array_week[$getCompanykey][6]['crm_name'] = $getusername['crm_name'];
        $new_array_week[$getCompanykey][6]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_week[$getCompanykey][6]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_week[$getCompanykey][6]['crm_deadline_types'] = "Payroll Due Date";
        $new_array_week[$getCompanykey][6]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_rti_deadline']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) : '');


        fputcsv($file, $new_array_week[$getCompanykey][6]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && $oneweek_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])));
          break;
      }

      if (($zz) && ($deadline_type == 'WorkPlace Pension - AE' || $deadline_type == '')) {

        $new_array_week[$getCompanykey][7]['crm_name'] = $getusername['crm_name'];
        $new_array_week[$getCompanykey][7]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_week[$getCompanykey][7]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_week[$getCompanykey][7]['crm_deadline_types'] = "WorkPlace Pension - AE Due Date";
        $new_array_week[$getCompanykey][7]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_pension_subm_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) : '');


        fputcsv($file, $new_array_week[$getCompanykey][7]);
      }


      if ($deadline_type == 'CIS - Contractor' || $deadline_type == '') {

        $new_array_week[$getCompanykey][8]['crm_name'] = $getusername['crm_name'];
        $new_array_week[$getCompanykey][8]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_week[$getCompanykey][8]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_week[$getCompanykey][8]['crm_deadline_types'] = "CIS - Contractor Due Date";
        $new_array_week[$getCompanykey][8]['crm_deadline_date'] = "-";
        fputcsv($file, $new_array_week[$getCompanykey][8]);
      }

      if ($deadline_type == 'CIS - Sub Contractor' || $deadline_type == '') {
        $new_array_week[$getCompanykey][9]['crm_name'] = $getusername['crm_name'];
        $new_array_week[$getCompanykey][9]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_week[$getCompanykey][9]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_week[$getCompanykey][9]['crm_deadline_types'] = "CIS - Sub Contractor Due Date";
        $new_array_week[$getCompanykey][9]['crm_deadline_date'] = "-";
        fputcsv($file, $new_array_week[$getCompanykey][9]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && $oneweek_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])));
          break;
      }

      if (($zz) && ($deadline_type == 'P11D' || $deadline_type == '')) {

        $new_array_week[$getCompanykey][10]['crm_name'] = $getusername['crm_name'];
        $new_array_week[$getCompanykey][10]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_week[$getCompanykey][10]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_week[$getCompanykey][10]['crm_deadline_types'] = "P11D Due Date";
        $new_array_week[$getCompanykey][10]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_next_p11d_return_due']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) : '');


        fputcsv($file, $new_array_week[$getCompanykey][10]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && $oneweek_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])));
          break;
      }

      if (($zz) && ($deadline_type == 'Management Accounts' || $deadline_type == '')) {

        $new_array_week[$getCompanykey][11]['crm_name'] = $getusername['crm_name'];
        $new_array_week[$getCompanykey][11]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_week[$getCompanykey][11]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_week[$getCompanykey][11]['crm_deadline_types'] = "Management Accounts Due Date";
        $new_array_week[$getCompanykey][11]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_next_manage_acc_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) : '');


        fputcsv($file, $new_array_week[$getCompanykey][11]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && $oneweek_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])));
          break;
      }
      if (($zz) && ($deadline_type == 'Bookkeeping' || $deadline_type == '')) {

        $new_array_week[$getCompanykey][12]['crm_name'] = $getusername['crm_name'];
        $new_array_week[$getCompanykey][12]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_week[$getCompanykey][12]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_week[$getCompanykey][12]['crm_deadline_types'] = "Bookkeeping Due Date";
        $new_array_week[$getCompanykey][12]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_next_booking_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) : '');


        fputcsv($file, $new_array_week[$getCompanykey][12]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && $oneweek_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])));
          break;
      }

      if (($zz) && ($deadline_type == 'Investigation Insurance' || $deadline_type == '')) {

        $new_array_week[$getCompanykey][13]['crm_name'] = $getusername['crm_name'];
        $new_array_week[$getCompanykey][13]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_week[$getCompanykey][13]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_week[$getCompanykey][13]['crm_deadline_types'] = "Investigation Insurance Due Date";
        $new_array_week[$getCompanykey][13]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_insurance_renew_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) : '');


        fputcsv($file, $new_array_week[$getCompanykey][13]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && $oneweek_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])));
          break;
      }
      if (($zz) && ($deadline_type == 'Registered Address' || $deadline_type == '')) {

        $new_array_week[$getCompanykey][14]['crm_name'] = $getusername['crm_name'];
        $new_array_week[$getCompanykey][14]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_week[$getCompanykey][14]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_week[$getCompanykey][14]['crm_deadline_types'] = "Registered Address Due Date";
        $new_array_week[$getCompanykey][14]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_registered_renew_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) : '');


        fputcsv($file, $new_array_week[$getCompanykey][14]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneweek_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
          break;
      }

      if (($zz) && ($deadline_type == 'Tax Advice' || $deadline_type == '')) {

        $new_array_week[$getCompanykey][15]['crm_name'] = $getusername['crm_name'];
        $new_array_week[$getCompanykey][15]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_week[$getCompanykey][15]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_week[$getCompanykey][15]['crm_deadline_types'] = "Tax Advice Due Date";
        $new_array_week[$getCompanykey][15]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_investigation_end_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) : '');

        fputcsv($file, $new_array_week[$getCompanykey][15]);
      }


      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneweek_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
          break;
      }

      if (($zz) && ($deadline_type == 'Tax Investigation' || $deadline_type == '')) {

        $new_array_week[$getCompanykey][16]['crm_name'] = $getusername['crm_name'];
        $new_array_week[$getCompanykey][16]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_week[$getCompanykey][16]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_week[$getCompanykey][16]['crm_deadline_types'] = "Tax Investigation Due Date";
        $new_array_week[$getCompanykey][16]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_investigation_end_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) : '');


        fputcsv($file, $new_array_week[$getCompanykey][16]);
      }

      $count = '17';

      foreach ($other_services as $key => $value) {
        switch ($due_val) {
          case 'over_due':
            $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])));
            break;

          default:
            $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])) && $oneweek_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])));
            break;
        }

        if (($zz) && ($deadline_type == $value['service_name'] || $deadline_type == '')) {
          $new_array_week[$getCompanykey][$count]['crm_name'] = $getusername['crm_name'];
          $new_array_week[$getCompanykey][$count]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];
          $new_array_week[$getCompanykey][$count]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
          $new_array_week[$getCompanykey][$count]['crm_deadline_types'] = "" . $value['service_name'] . " Due Date";
          $new_array_week[$getCompanykey][$count]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])) : '');
          fputcsv($file, $new_array_week[$getCompanykey][$count]);
          $count++;
        }
      }
    }

    /***************************************************/
    /** for this month **/

    $header = array("This Month");
    fputcsv($file, $header);
    $header = array("Client Name", "Client Type", "Company Name", "Deadline Type", "Date");
    fputcsv($file, $header);
    $i = 0;

    $time = date('Y-m-d');

    // $onemonth_end_date = date('Y-m-d', strtotime($time . ' +1 month'));
    if ($due_val == 'over_due') {
      $onemonth_end_date = date('Y-m-d', strtotime($time . ' +1 month'));
    } else {
      $onemonth_end_date = date('Y-m-d', strtotime($time . ' -1 month'));
    }
    $current_date = date('Y-m-d');
    foreach ($data['getCompany_onemonth'] as $getCompanykey => $getCompanyvalue) {
      $getusername = $this->Common_mdl->select_record('user', 'id', $getCompanyvalue['user_id']);
      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) && $onemonth_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])));
          break;
      }
      if (($zz) && ($deadline_type == 'Confirmation statement' || $deadline_type == '')) {

        $new_array_month[$getCompanykey][1]['crm_name'] = $getusername['crm_name'];
        $new_array_month[$getCompanykey][1]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_month[$getCompanykey][1]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_month[$getCompanykey][1]['crm_deadline_types'] = "Confirmation statement Due Date";
        $new_array_month[$getCompanykey][1]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) : '');

        fputcsv($file, $new_array_month[$getCompanykey][1]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) && $onemonth_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])));
          break;
      }

      if (($zz) && ($deadline_type == 'Accounts' || $deadline_type == '')) {

        $new_array_month[$getCompanykey][2]['crm_name'] = $getusername['crm_name'];
        $new_array_month[$getCompanykey][2]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_month[$getCompanykey][2]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_month[$getCompanykey][2]['crm_deadline_types'] = "Accounts Due Date";

        $new_array_month[$getCompanykey][2]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) : '');


        fputcsv($file, $new_array_month[$getCompanykey][2]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $onemonth_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])));
          break;
      }



      if (($zz) && ($deadline_type == 'Company Tax Return' || $deadline_type == '')) {

        $new_array_month[$getCompanykey][3]['crm_name'] = $getusername['crm_name'];
        $new_array_month[$getCompanykey][3]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];
        $new_array_month[$getCompanykey][3]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_month[$getCompanykey][3]['crm_deadline_types'] = "Company Tax Return Due Date";
        $new_array_month[$getCompanykey][3]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) : '');


        fputcsv($file, $new_array_month[$getCompanykey][3]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) && $onemonth_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])));
          break;
      }

      if (($zz) && ($deadline_type == 'Personal Tax Return' || $deadline_type == '')) {

        $new_array_month[$getCompanykey][4]['crm_name'] = $getusername['crm_name'];
        $new_array_month[$getCompanykey][4]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_month[$getCompanykey][4]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_month[$getCompanykey][4]['crm_deadline_types'] = "Personal Tax Return Due Date";
        $new_array_month[$getCompanykey][4]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_personal_due_date_return']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) : '');


        fputcsv($file, $new_array_month[$getCompanykey][4]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && $onemonth_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])));
          break;
      }
      if (($zz) && ($deadline_type == 'VAT' || $deadline_type == '')) {

        $new_array_month[$getCompanykey][5]['crm_name'] = $getusername['crm_name'];
        $new_array_month[$getCompanykey][5]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_month[$getCompanykey][5]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_month[$getCompanykey][5]['crm_deadline_types'] = "VAT Due Date";
        $new_array_month[$getCompanykey][5]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_vat_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) : '');


        fputcsv($file, $new_array_month[$getCompanykey][5]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) && $onemonth_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])));
          break;
      }

      if (($zz) && $deadline_type == 'Payroll' || $deadline_type == '') {

        $new_array_month[$getCompanykey][6]['crm_name'] = $getusername['crm_name'];
        $new_array_month[$getCompanykey][6]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_month[$getCompanykey][6]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_month[$getCompanykey][6]['crm_deadline_types'] = "Payroll Due Date";
        $new_array_month[$getCompanykey][6]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_rti_deadline']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) : '');


        fputcsv($file, $new_array_month[$getCompanykey][6]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && $onemonth_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])));
          break;
      }
      if (($zz) && ($deadline_type == 'WorkPlace Pension - AE' || $deadline_type == '')) {

        $new_array_month[$getCompanykey][7]['crm_name'] = $getusername['crm_name'];
        $new_array_month[$getCompanykey][7]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_month[$getCompanykey][7]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_month[$getCompanykey][7]['crm_deadline_types'] = "WorkPlace Pension - AE Due Date";
        $new_array_month[$getCompanykey][7]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_pension_subm_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) : '');


        fputcsv($file, $new_array_month[$getCompanykey][7]);
      }


      if ($deadline_type == 'CIS - Contractor' || $deadline_type == '') {

        $new_array_month[$getCompanykey][8]['crm_name'] = $getusername['crm_name'];
        $new_array_month[$getCompanykey][8]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_month[$getCompanykey][8]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_month[$getCompanykey][8]['crm_deadline_types'] = "CIS - Contractor Due Date";
        $new_array_month[$getCompanykey][8]['crm_deadline_date'] = "-";
        fputcsv($file, $new_array_month[$getCompanykey][8]);
      }

      if ($deadline_type == 'CIS - Sub Contractor' || $deadline_type == '') {
        $new_array_month[$getCompanykey][9]['crm_name'] = $getusername['crm_name'];
        $new_array_month[$getCompanykey][9]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_month[$getCompanykey][9]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_month[$getCompanykey][9]['crm_deadline_types'] = "CIS - Sub Contractor Due Date";
        $new_array_month[$getCompanykey][9]['crm_deadline_date'] = "-";
        fputcsv($file, $new_array_month[$getCompanykey][9]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && $onemonth_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])));
          break;
      }

      if (($zz) && ($deadline_type == 'P11D' || $deadline_type == '')) {

        $new_array_month[$getCompanykey][10]['crm_name'] = $getusername['crm_name'];
        $new_array_month[$getCompanykey][10]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_month[$getCompanykey][10]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_month[$getCompanykey][10]['crm_deadline_types'] = "P11D Due Date";
        $new_array_month[$getCompanykey][10]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_next_p11d_return_due']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) : '');


        fputcsv($file, $new_array_month[$getCompanykey][10]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && $onemonth_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])));
          break;
      }

      if (($zz) && ($deadline_type == 'Management Accounts' || $deadline_type == '')) {

        $new_array_month[$getCompanykey][11]['crm_name'] = $getusername['crm_name'];
        $new_array_month[$getCompanykey][11]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_month[$getCompanykey][11]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_month[$getCompanykey][11]['crm_deadline_types'] = "Management Accounts Due Date";
        $new_array_month[$getCompanykey][11]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_next_manage_acc_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) : '');


        fputcsv($file, $new_array_month[$getCompanykey][11]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && $onemonth_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])));
          break;
      }
      if (($zz) && ($deadline_type == 'Bookkeeping' || $deadline_type == '')) {

        $new_array_month[$getCompanykey][12]['crm_name'] = $getusername['crm_name'];
        $new_array_month[$getCompanykey][12]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_month[$getCompanykey][12]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_month[$getCompanykey][12]['crm_deadline_types'] = "Bookkeeping Due Date";
        $new_array_month[$getCompanykey][12]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_next_booking_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) : '');


        fputcsv($file, $new_array_month[$getCompanykey][12]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && $onemonth_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])));
          break;
      }

      if (($zz) && ($deadline_type == 'Investigation Insurance' || $deadline_type == '')) {

        $new_array_month[$getCompanykey][13]['crm_name'] = $getusername['crm_name'];
        $new_array_month[$getCompanykey][13]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_month[$getCompanykey][13]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_month[$getCompanykey][13]['crm_deadline_types'] = "Investigation Insurance Due Date";
        $new_array_month[$getCompanykey][13]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_insurance_renew_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) : '');


        fputcsv($file, $new_array_month[$getCompanykey][13]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && $onemonth_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])));
          break;
      }
      if (($zz) && ($deadline_type == 'Registered Address' || $deadline_type == '')) {

        $new_array_month[$getCompanykey][14]['crm_name'] = $getusername['crm_name'];
        $new_array_month[$getCompanykey][14]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_month[$getCompanykey][14]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_month[$getCompanykey][14]['crm_deadline_types'] = "Registered Address Due Date";
        $new_array_month[$getCompanykey][14]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_registered_renew_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) : '');


        fputcsv($file, $new_array_month[$getCompanykey][14]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $onemonth_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
          break;
      }

      if (($zz) && ($deadline_type == 'Tax Advice' || $deadline_type == '')) {

        $new_array_month[$getCompanykey][15]['crm_name'] = $getusername['crm_name'];
        $new_array_month[$getCompanykey][15]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_month[$getCompanykey][15]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_month[$getCompanykey][15]['crm_deadline_types'] = "Tax Advice Due Date";
        $new_array_month[$getCompanykey][15]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_investigation_end_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) : '');

        fputcsv($file, $new_array_month[$getCompanykey][15]);
      }


      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $onemonth_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
          break;
      }

      if (($zz) && ($deadline_type == 'Tax Investigation' || $deadline_type == '')) {

        $new_array_month[$getCompanykey][16]['crm_name'] = $getusername['crm_name'];
        $new_array_month[$getCompanykey][16]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_month[$getCompanykey][16]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_month[$getCompanykey][16]['crm_deadline_types'] = "Tax Investigation Due Date";
        $new_array_month[$getCompanykey][16]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_investigation_end_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) : '');


        fputcsv($file, $new_array_month[$getCompanykey][16]);
      }

      $count = '17';

      foreach ($other_services as $key => $value) {
        switch ($due_val) {
          case 'over_due':
            $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])));
            break;

          default:
            $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])) && $onemonth_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])));
            break;
        }

        if (($zz) && ($deadline_type == $value['service_name'] || $deadline_type == '')) {
          $new_array_month[$getCompanykey][$count]['crm_name'] = $getusername['crm_name'];
          $new_array_month[$getCompanykey][$count]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];
          $new_array_month[$getCompanykey][$count]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
          $new_array_month[$getCompanykey][$count]['crm_deadline_types'] = "" . $value['service_name'] . " Due Date";
          $new_array_month[$getCompanykey][$count]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])) : '');
          fputcsv($file, $new_array_month[$getCompanykey][$count]);
          $count++;
        }
      }
    }

    /*********************************************************/

    /********* this year **/

    $header = array("This Year");
    fputcsv($file, $header);
    $header = array("Client Name", "Client Type", "Company Name", "Deadline Type", "Date");
    fputcsv($file, $header);
    $i = 0;
    $time = date('Y-m-d');

    if ($due_val == 'over_due') {
      $oneyear_end_date = date('Y-m-d', strtotime($time . ' +1 year'));
    } else {
      $oneyear_end_date = date('Y-m-d', strtotime($time . ' -1 year'));
    }

    $current_date = date('Y-m-d');

    foreach ($data['getCompany_oneyear'] as $getCompanykey => $getCompanyvalue) {

      $getusername = $this->Common_mdl->select_record('user', 'id', $getCompanyvalue['user_id']);

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) && $oneyear_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])));
          break;
      }

      if (($zz) && ($deadline_type == 'Confirmation statement' || $deadline_type == '')) {

        $new_array_year[$getCompanykey][1]['crm_name'] = $getusername['crm_name'];
        $new_array_year[$getCompanykey][1]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_year[$getCompanykey][1]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_year[$getCompanykey][1]['crm_deadline_types'] = "Confirmation statement Due Date";
        $new_array_year[$getCompanykey][1]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) : '');

        fputcsv($file, $new_array_year[$getCompanykey][1]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) && $oneyear_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])));
          break;
      }

      if (($zz) && ($deadline_type == 'Accounts' || $deadline_type == '')) {

        $new_array_year[$getCompanykey][2]['crm_name'] = $getusername['crm_name'];
        $new_array_year[$getCompanykey][2]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_year[$getCompanykey][2]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_year[$getCompanykey][2]['crm_deadline_types'] = "Accounts Due Date";

        $new_array_year[$getCompanykey][2]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) : '');


        fputcsv($file, $new_array_year[$getCompanykey][2]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $oneyear_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])));
          break;
      }

      if (($zz) && ($deadline_type == 'Company Tax Return' || $deadline_type == '')) {

        $new_array_year[$getCompanykey][3]['crm_name'] = $getusername['crm_name'];
        $new_array_year[$getCompanykey][3]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];
        $new_array_year[$getCompanykey][3]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_year[$getCompanykey][3]['crm_deadline_types'] = "Company Tax Return Due Date";
        $new_array_year[$getCompanykey][3]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) : '');


        fputcsv($file, $new_array_year[$getCompanykey][3]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) && $oneyear_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])));
          break;
      }

      if (($zz) && ($deadline_type == 'Personal Tax Return' || $deadline_type == '')) {

        $new_array_year[$getCompanykey][4]['crm_name'] = $getusername['crm_name'];
        $new_array_year[$getCompanykey][4]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_year[$getCompanykey][4]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_year[$getCompanykey][4]['crm_deadline_types'] = "Personal Tax Return Due Date";
        $new_array_year[$getCompanykey][4]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_personal_due_date_return']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) : '');


        fputcsv($file, $new_array_year[$getCompanykey][4]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && $oneyear_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])));
          break;
      }
      if (($zz) && ($deadline_type == 'VAT' || $deadline_type == '')) {

        $new_array_year[$getCompanykey][5]['crm_name'] = $getusername['crm_name'];
        $new_array_year[$getCompanykey][5]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_year[$getCompanykey][5]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_year[$getCompanykey][5]['crm_deadline_types'] = "VAT Due Date";
        $new_array_year[$getCompanykey][5]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_vat_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) : '');


        fputcsv($file, $new_array_year[$getCompanykey][5]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) && $oneyear_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])));
          break;
      }

      if (($zz) && $deadline_type == 'Payroll' || $deadline_type == '') {

        $new_array_year[$getCompanykey][6]['crm_name'] = $getusername['crm_name'];
        $new_array_year[$getCompanykey][6]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_year[$getCompanykey][6]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_year[$getCompanykey][6]['crm_deadline_types'] = "Payroll Due Date";
        $new_array_year[$getCompanykey][6]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_rti_deadline']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) : '');


        fputcsv($file, $new_array_year[$getCompanykey][6]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && $oneyear_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])));
          break;
      }
      if (($zz) && ($deadline_type == 'WorkPlace Pension - AE' || $deadline_type == '')) {

        $new_array_year[$getCompanykey][7]['crm_name'] = $getusername['crm_name'];
        $new_array_year[$getCompanykey][7]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_year[$getCompanykey][7]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_year[$getCompanykey][7]['crm_deadline_types'] = "WorkPlace Pension - AE Due Date";
        $new_array_year[$getCompanykey][7]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_pension_subm_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) : '');


        fputcsv($file, $new_array_year[$getCompanykey][7]);
      }


      if ($deadline_type == 'CIS - Contractor' || $deadline_type == '') {

        $new_array_year[$getCompanykey][8]['crm_name'] = $getusername['crm_name'];
        $new_array_year[$getCompanykey][8]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_year[$getCompanykey][8]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_year[$getCompanykey][8]['crm_deadline_types'] = "CIS - Contractor Due Date";
        $new_array_year[$getCompanykey][8]['crm_deadline_date'] = "-";
        fputcsv($file, $new_array_year[$getCompanykey][8]);
      }

      if ($deadline_type == 'CIS - Sub Contractor' || $deadline_type == '') {
        $new_array_year[$getCompanykey][9]['crm_name'] = $getusername['crm_name'];
        $new_array_year[$getCompanykey][9]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_year[$getCompanykey][9]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_year[$getCompanykey][9]['crm_deadline_types'] = "CIS - Sub Contractor Due Date";
        $new_array_year[$getCompanykey][9]['crm_deadline_date'] = "-";
        fputcsv($file, $new_array_year[$getCompanykey][9]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && $oneyear_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])));
          break;
      }

      if (($zz) && ($deadline_type == 'P11D' || $deadline_type == '')) {

        $new_array_year[$getCompanykey][10]['crm_name'] = $getusername['crm_name'];
        $new_array_year[$getCompanykey][10]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_year[$getCompanykey][10]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_year[$getCompanykey][10]['crm_deadline_types'] = "P11D Due Date";
        $new_array_year[$getCompanykey][10]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_next_p11d_return_due']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) : '');


        fputcsv($file, $new_array_year[$getCompanykey][10]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && $oneyear_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])));
          break;
      }

      if (($zz) && ($deadline_type == 'Management Accounts' || $deadline_type == '')) {

        $new_array_year[$getCompanykey][11]['crm_name'] = $getusername['crm_name'];
        $new_array_year[$getCompanykey][11]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_year[$getCompanykey][11]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_year[$getCompanykey][11]['crm_deadline_types'] = "Management Accounts Due Date";
        $new_array_year[$getCompanykey][11]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_next_manage_acc_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) : '');


        fputcsv($file, $new_array_year[$getCompanykey][11]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && $oneyear_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])));
          break;
      }

      if (($zz) && ($deadline_type == 'Bookkeeping' || $deadline_type == '')) {

        $new_array_year[$getCompanykey][12]['crm_name'] = $getusername['crm_name'];
        $new_array_year[$getCompanykey][12]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_year[$getCompanykey][12]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_year[$getCompanykey][12]['crm_deadline_types'] = "Bookkeeping Due Date";
        $new_array_year[$getCompanykey][12]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_next_booking_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) : '');


        fputcsv($file, $new_array_year[$getCompanykey][12]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && $oneyear_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])));
          break;
      }

      if (($zz) && ($deadline_type == 'Investigation Insurance' || $deadline_type == '')) {

        $new_array_year[$getCompanykey][13]['crm_name'] = $getusername['crm_name'];
        $new_array_year[$getCompanykey][13]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_year[$getCompanykey][13]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_year[$getCompanykey][13]['crm_deadline_types'] = "Investigation Insurance Due Date";
        $new_array_year[$getCompanykey][13]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_insurance_renew_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) : '');


        fputcsv($file, $new_array_year[$getCompanykey][13]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && $oneyear_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])));
          break;
      }
      if (($zz) && ($deadline_type == 'Registered Address' || $deadline_type == '')) {

        $new_array_year[$getCompanykey][14]['crm_name'] = $getusername['crm_name'];
        $new_array_year[$getCompanykey][14]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_year[$getCompanykey][14]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_year[$getCompanykey][14]['crm_deadline_types'] = "Registered Address Due Date";
        $new_array_year[$getCompanykey][14]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_registered_renew_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) : '');


        fputcsv($file, $new_array_year[$getCompanykey][14]);
      }

      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneyear_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
          break;
      }

      if (($zz) && ($deadline_type == 'Tax Advice' || $deadline_type == '')) {

        $new_array_year[$getCompanykey][15]['crm_name'] = $getusername['crm_name'];
        $new_array_year[$getCompanykey][15]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_year[$getCompanykey][15]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_year[$getCompanykey][15]['crm_deadline_types'] = "Tax Advice Due Date";
        $new_array_year[$getCompanykey][15]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_investigation_end_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) : '');

        fputcsv($file, $new_array_year[$getCompanykey][15]);
      }


      switch ($due_val) {
        case 'over_due':
          $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
          break;

        default:
          $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneyear_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])));
          break;
      }

      if (($zz) && ($deadline_type == 'Tax Investigation' || $deadline_type == '')) {

        $new_array_year[$getCompanykey][16]['crm_name'] = $getusername['crm_name'];
        $new_array_year[$getCompanykey][16]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_year[$getCompanykey][16]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_year[$getCompanykey][16]['crm_deadline_types'] = "Tax Investigation Due Date";
        $new_array_year[$getCompanykey][16]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_investigation_end_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) : '');


        fputcsv($file, $new_array_year[$getCompanykey][16]);
      }


      $count = '17';

      foreach ($other_services as $key => $value) {
        switch ($due_val) {
          case 'over_due':
            $zz = ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])));
            break;

          default:
            $zz = ($current_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])) && $oneyear_end_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])));
            break;
        }

        if (($zz) && ($deadline_type == $value['service_name'] || $deadline_type == '')) {
          $new_array_year[$getCompanykey][$count]['crm_name'] = $getusername['crm_name'];
          $new_array_year[$getCompanykey][$count]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];
          $new_array_year[$getCompanykey][$count]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
          $new_array_year[$getCompanykey][$count]['crm_deadline_types'] = "" . $value['service_name'] . " Due Date";
          $new_array_year[$getCompanykey][$count]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_' . $value['services_subnames'] . '_statement_date'])) : '');
          fputcsv($file, $new_array_year[$getCompanykey][$count]);
          $count++;
        }
      }
    }

    fclose($file);
    exit;
  }

  /** for status board **/

  public function deadline_exportCSV_status()
  {
    $com_num = $_GET['com_num'];
    $legal_form = $_GET['legal_form'];

    if (!empty($_GET['values']) && !empty($_GET['fields'])) {
      $for_values = explode(',', $_GET['values']);
      $for_fields = explode(',', $_GET['fields']);
    } else {
      $for_values = "";
      $for_fields = "";
    }

    $qy = $this->getStatusBoard_Query($com_num, $legal_form, $for_values, $for_fields);
    $getCompany = $this->db->query($qy)->result_array();

    // file name
    $filename = 'status_board_' . date('Ymd') . '.csv';

    header("Content-Description: File Transfer");
    header("Content-Disposition: attachment; filename=$filename");
    header("Content-Type: application/csv; ");

    // file creation
    $file = fopen('php://output', 'w');
    $service_list = $this->Common_mdl->getServicelist();
    $services = array_column($service_list, 'service_name');
    array_unshift($services, 'Client Type');
    array_unshift($services, 'Client Name');

    fputcsv($file, $services);

    unset($services[0]);
    unset($services[1]);

    $data = $this->Common_mdl->getClientsServiceCharge($getCompany);

    foreach ($data as $key => $value) {
      unset($value['id']);
      unset($value['client_subscribed_services']);

      foreach ($services as $key1 => $value1) {
        $value[$value1] = 'Due: ' . $value[$value1]['charge'] . ',Basic: ' . $value[$value1]['basic'];
      }

      fputcsv($file, $value);
    }

    fclose($file);
    exit;
  }

  function pdf_status()
  {
    $com_num = $_GET['com_num'];
    $legal_form = $_GET['legal_form'];

    if (!empty($_GET['values']) && !empty($_GET['fields'])) {
      $for_values = explode(',', $_GET['values']);
      $for_fields = explode(',', $_GET['fields']);
    } else {
      $for_values = "";
      $for_fields = "";
    }

    $qy = $this->getStatusBoard_Query($com_num, $legal_form, $for_values, $for_fields);
    $getCompany = $this->db->query($qy)->result_array();

    $data['services'] = $this->Common_mdl->getClientsServiceCharge($getCompany);
    $data['title'] = "Status Board";
    $data['service_list'] = $this->Common_mdl->getServicelist();

    $customPaper = array(0, 0, 1000, 1000);

    $this->load->library('pdf');
    $this->pdf->set_paper($customPaper);
    $this->pdf->load_view('Deadline_manager/pdf_view_new_status', $data);
    $this->pdf->render();
    $this->pdf->stream('status_board_' . date('Ymd') . '.pdf');
  }

  function html_status()
  {
    $com_num = $_GET['com_num'];
    $legal_form = $_GET['legal_form'];

    if (!empty($_GET['values']) && !empty($_GET['fields'])) {
      $for_values = explode(',', $_GET['values']);
      $for_fields = explode(',', $_GET['fields']);
    } else {
      $for_values = "";
      $for_fields = "";
    }

    $qy = $this->getStatusBoard_Query($com_num, $legal_form, $for_values, $for_fields);
    $getCompany = $this->db->query($qy)->result_array();

    $data['services'] = $this->Common_mdl->getClientsServiceCharge($getCompany);
    $data['service_list'] = $this->Common_mdl->getServicelist();
    $data['title'] = "Status Board";

    $this->load->view('Deadline_manager/deadline_print_page_status', $data);
  }

  public function updateSubscription()
  {
    $client = $this->Common_mdl->select_record('client', 'id', $_POST['client_id']);
    $client_subscribed_services = json_decode($client['client_subscribed_services']);

    $item_name = explode(',', $client_subscribed_services->item_name);
    $quantity = explode(',', $client_subscribed_services->quantity);
    $unit_price = explode(',', $client_subscribed_services->amount);
    $discount = explode(',', $client_subscribed_services->discount);
    $tax_amount = explode(',', $client_subscribed_services->tax_amount);

    if (!empty($client_subscribed_services) && in_array($_POST['item_name'], $item_name)) {
      $key = array_search($_POST['item_name'], $item_name);
      $quantity[$key] = $_POST['quantity'];
      $unit_price[$key] = $_POST['amount'];
      $discount[$key] = $_POST['discount'];
      $tax_amount[$key] = $_POST['tax_amount'];
    } else {
      if (!empty($client_subscribed_services)) {
        $key = count($item_name);
      } else {
        $key = 0;
      }

      $item_name[$key] = $_POST['item_name'];
      $quantity[$key] = $_POST['quantity'];
      $unit_price[$key] = $_POST['amount'];
      $discount[$key] = $_POST['discount'];
      $tax_amount[$key] = $_POST['tax_amount'];
    }

    $subscribed = new stdClass();
    $subscribed->item_name = implode(',', $item_name);
    $subscribed->quantity = implode(',', $quantity);
    $subscribed->amount = implode(',', $unit_price);
    $subscribed->discount = implode(',', $discount);
    $subscribed->tax_amount = implode(',', $tax_amount);

    $data['client_subscribed_services'] = json_encode($subscribed);

    $update = $this->Common_mdl->update('client', $data, 'id', $_POST['client_id']);

    if ($update == '1') {
      $_SESSION['success_msg'] = 'Updated Successfully';
    }

    redirect($_POST['REQUEST_URI']);
  }

  /* Modified By Shashmethah*/

  public function deadline_info()
  {

    $this->Security_model->chk_login();
    $id = $this->session->userdata('admin_id');

    // $data['user_details']=$this->Common_mdl->select_record('user','id',$id);
    // $data['create_clients_list']=$this->db->query("select * from user where firm_admin_id='".$id."' and crm_name != '' ")->result_array();
    //  $user_id = $this->session->userdata('id');
    //    $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = ".$id." and role = 4 and crm_name !=''")->row_array();

    // $us_id =  explode(',', $sql['us_id']);

    if ($_SESSION['role'] == 4) {
      $id = $_SESSION['id'];
      $data['user_details'] = $this->Common_mdl->select_record('user', 'id', $id);
      $data['create_clients_list'] = $this->db->query("select * from user where (firm_admin_id='" . $id . "' or id=" . $id . ") and crm_name != '' ")->result_array();
      $user_id = $this->session->userdata('id');
      $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where role = 4 and crm_name !='' and (firm_admin_id = " . $id . " or id=" . $id . ")  ")->row_array();

      $us_id =  explode(',', $sql['us_id']);
    } else {
      $id = $this->session->userdata('admin_id');
      $data['user_details'] = $this->Common_mdl->select_record('user', 'id', $id);
      $data['create_clients_list'] = $this->db->query("select * from user where firm_admin_id='" . $id . "' and crm_name != '' ")->result_array();
      $user_id = $this->session->userdata('id');
      $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = " . $id . " and role = 4 and crm_name !=''")->row_array();

      $us_id =  explode(',', $sql['us_id']);
    }

    $data['getCompany'] = $this->db->query("SELECT * FROM client WHERE user_id IN ( '" . implode("','", $us_id) . "' ) and crm_company_name !=''")->result_array();
    $current_date = date("Y-m-d");
    $time = date("Y-m-d");;

    $data['getCompany_today'] = $this->db->query("SELECT * FROM client WHERE user_id IN ( '" . implode("','", $us_id) . "' ) and crm_company_name !='' and ((crm_confirmation_statement_due_date= '" . $current_date . "') or (crm_ch_accounts_next_due = '" . $current_date . "') or (crm_accounts_tax_date_hmrc = '" . $current_date . "' ) or (crm_personal_due_date_return = '" . $current_date . "' ) or (crm_vat_due_date = '" . $current_date . "' ) or (crm_rti_deadline = '" . $current_date . "')  or (crm_pension_subm_due_date = '" . $current_date . "') or (crm_next_p11d_return_due = '" . $current_date . "')  or (crm_next_manage_acc_date = '" . $current_date . "') or (crm_next_booking_date = '" . $current_date . "' ) or (crm_insurance_renew_date = '" . $current_date . "')  or (crm_registered_renew_date = '" . $current_date . "' ) or (crm_investigation_end_date = '" . $current_date . "' )) ")->result_array();

    $oneweek_end_date = date('Y-m-d', strtotime($time . ' +1 week'));
    //echo $end_date;
    $data['getCompany_oneweek'] = $this->db->query("SELECT * FROM client WHERE user_id IN ( '" . implode("','", $us_id) . "' ) and crm_company_name !='' and ((crm_confirmation_statement_due_date BETWEEN '" . $current_date . "' AND '" . $oneweek_end_date . "' ) or (crm_ch_accounts_next_due BETWEEN '" . $current_date . "' AND '" . $oneweek_end_date . "' ) or (crm_accounts_tax_date_hmrc BETWEEN '" . $current_date . "' AND '" . $oneweek_end_date . "' ) or (crm_personal_due_date_return BETWEEN '" . $current_date . "' AND '" . $oneweek_end_date . "' ) or (crm_vat_due_date BETWEEN '" . $current_date . "' AND '" . $oneweek_end_date . "' ) or (crm_rti_deadline BETWEEN '" . $current_date . "' AND '" . $oneweek_end_date . "' )  or (crm_pension_subm_due_date BETWEEN '" . $current_date . "' AND '" . $oneweek_end_date . "' ) or (crm_next_p11d_return_due BETWEEN '" . $current_date . "' AND '" . $oneweek_end_date . "' )  or (crm_next_manage_acc_date BETWEEN '" . $current_date . "' AND '" . $oneweek_end_date . "' ) or (crm_next_booking_date BETWEEN '" . $current_date . "' AND '" . $oneweek_end_date . "' ) or (crm_insurance_renew_date BETWEEN '" . $current_date . "' AND '" . $oneweek_end_date . "' )  or (crm_registered_renew_date BETWEEN '" . $current_date . "' AND '" . $oneweek_end_date . "' ) or (crm_investigation_end_date BETWEEN '" . $current_date . "' AND '" . $oneweek_end_date . "' )) ")->result_array();

    $onemonth_end_date = date('Y-m-d', strtotime($time . ' +1 month'));
    //echo $end_date;
    $data['getCompany_onemonth'] = $this->db->query("SELECT * FROM client WHERE user_id IN ( '" . implode("','", $us_id) . "' ) and crm_company_name !='' and ((crm_confirmation_statement_due_date BETWEEN '" . $current_date . "' AND '" . $onemonth_end_date . "' ) or (crm_ch_accounts_next_due BETWEEN '" . $current_date . "' AND '" . $onemonth_end_date . "' ) or (crm_accounts_tax_date_hmrc BETWEEN '" . $current_date . "' AND '" . $onemonth_end_date . "' ) or (crm_personal_due_date_return BETWEEN '" . $current_date . "' AND '" . $onemonth_end_date . "' ) or (crm_vat_due_date BETWEEN '" . $current_date . "' AND '" . $onemonth_end_date . "' ) or (crm_rti_deadline BETWEEN '" . $current_date . "' AND '" . $onemonth_end_date . "' )  or (crm_pension_subm_due_date BETWEEN '" . $current_date . "' AND '" . $onemonth_end_date . "' ) or (crm_next_p11d_return_due BETWEEN '" . $current_date . "' AND '" . $onemonth_end_date . "' )  or (crm_next_manage_acc_date BETWEEN '" . $current_date . "' AND '" . $onemonth_end_date . "' ) or (crm_next_booking_date BETWEEN '" . $current_date . "' AND '" . $onemonth_end_date . "' ) or (crm_insurance_renew_date BETWEEN '" . $current_date . "' AND '" . $onemonth_end_date . "' )  or (crm_registered_renew_date BETWEEN '" . $current_date . "' AND '" . $onemonth_end_date . "' ) or (crm_investigation_end_date BETWEEN '" . $current_date . "' AND '" . $onemonth_end_date . "' )) ")->result_array();

    $oneyear_end_date = date('Y-m-d', strtotime($time . ' +1 year'));
    //echo $end_date;
    $data['getCompany_oneyear'] = $this->db->query("SELECT * FROM client WHERE user_id IN ( '" . implode("','", $us_id) . "' ) and crm_company_name !='' and ((crm_confirmation_statement_due_date BETWEEN '" . $current_date . "' AND '" . $oneyear_end_date . "' ) or (crm_ch_accounts_next_due BETWEEN '" . $current_date . "' AND '" . $oneyear_end_date . "' ) or (crm_accounts_tax_date_hmrc BETWEEN '" . $current_date . "' AND '" . $oneyear_end_date . "' ) or (crm_personal_due_date_return BETWEEN '" . $current_date . "' AND '" . $oneyear_end_date . "' ) or (crm_vat_due_date BETWEEN '" . $current_date . "' AND '" . $oneyear_end_date . "' ) or (crm_rti_deadline BETWEEN '" . $current_date . "' AND '" . $oneyear_end_date . "' )  or (crm_pension_subm_due_date BETWEEN '" . $current_date . "' AND '" . $oneyear_end_date . "' ) or (crm_next_p11d_return_due BETWEEN '" . $current_date . "' AND '" . $oneyear_end_date . "' )  or (crm_next_manage_acc_date BETWEEN '" . $current_date . "' AND '" . $oneyear_end_date . "' ) or (crm_next_booking_date BETWEEN '" . $current_date . "' AND '" . $oneyear_end_date . "' ) or (crm_insurance_renew_date BETWEEN '" . $current_date . "' AND '" . $oneyear_end_date . "' )  or (crm_registered_renew_date BETWEEN '" . $current_date . "' AND '" . $oneyear_end_date . "' ) or (crm_investigation_end_date BETWEEN '" . $current_date . "' AND '" . $oneyear_end_date . "' )) ")->result_array();


    //         $end_date = date('Y-m-d', strtotime($time . ' +1 week'));
    // echo $end_date;

    $this->load->view('Deadline_manager/deadline_info', $data);
  }



  public function update_bank()
  {
    $data['bank_name'] = $_POST['bank_name'];
    $data['branch_name'] = $_POST['branch_name'];
    $data['account_name'] = $_POST['account_name'];
    $data['account_no'] = $_POST['account_no'];
    $id = $this->session->userdata('admin_id');
    $this->Common_mdl->update('user', $data, 'id', $id);

    $datas['log'] = "Bank Account Details Update";
    $datas['createdTime'] = time();
    $datas['module'] = 'Profile change';
    $datas['user_id'] = $id;
    $this->Common_mdl->insert('activity_log', $datas);


    //echo $this->db->last_query();die;
    $this->session->set_flashdata('success', 'Staff Profile Details Updated Successfully');

    redirect('staff/Dashboard');
  }

  function weeks($month, $year)
  {
    $num_of_days = date("t", mktime(0, 0, 0, $month, 1, $year));
    $lastday = date("t", mktime(0, 0, 0, $month, 1, $year));
    $no_of_weeks = 0;
    $count_weeks = 0;
    while ($no_of_weeks < $lastday) {
      $no_of_weeks += 7;
      $count_weeks++;
    }
    return $count_weeks;
  }

  function month_cnt($date)
  {
    $week_num =  date('W', strtotime($date));
    return $week_num;
  }

  function start_end_date($date)
  {
    $search_form = date('Y-m-01', strtotime($date));
    $search_to =  date('Y-m-t', strtotime($date));
    return $search_form . ',' . $search_to;
  }

  function time_card()
  {
    $mon = $_POST['month'];
    $ex_mon = explode('/', $mon);
    $month = $ex_mon[0];
    $year = $ex_mon[1];
    $data['start_end_date'] = $this->start_end_date('01-' . $month . '-' . $year);
    $exp_date = explode(',', $data['start_end_date']);
    //print_r($data['start_end_date'] );die;

    $start = $exp_date[0];
    $end = $exp_date[1];

    $data['start_month'] = $this->month_cnt($start);
    if ($month == '12') {
      // echo date('W', mktime(0,0,0,12,28,$year));die;
      $data['end_month'] = $this->getIsoWeeksInYear($year) + 1;
      // echo $data['end_month'];die;
    } else {
      $data['end_month'] = $this->month_cnt($end);
    }
    $data['total_count_month'] = $this->weeks($month, $year);

    $data['month_ex'] =  $month;
    $data['year_ex'] =  $year;

    $this->load->view('staffs/time_card_filter', $data);
  }

  function getIsoWeeksInYear($year)
  {
    $date = new DateTime;
    $date->setISODate($year, 53);
    return ($date->format("W") === "53" ? 53 : 52);
  }


  public function service_update()
  {
    if ($_SESSION['permission']['Reminder_Settings']['edit'] == '1') {
      $values = $_POST['values'];
      $stat = $_POST['stat'];

      $exp_value = explode('_', $values);
      $field_name = $exp_value[0];
      $id = $exp_value[1];

      $client_rec = $this->Common_mdl->select_record('client', 'id', $id);

      $conf_statement = $client_rec['conf_statement'];
      $accounts = $client_rec['accounts'];
      $company_tax_return = $client_rec['company_tax_return'];
      $personal_tax_return = $client_rec['personal_tax_return'];
      $payroll = $client_rec['payroll'];
      $workplace = $client_rec['workplace'];
      $vat = $client_rec['vat'];
      $cis = $client_rec['cis'];
      $cissub = $client_rec['cissub'];
      $p11d = $client_rec['p11d'];
      $bookkeep = $client_rec['bookkeep'];
      $management  = $client_rec['management'];
      $investgate  = $client_rec['investgate'];
      $registered  = $client_rec['registered'];
      $taxadvice  = $client_rec['taxadvice'];
      $taxinvest  = $client_rec['taxinvest'];

      if ($field_name == "vats") {
        $f_name = $vat;
        $fname = "vat";
      } elseif ($field_name == "payrolls") {
        $f_name = $payroll;
        $fname = "payroll";
      } elseif ($field_name == "accountss") {
        $f_name = $accounts;
        $fname = "accounts";
      } elseif ($field_name == "confstatements") {
        $f_name = $conf_statement;
        $fname = "conf_statement";
      } elseif ($field_name == "companytaxs") {
        $f_name = $company_tax_return;
        $fname = "company_tax_return";
      } elseif ($field_name == "personaltaxs") {
        $f_name = $personal_tax_return;
        $fname = "personal_tax_return";
      } elseif ($field_name == "workplaces") {
        $f_name = $workplace;
        $fname = "workplace";
      } elseif ($field_name == "ciss") {
        $f_name = $cis;
        $fname = "cis";
      } elseif ($field_name == "cissubs") {
        $f_name = $cissub;
        $fname = "cissub";
      } elseif ($field_name == "p11ds") {
        $f_name = $p11d;
        $fname = "p11d";
      } elseif ($field_name == "bookkeeps") {
        $f_name = $bookkeep;
        $fname = "bookkeep";
      } elseif ($field_name == "managements") {
        $f_name = $management;
        $fname = "management";
      } elseif ($field_name == "investgate") {
        $f_name = $investgate;
        $fname = "investgate";
      } elseif ($field_name == "registereds") {
        $f_name = $registered;
        $fname = "registered";
      } elseif ($field_name == "taxadvices") {
        $f_name = $taxadvice;
        $fname = "taxadvice";
      } elseif ($field_name == "taxinvests") {
        $f_name = $taxinvest;
        $fname = "taxinvest";
      } else {
        $other = $this->db->query('SELECT * FROM service_lists WHERE services_subnames = "' . str_replace('.', '_', $field_name) . '"')->row_array();
        $f_name = $client_rec[$other['services_subnames']];
        $fname = $other['services_subnames'];
      }

      if ($stat == 'on') {
        $rec = json_decode($f_name);

        if ($rec != '') {
          $arr_rec = array("reminder" => "on");
          $arr_old = $this->object_to_array($rec);
          $result = array_merge($arr_rec, $arr_old);
        } else {
          $result = array("reminder" => "on");
        }
      } else {
        $rec = json_decode($f_name);

        if ($rec != "") {
          unset($rec->reminder);
          $result = $this->object_to_array($rec);
        } else {
          $result = array();
        }
      }

      $up_rec = json_encode($result);
      $data[$fname] = $up_rec;

      $result =  $this->Common_mdl->update('client', $data, 'id', $id);


      echo $result;
    } else {
      echo 'You Do Not Have Permission To Edit.';
    }
  }


  public function service_update_mail()
  {
    if ($_SESSION['permission']['Invoices']['edit'] == '1') {
      $values = $_POST['values'];
      $stat = $_POST['stat'];

      $exp_value = explode('_', $values);
      $field_name = $exp_value[0];
      $id = $exp_value[1];

      $client_rec = $this->Common_mdl->select_record('client', 'id', $id);

      $conf_statement = $client_rec['conf_statement'];
      $accounts = $client_rec['accounts'];
      $company_tax_return = $client_rec['company_tax_return'];
      $personal_tax_return = $client_rec['personal_tax_return'];
      $payroll = $client_rec['payroll'];
      $workplace = $client_rec['workplace'];
      $vat = $client_rec['vat'];
      $cis = $client_rec['cis'];
      $cissub = $client_rec['cissub'];
      $p11d = $client_rec['p11d'];
      $bookkeep = $client_rec['bookkeep'];
      $management  = $client_rec['management'];
      $investgate  = $client_rec['investgate'];
      $registered  = $client_rec['registered'];
      $taxadvice  = $client_rec['taxadvice'];
      $taxinvest  = $client_rec['taxinvest'];

      if ($field_name == "vatsmail") {
        $f_name = $vat;
        $fname = "vat";
      } elseif ($field_name == "payrollsmail") {
        $f_name = $payroll;
        $fname = "payroll";
      } elseif ($field_name == "accountssmail") {
        $f_name = $accounts;
        $fname = "accounts";
      } elseif ($field_name == "confstatementsmail") {
        $f_name = $conf_statement;
        $fname = "conf_statement";
      } elseif ($field_name == "companytaxsmail") {
        $f_name = $company_tax_return;
        $fname = "company_tax_return";
      } elseif ($field_name == "personaltaxsmail") {
        $f_name = $personal_tax_return;
        $fname = "personal_tax_return";
      } elseif ($field_name == "workplacesmail") {
        $f_name = $workplace;
        $fname = "workplace";
      } elseif ($field_name == "cissmail") {
        $f_name = $cis;
        $fname = "cis";
      } elseif ($field_name == "cissubsmail") {
        $f_name = $cissub;
        $fname = "cissub";
      } elseif ($field_name == "p11dsmail") {
        $f_name = $p11d;
        $fname = "p11d";
      } elseif ($field_name == "bookkeepsmail") {
        $f_name = $bookkeep;
        $fname = "bookkeep";
      } elseif ($field_name == "managementsmail") {
        $f_name = $management;
        $fname = "management";
      } elseif ($field_name == "investgatemail") {
        $f_name = $investgate;
        $fname = "investgate";
      } elseif ($field_name == "registeredsmail") {
        $f_name = $registered;
        $fname = "registered";
      } elseif ($field_name == "taxadvicesmail") {
        $f_name = $taxadvice;
        $fname = "taxadvice";
      } elseif ($field_name == "taxinvestsmail") {
        $f_name = $taxinvest;
        $fname = "taxinvest";
      } else {
        $other = $this->db->query('SELECT * FROM service_lists WHERE services_subnames = "' . str_replace('.', '_', substr_replace($field_name, "", -4)) . '"')->row_array();
        $f_name = $client_rec[$other['services_subnames']];
        $fname = $other['services_subnames'];
      }

      if ($stat == 'on') {
        $rec = json_decode($f_name);

        if ($rec != '') {
          $arr_rec = array("invoice" => "on");
          $arr_old = $this->object_to_array($rec);
          $result = array_merge($arr_rec, $arr_old);
        } else {
          $result = array("invoice" => "on");
        }
      } else {
        $rec = json_decode($f_name);

        if ($rec != "") {
          unset($rec->invoice);
          $result = $this->object_to_array($rec);
        } else {
          $result = array();
        }
      }

      $up_rec = json_encode($result);
      $data[$fname] = $up_rec;
      echo $this->Common_mdl->update('client', $data, 'id', $id);
    } else {
      echo 'You Do Not Have Permission To Edit.';
    }
  }

  function object_to_array($data)
  {
    if (is_array($data) || is_object($data)) {
      $result = array();
      foreach ($data as $key => $value) {
        $result[$key] = $this->object_to_array($value);
      }
      return $result;
    }
    return $data;
  }

  /****************************************************/
  public function deadline_exportCSV()
  {

    if ($_SESSION['role'] == 4) {

      $id = $_SESSION['id'];
      $data['user_details'] = $this->Common_mdl->select_record('user', 'id', $id);
      $data['create_clients_list'] = $this->db->query("select * from user where (firm_admin_id='" . $id . "' or id=" . $id . ") and crm_name != '' ")->result_array();
      $user_id = $this->session->userdata('id');
      $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where role = 4 and crm_name !='' and  (firm_admin_id = " . $id . " or id=" . $id . " )  ")->row_array();

      $us_id =  explode(',', $sql['us_id']);
    } else {
      $id = $this->session->userdata('admin_id');
      $data['user_details'] = $this->Common_mdl->select_record('user', 'id', $id);
      $data['create_clients_list'] = $this->db->query("select * from user where firm_admin_id='" . $id . "' and crm_name != '' ")->result_array();
      $user_id = $this->session->userdata('id');
      $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = " . $id . " and role = 4 and crm_name !=''")->row_array();

      $us_id =  explode(',', $sql['us_id']);
    }



    //  $data['getCompany'] = $this->db->query( "SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !=''")->result_array();
    $current_date = date("Y-m-d");
    $time = date("Y-m-d");;

    $data['getCompany_today'] = $this->db->query("SELECT * FROM client WHERE user_id IN ( '" . implode("','", $us_id) . "' ) and crm_company_name !='' and ((crm_confirmation_statement_due_date= '" . $current_date . "') or (crm_ch_accounts_next_due = '" . $current_date . "') or (crm_accounts_tax_date_hmrc = '" . $current_date . "' ) or (crm_personal_due_date_return = '" . $current_date . "' ) or (crm_vat_due_date = '" . $current_date . "' ) or (crm_rti_deadline = '" . $current_date . "')  or (crm_pension_subm_due_date = '" . $current_date . "') or (crm_next_p11d_return_due = '" . $current_date . "')  or (crm_next_manage_acc_date = '" . $current_date . "') or (crm_next_booking_date = '" . $current_date . "' ) or (crm_insurance_renew_date = '" . $current_date . "')  or (crm_registered_renew_date = '" . $current_date . "' ) or (crm_investigation_end_date = '" . $current_date . "' )) ")->result_array();

    $oneweek_end_date = date('Y-m-d', strtotime($time . ' -1 week'));
    $con = "'" . $oneweek_end_date . "' AND '" . $current_date . "'";
    //echo $end_date;
    $data['getCompany_oneweek'] = $this->db->query("SELECT * FROM client WHERE user_id IN ( '" . implode("','", $us_id) . "' ) and crm_company_name !='' and ((crm_confirmation_statement_due_date BETWEEN " . $con . " ) or (crm_ch_accounts_next_due BETWEEN " . $con . " ) or (crm_accounts_tax_date_hmrc BETWEEN " . $con . " ) or (crm_personal_due_date_return BETWEEN " . $con . " ) or (crm_vat_due_date BETWEEN " . $con . " ) or (crm_rti_deadline BETWEEN " . $con . " )  or (crm_pension_subm_due_date BETWEEN " . $con . " ) or (crm_next_p11d_return_due BETWEEN " . $con . " )  or (crm_next_manage_acc_date BETWEEN " . $con . " ) or (crm_next_booking_date BETWEEN " . $con . " ) or (crm_insurance_renew_date BETWEEN " . $con . " )  or (crm_registered_renew_date BETWEEN " . $con . " ) or (crm_investigation_end_date BETWEEN " . $con . " )) ")->result_array();

    $onemonth_end_date = date('Y-m-d', strtotime($time . ' -1 month'));
    $con = "'" . $onemonth_end_date . "' AND '" . $current_date . "'";
    //echo $end_date;
    $data['getCompany_onemonth'] = $this->db->query("SELECT * FROM client WHERE user_id IN ( '" . implode("','", $us_id) . "' ) and crm_company_name !='' and ((crm_confirmation_statement_due_date BETWEEN " . $con . " ) or (crm_ch_accounts_next_due BETWEEN " . $con . " ) or (crm_accounts_tax_date_hmrc BETWEEN " . $con . " ) or (crm_personal_due_date_return BETWEEN " . $con . " ) or (crm_vat_due_date BETWEEN " . $con . " ) or (crm_rti_deadline BETWEEN " . $con . " )  or (crm_pension_subm_due_date BETWEEN " . $con . " ) or (crm_next_p11d_return_due BETWEEN " . $con . " )  or (crm_next_manage_acc_date BETWEEN " . $con . " ) or (crm_next_booking_date BETWEEN " . $con . " ) or (crm_insurance_renew_date BETWEEN " . $con . " )  or (crm_registered_renew_date BETWEEN " . $con . " ) or (crm_investigation_end_date BETWEEN " . $con . " )) ")->result_array();

    $oneyear_end_date = date('Y-m-d', strtotime($time . ' -1 year'));
    $con = "'" . $oneyear_end_date . "' AND '" . $current_date . "'";
    //echo $end_date;
    $data['getCompany_oneyear'] = $this->db->query("SELECT * FROM client WHERE user_id IN ( '" . implode("','", $us_id) . "' ) and crm_company_name !='' and ((crm_confirmation_statement_due_date BETWEEN " . $con . " ) or (crm_ch_accounts_next_due BETWEEN " . $con . " ) or (crm_accounts_tax_date_hmrc BETWEEN " . $con . " ) or (crm_personal_due_date_return BETWEEN " . $con . " ) or (crm_vat_due_date BETWEEN " . $con . " ) or (crm_rti_deadline BETWEEN " . $con . " )  or (crm_pension_subm_due_date BETWEEN " . $con . " ) or (crm_next_p11d_return_due BETWEEN " . $con . " )  or (crm_next_manage_acc_date BETWEEN " . $con . " ) or (crm_next_booking_date BETWEEN " . $con . " ) or (crm_insurance_renew_date BETWEEN " . $con . " )  or (crm_registered_renew_date BETWEEN " . $con . " ) or (crm_investigation_end_date BETWEEN " . $con . " )) ")->result_array();

    header("Content-Description: File Transfer");

    if (isset($_GET['de'])) {
      $filename = 'dedaline_' . date('Ymd') . '.csv';
    }
    if (isset($_GET['de7'])) {
      $filename = 'deadline_' . date('Ymd') . '.xlsx';
    }

    header("Content-Disposition: attachment; filename=" . $filename . "");
    header("Content-Type: application/csv; ");

    $file = fopen('php://output', 'w');
    $header = array("Today");
    fputcsv($file, $header);
    $header = array("Client Name", "Client Type", "Company Name", "Deadline Type", "Date");
    fputcsv($file, $header);
    $i = 0;

    $new_array = array();
    $new_array_week = array();
    $new_array_month = array();
    $new_array_year = array();

    foreach ($data['getCompany_today'] as $getCompanykey => $getCompanyvalue) {

      // if($line['id']!=0){ $i++; $line['id']=$i; }
      $getusername = $this->Common_mdl->select_record('user', 'id', $getCompanyvalue['user_id']);

      if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date']))) {

        $new_array[$getCompanykey][1]['crm_name'] = $getusername['crm_name'];
        $new_array[$getCompanykey][1]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array[$getCompanykey][1]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array[$getCompanykey][1]['crm_deadline_types'] = "Confirmation statement Due Date";
        $new_array[$getCompanykey][1]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) : '');

        fputcsv($file, $new_array[$getCompanykey][1]);
      }
      if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due']))) {

        $new_array[$getCompanykey][2]['crm_name'] = $getusername['crm_name'];
        $new_array[$getCompanykey][2]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array[$getCompanykey][2]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array[$getCompanykey][2]['crm_deadline_types'] = "Accounts Due Date";

        $new_array[$getCompanykey][2]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) : '');


        fputcsv($file, $new_array[$getCompanykey][2]);
      }
      if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']))) {

        $new_array[$getCompanykey][3]['crm_name'] = $getusername['crm_name'];
        $new_array[$getCompanykey][3]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array[$getCompanykey][3]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array[$getCompanykey][3]['crm_deadline_types'] = "Company Tax Return Due Date";
        $new_array[$getCompanykey][3]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) : '');


        fputcsv($file, $new_array[$getCompanykey][3]);
      }
      if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return']))) {

        $new_array[$getCompanykey][4]['crm_name'] = $getusername['crm_name'];
        $new_array[$getCompanykey][4]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array[$getCompanykey][4]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array[$getCompanykey][4]['crm_deadline_types'] = "Personal Tax Return Due Date";
        $new_array[$getCompanykey][4]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_personal_due_date_return']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) : '');


        fputcsv($file, $new_array[$getCompanykey][4]);
      }
      if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date']))) {

        $new_array[$getCompanykey][5]['crm_name'] = $getusername['crm_name'];
        $new_array[$getCompanykey][5]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array[$getCompanykey][5]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array[$getCompanykey][5]['crm_deadline_types'] = "VAT Due Date";
        $new_array[$getCompanykey][5]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_vat_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) : '');


        fputcsv($file, $new_array[$getCompanykey][5]);
      }
      if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline']))) {

        $new_array[$getCompanykey][6]['crm_name'] = $getusername['crm_name'];
        $new_array[$getCompanykey][6]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array[$getCompanykey][6]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array[$getCompanykey][6]['crm_deadline_types'] = "Payroll Due Date";
        $new_array[$getCompanykey][6]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_rti_deadline']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) : '');


        fputcsv($file, $new_array[$getCompanykey][6]);
      }
      if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date']))) {

        $new_array[$getCompanykey][7]['crm_name'] = $getusername['crm_name'];
        $new_array[$getCompanykey][7]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array[$getCompanykey][7]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array[$getCompanykey][7]['crm_deadline_types'] = " WorkPlace Pension - AE Due Date";
        $new_array[$getCompanykey][7]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_pension_subm_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) : '');


        fputcsv($file, $new_array[$getCompanykey][7]);
      }


      $new_array[$getCompanykey][8]['crm_name'] = $getusername['crm_name'];
      $new_array[$getCompanykey][8]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

      $new_array[$getCompanykey][8]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
      $new_array[$getCompanykey][8]['crm_deadline_types'] = "CIS - Contractor Due Date";
      $new_array[$getCompanykey][8]['crm_deadline_date'] = "-";
      fputcsv($file, $new_array[$getCompanykey][8]);




      $new_array[$getCompanykey][9]['crm_name'] = $getusername['crm_name'];
      $new_array[$getCompanykey][9]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

      $new_array[$getCompanykey][9]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
      $new_array[$getCompanykey][9]['crm_deadline_types'] = "CIS - Sub Contractor Due Date";
      $new_array[$getCompanykey][9]['crm_deadline_date'] = "-";
      fputcsv($file, $new_array[$getCompanykey][9]);



      if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due']))) {

        $new_array[$getCompanykey][10]['crm_name'] = $getusername['crm_name'];
        $new_array[$getCompanykey][10]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array[$getCompanykey][10]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array[$getCompanykey][10]['crm_deadline_types'] = "P11D Due Date";
        $new_array[$getCompanykey][10]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_next_p11d_return_due']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) : '');


        fputcsv($file, $new_array[$getCompanykey][10]);
      }
      if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date']))) {

        $new_array[$getCompanykey][11]['crm_name'] = $getusername['crm_name'];
        $new_array[$getCompanykey][11]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array[$getCompanykey][11]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array[$getCompanykey][11]['crm_deadline_types'] = "Management Accounts Due Date";
        $new_array[$getCompanykey][11]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_next_manage_acc_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) : '');


        fputcsv($file, $new_array[$getCompanykey][11]);
      }
      if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date']))) {

        $new_array[$getCompanykey][12]['crm_name'] = $getusername['crm_name'];
        $new_array[$getCompanykey][12]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array[$getCompanykey][12]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array[$getCompanykey][12]['crm_deadline_types'] = "Bookkeeping Due Date";
        $new_array[$getCompanykey][12]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_next_booking_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) : '');


        fputcsv($file, $new_array[$getCompanykey][12]);
      }
      if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date']))) {

        $new_array[$getCompanykey][13]['crm_name'] = $getusername['crm_name'];
        $new_array[$getCompanykey][13]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array[$getCompanykey][13]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array[$getCompanykey][13]['crm_deadline_types'] = "Investigation Insurance Due Date";
        $new_array[$getCompanykey][13]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_insurance_renew_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) : '');


        fputcsv($file, $new_array[$getCompanykey][13]);
      }
      if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date']))) {

        $new_array[$getCompanykey][14]['crm_name'] = $getusername['crm_name'];
        $new_array[$getCompanykey][14]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array[$getCompanykey][14]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array[$getCompanykey][14]['crm_deadline_types'] = "Registered Address Due Date";
        $new_array[$getCompanykey][14]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_registered_renew_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) : '');


        fputcsv($file, $new_array[$getCompanykey][14]);
      }
      if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date']))) {

        $new_array[$getCompanykey][15]['crm_name'] = $getusername['crm_name'];
        $new_array[$getCompanykey][15]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array[$getCompanykey][15]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array[$getCompanykey][15]['crm_deadline_types'] = "Tax Advice Due Date";
        $new_array[$getCompanykey][15]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_investigation_end_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) : '');


        fputcsv($file, $new_array[$getCompanykey][15]);
      }
      if ($current_date == date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date']))) {

        $new_array[$getCompanykey][16]['crm_name'] = $getusername['crm_name'];
        $new_array[$getCompanykey][16]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array[$getCompanykey][16]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array[$getCompanykey][16]['crm_deadline_types'] = "Tax Investigation Due Date";
        $new_array[$getCompanykey][16]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_investigation_end_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) : '');

        fputcsv($file, $new_array[$getCompanykey][16]);
      }
      // fputcsv($file,$new_array);
      // echo json_encode($new_array);
      // echo "<br>";
    }

    /**************************************************************/

    $header = array("This Week");
    fputcsv($file, $header);
    $header = array("Client Name", "Client Type", "Company Name", "Deadline Type", "Date");
    fputcsv($file, $header);
    $i = 0;
    $time = date('Y-m-d');

    $oneweek_end_date = date('Y-m-d', strtotime($time . ' +1 week'));
    //  $new_array=array();
    foreach ($data['getCompany_oneweek'] as $getCompanykey => $getCompanyvalue) {

      $getusername = $this->Common_mdl->select_record('user', 'id', $getCompanyvalue['user_id']);
      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date']))) {

        $new_array_week[$getCompanykey][1]['crm_name'] = $getusername['crm_name'];
        $new_array_week[$getCompanykey][1]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_week[$getCompanykey][1]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_week[$getCompanykey][1]['crm_deadline_types'] = "Confirmation statement Due Date";
        $new_array_week[$getCompanykey][1]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) : '');

        fputcsv($file, $new_array_week[$getCompanykey][1]);
      }
      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due']))) {

        $new_array_week[$getCompanykey][2]['crm_name'] = $getusername['crm_name'];
        $new_array_week[$getCompanykey][2]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_week[$getCompanykey][2]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_week[$getCompanykey][2]['crm_deadline_types'] = "Accounts Due Date";

        $new_array_week[$getCompanykey][2]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) : '');


        fputcsv($file, $new_array_week[$getCompanykey][2]);
      }
      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']))) {

        $new_array_week[$getCompanykey][3]['crm_name'] = $getusername['crm_name'];
        $new_array_week[$getCompanykey][3]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];
        $new_array_week[$getCompanykey][3]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_week[$getCompanykey][3]['crm_deadline_types'] = "Company Tax Return Due Date";
        $new_array_week[$getCompanykey][3]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) : '');


        fputcsv($file, $new_array_week[$getCompanykey][3]);
      }
      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']))) {

        $new_array_week[$getCompanykey][4]['crm_name'] = $getusername['crm_name'];
        $new_array_week[$getCompanykey][4]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_week[$getCompanykey][4]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_week[$getCompanykey][4]['crm_deadline_types'] = "Personal Tax Return Due Date";
        $new_array_week[$getCompanykey][4]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_personal_due_date_return']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) : '');


        fputcsv($file, $new_array_week[$getCompanykey][4]);
      }
      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date']))) {

        $new_array_week[$getCompanykey][5]['crm_name'] = $getusername['crm_name'];
        $new_array_week[$getCompanykey][5]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_week[$getCompanykey][5]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_week[$getCompanykey][5]['crm_deadline_types'] = "VAT Due Date";
        $new_array_week[$getCompanykey][5]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_vat_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) : '');


        fputcsv($file, $new_array_week[$getCompanykey][5]);
      }
      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline']))) {

        $new_array_week[$getCompanykey][6]['crm_name'] = $getusername['crm_name'];
        $new_array_week[$getCompanykey][6]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_week[$getCompanykey][6]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_week[$getCompanykey][6]['crm_deadline_types'] = "Payroll Due Date";
        $new_array_week[$getCompanykey][6]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_rti_deadline']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) : '');


        fputcsv($file, $new_array_week[$getCompanykey][6]);
      }
      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date']))) {

        $new_array_week[$getCompanykey][7]['crm_name'] = $getusername['crm_name'];
        $new_array_week[$getCompanykey][7]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_week[$getCompanykey][7]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_week[$getCompanykey][7]['crm_deadline_types'] = "WorkPlace Pension - AE Due Date";
        $new_array_week[$getCompanykey][7]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_pension_subm_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) : '');


        fputcsv($file, $new_array_week[$getCompanykey][7]);
      }


      $new_array_week[$getCompanykey][8]['crm_name'] = $getusername['crm_name'];
      $new_array_week[$getCompanykey][8]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

      $new_array_week[$getCompanykey][8]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
      $new_array_week[$getCompanykey][8]['crm_deadline_types'] = "CIS - Contractor Due Date";
      $new_array_week[$getCompanykey][8]['crm_deadline_date'] = "-";
      fputcsv($file, $new_array_week[$getCompanykey][8]);


      $new_array_week[$getCompanykey][9]['crm_name'] = $getusername['crm_name'];
      $new_array_week[$getCompanykey][9]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

      $new_array_week[$getCompanykey][9]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
      $new_array_week[$getCompanykey][9]['crm_deadline_types'] = "CIS - Sub Contractor Due Date";
      $new_array_week[$getCompanykey][9]['crm_deadline_date'] = "-";
      fputcsv($file, $new_array_week[$getCompanykey][9]);

      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due']))) {

        $new_array_week[$getCompanykey][10]['crm_name'] = $getusername['crm_name'];
        $new_array_week[$getCompanykey][10]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_week[$getCompanykey][10]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_week[$getCompanykey][10]['crm_deadline_types'] = "P11D Due Date";
        $new_array_week[$getCompanykey][10]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_next_p11d_return_due']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) : '');


        fputcsv($file, $new_array_week[$getCompanykey][10]);
      }

      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date']))) {

        $new_array_week[$getCompanykey][11]['crm_name'] = $getusername['crm_name'];
        $new_array_week[$getCompanykey][11]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_week[$getCompanykey][11]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_week[$getCompanykey][11]['crm_deadline_types'] = "Management Accounts Due Date";
        $new_array_week[$getCompanykey][11]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_next_manage_acc_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) : '');


        fputcsv($file, $new_array_week[$getCompanykey][11]);
      }
      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date']))) {

        $new_array_week[$getCompanykey][12]['crm_name'] = $getusername['crm_name'];
        $new_array_week[$getCompanykey][12]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_week[$getCompanykey][12]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_week[$getCompanykey][12]['crm_deadline_types'] = "Bookkeeping Due Date";
        $new_array_week[$getCompanykey][12]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_next_booking_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) : '');


        fputcsv($file, $new_array_week[$getCompanykey][12]);
      }
      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date']))) {

        $new_array_week[$getCompanykey][13]['crm_name'] = $getusername['crm_name'];
        $new_array_week[$getCompanykey][13]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_week[$getCompanykey][13]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_week[$getCompanykey][13]['crm_deadline_types'] = "Investigation Insurance Due Date";
        $new_array_week[$getCompanykey][13]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_insurance_renew_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) : '');


        fputcsv($file, $new_array_week[$getCompanykey][13]);
      }
      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date']))) {

        $new_array_week[$getCompanykey][14]['crm_name'] = $getusername['crm_name'];
        $new_array_week[$getCompanykey][14]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_week[$getCompanykey][14]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_week[$getCompanykey][14]['crm_deadline_types'] = "Registered Address Due Date";
        $new_array_week[$getCompanykey][14]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_registered_renew_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) : '');


        fputcsv($file, $new_array_week[$getCompanykey][14]);
      }
      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date']))) {

        $new_array_week[$getCompanykey][15]['crm_name'] = $getusername['crm_name'];
        $new_array_week[$getCompanykey][15]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_week[$getCompanykey][15]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_week[$getCompanykey][15]['crm_deadline_types'] = "Tax Advice Due Date";
        $new_array_week[$getCompanykey][15]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_investigation_end_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) : '');

        fputcsv($file, $new_array_week[$getCompanykey][15]);
      }
      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneweek_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date']))) {

        $new_array_week[$getCompanykey][16]['crm_name'] = $getusername['crm_name'];
        $new_array_week[$getCompanykey][16]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_week[$getCompanykey][16]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_week[$getCompanykey][16]['crm_deadline_types'] = "Tax Investigation Due Date";
        $new_array_week[$getCompanykey][16]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_investigation_end_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) : '');


        fputcsv($file, $new_array_week[$getCompanykey][16]);
      }
      // fputcsv($file,$new_array);
    }
    /**********************************************************/
    /** for this month **/

    $header = array("This Month");
    fputcsv($file, $header);
    $header = array("Client Name", "Client Type", "Company Name", "Deadline Type", "Date");
    fputcsv($file, $header);
    $i = 0;
    $time = date('Y-m-d');
    $onemonth_end_date = date('Y-m-d', strtotime($time . ' +1 month'));
    foreach ($data['getCompany_onemonth'] as $getCompanykey => $getCompanyvalue) {
      $getusername = $this->Common_mdl->select_record('user', 'id', $getCompanyvalue['user_id']);

      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date']))) {

        $new_array_month[$getCompanykey][1]['crm_name'] = $getusername['crm_name'];
        $new_array_month[$getCompanykey][1]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_month[$getCompanykey][1]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_month[$getCompanykey][1]['crm_deadline_types'] = "Confirmation statement Due Date";
        $new_array_month[$getCompanykey][1]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) : '');

        fputcsv($file, $new_array_month[$getCompanykey][1]);
      }

      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due']))) {

        $new_array_month[$getCompanykey][2]['crm_name'] = $getusername['crm_name'];
        $new_array_month[$getCompanykey][2]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_month[$getCompanykey][2]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_month[$getCompanykey][2]['crm_deadline_types'] = "Accounts Due Date";

        $new_array_month[$getCompanykey][2]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) : '');


        fputcsv($file, $new_array_month[$getCompanykey][2]);
      }

      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']))) {

        $new_array_month[$getCompanykey][3]['crm_name'] = $getusername['crm_name'];
        $new_array_month[$getCompanykey][3]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];
        $new_array_month[$getCompanykey][3]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_month[$getCompanykey][3]['crm_deadline_types'] = "Company Tax Return Due Date";
        $new_array_month[$getCompanykey][3]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) : '');


        fputcsv($file, $new_array_month[$getCompanykey][3]);
      }

      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']))) {

        $new_array_month[$getCompanykey][4]['crm_name'] = $getusername['crm_name'];
        $new_array_month[$getCompanykey][4]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_month[$getCompanykey][4]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_month[$getCompanykey][4]['crm_deadline_types'] = "Personal Tax Return Due Date";
        $new_array_month[$getCompanykey][4]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_personal_due_date_return']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) : '');


        fputcsv($file, $new_array_month[$getCompanykey][4]);
      }

      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date']))) {

        $new_array_month[$getCompanykey][5]['crm_name'] = $getusername['crm_name'];
        $new_array_month[$getCompanykey][5]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_month[$getCompanykey][5]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_month[$getCompanykey][5]['crm_deadline_types'] = "VAT Due Date";
        $new_array_month[$getCompanykey][5]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_vat_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) : '');


        fputcsv($file, $new_array_month[$getCompanykey][5]);
      }

      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline']))) {

        $new_array_month[$getCompanykey][6]['crm_name'] = $getusername['crm_name'];
        $new_array_month[$getCompanykey][6]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_month[$getCompanykey][6]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_month[$getCompanykey][6]['crm_deadline_types'] = "Payroll Due Date";
        $new_array_month[$getCompanykey][6]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_rti_deadline']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) : '');


        fputcsv($file, $new_array_month[$getCompanykey][6]);
      }

      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date']))) {

        $new_array_month[$getCompanykey][7]['crm_name'] = $getusername['crm_name'];
        $new_array_month[$getCompanykey][7]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_month[$getCompanykey][7]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_month[$getCompanykey][7]['crm_deadline_types'] = "WorkPlace Pension - AE Due Date";
        $new_array_month[$getCompanykey][7]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_pension_subm_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) : '');


        fputcsv($file, $new_array_month[$getCompanykey][7]);
      }


      $new_array_month[$getCompanykey][8]['crm_name'] = $getusername['crm_name'];
      $new_array_month[$getCompanykey][8]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

      $new_array_month[$getCompanykey][8]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
      $new_array_month[$getCompanykey][8]['crm_deadline_types'] = "CIS - Contractor Due Date";
      $new_array_month[$getCompanykey][8]['crm_deadline_date'] = "-";
      fputcsv($file, $new_array_month[$getCompanykey][8]);


      $new_array_month[$getCompanykey][9]['crm_name'] = $getusername['crm_name'];
      $new_array_month[$getCompanykey][9]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

      $new_array_month[$getCompanykey][9]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
      $new_array_month[$getCompanykey][9]['crm_deadline_types'] = "CIS - Sub Contractor Due Date";
      $new_array_month[$getCompanykey][9]['crm_deadline_date'] = "-";
      fputcsv($file, $new_array_month[$getCompanykey][9]);


      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due']))) {

        $new_array_month[$getCompanykey][10]['crm_name'] = $getusername['crm_name'];
        $new_array_month[$getCompanykey][10]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_month[$getCompanykey][10]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_month[$getCompanykey][10]['crm_deadline_types'] = "P11D Due Date";
        $new_array_month[$getCompanykey][10]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_next_p11d_return_due']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) : '');


        fputcsv($file, $new_array_month[$getCompanykey][10]);
      }

      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date']))) {

        $new_array_month[$getCompanykey][11]['crm_name'] = $getusername['crm_name'];
        $new_array_month[$getCompanykey][11]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_month[$getCompanykey][11]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_month[$getCompanykey][11]['crm_deadline_types'] = "Management Accounts Due Date";
        $new_array_month[$getCompanykey][11]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_next_manage_acc_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) : '');


        fputcsv($file, $new_array_month[$getCompanykey][11]);
      }

      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date']))) {

        $new_array_month[$getCompanykey][12]['crm_name'] = $getusername['crm_name'];
        $new_array_month[$getCompanykey][12]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_month[$getCompanykey][12]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_month[$getCompanykey][12]['crm_deadline_types'] = "Bookkeeping Due Date";
        $new_array_month[$getCompanykey][12]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_next_booking_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) : '');


        fputcsv($file, $new_array_month[$getCompanykey][12]);
      }

      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date']))) {

        $new_array_month[$getCompanykey][13]['crm_name'] = $getusername['crm_name'];
        $new_array_month[$getCompanykey][13]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_month[$getCompanykey][13]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_month[$getCompanykey][13]['crm_deadline_types'] = "Investigation Insurance Due Date";
        $new_array_month[$getCompanykey][13]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_insurance_renew_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) : '');


        fputcsv($file, $new_array_month[$getCompanykey][13]);
      }

      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date']))) {

        $new_array_month[$getCompanykey][14]['crm_name'] = $getusername['crm_name'];
        $new_array_month[$getCompanykey][14]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_month[$getCompanykey][14]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_month[$getCompanykey][14]['crm_deadline_types'] = "Registered Address Due Date";
        $new_array_month[$getCompanykey][14]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_registered_renew_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) : '');


        fputcsv($file, $new_array_month[$getCompanykey][14]);
      }

      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date']))) {

        $new_array_month[$getCompanykey][15]['crm_name'] = $getusername['crm_name'];
        $new_array_month[$getCompanykey][15]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_month[$getCompanykey][15]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_month[$getCompanykey][15]['crm_deadline_types'] = "Tax Advice Due Date";
        $new_array_month[$getCompanykey][15]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_investigation_end_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) : '');

        fputcsv($file, $new_array_month[$getCompanykey][15]);
      }


      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $onemonth_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date']))) {

        $new_array_month[$getCompanykey][16]['crm_name'] = $getusername['crm_name'];
        $new_array_month[$getCompanykey][16]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_month[$getCompanykey][16]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_month[$getCompanykey][16]['crm_deadline_types'] = "Tax Investigation Due Date";
        $new_array_month[$getCompanykey][16]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_investigation_end_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) : '');


        fputcsv($file, $new_array_month[$getCompanykey][16]);
      }
    }

    /*********************************************************/
    /** this year **/
    $header = array("This Year");
    fputcsv($file, $header);
    $header = array("Client Name", "Client Type", "Company Name", "Deadline Type", "Date");
    fputcsv($file, $header);
    $i = 0;
    $time = date('Y-m-d');

    $oneyear_end_date = date('Y-m-d', strtotime($time . ' +1 year'));

    foreach ($data['getCompany_oneyear'] as $getCompanykey => $getCompanyvalue) {

      $getusername = $this->Common_mdl->select_record('user', 'id', $getCompanyvalue['user_id']);
      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date']))) {

        $new_array_year[$getCompanykey][1]['crm_name'] = $getusername['crm_name'];
        $new_array_year[$getCompanykey][1]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_year[$getCompanykey][1]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_year[$getCompanykey][1]['crm_deadline_types'] = "Confirmation statement Due Date";
        $new_array_year[$getCompanykey][1]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])) : '');

        fputcsv($file, $new_array_year[$getCompanykey][1]);
      }

      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due']))) {

        $new_array_year[$getCompanykey][2]['crm_name'] = $getusername['crm_name'];
        $new_array_year[$getCompanykey][2]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_year[$getCompanykey][2]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_year[$getCompanykey][2]['crm_deadline_types'] = "Accounts Due Date";

        $new_array_year[$getCompanykey][2]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])) : '');


        fputcsv($file, $new_array_year[$getCompanykey][2]);
      }

      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']))) {

        $new_array_year[$getCompanykey][3]['crm_name'] = $getusername['crm_name'];
        $new_array_year[$getCompanykey][3]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];
        $new_array_year[$getCompanykey][3]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_year[$getCompanykey][3]['crm_deadline_types'] = "Company Tax Return Due Date";
        $new_array_year[$getCompanykey][3]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) : '');


        fputcsv($file, $new_array_year[$getCompanykey][3]);
      }

      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']))) {

        $new_array_year[$getCompanykey][4]['crm_name'] = $getusername['crm_name'];
        $new_array_year[$getCompanykey][4]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_year[$getCompanykey][4]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_year[$getCompanykey][4]['crm_deadline_types'] = "Personal Tax Return Due Date";
        $new_array_year[$getCompanykey][4]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_personal_due_date_return']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])) : '');


        fputcsv($file, $new_array_year[$getCompanykey][4]);
      }

      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date']))) {

        $new_array_year[$getCompanykey][5]['crm_name'] = $getusername['crm_name'];
        $new_array_year[$getCompanykey][5]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_year[$getCompanykey][5]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_year[$getCompanykey][5]['crm_deadline_types'] = "VAT Due Date";
        $new_array_year[$getCompanykey][5]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_vat_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])) : '');


        fputcsv($file, $new_array_year[$getCompanykey][5]);
      }

      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline']))) {

        $new_array_year[$getCompanykey][6]['crm_name'] = $getusername['crm_name'];
        $new_array_year[$getCompanykey][6]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_year[$getCompanykey][6]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_year[$getCompanykey][6]['crm_deadline_types'] = "Payroll Due Date";
        $new_array_year[$getCompanykey][6]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_rti_deadline']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])) : '');


        fputcsv($file, $new_array_year[$getCompanykey][6]);
      }

      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date']))) {

        $new_array_year[$getCompanykey][7]['crm_name'] = $getusername['crm_name'];
        $new_array_year[$getCompanykey][7]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_year[$getCompanykey][7]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_year[$getCompanykey][7]['crm_deadline_types'] = "WorkPlace Pension - AE Due Date";
        $new_array_year[$getCompanykey][7]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_pension_subm_due_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])) : '');


        fputcsv($file, $new_array_year[$getCompanykey][7]);
      }


      $new_array_year[$getCompanykey][8]['crm_name'] = $getusername['crm_name'];
      $new_array_year[$getCompanykey][8]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

      $new_array_year[$getCompanykey][8]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
      $new_array_year[$getCompanykey][8]['crm_deadline_types'] = "CIS - Contractor Due Date";
      $new_array_year[$getCompanykey][8]['crm_deadline_date'] = "-";
      fputcsv($file, $new_array_year[$getCompanykey][8]);


      $new_array_year[$getCompanykey][9]['crm_name'] = $getusername['crm_name'];
      $new_array_year[$getCompanykey][9]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

      $new_array_year[$getCompanykey][9]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
      $new_array_year[$getCompanykey][9]['crm_deadline_types'] = "CIS - Sub Contractor Due Date";
      $new_array_year[$getCompanykey][9]['crm_deadline_date'] = "-";
      fputcsv($file, $new_array_year[$getCompanykey][9]);

      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due']))) {

        $new_array_year[$getCompanykey][10]['crm_name'] = $getusername['crm_name'];
        $new_array_year[$getCompanykey][10]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_year[$getCompanykey][10]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_year[$getCompanykey][10]['crm_deadline_types'] = "P11D Due Date";
        $new_array_year[$getCompanykey][10]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_next_p11d_return_due']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])) : '');


        fputcsv($file, $new_array_year[$getCompanykey][10]);
      }

      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date']))) {

        $new_array_year[$getCompanykey][11]['crm_name'] = $getusername['crm_name'];
        $new_array_year[$getCompanykey][11]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_year[$getCompanykey][11]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_year[$getCompanykey][11]['crm_deadline_types'] = "Management Accounts Due Date";
        $new_array_year[$getCompanykey][11]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_next_manage_acc_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])) : '');


        fputcsv($file, $new_array_year[$getCompanykey][11]);
      }

      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date']))) {

        $new_array_year[$getCompanykey][12]['crm_name'] = $getusername['crm_name'];
        $new_array_year[$getCompanykey][12]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_year[$getCompanykey][12]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_year[$getCompanykey][12]['crm_deadline_types'] = "Bookkeeping Due Date";
        $new_array_year[$getCompanykey][12]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_next_booking_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])) : '');


        fputcsv($file, $new_array_year[$getCompanykey][12]);
      }

      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date']))) {

        $new_array_year[$getCompanykey][13]['crm_name'] = $getusername['crm_name'];
        $new_array_year[$getCompanykey][13]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_year[$getCompanykey][13]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_year[$getCompanykey][13]['crm_deadline_types'] = "Investigation Insurance Due Date";
        $new_array_year[$getCompanykey][13]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_insurance_renew_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])) : '');


        fputcsv($file, $new_array_year[$getCompanykey][13]);
      }

      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date']))) {

        $new_array_year[$getCompanykey][14]['crm_name'] = $getusername['crm_name'];
        $new_array_year[$getCompanykey][14]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_year[$getCompanykey][14]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_year[$getCompanykey][14]['crm_deadline_types'] = "Registered Address Due Date";
        $new_array_year[$getCompanykey][14]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_registered_renew_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])) : '');


        fputcsv($file, $new_array_year[$getCompanykey][14]);
      }

      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date']))) {

        $new_array_year[$getCompanykey][15]['crm_name'] = $getusername['crm_name'];
        $new_array_year[$getCompanykey][15]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_year[$getCompanykey][15]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_year[$getCompanykey][15]['crm_deadline_types'] = "Tax Advice Due Date";
        $new_array_year[$getCompanykey][15]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_investigation_end_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) : '');

        fputcsv($file, $new_array_year[$getCompanykey][15]);
      }

      if ($current_date <= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) && $oneyear_end_date >= date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date']))) {

        $new_array_year[$getCompanykey][16]['crm_name'] = $getusername['crm_name'];
        $new_array_year[$getCompanykey][16]['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];

        $new_array_year[$getCompanykey][16]['crm_company_name'] = $getCompanyvalue['crm_company_name'];
        $new_array_year[$getCompanykey][16]['crm_deadline_types'] = "Tax Investigation Due Date";
        $new_array_year[$getCompanykey][16]['crm_deadline_date'] = (strtotime($getCompanyvalue['crm_investigation_end_date']) != '' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])) : '');


        fputcsv($file, $new_array_year[$getCompanykey][16]);
      }
    }


    fclose($file);
    exit;
  }





  function pdf()
  {
    if ($_SESSION['role'] == 4) {

      $id = $_SESSION['id'];
      $data['user_details'] = $this->Common_mdl->select_record('user', 'id', $id);
      $data['create_clients_list'] = $this->db->query("select * from user where (firm_admin_id='" . $id . "' or id=" . $id . ") and crm_name != '' ")->result_array();
      $user_id = $this->session->userdata('id');
      $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where role = 4 and crm_name !='' and  (firm_admin_id = " . $id . " or id=" . $id . " )  ")->row_array();

      $us_id =  explode(',', $sql['us_id']);
    } else {
      $id = $this->session->userdata('admin_id');
      $data['user_details'] = $this->Common_mdl->select_record('user', 'id', $id);
      $data['create_clients_list'] = $this->db->query("select * from user where firm_admin_id='" . $id . "' and crm_name != '' ")->result_array();
      $user_id = $this->session->userdata('id');
      $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = " . $id . " and role = 4 and crm_name !=''")->row_array();

      $us_id =  explode(',', $sql['us_id']);
    }



    //  $data['getCompany'] = $this->db->query( "SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !=''")->result_array();
    $current_date = date("Y-m-d");
    $time = date("Y-m-d");;

    $data['getCompany_today'] = $this->db->query("SELECT * FROM client WHERE user_id IN ( '" . implode("','", $us_id) . "' ) and crm_company_name !='' and ((crm_confirmation_statement_due_date= '" . $current_date . "') or (crm_ch_accounts_next_due = '" . $current_date . "') or (crm_accounts_tax_date_hmrc = '" . $current_date . "' ) or (crm_personal_due_date_return = '" . $current_date . "' ) or (crm_vat_due_date = '" . $current_date . "' ) or (crm_rti_deadline = '" . $current_date . "')  or (crm_pension_subm_due_date = '" . $current_date . "') or (crm_next_p11d_return_due = '" . $current_date . "')  or (crm_next_manage_acc_date = '" . $current_date . "') or (crm_next_booking_date = '" . $current_date . "' ) or (crm_insurance_renew_date = '" . $current_date . "')  or (crm_registered_renew_date = '" . $current_date . "' ) or (crm_investigation_end_date = '" . $current_date . "' )) ")->result_array();

    $oneweek_end_date = date('Y-m-d', strtotime($time . ' -1 week'));
    $con = "'" . $oneweek_end_date . "' AND '" . $current_date . "'";
    //echo $end_date;
    $data['getCompany_oneweek'] = $this->db->query("SELECT * FROM client WHERE user_id IN ( '" . implode("','", $us_id) . "' ) and crm_company_name !='' and ((crm_confirmation_statement_due_date BETWEEN " . $con . " ) or (crm_ch_accounts_next_due BETWEEN " . $con . " ) or (crm_accounts_tax_date_hmrc BETWEEN " . $con . " ) or (crm_personal_due_date_return BETWEEN " . $con . " ) or (crm_vat_due_date BETWEEN " . $con . " ) or (crm_rti_deadline BETWEEN " . $con . " )  or (crm_pension_subm_due_date BETWEEN " . $con . " ) or (crm_next_p11d_return_due BETWEEN " . $con . " )  or (crm_next_manage_acc_date BETWEEN " . $con . " ) or (crm_next_booking_date BETWEEN " . $con . " ) or (crm_insurance_renew_date BETWEEN " . $con . " )  or (crm_registered_renew_date BETWEEN " . $con . " ) or (crm_investigation_end_date BETWEEN " . $con . " )) ")->result_array();

    $onemonth_end_date = date('Y-m-d', strtotime($time . ' -1 month'));
    $con = "'" . $onemonth_end_date . "' AND '" . $current_date . "'";
    //echo $end_date;
    $data['getCompany_onemonth'] = $this->db->query("SELECT * FROM client WHERE user_id IN ( '" . implode("','", $us_id) . "' ) and crm_company_name !='' and ((crm_confirmation_statement_due_date BETWEEN " . $con . " ) or (crm_ch_accounts_next_due BETWEEN " . $con . " ) or (crm_accounts_tax_date_hmrc BETWEEN " . $con . " ) or (crm_personal_due_date_return BETWEEN " . $con . " ) or (crm_vat_due_date BETWEEN " . $con . " ) or (crm_rti_deadline BETWEEN " . $con . " )  or (crm_pension_subm_due_date BETWEEN " . $con . " ) or (crm_next_p11d_return_due BETWEEN " . $con . " )  or (crm_next_manage_acc_date BETWEEN " . $con . " ) or (crm_next_booking_date BETWEEN " . $con . " ) or (crm_insurance_renew_date BETWEEN " . $con . " )  or (crm_registered_renew_date BETWEEN " . $con . " ) or (crm_investigation_end_date BETWEEN " . $con . " )) ")->result_array();

    $oneyear_end_date = date('Y-m-d', strtotime($time . ' -1 year'));
    $con = "'" . $oneyear_end_date . "' AND '" . $current_date . "'";
    //echo $end_date;
    $data['getCompany_oneyear'] = $this->db->query("SELECT * FROM client WHERE user_id IN ( '" . implode("','", $us_id) . "' ) and crm_company_name !='' and ((crm_confirmation_statement_due_date BETWEEN " . $con . " ) or (crm_ch_accounts_next_due BETWEEN " . $con . " ) or (crm_accounts_tax_date_hmrc BETWEEN " . $con . " ) or (crm_personal_due_date_return BETWEEN " . $con . " ) or (crm_vat_due_date BETWEEN " . $con . " ) or (crm_rti_deadline BETWEEN " . $con . " )  or (crm_pension_subm_due_date BETWEEN " . $con . " ) or (crm_next_p11d_return_due BETWEEN " . $con . " )  or (crm_next_manage_acc_date BETWEEN " . $con . " ) or (crm_next_booking_date BETWEEN " . $con . " ) or (crm_insurance_renew_date BETWEEN " . $con . " )  or (crm_registered_renew_date BETWEEN " . $con . " ) or (crm_investigation_end_date BETWEEN " . $con . " )) ")->result_array();

    $this->load->library('pdf');
    $customPaper = array(0, 0, 1000, 1000);
    $this->pdf->set_paper($customPaper);
    $this->pdf->load_view('Deadline_manager/pdf_view_new', $data);
    $this->pdf->render();
    $this->pdf->stream("User_list.pdf");
  }




  function html()
  {

    // $id = $this->session->userdata('admin_id');
    //    $data['user_details']=$this->Common_mdl->select_record('user','id',$id);
    //    $data['create_clients_list']=$this->db->query("select * from user where firm_admin_id='".$id."' and crm_name != '' ")->result_array();
    //     $user_id = $this->session->userdata('id');
    //       $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = ".$id." and role = 4 and crm_name !=''")->row_array();

    //    $us_id =  explode(',', $sql['us_id']);
    if ($_SESSION['role'] == 4) {

      $id = $_SESSION['id'];
      $data['user_details'] = $this->Common_mdl->select_record('user', 'id', $id);
      $data['create_clients_list'] = $this->db->query("select * from user where (firm_admin_id='" . $id . "' or id=" . $id . ") and crm_name != '' ")->result_array();
      $user_id = $this->session->userdata('id');
      $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where role = 4 and crm_name !='' and  (firm_admin_id = " . $id . " or id=" . $id . " )  ")->row_array();

      $us_id =  explode(',', $sql['us_id']);
    } else {
      $id = $this->session->userdata('admin_id');
      $data['user_details'] = $this->Common_mdl->select_record('user', 'id', $id);
      $data['create_clients_list'] = $this->db->query("select * from user where firm_admin_id='" . $id . "' and crm_name != '' ")->result_array();
      $user_id = $this->session->userdata('id');
      $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = " . $id . " and role = 4 and crm_name !=''")->row_array();

      $us_id =  explode(',', $sql['us_id']);
    }



    //  $data['getCompany'] = $this->db->query( "SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !=''")->result_array();
    $current_date = date("Y-m-d");
    $time = date("Y-m-d");;

    $data['getCompany_today'] = $this->db->query("SELECT * FROM client WHERE user_id IN ( '" . implode("','", $us_id) . "' ) and crm_company_name !='' and ((crm_confirmation_statement_due_date= '" . $current_date . "') or (crm_ch_accounts_next_due = '" . $current_date . "') or (crm_accounts_tax_date_hmrc = '" . $current_date . "' ) or (crm_personal_due_date_return = '" . $current_date . "' ) or (crm_vat_due_date = '" . $current_date . "' ) or (crm_rti_deadline = '" . $current_date . "')  or (crm_pension_subm_due_date = '" . $current_date . "') or (crm_next_p11d_return_due = '" . $current_date . "')  or (crm_next_manage_acc_date = '" . $current_date . "') or (crm_next_booking_date = '" . $current_date . "' ) or (crm_insurance_renew_date = '" . $current_date . "')  or (crm_registered_renew_date = '" . $current_date . "' ) or (crm_investigation_end_date = '" . $current_date . "' )) ")->result_array();

    $oneweek_end_date = date('Y-m-d', strtotime($time . ' -1 week'));
    $con = "'" . $oneweek_end_date . "' AND '" . $current_date . "'";
    //echo $end_date;
    $data['getCompany_oneweek'] = $this->db->query("SELECT * FROM client WHERE user_id IN ( '" . implode("','", $us_id) . "' ) and crm_company_name !='' and ((crm_confirmation_statement_due_date BETWEEN " . $con . " ) or (crm_ch_accounts_next_due BETWEEN " . $con . " ) or (crm_accounts_tax_date_hmrc BETWEEN " . $con . " ) or (crm_personal_due_date_return BETWEEN " . $con . " ) or (crm_vat_due_date BETWEEN " . $con . " ) or (crm_rti_deadline BETWEEN " . $con . " )  or (crm_pension_subm_due_date BETWEEN " . $con . " ) or (crm_next_p11d_return_due BETWEEN " . $con . " )  or (crm_next_manage_acc_date BETWEEN " . $con . " ) or (crm_next_booking_date BETWEEN " . $con . " ) or (crm_insurance_renew_date BETWEEN " . $con . " )  or (crm_registered_renew_date BETWEEN " . $con . " ) or (crm_investigation_end_date BETWEEN " . $con . " )) ")->result_array();

    $onemonth_end_date = date('Y-m-d', strtotime($time . ' -1 month'));
    $con = "'" . $onemonth_end_date . "' AND '" . $current_date . "'";
    //echo $end_date;
    $data['getCompany_onemonth'] = $this->db->query("SELECT * FROM client WHERE user_id IN ( '" . implode("','", $us_id) . "' ) and crm_company_name !='' and ((crm_confirmation_statement_due_date BETWEEN " . $con . " ) or (crm_ch_accounts_next_due BETWEEN " . $con . " ) or (crm_accounts_tax_date_hmrc BETWEEN " . $con . " ) or (crm_personal_due_date_return BETWEEN " . $con . " ) or (crm_vat_due_date BETWEEN " . $con . " ) or (crm_rti_deadline BETWEEN " . $con . " )  or (crm_pension_subm_due_date BETWEEN " . $con . " ) or (crm_next_p11d_return_due BETWEEN " . $con . " )  or (crm_next_manage_acc_date BETWEEN " . $con . " ) or (crm_next_booking_date BETWEEN " . $con . " ) or (crm_insurance_renew_date BETWEEN " . $con . " )  or (crm_registered_renew_date BETWEEN " . $con . " ) or (crm_investigation_end_date BETWEEN " . $con . " )) ")->result_array();

    $oneyear_end_date = date('Y-m-d', strtotime($time . ' -1 year'));
    $con = "'" . $oneyear_end_date . "' AND '" . $current_date . "'";
    //echo $end_date;
    $data['getCompany_oneyear'] = $this->db->query("SELECT * FROM client WHERE user_id IN ( '" . implode("','", $us_id) . "' ) and crm_company_name !='' and ((crm_confirmation_statement_due_date BETWEEN " . $con . " ) or (crm_ch_accounts_next_due BETWEEN " . $con . " ) or (crm_accounts_tax_date_hmrc BETWEEN " . $con . " ) or (crm_personal_due_date_return BETWEEN " . $con . " ) or (crm_vat_due_date BETWEEN " . $con . " ) or (crm_rti_deadline BETWEEN " . $con . " )  or (crm_pension_subm_due_date BETWEEN " . $con . " ) or (crm_next_p11d_return_due BETWEEN " . $con . " )  or (crm_next_manage_acc_date BETWEEN " . $con . " ) or (crm_next_booking_date BETWEEN " . $con . " ) or (crm_insurance_renew_date BETWEEN " . $con . " )  or (crm_registered_renew_date BETWEEN " . $con . " ) or (crm_investigation_end_date BETWEEN " . $con . " )) ")->result_array();

    $this->load->view('Deadline_manager/deadline_print_page', $data);
  }






  /*******************************************/
  public function deadline_exportCSV_new_status()
  {
    $filename = 'status_board_' . date('Ymd') . '.csv';

    header("Content-Description: File Transfer");
    header("Content-Disposition: attachment; filename=$filename");
    header("Content-Type: application/csv; ");
    $com_num = $_GET['com_num'];
    $legal_form = $_GET['legal_form'];

    $id = $this->session->userdata('admin_id');


    // $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = ".$id." and role = 4 and crm_name !=''")->row_array();

    //    $us_id =  explode(',', $sql['us_id']);

    if ($_SESSION['role'] == 4) {
      $id = $_SESSION['id'];
      $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where  role = 4 and crm_name !='' and (firm_admin_id = " . $id . " or id=" . $id . ")  ")->row_array();

      $us_id =  explode(',', $sql['us_id']);
    } else {
      $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = " . $id . " and role = 4 and crm_name !=''")->row_array();

      $us_id =  explode(',', $sql['us_id']);
    }


    // $v_id = $_GET['values'];
    // $fields = $_GET['fields'];
    $for_values = explode(',', $_GET['values']);
    $for_fields = explode(',', $_GET['fields']);
    /*$v_id =  implode(',', $val);
echo '<pre>';
print_r($val);die;*/
    // if($v_id=='true')
    // {
    // $s_val = 'on';
    // }else{
    //   $s_val = 'off';
    // }


    $data['getCompany'] = $this->db->query("SELECT * FROM client WHERE user_id IN ( '" . implode("','", $us_id) . "' ) and crm_company_name !='' ")->result_array();



    $ids = array();
    foreach ($data['getCompany'] as $getCompanykey => $getCompanyvalue) {

      if ($getCompanyvalue['blocked_status'] == 0) {
        $conf_statement = (isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnCst =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnCst = 'off';

        $accounts = (isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnAcc =  json_decode($getCompanyvalue['accounts'])->tab : $jsnAcc = 'off';

        $company_tax_return = (isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsntax =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsntax = 'off';

        $personal_tax_return = (isset(json_decode($getCompanyvalue['personal_tax_return'])->tab) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnp_tax =  json_decode($getCompanyvalue['personal_tax_return'])->tab : $jsnp_tax = 'off';

        $payroll = (isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpay =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpay = 'off';

        $workplace = (isset(json_decode($getCompanyvalue['workplace'])->tab) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->tab : $jsnworkplace = 'off';

        $vat = (isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = 'off';

        $cis = (isset(json_decode($getCompanyvalue['cis'])->tab) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->tab : $jsncis = 'off';

        $cissub = (isset(json_decode($getCompanyvalue['cissub'])->tab) && $getCompanyvalue['cissub'] != '') ? $jsncis_sub =  json_decode($getCompanyvalue['cissub'])->tab : $jsncis_sub = 'off';

        $p11d = (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = 'off';

        $bookkeep = (isset(json_decode($getCompanyvalue['bookkeep'])->tab) && $getCompanyvalue['bookkeep'] != '') ? $jsnbk =  json_decode($getCompanyvalue['bookkeep'])->tab : $jsnbk = 'off';

        $management = (isset(json_decode($getCompanyvalue['management'])->tab) && $getCompanyvalue['management'] != '') ? $jsnmgnt =  json_decode($getCompanyvalue['management'])->tab : $jsnmgnt = 'off';

        $investgate = (isset(json_decode($getCompanyvalue['investgate'])->tab) && $getCompanyvalue['investgate'] != '') ? $jsninvest =  json_decode($getCompanyvalue['investgate'])->tab : $jsninvest = 'off';

        $registered = (isset(json_decode($getCompanyvalue['registered'])->tab) && $getCompanyvalue['registered'] != '') ? $jsnreg =  json_decode($getCompanyvalue['registered'])->tab : $jsnreg = 'off';

        $taxadvice = (isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxad =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxad = 'off';

        $taxinvest = (isset(json_decode($getCompanyvalue['taxinvest'])->tab) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->tab : $jsntaxinvest = 'off';
        $new_val = array();
        //echo count($for_fields);
        foreach (array_combine($for_fields, $for_values) as $fields => $v_id) {
          if ($v_id == 'true') {
            $s_val = 'on';
          } else {
            $s_val = 'off';
          }

          if ($fields == 'conf_statement') {
            $f_val = $conf_statement;
          } elseif ($fields == 'accounts') {
            $f_val = $accounts;
          } elseif ($fields == 'company_tax_return') {
            $f_val = $company_tax_return;
          } elseif ($fields == 'personal_tax_return') {
            $f_val = $personal_tax_return;
          } elseif ($fields == 'payroll') {
            $f_val = $payroll;
          } elseif ($fields == 'workplace') {
            $f_val = $workplace;
          } elseif ($fields == 'vat') {
            $f_val = $vat;
          } elseif ($fields == 'cis') {
            $f_val = $cis;
          } elseif ($fields == 'cissub') {
            $f_val = $cissub;
          } elseif ($fields == 'p11d') {
            $f_val = $p11d;
          } elseif ($fields == 'bookkeep') {
            $f_val = $bookkeep;
          } elseif ($fields == 'management') {
            $f_val = $management;
          } elseif ($fields == 'investgate') {
            $f_val = $investgate;
          } elseif ($fields == 'registered') {
            $f_val = $registered;
          } elseif ($fields == 'taxadvice') {
            $f_val = $taxadvice;
          } elseif ($fields == 'taxinvest') {
            $f_val = $taxinvest;
          } else {
            $f_val = '';
          }

          // if($v_id=='true'){
          //  if($f_val==$s_val)
          //  {
          //    $ids[] = $getCompanyvalue['id'];
          //  }}
          //  else{
          //    if($f_val==$s_val){
          //        $ids[] = $getCompanyvalue['id'];

          //    }
          //  }

          if ($v_id == 'true') {
            if ($f_val == $s_val) {
              //$ids[] = $getCompanyvalue['id'];
              array_push($new_val, 'on');
            } else {
              array_push($new_val, 'off');
            }
          } else {
            array_push($new_val, 'off');
          }
        }

        if (!in_array('off', $new_val)) {
          $ids[] = $getCompanyvalue['id'];
        }
      }
    }
    /* echo '<pre>';
print_r($ids);die;*/
    /*echo '<pre>';
print_r($ids);die;*/
    /* $data['getCompany'] = $this->db->query( "SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and id in (  '" . implode( "','", $ids ) . "' )")->result_array();
*/
    if ($_GET['values'] == '' || $_GET['fields'] == '') {
      $qy = "SELECT * FROM client WHERE user_id IN ( '" . implode("','", $us_id) . "' ) and crm_company_name !='' and  crm_company_name !='' ";
    } else {
      $qy = "SELECT * FROM client WHERE user_id IN ( '" . implode("','", $us_id) . "' ) and crm_company_name !='' and  crm_company_name !='' and id in (  '" . implode("','", $ids) . "' )";
    }





    if ($com_num != '' && $legal_form != '') {
      // $qy .="and crm_company_number = '$com_num' and crm_legal_form = '$legal_form'" ;
      $qy .= "and crm_company_name like '%$com_num%' and crm_legal_form = '$legal_form'";
    } elseif ($com_num != '' && $legal_form == '') {
      //$qy .="and crm_company_number = '$com_num' ";
      $qy .= "and crm_company_name like '%$com_num%' ";
    } elseif ($com_num == '' && $legal_form != '') {
      $qy .= "and crm_legal_form = '$legal_form' ";
    }


    $data['getCompany'] = $this->db->query($qy)->result_array();
    //$this->load->view('Deadline_manager/filter_rec',$data);

    $file = fopen('php://output', 'w');

    $header = array('Client Name', 'Client Type', 'VAT', 'Payroll', 'Accounts', 'Confirmation statement', 'Company Tax Return', 'Personal Tax Return', 'WorkPlace Pension - AE', 'CIS - Contractor', 'CIS - Sub Contractor', 'P11D', 'Bookkeeping', 'Management Accounts', 'Investigation Insurance', 'Registered Address', 'Tax Advice', 'Tax Investigation');
    fputcsv($file, $header);
    $new_array = array();
    foreach ($data['getCompany'] as $getCompanykey => $getCompanyvalue) {

      $getusername = $this->Common_mdl->select_record('user', 'id', $getCompanyvalue['user_id']);



      (isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnCst =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnCst = '';
      ($jsnCst != '') ? $cst = "checked='checked'" : $cst = "";

      (isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnAcc =  json_decode($getCompanyvalue['accounts'])->tab : $jsnAcc = '';
      ($jsnAcc != '') ? $acc = "checked='checked'" : $acc = "";

      (isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsntax =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsntax = '';
      ($jsntax != '') ? $tax = "checked='checked'" : $tax = "";

      (isset(json_decode($getCompanyvalue['personal_tax_return'])->tab) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnp_tax =  json_decode($getCompanyvalue['personal_tax_return'])->tab : $jsnp_tax = '';
      ($jsnp_tax != '') ? $p_tax = "checked='checked'" : $p_tax = "";

      (isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpay =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpay = '';
      ($jsnpay != '') ? $pay = "checked='checked'" : $pay = "";

      (isset(json_decode($getCompanyvalue['workplace'])->tab) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->tab : $jsnworkplace = '';
      ($jsnworkplace != '') ? $workplace = "checked='checked'" : $workplace = "";

      (isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = '';
      ($jsnvat != '') ? $vat = "checked='checked'" : $vat = "";

      (isset(json_decode($getCompanyvalue['cis'])->tab) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->tab : $jsncis = '';
      ($jsncis != '') ? $cis = "checked='checked'" : $cis = "";

      (isset(json_decode($getCompanyvalue['cissub'])->tab) && $getCompanyvalue['cissub'] != '') ? $jsncis_sub =  json_decode($getCompanyvalue['cissub'])->tab : $jsncis_sub = '';
      ($jsncis_sub != '') ? $cissub  = "checked='checked'" : $cissub     = "";

      (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = '';
      ($jsnp11d != '') ? $p11d   = "checked='checked'" : $p11d   = "";

      (isset(json_decode($getCompanyvalue['bookkeep'])->tab) && $getCompanyvalue['bookkeep'] != '') ? $jsnbk =  json_decode($getCompanyvalue['bookkeep'])->tab : $jsnbk = '';
      ($jsnbk != '') ? $bookkeep     = "checked='checked'" : $bookkeep   = "";

      (isset(json_decode($getCompanyvalue['management'])->tab) && $getCompanyvalue['management'] != '') ? $jsnmgnt =  json_decode($getCompanyvalue['management'])->tab : $jsnmgnt = '';
      ($jsnmgnt != '') ? $management     = "checked='checked'" : $management     = "";

      (isset(json_decode($getCompanyvalue['investgate'])->tab) && $getCompanyvalue['investgate'] != '') ? $jsninvest =  json_decode($getCompanyvalue['investgate'])->tab : $jsninvest = '';
      ($jsninvest != '') ? $investgate   = "checked='checked'" : $investgate     = "";

      (isset(json_decode($getCompanyvalue['registered'])->tab) && $getCompanyvalue['registered'] != '') ? $jsnreg =  json_decode($getCompanyvalue['registered'])->tab : $jsnreg = '';
      ($jsnreg != '') ? $registered  = "checked='checked'" : $registered     = "";

      (isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxad =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxad = '';
      ($jsntaxad != '') ? $taxadvice     = "checked='checked'" : $taxadvice  = "";

      (isset(json_decode($getCompanyvalue['taxinvest'])->tab) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->tab : $jsntaxinvest = '';
      ($jsntaxinvest != '') ? $taxinvest     = "checked='checked'" : $taxinvest  = "";


      $new_array['crm_name'] = $getusername['crm_name'];
      $new_array['crm_legal_form'] = $getCompanyvalue['crm_legal_form'];
      if ($vat != '') {
        $new_array['vat'] = "on";
      } else {
        $new_array['vat'] = "off";
      }
      if ($pay != '') {
        $new_array['payroll'] = "on";
      } else {
        $new_array['payroll'] = "off";
      }
      if ($acc != '') {
        $new_array['accounts'] = "on";
      } else {
        $new_array['accounts'] = "off";
      }
      if ($cst != '') {
        $new_array['conf_statement'] = "on";
      } else {
        $new_array['conf_statement'] = "off";
      }
      if ($tax != '') {
        $new_array['company_tax'] = "on";
      } else {
        $new_array['company_tax'] = "off";
      }
      if ($p_tax != '') {
        $new_array['personal_tax'] = "on";
      } else {
        $new_array['personal_tax'] = "off";
      }
      if ($workplace != '') {
        $new_array['workplace'] = "on";
      } else {
        $new_array['workplace'] = "off";
      }
      if ($cis != '') {
        $new_array['cis'] = "on";
      } else {
        $new_array['cis'] = "off";
      }
      if ($cissub != '') {
        $new_array['cissub'] = "on";
      } else {
        $new_array['cissub'] = "off";
      }
      if ($p11d != '') {
        $new_array['p11d'] = "on";
      } else {
        $new_array['p11d'] = "off";
      }
      if ($bookkeep != '') {
        $new_array['bookkeep'] = "on";
      } else {
        $new_array['bookkeep'] = "off";
      }
      if ($management != '') {
        $new_array['management'] = "on";
      } else {
        $new_array['management'] = "off";
      }
      if ($investgate != '') {
        $new_array['investgate'] = "on";
      } else {
        $new_array['investgate'] = "off";
      }
      if ($registered != '') {
        $new_array['registered'] = "on";
      } else {
        $new_array['registered'] = "off";
      }
      if ($taxadvice != '') {
        $new_array['taxadvice'] = "on";
      } else {
        $new_array['taxadvice'] = "off";
      }

      if ($taxinvest != '') {
        $new_array['taxinvest'] = "on";
      } else {
        $new_array['taxinvest'] = "off";
      }
      fputcsv($file, $new_array);
    }

    // foreach ($usersData as $key=>$line){
    //  fputcsv($file,$line);
    // }

    fclose($file);
    exit;
  }



  public function pdf_new_status()
  {


    $com_num = $_GET['com_num'];
    $legal_form = $_GET['legal_form'];

    $id = $this->session->userdata('admin_id');


    // $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = ".$id." and role = 4 and crm_name !=''")->row_array();

    //    $us_id =  explode(',', $sql['us_id']);

    if ($_SESSION['role'] == 4) {
      $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where  role = 4 and crm_name !='' and (firm_admin_id = " . $id . " or id=" . $id . ")  ")->row_array();

      $us_id =  explode(',', $sql['us_id']);
    } else {
      $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = " . $id . " and role = 4 and crm_name !=''")->row_array();

      $us_id =  explode(',', $sql['us_id']);
    }


    // $v_id = $_GET['values'];
    // $fields = $_GET['fields'];

    /*$v_id =  implode(',', $val);
echo '<pre>';
print_r($val);die;*/
    // if($v_id=='true')
    // {
    // $s_val = 'on';
    // }else{
    //   $s_val = 'off';
    // }

    $for_values = explode(',', $_GET['values']);
    $for_fields = explode(',', $_GET['fields']);


    $data['getCompany'] = $this->db->query("SELECT * FROM client WHERE user_id IN ( '" . implode("','", $us_id) . "' ) and crm_company_name !='' ")->result_array();



    $ids = array();
    foreach ($data['getCompany'] as $getCompanykey => $getCompanyvalue) {

      if ($getCompanyvalue['blocked_status'] == 0) {
        $conf_statement = (isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnCst =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnCst = 'off';

        $accounts = (isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnAcc =  json_decode($getCompanyvalue['accounts'])->tab : $jsnAcc = 'off';

        $company_tax_return = (isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsntax =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsntax = 'off';

        $personal_tax_return = (isset(json_decode($getCompanyvalue['personal_tax_return'])->tab) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnp_tax =  json_decode($getCompanyvalue['personal_tax_return'])->tab : $jsnp_tax = 'off';

        $payroll = (isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpay =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpay = 'off';

        $workplace = (isset(json_decode($getCompanyvalue['workplace'])->tab) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->tab : $jsnworkplace = 'off';

        $vat = (isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = 'off';

        $cis = (isset(json_decode($getCompanyvalue['cis'])->tab) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->tab : $jsncis = 'off';

        $cissub = (isset(json_decode($getCompanyvalue['cissub'])->tab) && $getCompanyvalue['cissub'] != '') ? $jsncis_sub =  json_decode($getCompanyvalue['cissub'])->tab : $jsncis_sub = 'off';

        $p11d = (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = 'off';

        $bookkeep = (isset(json_decode($getCompanyvalue['bookkeep'])->tab) && $getCompanyvalue['bookkeep'] != '') ? $jsnbk =  json_decode($getCompanyvalue['bookkeep'])->tab : $jsnbk = 'off';

        $management = (isset(json_decode($getCompanyvalue['management'])->tab) && $getCompanyvalue['management'] != '') ? $jsnmgnt =  json_decode($getCompanyvalue['management'])->tab : $jsnmgnt = 'off';

        $investgate = (isset(json_decode($getCompanyvalue['investgate'])->tab) && $getCompanyvalue['investgate'] != '') ? $jsninvest =  json_decode($getCompanyvalue['investgate'])->tab : $jsninvest = 'off';

        $registered = (isset(json_decode($getCompanyvalue['registered'])->tab) && $getCompanyvalue['registered'] != '') ? $jsnreg =  json_decode($getCompanyvalue['registered'])->tab : $jsnreg = 'off';

        $taxadvice = (isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxad =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxad = 'off';

        $taxinvest = (isset(json_decode($getCompanyvalue['taxinvest'])->tab) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->tab : $jsntaxinvest = 'off';
        $new_val = array();
        //echo count($for_fields);
        foreach (array_combine($for_fields, $for_values) as $fields => $v_id) {
          if ($v_id == 'true') {
            $s_val = 'on';
          } else {
            $s_val = 'off';
          }

          if ($fields == 'conf_statement') {
            $f_val = $conf_statement;
          } elseif ($fields == 'accounts') {
            $f_val = $accounts;
          } elseif ($fields == 'company_tax_return') {
            $f_val = $company_tax_return;
          } elseif ($fields == 'personal_tax_return') {
            $f_val = $personal_tax_return;
          } elseif ($fields == 'payroll') {
            $f_val = $payroll;
          } elseif ($fields == 'workplace') {
            $f_val = $workplace;
          } elseif ($fields == 'vat') {
            $f_val = $vat;
          } elseif ($fields == 'cis') {
            $f_val = $cis;
          } elseif ($fields == 'cissub') {
            $f_val = $cissub;
          } elseif ($fields == 'p11d') {
            $f_val = $p11d;
          } elseif ($fields == 'bookkeep') {
            $f_val = $bookkeep;
          } elseif ($fields == 'management') {
            $f_val = $management;
          } elseif ($fields == 'investgate') {
            $f_val = $investgate;
          } elseif ($fields == 'registered') {
            $f_val = $registered;
          } elseif ($fields == 'taxadvice') {
            $f_val = $taxadvice;
          } elseif ($fields == 'taxinvest') {
            $f_val = $taxinvest;
          } else {
            $f_val = '';
          }

          // if($v_id=='true'){
          //  if($f_val==$s_val)
          //  {
          //    $ids[] = $getCompanyvalue['id'];
          //  }}
          //  else{
          //    if($f_val==$s_val){
          //        $ids[] = $getCompanyvalue['id'];

          //    }
          //  }


          if ($v_id == 'true') {
            if ($f_val == $s_val) {
              //$ids[] = $getCompanyvalue['id'];
              array_push($new_val, 'on');
            } else {
              array_push($new_val, 'off');
            }
          } else {
            array_push($new_val, 'off');
          }
        }

        if (!in_array('off', $new_val)) {
          $ids[] = $getCompanyvalue['id'];
        }
      }
    }
    /* echo '<pre>';
print_r($ids);die;*/
    /*echo '<pre>';
print_r($ids);die;*/
    /* $data['getCompany'] = $this->db->query( "SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and id in (  '" . implode( "','", $ids ) . "' )")->result_array();
*/
    if ($_GET['values'] == '' || $_GET['fields'] == '') {
      $qy = "SELECT * FROM client WHERE user_id IN ( '" . implode("','", $us_id) . "' ) and crm_company_name !='' and  crm_company_name !='' ";
    } else {
      $qy = "SELECT * FROM client WHERE user_id IN ( '" . implode("','", $us_id) . "' ) and crm_company_name !='' and  crm_company_name !='' and id in (  '" . implode("','", $ids) . "' )";
    }





    if ($com_num != '' && $legal_form != '') {
      // $qy .="and crm_company_number = '$com_num' and crm_legal_form = '$legal_form'" ;
      $qy .= "and crm_company_name like '%$com_num%' and crm_legal_form = '$legal_form'";
    } elseif ($com_num != '' && $legal_form == '') {
      //$qy .="and crm_company_number = '$com_num' ";
      $qy .= "and crm_company_name like '%$com_num%' ";
    } elseif ($com_num == '' && $legal_form != '') {
      $qy .= "and crm_legal_form = '$legal_form' ";
    }


    $data['getCompany'] = $this->db->query($qy)->result_array();


    $data['title'] = "Users List";
    $data['column_setting'] = $this->Common_mdl->getallrecords('column_setting_new');
    $this->load->library('pdf');
    $customPaper = array(0, 0, 1000, 1000);
    $this->pdf->set_paper($customPaper);
    $this->pdf->load_view('Deadline_manager/pdf_view_new_status', $data);
    $this->pdf->render();
    $this->pdf->stream("User_list.pdf");
  }



  public function html_new_status()
  {


    $com_num = $_GET['com_num'];
    $legal_form = $_GET['legal_form'];

    $id = $this->session->userdata('admin_id');


    // $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = ".$id." and role = 4 and crm_name !=''")->row_array();

    //    $us_id =  explode(',', $sql['us_id']);

    if ($_SESSION['role'] == 4) {
      $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where  role = 4 and crm_name !='' and (firm_admin_id = " . $id . " or id=" . $id . ")  ")->row_array();

      $us_id =  explode(',', $sql['us_id']);
    } else {
      $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = " . $id . " and role = 4 and crm_name !=''")->row_array();

      $us_id =  explode(',', $sql['us_id']);
    }


    $v_id = $_GET['values'];
    $fields = $_GET['fields'];

    $for_values = explode(',', $_GET['values']);
    $for_fields = explode(',', $_GET['fields']);

    /*$v_id =  implode(',', $val);
echo '<pre>';
print_r($val);die;*/
    // if($v_id=='true')
    // {
    // $s_val = 'on';
    // }else{
    //   $s_val = 'off';
    // }


    $data['getCompany'] = $this->db->query("SELECT * FROM client WHERE user_id IN ( '" . implode("','", $us_id) . "' ) and crm_company_name !='' ")->result_array();



    $ids = array();
    foreach ($data['getCompany'] as $getCompanykey => $getCompanyvalue) {
      $conf_statement = (isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnCst =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnCst = 'off';

      $accounts = (isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnAcc =  json_decode($getCompanyvalue['accounts'])->tab : $jsnAcc = 'off';

      $company_tax_return = (isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsntax =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsntax = 'off';

      $personal_tax_return = (isset(json_decode($getCompanyvalue['personal_tax_return'])->tab) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnp_tax =  json_decode($getCompanyvalue['personal_tax_return'])->tab : $jsnp_tax = 'off';

      $payroll = (isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpay =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpay = 'off';

      $workplace = (isset(json_decode($getCompanyvalue['workplace'])->tab) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->tab : $jsnworkplace = 'off';

      $vat = (isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = 'off';

      $cis = (isset(json_decode($getCompanyvalue['cis'])->tab) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->tab : $jsncis = 'off';

      $cissub = (isset(json_decode($getCompanyvalue['cissub'])->tab) && $getCompanyvalue['cissub'] != '') ? $jsncis_sub =  json_decode($getCompanyvalue['cissub'])->tab : $jsncis_sub = 'off';

      $p11d = (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = 'off';

      $bookkeep = (isset(json_decode($getCompanyvalue['bookkeep'])->tab) && $getCompanyvalue['bookkeep'] != '') ? $jsnbk =  json_decode($getCompanyvalue['bookkeep'])->tab : $jsnbk = 'off';

      $management = (isset(json_decode($getCompanyvalue['management'])->tab) && $getCompanyvalue['management'] != '') ? $jsnmgnt =  json_decode($getCompanyvalue['management'])->tab : $jsnmgnt = 'off';

      $investgate = (isset(json_decode($getCompanyvalue['investgate'])->tab) && $getCompanyvalue['investgate'] != '') ? $jsninvest =  json_decode($getCompanyvalue['investgate'])->tab : $jsninvest = 'off';

      $registered = (isset(json_decode($getCompanyvalue['registered'])->tab) && $getCompanyvalue['registered'] != '') ? $jsnreg =  json_decode($getCompanyvalue['registered'])->tab : $jsnreg = 'off';

      $taxadvice = (isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxad =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxad = 'off';

      $taxinvest = (isset(json_decode($getCompanyvalue['taxinvest'])->tab) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->tab : $jsntaxinvest = 'off';
      $new_val = array();
      //echo count($for_fields);
      foreach (array_combine($for_fields, $for_values) as $fields => $v_id) {

        if ($v_id == 'true') {
          $s_val = 'on';
        } else {
          $s_val = 'off';
        }

        if ($fields == 'conf_statement') {
          $f_val = $conf_statement;
        } elseif ($fields == 'accounts') {
          $f_val = $accounts;
        } elseif ($fields == 'company_tax_return') {
          $f_val = $company_tax_return;
        } elseif ($fields == 'personal_tax_return') {
          $f_val = $personal_tax_return;
        } elseif ($fields == 'payroll') {
          $f_val = $payroll;
        } elseif ($fields == 'workplace') {
          $f_val = $workplace;
        } elseif ($fields == 'vat') {
          $f_val = $vat;
        } elseif ($fields == 'cis') {
          $f_val = $cis;
        } elseif ($fields == 'cissub') {
          $f_val = $cissub;
        } elseif ($fields == 'p11d') {
          $f_val = $p11d;
        } elseif ($fields == 'bookkeep') {
          $f_val = $bookkeep;
        } elseif ($fields == 'management') {
          $f_val = $management;
        } elseif ($fields == 'investgate') {
          $f_val = $investgate;
        } elseif ($fields == 'registered') {
          $f_val = $registered;
        } elseif ($fields == 'taxadvice') {
          $f_val = $taxadvice;
        } elseif ($fields == 'taxinvest') {
          $f_val = $taxinvest;
        } else {
          $f_val = '';
        }

        if ($v_id == 'true') {
          if ($f_val == $s_val) {
            //$ids[] = $getCompanyvalue['id'];
            array_push($new_val, 'on');
          } else {
            array_push($new_val, 'off');
          }
        } else {
          array_push($new_val, 'off');
        }
      }
      if (!in_array('off', $new_val)) {
        $ids[] = $getCompanyvalue['id'];
      }
      // if($v_id=='true'){
      //  if($f_val==$s_val)
      //  {
      //    $ids[] = $getCompanyvalue['id'];
      //  }}
      //  else{
      //    if($f_val==$s_val){
      //        $ids[] = $getCompanyvalue['id'];

      //    }
      //  }
    }
    /* echo '<pre>';
print_r($ids);die;*/
    /*echo '<pre>';
print_r($ids);die;*/
    /* $data['getCompany'] = $this->db->query( "SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and id in (  '" . implode( "','", $ids ) . "' )")->result_array();
*/
    if ($_GET['values'] == '' || $_GET['fields'] == '') {
      $qy = "SELECT * FROM client WHERE user_id IN ( '" . implode("','", $us_id) . "' ) and crm_company_name !='' and  crm_company_name !='' ";
    } else {
      $qy = "SELECT * FROM client WHERE user_id IN ( '" . implode("','", $us_id) . "' ) and crm_company_name !='' and  crm_company_name !='' and id in (  '" . implode("','", $ids) . "' )";
    }





    if ($com_num != '' && $legal_form != '') {
      // $qy .="and crm_company_number = '$com_num' and crm_legal_form = '$legal_form'" ;
      $qy .= "and crm_company_name like '%$com_num%' and crm_legal_form = '$legal_form'";
    } elseif ($com_num != '' && $legal_form == '') {
      //$qy .="and crm_company_number = '$com_num' ";
      $qy .= "and crm_company_name like '%$com_num%' ";
    } elseif ($com_num == '' && $legal_form != '') {
      $qy .= "and crm_legal_form = '$legal_form' ";
    }


    $data['getCompany'] = $this->db->query($qy)->result_array();


    $data['title'] = "Users List";
    $data['column_setting'] = $this->Common_mdl->getallrecords('column_setting_new');

    $this->load->view('Deadline_manager/deadline_print_page_status', $data);
  }
}
