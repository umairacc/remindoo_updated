<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Client_component extends CI_Controller
{
	public function __construct()
    {
		parent::__construct();

		$models = array('Common_mdl','CompanyHouse_Model','Client_model','Service_Reminder_Model','Security_model','FirmFields_model');

        $this->load->model( $models );

        $this->load->helper(['template_decode']);
    }
    public function required_information( $mode , $user_id ='' )
    {
    	$this->Security_model->chk_login(); 

    	$data = $this->Common_mdl->Get_Client_fields_ManagementSettings( $_SESSION['firm_id'] , 'required_information');
    	//print_r($data);

    	$this->load->view( 'clients/component/required_information_view' , $data );
    }
}    