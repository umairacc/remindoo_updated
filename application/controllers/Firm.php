<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(-1);
class Firm extends CI_Controller {
 //public $companieshouseAPIkey = "eHgKR3qG51_yHpL0-VSiM97QK-H6Z7fYiienSlzv";
 public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
 public $Firm_Status =  [
                          'all' => ['label'=>'All','count'=>0],
                          0 => ['label'=>'Non-Activeted','count'=>0],
                          1 => ['label'=>'Active','count'=>0],
                          2 => ['label'=>'Frozen','count'=>0],
                          3 => ['label'=>'Archive','count'=>0] //3 also check in firm_list page staticly
                        ];
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Common_mdl','CompanyHouse_Model','Loginchk_model','Task_invoice_model','Task_creating_model','Firm_setting_mdl'));

    }
     public function index()
    { 
      $this->Loginchk_model->superAdmin_LoginCheck();
            
      $data['firm_admin'] = $this->db->query("select u.* from user u inner join firm f on f.user_id = u.id and f.is_deleted !=1 where u.user_type = 'FA' ORDER BY id DESC")->result_array();

      if(count( $data['firm_admin'] ))
      {        
        $FirmAdmin_Ids = implode( ',' , array_column( $data['firm_admin'] ,'id' ) );

        $data['firm'] = $this->db->query("select * from firm where user_id  IN (".$FirmAdmin_Ids.") ORDER BY firm_id DESC")->result_array();
      }    
      $this->Update_Firm_StatusCount_var();      
      $data['Firms_Status'] = $this->Firm_Status;

      $this->load->view('Firm/firm_list' , $data);
    }
    public function add_firm($user_id=false)
    {
        $this->Loginchk_model->superAdmin_LoginCheck();
       
          $data['firm'] = current ( $this->Common_mdl->GetAllWithWhere('firm','user_id',$user_id) );  
       
          $data['user'] = current ( $this->Common_mdl->GetAllWithWhere('user','id',$user_id) ); 

          $data['contactRec'] = $data['legalform'] = array();
          if($user_id!='')
          {
            
            $data['contactRec']= $this->db->query("SELECT * FROM  `firm_contacts` WHERE user_id =  '".$user_id."' ORDER BY make_primary DESC ")->result_array();
            $data['legalform'] = $this->db->query("select * from client_legalform  where user_id=".$user_id)->row_array();
            $data['subscribed_plan'] = $this->Firm_setting_mdl->Get_FirmSubscribed_Plan( $data['firm']['firm_id'] );

          }
          
          $data['Firms_Status'] = $this->Firm_Status;
          $data['countries']    = $this->db->query("SELECT * FROM `countries` order by name asc")->result_array();
          $data['plans']        = $this->Common_mdl->GetAllWithWhere('subscription_plan','status','1');
          $data['user_ids']     = '';
          if($user_id!='')
          {
              $data['user_ids'] = $user_id;
          }
        
        $this->load->view('Firm/addnewFirm',$data);    
    }
   public function ChangeFirm_Status()
  {
    $Returndata = []; 

    if( $_POST['ids'] !='' && $_POST['status'] !='' )
    {
      $Ids = implode(',' , $_POST['ids']);

      if( $_POST['status'] == 'unarchive')
      {
         $this->db->query("update firm set status=@s:=status,status=old_status,old_status=@s where firm_id IN(".$Ids.") and status=3");
      }
      else
      {
        $IsArchive = $IsArchiveCon ='';

        if( $_POST['status'] == 3 )
        {
          $IsArchiveCon = "and status!=3";
        }
        $this->db->query("update firm set old_status=status,status=".$_POST['status']." where firm_id IN (".$Ids.") ".$IsArchiveCon." "); 
      }
          
      $Returndata['result'] = $this->db->affected_rows();      
      $this->Update_Firm_StatusCount_var();
      $Returndata['status'] = $this->Firm_Status;

      echo json_encode( $Returndata );
    }
  }
  public function Delete_Firms()
  {
    $res['result'] = 0;
    if( !empty($_POST['ids']) )
    {
      $res['result'] = $this->Firm_setting_mdl->Delete_Firms( $_POST['ids'] );
    }
    echo json_encode($res);
  }
  public function Update_Firm_StatusCount_var()
  {
    $Firm_Status = $this->Firm_Status; 

    $status_count = $this->db->query("select count(*) as num , status from firm where is_deleted!=1  GROUP by status")->result_array();  
      $All_count = 0;    
      foreach ( $status_count as $key => $value )
      {
        $Firm_Status[ $value['status'] ]['count'] = $value['num'];
        $All_count += $value['num'];
      }

      $Firm_Status['all']['count'] = $All_count;

      $this->Firm_Status = $Firm_Status;
  }

  public function login_us_firm($id,$admin_id)
  {
    session_destroy(); 
    $query=$this->db->query("select * from user where id=".$id."")->row_array(); 
    $data['username']=$query['username'];
    $data['password']=$query['confirm_password'];
    $_SESSION['last_login_id']=$admin_id;
    $this->load->view('Firm/firm_login',$data);
  }
  public function Update_firm_column_settings_order()
  {
    $firm=$_SESSION['firm_id'];
    $i=1;
    $table = $_POST['table_name'];
    // foreach ($_POST['reorder'] as $column) 
    // {

    //   $this->db->update('firm_column_settings',["order_firm".$firm => $i],"table_name='".$table."' and name='".trim( $column )."'");
    //   $i++;
    //   echo $this->db->affected_rows();
    // }
    echo 1;
  }
  public function Update_firm_column_settings_visibility()
  {
    $firm=$_SESSION['firm_id'];
    $table = $_POST['table_name'];
    $column = $_POST['column'];
    $state = $_POST['state'];
    $this->db->update('firm_column_settings',["visible_firm".$firm => $state],"table_name='".$table."' and name='".trim( $column )."'");
    echo $this->db->affected_rows();
  }
    public function check_company_numberExist()
    {
      
      $company_number = $_POST['company_number'];

      if( isset( $_POST['id'] ) )
        {
            $sql=$this->db->query("select firm_id from firm where crm_company_number='".$company_number."' and user_id!=".$_POST['id'].' and is_deleted!=1')->num_rows();
        }
        else
        {
          $sql = $this->db->query('SELECT firm_id FROM firm WHERE crm_company_number='.$company_number.' AND is_deleted!=1')->num_rows();
        }
        echo ( $sql ? 'false' : 'true');
        
    }
    
     public function check_username()
      {
        $username = $this->input->post('user_name');
        
        $Deleted_firms        = $this->Common_mdl->Get_Deleted_FirmsIds();
        $Remove_Deleted_Firms = (!empty( $Deleted_firms ) ? ' AND firm_id NOT IN('.$Deleted_firms.') ':''); 

        if( isset($_POST['id']) )
        {
          $sql = $this->db->query("select id from user where username='".$username."' and id!=".$_POST['id'].$Remove_Deleted_Firms)->num_rows();
        }
        else
        {
          $sql = $this->db->query("SELECT id FROM user WHERE username='".$username."'".$Remove_Deleted_Firms)->num_rows();
        }
        echo ( $sql ? 'false' : 'true');
      }

    public function Check_Firm_Mail()
    {
      $MailId = $this->input->post('firm_mailid');
      
      $Deleted_firms        = $this->Common_mdl->Get_Deleted_FirmsIds();
      $Remove_Deleted_Firms = (!empty( $Deleted_firms ) ? ' AND firm_id NOT IN('.$Deleted_firms.') ':'');

      if(isset( $_POST['id']))
      {
        $sql = $this->db->query("select * from firm where firm_mailid='".$MailId."' and firm_id!=".$_POST['id'].$Remove_Deleted_Firms)->num_rows();
      }
      else
      {
        $sql = $this->db->query("select * from firm where firm_mailid='".$MailId."'".$Remove_Deleted_Firms)->num_rows();
      }
      echo ( $sql ? 'false' : 'true');
    }

    public function insert_firm()
    {           
      $this->Loginchk_model->superAdmin_LoginCheck();        
      
         if($_POST['company_name']!='')
         {
            $var=rand(1,10);            
            $image_one = (isset($_POST['image_name'])&&($_POST['image_name']!=''))? $_POST['image_name'] : '825898.jpg';
            $image= (isset($_FILES['profile_image']['name'])&&($_FILES['profile_image']['name']!=''))? $this->Common_mdl->do_upload($_FILES['profile_image'], 'uploads') : $image_one ;

            $data['crm_name']= $_POST['company_name'];
            $data['username']= $_POST['user_name'];
            
            $data['password']=(isset($_POST['password'])&&($_POST['password']!=''))? md5($_POST['password']) : md5($var);
            $data['confirm_password']=(isset($_POST['password'])&&($_POST['password']!=''))? $_POST['password'] : $var;
            $data['status']=1;
            $data['company_roles']=(isset($_POST['company_house'])&&($_POST['company_house']!=''))? $_POST['company_house'] : 0;
            $data['CreatedTime']=time();
            $data['client_id']=(isset($_POST['client_id'])&&($_POST['client_id']!=''))? $_POST['client_id'] : rand(1,5);
            $data['crm_profile_pic']=$image;
            $data['autosave_status']=0;
            $data['user_type']='FA';
           /* $data['crm_packages']=(isset($_POST['packages'])&&($_POST['packages']!=''))? $_POST['packages'] : '';
            $data['crm_gender']=(isset($_POST['gender'])&&($_POST['gender']!=''))? $_POST['gender'] : '';
            */          
            if( isset($_POST['user_id']) && ($_POST['user_id']=='0'|| $_POST['user_id']=='') ) 
            {
              //after firm created we will update these data. 
              $data['role']=2; 
              $data['firm']=1; 
              $this->db->insert( 'user', $data );
              $_POST['user_id'] = $in = $this->db->insert_id();
            }
            else
            {
              $this->Common_mdl->update('user',$data,'id',$_POST['user_id']);
              $in = $_POST['user_id'];
            }

                $data1['user_id']=$_POST['user_id'];
                
              
               $data1['trial_end_date']         = $this->input->post('trial_end_date');
               $data1['no_of_clients_allowed']  = $this->input->post('per_client_limit');
               $data1['no_of_users_allowed']    = $this->input->post('per_user_limit');
                
                //requried information
               $data1['crm_company_name']=(isset($_POST['company_name'])&&($_POST['company_name']!=''))? $_POST['company_name']:'';
               $data1['crm_legal_form']=(isset($_POST['legal_form'])&&($_POST['legal_form']!=''))? $_POST['legal_form']:'';
               $data1['crm_allocation_holder']=(isset($_POST['allocation_holders'])&&($_POST['allocation_holders']!=''))? $_POST['allocation_holders']:'';
                //basic deatils
               $data1['crm_company_number']=(isset($_POST['company_number'])&&($_POST['company_number']!=''))? $_POST['company_number']:'';
               $data1['crm_company_url']=(isset($_POST['company_url'])&&($_POST['company_url']!=''))? $_POST['company_url']:'';
               $data1['crm_officers_url']=(isset($_POST['officers_url'])&&($_POST['officers_url']!=''))? $_POST['officers_url']:'';
               $data1['crm_company_name1']=(isset($_POST['company_name'])&&($_POST['company_name']!=''))? $_POST['company_name']:'';
               $data1['crm_register_address']=(isset($_POST['register_address'])&&($_POST['register_address']!=''))? $_POST['register_address']:'';
               $data1['crm_company_status']=(isset($_POST['company_status'])&&($_POST['company_status']!=''))? $_POST['company_status']:'';
               $data1['crm_company_type']=(isset($_POST['company_type'])&&($_POST['company_type']!=''))? $_POST['company_type']:'';
               $data1['crm_company_sic']=(isset($_POST['company_sic'])&&($_POST['company_sic']!=''))? $_POST['company_sic']:'';
               $data1['crm_sic_codes']=(isset($_POST['sic_codes'])&&($_POST['sic_codes']!=''))? $_POST['sic_codes']:'';
               $data1['crm_company_utr']=(isset($_POST['company_utr'])&&($_POST['company_utr']!=''))? $_POST['company_utr']:'';
               $data1['crm_incorporation_date']=(isset($_POST['date_of_creation'])&&($_POST['date_of_creation']!=''))? Change_Date_Format ( $_POST['date_of_creation'],'d-m-Y','Y-m-d'):'';
               $data1['crm_registered_in']=(isset($_POST['registered_in'])&&($_POST['registered_in']!=''))? $_POST['registered_in']:'';
               $data1['crm_address_line_one']=(isset($_POST['address_line_one'])&&($_POST['address_line_one']!=''))? $_POST['address_line_one']:'';
               $data1['crm_address_line_two']=(isset($_POST['address_line_two'])&&($_POST['address_line_two']!=''))? $_POST['address_line_two']:'';
               $data1['crm_address_line_three']=(isset($_POST['address_line_three'])&&($_POST['address_line_three']!=''))? $_POST['address_line_three']:'';
               $data1['crm_town_city']=(isset($_POST['crm_town_city'])&&($_POST['crm_town_city']!=''))? $_POST['crm_town_city']:'';
               $data1['crm_post_code']=(isset($_POST['crm_post_code'])&&($_POST['crm_post_code']!=''))? $_POST['crm_post_code']:'';
               $data1['crm_hashtag']=(isset($_POST['client_hashtag'])&&($_POST['client_hashtag']!=''))? $_POST['client_hashtag']:'';
               $data1['crm_accounts_office_reference']=(isset($_POST['accounts_office_reference'])&&($_POST['accounts_office_reference']!=''))? $_POST['accounts_office_reference']:'';
               $data1['crm_vat_number']=(isset($_POST['vat_number'])&&($_POST['vat_number']!=''))? $_POST['vat_number']:'';
              // client information
               $data1['crm_letter_sign']=(isset($_POST['engagement_letter'])&&($_POST['engagement_letter']!=''))? $_POST['engagement_letter'] : '';
               $data1['crm_business_website']=(isset($_POST['business_website'])&&($_POST['business_website']!=''))? $_POST['business_website'] : '';
               $data1['crm_accounting_system']=(isset($_POST['accounting_system_inuse'])&&($_POST['accounting_system_inuse']!=''))? $_POST['accounting_system_inuse'] : '';
               $data1['crm_paye_ref_number']=(isset($_POST['paye_ref_number'])&&($_POST['paye_ref_number']!=''))? $_POST['paye_ref_number'] : '';
               $data1['crm_assign_client_id_verified']=(isset($_POST['client_id_verified'])&&($_POST['client_id_verified']!=''))? $_POST['client_id_verified'] : '';
               $data1['crm_assign_type_of_id']=(isset($_POST['type_of_id'])&&($_POST['type_of_id']!=''))? implode(',',$_POST['type_of_id']) : '';
          /** 29-08-2018 proof attch **/
                $proof_attach_file='';
                if(!empty($_FILES['proof_attach_file']['name'])){
                      /** for multiple **/
                      $files = $_FILES;
                      $cpt = count($_FILES['proof_attach_file']['name']);
                      for($i=0; $i<$cpt; $i++)
                      {           
                      /** end of multiple **/
                          $_FILES['proof_attach_file']['name']= $files['proof_attach_file']['name'][$i];
                          $_FILES['proof_attach_file']['type']= $files['proof_attach_file']['type'][$i];
                          $_FILES['proof_attach_file']['tmp_name']= $files['proof_attach_file']['tmp_name'][$i];
                          $_FILES['proof_attach_file']['error']= $files['proof_attach_file']['error'][$i];
                          $_FILES['proof_attach_file']['size']= $files['proof_attach_file']['size'][$i]; 
                          $uploadPath = 'uploads/client_proof/';
                          $config['upload_path'] = $uploadPath;
                          $config['allowed_types'] = '*';    
                          $config['max_size']='0';      
                          $this->load->library('upload', $config);
                          $this->upload->initialize($config);
                          if($this->upload->do_upload('proof_attach_file'))
                          {
                                 $fileData = $this->upload->data();
                                 $proof_attach_file=$proof_attach_file.",".$fileData['file_name'];
                          }
                      }
                }
              $ex_img=(isset($_POST['already_upload_img']) && $_POST['already_upload_img']!='')?$_POST['already_upload_img']:'';
              //echo implode(',',$ex_img);
               if(!empty($ex_img))
               {
                $proof_attach_file=$proof_attach_file.",".implode(',', $ex_img);
               }
              if($proof_attach_file!='')
              {
                $data1['proof_attach_file']=implode(',', array_filter(explode(',', $proof_attach_file)));
              }
              else
              {
                $data1['proof_attach_file']='';
              }
              $data1['proof_attach_file']=$proof_attach_file;
              $data1['crm_assign_other_custom']=(isset($_POST['assign_other_custom'])&&($_POST['assign_other_custom']!=''))? $_POST['assign_other_custom'] : '';
              $data1['crm_assign_proof_of_address']=(isset($_POST['proof_of_address'])&&($_POST['proof_of_address']!=''))? $_POST['proof_of_address'] : '';
              $data1['crm_assign_meeting_client']=(isset($_POST['meeting_client'])&&($_POST['meeting_client']!=''))? $_POST['meeting_client'] : '';
              $data1['crm_assign_source']=(isset($_POST['source'])&&($_POST['source']!=''))? $_POST['source'] : '';
              $data1['crm_refered_by']=(isset($_POST['refered_by'])&&($_POST['refered_by']!=''))? $_POST['refered_by'] : '';
              $data1['crm_assign_relationship_client']=(isset($_POST['relationship_client'])&&($_POST['relationship_client']!=''))? $_POST['relationship_client'] : '';
              $data1['crm_assign_notes']=(isset($_POST['assign_notes'])&&($_POST['assign_notes']!=''))? $_POST['assign_notes'] : '';
              // other

              $data1['crm_previous_accountant']=(!empty($_POST['previous_account']) )? $_POST['previous_account'] : '';
              $data1['crm_other_name_of_firm']=(!empty($_POST['previous_account']) && !empty($_POST['name_of_firm']) )? $_POST['name_of_firm'] : '';
              $data1['crm_other_address']=(!empty($_POST['previous_account']) && !empty($_POST['other_address']))? $_POST['other_address'] : '';
              $data1['crm_other_contact_no']=(!empty($_POST['previous_account']) && !empty($_POST['pn_no_rec']))? $_POST['cun_code'].'-'.$_POST['pn_no_rec'] : '';
              $data1['crm_email']=(!empty($_POST['previous_account']) && !empty($_POST['emailid']))? $_POST['emailid'] : '';
              $data1['crm_other_chase_for_info']=(!empty($_POST['previous_account']) && !empty($_POST['chase_for_info']))? $_POST['chase_for_info'] : '';
              $data1['crm_other_notes']=(!empty($_POST['previous_account']) && !empty($_POST['other_notes']))? $_POST['other_notes'] : '';

              $data1['crm_other_internal_notes']=(isset($_POST['other_internal_notes'])&&($_POST['other_internal_notes']!=''))? $_POST['other_internal_notes'] : '';
              $data1['crm_other_invite_use']=(isset($_POST['invite_use'])&&($_POST['invite_use']!=''))? $_POST['invite_use'] : '';
              $data1['crm_other_crm']=(isset($_POST['other_crm'])&&($_POST['other_crm']!=''))? $_POST['other_crm'] : '';
              $data1['crm_other_proposal']=(isset($_POST['other_proposal'])&&($_POST['other_proposal']!=''))? $_POST['other_proposal'] : '';
              $data1['crm_other_task']=(isset($_POST['other_task'])&&($_POST['other_task']!=''))? $_POST['other_task'] : '';
              $data1['crm_other_send_invit_link']=(isset($_POST['send_invit_link'])&&($_POST['send_invit_link']!=''))? $_POST['send_invit_link'] : '';
              $data1['crm_other_username']=(isset($_POST['user_name'])&&($_POST['user_name']!=''))? $_POST['user_name'] : '';
              $data1['crm_other_password']=(isset($_POST['password'])&&($_POST['password']!=''))? $_POST['password'] : '';
              $data1['crm_other_any_notes']=(isset($_POST['other_any_notes'])&&($_POST['other_any_notes']!=''))? $_POST['other_any_notes'] : '';
              $data1['select_responsible_type']=(isset($_POST['select_responsible_type'])&&($_POST['select_responsible_type']!=''))? $_POST['select_responsible_type'] : '';
              $data1['firm_mailid']=(isset($_POST['firm_mailid'])&&($_POST['firm_mailid']!=''))? $_POST['firm_mailid'] : '';
              $data1['firm_contact_no']=(isset($_POST['firm_contact_no'])&&($_POST['firm_contact_no']!=''))? $_POST['firm_contact_no'] : '';

              $data1['status'] = (isset($_POST['company_status'])&&($_POST['company_status']!=''))? $_POST['company_status']:'';
             
           
              // documents tab
              $data1['crm_documents']=(isset($_FILES['document']['name'])&&($_FILES['document']['name']!=''))? $this->Common_mdl->do_upload($_FILES['document'], 'uploads') : '';
              $data1['created_date']=time();
              $data1['autosave_status']=0;
              //busness tab that only on if u choose selfassesment
              $datalegal['user_id']=$_POST['user_id'];
              $datalegal['company_no'] = isset($_POST['fa_cmyno']) ? $_POST['fa_cmyno'] : "";  
              $datalegal['incorporation_date'] = isset($_POST['fa_incordate']) ? $_POST['fa_incordate'] : "";  
              $datalegal['regist_address'] = isset($_POST['fa_registadres']) ? $_POST['fa_registadres'] : "";  
              $datalegal['turnover'] = isset($_POST['fa_turnover']) ? $_POST['fa_turnover'] : "";  
              $datalegal['date_of_trading'] = isset($_POST['fa_dateoftrading']) ? $_POST['fa_dateoftrading'] : ""; 
              $datalegal['sic_code'] = isset($_POST['fa_siccode']) ? $_POST['fa_siccode'] : "";  
              $datalegal['nuture_of_business'] = isset($_POST['fa_nutureofbus']) ? $_POST['fa_nutureofbus'] : "";  
              $datalegal['company_utr'] = isset($_POST['fa_cmyutr']) ? $_POST['fa_cmyutr'] : "";  
              $datalegal['company_house_auth_code'] = isset($_POST['fa_cmyhouse']) ? $_POST['fa_cmyhouse'] : "";  
              $datalegal['trading_as'] = isset($_POST['bus_tradingas']) ? $_POST['bus_tradingas'] : "";  
              $datalegal['commenced_trading'] = isset($_POST['bus_commencedtrading']) ? $_POST['bus_commencedtrading'] : "";  
              $datalegal['register_sa'] = isset($_POST['bus_regist']) ? $_POST['bus_regist'] : "";  
              $datalegal['bus_turnover'] = isset($_POST['bus_turnover']) ? $_POST['bus_turnover'] : "";  
              $datalegal['bus_nuture_of_business'] = isset($_POST['bus_nutureofbus']) ? $_POST['bus_nutureofbus'] : "";  
              
              $check_exist = $this->db->query("select * from client_legalform where user_id=".$_POST['user_id'])->row_array();

              if(count($check_exist))
              {
                $this->db->update( 'client_legalform', $datalegal ,"user_id = ".$_POST['user_id']);
              }
              else
              {
                $this->db->insert( 'client_legalform', $datalegal );
              }

              //busness tab that only on if u choose selfassesment


              $services = array_fill_keys( range(1,16) , 0 );
              if( !empty( $_POST['service'] ) )
              {
                $data1['services'] = array_replace( $services , $_POST['service'] );
                $data1['services'] = json_encode( $data1['services'] );
              }

              $c_id = $_POST['user_id'];
              $sql_c = $this->db->query("select * from firm where user_id = $c_id")->row_array();



              if(!empty( $sql_c ))
              {
                $updates_client =  $this->Common_mdl->update('firm',$data1,'user_id',$_POST['user_id']);

                if($sql_c['is_welcome_mail_send']==0)
                {
                  $this->Firm_setting_mdl->send_welcome_mail($sql_c['firm_id']);
                }
                $this->Firm_setting_mdl->Add_SelectedPlan( $sql_c['firm_id'] , $_POST['subscription_plan'] );
                $this->Firm_setting_mdl->SetUp_BasicSettings( $sql_c['firm_id'] );
              }
              else
              {
                  $updates_client =  $this->db->insert( 'firm', $data1 );
          
                  $id=$this->db->insert_id();


                  $data_fid=array('firm_id'=>$id);

                  $this->Common_mdl->update('user',$data_fid,'id',$_POST['user_id']);

                  $this->Firm_setting_mdl->Add_SelectedPlan( $id , $_POST['subscription_plan'] );

                  $this->Firm_setting_mdl->send_welcome_mail( $id );
                  
                  $this->Firm_setting_mdl->SetUp_BasicSettings($id);

                  
              }
              echo json_encode(['user_id'=>$in]);//must return user id.
        }
                 
  }
  public function AddFirmContacts()
  {
   if(isset($_POST['title']))
   {
  
   foreach ($_POST['title'] as $key => $details)
   {
    $data = array();
      

      if(isset($_POST['title']) &&  !empty(array_filter($_POST['title']))) {  $data['title'] =  $_POST['title'][$key];}else{ $data['title'] = ''; }
      if(isset($_POST['first_name']) &&  !empty(array_filter($_POST['first_name']))) {  $data['first_name'] =  $_POST['first_name'][$key];}else{ $data['first_name'] = ''; }
      if(isset($_POST['middle_name']) &&  !empty(array_filter($_POST['middle_name']))) {  $data['middle_name'] =  $_POST['middle_name'][$key];}else{ $data['middle_name'] = ''; }
     // echo json_encode(array_filter($_POST['last_name']))."//l name";
      if(isset($_POST['last_name']) &&  !empty(array_filter($_POST['last_name']))) {  $data['last_name'] =  $_POST['last_name'][$key];}else{ $data['last_name'] = ''; }
      if(isset($_POST['surname']) &&  !empty(array_filter($_POST['surname']))) {  $data['surname'] =  $_POST['surname'][$key];}else{ $data['surname'] = ''; }
      if(isset($_POST['preferred_name']) && !empty(array_filter($_POST['preferred_name']))) {  $data['preferred_name'] =  $_POST['preferred_name'][$key];}else{ $data['preferred_name'] = ''; }
      if(isset($_POST['mobile']) &&  !empty(array_filter($_POST['mobile']))) {  $data['mobile'] =  $_POST['mobile'][$key];}else{ $data['mobile'] = ''; }
      if(isset($_POST['main_email']) &&  !empty(array_filter($_POST['main_email']))) {  $data['main_email'] =  $_POST['main_email'][$key];}else{ $data['main_email'] = ''; }

      if(isset($_POST['work_email'][$key])) {  $data['work_email'] =  json_encode(array_filter($_POST['work_email'][$key]));}else{ $data['work_email'] = ''; }

      if(isset($_POST['nationality'])) {  $data['nationality'] =  $_POST['nationality'][$key];}else{ $data['nationality'] = ''; }
      if(isset($_POST['psc'])) {  $data['psc'] =  $_POST['psc'][$key];}else{ $data['psc'] = ''; }
      if(isset($_POST['shareholder'])) {  $data['shareholder'] =  $_POST['shareholder'][$key];}else{ $data['shareholder'] = ''; }
      if(isset($_POST['ni_number'])) {  $data['ni_number'] =  $_POST['ni_number'][$key];}else{ $data['ni_number'] = ''; }
      if(isset($_POST['content_type'])) {  $data['content_type'] =  $_POST['content_type'][$key];}else{ $data['content_type'] = ''; }
      if(isset($_POST['address_line1'])) {  $data['address_line1'] =  $_POST['address_line1'][$key];}else{ $data['address_line1'] = ''; }
      if(isset($_POST['address_line2'])) {  $data['address_line2'] =  $_POST['address_line2'][$key];}else{ $data['address_line2'] = ''; }
      if(isset($_POST['town_city'])) {  $data['town_city'] =  $_POST['town_city'][$key];}else{ $data['town_city'] = ''; }
      if(isset($_POST['post_code'])) {  $data['post_code'] =  $_POST['post_code'][$key];}else{ $data['post_code'] = ''; }
      
      if(!empty($_POST['landline'][$key]))
        {
          $_POST['landline'][$key] = array_filter($_POST['landline'][$key]);

          $data['landline'] =  json_encode($_POST['landline'][$key]);
        }
        else
        {
          $data['landline'] = ''; 
        }
        if(!empty($_POST['pre_landline'][$key]) && !empty($_POST['landline'][$key]))
        {
          $_POST['pre_landline'][$key] = array_intersect_key($_POST['pre_landline'][$key], $_POST['landline'][$key]);
          $data['pre_landline'] =  json_encode($_POST['pre_landline'][$key]);
        }
        else
        {
          $data['pre_landline'] = '';
        }  

      if(isset($_POST['date_of_birth'])) {  $data['date_of_birth'] =  $_POST['date_of_birth'][$key];}else{ $data['date_of_birth'] = ''; }
      if(isset($_POST['nature_of_control'])) {  $data['nature_of_control'] =  $_POST['nature_of_control'][$key];}else{ $data['nature_of_control'] = ''; }
      if(isset($_POST['utr_number'])) {  $data['utr_number'] =  $_POST['utr_number'][$key];}else{ $data['utr_number'] = ''; }
      if(isset($_POST['marital_status'])) {  $data['marital_status'] =  $_POST['marital_status'][$key];}else{ $data['marital_status'] = ''; }  if(isset($_POST['contact_type'])) {  $data['contact_type'] =  $_POST['contact_type'][$key];}else{ $data['contact_type'] = ''; }
      //ddddddddddddd
      /*if(isset($_POST['make_primary'])) {  $data['make_primary'] =  $_POST['make_primary'][$key];}else{ $data['make_primary'] = '0'; }*/
      $mk = (isset($_POST['make_primary']))?$_POST['make_primary']:'0';
     // $mk = $_POST['make_primary'] - 1;
    //  $re_key=$key+1;
     
      $make_primary_loop=(isset($_POST['make_primary_loop']))?$_POST['make_primary_loop'][$key]:'0';
    
      $data['make_primary'] = '0';
      if($mk == $make_primary_loop)
      {
        $data['make_primary'] = '1';
      }
      

      if(isset($_POST['user_id'])) {  $data['user_id'] =  $_POST['user_id'];}else{ $data['user_id'] = ''; }

      if(isset($_POST['occupation'])) {  $data['occupation'] =  $_POST['occupation'][$key];}else{ $data['occupation'] = ''; }

      if(isset($_POST['appointed_on'])) {  $data['appointed_on'] =  $_POST['appointed_on'][$key];}else{ $data['appointed_on'] = ''; }
      
      if(isset($_POST['country_of_residence'])) {  $data['country_of_residence'] =  $_POST['country_of_residence'][$key];}else{ $data['country_of_residence'] = ''; }

      if(isset($_POST['other_custom'])) {  $data['other_custom'] =  $_POST['other_custom'][$key];}else{ $data['other_custom'] = ''; }



       if( $_POST['firm_contact_table_id'][$key]!='')
       {
          $this->db->update('firm_contacts',$data,"id =".$_POST['firm_contact_table_id'][$key]);
       }
       else
       {
        $this->Common_mdl->insert('firm_contacts',$data);
       }
       
   
   }
   


}
   //exit;
           


  }
  public function edit_firm($user_id)
  {

        $this->Loginchk_model->superAdmin_LoginCheck();
       
          $data['firm'] = $this->db->query("select * from firm where user_id=".$user_id)->row_array();  
       
          $data['user'] = $this->db->query("select * from user where id=".$user_id)->row_array();  
          
          $data['contactRec'] = $this->db->query("SELECT * FROM  `firm_contacts` WHERE user_id =  '".$user_id."' ORDER BY make_primary DESC ")->result_array();
          $data['legalform'] = $this->db->query("select * from client_legalform  where user_id=".$user_id)->row_array();  
          
          $data['Firms_Status'] = $this->Firm_Status;
     
          $data['user_id'] = $user_id;
        //$this->load->view('Firm/editFirm',$data);    
        $this->load->view('Firm/addnewFirm',$data);    

  }
  public function view_firm($user_id)
  {

        $this->Loginchk_model->superAdmin_LoginCheck();
       
          $data['firm']             = $this->db->query("select * from firm where user_id=".$user_id)->row_array();  
          $data['user']             = $this->db->query("select * from user where id=".$user_id)->row_array();  
          $data['subscribed_plan']  = $this->Firm_setting_mdl->Get_FirmSubscribed_Plan( $data['firm']['firm_id'] );
          $data['contactRec']       = $this->db->query("SELECT * FROM  `firm_contacts` WHERE user_id =  '".$user_id."' ORDER BY make_primary DESC ")->result_array();
          $data['legalform']        = $this->db->query("select * from client_legalform  where user_id=".$user_id)->row_array();
          $data['Firms_Status']     = $this->Firm_Status;
          $data['user_id']          = $user_id;
          $data['country_sort_name']= '';
          if( !empty( $data['firm']['country_code']  ))
          {
            $data['country_sort_name'] = $this->Common_mdl->get_field_value('countries','sortname','phonecode',$data['firm']['country_code']);
          }
          
        $this->load->view('Firm/firm_information',$data);    
  }
     public function mailsettings()
    {   
        $this->load->library('email');
        $config['wrapchars'] = 76; // Character count to wrap at.
        $config['mailtype'] = 'html'; // text or html Type of mail. If you send HTML email you must send it as a complete web page. Make sure you don't have any relative links or relative image paths otherwise they will not work.
        //$config['charset'] = 'utf-8'; // Character set (utf-8, iso-8859-1, etc.).
        $this->email->initialize($config);  
    }

    public function user()
    {
        $this->Loginchk_model->superAdmin_LoginCheck();        
        $data['getallUser']=$this->db->query("SELECT * FROM user where firm='1' AND role='2' AND autosave_status!='1' and  firm_admin_id='".$_SESSION['id']."' order by id DESC")->result_array();
        $data['column_setting']=$this->Common_mdl->getallrecords('column_setting_new');
        $data['client'] =$this->db->query('select * from column_setting_new where id=1')->row_array();
        $data['feedback'] =$this->db->query("select id,username,crm_email_id from user where username!='' and username!='0' and  crm_email_id!='' and crm_email_id!='0' "  )->result();
        $data["email_temp"]=$this->db->query("select * from proposal_proposaltemplate where user_id=".$_SESSION['id']." and type='add_email_template'")->result_array(); 
        $this->load->view('users/user_list',$data);
    }



    function Firm_Permission($id){
   //   echo $id;
     // echo $_SESSION['role_name'];
    //  echo $_SESSION['firm_admin_id'];

      $query1=$this->db->query("INSERT INTO `roles_section`(`parent_id`, `role`, `firm_id`) VALUES ('0','Admin','".$id."')");

      $role_id=$this->db->insert_id();

      $query=$this->db->query("INSERT INTO `Role`( `id`,`firm_id`, `role`) VALUES ('".$role_id."','".$id."','Admin')");

     $data=array('role'=>$role_id);
     $update=$this->Common_mdl->update('user',$data,'id',$id);

      $query2=$this->db->query("INSERT INTO `role_permission1` ( `firm_id`, `role_id`, `role`, `Dashboard`, `Task`, `Leads`, `Webtolead`, `Proposal_Dashboad`, `Create_Proposal`, `Catalog`, `Template`, `Setting`, `Client_Section`, `Add_Client`, `Add_From_Company_House`, `Import_Client`, `Services_Timeline`, `Deadline_manager`, `Reports`, `Admin_Settings`, `Firm_Settings`, `Reminder_Settings`, `Team_and_Management`, `Staff_Custom_Firmlist`, `Tickets`, `Chat`, `Documents`, `Invoices`, `Give_Feedback`, `Task_Column_Settings`, `Client_Column_Settings`, `Firm_Tickets`, `Support_Tickets`, `My_Profile`, `Section_Settings`, `Notification_Settings`, `Pricelist_Settings`, `Services`, `Email_Template`, `Import_Task`, `Task_Sub_Task`, `Task_Timer`, `Import_leads`, `Leads_source`, `Lead_Status`) SELECT '".$id."', '".$role_id."','Admin', `Dashboard`, `Task`, `Leads`, `Webtolead`, `Proposal_Dashboad`, `Create_Proposal`, `Catalog`, `Template`, `Setting`, `Client_Section`, `Add_Client`, `Add_From_Company_House`, `Import_Client`, `Services_Timeline`, `Deadline_manager`, `Reports`, `Admin_Settings`, `Firm_Settings`, `Reminder_Settings`, `Team_and_Management`, `Staff_Custom_Firmlist`, `Tickets`, `Chat`, `Documents`, `Invoices`, `Give_Feedback`, `Task_Column_Settings`, `Client_Column_Settings`, `Firm_Tickets`, `Support_Tickets`, `My_Profile`, `Section_Settings`, `Notification_Settings`, `Pricelist_Settings`, `Services`, `Email_Template`, `Import_Task`, `Task_Sub_Task`, `Task_Timer`, `Import_leads`, `Leads_source`, `Lead_Status` FROM `role_permission1` where role='".$_SESSION['role_name']."' and firm_id='".$_SESSION['firm_admin_id']."'");

    }









public function object_2_array($result)
{
    $array = array();
    if(!empty($result)){
    foreach ($result as $key=>$value)
    {

       # if $value is an array then
        if (is_array($value))
        {
            #you are feeding an array to object_2_array function it could potentially be a perpetual loop.
            $array[$key]=$this->object_2_array($value);
        }

       # if $value is not an array then (it also includes objects)
        else
        {
       # if $value is an object then
        if (is_object($value))
        {
            $array[$key]=$this->object_2_array($value);
        } else {

            $array[$key]=$value;
}
        }
    }
  }
    return $array;
}




//Company house function End//


//Add  //


    public function firm_information($user_id=false)
      {
          error_reporting(0);
          $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
          $this->session->set_userdata('mail_url', $actual_link);
          $this->Loginchk_model->superAdmin_LoginCheck();   
          $data['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user','role','1');
          $data['client']=$this->Common_mdl->GetAllWithWhere('firm','user_id',$user_id);
          $data['responsible_team']=$this->Common_mdl->GetAllWithWhere('responsible_team','client_id',$user_id);
          $data['responsible_user']=$this->Common_mdl->GetAllWithWhere('responsible_user','client_id',$user_id);
          $data['responsible_department']=$this->Common_mdl->GetAllWithWhere('responsible_department','client_id',$user_id);
          $data['responsible_member']=$this->Common_mdl->GetAllWithWhere('responsible_members','client_id',$user_id);
          //$data['contactRec']=$this->Common_mdl->GetAllWithWhere('client_contacts','client_id',$user_id);
          $data['contactRec']=$this->db->query("SELECT * FROM  `firm_contacts` WHERE user_id =  '".$user_id."' ORDER BY make_primary DESC ")->result_array();
          $data['user']=$this->Common_mdl->GetAllWithWhere('user','id',$user_id);

          $data['staff_form'] = $this->Common_mdl->GetAllWithWheretwo('user','role','6','firm_admin_id',$_SESSION['id']);
         // $data['referby'] = $this->db->query("SELECT * FROM `client` where crm_first_name!='' and crm_first_name!='0' GROUP BY `crm_first_name`")->result_array();
          /** 21-08-2018 **/
  $query1=$this->db->query("SELECT * FROM `user` where role=4 AND autosave_status!='1' and crm_name!='' and firm_admin_id=".$_SESSION['id']." and status=1 order by id DESC");
            $results1 = $query1->result_array();
            $res=array();
            if(count($results1)>0){
              foreach ($results1 as $key => $value) {
                   array_push($res, $value['id']);
                }  
            }
            if(!empty($res)){
              $im_val=implode(',',$res);
              //echo $im_val."abc abc";
          $data['referby']=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id in (".$im_val.") order by id desc ")->result_array();    
           //$results = $query->result_array(); 
           }
           else
           {
             $data['referby']=array();
           }
          /** end of 21-08-2018 **/
          $data['teamlist']=$this->Common_mdl->getallrecords('team');
          $data['deptlist']=$this->Common_mdl->getallrecords('department');
          // $data['staff_form'] = $this->Common_mdl->GetAllWithWhere('user','role','6');
          // $data['managed_by'] = $this->Common_mdl->GetAllWithWhere('user','role','5');
           $data['staff_form'] =$this->db->query("select * from user where role in(5,6,3) and firm_admin_id=".$_SESSION['id']." ")->result_array();
        $data['managed_by'] = $this->db->query("select * from user where role in(5,3) and firm_admin_id=".$_SESSION['id']." ")->result_array();
          if($user_id!=''){
             $query=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id='".$user_id."' order by id desc ")->result_array();
             $res=array();
                foreach ($query as $key => $value) {
                     array_push($res, $value['id']);
                  }  
                  $im_val=implode(',',$res); 
                  $query_res=$this->db->query("SELECT * FROM add_new_task WHERE company_name in ('".$im_val."') order by id desc "); 
          // $data['task_list']=$this->Common_mdl->GetAllWithWhere('add_new_task','user_id',$user_id);
          $data['task_list']=$query_res->result_array();
          }else{
          $data['task_list']=$this->Common_mdl->getallrecords('add_new_task');
          }
          
          if($user_id!=''){

            // $query=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id='".$user_id."' order by id desc ")->result_array();
            //  $res=array();
            //     foreach ($query as $key => $value) {
            //          array_push($res, $value['id']);
            //       }  
            //       $im_val=implode(',',$res); 
            //       $query=$this->db->query("SELECT * FROM proposals WHERE company_id in ('".$im_val."') order by id desc "); 
             $query=$this->db->query("SELECT * FROM proposals WHERE company_id=".$user_id."  order by id desc ");      
           $data['proposals']=$query->result_array();
          }else{
          $data['proposals']=$this->Common_mdl->getallrecords('proposals');
          }

          /** rspt for leads **/
          if($user_id!=''){
             $query=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and user_id='".$user_id."' order by id desc ")->result_array();
             $res=array();
                                foreach ($query as $key => $value) {
                                     array_push($res, $value['id']);
                                  }  
                                  $im_val=implode(',',$res); 
                                   $query=$this->db->query("SELECT * FROM leads WHERE company in ('".$im_val."') order by id desc ");  
                                  // echo "SELECT * FROM leads WHERE user_id in ('".$im_val."') order by id desc";
          $data['leads']=$query->result_array();
          }else{
          $data['leads']=$this->Common_mdl->getallrecords('leads');
          }

          /** rspt 20-08-2018 for invoice view **/
           if($user_id!=''){
          $data['invoice_details'] = $this->Invoice_model->selectAllRecord('Invoice_details', 'client_email', $user_id);
           }

           if($user_id!=''){
          $data['reminder_details'] = $this->Invoice_model->selectAllRecord('client_reminder', 'user_id', $user_id);
           }


          /** end of 20-08-2018 **/

          /** end opf leads **/
  
  //print_r($data['staff_form']);die;
          //first api
          $curl = curl_init();
          $items_per_page = '10';
       
  curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk/company/'.$data['client'][0]['crm_company_number'].'/'); 
  
          curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET'); 
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($curl, CURLOPT_HEADER, false);
          curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($curl, CURLOPT_USERPWD,$this->companieshouseAPIkey);
          $response = curl_exec($curl);
         /* echo '<pre>';
          print_r($response);
          die;*/
          if($errno = curl_errno($curl)) {
          $error_message = curl_strerror($errno);
          echo "cURL error ({$errno}):\n {$error_message}";
          } else {
          curl_close($curl);
          $data_rec = json_decode($response);
           $array_rec = $this->object_2_array($data_rec);
           $data['company']=$array_rec;
  
  // second api
          $curl = curl_init();
       
  curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk/company/'.$data['client'][0]['crm_company_number'].'/officers?items_per_page=10&register_type=directors&register_view=false'); 
  
          curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET'); 
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($curl, CURLOPT_HEADER, false);
          curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($curl, CURLOPT_USERPWD,$this->companieshouseAPIkey);
          $response1 = curl_exec($curl);
  
          
          if($errno = curl_errno($curl)) {
          $error_message = curl_strerror($errno);
          echo "cURL error ({$errno}):\n {$error_message}";
          }
          curl_close($curl);
          $data1 = json_decode($response1);
          $array_rec1 = $this->object_2_array($data1);   
    /*echo '<pre>';
          print_r($array_rec1);
         // die; */ 
  
         
  $data['officers']=$array_rec1['items'];
    
  foreach($array_rec1['items'] as $key => $value){ 
  if($value['officer_role'] == 'director') {
  
  // third api
          $curl = curl_init();
          $items_per_page = '10';
    
  curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk'.$value['links']['officer']['appointments']);
   
          curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET'); 
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($curl, CURLOPT_HEADER, false);
          curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($curl, CURLOPT_USERPWD,$this->companieshouseAPIkey);
          $response2 = curl_exec($curl);
  
          
          if($errno = curl_errno($curl)) {
          $error_message = curl_strerror($errno);
          echo "cURL error ({$errno}):\n {$error_message}";
          } 
          curl_close($curl);
          $data2 = json_decode($response2);
          //$data['array_rec2'][]= $this->object_2_array($data2);
          $array_rec2= $this->object_2_array($data2);
          /*echo "<pre>";
          print_r($array_rec2['items']);die; */
  
            foreach ($array_rec2['items'] as $key => $val) {
          
  if((isset($val['appointed_to']['company_number']) == isset($data['client'][0]['crm_company_number'])) &&  (isset($val['appointed_on']) == isset($value['appointed_on']))  ) {
            
  $data['content'][]=$val;
  } /*else{
     $data['content'][]=$value; 
  }*/
  }
          //echo json_encode($datas);
          //echo $content;
  }
  } 
  
  // fourth filling history api
          $curl = curl_init();
       
  curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk/company/'.$data['client'][0]['crm_company_number'].'/filing-history'); 
  
          curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET'); 
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($curl, CURLOPT_HEADER, false);
          curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($curl, CURLOPT_USERPWD,$this->companieshouseAPIkey);
          $response3 = curl_exec($curl);
  
          
          if($errno = curl_errno($curl)) {
          $error_message = curl_strerror($errno);
          echo "cURL error ({$errno}):\n {$error_message}";
          }
          curl_close($curl);
          $data3 = json_decode($response3);
          $data['array_rec3'] = $this->object_2_array($data3);  
    }  //else close  
        $data['feedback'] =$this->db->query("select id,username,crm_email_id from user where username!='' and username!='0' and  crm_email_id!='' and crm_email_id!='0' "  )->result();  
          $this->load->view('Firm/firm_information',$data);
      }






function update_firm_contacts()
{

   if(isset($_POST['title'])){
  $client_id = $_POST['user_id'];
  $this->db->where('user_id',$client_id);
  $this->db->delete('firm_contacts');
   $i=1;
    $data = array();
   foreach ($_POST['title'] as $key => $details) {
      
      /* $data['title'] = $_POST['title'][$key];
       $data['first_name'] = $_POST['first_name'][$key];
       $data['middle_name'] = $_POST['middle_name'][$key];*/
      if(isset($_POST['title']) &&  !empty(array_filter($_POST['title']))) {  $data['title'] =  $_POST['title'][$key];}else{ $data['title'] = ''; }
      if(isset($_POST['first_name']) &&  !empty(array_filter($_POST['first_name']))) {  $data['first_name'] =  $_POST['first_name'][$key];}else{ $data['first_name'] = ''; }
      if(isset($_POST['middle_name']) &&  !empty(array_filter($_POST['middle_name']))) {  $data['middle_name'] =  $_POST['middle_name'][$key];}else{ $data['middle_name'] = ''; }
     // echo json_encode(array_filter($_POST['last_name']))."//l name";
      if(isset($_POST['last_name']) &&  !empty(array_filter($_POST['last_name']))) {  $data['last_name'] =  $_POST['last_name'][$key];}else{ $data['last_name'] = ''; }
      if(isset($_POST['surname']) &&  !empty(array_filter($_POST['surname']))) {  $data['surname'] =  $_POST['surname'][$key];}else{ $data['surname'] = ''; }
      if(isset($_POST['preferred_name']) && !empty(array_filter($_POST['preferred_name']))) {  $data['preferred_name'] =  $_POST['preferred_name'][$key];}else{ $data['preferred_name'] = ''; }
      if(isset($_POST['mobile']) &&  !empty(array_filter($_POST['mobile']))) {  $data['mobile'] =  $_POST['mobile'][$key];}else{ $data['mobile'] = ''; }
      if(isset($_POST['main_email']) &&  !empty(array_filter($_POST['main_email']))) {  $data['main_email'] =  $_POST['main_email'][$key];}else{ $data['main_email'] = ''; }

      if(isset($_POST['work_email'.$i])) {  $data['work_email'] =  json_encode($_POST['work_email'.$i]);}else{ $data['work_email'] = ''; }

      if(isset($_POST['nationality'])) {  $data['nationality'] =  $_POST['nationality'][$key];}else{ $data['nationality'] = ''; }
      if(isset($_POST['psc'])) {  $data['psc'] =  $_POST['psc'][$key];}else{ $data['psc'] = ''; }
      if(isset($_POST['shareholder'])) {  $data['shareholder'] =  $_POST['shareholder'][$key];}else{ $data['shareholder'] = ''; }
      if(isset($_POST['ni_number'])) {  $data['ni_number'] =  $_POST['ni_number'][$key];}else{ $data['ni_number'] = ''; }
      if(isset($_POST['content_type'])) {  $data['content_type'] =  $_POST['content_type'][$key];}else{ $data['content_type'] = ''; }
      if(isset($_POST['address_line1'])) {  $data['address_line1'] =  $_POST['address_line1'][$key];}else{ $data['address_line1'] = ''; }
      if(isset($_POST['address_line2'])) {  $data['address_line2'] =  $_POST['address_line2'][$key];}else{ $data['address_line2'] = ''; }
      if(isset($_POST['town_city'])) {  $data['town_city'] =  $_POST['town_city'][$key];}else{ $data['town_city'] = ''; }
      if(isset($_POST['post_code'])) {  $data['post_code'] =  $_POST['post_code'][$key];}else{ $data['post_code'] = ''; }

      if(isset($_POST['pre_landline'.$i])) {  $data['pre_landline'] =  json_encode($_POST['pre_landline'.$i]);}else{ $data['pre_landline'] = ''; }

      if(isset($_POST['landline'.$i])) {  $data['landline'] =  json_encode($_POST['landline'.$i]);}else{ $data['landline'] = ''; }

      if(isset($_POST['date_of_birth'])) {  $data['date_of_birth'] =  $_POST['date_of_birth'][$key];}else{ $data['date_of_birth'] = ''; }
      if(isset($_POST['nature_of_control'])) {  $data['nature_of_control'] =  $_POST['nature_of_control'][$key];}else{ $data['nature_of_control'] = ''; }
      if(isset($_POST['utr_number'])) {  $data['utr_number'] =  $_POST['utr_number'][$key];}else{ $data['utr_number'] = ''; }
      if(isset($_POST['marital_status'])) {  $data['marital_status'] =  $_POST['marital_status'][$key];}else{ $data['marital_status'] = ''; }  if(isset($_POST['contact_type'])) {  $data['contact_type'] =  $_POST['contact_type'][$key];}else{ $data['contact_type'] = ''; }
      //ddddddddddddd
      /*if(isset($_POST['make_primary'])) {  $data['make_primary'] =  $_POST['make_primary'][$key];}else{ $data['make_primary'] = '0'; }*/
      $mk = (isset($_POST['make_primary']))?$_POST['make_primary']:'0';
     // $mk = $_POST['make_primary'] - 1;
    //  $re_key=$key+1;
     
      $make_primary_loop=(isset($_POST['make_primary_loop']))?$_POST['make_primary_loop'][$key]:'0';
      echo $mk."--".$make_primary_loop."<br>";
      if($mk == $make_primary_loop){
        $data['make_primary'] = '1';
      }else{
        $data['make_primary'] = '0';
      }

      if(isset($_POST['user_id'])) {  $data['user_id'] =  $_POST['user_id'];}else{ $data['user_id'] = ''; }

      if(isset($_POST['occupation'])) {  $data['occupation'] =  $_POST['occupation'][$key];}else{ $data['occupation'] = ''; }

      if(isset($_POST['appointed_on'])) {  $data['appointed_on'] =  $_POST['appointed_on'][$key];}else{ $data['appointed_on'] = ''; }
      
      if(isset($_POST['country_of_residence'])) {  $data['country_of_residence'] =  $_POST['country_of_residence'][$key];}else{ $data['country_of_residence'] = ''; }

      if(isset($_POST['other_custom'])) {  $data['other_custom'] =  $_POST['other_custom'][$key];}else{ $data['other_custom'] = ''; }


       /*$data['preferred_name'] = $_POST['preferred_name'][$key];
       $data['mobile'] = $_POST['mobile'][$key];
       $data['main_email'] = $_POST['main_email'][$key];
       $data['nationality'] = $_POST['nationality'][$key];
       $data['psc'] = $_POST['psc'][$key];
       $data['ni_number'] = $_POST['ni_number'];
       $data['address_line1'] = $_POST['address_line1'][$key];
       $data['address_line2'] = $_POST['address_line2'][$key];
       $data['town_city'] = $_POST['town_city'][$key];
       $data['post_code'] = $_POST['post_code'][$key];
       $data['landline'] = $_POST['landline'][$key];
       $data['work_email'] = $_POST['work_email'][$key];
       $data['date_of_birth'] = $_POST['date_of_birth'][$key];
       $data['nature_of_control'] = $_POST['nature_of_control'][$key];
       $data['utr_number'] = $_POST['utr_number'][$key];
       $data['create_self_assessment_client'] = $_POST['create_self_assessment_client'][$key];
       $data['client_does_self_assessment'] = $_POST['client_does_self_assessment'][$key];
       $data['photo_id_verified'] = $_POST['photo_id_verified'][$key];
       $data['address_verified'] = $_POST['address_verified'][$key];
       $data['terms_signed'] = $_POST['terms_signed'][$key];*/

     // print_r($data);
       $this->Common_mdl->insert('firm_contacts',$data);



       //echo $this->db->last_query();
      /* if($this->Common_mdl->insert('client_contacts',$data))
       {
        //echo $this->db->last_query();
        return true;
       }else{
        return false;
       }*/
       $i++;
   }

/* 24.12.2018 */
//$this->email_remindersms($_POST['user_id']);


/** 03-09-2018 **/
$its_event=$_POST['event'];
$its_status=(isset($_POST['status']))?$_POST['status']:'no';
if($its_event=='add')
{
  /** 03-09-2018 **/
        /** welcome mail function **/
        $user_data=$this->db->query("select * from user where id=".$_POST['user_id']." " )->row_array();
        $tot_rec=$this->db->query("select * from firm where user_id=".$_POST['user_id']." " )->row_array();
        $mail_data['name']=$user_data['crm_name'];
        $mail_data['user_name']=$user_data['username'];
        $mail_data['password']=$user_data['confirm_password'];
        
           $contact_data=$this->Common_mdl->GetAllWithWheretwo('firm_contacts','user_id',$_POST['user_id'],'make_primary',1);
           if(count($contact_data)>0){

            /* 18.09.2018 */
         $proposal_content=$this->Common_mdl->select_record('proposal_proposaltemplate','title','welcome_mail');

          $sender_details=$this->db->query('select company_name,crm_name from admin_setting where user_id='.$_SESSION['id'].'')->row_array();

          $sender_company=$sender_details['company_name'];
          $sender_name=$sender_details['crm_name'];
          $body1=$proposal_content['body'];
          $subject=$proposal_content['subject'];
        
        //  echo $link;
           $a1  =   array('::Client Name::'=>$user_data['crm_name'],
            '::Client Company::'=>$tot_rec['crm_company_name'],
            '::ClientPhoneno::'=> $tot_rec['crm_mobile_number'],
            '::ClientWebsite::'=>$tot_rec['crm_company_url'],
            '::Client Username::'=>$user_data['username'],
            '::Client Password::'=> $user_data['confirm_password'],         
            ':: Sender Name::'=>$sender_company,
            ':: Sender Company::'=>$sender_name,       
            '::Date::'=>date("Y-m-d h:i:s"));

          $mail_data['body']  =   strtr($body1,$a1); 
          $subject  =   strtr($subject,$a1);
       //   $random_string=$this->generateRandomString();
          $body = $this->load->view('email/welcome-email-mail.php', $mail_data, TRUE);

      /* 18.09.2018 */

     // $body = $this->load->view('email/welcome-email-mail.php', $mail_data, TRUE);
      $this->load->library('email');
      $this->email->set_mailtype('html');
      $this->email->from('info@remindoo.org');
     // $this->email->to($_POST['emailid']);
      $this->email->to($contact_data[0]['main_email']);
      $this->email->subject(preg_replace('/ {2,}/', ' ', str_replace('&nbsp;', ' ', strip_tags($subject))));
      $this->email->message($body);
      $send = $this->email->send();
            }
      
        /** end of welcome mail function **/
/** end of 03-09-2018 **/
}
else
{
  if($its_status!='no' && $its_status==4)
  {
      /** 03-09-2018 **/
        /** welcome mail function **/
        $user_data=$this->db->query("select * from user where id=".$_POST['user_id']." " )->row_array();
        $tot_rec=$this->db->query("select * from client where user_id=".$_POST['user_id']." " )->row_array();

        $mail_data['name']=$user_data['crm_name'];
        $mail_data['user_name']=$user_data['username'];
        $mail_data['password']=$user_data['confirm_password'];
        
           $contact_data=$this->Common_mdl->GetAllWithWheretwo('firm_contacts','user_id',$_POST['user_id'],'make_primary',1);
           if(count($contact_data)>0){


            /* 18.09.2018 */
     // $tot_rec=$this->db->query("select * from client where user_id=".$_POST['client_id']." " )->row_array();
         $proposal_content=$this->Common_mdl->select_record('proposal_proposaltemplate','title','welcome_mail');

          $sender_details=$this->db->query('select company_name,crm_name from admin_setting where user_id='.$_SESSION['id'].'')->row_array();

          $sender_company=$sender_details['company_name'];
          $sender_name=$sender_details['crm_name'];
          $body1=$proposal_content['body'];
          $subject=$proposal_content['subject'];
        //  $random_string=$this->generateRandomString();
       //   $link=base_url().'Proposal_page/step_proposal/'.$random_string.'---'.$id;
        //  echo $link;
           $a1  =   array('::Client Name::'=>$user_data['crm_name'],
            '::Client Company::'=>$tot_rec['crm_company_name'],
            '::ClientPhoneno::'=> $tot_rec['crm_mobile_number'],
            '::ClientWebsite::'=>$tot_rec['crm_company_url'],
            '::Client Username::'=>$user_data['user_name'],
            '::Client Password::'=> $user_data['confirm_password'],         
            ':: Sender Name::'=>$sender_company,
            ':: Sender Company::'=>$sender_name,       
            '::Date::'=>date("Y-m-d h:i:s"));

          $mail_data['body']  =   strtr($body1,$a1); 
          $subject  =   strtr($subject,$a1);
        //  $random_string=$this->generateRandomString();
          $body = $this->load->view('email/welcome-email-mail.php', $mail_data, TRUE);


          /* 18.09.2018 */
    //  $body = $this->load->view('email/welcome-email-mail.php', $mail_data, TRUE);
      $this->load->library('email');
      $this->email->set_mailtype('html');
      $this->email->from('info@remindoo.org');
     // $this->email->to($_POST['emailid']);
      $this->email->to($contact_data[0]['main_email']);
      $this->email->subject(preg_replace('/ {2,}/', ' ', str_replace('&nbsp;', ' ', strip_tags($subject))));
      $this->email->message($body);
      $send = $this->email->send();
            }
      
        /** end of welcome mail function **/
/** end of 03-09-2018 **/

  }
}
/** end of 03-09-2018 **/

}
   //exit;
           return true;

}
public function Delete_firm_contact( $id )
{
  $this->db->query("delete from firm_contacts where id=".$id);
  echo $this->db->affected_rows();
}
public function SearchCompany()
  {
      $searchKey = $this->input->post('term');
      $alredyExist =$this->db->query("select crm_company_number from firm")->result_array();
      $alredyExist = array_column($alredyExist, 'crm_company_number');
  
      $content = $this->CompanyHouse_Model->SearchResults( $searchKey , $alredyExist );
      echo $content;
  }
public function CompanyDetails()
  {
    $companyNo = $_POST['companyNo'];
    $content = $this->CompanyHouse_Model->Get_Profile_Content( $companyNo);
    echo $content;     
  }

//Company house function Start//
/*
  @ user_id reafer to get exist company contacts
*/
public function selectcompany($user_id=0)
{
   $this->session->set_userdata('c_status', '1');
   $companyNo = $_POST['companyNo'];
   $Company_Details = $this->CompanyHouse_Model->Get_Company_Details( $companyNo );
   $content = '';
   if( !isset($Company_Details['error']) )
   {
      	$company_details = $Company_Details['response'];
        
        $address1= (!empty($company_details['registered_office_address']['address_line_1']))? $company_details['registered_office_address']['address_line_1']:''; 
        
        $address2= (!empty($company_details['registered_office_address']['address_line_2']))? $company_details['registered_office_address']['address_line_2']:''; 

        $address3= (!empty($company_details['registered_office_address']['address_line_3']))? $company_details['registered_office_address']['address_line_3']:''; 

        $registered_in=(!empty($company_details['jurisdiction']))? $company_details['jurisdiction']:''; 

        $crm_town_city=(!empty($company_details['registered_office_address']['locality']))? $company_details['registered_office_address']['locality']:'';

        $crm_post_code=(!empty($company_details['registered_office_address']['postal_code']))? $company_details['registered_office_address']['postal_code']:'';
      
         $company_url= '';
         $officers_url='';
        if(!empty($company_details['links']))
        {
              $company_url= 'https://beta.companieshouse.gov.uk'.$company_details['links']['self'];
              $officers_url=(isset($company_details['links']['officers']))?'https://beta.companieshouse.gov.uk'.$company_details['links']['officers']:'';
        }
        
        // $company_url= 'https://beta.companieshouse.gov.uk'.$company_details['links']['self'];
        // $officers_url='https://beta.companieshouse.gov.uk'.$company_details['links']['officers'];
        $sic_code='';
        $sic_codes='';
        $nature_business='';
       if(!empty($company_details['sic_codes']))
       {  

               /* foreach($company_details['sic_codes'] as $key => $val){
        $code=$this->db->query('SELECT * FROM sic_code WHERE COL1="'.$val.'"')->row_array();
        $sic_code[]='<option value='.$code['COL1'].'>'.$code['COL1'].'-'.$code['COL2'].'</option>';

        }*/
          $code=$this->db->query('SELECT * FROM sic_code WHERE COL1="'.$company_details['sic_codes'][0].'"')->row_array();
          $sic_code = $nature_business= $code['COL1'].'-'.$code['COL2'];
          $sic_codes=$company_details['sic_codes'];
        }

        $cmpny_type='';
        if(!empty($company_details['type']))
        {
           if($company_details['type']=='ltd')
           {
              $cmpny_type='Private Limited company';
           }
           else if($company_details['type']=='plc')
           {
            $cmpny_type='Public Limited company';
           }
           else if($company_details['type']=='llp')
           {
            $cmpny_type='Limited Liability Partnership';
           }
           else
           {
            $cmpny_type=$company_details['type'];
           }
         }
         /*should not insert data exist company*/
        if($user_id==0)
        {
            $user_value=
            array(
                'role'=> '',
                'status'=>0,
                'crm_name'=>$company_details['company_name'],
                //'autosave_status'=>1,
                'autosave_status'=>0,// rs 30-06-2018
                //'company_roles'=>1,
                'CreatedTime'=>time(),
                'user_type'=>'FA',
            );

              $this->db->insert( 'user', $user_value );
              $user_id = $this->db->insert_id();
              $client_value=array(
            'crm_company_name' => (!empty($company_details['company_name']))?$company_details['company_name']:'',            
            'crm_company_number'=>$companyNo,
            'crm_register_address'=>$address1.$address2.$crm_town_city.$crm_post_code,
            'crm_company_url'=>$company_url,
            'crm_officers_url'=>$officers_url,
            'crm_company_status'=>0,
            // 'crm_company_type'=>$company_details['type'],
            'crm_company_type'=>$cmpny_type,
            'crm_incorporation_date'=>(!empty($company_details['date_of_creation']))?$company_details['date_of_creation']:'',                
            'created_date'=>time(),
            //'user_id'=>(isset($_POST['user_id'])&&($_POST['user_id']==''))? $idd : $_POST['user_id'],
            'user_id'=>$user_id,
            //'autosave_status'=>1,
            'autosave_status'=>0,//rs 30-06-2018
            "crm_company_sic"=> $sic_code,                
            "crm_allocation_holder" => '',
            'crm_registered_in'=>$registered_in,
            'crm_address_line_one'=>$address1,
            'crm_address_line_two'=>$address2,
            'crm_address_line_three'=>$address3,
            'crm_town_city'=>$crm_town_city,
            'crm_post_code'=>$crm_post_code

                                );

              $this->db->insert( 'firm', $client_value );

              $id=$this->db->insert_id();
              $this->db->query("update user set firm_id=".$id." where id = ".$user_id);

              


        }
    $content .='<h3>'.$company_details['company_name'].'</h3>';
    $content .=  $this->CompanyHouse_Model->Get_Company_Contact_Content( $companyNo , $user_id );  
   }

  $content .='<span class="view_contacts" onclick="return addmorecontact('.$user_id.');">Next</span>';

  echo json_encode( array( "html" => $content) );
  }      

  public function maincontact()
  {
    $data_c = $this->CompanyHouse_Model->Add_Contact('firm_contacts');
      
    if(isset($_POST['cnt']) && $_POST['cnt']!='')
    {  
      $data['cnt'] = $_POST['cnt'];
      $data['birth']='';
      $data['contact_from']='company_house';
      $data = array_merge($data_c,$data);
      $this->load->view('Firm/contact_form',$data);           
    }
    else
    {
      echo $data_c['contact_id'];
    }
  }

  public function plans()
  {
     $this->Loginchk_model->superAdmin_LoginCheck();

     $data['currency'] = $this->Common_mdl->get_price('super_admin_details','id','1','currency');
     $data['plans'] = $this->Common_mdl->getallrecords('subscription_plan');

     $this->load->view('Firm/plans',$data);
     unset($_SESSION['flash_msg']);
  }

  public function add_plan()
  {
     $this->Loginchk_model->superAdmin_LoginCheck();
  
     $data['plandetails'] = array();
     
     $this->load->view('Firm/addplan',$data);
  }

  public function edit_plan($id)
  { 
     $this->Loginchk_model->superAdmin_LoginCheck();

     $data['plandetails'] = $this->Common_mdl->GetAllWithWhere('subscription_plan','id',$id);     

     $this->load->view('Firm/addplan',$data);
  }

  public function delete_plan($id)
  {
     $this->Loginchk_model->superAdmin_LoginCheck();

     $delete = $this->db->query('DELETE FROM subscription_plan WHERE id = "'.$id.'"');

     if($delete!='0')
     {
        $_SESSION['flash_msg'] = 'Plan Deleted Successfully.';
     }
     else
     {
        $_SESSION['flash_msg'] = 'Process Failed.';
     }

     redirect('/firm/plans');
  }

  public function insertplan()
  {  
     $this->Loginchk_model->superAdmin_LoginCheck();

     $_POST['created_at'] = time();

     $insert = $this->Common_mdl->insert('subscription_plan',$_POST); 

     if($insert!='0')
     {
        $_SESSION['flash_msg'] = 'Plan Added Successfully.';
     }
     else
     {
        $_SESSION['flash_msg'] = 'Process Failed.';
     }

     redirect('/firm/plans');
  }

  public function updateplan()
  {
     $this->Loginchk_model->superAdmin_LoginCheck();

     $plan_id = $_POST['plan_id'];
     unset($_POST['plan_id']);

     $_POST['updated_at'] = time();

     $update = $this->Common_mdl->update('subscription_plan',$_POST,'id',$plan_id);

     if($update!='0')
     {
        $_SESSION['flash_msg'] = 'Plan Updated Successfully.';
     }
     else
     {
        $_SESSION['flash_msg'] = 'Process Failed.';
     }

     redirect('/firm/plans');
  }

  public function check_distinct()
  {   
      $sql = "SELECT count(*) as count FROM subscription_plan WHERE plan_name LIKE '%".trim($_POST['plan_name'])."%'";

      if(!empty($_POST['id']))
      {
         $sql = $sql." AND id!='".$_POST['id']."'";
      }
    
      $data = $this->db->query($sql)->row_array(); 
      
      echo json_encode($data);
  }

  public function stripe()
  {
     $this->Loginchk_model->superAdmin_LoginCheck();     

     $data['stripe'] = json_decode($this->Common_mdl->get_price('firm','firm_id','0','payment_details'));

     $this->load->view('Firm/stripe',$data);  
     unset($_SESSION['msg']);
  }

  public function keys_update()
  {
     if(isset($_POST['save']))
     {
        $stripe = json_encode(array('publish_key' => $_POST['publish_key'],'secret_key' => $_POST['secret_key']));
        $update = $this->db->update('firm',['payment_details'=>$stripe],"firm_id='0'");

        if($update == true)
        {
           $_SESSION['msg'] = 'Updated Successfully.';
        }
        else
        {
           $_SESSION['msg'] = 'Process Failed.';
        }

        redirect('/Firm/stripe');
     }
  }
  public function uplode_mail_header_image()
  {
    //print_r($_FILES);
    if( !empty( $_FILES['file'] ) )
    {
      $name = $this->Common_mdl->do_upload( $_FILES['file'] , 'uploads/firm_logo' );
      echo json_encode( [ 'location'=>base_url()."uploads/firm_logo/".$name ] );
    }
  }
  public function image_upload_from_editor()
  {
    //print_r($_FILES);
    if( !empty( $_FILES['file'] ) )
    {
      $name = $this->Common_mdl->do_upload( $_FILES['file'] , 'uploads/' );
      echo json_encode( [ 'location'=>base_url()."uploads/".$name ] );
    }
  }
  public function test_smtp_details()
  {
   /* echo "<pre>";
    print_r($_POST);*/
    $this->Security_model->chk_login();  
    $data['smtp_server']    = $_POST['smtp_server'];
    $data['smtp_username']  = $_POST['smtp_username'];
    $data['smtp_password']  = $_POST['smtp_password'];
    $data['smtp_security']  = $_POST['smtp_security'];
    $data['smtp_port']      = $_POST['smtp_port'];
    $data['smtp_email']     = $_POST['smtp_email'];
    $data['smtp_from_name'] = $_POST['smtp_from_name'];
    $data['firm_id']        = $_SESSION['firm_id'];

    $this ->Common_mdl->UpdateExit_or_Insert( 'mail_protocol_settings' , $data , [ 'firm_id'=> $_SESSION['firm_id'] ] );
    $Issend =  firm_settings_send_mail( $_SESSION['firm_id'] , $_POST['to'] , 'SMTP Settings Test' , 'SMTP Settings Test Mail' , 1 );
    ob_clean();
    echo json_encode( $Issend );

  }

}
?>