<?php
/**
 * @author        Tharanga Kothalawala <tharanga.kothalawala@hubculture.com>
 * @copyright (c) 2020 by HubCulture Ltd.
 */

namespace App\Mail;

use Aws\Ses\Exception\SesException;
use Aws\Ses\SesClient;
use Exception;
//use Psr\Log\LoggerInterface;
//use Psr\Log\NullLogger;

//class AwsSesBasedMailer implements Mailer
class AwsSesBasedMailer implements Mailer
{
    /**
     * @var string
     */
    private $baseDomainName;

    /**
     * @var string
     */
    private $fromEmail;

    /**
     * @var SesClient
     */
    private $sesClient;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * AwsSesBasedMailer constructor.
     *
     * @param string               $baseDomainName  Website's base domain name
     *                                              Ex: http://hubculture.com
     * @param string               $fromEmail       The sender email address. This must be a verified email by the SES
     *                                              console.
     *                                              Ex : "Zeke from Hub Culture<zeke@hubculture.com>"
     *
     * @see https://console.aws.amazon.com/ses/home?region=us-east-1#verified-senders-email:
     *
     * @param string               $awsAccessKey    AWS API access username. This related user must have
     *                                              'ses:SendEmail' grant in the 'AmazonSesSendingAccess' permission
     *                                              policy set.
     * @param string               $awsAccessSecret AWS API access password
     * @param LoggerInterface|null $logger          [optional] logger to log any error
     */
    public function __construct(
        //$baseDomainName,
        //$fromEmail,
        //$awsAccessKey,
        //$awsAccessSecret,
        //LoggerInterface $logger = null
    ) {
        $this->baseDomainName = 'accotax.co.uk'; //$baseDomainName;
        $this->fromEmail      = 'reminders@accotax.co.uk'; //$fromEmail;
        //$this->logger         = is_null($logger) ? new NullLogger() : $logger;
        $config               = [
            'version'     => '2010-12-01',
            //'region'    => 'us-east-1',
            'region'      => 'eu-west-2',
            /**
             * @see https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/guide_credentials_hardcoded.html
             */
            'credentials' => [
                'key'    => 'AKIAJEG55BK3WQHGVDFA', //$awsAccessKey,
                'secret' => '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', //$awsAccessSecret,
            ],
        ];
        $this->sesClient = SesClient::factory($config);
    }

    /**
     * Use this to send an email using the standard mail template.
     *
     * @param string $subject Subject of the email
     * @param string $toEmail The recipient email address
     * @param string $message Email message
     *
     * @return bool true if the email was sent with no error or false otherwise.
     */
    public function send($subject, $toEmail, $message)
    {
        $message = str_replace('__DOMAIN__', $this->baseDomainName, $message);
        return $this->sendWithTemplate(
            'emails.default',
            $subject,
            $toEmail,
            [
                'message' => $message,
            ]
        );
    }

    /**
     * Use this to send an email using a template.
     *
     * @param string $templateName The name of the template
     * @param string $subject      Subject of the email
     * @param string $toEmail      The recipient email address
     * @param array  $templateData Data for any template variables
     *
     * @return bool true if the email was sent with no error or false otherwise.
     */
    public function sendWithTemplate($templateName, $subject, $toEmail, array $templateData)
    {
        $templateData = array_merge(
            [
                'title' => $subject,
                'newsList' => newsList(),
            ],
            $templateData
        );
        return $this->sendRaw($subject, $toEmail, view($templateName, $templateData));
    }

    /**
     * Use this to send an email using any raw HTML.
     *
     * @param string $subject     Subject of the email
     * @param string $toEmail     The recipient email address
     * @param string $htmlContent Email body
     *
     * @return bool true if the email was sent with no error or false otherwise.
     */
    public function sendRaw($subject, $toEmail, $htmlContent)
    {
        /**
         * @see https://docs.aws.amazon.com/ses/latest/DeveloperGuide/send-using-sdk-php.html
         */
        $charSet = 'UTF-8';
        $messageId = 0;
        try {
            $result = $this->sesClient->sendEmail([
                'Destination' => [
                    'ToAddresses' => [$toEmail],
                ],
                'ReplyToAddresses' => [$this->fromEmail],
                'Source' => $this->fromEmail,
                'Message' => [
                    'Body' => [
                        'Html' => [
                            'Charset' => $charSet,
                            'Data' => $htmlContent,
                        ],
                        'Text' => [
                            'Charset' => $charSet,
                            'Data' => strip_tags($htmlContent),
                        ],
                    ],
                    'Subject' => [
                        'Charset' => $charSet,
                        'Data' => $subject,
                    ],
                ],
            ]);

            $messageId = $result['MessageId'];
        } catch (SesException $ex) {
            $this->logger->error(
                sprintf("Email cannot be sent to [%s] due to aws error : '%s'", $toEmail, $ex)
            );
        } catch (Exception $ex) {
            $this->logger->error(
                sprintf("Email cannot be sent to [%s] due to error : '%s'", $toEmail, $ex->getMessage())
            );
        }

        if (intval($messageId) === 0) {
            $this->logger->error(sprintf("Email cannot be sent to [%s] due to some error.", $toEmail), [__CLASS__]);
            return false;
        }

        $this->logger->debug(sprintf("Email sent to [%s]. AWS message id [%s]", $toEmail, $messageId), [__CLASS__]);
        return true;
    }
}
