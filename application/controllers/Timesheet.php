<?php
error_reporting(E_STRICT);
class TimeSheet extends CI_Controller
{
	public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('Common_mdl', 'Report_model', 'Security_model', 'Task_creating_model'));
		$this->load->library('Excel');
		$this->load->helper('comman');
	}

	public function index()
	{
		$this->Security_model->chk_login();

		if ($_SESSION['permission']['Reports']['view'] == '1') {
			$data['roles'] = $this->Common_mdl->GetAllWithWhere('Role', 'firm_id', $_SESSION['firm_id']);
			$data['task_list'] = $this->Report_model->getTasks();
			$first_day = date("Y-m-01");
			$end_day = date("Y-m-t");
			$end_day = date('Y-m-d', strtotime($end_day . ' +1 day'));
			$data['task_list_this_month'] = $this->Report_model->task_this_month('add_new_task', $first_day, $end_day);
			$data['groups'] = $this->db->query("SELECT * from `Groups` where user_id='" . $_SESSION['id'] . "'")->result_array();
			$data['firm_users'] = $this->Report_model->getFirmUsers();
			$data['schedule_records'] = $this->db->query('SELECT * from reports_schedule where user_id=' . $_SESSION['id'] . ' and reports_type="timesheet"')->row_array();
			$data['getallUser'] = $this->Report_model->getClients();
			$data['column_setting'] = $this->Common_mdl->getallrecords('column_setting_new');

			$this->load->view('reports/report', $data);
		} else {
			$this->load->view('users/blank_page');
		}
	}
	public function email()
	{
		$this->Security_model->chk_login();
		$this->load->view('reports/email-home');
	}

	public function task_filter()
	{
		$staff = isset($_POST['staff']) ? $_POST['staff'] : "";
		$period = isset($_POST['period']) ? $_POST['period'] : "";
		$role = isset($_POST['role']) ? $_POST['role'] : "";

		if (!empty($period)) {
			if ($period == '7') {
				$first_day = date('Y-m-d', strtotime('-7 days'));
				$end_day = date("Y-m-d");
			} else if ($period == '30') {
				$first_day = date('Y-m-d', strtotime('-30 days'));
				$end_day = date("Y-m-d");
			} else if ($period == '90') {
				$first_day = date('Y-m-d', strtotime('-90 days'));
				$end_day = date("Y-m-d");
			} else if ($period == '365') {
				$first_day = date("Y-m-d", strtotime("-1 year"));
				$end_day = date("Y-m-d");
			}

			$time_cond = 'AND (start_date BETWEEN "' . $first_day . '" AND "' . $end_day . '" OR end_date BETWEEN "' . $first_day . '" AND "' . $end_day . '")';
		} else {
			$time_cond = '';
		}

		$Assigned_Task = Get_Assigned_Datas('TASK');
		$assigned_task = implode(',', $Assigned_Task);

		$task_list = $this->db->query('SELECT * from add_new_task WHERE FIND_IN_SET(id,"' . $assigned_task . '") AND firm_id = "' . $_SESSION['firm_id'] . '" AND task_status !="" AND task_status IN("notstarted","inprogress","awaiting","archive","complete") ' . $time_cond . '')->result_array();

		if (!empty($role) || !empty($staff)) {
			$i = 0;
			foreach ($task_list as $key => $value) {
				$arr[$i]['id'] = $value['id'];
				$arr[$i]['subject'] = $value['subject'];
				$arr[$i]['start_date'] = $value['start_date'];
				$arr[$i]['end_date'] = $value['end_date'];

				if (!empty($staff)) {
					$hours = $this->Common_mdl->get_working_hours($value['id'], $staff);
				} else {
					$hours = 0;
				}

				$duration = array();

				if (!empty($role)) {
					$role_users_id = explode(',', $role);

					foreach ($role_users_id as $key1 => $value1) {
						$duration[$value1] = $this->Common_mdl->get_working_hours($value['id'], $value1);
					}
				}

				$arr[$i]['hours_worked'] = $hours + array_sum($duration);
				$i++;
			}

			$data['task_list'] = $arr;
		} else {
			$data['task_list'] = $task_list;
		}

		$this->load->view('reports/time_sheetsuccess', $data);
	}

	public function update_timeperiod()
	{
		$time_period = $_POST['time_period'];
		$id = $_POST['id'];
		$data = array('time_period' => $time_period);
		$up = $this->Common_mdl->update('add_new_task', $data, 'id', $id);
		if ($up) {
			$data['status'] = 1;
		}
		echo json_encode($data);
	}

	public function update_reportto()
	{
		$team = array();
		$department = array();
		$assign = array();
		$visible_to = array();
		$id = $_POST['id'];
		$value = $this->db->query("select reports_to,reports_to_team,reports_to_dept,visible_to from add_new_task where id=" . $id . "")->row_array();
		if (isset($value['reports_to'])) {
			$reports_to = explode(',', $value['reports_to']);
			for ($i = 0; $i < count($reports_to); $i++) {
				array_push($assign, $reports_to[$i]);
			}
		}
		if (isset($value['visible_to'])) {
			$visible_to_value = explode(',', $value['visible_to']);
			for ($i = 0; $i < count($visible_to_value); $i++) {
				array_push($visible_to, $visible_to_value[$i]);
			}
		}
		if (isset($value['reports_to_team'])) {
			$reports_to_team = explode(',', $value['reports_to_team']);
			for ($i = 0; $i < count($reports_to_team); $i++) {
				array_push($team, $reports_to_team[$i]);
			}
		}
		if (isset($value['reports_to_dept'])) {
			$reports_to_dept = explode(',', $value['reports_to_dept']);
			for ($i = 0; $i < count($reports_to_dept); $i++) {
				array_push($department, $reports_to_dept[$i]);
			}
		}
		if (isset($_POST['worker'])) {
			foreach ($_POST['worker'] as $key => $value) {
				$ex_val = explode('_', $value);
				if (count($ex_val) > 1) {
					if ($ex_val[0] == 'tm') {
						array_push($team, $ex_val[1]);
					}
					if ($ex_val[0] == 'de') {
						array_push($department, $ex_val[1]);
					}
				} else {
					array_push($assign, $ex_val[0]);
				}
			}
			$data['reports_to'] = implode(',', array_unique($assign));
			$data['reports_to_team'] = implode(',', array_unique($team));
			$data['reports_to_dept'] = implode(',', array_unique($department));
		}
		$roles_value = implode(',', $_POST['roles']);
		$roles = explode(',', $roles_value);
		for ($i = 0; $i < count($roles); $i++) {
			array_push($visible_to, $roles[$i]);
		}
		$string = implode(',', array_unique($visible_to));
		$data['visible_to'] = trim($string, ',');
		$up = $this->Common_mdl->update('add_new_task', $data, 'id', $id);
		if ($up) {
			$data['status'] = 1;
		}
		echo json_encode($data);
	}

	public function last_seven()
	{
		$search = $_POST['days'];
		//	echo $search;
		$first_day = date('Y-m-d', strtotime('-7 days'));
		$end_day = date("Y-m-d");
		//	  echo $first_day;
		//	  echo $end_day;
		$data['task_list_this_month'] = $this->Report_model->task_this_month('add_new_task', $first_day, $end_day);
		$this->load->view('reports/timesheet_success.php', $data);
	}
	public function last_thirty()
	{
		$search = $_POST['days'];
		//	echo $search;
		$first_day = date('Y-m-d', strtotime('-30 days'));
		$end_day = date("Y-m-d");
		//   echo $first_day;
		//  echo $end_day;
		$data['task_list_this_month'] = $this->Report_model->task_this_month('add_new_task', $first_day, $end_day);
		$this->load->view('reports/timesheet_success.php', $data);
	}
	public function last_ninty()
	{
		$search = $_POST['days'];
		//	echo $search;
		$first_day = date('Y-m-d', strtotime('-90 days'));
		$end_day = date("Y-m-d");
		//   echo $first_day;
		//  echo $end_day;
		$data['task_list_this_month'] = $this->Report_model->task_this_month('add_new_task', $first_day, $end_day);
		$this->load->view('reports/timesheet_success.php', $data);
	}
	public function last_year()
	{
		$search = $_POST['days'];
		//	echo $search;
		$first_day = date("Y-m-d", strtotime("-1 year"));
		$end_day = date("Y-m-d");
		//   echo $first_day;
		//  echo $end_day;
		$data['task_list_this_month'] = $this->Report_model->task_this_month('add_new_task', $first_day, $end_day);
		$this->load->view('reports/timesheet_success.php', $data);
	}
	public function month_wise()
	{
		$search = $_POST['days'];
		//	echo $search;
		$first_day = date("Y-m-01", strtotime($search));
		$end_day = date("Y-m-t", strtotime($search));
		// echo $first_day;
		//	  echo $end_day;
		$data['task_list_this_month'] = $this->Report_model->task_this_month_demo('add_new_task', $first_day, $end_day);
		$this->load->view('reports/timesheet_success.php', $data);
	}


	public function timesheet_custom()
	{
		$this->Security_model->chk_login();

		if ($_SESSION['permission']['Reports']['view'] == '1') {
			$data['client'] = $this->Report_model->client_list();
			$data['firm_users'] = $this->Report_model->getFirmUsers();

			$this->load->view('reports/Timesheet_customisation.php', $data);
		} else {
			$this->load->view('users/blank_page');
		}
	}

	public function custom()
	{
		if (empty($_POST['assignee']) || $_POST['assignee'] == 0) {
			$firm_users = $this->Report_model->getFirmUsers();

			foreach ($firm_users as $key => $value) {
				$assignee[] = $value['id'];
			}
		} else {
			$assignee = $_POST['assignee'];
		}

		if (!empty($_POST['filter']) && $_POST['filter'] != 'all') {
			if ($_POST['filter'] == 'week') {
				$current_date = date('Y-m-d');
				$week = date('W', strtotime($current_date));
				$year = date('Y', strtotime($current_date));

				$dto = new DateTime();
				$start_date = $dto->setISODate($year, $week, 0)->format('Y-m-d');
				$end_date = $dto->setISODate($year, $week, 6)->format('Y-m-d');
				$end_date = date('Y-m-d', strtotime($end_date . ' +1 day'));
			} else if ($_POST['filter'] == 'three_week') {
				$current_date = date('Y-m-d');
				$weeks = strtotime('- 3 week', strtotime($current_date));
				$start_date = date('Y-m-d', $weeks);
				$addweeks = strtotime('+ 3 week', strtotime($start_date));
				$end_Week = date('Y-m-d', $addweeks);
				$end_date = date('Y-m-d', strtotime($end_Week . ' +1 day'));
			} else if ($_POST['filter'] == 'month') {
				$start_date = date("Y-m-01");
				$month_end = date("Y-m-t");
				$end_date = date('Y-m-d', strtotime($month_end . ' +1 day'));
			}

			if ($_POST['filter'] == 'Three_month') {
				$end_date = date('Y-m-d', strtotime(date('Y-m-d') . '+1 day'));
				$start_date = date('Y-m-d', strtotime('- 3 month', strtotime($end_date)));
			}

			$filter = 'AND (start_date BETWEEN "' . $start_date . '" AND "' . $end_date . '" OR end_date BETWEEN "' . $start_date . '" AND "' . $end_date . '")';
		} else if (!empty($_POST['from_date']) && !empty($_POST['to_date'])) {
			$start_date = $_POST['from_date'];
			$end_date = $_POST['to_date'];
			$filter = 'AND (start_date BETWEEN "' . $start_date . '" AND "' . $end_date . '" OR end_date BETWEEN "' . $start_date . '" AND "' . $end_date . '")';
		} else {
			$filter = '';
		}

		if (!empty($_POST['client'])) {
			$rec = $this->Report_model->selectRecord('client', 'id', $_POST['client']);
			$client = 'AND user_id = "' . $rec['user_id'] . '"';
		} else {
			$client = '';
		}

		$Assigned_Task = Get_Assigned_Datas('TASK');
		$assigned_task = implode(',', $Assigned_Task);
		$sql_query = 'SELECT * from add_new_task WHERE FIND_IN_SET(id,"' . $assigned_task . '") AND firm_id = "' . $_SESSION['firm_id'] . '" AND task_status IN(1,2,3,4,5) ' . $filter . ' ' . $client . ' ORDER BY id ASC';
		//echo $sql_query;
		$task_list = $this->db->query($sql_query)->result_array();

		//print_r($task_list);

		$i = 0;
		
		foreach ($task_list as $key => $value) {
			$estimated_time =  (float)$value['estimated_time'];

			if (!$estimated_time) {
				$related_to_service = $value['related_to_services'];

				if (strrpos($related_to_service, 'ser_') !== false) {
					$related_to_service_arr = explode('_', $related_to_service);
					$related_to_service = $related_to_service_arr[1];
				}

				$related_service = $this->Common_mdl->select_record('service_lists', 'id', $related_to_service);

				if (is_array($related_service) && count($related_service)) {
					$estimated_time = (float)$related_service['estimated_time'];
				}
			}
			$stop_sub_task_id            = ($value["sub_task_id"]) ? $value["sub_task_id"] : "0";
			$userwiseTimers = $this->Task_creating_model->getSubtaskTimers($stop_sub_task_id);
			$tot_time_spent = 0;

			foreach ($userwiseTimers as $ut) {
				$tot_time_spent += $ut['total_time_spent'];
			}
			$tot_time_spent = $tot_time_spent/3600;
			$task_assignees = Get_Module_Assigees('TASK', $value['id']);
			$assignees_id = array();
			$duration = array();

			//print_r($task_assignees);
			
			foreach ($assignee as $key1 => $value1) {
				foreach ($task_assignees as $key2 => $value2) {
					if ($value1 == $value2) {
						$assignees_id[] = $value2;
						$time = $this->Common_mdl->get_working_hours($value['id'], $value2);
						if ($time > 0) {
							$duration[$value2] = $time;
						}
					}
				}
			}

			if (count($assignees_id) > 0) {
				$arr[$i]['id'] = $value['id'];
				$arr[$i]['subject'] = $value['subject'];
				$arr[$i]['start_date'] = $value['start_date'];
				$arr[$i]['end_date'] = $value['end_date'];
				$arr[$i]['hours_worked'] = round(array_sum($duration), 2);
				$arr[$i]['hours_estimated'] = round($estimated_time, 2);
				$arr[$i]['hours_total'] = round($tot_time_spent, 2);
				$i++;
			}
		}

		$data['task_list'] = $arr;

		$this->load->view('reports/Timesheet_ajax', $data);
	}



	public function document_write()
	{

		$this->load->view('reports/document_file');
	}
}
