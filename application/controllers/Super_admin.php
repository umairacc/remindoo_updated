<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Super_admin extends CI_Controller 
{

  	public function __construct()
  	{
        parent::__construct();
        $this->load->model(array('Common_mdl','Loginchk_model'));
        $this->load->helper(array("cookie"));
    }

    public function index()
    {	
     // print_r( $this->input->cookie("logout_data"));
         
    	if( isset( $_SESSION['is_superAdmin_login'] ) )
    	{ 
    		redirect('firm');
    	}
    	else
    	{               
    		$this->load->view('super_admin/superAdmin_login');
    	}
    }
    
    public function logout()
    {            
        session_destroy();
        unset( $_SESSION );
        redirect('Super_admin', 'refresh');
    }

    public function check_credentials()
    {
    	if( !empty( $_POST['user_name'] ) &&  !empty( $_POST['password'] ) )
    	{	
    		$password = md5( $_POST['password'] );
    		$query = $this->db->query("select * from super_admin_details where user_name='".$_POST['user_name']."' and password='".$password."'");
    		if($query -> num_rows() == 1)
    		{    
                //clear other session datas
                $_SESSION = [];
                $data = $query->row_array();       
                unset( $data['password'] , $data['decrypted_password'] );
                $this->session->set_userdata($data);
                $this->session->set_userdata('firm_id',0);
                $this->session->set_userdata('is_superAdmin_login',1);
                $this->session->set_userdata('user_type','SA');
                $this->session->set_userdata('userId',1);

                
                $permission_Modules = $this->db->query("select * from role_permission1 limit 1")->row_array();

                $permission_Modules = array_slice( array_keys( $permission_Modules ) , 4 , 43 );                

                $_SESSION['permission'] = array_fill_keys( $permission_Modules , ['view'=>1,'create'=>1,'edit'=>1,'delete'=>1]);
                
               // $_SESSION['userId']

                $cookie_name = "remindoo_logout";
                $cookie_value = "remindoo_superadmin";
                setcookie($cookie_name, $cookie_value,NULL,"/");
            

                


    			redirect('firm');
    		}
    		else
	    	{
	    		$this->session->set_flashdata('Login_result','Please Check Your Credentials!..');
	    		redirect('Super_admin');
	    	}
    	}
    	else
    	{
    		$this->session->set_flashdata('Login_result','Please Check Your Credentials!..');
    		redirect('Super_admin');
    	}
    }
    public function Switch_to_SuperAdmin()
    {   
        if(isset($_SESSION['SA_TO_FA']) && $_SESSION['SA_TO_FA']==1)
        {
            $data = $this->db->query("select * from  super_admin_details where id='1'")->row_array();
            $_POST['user_name'] = $data['user_name'];
            $_POST['password']  = $data['decrypted_password'];
            $this->check_credentials();
        }

    }
    public function GetProfile_Data()
    {
        $data = $this->Common_mdl->select_record('super_admin_details','id',1);
        echo json_encode( $data );
    }        
    public function Edit_Profile()
    {   
    	if( !empty($_POST) )
    	{    		    		
            if( !empty($_FILES['profile_picture']['name']) )
            {

                $data['profile_picture_path'] = $this->Common_mdl->do_upload( $_FILES['profile_picture'] , 'uploads/super_admin' );
                $data['profile_picture_path'] = "uploads/super_admin/".$data['profile_picture_path'];
            }
            $data['display_name']   = $_POST['display_name'];
            $data['user_name']      = $_POST['user_name'];
            $data['password']   = md5( trim( $_POST['password'] ) );
            $data['decrypted_password']       = trim( $_POST['password'] );
		        $data['currency'] = $_POST['currency'];
      			$this->db->update("super_admin_details",$data,"id=1");

      			if ( $this->db->affected_rows() )
      			{
                      $_SESSION['display_name']   = $data['display_name'];
                      $_SESSION['user_name']      = $data['user_name'];
                      if(! empty( $data['profile_picture_path'] ) )
                      {
                          $_SESSION['profile_picture_path'] = $data['profile_picture_path'];
                      }

      				echo json_encode(['result'=>1]);die;
      			}
      			else
      			{
      				echo json_encode(['result'=>0,'message'=>'Data Dose\'nt Update']);die;
      			}
    	}
    	else 
    	{
    		echo json_encode(['result'=>0,'message'=>'All Feilds Are Required..!']);die;
    	}    	
    }

    public function Update_column_settings_visibility()
    {      
      $table = $_POST['table_name'];
      $column = $_POST['column'];
      $state = $_POST['state'];
      $this->db->update('super_admin_column_settings',["visible" => $state],"table_name='".$table."' and name='".trim( $column )."'");
      echo $this->db->affected_rows();
    }
    public function Update_column_settings_order()
    {      
      $i=1;
      $table = $_POST['table_name'];
      foreach ($_POST['reorder'] as $column) 
      {
        $this->db->update('super_admin_column_settings',["orders" => $i],"table_name='".$table."' and name='".trim( $column )."'");
        $i++;
        echo $this->db->affected_rows();
      }
    }

    public function task_progress_view()
    {
     
      // $this->Common_mdl->document_function();
      $this->Loginchk_model->superAdmin_LoginCheck();
       $data['progress_task_status']=$this->db->query('SELECT * from progress_status  where firm_id IN (0) and status="0" ORDER BY order_by ASC')->result_array();
      $this->load->view('users/task_status_view',$data);
    }



/////////////////////////////////SATZ/////////////////////////////////////////

//Take home page content based on database id
// home_page_header 
   public function home_content()
    {
      $this->Loginchk_model->superAdmin_LoginCheck();


     // echo '<pre>';print_r($this->input->post());exit;

      if($this->input->post('submit') == 'submit'){

         


        foreach ($this->input->post() as $i_key => $i_val) {
            if($i_val != ''){
            $data = array('keyword' => $i_key,'created_at' => strtotime(date('Y-m-d h:i:s a', time())), 'output' => $i_val );

            $condition = array('keyword' => $i_key);
            $isAvail = $this->db->select('keyword')->get_where('page_content',$condition)->num_rows();
            if(($isAvail) > 0)  $this->db->where($condition)->update('page_content',$data);
            else   $this->db->insert('page_content',$data);
            }
            }
           echo $this->db->affected_rows();

          // echo json_encode(['result'=>0,'message'=>'Data Dose\'nt Update']);die;
            
      }
      else
      {
        $this->load->view('super_admin/home_page_content');

      }



      
    }


    public function get_page_content()
    {
     
      // $this->Common_mdl->document_function();
      $this->Loginchk_model->superAdmin_LoginCheck();
     // $this->load->helper(array("comman_helper"));
       $data['page_id']=$_POST['page_id'];
       $datas['holdpage']=$this->load->view('super_admin/page_content_view',$data,true);
    //   print_r($datas);exit;
       echo json_encode($datas);
    }





      public function mail_setting()
    {
     
      // $this->Common_mdl->document_function();
      $this->Loginchk_model->superAdmin_LoginCheck();
       $data['admin_set']  = $this->db->query('select * from admin_setting where firm_id='.$_SESSION['firm_id'])->row_array();
      $data['smtp_settings']  = $this->Common_mdl->select_record( 'mail_protocol_settings' , 'firm_id' , $_SESSION['firm_id']);         
        $this->load->view('super_admin/mail_setting',$data);
    }

    public function update_firm_admin_settings(){

       if($this->input->post('submit') == 'submit'){

            $data['header_footer_background_color'] = $this->input->post( 'background_color' );
            $data['header1']                = $this->input->post( 'header1' );
            $data['footer1']                = $this->input->post( 'footer1' );
            
            $this->db->update( 'admin_setting' , $data , 'firm_id='.$_SESSION['firm_id'] );

            $mail_settings[ 'smtp_server' ]     = $this->input->post( 'smtp_server' );
            $mail_settings[ 'smtp_email'  ]     = $this->input->post( 'smtp_email' );
            $mail_settings[ 'smtp_from_name' ]  = $this->input->post( 'smtp_from_name' );
            $mail_settings[ 'smtp_security' ]   = $this->input->post( 'smtp_security' );
            $mail_settings[ 'smtp_port' ]       = $this->input->post( 'smtp_port' );
            $mail_settings[ 'smtp_username' ]   = $this->input->post( 'smtp_username' );
            $mail_settings[ 'smtp_password' ]   = $this->input->post( 'smtp_password' );

            $Is_exists = $this->db->get_where( 'mail_protocol_settings' , 'firm_id='.$_SESSION['firm_id'] )
                                  ->num_rows();
            if( !$Is_exists )
            {   
                $mail_settings['firm_id'] = $_SESSION['firm_id'];
                
                $this->db->insert('mail_protocol_settings' , $mail_settings );
            }
            else
            {
                $this->db->update('mail_protocol_settings' , $mail_settings , 'firm_id='.$_SESSION['firm_id'] );
            }

           $res['response'] =1;
      echo json_encode( $res );
     }
    }


/////////////////////////////////END SATZ/////////////////////////////////////



}	