<?php

class Home extends CI_Controller 
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('Common_mdl','Firm_setting_mdl'));    
	}

	public function index()
	{ 
		$data['plans'] = $this->Common_mdl->GetAllWithWhere('subscription_plan','status','1');
		$data['currency'] = $this->Common_mdl->get_price('super_admin_details','id','1','currency');
		$this->load->view('home/index',$data);
	}

	public function signup()
	{
        //$data['countries'] = $this->Common_mdl->getallrecords('countries');
        $data['currency'] = $this->Common_mdl->get_price('super_admin_details','id','1','currency');
        $data['plans'] = $this->Common_mdl->GetAllWithWhere('subscription_plan','status','1');

		$this->load->view('home/signup_form',$data);
		unset($_SESSION['error_msg']);
		unset($_SESSION['success_msg']);
	}

	public function check_email()
	{ 
		$firm_email = $this->db->query('SELECT count(*) as count FROM firm WHERE firm_mailid LIKE "%'.$_POST['email'].'%" AND is_deleted = "0"')->row_array();
		
		if($firm_email['count'] == '0')
		{
           $user = $this->db->query('SELECT count(*) as count FROM user WHERE crm_email_id LIKE "%'.$_POST['email'].'%" OR username LIKE "%'.$_POST['email'].'%"')->row_array();
           echo $user['count'];
		}
		else
		{
		   echo $firm_email['count'];
		}
	}

	public function pricing()
	{
		$data['plans'] = $this->Common_mdl->GetAllWithWhere('subscription_plan','status','1');
		$data['currency'] = $this->Common_mdl->get_price('super_admin_details','id','1','currency');
		$this->load->view('home/pricing',$data);
	}

	public function features()
	{
		$this->load->view('home/features');
	}

	public function about()
	{
        $this->load->view('home/about');
	}

	public function faq()
	{
		$this->load->view('home/faq');
	}

	public function privacy_policy()
	{
		$this->load->view('home/privacy');
	}

	public function terms()
	{
		$this->load->view('home/terms');
	}

	public function get_plan_details()
	{
		$data = $this->Common_mdl->GetAllWithWhere('subscription_plan','id',$_POST['id']);

		echo json_encode($data[0]);
	}

	public function checkout()
	{
		if(isset($_POST) && count($_POST)>0)
		{ 
			$_SESSION['checkout_data'] = $_POST;

			$plan = $this->Common_mdl->GetAllWithWhere('subscription_plan','id',$_POST['plan']);
			$keys = json_decode($this->Common_mdl->get_price('firm','firm_id','0','payment_details'));
			$data['currency'] = $this->Common_mdl->get_price('super_admin_details','id','1','currency');

			$data['secret_key'] = $keys->secret_key;
			$data['publish_key'] = $keys->publish_key;
			$data['stripe_failure_url'] = base_url().'home/failed'; 
			$data['stripe_success_url'] = base_url().'home/success';

			$data['plan'] = $plan[0];
	        
	        $input = array("email" => $_POST['email']);

	        $check_customer = $this->Common_mdl->Curl_Call('https://api.stripe.com/v1/customers?email='.$_POST['email'].'','',$data['secret_key'],'GET');
						
			if(gettype($check_customer) == 'string')
			{
			   $get_customer = json_decode($check_customer); 
			
			   if(count($get_customer->data)>0 && isset($get_customer->data[0]->id))
			   {
			     $customer_id = $get_customer->data[0]->id;	 
			   }
			   else
			   {  
			   	  $customer = $this->Common_mdl->Curl_Call('https://api.stripe.com/v1/customers',$input,$data['secret_key'],'POST');

			   	  if(gettype($customer) == 'string')
			   	  {
			   	     $customer = json_decode($customer); 
			   	     
			   	     if(isset($customer->id))
			   	     {
			   	        $customer_id = $customer->id;	
			   	     }
			   	  }
			   	  else 
			   	  {
			   	     $_SESSION['error_msg'] = 'Error Occurred. Try Later.';
			   	     redirect('/home/signup');
			   	  }     
			   } 
			}
			else if(gettype($check_customer) == 'array')
			{
			   $_SESSION['error_msg'] = $check_customer['error'];
			   redirect('/home/signup');
			}

			if(isset($customer_id))
			{
				$_SESSION['checkout_data']['customer_id'] = $customer_id;
                $_SESSION['checkout_data']['currency'] = $data['currency'];

                if($plan[0]['trial'] == '1')
                {
                   $amount = $_POST['amount'];
                }
                else
                {
                   $client_amount = 0;
                   $user_amount = 0;

                   if(isset($_POST['no_of_clients']) && $_POST['no_of_clients']>0)
                   {
                      $client_amount = $_POST['no_of_clients']*$plan[0]['cost_per_client'];
                   }

                   if(isset($_POST['no_of_users']) && $_POST['no_of_users']>0)
                   {
                      $user_amount = $_POST['no_of_users']*$plan[0]['cost_per_user'];
                   }
                  
                   $amount = ($_POST['amount']+$client_amount+$user_amount+$plan[0]['tax']) - $plan[0]['discount'];
                }                
				
				$data['plan']['plan_amount'] = $amount;

				$input = array(
					"amount" => ($amount*100),
					"currency" => $data['currency'],
					"receipt_email" => $_POST['email'],
					"customer" => $customer_id,
					"description" => 'Subscribing The Plan '.$plan[0]['plan_name'].'',
					"setup_future_usage" => 'off_session',
					'confirmation_method' => 'automatic'	
				); 

			   	$intent = $this->Common_mdl->Curl_Call('https://api.stripe.com/v1/payment_intents',$input,$data['secret_key'],'POST');
 
			   	if(gettype($intent) == 'string')
			   	{
			   	   $data['intent'] = json_decode($intent);
			   	}
			   	else 
			   	{
			   	   $_SESSION['error_msg'] = 'Error Occurred. Try Later.';
			   	   redirect('/home/signup');
			   	}
			}  
	             
			$this->load->view('home/checkout',$data);
	    } 
	}

	public function success()
	{       
       if(isset($_GET['token']) && !empty($_GET['token']) && isset($_GET['card']) && !empty($_GET['card']) && isset($_GET['paymentIntent']) && count($_GET['paymentIntent'])>0)
       {     
            $data = $_SESSION['checkout_data'];
            unset($_SESSION['checkout_data']);
            $paymentIntent = $_GET['paymentIntent'];
            
            $plan = $this->Common_mdl->GetAllWithWhere('subscription_plan','id',$data['plan']); 
            $plan_details = json_encode(array('plan_id' => $data['plan'],'plan_amount' => $data['amount'],'currency' => $data['currency']));
            $payment_details = json_encode(array('token' => $_GET['token'],'card_id' => $_GET['card'],'client_secret' => $paymentIntent['client_secret'],'payment_method' => $paymentIntent['payment_method'],'receipt_email' => $paymentIntent['receipt_email'],'customer_id' => $data['customer_id']));  

            $data1 = array();

            if($plan[0]['trial'] == '1')
            {               
               $data1['trial_end_date'] = date('d-m-Y', strtotime('+'.$plan[0]['trial_days'].' days'));
            }
            
            if(isset($data['no_of_clients']) && $data['no_of_clients']>0)
            {
               $no_of_clients_allowed = $data['no_of_clients'];
            }
            else if($plan[0]['unlimited'] == '1')
            {
               $no_of_clients_allowed = 'unlimited';
            }
            else if($plan[0]['limited'] == '1')
            {
               $no_of_clients_allowed = $plan[0]['client_limit'];
            }

            if(isset($data['no_of_users']) && $data['no_of_users']>0)
            {
               $no_of_users_allowed = $data['no_of_users'];
            }
            else if($plan[0]['unlimited'] == '0')
            {
               $no_of_users_allowed = 'unlimited';
            }
            else if($plan[0]['limited'] == '0')
            {
               $no_of_users_allowed = $plan[0]['user_limit'];
            }              
           
            $data1['crm_company_name'] = $data['company_name'];  
            $data1['firm_mailid'] = $data['email'];
            $data1['firm_contact_no'] = $data['phone_numder'];
            $data1['plan_details'] = $plan_details;
            $data1['plan_type'] = $plan[0]['type'];
            $data1['payment_details'] = $payment_details;
            $data1['firm_size'] = $data['company-size'];
            $data1['status'] = 0;
            $data1['created_date'] = time();
            $data1['last_subscribed_date'] = time();            
            $data1['country_code'] = $data['phonecode'];
            $data1['no_of_clients_allowed'] = $no_of_clients_allowed;
            $data1['no_of_users_allowed'] = $no_of_users_allowed;
            
            $this->db->insert("firm",$data1);
            $firm_id = $this->db->insert_id();

            if(!empty($firm_id))
            {
	            $user_data = array("crm_name"=>$data['first_name'], "crm_last_name"=>$data['last_name'],"username"=> $data['email'],"password"=> md5($data['first_name'].$data['last_name']),"confirm_password" => $data['first_name'].$data['last_name'], "crm_email_id" => $data['email'], "crm_phone_number" => $data['phonecode'].$data['phone_numder'],"crm_firm_name"=> $data['company_name'],"crm_profile_pic"=>"825898.jpg","status"=>1,"frozen_freeze"=>1,"company_roles"=>2,"CreatedTime"=>time(),"autosave_status"=>0,'firm_id'=>$firm_id,'user_type' => 'FA' );
	            $this->db->insert("user",$user_data);

	            $user_id = $this->db->insert_id();
                
                if(!empty($user_id))
                {
	              $this->db->update('firm',['user_id'=>$user_id],"firm_id='".$firm_id."'");
	            }
	        }  

	        if(!empty($firm_id) && !empty($user_id))
	        {
	            $this->Firm_setting_mdl->send_welcome_mail($firm_id,true); 
	            $this->Firm_setting_mdl->SetUp_BasicSettings($firm_id);
	            $_SESSION['success_msg'] = '<p style="color:green;">Registered Successfully. Check Your Registered Email For Verifying Your Account.</p>';/*<p style="color:blue;"><a href="'.base_url().'home/Add_Clients/'.$data['plan'].'/'.$firm_id.'">Click here to subscribe allowed number of clients to your firm.</a></p>*/
	            redirect('/home/signup');                
            } 
        } 
	}

	public function failed($client=false)
	{
		$_SESSION['error_msg'] = 'Error Occurred. Try Later.';

		if($client == false)
		{
		   redirect('/home/signup'); 
		}
		else
		{
		   redirect('/home/Add_Clients');
		}
	}

	public function Add_Clients($plan_id,$firm_id)
	{ 
        $data['plans'] = $this->Common_mdl->GetAllWithWhere('subscription_plan','id',$plan_id);
        $firm = $this->Common_mdl->GetAllWithWhere('firm','firm_id',$firm_id);
        $data['currency'] = $this->Common_mdl->get_price('super_admin_details','id','1','currency');
        $data['firm'] = $firm[0];       

		$this->load->view('home/add_client_subscribe',$data);
		unset($_SESSION['error_msg']);
		unset($_SESSION['success_msg']);
	}

	public function payment($firm_id,$plan_id)
	{ 
        if(isset($_POST) && count($_POST)>0)
        {
	        $firm = $this->Common_mdl->GetAllWithWhere('firm','firm_id',$firm_id);
	        $keys = json_decode($this->Common_mdl->get_price('firm','firm_id','0','payment_details'));
	        $_SESSION['checkout_data'] = $_POST;
	        $data['currency'] = $this->Common_mdl->get_price('super_admin_details','id','1','currency');

	        $data['secret_key'] = $keys->secret_key;
	        $data['publish_key'] = $keys->publish_key;
	        $data['stripe_failure_url'] = base_url().'home/failed/1'; 
	        $data['stripe_success_url'] = base_url().'home/add_client_count/'.$firm_id.'/'.$plan_id;
	        
	        $input = array("email" => $firm[0]['firm_mailid']);
	        $check_customer = $this->Common_mdl->Curl_Call('https://api.stripe.com/v1/customers?email='.$firm[0]['firm_mailid'].'','',$data['secret_key'],'GET');
						
			if(gettype($check_customer) == 'string')
			{
			   $get_customer = json_decode($check_customer); 
			
			   if(count($get_customer->data)>0 && isset($get_customer->data[0]->id))
			   {
			     $customer_id = $get_customer->data[0]->id;	 
			   }
			   else
			   {  
			   	  $customer = $this->Common_mdl->Curl_Call('https://api.stripe.com/v1/customers',$input,$data['secret_key'],'POST');

			   	  if(gettype($customer) == 'string')
			   	  {
			   	     $customer = json_decode($customer); 
			   	     
			   	     if(isset($customer->id))
			   	     {
			   	        $customer_id = $customer->id;	
			   	     }
			   	  }
			   	  else 
			   	  {
			   	     $_SESSION['error_msg'] = 'Error Occurred. Try Later.';
			   	     redirect('/home/Add_Clients');
			   	  }     
			   } 
			}
			else if(gettype($check_customer) == 'array')
			{
			   $_SESSION['error_msg'] = $check_customer['error'];
			   redirect('/home/Add_Clients');
			}

			if(isset($customer_id))
			{
				$_SESSION['checkout_data']['customer_id'] = $customer_id; 
				$_SESSION['checkout_data']['currency'] = $data['currency'];
				 
				$plan = $this->Common_mdl->GetAllWithWhere('subscription_plan','id',$plan_id); 
				$data['plan'] = $plan[0];
				$data['plan']['plan_amount'] = $_POST['total_cost'];

				$description = "";

				if(isset($_POST['no_of_clients']) && !empty($_POST['no_of_clients']))
				{
					$description .= $_POST['no_of_clients'].' Client(s) & ';
				}

				if(isset($_POST['no_of_users']) && !empty($_POST['no_of_users']))
				{
					$description .= $_POST['no_of_users'].' User(s)';
				}

				$description = trim($description);
				$description = trim($description,'&');

				$input = array(
					"amount" => ($_POST['total_cost']*100),
					"currency" => $data['currency'],
					"receipt_email" => $firm[0]['firm_mailid'],
					"customer" => $customer_id,
					"description" => 'Subscribing '.$description.' In The Plan '.$plan[0]['plan_name'].'',
					"setup_future_usage" => 'off_session',
					'confirmation_method' => 'automatic'	
				); 

			   	$intent = $this->Common_mdl->Curl_Call('https://api.stripe.com/v1/payment_intents',$input,$data['secret_key'],'POST');

			   	if(gettype($intent) == 'string')
			   	{
			   	   $data['intent'] = json_decode($intent);
			   	}
			   	else 
			   	{
			   	   $_SESSION['error_msg'] = 'Error Occurred. Try Later.';
			   	   redirect('/home/Add_Clients');
			   	}
			}

	        $this->load->view('home/checkout',$data);	
	    }
	}

	public function add_client_count($firm_id,$plan_id)
	{
		if(isset($_GET['token']) && !empty($_GET['token']) && isset($_GET['card']) && !empty($_GET['card']) && isset($_GET['paymentIntent']) && count($_GET['paymentIntent'])>0)
		{     
		     $data = $_SESSION['checkout_data'];
		     unset($_SESSION['checkout_data']);
		     $paymentIntent = $_GET['paymentIntent'];		     
		     
		     $payment_details = json_encode(array('token' => $_GET['token'],'card_id' => $_GET['card'],'client_secret' => $paymentIntent['client_secret'],'payment_method' => $paymentIntent['payment_method'],'receipt_email' => $paymentIntent['receipt_email'],'customer_id' => $data['customer_id']));  
		     
		     $firm = $this->Common_mdl->GetAllWithWhere('firm','firm_id',$firm_id);		
		     $plan = $this->Common_mdl->GetAllWithWhere('subscription_plan','id',$plan_id);
		     $data_arr = array();

		     if(isset($data['no_of_clients']) && !empty($data['no_of_clients']))
		     {
		        if(!empty($firm[0]['no_of_clients_allowed']))
		        {
		           $no_of_clients_allowed = $firm[0]['no_of_clients_allowed'] + $data['no_of_clients'];                      
		        }
		        else
		        {
		           $no_of_clients_allowed = $data['no_of_clients'];
		        }

		        $data_arr['no_of_clients_allowed'] = $no_of_clients_allowed;
		     }
		     else
		     {
		     	if(empty($firm[0]['no_of_clients_allowed']))
		     	{
		     	   if($plan[0]['unlimited'] == '1')
		     	   {
		     	   	  $data_arr['no_of_clients_allowed'] = 'unlimited';
		     	   }
                   else if($plan[0]['limited'] == '1')
                   {
                      $data_arr['no_of_clients_allowed'] = $plan[0]['client_limit'];
                   }		     	                  
		     	}
		     }

		     if(isset($data['no_of_users']) && !empty($data['no_of_users']))
		     {
		        if(!empty($firm[0]['no_of_users_allowed']))
		        {
		           $no_of_users_allowed = $firm[0]['no_of_users_allowed'] + $data['no_of_users']; 
		        }
		        else
		        {
		           $no_of_users_allowed = $data['no_of_users'];
		        }

		        $data_arr['no_of_users_allowed'] = $no_of_users_allowed;
		     }
		     else
		     {
		     	if(empty($firm[0]['no_of_users_allowed']))
		     	{
		     	   if($plan[0]['unlimited'] == '0')
		     	   {
		     	   	  $data_arr['no_of_users_allowed'] = 'unlimited';
		     	   }
                   else if($plan[0]['limited'] == '0')
                   {
                      $data_arr['no_of_users_allowed'] = $plan[0]['user_limit'];
                   }		     	                  
		     	}
		     }
		     
		     $data_arr['payment_details'] = $payment_details;

		     $this->db->update('firm',$data_arr,'firm_id = "'.$firm_id.'"'); 
		      
		     $_SESSION['success_msg'] = 'Subscribed Successfully.';
		     redirect('/home/Add_Clients/'.$plan_id.'/'.$firm_id.'');		      
		}  
	}

	public function client_subscription($firm_id,$plan_id)
	{
		if(count($_POST)>0 && isset($_POST['total_cost']) && $_POST['total_cost']>0 && !empty($firm_id) && !empty($plan_id))
		{
			$plan = $this->Common_mdl->GetAllWithWhere('subscription_plan','id',$plan_id);
            $firm = $this->Common_mdl->GetAllWithWhere('firm','firm_id',$firm_id);
            $keys = json_decode($this->Common_mdl->get_price('firm','firm_id','0','payment_details'));
            $currency = $this->Common_mdl->get_price('super_admin_details','id','1','currency');

            $data['secret_key'] = $keys->secret_key;
            $data['publish_key'] = $keys->publish_key;
			
			$payment_details = json_decode($firm[0]['payment_details']);

			$input = array(
				"amount" => ($_POST['total_cost']*100),
				"currency" => $currency,
				"customer" => $payment_details->customer_id,
				"receipt_email" => $payment_details->receipt_email,
				"description" => 'Charging For The Plan '.$plan[0]['plan_name'].''
			); 

			$charge_obj = $this->Common_mdl->Curl_Call('https://api.stripe.com/v1/charges',$input,$keys->secret_key,'POST');

			if(gettype($charge_obj) == 'string')
			{
			   $charge = json_decode($charge_obj);

               $charge_data = json_encode(array('charge_id' => $charge->id,'txn_id' => $charge->balance_transaction,'receipt_url' => $charge->receipt_url));
			   $payment = array('firm_id' => $firm_id,'payment_data' => $charge_data);
			   
			   $this->db->insert('firm_payments',$payment);

			   if($charge->status == 'succeeded')
			   {   
                  $data_arr = array();

                  if(isset($_POST['no_of_clients']) && !empty($_POST['no_of_clients']))
                  {
                     if(!empty($firm[0]['no_of_clients_allowed']))
                     {
                        $no_of_clients_allowed = $firm[0]['no_of_clients_allowed'] + $_POST['no_of_clients'];                      
                     }
                     else
                     {
                        $no_of_clients_allowed = $_POST['no_of_clients'];
                     }

                     $data_arr['no_of_clients_allowed'] = $no_of_clients_allowed;
                  }

                  if(isset($_POST['no_of_users']) && !empty($_POST['no_of_users']))
                  {
                     if(!empty($firm[0]['no_of_users_allowed']))
                     {
                        $no_of_users_allowed = $firm[0]['no_of_users_allowed'] + $_POST['no_of_users']; 
                     }
                     else
                     {
                        $no_of_users_allowed = $_POST['no_of_users'];
                     }

                     $data_arr['no_of_users_allowed'] = $no_of_users_allowed;
                  }

                  $data_arr['recent_charge_data'] = $charge_data;

                  $this->db->update('firm',$data_arr,'firm_id = "'.$firm_id.'"');

                  $_SESSION['success_msg'] = 'Subscribed Successfully.';
                  redirect('/home/Add_Clients/'.$plan_id.'/'.$firm_id.'');
               }
               else
               {
                  $_SESSION['error_msg'] = 'Payment Process Is Unsuccessfull. Contact Admin, If Your Card Has Been Debited.';
                  redirect('/home/Add_Clients/'.$plan_id.'/'.$firm_id.'');
               }
            }
            else if(gettype($charge_obj) == 'array')
            {
                $_SESSION['error_msg'] = 'Payment Process Is Unsuccessfull.';
                redirect('/home/Add_Clients/'.$plan_id.'/'.$firm_id.'');
            }
		}
		else
		{
			$_SESSION['error_msg'] = 'Invalid Request.';
			redirect('/home/Add_Clients/'.$plan_id.'/'.$firm_id.'');
		}
	}

	public function UpdateFirmSubscription()
	{
		$today = date('d-m-Y'); 
		$firm = $this->db->query('SELECT * FROM firm WHERE firm_id != "0" AND is_deleted = "0" AND status = "1" ORDER BY firm_id ASC')->result_array();
		$keys = json_decode($this->Common_mdl->get_price('firm','firm_id','0','payment_details'));
		$currency = $this->Common_mdl->get_price('super_admin_details','id','1','currency');
		
		foreach($firm as $key => $value) 
		{ 
			if($value['plan_type'] == '1' || $value['plan_type'] == '2')
			{
				if(!empty($value['trial_end_date']))
                {
                   $subscription_plan_end_date = $value['trial_end_date'];
                }
				else if($value['plan_type'] == '1')
				{
                   $subscription_plan_end_date = date('d-m-Y', strtotime('+1 month',$value['last_subscribed_date']));
				}
				else if($value['plan_type'] == '2')
				{
                   $subscription_plan_end_date = date('d-m-Y', strtotime('+365 days',$value['last_subscribed_date']));
				}

				if($subscription_plan_end_date == $today)
				{
                    $this->db->update('user',['status' => '4'],'id = "'.$value['user_id'].'"');
                    $this->db->update('firm',['trial_end_date' => ''],'firm_id = "'.$value['firm_id'].'"');

                    if(!empty($value['plan_details']) && !empty($value['payment_details']))
                    {
	                    $plan_details = json_decode($value['plan_details']);
	                    $payment_details = json_decode($value['payment_details']);

	                    $plan = $this->Common_mdl->GetAllWithWhere('subscription_plan','id',$plan_details->plan_id);

	                    $input = array(
	                    	"amount" => ($plan[0]['plan_amount']*100),
	                    	"currency" => $currency,
	                    	"customer" => $payment_details->customer_id,
	                    	"receipt_email" => $payment_details->receipt_email,
	                    	"description" => 'Charging For The Plan '.$plan[0]['plan_name'].''
	                    ); 

	                    $charge_obj = $this->Common_mdl->Curl_Call('https://api.stripe.com/v1/charges',$input,$keys->secret_key,'POST');

	                    if(gettype($charge_obj) == 'string')
	                    {
	                       $charge = json_decode($charge_obj);

	                       $charge_data = json_encode(array('charge_id' => $charge->id,'txn_id' => $charge->balance_transaction,'receipt_url' => $charge->receipt_url));
	                       $payment = array('firm_id' => $value['firm_id'],'payment_data' => $charge_data);

	                       $this->db->insert('firm_payments',$payment);

	                       if($charge->status == 'succeeded')
	                       {                   	  
	                       	  $this->db->update('user',['status' => '1'],'id = "'.$value['user_id'].'"');
	                       	  $this->db->update('firm',['last_subscribed_date' => time(), 'recent_charge_data' => $charge_data],'firm_id = "'.$value['firm_id'].'"');   

	                       	  $this->Send_FirmSubscription_UpdateEmail($value,'success',$plan[0],$payment_details->receipt_email,$currency);
	                       }
	                       else
	                       {
	                          $this->Send_FirmSubscription_UpdateEmail($value,'charging_fail',$plan[0],$payment_details->receipt_email,$currency);
	                       }
	                    }
	                    else if(gettype($charge_obj) == 'array')
	                    {
	                        $this->Send_FirmSubscription_UpdateEmail($value,'fail',$plan[0],$payment_details->receipt_email);
	                    }
	                }
	                else
	                {
	                	$this->Send_FirmSubscription_UpdateEmail($value,'deactivated');
	                }
				}
			}
		}
	}

	public function Send_FirmSubscription_UpdateEmail($firm_data,$status,$plan=false,$email=false,$currency=false)
	{
        $this->load->helper(['comman']);
        $super_admin_details = $this->Common_mdl->GetAllWithWhere('super_admin_details','id','1');

		$mail_content = '<h2>Hi '.ucwords($firm_data['crm_company_name']).',</h2><p>This is regarding your account subscription with us.</p>';

		if($status == 'success')
		{
           $mail_content .= '<p>You have been charged '.$plan['plan_amount'].' '.$currency.' for the '.ucwords($plan['plan_name']).' subscription plan.</p>';
		}
		else if($status == 'charging_fail')
		{
           $mail_content .= '<p>Charging '.$plan['plan_amount'].' '.$currency.' for the '.ucwords($plan['plan_name']).' subscription plan failed.</p><p>Hence, your account with us has been deactivated.</p><p>If, your card has been debited, contact admin.</p>';
		}
		else if($status == 'fail')
		{
			$mail_content .= '<p>Charging for the '.ucwords($plan['plan_name']).' subscription plan failed.</p><p>Hence, your account with us has been deactivated.</p><p>For further queries, contact admin.</p>';
		}
		else if($status == 'deactivated')
		{
			$mail_content .= '<p>Your account has been deactivated as the subscription duration ends.</p><p>For further queries, contact admin.</p>';
		}

		$mail_content .= '<p>Thanks,</p><p>Have a nice day!</p>';

		$_SESSION['firm_id'] = $firm_data['firm_id'];
		$body = html_entity_decode(firm_mail_header()).$mail_content.html_entity_decode(firm_mail_footer());
        unset($_SESSION['firm_id']);

        if($email == false)
        {
           $email = $firm_data['firm_mailid'];
        }

        $this->load->library('email');
        $this->email->set_mailtype("html");
        $this->email->from($super_admin_details[0]['email_id']);
        $this->email->to($email);
        $this->email->subject('Subscription Plan Update');
        $this->email->message($body);
        $this->email->send();   

        return true;
	}	

	
}

?>