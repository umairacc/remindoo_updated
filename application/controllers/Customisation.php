<?php

class Customisation extends CI_Controller 
{
        public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
		public function __construct()
		{
			parent::__construct();
			$this->load->model(array('Common_mdl','Report_model','Security_model'));
			$this->load->library('Excel');
			$this->load->helper('comman');
		}


		public function client_custom()
		{			
			if(empty($_POST['company_type']))
			{
				$company_type = "";
			}
			else
			{
				$company = implode(',',$_POST['company_type']);
				$company_type = 'AND FIND_IN_SET(client.crm_legal_form,"'.$company.'")';
			}

			if(empty($_POST['status'])) 
			{				
				$status = "";
			}
			else
			{
				$poststatus = implode(',',$_POST['status']);
				$status = 'AND FIND_IN_SET(user.status,"'.$poststatus.'")';
			}
		

			if(!empty($_POST['filter']) && $_POST['filter'] != 'all')
			{					
				if($_POST['filter']=='week')
				{
					$current_date = date('Y-m-d');
					$week = date('W', strtotime($current_date));
					$year = date('Y', strtotime($current_date));

					$dto = new DateTime();
					$start_date = $dto->setISODate($year, $week, 0)->format('Y-m-d');
					$end_date = $dto->setISODate($year, $week, 6)->format('Y-m-d');
					$end_date = date('Y-m-d', strtotime($end_date . ' +1 day')); 
				}
                else if($_POST['filter']=='three_week')
                {
					$current_date = date('Y-m-d');
					$weeks = strtotime('- 3 week', strtotime( $current_date ));
					$start_date = date('Y-m-d', $weeks); 
					$addweeks = strtotime('+ 3 week', strtotime( $start_date ));
					$end_Week = date('Y-m-d', $addweeks); 
					$end_date = date('Y-m-d', strtotime($end_Week . ' +1 day'));
				}

				else if($_POST['filter']=='month')
				{
					$start_date = date("Y-m-01");
					$month_end = date("Y-m-t"); 
					$end_date = date('Y-m-d', strtotime($month_end . ' +1 day'));
				}

				if($_POST['filter']=='Three_month')
				{
					$end_date = date('Y-m-d',strtotime(date('Y-m-d').'+1 day'));
					$start_date = date('Y-m-d',strtotime('- 3 month', strtotime($end_date)));
				}	

				$filter = 'AND from_unixtime(client.created_date) BETWEEN "'.$start_date.'" AND "'.$end_date.'"';	
		    }
		    else if(!empty($_POST['from_date']) && !empty($_POST['to_date']))
			{
				$filter = 'AND from_unixtime(client.created_date) BETWEEN "'.$_POST['from_date'].'" AND "'.$_POST['to_date'].'"';
			}
		    else
		    {
		    	$filter = '';
		    }			

			if(!empty($_POST['client']))
			{
                $assigned_clients = $_POST['client'];
			}
			else
			{
				$Assigned_Client = Get_Assigned_Datas('CLIENT');
				$assigned_clients = implode(',', $Assigned_Client);
			}	
			
			$data['records'] = $this->db->query('SELECT client.* FROM client LEFT JOIN user ON client.user_id = user.id WHERE client.autosave_status=0 AND FIND_IN_SET(client.id,"'.$assigned_clients.'") AND client.firm_id = "'.$_SESSION['firm_id'].'" AND client.crm_company_name != "" '.$company_type.' '.$status.' '.$filter.' ORDER BY id ASC')->result_array();
				
			$this->load->view('reports/client_customresult',$data);
	   }

}

?>
