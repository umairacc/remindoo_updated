<?php
// error_reporting(E_STRICT);
defined('BASEPATH') OR exit('No direct script access allowed');

class Proposal_pdf extends CI_Controller {
public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct(){
        parent::__construct();
        $this->load->model(array('Common_mdl','Security_model','Proposal_model'));
        $this->load->helper(['comman']);
    }

    public function index()
    {
       
    }
     public function pdf_download(){

     $data['records']['proposal_contents']=$_POST['body'];
     $p_no=$_POST['proposal_no'];
       //echo $id; 
       // $data['records']=$this->Common_mdl->select_record('proposals','id',$id); 
       $user_id=$this->session->userdata['userId'];         
       $data['signature']=$this->db->query('select * from pdf_signatures where proposal_no='.$p_no.' Order By id Desc Limit 1')->row_array();
       $proposal_name=$_POST['proposal_name'];
       $email_signature=$_POST['email_signature'];

       if($_POST['company_name']!='')
          {          
             if(strpos($_POST['company_name'], 'Leads_') !== false) 
             {
               $lead = explode('_',$_POST['company_name']);
               $lead_id = $lead[1];
               $user_name=$_POST['receiver_mail_id'];
               $client_name= $_POST['receiver_company_name'];
             }
             else
             {
               $details=$this->db->query("SELECT * FROM `user` where `id` = '".$_POST['company_name']."'")->row_array();
               $client_name=$details['crm_name'];        
               $email=$details['crm_email_id'];         
               $last_name=$details['crm_last_name']; 
               $country=$details['crm_country'];
               $city=$details['crm_city'];
               $religion=$details['crm_state'];
               
               $client_det = $this->db->query("SELECT * FROM `client` where `user_id` = '".$_POST['company_name']."'")->row_array();

               $client_website=$client_det['crm_company_url'];
               $client_mobile_number=$client_det['crm_mobile_number'];
               $lead_id = 0;
            }
          }
          
          $user_details= $this->db->query("SELECT * FROM `user` where `id` = '".$_SESSION['id']."'")->row_array();       
          $account_name=$user_details['crm_name'];

          $sender_address = "";

          if(!empty($user_details['crm_street1']))
          { 
             $sender_address .='<p>'.$user_details['crm_street1'].'</p>';
          }   
          if(!empty($user_details['crm_street2']))
          { 
             $sender_address .='<p>'.$user_details['crm_street2'].'</p>';
          } 
          if($user_details['crm_country']!=0)
          { 
             $sender_address .='<p>'.$user_details['crm_country'].'</p>';
          } 
          if($user_details['crm_state']!=0)
          { 
             $sender_address .='<p>'.$user_details['crm_state'].'</p>';
          } 
          if($user_details['crm_city']!=0)
          { 
             $sender_address .='<p>'.$user_details['crm_city'].'</p>';
          } 
          if($user_details['crm_postal_code']!=0)
          { 
             $sender_address .='<p>'.$user_details['crm_postal_code'].'</p>';
          } 

            if($email_signature == 'true')
          {
             $replace_data = '<div class="get-options choose ui-draggable ui-draggable-handle" data-id="e_signature"><img src="'.base_url().$data['signature']['signature_path'].'" style="width:200px;display: block;"></div>';
             $sign_img='<img src="'.base_url().$data['signature']['signature_path'].'" style="width:100px;display: block;">';
          }
          else
          {
             $replace_data = '<div class="get-options choose ui-draggable ui-draggable-handle" data-id="e_signature"></div>';

              $sign_img="";
          }


           $body1=$_POST['body'];
          $random_string=$this->generateRandomString();
          $a1  =   array('::Username::'=>$user_name,
            '::Client Name::'=>$client_name,
            '::Client Company::'=>$client_name,
            '::Client Company City::'=>$city,
            '::Client Company Country::'=>$country,
            '::Client Company Religion::'=>$religion,
            '::Client First Name::'=>$client_name,
            '::Client Last Name::'=>$last_name,
            ':: ClientPhoneno::'=> $client_mobile_number,
            ':: ClientWebsite::'=>$client_website,
            ':: Proposal Link ::'=>'',
            ':: ProposalCurrency::'=>$_POST['currency_symbol'],
            ':: ProposalExpirationDate::'=>$expiration_date,
            ':: ProposalName::'=>$_POST['proposal_name'],
            '::SenderCompany::'=>$_POST['sender_company'],
            '::SenderCompanyAddress::'=>$sender_address,
            '::SenderCompanyCity::'=>$user_details['crm_city'],
            '::SenderCompanyCountry::'=>$user_details['crm_country'],
            '::SenderCompanyReligion::'=>$user_details['crm_state'],
            '::SenderEmail::'=>$_POST['sender_mail_id'],
            '::SenderEmailSignature::'=> $sign_img,
            ':: Sender Name::'=>$account_name,
            '::senderPhoneno::'=>$user_details['crm_phone_number'],
            ':: Date ::'=>date("Y-m-d h:i:s"));  

          $proposal_content_new  =   strtr($body1,$a1);  

        

          $doc = new DOMDocument();      
          $doc->loadHTML(preg_replace("/\s\s+/", "", $proposal_content_new));                
          $xpath = new DOMXPath($doc);      
          $elementsThatHaveData_id = $xpath->query('//*[@data-id="e_signature"]');     

          foreach($elementsThatHaveData_id as $value) 
          {   
             $string = $doc->saveHTML($value);
             $string = preg_replace("/>\s+/", ">", $string);
           
             $proposal_content_new = str_replace($string,$replace_data,preg_replace("/\s\s+/", "", $proposal_content_new)); 
          } 
          
          $data['records']['pdf_content']=$proposal_content_new;

         // echo $proposal_content_new;exit;

        // $this->load->library('Tc');
        // $pdf = new Tc('P', 'mm', 'A4', true, 'UTF-8', false); 
        // $pdf->AddPage('L','A4');
        // $html = $this->load->view('proposal/test_pdf',$data,true);    
        $footer_logo_html=base_url().'uploads/doc_signs/15904e140653b540bf1f247e49140ae4.png';
        // $pdf->setPrintHeader(true);
        // $pdf->SetTitle($proposal_name);
        // $pdf->SetHeaderMargin(30);
        // $pdf->SetTopMargin(20);
        // $pdf->setFooterMargin(20);
        // $pdf->SetAutoPageBreak(true, 20);
        // $pdf->SetAuthor('Author');
        // $pdf->SetDisplayMode('real', 'default');
        // $pdf->WriteHTML($html);
        // $dir = 'uploads/proposal_pdf/';
        // $filename=$proposal_name.'.pdf';
        // $pdf->Image('http://remindoo.org/CRMTool/uploads/doc_signs/15904e140653b540bf1f247e49140ae4.png', 180, 60, 30, 30, 'PNG');
        // $pdf->Image( 'http://remindoo.org/CRMTool/uploads/doc_signs/15904e140653b540bf1f247e49140ae4.png', 200, 250 , 30  , 30 , 'PNG' , '#'  , 'T', FALSE  , 300  , 'R' , FALSE  , FALSE , 1 , FALSE , FALSE  , TRUE );
        // // define active area for signature appearance
        // $pdf->setSignatureAppearance(180, 60, 30, 30);
        // // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        // // *** set an empty signature appearance ***
        // $pdf->addEmptySignatureAppearance(180, 80, 30, 30);
       // $pdf->Output(FCPATH . $dir . $filename, 'F');


        


        //general options for pdf
        $this->load->library('Pdf');
        $dompdf = new Pdf();
        $dompdf->set_option('isHtml5ParserEnabled', true);
        $dompdf->set_option('defaultMediaType', 'all');
        $dompdf->set_option('isFontSubsettingEnabled', true);
        $dompdf->set_option('isRemoteEnabled', true);
        $dompdf->set_paper("A4");
        // load the html content
        $html= $this->load->view('proposal/test_pdf', $data, true);

      //  echo $html;exit;
        $dompdf->load_html($html);
        $dompdf->render();
     //   $dompdf->stream("dompdf_out.pdf", array("Attachment" => false));exit;
        // exit;
        $output = $dompdf->output();
        $dir = 'uploads/proposal_pdf/';
        $timestamp = date("Y-m-d_H:i:s");
         $filename=$proposal_name.'_'.$timestamp.'.pdf';       
        //  $pdf->Output(FCPATH . $dir . $filename, 'F');
        file_put_contents(FCPATH . $dir . $filename, $output);

        $this->load->helper('url');
        echo json_encode(array(
          'path' => FCPATH . $dir . $filename,
          'url'  => base_url( $dir . $filename )
        ));
    }

    public function access_key(){
      $access_key=$_POST['password'];
      $id=$_POST['id'];
      $check_result=$this->Common_mdl->select_record('proposals','id',$id);
      $pdf_password=$check_result['pdf_password'];
      if($pdf_password==$access_key){
        $data=1;
      }else{
        $data=0;
      }
      echo json_encode($data);
    }

    public function proposal_expires(){
        $today=date("d-m-Y");
        $expired_date=$this->Common_mdl->getallrecords('proposals');
        foreach($expired_date as $expire){
            $expiration=$expire['expiration'];
            if($expiration=='on'){
                $expiration_date=$expire['expiration_date'];
                $id=$expire['id'];
                if($expiration_date==$today){
                    $data=array('status'=>'expired');
                    $this->Common_mdl->update('proposals',$data,'id',$id);                     
                }
            }
        }
    }

    public function conversation()
    {
      if(isset($_POST['sender_id'])){
        $sender_id=$_POST['sender_id'];
      }else{
        $sender_id='';
      }
      if(isset($_POST['receiver_companyname'])){
        $receiver_companyname=$_POST['receiver_companyname'];
      }else{
        $receiver_companyname='';
      }
      if($_POST['types']!='0'){
        $image_update=array('image'=>$_POST['image_message'],'proposal_id'=>$_POST['proposal_id'],'type'=>$_POST['types']);
        $insert=$this->Common_mdl->insert('proposal_discussion_attachments',$image_update);
      }
      $data=array('sender_id'=>$sender_id,'receiver_companyname'=>$receiver_companyname,'proposal_id'=>$_POST['proposal_id'],'message'=>$_POST['message'],'status'=>'0');
      $insert=$this->Common_mdl->insert('proposal_discussion',$data);
      $data=array('proposal_id'=>$_POST['proposal_id'],'tag'=>'comments','created_at'=>date('Y-m-d h:i:s'));
      $inse=$this->Common_mdl->insert('proposal_history',$data);
      $proposal_details=$this->Common_mdl->select_record('proposals','id',$_POST['proposal_id']); 
      $receiver_company_name=$proposal_details['company_name']; 
        if($sender_id!=''){
          $proposal_details=$this->Common_mdl->select_record('proposals','id',$_POST['proposal_id']); 

           $receiver_company_name=$proposal_details['receiver_company_name'];
           $first_name=$proposal_details['receiver_company_name'];
           $email=$proposal_details['receiver_mail_id'];
           $first_name='';   
           $sender_company=$proposal_details['sender_company_name'];


          if($proposal_details['company_id']==1){
                $user_details=$this->Common_mdl->select_record('leads','id',$proposal_details['lead_id']);
                $client_website=$user_details['website'];
                $client_mobile_number=$user_details['phone'];
                $email=$user_details['email_address']; 
                $name=$user_details['name']; 
                $last_name=$user_details['name']; 
                $country='';
                $city=$user_details['city'];
                $religion=$user_details['state'];             
             }else{
               $email=$proposal_details['receiver_mail_id'];
                $user_details=$this->Common_mdl->select_record('user','id',$proposal_details['company_id']);
                $client_details=$this->Common_mdl->select_record('client','user_id',$proposal_details['company_id']);
                $client_website=$client_details['crm_company_url'];
                $client_mobile_number=$client_details['crm_mobile_number'];
                $name=$user_details['crm_name']; 
                $last_name=$user_details['crm_last_name']; 
                $country=$user_details['crm_country'];
                $city=$user_details['crm_city'];
                $religion=$user_details['crm_state'];               
            }

          $senders_details=$this->Common_mdl->select_record('user','id',$proposal_details['user_id']);

          $contact_data=$this->Common_mdl->GetAllWithWheretwo('client_contacts','client_id',$proposal_details['user_id'],'make_primary',1);         

          $sender_name=$senders_details['crm_name']; 
          $sender_last_name=$senders_details['crm_last_name']; 
          $sender_country=$senders_details['crm_country'];
          $sender_city=$senders_details['crm_city'];
          $sender_religion=$senders_details['crm_state']; 

          $proposal_content=$this->Common_mdl->getTemplates(5);
          $proposal_content= $proposal_content[0];

          $pdf_signature=$this->Common_mdl->select_record('pdf_signatures','proposal_no',$_POST['proposal_id']);

          if($pdf_signature[0]==''){
            $email_signature='';
          }else{
            $email_signature='<img src='.$pdf_signature['signature_path'].'>';
          }

          $sender_mobile_number='';
          $body1=html_entity_decode( $proposal_content['body'] );
          $subject=html_entity_decode($proposal_content['subject']);

          $random_string=$this->generateRandomString();
          $link=base_url().'Proposal/proposal/'.$random_string.'---'.$_POST['proposal_id'];
  
           $a1  =   array('::Client Name::'=>$name,
            '::Client Company::'=>$proposal_details['receiver_company_name'],
            '::Client Company City::'=>$city,
            '::Client Company Country::'=>$country,
            '::Client Company Religion::'=>$religion,
            '::Client First Name::'=>$name,
            '::Client Last Name::'=>$last_name,
            '::ClientPhoneno::'=> $client_mobile_number,
            '::ClientWebsite::'=>$client_website,
            ':: Proposal Link ::'=>$link,
            ':: ProposalCurrency::'=>$proposal_details['currency'],
            ':: ProposalExpirationDate::'=>$proposal_details['expiration_date'],
            ':: ProposalName::'=>$proposal_details['proposal_name'],
            ':: ProposalCodeNumber::'=>$proposal_details['proposal_no'],
            '::SenderCompany::'=>$proposal_details['sender_company_name'],
            '::SenderCompanyAddress::'=>'',
            '::SenderCompanyCity::'=>$sender_city,
            '::SenderCompanyCountry::'=>$sender_country,
            '::SenderCompanyReligion::'=>$sender_religion,
            '::SenderEmail::'=>$proposal_content['from_address'],
            '::SenderEmailSignature::'=>$email_signature,
            '::SenderName::'=>$sender_name,
            '::senderPhoneno::'=>$sender_mobile_number,
            '::ProposalTitle::'=>$proposal_details['sender_company_name'],
            ':: Date ::'=>date("Y-m-d h:i:s"),'::Message::'=>$_POST['message']);
            $proposal_contents  =   strtr($body1,$a1); 
            $subject  =   strtr($subject,$a1);
            $random_string=$this->generateRandomString();
            $body= $proposal_contents;

            $email_subject  = preg_replace('/ {2,}/', ' ', str_replace('&nbsp;', ' ', strip_tags($subject)));
            $send           = firm_settings_send_mail( $senders_details['firm_id'] , $email , $email_subject , $body );

        }else{
              $proposal_details=$this->Common_mdl->select_record('proposals','id',$_POST['proposal_id']);
              $user_id=$proposal_details['user_id'];
              $sender_company=$proposal_details['receiver_company_name'];
              $sender_mobile_number='';     
              $email=$proposal_details['sender_company_mailid'];
              $first_name=$proposal_details['company_name'];

            if($proposal_details['company_id']==1){
                $user_details=$this->Common_mdl->select_record('leads','id',$proposal_details['lead_id']);
                $client_website=$user_details['website'];
                $client_mobile_number=$user_details['phone'];
                $email=$user_details['email_address']; 
                $name=$user_details['name']; 
                $last_name=$user_details['name']; 
                $country='';
                $city=$user_details['city'];
                $religion=$user_details['state'];             
             }else{
                $email=$proposal_details['sender_company_mailid'];
                $user_details=$this->Common_mdl->select_record('user','id',$proposal_details['company_id']);
                $client_details=$this->Common_mdl->select_record('client','user_id',$proposal_details['company_id']);
                $client_website=$client_details['crm_company_url'];
                $client_mobile_number=$client_details['crm_mobile_number'];
                $name=$user_details['crm_name']; 
                $last_name=$user_details['crm_last_name']; 
                $country=$user_details['crm_country'];
                $city=$user_details['crm_city'];
                $religion=$user_details['crm_state'];               
            }
            $contact_data=$this->Common_mdl->GetAllWithWheretwo('client_contacts','client_id',$proposal_details['user_id'],'make_primary',1);
 
            $user_details=$this->Common_mdl->select_record('user','id',$proposal_details['user_id']);
            $senders_details=$this->Common_mdl->select_record('client','user_id',$proposal_details['user_id']);
            $sender_email=$contact_data[0]['main_email']; 
            $sender_name=$senders_details['crm_name']; 
            $sender_last_name=$senders_details['crm_last_name']; 
            $sender_country=$senders_details['crm_country'];
            $sender_city=$senders_details['crm_city'];
            $sender_religion=$senders_details['crm_state']; 

          $proposal_content=$this->Common_mdl->getTemplates(23);
          $proposal_content= $proposal_content[0];

          $pdf_signature=$this->Common_mdl->select_record('pdf_signatures','proposal_no',$_POST['proposal_id']);
          if(count($pdf_signature) > 0){
           
             $email_signature='<img src='.$pdf_signature['signature_path'].'>';
          }else{
            $email_signature='';
          }
          $body1=$proposal_content['body'];
          $subject=html_entity_decode($proposal_content['subject']);

          $random_string=$this->generateRandomString();
          $link=base_url().'Proposal/proposal/'.$random_string.'---'.$_POST['proposal_id'];
        //  echo $link;
           $a1  =   array('::Client Name::'=>$name,
            '::Client Company::'=>$proposal_details['receiver_company_name'],
            '::Client Company City::'=>$city,
            '::Client Company Country::'=>$country,
            '::Client Company Religion::'=>$religion,
            '::Client First Name::'=>$name,
            '::Client Last Name::'=>$last_name,
            '::ClientPhoneno::'=> $client_mobile_number,
            '::ClientWebsite::'=>$client_website,
            ':: Proposal Link ::'=>$link,
            ':: ProposalCurrency::'=>$proposal_details['currency'],
            ':: ProposalExpirationDate::'=>$proposal_details['expiration_date'],
            ':: ProposalName::'=>$proposal_details['proposal_name'],
            ':: ProposalCodeNumber::'=>$proposal_details['proposal_no'],
            '::SenderCompany::'=>$proposal_details['sender_company_name'],
            '::SenderCompanyAddress::'=>'',
            '::SenderCompanyCity::'=>$sender_city,
            '::SenderCompanyCountry::'=>$sender_country,
            '::SenderCompanyReligion::'=>$sender_religion,
            '::SenderEmail::'=>$sender_email,
            '::SenderEmailSignature::'=>$email_signature,
            '::SenderName::'=>$sender_name,
            '::senderPhoneno::'=>$sender_mobile_number,
            ':: Date ::'=>date("Y-m-d h:i:s"),
            '::Message::'=>$_POST['message']);
          $proposal_contents  =   strtr($body1,$a1); 
          $subject  =   strtr($subject,$a1);
          $random_string=$this->generateRandomString();
          $body= $proposal_contents;
              
             

            $email_subject  = preg_replace( '/ {2,}/', ' ', str_replace( '&nbsp;', ' ', strip_tags($subject) ) );
            $send           = firm_settings_send_mail( $user_details['firm_id'] , $email , $email_subject , $body );
        }
            $content=$this->db->query("select * from `proposal_discussion` where proposal_id=".$_POST['proposal_id']." order by id desc")->result_array();          
             $datas['msg_content']='';
            foreach($content as $con){
                if($con['sender_id']!=''){                   
                    $datas['msg_content'].='<div class="else-msgcont blockquote"><strong>'.$sender_company.' Wrote</strong><span>'.$con['message'].'</span></div>';
                }else{
                     $datas['msg_content'].='<div class="else-msgcont blockquote"><strong>'.$receiver_company_name.' Wrote</strong><span>'.$con['message'].'</span></div>';
                }
            }

        echo json_encode($datas);
    }

    public function generateRandomString($length = 100) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }

    public function followup(){
      $body=html_entity_decode(firm_mail_header()).$_POST['body'].html_entity_decode(firm_mail_footer());
      $text=str_ireplace('<p>','',$_POST['subject']);
      $text=str_ireplace('</p>','',$text); 
      $subject=$text;
      $id=$_POST['proposal_id'];
      $proposal_details=$this->Common_mdl->select_record('proposals','id',$id);
      $proposal_name=$proposal_details['proposal_name'];
      $sender_comapany_mail=$proposal_details['sender_company_mailid'];
        $data=array('proposal_id'=>$proposal_details['id'],'tag'=>'followup','created_at'=>date('Y-m-d h:i:s'));
        $inse=$this->Common_mdl->insert('proposal_history',$data);
        $sender_company=$proposal_details['sender_company_name'];
        $client_companyname=$proposal_details['company_name'];

      if($proposal_details['receiver_mail_id']=='')
      {
        $emailid=$this->db->query("SELECT * FROM `client` where `crm_company_name` = '".$client_companyname."'")->row_array();     
        $email=$emailid['crm_email'];
        $client_name=$emailid['crm_first_name'];
  		}
      else
      {
  			$email=$proposal_details['receiver_mail_id'];
  			$client_name=$proposal_details['receiver_mail_id'];
  		}

      $a  =   array('::ClientName::'=>$client_name,':: ProposalName::'=>$proposal_name,);
      $email_body  =   strtr($body,$a); 
      $a1  =   array(':: ProposalName::'=>$proposal_name,'::SenderCompany::'=>$sender_company,':: Proposal Link ::'=>'');
      $email_subject  =   strtr($subject,$a1); 
             
      $email_subject  = preg_replace( '/ {2,}/', ' ', str_replace( '&nbsp;', ' ', strip_tags($email_subject) ) );
      $send           = firm_settings_send_mail( $proposal_details['firm_id'] , $email , $email_subject , $email_body );

      $data['status']='1';
      echo json_encode($data);         
    }

    public function session_set(){
      $id=$_POST['id'];
       $this->session->set_userdata('template_id',$id);
       $data['status']='1'; 
       echo json_encode($data);
    }

    public function check_price_list_name()
    {
       $result = $this->Common_mdl->check_distinct('proposal_pricelist','pricelist_name',$_POST['name'],$_POST['id']);  

       echo json_encode($result);
    }

    public function check_tax_name()
    {
       $result = $this->Common_mdl->check_distinct('proposal_tax','tax_name',$_POST['name'],$_POST['id']);      

       echo json_encode($result);
    }
    
    public function add_pricelist()
    {
      $user_id=$this->session->userdata['userId'];  
      $pricelist_name=$_POST['pricelist_name'];
     
      $created_by=$this->db->query('select crm_company_name from client where firm_id ="'.$_SESSION['firm_id'].'"')->row_array();      
      $company_name=$created_by['crm_company_name'];
      
      if($company_name=='')
      {
        $company_name='Remindoo.org';
      }

      $service_list=implode(',',$_POST['service_list']);
      $product_list=implode(',',$_POST['product_list']);
      $subscription_list=implode(',',$_POST['subscription_list']);
      $service_list1=explode(',',$service_list);
      $product_list1=explode(',',$product_list);
      $subscription_list1=explode(',',$subscription_list);
      $service_name=array();
      $service_price=array();
      $service_unit=array();
      $service_description=array();
      $service_qty=array();
      $tax=array();
      $discount=array();
      $service_optional=array();
      $service_quantity=array();

      for($i=0;$i<count($service_list1);$i++)
      {
        $service_list=$this->Common_mdl->select_record('proposal_service','id',$service_list1[$i]);     
        array_push($service_name,$service_list['service_name']);
        array_push($service_price,$service_list['service_price']);
        array_push($service_unit,$service_list['service_unit']);
        array_push($service_description,$service_list['service_description']);
        array_push($service_qty,$service_list['service_qty']);
        array_push($tax,0);
        array_push($discount,0);
        array_push($service_optional,0);
        array_push($service_quantity,0);
      }

      $product_name=array();
      $product_price=array();     
      $product_description=array();
      $product_qty=array();
      $product_tax=array();
      $product_discount=array();
      $product_optional=array();
      $product_quantity=array();

      for($i=0;$i<count($product_list1);$i++)
      {
        $product_list=$this->Common_mdl->select_record('proposal_products','id',$product_list1[$i]);      
        array_push($product_name,$product_list['product_name']);
        array_push($product_price,$product_list['product_price']);      
        array_push($product_description,$product_list['product_description']);
        array_push($product_qty,0);
        array_push($product_tax,0);
        array_push($product_discount,0);
        array_push($product_optional,0);
        array_push($product_quantity,0);        
      }

      $subscription_name=array();
      $subscription_price=array();     
      $subscription_description=array();
      $subscription_unit=array();
      $subscription_tax=array();
      $subscription_discount=array();
      $subscription_optional=array();
      $subscription_quantity=array();

      for($i=0;$i<count($subscription_list1);$i++)
      {
        $subscription_list=$this->Common_mdl->select_record('proposal_subscriptions','id',$subscription_list1[$i]);
        array_push($subscription_name,$subscription_list['subscription_name']);
        array_push($subscription_price,$subscription_list['subscription_price']);
        array_push($subscription_unit,$subscription_list['subscription_unit']);
        array_push($subscription_description,$subscription_list['subscription_description']);  
        array_push($subscription_tax,0);
        array_push($subscription_discount,0);
        array_push($subscription_optional,0);
        array_push($subscription_quantity,0);
      }

      $service_name= implode(',',$service_name);
      $service_price=implode(',',$service_price);
      $service_unit=implode(',',$service_unit);
      $service_description= implode(',',$service_description);
      $service_qty=implode(',',$service_qty);
      $tax=implode(',',$tax);
      $discount= implode(',',$discount);
      $service_optional=  implode(',',$service_optional);
      $service_quantity= implode(',',$service_quantity);
      $product_name=implode(',',$product_name);
      $product_price=implode(',',$product_price);     
      $product_description=implode(',',$product_description);
      $product_qty=implode(',',$product_qty);
      $product_tax=implode(',', $product_tax);
      $product_discount= implode(',',$product_discount);
      $product_optional=implode(',',$product_optional);
      $product_quantity=implode(',',$product_quantity);
      $subscription_name=  implode(',', $subscription_name);
      $subscription_price=implode(',',$subscription_price);     
      $subscription_description=implode(',',$subscription_description);
      $subscription_unit=implode(',', $subscription_unit);
      $subscription_tax=implode(',', $subscription_tax);
      $subscription_discount= implode(',',$subscription_discount);
      $subscription_optional=implode(',',$subscription_optional);
      $subscription_quantity=implode(',', $subscription_quantity);

      $data = array('service_name'=>$service_name,
                  'service_price'=>$service_price,
                  'service_unit'=>$service_unit,
                  'service_description'=>$service_description,         
                  'service_qty'=>$service_qty,
                  'product_name'=>$product_name,
                  'product_price'=>$product_price,
                  'product_description'=>$product_description,
                  'product_qty'=>$product_qty,
                  'subscription_name'=>$subscription_name,
                  'subscription_price'=>$subscription_price,
                  'subscription_unit'=>$subscription_unit,
                  'subscription_description'=>$subscription_description,
                  'service_optional'=>$service_optional,
                  'service_quantity'=> $service_quantity,
                  'product_optional'=>$product_optional,
                  'product_quantity'=>$product_quantity,
                  'subscription_optional'=> $subscription_optional,
                  'subscription_quantity'=>$subscription_quantity,
                  'tax'=>$tax,
                  'discount'=>$discount,
                  'product_tax'=>$product_tax,
                  'product_discount'=>$product_discount,
                  'subscription_tax'=>$subscription_tax,
                  'subscription_discount'=>$subscription_discount,
                  'pricelist_name'=>$_POST['pricelist_name'],
                  'user_id'=>$_SESSION['id'],
                  'created_by'=>$company_name,
                  'firm_id' => $_SESSION['firm_id']);     

      $insert=$this->Common_mdl->insert('proposal_pricelist',$data);
      $this->session->unset_userdata('tab_name');
      $this->session->set_userdata('tab_name', 'price_tab');
      redirect('Proposal/fee_schedule');
    }

    public function edit_pricelist(){ 
      error_reporting(0);
      $id=$_POST['id'];
      $pricelist_name=$_POST['pricelist_name'];
      $service_name= implode(',',$_POST['pricelist_service_name']);
      $service_price=implode(',',$_POST['service_price']);
      $service_unit=implode(',',$_POST['service_unit']);
      $service_description= implode(',',$_POST['service_description']);
      $service_qty=implode(',',$_POST['service_qty']);
      $product_name=implode(',',$_POST['pricelist_product_name']);
      $product_price=implode(',',$_POST['product_price']);     
      $product_description=implode(',',$_POST['product_description']);
      //$product_qty=implode(',',$_POST['product_qty']);
      $subscription_name=  implode(',', $_POST['pricelist_subscription_name']);
      $subscription_price=implode(',',$_POST['subscription_price']);     
      $subscription_description=implode(',',$_POST['subscription_description']);
      $subscription_unit=implode(',', $_POST['subscription_unit']);

       $data=array('service_name'=>$service_name,
                  'service_price'=>$service_price,
                  'service_unit'=>$service_unit,
                  'service_description'=>$service_description,         
                  'service_qty'=>$service_qty,
                  'product_name'=>$product_name,
                  'product_price'=>$product_price,
                  'product_description'=>$product_description,
                  //'product_qty'=>$product_qty,
                  'subscription_name'=>$subscription_name,
                  'subscription_price'=>$subscription_price,
                  'subscription_unit'=>$subscription_unit,
                  'subscription_description'=>$subscription_description,
                  'service_optional'=>$service_optional,                 
                  'pricelist_name'=>$_POST['pricelist_name']
                 );   

      $update=$this->Common_mdl->update('proposal_pricelist',$data,'id',$id);
       $this->session->unset_userdata('tab_name');
            $this->session->set_userdata('tab_name', 'price_tab');
      redirect('Proposal/fee_schedule');
    }

      public function delete_pricelist(){
            $this->session->unset_userdata('tab_name');
            $this->session->set_userdata('tab_name', 'price_tab');
            $checkbox=$_POST['checkbox'];
            for($i=0;$i<count($checkbox);$i++){           
            $id=$checkbox[$i];  
             $delete=$this->Common_mdl->delete('proposal_pricelist','id',$id);
            }
            $this->session->set_flashdata('success', 'Record Deleted Successfully!'); 
            redirect('Proposal/fee_schedule');
        }
      public function mytemplate()
      {
        $template_title = $_POST['template_title'];
        $template_content = $_POST['template_content'];
        $user_id=$this->session->userdata['userId'];

        $datas=array('template_title'=>$template_title,'template_content'=>$template_content,'firm_id'=>$_SESSION['firm_id'],'status'=>'active','created_at' => date('Y-m-d H:i:s'),'user_id'=>$user_id);
        
        $data=$this->Common_mdl->insert('proposal_template',$datas);
        echo json_encode($data);       
      }


      public function mytemplate_update()
      {
         $template_title = $_POST['template_title'];
         $template_content = $_POST['template_content'];
         $template_id = $_POST['id'];
         $user_id=$this->session->userdata['userId'];

         $rec = $this->Common_mdl->select_record('proposal_template','id',$template_id);

         if($rec['firm_id'] == $_SESSION['firm_id'])
         {
             $datas = array('template_title'=>$template_title,'template_content'=>$template_content,'user_id'=>$user_id,'status'=>'active','updated_at' => date('Y-m-d H:i:s'));
             $data = $this->Common_mdl->update('proposal_template',$datas,'id',$template_id);
         }
         else
         {
             $datas = array('template_title'=>$template_title,'template_content'=>$template_content,'firm_id'=>$_SESSION['firm_id'],'status'=>'active','created_at' => date('Y-m-d H:i:s'),'user_id'=>$user_id,'super_admin_owned' => $template_id);             
             $data = $this->Common_mdl->insert('proposal_template',$datas);
         }

         echo json_encode($data);
      }

      public function Archive_status()
      {        
        if( !empty( $_POST['id'] ) )
        {           
          foreach ($_POST['id'] as $id) 
          {
            $this->db->query("UPDATE proposals set old_status=status,status='archive' where id='".$id."'"); //and old_status!='archive'
          }
          echo 1;
        } 
      }
      public function UnArchive_status()
      {        
        if( !empty( $_POST['id'] ) )
        {           
          foreach ($_POST['id'] as $id) 
          {
            $this->db->query("UPDATE proposals set status=@s:=status,status=old_status,old_status=@s where id='".$id."'");                      
          }
          echo 1;
        } 
      }
      


    /*  public function Unarchive_status(){
        $id=$_POST['id'];
        $stat=$this->Common_mdl->select_record('proposals','id',$id);
        $old_status=$stat['old_status'];
       // echo $old_status;
        $data=array('status'=>$old_status);
        $update=$this->Common_mdl->update('proposals',$data,'id',$id);
        echo json_encode($update);
      }*/

      public function Export($id)
      {
        //echo $id;
        $filename = 'Price-list_'.date('Ymd').'.xls';    
        $post=$this->Common_mdl->select_record('proposals','id',$id);
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=$filename");
        header("Content-Type: application/csv; "); 
        $file = fopen('php://output', 'w'); 
        $header = array("S.NO","Item Name","Price","Qty","Unit","Total");
        fputcsv($file, $header);
        $j=1; 

        $item_name=explode(',', $post['item_name']);
        $price=explode(',', $post['price']);
        $qty=explode(',', $post['qty']);
        $unit=explode(',', $post['unit']);

          for($i=0;$i<count($item_name);$i++)
          {
            if($item_name[$i]!='')
            { 
              $value['id']=$j; 
              $value['item_name']=$item_name[$i];
              $value['price']=$price[$i].' '.$post['currency']; 
              $value['qty']=$qty[$i]; 
              $value['unit']=$unit[$i];     
              $value['total']=($price[$i]*$qty[$i]).' '.$post['currency']; 
              $j++;              
              fputcsv($file,$value);
            } 
          }
         
          $k=$j;
          $product_name=explode(',', $post['product_name']);
          $product_price=explode(',', $post['product_price']);
          $product_qty=explode(',', $post['product_qty']);

          for($i=0;$i<count($product_name);$i++)
          { 
            if($product_name[$i]!='')
            {
              $value['id']=$k; 
              $value['item_name']=$product_name[$i]; 
              $value['price']=$product_price[$i].' '.$post['currency'];  
              $value['qty']= $product_qty[$i]; 
              $value['unit']='per_service';
              $value['total']=($product_price[$i]*$product_qty[$i]).' '.$post['currency'];
              $k++;
               $value['id']=$k;
              fputcsv($file,$value); 
            }
          }

          $m=$k;
          $subscription_name=explode(',', $post['subscription_name']);
          $subscription_price=explode(',', $post['subscription_price']);
          $subscription_unit=explode(',', $post['subscription_unit']);

          for($i=0;$i<count($subscription_name);$i++)
          { 
              if($subscription_name[$i]!='')
              {
                $value['id']=$m;
                $value['item_name']=$subscription_name[$i];
                $value['price']=$subscription_price[$i].' '.$post['currency'];  
                $value['qty']= 1;
                $value['unit']=$subscription_unit[$i]; 
                $value['total']=$subscription_price[$i].' '.$post['currency'];
                $m++; 
                fputcsv($file,$value);
              }
          }

          fputcsv($file,array("","","","","Sub Total",$post['total_amount'].' '.$post['currency']));
          fputcsv($file,array("","","","","Total",$post['grand_total'].' '.$post['currency']));

          fclose($file);
          exit;        
      }  

      public function convert_template(){
        $user_id=$this->session->userdata['userId']; 
        $id=$_POST['id'];
        $proposal_content=$this->Common_mdl->select_record('proposals','id',$id);
        $template=$proposal_content['proposal_contents'];
        $proposal_name=$proposal_content['proposal_name'];
        $data=array('template_title'=>$proposal_name,'template_content'=>$template,'user_id'=>$user_id);
        $insert=$this->Common_mdl->insert('proposal_template',$data);
        echo json_encode($insert);
      } 

      public function delete($id){
        $this->Common_mdl->delete('proposals','id',$id);        
        redirect('user');
      } 

      public function internal_notes(){
        $id=$_POST['id'];
        $internal_notes=$_POST['internal_notes'];
        $data=array('proposal_id'=>$id,'internal_notes'=>$_POST['internal_notes'],'status'=>'active');
        $insert=$this->Common_mdl->insert('proposal_internalnotes',$data);
        $internal=$this->Common_mdl->GetAllWithWhere('proposal_internalnotes','proposal_id',$id);


        $data=array('proposal_id'=>$id,'tag'=>'internal_notes','created_at'=>date('Y-m-d h:i:s'));
        $inse=$this->Common_mdl->insert('proposal_history',$data);

        $datas['msg_content']='';

        foreach($internal as $con)
        {  
          $timestamp = strtotime($con['created_at']);
          $newDate = date('d F Y H:i', $timestamp);                               
          $datas['msg_content'].='<div class="else-msgcont blockquote"><span>
              '.$con['internal_notes'].'</span><span class="created_at">'.$newDate.'</span></div>';               
        } 

        echo json_encode($datas);
      }

      public function tax_rate(){
        $id=$_POST['id'];
        $tax_rate=$this->db->query('select tax_rate from proposal_tax where id='.$id.'')->row_array();
        $data['tax']=$tax_rate['tax_rate'];
        echo json_encode($data);
      }

      public function sender_company_details(){
        $sender_company_name=$_POST['sender_company_name'];
        $sender_company_mailid=$_POST['sender_company_mailid'];
        $data['content']='';
        $data['content']='<span>'.strtoupper($sender_company_name).'</span><input type="hidden" name="sender_company" value="'.$sender_company_name.'" >
                                    <strong>'.$sender_company_mailid.'</strong><input type="hidden" name="sender_mail_id" value="'.$sender_company_mailid.'" >';
        echo json_encode($data);
      }

      public function proposalclient_companyadd(){
        $this->session->set_flashdata('client_added','company');
        redirect('client/addnewclient');
      }

      public function proposalclient_manualadd(){
        $this->session->set_flashdata('client_added','manual');
        redirect('client/addnewclient');
      }


      public function pricelist_insert(){

        //   $services='';
        //   $products='';
        //   $subs='';

        // if(isset($_POST['item_name']) && $_POST['item_name']!=''){
        //   $dummy_service=array();
        //   for($i=0;$i<count($_POST['item_name']);$i++){
        //     array_push($dummy_service,0);
        //   }

        //   $services=implode(',',$dummy_service);
        // }


        //  if(isset($_POST['product_name']) && $_POST['product_name']!=''){
        //   $dummy_product=array();
        //   for($i=0;$i<count($_POST['product_name']);$i++){
        //     array_push($dummy_product,0);
        //   }
        //   $products=implode(',',$dummy_product);
        // }


        // if(isset($_POST['subscription_name']) && $_POST['subscription_name']!=''){
        //   $dummy_sub=array();
        //   for($i=0;$i<count($_POST['subscription_name']);$i++){
        //     array_push($dummy_sub,0);
        //   }
        //   $subs=implode(',',$dummy_sub);
        // }



        //  $datas=array('service_name'=>(isset($_POST['item_name']) && $_POST['item_name']!='') ? implode(',',array_filter($_POST['item_name'])) : $services,
        //   'service_price'=>(isset($_POST['price']) && $_POST['price']!='') ? rtrim(implode(',',array_filter($_POST['price'])), ',') : $services,
        //   'service_unit'=>(isset($_POST['unit']) && $_POST['unit']!='') ? implode(',',array_filter($_POST['unit'])) : $services,
        //   'service_description'=>(isset($_POST['description']) && $_POST['description']!='') ? implode(',',array_filter($_POST['description'])) : $services,         
        //   'service_qty'=>(isset($_POST['qty']) && $_POST['qty']!='') ? implode(',',array_filter($_POST['qty'])) : $services,
        //   'product_name'=> (isset($_POST['product_name']) && $_POST['product_name']!='') ? implode(',',array_filter($_POST['product_name'])) : $products,
        //   'product_price'=>(isset($_POST['product_price']) && $_POST['product_price']!='') ? implode(',',array_filter($_POST['product_price'])) : $products,
        //   'product_description'=>(isset($_POST['product_description']) && $_POST['product_description']!='') ? implode(',',array_filter($_POST['product_description'])) : '',
        //   'product_qty'=>(isset($_POST['product_qty']) && $_POST['product_qty']!='') ? implode(',',array_filter($_POST['product_qty'])) : $products,
        //   'subscription_name'=>(isset($_POST['subscription_name']) && $_POST['subscription_name']!='') ? implode(',',array_filter($_POST['subscription_name'])): $subs,
        //   'subscription_price'=>(isset($_POST['subscription_price']) && $_POST['subscription_price']!='') ? implode(',',array_filter($_POST['subscription_price'])): $subs,
        //   'subscription_unit'=>(isset($_POST['subscription_unit']) && $_POST['subscription_unit']!='') ? implode(',',array_filter($_POST['subscription_unit'])) : $subs,
        //   'subscription_description'=>(isset($_POST['subscription_description']) && $_POST['subscription_description']!='') ? implode(',',array_filter($_POST['subscription_description'])) : $subs,
        //   'service_optional'=>(isset($_POST['service_optional']) && $_POST['service_optional']!='') ? implode(',',array_filter($_POST['service_optional'])) : $services,
        //   'service_quantity'=>(isset($_POST['service_quantity']) && $_POST['service_quantity']!='') ? implode(',',array_filter($_POST['service_quantity'])) : $services,
        //   'product_optional'=>(isset($_POST['product_optional']) && $_POST['product_optional']!='') ? implode(',',array_filter($_POST['product_optional'])) : $products,
        //   'product_quantity'=>(isset($_POST['product_quantity']) && $_POST['product_quantity']!='') ? implode(',',array_filter($_POST['product_quantity'])) : $products,
        //   'subscription_optional'=>(isset($_POST['subscription_optional']) && $_POST['subscription_optional']!='') ? implode(',',array_filter($_POST['subscription_optional'])) : $subs,
        //   'subscription_quantity'=>(isset($_POST['subscription_quantity']) && $_POST['subscription_quantity']!='') ? implode(',',array_filter($_POST['subscription_quantity'])) : $subs,
        //   'tax'=>(isset($_POST['tax']) && $_POST['tax']!='') ? implode(',',array_filter($_POST['tax'])) : $services,
        //   'discount'=>(isset($_POST['discount']) && $_POST['discount']!='') ? implode(',',array_filter($_POST['discount'])) : $services,
        //   'product_tax'=>(isset($_POST['product_tax']) && $_POST['product_tax']!='') ? implode(',',array_filter($_POST['product_tax'])) : $products,
        //   'product_discount'=>(isset($_POST['product_discount']) && $_POST['product_discount']!='') ? implode(',',array_filter($_POST['product_discount'])) : $products,
        //   'subscription_tax'=>(isset($_POST['subscription_tax']) && $_POST['subscription_tax']!='') ? implode(',',array_filter($_POST['subscription_tax'])) : $subs,
        //   'subscription_discount'=>(isset($_POST['subscription_discount']) && $_POST['subscription_discount']!='') ? implode(',',array_filter($_POST['subscription_discount'])) : $subs,
        //   'pricelist_name'=>$_POST['pricelist_name'],'user_id'=>$_SESSION['id']);   


         $datas=array('service_name'=>rtrim(implode(',',$_POST['item_name']),','),
          'service_price'=>rtrim(implode(',',$_POST['price']), ','),
          'service_unit'=>rtrim(implode(',',$_POST['unit']),','),
          'service_description'=>rtrim(implode(',',$_POST['description']),','),         
          'service_qty'=>rtrim(implode(',',$_POST['qty']),','),
          'product_name'=>rtrim(implode(',',$_POST['product_name']),','),
          'product_price'=>rtrim(implode(',',$_POST['product_price']),','),
          'product_description'=>rtrim(implode(',',$_POST['product_description']),','),
          'product_qty'=>rtrim(implode(',',$_POST['product_qty']),','),
          'subscription_name'=>rtrim(implode(',',$_POST['subscription_name']),','),
          'subscription_price'=>rtrim(implode(',',$_POST['subscription_price']),','),
          'subscription_unit'=>rtrim(implode(',',$_POST['subscription_unit']),','),
          'subscription_description'=>rtrim(implode(',',$_POST['subscription_description']),','),
          'service_optional'=>rtrim(implode(',',$_POST['service_optional']),','),
          'service_quantity'=>rtrim(implode(',',$_POST['service_quantity']),','),
          'product_optional'=>rtrim(implode(',',$_POST['product_optional']),','),
          'product_quantity'=>rtrim(implode(',',$_POST['product_quantity']),','),
          'subscription_optional'=>rtrim(implode(',',$_POST['subscription_optional']),','),
          'subscription_quantity'=>rtrim(implode(',',$_POST['subscription_quantity']),','),
          'tax'=>rtrim(implode(',',$_POST['tax']),','),
          'discount'=>rtrim(implode(',',$_POST['discount']),','),
          'product_tax'=>rtrim(implode(',',$_POST['product_tax']),','),
          'product_discount'=>rtrim(implode(',',$_POST['product_discount']),','),
          'subscription_tax'=>rtrim(implode(',',$_POST['subscription_tax']),','),
          'subscription_discount'=>rtrim(implode(',',$_POST['subscription_discount']),','),
          'pricelist_name'=>$_POST['pricelist_name'],'user_id'=>$_SESSION['id'],'firm_id'=>$_SESSION['firm_id']); 


         // echo '<pre>';
         // print_r($datas);
         // echo '</pre>';
         // exit;

        $insert=$this->Common_mdl->insert('proposal_pricelist',$datas);
        $pricelist=$this->Common_mdl->GetAllWithWhere('proposal_pricelist','firm_id',$_SESSION['firm_id']);
        $data['content']='<select name="pricelist" onchange="pricelist_update(this)"><option value="">Select</option>';
        foreach($pricelist as $price){        
                   
          $data['content'].=' <option value="'.$price['id'].'">'.$price['pricelist_name'].'</option>';
        }
         $data['content'].='</select>';
         echo json_encode($data);    //  echo $insert;

      }


      public function pricelist_select(){
        $id=$_POST['id'];
        $records=$this->Common_mdl->select_record('proposal_pricelist','id',$id);
         $tax_array=array();
         $discount_array=array();

         // $service_length=(isset($_POST['service_length']))?$_POST['service_length']:'0';
         // $product_length=(isset($_POST['product_length']))?$_POST['product_length']:'0';
         // $subscription_length=(isset($_POST['subscription_length']))?$_POST['subscription_length']:'0';


        if($records['service_name']!=''){
          $service_name=explode(',',$records['service_name']);
          $service_price=explode(',',$records['service_price']);
          $service_unit=explode(',',$records['service_unit']);
          $service_description=explode(',',$records['service_description']);     
          $service_qty=explode(',',$records['service_qty']);
          $service_tax=explode(',',$records['tax']);     
          $service_discount=explode(',',$records['discount']);
          $service_optional=explode(',',$records['service_optional']);     
          $service_quantity=explode(',',$records['service_quantity']);
          $data['service']='';
          // echo count($service_name);

          //  for($i=0;$i<count($service_name);$i++){
          //       echo $i;
          //  }
          // exit;
          for($i=0;$i<count($service_name);$i++){

            array_push($tax_array,$service_tax[$i]);
            array_push($discount_array,$service_discount[$i]);

          if(array_sum($tax_array)!=0){

           $style="tax-exits";

            }else{
               $style="";
            }

          if(array_sum($discount_array)!=0){

          $style1="discount-exits";

          }else{
          $style1="";
          }
         // echo $i;

            $data['service'].='<div class="add-service input-fields">                        
                           <div class="top-field-heads "><div class="proposal-fields1 service-items">
                                 <input name="item_name[]" id="item_name" class="required item_name" value="'.$service_name[$i].'" readonly> 
                                 <input type="hidden" name="service_category_name[]" id="service_category_name" class="service_category_name" value="">                                  
                              </div>
                              <div class="proposal-fields2 price-services service-items">
                              <input type="hidden" placeholder="1" name="original_price[]" id="original_price" class="decimal  original_price" onchange="sub_total()" value="'.$service_price[$i].'">
                              <input type="text" placeholder="1" name="price[]" id="price" maxlength="6" class="decimal required service_price  serviceprice price" onchange="sub_total()" value="'.$service_price[$i].'">
                                 <span>/</span>
                                 <select name="unit[]" id="unit" class="required unit">
                                    <option value="per_service" if(service_unit[$i])=="per_service"){ selected="selected" }>Per Service</option>
                                    <option value="per_day" if(service_unit[$i])=="per_day"){ selected="selected" }>Per Day</option>
                                    <option value="per_month" if(service_unit[$i])=="per_month"){ selected="selected" }>Per Month</option>
                                    <option value="per_2month" if(service_unit[$i])=="per_2month"){ selected="selected" }>Per 2Months</option>
                                    <option value="per_6month" if(service_unit[$i])=="per_6month"){ selected="selected" }>Per 6Month</option>
                                    <option value="per_year" if(service_unit[$i])=="per_year"){ selected="selected" }>Per Year</option>
                                 </select>
                              </div>
                              <div class="proposal-fields3 service-items">
                                <input onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" maxlength="4" min="1" max="9999" type="text" name="qty[]" id="qty" class="decimal decimal_qty required qty" value="'.$service_qty[$i].'" onchange="qty_check()">
                              </div>

                              <div class="proposal-fields5 service-items tax-field '.$style.'" style="">
                                 <input type="text" name="service_tax[]" maxlength="4" id="service_tax" value="'.$service_tax[$i].'" class="decimal tax" onchange="sub_total(this)" >
                              </div>
                              <div class="proposal-fields6 service-items discount-field '.$style.'" style="">
                                 <input type="text" name="service_discount[]" id="service_discount" value="'.$service_discount[$i].'" class="decimal discount" maxlength="4" onchange="sub_total(this)">
                              </div>                               
                           </div>
                           <div class="bottom-field-heads">
                              <textarea rows="3" name="description[]" id="description" class="required description">'.$service_description[$i].'</textarea>
                              <div class="mark-optional">
                                 <div class="mark-left-optional pull-left border-checkbox-section">
								 
                                    <div class="update-qual1">
										<label class="custom_checkbox1">
									   <input class="border-checkbox optional" type="checkbox" id="checkbox_'.$i.'" name="service_optional[]" value="1" > <i></i>
                                       </label>
									   <span>mark as optional</span>
                                    </div>
                                    <div class="update-qual1">
										<label class="custom_checkbox1">
									  <input class="border-checkbox quantity" type="checkbox" id="checkbox_101'.$i.'" name="service_quantity[]" value="1" > <i></i></label>
                                       <span>quantity  editables</span>
                                    </div>
                                 </div>
                                 <div class="pull-right mark-right-optional">
                                                                     <a class="sve-action1 edit_before_save" id="service_save" onclick="saved(this)">save</a>
                                    <a class="sve-action1 edit_after_save" id="service_save" onclick="saved_before(this)" style="display:none">save</a>
                                     <a class="sve-action2" id="save_to_catalog" onclick="save_to_catalog(this)">save to package</a>
                                     <div class="delete_service_div buttons_hide">
                                    <a class="sve-action2 service_close" id="service_close" onclick="deleted(this)">
                                    Delete</a>
                                    </div>
                                   <div class="cancel_service_div cancel-comdiv" style="display: none">
                                    <a class="sve-action2 service_cancel" id="service_close" onclick="cancelled(this)">
                                    Cancel</a>
                                    </div>
                                                                   </div>
                              </div>
                              </div>
                              </div>';
          }
        }

        if($records['product_name']!=''){
           $product_name=explode(',',$records['product_name']);
          $product_price=explode(',',$records['product_price']);
          $product_description=explode(',',$records['product_description']);
          $product_qty=explode(',',$records['product_qty']);  
          $product_tax=explode(',',$records['product_tax']);
          $product_discount=explode(',',$records['product_discount']);
          $product_optional=explode(',',$records['product_optional']);
          $product_quantity=explode(',',$records['product_quantity']);
          $data['product']='';
          for($i=0;$i<count($product_name);$i++){

              array_push($tax_array,$product_tax[$i]);
            array_push($discount_array,$product_discount[$i]);

                if(array_sum($tax_array)!=0){

           $style2="tax-exits";

            }else{
               $style2="";
            }

          if(array_sum($discount_array)!=0){

          $style3="discount-exits";

          }else{
          $style3="";
          }

          if(empty($product_qty[$i]))
          {
             $product_qty[$i] = 1;
          }

             $data['product'].='<div class="add-product input-fields">
                           <div class="top-field-heads">
                              <div class="proposal-fields1 service-items">
                                 <input type="text" name="product_name[]" id="product_name" class="product_name" value="'.$product_name[$i].'">
                                     <input type="hidden" name="product_category_name[]" id="product_category_name" class="product_category_name" value=""> 
                              </div>
                              <div class="proposal-fields2 price-services service-items">
                                
                                 <input type="text" placeholder="1" name="product_price[]" maxlength="6" id="product_price" class="decimal product_price price" onchange="sub_total()" value="'.$product_price[$i].'">
                                 <span>/</span>
                                 <select name="unit[]" id="unit" class="required unit">
                                    <option value="per_service" if(service_unit[$i])=="per_service"){ selected="selected" }>Per Service</option>
                                    <option value="per_day" if(service_unit[$i])=="per_day"){ selected="selected" }>Per Day</option>
                                    <option value="per_month" if(service_unit[$i])=="per_month"){ selected="selected" }>Per Month</option>
                                    <option value="per_2month" if(service_unit[$i])=="per_2month"){ selected="selected" }>Per 2Months</option>
                                    <option value="per_6month" if(service_unit[$i])=="per_6month"){ selected="selected" }>Per 6Month</option>
                                    <option value="per_year" if(service_unit[$i])=="per_year"){ selected="selected" }>Per Year</option>
                                 </select>
                              </div>
                              <div class="proposal-fields3 service-items">
                                 <input type="text" name="product_qty[]" maxlength="4" id="product_qty" class="decimal decimal_qty product_qty" value="'.$product_qty[$i].'" onchange="qty_check()">
                              </div>
                              <div class="proposal-fields5 service-items tax-field '.$style2.'">
                                 <input type="text" name="product_tax[]" id="product_tax" maxlength="4" class="decimal product_tax" value="'.$product_tax[$i].'" onchange="sub_total()">
                              </div>
                              <div class="proposal-fields6 service-items discount-field '.$style3.'" >
                                 <input type="text" name="product_discount[]" id="product_discount" maxlength="4" class="decimal product_discount" value="'.$product_discount[$i].'" onchange="sub_total()">
                              </div>
                              
                           </div>
                           <div class="bottom-field-heads">
                              <textarea rows="3" name="product_description[]" id="product_description" class="product_description">'.$product_description[$i].'</textarea>
                              <div class="mark-optional">
                                 <div class="mark-left-optional pull-left border-checkbox-section">
                                    <div class="update-qual1">
									<label class="custom_checkbox1">
                                       <input class="border-checkbox product_optional" type="checkbox" id="check_box'.$i.'" name="product_optional[]" value="1" ><i></i> </label>
                                       <span>mark as optional</span>
                                    </div>
                                  <div class="update-qual1">
									<label class="custom_checkbox1">
                                       <input class="border-checkbox product_quantity" type="checkbox" id="check_box201'.$i.'" name="product_quantity[]" value="1">  <i></i></label>
                                       <span>quantity  editables</span>
                                    </div>
                                 </div>
                                 <div class="pull-right mark-right-optional">
                                                                      <a class="sve-action1 edit_beforeproduct_save" id="product_save" onclick="saved_product(this)">save</a>

                                     <a class="sve-action1 edit_afterproduct_save" id="product_save" onclick="savedbefore_product(this)" style="display:none;">save</a>

                                     <a class="sve-action2" id="product_save_to_catalog" onclick="pro_save_to_catalog(this)">save to package</a>
                                    <div class="delete_product_div buttons_hide"> <a class="sve-action2 hgfghfgh product_close" id="product_close" onclick="deleted_product(this)">Delete</a></div>
                                    <div class="cancel_product_div cancel-comdiv" style="display:none"> 
                                    <a class="sve-action2 hgfghfgh product_cancel" id="product_close" onclick="cancelled_product(this)">Cancel</a></div>
                                                                 </div>
                              </div>
                              </div>
                              </div>';
          }


        }

         if($records['subscription_name']!=''){
          $subscription_name=explode(',',$records['subscription_name']);
          $subscription_price=explode(',',$records['subscription_price']);
          $subscription_description=explode(',',$records['subscription_description']);
          $subscription_unit=explode(',',$records['subscription_unit']);  
          $subscription_tax=explode(',',$records['subscription_tax']);
          $subscription_discount=explode(',',$records['subscription_discount']);
          $subscription_optional=explode(',',$records['subscription_optional']);
          $subscription_quantity=explode(',',$records['subscription_quantity']);

          $data['subscription']='';
          for($i=0;$i<count($subscription_name);$i++){
                array_push($tax_array,$subscription_tax[$i]);
            array_push($discount_array,$subscription_discount[$i]);


                    if(array_sum($tax_array)!=0){

           $style4="tax-exits";

            }else{
               $style4="";
            }

          if(array_sum($discount_array)!=0){

          $style5="discount-exits";

          }else{
          $style5="";
          }

             $data['subscription'].='<div class="add-subscription input-fields">
                           <div class="top-field-heads">
                              <div class="proposal-fields1 service-items">
                                 <input type="text" name="subscription_name[]" id="subscription_name" class="subscription_name" value="'.$subscription_name[$i].'">
                                   <input type="hidden" name="subscription_category_name[]" id="subscription_category_name" class="subscription_category_name" value="">
                              </div>
                              <div class="proposal-fields2 price-services service-items">                                

                                 <input type="text" placeholder="1" name="subscription_price[]" id="subscription_price" class="decimal subscription_price price" maxlength="6" onchange="sub_total()" value="'.$subscription_price[$i].'">
                                 <span>/</span>
                                 <select name="subscription_unit[]" id="subscription_unit" class="subscription_unit">
                                  <option value="per_hour" if($subscription_unit[$i]=="per_hour"){ selected="selected" }>Per Hour</option>
                                  <option value="per_day" if($subscription_unit[$i]=="per_day"){ selected="selected" }>Per Day</option>
                                  <option value="per_week" if($subscription_unit[$i]=="per_week"){ selected="selected" }>Per Week</option>
                                  <option value="per_month" if($subscription_unit[$i]=="per_month"){ selected="selected" }>Per Month</option>
                                  <option value="per_2month" if($subscription_unit[$i]=="per_2month"){ selected="selected" }>Per 2Month</option>
                                  <option value="per_5month" if($subscription_unit[$i]=="per_5month"){ selected="selected" }>Per 5month</option>
                                  <option value="per_year" if($subscription_unit[$i]=="per_year"){ selected="selected" }>Per Year</option>
                                </select>
                              </div>
                              <div class="proposal-fields3 service-items">
                                <input type="text" name="subscription_qty[]" maxlength="4" id="subscription_qty" class="decimal decimal_qty product_qty" value="'.$subscription_quantity[$i].'" onchange="qty_check()">
                              </div>
                              <div class="proposal-fields5 service-items tax-field '.$style4.'">
                                 <input type="text" name="subscription_tax[]" maxlength="4" id="subscription_tax" class="decimal subscription_tax" value="'.$subscription_tax[$i].'" onchange="sub_total()">
                              </div>
                              <div class="proposal-fields6 service-items discount-field '.$style5.'" >
                                 <input type="text" name="subscription_discount[]" maxlength="4" id="subscription_discount" class="decimal subscription_discount" value="'.$subscription_discount[$i].'" onchange="sub_total()">
                              </div>                              
                           </div>
                           <div class="bottom-field-heads">
                              <textarea rows="3" name="subscription_description[]" id="subscription_description" class="subscription_description">'.$subscription_description[$i].'</textarea>
                              <div class="mark-optional">
                                 <div class="mark-left-optional pull-left border-checkbox-section">
                                    <div class="update-qual1">
									   <label class="custom_checkbox1"><input class="border-checkbox subscription_optional" type="checkbox" id="che_ckbox'.$i.'" name="subscription_optional[]" value="1">  <i></i> </label>
                                       <span>mark as optional</span>
                                    </div>
                                     <div class="update-qual1">
                                       <label class="custom_checkbox1"><input class="border-checkbox subscription_quantity" type="checkbox" id="che_ckbox301'.$i.'" name="subscription_quantity[]" value="1"><i></i> </label>
                                       <span>quantity  editables</span>
                                    </div>
                                 </div>
                                 <div class="pull-right mark-right-optional">
                                                                       <a class="sve-action1 edit_beforesubscription_save" id="subscription_save" onclick="saved_subscription(this)">save</a>

                                     <a class="sve-action1 edit_aftersubscription_save" id="subscription_save" onclick="savedbefore_subscription(this)" style="display: none;">save</a>

                                     <a class="sve-action2" id="subscription_save_to_catalog" onclick="sub_save_to_catalog(this)">save to package</a>
                                      <div class="delete_subscription_div buttons_hide">  <a class="sve-action2 subscription_close" id="subscription_close" onclick="delete_subscription(this)">Delete</a></div>
                                    <div class="cancel_subscription_div cancel-comdiv" style="display:none"> 
                                   <a class="sve-action2 subscription_cancel" id="subscription_close" onclick="cancelled_subscription(this)">Cancel</a></div>
                                                                   </div>
                              </div>
                              </div>
                              </div>';
          }
        }

        $count_tax=count($tax_array);
        $count_discount=count($discount_array);

      if($count_tax > 0 ){
       $sum_tax=array_sum($tax_array);
         if($sum_tax > 0){
              $data['tax_option']=1;
            }else{
              $data['tax_option']=0;
            }          

      }

      if($count_discount > 0 ){
        $sum_discount=array_sum($discount_array);
        if($sum_discount > 0){
              $data['discount_option']=1;
            }else{
              $data['discount_option']=0;
            }
      }

        echo json_encode($data);
      }


      public function document_attachments(){ 
        $id=$_POST['id'];
        $document_attachment=implode(',',$_POST['document_attachment']);
        $data=array('document_attachment'=>$document_attachment);
        $value=$this->Common_mdl->update('proposals',$data,'id',$id);
        echo $value;
      }


      public function attachments(){
        $id=$_POST['id'];
        $attachment=implode(',',$_POST['document_attachment']);
        $data=array('attachment'=>$attachment);
        $value=$this->Common_mdl->update('proposals',$data,'id',$id);
        echo $value;
      }

      public function optional_section_service()
      {
           $id=$_POST['id'];
           $data=array('proposal_id'=>$id,'tag'=>'optional','created_at'=>date('Y-m-d h:i:s'));
           $inse=$this->Common_mdl->insert('proposal_history',$data);     
           $result = $this->GeneratePdf($id,$_POST['proposal_contents']);

           $data=array('proposal_contents'=>$_POST['proposal_contents'],'pdf_file' => $result[0],'pdf_proposal_content' => $result[1]);    
           $update=$this->Common_mdl->update('proposals',$data,'id',$id);
           echo $update;
      }

      public function optional_section_product()
      {
          $id=$_POST['id'];
          $data=array('proposal_id'=>$id,'tag'=>'optional','created_at'=>date('Y-m-d h:i:s'));
          $inse=$this->Common_mdl->insert('proposal_history',$data); 
          $result = $this->GeneratePdf($id,$_POST['proposal_contents']);

          $data=array('proposal_contents'=>$_POST['proposal_contents'],'pdf_file' => $result[0],'pdf_proposal_content' => $result[1]);
          $update=$this->Common_mdl->update('proposals',$data,'id',$id);
          echo $update;
      }

      public function optional_section_subscription()
      {
           $id=$_POST['id'];
           $data=array('proposal_id'=>$id,'tag'=>'optional','created_at'=>date('Y-m-d h:i:s'));
           $inse=$this->Common_mdl->insert('proposal_history',$data); 
           $result = $this->GeneratePdf($id,$_POST['proposal_contents']);

           $data=array('proposal_contents'=>$_POST['proposal_contents'],'pdf_file' => $result[0],'pdf_proposal_content' => $result[1]);
           $update=$this->Common_mdl->update('proposals',$data,'id',$id);
           echo $update;
      }

      public function GeneratePdf($id,$proposal_content_new)
      {
          /* get content from template*/
          $dom = new DomDocument();
          $dom->loadHTML(preg_replace("/\s\s+/", "", $proposal_content_new));
          $finder = new DomXPath($dom);
          $classname = "ready-for-edit"; 
          $nodes = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname ')]"); 
          $links = '';
          foreach($nodes as $container) 
          {
              $string = $dom->saveHTML($container);
              $links .= $string;
          }         
          
          $dom = new DomDocument();
          $dom->loadHTML(preg_replace("/\s\s+/", "", $links));

          $optpath = new DOMXPath($dom);      
          $optional_tags = $optpath->query('//*[@name="optional"]'); 

          $finder = new DomXPath($dom);
          $classname = "checkbox-fade";          
          $val_nodes = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname ')]");

          foreach($optional_tags as $optional_tag)
          { 
              if($optional_tag->getAttribute('checked') == 'checked')
              {
                 $check_box = "<input type='checkbox' checked>";
              }
              else
              {
                 $check_box = "<input type='checkbox'>";
              }

              foreach($val_nodes as $val_container) 
              {
                  $string = $dom->saveHTML($val_container);
                  $links = str_replace($string, $check_box, preg_replace("/\s\s+/", "", $links)); 
              }
          }
          
          /* get content from template */   

          $proposal = $this->Common_mdl->select_record('proposals','id',$id);

          $data['proposal_name'] = $proposal['proposal_name'];
          $data['client_name'] = $proposal['company_name'];
          $data['records']['pdf_content'] = $links;
          $data['records']['accept_signature'] = $proposal['accept_signature'];
          $data['records']['accept_date'] = $proposal['accept_date'];

          if($proposal['email_signature'] == 'on')
          {
            $data['records']['email_signature'] = $proposal['email_signature'];
            $signature = $this->db->query('SELECT * from pdf_signatures where proposal_no = "'.$id.'" Order By id Desc Limit 1')->row_array();
            $data['signature'] = $signature;
          }

          $this->load->library('Pdf');
          $dompdf = new Pdf();

          //general options for pdf
          $dompdf ->set_option('isHtml5ParserEnabled', true);
          $dompdf ->set_option('defaultMediaType', 'all');
          $dompdf ->set_option('isFontSubsettingEnabled', true);
          $dompdf ->set_option('isRemoteEnabled', true);
          $dompdf->set_paper("A4");
          // load the html content
          $html = ''; 
          $html = $this->load->view('proposal/firstpage',$data,true); 
          $html .= $this->load->view('proposal/test_pdf',$data,true);    
          $html = $this->Common_mdl->closetags($html); 
          $dompdf->load_html($html); 
          $dompdf->render();

          $output = $dompdf->output();
          $dir = 'uploads/proposal_pdf/';
          $timestamp = date("Y-m-d_H:i:s");
          $filename = $data['proposal_name'].'_'.$timestamp.'.pdf';       

          file_put_contents(FCPATH . $dir . $filename, $output);
          
          return [$filename,$links];
      }

      public function quantity_update()
      {
         $id = $_POST['id'];
         $data = array('proposal_id'=>$id,'tag'=>'quantity_update','created_at'=>date('Y-m-d h:i:s'));
         $inse = $this->Common_mdl->insert('proposal_history',$data);          
         
         $data = array('qty'=>implode(',',$_POST['qty']),'product_qty'=>implode(',',$_POST['product_qty']),'proposal_contents'=>$_POST['proposal_contents'],'grand_total'=>$_POST['grand_total'],'total_amount'=>$_POST['total_amount']);
         $update = $this->Common_mdl->update('proposals',$data,'id',$id);
         $proposal_status = $this->Common_mdl->select_record('proposals','id',$id);

         if($proposal_status['status'] == 'accepted')
         {
            $invoice_id=$proposal_status['draft_invoice'];
            $this->Proposal_model->invoive_create($id,$invoice_id);
         }

         echo $update;
      }

    public function attachments_upload()
    {
      $name = $_FILES['file']['name'];
      $type=$_FILES['file']['type'];     
      $config['upload_path'] = 'uploads/edittab';
      $config['allowed_types'] = '*';
      $this->load->library('upload', $config);
      $attach_file=$this->Common_mdl->do_upload($_FILES['file'], 'uploads/edittab');
      $data['image_name']=base_url().'uploads/edittab/'.$attach_file;
      $data['type']=$type;
      $image_name=base_url().'uploads/edittab/'.$attach_file;
      echo json_encode($data);
    }

    public function images_upload(){
      $name = $_FILES['file']['name'];     
      $config['upload_path'] = 'uploads/edittab';
      $config['allowed_types'] = '*';
      $this->load->library('upload', $config);
      $attach_file=$this->Common_mdl->do_upload($_FILES['file'], 'uploads/edittab');
      $data['image_name']=base_url().'uploads/edittab/'.$attach_file;
      echo json_encode($data);
    }

    public function templatename_get(){
      $template_id=$_POST['template_id'];
       $template_title=$this->db->query('select template_title from proposal_template where id="'.$template_id.'" ')->row_array(); 
       $data['proposal_name']=  $template_title['template_title'];
       echo json_encode($data);
    }
    public function discuss_attachments(){
      $id=$_POST['id'];
      $delete=$this->Common_mdl->delete('proposal_discussion_attachments','id',$id);
     echo $delete;
    }

    public function signed_document($id)
    {
        $records=$this->Common_mdl->select_record('proposals','id',$id); 
        $signature=$this->Common_mdl->select_record('pdf_signatures','proposal_no',$id);
        $data['records']['pdf_content']=$records['pdf_proposal_content'];
        $p_no=$records['id'];
        $data['records']['accept_signature']=$records['accept_signature'];
        $data['records']['accept_date']=$records['accept_date'];

        $data['signature']=$this->db->query('select * from pdf_signatures where proposal_no='.$p_no.' Order By id Desc Limit 1')->row_array();
        $proposal_name=$records['proposal_name'];


         $this->load->library('Pdf');
         $dompdf = new Pdf();

           //general options for pdf
         $dompdf ->set_option('isHtml5ParserEnabled', true);
         $dompdf ->set_option('defaultMediaType', 'all');
         $dompdf ->set_option('isFontSubsettingEnabled', true);
         $dompdf ->set_option('isRemoteEnabled', true);


          $dompdf->set_paper("A4");
          // load the html content
          $html = '';
          $html = $this->load->view('proposal/test_pdf',$data,true);    

        // echo $html;exit;
          $dompdf->load_html($html);
          $dompdf->render();
         
         //$dompdf->stream("dompdf_out.pdf", array("Attachment" => false));

        //  $output = $dompdf->output();

          //print_r($output);exit;
        $dir = 'uploads/proposal_pdf/';
        $timestamp = date("Y-m-d_H:i:s");
        $filename=$proposal_name.'_'.$timestamp.'.pdf';           

      //  $pdf->Output(FCPATH . $dir . $filename, 'F');
       //  $dompdf->output($filename);

        $dompdf->stream($filename);

       // file_put_contents(FCPATH . $dir . $filename, $output);

     //   $dompdf->stream($filename, array("Attachment" => false));
       

        // $dompdf->stream($filename);
    }

}

?>