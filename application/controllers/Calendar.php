<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calendar extends CI_Controller {
public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
	public function __construct() {
		parent::__construct();

		$this->load->model(array('Common_mdl','Security_model'));
	}

	public function index($timeline_schedule=false)
	{
		$this->Security_model->chk_login();
        $id = $this->session->userdata('admin_id');
        $data['user_details']=$this->Common_mdl->select_record('user','id',$id);
        $data['create_clients_list']=$this->db->query("select * from user where firm_admin_id='".$id."' and crm_name != '' ")->result_array();
        $user_id = $this->session->userdata('id');
        $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = ".$id." and role = 4 and crm_name !=''")->row_array();

        $us_id =  explode(',', $sql['us_id']);
        $sql_id = implode(',', $sql);

        $data['getClient_info'] = $this->db->query( "SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) ")->result_array();
        
  		$data['timeline_schedule_data']=$this->db->query("SELECT * from timeline_services_notes_added where module='schedule' and comments_for_reference!='' ")->result_array();
        $data['timeline_schedule_id']=$timeline_schedule;
        /** reminder events for task section **/
        $data['get_task_reminders']=$this->db->query("SELECT * FROM add_new_task where for_reminder_chk_box!='' ")->result_array();
        $data['get_leads_reminders']=$this->db->query("SELECT * FROM lead_reminder where reminder_date!='' ")->result_array();
        /** end of reminder evnet **/
        $data['get_meeting_event']=$this->db->query("SELECT * from timeline_services_notes_added where module='meeting'")->result_array();

        /** new event data **/
        $data['get_newevent_data']=$this->db->query("SELECT * FROM calender_event_data where event_title!='' ")->result_array();
        /** end of new event **/
         $data['user_client_list']=$this->db->query("SELECT * from user where firm_admin_id=".$_SESSION['id']." and role='4' AND autosave_status!='1' ")->result_array();


		$this->load->view('calendar/settingscalender_view', $data);
	}
	public function addCalendar_event()
	{
		// echo "<pre>";
		// print_r($_POST);
		// exit;
    $data['event_title']=(isset($_POST['event_title']))?$_POST['event_title']:'';
    $data['from_date']=(isset($_POST['from_date']))?$_POST['from_date']:'';
    $data['from_time']=(isset($_POST['from_timepic']))?$_POST['from_timepic']:''; 
    $data['to_date']=isset($_POST['to_date'])?$_POST['to_date']:'';
    $data['to_time']=isset($_POST['to_timepic'])?$_POST['to_timepic']:'';
    $data['all_days']=isset($_POST['all_days'])?$_POST['all_days']:'';  
    $data['event_repeats']=isset($_POST['event_repeats'])?$_POST['event_repeats']:'';
    $data['add_location']=isset($_POST['add_location'])?$_POST['add_location']:'';
    $notification_status=isset($_POST['notification_status'])?$_POST['notification_status']:'';
    $notification_timing=isset($_POST['notification_timing'])?$_POST['notification_timing']:'';
    $notification_notes=isset($_POST['notification_notes'])?$_POST['notification_notes']:'';
    $notification_status_arr=array();
    $notification_timing_arr=array();
    $notification_notes_arr=array();
if($notification_status!=''){
	foreach ($notification_status as $not_key => $not_value) {
		if($not_value!='')
		{
			array_push($notification_status_arr, $not_value);
			array_push($notification_timing_arr, $notification_timing[$not_key]);
			array_push($notification_notes_arr, $notification_notes[$not_key]);
		}
		
	}
}
	$data['notification_status']=implode(',', $notification_status_arr);
	$data['notification_timing']=implode(',', $notification_timing_arr);
	$data['notification_notes']=implode(',', $notification_notes_arr);

	$data['notification_mail']=implode(',',$_POST['notification_mail']);
    $data['notification_description']=$_POST['notification_description'];
    $data['create_by']=$_SESSION['id'];
    $data['created_time']=time();

    $this->Common_mdl->insert('calender_event_data',$data);
    redirect('Calendar');
	}

	public function addCalendar()
	{
		$this->Security_model->chk_login();
		$designation = $_POST['designation'];
		$before_surname = $_POST['before_surname'];
		$fname = $_POST['firstname'];
		$mname = $_POST['middlename'];
		$lname = $_POST['lastname'];
		$contact_email = $_POST['contact_email'];
		$phone = $_POST['phone'];
		$adres_line1 = $_POST['addressline1'];
		$adres_line2 = $_POST['addressline2'];
		$city_town = $_POST['city_town'];
		$postcode = $_POST['postcode'];
		$county = $_POST['county'];
		$country = $_POST['country'];

		$data = array('designation' => $designation,
						'before_surname' => $before_surname,
						'firstname' => $fname,
						'middlename' => $mname,
						'lastname' => $lname,
						'contact_email' => $contact_email,
						'phone' => $phone,
						'address_line1' => $adres_line1,
						'address_line2' => $adres_line2,
						'city_town' => $city_town,
						'postcode' => $postcode,
						'county' => $county,
						'country' => $country,
						'created_at' => time()
				);
		$result = $this->Common_mdl->insert('contact_calendar', $data);
		if($result)
		{
			$this->session->set_flashdata('success', '<div class="modal-alertsuccess alert alert-success">
                       <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                       <div class="pop-realted1">
                        <div class="position-alert1">
                        <strong>Success!</strong> Your Contact Successfully Added.</div>
                        </div></div>');
			redirect('Calendar');
		}	
	}

	public function addCalendar_event_edit(){
		$this->Security_model->chk_login();
		$event_id=$_POST['event_id'];
	$data['event_title']=(isset($_POST['event_title']))?$_POST['event_title']:'';
    $data['from_date']=(isset($_POST['from_date']))?$_POST['from_date']:'';
    $data['from_time']=(isset($_POST['from_timepic']))?$_POST['from_timepic']:''; 
    $data['to_date']=isset($_POST['to_date'])?$_POST['to_date']:'';
    $data['to_time']=isset($_POST['to_timepic'])?$_POST['to_timepic']:'';
    $data['all_days']=isset($_POST['all_days'])?$_POST['all_days']:'';  
    $data['event_repeats']=isset($_POST['event_repeats'])?$_POST['event_repeats']:'';
    $data['add_location']=isset($_POST['add_location'])?$_POST['add_location']:'';
    $notification_status=isset($_POST['notification_status'])?$_POST['notification_status']:'';
    $notification_timing=isset($_POST['notification_timing'])?$_POST['notification_timing']:'';
    $notification_notes=isset($_POST['notification_notes'])?$_POST['notification_notes']:'';
    $notification_status_arr=array();
    $notification_timing_arr=array();
    $notification_notes_arr=array();
if($notification_status!=''){
	foreach ($notification_status as $not_key => $not_value) {
		if($not_value!='')
		{
			array_push($notification_status_arr, $not_value);
			array_push($notification_timing_arr, $notification_timing[$not_key]);
			array_push($notification_notes_arr, $notification_notes[$not_key]);
		}
		
	}
}
	$data['notification_status']=implode(',', $notification_status_arr);
	$data['notification_timing']=implode(',', $notification_timing_arr);
	$data['notification_notes']=implode(',', $notification_notes_arr);

	$data['notification_mail']=implode(',',$_POST['notification_mail']);
    $data['notification_description']=$_POST['notification_description-'.$event_id];
    //$data['create_by']=$_SESSION['id'];
    //$data['created_time']=time();

    $this->Common_mdl->update('calender_event_data',$data,'id',$event_id);
    redirect('Calendar');
	}

	public function add_event()
	{
		$this->load->view('calendar/add_event');
	}

	public function CheckCompanyName()
	{
		$cmyName_id = $_POST['cmy_name'];
		$cmy_type = $_POST['cmytype'];

		if($cmyName_id !='' && $cmy_type !=''){
				if($cmy_type == '2') {
					$cond='and crm_legal_form="Private Limited company"';
				} elseif($cmy_type == '3') {
					$cond='and crm_legal_form="Public Limited company"';
				} elseif($cmy_type == '4') {
					$cond='and crm_legal_form="Limited Liability Partnership"';
				} elseif($cmy_type == '5') {
					$cond='and crm_legal_form="Partnership"';
				} elseif($cmy_type == '6') {
					$cond='and crm_legal_form="Self Assessment"';
				} elseif($cmy_type == '7') {
					$cond='and crm_legal_form="Trust"';
				} elseif($cmy_type == '8') {
					$cond='and crm_legal_form="Charity"';
				} elseif($cmy_type == '9') {
					$cond='and crm_legal_form="Other"';
				} else {
					$cond='';
				}
				$data['getclient_event'] = $this->db->query( "SELECT * FROM client WHERE user_id = '".$cmyName_id."' ".$cond."  ")->row_array();
				if($data['getclient_event'] != '') {
							$client_user_id=array();
				foreach ($data['getclient_event'] as $get_user_key => $get_user_value) {
					array_push($client_user_id, $get_user_value['user_id']);
				}
					if(count($client_user_id)>0){
						$user_ids=implode(',', $client_user_id);
						$data['timeline_schedule_data']=$this->db->query("SELECT * from timeline_services_notes_added where module='schedule' and comments_for_reference!='' and user_id in(".$user_ids.") ")->result_array();
				    }
	        		$this->load->view('calendar/filter_calendar_view.php', $data);	
				}

		}else{	
			if($cmyName_id !='') 
			{ 
			$data['getclient_event'] = $this->db->query( "SELECT * FROM client WHERE user_id = '".$cmyName_id."' ")->row_array();

			if($data['getclient_event'] != '') {
			$client_user_id=array();
			foreach ($data['getclient_event'] as $get_user_key => $get_user_value) {
			array_push($client_user_id, $get_user_value['user_id']);
			}
			if(count($client_user_id)>0){
				$user_ids=implode(',', $client_user_id);
				$data['timeline_schedule_data']=$this->db->query("SELECT * from timeline_services_notes_added where module='schedule' and comments_for_reference!='' and user_id in(".$user_ids.") ")->result_array();
			}
			$this->load->view('calendar/filter_calendar_view.php', $data);	
			}
			}

			if($cmy_type !='')
			{
			if($cmy_type == '2') {
				$cond='and crm_legal_form="Private Limited company"';
			} elseif($cmy_type == '3') {
				$cond='and crm_legal_form="Public Limited company"';
			} elseif($cmy_type == '4') {
				$cond='and crm_legal_form="Limited Liability Partnership"';
			} elseif($cmy_type == '5') {
				$cond='and crm_legal_form="Partnership"';
			} elseif($cmy_type == '6') {
				$cond='and crm_legal_form="Self Assessment"';
			} elseif($cmy_type == '7') {
				$cond='and crm_legal_form="Trust"';
			} elseif($cmy_type == '8') {
				$cond='and crm_legal_form="Charity"';
			} elseif($cmy_type == '9') {
				$cond='and crm_legal_form="Other"';
			} else {
				$cond='';
			}

			$id = $this->session->userdata('admin_id');
			$data['user_details']=$this->Common_mdl->select_record('user','id',$id);
			$data['create_clients_list']=$this->db->query("select * from user where firm_admin_id='".$id."' and crm_name != '' ")->result_array();
			$user_id = $this->session->userdata('id');
			$sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = ".$id." and role = 4 and crm_name !=''")->row_array();

			$us_id =  explode(',', $sql['us_id']);

			$data['getClient_type'] = $this->db->query( "SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) ".$cond." ")->result_array();
			if($data['getClient_type'] != '') {
			$client_user_id=array();
			foreach ($data['getClient_type'] as $get_user_key => $get_user_value) {
			array_push($client_user_id, $get_user_value['user_id']);
			}		
			if(count($client_user_id)>0){
				$user_ids=implode(',', $client_user_id);
				$data['timeline_schedule_data']=$this->db->query("SELECT * from timeline_services_notes_added where module='schedule' and comments_for_reference!='' and user_id in(".$user_ids.") ")->result_array();				
			}
			$this->load->view('calendar/filter_calendarType_view.php', $data);	
			}

			}
	}
       
	}	

	public function filterCompanyType()
	{
		$cmyType = $_POST['company_type'];
		$search_value=$_POST['search_value'];	
		$getCmy_type = $this->db->query("select * from client where user_id='".$cmyType."'")->row_array();	
		echo json_encode( $getCmy_type );		
	}
	public function update_timeline_schedule_data(){
		$sch_id=$_POST['id'];
		$explode_id=explode('_',$sch_id);
		$sch_id=$explode_id[1];
		$sch_date=$_POST['date'];
		//echo date('Y-m-d',strtotime($sch_date));
		$data['comments_for_reference']=date('Y-m-d',strtotime($sch_date));
        $this->Common_mdl->update('timeline_services_notes_added',$data,'id',$sch_id); 
	}
/** for calendar event 20-09-2018 **/
     public function calendar_events(){
     	$this->Security_model->chk_login();
     	$event_name=$_POST['event'];
        $id = $this->session->userdata('admin_id');
        $data['user_details']=$this->Common_mdl->select_record('user','id',$id);
        $data['create_clients_list']=$this->db->query("select * from user where firm_admin_id='".$id."' and crm_name != '' ")->result_array();
        $user_id = $this->session->userdata('id');
        $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = ".$id." and role = 4 and crm_name !=''")->row_array();

        $us_id =  explode(',', $sql['us_id']);
        $sql_id = implode(',', $sql);
    if($event_name=='event_allevent'){
        $data['getClient_info'] = $this->db->query( "SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) ")->result_array();
        $data['timeline_schedule_data']=$this->db->query("SELECT * from timeline_services_notes_added where module='schedule' and comments_for_reference!='' ")->result_array();
    }
    if($event_name=='event_reminder' || $event_name=='event_allevent')
    {
		$data['get_task_reminders']=$this->db->query("SELECT * FROM add_new_task where for_reminder_chk_box!='' ")->result_array();
        $data['get_leads_reminders']=$this->db->query("SELECT * FROM lead_reminder where reminder_date!='' ")->result_array();
    }  

      if($event_name=='event_meetings' || $event_name=='event_allevent')
    {
		 $data['get_meeting_event']=$this->db->query("SELECT * from timeline_services_notes_added where module='meeting'")->result_array();
    }  


        /** reminder events for task section **/
        // $data['get_task_reminders']=$this->db->query("SELECT * FROM add_new_task where for_reminder_chk_box!='' ")->result_array();
        // $data['get_leads_reminders']=$this->db->query("SELECT * FROM lead_reminder where reminder_date!='' ")->result_array();
        /** end of reminder evnet **/
        $data['event_name']=$_POST['event'];
		$this->load->view('calendar/filter_event_calendar_view', $data);
     }
/** end of 20-09-2018 **/

/** 27-09-2018 for calendar section notification  **/
 public function calendar_notifications()
 {
 	$db_val=$this->db->query("select * from calender_event_data where create_by=".$_SESSION['id']."  and FIND_IN_SET('email',notification_status)  ")->result_array();
 	if(count($db_val)>0)
 	{
 		foreach ($db_val as $db_key => $db_value) {
	 			if(date('Y-m-d',strtotime($db_value['from_date'])) == date('Y-m-d'))
	 			{
	 				$notification_status=explode(',',$db_value['notification_status']);
	 				$notification_timing=explode(',',$db_value['notification_timing']);
	 				$notification_notes=explode(',',$db_value['notification_notes']);
	 				if(count($notification_status)>0)
	 				{
	 					foreach ($notification_status as $notsta_key => $notsta_value) {
	 						
	 						if($notsta_value=='email')
	 						{
	 							if(isset($notification_timing[$notsta_key]) && isset($notification_notes[$notsta_key]))
	 							{
	 								 $notify_time=isset($notification_timing[$notsta_key])?$notification_timing[$notsta_key]:'';
	 								 $notify_notes=isset($notification_notes[$notsta_key])?$notification_notes[$notsta_key]:'';
	 								if(isset($db_value['from_date']) && isset($db_value['from_time']) && $db_value['from_time']!='')
	 								{
	 									  $it_time=$db_value['from_time'];
	 									  $time = strtotime($it_time);
										  $round = 5*60;
										  $rounded = round($time / $round) * $round;
										  $it_time=date("h:i A", $rounded);
										  /** for get current date time **/
										  $dateTime = new DateTime('now', new DateTimeZone(date_default_timezone_get()));   
										  $zz= $dateTime->format("h:i A"); 
										/** end of current date time **/
										if($notify_notes!='' && $notify_time!='')
										{									

										$queries=$this->Common_mdl->get_price('admin_setting','id','1','meeting_reminder');
										if($queries=='1'){ 


											if($notify_notes=='hours')
											{
													$timestamp = strtotime($it_time) - 60*60*$notify_time;
													$time = date('h:i A', $timestamp);
													if($zz==$time)
													{
															$mail_data['title']=$db_value['event_title'];
															$mail_data['notification_description']=$db_value['notification_description'];
															$mail_data['from_date']=$db_value['from_date']." ".$db_value['from_time'];
															$mail_data['to_date']=$db_value['to_date']." ".$db_value['to_time'];
															$mail_data['add_location']=$db_value['add_location'];
															$body = $this->load->view('calendar/calendar_notify_empty_mail_content.php', $mail_data, TRUE);

																if($db_value['notification_mail']!=''){
																	$email_ids=explode(',',$db_value['notification_mail']);
																	foreach ($email_ids as $email_key => $email_value) {
																	//echo $email_value."<br>";
																		$this->load->library('email');
																		$this->email->set_mailtype('html');
																		$this->email->from('info@remindoo.org');
																		$this->email->to($email_value);
																		//  $this->email->to('shanmugapriya.techleaf@gmail.com');
																		$this->email->subject('Calendar Reminder');
																		$this->email->message($body);
																			if($this->email->send()){
																			//echo "mail send";
																			}
																			else
																			{
																			//echo "email not";
																			}
																	}
															}
													}
											}
											else
											{
												$endTime = strtotime("-".$notify_time." minutes", strtotime('12:09 am'));
												$time = date('h:i A', $endTime);
												if($zz==$time)
												{
													//echo "mail another";
													$mail_data['title']=$db_value['event_title'];
													$mail_data['notification_description']=$db_value['notification_description'];
													$mail_data['from_date']=$db_value['from_date']." ".$db_value['from_time'];
													$mail_data['to_date']=$db_value['to_date']." ".$db_value['to_time'];
													$mail_data['add_location']=$db_value['add_location'];
													$body = $this->load->view('calendar/calendar_notify_empty_mail_content.php', $mail_data, TRUE);
														if($db_value['notification_mail']!=''){
														$email_ids=explode(',',$db_value['notification_mail']);
														foreach ($email_ids as $email_key => $email_value) {
															$this->load->library('email');
															$this->email->set_mailtype('html');
															$this->email->from('info@remindoo.org');
															$this->email->to($email_value);														
															$this->email->subject('Calendar Reminder');
															$this->email->message($body);
																if($this->email->send()){																
																}
																else
																{																
																}
														}
													}
												}

											}
										}

										}
										
	 								}
	 								else if(isset($db_value['from_date']))
	 								{
	 									  $it_time='10:00 am';
	 									  $time = strtotime($it_time);
										  $round = 5*60;
										  $rounded = round($time / $round) * $round;
										  $it_time=date("h:i A", $rounded);										 
										  $dateTime = new DateTime('now', new DateTimeZone(date_default_timezone_get()));   
										  $zz= $dateTime->format("h:i A"); 									
										if($notify_notes!='' && $notify_time!='')
										{
											$queries=$this->Common_mdl->get_price('admin_setting','id','1','meeting_reminder');
										if($queries=='1'){ 


											if($notify_notes=='hours')
											{
												$timestamp = strtotime($it_time) - 60*60*$notify_time;
												$time = date('h:i A', $timestamp);												
												if($zz==$time)
												{													
													$mail_data['title']=$db_value['event_title'];
													$mail_data['notification_description']=$db_value['notification_description'];
													$mail_data['from_date']=$db_value['from_date']." ".$db_value['from_time'];
													$mail_data['to_date']=$db_value['to_date']." ".$db_value['to_time'];
													$mail_data['add_location']=$db_value['add_location'];
													$body = $this->load->view('calendar/calendar_notify_empty_mail_content.php', $mail_data, TRUE);

													if($db_value['notification_mail']!=''){
														$email_ids=explode(',',$db_value['notification_mail']);
															foreach ($email_ids as $email_key => $email_value) {
																$this->load->library('email');
																$this->email->set_mailtype('html');
																$this->email->from('info@remindoo.org');
																$this->email->to($email_value);															
																$this->email->subject('Calendar Reminder');
																$this->email->message($body);
																	if($this->email->send()){
																	
																	}
																	else
																	{
																	
																	}
															}
													}
												}
											}
											else
											{
											   $endTime = strtotime("-".$notify_time." minutes", strtotime($it_time));
                                                $time = date('h:i A', $endTime);                                          
												if($zz==$time)
												{													
												$mail_data['title']=$db_value['event_title'];
												$mail_data['notification_description']=$db_value['notification_description'];
												$mail_data['from_date']=$db_value['from_date']." ".$db_value['from_time'];
												$mail_data['to_date']=$db_value['to_date']." ".$db_value['to_time'];
												$mail_data['add_location']=$db_value['add_location'];
												$body = $this->load->view('calendar/calendar_notify_empty_mail_content.php', $mail_data, TRUE);
													if($db_value['notification_mail']!=''){
														$email_ids=explode(',',$db_value['notification_mail']);
														foreach ($email_ids as $email_key => $email_value) {	
															$this->load->library('email');
															$this->email->set_mailtype('html');
															$this->email->from('info@remindoo.org');
															$this->email->to($email_value);														
															$this->email->subject('Calendar Reminder');
															$this->email->message($body);
															if($this->email->send()){
															
															}
															else
															{
															
															}
														}
													}
												}

											}
										}

										}
	 								}
	 							}
	 						}
	 					}
	 				}
	 			}
 			}	
 	}
 }
/** end of 27-09-2018 **/

}	
