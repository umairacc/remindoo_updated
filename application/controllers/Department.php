<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Department extends CI_Controller {
public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct(){
        parent::__construct();
        $this->load->model(array('Common_mdl','Security_model'));
    }

    public function index()
    {
        $this->Security_model->chk_login();
        $data['department']=$this->Common_mdl->getallrecords('department');
        $this->load->view('department/department',$data);
    }

    public function add_department()
    {
        $this->Security_model->chk_login();
        $this->load->view('department/add_department');
    }

    public function add_new()
    {
        $this->Security_model->chk_login();
        $data['department'] = $this->input->post('new_department');
        $data['createdTime'] = time();
        $this->Common_mdl->insert('department',$data);
        $this->session->set_flashdata('success', 'Department Added Successfully');
        //redirect('department/add_department');
        redirect('department');

    }

    public function delete($id)
    {
        $this->Security_model->chk_login();
        $this->Common_mdl->delete('department','id',$id);
        $this->session->set_flashdata('success', 'Department Deleted Successfully');
        redirect('department');
    }
      public function update_department($id)
    {
        $this->Security_model->chk_login();
        $data['department']=$this->Common_mdl->select_record('department','id',$id);
        $data['id'] = $id;
        $this->load->view('department/update_department',$data);
    }

    public function update($id)
    {
        $this->Security_model->chk_login();
        $data['department'] = $this->input->post('new_department');
        $this->Common_mdl->update('department',$data,'id',$id);
        $this->session->set_flashdata('success', 'Department Updated Successfully');
        //redirect('department/update_department/'.$id);
        redirect('department/');

    }

  /*  public function department_permissions()
    {
        $this->Security_model->chk_login();
        $this->load->view('department/add_department_permissions');
    }*/

    public function department_permission()
    {
        $this->Security_model->chk_login();
        $sql = $this->db->query("SELECT GROUP_CONCAT(  `dept_id` SEPARATOR  ',' ) AS  `dept`  FROM department_permission ")->row_array();
        $dept =  explode(',', $sql['dept']);
        $data['department'] = $this->db->query( "SELECT * FROM department WHERE id NOT IN ( '" . implode( "','", $dept ) . "' )")->result_array();
        $data['new_department'] = $this->db->query( "SELECT * FROM department_permission WHERE new_dept!=''")->result_array();

        //$data['department']=$this->Common_mdl->getallrecords('department');
        $this->load->view('department/department_permissions',$data);
    }

    public function assign_dept()
    {
         $this->Security_model->chk_login();
         $from_depart = $this->session->flashdata('from_depart');
         if(isset($from_depart) && $from_depart!='')
         {
          $data['from_department']=$from_depart;
         }
         
         $data['department_assign_team']=$this->Common_mdl->getallrecords('department_assign_team');
/*
         $sql = $this->db->query("SELECT GROUP_CONCAT(  `team_id` SEPARATOR  ',' ) AS  `team` , GROUP_CONCAT(  `depart_id` SEPARATOR  ',' ) AS  `department` FROM department_assign_team")->row_array();

         $team =  explode(',', $sql['team']);
         $department = explode(',',$sql['department']);
        */
        $data['team'] = $this->db->query( "SELECT * FROM team WHERE  create_by=".$_SESSION['id']." " )->result_array();

       // $data['department'] = $this->db->query( "SELECT * FROM department WHERE id NOT IN ( '" . implode( "','", $department ) . "' )")->result_array();
        $data['department'] = $this->db->query( "SELECT * FROM department_permission WHERE new_dept!='' and create_by=".$_SESSION['id']." ")->result_array();
        
       /* $data['staff'] = $this->db->query( "SELECT * FROM user WHERE id NOT IN ( '" .  implode( "','", $staff ) . "' ) and (role='6' or role='5') ")->result_array();*/
       
        $this->load->view('department/assign_department',$data);
    }

    public function assign_to_department()
    {
        $this->Security_model->chk_login();
        $data['depart_id'] = $this->input->post('department');
        $team = $this->input->post('team');
        if(!empty($team))
        {
          $teams = implode(',', $team);
        }
        $data['team_id'] = $teams;
        $data['createdTime'] = time();
        // for crearteby
        $data['create_by']=$_SESSION['id'];
        $this->Common_mdl->insert('department_assign_team',$data);
        $this->session->set_flashdata('success', 'Team Assigned Successfully');
        redirect('department/assigned_dept');
    }

     public function assigned_dept()
    { 
        $this->Security_model->chk_login();
        //$data['assigned_depart']=$this->Common_mdl->getallrecords('department_assign_team');
        $data['assigned_depart'] = $this->Common_mdl->GetAllWithWhere('department_assign_team','create_by',$_SESSION['id']);
        $this->load->view('department/assigned_department',$data);
    }
     public function delete_assigneddept($id)
    {
        $this->Security_model->chk_login();
        $this->Common_mdl->delete('department_assign_team','id',$id);
        $this->session->set_flashdata('success', 'Department Deleted Successfully');
        redirect('department/assigned_dept');
    }
    public function update_assigneddept($id)
    {

        $data['departments']=$this->Common_mdl->select_record('department_assign_team','id',$id);
        //print_r($data['departments']);die;
        $sql = $this->db->query("SELECT GROUP_CONCAT(  `team_id` SEPARATOR  ',' ) AS  `team` , GROUP_CONCAT(  `depart_id` SEPARATOR  ',' ) AS  `department` FROM department_assign_team where id NOT IN( '" .$id."' )")->row_array();
  
        $team =  explode(',', $sql['team']);
        $department = explode(',',$sql['department']);
        
        $data['team'] = $this->db->query( "SELECT * FROM team WHERE id NOT IN ( '" . implode( "','", $team ) . "' ) and create_by=".$_SESSION['id']." ")->result_array();

       // $data['department'] = $this->db->query( "SELECT * FROM department WHERE id NOT IN ( '" . implode( "','", $department ) . "' )")->result_array();
        $data['department'] = $this->db->query( "SELECT * FROM department_permission WHERE id NOT IN ( '" . implode( "','", $department ) . "' ) AND new_dept!='' and create_by=".$_SESSION['id']." ")->result_array();
      /*
        $data['staff'] = $this->db->query( "SELECT * FROM user WHERE id NOT IN ( '" .  implode( "','", $staff ) . "' ) and (role='6' or role='5') ")->result_array();*/
       //print_r($data['team']);
       //print_r($data['staff']);
       //die;
       $this->load->view('department/update_assigned_depart',$data);

    }

      public function update_assigned_depart($id)
    {
      /*echo '<pre>';
      echo $id;die;
      print_r($_POST);die;*/
        $this->Security_model->chk_login();
        $data['depart_id'] = $this->input->post('department');
        $team = $this->input->post('team');
        if(!empty($team))
        {
          $teams = implode(',', $team);
        }
        $data['team_id'] = $teams;
        
        $this->Common_mdl->update('department_assign_team',$data,'id',$id);

        $this->session->set_flashdata('success', 'Team Assigned Successfully');
        redirect('department/assigned_dept');
    }
 
   public function set_department_permissions()
   {
    
 //   $data['dept_id'] = $_POST['select_department'];
    //$data['new_dept'] = $_POST['new_department'];
     $sql=$this->db->query("select * from department_permission where new_dept='".$_POST['new_department']."' and create_by=".$_SESSION['id']." ")->result_array();
   // $data['designation'] = $_POST['designation'];
      if(!empty($sql))
        {
            echo 'false';
        }
        else{
     $datas['new_dept'] = $_POST['new_department'];
  //  $data['permissions'] =(isset($_POST['permissions'])&&($_POST['permissions']!=''))? implode(",",$_POST['permissions']) : '';
    $datas['createdtime'] = time();
    $datas['create_by']=$_SESSION['id'];
    // if($data['new_dept']==''){
    // $row =$this->Common_mdl->select_record('department_permission','dept_id',$data['dept_id']);
    // if($row)
    // {
    //   $this->Common_mdl->update('department_permission',$data,'dept_id',$data['dept_id']);
    // }else{
    //   $this->Common_mdl->insert('department_permission',$data);
    // }}else{
    //   $this->Common_mdl->insert('department_permission',$data);
    // }
      $res=$this->Common_mdl->insert('department_permission',$datas);
  $for_bulkview=$_POST['for_bulkview'];
    $for_bulkcreates=$_POST['for_bulkcreates'];
    $for_bulkedit=$_POST['for_bulkedit'];
    $for_bulkdelete=$_POST['for_bulkdelete'];

    $data['department_id']=$res;
    $sql=$this->db->query('select * from department_access_permission where department_id='.$res.' ')->result_array();
    if(count($sql)>0)
    {
 $this->Common_mdl->delete('department_access_permission','department_id',$res);
    }
      
    foreach ($for_bulkview as $for_key => $for_value) {

      if($for_value!='')
      {
      $un_check=explode('//', $for_value);
      if(count($un_check)>1){
        $data['view']=0;
      }
      else
      {
        $data['view']=1;
      }
      $it_view=explode('-', $for_value);
      $main='';
      $sub='';
      if($it_view[0]!='')
      {
        $main=$it_view[0];
      }
      if($it_view[1]!='')
      {
        $sub=$it_view[1];
      }
      
      $data['main']=$main;
      if(count($it_view)>2)
      {
         $data['sub']=$sub;
      }
      else
      {
        $data['sub']='';
      }
     
      //$data['view']=1;
     }     
     if($for_bulkcreates[$for_key]!='')
      {
      $un_check=explode('//', $for_bulkcreates[$for_key]);
      if(count($un_check)>1){
        $data['create']=0;
      }
      else
      {
        $data['create']=1;
      }
      $it_view=explode('-', $for_bulkcreates[$for_key]);
      $main='';
      $sub='';
      if($it_view[0]!='')
      {
        $main=$it_view[0];
      }
      if($it_view[1]!='')
      {
        $sub=$it_view[1];
      }
      
      $data['main']=$main;
   //   $data['sub']=$sub;
      if(count($it_view)>2)
      {
         $data['sub']=$sub;
      }
      else
      {
        $data['sub']='';
      }
    //  $data['create']=1;
     }
      if($for_bulkedit[$for_key]!='')
      {
      $un_check=explode('//', $for_bulkedit[$for_key]);
      if(count($un_check)>1){
        $data['edit']=0;
      }
      else
      {
        $data['edit']=1;
      }
      $it_view=explode('-', $for_bulkedit[$for_key]);
      $main='';
      $sub='';
      if($it_view[0]!='')
      {
        $main=$it_view[0];
      }
      if($it_view[1]!='')
      {
        $sub=$it_view[1];
      }
      
      $data['main']=$main;
      //$data['sub']=$sub;
      if(count($it_view)>2)
      {
         $data['sub']=$sub;
      }
      else
      {
        $data['sub']='';
      }
    //  $data['edit']=1;
     }
      if($for_bulkdelete[$for_key]!='')
      {
      $un_check=explode('//', $for_bulkdelete[$for_key]);
      if(count($un_check)>1){
        $data['delete']=0;
      }
      else
      {
        $data['delete']=1;
      }
      $it_view=explode('-', $for_bulkdelete[$for_key]);
      $main='';
      $sub='';
      if($it_view[0]!='')
      {
        $main=$it_view[0];
      }
      if($it_view[1]!='')
      {
        $sub=$it_view[1];
      }
      
      $data['main']=$main;
      //$data['sub']=$sub;
      if(count($it_view)>2)
      {
         $data['sub']=$sub;
      }
      else
      {
        $data['sub']='';
      }
     // $data['delete']=1;
     }
// echo "<pre>";
// print_r($data);

$res=$this->Common_mdl->insert('department_access_permission',$data);
 
//echo $res;
      }

 if($_POST['it_value']=="create"){
        $this->session->set_flashdata('success', 'Department have been Added Successfully');
      }
        $this->session->set_flashdata('from_depart', $res);

    return $res;
       }
   }

   public function dept_permission()
   {
        //$data['dept_permission']=$this->Common_mdl->getallrecords('department_permission');
      $data['dept_permission']=$this->Common_mdl->GetAllWithWhere('department_permission','create_by',$_SESSION['id']);
        $this->load->view('department/dept_permission',$data);
   }
   public function archive_dept_permission($id)
   {
    $this->db->query("update department_permission set status=1 where id=$id");
    echo $this->db->affected_rows();
   }

   public function delete_permission($id)
   {
       $this->Security_model->chk_login();
        $this->Common_mdl->delete('department_permission','id',$id);
        $this->session->set_flashdata('success', 'Department have been Deleted Successfully');
        redirect('department/dept_permission');
   }

   public function deptDelete()
  {
      if(isset($_POST['data_ids']))
      {
          $id = trim($_POST['data_ids']);
          $sql = $this->db->query("DELETE FROM department_permission WHERE id in ($id)");
          echo $id;
      }  
  }

  public function AssignDept_Delete()
  {
      if(isset($_POST['data_ids']))
      {
          $id = trim($_POST['data_ids']);
          $sql = $this->db->query("DELETE FROM department_assign_team WHERE id in ($id)");
          echo $id;
      }  
  }

   public function update_permission($id)
   {
      $data['permissions'] =$this->Common_mdl->select_record('department_permission','id',$id);


      $sql = $this->db->query("SELECT GROUP_CONCAT(  `dept_id` SEPARATOR  ',' ) AS  `dept`  FROM department_permission where id NOT IN( '" .$id."' ) ")->row_array();

      $dept =  explode(',', $sql['dept']);
      $data['department'] = $this->db->query( "SELECT * FROM department WHERE id NOT IN ( '" . implode( "','", $dept ) . "' )")->result_array();
      // echo "<pre>";
      // print_r($sql['dept']); exit();

      $this->load->view('department/update_permission',$data);
   }

   public function update_department_permissions()
   {
    $id = $_POST['id'];
       $sql=$this->db->query("select * from department_permission where new_dept='".$_POST['new_department']."' and id!=".$id." and create_by=".$_SESSION['id']." and create_by=".$_SESSION['id']." ")->result_array();
   // $data['designation'] = $_POST['designation'];
      if(!empty($sql))
        {
            echo 'false';
        }
        else{
   // $data['dept_id'] = $_POST['select_department'];
    $datas['new_dept'] = $_POST['new_department'];
   // $data['designation'] = $_POST['designation'];
   // $data['permissions'] = implode(",",$_POST['permissions']);
    
    $this->Common_mdl->update('department_permission',$datas,'id',$_POST['id']);
    /** for access permission **/
    $res=$_POST['id'];
    $for_bulkview=$_POST['for_bulkview'];
    $for_bulkcreates=$_POST['for_bulkcreates'];
    $for_bulkedit=$_POST['for_bulkedit'];
    $for_bulkdelete=$_POST['for_bulkdelete'];

    $data['department_id']=$res;
    $sql=$this->db->query('select * from department_access_permission where department_id='.$res.' ')->result_array();
    if(count($sql)>0)
    {
 $this->Common_mdl->delete('department_access_permission','department_id',$res);
    }
      
    foreach ($for_bulkview as $for_key => $for_value) {

      if($for_value!='')
      {
      $un_check=explode('//', $for_value);
      if(count($un_check)>1){
        $data['view']=0;
      }
      else
      {
        $data['view']=1;
      }
      $it_view=explode('-', $for_value);
      $main='';
      $sub='';
      if($it_view[0]!='')
      {
        $main=$it_view[0];
      }
      if($it_view[1]!='')
      {
        $sub=$it_view[1];
      }
      
      $data['main']=$main;
      if(count($it_view)>2)
      {
         $data['sub']=$sub;
      }
      else
      {
        $data['sub']='';
      }
     
      //$data['view']=1;
     }     
     if($for_bulkcreates[$for_key]!='')
      {
      $un_check=explode('//', $for_bulkcreates[$for_key]);
      if(count($un_check)>1){
        $data['create']=0;
      }
      else
      {
        $data['create']=1;
      }
      $it_view=explode('-', $for_bulkcreates[$for_key]);
      $main='';
      $sub='';
      if($it_view[0]!='')
      {
        $main=$it_view[0];
      }
      if($it_view[1]!='')
      {
        $sub=$it_view[1];
      }
      
      $data['main']=$main;
   //   $data['sub']=$sub;
      if(count($it_view)>2)
      {
         $data['sub']=$sub;
      }
      else
      {
        $data['sub']='';
      }
    //  $data['create']=1;
     }
      if($for_bulkedit[$for_key]!='')
      {
      $un_check=explode('//', $for_bulkedit[$for_key]);
      if(count($un_check)>1){
        $data['edit']=0;
      }
      else
      {
        $data['edit']=1;
      }
      $it_view=explode('-', $for_bulkedit[$for_key]);
      $main='';
      $sub='';
      if($it_view[0]!='')
      {
        $main=$it_view[0];
      }
      if($it_view[1]!='')
      {
        $sub=$it_view[1];
      }
      
      $data['main']=$main;
      //$data['sub']=$sub;
      if(count($it_view)>2)
      {
         $data['sub']=$sub;
      }
      else
      {
        $data['sub']='';
      }
    //  $data['edit']=1;
     }
      if($for_bulkdelete[$for_key]!='')
      {
      $un_check=explode('//', $for_bulkdelete[$for_key]);
      if(count($un_check)>1){
        $data['delete']=0;
      }
      else
      {
        $data['delete']=1;
      }
      $it_view=explode('-', $for_bulkdelete[$for_key]);
      $main='';
      $sub='';
      if($it_view[0]!='')
      {
        $main=$it_view[0];
      }
      if($it_view[1]!='')
      {
        $sub=$it_view[1];
      }
      
      $data['main']=$main;
      //$data['sub']=$sub;
      if(count($it_view)>2)
      {
         $data['sub']=$sub;
      }
      else
      {
        $data['sub']='';
      }
     // $data['delete']=1;
     }
// echo "<pre>";
// print_r($data);

$res=$this->Common_mdl->insert('department_access_permission',$data);
 
//echo $res;
      }

    /**end of permisison **/
    if($_POST['it_value']=="create"){
    $this->session->set_flashdata('success', 'Department Permissions updated Successfully');
            }
      $this->session->set_flashdata('from_depart', $id);
     echo "wrk";
    }
   }

   /*public function textbelt_integration($ph_no)
   {

      $ch = curl_init('https://textbelt.com/text');
      $data = array(
      'phone' => $ph_no,
      'message' => 'Hello world',
      'key' => 'textbelt',
      );

      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      $response = curl_exec($ch);
      curl_close($ch);

   }*/

    public function textbelt_integration($ph_no)
   {

//99f3f404fc164cb7e3225b5b62aae202b5a8d5e2wuTlM2XNkWQwjeii95vOfiUZY
      $ch = curl_init('https://textbelt.com/text');
      $data = array(
      'phone' => '+91'.$ph_no,
      'message' => 'Hello world',
      'key' => '99f3f404fc164cb7e3225b5b62aae202b5a8d5e2wuTlM2XNkWQwjeii95vOfiUZY',
      );

      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      $response = curl_exec($ch);
      curl_close($ch);
      print_r($response);
   /*   $data['ph_no'] = '+91'.$ph_no;
      $this->load->view('department/textbelt_api',$data);*/
   }

  /*public function twilio_api($ph_no){

  // Get the PHP helper library from twilio.com/docs/php/install
  require_once '/path/to/vendor/autoload.php'; // Loads the library
  use Twilio\Rest\Client;
  // Your Account Sid and Auth Token from twilio.com/user/account
  $sid = "AC093901d77ee48f659b4309d83a5ad950";
  $token = "b300115b98d791dcb1c1cd1a642349cf";
  $client = new Client($sid, $token);
  $client->messages->create(
  "+16518675309",
  array(
  'from' => "+14158141829",
  'body' => "Tomorrow's forecast in Financial District, San Francisco is Clear.",
  'mediaUrl' => "https://climacons.herokuapp.com/clear.png",
  )
  );
  }

public function textmagic_api($form_no,$to_no){

  // Get the PHP helper library from twilio.com/docs/php/install
  require_once '/path/to/vendor/autoload.php'; // Loads the library
  use Twilio\Rest\Client;
  // Your Account Sid and Auth Token from twilio.com/user/account
  $sid = "AC093901d77ee48f659b4309d83a5ad950";
  $token = "b300115b98d791dcb1c1cd1a642349cf";
  $client = new Client($sid, $token);
  $client->messages->create(
  "+16518675309",
  array(
  'from' => "+14158141829",
  'body' => "Tomorrow's forecast in Financial District, San Francisco is Clear.",
  'mediaUrl' => "https://climacons.herokuapp.com/clear.png",
  )
  );
  }

 public function nexmo_integration()
{
$key = "226a930f";
$secret = "56231124c516302f";
}
*/

  public function bulkArchive(){  
    $id_value=explode(',',$_POST['data_ids']);
    for($i=0;$i<count($id_value);$i++){
      $data=array('status'=>1);
        $this->Common_mdl->update('department_permission',$data,'id',$id_value[$i]);
    }
    echo 1;
  }

    public function bulkUnarchive(){  
    $id_value=explode(',',$_POST['data_ids']);
    for($i=0;$i<count($id_value);$i++){
        $data=array('status'=>0);
        $this->Common_mdl->update('department_permission',$data,'id',$id_value[$i]);
    }
    echo 1;
  }

   
}


?>
