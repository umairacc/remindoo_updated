<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Minvoice extends CI_Controller {
public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
   public function __construct(){
        parent::__construct();
                    // Load form helper library
                    $this->load->helper('form');

                    // Load form validation library
                    $this->load->library('form_validation');

                    // Load session library
                    $this->load->library('session');
                    
                    $this->load->database();

        $this->load->model(array('Common_mdl','Security_model','Invoice_model'));
    }


    //invoice generator
     public function invoice()
    {
       $this->load->model('Invoice_model');
       $data['records'] = $this->Invoice_model->getAllRecords();
       $this->load->view('iv/binvoice',$data); 
    }

    public function invoicedetails(){
         $this->load->model('Invoice_model');
         $data['records'] = $this->Invoice_model->invoiceRecord();
        $this->load->view("iv/invoice_details",$data);
    }

    public function repeat_invoice()
    {
       $this->load->view('iv/repeatinv'); 
    }

     public function insert()
                {
                    $this->load->library('email');
                    $config = array(
 'mailtype' => 'html',
 'charset' => 'iso-8859-1',
 'priority' => '1'
  );
$this->email->initialize($config);

                    $invoicedetails = array (
                        'clientname' => $this->input->post('clientname'),
                        'invoicedate' => $this->input->post('invoicedate'),
                        'duedate'=> $this->input->post('duedate'),
                        'invoiceNo' => $this->input->post('invoiceNo'),
                        'refer' => $this->input->post('refer'),
                        'amountstype' => $this->input->post('amountstype'),
                        'createdtime' =>time()
                        );
                    
                    
                        $insert=$this->Invoice_model->insertinvoice($invoicedetails);                                           
                      
                    $invoiceDate = $this->input->post('invoicedate');
                    $invoiceNo = $this->input->post('invoiceNo');
                    $refer = $this->input->post('refer');                                                       
                    $item = $this->input->post('item');
                    $description = $this->input->post('descr');
                    $price = $this->input->post('price');
                    $quantity = $this->input->post('quantity');
                    $total = $this->input->post('total');
                        
                        foreach ($item as $key => $value) 
                            {                            
                                $productdetails = array(
                                    'invoiceDate' => $invoiceDate,
                                    'invoiceNo' => $invoiceNo,
                                    'refer' => $refer,                                  
                                    'item' => $item[$key],
                                    'description' => $description[$key],
                                    'price' => $price[$key],
                                    'quantity' => $quantity[$key],
                                    'total' => $total[$key],
                                );
                                $insert=$this->Invoice_model->insertproduct($productdetails); 
                            }

                            

                    $invoiceDate = $this->input->post('invoicedate');
                    $invoiceNo = $this->input->post('invoiceNo');
                    $refer = $this->input->post('refer');    

                    $subtotal = $this->input->post('data[Invoice][invoice_subtotal]');
                    // $totalQuantity = $this->input->post('data[Invoice][Invoice_quantity]');
                    $tax = $this->input->post('data[Invoice][tax]');
                    $taxAmount = $this->input->post('data[Invoice][taxAmount]');
                    $totalAftertax = $this->input->post('data[Invoice][invoice_total]');
                    // $amountPaid = $this->input->post('data[Invoice][amount_paid]');
                    // $amountDue = $this->input->post('data[Invoice][amount_due]');
                    $status = $this->input->post('state');

                        $calculation = array(
                            'invoicedate' => $invoiceDate,
                            'invoiceNo' => $invoiceNo,
                            'refer' => $refer,

                            'subtotal' => $subtotal,
                            'tax' => $tax,
                            'taxAmount' => $taxAmount,
                            'totalAftertax' => $totalAftertax,
                            'state' => $status,
                            );
                       
                        $insert=$this->Invoice_model->insertcalculation($calculation); 

                        $this->email->from('only4ututorials@gmail.com'); // change it to yours
             $this->email->to($this->input->post('clientname')); // change it to yours
             $this->email->subject($this->input->post('invoicedate'));


             $invoiceNo=$this->input->post('invoiceNo');
             $datas['invoice'] = $this->Invoice_model->show_invoice_data($invoiceNo);                    
            $datas['product'] = $this->Invoice_model->show_product_data($invoiceNo);
            $datas['calculation'] = $this->Invoice_model->show_calculation_data($invoiceNo); 

             $body = $this->load->view('iv/invoice_information',$datas,TRUE);
             $this->email->message($body); 

             if ($this->email->send()) {
                 redirect('Minvoice/invoicedetails');   
                 return true;
             } else {
                 show_error($this->email->print_debugger());
             }
                        // $this->invoice();     
                                                                                                                         
                }

                public function edit($invoiceNo)
                {                 

                    $datas['invoice'] = $this->Invoice_model->show_invoice_data($invoiceNo);                    
                    $datas['product'] = $this->Invoice_model->show_product_data($invoiceNo);
                    $datas['calculation'] = $this->Invoice_model->show_calculation_data($invoiceNo);                    
                    $this->load->view('iv/invoice_edit', $datas);
                }

                

                public function delete($invoiceNo) 
                {                               
                    $datas['invoice'] = $this->Invoice_model->show_invoice_data($invoiceNo);                    
                    $datas['product'] = $this->Invoice_model->show_product_data($invoiceNo);
                    $datas['calculation'] = $this->Invoice_model->show_calculation_data($invoiceNo);                    
                    $this->Invoice_model->delete_invoice($invoiceNo,$datas);
                    $this->Invoice_model->delete_product($invoiceNo,$datas);
                    $this->Invoice_model->delete_calculation($invoiceNo,$datas);
                    redirect('Minvoice/invoicedetails');
                }

                public function info($invoiceNo)
                {                   
                    $datas['invoice'] = $this->Invoice_model->show_invoice_data($invoiceNo);                    
                    $datas['product'] = $this->Invoice_model->show_product_data($invoiceNo);
                    $datas['calculation'] = $this->Invoice_model->show_calculation_data($invoiceNo);                    
                    $this->load->view('iv/invoice_information', $datas);
                }

                public function update($invoiceNo)
                {
                    
                    $invoicedetails = array (
                        'clientname' => $this->input->post('clientname'),
                        'invoicedate' => $this->input->post('invoicedate'),
                        'duedate'=> $this->input->post('duedate'),
                        'invoiceNo' => $this->input->post('invoiceNo'),
                        'refer' => $this->input->post('refer'),
                        'amountstype' => $this->input->post('amountstype'),
                        'createdtime' => time()
                    );

            $update=$this->Invoice_model->update_invoice($invoiceNo,$invoicedetails);


                    $invoiceDate = $this->input->post('invoicedate');
                    $invoiceNo = $this->input->post('invoiceNo');
                    $refer = $this->input->post('refer');                                                       
                    $item = $this->input->post('item');
                    $description = $this->input->post('descr');
                    $price = $this->input->post('price');
                    $quantity = $this->input->post('quantity');
                    $total = $this->input->post('total');
                    $id = $this->input->post('rno');

                    


                        if(isset($item))
                            {
                                foreach ($item as $key => $value) 
                                {
                                    if (isset($id[$key])) 
                                        {
                                            $productdetails = array(
                                                'id' => $id[$key],
                                                'invoiceDate' => $invoiceDate,
                                                'invoiceNo' => $invoiceNo,
                                                'refer' => $refer,                                  
                                                'item' => $item[$key],
                                                'description' => $description[$key],
                                                'price' => $price[$key],
                                                'quantity' => $quantity[$key],
                                                'total' => $total[$key]
                                            );                                         

                          $this->Invoice_model->update_product($invoiceNo,$id[$key],$productdetails); 
                                        }
                                    else
                                        {
                                            $productdetails = array(                                
                                                'invoiceDate' => $invoiceDate,
                                                'invoiceNo' => $invoiceNo,
                                                'refer' => $refer,                                  
                                                'item' => $item[$key],
                                                'description' => $description[$key],
                                                'price' => $price[$key],
                                                'quantity' => $quantity[$key],
                                                'total' => $total[$key]
                                            );

                                                $this->Invoice_model->insertproduct($productdetails); 
                                        }
                                }
                            }
                        else
                            {
                                
                            }
                                    
                    $invoiceDate = $this->input->post('invoicedate');
                    $invoiceNo = $this->input->post('invoiceNo');
                    $refer = $this->input->post('refer');  

                    $subtotal = $this->input->post('data[Invoice][invoice_subtotal]');
                    $tax = $this->input->post('data[Invoice][tax]');
                    $taxAmount = $this->input->post('data[Invoice][taxAmount]');
                    $totalAftertax = $this->input->post('data[Invoice][invoice_total]');
                    $status = $this->input->post('state');

                    
                        $calculation = array(
                            'invoicedate' => $invoiceDate,
                            'invoiceNo' => $invoiceNo,
                            'refer' => $refer,

                            'subtotal' => $subtotal,
                            'tax' => $tax,
                            'taxAmount' => $taxAmount,
                            'totalAftertax' => $totalAftertax,
                            'state' => $status
                            );
                                
                $this->Invoice_model->update_calculation($invoiceNo,$calculation);
                $this->edit($invoiceNo);                                                    
                                                                                
                }
//end invoice
}
?>
