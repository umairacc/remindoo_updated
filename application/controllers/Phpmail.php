<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Phpmail extends CI_Controller{

    function  __construct(){
        parent::__construct();
    }

    function index(){
        // Load PHPMailer library
        $this->load->library('phpmailer_lib');

        // PHPMailer object
        $mail = $this->phpmailer_lib->load();

        // SMTP configuration
        $mail->isSMTP();
        $mail->Host     = 'localhost';
        $mail->SMTPAuth = false;
        $mail->Username = 'w.s@accotax.co.uk';
        $mail->SMTPSecure = 'false';
        $mail->Port     = 25;

        $mail->setFrom('w.s@accotax.co.uk', 'Techleaf');
        //$mail->addReplyTo('info@example.com', 'CodexWorld');

        // Add a recipient
        $mail->addAddress('sandy.techleaf@gmail.com');

        // Add cc or bcc 
        //$mail->addCC('cc@example.com');
        //$mail->addBCC('bcc@example.com');

        // Email subject
        $mail->Subject = 'Send Email via SMTP using PHPMailer in CodeIgniter';

        // Set email format to HTML
        $mail->isHTML(true);

        // Email body content
        $mailContent = "<h1>Send HTML Email using SMTP in CodeIgniter</h1>
            <p>This is a test email sending using SMTP mail server with PHPMailer.</p>";
        $mail->Body = $mailContent;

        // Send email
        if(!$mail->send()){
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        }else{
            echo 'Message has been sent';
        }
    }

}
?>
?>