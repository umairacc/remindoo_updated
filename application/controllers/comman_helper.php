<?php

function GetAssignees_SelectBox( $id=0 , $data_value='')
{ 
  $CI =& get_instance();  
  $CHILDS = [];

      $DB_DATA = $CI->db->query("select * from organisation_tree where firm_id = ".$_SESSION['firm_id']." and  parent_id=".$id." ")->result_array();  

    
      foreach( $DB_DATA as $val )
      {
        $CHILD_DATA = [];

        $user_info = $CI->db->query("select crm_name from user where id=".$val['user_id'])->row_array();
        
         if(empty( $data_value ))
          {
            $value = $val['id'];
          }
          else
          {
            $value = $data_value.':'.$val['id'];
          }
            $CHILD_DATA['id'] = $value.'_'.$val['user_id'];
            $CHILD_DATA['title'] = $user_info['crm_name'];

            $hasChild = GetAssignees_SelectBox( $val['id'] , $value );

        if( !empty( $hasChild ) )
        {
          $CHILD_DATA['subs']= $hasChild;
        }
        $CHILDS[]= $CHILD_DATA;
      }
      return $CHILDS;
}
function Get_Assigned_Datas( $Module ,$user_id = 0 ){

  $CI =& get_instance();
  
  if(!$user_id){
    $user_id = $_SESSION['userId'];
  } 
  
  $ids        = $CI->db->query("SELECT id FROM organisation_tree WHERE user_id=$user_id LIMIT 1")->result_array();
  $Module_ids = [];
  
  foreach ($ids as $key => $value){
    $data = $CI->db->query("SELECT module_id FROM firm_assignees WHERE module_name='$Module' AND (CONCAT(':',assignees,':') REGEXP ':".$value['id'].":') ")->result_array();

    $Module_ids=array_merge($Module_ids,array_column($data, 'module_id'));
    //print_r(array_column($data, 'module_id'));
  }
  return array_unique( $Module_ids );
    
  /*$sql = "SELECT DISTINCT(FA.module_id)
          FROM organisation_tree AS OT
          INNER JOIN firm_assignees AS FA
              ON FA.module_name='$Module' 
                 AND OT.id IN(REPLACE(FA.assignees, ':', ','))
          WHERE OT.user_id=$user_id";

  $data = $CI->db->query($sql)->result_array();
  return $data;*/
}


function find_underlevel_users($role,$user_id)
  {	$break=0;
    do{
      if($break==1000)break;
      $CI =& get_instance();
      $data=$CI->db->query("select * from assign_member where assign_to_id IN ($user_id)")->result_array();
      //echo "<pre>";print_r($data);die;
      $t='';
      $atr='';
      foreach ($data as $v) 
      {
        if($role==($v['assign_to_roll']+1))
        {
          $t.=($t=='')?$v['members_id']:",".$v['members_id'];
        }
        else 
        {
          $atr.=($atr==''?$v['members_id']:",".$v['members_id']);
        }  
      }
      $user_id=$atr;
     $break++;
   }while($t=='');
  
  return $CI->db->query("select * from user where id IN ($t)")->result_array();
  }

 
function getMenuURL( $id )
{ 
  $CI =& get_instance();
  
  $url="javascript::void(0)";

  if ( !empty( $id ) )
    {
      
      $url = $CI->db->query(" SELECT url FROM `module_mangement` where module_mangement_id = $id ")->row_array();
      
      if ( strpos( $url['url'] , "#" ) === 0 )        
      {
        $url = $url['url'];
      
      }
      else
      {
        $url = base_url().$url['url'];
      }

    }

    return $url;
}

function getHeader( $PARENT_LI , $CHILD_UL , $CHILD_LI )
{ 
  $CI =& get_instance();

  $menus = $CI->db->query(" SELECT `permited_menus` FROM `user` where id = ".$_SESSION['id'])->row_array();
  $menus = json_decode( $menus['permited_menus'] , TRUE );
  //echo '<pre>';print_r($menus);die;
  $menu_data = '';

  foreach ($menus as $datas)
  {
    
    $url = getMenuURL( $datas['url'] );
    
    $ParentLi = str_replace('{URL}', $url , $PARENT_LI );  
    $ParentLi = str_replace('{ICON_PATH}', $datas['icon_path'] , $ParentLi );  
    $ParentLi = str_replace('{MENU_LABEL}', strtoupper( $datas['label'] ) , $ParentLi );  

    $childCnt ='';

    if( isset( $datas['child'] ) )
    {
      $childCnt = recursive_menu( $datas['child'] , $CHILD_UL , $CHILD_LI );
    }

//echo htmlspecialchars($ParentLi)."<br>";
//echo  htmlspecialchars($childCnt)."child<br>";

    $ParentLi = str_replace('{CHILD_CONTENT}', $childCnt , $ParentLi ); 




    $menu_data.=$ParentLi; 
  }

 // echo htmlspecialchars( $menu_data ); 

  return $menu_data;

}


function recursive_menu( $Child_menu , $CHILD_UL , $CHILD_LI )
{
  
  $CI =& get_instance();

  $menu_data = '';

  foreach ($Child_menu as $datas)
  {
                          
    $url = getMenuURL( $datas['url'] );

    $ChildLi = str_replace('{URL}', $url , $CHILD_LI );  
    $ChildLi = str_replace('{MENU_LABEL}', strtoupper( $datas['label'] ) , $ChildLi );  

    $childCnt ='';

     if( isset( $datas['child'] ) )
    {
      $childCnt = recursive_menu( $datas['child'] , $CHILD_UL , $CHILD_LI );
    }

    $ChildLi = str_replace('{CHILD_CONTENT}', $childCnt , $ChildLi ); 

    $menu_data.= $ChildLi; 

  }
    
  $menu_data = str_replace('{CHILD_CONTENT}', $menu_data , $CHILD_UL );  
   
  return  $menu_data;
}
function templateDecode( $tem , $data )
{
  echo "";
}

function dateDiffrents( $D1 , $D2 )
{
    $d1 = date_create( $D1 );
    $d2 = date_create( $D2 );
    $interval=date_diff( $d1 ,$d2 );
          
    return intval( $interval->format('%R%a') );
}

function Firm_column_settings($table)

{
  $CI =& get_instance();
  $firm_id = "firm".'0';
  
  $column_setting = $CI->db->query("select name,visible_".$firm_id.",order_".$firm_id.",cusname_".$firm_id." from firm_column_settings where table_name='".$table."' order by order_".$firm_id." ASC")->result_array();

  $hidden_coulmns = array_keys( array_column( $column_setting , 'visible_'.$firm_id , 'name' ) , 0 );

  $column_order = array_column( $column_setting , 'name' );

  $hidden_coulmns = array_map(function ($a){return trim( $a ).'_TH';}, $hidden_coulmns );

  $column_order = array_map(function ($a){return trim( $a ).'_TH';}, $column_order );

  $return['hidden'] = json_encode( $hidden_coulmns );

  $return['order'] = json_encode( $column_order );

  return $return;
}

function SuperAdmin_column_settings($table)

{
  $CI =& get_instance();

  
  $column_setting = $CI->db->query("select name,visible,orders,cusname from super_admin_column_settings where table_name='".$table."' order by orders ASC")->result_array();

    
  $hidden_coulmns = array_keys( array_column( $column_setting , 'visible' , 'name' ) , 0 );

  $column_order = array_column( $column_setting , 'name' );

  $hidden_coulmns = array_map(function ($a){return trim( $a ).'_TH';}, $hidden_coulmns );

  $column_order = array_map(function ($a){return trim( $a ).'_TH';}, $column_order );

  $return['hidden'] = json_encode( $hidden_coulmns );

  $return['order'] = json_encode( $column_order );

  return $return;
}

?>
