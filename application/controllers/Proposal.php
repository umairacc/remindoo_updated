<?php 
//error_reporting(E_STRICT);
class Proposal extends CI_Controller 
{
        public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";

		public function __construct()
        {
            parent::__construct();
            $this->load->model(array('Common_mdl','Proposal_model','Security_model'));           
            // $this->load->library('Excel');
		}

        public function index()
        { 
            $this->Security_model->chk_login();

            $user_id=$this->session->userdata['userId'];  
            /*$data['send']=$this->Proposal_model->proposal_details('sent');
            $data['opened']=$this->Proposal_model->proposal_details('opened');
            $data['indiscussion']=$this->Proposal_model->proposal_details('in discussion');
            $data['accepted']=$this->Proposal_model->proposal_details('accepted');
            $data['decline']=$this->Proposal_model->proposal_details('declined');
            $data['draft']=$this->Proposal_model->proposal_details('draft');*/
        	  $data['proposal']=$this->Proposal_model->proposals();     
           
            $data['admin_settings']=$this->Common_mdl->select_record('admin_setting','user_id',$user_id);
           
            $this->load->view('proposal/proposal',$data);
            unset($_SESSION['firm_seen']);
        }
        /*Shashmethah*/
        public function all_data()
        { 
            $this->Security_model->chk_login();
           
            /*$data['send']=$this->Proposal_model->proposal_details('sent');
            $data['opened']=$this->Proposal_model->proposal_details('opened');
            $data['indiscussion']=$this->Proposal_model->proposal_details('in discussion');
            $data['accepted']=$this->Proposal_model->proposal_details('accepted');
            $data['decline']=$this->Proposal_model->proposal_details('declined');
            $data['draft']=$this->Proposal_model->proposal_details('draft');*/
            $data['proposal']=$this->Proposal_model->proposals();       
             
            $this->load->view('proposal/proposal_success_msg', $data); 
        }

        public function checkdatetime_weeks()
        {  
            $current_date = date('Y-m-d');
            $week = date('W', strtotime($current_date));
            $year = date('Y', strtotime($current_date));

            $dto = new DateTime();
            $start_date = $dto->setISODate($year, $week, 0)->format('Y-m-d');
            $end_date = $dto->setISODate($year, $week, 6)->format('Y-m-d');
            $end_date = date('Y-m-d', strtotime($end_date . ' +1 day'));  

            /*$data['send'] = $this->Proposal_model->proposal_details_date("sent",$start_date,$end_date);
            $data['opened'] = $this->Proposal_model->proposal_details_date("opened",$start_date,$end_date);
            $data['indiscussion'] = $this->Proposal_model->proposal_details_date("in discussion",$start_date,$end_date);
            $data['accepted'] = $this->Proposal_model->proposal_details_date("accepted",$start_date,$end_date);
            $data['decline'] = $this->Proposal_model->proposal_details_date("declined",$start_date,$end_date);
            $data['draft'] = $this->Proposal_model->proposal_details_date("draft",$start_date,$end_date);*/
            $data['proposal'] = $this->Proposal_model->proposal_details_date("",$start_date,$end_date);                

            $this->load->view('proposal/proposal_success_msg', $data);
        }

        public function CheckdateTime_threeweeks()
        {             
            $current_date = date('Y-m-d');
            $weeks = strtotime('- 3 week', strtotime( $current_date ));
            $start_Week = date('Y-m-d', $weeks); 
            $addweeks = strtotime('+ 3 week', strtotime( $start_Week ));
            $end_Week = date('Y-m-d', $addweeks); 
            $end_Week = date('Y-m-d', strtotime($end_Week . ' +1 day'));
            
            /*$data['send'] = $this->Proposal_model->proposal_details_date("sent",$start_Week,$end_Week);
            $data['opened'] = $this->Proposal_model->proposal_details_date("opened",$start_Week,$end_Week);
            $data['indiscussion'] = $this->Proposal_model->proposal_details_date("in discussion",$start_Week,$end_Week);
            $data['accepted'] = $this->Proposal_model->proposal_details_date("accepted",$start_Week,$end_Week);
            $data['decline'] = $this->Proposal_model->proposal_details_date("declined",$start_Week,$end_Week);
            $data['draft'] = $this->Proposal_model->proposal_details_date("draft",$start_Week,$end_Week);*/
            $data['proposal'] = $this->Proposal_model->proposal_details_date("",$start_Week,$end_Week); 
                
            $this->load->view('proposal/proposal_success_msg', $data);
        }

        public function CheckdateTime_Month()
        { 
            $last_month_start = date("Y-m-01");
            $last_month_end = date("Y-m-t"); 
            $last_month_end = date('Y-m-d', strtotime($last_month_end . ' +1 day'));
           
           /* $data['send'] = $this->Proposal_model->proposal_details_date("sent",$last_month_start,$last_month_end);
            $data['opened'] = $this->Proposal_model->proposal_details_date("opened",$last_month_start,$last_month_end);
            $data['indiscussion'] = $this->Proposal_model->proposal_details_date("in discussion",$last_month_start,$last_month_end);
            $data['accepted'] = $this->Proposal_model->proposal_details_date("accepted",$last_month_start,$last_month_end);
            $data['decline'] = $this->Proposal_model->proposal_details_date("declined",$last_month_start,$last_month_end);
            $data['draft'] = $this->Proposal_model->proposal_details_date("draft",$last_month_start,$last_month_end);*/
            $data['proposal'] = $this->Proposal_model->proposal_details_date("",$last_month_start,$last_month_end); 

            $this->load->view('proposal/proposal_success_msg', $data);        
        }

        public function CheckdateTime_threeMonth()
        {
            $curr_date = date('Y-m-d',strtotime(date('Y-m-d').'+1 day'));
            $start_month = date('Y-m-d',strtotime('- 3 month', strtotime($curr_date)));            
            
            /*$data['send'] = $this->Proposal_model->proposal_details_date("sent",$start_month,$curr_date);
            $data['opened'] = $this->Proposal_model->proposal_details_date("opened",$start_month,$curr_date);
            $data['indiscussion'] = $this->Proposal_model->proposal_details_date("in discussion",$start_month,$curr_date);
            $data['accepted'] = $this->Proposal_model->proposal_details_date("accepted",$start_month,$curr_date);
            $data['decline'] = $this->Proposal_model->proposal_details_date("declined",$start_month,$curr_date);
            $data['draft'] = $this->Proposal_model->proposal_details_date("draft",$start_month,$curr_date);*/
            $data['proposal'] = $this->Proposal_model->proposal_details_date("",$start_month,$curr_date); 

            $this->load->view('proposal/proposal_success_msg', $data); 
        }

        public function Date_filter()
        {      
            $this->Security_model->chk_login();

            $fromdate = date('Y-m-d',strtotime( $_POST['fromdate'] ) );
            $todate   = date('Y-m-d',strtotime( $_POST['todate'] ) ); 
             
            /*$data['send']=$this->Proposal_model->proposal_details_date('sent',$fromdate,$todate);
            $data['opened']=$this->Proposal_model->proposal_details_date('opened',$fromdate,$todate);
            $data['indiscussion']=$this->Proposal_model->proposal_details_date('in discussion',$fromdate,$todate);
            $data['accepted']=$this->Proposal_model->proposal_details_date('accepted',$fromdate,$todate);
            $data['decline']=$this->Proposal_model->proposal_details_date('declined',$fromdate,$todate);
            $data['draft']=$this->Proposal_model->proposal_details_date('draft',$fromdate,$todate);*/
            $data['proposal']=$this->Proposal_model->proposal_details_date('',$fromdate,$todate);             

            $this->load->view('proposal/proposal_success_msg',$data);
        }

        /* Proposal Template View File */
        public function template()
        {
            $this->Security_model->chk_login();
            
            $data['result'] = $this->db->query('select * from proposal_template WHERE firm_id = "'.$_SESSION['firm_id'].'"')->result_array();  
           
            $data['email_type'] = $this->getEmailactions();    
        	$data['templates'] = $this->Common_mdl->getTemplates();

            $this->load->view('proposal/proposal_template',$data);
        }

        public function getEmailactions()
        {
           $email_actions = $this->db->query("SELECT * FROM email_templates_actions WHERE module = 'proposal'")->result_array();
           $email_type = array();

           foreach($email_actions as $value)
           {
              $email_type[$value['module']][$value['action_id']]  =  $value['action'];
           }

           return $email_type;
        }

        public function get_template()
        { 
           $template = array();
           $id = $_POST['id'];
           $template = $this->Common_mdl->select_record('email_templates','id',$id); 
           
           $template['subject'] = html_entity_decode($template['subject']);
           $template['body'] = html_entity_decode($template['body']);

           echo json_encode($template);       
        }

        public function deletetemplate()
        { 
            if(isset($_POST['data_id']))
            {
                $id = trim($_POST['data_id']);
                $sql = $this->db->query("DELETE FROM proposal_template WHERE id in ($id)");
                echo $sql;
            }  
        }

        public function getemail_titles()
        {
            $titles = "";
            $action_id = $_POST['action_id'];

            $records = $this->db->query('SELECT * FROM email_templates WHERE firm_id IN("'.$_SESSION['firm_id'].'","0") AND status = "1" AND reminder_heading IS NULL AND action_id = "'.$action_id.'"')->result_array();
                                
            if($_SESSION['firm_id'] != 0)
            {
               $record = $this->db->query("SELECT super_admin_owned FROM email_templates WHERE firm_id = '".$_SESSION['firm_id']."' AND reminder_heading IS NULL AND status = '1' AND action_id = '".$action_id."'")->result_array();
            
               foreach ($records as $key => $value) 
               {
                  foreach ($record as $value1) 
                  {
                     if($value['id'] == $value1['super_admin_owned'])
                     {
                        unset($records[$key]);
                     }
                  }
               }
            }

            $titles  ="<div class='col-xs-12 col-md-5'><label>Template</label></div><div class='col-xs-12 col-md-7'><select name='template' id='template'> <option value=''>Select</option>";

            foreach($records as $key => $value)
            {                         
                $titles.="<option value='".$value['id']."'>".ucwords($value['title'])."</option>";
            }           
             
            $titles.= "</select></div>";
            echo $titles;                
        }
         /*Shashmethah*/
        /* Proposal  View File */
         public function proposal($id)
         {  
             error_reporting(0);
             $value=explode( "---", $id );            
             $values=end($value); 
             $status_check=$this->Common_mdl->select_record('proposals','id',$values);
             $proposal_name=$status_check['proposal_name'];
             $company_name=$status_check['company_name'];           
             $sender_id=$status_check['user_id'];      
             $data['records'] = $status_check; 
             $user_details=$this->Common_mdl->select_record('user','id',$sender_id);
             $email=$user_details['crm_email_id']; 
             $random_string=$this->generateRandomString();

             if($status_check['status']=='sent')
             {
                $up=array('status'=>'opened');
                $this->Common_mdl->update('proposals',$up,'id',$values);

                $datas=array('proposal_id'=>$values,'tag'=>'viewed','created_at'=>date('Y-m-d h:i:s'));
                $inse=$this->Common_mdl->insert('proposal_history',$datas);                

                $body           =   '<p>Congratulations!</p><br/><p>' .$proposal_name.'proposal was viewed by'.$company_name.' at'.date("Y-m-d h:i:s").'</p><br/><br/><p>Use Following link to open proposal '.base_url().'Proposal_page/step_proposal/'.$random_string.'---'.$values.'</p> <br/> Thanks';

                $email_subject  = '<p>'.$proposal_name.' proposal was viewed by '.$company_name.'</p>';
                $email_subject  = preg_replace('/ {2,}/', ' ', str_replace('&nbsp;', ' ', strip_tags($email_subject)));

                firm_settings_send_mail( $user_details['firm_id'] , $status_check['sender_company_mailid'] , $email_subject , $body );
             }

             if($status_check['status']=='expired')
             {
                $this->load->view('proposal/expirednotification',$data);
             }
             else
             {
                $this->load->view('proposal/proposal_view',$data);
             }
        }
        /* Proposal  View File */
         public function proposal_feedback($values)
         {    error_reporting(-1);  
             $data['records']=$this->Common_mdl->select_record('proposals','id',$values);
             $data['conversation']=$this->Common_mdl->GetAllWithWhereorderBy('proposal_discussion','proposal_id', $values);
             $this->load->view('proposal/proposal_feedback',$data);
        }

        /* Proposal template insert */
        public function insert_template(){  
            $id=$_POST['templateID'];         


            if (strpos($_POST['template_content'], '[Price Table]') !== false) {
            //echo 'true';
                $template_content=$_POST['template_content'];
            }else{
                $template_content='<table class="propdf" style="background-color:#ffffff; width:600px;padding-left:20px" vspace="0" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                    <thead>
                        <tr>
                            <td id="dd-head" style="vertical-align:top;" class="ui-sortable">'.$_POST['template_content'].'</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td id="dd-body" style="vertical-align:top;" class="ui-sortable"><span class="price_table">[Price Table]</span </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td id="dd-footer" style="vertical-align:top;" class="ui-sortable"></td>
                        </tr>
                    </tfoot>
                </table>';
            }
            
            $email_actions = $this->db->query("SELECT * FROM email_templates_actions WHERE module = 'proposal'")->result_array();
     
            $template_type = "";

            foreach($email_actions as $key => $value) 
            {
                if($value['action_id'] == $_POST['template_type'])
                {
                    $template_type =  ucwords(str_replace('_', ' ', $value['action']));
                }
            }

            if($id != '') {
                $data=array('template_type'=>$template_type,
                         'template_title'=>$_POST['template_title'],
                         'template_content'=>$template_content,
                         'status'=>'active'
                        );    
                $data['value']=$this->Common_mdl->update('proposal_template',$data,'id',$id);
                $data['messsage']=1;
                echo json_encode($data);
            } else {
                $user_id=$this->session->userdata['userId'];         
                $value=array('template_type'=>$template_type,
                         'template_title'=>$_POST['template_title'],
                         'template_content'=>$template_content,
                         'status'=>'active',
                         'user_id'=>$user_id,
                         'firm_id'=>$_SESSION['firm_id']);    
                $data['value']=$this->Common_mdl->insert('proposal_template',$value);
                 $data['messsage']=0;
                echo json_encode($data);
            }          
                
        }
        /* Template Bulk Delete */
        public function template_delete(){   
            $checkbox=$_POST['checkbox']; 
            for($i=0;$i<count($checkbox);$i++){           
             $id=$checkbox[$i];  
             $delete=$this->Common_mdl->delete('proposal_template','id',$id);
            }
            $this->session->set_flashdata('success', 'Record Deleted Successfully!'); 
            redirect('Proposal/template');
        }
        /* Status Update */
        public function update_status(){
           $id=$_POST['id'];
           $task_status=$_POST['task_status'];
           $up=array('status'=>$_POST['task_status']);
           $data['result']=$this->Common_mdl->updateFields('proposal_template','id',$id,$up);
                if($data){
                   echo json_encode($data);      
                }else{
                   return false;
                }
        }
        /* Template Update View File */
        public function update_contents($id){
            $data['value']=$this->Common_mdl->select_record('proposal_template','id',$id);
            $this->load->view('proposal/edit_mail_content',$data);
        }
        /* Template Update */
        public function update_template(){           
            $id=$_POST['temp_ID'];
            $data=array('template_type'=>$_POST['temp_type'],
                         'template_title'=>$_POST['temp_title'],
                         'template_content'=>$_POST['temp_content1'],
                         'status'=>'active'
                        );    
           //print_r($data); 
         $value=$this->Common_mdl->update('proposal_template',$data,'id',$id);
         echo json_encode($value);
        }
        public function step_proposal(){        
            $this->Security_model->chk_login();
            $this->load->view('proposal/step_proposal_view');
        }

        public function fee_schedule()
        {    
            $this->Security_model->chk_login();       
            
            $data['category']=$this->Common_mdl->GetAllWithWhereorderBy('proposal_category','firm_id',$_SESSION['firm_id']);
            $category=$this->Common_mdl->GetAllWithWhere('proposal_category','firm_id',$_SESSION['firm_id']);
            
            $product_category=array();
            $service_category=array();
            $subscription_category=array();

            foreach($category as $cat)
            {
                if($cat['category_type']=='product'){
                     array_push($product_category,"'".$cat['category_name']."'");
                }

                 if($cat['category_type']=='service'){
                     array_push($service_category,"'".$cat['category_name']."'");
                }

                 if($cat['category_type']=='subscription'){
                     array_push($subscription_category,"'".$cat['category_name']."'");
                }
            }

            $subscription_category=implode(',',$subscription_category);
            $product_category=implode(',',$product_category);
            $service_category=implode(',',$service_category);

            $count1=explode(',',$subscription_category);
            $count2=explode(',',$product_category);
            $count3=explode(',',$service_category);

            $firm_services = $this->Common_mdl->getServicelist();
            $firm_enabled_services = array();        

            function firm_enabled($val)
            {          
               return $val['service_name'];
            }

            $firm_enabled_services = array_map('firm_enabled',$firm_services); 
            $firm_enabled_services = implode("','", $firm_enabled_services);

            if($count1['0']!='')
            {
               $data['subscription']=$this->db->query("select * from proposal_subscriptions where firm_id = '".$_SESSION['firm_id']."' and  subscription_category in (".$subscription_category.")")->result_array();
            }
            else
            {
               $data['subscription']=$this->db->query("select * from proposal_subscriptions where firm_id = '".$_SESSION['firm_id']."' and  ( subscription_category!='' and subscription_category!='0')")->result_array();
            }

            if($count3['0']!='')
            {
               // $data['services']=$this->db->query("select * from proposal_service where firm_id = '".$_SESSION['firm_id']."' and  service_category in (".$service_category.") AND service_name IN('".$firm_enabled_services."')")->result_array();

            	 $data['services']=$this->db->query("select * from proposal_service where firm_id = '".$_SESSION['firm_id']."' and  service_category in (".$service_category.") ")->result_array();

               //echo 's';
            }
            else
            {
               // $data['services']=$this->db->query("select * from proposal_service where firm_id = '".$_SESSION['firm_id']."' and ( service_category!='' and service_category!='0') AND service_name IN('".$firm_enabled_services."')")->result_array();
            	$data['services']=$this->db->query("select * from proposal_service where firm_id = '".$_SESSION['firm_id']."' and ( service_category!='' and service_category!='0') ")->result_array();
            }

            if($count2['0']!='')
            {
               $data['products']=$this->db->query("select * from proposal_products where firm_id = '".$_SESSION['firm_id']."' and  product_category in (".$product_category.")")->result_array();
            }
            else
            {
               $data['products']=$this->db->query("select * from proposal_products where firm_id = '".$_SESSION['firm_id']."' and ( product_category!='')")->result_array();
            }

            $data['taxes']=$this->Common_mdl->GetAllWithWhereorderBy('proposal_tax','firm_id',$_SESSION['firm_id']);          
            $data['price_list']=$this->Common_mdl->GetAllWithWhereorderBy('proposal_pricelist','firm_id',$_SESSION['firm_id']);

            // echo "<pre>";
            // print_r($service_category);
            // print_r($firm_enabled_services);
            // print_r($data['services']);


         //   exit;
            $this->load->view('proposal/fee_schedule_view',$data);
            unset($_SESSION['success']);            
        }

        /* Add Category in Fees Schedule */
        public function add_category()
        {  
            $user_id = $this->session->userdata['userId'];         
            $value = array('category_name'=>$_POST['category_name'],'status'=>'active','user_id'=>$user_id,'category_type'=>$_POST['category_type'],'firm_id' => $_SESSION['firm_id']);
            $datas = $this->Common_mdl->insert('proposal_category',$value);
            $id = $this->db->insert_id();
            $category=$this->db->query('select * from proposal_category where firm_id="'.$_SESSION['firm_id'].'" and `category_type`="'.$_POST['category_type'].'"')->result_array();             
            $data['category_list']=''; 
            $data['category_add']='';       

            $data['category_pop'] = '<div id="delete-category_service-'.$id.'" class="modal fade new-adds-categories" role="dialog"><div class="modal-dialog"><form class="form-horizontal" action="'.base_url().'proposal/delete_category" method="post" id="login_form"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">×</button><h4 class="modal-title">Delete Category</h4></div><div class="modal-body"><input type="hidden" name="checkbox" value="'.$id.'"><input type="hidden" name="type" value="'.$value['category_type'].'"> Do you want to delete?</div><div class="modal-footer"><button type="submit" id="submit" name="save" class="btn btn-primary button-loading">Yes</button><a href="#" data-dismiss="modal">no</a></div></div></form></div></div><div id="edit-category_service-'.$id.'" class="modal fade new-adds-categories edit_categories" role="dialog"><div class="modal-dialog"><form action="'.base_url().'proposal/edit_category" method="post" name="form" id="category_edit'.$id.'"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">×</button><h4 class="modal-title">Edit category</h4></div><div class="modal-body"><p id="category_success" style="display: none;color: #4CAF50"> Category Added  </p><div class="add-input-service"><input type="hidden" name="id" id="edit_category_id" value="'.$id.'"><label>Category name</label><input type="text" name="category_name" id="edit_category_name" required="required" value="'.$_POST['category_name'].'"></div><p id="category_error" style="display: none;color: red"> Category Required  </p></div><div class="modal-footer"><a href="javascript:;" data-dismiss="modal">Cancel</a><button type="button" name="save" id="'.$id.'" class="edit_service_category edit_category category_save" value="Save">Save</button></div></div></form></div></div>';

            foreach($category as $cat){ 
            $data['category_list'].='<li><a href="#">'.$cat['category_name'].'</a><a href="javascript:;" class="edit-wed1 icon-c2" data-toggle="modal" data-target="#edit-category_service-'.$cat['id'].'"><i class="fa fa-pencil fa-6" aria-hidden="true"></i></a>
             <a href="javascript:;" class="edit-wed1 icon-c3" data-toggle="modal" data-target="#delete-category_service-'.$cat['id'].'"><i class="icofont icofont-ui-delete" aria-hidden="true"></i></a></li>';
            } 


            if($_POST['category_type']=='service'){
                $data['category_add'].='<select name="service_category" id="service_category" required="required">
                <option value="">Select </option>';
                foreach($category as $cat){ 
                $data['category_add'].='<option value="'.$cat['id'].'">'.$cat['category_name'].'</option>';
                } 
                $data['category_add'].='</select>';

            }


            if($_POST['category_type']=='product'){
                $data['category_add'].='<select name="product_category" id="product_category" required="required">
                <option value="">Select </option>';
                foreach($category as $cat){ 
                $data['category_add'].='<option value="'.$cat['id'].'">'.$cat['category_name'].'</option>';
                } 
                $data['category_add'].='</select>';

            }

            if($_POST['category_type']=='subscription'){
                $data['category_add'].='<select name="subscription_category" id="subscription_category" required="required">
                <option value="">Select </option>';
                foreach($category as $cat){ 
                $data['category_add'].='<option value="'.$cat['id'].'">'.$cat['category_name'].'</option>';
                } 
                $data['category_add'].='</select>';

            }     

            echo json_encode($data);
        }
        /* Add Service in Fees Schedule */
        public function add_service()
        {
            $user_id=$this->session->userdata['userId'];  
            $this->session->unset_userdata('tab_name');

            $service_category=$_POST['service_category'];
            $service_category_name=$this->db->query("select category_name from proposal_category where id='".$service_category."' and  category_type='service'")->row_array();
            $service_category_name=$service_category_name['category_name'];

            if($_POST['service_name'] == 'other')
            {
               $_POST['service_name'] = $_POST['other_service_name'];

               $exist = $this->db->query('SELECT id,firm_id,count(*) as count FROM service_lists WHERE service_name LIKE "%'.$_POST['service_name'].'%" GROUP BY id,firm_id')->row_array();

               if($exist['count'] == '0')
               {
                   $firm_service = array('service_name' => ucwords($_POST['service_name']),'services_subnames' => strtolower(str_replace(' ', '_', $_POST['service_name'])), 'firm'.$_SESSION['firm_id'].'_price' => $_POST['service_price'], 'firm_id' => json_encode(array($_SESSION['firm_id'])));

                   $this->Common_mdl->insert('service_lists',$firm_service);
                   $service_id = $this->db->insert_id();

                   $this->db->query('ALTER TABLE client ADD COLUMN crm_'.strtolower(str_replace(' ', '_', $_POST['service_name'])).'_email_remainder TEXT NULL, ADD COLUMN crm_'.strtolower(str_replace(' ', '_', $_POST['service_name'])).'_add_custom_reminder TEXT NULL, ADD COLUMN crm_'.strtolower(str_replace(' ', '_', $_POST['service_name'])).'_statement_date TEXT NULL,ADD COLUMN '.strtolower(str_replace(' ', '_', $_POST['service_name'])).' TEXT NULL IF NOT EXISTS');  
               }
               else
               {
                  $service_id = $exist['id'];
                  $firm_ids = json_decode($exist['firm_id'],true);  
                  $firm_ids[count($firm_ids)] = $_SESSION['firm_id'];

                  $this->db->update('service_lists',['firm_id' => json_encode($firm_ids)],'id = '.$service_id.'');
               }

               $services = $this->Common_mdl->get_price('admin_setting','firm_id',$_SESSION['firm_id'],'services');
               $datas = json_decode($services,true); 
               $datas[$service_id] = 1;

               $this->db->update('admin_setting',['services' => json_encode($datas)],'firm_id = '.$_SESSION['firm_id'].'');
            }

            $value = array('service_name'=>$_POST['service_name'],'service_price'=>$_POST['service_price'],'service_unit'=>$_POST['service_unit'],'service_description'=>$_POST['service_description'],'service_qty'=>$_POST['service_qty'],'service_category'=>$service_category_name,'service_category_id'=>$_POST['service_category'],'service_scode' => $_POST['service_scode'],'status'=>'active','user_id'=>$user_id,'firm_id' => $_SESSION['firm_id']);  
              
            if(isset($service_id) && !empty($service_id))
            {
              $value['service_lists_id'] = $service_id;          
            }

            $data=$this->Common_mdl->insert('proposal_service',$value);
            $this->session->set_flashdata('success', 'Service Added Successfully!'); 
            redirect('Proposal/fee_schedule');
        }
        /* Import Services */
        public function import_service()
        { 
            $user_id=$this->session->userdata['userId'];  
            $this->session->unset_userdata('tab_name');
            $this->session->set_userdata('tab_name', 'start_tab');
            $file_info = pathinfo($_FILES["file"]["name"]);
            $file_directory = "uploads/excel";
            $new_file_name = date("d-m-Y ") . rand(000000, 999999) .".". $file_info["extension"];

            if(move_uploaded_file($_FILES["file"]["tmp_name"], $file_directory . $new_file_name))
            {   
                $file_type  = PHPExcel_IOFactory::identify($file_directory . $new_file_name);
                $objReader  = PHPExcel_IOFactory::createReader($file_type);
                $objPHPExcel = $objReader->load($file_directory . $new_file_name);
                $sheet_data = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
                unset($sheet_data[1]);
                
                $exist = array();
                
                foreach($sheet_data as $data)
                {
                    $result = $this->db->query("SELECT count(*) as count FROM proposal_service where service_name = '".$data['B']."' and firm_id = '".$_SESSION['firm_id']."' and (service_category!='' and service_category!='0')")->row_array();

                    if($result['count'] == 0)
                    {
                        $category = $this->db->query("SELECT id FROM proposal_category where category_name LIKE '%".$data['G']."%' and category_type = 'service' and firm_id ='".$_SESSION['firm_id']."'")->row_array();
                        if(empty($category['id']))
                        {
                            $value = array('category_name'=>$data['G'],'status'=>'active','user_id'=>$user_id,'category_type'=>'service','firm_id' => $_SESSION['firm_id']);
                            $this->Common_mdl->insert('proposal_category',$value);
                            $category['id'] = $this->db->insert_id();
                        }

                        $data = array(
                        'service_name' => $data['B'],
                        'service_price' => $data['C'],
                        'service_unit' => $data['D'],
                        'service_description'=> $data['E'],
                        'service_qty' => $data['F'],  
                        'service_category' => $data['G'],
                        'service_category_id' => $category['id'],                     
                        'status' => 'active',
                        'user_id'=>$user_id,
                        'firm_id' => $_SESSION['firm_id']);
                        $insertId = $this->Common_mdl->insertCSV('proposal_service',$data);
                    }
                    else
                    {
                        array_push($exist, $data['B']);
                    } 
                  }

                $existing = implode(',', $exist);
            } 

            if(!empty($insertId) && $insertId != '0' && count($exist) >0)
            {
               $this->session->set_userdata('success', 'Record(s) Added Successfully!,'.$existing.' - Already Exists.');
            }
            else if(!empty($insertId) && $insertId != '0' && count($exist) == '0')
            {
               $this->session->set_userdata('success', 'Record(s) Added Successfully!');
            }
            else if(empty($insertId) && count($exist) >0)
            {
               $this->session->set_userdata('success', ''.$existing.' - Already Exists.');
            }

            redirect('Proposal/fee_schedule');
        }
        /* Export Service */
        public function Export()
        {
            $filename = 'Service-list.xlsx';                 
            $this->session->unset_userdata('tab_name');

            $firm_services = $this->Common_mdl->getServicelist();
            $firm_enabled_services = array();        

            function firm_enabled($val)
            {          
               return $val['service_name'];
            }

            $firm_enabled_services = array_map('firm_enabled',$firm_services); 
            $firm_enabled_services = implode("','", $firm_enabled_services);

            $records = $this->db->query("SELECT * from proposal_service where firm_id = '".$_SESSION['firm_id']."' and ( service_category!='' and service_category!='0') AND service_name IN('".$firm_enabled_services."')")->result_array();

            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/csv; "); 
            $file = fopen('php://output', 'w'); 
            $header = array("S.NO","Service Name","Service Price","Service Unit","Service Description","Service Qty");
            fputcsv($file, $header);
            $i=1;

            foreach($records as $data)
            {             
                $value['id']=$i; 
                $value['service_name']=$data['service_name']; 
                $value['service_price']=$data['service_price'];
                $value['service_unit']=$data['service_unit']; 
                $value['service_description']=$data['service_description']; 
                $value['service_qty']=$data['service_qty'];                              
                $i++;
                fputcsv($file,$value); 
             } 
             fclose($file);
             exit;  
           // $this->load->view('proposal/excel_list',$post);
        }
        /* Edit Service */
        public function edit_service()
        {
            $this->session->unset_userdata('tab_name');
            $id = $_POST['id'];
            if(isset($_POST['edit_other_service_name']) && $_POST['edit_other_service_name']!='')
            {
            	$_POST['service_name']=$_POST['edit_other_service_name'];
            }
            $service_category = $_POST['service_category'];
            $service_category_name = $this->db->query("select category_name from proposal_category where id='".$service_category."'  and  category_type='service'")->row_array();

            $service_category_name = $service_category_name['category_name'];

            $value = array('service_name'=>$_POST['service_name'],'service_price'=>$_POST['service_price'],'service_unit'=>$_POST['service_unit'],'service_description'=>$_POST['service_description'],'service_qty'=>$_POST['service_qty'],'service_category'=>$service_category_name,'service_category_id'=>$_POST['service_category'],'status'=>'active','service_scode' => $_POST['service_scode']);

            $service_lists_id = $this->Common_mdl->get_price('proposal_service','id',$id,'service_lists_id');

            if(!empty($service_lists_id))
            {
                $service_lists = array('firm'.$_SESSION['firm_id'].'_price' => $_POST['service_price']);

                $this->Common_mdl->update('service_lists',$service_lists,'id',$service_lists_id);
            }

            $data=$this->Common_mdl->update('proposal_service',$value,'id',$id);
            $this->session->set_flashdata('success', 'Service Updated Successfully!'); 
            redirect('Proposal/fee_schedule');
        }


       
        /* Service Delete */
        public function delete_service()
        {
            $this->session->unset_userdata('tab_name');
            $this->session->set_userdata('tab_name', 'start_tab');
            $checkbox=$_POST['checkbox'];

            for($i=0;$i<count($checkbox);$i++)
            {           
              $id=$checkbox[$i];  

              $service_lists_id = $this->Common_mdl->get_price('proposal_service','id',$id,'service_lists_id');
              $firm_ids = json_decode($this->Common_mdl->get_price('service_lists','id',$service_lists_id,'firm_id'),true);

              $index = array_search($_SESSION['firm_id'],$firm_ids);
              unset($firm_ids[$index]);

              if(!empty($service_lists_id) && count($firm_ids) == '0')
              {
                 $services_subnames = $this->Common_mdl->get_price('service_lists','id',$service_lists_id,'services_subnames');

                 $this->Common_mdl->delete('service_lists','id',$service_lists_id); 
                 $this->Common_mdl->delete('reminder_setting','service_id',$service_lists_id);
                 $this->Common_mdl->delete('service_reminder_cron_data','service_id',$service_lists_id);

                 $services = $this->Common_mdl->get_price('admin_setting','firm_id',$_SESSION['firm_id'],'services');
                 $datas = json_decode($services,true); 
                 unset($datas[$service_lists_id]);

                 $this->db->update('admin_setting',['services' => json_encode($datas)],'firm_id = '.$_SESSION['firm_id'].'');

                 $this->db->query('ALTER TABLE client DROP crm_'.$services_subnames.'_email_remainder, DROP crm_'.$services_subnames.'_add_custom_reminder, DROP crm_'.$services_subnames.'_statement_date, DROP '.$services_subnames.''); 
              }

              $delete=$this->Common_mdl->delete('proposal_service','id',$id);
            }

            $this->session->set_flashdata('success', 'Service Deleted Successfully!'); 
            redirect('Proposal/fee_schedule');
        }
        /* Add Products */
        public function add_product()
        {
            $user_id=$this->session->userdata['userId'];  
            $this->session->unset_userdata('tab_name');
            $this->session->set_userdata('tab_name', 'product_tab');

            $service_category=$_POST['product_category'];
            $service_category_name=$this->db->query("select category_name from proposal_category where id='".$service_category."'  and  category_type='product'")->row_array();
            $service_category_name=$service_category_name['category_name'];

            $value=array('product_name'=>$_POST['product_name'],'product_price'=>$_POST['product_price'],'product_description'=>$_POST['product_description'],'product_category'=>$service_category_name,'product_category_id'=>$_POST['product_category'],'status'=>'active','user_id'=>$user_id,'firm_id' => $_SESSION['firm_id']);
            $data=$this->Common_mdl->insert('proposal_products',$value);
            $this->session->set_flashdata('success', 'Record Added Successfully!'); 
            redirect('Proposal/fee_schedule');
        }
        /* Import Products */
        public function import_products()
        { 
            $user_id=$this->session->userdata['userId'];  
            $this->session->unset_userdata('tab_name');
            $this->session->set_userdata('tab_name', 'product_tab');
            $file_info = pathinfo($_FILES["file"]["name"]);
            $file_directory = "uploads/excel";
            $new_file_name = date("d-m-Y ") . rand(000000, 999999) .".". $file_info["extension"];

            function check_for_null($arr)
            {
               if(empty($arr))
               { 
                  return 'Not Valid';
               }

               return $arr;
            }

            function check_for_valid($value)
            {
                if(!is_numeric($value))
                { 
                   return 'Not Valid';
                }
                else
                {
                   if($value <= '0')
                   { 
                      return 'Not Valid';
                   } 
                }

                return $value;
            }
            
            if(in_array($file_info["extension"], ['xls','xlsx','xlt','xla']))
            {               
               if(move_uploaded_file($_FILES["file"]["tmp_name"], $file_directory . $new_file_name))
               {   
                   $file_type  = PHPExcel_IOFactory::identify($file_directory . $new_file_name);
                   $objReader  = PHPExcel_IOFactory::createReader($file_type);
                   $objPHPExcel = $objReader->load($file_directory . $new_file_name);
                   $sheet_data = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
                   unset($sheet_data[1]);
                   
                   $exist = array();
                   $price_error = array();
                   $insuff_error = 0;

                   foreach($sheet_data as $data)
                   {
                       $check_for_null = array_map('check_for_null', $data);
                       $check_for_valid = array_map('check_for_valid',[$data['C']]);

                       if(!in_array('Not Valid', $check_for_null) && !in_array('Not Valid', $check_for_valid))
                       {
                          $result = $this->db->query("SELECT count(*) as count FROM proposal_products where product_name = '".$data['B']."' and firm_id = '".$_SESSION['firm_id']."' and (product_category!='' and product_category!='0')")->row_array();
                          
                          if($result['count'] == 0)
                          {
                              $category = $this->db->query("SELECT id,category_name FROM proposal_category where category_name = '".$data['E']."' and category_type = 'product' and firm_id ='".$_SESSION['firm_id']."'")->row_array();
                               
                              if(empty($category['id']))
                              {
                                  $value = array('category_name'=>$data['E'],'status'=>'active','user_id'=>$user_id,'category_type'=>'product','firm_id' => $_SESSION['firm_id']);
                                  $this->Common_mdl->insert('proposal_category',$value);
                                  $category['id'] = $this->db->insert_id();
                              }

                              if(!empty($category['id']))
                              {                            
                                  $data = array(
                                      'product_name' => $data['B'],
                                      'product_price' => $data['C'],
                                      'product_description' => $data['D'],
                                      'product_category' => (isset($category['category_name']) && $category['category_name'] != '') ? $category['category_name'] : $data['E'],
                                      'product_category_id' => $category['id'],
                                      'status' => 'active',
                                      'user_id'=>$user_id,
                                      'firm_id' => $_SESSION['firm_id']);
                                  $insertId = $this->Common_mdl->insertCSV('proposal_products',$data); 
                              }                              
                          }
                          else
                          {
                              array_push($exist, $data['B']);
                          }                          
                       }
                       else
                       {  
                          if(in_array('Not Valid', $check_for_null))
                          {
                              $insuff_error++;
                          }

                          if(in_array('Not Valid', $check_for_valid))
                          {             
                              array_push($price_error, $data['B']);
                          }
                       }   
                   }

                   $existing = implode(',', $exist);
                   $price_err = implode(',', $price_error);
               }

               $success_msg = '';

               if(!empty($insertId) && $insertId != '0')
               {
                  $success_msg = 'Record(s) Added Successfully!';
               }
               else
               {
                  $success_msg = 'Process Failed!';
               }

               if(count($exist) > 0)
               {
                  $success_msg .= ', "'.$existing.'" - Already Exists.';
               }

               if(count($price_error) > 0)
               {
                  $success_msg .= ', "'.$price_err.'" - Invalid Price.';
               }

               if($insuff_error > '0')
               {
                  $success_msg .= ', '.$insuff_error.' Record(s) Have Insufficient Data.';
               }                
                 
               $this->session->set_userdata('success', $success_msg);
            }
            else
            {
               $this->session->set_userdata('success', 'Invalid File Format.');
            }

            redirect('Proposal/fee_schedule');  
        }
        /* Export Products */
        public function Products_Export()
        {
            $records=$this->Common_mdl->GetAllWithWhere('proposal_products','firm_id',$_SESSION['firm_id']);
            $filename = 'Product-list.xlsx';   
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/csv; "); 
            $file = fopen('php://output', 'w'); 
            $header = array("S.NO","Product Name","Product Price","Product Description");
            fputcsv($file, $header);
            $i=1;

            foreach($records as $data)
            {
                $value['id']=$i; 
                $value['product_name']=$data['product_name']; 
                $value['product_price']=$data['product_price']; 
                $value['product_description']=$data['product_description'];
                $i++;
                fputcsv($file,$value);
            } 

            fclose($file);
            exit; 
        }
        /* Delete Products */
        public function delete_products(){
            $this->session->unset_userdata('tab_name');
             $this->session->set_userdata('tab_name', 'product_tab');
             $checkbox=$_POST['checkbox'];
            for($i=0;$i<count($checkbox);$i++){           
            $id=$checkbox[$i];  
             $delete=$this->Common_mdl->delete('proposal_products','id',$id);
            }
            $this->session->set_flashdata('success', 'Record Deleted Successfully!'); 
            redirect('Proposal/fee_schedule');
        }
        /* Edit Products */
        public function edit_products(){
            $this->session->unset_userdata('tab_name');
             $this->session->set_userdata('tab_name', 'product_tab');
            $id=$_POST['id'];

             $service_category=$_POST['product_category'];
             $service_category_name=$this->db->query("select category_name from proposal_category where id='".$service_category."'  and  category_type='product'")->row_array();
             $service_category_name=$service_category_name['category_name'];

             $value=array('product_name'=>$_POST['product_name'],'product_price'=>$_POST['product_price'],'product_description'=>$_POST['product_description'],'product_category'=>$service_category_name,'product_category_id'=>$_POST['product_category'],'status'=>'active');
            $data=$this->Common_mdl->update('proposal_products',$value,'id',$id);
            $this->session->set_flashdata('success', 'Record Added Successfully!'); 
            redirect('Proposal/fee_schedule');
        }

        public function edit_category(){
            $user_id=$this->session->userdata['userId'];     
             $id=$_POST['id'];     
            $value=array('category_name'=>$_POST['category_name']);
            $datas=$this->Common_mdl->update('proposal_category',$value,'id',$id);

            $service_update=array('service_category'=>$_POST['category_name']);
            $update=$this->Common_mdl->update('proposal_service',$service_update,'service_category_id',$id);

            $product_update=array('product_category'=>$_POST['category_name']);
            $update=$this->Common_mdl->update('proposal_products',$product_update,'product_category_id',$id);


            $subscription_update=array('subscription_category'=>$_POST['category_name']);
            $update=$this->Common_mdl->update('proposal_subscriptions',$subscription_update,'subscription_category_id',$id);
           // if($datas){
               redirect('Proposal/fee_schedule'); 
          //  }
        }



        public function delete_category()
        {
            $id = $_POST['checkbox'];   

            if($_POST['type'] == 'service')
            {
               $table = 'proposal_service';
               $field = 'service_category_id';
               $title = 'Service';
            }
            else if($_POST['type'] == 'product')
            {
               $table = 'proposal_products';
               $field = 'product_category_id';
               $title = 'Product';
            }
            else if($_POST['type'] == 'subscription')
            {
               $table = 'proposal_subscriptions';
               $field = 'subscription_category_id';
               $title = 'Subscription';
            }
            
            $exist = count($this->Common_mdl->GetAllWithWhere($table,$field,$id));

            if($exist > '0' && !isset($_POST['session_category_delete']))
            { 
                if($exist > '1')
                {
                   $title = $title.'s';
                }

                $this->session->set_userdata('delete_error', $exist.' '.$title.' Been Under This Category.');
                $this->session->set_userdata('deleting_categoryid', $id);
                $delete = true;
            }
            else
            {
                $sub_delete = $this->Common_mdl->delete($table,$field,$id);
                $delete = $this->Common_mdl->delete('proposal_category','id',$id);   
                $this->session->set_flashdata('success', 'Deleted Successfully!');
            }

            if($delete)
            {              
              redirect('Proposal/fee_schedule');
            }
        }

        public function generateRandomString($length = 100) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }

         public function pdf_download($id){
       //echo $id; 
       $data['records']=$this->Common_mdl->select_record('proposals','id',$id); 


       $user_id=$data['records']['user_id'];
       if($data['records']['email_signature']=='on'){
          $data['signature']=$this->db->query('select * from pdf_signatures where proposal_no='.$id.' Order By id Desc Limit 1')->row_array();
       }
       $proposal_name=$data['records']['proposal_name'];  
        $this->load->library('Tc');
        $pdf = new Tc('P', 'mm', 'A4', true, 'UTF-8', false); 
        $pdf->AddPage('L','A4');
        // $html = '<style>'.file_get_contents(base_url().'assets/css/style1.css').'</style>';
        $html = $this->load->view('proposal/test_pdf',$data,true);    
       // $footer_logo_html=base_url().'uploads/doc_signs/15904e140653b540bf1f247e49140ae4.png';
        $pdf->SetTitle($proposal_name);
        // $pdf->SetMargins(20, 30, 20);
        $pdf->SetHeaderMargin(30);
        $pdf->SetTopMargin(20);
        $pdf->setFooterMargin(20);
        $pdf->SetAutoPageBreak(true, 20);
        $pdf->SetAuthor('Author');
        $pdf->SetDisplayMode('real', 'default');
        $pdf->WriteHTML($html);       
        $pdf->Output($proposal_name.'.pdf', 'D');
        $pdf->SetXY(50, 50);
    }

  
    public function addCurrency()
    {
        $currency = $_POST['currency'];
        if($currency != '')
        {
            $data = array('currency_name' => $currency);
            $sql = $this->Common_mdl->insert('currency', $data);

            $currency=$this->Common_mdl->getallrecords('currency');

            $data['content']='';

            $data['content'].='<h3>Currency</h3>
                        <select name="currency">';

                foreach($currency as $cur){
                    $data['content'].='<option value="'.$cur['id'].'">'.$cur['currency_name'].'</option>';
                }
                        
                $data['content'].='</select>';
           echo json_encode($data);
        } 
    }

    public function email_signature(){
        $this->load->view('proposal/email_signature');
    }

    
    public function deleteProposal()
    {
        if(isset($_POST['data_id']))
        {
            $id = trim($_POST['data_id']);
            $sql = $this->db->query("DELETE FROM proposals WHERE id in ($id)");
            echo $id;
        }  
    }

    public function category_check()
    {
        $category_name=$_POST['category_name'];
        $category_type=$_POST['category_type'];
        $query=$this->db->query("select * from proposal_category where category_name='".$category_name."' and category_type='".$category_type."' and firm_id ='".$_SESSION['firm_id']."'")->row_array();

        if($query['category_name']!='')
        {
            $data['status']=1;
        }
        else
        {
            $data['status']=0;
        }

        echo json_encode($data);
    }


     public function service_check()
     {
        $service_name = $_POST['service_name'];       
        $category_name = $_POST['service_category'];

        $res = $this->db->query("select * from proposal_service where service_name='".$service_name."' and firm_id='".$_SESSION['firm_id']."'")->row_array();
        //and  service_category_id in (".$category_name.")
        if(is_array($res) && count($res)>0){
            $data['status']=1;
        }else{
            $data['status']=0;
        }
        echo json_encode($data);
     }


     public function product_check(){
         $user_id=$_SESSION['id'];
        $product_name=$_POST['product_name'];
        $category_name=$_POST['product_category'];

         $category=$this->Common_mdl->GetAllWithWhere('proposal_category','user_id',$user_id);
            $product_category=array();             
            foreach($category as $cat){
                if($cat['category_type']=='product'){
                     array_push($product_category,"'".$cat['category_name']."'");
                }
            }
             $product_category=implode(',',$product_category);            

             $count2=explode(',',$product_category);
            

               if($count2['0']!=''){
                    $query=$this->db->query("select * from proposal_products where product_name='".$product_name."' and  user_id='".$user_id."' and  product_category in (".$product_category.")")->row_array();

                }else{
                      $query=$this->db->query("select * from proposal_products where product_name='".$product_name."' and user_id='".$user_id."'")->row_array();
                }
        if($query){
            $data['status']=1;
        }else{
            $data['status']=0;
        }
        echo json_encode($data);
    }

     public function subscription_check()
     { 
        $user_id=$_SESSION['id'];
        $subscription_name=$_POST['subscription_name'];
        $category_name=$_POST['subscription_category'];

        $category=$this->Common_mdl->GetAllWithWhere('proposal_category','user_id',$user_id);
        $subscription_category=array();
        foreach($category as $cat){
        if($cat['category_type']=='subscription'){
        array_push($subscription_category,"'".$cat['category_name']."'");
        }
        } 
        $subscription_category=implode(',',$subscription_category);
        $count1=explode(',',$subscription_category);

        if($count1['0']!=''){
        $query=$this->db->query("select * from proposal_subscriptions where subscription_name='".$subscription_name."' and firm_id = '".$_SESSION['firm_id']."' and  subscription_category in (".$subscription_category.")")->row_array();

        }else{
        $query=$this->db->query("select * from proposal_subscriptions where subscription_name='".$subscription_name."' and firm_id = '".$_SESSION['firm_id']."' and  ( subscription_category!='' and subscription_category!='0')")->row_array();
        }

        if($query){
        $data['status']=1;
        }else{
        $data['status']=0;
        } 

        echo json_encode($data);      
    }




    public function edit_category_check(){

        $id=$_POST['id'];
        $category_name=$_POST['category_name'];
        $category_type=$_POST['category_type'];
        $query=$this->db->query("select * from proposal_category where TRIM(category_name)='".$category_name."' and id!='".$id."' and category_type='".$category_type."' and user_id='".$_SESSION['id']."'")->row_array();
      //  echo $query;
        if($query){
            $data['status']=1;
        }else{
            $data['status']=0;
        }
        echo json_encode($data);
    }   


     public function edit_service_check()
     {
          $id=$_POST['id'];
          $service_name = $_POST['service_name'];
          $category_name = $_POST['service_category'];

          $res = $this->db->query("select * from proposal_service where service_name='".trim($service_name)."' and firm_id='".$_SESSION['firm_id']."' and  service_category_id in (".$category_name.") AND id!='".$id."'")->row_array();
      
          if(is_array($res) && count($res)>0){
              $data['status']=1;
          }else{
              $data['status']=0;
          }
          echo json_encode($data);
     }


     public function edit_product_check(){
          $id=$_POST['id'];
        $product_name=$_POST['product_name'];
         $user_id=$_SESSION['id'];
         $category=$this->Common_mdl->GetAllWithWhere('proposal_category','user_id',$user_id);
            $product_category=array();             
            foreach($category as $cat){
                if($cat['category_type']=='product'){
                     array_push($product_category,"'".$cat['category_name']."'");
                }
             }


             $product_category=implode(',',$product_category);            

             $count2=explode(',',$product_category);
            

               if($count2['0']!=''){
                    $query=$this->db->query("select * from proposal_products where product_name='".$product_name."' and  user_id='".$user_id."' and  product_category in (".$product_category.") and id!='".$id."'")->row_array();

                }else{
                      $query=$this->db->query("select * from proposal_products where product_name='".$product_name."' and id!='".$id."'")->row_array();
                }

       // $category_name=$_POST['product_category'];
      //  $query=$this->db->query("select * from proposal_products where product_name='".$product_name."' ")->row_array();
        if($query){
            $data['status']=1;
        }else{
            $data['status']=0;
        }
        echo json_encode($data);
    }

     public function edit_subscription_check(){
         $user_id=$_SESSION['id'];
          $id=$_POST['id'];
        $subscription_name=$_POST['subscription_name'];



           $category=$this->Common_mdl->GetAllWithWhere('proposal_category','user_id',$user_id);
        $subscription_category=array();
          foreach($category as $cat){
             if($cat['category_type']=='subscription'){
                   array_push($subscription_category,"'".$cat['category_name']."'");
             }
          } 
           $subscription_category=implode(',',$subscription_category);
            $count1=explode(',',$subscription_category);
             if($count1['0']!=''){
                 $query=$this->db->query("select * from proposal_subscriptions where subscription_name='".$subscription_name."' and id!='".$id."' and firm_id = '".$_SESSION['firm_id']."' and  subscription_category in (".$subscription_category.")")->row_array();

            }else{
                $query=$this->db->query("select * from proposal_subscriptions where subscription_name='".$subscription_name."' and id!='".$id."' and firm_id = '".$_SESSION['firm_id']."' and  subscription_category in (".$subscription_category.")")->row_array();

            }

        if($query){
            $data['status']=1;
        }else{
            $data['status']=0;
        }
        echo json_encode($data);
    }


    public function templatename_check(){
        $template_title=$_POST['template_title'];
         $query=$this->db->query("select * from proposal_template where template_title='".$template_title."'")->row_array();
        if($query){
            $data['msg']=1;
        }else{
            $data['msg']=0;
        }
        echo json_encode($data);
    }

     public function edit_templatename_check(){
        $template_title=$_POST['template_title'];
        $id=$_POST['templateID'];
         $query=$this->db->query("select * from proposal_template where template_title='".$template_title."' and id!=".$id."")->row_array();
        if($query){
            $data['msg']=1;
        }else{
            $data['msg']=0;
        }
        echo json_encode($data);
    }

     public function deleting_proposals($id){

        $this->Common_mdl->delete('proposals','id',$id);

        redirect('proposal/');
    }


         public function save_accept_sign()
         {
            $p_id=$_SESSION['accept_id'];
        
            $result = array();
            $imagedata = base64_decode($_POST['img_data']);
            $filename = md5(date("dmYhisA"));
            
            $file_name = 'uploads/doc_signs/'.$filename.'.png';
            file_put_contents($file_name,$imagedata);
            $result['status'] = 1;
            $result['file_name'] = $file_name;
            
            $data=array('accept_signature'=>$file_name,'accept_date'=>time());          
            $insert=$this->Common_mdl->update('proposals',$data,'id',$p_id);
            echo json_encode($result);           exit;   
        }


        public function Terms_and_conditions()
        {
          $this->Security_model->chk_login();
          if( $_SESSION['permission']['Term_and_Condition']['view'] != 1 )
          {
            $this->load->view('users/blank_page');
          }
          else 
          {
            $data['content']=$this->Common_mdl->select_record('Terms_and_conditions','firm_id',$_SESSION['firm_id']);
            $this->load->view('proposal/terms_conditions',$data);
          }  
        }

        public function terms_insert()
        {
            $data=array('terms_content'=>$_POST['description'],'firm_id'=>$_SESSION['firm_id'],'status'=>'active');

            $insert=$this->Common_mdl->insert('Terms_and_conditions',$data);

            redirect('proposal/Terms_and_conditions');
        }

        public function terms_update()
        {
            $id = $_POST['id'];

            $data=array('terms_content'=>$_POST['description']);

            $insert=$this->Common_mdl->update('Terms_and_conditions',$data,'id',$id);

            redirect('proposal/Terms_and_conditions');
        }

        public function termsconditions($firm_id)
        {
             $data['content']=$this->Common_mdl->select_record('Terms_and_conditions','firm_id',$firm_id);
             $this->load->view('proposal/terms_page',$data);
        }


    
}?>
