<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Demo_check extends CI_Controller {


    public function __construct(){
        parent::__construct();
        $this->load->model(array('Task_section_model','Common_mdl','Security_model' ,'Task_invoice_model','Demo_model'));
    }

    public function index(){
     $ques=Get_Assigned_Datas('TASK');
      foreach ($ques as $key => $value) {
          $res1=Get_Module_Assigees( 'TASK' , $value );
         if (in_array($_SESSION['userId'], $res1))
          {
            $res2[]=$value;
            
          }      
        }
        $res=(!empty($res2))?implode(',',array_filter(array_unique($res2))):'-1';
        $data['ass_res']=$res;
         
          if($res!='')
          {
          $data['task_list']=$this->db->query("SELECT * FROM add_new_task AS tl where tl.id in ($res) and tl.firm_id = ". $_SESSION['firm_id']." order by id desc
    ")->result_array();
            }
            else
            {
               $data['task_list']='';
            }


    foreach ($data['task_list'] as $key => $value) {

         $sql_val=$this->db->query("SELECT * FROM firm_assignees where module_id = ". $value['id']." AND module_name='TASK'")->row_array();

         foreach (explode(",",$sql_val['assignees']) as $key1 => $value1) {
         $data['asssign_group'][] =$value1;
         }
     
      }

    	$data['user_list']=$this->db->query("select * from user where firm_id=".$_SESSION['firm_id']." and user_type !='FC' and user_type !='SA'")->result_array();
        $this->load->view('users/task_individual_view',$data);

            }


     public function notification_list(){
       $query11=$this->Common_mdl->section_menu('task_notification',$_SESSION['id']);
        if(empty($query11) || $query11=='0')
        {   
            $data['Task_Module_Notification'] = $this->db->query("select * from notification_management where module='Task' and receiver_user_id = ".$_SESSION['id']." and status IN(1,2,4)")->result_array();
          }

        $this->load->view('Notification/Allnotification_view',$data);
     }

     public function menu_mangement(){

      echo '<pre>';
      if( $_SESSION['permission']['Menu_Management_Settings']['view'] != 1 )
      {
        $this->load->view('users/blank_page');
      }
      else 
      {
      $data['modules'] = $this->db->query('select * from module_mangement')->result_array();

     $admin_val='[{"text":"My Desk","href":"1","icon":"http://remindoo.uk/uploads/menu_icons/1551365799.jpg","target":"_top","title":"1"},{"text":"Tasks","href":"","icon":"http://remindoo.uk/uploads/menu_icons/1551700316.jpg","target":"_self","title":"2","children":[{"text":"All Task","href":"2","icon":"","target":"_self","title":"3"},{"text":"New Task","href":"3","icon":"","target":"_self","title":"4"},{"text":"Tasks Timeline","href":"10","icon":"","target":"_self","title":"5"}]},{"text":"CRM","href":"","icon":"http://remindoo.uk/uploads/menu_icons/1551367854.jpg","target":"_self","title":"6","children":[{"text":"Leads Dashboard","href":"4","icon":"","target":"_self","title":"7"},{"text":"Web to Lead","href":"5","icon":"","target":"_self","title":"8"}]},{"text":"Proposal","href":"1","icon":"http://remindoo.uk/uploads/menu_icons/1551368286.jpg","target":"_self","title":"9","children":[{"text":"Proposal Dashboard","href":"7","icon":"","target":"_self","title":"10"},{"text":"New Proposal","href":"8","icon":"","target":"_self","title":"11"}]},{"text":"Clients","href":"","icon":"http://remindoo.uk/uploads/menu_icons/1551368496.jpg","target":"_self","title":"12","children":[{"text":"Add from Companies House","href":"13","icon":"fa fa-map-marker","target":"_self","title":"13"},{"text":"Add Client","href":"12","icon":"fa fa-filter","target":"_self","title":"14"},{"text":"Clients List","href":"11","icon":"fa fa-filter","target":"_self","title":"15"},{"text":"Import Client","href":"14","icon":"fa fa-filter","target":"_self","title":"16"},{"text":"Services Timeline","href":"15","icon":"fa fa-filter","target":" _self","title":"17"}]},{"text":"Deadline Manager","href":"17","icon":"http://remindoo.uk/uploads/menu_icons/1551427360.jpg","target":"_top","title":"18"},{"text":"Reports","href":"18","icon":"http://remindoo.uk/uploads/menu_icons/1551427406.jpg","target":"_top","title":"19"},{"text":"Settings","href":"18","icon":"http://remindoo.uk/uploads/menu_icons/1551700603.jpg","target":"_top","title":"20","children":[{"text":"General Settings","href":"","icon":"fa fa-crop","target":"_self","title":"21","children":[{"text":"User Management","href":"42","icon":"fa fa-map-marker","target":"_self","title":"22"},{"text":"Services Settings","href":"34","icon":"fa fa-filter","target":"_self","title":"23"},{"text":"Email Templates ","href":"33","icon":"fa fa-filter","target":"_self","title":"24"},{"text":"Firm Admin Settings","href":"19","icon":"fa fa-filter","target":"_self","title":"25"},{"text":"Notifications Settings","href":"36","icon":"fa fa-filter","target":"_self","title":"26"},{"text":"Services Reminders Settings","href":"21","icon":"fa fa-filter","target":"_self","title":"27"}]},{"text":"Advance Settings","href":"","icon":"fa fa-plug","target":"_self","title":"28","children":[{"text":"Firm Fields Settings","href":"20","icon":"fa fa-map-marker","target":"_self","title":"29"},{"text":"Staffs Custom Fields","href":"23","icon":"fa fa-filter","target":"_self","title":"30"},{"text":"Menu Management","href":"45","icon":"fa fa-filter","target":"_self","title":"31"}]},{"text":"Feedback","href":"39","icon":"http://remindoo.uk/uploads/menu_icons/1564120265.jpg","target":"_top","title":"32"},{"text":"Tickets","href":"24","icon":"http://remindoo.uk/uploads/menu_icons/1564128192.jpg","target":"_top","title":"33"},{"text":"Chats","href":"25","icon":"http://remindoo.uk/uploads/menu_icons/1564131040.jpg","target":"_top","title":"34"}]},{"text":"Documents","href":"26","icon":"http://remindoo.uk/uploads/menu_icons/1551700627.jpg","target":null,"title":"35"},{"text":"Invoices","href":"28","icon":"http://remindoo.uk/uploads/menu_icons/1551713659.jpg","target":"_top","title":"36"}]';

     $firm_val='[{"text":"MY DESK","href":"1","icon":"https://remindoo.uk/uploads/menu_icons/1551365799.jpg","target":"_top","title":"1"},{"text":"TASKS","href":"","icon":"https://remindoo.uk/uploads/menu_icons/1551700316.jpg","target":"_self","title":"2","children":[{"text":"ALL Task","href":"2","icon":"","target":"_self","title":"3"},{"text":"New Task","href":"3","icon":"","target":"_self","title":"4"},{"text":"TASK TIMELINE","href":"10","icon":"","target":"_self","title":"5"}]},{"text":"CRM","href":"","icon":"https://remindoo.uk/uploads/menu_icons/1551367854.jpg","target":"_self","title":"6","children":[{"text":"Leads Dashboard","href":"4","icon":"","target":"_self","title":"7"},{"text":"WEB TO LEAD","href":"5","icon":"","target":"_self","title":"8"}]},{"text":"PROPOSAL","href":"","icon":"https://remindoo.uk/uploads/menu_icons/1551368286.jpg","target":"_self","title":"9","children":[{"text":"Proposal Dashboard","href":"7","icon":"","target":"_self","title":"10"},{"text":"New Proposal","href":"8","icon":"","target":"_self","title":"11"}]},{"text":"CLIENTS","href":"","icon":"https://remindoo.uk/uploads/menu_icons/1551368496.jpg","target":"_self","title":"12","children":[{"text":"Add from Companies House","href":"13","icon":"fa fa-map-marker","target":"_self","title":"13"},{"text":"Clients List","href":"11","icon":"fa fa-filter","target":"_self","title":"14"},{"text":"Import Client","href":"14","icon":"fa fa-filter","target":"_self","title":"15"},{"text":"Add Client","href":"12","icon":"fa fa-filter","target":"_self","title":"16"},{"text":"Services Timeline","href":"15","icon":"fa fa-filter","target":" _self","title":"17"}]},{"text":"Deadline Manager","href":"17","icon":"https://remindoo.uk/uploads/menu_icons/1551427360.jpg","target":"_top","title":"18"},{"text":"Reports","href":"18","icon":"https://remindoo.uk/uploads/menu_icons/1551427406.jpg","target":"_top","title":"19"},{"text":"SETTINGS","href":"18","icon":"https://remindoo.uk/uploads/menu_icons/1551700603.jpg","target":"_top","title":"20","children":[{"text":"General Settings","href":"","icon":"fa fa-crop","target":"_self","title":"21","children":[{"text":"User Management","href":"42","icon":"fa fa-map-marker","target":"_self","title":"22"},{"text":"Services Settings","href":"34","icon":"fa fa-filter","target":"_self","title":"23"},{"text":"Email Templates ","href":"33","icon":"fa fa-filter","target":"_self","title":"24"},{"text":"Firm Admin Settings","href":"19","icon":"fa fa-filter","target":"_self","title":"25"},{"text":"Notifications Settings","href":"36","icon":"fa fa-filter","target":"_self","title":"26"},{"text":"26","href":"21","icon":"fa fa-filter","target":"_self","title":"27"}]},{"text":"Advance Settings","href":"","icon":"fa fa-plug","target":"_self","title":"28","children":[{"text":"Firm Fields Settings","href":"20","icon":"fa fa-map-marker","target":"_self","title":"29"},{"text":"Staffs Custom Fields","href":"23","icon":"fa fa-filter","target":"_self","title":"30"},{"text":"Menu Management","href":"45","icon":"fa fa-filter","target":"_self","title":"31"}]}]},{"text":"DOCUMENTS","href":"26","icon":"https://remindoo.uk/uploads/menu_icons/1551700627.jpg","target":null,"title":"32"},{"text":"INVOICE","href":"28","icon":"https://remindoo.uk/uploads/menu_icons/1551713659.jpg","target":"_top","title":"33"},{"text":"GIVE FEEDBACK","href":"39","icon":"https://remindoo.uk/uploads/menu_icons/1564120265.jpg","target":"_top","title":"34"},{"text":"Tickets","href":"24","icon":"https://remindoo.uk/uploads/menu_icons/1564128192.jpg","target":"_top","title":"35"},{"text":"Chats","href":"25","icon":"https://remindoo.uk/uploads/menu_icons/1564131040.jpg","target":"_top","title":"36"}]
';


         $data_admin = $this->Demo_model->GetMenus($admin_val);
        $data_firm = $this->Demo_model->GetMenus($firm_val);


        // print_r($data_admin);
        // print_r($data_firm);
     
      exit;
      $this->load->view('menu_management/custom_menu',$data);
     }

     }


public function task_status_view()
{
  // $this->Common_mdl->document_function();
  $data['task_status']=$this->db->query('SELECT * from task_status where is_superadmin_owned=1')->result_array();
  $this->load->view('super_admin/task_status_view',$data);
}




public function demo_header()
{
    $val=$this->db->query("SELECT * FROM `menu_management` WHERE `icon_path`!=''")->result_array();
    $val=implode(",",array_column($val,'icon_path'));

    $this->load->view('Overview/demo_header');
}



}
?>