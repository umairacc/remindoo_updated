<?php
class Service extends CI_Controller {
	public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
	
	public function __construct(){
		parent::__construct();
		$this->load->model(array('Common_mdl','Service_model','Security_model','Loginchk_model'));
		// $this->load->library('Excel');
	}
	
	public function Check_Login_User()
	{
		if( !empty( $_COOKIE['remindoo_logout'] ) && $_COOKIE['remindoo_logout']=="remindoo_superadmin" )
		{
			$this->Loginchk_model->superAdmin_LoginCheck();
		}
		else
		{
			$this->Security_model->chk_login();
		}
	}
		
		public function index()
		{
			$this->Check_Login_User();
			if( $_SESSION['permission']['Services']['view'] != 1 )
		    {
		        $this->load->view('users/blank_page');
		    }
	  		else 
	      	{
	      		
				// $data['enabled_service'] = array_fill_keys( range(1,16) ,1 );
				//not super_admin
				/*if( $_SESSION['firm_id'] != 0 )
				{
					$firm_service = $this->db->query("SELECT services FROM firm where firm_id=".$_SESSION['firm_id'])->row_array();
					$firm_service = json_decode( $firm_service['services'] , true );	
					
					// $data['enabled_service'] = array_replace( $data['enabled_service'] , $firm_service );
				}*/

				$data['services'] = $this->Common_mdl->select_record('admin_setting','firm_id',$_SESSION['firm_id']);  
				$default_services = $this->db->query('SELECT * FROM service_lists WHERE id <= "16"')->result_array();	

                if(!empty($data['services']['services']))
                {
				   $services = implode(',', array_keys(json_decode($data['services']['services'],true)));
				}
				else
				{
				   $services = "";
				}
				
				$other_services = $this->db->query('SELECT * FROM service_lists WHERE id > "16" AND FIND_IN_SET (id,"'.$services.'")')->result_array();
				$data['service_lists'] = array_merge($default_services,$other_services); 

				$data['work_flow']     =  $this->Service_model->get_firm_workflow();  

				$this->load->view('services/service_page',$data);

				unset( $_SESSION['show_work_flow'] );
				}	
		}
		public function add_update_workflow()
		{
			$this->Check_Login_User();

			if( !empty( $_POST['id'] ) && !empty( $_POST['data'] ))
			{
				//$this->db->update( 'work_flow' , ['service_name'=>$_POST['name']] , "id=".$_POST['id'] );
				
				$this->Service_model->workflow_action( $_POST['id'] , $_POST['data'] );
			}
			else
			{	
				$_POST['data']['firm_id'] = $_SESSION['firm_id'];

				$this->db->insert('work_flow', $_POST['data'] );
			}

			$data['result'] = $this->db->affected_rows();

			if( $data['result'] != 0 ) $_SESSION['show_work_flow'] = 1;

			echo json_encode($data);
		}
		public function delete_workflow()
		{
			$this->Check_Login_User();

			if( !empty( $_POST['id'] ) )
			{
				$this->db->query("DELETE FROM work_flow WHERE id=".$_POST['id']);
			}

			$data['result'] = $this->db->affected_rows();
			if( $data['result'] != 0 ) $_SESSION['show_work_flow'] = 1;
			echo json_encode( $data ); 
		}

		public function update_service_check() {
			$this->Check_Login_User();
			$data['serv'] = $this->input->post('serv');
			$data['serv_id'] = $this->input->post('serv_id');
			if(!empty($data))
			{
				$res = $this->db->query('SELECT services FROM admin_setting WHERE firm_id='.$_SESSION['firm_id'].'')->row_array();
				$json = json_decode($res['services'],true);
				$json[$data['serv']] = $data['serv_id'];
				$encode = json_encode($json);
				if($this->db->where('firm_id',$_SESSION['firm_id'])->set('services',$encode)->update('admin_setting' 	))
				{
					echo 1;
				}
				else
				{
					echo 0;
				}
			}

		}

		public function get_service_details_content( $id , $is_workflow =0)
		{
			$this->Check_Login_User();
			
			$data['currency'] = $this->Service_model->firm_currence();
			
			$data['is_workflow'] = $is_workflow;
			$field = ( $is_workflow == '1' ? "work_flow_id" : "service_id" );

			$data['service'] = $this->Service_model->get_service_workflow( $id , $is_workflow );
			$data['records'] = $this->Service_model->get_steps( $id , $field );
			$data['steps_details'] = $this->Service_model->get_sub_steps( $id , $field );

			return $this->load->view('services/service_edit',$data,true);
		}
		public function UpdateService_price()
		{
			$this->Check_Login_User();
			if( !empty($_POST['service_price']) && !empty($_POST['service_id']) )
			{
				$field = 'firm'.$_SESSION['firm_id'].'_price';
				$this->db->update( "service_lists" ,[ $field => $_POST['service_price'] ] ,"id=".$_POST['service_id']);
			}
			echo json_encode([ 'result'=> $this->db->affected_rows() ] );
		}


		public function add_title()
		{
			$this->Check_Login_User();
			$return = ['result'=>0,'content'=>''];
			
			$id          = $_POST['id'];
			$title       = $_POST['title'];
			$is_workflow = $_POST['is_workflow'];

			$condition = ( $is_workflow == 1 ? 'work_flow_id' : 'service_id' );
			
			$max_order = $this->db->query("select max(order_no) as order_no from  service_steps where `".$condition."`=".$id." and firm_id = ".$_SESSION['firm_id'])->row_array();

			if(empty($max_order))
			{
				$max_order = 1;
			}
			else
			{
				$max_order = $max_order['order_no']+1;
			}


			$data=array($condition=>$id,'title'=>$title,'order_no'=>$max_order,'firm_id'=>$_SESSION['firm_id'],'created_user'=>$_SESSION['id']);
			$insert=$this->Common_mdl->insert('service_steps',$data);
			
			$return['result'] = $this->db->affected_rows();
		
			$return['content'] = $this->get_service_details_content( $id , $is_workflow);
			echo json_encode($return);
		}
		public function steps_details()
		{
			$id=$_POST['id'];
			$service=$this->Common_mdl->select_record('service_steps','id',$id);
			$data['title']=$service['title'];
			$data['id']=$service['id'];
			echo json_encode($data);
		}
		public function edit_delete_steps()
		{	
			$this->Check_Login_User();
			$return  = ['result'=>0,'content'=>''];
			if( !empty( $_POST['id'] ) && !empty( $_POST['data'] ) )
			{

				$id      = $_POST['id'];
				$service = $this->Common_mdl->select_record( 'service_steps' , 'id' , $id );
				
				$source['table']      =  'service_steps';
				$source['clone']      =  $service;
				$source['overwrite']  =  $_POST['data'];
				

				$update = $this->Service_model->action( $source );				

				if( !empty( $update['insert'] ) )
				{
					$this->Service_model->sub_step_action( [ 'step_id' => $id ] , ['step_id' => $update['insert'] ]);	
				}

				$is_workflow = $_POST['is_workflow'];
				$service_id  = ( $is_workflow==1 ? $service['work_flow_id'] : $service['service_id'] );

				$return['result']  = 1;
				$return['content'] = $this->get_service_details_content( $service_id , $is_workflow );

			}

			echo json_encode($return);
		}
		public function delete_steps()
		{
			$this->Check_Login_User();
			$return = ['result'=>0,'content'=>''];
			$id=$_POST['id'];
			$is_workflow = $_POST['is_workflow'];

			$service=$this->Common_mdl->select_record('service_steps','id',$id);
			$return['result'] = $this->Common_mdl->delete('service_steps','id',$id);

			$service_id = ( $is_workflow==1 ? $service['work_flow_id'] : $service['service_id'] );

			$return['content'] = $this->get_service_details_content( $service_id , $is_workflow );

			echo json_encode($return);			
			//redirect('service');
		}

		public function steps_secion_add()
		{
			$this->Check_Login_User();
			$return = ['result'=>0,'content'=>''];
			$id=$_POST['id'];
			$step_content= $_POST['step_content'];
			$is_workflow = $_POST['is_workflow'];


			$service    = $this->Common_mdl->select_record('service_steps','id',$id);
			
			$condition  = (  $is_workflow ==1 ? 'work_flow_id' : 'service_id' );
			$service_id = ( $is_workflow ==1 ? $service['work_flow_id'] : $service['service_id'] );

			$datas=array($condition=>$service_id,'step_id'=>$id,'step_content'=>$step_content,'firm_id'=>$_SESSION['firm_id'],'created_user'=>$_SESSION['id']);

			$return['result'] = $this->Common_mdl->insert('service_step_details',$datas);
			
			$return['content'] = $this->get_service_details_content( $service_id , $is_workflow );

			echo json_encode($return);	
		}

		public function steps_secion_details(){
			$id=$_POST['id'];
			$service=$this->Common_mdl->select_record('service_step_details','id',$id);
			$data['step_name']=$service['step_content'];
			$data['id']=$service['id'];
			echo json_encode($data);
		}



		

		

		public function steps_section_details_update_delete()
		{
			$this->Check_Login_User();

			$return           = ['result'=>0,'content'=>''];
			if( !empty( $_POST['id'] ) && !empty( $_POST['data'] ) )
			{
				$id                  = $_POST['id'];
				$service             = $this->Common_mdl->select_record('service_step_details','id',$id);

				$source['table']     = 'service_step_details';
				$source['clone']     = $service;
				$source['overwrite'] = $_POST['data'];

				$this->Service_model->action( $source );

				$is_workflow       = $_POST['is_workflow'];
				$service_id        = ( $is_workflow==1 ? $service['work_flow_id'] : $service['service_id'] );

				$return['result']  = 1;
				$return['content'] = $this->get_service_details_content( $service_id , $is_workflow );
			} 
			

			echo json_encode($return);	

			//echo $update;
		}

		public function delete_steps_details()
		{
			$this->Check_Login_User();
			$return = ['result'=>0,'content'=>''];
			$id=$_POST['id'];
			$is_workflow = $_POST['is_workflow'];

			$service=$this->Common_mdl->select_record('service_step_details','id',$id);
			$return['result'] = $this->Common_mdl->delete('service_step_details','id',$id);
			$service_id = ( $is_workflow==1 ? $service['work_flow_id'] : $service['service_id'] );
			
			$return['content'] = $this->get_service_details_content( $service_id , $is_workflow );

			echo json_encode($return);		
		}


		public function dashboard(){
			$data['service_details']=$this->Service_model->service_list();
			$this->load->view('services/service_dummy',$data);
		}

		public function service_add(){
			$service=$_POST['service'];
			$data=array('service_name'=>$service,'by_default'=>'1','created_user'=>$_SESSION['id']);
			$insert=$this->Common_mdl->insert('service_lists',$data);
			redirect('service');
		}

		public function custom_change(){
			$id=$_POST['id'];
			$records=$this->Common_mdl->select_record('service_lists','id',$id);			
			$datas=array('custom'=>'1');
			$update=$this->Common_mdl->update('service_lists',$datas,'id',$id);
			$data['content']='';
			     $data['content'].= ' 

			      <div class="radio radio-inline">
                        <label><input type="radio" name="servicestatus" class="defalut_service" value="1" id="'.$records['id'].'" ><i class="helper"></i>By Default</label>
                        </div>

			      <div class="services-lists"><div class="foteditop">
                          <div class="checkbox-fade fade-in-primary">                        
                            <label class="check-task">
                            <span class="task-title-sp">'.$records['service_name'].'</span>
                            <span class="f-right">
                            <p class="action_01">
                              <a href="javascript:;" id="'.$records['id'].'" class="'.$records['service_name'].' delete_service" onclick="delete_service(this)"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true"></i></a>
                              <a href="javascript:;" id="'.$records['id'].'" class="'.$records['service_name'].' edit_service" onclick="edit_service(this)"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                            </p>
                            </span>
                            </label>
                          </div>
                        </div></div>    <div class="services_edit"></div>';              

			echo json_encode($data);
		}

		public function bydefault_change(){
			$id=$_POST['id'];
			$data=array('by_default'=>'1','custom'=>'0');
			$update=$this->Common_mdl->update('service_lists',$data,'id',$id);
			echo $update;
		}

		public function service_delete(){
			$id=$_POST['id'];
			$delete=$this->Common_mdl->delete('service_lists','id',$id);
			redirect('service');
		}

		public function service_edit(){
			$this->load->view('services/service_edit');
		}

		public function service_details()
		{
			if(isset($_POST['id']))
			{
				$is_workflow = ( $_POST['is_workflow'] == 1 ? 1 : 0 );
				echo $this->get_service_details_content( $_POST['id'] ,$is_workflow );
			}
		}

	public function update_orderNumber()
	{
		if(isset($_POST['datas']))
		{
			$this->Check_Login_User();
			$service_id = $_POST['id'];
			$i=1;
			foreach ($_POST['datas'] as $key => $value)
			{
            	//$update=$this->Common_mdl->update('service_steps',['order_no'=>$i],'id',$value);            
				$id      = $value;
				$service = $this->Common_mdl->select_record( 'service_steps' , 'id' , $id );
				
				$source['table']      =  'service_steps';
				$source['clone']      =  $service;
				$source['overwrite']  =  ['order_no'=>$i];

				$update = $this->Service_model->action( $source );
				if( !empty( $update['insert'] ) )
				{
					$this->Service_model->sub_step_action( [ 'step_id' => $id ] , ['step_id' => $update['insert'] ]);	
				}
				$i++;
			}
		
			$return['content'] = $this->get_service_details_content( $service_id , $_POST['is_workflow']);
			echo json_encode($return);
		}

	}

	public function update_estimated_time()
	{
		try {
			$data = $_POST;
			$service_id = $data['service_id'] ?? null;
			$estimated_time = $data['estimated_time'] ?? null;
	
			if (empty($estimated_time) || !is_numeric($estimated_time) || $estimated_time < 0) {
				echo json_encode([
					'status' => 'error',
					'message' => 'Please enter a valid estimated time'
				]);
				return;
			}
	
			$update_data = [
				'estimated_time' => $estimated_time
			];
	
			$this->Common_mdl->update('service_lists', $update_data, 'id', $service_id);
			
			echo json_encode([
				'status' => 'success',
				'message' => 'Estimated time updated'
			]);
			return;
		} catch (\Exception $e) {
			echo json_encode([
				'status' => 'error',
				'message' => 'Some error occurred!',
				'error_details' => $e->getMessage(),
			]);
			return;
		}
	}

	public function get_service_estimated_time()
	{
		$data = $_POST;
		$service_id = $data['service_id'] ?? null;
		
		if (empty($service_id)) {
			echo 0;
			return;
		}

		if (strrpos($service_id, 'ser') !== -1) {
			$service_id = str_replace('ser_', '', $service_id);
		} elseif (strrpos($service_id, 'wor') !== -1) {
			echo 0;
			return;
		}

		$service = $this->Common_mdl->select_record('service_lists', 'id' , $service_id);

		if (is_array($service) && count($service)) {
			echo $service['estimated_time'];
			return;
		}

		echo 0;
		return;
	}

} ?>
