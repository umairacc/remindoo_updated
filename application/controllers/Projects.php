<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Projects extends CI_Controller {
public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct(){
        parent::__construct();
        $this->load->model(array('Common_mdl','Security_model'));
    }

    public function index()
    {   
        $data['projects'] = $this->Common_mdl->getallrecords('projects');
        $this->load->view('projects/project_list',$data);
    }

    public function add_project()
    {
        $data['client'] = $this->Common_mdl->GetAllWithWhere('user','role','3');
        $data['staff'] = $this->Common_mdl->GetAllWithWhere('user','role','6');
        $this->load->view('projects/add_new_project_view',$data);
    }

    public function add_new()
    {
        $data['project_name'] = $_POST['project_name'];
        $data['customer_id'] = $_POST['client'];
        if(isset($_POST['calculate_progress_through_task'])) { $data['proj_progress_based_on_task'] =  $_POST['calculate_progress_through_task']; } else { $data['proj_progress_based_on_task'] =  '0'; }

        if(isset($_POST['progress'])) { $data['progress'] =  $_POST['progress']; } else { $data['progress'] =  '0'; }

       
        $data['billing_type'] = $_POST['billing_type'];
        $data['status'] = $_POST['project_status'];
        $data['rate_per_hours'] = $_POST['rate_per_hour'];
        $data['estimation_hrs'] = $_POST['estimation_hour'];
        $data['members'] = $_POST['members'];
        $data['start_date'] = $_POST['start_date'];
        $data['deadline'] = $_POST['deadline'];
        $data['tag'] = $_POST['tags'];
        $data['description'] = $_POST['desc'];
         if(isset($_POST['allow_view_task'])){ $data['allow_view_task'] = $_POST['allow_view_task']; } else { $data['allow_view_task'] = '0'; }
        if(isset($_POST['allow_create_task'])){ $data['allow_create_task'] =  $_POST['allow_create_task']; } else { $data['allow_create_task'] =  '0'; }
        if(isset($_POST['allow_customer_edit_task'])){ $data['allow_customer_edit_task'] =  $_POST['allow_customer_edit_task']; } else { $data['allow_customer_edit_task'] =  '0'; }
        if(isset($_POST['allow_customer_comment_task'])){  $data['allow_customer_comment_task'] = $_POST['allow_customer_comment_task']; } else {  $data['allow_customer_comment_task'] = '0'; }
        if(isset($_POST['allow_view_task_comment'])){ $data['allow_view_task_comment'] = $_POST['allow_view_task_comment']; } else { $data['allow_view_task_comment'] = '0'; }
        if(isset($_POST['allow_customer_view_task_attachment'])){ $data['allow_customer_view_task_attachment'] = $_POST['allow_customer_view_task_attachment']; } else { $data['allow_customer_view_task_attachment'] = '0'; }
        if(isset($_POST['allow_customer_view_task_checklist'])){ $data['allow_customer_view_task_checklist'] = $_POST['allow_customer_view_task_checklist']; } else { $data['allow_customer_view_task_checklist'] = '0'; }
        if(isset($_POST['allow_customer_upload_attachments'])){ $data['allow_customer_upload_attachments'] = $_POST['allow_customer_upload_attachments']; } else { $data['allow_customer_upload_attachments'] = '0'; }
        if(isset($_POST['allow_customer_to_view_total_logged_time'])){  $data['allow_customer_to_view_total_logged_time'] = $_POST['allow_customer_to_view_total_logged_time']; } else {  $data['allow_customer_to_view_total_logged_time'] = '0'; }
        if(isset($_POST['allow_customer_view_finance'])){  $data['allow_customer_view_finance'] = $_POST['allow_customer_view_finance']; } else {  $data['allow_customer_view_finance'] = '0'; }
        if(isset($_POST['all_customer_upload_file'])){ $data['all_customer_upload_file'] = $_POST['all_customer_upload_file']; } else { $data['all_customer_upload_file'] = '0'; }
        if(isset($_POST['allow_customer_open_discussion'])){  $data['allow_customer_open_discussion'] = $_POST['allow_customer_open_discussion']; } else {  $data['allow_customer_open_discussion'] = '0'; }
        if(isset($_POST['allow_customer_view_milestone'])){ $data['allow_customer_view_milestone'] =  $_POST['allow_customer_view_milestone']; } else { $data['allow_customer_view_milestone'] =  '0'; }
        if(isset($_POST['allow_customer_view_gantt'])){ $data['allow_customer_view_gantt'] = $_POST['allow_customer_view_gantt']; } else { $data['allow_customer_view_gantt'] = '0'; }
        if(isset($_POST['allow_customer_view_timesheet'])){ $data['allow_customer_view_timesheet'] = $_POST['allow_customer_view_timesheet']; } else { $data['allow_customer_view_timesheet'] = '0'; }
        if(isset($_POST['allow_customer_view_activitylog'])){ $data['allow_customer_view_activitylog'] =  $_POST['allow_customer_view_activitylog']; } else { $data['allow_customer_view_activitylog'] =  '0'; }
        if(isset($_POST['allow_customer_view_team_member'])){  $data['allow_customer_view_team_member'] = $_POST['allow_customer_view_team_member']; } else {  $data['allow_customer_view_team_member'] = '0'; }
        if(isset($_POST['hide_project_task'])){ $data['hide_project_task'] = $_POST['hide_project_task']; } else { $data['hide_project_task'] = '0'; }

        if(isset($_POST['send_proj_create_mail'])){ $data['send_proj_create_mail'] = $_POST['send_proj_create_mail']; } else { $data['send_proj_create_mail'] = '0'; }
        $data['createdTime'] = time();
        
        $result = $this->Common_mdl->insert('projects',$data);
        if($result)
        {
            redirect('projects/index');
        }
    }

    public function delete($id)
    {
        $this->Common_mdl->delete('projects','id',$id);
        redirect('projects');
    }

     public function update($id)
    {
        $data['client'] = $this->Common_mdl->GetAllWithWhere('user','role','3');
        $data['staff'] = $this->Common_mdl->GetAllWithWhere('user','role','6');
        $data['project_rec'] = $this->Common_mdl->select_record('projects','id',$id);
        //print_r($id);
        //print_r($data['project_rec']);die;
        $this->load->view('projects/edit_project',$data);
    }

    public function edit_proj($id)
    {
$data['project_name'] = $_POST['project_name'];
        $data['customer_id'] = $_POST['client'];
        if(isset($_POST['calculate_progress_through_task'])) { $data['proj_progress_based_on_task'] =  $_POST['calculate_progress_through_task']; } else { $data['proj_progress_based_on_task'] =  '0'; }

        if(isset($_POST['progress'])) { $data['progress'] =  $_POST['progress']; } else { $data['progress'] =  '0'; }

       
        $data['billing_type'] = $_POST['billing_type'];
        $data['status'] = $_POST['project_status'];
        $data['rate_per_hours'] = $_POST['rate_per_hour'];
        $data['estimation_hrs'] = $_POST['estimation_hour'];
        $data['members'] = $_POST['members'];
        $data['start_date'] = $_POST['start_date'];
        $data['deadline'] = $_POST['deadline'];
        $data['tag'] = $_POST['tags'];
        $data['description'] = $_POST['desc'];
         if(isset($_POST['allow_view_task'])){ $data['allow_view_task'] = $_POST['allow_view_task']; } else { $data['allow_view_task'] = '0'; }
        if(isset($_POST['allow_create_task'])){ $data['allow_create_task'] =  $_POST['allow_create_task']; } else { $data['allow_create_task'] =  '0'; }
        if(isset($_POST['allow_customer_edit_task'])){ $data['allow_customer_edit_task'] =  $_POST['allow_customer_edit_task']; } else { $data['allow_customer_edit_task'] =  '0'; }
        if(isset($_POST['allow_customer_comment_task'])){  $data['allow_customer_comment_task'] = $_POST['allow_customer_comment_task']; } else {  $data['allow_customer_comment_task'] = '0'; }
        if(isset($_POST['allow_view_task_comment'])){ $data['allow_view_task_comment'] = $_POST['allow_view_task_comment']; } else { $data['allow_view_task_comment'] = '0'; }
        if(isset($_POST['allow_customer_view_task_attachment'])){ $data['allow_customer_view_task_attachment'] = $_POST['allow_customer_view_task_attachment']; } else { $data['allow_customer_view_task_attachment'] = '0'; }
        if(isset($_POST['allow_customer_view_task_checklist'])){ $data['allow_customer_view_task_checklist'] = $_POST['allow_customer_view_task_checklist']; } else { $data['allow_customer_view_task_checklist'] = '0'; }
        if(isset($_POST['allow_customer_upload_attachments'])){ $data['allow_customer_upload_attachments'] = $_POST['allow_customer_upload_attachments']; } else { $data['allow_customer_upload_attachments'] = '0'; }
        if(isset($_POST['allow_customer_to_view_total_logged_time'])){  $data['allow_customer_to_view_total_logged_time'] = $_POST['allow_customer_to_view_total_logged_time']; } else {  $data['allow_customer_to_view_total_logged_time'] = '0'; }
        if(isset($_POST['allow_customer_view_finance'])){  $data['allow_customer_view_finance'] = $_POST['allow_customer_view_finance']; } else {  $data['allow_customer_view_finance'] = '0'; }
        if(isset($_POST['all_customer_upload_file'])){ $data['all_customer_upload_file'] = $_POST['all_customer_upload_file']; } else { $data['all_customer_upload_file'] = '0'; }
        if(isset($_POST['allow_customer_open_discussion'])){  $data['allow_customer_open_discussion'] = $_POST['allow_customer_open_discussion']; } else {  $data['allow_customer_open_discussion'] = '0'; }
        if(isset($_POST['allow_customer_view_milestone'])){ $data['allow_customer_view_milestone'] =  $_POST['allow_customer_view_milestone']; } else { $data['allow_customer_view_milestone'] =  '0'; }
        if(isset($_POST['allow_customer_view_gantt'])){ $data['allow_customer_view_gantt'] = $_POST['allow_customer_view_gantt']; } else { $data['allow_customer_view_gantt'] = '0'; }
        if(isset($_POST['allow_customer_view_timesheet'])){ $data['allow_customer_view_timesheet'] = $_POST['allow_customer_view_timesheet']; } else { $data['allow_customer_view_timesheet'] = '0'; }
        if(isset($_POST['allow_customer_view_activitylog'])){ $data['allow_customer_view_activitylog'] =  $_POST['allow_customer_view_activitylog']; } else { $data['allow_customer_view_activitylog'] =  '0'; }
        if(isset($_POST['allow_customer_view_team_member'])){  $data['allow_customer_view_team_member'] = $_POST['allow_customer_view_team_member']; } else {  $data['allow_customer_view_team_member'] = '0'; }
        if(isset($_POST['hide_project_task'])){ $data['hide_project_task'] = $_POST['hide_project_task']; } else { $data['hide_project_task'] = '0'; }

        if(isset($_POST['send_proj_create_mail'])){ $data['send_proj_create_mail'] = $_POST['send_proj_create_mail']; } else { $data['send_proj_create_mail'] = '0'; }

        $result = $this->Common_mdl->update('projects',$data,'id',$id);
        if($result)
        {
            redirect('projects/index');
        } 
    }
   
}
?>
