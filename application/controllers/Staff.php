<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
ob_start();
class Staff extends CI_Controller {
public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
      parent::__construct();
      @session_start();
      $this->load->model(array('Common_mdl','Security_model','User_management'));
      $this->load->library('upload');
      $this->load->helper(['template_decode']);
    }



    public function index($user_id=false)
    {
      $this->Security_model->chk_login();
      $user_limit  = $this->Common_mdl->Check_Firm_User_limit( $_SESSION['firm_id'] );
      if($user_id && $_SESSION['permission']['User_and_Management']['edit']!=1)
      {
        $this->load->view('users/blank_page');
      }
      else if($_SESSION['permission']['User_and_Management']['create']!=1)
      {
        $this->load->view('users/blank_page');
      }
      else if( empty( $user_id ) && !( $user_limit >0 || $user_limit == 'unlimited' ) )
      { 
        $this->load->view('users/user_limit_exit');                
      }
      else
      {
        $this->staff_AddEdit_page( $user_id );
      }
    }
    public function deleteStaffs()
    {
      $this->Security_model->chk_login();
      if( !empty( $this->input->post( 'data_id' ) ) )
      {
        echo $this->User_management->Delete_staffs( $this->input->post( 'data_id' ) );
      }  
    }
    public function staff_AddEdit_page( $user_id = 0 , $is_profile_page = 0 )
    {
      $data['staff'] = [];
      if( !empty( $user_id ) )
      {
        $data['staff'] = $this->db->query('select * from staff_form where user_id='.$user_id)->row_array();
      }
      //$GetAdministration = $this->db->query("select * from roles_section where firm_id = ".$_SESSION['firm_id']." and  parent_id=0")->row_array();

      $data['roles_tag'] = GetAssignees_SelectBox_Content( 0, 0 ,0);       
      //$data['custom_form']=$this->Common_mdl->select_record('staff_custom_form','user_id',$user_id);
      $data['is_profile_page']  = $is_profile_page;
      $data['countries'] = $this->db->query("SELECT * FROM `countries` order by name asc")->result_array();

      //firm enabled service
      $services = $this->Common_mdl->get_FirmEnabled_Services();
      $data['services'] = [];
      if( !empty( $services ) )
      {
        $services = implode(',' , array_filter( array_keys( $services ) )  );

        $data['services'] = $this->db->get_where("service_lists","id IN(".$services.")")->result_array();
      }

      //
      $this->load->model('Service_model');
      $data['work_flow'] = $this->Service_model->get_firm_workflow();


      $this->load->view('staffs/add_staff_view',$data);       
    }

    public function staff_profile_edit($user_id=false)
    {  
        $this->Security_model->chk_login();
        $this->staff_AddEdit_page( $user_id , 1 );

      /*  $data['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user','role','1');
        $data['staff'] = $this->Common_mdl->GetAllWithWhere('staff_form','user_id',$user_id);
       // $data['roles']=$this->Common_mdl->getallrecords('Role');
        $data['roles']=$this->db->query('select * from Role where firm_id ='.$_SESSION['firm_id'])->result_array(); // 1->admin 2->sub admin 4->client
           
        $data['departments']=$this->Common_mdl->getallrecords('department');
        $data['new_department'] = $this->db->query( "SELECT * FROM department_permission WHERE new_dept!=''")->result_array();
        $user_id=$this->session->userdata['userId'];
        $get_author_user=$this->db->query('select * from user where id='.$user_id.' ')->row_array();

        
      //  echo 'select * from user where id='.$user_id.'';
        $firm_admin_id='';
        if(count($get_author_user)>0)
        {
          $firm_admin_id=$get_author_user['firm_admin_id'];
        }
      //  echo $firm_admin_id;
        $data['custom_form']=$this->Common_mdl->select_record('staff_custom_form','user_id',$firm_admin_id);
        $this->load->view('staffs/staff_profile_edit',$data);*/
    }

    public function addnew_staff1()
    {
        $this->Security_model->chk_login();
       $this->load->view('staffs/addnew_staff1_view'); 
    }

public function add_staff()
{  
  $this->Security_model->chk_login();  

  if(isset($_POST) && $_POST['first_name']!='' && $_POST['email_id'] != '' )
  {
    $image                  = ( !empty( $_POST['image_name'] )  ? $_POST['image_name'] : '' );

    if( isset( $_FILES['image_upload']['name'] ) &&( $_FILES['image_upload']['name']!='' ) )
    {
      $image                = $this->Common_mdl->do_upload( $_FILES['image_upload'], 'uploads' );
    }

    $data['crm_name']         = $_POST['first_name'];
    $data['crm_last_name']    = $_POST['last_name'];
    $data['username']         = $_POST['user_name'];
    $data['password']         = md5( $_POST['password'] );
    $data['confirm_password'] = $_POST['password'];
    $data['crm_email_id']     = $_POST['email_id'];
    $data['crm_phone_number'] = $_POST['telephone_number'];
    $data['crm_profile_pic']  = $image;
    $data['status']           = $_POST['status'];

    if( $_POST['is_profile_page'] != 1)
    {
      $data['firm_id']        = $_SESSION['firm_id'];
      $data['role']           = $_POST['roles'];
      $data['CreatedTime']    = time();
      $data['status']         = 1;
    }
    //FU Data save while new user creation 
    if( empty( $_POST['user_id'] ) )
    {
      $data['user_type']      = 'FU';
    }


    if( isset( $_POST['user_id'] ) && ( $_POST['user_id']=='' ) )
    {
      $this->db->insert( 'user', $data );
      $in = $this->db->insert_id();
    }
    else
    {
      $this->Common_mdl->update( 'user' , $data , 'id' , $_POST['user_id'] );
      $in = $_POST['user_id'];
    }   
      $datas['user_id']                 = $in;
      $datas['profile']                 = $image;
      $datas['first_name']              = $_POST['first_name'];
      $datas['last_name']               = $_POST['last_name'];
      $datas['email_id']                = $_POST['email_id'];
      $datas['telephone_number']        = $_POST['country_code'].'-'.$_POST['telephone_number'];
      $datas['facebook']                = (isset($_POST['facebook'])&&($_POST['facebook']!=''))? $_POST['facebook']:'0';
      $datas['linkedin']                = (isset($_POST['linkedin'])&&($_POST['linkedin']!=''))? $_POST['linkedin']:'0';
      $datas['skype']                   = (isset($_POST['skype'])&&($_POST['skype']!=''))? $_POST['skype']:'0';
      $datas['email_signature']         = (isset($_POST['email_signature'])&&($_POST['email_signature']!=''))? $_POST['email_signature']:'0';
      /*
      $datas['two_factor']            = (isset($_POST['two_factor'])&&($_POST['two_factor']!=''))? $_POST['two_factor']:0;
      $datas['not_staff']             = (isset($_POST['not_staff'])&&($_POST['not_staff']!=''))? $_POST['not_staff']:0;
      $datas['language']              = (isset($_POST['language'])&&($_POST['language']!=''))? $_POST['language']:'0';
      $datas['direction']             = (isset($_POST['direction'])&&($_POST['direction']!=''))? $_POST['direction']:'0';
      $datas['marketing']             = (isset($_POST['marketing'])&&($_POST['marketing']!=''))? $_POST['marketing']:'0';
      $datas['sales']                 = (isset($_POST['sales'])&&($_POST['sales']!=''))? $_POST['sales']:'0';
      $datas['abuse']                 = (isset($_POST['abuse'])&&($_POST['abuse']!=''))? $_POST['abuse']:'0';
      $datas['logistics']             = (isset($_POST['logistics'])&&($_POST['logistics']!=''))? $_POST['logistics']:'0';
      $datas['administrator']         = (isset($_POST['administrator'])&&($_POST['administrator']!=''))? $_POST['administrator']:'0';
      */
      $datas['welcome_mail']            = ( isset($_POST['welcome_mail'])&&($_POST['welcome_mail']!='') )? $_POST['welcome_mail'] :'0';
      $datas['username']                = $_POST['user_name'];
      $datas['password']                = md5( $_POST['password'] );
      $datas['confirm_password']        = $_POST['password'];

      if( $_POST['is_profile_page'] !=1 )
      {
        $datas['roles']                   = $_POST['roles'];
        $datas['hourly_rate']             = $_POST['hourly_rate'];
        $datas['status']                  = $_POST['status'];
        $datas['created_date']            = time();
      }

      $datas['firm_id']                   = $_SESSION['firm_id'];
      $sign_data                          = $_POST['sign_data'];
      $datas['signature_path']            = $sign_data;


      $ser_wor                            = $this->get_posted_service_workflow_value();
     
      $datas['allocated_work_flow']       = $ser_wor['allocated_work_flow'];
      $datas['allocated_service']         = $ser_wor['allocated_service'];

      if( isset( $_POST['user_id'] ) && ( $_POST['user_id'] == '' ) )
      {
        $this->db->insert( 'staff_form', $datas );
      }
      else
      {
        $this->Common_mdl->update( 'staff_form' , $datas , 'user_id' , $_POST['user_id'] );
      }

      if( empty( $_POST['user_id'] ) )
      {
        $this->Common_mdl->Increase_Decrease_UserCounts( $_SESSION['firm_id'] , '-1');

        $admin_node = $this->db->query("select id from organisation_tree where firm_id= ".$_SESSION['firm_id']." and parent_id=0 ")->row_array();
        
        $title = $this->db->query("select role from roles_section where id = ".$_POST['roles'])->row_array();
        
        $this->db->insert("organisation_tree",['title'=>$title['role'],'parent_id'=>$admin_node['id'],'firm_id'=>$_SESSION['firm_id'],'user_id'=>$in]);
      }
      else
      {
        $title = $this->db->query("select role from roles_section where id = ".$_POST['roles'])->row_array();
        
        $this->db->update("organisation_tree",['title'=>$title['role']],['firm_id'=>$_SESSION['firm_id'],'user_id'=>$_POST['user_id']]);
      }

      $old_service    = $this->db->select( "allocated_service" )
                            ->get_where( "staff_form", 'user_id='.$in )
                            ->row("allocated_service");

      $old_service    = explode( ',' , $old_service );

      $update_service = explode( ',' , $ser_wor['allocated_service'] );

      $this->User_management->AddRemoveStaffFrom_ClientServices( $update_service , $old_service , $in );

      //24 refer to staff welcome mail.
      $EMAIL_TEMPLATE = $this->Common_mdl->getTemplates(24);

      if( empty( $_POST['user_id'] ) && !empty( $EMAIL_TEMPLATE ) && !empty( $in ) )
      {
        $sender_details = $this->db->query('select company_name,company_email from admin_setting where firm_id='.$_SESSION['firm_id'])->row_array();

        $EMAIL_TEMPLATE = end($EMAIL_TEMPLATE);

        $decode['subject'] = html_entity_decode($EMAIL_TEMPLATE['subject']);
        $decode['body']    = html_entity_decode($EMAIL_TEMPLATE['body']);    

        $decoded = Decode_StaffTable_Datas( $in , $decode );
               
        $subject = preg_replace( '/ {2,}/' , ' ', str_replace( '&nbsp;', ' ', strip_tags( $decoded['subject'] ) ) );
        
        firm_settings_send_mail( $_SESSION['firm_id'] , $_POST['email_id'] , $subject , $decoded['body'] );
      }
    //* for access permisison *

    //* for staff custom form fields *
    /*
    if(isset($_POST['user_id'])&&($_POST['user_id']==''))
    {
    $in_val=$this->db->insert_id();
    $staff_id=$in;
    }
    else
    {
       $staff_id=$_POST['user_id'];
    }

    $staff_custom_data='';
    if(isset($_POST['user_firm_id']) && $_POST['user_firm_id']!='')
    {
      $custom_form=$this->db->query('select * from staff_custom_form where user_id='.$_POST['user_firm_id'].' ')->row_array();
    }
    else
    {
      $custom_form=$this->db->query('select * from staff_custom_form where user_id='.$_SESSION['id'].' ')->row_array();
    }

            $custom_data_array=array();
            if(!empty($custom_form['labels']) && isset($custom_form['labels']))
            { 

              $labels=array_filter(explode(',',$custom_form['labels']));
              $field_type=array_filter(explode(',',$custom_form['field_type']));
              $field_name=array_filter(explode(',',$custom_form['field_name']));
              $values=array_filter(explode(',',$custom_form['values']));

              for($i=0;$i< count($labels);$i++)
              {

                //echo $field_type[$i]."--".$field_name[$i]."<br>";
                if(isset($field_type) && $field_type[$i]=="file")
                {
                  //echo 'name---'.$field_name[$i];
                        $attach_files='';
                        if(!empty(array_filter($_FILES[$field_name[$i]]['name']))){
                        if(!empty($_FILES[$field_name[$i]]['name'])){
                          
                                                       //* for multiple *
                        $files = $_FILES;
                        $cpt = count($_FILES[$field_name[$i]]['name']);
                        for($z=0; $z<$cpt; $z++)
                        {           
                                  //* end of multiple *
                            $_FILES[$field_name[$i]]['name']= $files[$field_name[$i]]['name'][$z];
                            $_FILES[$field_name[$i]]['type']= $files[$field_name[$i]]['type'][$z];
                            $_FILES[$field_name[$i]]['tmp_name']= $files[$field_name[$i]]['tmp_name'][$z];
                            $_FILES[$field_name[$i]]['error']= $files[$field_name[$i]]['error'][$z];
                            $_FILES[$field_name[$i]]['size']= $files[$field_name[$i]]['size'][$z]; 
                                   $uploadPath = 'uploads/';
                                   $config['upload_path'] = $uploadPath;
                                   $config['allowed_types'] = '*';    
                                   $config['max_size']='0';
                                   $this->load->library('upload', $config);
                                   $this->upload->initialize($config);
                                   if($this->upload->do_upload($field_name[$i])){
                                       $fileData = $this->upload->data();
                                    if($attach_files=='')
                                    {
                                       $attach_files=base_url().'uploads/'.$fileData['file_name'];
                                    }
                                    else
                                    {
                                       $attach_files=$attach_files.",".base_url().'uploads/'.$fileData['file_name'];
                                    }
                                   }
                                 }
                               }
                               }
                               else
                               {
                             //   echo "not";
                               $attach_files=$_POST[$field_name[$i]."_hidden"];
                               }

              
                          $res=$attach_files;
                      }
                    
                      else if(isset($_POST[$field_name[$i]]))
                      {
                        //echo "res";
                          $res=$_POST[$field_name[$i]];
                      }
              
      
                $custom_data_array[$field_name[$i]] = $res;
              }
            $staff_custom_data=json_encode($custom_data_array);
            }
       
    $this->Common_mdl->delete('staff_custom_form_values','staff_id',$staff_id);
    $staff_cus_data['staff_id']=$staff_id;
    $staff_cus_data['custom_form_id']=$_SESSION['id'];
    $staff_cus_data['values']=$staff_custom_data;
    $staff_cus_data['create_by']=$_SESSION['id'];
    $staff_cus_data['created_time']=time();
    $this->Common_mdl->insert('staff_custom_form_values',$staff_cus_data);
    */
    /** end of staff custom form **/
    /** welcome mail 20-08-2018 **/
    echo '1'; 
  }
  else
  {
    echo '0';
  }
}
public function get_posted_service_workflow_value()
{
  $datas['allocated_work_flow']     = [];
  $datas['allocated_service']       = [];

  if( !empty( $_POST['service_work_flow'] ) )
  {
    foreach ($_POST['service_work_flow'] as $key => $value) {
      $value = explode('_', $value );
      if( $value[0] == 'ser' ) $datas['allocated_service'][] = $value[1];
      else $datas['allocated_work_flow'][] = $value[1];
    }
  }

  $datas['allocated_work_flow']     = implode( ',', $datas['allocated_work_flow'] );
  $datas['allocated_service']       = implode(',',  $datas['allocated_service'] );

  return $datas;
}
public function change_staff_assigned_ser_wor()
{ 
  $User_id = $this->input->post('user_id');

  if( !empty( $User_id ) )
  {
    $data = $this->get_posted_service_workflow_value();

    $update_service = explode( ',', $data['allocated_service'] );

    $old_service    = $this->db->select( "allocated_service" )
                            ->get_where( "staff_form", 'user_id='.$User_id )
                            ->row("allocated_service");
    $old_service    = explode(',', $old_service );

    $response = $this->User_management->AddRemoveStaffFrom_ClientServices( $update_service , $old_service , $User_id );

    $this->db->update( 'staff_form' , $data , 'user_id='.$User_id );
    echo $this->db->affected_rows();
    return;
  }
  echo '0';   
}

public function staff_delete($id, $team_id=false)
{
  $this->Common_mdl->delete('user','id',$id);
  $this->Common_mdl->delete('staff_form','user_id',$id);
  
    $data['getallUser'] = $this->Common_mdl->GetAllWithExceptField('user','role','1');
    $data['teams']=$this->Common_mdl->select_record('team_assign_staff','id',$team_id);
    $data['staff_list']=$this->Common_mdl->GetAllWithWhere('staff_form','status','1');
    $data['department_list']=$this->Common_mdl->getallrecords('department');
    $data['new_department'] = $this->db->query( "SELECT * FROM department_permission WHERE new_dept!=''")->result_array();
    $this->load->view('team/team_and_management',$data); 
  
}

public function staff_permission()
    {
        
        $this->Security_model->chk_login();
       // echo "<pre>";
        //print_r($_POST);
       // exit;
    }
/** for us 09-08-2018 **/
function time_to_sec($time)
{
list($h, $m, $s) = explode (":", $time);
$seconds = 0;
$seconds += (intval($h) * 3600);
$seconds += (intval($m) * 60);
$seconds += (intval($s));
return $seconds;
}
function sec_to_time($sec) {
return sprintf('%02d:%02d:%02d', floor($sec / 3600), floor($sec / 60 % 60), floor($sec % 60));
}
function calculate_test($a,$b){
$difference = $b-$a;

$second = 1;
$minute = 60*$second;
$hour   = 60*$minute;

$ans["hour"]   = floor(($difference)/$hour);
$ans["minute"] = floor((($difference)%$hour)/$minute);
$ans["second"] = floor(((($difference)%$hour)%$minute)/$second);
//echo  $ans["hour"] . " hours, "  . $ans["minute"] . " minutes, " . $ans["second"] . " seconds";

$test=$ans["hour"].":".$ans["minute"].":".$ans["second"];
return $test;
}
/** end of 09-08-2018 **/
    public function Dashboard()
    {
       $this->Security_model->chk_login();
       $id = $this->session->userdata('admin_id');
       $data['user_details']=$this->Common_mdl->select_record('user','id',$id);
       //$data['create_clients_list']=$this->Common_mdl->GetAllWithWhere('user','firm_admin_id',$id);
       //$data['create_clients_list']=$this->db->query("select * from user where firm_admin_id='".$id."' and role = '6' ")->result_array();
       $data['create_clients_list']=$this->db->query("select * from user where firm_admin_id='".$id."' and crm_name != '' ")->result_array();
       if(isset($_POST['date']))
       {
        $month = '';
        $year = '';
       }else{
        $month = date('m');
        $year = date('Y');
       }   


       $data['start_end_date'] = $this->start_end_date('01-'.$month.'-'.$year);

       $exp_date = explode(',', $data['start_end_date']);

       $start = $exp_date[0];
       $end = $exp_date[1];

       $data['start_month'] = $this->month_cnt($start);
       $data['end_month'] = $this->month_cnt($end);

       $data['total_count_month'] = $this->weeks($month,$year);

               
      $assigned_task=$this->Common_mdl->getallrecords('add_new_task');
      $user_id = $this->session->userdata('id');
      $high = 0;
      $low = 0;
      $medium = 0;
      $super_urgent = 0;
      $tot_tim[] = '';
      foreach ($assigned_task as $task_key => $task_value) {
        $workers = $task_value['worker'];
        $ex_worker = explode(',', $workers);
        if(in_array($user_id, $ex_worker))
        {
          $tot_tim[] = $task_value['timer_status'];
          $task_status = $task_value['priority'];
          if($task_status=='high'){
            $high++;
          }
          else if($task_status=='low'){
            $low++;
          }
          else if($task_status=='medium'){
            $medium++;
          }
          else if($task_status=='super_urgent')
          {
            $super_urgent++;
          }
        }
      }

  /** for 09-08-2018 task timer **/
  $individual_timer=$this->db->query("select * from individual_task_timer where user_id=".$_SESSION['id']." ")->result_array();
$pause_val='on';
$pause='on';
$for_total_time=0;
if(count($individual_timer)>0){
foreach ($individual_timer as $intime_key => $intime_value) {
	 $task_id=$intime_value['task_id'];
  $task_data=$this->db->query("select * from add_new_task where id=".$task_id." ")->row_array();
  if(!empty($task_data)){
  $its_time=$intime_value['time_start_pause'];
  $res=explode(',', $its_time);
    $res1=array_chunk($res,2);
  $result_value=array();
//  $pause='on';
  foreach($res1 as $rre_key => $rre_value)
  {
     $abc=$rre_value;
     if(count($abc)>1){
     if($abc[1]!='')
     {
        $ret_val=$this->calculate_test($abc[0],$abc[1]);
        array_push($result_value, $ret_val) ;
     }
     else
     {
      $pause='';
      $pause_val='';
        $ret_val=$this->calculate_test($abc[0],time());
         array_push($result_value, $ret_val) ;
     }
    }
    else
    {
      $pause='';
      $pause_val='';
        $ret_val=$this->calculate_test($abc[0],time());
         array_push($result_value, $ret_val) ;
    }
  }
  // $time_tot=0;
   foreach ($result_value as $re_key => $re_value) {
      //$time_tot+=time_to_sec($re_value) ;
      $for_total_time+=$this->time_to_sec($re_value) ;
   }

} // task check
} // for
} // if

    //  $tot_time = array_sum($tot_tim);
      $tot_time=$for_total_time;
      $task_cnt = count($tot_tim);
      $data['high'] = $high;
      $data['low'] = $low;
      $data['medium'] = $medium;
      $data['super_urgent'] = $super_urgent;
      $data['tot_tim'] = $tot_time;
      $data['task_cnt'] = $task_cnt;

       //$data['user_activity']=$this->Common_mdl->GetAllWithWhere('activity_log','user_id',$id);
       $data['user_activity']=$this->Common_mdl->orderby('activity_log','user_id',$id,'desc');
      
//echo $this->db->last_query();die;
      /* for($i=1; $i<=$data['total_count_month'];$i++)
       {
        echo $i;
       }die;
*/
        $data['task_list']=$this->Common_mdl->getallrecords('add_new_task');
        //$data['getCompany']=$this->db->query('select * from user where firm_admin_id = '.$id.' and role = 4')->result_array();
        $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = ".$id." and role = 4 and crm_name !=''")->row_array();

        $us_id =  explode(',', $sql['us_id']);





        $data['getCompany'] = $this->db->query( "SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and reminder_block=0")->result_array();
        /*  echo '<pre>';
          print_r($data['getCompany']);die;*/
        $con_stat = 'on';
        $acc_stat = 'on';
        $c_tax_stat = 'on';
        $p_tax_stat = 'on';
        $payroll_stat = 'on';
        $workplace_stat = 'on';
        $vat_stat = 'on';
        $cis_stat = 'on';
        $cissub_stat = 'on';
        $p11d_stat = 'on';
        $bookkeep_stat = 'on';
        $management_stat = 'on';
        $investgate_stat = 'on';
        $registered_stat = 'on';
        $taxadvice_stat = 'on';
        $taxinvest_stat = 'on';

          $ids = array();
        foreach ($data['getCompany'] as $getCompanykey => $getCompanyvalue) {
          $con = (isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnCst =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnCst = '';

          $acc = (isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnAcc =  json_decode($getCompanyvalue['accounts'])->tab : $jsnAcc = '';

          $c_tax = (isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsntax =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsntax = '';

          $p_tax = (isset(json_decode($getCompanyvalue['personal_tax_return'])->tab) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnp_tax =  json_decode($getCompanyvalue['personal_tax_return'])->tab : $jsnp_tax = '';

          $payroll = (isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpay =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpay = '';

          $workplace = (isset(json_decode($getCompanyvalue['workplace'])->tab) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->tab : $jsnworkplace = '';

          $vat = (isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = '';

          $cis = (isset(json_decode($getCompanyvalue['cis'])->tab) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->tab : $jsncis = '';

          $cissub = (isset(json_decode($getCompanyvalue['cissub'])->tab) && $getCompanyvalue['cissub'] != '') ? $jsncis_sub =  json_decode($getCompanyvalue['cissub'])->tab : $jsncis_sub = '';

          $p11d = (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = '';

          $bookkeep = (isset(json_decode($getCompanyvalue['bookkeep'])->tab) && $getCompanyvalue['bookkeep'] != '') ? $jsnbk =  json_decode($getCompanyvalue['bookkeep'])->tab : $jsnbk = '';

          $management = (isset(json_decode($getCompanyvalue['management'])->tab) && $getCompanyvalue['management'] != '') ? $jsnmgnt =  json_decode($getCompanyvalue['management'])->tab : $jsnmgnt = '';

          $investgate = (isset(json_decode($getCompanyvalue['investgate'])->tab) && $getCompanyvalue['investgate'] != '') ? $jsninvest =  json_decode($getCompanyvalue['investgate'])->tab : $jsninvest = '';

          $registered = (isset(json_decode($getCompanyvalue['registered'])->tab) && $getCompanyvalue['registered'] != '') ? $jsnreg =  json_decode($getCompanyvalue['registered'])->tab : $jsnreg = '';

          $taxadvice =(isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxad =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxad = '';

          $taxinvest = (isset(json_decode($getCompanyvalue['taxinvest'])->tab) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->tab : $jsntaxinvest = '';


          if($con==$con_stat && $acc==$acc_stat && $c_tax==$c_tax_stat && $p_tax==$p_tax_stat && $payroll==$payroll_stat && $workplace==$workplace_stat && $vat==$vat_stat && $cis==$cis_stat && $cissub==$cissub_stat && $p11d==$p11d_stat && $bookkeep==$bookkeep_stat && $management==$management_stat && $investgate==$investgate_stat && $registered==$registered_stat && $taxadvice==$taxadvice_stat && $taxinvest==$taxinvest_stat)
          {
            $ids[] = $getCompanyvalue['id'];
          }
        }

             $data['getCompany'] = $this->db->query( "SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and id in (  '" . implode( "','", $ids ) . "' )")->result_array();


  $data['getCompanyss'] = $this->db->query( "SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !=''")->result_array();

   $data['getCompany'] = $this->db->query( "SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' ")->result_array();
  $user_id=$this->session->userdata['userId'];
  $data['custom_form']=$this->Common_mdl->select_record('staff_custom_form','user_id',$user_id);


       $us_id = $this->session->userdata('admin_id');
      // echo $id;
        // $user_id = $this->session->userdata('id');
        // $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = ".$id." and role = 4 and crm_name !=''")->row_array();
        //  echo $this->db->last_query();
        // $us_id =  explode(',', $sql['us_id']);
        $current_date=date("Y-m-d");
        $time=date("Y-m-d");
        $data['today'] = $this->db->query( "SELECT * FROM client WHERE user_id ='".$us_id."' and crm_company_name !='' and ((crm_confirmation_statement_due_date= '".$current_date."') or (crm_ch_accounts_next_due = '".$current_date."') or (crm_accounts_tax_date_hmrc = '".$current_date."' ) or (crm_personal_due_date_return = '".$current_date."' ) or (crm_vat_due_date = '".$current_date."' ) or (crm_rti_deadline = '".$current_date."')  or (crm_pension_subm_due_date = '".$current_date."') or (crm_next_p11d_return_due = '".$current_date."')  or (crm_next_manage_acc_date = '".$current_date."') or (crm_next_booking_date = '".$current_date."' ) or (crm_insurance_renew_date = '".$current_date."')  or (crm_registered_renew_date = '".$current_date."' ) or (crm_investigation_end_date = '".$current_date."' )) and reminder_block=0")->result_array();

      //  echo $this->db->last_query();
        $oneweek_end_date = date('Y-m-d', strtotime($time . ' +1 week'));
          //echo $end_date;
        $data['oneweek'] = $this->db->query( "SELECT * FROM client WHERE user_id ='".$us_id."' and crm_company_name !='' and ((crm_confirmation_statement_due_date BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_ch_accounts_next_due BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_personal_due_date_return BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_vat_due_date BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_rti_deadline BETWEEN '".$current_date."' AND '".$oneweek_end_date."' )  or (crm_pension_subm_due_date BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_next_p11d_return_due BETWEEN '".$current_date."' AND '".$oneweek_end_date."' )  or (crm_next_manage_acc_date BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_next_booking_date BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_insurance_renew_date BETWEEN '".$current_date."' AND '".$oneweek_end_date."' )  or (crm_registered_renew_date BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_investigation_end_date BETWEEN '".$current_date."' AND '".$oneweek_end_date."' )) and reminder_block=0")->result_array();
        $onemonth_end_date = date('Y-m-d', strtotime($time . ' +1 month'));
        $data['onemonth'] = $this->db->query( "SELECT * FROM client WHERE user_id ='".$us_id."' and crm_company_name !='' and ((crm_confirmation_statement_due_date BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_ch_accounts_next_due BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_personal_due_date_return BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_vat_due_date BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_rti_deadline BETWEEN '".$current_date."' AND '".$onemonth_end_date."' )  or (crm_pension_subm_due_date BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_next_p11d_return_due BETWEEN '".$current_date."' AND '".$onemonth_end_date."' )  or (crm_next_manage_acc_date BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_next_booking_date BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_insurance_renew_date BETWEEN '".$current_date."' AND '".$onemonth_end_date."' )  or (crm_registered_renew_date BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_investigation_end_date BETWEEN '".$current_date."' AND '".$onemonth_end_date."' )) and reminder_block=0 ")->result_array();
        $oneyear_end_date = date('Y-m-d', strtotime($time . ' +1 year'));
        $data['oneyear'] = $this->db->query( "SELECT * FROM client WHERE user_id ='".$us_id."' and crm_company_name !='' and ((crm_confirmation_statement_due_date BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_ch_accounts_next_due BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_personal_due_date_return BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_vat_due_date BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_rti_deadline BETWEEN '".$current_date."' AND '".$oneyear_end_date."' )  or (crm_pension_subm_due_date BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_next_p11d_return_due BETWEEN '".$current_date."' AND '".$oneyear_end_date."' )  or (crm_next_manage_acc_date BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_next_booking_date BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_insurance_renew_date BETWEEN '".$current_date."' AND '".$oneyear_end_date."' )  or (crm_registered_renew_date BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_investigation_end_date BETWEEN '".$current_date."' AND '".$oneyear_end_date."' )) and reminder_block=0 ")->result_array();
      

        $last_month_start = date("Y-m-01");
        $last_mon_end = date("Y-m-t");  

         $data['confirmation'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_confirmation_statement_due_date between "'.$last_month_start.'" AND "'.$last_mon_end.'")) and reminder_block=0')->row_array();
        $data['account'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_ch_accounts_next_due between "'.$last_month_start.'" AND "'.$last_mon_end.'")) and reminder_block=0')->row_array();
        $data['company_tax'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_accounts_tax_date_hmrc between "'.$last_month_start.'" AND "'.$last_mon_end.'")) and reminder_block=0')->row_array();
        $data['personal_due'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id ='.$us_id.' and crm_company_name !="" and ((crm_personal_due_date_return between "'.$last_month_start.'" AND "'.$last_mon_end.'")) and reminder_block=0')->row_array();
        $data['vat'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_vat_due_date between "'.$last_month_start.'" AND "'.$last_mon_end.'")) and reminder_block=0')->row_array();
        $data['payroll'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_rti_deadline between "'.$last_month_start.'" AND "'.$last_mon_end.'")) and reminder_block=0')->row_array();
        $data['pension'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_pension_subm_due_date between "'.$last_month_start.'" AND "'.$last_mon_end.'")) and reminder_block=0')->row_array();
        $data['plld'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_next_p11d_return_due between "'.$last_month_start.'" AND "'.$last_mon_end.'")) and reminder_block=0')->row_array();
        $data['management'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_next_manage_acc_date between "'.$last_month_start.'" AND "'.$last_mon_end.'")) and reminder_block=0')->row_array();
        $data['booking'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_next_booking_date between "'.$last_month_start.'" AND "'.$last_mon_end.'")) and reminder_block=0')->row_array();
        $data['insurance'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_insurance_renew_date between "'.$last_month_start.'" AND "'.$last_mon_end.'")) and reminder_block=0')->row_array();
        $data['registeration'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_registered_renew_date between "'.$last_month_start.'" AND "'.$last_mon_end.'")) and reminder_block=0')->row_array();
        $data['investigation'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_investigation_end_date between "'.$last_month_start.'" AND "'.$last_mon_end.'")) and reminder_block=0')->row_array(); 

        $data['todo_list']=$this->db->query("select * from `todos_list` where status='active'  and user_id='".$_SESSION['id']."' order by order_number Asc")->result_array();

        $data['todo_completedlist']=$this->db->query("select * from `todos_list` where status='completed'  and user_id='".$_SESSION['id']."' order by order_number Asc")->result_array();

        $data['todolist']=$this->db->query("select * from `todos_list` order by order_number Asc")->result_array();

        $last_month_start = date("Y-m-d");
        $last_month_end = date("Y-m-t"); 

        // $data['task_list']=$this->db->query("SELECT * FROM `add_new_task` WHERE `end_date` between '".$last_month_start."' and  '".$last_month_end."' and priority='high' and user_id='".$us_id."'  order by id desc Limit 0,5")->result_array();

           if($_SESSION['role']=='6'){

            $data['task_list']=$this->db->query("SELECT * FROM `add_new_task` WHERE  FIND_IN_SET('".$_SESSION['id']."',worker)  order by id desc Limit 0,5")->result_array();

        }else if($_SESSION['role']=='4'){

            $id_get=$this->db->query("select id from client where user_id='".$_SESSION['id']."'")->row_array();
           // echo "SELECT * FROM `add_new_task` WHERE  id='".$id_get['id']."'  order by id desc Limit 0,5";
            $data['task_list']=$this->db->query("SELECT * FROM `add_new_task` WHERE  company_name='".$id_get['id']."'  order by id desc Limit 0,5")->result_array();

        }else{

            $data['task_list']=$this->db->query("SELECT * FROM `add_new_task` WHERE  create_by ='".$_SESSION['id']."' order by id desc Limit 0,5")->result_array(); 
        }
        


        $last_month_start = date("Y-m-01");
        $last_mon_end = date("Y-m-t");  


          if($_SESSION['role']=='1'){


              $data['accept_proposal']=$this->db->query('select count(*) as proposal_count from proposals
            where status="accepted" and  created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"')->row_array();
            $data['indiscussion_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="in discussion" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"')->row_array();
            $data['sent_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="sent"  and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
            $data['viewed_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="opened"  and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
            $data['declined_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="declined" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
            $data['archive_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="archive" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
             $data['draft_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="draft"  and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();   

          
        }else if($_SESSION['role']=='4'){
          echo "select count(*) as proposal_count from proposals
            where status='accepted' and company_id='".$_SESSION['id']."' and created_at BETWEEN '".$last_month_start."' AND '".$last_mon_end."'";
             $data['accept_proposal']=$this->db->query('select count(*) as proposal_count from proposals
            where status="accepted" and company_id="'.$_SESSION['id'].'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"')->row_array();
            $data['indiscussion_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="in discussion" and company_id="'.$_SESSION['id'].'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"')->row_array();
            $data['sent_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="sent"  and company_id="'.$_SESSION['id'].'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
            $data['viewed_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="opened" and company_id="'.$_SESSION['id'].'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
            $data['declined_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="declined" and company_id="'.$_SESSION['id'].'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
            $data['archive_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="archive" and company_id="'.$_SESSION['id'].'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
             $data['draft_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="draft" and company_id="'.$_SESSION['id'].'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();


        }else{

            $data['accept_proposal']=$this->db->query('select count(*) as proposal_count from proposals
            where status="accepted" and user_id="'.$_SESSION['id'].'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"')->row_array();
            $data['indiscussion_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="in discussion" and user_id="'.$_SESSION['id'].'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"')->row_array();
            $data['sent_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="sent"  and user_id="'.$_SESSION['id'].'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
            $data['viewed_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="opened" and user_id="'.$_SESSION['id'].'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
            $data['declined_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="declined" and user_id="'.$_SESSION['id'].'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
            $data['archive_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="archive" and user_id="'.$_SESSION['id'].'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
             $data['draft_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="draft" and user_id="'.$_SESSION['id'].'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
          
        }



       $last_Year_start = date("Y-01-01");
          $last_month_end = date("Y-12-31");
        $data['Private']=$this->db->query('select count(*) as limit_count from client where crm_legal_form="Private Limited company" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"')->row_array();         
        $data['Public']=$this->db->query('select count(*) as limit_count from client where crm_legal_form="Public Limited company" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"' )->row_array();
        $data['limited']=$this->db->query('select count(*) as limit_count from client where crm_legal_form="Limited Liability Partnership" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"' )->row_array();
        $data['Partnership']=$this->db->query('select count(*) as limit_count from client where crm_legal_form="Partnership" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"' )->row_array();
        $data['self']=$this->db->query('select count(*) as limit_count from client where crm_legal_form="Self Assessment" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"' )->row_array();
        $data['Trust']=$this->db->query('select count(*) as limit_count from client where crm_legal_form="Trust" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"')->row_array();
        $data['Charity']=$this->db->query('select count(*) as limit_count from client where crm_legal_form="Charity" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"')->row_array();
        $data['Other']=$this->db->query('select count(*) as limit_count from client where crm_legal_form="Other" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"' )->row_array();

         $data['active_client']=$this->db->query("SELECT count(*) as client_count FROM user where firm_admin_id='".$us_id."' AND autosave_status!='1' and crm_name!='' and status='1' order by id DESC")->row_array();
        $data['inactive_client']=$this->db->query("SELECT count(*) as client_count FROM user where firm_admin_id='".$us_id."' AND autosave_status!='1' and crm_name!='' and status='2' order by id DESC")->row_array();
        $data['completed_task']=$this->db->query("SELECT count(*) as task_count FROM add_new_task
        where user_id='".$us_id."' and task_status='complete'")->row_array();
        $data['inprogress_task']=$this->db->query("SELECT count(*) as task_count FROM add_new_task
        where user_id='".$us_id."' and task_status='inprogress'")->row_array();
        $data['getCompanyvalue'] = $this->db->query( "SELECT * FROM client WHERE user_id ='".$_SESSION['id']."'")->row_array();
        $data['manager'] = $this->db->query('select * from user where (role="5" or role="3") and status="1"')->result_array();



        if($_SESSION['role']=='4'){

        $data['log_datas']=$this->db->query("select * from timeline_services_notes_added where user_id=".$_SESSION['id']." and( module='calllog' or module='meeting') order by date asc")->result_array();          
      }


        if($_SESSION['role']=='6'){

         // echo $_SESSION['id'];

         // echo "select * from responsible_user where manager_reviewer=".$_SESSION['id']." ";

          $Client_id=$this->db->query("select * from responsible_user where manager_reviewer=".$_SESSION['id']."")->result_array();
            
          $notes=array();
          foreach($Client_id as $client){     

          // echo "select * from timeline_services_notes_added where user_id=".$client['client_id']." and( module='calllog' or module='meeting') order by date asc ";

              $notes_section=$this->db->query("select * from timeline_services_notes_added where user_id=".$client['client_id']." and( module='calllog' or module='meeting') order by date asc ")->result_array();     
              foreach($notes_section as $note){     
              if($note['module']!=''){
                  array_push($notes,$note);
             }  
             } 
          }      

                  $data['log_datas']=$notes;
      //     $data['log_datas']=$this->Common_mdl->GetAllWithWhere('timeline_services_notes_added','user_id',$_SESSION['id']);
      }


        if($_SESSION['role']=='6'){

             $data['tasks']=$this->db->query('select count(*) as task_count from add_new_task where FIND_IN_SET("'.$_SESSION['id'].'",worker)')->row_array();
            $data['closed_tasks']=$this->db->query('select count(*) as task_count from add_new_task where FIND_IN_SET("'.$_SESSION['id'].'",worker) AND task_status="complete"')->row_array();
            $data['pending_tasks']=$this->db->query('select count(*) as task_count from add_new_task where FIND_IN_SET("'.$_SESSION['id'].'",worker) AND task_status="inprogress"')->row_array();
            $data['cancelled_tasks']=$this->db->query('select count(*) as task_count from add_new_task where FIND_IN_SET("'.$_SESSION['id'].'",worker) AND task_status="notstarted"')->row_array(); 
        }else if($_SESSION['role']=='4'){

            $id_get=$this->db->query("select id from client where user_id='".$_SESSION['id']."'")->row_array();            
            $data['tasks']=$this->db->query('select count(*) as task_count from add_new_task where company_name='.$id_get['id'].'')->row_array();

            $data['closed_tasks']=$this->db->query('select count(*) as task_count from add_new_task where company_name='.$id_get['id'].' AND task_status="complete"')->row_array();
            $data['pending_tasks']=$this->db->query('select count(*) as task_count from add_new_task where company_name='.$id_get['id'].' AND task_status="inprogress"')->row_array();
            $data['cancelled_tasks']=$this->db->query('select count(*) as task_count from add_new_task where company_name='.$id_get['id'].' AND task_status="notstarted"')->row_array();
        }else{            
            $data['tasks']=$this->db->query('select count(*) as task_count from add_new_task
            where create_by='.$_SESSION['id'].'')->row_array();
            $data['closed_tasks']=$this->db->query('select count(*) as task_count from add_new_task where create_by='.$_SESSION['id'].' AND task_status="complete"')->row_array();
            $data['pending_tasks']=$this->db->query('select count(*) as task_count from add_new_task where create_by='.$_SESSION['id'].' AND task_status="inprogress"')->row_array();
            $data['cancelled_tasks']=$this->db->query('select count(*) as task_count from add_new_task where create_by='.$_SESSION['id'].' AND task_status="notstarted"')->row_array();                
        }



       $this->load->view('staffs/staff_profile_update',$data); 
    }



      public function client_filter(){
        $last_Year_start = date("Y-m-01");
        $last_month_end = date("Y-m-t");        

        $data['Private']=$this->db->query('select count(*) as limit_count from client where crm_legal_form="Private Limited company" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"')->row_array();
       //   echo $this->db->last_query();
        $data['Public']=$this->db->query('select count(*) as limit_count from client where crm_legal_form="Public Limited company" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"' )->row_array();
        $data['limited']=$this->db->query('select count(*) as limit_count from client where crm_legal_form="Limited Liability Partnership" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"' )->row_array();
        $data['Partnership']=$this->db->query('select count(*) as limit_count from client where crm_legal_form="Partnership" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"' )->row_array();
        $data['self']=$this->db->query('select count(*) as limit_count from client where crm_legal_form="Self Assessment" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"' )->row_array();
        $data['Trust']=$this->db->query('select count(*) as limit_count from client where crm_legal_form="Trust" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"')->row_array();
        $data['Charity']=$this->db->query('select count(*) as limit_count from client where crm_legal_form="Charity" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"')->row_array();
        $data['Other']=$this->db->query('select count(*) as limit_count from client where crm_legal_form="Other" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"' )->row_array();
        $this->load->view('firm_dashboard/chart_success',$data);
        //echo json_encode($data);
    }


    


    public function client_filter_year(){
        $last_Year_start = date("Y-01-01");
        $last_month_end = date("Y-12-31");        

        $data['Private']=$this->db->query('select count(*) as limit_count from client where crm_legal_form="Private Limited company" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"')->row_array();
       //   echo $this->db->last_query();
        $data['Public']=$this->db->query('select count(*) as limit_count from client where crm_legal_form="Public Limited company" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"' )->row_array();
        $data['limited']=$this->db->query('select count(*) as limit_count from client where crm_legal_form="Limited Liability Partnership" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"' )->row_array();
        $data['Partnership']=$this->db->query('select count(*) as limit_count from client where crm_legal_form="Partnership" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"' )->row_array();
        $data['self']=$this->db->query('select count(*) as limit_count from client where crm_legal_form="Self Assessment" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"' )->row_array();
        $data['Trust']=$this->db->query('select count(*) as limit_count from client where crm_legal_form="Trust" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"')->row_array();
        $data['Charity']=$this->db->query('select count(*) as limit_count from client where crm_legal_form="Charity" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"')->row_array();
        $data['Other']=$this->db->query('select count(*) as limit_count from client where crm_legal_form="Other" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"' )->row_array();
        $this->load->view('firm_dashboard/chart_success',$data);
        //echo json_encode($data);
    }

    public function deadline_filter_year(){
       
      $us_id = $this->session->userdata('admin_id');
          // $user_id = $this->session->userdata('id');
          // $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = ".$id." and role = 4 and crm_name !=''")->row_array();

          // $us_id =  explode(',', $sql['us_id']);

          $current_date=date("Y-01-01");
          $time=date("Y-m-d");         
          $oneyear_end_date = date('Y-m-d', strtotime($time . ' +1 year'));
          $data['confirmation'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id ="'.$us_id .'" and crm_company_name !="" and ((crm_confirmation_statement_due_date between "'.$current_date.'" AND "'.$oneyear_end_date.'"))')->row_array();
           $data['account'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id ="'.$us_id .'" and crm_company_name !="" and ((crm_ch_accounts_next_due between "'.$current_date.'" AND "'.$oneyear_end_date.'"))')->row_array();
           $data['company_tax'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id ="'.$us_id .'" and crm_company_name !="" and ((crm_accounts_tax_date_hmrc between "'.$current_date.'" AND "'.$oneyear_end_date.'"))')->row_array();
           $data['personal_due'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id ="'.$us_id .'" and crm_company_name !="" and ((crm_personal_due_date_return between "'.$current_date.'" AND "'.$oneyear_end_date.'"))')->row_array();
           $data['vat'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id ="'.$us_id .'" and crm_company_name !="" and ((crm_vat_due_date between "'.$current_date.'" AND "'.$oneyear_end_date.'"))')->row_array();
           $data['payroll'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id ="'.$us_id .'" and crm_company_name !="" and ((crm_rti_deadline between "'.$current_date.'" AND "'.$oneyear_end_date.'"))')->row_array();
          $data['pension'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id ="'.$us_id .'" and crm_company_name !="" and ((crm_pension_subm_due_date between "'.$current_date.'" AND "'.$oneyear_end_date.'"))')->row_array();
          $data['plld'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id ="'.$us_id .'" and crm_company_name !="" and ((crm_next_p11d_return_due between "'.$current_date.'" AND "'.$oneyear_end_date.'"))')->row_array();
          $data['management'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id ="'.$us_id .'" and crm_company_name !="" and ((crm_next_manage_acc_date between "'.$current_date.'" AND "'.$oneyear_end_date.'"))')->row_array();
          $data['booking'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id ="'.$us_id .'" and crm_company_name !="" and ((crm_next_booking_date between "'.$current_date.'" AND "'.$oneyear_end_date.'"))')->row_array();
          $data['insurance'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id ="'.$us_id .'" and crm_company_name !="" and ((crm_insurance_renew_date between "'.$current_date.'" AND "'.$oneyear_end_date.'"))')->row_array();
          $data['registeration'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id ="'.$us_id .'" and crm_company_name !="" and ((crm_registered_renew_date between "'.$current_date.'" AND "'.$oneyear_end_date.'"))')->row_array();
          $data['investigation'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id ="'.$us_id .'" and crm_company_name !="" and ((crm_investigation_end_date between "'.$current_date.'" AND "'.$oneyear_end_date.'"))')->row_array(); 
          $this->load->view('staffs/deadline_chart',$data);
    }

        public function deadline_filter_month(){
       
      $us_id = $this->session->userdata('admin_id');
          // $user_id = $this->session->userdata('id');
          // $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = ".$id." and role = 4 and crm_name !=''")->row_array();

          // $us_id =  explode(',', $sql['us_id']);

          $current_date=date("Y-m-d");
          $time=date("Y-m-d");
          $current_date=date("Y-m-d");
          $oneyear_end_date = date('Y-m-d', strtotime($time . ' +1 year'));
           $last_Year_start = date("Y-m-01");
          $last_month_end = date("Y-m-t");  
          $data['confirmation'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id ="'.$us_id.'" and crm_company_name !="" and ((crm_confirmation_statement_due_date between "'.$last_Year_start.'" AND "'.$last_month_end.'"))')->row_array();
        //  echo $this->db->last_query();
           $data['account'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id ="'.$us_id.'" and crm_company_name !="" and ((crm_ch_accounts_next_due between "'.$last_Year_start.'" AND "'.$last_month_end.'"))')->row_array();
           $data['company_tax'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id ="'.$us_id.'" and crm_company_name !="" and ((crm_accounts_tax_date_hmrc between "'.$last_Year_start.'" AND "'.$last_month_end.'"))')->row_array();

           $data['personal_due'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id ="'.$us_id.'" and crm_company_name !="" and ((crm_personal_due_date_return between "'.$last_Year_start.'" AND "'.$last_month_end.'"))')->row_array();

           $data['vat'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id ="'.$us_id.'" and crm_company_name !="" and ((crm_vat_due_date between "'.$last_Year_start.'" AND "'.$last_month_end.'"))')->row_array();

          $data['payroll'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id ="'.$us_id.'" and crm_company_name !="" and ((crm_rti_deadline between "'.$last_Year_start.'" AND "'.$last_month_end.'"))')->row_array();

          $data['pension'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id ="'.$us_id.'" and crm_company_name !="" and ((crm_pension_subm_due_date between "'.$last_Year_start.'" AND "'.$last_month_end.'"))')->row_array();

          $data['plld'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id ="'.$us_id.'" and crm_company_name !="" and ((crm_next_p11d_return_due between "'.$last_Year_start.'" AND "'.$last_month_end.'"))')->row_array();

          $data['management'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id="'.$us_id.'" and crm_company_name !="" and ((crm_next_manage_acc_date between "'.$last_Year_start.'" AND "'.$last_month_end.'"))')->row_array();

          $data['booking'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id ="'.$us_id.'" and crm_company_name !="" and ((crm_next_booking_date between "'.$last_Year_start.'" AND "'.$last_month_end.'"))')->row_array();

          $data['insurance'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id ="'.$us_id.'" and crm_company_name !="" and ((crm_insurance_renew_date between "'.$last_Year_start.'" AND "'.$last_month_end.'"))')->row_array();

          $data['registeration'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id ="'.$us_id.'" and crm_company_name !="" and ((crm_registered_renew_date between "'.$last_Year_start.'" AND "'.$last_month_end.'"))')->row_array();

          $data['investigation'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id ="'.$us_id.'" and crm_company_name !="" and ((crm_investigation_end_date between "'.$last_Year_start.'" AND "'.$last_month_end.'"))')->row_array(); 
          $this->load->view('staffs/deadline_chart',$data);
    }

    public function filter_proposal_year(){
       $last_month_start = date("Y-01-01");
        $last_mon_end = date("Y-12-31");  
          $us_id = $this->session->userdata('admin_id');
        $data['accept_proposal']=$this->db->query('select count(*) as proposal_count from proposals
        where status="accepted" and user_id ="'.$us_id.'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"')->row_array();       
        $data['indiscussion_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="in discussion" and user_id ="'.$us_id.'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"')->row_array();
        $data['sent_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="sent" and user_id ="'.$us_id.'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
        $data['viewed_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="opened" and user_id ="'.$us_id.'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
        $data['declined_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="declined" and user_id ="'.$us_id.'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
        $data['archive_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="archive" and user_id ="'.$us_id.'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
        $this->load->view('firm_dashboard/proposal_chart',$data);        
    }

    public function filter_proposal_month(){
       $last_month_start = date("Y-m-01");
        $last_mon_end = date("Y-m-t");  
        $us_id = $this->session->userdata('admin_id');
        $data['accept_proposal']=$this->db->query('select count(*) as proposal_count from proposals
        where status="accepted" and user_id ="'.$us_id.'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"')->row_array();       
        $data['indiscussion_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="in discussion" and user_id ="'.$us_id.'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"')->row_array();
        $data['sent_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="sent" and user_id ="'.$us_id.'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
        $data['viewed_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="opened" and user_id ="'.$us_id.'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
        $data['declined_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="declined" and user_id ="'.$us_id.'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
        $data['archive_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="archive" and user_id ="'.$us_id.'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
        $this->load->view('firm_dashboard/proposal_chart',$data);
    }

    public function update_bank()
    {
       $data['bank_name'] = $_POST['bank_name'];
       $data['branch_name'] = $_POST['branch_name'];
       $data['account_name'] = $_POST['account_name'];
       $data['account_no'] = $_POST['account_no'];
       $id = $this->session->userdata('admin_id');
       $this->Common_mdl->update('user',$data,'id',$id);
            $datas['log'] = "Bank Account Details Update";
            $datas['createdTime'] = time();
            $datas['module'] = 'Profile change';
            $datas['user_id'] = $id;
            $this->Common_mdl->insert('activity_log',$datas);
       //echo $this->db->last_query();die;
       $this->session->set_flashdata('success', 'Staff Profile Details Updated Successfully');

       redirect('staff/Dashboard');
    }

    function weeks($month, $year){
        $num_of_days = date("t", mktime(0,0,0,$month,1,$year)); 
        $lastday = date("t", mktime(0, 0, 0, $month, 1, $year)); 
        $no_of_weeks = 0; 
        $count_weeks = 0; 
        while($no_of_weeks < $lastday){ 
            $no_of_weeks += 7; 
            $count_weeks++; 
        } 
    return $count_weeks;
}

function month_cnt($date)
{
$week_num =  date('W', strtotime($date));
return $week_num;
}

function start_end_date($date)
{
   $search_form = date('Y-m-01',strtotime($date));
   $search_to =  date('Y-m-t',strtotime($date));
   return $search_form.','.$search_to;
}

function time_card()
{
  $mon = $_POST['month'];
  $ex_mon = explode('/', $mon);
  $month = $ex_mon[0];
  $year = $ex_mon[1];
       $data['start_end_date'] = $this->start_end_date('01-'.$month.'-'.$year);
       $exp_date = explode(',', $data['start_end_date']);
//print_r($data['start_end_date'] );die;

       $start = $exp_date[0];
       $end = $exp_date[1];

       $data['start_month'] = $this->month_cnt($start);
       if($month=='12')
       {
       // echo date('W', mktime(0,0,0,12,28,$year));die;
        $data['end_month'] =$this->getIsoWeeksInYear($year)+1;
       // echo $data['end_month'];die;
       }else{
        $data['end_month'] = $this->month_cnt($end);
       }
       $data['total_count_month'] = $this->weeks($month,$year);

       $data['month_ex'] =  $month; 
       $data['year_ex'] =  $year; 

       $this->load->view('staffs/time_card_filter',$data);
}

function getIsoWeeksInYear($year) {
    $date = new DateTime;
    $date->setISODate($year, 53);
    return ($date->format("W") === "53" ? 53 : 52);
}

  public function delete($id)
    {
        $this->Common_mdl->delete('user','id',$id);
        $this->Common_mdl->delete('client','user_id',$id);
        redirect('staff/Dashboard');
    }

  public function companysearch()
  {
     $com_num = $_POST['com_num'];
     $legal_form = $_POST['legal_form'];

     $id = $this->session->userdata('admin_id');


     $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = ".$id." and role = 4 and crm_name !=''")->row_array();

        $us_id =  explode(',', $sql['us_id']);



$v_id = $_POST['values'];
$fields = $_POST['fields'];

/*$v_id =  implode(',', $val);
echo '<pre>';
print_r($val);die;*/
if($v_id=='true')
{
$s_val = 'on';
}else{
  $s_val = 'off';
}

/*if($v_id[1]=='true')
{
$acc_stat = 'on';
}else{
  $acc_stat = 'off';
}
if($v_id[2]=='true')
{
$c_tax_stat = 'on';
}else{
  $c_tax_stat = 'off';
}
if($v_id[3]=='true')
{
$p_tax_stat = 'on';
}else{
  $p_tax_stat = 'off';
}
if($v_id[4]=='true')
{
$payroll_stat = 'on';
}else{
  $payroll_stat = 'off';
}
if($v_id[5]=='true')
{
$workplace_stat = 'on';
}else{
  $workplace_stat = 'off';
}
if($v_id[6]=='true')
{
$vat_stat = 'on';
}else{
  $vat_stat = 'off';
}
if($v_id[7]=='true')
{
$cis_stat = 'on';
}else{
  $cis_stat = 'off';
}
if($v_id[8]=='true')
{
$cissub_stat = 'on';
}else{
  $cissub_stat = 'off';
}
if($v_id[9]=='true')
{
$p11d_stat = 'on';
}else{
  $p11d_stat = 'off';
}
if($v_id[10]=='true')
{
$bookkeep_stat = 'on';
}else{
  $bookkeep_stat = 'off';
}
if($v_id[11]=='true')
{
$management_stat = 'on';
}else{
  $management_stat = 'off';
}
if($v_id[12]=='true')
{
$investgate_stat = 'on';
}else{
  $investgate_stat = 'off';
}
if($v_id[13]=='true')
{
$registered_stat = 'on';
}else{
  $registered_stat = 'off';
}
if($v_id[14]=='true')
{
$taxadvice_stat = 'on';
}else{
  $taxadvice_stat = 'off';
}
if($v_id[15]=='true')
{
$taxinvest_stat = 'on';
}else{
  $taxinvest_stat = 'off';
}*/

/*$acc_stat = 'on';
$c_tax_stat = 'on';
$p_tax_stat = 'on';
$payroll_stat = 'on';
$workplace_stat = 'on';
$vat_stat = 'on';
$cis_stat = 'on';
$cissub_stat = 'on';
$p11d_stat = 'on';
$bookkeep_stat = 'on';
$management_stat = 'on';
$investgate_stat = 'on';
$registered_stat = 'on';
$taxadvice_stat = 'on';
$taxinvest_stat = 'on';*/

  $data['getCompany'] = $this->db->query( "SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' ")->result_array();



          $ids = array();
        foreach ($data['getCompany'] as $getCompanykey => $getCompanyvalue) {
          $conf_statement = (isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnCst =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnCst = 'off';

          $accounts = (isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnAcc =  json_decode($getCompanyvalue['accounts'])->tab : $jsnAcc = 'off';

          $company_tax_return = (isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsntax =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsntax = 'off';

          $personal_tax_return = (isset(json_decode($getCompanyvalue['personal_tax_return'])->tab) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnp_tax =  json_decode($getCompanyvalue['personal_tax_return'])->tab : $jsnp_tax = 'off';

          $payroll = (isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpay =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpay = 'off';

          $workplace = (isset(json_decode($getCompanyvalue['workplace'])->tab) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->tab : $jsnworkplace = 'off';

          $vat = (isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = 'off';

          $cis = (isset(json_decode($getCompanyvalue['cis'])->tab) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->tab : $jsncis = 'off';

          $cissub = (isset(json_decode($getCompanyvalue['cissub'])->tab) && $getCompanyvalue['cissub'] != '') ? $jsncis_sub =  json_decode($getCompanyvalue['cissub'])->tab : $jsncis_sub = 'off';

          $p11d = (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = 'off';

          $bookkeep = (isset(json_decode($getCompanyvalue['bookkeep'])->tab) && $getCompanyvalue['bookkeep'] != '') ? $jsnbk =  json_decode($getCompanyvalue['bookkeep'])->tab : $jsnbk = 'off';

          $management = (isset(json_decode($getCompanyvalue['management'])->tab) && $getCompanyvalue['management'] != '') ? $jsnmgnt =  json_decode($getCompanyvalue['management'])->tab : $jsnmgnt = 'off';

          $investgate = (isset(json_decode($getCompanyvalue['investgate'])->tab) && $getCompanyvalue['investgate'] != '') ? $jsninvest =  json_decode($getCompanyvalue['investgate'])->tab : $jsninvest = 'off';

          $registered = (isset(json_decode($getCompanyvalue['registered'])->tab) && $getCompanyvalue['registered'] != '') ? $jsnreg =  json_decode($getCompanyvalue['registered'])->tab : $jsnreg = 'off';

          $taxadvice =(isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxad =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxad = 'off';

          $taxinvest = (isset(json_decode($getCompanyvalue['taxinvest'])->tab) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->tab : $jsntaxinvest = 'off';

if($fields=='conf_statement')
{
  $f_val = $conf_statement;
}elseif($fields=='accounts')
{
  $f_val = $accounts;
}elseif($fields=='company_tax_return')
{
  $f_val = $company_tax_return;
}elseif($fields=='personal_tax_return')
{
  $f_val = $personal_tax_return;
}elseif($fields=='payroll')
{
  $f_val = $payroll;
}elseif($fields=='workplace')
{
  $f_val = $workplace;
}elseif($fields=='vat')
{
  $f_val = $vat;
}elseif($fields=='cis')
{
  $f_val = $cis;
}elseif($fields=='cissub')
{
  $f_val = $cissub;
}elseif($fields=='p11d')
{
  $f_val = $p11d;
}elseif($fields=='bookkeep')
{
  $f_val = $bookkeep;
}elseif($fields=='management')
{
  $f_val = $management;
}elseif($fields=='investgate')
{
  $f_val = $investgate;
}elseif($fields=='registered')
{
  $f_val = $registered;
}elseif($fields=='taxadvice')
{
  $f_val = $taxadvice;
}elseif($fields=='taxinvest')
{
  $f_val = $taxinvest;
}else
{
  $f_val = '';
}
         /* if($con==$con_stat && $acc==$acc_stat && $c_tax==$c_tax_stat && $p_tax==$p_tax_stat && $payroll==$payroll_stat && $workplace==$workplace_stat && $vat==$vat_stat && $cis==$cis_stat && $cissub==$cissub_stat && $p11d==$p11d_stat && $bookkeep==$bookkeep_stat && $management==$management_stat && $investgate==$investgate_stat && $registered==$registered_stat && $taxadvice==$taxadvice_stat && $taxinvest==$taxinvest_stat)*/
//echo $f_val.'--'.$s_val;
         if($v_id=='true'){
          if($f_val==$s_val)
          {
            $ids[] = $getCompanyvalue['id'];
          }}
          else{
            if($f_val==$s_val){
                $ids[] = $getCompanyvalue['id'];

            }
          }
        }
       /* echo '<pre>';
print_r($ids);die;*/
/*echo '<pre>';
print_r($ids);die;*/
           /* $data['getCompany'] = $this->db->query( "SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and id in (  '" . implode( "','", $ids ) . "' )")->result_array();
*/
if($_POST['values']=='' || $_POST['fields']=='' )
{
   $qy = "SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and  crm_company_name !='' ";
 }else{
   $qy = "SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and  crm_company_name !='' and id in (  '" . implode( "','", $ids ) . "' )";
 }




    
     if($com_num!='' && $legal_form!='')
     {
      $qy .="and crm_company_number = '$com_num' and crm_legal_form = '$legal_form'" ;
     }elseif($com_num!='' && $legal_form=='')
     {
      $qy .="and crm_company_number = '$com_num' ";
     }elseif($com_num=='' && $legal_form!='')
     {
      $qy .="and crm_legal_form = '$legal_form' ";
     }


    $data['getCompany'] = $this->db->query( $qy )->result_array();
    $this->load->view('staffs/filter_rec',$data);
  }

  public function service_update()
  {
    $values = $_POST['values'];
    $stat = $_POST['stat'];

    $exp_value = explode('_', $values);
    $field_name = $exp_value[0];
    $id = $exp_value[1];

    $client_rec = $this->Common_mdl->select_record('client','id',$id);

    //print_r($client_rec);

    $conf_statement = $client_rec['conf_statement'];
    $accounts = $client_rec['accounts'];
    $company_tax_return = $client_rec['company_tax_return'];
    $personal_tax_return = $client_rec['personal_tax_return'];
    $payroll = $client_rec['payroll'];
    $workplace = $client_rec['workplace'];
    $vat = $client_rec['vat'];
    $cis = $client_rec['cis'];
    $cissub = $client_rec['cissub'];
    $p11d = $client_rec['p11d'];
    $bookkeep = $client_rec['bookkeep'];
    $management  = $client_rec['management'];
    $investgate  = $client_rec['investgate'];
    $registered  = $client_rec['registered'];
    $taxadvice  = $client_rec['taxadvice'];
    $taxinvest  = $client_rec['taxinvest'];

    if($field_name=="vat")
    {
      $f_name = $vat;
      $fname = "vat";
    }elseif ($field_name=="payroll") {
    $f_name=$payroll;
    $fname="payroll";
    }elseif($field_name=="accounts"){
      $f_name = $accounts;
      $fname = "accounts";
    }elseif($field_name=="confstatement"){
      $f_name = $conf_statement;
      $fname = "conf_statement";
    }elseif($field_name=="companytax")
    {
      $f_name = $company_tax_return;
      $fname = "company_tax_return";
    }elseif($field_name=="personaltax"){
      $f_name = $personal_tax_return;
      $fname = "personal_tax_return";
    }elseif($field_name=="workplace"){
      $f_name = $workplace;
      $fname = "workplace";
    }elseif($field_name=="cis"){
      $f_name = $cis;
      $fname = "cis";
    }elseif($field_name=="cissub"){
      $f_name = $cissub;
      $fname = "cissub";
    }elseif($field_name=="p11d"){
      $f_name = $p11d;
      $fname = "p11d";
    }elseif($field_name=="bookkeep"){
      $f_name = $bookkeep;
      $fname = "bookkeep";
    }elseif($field_name=="management"){
      $f_name = $management;
      $fname = "management";
    }elseif($field_name=="investgate"){
      $f_name = $investgate;
      $fname = "investgate";
    }elseif($field_name=="registered"){
      $f_name = $registered;
      $fname = "registered";
    }elseif($field_name=="taxadvice"){
      $f_name = $taxadvice;
      $fname = "taxadvice";
    }elseif($field_name=="taxinvest"){
      $f_name = $taxinvest;
      $fname = "taxinvest";
    }

    if($stat=='on')
    {
      $rec = json_decode($f_name);
      /*echo "<pre>";
      print_r($rec);*/
      if($rec!=''){
      unset($rec->tab);
      $arr_rec = array("tab"=>"on");
      $arr_old = $this->object_to_array($rec);
      $result = array_merge($arr_rec, $arr_old);
    }
      else{
        $result = array("tab"=>"on");
      }
    }
    else{
      $rec = json_decode($f_name);
      /* echo "<pre>";
      print_r($rec);*/
      if($rec!=""){
      unset($rec->tab);
      $result = $this->object_to_array($rec);
      }else{
        $result = array();
      }
    }
    echo '<pre>';
print_r($result);
    $up_rec = json_encode($result);

    $data[$fname] = $up_rec;

  $this->Common_mdl->update('client',$data,'id',$id);
  echo $this->db->last_query();

  }

  function object_to_array($data)
{
    if (is_array($data) || is_object($data))
    {
        $result = array();
        foreach ($data as $key => $value)
        {
            $result[$key] = $this->object_to_array($value);
        }
        return $result;
    }
    return $data;
}

public function staff_report($id)
{
 
  require  'PHPExcel-1.8/Classes/PHPExcel.php';
  require   'PHPExcel-1.8/Classes/PHPExcel/IOFactory.php';
    
  //$filepath = base_url().'/download_excel/';
    
    //$fileName = 'Reporte_Semanal_'.date('d-m-Y').'.xls';

    $staff_name=$this->Common_mdl->select_record('user','id',$id);

    $fileName = $staff_name['crm_name'].'-Report.xls';

    //$login_history=$this->Common_mdl->GetAllWithWhere('activity_log','user_id',$id);
    $login_history=$this->db->query("select * from activity_log where user_id=".$id." and ( module='Login' or module='Logout' )")->result_array();


  //echo "<pre>"; print_r($seller_products); echo "</pre>";

    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();
    
    // Create a first sheet, representing sales data
     $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', ''. $staff_name['crm_name'] .' - Login History');

    $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A3', 'S.No')
            ->setCellValue('B3', 'Date')  
            ->setCellValue('C3', 'Activity')  
            ->setCellValue('D3', 'Log');  
            

    $i = 4;
    $k = 1;
    foreach($login_history as $row) {
    
      $objPHPExcel->setActiveSheetIndex(0)
          ->setCellValue('A'.$i, $k)
          ->setCellValue('B'.$i, date('d-m-Y H:i:s', $row['CreatedTime']))
          ->setCellValue('C'.$i, $row['module'])
          ->setCellValue('D'.$i, $row['log']);
      $i++;
      $k++;
    }
    
    // Rename sheet
    $objPHPExcel->getActiveSheet()->setTitle('login_history');

    $task_done = $this->db->query("select * from add_new_task where task_status='complete' and related_to !='Auto created task'")->result_array();

    // Create a new worksheet, after the default sheet
    $objPHPExcel->createSheet();

 $objPHPExcel->setActiveSheetIndex(1)
            ->setCellValue('A1', ''. $staff_name['crm_name'] .' - Completed Task Report');

    // Add some data to the second sheet, resembling some different data types
    $objPHPExcel->setActiveSheetIndex(1)
          ->setCellValue('A3', 'S.No')  
          ->setCellValue('B3', 'Task Name')
          ->setCellValue('C3', 'Task Start Date')
          ->setCellValue('D3', 'Task End Date')
          ->setCellValue('E3', 'Task Status');


    $j=4;
    $s=1;
    foreach ($task_done as $value) {
      if($value['worker']=='')
      {
      $value['worker'] = 0;
      }
      $explode_worker=explode(',',$value['worker']);
     if(in_array( $id ,$explode_worker )){
        $objPHPExcel->setActiveSheetIndex(1)
          ->setCellValue('A'.$j, $s)
          ->setCellValue('B'.$j, $value['subject'])  
          ->setCellValue('C'.$j, $value['start_date'])
          ->setCellValue('D'.$j, $value['end_date'])
          ->setCellValue('E'.$j, $value['task_status']);
      $j++;
      $s++;
    }}
      
    // Rename 2nd sheet
    $objPHPExcel->getActiveSheet()->setTitle('Task Done Records');


     $ser_task_done = $this->db->query("select * from add_new_task where task_status='complete' and related_to ='Auto created task'")->result_array();

    // Create a new worksheet, after the default sheet
    $objPHPExcel->createSheet();

$objPHPExcel->setActiveSheetIndex(2)
            ->setCellValue('A1', ''. $staff_name['crm_name'] .' - Completed Task Report');

    // Add some data to the second sheet, resembling some different data types
    $objPHPExcel->setActiveSheetIndex(2)
          ->setCellValue('A3', 'S.No')  
          ->setCellValue('B3', 'Service Name')
          ->setCellValue('C3', 'Task Start Date')
          ->setCellValue('D3', 'Task End Date')
          ->setCellValue('E3', 'Task Status');


    $j=4;
    $s=1;
    foreach ($ser_task_done as $s_value) {
      if($s_value['worker']=='')
      {
      $s_value['worker'] = 0;
      }
      if($s_value['task_status']=='')
      {
        $s_value['task_status'] = 'Not started';
      }
      $explode_worker=explode(',',$s_value['worker']);
     if(in_array( $id ,$explode_worker )){
        $objPHPExcel->setActiveSheetIndex(2)
          ->setCellValue('A'.$j, $s)
          ->setCellValue('B'.$j, $s_value['subject'])  
          ->setCellValue('C'.$j, $s_value['start_date'])
          ->setCellValue('D'.$j, $s_value['end_date'])
          ->setCellValue('E'.$j, $s_value['task_status']);
      $j++;
      $s++;
    }}
      
    // Rename 3rd sheet
    $objPHPExcel->getActiveSheet()->setTitle('service Based Task Done Records');
       // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        
        // It will be called file.xls
        header('Content-Disposition: attachment; filename="'.$fileName.'"');
        
        header('Cache-Control: max-age=0');
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');

}


public function staff_custom_form()
{
  $this->Security_model->chk_login();
  $user_id=$this->session->userdata['userId'];
  $result['data']=$this->Common_mdl->select_record('staff_custom_form','user_id',$user_id);
  $this->load->view('staffs/staff_custom_form',$result);
}

public function add_custom_form(){
  $data['user_id']=$_POST['staff_id'];
  $data['labels']=implode(',',$_POST['label']);
  $data['field_type']=implode(',',$_POST['field_type']);
  $field_name=explode(',',$data['labels']);
  for($i=0;$i<count($field_name);$i++){
    $sub_strlower = strtolower(preg_replace("~[\\\\/:*?'&,<>|]~", '', $field_name[$i]));
    $sub_lesg = str_replace(' ', '-', $sub_strlower); 
    $sub_lesgs[]= rtrim($sub_lesg, "-");
  }
  $data['field_name'] = implode(',',$sub_lesgs);
  $data['status']='active';
  $check_db=$this->db->query('select * from staff_custom_form where user_id='.$_POST['staff_id'].' ')->row_array();
    if(count($check_db)>0)
    {
      $this->Common_mdl->update('staff_custom_form',$data,'id',$check_db['id']);
    }
    else
    {
      $insert=$this->Common_mdl->insert('staff_custom_form',$data);
    }
//  $insert=$this->Common_mdl->insert('staff_custom_form',$data);
 //if($insert){
    $this->session->set_flashdata('success', 'Record Added Successfully!');
    redirect('staff/staff_custom_form', 'refresh');
 // }
}

public function update_custom_form(){
  $user_id=$_POST['staff_id'];
  $data['labels']=rtrim(implode(',',$_POST['label']),',');
  $data['field_type']=rtrim(implode(',',$_POST['field_type']),',');
  $field_name=explode(',',$data['labels']);
  for($i=0;$i<count($field_name);$i++){
    $sub_strlower = strtolower(preg_replace("~[\\\\/:*?'&,<>|]~", '', $field_name[$i]));
    $sub_lesg = str_replace(' ', '-', $sub_strlower); 
    $sub_lesgs[]= rtrim($sub_lesg, "-");
  }
  $data['field_name'] = rtrim(implode(',',$sub_lesgs),',');
  $data['status']='active';
  $insert=$this->Common_mdl->update('staff_custom_form',$data,'user_id',$user_id);
  if($insert){
    $this->session->set_flashdata('success', 'Record Added Successfully!');
    redirect('staff/staff_custom_form', 'refresh');
  }
}

public function update_custom(){
  $values=$_POST;
  array_pop($values);
  $user_id=$this->session->userdata['userId'];
  $data['values']=implode(',',$values);
  $insert=$this->Common_mdl->update('staff_custom_form',$data,'user_id',$user_id);
  if($insert){
    $this->session->set_flashdata('success', 'Record Added Successfully!');
    redirect('staff/Dashboard', 'refresh');
  }
}

public function unset_value(){
  
 $user_id=$_POST['user_id'];
 $position=$_POST['id'];
   $result=$this->Common_mdl->select_record('staff_custom_form','user_id',$user_id);
      $labels=explode(',',$result['labels']);
      $field_type=explode(',',$result['field_type']);   
      $field_name=explode(',',$result['field_name']);
      $values=explode(',',$result['values']);  
      unset($labels[$position]);
      unset($field_type[$position]);
      unset($field_name[$position]);
      unset($values[$position]);
      $data['labels']=implode(',',$labels);
      $data['field_type']=implode(',',$field_type);
      $data['field_name']=implode(',',$field_name);
      $data['values']=implode(',',$values);
      $insert=$this->Common_mdl->update('staff_custom_form',$data,'user_id',$user_id);
      if($insert){
        $data['status']='1';
        echo json_encode($data);        
      }          
}

  public function Firstname_Check()
  {
    $fname = $_POST['fname'];
    if(isset($fname))
    {
      if(isset($_POST['user_id']) && $_POST['user_id']!=''){
      $staff_list=$this->db->query("select * from user where username='".$fname."' and id!=".$_POST['user_id']." ")->result_array();
      }
      else{ 
       $staff_list=$this->db->query("select * from user where username='".$fname."'")->result_array();
      }

      // print_r($staff_list['first_name']) ;
      if($staff_list)
      { 
        echo "1";
      } else {
        echo "0";
      }
    }

  }

     public function save_sign(){
      //$p_id=$_SESSION['proposals'];
      //echo $p_id;
      $p_id=0;
      $results = array();
      $result = array();
      $json_data=$_POST['json_data'];
      $imagedata = base64_decode($_POST['img_data']);
      $filename = md5(date("dmYhisA"));
      //Location to where you want to created sign image
      $file_name = 'uploads/doc_signs/'.$filename.'.png';
      file_put_contents($file_name,$imagedata);
      $results['status'] = 1;
      $results['file_name'] = $file_name;
      $user_id=$this->session->userdata['userId'];  
      $data=array('user_id'=>$user_id,'signature_path'=>$file_name,'status'=>'active','proposal_no'=>$p_id,'signature_json_data'=>$json_data);      
      $insert=$this->Common_mdl->insert('pdf_signatures',$data);
     // echo $insert."//".$file_name;
      $result['insert_data']=$insert;
      $result['file_name']=$file_name;
      echo json_encode($result);        
    }

    /** 20-08-2018 **/
    public function staff_profile_old(){

      $this->load->view('staffs/staff-profile-view');

    }
   public function staff_profile($id)
    {
  $this->Security_model->chk_login();
     //  $id = $this->session->userdata('admin_id');
       $data['user_details']=$this->Common_mdl->select_record('user','id',$id);
       //$data['create_clients_list']=$this->Common_mdl->GetAllWithWhere('user','firm_admin_id',$id);
       //$data['create_clients_list']=$this->db->query("select * from user where firm_admin_id='".$id."' and role = '6' ")->result_array();
       $data['create_clients_list']=$this->db->query("select * from user where firm_admin_id='".$id."' and crm_name != '' ")->result_array();
       if(isset($_POST['date']))
       {
        $month = '';
        $year = '';
       }else{
        $month = date('m');
        $year = date('Y');
       }
       


       $data['start_end_date'] = $this->start_end_date('01-'.$month.'-'.$year);

       $exp_date = explode(',', $data['start_end_date']);

       $start = $exp_date[0];
       $end = $exp_date[1];

       $data['start_month'] = $this->month_cnt($start);
       $data['end_month'] = $this->month_cnt($end);

       $data['total_count_month'] = $this->weeks($month,$year);

               
      $assigned_task=$this->Common_mdl->getallrecords('add_new_task');
    //  $user_id = $this->session->userdata('id');
      $user_id=$id;
      $high = 0;
      $low = 0;
      $medium = 0;
      $super_urgent = 0;
      $tot_tim[] = '';
      $days_count=0;
      foreach ($assigned_task as $task_key => $task_value) {

        $workers = $task_value['worker'];
        $ex_worker = explode(',', $workers);
        if(in_array($user_id, $ex_worker))
        {
		$task_startdate=$task_value['start_date'];
		$task_enddate=$task_value['end_date'];
		$date1=date_create($task_startdate);
		$date2=date_create($task_enddate);
		$diff=date_diff($date1,$date2);
		$diffence=$diff->format("%a");
		$days_count=$days_count+$diffence;// for difference between start&end date
          $tot_tim[] = $task_value['timer_status'];
          $task_status = $task_value['priority'];
          if($task_status=='high'){
            $high++;
          }
          else if($task_status=='low'){
            $low++;
          }
          else if($task_status=='medium'){
            $medium++;
          }
          else if($task_status=='super_urgent')
          {
            $super_urgent++;
          }
        }
      }

  /** for 09-08-2018 task timer **/
  $individual_timer=$this->db->query("select * from individual_task_timer where user_id=".$id." ")->result_array();
$pause_val='on';
$pause='on';
$for_total_time=0;
if(count($individual_timer)>0){
foreach ($individual_timer as $intime_key => $intime_value) {
	 $task_id=$intime_value['task_id'];
  $task_data=$this->db->query("select * from add_new_task where id=".$task_id." ")->row_array();
  if(!empty($task_data)){
  $its_time=$intime_value['time_start_pause'];
  $res=explode(',', $its_time);
    $res1=array_chunk($res,2);
  $result_value=array();
//  $pause='on';
  foreach($res1 as $rre_key => $rre_value)
  {
     $abc=$rre_value;
     if(count($abc)>1){
     if($abc[1]!='')
     {
        $ret_val=$this->calculate_test($abc[0],$abc[1]);
        array_push($result_value, $ret_val) ;
     }
     else
     {
      $pause='';
      $pause_val='';
        $ret_val=$this->calculate_test($abc[0],time());
         array_push($result_value, $ret_val) ;
     }
    }
    else
    {
      $pause='';
      $pause_val='';
        $ret_val=$this->calculate_test($abc[0],time());
         array_push($result_value, $ret_val) ;
    }
  }
  // $time_tot=0;
   foreach ($result_value as $re_key => $re_value) {
      //$time_tot+=time_to_sec($re_value) ;
      $for_total_time+=$this->time_to_sec($re_value) ;
   }

} //if
} //for
} // if

    //  $tot_time = array_sum($tot_tim);
      $tot_time=$for_total_time;
      $task_cnt = count(array_filter($tot_tim));
      $data['high'] = $high;
      $data['low'] = $low;
      $data['medium'] = $medium;
      $data['super_urgent'] = $super_urgent;
      $data['tot_tim'] = $tot_time;
      $data['task_cnt'] = $task_cnt;
      $data['days_count']=$days_count;// days count between task start and end date // 23-08-2018
      //echo $days_count."count count";
//echo $task_cnt."cnt count";
       //$data['user_activity']=$this->Common_mdl->GetAllWithWhere('activity_log','user_id',$id);
       $data['user_activity']=$this->Common_mdl->orderby('activity_log','user_id',$id,'desc');
      
//echo $this->db->last_query();die;
      /* for($i=1; $i<=$data['total_count_month'];$i++)
       {
        echo $i;
       }die;
*/
        $data['task_list']=$this->Common_mdl->getallrecords('add_new_task');
        //$data['getCompany']=$this->db->query('select * from user where firm_admin_id = '.$id.' and role = 4')->result_array();
        $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = ".$id." and role = 4 and crm_name !=''")->row_array();

        $us_id =  explode(',', $sql['us_id']);





        $data['getCompany'] = $this->db->query( "SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !=''")->result_array();
        /*  echo '<pre>';
          print_r($data['getCompany']);die;*/
        $con_stat = 'on';
        $acc_stat = 'on';
        $c_tax_stat = 'on';
        $p_tax_stat = 'on';
        $payroll_stat = 'on';
        $workplace_stat = 'on';
        $vat_stat = 'on';
        $cis_stat = 'on';
        $cissub_stat = 'on';
        $p11d_stat = 'on';
        $bookkeep_stat = 'on';
        $management_stat = 'on';
        $investgate_stat = 'on';
        $registered_stat = 'on';
        $taxadvice_stat = 'on';
        $taxinvest_stat = 'on';

          $ids = array();
        foreach ($data['getCompany'] as $getCompanykey => $getCompanyvalue) {
          $con = (isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnCst =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnCst = '';

          $acc = (isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnAcc =  json_decode($getCompanyvalue['accounts'])->tab : $jsnAcc = '';

          $c_tax = (isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsntax =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsntax = '';

          $p_tax = (isset(json_decode($getCompanyvalue['personal_tax_return'])->tab) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnp_tax =  json_decode($getCompanyvalue['personal_tax_return'])->tab : $jsnp_tax = '';

          $payroll = (isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpay =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpay = '';

          $workplace = (isset(json_decode($getCompanyvalue['workplace'])->tab) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->tab : $jsnworkplace = '';

          $vat = (isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = '';

          $cis = (isset(json_decode($getCompanyvalue['cis'])->tab) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->tab : $jsncis = '';

          $cissub = (isset(json_decode($getCompanyvalue['cissub'])->tab) && $getCompanyvalue['cissub'] != '') ? $jsncis_sub =  json_decode($getCompanyvalue['cissub'])->tab : $jsncis_sub = '';

          $p11d = (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = '';

          $bookkeep = (isset(json_decode($getCompanyvalue['bookkeep'])->tab) && $getCompanyvalue['bookkeep'] != '') ? $jsnbk =  json_decode($getCompanyvalue['bookkeep'])->tab : $jsnbk = '';

          $management = (isset(json_decode($getCompanyvalue['management'])->tab) && $getCompanyvalue['management'] != '') ? $jsnmgnt =  json_decode($getCompanyvalue['management'])->tab : $jsnmgnt = '';

          $investgate = (isset(json_decode($getCompanyvalue['investgate'])->tab) && $getCompanyvalue['investgate'] != '') ? $jsninvest =  json_decode($getCompanyvalue['investgate'])->tab : $jsninvest = '';

          $registered = (isset(json_decode($getCompanyvalue['registered'])->tab) && $getCompanyvalue['registered'] != '') ? $jsnreg =  json_decode($getCompanyvalue['registered'])->tab : $jsnreg = '';

          $taxadvice =(isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxad =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxad = '';

          $taxinvest = (isset(json_decode($getCompanyvalue['taxinvest'])->tab) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->tab : $jsntaxinvest = '';


          if($con==$con_stat && $acc==$acc_stat && $c_tax==$c_tax_stat && $p_tax==$p_tax_stat && $payroll==$payroll_stat && $workplace==$workplace_stat && $vat==$vat_stat && $cis==$cis_stat && $cissub==$cissub_stat && $p11d==$p11d_stat && $bookkeep==$bookkeep_stat && $management==$management_stat && $investgate==$investgate_stat && $registered==$registered_stat && $taxadvice==$taxadvice_stat && $taxinvest==$taxinvest_stat)
          {
            $ids[] = $getCompanyvalue['id'];
          }
        }

             $data['getCompany'] = $this->db->query( "SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' and id in (  '" . implode( "','", $ids ) . "' )")->result_array();


  $data['getCompanyss'] = $this->db->query( "SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !=''")->result_array();

   $data['getCompany'] = $this->db->query( "SELECT * FROM client WHERE user_id IN ( '" . implode( "','", $us_id ) . "' ) and crm_company_name !='' ")->result_array();
  //$user_id=$this->session->userdata['userId'];
   $user_id=$id;
  $data['custom_form']=$this->Common_mdl->select_record('staff_custom_form','user_id',$user_id);


       //$us_id = $this->session->userdata('admin_id');
  $us_id=$id;
      // echo $id;
        // $user_id = $this->session->userdata('id');
        // $sql = $this->db->query("SELECT GROUP_CONCAT(  `id` SEPARATOR  ',' ) AS  `us_id`  FROM user where firm_admin_id = ".$id." and role = 4 and crm_name !=''")->row_array();
        //  echo $this->db->last_query();
        // $us_id =  explode(',', $sql['us_id']);
        $current_date=date("Y-m-d");
        $time=date("Y-m-d");
        $data['today'] = $this->db->query( "SELECT * FROM client WHERE user_id ='".$us_id."' and crm_company_name !='' and ((crm_confirmation_statement_due_date= '".$current_date."') or (crm_ch_accounts_next_due = '".$current_date."') or (crm_accounts_tax_date_hmrc = '".$current_date."' ) or (crm_personal_due_date_return = '".$current_date."' ) or (crm_vat_due_date = '".$current_date."' ) or (crm_rti_deadline = '".$current_date."')  or (crm_pension_subm_due_date = '".$current_date."') or (crm_next_p11d_return_due = '".$current_date."')  or (crm_next_manage_acc_date = '".$current_date."') or (crm_next_booking_date = '".$current_date."' ) or (crm_insurance_renew_date = '".$current_date."')  or (crm_registered_renew_date = '".$current_date."' ) or (crm_investigation_end_date = '".$current_date."' )) ")->result_array();

      //  echo $this->db->last_query();
        $oneweek_end_date = date('Y-m-d', strtotime($time . ' +1 week'));
          //echo $end_date;
        $data['oneweek'] = $this->db->query( "SELECT * FROM client WHERE user_id ='".$us_id."' and crm_company_name !='' and ((crm_confirmation_statement_due_date BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_ch_accounts_next_due BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_personal_due_date_return BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_vat_due_date BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_rti_deadline BETWEEN '".$current_date."' AND '".$oneweek_end_date."' )  or (crm_pension_subm_due_date BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_next_p11d_return_due BETWEEN '".$current_date."' AND '".$oneweek_end_date."' )  or (crm_next_manage_acc_date BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_next_booking_date BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_insurance_renew_date BETWEEN '".$current_date."' AND '".$oneweek_end_date."' )  or (crm_registered_renew_date BETWEEN '".$current_date."' AND '".$oneweek_end_date."' ) or (crm_investigation_end_date BETWEEN '".$current_date."' AND '".$oneweek_end_date."' )) ")->result_array();
        $onemonth_end_date = date('Y-m-d', strtotime($time . ' +1 month'));
        $data['onemonth'] = $this->db->query( "SELECT * FROM client WHERE user_id ='".$us_id."' and crm_company_name !='' and ((crm_confirmation_statement_due_date BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_ch_accounts_next_due BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_personal_due_date_return BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_vat_due_date BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_rti_deadline BETWEEN '".$current_date."' AND '".$onemonth_end_date."' )  or (crm_pension_subm_due_date BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_next_p11d_return_due BETWEEN '".$current_date."' AND '".$onemonth_end_date."' )  or (crm_next_manage_acc_date BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_next_booking_date BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_insurance_renew_date BETWEEN '".$current_date."' AND '".$onemonth_end_date."' )  or (crm_registered_renew_date BETWEEN '".$current_date."' AND '".$onemonth_end_date."' ) or (crm_investigation_end_date BETWEEN '".$current_date."' AND '".$onemonth_end_date."' )) ")->result_array();
        $oneyear_end_date = date('Y-m-d', strtotime($time . ' +1 year'));
        $data['oneyear'] = $this->db->query( "SELECT * FROM client WHERE user_id ='".$us_id."' and crm_company_name !='' and ((crm_confirmation_statement_due_date BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_ch_accounts_next_due BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_accounts_tax_date_hmrc BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_personal_due_date_return BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_vat_due_date BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_rti_deadline BETWEEN '".$current_date."' AND '".$oneyear_end_date."' )  or (crm_pension_subm_due_date BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_next_p11d_return_due BETWEEN '".$current_date."' AND '".$oneyear_end_date."' )  or (crm_next_manage_acc_date BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_next_booking_date BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_insurance_renew_date BETWEEN '".$current_date."' AND '".$oneyear_end_date."' )  or (crm_registered_renew_date BETWEEN '".$current_date."' AND '".$oneyear_end_date."' ) or (crm_investigation_end_date BETWEEN '".$current_date."' AND '".$oneyear_end_date."' )) ")->result_array();
        $last_month_start = date("Y-m-01");
        $last_mon_end = date("Y-m-t");  

         $data['confirmation'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_confirmation_statement_due_date between "'.$last_month_start.'" AND "'.$last_mon_end.'"))')->row_array();
        $data['account'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_ch_accounts_next_due between "'.$last_month_start.'" AND "'.$last_mon_end.'"))')->row_array();
        $data['company_tax'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_accounts_tax_date_hmrc between "'.$last_month_start.'" AND "'.$last_mon_end.'"))')->row_array();
        $data['personal_due'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id ='.$us_id.' and crm_company_name !="" and ((crm_personal_due_date_return between "'.$last_month_start.'" AND "'.$last_mon_end.'"))')->row_array();
        $data['vat'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_vat_due_date between "'.$last_month_start.'" AND "'.$last_mon_end.'"))')->row_array();
        $data['payroll'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_rti_deadline between "'.$last_month_start.'" AND "'.$last_mon_end.'"))')->row_array();
        $data['pension'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_pension_subm_due_date between "'.$last_month_start.'" AND "'.$last_mon_end.'"))')->row_array();
        $data['plld'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_next_p11d_return_due between "'.$last_month_start.'" AND "'.$last_mon_end.'"))')->row_array();
        $data['management'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_next_manage_acc_date between "'.$last_month_start.'" AND "'.$last_mon_end.'"))')->row_array();
        $data['booking'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_next_booking_date between "'.$last_month_start.'" AND "'.$last_mon_end.'"))')->row_array();
        $data['insurance'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_insurance_renew_date between "'.$last_month_start.'" AND "'.$last_mon_end.'"))')->row_array();
        $data['registeration'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_registered_renew_date between "'.$last_month_start.'" AND "'.$last_mon_end.'"))')->row_array();
        $data['investigation'] = $this->db->query('SELECT count(*) as deadline_count FROM client WHERE user_id  ='.$us_id.' and crm_company_name !="" and ((crm_investigation_end_date between "'.$last_month_start.'" AND "'.$last_mon_end.'"))')->row_array(); 
        $data['todo_list']=$this->db->query("select * from `todos_list` where status='active' and user_id='".$us_id."' order by order_number Asc")->result_array();
        $data['todo_completedlist']=$this->db->query("select * from `todos_list` where status='completed'  and user_id='".$us_id."' order by order_number Asc")->result_array();
        $data['todolist']=$this->db->query("select * from `todos_list` where user_id='".$us_id."' order by order_number Asc")->result_array();
        $last_month_start = date("Y-m-d");
        $last_month_end = date("Y-m-t"); 

        $data['task_list']=$this->db->query("SELECT * FROM `add_new_task` WHERE `end_date` between '".$last_month_start."' and  '".$last_month_end."' and priority='high' and user_id='".$us_id."'  order by id desc Limit 0,5")->result_array();
        /** 23-08-2018 **/
        $data['task_list']=$this->db->query("SELECT * FROM `add_new_task` where worker in (".$us_id.") ")->result_array();
        /** end of 23-08-2018 **/
        $last_month_start = date("Y-m-01");
        $last_mon_end = date("Y-m-t");  
          $data['accept_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="accepted" and user_id="'.$us_id.'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"')->row_array();       
        $data['indiscussion_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="in discussion" and user_id="'.$us_id.'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"')->row_array();
        $data['sent_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="sent" and user_id="'.$us_id.'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
        $data['viewed_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="opened" and user_id="'.$us_id.'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
        $data['declined_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="declined" and user_id="'.$us_id.'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
        $data['archive_proposal']=$this->db->query('select count(*) as proposal_count from proposals where status="archive" and user_id="'.$us_id.'" and created_at BETWEEN "'.$last_month_start.'" AND "'.$last_mon_end.'"' )->row_array();
       $last_Year_start = date("Y-01-01");
          $last_month_end = date("Y-12-31");
        $data['Private']=$this->db->query('select count(*) as limit_count from client where crm_legal_form="Private Limited company" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"')->row_array();         
        $data['Public']=$this->db->query('select count(*) as limit_count from client where crm_legal_form="Public Limited company" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"' )->row_array();
        $data['limited']=$this->db->query('select count(*) as limit_count from client where crm_legal_form="Limited Liability Partnership" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"' )->row_array();
        $data['Partnership']=$this->db->query('select count(*) as limit_count from client where crm_legal_form="Partnership" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"' )->row_array();
        $data['self']=$this->db->query('select count(*) as limit_count from client where crm_legal_form="Self Assessment" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"' )->row_array();
        $data['Trust']=$this->db->query('select count(*) as limit_count from client where crm_legal_form="Trust" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"')->row_array();
        $data['Charity']=$this->db->query('select count(*) as limit_count from client where crm_legal_form="Charity" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"')->row_array();
        $data['Other']=$this->db->query('select count(*) as limit_count from client where crm_legal_form="Other" and from_unixtime(created_date) BETWEEN "'.$last_Year_start.'" AND "'.$last_month_end.'"' )->row_array();

         $data['active_client']=$this->db->query("SELECT count(*) as client_count FROM user where firm_admin_id='".$us_id."' AND autosave_status!='1' and crm_name!='' and status='1' order by id DESC")->row_array();
        $data['inactive_client']=$this->db->query("SELECT count(*) as client_count FROM user where firm_admin_id='".$us_id."' AND autosave_status!='1' and crm_name!='' and status='2' order by id DESC")->row_array();
        $data['completed_task']=$this->db->query("SELECT count(*) as task_count FROM add_new_task
        where user_id='".$us_id."' and task_status='complete'")->row_array();
        $data['inprogress_task']=$this->db->query("SELECT count(*) as task_count FROM add_new_task
        where user_id='".$us_id."' and task_status='inprogress'")->row_array();

        $data['staff_list']=$this->Common_mdl->GetAllWithWhere('staff_form','status','1');

        $data['staff_form'] = $this->Common_mdl->GetAllWithWheretwo('user','role','6','firm_admin_id',$_SESSION['id']);
        $data['team'] = $this->db->query("select * from team where create_by=".$_SESSION['id']." ")->result_array();
        $data['department'] = $this->db->query("select * from department_permission where create_by=".$_SESSION['id']." ")->result_array();
                

        $data['its_id']=$id;

       $this->load->view('staffs/staff_profile_view',$data); 
    }
    public function user_profile(){
          $this->Security_model->chk_login();

    /*      $user_id=$_SESSION['id'];

          $data['staff']            = $this->db->query('select * from staff_form where user_id='.$user_id)->row_array();
          $GetAdministration        = $this->db->query("select * from roles_section where firm_id = ".$_SESSION['firm_id']." and  parent_id=0")->row_array();
          $data['roles_tag']        = GetAssignees_SelectBox_Content( 0, 0 ,$GetAdministration['id']);
          $data['countries']        = $this->db->query("SELECT * FROM `countries` order by name asc")->result_array();
          $data['is_profile_page']  = 1;
          $this->load->view('staffs/add_staff_view',$data);*/

        $user_id=$_SESSION['id'];
        
        $data['staff'] = $this->Common_mdl->select_record('staff_form','user_id' , $user_id);

        $data['role']  = $this->Common_mdl->select_record('roles_section','id',$data['staff']['roles']);

        $this->load->view('staffs/profile-page', $data );

      /*if($_SESSION['user_type']=='FU'){
          }
          else
          {
            redirect('user');
          }*/
       //  $data['staff'] = $this->Common_mdl->GetAllWithWhere('staff_form','user_id',$user_id);

       // // $data['roles']=$this->Common_mdl->getallrecords('Role');
       //  $data['roles']=$this->db->query('select * from Role where id not in(1,2,4)')->result_array(); // 1->admin 2->sub admin 4->client
       //  $data['custom_form']=$this->Common_mdl->select_record('staff_custom_form','user_id',$user_id);
      
      
      //  }
      //  else if($_SESSION['role']==1)
      //  {
      //  	$user_id=$_SESSION['id'];
      //   $data['user']=$this->Common_mdl->GetAllWithWhere('user','id',$user_id);
     	// $this->load->view('staffs/user_profile',$data);
      //  }
      //   else if($_SESSION['role']==4){
      //   $user_id=$_SESSION['id'];
      //   $data['user']=$this->Common_mdl->GetAllWithWhere('user','id',$user_id);
      //   $data['client']=$this->Common_mdl->GetAllWithWhere('client','user_id',$user_id);
      //   $this->load->view('staffs/client_profile',$data);
      //  }

    }
    public function client_profile(){
      /* for getting data **/
      if($_SESSION['role']==4){
        $user_id=$_SESSION['id'];
        $data['user']=$this->Common_mdl->GetAllWithWhere('user','id',$user_id);
        $data['client']=$this->Common_mdl->GetAllWithWhere('client','user_id',$user_id);
        $this->load->view('staffs/client_profile',$data);
       }

    }
    /** end of 20-08-2018 **/

    public function user_change_password(){
      $user_id=$_POST['user_id'];
      $staff_id=$_POST['staff_id'];
      $new_password=$_POST['new_password'];
      
      $convert_data['password']=md5($new_password);
      $convert_data['confirm_password']=$new_password;
      $this->Common_mdl->update('user',$convert_data,'id',$user_id);
      $this->Common_mdl->update('staff_form',$convert_data,'id',$staff_id);
    //  $this->session->set_flashdata('success', 'Password Change Successfully!');
      echo 'success';
    }
     public function client_change_password(){
      $user_id=$_POST['user_id'];

      $client_id=(isset($_POST['client_id']))?$_POST['client_id']:'';
      $new_password=$_POST['new_password'];
      
      $convert_data['password']=md5($new_password);
      $convert_data['confirm_password']=$new_password;
      if($client_id!=''){
      	$client_convert_data['crm_other_password']=$new_password;
      	$this->Common_mdl->update('client',$client_convert_data,'id',$client_id);
      }
      
      $this->Common_mdl->update('user',$convert_data,'id',$user_id);
      
    //  $this->session->set_flashdata('success', 'Password Change Successfully!');
      echo 'success';
    }


}
?>
