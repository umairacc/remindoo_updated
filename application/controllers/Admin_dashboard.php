<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_dashboard extends CI_Controller 
{
    public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
  
    public function __construct()
    {
        parent::__construct();
        //@session_start();
        $this->load->model(array('Common_mdl','Security_model','Invoice_model','Proposal_model','Report_model','Loginchk_model'));
        $this->load->helper(['comman']);
    }

    public function array_icount_values($arr) { 
     $arr2=array(); 
     if(!is_array($arr['0'])){$arr=array($arr);} 
     foreach($arr as $k=> $v){ 
      foreach($v as $v2){ 
     
              if(!isset($arr2[$v2])){ 
                  $arr2[$v2]=1; 
              }else{ 
                   $arr2[$v2]++; 
                   } 
            } 
            } 
            return $arr2; 
        } 

    public function index()
    {
        $this->Loginchk_model->superAdmin_LoginCheck();
     //   echo "check";
        $this->adminDashboard();  
    }

    public function adminDashboard()
    {
             
            $last_month_start = date("Y-m-01");
            $last_month_end = date("Y-m-t"); 
            $last_month_end = date('Y-m-d', strtotime($last_month_end . ' +1 day'));   
                    
            $Assigned_Client = Get_Assigned_Datas('CLIENT'); 
            $assigned_client = implode(',',$Assigned_Client);

            $Assigned_Task = Get_Assigned_Datas('TASK');
            $assigned_task = implode(',',$Assigned_Task);

            $Assigned_Leads = Get_Assigned_Datas('LEADS');
            $assigned_leads = implode(',',$Assigned_Leads);
             $data['count_details']=array();
             $service_filter1=$service_filter2=$service_filter2=$final_result=array();
             $service_filter2==array();

            $data['firm_details']=$this->db->query('SELECT * from firm ')->result_array();
            $data['service_filter']=$this->db->query('SELECT * from service_lists ')->result_array();

            $service_filter_column=array_column($data['service_filter'],'id');

            if(count($data['firm_details']) > 0){
            $data['count_details']=array_count_values(array_column($data['firm_details'],'status'));
            foreach ( $data['firm_details'] as $key => $value) {
                if($value['services']!='')
                {
              $service_filter1=json_decode($value['services'],true);
              $service_filter2[]=array_keys(array_diff($service_filter1,array(0)));

                }
              }
                $data['res'] = $this->array_icount_values ($service_filter2); 


            }


      $this->load->view('super_admin/admin_desk_dashboard',$data); 
    }
    public function Active_firm(){
     
      $this->session->set_flashdata('firm_seen','active');
      redirect('firm');
    }
     public function All_firm(){

      $this->session->set_flashdata('firm_seen','allfirm');
      redirect('firm');
    }
    public function Inactive_firm(){

      $this->session->set_flashdata('firm_seen','inactive');
      redirect('firm');
    }
      public function Archive_firm(){
      
      $this->session->set_flashdata('firm_seen','archive');
      redirect('firm');
    }
    public function Frozen_firm(){
     
      $this->session->set_flashdata('firm_seen','frozen');
      redirect('firm');
    }
      public function Active_services($name){
   
     $this->session->set_flashdata('firm_service',strtolower($name));
      redirect('firm');
    }

    //     public function firm_filter_year()
    // {
             

    //     $data['Private']=$this->Common_mdl->ClientDetailsByLegalForm('Private Limited company',$last_Year_start,$last_month_end); 
    //     $data['Public']=$this->Common_mdl->ClientDetailsByLegalForm('Public Limited company',$last_Year_start,$last_month_end);
    //     $data['limited']=$this->Common_mdl->ClientDetailsByLegalForm('Limited Liability Partnership',$last_Year_start,$last_month_end);
    //     $data['Partnership']=$this->Common_mdl->ClientDetailsByLegalForm('Partnership',$last_Year_start,$last_month_end);
    //     $data['self']=$this->Common_mdl->ClientDetailsByLegalForm('Self Assessment',$last_Year_start,$last_month_end);
    //     $data['Trust']=$this->Common_mdl->ClientDetailsByLegalForm('Trust',$last_Year_start,$last_month_end);
    //     $data['Charity']=$this->Common_mdl->ClientDetailsByLegalForm('Charity',$last_Year_start,$last_month_end);
    //     $data['Other']=$this->Common_mdl->ClientDetailsByLegalForm('Other',$last_Year_start,$last_month_end);
        
    //     $this->load->view('firm_dashboard/year_chart_clients',$data);        
    // }
    public function firm_filter()
    {  
         if($_POST['id']=='month')  {
        $last_start = date("Y-m-01");
        $last_end = date("Y-m-t"); 
        $last_end = date('Y-m-d', strtotime($last_end . ' +1 day'));

         }   
       if($_POST['id']=='year')
        {
        $last_start = date("Y-01-01");
        $last_end = date("Y-12-31");   
        }


        $data['count_details']=array();

            if($_POST['id']=='month' || $_POST['id']=='year')
            {
                     $last_start = strtotime($last_start);
                     $last_end = strtotime($last_end);

                 $data['firm_details']=$this->db->query('SELECT * from firm where created_date >='.$last_start.' and created_date <='.$last_end.' ')->result_array();
            }
            else
            {
                $data['firm_details']=$this->db->query('SELECT * from firm')->result_array();
            }

           
            if(count($data['firm_details']) > 0){
            $data['count_details']=array_count_values(array_column($data['firm_details'],'status'));
            }

        $this->load->view('super_admin/chart_success',$data);       
    }


  }

?>