<?php
//error_reporting(E_STRICT);
defined('BASEPATH') OR exit('No direct script access allowed');

class Overview extends CI_Controller 
{
	   public function __construct()
	   {
	       parent::__construct();
	       @session_start();
	       $this->load->model(array('Common_mdl','Security_model','Invoice_model'));
	   }

	   public function index()
	   {
	   	   $this->Security_model->check_login();
        
	   	   $data['user_details'] = $this->Common_mdl->select_record('user','id',$_SESSION['client_uid']);
	   	   $data['proposals'] = $this->Common_mdl->GetAllWithWheretwo('proposals','company_id',$_SESSION['client_uid'],'client_view','0'); 
	   	   $data['service_list'] = $this->Common_mdl->getClientServices($_SESSION['client_uid']);
	  	   $current_date = date('Y-m-d');
	   	   $oneyear_end_date = date('Y-m-d', strtotime($current_date . ' +1 year'));
	   	   $service_cond = $this->Common_mdl->getServicedue($current_date,$oneyear_end_date);
	   	   $data['oneyear'] = $this->db->query('SELECT * FROM client WHERE user_id = "'.$_SESSION['client_uid'].'" '.$service_cond.'')->row_array();
	   	   $data['firm_currency'] = $this->Common_mdl->get_price('admin_setting','firm_id',$_SESSION['firm_id'],'crm_currency');
	   	   $data['reminder_details'] = $this->Invoice_model->selectAllRecord('client_reminder','client_id',$_SESSION['id']);

	   	   $this->load->view('Overview/index',$data);
	   }

	   public function deleteProposal()
	   {
	       if(isset($_POST['data_id']))
	       { 
	           $id = str_replace('[', '', $_POST['data_id']);
	           $id = trim(str_replace(']', '', $id)); 
	           
	           $sql = $this->db->query("UPDATE proposals SET client_view = '1' WHERE id in ($id)");
	           echo $sql;
	       }   
	   }
	   
	   public function Invoices()
	   {
	   	  $data['invoice'] = $this->Invoice_model->selectInvoiceByType('','','','client_email',$_SESSION['client_uid']);			
          $data['repeatinvoice'] = $this->Invoice_model->getRepeatInvoice('client_email',$_SESSION['client_uid']);
          $data['firm_currency'] = $this->Common_mdl->get_price('admin_setting','firm_id',$_SESSION['firm_id'],'crm_currency');

	   	  $this->load->view('Overview/invoice',$data);
	   } 	  

}	

?>