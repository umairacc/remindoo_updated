<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Task_Status extends CI_Controller {
public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";

  public function __construct(){
        parent::__construct();
        $this->load->model(array('Common_mdl','Security_model','Invoice_model','Task_invoice_model','Task_creating_model','Task_section_model','Loginchk_model'));
        $this->load->helper('download');
    }

 public function task_progress_view()
{
     $this->Security_model->chk_login();

       $datas=$this->Common_mdl->get_firm_progress();

    $data['progress_task_status']=$datas;

 if($_SESSION['user_type']=='FA'){
  $this->load->view('users/task_status_view',$data);
  }
  else
  {
    redirect('user');
  }
}
      public function insert_task_status()
      {
          if(isset($_COOKIE['remindoo_logout']) && $_COOKIE['remindoo_logout']=='remindoo_superadmin') 
              {
                 $this->Loginchk_model->superAdmin_LoginCheck();

                 $maxid = $this->db->query('SELECT MAX(order_by) AS `maxid` FROM `progress_status` where firm_id=0')->row()->maxid;
                 $data['order_by']= $maxid+1;
              
              }
              else
              {
                  $data['firm_id']=$_SESSION["firm_id"];
                  $align_array=$this->Common_mdl->select_record('admin_setting','firm_id',$_SESSION['firm_id']);
                  $align_array=json_decode($align_array['progress_order'],true);

                 $this->Security_model->chk_login();
              }
        // $this->Common_mdl->document_function();
        $data['status_name']=$_POST['status_name'];

        $data['created_date']=time();
        
        $this->db->insert('progress_status',$data);

        if(!isset($data['order_by']))
        {
          array_push($align_array,$this->db->insert_id());
        $datas['progress_order']=json_encode($align_array);
        $this->db->where('firm_id',$_SESSION['firm_id']);
         $this->db->update('admin_setting',$datas);
        }
        //$this->load->view('super_admin/task_status_view',$data);
        echo 1;

      }
    public function updated_task_status()
    {

        if(isset($_COOKIE['remindoo_logout']) && $_COOKIE['remindoo_logout']=='remindoo_superadmin') 
            {
               $this->Loginchk_model->superAdmin_LoginCheck();
            }
            else
            {
               $this->Security_model->chk_login();
            }

        if($_POST['status_id']!=''){

            $id=$_POST['status_id'];

             $data['status_name']=$_POST['status_name'];

               $sql=$this->db->query('select * from progress_status where id="'.$_POST['status_id'].'"')->row_array();

          if( $_POST['user_val']==0 && $sql['firm_id']==0)
          {
           
              $data['is_superadmin_owned']=$sql['id'];
               $data['firm_id']=$_SESSION['firm_id'];
              unset($sql['id']);
              unset($sql['order_by']);
             $this->db->insert('progress_status',$sql);
             $id=$this->db->insert_id();  

           if($_POST['del_status_id']!='' && $_POST['del_status_id']==1 ){

            $data['is_delete']=$sql['id'];
            $data['status']=$_POST['del_status_id'];
            $data['deleted_date']=time();
            $data['deleted_by']=$_SESSION['id'];
            // $this->db->where('id',$id);
            // $this->db->update('progress_status',$data);
            }
            else
            {
               $align_array=$this->Common_mdl->select_record('admin_setting','firm_id',$_SESSION['firm_id']);
               $align_array=json_decode($align_array['progress_order'],true);
               array_push($align_array,$id);
               $datas['progress_order']=json_encode($align_array);
               $this->db->where('firm_id',$_SESSION['firm_id']);
               $this->db->update('admin_setting',$datas);
            }
          }

          if($_POST['del_status_id']!='' && $_POST['del_status_id']==1 &&  $_SESSION['firm_id']==$sql['firm_id'])
          {
           
             $data['status']=$_POST['del_status_id'];
             $data['deleted_date']=time();
             $data['deleted_by']=$_SESSION['id'];

          }
          $this->db->where('id',$id);
          $this->db->update('progress_status',$data);
      }
      //$this->load->view('super_admin/task_status_view',$data);
      echo 1;
    }

    public function alltasks_delete()
    {
     if(isset($_COOKIE['remindoo_logout']) && $_COOKIE['remindoo_logout']=='remindoo_superadmin') 
            {
               $this->Loginchk_model->superAdmin_LoginCheck();
            }
            else
            {
               $this->Security_model->chk_login();
            }
              //print_r($_POST['id']);
        if(isset($_POST['id']))
          {
            $ids=explode(",",$_POST['id']);

            foreach ($ids as $key=> $id)
             { 
              $data['status']=1;

               $sql=$this->db->query('select * from progress_status where id="'.$id.'"')->row_array();

          if( $_POST['user_val']==0 &&  $sql['firm_id']==0)
          {
            
              $data['is_superadmin_owned']=$sql['id'];
              $data['is_delete']=$sql['id'];
               $data['firm_id']=$_SESSION['firm_id'];
              unset($sql['id']);
              unset($sql['order_by']);
             $this->db->insert('progress_status',$sql);
             $id=$this->db->insert_id();  
          }
             $data['deleted_date']=time();
            $data['deleted_by']=$_SESSION['id'];
            $this->db->where('id',$id);
             $this->db->update('progress_status',$data);

             }
          }
          echo 1;
    }

  public function order_update(){

    if(isset($_COOKIE['remindoo_logout']) && $_COOKIE['remindoo_logout']=='remindoo_superadmin') 
        {
           $this->Loginchk_model->superAdmin_LoginCheck();
        }
        else
        {
           $this->Security_model->chk_login();
        }
          if(!empty($_POST['idsInOrder'])){ 

           // print_r(json_encode($_POST['idsInOrder']));

            if($_POST['user_val']==1){
             // echo 'chk';
           $orderno=$_POST['idsInOrder'];
           foreach($orderno as $key=>$val){
        // $count = $key ;
      //$data['order_by']=$val;
          $this->db->set('order_by',$key+1);
         $this->db->where('id',$val);
         $this->db->update('progress_status',$data);
           }
       }
       else
       {
         // echo 'else';
        $data['progress_order']=json_encode($_POST['idsInOrder']);
        $this->db->where('firm_id',$_SESSION['firm_id']);
         $this->db->update('admin_setting',$data);
       }

     // $i++;
     } 
     echo 1;   
     //     $datas['progress_task_status']=$this->db->query('SELECT * from progress_status  where firm_id IN (0) and status="0" ORDER BY order_by ASC')->result_array();

     //  $hold=$this->load->view('users/ajax_progress_view',$datas,true);
            
     // echo json_encode(array( 'holdproduct' => $hold));
  }

}


?>
