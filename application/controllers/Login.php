<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct(){

        parent::__construct();
        $this->load->model(array('Loginchk_model','Firm_setting_mdl'));
    }

    public function index()
    { 
        $data['uri_seg'] = 'login';
        if($this->session->userdata('admin_id'))
        { 
            redirect('user');
        }
        else
        {           
            $this->load->view('login',$data);
        }
    }

    public function login_chk()
    {   
        //echo "qqqqqqqq";die;
        $data['uri_seg'] = 'login';
        $checked = $this->Loginchk_model->login_chk();
        //print_r($checked);die();
        if($checked)
        {
            $id       = $checked->id;
            $name     = $checked->crm_name;
            $username = $checked->username;
            $email_id = $checked->crm_email_id;
            $status   = $checked->status;
            $role     = $checked->role;

            $current_date   = date("Y-m-d");
            $previous       = date("Y-m-d", strtotime($current_date." -1 days"));
            $next           = date("Y-m-d",strtotime($current_date." +1 days"));

            $count = $this->db->query('SELECT count(id) as login_count FROM activity_log WHERE from_unixtime(CreatedTime) BETWEEN "'.$previous.'" AND "'.$next.'" AND user_id = "'.$id.'" AND module = "Login"')->row_array();           

            $datas['log']           = "Activate an account( Login )";
            $datas['createdTime']   = time();
            $datas['module']        = 'Login';
            $datas['user_id']       = $id;
            //$this->Common_mdl->insert('activity_log',$datas);
            //$this->login_time_update();
            //echo $this->db->last_query();die;

            
 
            if( $status != '1' )
            {
                $this->session->set_flashdata('error','Your CRM Account was Deactivated');
                $this->load->view('login',$data); 
            }
            else
            {
                
                $_SESSION['firmId']     = $checked->firm_id;/*Temprary*/
                $_SESSION['firm_id']    = $checked->firm_id;

                //echo "The firm id is " . $checked->firm_id;die();

                if( isset( $_SESSION['mail_url'] ) && $_SESSION['mail_url']!='' )
                {
                    $this->session->set_userdata('username', $username);
                    $this->session->set_userdata('crm_email_id', $email_id);
                    $this->session->set_userdata('admin_id', $id);
                    $data =  $this->db->query("SELECT * FROM user WHERE id='".$id."'")->row_array();
                    $this->session->set_userdata($data);
                    $this->session->set_flashdata('email_url_redirect', "mail redirect");
                    header( 'Location: '.$_SESSION['mail_url'] );
                    //echo "hi";die();
                }
                else
                {
                    if($_SESSION['user_type'] == 'FC')
                    {
                        $this->session->set_userdata('username', $username);
                        $this->session->set_userdata('crm_email_id', $email_id);
                        $this->session->set_userdata('client_uid', $id);
                        $data=$this->db->query("SELECT * FROM user WHERE id='".$id."'")->row_array();
                        $this->session->set_userdata($data);
                        //echo "Hello";die();
                        redirect('Overview');
                    }
                    else
                    { 
                        $this->session->set_userdata('username', $username);
                        $this->session->set_userdata('crm_email_id', $email_id);
                        $this->session->set_userdata('admin_id', $id);
                        $data=$this->db->query("SELECT * FROM user WHERE id='".$id."'")->row_array();
                        
                        $this->session->set_userdata($data);     
                        $this->Common_mdl->role_identify();
                        
                        //echo "Hello";die();
                        if($count['login_count'] == 0 && ($_SESSION['user_type'] == 'FU' || $_SESSION['user_type'] == 'FA'))
                        {
                          $this->Common_mdl->add_deadline_notification();
                        }               
                        redirect('firm_dashboard');
                    }
                }
           }
        }
        else
        {
            $this->session->set_flashdata('error','Incorrect Login Credentials!!!');
            $this->load->view('login',$data);
        }
    }
    public function send_email_notification(){
        $this->Loginchk_model->send_user_login_nofification();
    }
    public  function SA_LoginAs_FA( $Firm_id )
    {
        if( !empty( $_SESSION['is_superAdmin_login'] ) )
        { 
            $data = $this->db->query("SELECT username,confirm_password FROM user WHERE `Firm_id`=".$Firm_id." and user_type='FA' ")->row_array();
            if( !empty( $data ) )
            {   

                $_POST['username'] =  $data['username'];
                $_POST['password'] =  $data['confirm_password'];
                $_SESSION['SA_TO_FA'] = 1; 
                $this->login_chk();
            }
            else
            {
                redirect('Super_admin');        
            }

        }
        else
        {
            redirect('Super_admin');
        }
    }


 

    public function forgotPwd()
    {
        $data['uri_seg'] = $this->uri->segment(2);
        $this->load->view('login',$data);
    }

    public function Forgot_pwd()
    {
        $user_name = $this->input->post('user_name');
        $email='';
        $query = $this->db->query('select * from user where username="'.$user_name.'"')->row_array();
        if($query['username']!='')
        { 
            $data['username'] = $query['username'];
             $email = $query['crm_email_id'];
            $user_id = $query['id'];
            $password = $query['password'];
            $encrypt_id = base64_encode($user_id);
            $email_subject  = 'CRM Forgot Password';
            $data['email_content']  = '<a href='.base_url().'login/resetPwd/'.$encrypt_id.' style="display: inline-block;text-align: center;color: #00aeef;text-decoration: none;font-family:;background: #00aeef;color: #fff;font-weight: 600;padding: 5px 10px;border-radius: 3px;">Click Here</a> for Reset Your CRM Account Password';
            $this->session->set_flashdata('email',$email);
            //email template
           // $body = "";
            ob_start(); ?>

                    <div id="m_5349442688376831455m_-3036873862170083609body_content_inner" style="color:#636363;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;font-size:14px;line-height:150%;text-align:left">                                  
                                  <p style="padding: 10px 0;font-family:'Lato';font-size:17px;font-weight: normal;">Hi<span class="blue-name"><a href="javascript:;" style="    color: #00aeef;text-decoration: none; padding-left: 3px;text-transform: capitalize;"><?php echo $data['username'] ?></a></span></p>
                                    <h2 style="color:#555;display:block;font-family:lato;font-size:16px;line-height:130%;margin:16px 0 8px;text-align:left;text-transform:capitalize">we got a request to reset your password.
                                    </h2> 
                                    <div class="re-btn btn btn-primary" style="padding: 15px;text-align: center;">
                                      <?php echo $data['email_content'] ;?>
                                    </div> 
                                    <h2 style="color:#555;display:block;font-family:lato;font-size:16px;line-height:130%;margin:16px 0 8px;text-align:left;text-transform:capitalize">if you ignore this message, your password won't be changed.
                                    </h2>  
                                        
            
                                  </div>


            <?php
            $body = ob_get_clean();

             ob_end_flush(); 

            //redirect('login/email_format');
            //echo $email_content;
            //die;
            // $this->mailsettings();
            // $this->email->from('info@remindoo.co');
            // $this->email->to($email);
            // $this->email->subject($email_subject);
            // $this->email->message($body);

            $send = firm_settings_send_mail( 0 ,$email , $email_subject , $body ); 

           // $send = $this->email->send();
            if($send){
            $this->session->set_flashdata('success', "Kindly Check Your Mail Id");
            redirect('login/forgotPwd');
            }else{
            $this->session->set_flashdata('warning', "Mail Not sent"); 
            redirect('login/forgotPwd'); 
            }
            //$this->session->set_flashdata('success', "Kindly Check Your Mail Id");
            //redirect('login/forgotPwd');
            return true;
        }else{
            $this->session->set_flashdata('warning', "Invalid username");
            redirect('login/forgotPwd');
        }
    }
    public function mailsettings()
    {   
        $this->load->library('email');
        $config['wrapchars'] = 76; // Character count to wrap at.
        $config['mailtype'] = 'html'; // text or html Type of mail. If you send HTML email you must send it as a complete web page. Make sure you don't have any relative links or relative image paths otherwise they will not work.
        //$config['charset'] = 'utf-8'; // Character set (utf-8, iso-8859-1, etc.).
        $this->email->initialize($config);  
    }
    public function resetPwd()
    {
        $data['uri_seg'] = $this->uri->segment(2);
        $data['encrypt_id'] = $this->uri->segment(3);
        $this->load->view('login',$data); 
    }

    public function changePwd($encrypt_id)
    {
       $newPwd = $this->input->post('confirmpwd');
       $id = base64_decode($encrypt_id);
       $data = array("password"=>md5($newPwd),"confirm_password"=>$newPwd);
       $query=$this->Loginchk_model->update('user',$data,'id',$id);
       if($query)
       {
        $this->session->set_flashdata('success', "Success!!! Password Has been changed");
        redirect('login');
       }
    }

    public function logout()
    {
        if($this->session->has_userdata('timer') && $this->session->has_userdata('totalSeconds'))
        {
            $this->session->set_userdata('logout_pause','true');
            redirect('/user/task_list');
        }

        if(empty($_SESSION['client_uid']))
        {
           $this->Security_model->chk_login();  
        }
        else
        {
           $this->Security_model->check_login(); 
        }

        $id = $this->session->userdata('id');
        $datas['log'] = "Deactivate an account( Logout )";
        $datas['createdTime'] = time();
        $datas['module'] = 'Logout';
        $datas['user_id'] = $id;
        $this->Common_mdl->insert('activity_log',$datas);

        session_destroy();

        redirect('login', 'refresh');
    }
public function email_format()
{
     $email = $_SESSION['email'];
        $query = $this->db->query('select * from user where email_id="'.$email.'"')->row_array();

        if($query['username']!='')
        { 
            $name = $query['username'];
            $user_id = $query['id'];
            $password = $query['password'];
            $encrypt_id = base64_encode($user_id);
            $email_subject  = 'CRM Forgot Password';
            $data['email_content']  = /*'<a href='.base_url().'login/resetPwd/'.$encrypt_id.'>Click Here</a> for Reset Your CRM Account Password';*/
            '<a href='.base_url().'login/resetPwd/'.$encrypt_id.' style="display: inline-block;text-align: center;color: #00aeef;text-decoration: none;font-family:;background: #00aeef;color: #fff;font-weight: 600;padding: 5px 10px;border-radius: 3px;">Click Here</a>';
            $data['username']=$query['username'];
$this->load->view('forgot_email_format',$data);
}
else{
  redirect('login/forgotPwd');  
}
}
//end class

// Login check update //

public function login_time_update()
{
    if(isset($_SESSION['userId'])){

        $data['last_login_time']=strtotime("now");
        $this->db->where('id',$_SESSION['userId']);
         $this->db->update('user',$data);

    }
  //  echo 1;
    
}

//End  Login check update //
}


