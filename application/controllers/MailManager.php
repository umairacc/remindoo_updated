<?php
class MailManager extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->library("gmail_api/Gmailapi");		
		$this->load->model(array('Common_mdl', 'Security_model'));
		$this->load->helper(['template_decode', 'firm_columnSettings']);
	}

	public function send_email(){     
      $client         = Gmailapi::getClient();
      $service        = new Google_Service_Gmail($client);      
      $from_email     = "santarshi.appycodes@gmail.com";
      $strRawMessage  = "From: Email <$from_email> \r\n";
      $strRawMessage .= "To: <santarshi.appycodes@gmail.com>\r\n";
      $strRawMessage .= 'Subject: =?utf-8?B?' . base64_encode("subjects") . "?=\r\n";
      $strRawMessage .= "MIME-Version: 1.0\r\n";
      $strRawMessage .= "Content-Type: text/html; charset=utf-8\r\n";
      $strRawMessage .= 'Content-Transfer-Encoding: quoted-printable' . "\r\n\r\n";      
      $mime           = rtrim(strtr(base64_encode($strRawMessage), '+/', '-_'), '=');
      $msg            = new Google_Service_Gmail_Message();
      
      $msg->setRaw($mime);
      $service->users_messages->send("Test Email", $msg);
}

} ?>