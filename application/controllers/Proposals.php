<?php
class Proposals extends CI_Controller 
{
  public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
  public function __construct(){
      parent::__construct();
      $this->load->model(array('Common_mdl','Proposal_model','Security_model'));
      //  $this->load->library('Excel');
       $this->load->library('encryption');
           $this->load->helper(['comman']);
       error_reporting(0);
    }
    public function index()
    {
      $this->Proposal_model->index();             
    }

    public function company_details()
        {        
        $crm_company_name=$_POST['crm_company_name'];
              $image_name='825898.png';

        if(strpos($crm_company_name, 'Leads_') !== false) 
        {
            $leads_id=explode('_',$crm_company_name);
            $lead=$this->Common_mdl->select_record('leads','id',$leads_id[1]);             
            
            $data['image']='<img src="'.base_url().'uploads/'.$image_name.'" alt="image">';           

            $data['content']='';
            $data['content']='<span>'.strtoupper($lead['name']).'</span><input type="hidden" name="receiver_company_name" id="receiver_company_name" value="'.$lead['name'].'">
              <strong>'.$lead['email_address'].'</strong><input type="hidden" name="receiver_mail_id" id="receiver_mail_id" value="'.$lead['email_address'].'">';

            if($lead['phone']!='')
            {
               $data['content'].='<a href="#"><b>ph:</b>'.$lead['phone'].'</a>';
            }
        }
        else
        {  
            $client = $this->Common_mdl->select_record('client','user_id',$crm_company_name);
            $user = $this->Common_mdl->select_record('user','id',$crm_company_name);
            $contact_data=$this->Common_mdl->GetAllWithWheretwo('client_contacts','client_id',$crm_company_name,'make_primary',1);        

            if($user['crm_profile_pic']=='')
            {       
               $data['image']='<img src="'.base_url().'uploads/'.$image_name.'" alt="image">';
            }else
            {
               $data['image']='<img src="'.base_url().'uploads/'.$user['crm_profile_pic'].'" alt="image">';
            }

            $data['content']='';

            $data['content']='<span>'.strtoupper($client['crm_company_name']).'</span><input type="hidden" name="receiver_company_name" id="receiver_company_name" value="'.$client['crm_company_name'].'"><strong>';
            if($contact_data[0]["main_email"]!='')
            {  
              $con=$contact_data[0]['main_email']; 
            }
            else if(!empty($data['crm_email']))
            {  
              $con=$data['crm_email']; 
            }
            else if(!empty($user['crm_email_id']))
            {  
              $con=$user['crm_email_id']; 
            }
            $data['content'].=$con;

            $data['content'].='</strong><input type="hidden" name="receiver_mail_id" id="receiver_mail_id" value="'.$con.'">';
                if($client['crm_mobile_number']!=''){
                    $data['content'].='<a href="#"><b>ph:</b>'.$client['crm_mobile_number'].'</a>';
                }
        } 

      echo json_encode($data);  
    } 

    public function service_category_update()
    {
        $user_id=$this->session->userdata['userId']; 

        $service_category=$_POST['service_category'];
        $service_category_name=$this->db->query("select category_name from proposal_category where id='".$service_category."' and  category_type='service' AND firm_id = '".$_SESSION['firm_id']."'")->row_array();
        $service_category_name=$service_category_name['category_name'];

        $value=array('service_name'=>$_POST['service_name'],'service_price'=>$_POST['service_price'],'service_unit'=>$_POST['service_unit'],'service_description'=>$_POST['service_description'],'service_qty'=>$_POST['service_qty'],'service_category'=>$service_category_name,'service_category_id'=>$_POST['service_category'],'status'=>'active','user_id'=>$user_id,'firm_id' => $_SESSION['firm_id']);
        $data['status']=$this->Common_mdl->insert('proposal_service',$value);
        echo json_encode($data);
    } 

    public function product_category_update()
    {     
        $user_id=$this->session->userdata['userId'];   
        $service_category=$_POST['product_category'];
        $service_category_name=$this->db->query("select category_name from proposal_category where id='".$service_category."'  and  category_type='product' AND firm_id = '".$_SESSION['firm_id']."'")->row_array();
        $service_category_name=$service_category_name['category_name'];

        $value=array('product_name'=>$_POST['product_name'],'product_price'=>$_POST['product_price'],'product_description'=>$_POST['product_description'],'product_category'=>$service_category_name,'product_category_id'=>$_POST['product_category'],'status'=>'active','user_id'=>$user_id,'firm_id' => $_SESSION['firm_id']);
        $data['status']=$this->Common_mdl->insert('proposal_products',$value);
        echo json_encode($data);
    }

    public function subscription_category_update()
    { 
        $user_id=$this->session->userdata['userId'];

        $service_category=$_POST['subscription_category'];
        $service_category_name=$this->db->query("select category_name from proposal_category where id='".$service_category."'  and  category_type='subscription' AND firm_id = '".$_SESSION['firm_id']."'")->row_array();
        $service_category_name=$service_category_name['category_name'];

        $value=array('subscription_name'=>$_POST['subscription_name'],'subscription_price'=>$_POST['subscription_price'],'subscription_unit'=>$_POST['subscription_unit'],'status'=>'active','user_id'=>$user_id,'subscription_description'=>$_POST['subscription_description'],'subscription_category'=>$service_category_name,'subscription_category_id'=>$_POST['subscription_category'],'firm_id' => $_SESSION['firm_id']);
        $data['status']=$this->Common_mdl->insert('proposal_subscriptions',$value);
        echo json_encode($data);
    }

     public function tab_details()
     {  
         $firm_currency = $this->Common_mdl->get_price('admin_setting','firm_id',$_SESSION['firm_id'],'crm_currency');

         $cur_symbols = $this->Common_mdl->getallrecords('crm_currency');

         foreach ($cur_symbols as $key => $value) 
         {
            $currency_symbols[$value['country']] = $value['currency'];
         }
  
         if($_POST['currency_symbol']=='')
         {
            $curr_symbols=$firm_currency;
         }
         else
         {
            $curr_symbols=$_POST['currency_symbol'];  
         }  

         //$tax_details=$_POST['tax_details'];
         
         $tax_details = $this->Common_mdl->GetAllWithWhere('proposal_tax','id',$_POST['tax_details']);
  
        $data['table']='';
        $data['table']='<div class="full-blog">
         <div class="table-scroll">
         <div class="table-services01">
         <div class="tab-topbar">
       <span class="col-span1" style="line-height: 16px;font-family: "pop-regular";">Name/Description</span>
            <span class="col-span2" style="line-height: 16px;font-family: "pop-regular";">Price</span>
            <span class="col-span3" style="line-height: 16px;font-family: "pop-regular";">Qty</span>
            <span class="col-span4" style="line-height: 16px;font-family: "pop-regular";">Subtotal</span>
          </div>';

      foreach ($_POST as $key => $value) 
      { 
         if(gettype($value) == 'string' && strpos($value, ']') != false)
         {  
           $value = trim(str_replace('[', '', $value));
           $value = trim(str_replace(']', '', $value));
           $value = str_replace('"', '', trim($value)); 

           $_POST[$key] = explode(',',$value);
         }
      }

      if(!empty($_POST['item_name'][0])){

        $data['table'].='<span class="service-title" style="font-family:"pop-regular";display: block;font-family:"pop-regular";">Services</span>';

      for($i=0;$i<count($_POST['item_name']);$i++){
      if($_POST['service_optional'][$i]=='1'){
         // $optional='<sup>(optional)</sup>';

          $optional='<div class="checkbox-fade fade-in-primary"><label><input type="checkbox" name="optional" class="optional_check" data-id="service" id="'.$i.'" onchange="edit_optional(this);" checked><span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span></label></div> (optional) ';
      }else{
        $optional='';
      }
      if($_POST['service_quantity'][$i]=='1'){
        // $quantity='true';
        $quantity='<div class="quantity_class" style="display:none;"><input type="text" name="service_quantity_field[]" id="quantity" class="quantity service_qty" value="" onchange="edit_checking()" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" maxlength = "4" min = "1" max = "9999"></div><span class="quantity_cla">'.$_POST['qty'][$i].'</span><button type="button" name="check_quantity" data-id="service" class="check_quantity edit_quantity btn btn-primary" onclick="Quantity_checking(this)">Edit</button><button type="button" name="check_quantity" data-id="service" class="check_quantity update_quantity btn btn-primary" onclick="edit_checking()" style="display:none;">Update</button>';
      }else{
        $quantity='<div class="quantity_class" style="display:none;"><input type="text" name="service_quantity_field[]" id="quantity" class="quantity service_qty" value="" onchange="edit_checking()" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" maxlength = "4" min = "1" max = "9999"></div><span class="quantity_cla">'.$_POST['qty'][$i].'</span>';
      //  $quantity='false';
      }


        $data['table'].='

        <div class="price-row">
            <div class="row3">
              <div class="full-acc">
                <div class="col-span1">
                  <p style="line-height: 11px;"><p> '.$optional.' '.$_POST['item_name'][$i].'</p></p>
                </div>
                <div class="col-span2">
                  <p style="line-height: 11px;text-align: left;">  <input type="hidden" name="edit_price" class="edit_price services_price" value="'.$_POST['price'][$i].'" onchange="edit_checking()"><span class="price_section">'.$_POST['price'][$i].'</span>/'.$_POST['unit'][$i].' </p>
                </div>
                <div class="col-span3">
                  <p style="line-height: 11px;">'.$quantity.'</p>
                </div>
              </div>


              <div class="width-full cmn-spaces" style="text-align:justify !important;">
              '.$_POST['description'][$i].'
        
              </div>
                 <input type="hidden" id="edit_service_tax" class="edit_service_tax" value="'.$_POST['tax'][$i].'"><input type="hidden" id="edit_service_discount" class="edit_service_discount" value="'.$_POST['discount'][$i].'">
              

            </div>

            <div class="row1 cmn-spaces">
              <span class="row-color" style="line-height: 11px;text-align: left;">'.$_POST['qty'][$i] * $_POST['price'][$i] .'</span>
            </div>
          </div>

                ';
      
      }}

    if(!empty($_POST['product_name'][0])){

      $data['table'].='<span class="service-title" style="font-family:"pop-regular";display: block;font-family:"pop-regular";">Products</span>';

      // $data['table'].='<tr><th align="left"  style="border:1px solid #ccc;">Product</th><th align="left"  style="border:1px solid #ccc;">Price/Unit</th><th align="left" style="border:1px solid #ccc;">Qty</th></tr>';
    for($i=0;$i<count($_POST['product_name']);$i++){
      if($_POST['product_optional'][$i]=='1'){
       //  $optional='<sup>(optional)</sup>';
          $optional='<div class="checkbox-fade fade-in-primary"><label><input type="checkbox" name="optional" class="optional_check" data-id="product" id="'.$i.'" onchange="edit_optional(this);" checked ><span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span></label></div> (optional) ';
      }else{
        $optional='';
      }
      if($_POST['product_quantity'][$i]=='1'){

        $quantity='<div class="quantity_class" style="display:none;"><input type="text" name="product_quantity_field[]" id="product_quantity" class="quantity product_qtys" value="" onchange="edit_checking()" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" maxlength = "4" min = "1" max = "9999"></div><span class="quantity_cla">'.$_POST['product_qty'][$i].'</span><button type="button" name="check_quantity" data-id="service" class="check_quantity edit_quantity btn btn-primary" onclick="Quantity_checking(this)">Edit</button><button type="button" name="check_quantity" data-id="service" class="check_quantity update_quantity btn btn-primary" onclick="edit_checking()" style="display:none;">Update</button>';
       //  $quantity='true';
      }else{

        $quantity='<div class="quantity_class" style="display:none;"><input type="text" name="product_quantity_field[]" id="product_quantity" class="quantity product_qtys" value="" onchange="edit_checking()" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" maxlength = "4" min = "1" max = "9999"></div><span class="quantity_cla">'.$_POST['product_qty'][$i].'</span>';
      /// $quantity='false';
      }

      $data['table'].='


        <div class="price-row">
            <div class="row3">
              <div class="full-acc">
                <div class="col-span1">
                  <p style="line-height: 11px;"><p> '.$optional.' '.$_POST['product_name'][$i].'</p></p>
                </div>
                <div class="col-span2">
                  <p style="line-height: 11px;text-align: left;">  <input type="hidden" name="edit_price" class="edit_price products_price" value="'.$_POST['product_price'][$i].'" onchange="edit_checking()"><span class="price_section">'.$_POST['product_price'][$i].'</span></p>
                </div>
                <div class="col-span3">
                  <p style="line-height: 11px;">'.$quantity.'</p>
                </div>
              </div>


              <div class="width-full cmn-spaces" style="text-align:justify !important;">
              '.$_POST['product_description'][$i].'
        
              </div>
                  <input type="hidden" id="edit_product_tax" class="edit_product_tax" value="'.$_POST['product_tax'][$i].'"><input type="hidden" id="edit_product_discount" class="edit_product_discount" value="'.$_POST['product_discount'][$i].'">
              

            </div>

            <div class="row1 cmn-spaces">
              <span class="row-color" style="line-height: 11px;text-align: left;">'.$_POST['product_qty'][$i] * $_POST['product_price'][$i] .'</span>
            </div>

          

          </div>

            ';
    }}

    if(!empty($_POST['subscription_name'][0])){

       $data['table'].='<span class="service-title" style="font-family:"pop-regular";display: block;font-family:"pop-regular";">Subscription</span>';

        // $data['table'].='<tr><th align="left"  style="border:1px solid #ccc;">Subscription</th><th align="left"  style="border:1px solid #ccc;">Price/Unit</th><th align="left" style="border:1px solid #ccc;">Qty</th></tr>';

    for($i=0;$i<count($_POST['subscription_name']);$i++){
      if($_POST['subscription_optional'][$i]=='1'){
        // $optional='<sup>(optional)</sup>';

          $optional='<div class="checkbox-fade fade-in-primary"><label><input type="checkbox" name="optional" class="optional_check" data-id="subscription" id="'.$i.'" onchange="edit_optional(this);" checked><span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span></label></div> (optional) ';
      }else{
        $optional='';
      }
      if($_POST['subscription_quantity'][$i]=='1'){
        $quantity='<div class="quantity_class" style="display:none;"><input type="text" name="subscription_quantity_field" id="subscription_quantity" class="quantity subscription_qty" value="" onchange="edit_checking()" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" maxlength = "4" min = "1" max = "9999"></div><span class="quantity_cla">1</span><button type="button" name="check_quantity" data-id="service" class="check_quantity edit_quantity btn btn-primary" onclick="Quantity_checking(this)">Edit</button><button type="button" name="check_quantity" data-id="service" class="check_quantity update_quantity btn btn-primary" onclick="edit_checking()" style="display:none;">Update</button>';
        // $quantity='true';
      }else{
        $quantity='<div class="quantity_class" style="display:none;"><input type="text" name="subscription_quantity_field" id="subscription_quantity" class="quantity subscription_qty" value="" onchange="edit_checking()" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" maxlength = "4" min = "1" max = "9999"></div><span class="quantity_cla">1</span>';
      //  $quantity='false';
      }

      $data['table'].='



        <div class="price-row">
            <div class="row3">
              <div class="full-acc">
                <div class="col-span1">
                  <p style="line-height: 11px;"><p> '.$optional.' '.$_POST['subscription_name'][$i].'</p></p>
                </div>
                <div class="col-span2">
                  <p style="line-height: 11px;text-align: left;">    <input type="hidden" name="price=" class="edit_price subscription_prices" value="'.$_POST['subscription_price'][$i].'" onchange="edit_checking()"><span class="price_section">'.$_POST['subscription_price'][$i].'</span>/'.$_POST['subscription_unit'][$i].'</p>
                </div>
                <div class="col-span3">
                  <p style="line-height: 11px;">'.$quantity.'</p>
                </div>
              </div>


              <div class="width-full cmn-spaces" style="text-align:justify !important;">
              '.$_POST['subscription_description'][$i].'
        
              </div>

                <input type="hidden" id="edit_subscription_tax" class="edit_subscription_tax" value="'.$_POST['subscription_tax'][$i].'"><input type="hidden" id="edit_subscription_discount" class="edit_subscription_discount" value="'.$_POST['subscription_discount'][$i].'">

            </div>

            <div class="row1 cmn-spaces">
              <span class="row-color" style="line-height: 11px;text-align: left;">'.$_POST['subscription_price'][$i].'</span>
            </div>

          

          </div>



               


            ';
    }}//$_POST['subscription_quantity'][$i] * 

       $data['table'].='
       </div> 
      </div>

    <div class="grand-total style="text-align: right; ">
        <div style="width: 60%;min-height: 10px;display: inline-block;vertical-align: top;"></div>
        <div style="width: 38%;display: inline-block;vertical-align: top;text-align: right;">
            
           
              <p><span>Sub Total</span> <strong><span class="currency_symbol">'.$currency_symbols[$curr_symbols].'</span><span class="Sub_total"> '.$_POST['total_total'].'</span> <input type="hidden" name="grand_total" class="sub_total_field" value=""></strong></p>';

              if($_POST['tax_total']!=0){

           $data['table'].='<p><span>Tax</span><strong><span class="currency_symbol">'.$currency_symbols[$curr_symbols].'</span><span class="ptag_tax">'.$_POST['tax_total'].'</span></strong></p>';
            }
            if($_POST['discount_amount']!=0){

           $data['table'].='<p><span>Discount</span><strong><span class="currency_symbol">'.$currency_symbols[$curr_symbols].'</span><span class="ptag_discount">'.$_POST['discount_amount'].'</span></strong></p>';
          }


           $data['table'].='<p class=""><span>Grand total</span> <strong><span class="currency_symbol">'.$currency_symbols[$curr_symbols].'</span><span class="grand_total_1">'.$_POST['grand_total'].'</span><input type="hidden" name="grand_total" class="grand_total_field" value=""> </strong></p>


                  <input type="hidden" id="over_alltax" class="over_alltax" value="'.$tax_details[0]['tax_rate'].'"><input type="hidden" id="tax_option" class="tax_option" value="'.$_POST['tax_option'].'"><input type="hidden" id="discount_option" class="discount_option" value="'.$_POST['discount_option'].'"><input type="hidden" id="over_alldiscount" class="over_alldiscount" value="'.$_POST['discount_amount'].'"><input type="hidden" id="discount_rate" value="'.$_POST['discount_rate'].'">
                   </div>
                 </div>
                </div>
              </div>';

    
       
       echo json_encode($data);
     }   

   public function template_content()
     {
        $id = $_POST['id']; 
        $data = array();
        $template = $this->Common_mdl->select_record('proposal_template','id',$id);        

        $doc = new DOMDocument(); 
        $doc->loadHTML($template['template_content']); 

        $ddhead = $doc->getElementById('dd-head'); 
        $ddbody = $doc->getElementById('dd-body');
        $ddfooter = $doc->getElementById('dd-footer');

        if(!empty(trim($ddhead->textContent)))
        {  
            foreach($ddhead->childNodes as $key => $value) 
            {
              $data['template_head'] .= trim($doc->saveHTML($value)); 
            }                     
        } 
        if(!empty(trim($ddbody->textContent)))
        {       
            foreach($ddbody->childNodes as $key => $value) 
            {
              $data['template_body'] .= trim($doc->saveHTML($value)); 
            }  
        }
        if(!empty(trim($ddfooter->textContent)))
        {         
            foreach($ddfooter->childNodes as $key => $value) 
            {
              $data['template_footer'] .= trim($doc->saveHTML($value)); 
            }  
        }

        //$body1=$data['template_heads'][0];
        //$random_string=$this->generateRandomString();
        //$a1  =   array('[Username]'=>'username','[Client Name]'=>'client name','[Accountant Name]'=>'account name','[Task Number]'=>'task number','[Task Name]'=>'task name','[Task Due Date]'=>'task due date','[Task Client]'=>'task client','[Firm Name]'=>'firmname','[accountant number]'=>'accoutant number','[invoice amount]'=>'invoice amount','[invoice due date]'=>'invoice due date');
        //$template_heads  =   strtr($body1,$a1); 


        //$data['template_head']=$data['template_heads'][0];

       // $body2=$data['template_heads'][1];
       // $random_string=$this->generateRandomString();
        //$a1  =  array('[Username]'=>'username','[Client Name]'=>'client name','[Accountant Name]'=>'account name','[Task Number]'=>'task number','[Task Name]'=>'task name','[Task Due Date]'=>'task due date','[Task Client]'=>'task client','[Firm Name]'=>'firmname','[accountant number]'=>'accoutant number','[invoice amount]'=>'invoice amount','[invoice due date]'=>'invoice due date');
        //$template_heads1  =   strtr($body2,$a1); 

        //$data['template_footer']=$data['template_heads'][1];

        $data['template_name']=$template['template_title'];
        $data['template_id']=$template['id']; 
        echo json_encode($data);
     }

     public function history()
     {
        $data['proposal_history'] = $this->Proposal_model->proposals();

        $this->load->view('proposal/proposal_history',$data);
     }

     public function New_proposal(){
      $title='new_proposal';
      $name=$_POST['company_name']; 
      $proposal_name=$_POST['proposal_name']; 
      $receiver_mail_id=$_POST['receiver_mail_id'];
      $sender_company=$_POST['sender_company'];
      // if($_POST['company_name']!=''){
      //  $company_details=$this->Common_mdl->select_record('client','crm_company_name',$company_name);
      //  $name=$company_details['crm_first_name'];
      // }else{
      //  $name=$receiver_mail_id;
      // }
      
      $datas=$this->Common_mdl->getTemplates(1);
            $datas= $datas[0];
      $views=$datas['body'];
      $views1=html_entity_decode($datas['subject']);
      $url=base_url().'proposals/';
//echo $name;
       $a  =   array('::ClientName::'=>$name,'::ProposalCodeNumber::'=>$url);
       $content['email_content']  =   strtr($views,$a); 
       $a1  =   array(':: ProposalName::'=>$proposal_name,'::SenderCompany::'=>$sender_company);
       $content['email_subject']  =   strtr($views1,$a1); 
       $data['body'] =html_entity_decode(firm_mail_header()).'<p id="mail_subject">'.$content['email_subject'].'</p></br><p>'.$content['email_content'].'</p>'.html_entity_decode(firm_mail_footer());
      echo json_encode($data);
     }  
    
      public function e_signature(){
      $this->load->view('proposal/my_sign');
     }

     public function save_sign(){
      $p_id=$_SESSION['proposals'];
      //echo $p_id;
      $result = array();
      $imagedata = base64_decode($_POST['img_data']);
      $filename = md5(date("dmYhisA"));
      //Location to where you want to created sign image
      $file_name = 'uploads/doc_signs/'.$filename.'.png';
      file_put_contents($file_name,$imagedata);
      $result['status'] = 1;
      $result['file_name'] = $file_name;
      $user_id=$this->session->userdata['userId'];  
      $data=array('user_id'=>$user_id,'signature_path'=>$file_name,'status'=>'active','proposal_no'=>$p_id);      
      $insert=$this->Common_mdl->insert('pdf_signatures',$data);
      echo json_encode($result);        
    }

    public function proposals_send()
    {  
       if(!empty($_FILES['userFiles']['name']))
       {
            $filesCount = count($_FILES['userFiles']['name']);

            for($i = 0; $i < $filesCount; $i++)
            {
                $_FILES['userFile']['name'] = $_FILES['userFiles']['name'][$i];
                $_FILES['userFile']['type'] = $_FILES['userFiles']['type'][$i];
                $_FILES['userFile']['tmp_name'] = $_FILES['userFiles']['tmp_name'][$i];
                $_FILES['userFile']['error'] = $_FILES['userFiles']['error'][$i];
                $_FILES['userFile']['size'] = $_FILES['userFiles']['size'][$i];
                $uploadPath = 'attachment/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'gif|jpg|png|pdf';                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if($this->upload->do_upload('userFile')){
                    $fileData = $this->upload->data();
                    $uploadData[$i]['file_name'] = base_url().'attachment/'.$fileData['file_name'];
                }
            }
            if(!empty($uploadData))
            {
                foreach($uploadData as $up) {
                  $file_name[] = $up['file_name'];
                }
                $images=implode(',',$file_name);
            }
            else
            {
              $images='';
            }
        }
     
      $user_id=$this->session->userdata['userId'];  
      if(isset($_POST['pdf_version'])){
       $pdf_version=$_POST['pdf_version'];
      }else{
        $pdf_version='off'; 
      }
      if(isset($_POST['pdf_table_content'])){
       $pdf_table_content=$_POST['pdf_table_content'];
      }else{
       $pdf_table_content='off';
      }
      if(isset($_POST['email_signature'])){ 
        $email_signature=$_POST['email_signature'];
      }else{ 
          $email_signature='off';
      }
      if(isset($_POST['password'])){
        $password=$_POST['password'];
      }else{
        $password='off';
      }
      if(isset($_POST['proposal_password'])){ 
        $proposal_password=$_POST['proposal_password'];
      }else{ 
        $proposal_password=''; 
      }
      if(isset($_POST['expiration'])){ 
        $expiration=$_POST['expiration'];
      }else{
        $expiration='off';
      }
      if(isset($_POST['expiration_date'])){
        $expiration_date=$_POST['expiration_date'];
      }else{ 
        $expiration_date=''; 
      }
      if(isset($_POST['reminder_mail'])){
        $reminder_mail=$_POST['reminder_mail'];
      }else{ 
        $reminder_mail='off'; 
      }

         if($_POST['company_name']!='')
         {          
            if(strpos($_POST['company_name'], 'Leads_') !== false) 
            {
              $lead = explode('_',$_POST['company_name']);
              $lead_id = $lead[1];
              $user_name=$_POST['receiver_mail_id'];
              $client_name= $_POST['receiver_company_name'];
            }
            else
            {
              $details=$this->db->query("SELECT * FROM `user` where `id` = '".$_POST['company_name']."'")->row_array();
              $client_name=$details['crm_name'];        
              $email=$details['crm_email_id'];         
              $last_name=$details['crm_last_name']; 
              $country=$details['crm_country'];
              $city=$details['crm_city'];
              $religion=$details['crm_state'];
              
              $client_det = $this->db->query("SELECT * FROM `client` where `user_id` = '".$_POST['company_name']."'")->row_array();

              $client_website=$client_det['crm_company_url'];
              $client_mobile_number=$client_det['crm_mobile_number'];
              $lead_id = 0;
           }
         }
       

        $user_details = $this->db->query("SELECT * FROM `user` where `id` = '".$_SESSION['id']."'")->row_array();       
        $account_name = $user_details['crm_name'];

        $sender_address = "";

        if(!empty($user_details['crm_street1']))
        { 
           $sender_address .='<p>'.$user_details['crm_street1'].'</p>';
        }   
        if(!empty($user_details['crm_street2']))
        { 
           $sender_address .='<p>'.$user_details['crm_street2'].'</p>';
        } 
        if($user_details['crm_country']!=0)
        { 
           $sender_address .='<p>'.$user_details['crm_country'].'</p>';
        } 
        if($user_details['crm_state']!=0)
        { 
           $sender_address .='<p>'.$user_details['crm_state'].'</p>';
        } 
        if($user_details['crm_city']!=0)
        { 
           $sender_address .='<p>'.$user_details['crm_city'].'</p>';
        } 
        if($user_details['crm_postal_code']!=0)
        { 
           $sender_address .='<p>'.$user_details['crm_postal_code'].'</p>';
        } 
        
        $p_no=$_POST['p_no'];
        $signature = $this->db->query('select * from pdf_signatures where proposal_no="'.$p_no.'" Order By id Desc Limit 1')->row_array();
 
        $body1=$_POST['proposal_contents'];
        $random_string=$this->generateRandomString();
        $a1  =   array('::Username::'=>$user_name,
          '::Client Name::'=>$client_name,
          '::Client Company::'=>$client_name,
          '::Client Company City::'=>$city,
          '::Client Company Country::'=>$country,
          '::Client Company Religion::'=>$religion,
          '::Client First Name::'=>$client_name,
          '::Client Last Name::'=>$last_name,
          ':: ClientPhoneno::'=> $client_mobile_number,
          ':: ClientWebsite::'=>$client_website,
          ':: ProposalCurrency::'=>$_POST['currency_symbol'],
          ':: ProposalExpirationDate::'=>$expiration_date,
          ':: ProposalName::'=>$_POST['proposal_name'],
          '::SenderCompany::'=>$_POST['sender_company'],
          '::SenderCompanyAddress::'=>$sender_address,
          '::SenderCompanyCity::'=>$user_details['crm_city'],
          '::SenderCompanyCountry::'=>$user_details['crm_country'],
          '::SenderCompanyReligion::'=>$user_details['crm_state'],
          '::SenderEmail::'=>$_POST['sender_mail_id'],
          '::SenderEmailSignature::'=> '<img src="'.base_url().$signature['signature_path'].'" style="width:100px;display: block;">',
          ':: Sender Name::'=>$account_name,
          '::senderPhoneno::'=>$user_details['crm_phone_number'],
          ':: Date ::'=>date("Y-m-d h:i:s"));  
        
        $proposal_content_new  =   strtr($body1,$a1); 
  
       if($email_signature == 'on')
        {
           //$replace_data = '<div class="get-options choose ui-draggable ui-draggable-handle e_signature" data-id="e_signature"><img src="'.base_url().$signature['signature_path'].'" style="width:100px;height:100px;display: block;"></div>';
        }
        else
        {
           $replace_data = '<div class="get-options choose ui-draggable ui-draggable-handle e_signature" data-id="e_signature"></div>';
        }

        $doc = new DOMDocument();      
        $doc->loadHTML(preg_replace("/\s\s+/", "", $proposal_content_new));                
        $xpath = new DOMXPath($doc);      
        $elementsThatHaveData_id = $xpath->query('//*[@data-id="e_signature"]');     

        foreach($elementsThatHaveData_id as $value) 
        {   
           $string = $doc->saveHTML($value); 
           $string = preg_replace("/>\s+/", ">", $string); 
         
           $proposal_content_new = str_replace($string,$replace_data,preg_replace("/\s\s+/", "", $proposal_content_new)); 
        }   

        $data['records']['proposal_contents']=$proposal_content_new;

      /* get content from template*/
        $dom = new DomDocument();
        $dom->loadHTML(preg_replace("/\s\s+/", "", $proposal_content_new));
        $finder = new DomXPath($dom);
        $classname = "ready-for-edit"; 
        $nodes = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname ')]"); 
        $links = '';
        foreach ($nodes as $container) 
        {
            $string = $dom->saveHTML($container);
            $links .= $string;
        }         
        
        $dom = new DomDocument();
        $dom->loadHTML(preg_replace("/\s\s+/", "", $links));
        $finder = new DomXPath($dom);
        $classname = "checkbox-fade";
        $check_box = "<input type='checkbox' checked>";
        $val_nodes = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname ')]");
        foreach ($val_nodes as $val_container) 
        {
            $string = $dom->saveHTML($val_container);
            $links = str_replace($string, $check_box, preg_replace("/\s\s+/", "", $links));
        }
        /* get content from template */           
       
        $data['records']['pdf_content']=$links;     


        $userid=$this->session->userdata['userId']; 
        $data['images_section']=$_POST['images_section'];     
        
        if($email_signature == 'on')
        {   
           $data['signature']=$signature;
           $data['records']['email_signature'] = 'on';
        }
        $proposal_name=$_POST['proposal_name'];
        $data['images_section']=$_POST['images_section'];  
        $data['client_name']  = $client_name;
        $data['proposal_name']  = $proposal_name;
        $data['sender_address']  = $sender_address;

        // $this->load->library('Tc');


        // $pdf = new Tc('P', 'mm', 'A4', true, 'UTF-8', false); 
        // // remove default header/footer
        // $pdf->setPrintHeader(false);
        // $pdf->setPrintFooter(false); 
        // $pdf->SetFont('times', '', 10, '', false);
        //  $pdf->setFontSpacing('0'); 
        // $pdf->AddPage('P','A4');    
        // $htmls = $this->load->view('proposal/firstpage',$data,true); 
        // $pdf->WriteHTML($htmls);
        // $pdf->SetMargins(10, 20, 10, true);
        // $pdf->setCellPadding(0,0,0,0); 
        // $pdf->AddPage('P','A4');
        // $pdf->SetFont('times', '', 10, '', false);
        // $pdf->setFontSpacing('0');
        // $html = $this->load->view('proposal/test_pdf',$data,true);        
        // $pdf->SetTitle($proposal_name);         
        // //$pdf->SetHeaderMargin(30);
        // //$pdf->SetTopMargin(10);        
        // //$pdf->setFooterMargin(20);
        // $pdf->setPageOrientation('Portrait',1,2);       
        // $pdf->SetAuthor('Author');      
        // $pdf->WriteHTML($html);


       // $this->load->view('proposal/test_pdf',$data);   exit; 

        $this->load->library('Pdf');
        $dompdf = new Pdf();

         //general options for pdf
        $dompdf ->set_option('isHtml5ParserEnabled', true);
        $dompdf ->set_option('defaultMediaType', 'all');
        $dompdf ->set_option('isFontSubsettingEnabled', true);
        $dompdf ->set_option('isRemoteEnabled', true);


        $dompdf->set_paper("A4");
        // load the html content
        $html = ''; 
        $html = $this->load->view('proposal/firstpage',$data,true); 
        $html .= $this->load->view('proposal/test_pdf',$data,true);    
        $html = $this->Common_mdl->closetags($html); 
        $dompdf->load_html($html); 
        $dompdf->render();
       // $dompdf->stream("dompdf_out.pdf", array("Attachment" => false));exit;

        $output = $dompdf->output();
        $dir = 'uploads/proposal_pdf/';
        $timestamp = date("Y-m-d_H:i:s");
        $filename=$proposal_name.'_'.$timestamp.'.pdf';       

      //  $pdf->Output(FCPATH . $dir . $filename, 'F');

        file_put_contents(FCPATH . $dir . $filename, $output);

         $crm_company_name=$_POST['company_name'];

         if (strpos($crm_company_name, 'Leads_') !== false) 
         {
           $leads_id=explode('_',$crm_company_name);
           $leads_name=$this->Common_mdl->select_record('leads','id',$leads_id[1]);
           $company_id=$leads_name['name'];
           $data['lead_id']=$leads_id[1];
         }
         else
         {          
            $company_id=$client_det['crm_company_name'];
            $user_id=$client_det['user_id'];
         }


          if(isset($_POST['sender_company'])){
            $sender_company_name=$_POST['sender_company'];
          }else{
            $sender_company_name=$account_name;
          }

          if(isset($_POST['sender_mail_id'])){
            $sender_company_mailid=$_POST['sender_mail_id'];
          }else{
            $sender_company_mailid=$user_details['crm_email_id'];
          }          

         $body1=$_POST['proposal_contents_new'];
         $random_string=$this->generateRandomString();              
         $proposal_content  =   strtr($body1,$a1); 

        $ima_ges=explode(',',$_POST['attch_image']);
        $attachhh=array();

        for($i=0;$i<count($ima_ges);$i++)
        {
            $imgExts = array("gif", "jpg", "jpeg", "png", "tiff", "tif");
            $urls = $ima_ges[$i];
            $urlExt = pathinfo($urls, PATHINFO_EXTENSION);

            $url1=pathinfo($urls)['filename'];
            $strlower = preg_replace("~[\\\\/:*?'&,<>|]~", '', $url1);
            $lesg = str_replace(' ', '_', $strlower);
            //  $lesg_s = str_replace('-', '_', $lesg);
            $lesgs = str_replace('.', '_', $lesg);

            //  echo pathinfo($url1)['extension']; // "ext"

            $ext = pathinfo($urls, PATHINFO_EXTENSION);

            // echo   $lesgs=pathinfo($url1)['filename']; // "file"
            $att_image=$lesgs.'.'.$ext;     

             if($att_image!='.'){
              array_push($attachhh,$att_image);
            }
         }

        if(isset($_POST['service_optional'])){

         $service_optional=implode(',',$_POST['service_optional']);
          }else{
            $service_optional='';
          }
         if(isset($_POST['service_quantity'])){ 
          $service_quantity=implode(',',$_POST['service_quantity']);
           }else{
            $service_quantity='';
           }
        if(isset($_POST['product_optional'])){

        $product_optional=implode(',',$_POST['product_optional']);
        }else{
          $product_optional='';
        }
        if(isset($_POST['product_quantity'])){ 
          $product_quantity=implode(',',$_POST['product_quantity']);
        }else{
          $product_quantity='';
        }
        if(isset($_POST['subscription_optional'])){ 
          $subscription_optional=implode(',',$_POST['subscription_optional']);
           }else{
            $subscription_optional='';
           }
        if(isset($_POST['subscription_quantity'])){ 
          $subscription_quantity=implode(',',$_POST['subscription_quantity']);
           }else{
            $subscription_quantity='';
           }

        $attaching_image=implode(',',$attachhh);


        $data=array('proposal_name'=>$_POST['proposal_name'],
                    'proposal_type'=>$_POST['proposal_type'],
                    'company_name'=>$company_id,
                    'proposal_no'=>$_POST['proposal_no'],       
                    'item_name'=>implode(',',array_filter($_POST['item_name'])),
                    'service_category'=>implode(',',array_filter($_POST['service_category_name'])),
                    'price'=>implode(',',array_filter($_POST['price'],'is_numeric')),
                    'unit'=>implode(',',array_filter($_POST['unit'],'is_numeric')),
                    'qty'=>implode(',',array_filter($_POST['qty'],'is_numeric')),
                    'tax'=>implode(',',array_filter($_POST['service_tax'],'is_numeric')),
                    'discount'=>implode(',',array_filter($_POST['service_discount'],'is_numeric')),
                    'description'=>implode('(,)',array_filter($_POST['description'])),
                    'product_name'=>implode(',',array_filter($_POST['product_name'])),
                    'product_category_name'=>implode(',',array_filter($_POST['product_category_name'])),
                    'product_price'=>implode(',',array_filter($_POST['product_price'],'is_numeric')),
                    'product_qty'=>implode(',',array_filter($_POST['product_qty'],'is_numeric')),
                    'product_tax'=>implode(',',array_filter($_POST['product_tax'],'is_numeric')),
                    'product_discount'=>implode(',',array_filter($_POST['product_discount'],'is_numeric')),
                    'product_description'=>implode('(,)',array_filter($_POST['product_description'])),
                    'subscription_name'=>implode(',',array_filter($_POST['subscription_name'])),
                    'subscription_category_name'=>implode(',',array_filter($_POST['subscription_category_name'])),
                    'subscription_price'=>implode(',',array_filter($_POST['subscription_price'],'is_numeric')),
                    'subscription_unit'=>implode(',',array_filter($_POST['subscription_unit'],'is_numeric')),
                    'subscription_tax'=>implode(',',array_filter($_POST['subscription_tax'],'is_numeric')),
                    'subscription_discount'=>implode(',',array_filter($_POST['subscription_discount'],'is_numeric')),
                    'subscription_description'=>implode('(,)',array_filter($_POST['subscription_description'])),
                    'proposal_contents'=>$proposal_content,
                    'proposal_mail'=>$_POST['proposal_mail'],
                    'pdf_version'=>$pdf_version,
                    'pdf_table_content'=>$pdf_table_content,
                    'email_signature'=>$email_signature,
                    'password'=>$password,
                    'pdf_password'=>$proposal_password,
                    'expiration'=>$expiration,
                    'expiration_date'=>$expiration_date,
                    'reminder_mail'=>$reminder_mail,
                    'status'=>$_POST['status'],
                    'user_id'=>$userid,
                    'attachment'=>$attaching_image,
                    'pdf_file'=>$filename,
                    'currency'=>isset($_POST['currency_symbol'])? $_POST['currency_symbol'] : '',
                    'pricelist'=>$_POST['pricelist'],
                    'company_id'=>$user_id,
                    'sender_company_name'=>$sender_company_name,
                    'sender_company_mailid'=>$sender_company_mailid,
                    'grand_total'=>$_POST['grand_total'],
                    'total_amount'=>$_POST['total_total'],
                    'lead_id'=>$lead_id,
                    'receiver_mail_id'=>$_POST['receiver_mail_id'],
                    'document_attachment'=>$_POST['document_attachments'],
                    'discount_option'=>isset($_POST['discount'])? $_POST['discount'] : '',
                    'tax_option'=>isset($_POST['tax'] )?$_POST['tax'] : '',
                    'receiver_company_name'=>$_POST['receiver_company_name'],
                    'created_at'=>date('Y-m-d h:i:s'),
                      'service_optional'=>$service_optional,
                      'service_quantity'=>$service_quantity,
                      'product_optional'=>$product_optional,
                      'product_quantity'=>$product_quantity,
                      'subscription_optional'=>$subscription_optional,
                      'subscription_quantity'=>$subscription_quantity,'images_section'=>$_POST['images_section'],'tax_amount'=>$_POST['tax_amount'],
                      'discount_amount'=>$_POST['discount_amount'],
                      'pdf_proposal_content'=>$links,
                      'firm_id' => $_SESSION['firm_id'],
                      'total_tax_id' => $_POST['total_tax_id']
                  );           

      if(!empty($uploadData)){        
          foreach($file_name as $key => $value) {         
            $file = $file_name[$key];           
          }
        }
        
        if($sender_company_mailid==''){
          $from='info@remindoo.org';
        }else{
          $from=$sender_company_mailid;
        }

      if($_POST['proposal_mail']!='' && $_POST['proposal_contents']!='')
      {
         $last_id=$this->Common_mdl->insert('proposals',$data);   

         $data=array('proposal_id'=>$last_id,'tag'=>'insert','created_at'=>date('Y-m-d h:i:s'));
         $inse=$this->Common_mdl->insert('proposal_history',$data);  

          if($_POST['status']=='sent')
          {          
              $email_subject=$_POST['subject'];
              $email_subject  =   strtr($email_subject,$a1); 
              !empty($_POST['receiver_mail_id'])?$email = $_POST['receiver_mail_id'] : $email = $email;
              $body1  = $_POST['proposal_mail'];
              $body1  =   strtr( $body1 , $a1 );
              $random_string=$this->generateRandomString();
              $a1    =     array(':: Proposal Link ::'=> base_url().'Proposal/proposal/'.$random_string.'---'.$last_id.'');
              $content['body']  =   strtr($body1,$a1); 
              $email_subject = preg_replace( '/ {2,}/', ' ', str_replace('&nbsp;', ' ', strip_tags( $email_subject ) ) );
              $content['attachment'][]  = FCPATH.'uploads/proposal_pdf/'.$filename;

           

              if($_POST['document_attachments']!='')
              {
                  $attaching=explode(',',$_POST['document_attachments']);
                  for($i=0;$i< count($attaching);$i++)
                  {
                    $content['attachment'][] = $attaching[$i];
                  }
              }

            if(!empty($attaching_image))
            { 
                $attaching_image=explode(',',$attaching_image);     
                for($i=0;$i<count($attaching_image);$i++)
                {
                    $content['attachment'][] = FCPATH.'attachment/'.$attaching_image[$i];           
                }
            }       

            $send           = firm_settings_send_mail( $user_details['firm_id'] , $email , $email_subject , $content );
              
              if( $send['result'] )
              {
                 $datas['log'] = "Sent ".$_POST['proposal_name']." Proposal To ".$email;
                 $datas['createdTime'] = time();
                 $datas['module'] = 'Proposal';
                 $datas['user_id'] = $company_id;
                 $this->Common_mdl->insert('activity_log',$datas);
              }
          } 
    

          $random_string=$this->generateRandomString();
          redirect('Proposal_page/step_proposal/'.$random_string.'---'.$last_id.'');
        }
        else
        {
          redirect('proposal_page/step_proposal');
        }
    }

    public function proposals_update()
    {
        if(!empty($_FILES['userFiles']['name']))
        {
          $filesCount = count($_FILES['userFiles']['name']);
                for($i = 0; $i < $filesCount; $i++){
                    $_FILES['userFile']['name'] = $_FILES['userFiles']['name'][$i];
                    $_FILES['userFile']['type'] = $_FILES['userFiles']['type'][$i];
                    $_FILES['userFile']['tmp_name'] = $_FILES['userFiles']['tmp_name'][$i];
                    $_FILES['userFile']['error'] = $_FILES['userFiles']['error'][$i];
                    $_FILES['userFile']['size'] = $_FILES['userFiles']['size'][$i];
                    $uploadPath = 'attachment/';
                    $config['upload_path'] = $uploadPath;
                    $config['allowed_types'] = 'gif|jpg|png|pdf';                
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if($this->upload->do_upload('userFile')){
                        $fileData = $this->upload->data();
                        $uploadData[$i]['file_name'] = base_url().'attachment/'.$fileData['file_name'];
                    }
                }
                if(!empty($uploadData)){
            foreach($uploadData as $up) {
            $file_name[] = $up['file_name'];
            }
            $images=implode(',',$file_name);
          }else{
            $images='';
          }
          }
          $user_id=$this->session->userdata['userId'];  
          if(isset($_POST['pdf_version'])){
           $pdf_version=$_POST['pdf_version'];
          }else{
            $pdf_version='off'; 
          }
          if(isset($_POST['pdf_table_content'])){
           $pdf_table_content=$_POST['pdf_table_content'];
          }else{
           $pdf_table_content='off';
          }
          if(isset($_POST['email_signature'])){ 
            $email_signature=$_POST['email_signature'];
          }else{ 
              $email_signature='off';
          }
          if(isset($_POST['password'])){
            $password=$_POST['password'];
          }else{
            $password='off';
          }
          if(isset($_POST['proposal_password'])){ 
            $proposal_password=$_POST['proposal_password'];
          }else{ 
            $proposal_password=''; 
          }
          if(isset($_POST['expiration'])){ 
            $expiration=$_POST['expiration'];
          }else{
            $expiration='off';
          }
          if(isset($_POST['expiration_date'])){
            $expiration_date=$_POST['expiration_date'];
          }else{ 
            $expiration_date=''; 
          }
          if(isset($_POST['reminder_mail'])){
            $reminder_mail=$_POST['reminder_mail'];
          }else{ 
            $reminder_mail='off'; 
          }

          if($_POST['company_name']!='')
          {          
             if(strpos($_POST['company_name'], 'Leads_') !== false) 
             {
               $lead = explode('_',$_POST['company_name']);
               $lead_id = $lead[1];
               $user_name=$_POST['receiver_mail_id'];
               $client_name= $_POST['receiver_company_name'];
             }
             else
             {
               $details=$this->db->query("SELECT * FROM `user` where `id` = '".$_POST['company_name']."'")->row_array();
               $client_name=$details['crm_name'];        
               $email=$details['crm_email_id'];         
               $last_name=$details['crm_last_name']; 
               $country=$details['crm_country'];
               $city=$details['crm_city'];
               $religion=$details['crm_state'];
               
               $client_det = $this->db->query("SELECT * FROM `client` where `user_id` = '".$_POST['company_name']."'")->row_array();

               $client_website=$client_det['crm_company_url'];
               $client_mobile_number=$client_det['crm_mobile_number'];
               $lead_id = 0;
            }
          }
          
          $user_details= $this->db->query("SELECT * FROM `user` where `id` = '".$_SESSION['id']."'")->row_array();       
          $account_name=$user_details['crm_name'];

          $sender_address = "";

          if(!empty($user_details['crm_street1']))
          { 
             $sender_address .='<p>'.$user_details['crm_street1'].'</p>';
          }   
          if(!empty($user_details['crm_street2']))
          { 
             $sender_address .='<p>'.$user_details['crm_street2'].'</p>';
          } 
          if($user_details['crm_country']!=0)
          { 
             $sender_address .='<p>'.$user_details['crm_country'].'</p>';
          } 
          if($user_details['crm_state']!=0)
          { 
             $sender_address .='<p>'.$user_details['crm_state'].'</p>';
          } 
          if($user_details['crm_city']!=0)
          { 
             $sender_address .='<p>'.$user_details['crm_city'].'</p>';
          } 
          if($user_details['crm_postal_code']!=0)
          { 
             $sender_address .='<p>'.$user_details['crm_postal_code'].'</p>';
          } 

          $p_no=$_POST['p_no'];
          $signature = $this->db->query('select * from pdf_signatures where proposal_no="'.$p_no.'" Order By id Desc Limit 1')->row_array();

          $body1=$_POST['proposal_contents'];
          $random_string=$this->generateRandomString();
          $a1  =   array('::Username::'=>$user_name,
            '::Client Name::'=>$client_name,
            '::Client Company::'=>$client_name,
            '::Client Company City::'=>$city,
            '::Client Company Country::'=>$country,
            '::Client Company Religion::'=>$religion,
            '::Client First Name::'=>$client_name,
            '::Client Last Name::'=>$last_name,
            ':: ClientPhoneno::'=> $client_mobile_number,
            ':: ClientWebsite::'=>$client_website,
            ':: ProposalCurrency::'=>$_POST['currency_symbol'],
            ':: ProposalExpirationDate::'=>$expiration_date,
            ':: ProposalName::'=>$_POST['proposal_name'],
            '::SenderCompany::'=>$_POST['sender_company'],
            '::SenderCompanyAddress::'=>$sender_address,
            '::SenderCompanyCity::'=>$user_details['crm_city'],
            '::SenderCompanyCountry::'=>$user_details['crm_country'],
            '::SenderCompanyReligion::'=>$user_details['crm_state'],
            '::SenderEmail::'=>$_POST['sender_mail_id'],
            '::SenderEmailSignature::'=>'<img src="'.base_url().$signature['signature_path'].'" style="width:100px;display: block;">',
            ':: Sender Name::'=>$account_name,
            '::senderPhoneno::'=>$user_details['crm_phone_number'],
            ':: Date ::'=>date("Y-m-d h:i:s"));  

          $proposal_content_new  =   strtr($body1,$a1);  

        /*   if($email_signature == 'on')
           {
              //$replace_data = '<div class="get-options choose ui-draggable ui-draggable-handle e_signature" data-id="e_signature"><img src="'.base_url().$signature['signature_path'].'" style="width:100px;height:100px;display: block;"></div>';
           }
           else
           {
              $replace_data = '<div class="get-options choose ui-draggable ui-draggable-handle e_signature" data-id="e_signature"></div>';
           }*/

          $doc = new DOMDocument();      
          $doc->loadHTML(preg_replace("/\s\s+/", "", $proposal_content_new));                
          $xpath = new DOMXPath($doc);      
          $elementsThatHaveData_id = $xpath->query('//*[@data-id="e_signature"]');     

          foreach($elementsThatHaveData_id as $value) 
          {   
             $string = $doc->saveHTML($value);
             $string = preg_replace("/>\s+/", ">", $string);
           
             $proposal_content_new = str_replace($string,$replace_data,preg_replace("/\s\s+/", "", $proposal_content_new)); 
          } 
          
          $data['records']['proposal_contents']=$proposal_content_new;



            /* get content from template*/
            $dom = new DomDocument();
            $dom->loadHTML(preg_replace("/\s\s+/", "", $proposal_content_new));
            $finder = new DomXPath($dom);
            $classname = "ready-for-edit";
            $nodes = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname ')]");
            $links = '';
            foreach ($nodes as $container) {
                $string = $dom->saveHTML($container);
                $links.= $string;
            }
            $dom = new DomDocument();
            $dom->loadHTML(preg_replace("/\s\s+/", "", $links));
            $finder = new DomXPath($dom);
            $classname = "checkbox-fade";
            $check_box = "<input type='checkbox' checked>";
            $val_nodes = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname ')]");
            foreach($val_nodes as $val_container) 
            {
                $string = $dom->saveHTML($val_container);
                $links = str_replace($string, $check_box, preg_replace("/\s\s+/", "", $links));
            }
            
            /* get content from template */
            $data['records']['pdf_content']=$links;

          $userid=$this->session->userdata['userId'];   

          if($email_signature == 'on')
          {   
             $data['signature']=$signature;
             $data['records']['email_signature'] = 'on';
          }

         $proposal_name=$_POST['proposal_name'];
         $data['client_name']  = $client_name;
         $data['proposal_name']  = $proposal_name;
         $data['sender_address']  = $sender_address;

          $this->load->library('Pdf');
          $dompdf = new Pdf();

             //general options for pdf
          $dompdf ->set_option('isHtml5ParserEnabled', true);
          $dompdf ->set_option('defaultMediaType', 'all');
          $dompdf ->set_option('isFontSubsettingEnabled', true);
          $dompdf ->set_option('isRemoteEnabled', true);


          $dompdf->set_paper("A4");
          // load the html content
          $html = ''; 
          $html = $this->load->view('proposal/firstpage',$data,true); 
          $html .= $this->load->view('proposal/test_pdf',$data,true); 
          $html = $this->Common_mdl->closetags($html);  
          $dompdf->load_html($html);
          $dompdf->render();
          // $dompdf->stream("dompdf_out.pdf", array("Attachment" => false));

          $output = $dompdf->output();
          $dir = 'uploads/proposal_pdf/';
          $timestamp = date("Y-m-d_H:i:s");
          $filename=$proposal_name.'_'.$timestamp.'.pdf';       

      //  $pdf->Output(FCPATH . $dir . $filename, 'F');

        file_put_contents(FCPATH . $dir . $filename, $output);

         $crm_company_name=$_POST['company_name'];

         if (strpos($crm_company_name, 'Leads_') !== false) 
         {
           $leads_id=explode('_',$crm_company_name);
           $leads_name=$this->Common_mdl->select_record('leads','id',$leads_id[1]);
           $company_id=$leads_name['name'];
           $data['lead_id']=$leads_id[1];
         }
         else
         {          
            $company_id=$client_det['crm_company_name'];
            $user_id=$client_det['user_id'];
         }       

         if(isset($_POST['sender_company'])){
          $sender_company_name=$_POST['sender_company'];
         }else{
          $sender_company_name=$account_name;
         }

         if(isset($_POST['sender_mail_id'])){
          $sender_company_mailid=$_POST['sender_mail_id'];
         }else{
          $sender_company_mailid=$user_details['crm_email_id'];
         }        

         $body1=$_POST['proposal_contents_new'];
         $random_string=$this->generateRandomString();              
         $proposal_content  =   strtr($body1,$a1);  

          $ima_ges=explode(',',$_POST['attch_image']);
          $attachhh=array();
          for($i=0;$i<count($ima_ges);$i++){
          $imgExts = array("gif", "jpg", "jpeg", "png", "tiff", "tif");
          $urls = $ima_ges[$i];
          $urlExt = pathinfo($urls, PATHINFO_EXTENSION);

          $url1=pathinfo($urls)['filename'];
          $strlower = preg_replace("~[\\\\/:*?'&,<>|]~", '', $url1);
          $lesg = str_replace(' ', '_', $strlower);
          //  $lesg_s = str_replace('-', '_', $lesg);
          $lesgs = str_replace('.', '_', $lesg);



          $ext = pathinfo($urls, PATHINFO_EXTENSION);

          $att_image=$lesgs.'.'.$ext;     
          array_push($attachhh,$att_image);
          }

          if(isset($_POST['service_optional'])){

          $service_optional=implode(',',$_POST['service_optional']);
          }else{
          $service_optional='';
          }
          if(isset($_POST['service_quantity'])){ 
          $service_quantity=implode(',',$_POST['service_quantity']);
          }else{
          $service_quantity='';
          }
          if(isset($_POST['product_optional'])){

          $product_optional=implode(',',$_POST['product_optional']);
          }else{
          $product_optional='';
          }
          if(isset($_POST['product_quantity'])){ 
          $product_quantity=implode(',',$_POST['product_quantity']);
          }else{
          $product_quantity='';
          }
          if(isset($_POST['subscription_optional'])){ 
          $subscription_optional=implode(',',$_POST['subscription_optional']);
          }else{
          $subscription_optional='';
          }
          if(isset($_POST['subscription_quantity'])){ 
          $subscription_quantity=implode(',',$_POST['subscription_quantity']);
          }else{
          $subscription_quantity='';
          }


        $attaching_image=implode(',',$attachhh);

        $data=array('proposal_name'=>$_POST['proposal_name'],
                    'proposal_type'=>$_POST['proposal_type'],
                    'company_name'=>$company_id,
                    'proposal_no'=>$_POST['proposal_no'],
                    'item_name'=>implode(',',array_filter($_POST['item_name'])),
                    'service_category'=>implode(',',array_filter($_POST['service_category_name'])),
                    'price'=>implode(',',array_filter($_POST['price'],'is_numeric')),
                    'unit'=>implode(',',array_filter($_POST['unit'],'is_numeric')),
                    'qty'=>implode(',',array_filter($_POST['qty'],'is_numeric')),
                    'tax'=>implode(',',array_filter($_POST['service_tax'],'is_numeric')),
                    'discount'=>implode(',',array_filter($_POST['service_discount'],'is_numeric')),
                    'description'=> implode('(,)',array_filter($_POST['description'])),
                    'product_name'=>implode(',',array_filter($_POST['product_name'])),
                    'product_category_name'=>implode(',',array_filter($_POST['product_category_name'])),
                    'product_price'=>implode(',',array_filter($_POST['product_price'],'is_numeric')),
                    'product_qty'=>implode(',',array_filter($_POST['product_qty'],'is_numeric')),
                    'product_tax'=>implode(',',array_filter($_POST['product_tax'],'is_numeric')),
                    'product_discount'=>implode(',',array_filter($_POST['product_discount'],'is_numeric')),
                    'product_description'=>implode('(,)',array_filter($_POST['product_description'])),
                    'subscription_name'=>implode(',',array_filter($_POST['subscription_name'])),
                    'subscription_category_name'=>implode(',',array_filter($_POST['subscription_category_name'])),
                    'subscription_price'=>implode(',',array_filter($_POST['subscription_price'],'is_numeric')),
                    'subscription_unit'=>implode(',',array_filter($_POST['subscription_unit'],'is_numeric')),
                    'subscription_tax'=>implode(',',array_filter($_POST['subscription_tax'],'is_numeric')),
                    'subscription_discount'=>implode(',',array_filter($_POST['subscription_discount'],'is_numeric')),
                    'subscription_description'=>implode('(,)',array_filter($_POST['subscription_description'])),                    
                    'proposal_contents'=>$proposal_content,
                    'proposal_mail'=>$_POST['proposal_mail'],
                    'pdf_version'=>$pdf_version,
                    'pdf_table_content'=>$pdf_table_content,
                    'email_signature'=>$email_signature,
                    'password'=>$password,
                    'pdf_password'=>$proposal_password,
                    'expiration'=>$expiration,
                    'expiration_date'=>$expiration_date,
                    'reminder_mail'=>$reminder_mail,
                    'status'=>$_POST['status'],'user_id'=>$userid,'attachment'=>$attaching_image,'pdf_file'=>$filename,'currency'=>$_POST['currency_symbol'],'pricelist'=>$_POST['pricelist'],'company_id'=>$user_id,'sender_company_name'=>$sender_company_name,'sender_company_mailid'=>$sender_company_mailid,'grand_total'=>$_POST['grand_total'],'total_amount'=>$_POST['total_total'],'lead_id'=>$lead_id,'receiver_mail_id'=>$_POST['receiver_mail_id'],'document_attachment'=>$_POST['document_attachments'],'discount_option'=>$_POST['discount'],'tax_option'=>$_POST['tax'],'receiver_company_name'=>$_POST['receiver_company_name'],'service_optional'=>$service_optional,
                      'service_quantity'=>$service_quantity,
                      'product_optional'=>$product_optional,
                      'product_quantity'=>$product_quantity,
                      'subscription_optional'=>$subscription_optional,
                      'subscription_quantity'=>$subscription_quantity,'images_section'=>$_POST['images_section'],'tax_amount'=>$_POST['tax_amount'],
                      'discount_amount'=>$_POST['discount_amount'],
                      'pdf_proposal_content'=>$links,
                      'total_tax_id' => $_POST['total_tax_id']
                  );

            if(!empty($uploadData)){        
              foreach($file_name as $key => $value) {         
                $file = $file_name[$key];           
              }
            }

            $crm_company_name=$_POST['company_name'];

            if (strpos($crm_company_name, 'Leads_') !== false) {

            $leads_id=explode('_',$crm_company_name);

            $leads_name=$this->Common_mdl->select_record('leads','id',$leads_id[1]);

            $company_id=$leads_name['name'];
            $data['lead_id']=$leads_id[1];

            }
        
            if($sender_company_mailid==''){
              $from='info@remindoo.org';
            }else{
              $from=$sender_company_mailid;
            }

   //     $companyid=$this->db->query("SELECT * FROM `client` where `id`='".$_POST['company_name']."'")->row_array();
   //       $company_id=$companyid['crm_company_name'];
   //        $user_id=$companyid['user_id'];

   //        if(isset($_POST['sender_company_name'])){
   //         $sender_company_name=$_POST['sender_company_name'];
   //        }else{
   //         $sender_company_name='Accotax Private Limited';
   //        }

   //        if(isset($_POST['sender_company_mailid'])){
   //         $sender_company_mailid=$_POST['sender_company_mailid'];
   //        }else{
   //         $sender_company_mailid='info@remindoo.org';
   //        }

      // $data=array('proposal_name'=>$_POST['proposal_name'],
      //  'proposal_type'=>$_POST['proposal_type'],
      //  'company_name'=>$company_id,
      //  'proposal_no'=>$_POST['proposal_no'],
      //  'item_name'=>implode(',',$_POST['item_name']),
      //  'service_category'=>implode(',',$_POST['service_category_name']),
      //  'price'=>implode(',',$_POST['price']),
      //  'unit'=>implode(',',$_POST['unit']),
      //  'qty'=>implode(',',$_POST['qty']),
      //  'tax'=>implode(',',$_POST['service_tax']),
      //  'discount'=>implode(',',$_POST['service_discount']),
      //  'description'=>implode(',',$_POST['description']),
      //  'product_name'=>implode(',',$_POST['product_name']),
      //  'product_category_name'=>implode(',',$_POST['product_category_name']),
      //  'product_price'=>implode(',',$_POST['product_price']),
      //  'product_qty'=>implode(',',$_POST['product_qty']),
      //  'product_tax'=>implode(',',$_POST['product_tax']),
      //  'product_discount'=>implode(',',$_POST['product_discount']),
      //  'product_description'=>implode(',',$_POST['product_description']),
      //  'subscription_name'=>implode(',',$_POST['subscription_name']),
      //  'subscription_category_name'=>implode(',',$_POST['subscription_category_name']),
      //  'subscription_price'=>implode(',',$_POST['subscription_price']),
      //  'subscription_unit'=>implode(',',$_POST['subscription_unit']),
      //  'subscription_tax'=>implode(',',$_POST['subscription_tax']),
      //  'subscription_discount'=>implode(',',$_POST['subscription_discount']),
      //  'subscription_description'=>implode(',',$_POST['subscription_description']),
      //  'proposal_contents'=>$_POST['proposal_contents'],
      //  'proposal_mail'=>$_POST['proposal_mail'],
      //  'pdf_version'=>$pdf_version,
      //  'pdf_table_content'=>$pdf_table_content,
      //  'email_signature'=>$email_signature,
      //  'password'=>$password,
      //  'pdf_password'=>$proposal_password,
      //  'expiration'=>$expiration,
      //  'expiration_date'=>$expiration_date,
      //  'reminder_mail'=>$reminder_mail,
      //  'status'=>$_POST['status'],'user_id'=>$userid,'attachment'=>$images,'pdf_file'=>$filename,'currency'=>$_POST['currency'],'pricelist'=>$_POST['pricelist'],'company_id'=>$user_id,'sender_company_name'=>$sender_company_name,'sender_company_mailid'=>$sender_company_mailid,'grand_total'=>$_POST['grand_total'],'total_amount'=>$_POST['total_total']);

      // if(!empty($uploadData)){       
      //    foreach($file_name as $key => $value) {
      //      //print_r($value);
      //      //print_r($key);
      //      $file = $file_name[$key];
      //      //echo $file;
      //      //$this->email->attach($file);
      //    }
      //  }
        
        // if($sender_company_mailid==''){
        //  $from='info@remindoo.org';
        // }else{
        //  $from=$sender_company_mailid;
        // }


        $proposal_id=$_POST['proposal_id'];
        
        $id=$this->Common_mdl->update('proposals',$data,'id',$proposal_id);

        $data=array('proposal_id'=>$proposal_id,'tag'=>'update','created_at'=>date('Y-m-d h:i:s'));
        $inse=$this->Common_mdl->insert('proposal_history',$data);
        
        if($_POST['status']=='sent')
        {          
            $email_subject=$_POST['subject'];

            if(empty($email_subject))
            { 
               $email_subject = '<p>'.$_POST['proposal_name'].' From '.$sender_company_name.'</p>';
            }

            $email_subject  =   strtr($email_subject,$a1); 
            !empty($_POST['receiver_mail_id'])?$email = $_POST['receiver_mail_id'] : $email = $email;
            $body1=$_POST['proposal_mail'];
            $body1  =   strtr($body1,$a1);
            $random_string=$this->generateRandomString();
            $a1  =   array(':: Proposal Link ::'=> base_url().'Proposal/proposal/'.$random_string.'---'.$proposal_id.'');
            $content['body']   =   strtr($body1,$a1);
            $email_subject     =    preg_replace('/ {2,}/', ' ', str_replace('&nbsp;', ' ', strip_tags($email_subject)));
            $content['attachment'][] = FCPATH.'uploads/proposal_pdf/'.$filename;

            if($_POST['document_attachments']!=''){
            $attaching=explode(',',$_POST['document_attachments']);
            for($i=0;$i< count($attaching);$i++){
              $content['attachment'][] = $attaching[$i];
            }
            }

            if(!empty($attaching_image)){ 
            $attaching_image=explode(',',$attaching_image);     
            for($i=0;$i<count($attaching_image);$i++) {
              $content['attachment'][] = FCPATH.'attachment/'.$attaching_image[$i];            
            }
            }

            $send           = firm_settings_send_mail( $user_details['firm_id'] , $email , $email_subject , $content );
            
            if($send['result'])
            {
               $datas['log'] = "Sent ".$_POST['proposal_name']." Proposal To ".$email." (Update)";
               $datas['createdTime'] = time();
               $datas['module'] = 'Proposal';
               $datas['user_id'] = $company_id;
               $this->Common_mdl->insert('activity_log',$datas);
            }
        } 
      
      $proposal_status=$this->Common_mdl->select_record('proposals','id',$proposal_id);

      if($proposal_status['status']=='accepted'){

        $this->invoive_create($proposal_id);  

      }
        $random_string=$this->generateRandomString();
        redirect('proposal/');
    }



    /*01.06.2018 */


    public function generateRandomString($length = 100) {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
    }

     public function mailsettings()
    {   
        $this->load->library('email');
        $config['wrapchars'] = 76; // Character count to wrap at.
        $config['mailtype'] = 'html'; 
        $this->email->initialize($config);  
    }

    public function status_change()
    {
     $id=$_POST['id'];
     $status=$_POST['status'];

       $data['records'] = $this->Common_mdl->select_record('proposals','id',$id);
       $pro = $data['records'];
       
         if($status=='accepted')
         {           
            $lead_id = $pro['lead_id']; 

            if($pro['status']!='accepted')
            { 
                $lead_id=$pro['lead_id'];

                if($lead_id!=0)
                {
                      $l_name = $this->Common_mdl->GetAllWithWheretwo('leads_status','status_name','Win','firm_id','0');
                      $status_id=$l_name[0]['id'];
                      $da_ta['lead_status']=$status_id;

                      $client_limit = $this->Common_mdl->Check_Firm_Clients_limit($pro['firm_id']);

                      if($client_limit>0 || $client_limit == 'unlimited')
                      {
                          if($this->Common_mdl->update('leads',$da_ta,'id',$lead_id))
                          {                         
                                $this->Proposal_model->leads_convert_client($lead_id); // convert to client
                                $this->Proposal_model->leads_convert_task($lead_id); // convert to task
                          }          
                          $this->invoive_create($pro['id']);

                          if($client_limit>0)
                          {
                             $this->Common_mdl->Decrement_Firm_Clients_limit($pro['firm_id']);
                          }
                      }
                      else
                      {
                         $res['msg'] = 'Allowed Client Limit Exceeded.';
                         echo json_encode($res); exit;
                      }    
                }
                else
                {
                        $task_id=array();

                        $assignees = $pro['firm_id'];     

                        $datas['subject']='create/update Client-'.$pro['company_name'];
                        $datas['start_date']=date('Y-m-d');
                        $datas['user_id']=$pro['user_id'];
                        $datas['end_date']=$date = date('Y-m-d', strtotime('+2 days'));;
                        $datas['company_name']= $pro['company_id'];
                        $datas['worker'] = '';
                        $datas['team'] = '';
                        $datas['department'] = '';
                        $datas['manager']='';
                        $datas['billable']='Billable';
                        $datas['description']='';
                        $datas['lead_id']=$pro['lead_id'];          
                        $datas['created_date']=time();
                        $datas['create_by']=$pro['user_id'];
                        $datas['priority'] = 'Low';
                        $datas['related_to'] = 'tasks';
                        $datas['task_status']='notstarted';     
                        $datas['firm_id'] = $pro['firm_id'];

                        $insert_data=$this->db->insert( 'add_new_task', $datas );                  

                        array_push($task_id,$this->db->insert_id());

                        $task = ['firm_id'=>$pro['firm_id'],'module_name'=>'TASK','module_id'=>$this->db->insert_id(),'assignees'=>$assignees ];  
                        $this->db->insert('firm_assignees',$task); 

                        $datas['subject']='Create invoice-'.$pro['company_name'];
                        $datas['start_date']=date('Y-m-d');
                        $datas['user_id']=$pro['user_id'];
                        $datas['end_date']=$date = date('Y-m-d', strtotime('+2 days'));;
                        $datas['company_name']=$pro['company_id'];
                        $datas['worker'] = '';
                        $datas['team'] = '';
                        $datas['department'] = '';
                        $datas['manager']='';
                        $datas['billable']='Billable';       
                        $datas['created_date']=time();
                        $datas['create_by']=$pro['user_id'];
                        $datas['priority'] = 'Low';
                        $datas['related_to'] = 'tasks';
                        $datas['task_status']='notstarted'; 
                        $datas['lead_id']=$pro['lead_id'];   
                        $datas['firm_id'] = $pro['firm_id'];

                        $insert_data=$this->db->insert( 'add_new_task', $datas ); 
                        array_push($task_id,$this->db->insert_id());

                        $task = ['firm_id'=>$pro['firm_id'],'module_name'=>'TASK','module_id'=>$this->db->insert_id(),'assignees'=>$assignees ];  
                        $this->db->insert('firm_assignees',$task); 

                        $convert_data_proposal['task_id']=implode(',',$task_id);
                        $this->Common_mdl->update('proposals',$convert_data_proposal,'id',$id);
                        $this->invoive_create($pro['id']);
                  }
              }
                    $proposal_no=$pro['proposal_no'];
                    $proposal_name=$pro['proposal_name'];
                    $company_name=$pro['company_name'];
                    $currency=$pro['currency'];
                    $expiration_date=$pro['expiration_date'];
                    if($company_name==0){
                         $company_name=$pro['receiver_company_name'];
                    }

                    $senders_details=$this->Common_mdl->select_record('user','id',$pro['user_id']);
                    $sender_email=$senders_details['crm_email_id']; 
                    $sender_name=$senders_details['crm_name']; 
                    $sender_last_name=$senders_details['crm_last_name']; 
                    $sender_country=$senders_details['crm_country'];
                    $sender_city=$senders_details['crm_city'];
                    $sender_religion=$senders_details['crm_state']; 

                    $sender_address = "";

                    if(!empty($senders_details['crm_street1']))
                    { 
                       $sender_address .='<p>'.$senders_details['crm_street1'].'</p>';
                    }   
                    if(!empty($senders_details['crm_street2']))
                    { 
                       $sender_address .='<p>'.$senders_details['crm_street2'].'</p>';
                    } 
                    if($senders_details['crm_country']!=0)
                    { 
                       $sender_address .='<p>'.$senders_details['crm_country'].'</p>';
                    } 
                    if($senders_details['crm_state']!=0)
                    { 
                       $sender_address .='<p>'.$senders_details['crm_state'].'</p>';
                    } 
                    if($senders_details['crm_city']!=0)
                    { 
                       $sender_address .='<p>'.$senders_details['crm_city'].'</p>';
                    } 
                    if($senders_details['crm_postal_code']!=0)
                    { 
                       $sender_address .='<p>'.$senders_details['crm_postal_code'].'</p>';
                    } 

                    $sender_company_details=$this->Common_mdl->select_record('client','user_id',$pro['user_id']);
                    if(count($sender_company_details)>0){
                        $sender_company=$pro['sender_company_name'];
                        $sender_mobile_number='';
                    }else{
                        $sender_company=$sender_company_details['crm_company_name'];
                        $sender_mobile_number=$sender_company_details['crm_mobile_number'];
                    }
                    $sender_company_mailid=$pro['sender_company_mailid'];
                    $proposal_name=$pro['proposal_name'];
                    $company=$pro['company_id'];

                        //if($company!=1){
                    $company_name=$pro['company_name']; 
                       // }else{
                           //  $company_name=$pro['receiver_mail_id'];
                      //  }
                   $sender_details=$this->Common_mdl->select_record('proposals','id',$id);
                   $sender_id=$sender_details['company_id'];  

                 /* if($sender_id =='' || $sender_id == 0){
                         $user_details=$this->Common_mdl->select_record('leads','id',$sender_details['lead_id']);
               //      $client_details=$this->Common_mdl->select_record('client','user_id',$sender_id);
                              $client_website=$user_details['website'];
                              $client_mobile_number=$user_details['phone'];
                              $email=$user_details['email_address']; 
                              $name=$user_details['name']; 
                              $last_name=$user_details['name']; 
                              $country='';
                              $city=$user_details['city'];
                              $religion=$user_details['state'];             
                 }else{*/
                    $email=$sender_details['receiver_mail_id']; 

                    $user_details=$this->Common_mdl->select_record('user','id',$sender_details['company_id']);
                    $client_details=$this->Common_mdl->select_record('client','user_id',$sender_details['company_id']);
                    $client_website=$client_details['crm_company_url'];
                    $client_mobile_number=$client_details['crm_mobile_number'];
                   // $email=$user_details['crm_email_id']; 
                    $name=$user_details['crm_name']; 
                    $last_name=$user_details['crm_last_name']; 
                    $country=$user_details['crm_country'];
                    $city=$user_details['crm_city'];
                    $religion=$user_details['crm_state'];    

            /*    }*/
              $proposal_content=$this->Common_mdl->getTemplates(8);
              $proposal_content= $proposal_content[0];

              $this->Common_mdl->Send_Notification('Proposal','Proposal_Accept',$pro['user_id'],$proposal_name.' proposal was accepted by'.$company_name.'');

              $pdf_signature=$this->Common_mdl->select_record('pdf_signatures','proposal_no',$id);

              if($pdf_signature[0]==''){
                 $email_signature='';
              }else{
                 $email_signature='<img src='.$pdf_signature['signature_path'].'>';
              }

              $body1= html_entity_decode( $proposal_content['body'] );
              $subject=html_entity_decode($proposal_content['subject']);
              $random_string=$this->generateRandomString();
              $link=base_url().'Proposal_page/step_proposal/'.$random_string.'---'.$id;
              $url=base_url().'user/online_registeration/'.$id;     
              $a1  =   array('::Client Name::'=>$name,
                '::Client Company::'=>$company_name,
                '::Client Company City::'=>$city,
                '::Client Company Country::'=>$country,
                '::Client Company Religion::'=>$religion,
                '::Client First Name::'=>$name,
                '::Client Last Name::'=>$last_name,
                '::Client Name::'=>$name,
                ':: ClientPhoneno::'=> $client_mobile_number,
                ':: ClientWebsite::'=>$client_website,
                ':: Proposal Link ::'=>$link,
                ':: ProposalCurrency::'=>$currency,
                ':: ProposalExpirationDate::'=>$expiration_date,
                ':: ProposalName::'=>$proposal_name,
                ':: ProposalCodeNumber::'=>$proposal_no,
                '::SenderCompany::'=>$sender_company,
                '::SenderCompanyAddress::'=>$sender_address,
                '::SenderCompanyCity::'=>$sender_city,
                '::SenderCompanyCountry::'=>$sender_country,
                '::SenderCompanyReligion::'=>$sender_religion,
                '::SenderEmail::'=>$sender_email,
                '::SenderEmailSignature::'=>$email_signature,
                ':: Sender Name::'=>$sender_name,
                '::senderPhoneno::'=>$sender_mobile_number,
                '::ProposalTitle::'=>$proposal_name,
                ':: Date ::'=>date("Y-m-d h:i:s"),
                '::OnlineRegistrationLink::'=>$url);
              $proposal_contents  =   strtr($body1,$a1); 
              $subject  =   strtr($subject,$a1);
              $random_string=$this->generateRandomString();
              $body= $proposal_contents; 
              
             

              $email_subject  =  preg_replace('/ {2,}/', ' ', str_replace( '&nbsp;', ' ', strip_tags($subject) ) );
              $send           =  firm_settings_send_mail( $user_details['firm_id'] , $sender_company_mailid , $email_subject , $body );


              $proposal_content=$this->Common_mdl->getTemplates(3);
              $proposal_content= $proposal_content[0];

              $body1 = html_entity_decode( $proposal_content['body'] );
              $subject = html_entity_decode($proposal_content['subject']);
      
              $random_string=$this->generateRandomString();

              $link=base_url().'Proposal_page/step_proposal/'.$random_string.'---'.$id;
              $url=base_url().'user/online_registeration/'.$id;
           
               $a1  =   array('::Client Name::'=>$name,
                '::Client Company::'=>$company_name,
                '::Client Company City::'=>$city,
                '::Client Company Country::'=>$country,
                '::Client Company Religion::'=>$religion,
                '::Client First Name::'=>$name,
                '::Client Last Name::'=>$last_name,
                '::Client Name::'=>$name,
                ':: ClientPhoneno::'=> $client_mobile_number,
                ':: ClientWebsite::'=>$client_website,
                ':: Proposal Link ::'=>$link,
                ':: ProposalCurrency::'=>$currency,
                ':: ProposalExpirationDate::'=>$expiration_date,
                ':: ProposalName::'=>$proposal_name,
                ':: ProposalCodeNumber::'=>$proposal_no,
                '::SenderCompany::'=>$sender_company,
                '::SenderCompanyAddress::'=>$sender_address,
                '::SenderCompanyCity::'=>$sender_city,
                '::SenderCompanyCountry::'=>$sender_country,
                '::SenderCompanyReligion::'=>$sender_religion,
                '::SenderEmail::'=>$sender_email,
                '::SenderEmailSignature::'=>$email_signature,
                ':: Sender Name::'=>$sender_name,
                '::senderPhoneno::'=>$sender_mobile_number,
                '::ProposalTitle::'=>$proposal_name,
                ':: Date ::'=>date("Y-m-d h:i:s"),
                '::OnlineRegistrationLink::'=>$url);

              $proposal_contents  =   strtr($body1,$a1); 
              $subject  =   strtr($subject,$a1);
              $random_string=$this->generateRandomString();
              $body= $proposal_contents;

              $random_string=$this->generateRandomString();       
                           

              $email_subject = preg_replace('/ {2,}/', ' ', str_replace( '&nbsp;', ' ', strip_tags($subject) ) );

              $send           =  firm_settings_send_mail( $user_details['firm_id'] , $email , $email_subject , $body );

         }
      if($status=='declined'){

        $data=array('proposal_id'=>$id,'tag'=>'declined','created_at'=>date('Y-m-d h:i:s'));
        $inse=$this->Common_mdl->insert('proposal_history',$data);    
      
        $data['records']=$this->Common_mdl->select_record('proposals','id',$id); 
        $pro=$this->Common_mdl->select_record('proposals','id',$id); 

        $proposal_no=$pro['proposal_no'];
        $proposal_name=$pro['proposal_name'];
        $company_name=$pro['company_name'];
        $currency=$pro['currency'];
        $expiration_date=$pro['expiration_date'];

        if($company_name==0){
          $company_name=$pro['receiver_company_name'];
        }

        $senders_details=$this->Common_mdl->select_record('user','id',$pro['user_id']);
        $sender_email=$senders_details['crm_email_id']; 
        $sender_name=$senders_details['crm_name']; 
        $sender_last_name=$senders_details['crm_last_name']; 
        $sender_country=$senders_details['crm_country'];
        $sender_city=$senders_details['crm_city'];
        $sender_religion=$senders_details['crm_state']; 

        $sender_address = "";

        if(!empty($senders_details['crm_street1']))
        { 
           $sender_address .='<p>'.$senders_details['crm_street1'].'</p>';
        }   
        if(!empty($senders_details['crm_street2']))
        { 
           $sender_address .='<p>'.$senders_details['crm_street2'].'</p>';
        } 
        if($senders_details['crm_country']!=0)
        { 
           $sender_address .='<p>'.$senders_details['crm_country'].'</p>';
        } 
        if($senders_details['crm_state']!=0)
        { 
           $sender_address .='<p>'.$senders_details['crm_state'].'</p>';
        } 
        if($senders_details['crm_city']!=0)
        { 
           $sender_address .='<p>'.$senders_details['crm_city'].'</p>';
        } 
        if($senders_details['crm_postal_code']!=0)
        { 
           $sender_address .='<p>'.$senders_details['crm_postal_code'].'</p>';
        } 

        $sender_company_details=$this->Common_mdl->select_record('client','user_id',$pro['user_id']);
       
       if($sender_company_details[0]==''){
        $sender_company=$pro['sender_company_name'];
        $sender_mobile_number='';
       }else{
        $sender_company=$sender_company_details['crm_company_name'];
        $sender_mobile_number=$sender_company_details['crm_mobile_number'];
       }

       $sender_company_mailid=$pro['sender_company_mailid'];
       $proposal_name=$pro['proposal_name'];
       $company=$pro['company_id'];
        if($company!=1){
          $company_name=$pro['company_name'];
        }else{
          $company_name=$pro['receiver_mail_id'];
        }

         $sender_details = $this->Common_mdl->select_record('proposals','id',$id);
         $sender_id=$sender_details['company_id'];  

         if($sender_id!=1){
            $user_details=$this->Common_mdl->select_record('user','id',$sender_id);
            $client_details=$this->Common_mdl->select_record('client','user_id',$sender_id);
            $client_website=$client_details['crm_company_url'];
            $client_mobile_number=$client_details['crm_mobile_number'];
            $email=$user_details['crm_email_id']; 
            $name=$user_details['crm_name']; 
            $last_name=$user_details['crm_last_name']; 
            $country=$user_details['crm_country'];
            $city=$user_details['crm_city'];
            $religion=$user_details['crm_state'];   
        }else{
           $email=$sender_details['receiver_mail_id']; 
              $client_website='';
              $client_mobile_number='';
              $name=''; 
              $last_name=''; 
              $country='';
              $city='';
              $religion=''; 
        }

        $proposal_content=$this->Common_mdl->getTemplates(7);
        $proposal_content= $proposal_content[0];

        $pdf_signature=$this->Common_mdl->select_record('pdf_signatures','proposal_no',$id);

          if($pdf_signature[0]=='')
          {
            $email_signature='';
          }
          else
          {
            $email_signature='<img src='.$pdf_signature['signature_path'].'>';
          }

          $body1 = html_entity_decode( $proposal_content['body'] );
          $subject=html_entity_decode( $proposal_content['subject'] );

          $random_string=$this->generateRandomString();
          $link=base_url().'Proposal_page/step_proposal/'.$random_string.'---'.$id;
        //  echo $link;
           $a1  =   array('::Client Name::'=>$name,
            '::Client Company::'=>$company_name,
            '::Client Company City::'=>$city,
            '::Client Company Country::'=>$country,
            '::Client Company Religion::'=>$religion,
            '::Client First Name::'=>$name,
            '::Client Last Name::'=>$last_name,
            '::Client Name::'=>$name,
            ':: ClientPhoneno::'=> $client_mobile_number,
            ':: ClientWebsite::'=>$client_website,
            ':: Proposal Link ::'=>$link,
            ':: ProposalCurrency::'=>$currency,
            ':: ProposalExpirationDate::'=>$expiration_date,
            ':: ProposalName::'=>$proposal_name,
            ':: ProposalCodeNumber::'=>$proposal_no,
            '::SenderCompany::'=>$sender_company,
            '::SenderCompanyAddress::'=>$sender_address,
            '::SenderCompanyCity::'=>$sender_city,
            '::SenderCompanyCountry::'=>$sender_country,
            '::SenderCompanyReligion::'=>$sender_religion,
            '::SenderEmail::'=>$sender_email,
            '::SenderEmailSignature::'=>$email_signature,
            ':: Sender Name::'=>$sender_name,
            '::senderPhoneno::'=>$sender_mobile_number,
            '::ProposalTitle::'=>$proposal_name,
            ':: Date ::'=>date("Y-m-d h:i:s"));

          $proposal_contents  =   strtr($body1,$a1); 
          $subject  =   strtr($subject,$a1);
          $random_string=$this->generateRandomString();
          $body= $proposal_contents;        
           
           
            $email_subject  =  preg_replace( '/ {2,}/', ' ', str_replace('&nbsp;', ' ', strip_tags($subject)) );
            $send           =  firm_settings_send_mail( $senders_details['firm_id'] , $sender_company_details , $email_subject , $body );


          $proposal_content = $this->Common_mdl->getTemplates(4);
          $proposal_content = $proposal_content[0];

          $body1   = html_entity_decode( $proposal_content['body'] );
          $subject = html_entity_decode( $proposal_content['subject'] );

          $random_string=$this->generateRandomString();
          $link=base_url().'Proposal_page/step_proposal/'.$random_string.'---'.$id;
        //  echo $link;
           $a1  =   array('::Client Name::'=>$name,
            '::Client Company::'=>$company_name,
            '::Client Company City::'=>$city,
            '::Client Company Country::'=>$country,
            '::Client Company Religion::'=>$religion,
            '::Client First Name::'=>$name,
            '::Client Last Name::'=>$last_name,            
            ':: ClientPhoneno::'=> $client_mobile_number,
            ':: ClientWebsite::'=>$client_website,
            ':: Proposal Link ::'=>$link,
            ':: ProposalCurrency::'=>$currency,
            ':: ProposalExpirationDate::'=>$expiration_date,
            ':: ProposalName::'=>$proposal_name,
            ':: ProposalCodeNumber::'=>$proposal_no,
            '::SenderCompany::'=>'invoice due date',
            '::SenderCompanyAddress::'=>$sender_address,
            '::SenderCompanyCity::'=>$sender_city,
            '::SenderCompanyCountry::'=>$sender_country,
            '::SenderCompanyReligion::'=>$sender_religion,
            '::SenderEmail::'=>$sender_email,
            '::SenderEmailSignature::'=>$email_signature,
            ':: Sender Name::'=>$sender_name,
            '::senderPhoneno::'=>$sender_mobile_number,
            ':: Date ::'=>date("Y-m-d h:i:s"));

          $proposal_contents  =   strtr($body1,$a1); 
          $subject  =   strtr($subject,$a1);
          $random_string=$this->generateRandomString();
          $body= $proposal_contents;
     
          
          $email_subject =   preg_replace('/ {2,}/', ' ', str_replace('&nbsp;', ' ', strip_tags($subject)));
          $send           =  firm_settings_send_mail( $senders_details['firm_id'] , $email , $email_subject , $body );

      }

        $data=array('proposal_id'=>$id,'tag'=>$status,'created_at'=>date('Y-m-d h:i:s'));
        $inse=$this->Common_mdl->insert('proposal_history',$data);

        $data=array('status'=>$status);
        $update=$this->Common_mdl->update('proposals',$data,'id',$id);      
     
        if($status == 'accepted')
        {
           $client = $this->Common_mdl->select_record('proposals','id',$id); 
           $res['user_id'] = $client['company_id'];
           $res['status'] = $update;
        }
        else if($status == 'declined' && $pro['lead_id'] != '0')
        {
           $res['lead_id'] = $pro['lead_id'];
           $res['status'] = $update;
        }
        else
        {
           $res['status'] = $update;
        }   
        
        echo json_encode($res);
    }

public function send_proposal(){

        $proposal_id=$_POST['proposal_id'];
        $data=array('status'=>'sent');
      $update=$this->Common_mdl->update('proposals',$data,'id',$proposal_id);


        $proposal_details=$this->db->query("select * from proposals where id='".$proposal_id."'")->row_array(); 
        $data=array('proposal_id'=>$proposal_details['id'],'tag'=>'Sent','created_at'=>date('Y-m-d h:i:s'));
        $inse=$this->Common_mdl->insert('proposal_history',$data);

        if($proposal_details['receiver_mail_id']==''){
          $emailid=$this->db->query("SELECT `crm_email` FROM `client` where `id` = '".$_POST['company_id']."'")->row_array();
            $email=$emailid['crm_email'];
          }else{
            $email=$proposal_details['receiver_mail_id'];
          }
          $from=$proposal_details['sender_company_mailid'];

        $email_subject=$proposal_details['proposal_name'].' from '.$proposal_details['sender_company_name'];
        $body1=$proposal_details['proposal_mail'];

       // $file_name= FCPATH.'/cv_uploads/'.$up_data['file_name'];
        //echo $filename;
        $random_string=$this->generateRandomString();
        $a1  =   array(base_url().'proposals/'=> base_url().'Proposal/proposal/'.$random_string.'---'.$proposal_details['id'].'');
        $content['body']         =   strtr($body1,$a1); 
        $content['attachment'][] =   FCPATH.'uploads/proposal_pdf/'.$proposal_details['pdf_file'];
        
    

        if($proposal_details['document_attachment']!=''){
          $attaching=explode(',',$proposal_details['document_attachment']);
          for($i=0;$i< count($attaching);$i++){
            $content['attachment'][] = $attaching[$i];
          }
        }


        if($proposal_details['attachment']!=''){
        $attachment=explode(',',$proposal_details['attachment']);
       
          for($i=0;$i< count($attachment);$i++){
            $content['attachment'][] = FCPATH.'attachments/'.$files;
          }         
        } 

   

        $email_subject = preg_replace('/ {2,}/', ' ', str_replace('&nbsp;', ' ', strip_tags($email_subject)));
        firm_settings_send_mail( $proposal_details['firm_id'] , $email , $email_subject , $content );

        echo json_encode($send);       
      } 
      function invoive_create($id)
      {   
         $pro=$this->Common_mdl->select_record('proposals','id',$id); 

         $invoice_insert=array('user_id'=>$pro['user_id'],'lead_id'=>$pro['lead_id'],'proposal_id'=>$pro['id'],'from_id'=>$pro['sender_company_mailid'],'to_id'=>$pro['company_id'],'item_name'=>$pro['item_name'],'qty'=>$pro['qty'],'price'=>$pro['price'],'description'=>$pro['description'],'tax'=>$pro['tax'],'discount'=>$pro['discount'],'tax_option'=>$pro['tax_option'],'discount_option'=>$pro['discount_option'],'tax_amount'=>$pro['tax_amount'],'discount_amount'=>$pro['discount_amount'],'date'=>date('d-m-Y'),'created_at'=>date('Y-m-d h:i:s'),'status'=>'draft', 'product_name'=>$pro['product_name'],
          'product_price'=>$pro['product_price'],
          'product_description'=>$pro['product_description'],
          'product_qty'=>$pro['product_qty'],
          'subscription_name'=>$pro['subscription_name'],
          'subscription_price'=>$pro['subscription_price'],
          'subscription_unit'=>$pro['subscription_unit'],
          'subscription_description'=>$pro['subscription_description'],'product_tax'=>$pro['product_tax'],
          'product_discount'=>$pro['product_discount'],
          'subscription_tax'=>$pro['subscription_tax'],
          'subscription_discount'=>$pro['subscription_discount'],'invoice_no'=>mt_rand(100000,999999),'currency'=>$pro['currency']);         

          $inserting=$this->Common_mdl->insert('invoices',$invoice_insert); 

          $da_ta=array('draft_invoice'=>$this->db->insert_id());

          $update=$this->Common_mdl->update('proposals',$da_ta,'id',$id);
      }


      public function pricelist()
      {
         $this->Security_model->chk_login();        

         $data['admin_settings']=$this->Common_mdl->select_record('admin_setting','firm_id',$_SESSION['firm_id']);
         $data['pricelist']=$this->Common_mdl->GetAllWithWhere('proposal_pricelist','firm_id',$_SESSION['firm_id']);
         $data['records']=$this->Common_mdl->select_record('pricelist_settings','firm_id',$_SESSION['firm_id']);
         
         $this->load->view('proposal/price_list_settings',$data);
      }

      public function insert_pricelist()
      {
         $data=array('tax_option'=>$_POST['tax'],'discount_option'=>$_POST['discount'],'currency'=>$_POST['currency_symbol'],'pricelist'=>$_POST['pricelist'],'user_id'=>$_SESSION['id'],'firm_id' => $_SESSION['firm_id']);       

         $insert=$this->Common_mdl->insert('pricelist_settings',$data);

         if($insert!='0')
         {
             $datas['log'] = "Added pricelist settings.";
             $datas['createdTime'] = time();
             $datas['module'] = 'Proposal';
             $datas['user_id'] = $_SESSION['id'];
             $this->Common_mdl->insert('activity_log',$datas);
         }

         redirect('proposals/pricelist');
      }

      public function update_pricelist()
      {
         $data=array('tax_option'=>$_POST['tax'],'discount_option'=>$_POST['discount'],'currency'=>$_POST['currency_symbol'],'pricelist'=>$_POST['pricelist'],'user_id'=>$_SESSION['id']);       

         $insert=$this->Common_mdl->update('pricelist_settings',$data,'id',$_POST['id']);

         if($insert!='0')
         {
             $datas['log'] = "Updated pricelist settings.";
             $datas['createdTime'] = time();
             $datas['module'] = 'Proposal';
             $datas['user_id'] = $_SESSION['id'];
             $this->Common_mdl->insert('activity_log',$datas);
         }

         redirect('proposals/pricelist');
      }


      public function check(){ 
        $this->load->view('proposal/Form_submit');
      }


      
  


    
}
?>