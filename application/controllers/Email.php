<?php
error_reporting(E_ERROR);
class Email extends CI_Controller {
public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
		public function __construct(){
			parent::__construct();
			$this->load->model(array('Common_mdl','Email_model','Security_model','Firm_setting_mdl'));
           $this->load->library('Excel');
		}

		public function index(){
			$this->Security_model->chk_login();
			$email_id=$this->Common_mdl->select_record('user','id',$_SESSION['id']);
			$emails=$email_id['crm_email_id'];
			//echo $emails;
			$data['inbox_content']=$this->db->query("select * from email_section where FIND_IN_SET('".$emails."', bcc_id) or FIND_IN_SET('".$emails."', email_id) or FIND_IN_SET('".$emails."', cc_id) or user_id='".$_SESSION['id']."'")->result_array();
			$data['sendbox_content']=$this->db->query("select * from email_section where FIND_IN_SET('".$emails."', from_id)")->result_array();
			$this->load->view('email/email',$data);
		}

		public function welcome_email(){
			$this->Security_model->chk_login();
			$this->load->view('email/welcome-email');
		}

		public function compose($timeline_ser_id=false){
			$this->Security_model->chk_login();

			$data['email_list']=$this->db->query("select crm_email_id from user where crm_email_id!=''")->result_array();
			if($timeline_ser_id!='')
			{
				$get_notes_from_timeline=$this->db->query("select * from timeline_services_notes_added where id=".$timeline_ser_id." ")->row_array();
				$notes=$get_notes_from_timeline['notes'];
				$user_id=$get_notes_from_timeline['user_id'];
				$user_mail_id=$this->Common_mdl->get_client_contact_primary_email($user_id);
				$data['notes']=$notes;
				$data['user_mail_id']=$user_mail_id;
				$this->load->view('email/email-compose',$data);
			}
			else
			{

				$this->load->view('email/email-compose',$data);
			}
			//$this->load->view('email/email-compose');
		}

		public function read(){
			$this->Security_model->chk_login();
			$this->load->view('email/email-read');
		}

		public function client_emails(){
			$email=explode(',',$_POST['email']);
		//print_r($email);
			$data['content']='';
			// for($i=0;$i<count($email);$i++){
			// }
			
			$query=$this->db->query("select crm_email_id from user where crm_email_id like '".end($email)."%'")->result_array();
			
			foreach($query as $datas){
				$data['content'].='<li class="client_data" id="'.$datas['crm_email_id'].'" onclick="getemail(this)">'.$datas['crm_email_id'].'</li>';
			}
		
			echo json_encode($data);
			
		}
		public function send_email(){
			// echo "<pre>";
			// print_r($_POST);
		    $email_to=implode(',',$_POST['email_to']);
			$email_cc=implode(',',$_POST['email_cc']);
			$email_bcc=implode(',',$_POST['email_bcc']);


			$email_subject=$_POST['mail_subject'];
			$mail_data['notes_section']=$_POST['notes_section'];

			$email_id=$this->Common_mdl->select_record('user','id',$_SESSION['id']);
			$emails=$email_id['crm_email_id'];

			$data=array('email_id'=>implode(',',$_POST['email_to']),'cc_id'=>implode(',',$_POST['email_cc']),'bcc_id'=>implode(',',$_POST['email_bcc']),'email_subject'=>$_POST['mail_subject'],'email_content'=>$_POST['notes_section'],'status'=>'send','user_id'=>$_SESSION['id'],'from_id'=>$emails);

			$insert=$this->Common_mdl->insert('email_section',$data);


			  $body = $this->load->view('empty_mail_content.php', $mail_data, TRUE);
			  $this->load->library('email');
		      $this->email->set_mailtype('html');
		      $this->email->from('info@remindoo.org');
		      $this->email->to($email_to);
		      if($email_cc!=''){
		      	$this->email->cc($email_cc);
		      }
		      if($email_bcc!='')
		      {
		      	$this->email->bcc($email_bcc);
		      }
		      $this->email->subject($email_subject);
		      $this->email->message($body);
		      $send = $this->email->send();
			if($send)
			{
				echo "send";
			}
			else
			{
				echo "not";
			}
		}

		public function Verify($firm_id)
		{
			$firm = $this->Common_mdl->GetAllWithWhere('firm','firm_id',$firm_id);

            if($firm[0]['status'] == '1')
            {
               $message = "<p>Your Account Has Already Been Verified.</p><p><a href='".base_url()."'>Click here to login.</a></p>";
            }
            else
            {               
			   $update = $this->db->update('firm',['status'=>1],"firm_id='".$firm_id."'");

			   if($update == true)
			   {
                  $message = "<p style='color:green;'>Your Account Has Been Verified.</p><p>Check Your Registered Email For Logging In Link.</p>";
                  
                  $this->Firm_setting_mdl->send_welcome_mail($firm_id);
			   }
			   else
			   {
                   $message = '<p style="color:red;">Error Occurred. Try Later.</p>';
			   }
		    }

		    $data['message'] = $message;	

		    $this->load->view('Firm/email_verification',$data);	
		}

}
?>
