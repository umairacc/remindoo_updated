
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Cron extends CI_Controller{   
  public function __construct(){
    parent::__construct();
    require_once(APPPATH.'controllers/AwsMailer.php');
    require_once(APPPATH.'controllers/NotificationStatus.php');
    $this->load->model([
      'Service_Reminder_Model',
      'Service_model',
      'Task_section_model',
      'Common_mdl',
      'Security_model',
      'Invoice_model',
      'Task_invoice_model',
      'Task_creating_model'
    ]);
    $this->load->helper([
      'download',
      'template_decode'
    ]);
  }

  public function testfunction(){
    echo '<br/>TEST Cron running - OK';
    // mail('iitb.riteshag@gmail.com', 'Test email from Remindoo', 'Test email from Remindoo');
  }

  /*For Service Reminder */
  public function index(){
    error_reporting(-1);
    echo "inside Cron/index()\n";
    
    $this->db->insert( "test_records" , [ 'value' => date('d-m-Y H:i:s') ] );
    
    $time  = date('h:i A');
    $query = "SELECT FS.* 
              FROM admin_setting as FS 
              INNER JOIN firm as F 
                      ON F.firm_id=FS.firm_id 
                     AND F.is_deleted!=1 
              WHERE FS.firm_id!='' 
              AND  FS.client_reminder_time='".$time."'";



    $firm_Settings = $this->db->query( $query )->result_array();

    foreach ( $firm_Settings as $key => $value ){
      $data = $this->Service_Reminder_Model->Get_Sending_Data( $value );

      if($data==1){
        $this->Service_Reminder_Model->firm_exc_sr_log   = json_encode( $value )."\n";
        $this->Service_Reminder_Model->today_dues_recurring( $value );
        $firm_log = $this->Service_Reminder_Model->firm_exc_sr_log;
        $log_data = [ 
          'date'    =>  date('Y-m-d'),
          'firm_id' =>  $value['firm_id'] ,
          'type'    =>  "service_reminder",
          'logs'    =>   $firm_log
        ];
        $this->db->insert( 'schedule_execution_log' , $log_data );
        $this->Service_Reminder_Model->firm_exc_sr_log = "";
      }
    }

    $this->send_queue_mails();
    
  }


  public function send_birthday_reminders(){
    $current_date = date('d-m-Y');
    // $current_date = '02-08-1989';
    $query = "SELECT * 
              FROM client_contacts 
              WHERE date_of_birth ='".$current_date."'
               AND send_birthday_reminders = 1";
    $client_contacts = $this->db->query( $query )->result_array();

    foreach($client_contacts as $client_contact){

        // CONSTANT
        $birthday_email_template = BIRTHDAY_EMAIL_TEMPLATE_ID;
        $contact_id = $client_contact['id'];
				$query    = "SELECT c.id,cc.main_email,work_email FROM client_contacts as cc 
								INNER JOIN client as c on c.user_id=cc.client_id
									WHERE cc.id=" . $contact_id;
				$client = $this->db->query($query)->row_array();
				$all_emails = [];
				$all_emails[] = $client["main_email"];
				if( !empty( $client['work_email'] ) )
				{
					$work_email = json_decode( $client['work_email'] , true );
					$all_emails = array_merge( $all_emails , $work_email );
				}	
				$refined_emails = [];
				$refined_emails = $all_emails;
				$email_temp = $this->db->query("SELECT * FROM `email_templates` WHERE `id` = ".$birthday_email_template)->row_array();
				$decode['subject'] = html_entity_decode($email_temp['subject']);
				$decode['body']    = html_entity_decode($email_temp['body']);
				$decoded           = Decode_ClientTable_Datas($client['id'], $decode);
				$send_to['to']	= 	$refined_emails;
				$send_to['cc']	= 	'';
				$send_to['bcc']	= 	'';
				$data = [
					'send_from'             =>  $_SESSION['firm_id'],
					'send_to'               =>  json_encode($send_to),
					'subject'               =>  $decoded['subject'],
					'content'               =>  $decoded['body'],
					'status'                =>  1,
					'relational_module'     =>  'client',
					'relational_module_id'  =>  $client['id'],
					'created_at'            =>  date('Y-m-d H:i:s')
				];

				$this->db->insert( "email" , $data );
				
				$final_emails = [];
				if(is_array($refined_emails)){
					foreach($refined_emails as $r_emails){
						$final_emails['to'][] = $r_emails;
					}
				}
				else{
					$final_emails = $refined_emails;
				}

				$Issend = firm_settings_send_mail($_SESSION['firm_id'], $final_emails, $decoded['subject'], $decoded['body']);

				if ($Issend['result'] == 1) {
					$log = strip_tags($decoded['subject']) . "- Email send";
					$user_id = $this->Common_mdl->get_field_value('client', 'user_id', 'id', $client['id']);
					$this->Common_mdl->Create_Log('client', $user_id, $log);
				}
    }
  }


    public function index_test(){
    error_reporting(-1);
    echo "inside Cron/index()\n";
    
    $this->db->insert( "test_records" , [ 'value' => date('d-m-Y H:i:s') ] );
    
    $time  = date('h:i A');
    // $query = "SELECT FS.* 
    //           FROM admin_setting as FS 
    //           INNER JOIN firm as F 
    //                   ON F.firm_id=FS.firm_id 
    //                  AND F.is_deleted!=1 
    //           WHERE FS.firm_id!='' 
    //           AND  FS.client_reminder_time='".$time."'";

    $query = "SELECT FS.* 
              FROM admin_setting as FS 
              INNER JOIN firm as F 
                      ON F.firm_id=FS.firm_id 
                     AND F.is_deleted!=1 
              WHERE FS.firm_id!='' 
              AND  FS.client_reminder_time='01:00 AM'";


    $firm_Settings = $this->db->query( $query )->result_array();

    foreach ( $firm_Settings as $key => $value ){
      $data = $this->Service_Reminder_Model->Get_Sending_Data_test( $value );

      if($data==1){
        $this->Service_Reminder_Model->firm_exc_sr_log   = json_encode( $value )."\n";
        $this->Service_Reminder_Model->today_dues_recurring( $value );
        $firm_log = $this->Service_Reminder_Model->firm_exc_sr_log;
        $log_data = [ 
          'date'    =>  date('Y-m-d'),
          'firm_id' =>  $value['firm_id'] ,
          'type'    =>  "service_reminder",
          'logs'    =>   $firm_log
        ];
        $this->db->insert( 'schedule_execution_log' , $log_data );
        $this->Service_Reminder_Model->firm_exc_sr_log = "";
      }
    }

    $this->send_queue_mails();
    
  }

  public function send_queue_mails(){
    echo "\n===================================\n";
    echo "inside send_queue_mails function\n";

    $this->load->library('phpmailer_lib');

    // $date     = date('Y-m-d');
    $date     = '2021-01-08';
    $firm_ids = $this->db
                  ->select("DISTINCT(send_from) as firm_id")
                  ->where("date(created_at) > '".$date."' AND status = 0")                  
                  ->get("email")
                  ->result_array(); 
    
    $aws_mailer = new AwsMailer();
        
    foreach ( $firm_ids as $key => $firm_id_arr ){
      $firm_id = $firm_id_arr['firm_id'];
      echo $firm_id." has queue mails\n";

      $emails = $this->db
                  ->where( "date(created_at) > '".$date."'  AND status = 0 AND send_from=".$firm_id )
                  ->limit(50)
                  ->get( "email" )
                  ->result_array();


      $emails = array_reverse($emails);
      //$mail   = $this->phpmailer_lib->Setup_config( $firm_id , 4 , true );    

      foreach ($emails as $key => $emails_data ){


        echo "Inside Email sending loop = ".$emails_data['id'];

        
        $emailData    = [
          'email_to'      => $emails_data['send_to'],
          'email_subject' => $emails_data['subject'],
          'email_body'    => $emails_data['content']            
        ];
        
        $IsSend       = $aws_mailer->prepare_email_to_send($emailData, false);
        $update_data  = [
          'smtp_response' => json_encode( $IsSend ),
          'status'        => ($IsSend['result'] == 1 || $IsSend['result'] == '1') ? 1 : 2
        ];
        if($this->db->update( 'email' , $update_data ,'id='.$emails_data['id'] ))
        {
          $emails_data['status'] = 1;
        }

        if( $emails_data['relational_module']  == 'service_reminder' ){
          $this->Service_Reminder_Model->update_reminder_status( $emails_data );            
        }
        echo $emails_data['id']." this queue result=".$IsSend['result']."\n"; 

      }
    }
  }

  
  /*For Company House Sync*/
  public function CH_auto_update(){
    $result = $this->Common_mdl->GetActiveFirm_Clients();      
    
    echo "<br/><pre>CH==================================================".date('d-m-Y')."=====================================================================CH";
    //print_r( $result );

    $this->load->model('CompanyHouse_Model');
    $this->load->model('Client_model');

    foreach ($result as $value){
      $new_datas = $this->CompanyHouse_Model->Find_NewData_Updates( $value['id'] );  
      if ($new_datas){
        echo "Firm ID = ".$value['id']." ".print_r( $new_datas, true );        
      }
      
      if( !empty( $new_datas ) ){
        $return =  $this->Client_model->Update_CH_Datas( $value['id'] , $new_datas );
        
        if( $return ){
          $fields = array_map(
                      function( $val ){ 
                        $res = preg_replace( ['/crm_/','/_/'],['',' '], $val );
                        return ucwords( $res );
                      },
                      array_keys( $new_datas ) 
                    );
        
          $client_name  = $this->Common_mdl->getcompany_name( $value['id'] );        
          $content      = "Data updated from CH -".$client_name."<br>";
          $content     .= "<p>".implode("</p><p>", $fields)."</p>";
        
          echo $content;
        
          $data = array(  
                'module'              =>  'client',
                'notification_type'   =>  'CH',
                'sender_id'           =>  $value['id'],     
                'status'              =>  1,
                'content'             =>  $content,
                'Createdtime'         =>  time()
          );
          $this->db->insert( "notification_management" , $data );
        }
      }
    }
    echo "<br/><pre>====";
  }

  public function sent_repeat_invoice(){
    $today        = date('Y-m-d');
    $invoice_date = $this->db->query("select * from repeatInvoiceDate_details where date(`invoice_startdate`)='$today' and status!=1")->result_array();      

    foreach($invoice_date as $repeat_invoice){
      $details   = $this->db->query("select rid.*,u.firm_id from repeatInvoice_details as rid inner join user u on u.id=rid.client_email and u.status NOT IN(2,0) where rid.client_id=".$repeat_invoice['client_id'])->row_array();
      $user_mail = $this->db->query("select main_email from client_contacts  where client_id=".$details['client_email']." and make_primary=1")->row_array();

      if(count($details)){
        $product_details           = $this->db->query("select * from repeatproducts_details where client_id = ".$details['client_id']." ")->result_array();
        $amount_details            = $this->db->query("select * from repeatamount_details where client_id = ".$details['client_id']." ")->row_array();                  
        $data['grand_total']       = $amount_details['grand_total'];
        $data['invoice_date']      = $repeat_invoice['invoice_startdate'];
        $data['invoice_startdate'] = $repeat_invoice['invoice_startdate'];
        $data['description']       = array_column($product_details, 'description');  
        $data['quantity']          = array_column($product_details, 'quantity');
        $data['unit_price']        = array_column($product_details, 'unit_price');
        $data['amount_gbp']        = array_column($product_details, 'amount_gbp'); 
        $data['sub_tot']           = $amount_details['sub_total'];        
        $body                      = $this->load->view('Invoice/invoice_remainder_email.php', $data, TRUE);        
        $IsSend                    = firm_settings_send_mail( $details['firm_id'] , $user_mail['main_email'] , 'Invoice' , $body );
        
        $this->db->query("update repeatInvoiceDate_details set status=1 where id=".$repeat_invoice['id']."");
      }
    }
  }
  
  public function reports_schedule(){
    $client = $this->db->query('select * from reports_schedule')->result_array();
    
    foreach ($client as $key => $value) {
      $this->Report_model->Client_reports($value['id']);
    }
  }

  public function show_info( $id ){
    $this->load->model('CompanyHouse_Model');
    $new_datas = $this->CompanyHouse_Model->Find_NewData_Updates( $id );  
    print_r( $new_datas );
  }

  public function fix_task_recurring_issue($firm_id){ 
    $client_dues = $this->db->query("SELECT * FROM `client_service_frequency_due` WHERE is_create_task = 1 and firm_id=".$firm_id)->result_array();
    $count       = 0;

    foreach ( $client_dues as $key => $client_due ) {      
      echo "<br/><pre>client due details\n";
      print_r( $client_due );

      $reminders        = $this->db->query( "SELECT `date` FROM `service_reminder_cron_data` WHERE client_id=".$client_due['client_id']." AND service_id=".$client_due['service_id'] )->result_array();
      $reminders        = array_column( $reminders , 'date' );
      $reminders['due'] = $client_due['date'];
      $reminders        = array_map(function($v){  return date_format( date_create_from_format( 'd-m-Y' , $v ) , 'Y-m-d');  }, $reminders );
    
      echo "\nreminder dates\n";
      print_r( $reminders );

      $reminders = "'".implode( "','" , $reminders )."'";
      echo "\nreminder dates str".$reminders."\n";

      $task = $this->db->query("SELECT id,start_date,subject FROM add_new_task WHERE related_to='service_reminder' AND company_name=".$client_due['client_id']." AND related_to_services=".$client_due['service_id']." AND start_date IN(".$reminders.")")->result_array();
    
      echo "\n task_details\n";
      print_r( $task );
    
      if( empty($task) ){
        $this->db->query( "UPDATE client_service_frequency_due set is_create_task=0 where id =".$client_due['id'] );
        $count++;
      }      
    }
    echo $count;
  }

  public function send_test_mail(){ 
    echo "inside function<pre>";
    $body     = 'test';
    $subject  = 'test';

    $this->load->library('phpmailer_lib');
    $mail  =  $this->phpmailer_lib->Setup_config( 270 , 4 , true );

    for( $i = 1 ; $i < 200 ; $i++ ){ 
      echo "\n============".$i."=================\n";      
      
      if($i == 70 || $i== 140 ){
        sleep(120);
        $mail  =  $this->phpmailer_lib->Setup_config( 270 , 4 , true );
      }

      $this->phpmailer_lib->AddRecipient( $mail , "ajith@techleafsolutions.in" );     
      $this->phpmailer_lib->AddSubjectBody( $mail , $subject."==".$i , $body."==".$i );
      
      $IsSend = $this->phpmailer_lib->Send( $mail );
      print_r( $IsSend );
      echo "\n============".$i."=================\n";
    }
  } 

  // public function revert_future_queue_reminder_statuses()
  // {
  //   ini_set('max_execution_time', '0');
  //   $query = 'Select * from add_new_task';
  //   $task_datas = $this->db->query( $query )->result_array();
  //   $id_str = '';
      
  //     foreach ($task_datas as $key => $task_data) {
  //       $this->db->where( 'firm_id' , $task_data['firm_id'] );
  //       $settings = $this->db->get('admin_setting')->row_array();
  //       if( !empty( $settings['reminder_stop_status'] ) )
  //       {
  //         $task_data['company_name'] = intval($task_data['company_name']);
  //         $task_data['related_to_services'] = intval($task_data['related_to_services']);

  //         $where = [
  //             'client_id'       => $task_data['company_name'],
  //             'client_contacts_id'  => $task_data['client_contacts_id'],
  //             'service_id'      => $task_data['related_to_services']
  //           ];
            
  //           $client_service_frequency_due  = $this->db->select( 'frequency' )
  //                                  ->where( $where )
  //                                  ->get( 'client_service_frequency_due' )  
  //                                  ->row();

  //           $frequency = $client_service_frequency_due->frequency;

  //           $check_days = 15;

  //           if($frequency == '1 month')
  //           {
  //             $check_days = 15;
  //           }
  //           elseif($frequency == '3 Months')
  //           {
  //             $check_days = 75;
  //           }
  //           elseif($frequency == '1 Year')
  //           {
  //             $check_days = 350;
  //           }

  //           $start_date = $task_data['start_date'];
  //           $end_date = date('Y-m-d', strtotime($start_date. ' + '.$check_days.' days'));
  //           $timestamp = strtotime($start_date);
  //           $start_date = date("d-m-Y", $timestamp);
  //           $timestamp = strtotime($end_date);
  //           $end_date = date("d-m-Y", $timestamp);
          
  //           $stop_status = json_decode( $settings['reminder_stop_status'] , true );
  //           if( is_array( $stop_status ) && (!in_array( $task_data['progress_status'] , $stop_status )) )
  //           {  

  //             $query1 = "SELECT id from service_reminder_cron_data WHERE client_id = ".$task_data['company_name']." AND client_contacts_id=".$task_data['client_contacts_id']." AND service_id=".$task_data['related_to_services']." AND UNIX_TIMESTAMP(STR_TO_DATE(  `date` ,'%d-%m-%Y' ) ) > UNIX_TIMESTAMP(STR_TO_DATE(  '".$end_date."' ,'%d-%m-%Y' ))";
  //             // $query1 = "UPDATE service_reminder_cron_data set status=0 WHERE client_id = ".$task_data['company_name']." AND client_contacts_id=".$task_data['client_contacts_id']." AND service_id=".$task_data['related_to_services']." AND UNIX_TIMESTAMP(STR_TO_DATE(  `date` ,'%d-%m-%Y' ) ) > UNIX_TIMESTAMP(STR_TO_DATE(  '".$end_date."' ,'%d-%m-%Y' ))";

  //            $id_row = $this->db->query( $query1 )->row();
  //            $id_str .= $id_row->id.',';
  //           }

  //         }
  //   }
  //   echo 'done..\n';
  //   echo $id_str;
  // }

  public function revert_future_queue_reminder_statuses()
  {
    ini_set('max_execution_time', '0');
    // $current_date = date('d-m-Y');
    $current_date = '01-01-2021';
    $query = "SELECT a.*,b.service_name,c.crm_company_name,UNIX_TIMESTAMP(STR_TO_DATE(  `date` ,'%d-%m-%Y' ) ) as date  FROM `service_reminder_cron_data` AS a INNER JOIN service_lists AS b on b.id = a.service_id INNER JOIN client AS c on c.id = a.client_id  WHERE UNIX_TIMESTAMP(STR_TO_DATE(  `date` ,'%d-%m-%Y' ) ) >= UNIX_TIMESTAMP(STR_TO_DATE(  '".$current_date."' ,'%d-%m-%Y' )) AND a.status = 2";


    // $query = "SELECT * from service_reminder_cron_data WHERE UNIX_TIMESTAMP(STR_TO_DATE(  `date` ,'%d-%m-%Y' ) ) > UNIX_TIMESTAMP(STR_TO_DATE(  '".$current_date."' ,'%d-%m-%Y' )) AND status = 2";
    $queue_reminders = $this->db->query( $query )->result_array();
    $reason_arr = [];
    foreach($queue_reminders as $reminder)
    {
      $reminder['client_id'] = intval($reminder['client_id']);
      $reminder['service_id'] = intval($reminder['service_id']);
      $where = [
              'client_id'       => $reminder['company_name'],
              'client_contacts_id'  => $reminder['client_contacts_id'],
              'service_id'      => $reminder['service_id']
            ];

      $reminder_date = date('Y-m-d' , strtotime($reminder['date']));

      $client_service_frequency_due  = $this->db->select( 'frequency' )
                                   ->where( $where )
                                   ->get( 'client_service_frequency_due' )  
                                   ->row();

      $frequency = $client_service_frequency_due->frequency;

      $check_days = 5;

      if($frequency == '1 month')
      {
        $check_days = 25;
      }
      elseif($frequency == '3 Months')
      {
        $check_days = 85;
      }
      elseif($frequency == '1 Year')
      {
        $check_days = 360;
      }
      elseif($frequency == '7 days')
      {
        $check_days = 5;
      }

      $start_date = date('Y-m-d', strtotime($start_date. ' - '.$check_days.' days'));

      $change_status_flag = 0;
      $change_status_reason = [];

      $query1 = "SELECT * from add_new_task WHERE company_name = '".$reminder['client_id']."' AND client_contacts_id=".$reminder['client_contacts_id']." AND related_to_services='".$reminder['service_id']."' AND (UNIX_TIMESTAMP(STR_TO_DATE(  `service_due_date` ,'%Y-%m-%d' ) ) BETWEEN UNIX_TIMESTAMP(STR_TO_DATE(  '".$start_date."' ,'%Y-%m-%d' )) AND UNIX_TIMESTAMP(STR_TO_DATE(  '".$reminder_date."' ,'%Y-%m-%d' )))";

      $task_row = $this->db->query( $query1 )->row();

      if(!$task_row)
      {
        $change_status_flag = 1;
        $change_status_reason['reason'] = 'No task found';
      }
      else
      {
        $this->db->where( 'firm_id' , $task_row->firm_id );
        $settings = $this->db->get('admin_setting')->row_array();
        if( !empty( $settings['reminder_stop_status'] ) )
        {
          $stop_status = json_decode( $settings['reminder_stop_status'] , true );
            if( is_array( $stop_status ) && (!in_array( $task_row->progress_status , $stop_status )) )
            { 
              $change_status_flag = 1;
              $change_status_reason['reason'] = 'Not in stop status';
              $change_status_reason['task_id'] = $task_row->id;
              $change_status_reason['task_subject'] = $task_row->subject;
            }
        }
      }

      if($change_status_flag == 1)
      {
        
        $this->db->update( 'service_reminder_cron_data' , [ 'status'=> 0 ] , "id=".$reminder['id'] );
        $change_status_reason['client_name'] = $reminder['crm_company_name'];
        $change_status_reason['service_name'] = $reminder['service_name'];
        $reason_arr[$reminder['id']] = $change_status_reason;
      }
    }
    echo '<pre>';
    print_r($reason_arr);
    exit;
  }

  public function adding_task_original_start_date()
  {
    ini_set('max_execution_time', '0');
    $query1 = "SELECT * from add_new_task WHERE related_to = 'service_reminder'";
    $task_array = $this->db->query( $query1 )->result_array();
    $result = [];
    foreach ($task_array as $key => $task) {
      $subject = $task['subject'];
      $temp_subject = $subject;
      $temp_subject = str_replace(' ', '-', $temp_subject);
      $rev_subject = strrev($temp_subject);
      $rev_subject_arr = explode('-', $rev_subject);
      if(isset($rev_subject_arr[2])&&isset($rev_subject_arr[1])&&isset($rev_subject_arr[0])&&is_numeric($rev_subject_arr[2])&&is_numeric($rev_subject_arr[1])&&is_numeric($rev_subject_arr[0]))
      {
        $task_actual_date = strrev($rev_subject_arr[0]).'-'.strrev($rev_subject_arr[1]).'-'.strrev($rev_subject_arr[2]);
      }
      else
      {
        $task_actual_date = $task['start_date'];
      }

      $this->db->update( 'add_new_task' , [ 'service_due_date'=> $task_actual_date ] , "id=".$task['id'] );

      $result[$task_actual_date] = $subject;

    }
    echo '<pre>';
    print_r($result);
    exit;
  }

  public function stop_overtimers(){
    stop_overtimes();
  }

  public function add_send_emails_to_tasks()
  {
    ini_set('max_execution_time', '0');
    $query1 = "SELECT * from add_new_task WHERE related_to = 'service_reminder'";
    $task_array = $this->db->query( $query1 )->result_array();
    $result = [];
    echo '<pre>';
    foreach ($task_array as $key => $task) {
      $client_service_contact_cond  = [
                        'client_id'     =>  $task['company_name'],
                        'service_id'    =>  $task['related_to_services'],
                        'client_contacts_id'=>  $task['client_contacts_id']
                      ];

      $due_data = $this->db->get_where( "client_service_frequency_due" , $client_service_contact_cond )->row_array();
      if($due_data)
      {
        // echo '<pre>';
        // print_r($due_data);
        // exit;
        $query1 ="SELECT *  FROM `reminder_setting` 
              WHERE custom_reminder=0 AND firm_id IN (0,".$due_data['firm_id'].") AND service_id=".$due_data['service_id']." AND 
                id NOT IN ( SELECT super_admin_owned from reminder_setting WHERE firm_id=".$due_data['firm_id']." ) 
                ORDER BY due,days DESC";

        $count_reminders = count($this->db->query( $query1 )->result_array());

        if($count_reminders)
        {
          $check_days = 5;

          if($due_data['frequency'] == '1 month')
          {
            $check_days = 15;
          }
          elseif($due_data['frequency'] == '3 Months')
          {
            $check_days = 75;
          }
          elseif($due_data['frequency'] == '1 Year')
          {
            $check_days = 350;
          }
          elseif($due_data['frequency'] == '7 days')
          {
            $check_days = 5;
          }

          $task_date = $task['service_due_date'];

          $task_due_date = date("d-m-Y", strtotime($task_date));

          $starting_reminder_date = date('d-m-Y', strtotime($task_date. ' - '.$check_days.' days'));

          $reminders = $this->db->where( "UNIX_TIMESTAMP(STR_TO_DATE(  `date` ,'%d-%m-%Y' ) ) > UNIX_TIMESTAMP(STR_TO_DATE(  '".$starting_reminder_date."' ,'%d-%m-%Y' )) AND UNIX_TIMESTAMP(STR_TO_DATE(  `date` ,'%d-%m-%Y' ) ) <= UNIX_TIMESTAMP(STR_TO_DATE(  '".$task_due_date."' ,'%d-%m-%Y' )) AND firm_id=".$due_data['firm_id'].' AND status = 1 AND client_id = '.$task['company_name'].' AND client_contacts_id = '.$task['client_contacts_id'].' AND service_id = '.$task['related_to_services'])
                            ->get( "service_reminder_cron_data" )
                            ->result_array();

          if($task['company_name'] == 1784)
          {
            
            print_r($reminders);
            
          }

          $count_sent_reminders = 0;
          if($reminders)
          {
            $count_sent_reminders = count($reminders);
            // echo '<pre>';
            // print_r($reminders);
            // exit;
          }     

          $query1 = "UPDATE add_new_task SET column1 = value1, column2 = value2 WHERE id = 'service_reminder'";
          $this->db->update( 'add_new_task' , [ 'email_count'=> $count_reminders, 'sent_email_count'=> $count_sent_reminders ] , "id=".$task['id'] );
        }
        
      }
    }
    exit;
  }

} ?>
