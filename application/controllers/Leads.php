<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Leads extends CI_Controller
{
	public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";

	public $lead;

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 *      http://example.com/index.php/welcome
	 *  - or -
	 *      http://example.com/index.php/welcome/index
	 *  - or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
		parent::__construct();
		$this->load->model(array('Demo_model', 'Common_mdl', 'Security_model', 'Task_invoice_model', 'CompanyHouse_Model'));
	}

	public function index($assigned = null){
		error_reporting(0);
		$this->Security_model->chk_login();

		$per_count = $this->Common_mdl->permission_count($_SESSION['permission']['Leads']);
		
		if (isset($per_count[1]) && $per_count[1] >= 1) {
			$it_session_id 	= $_SESSION['id'];
			$res2 			= array();
			if($assigned){
				$assigned = $_SESSION['userId'];
			}
			$ques 			= Get_Assigned_Datas('LEADS',$assigned);
			$res 			= (!empty($ques)) ? implode(',', array_filter(array_unique($ques))) : '-1';
					
			if ($res != '') {
				$data['leads'] = $this->db->query(
									"SELECT * 
									FROM leads li 
									WHERE li.id IN ($res) 
									AND li.firm_id=" . $_SESSION['firm_id'] . "  
									UNION
									SELECT * 
									FROM leads lis 
									WHERE lis.id IN($res) 
									AND public='on' 
									ORDER BY id DESC "
								)->result_array();
			
			} else {
				$data['leads'] = '';
			}
			$data['leads_status'] 	= $this->Common_mdl->dynamic_status('leads_status');
			$data['source'] 		= $this->Common_mdl->dynamic_status('source');
			$data['countries'] 		= $this->Common_mdl->getallrecords('countries');
			$data['Language'] 		= $this->Common_mdl->getallrecords('languages');
			$data['latestrec'] 		= $this->Common_mdl->orderby('activity_log', 'module', 'Lead', 'desc');
			$data['user_list'] 		= $this->db->query(
										"select * 
										from user 
										where firm_id=" . $_SESSION['firm_id'] . " 
										and user_type !='FC' 
										and user_type !='SA'"
									)->result_array();	
			$data["email_temp"]     = $this->Common_mdl->getTemplates();
			$this->load->view('leads/leads_view', $data);
		} else {
			$this->load->view('users/blank_page');
		}
	}

	function assigned() {
        $this->index('assigned');
    }

	public function leads_detailed_tab($id)
	{
		// print_r($_SESSION['permission']['Leads']);
		//   exit;
		$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$this->session->set_userdata('mail_url', $actual_link);
		$this->Security_model->chk_login();
		$this->session->unset_userdata($_SESSION['mail_url']);
		//$data['leads']=$this->db->select_record('leads','id',$id);
		$data['leads'] = $this->db->query("SELECT * FROM leads li where li.id =$id and li.firm_id=" . $_SESSION['firm_id'] . "  order by id desc")->row_array();



		$data['leads_status'] = $this->Common_mdl->dynamic_status('leads_status');

		// $data['leads_status']=$this->Common_mdl->GetAllWithWhere('leads_status','firm_id', $_SESSION['firm_id']);
		$data['source'] = $this->Common_mdl->dynamic_status('source');
		$data['countries'] = $this->Common_mdl->getallrecords('countries');
		$data['Language'] = $this->Common_mdl->getallrecords('languages');
		// $data['staffs'] = $this->Common_mdl->GetAllWithWhere('user','role','6');
		//  $data['manager'] = $this->Common_mdl->GetAllWithWhere('user','role','5');
		$data['proposal'] = $this->Common_mdl->GetAllWithWhere('proposals', 'lead_id', $id); //for leads proposal

		$data['latestrec'] = $this->Common_mdl->orderby('activity_log', 'module', 'Lead', 'desc');
		$data['task_status'] = $this->db->query('SELECT * from task_status where is_superadmin_owned=1')->result_array();
		$data['assign_check'] = $this->Common_mdl->assign_check($id);

		if ($_SESSION['permission']['Leads']['view'] == 1) {
			$this->load->view('leads/leads_detailed_tabs', $data);
		} else {
			$this->load->view('users/blank_page');
		}
	}
	public  function delete_leads()
	{
		if (isset($_POST['ids'])) {
			$ids = json_decode($_POST['ids'], true);
			foreach ($ids as $id) {

				$this->Common_mdl->delete('leads', 'id', $id);
				/** for delete leads 22-09-2018**/
				$this->Common_mdl->delete('lead_reminder', 'lead_id', $id);
				/** for delete leads reminder 22-09-2018 **/
				$activity_datas['log'] = "Delete Lead";
				$activity_datas['createdTime'] = time();
				$activity_datas['module'] = 'Lead';
				$activity_datas['user_id'] = $_SESSION['admin_id'];
				$activity_datas['module_id'] = $id;
				$this->Common_mdl->insert('activity_log', $activity_datas);
				$this->Common_mdl->assignees_delete('LEADS', $id);
			}
		}
		echo $this->db->affected_rows();


		/* $this->session->set_flashdata('success',"Lead Deleted Successfully!!!");
        $r_url = $type;
        if($r_url=='0'){
            redirect('leads');
        }else{
            redirect('leads/kanban');
        }
        redirect('leads');*/
	}
	/** 23-04-2018 **/
	public  function leads_details_delete()
	{
		if (isset($_POST['id'])) {
			$id = $_POST['id'];
			$this->Common_mdl->delete('leads', 'id', $id);
			/** for delete leads 22-09-2018**/
			$this->Common_mdl->delete('lead_reminder', 'lead_id', $id);
			/** for delete leads reminder 22-09-2018 **/
			$activity_datas['log'] = "Delete Lead";
			$activity_datas['createdTime'] = time();
			$activity_datas['module'] = 'Lead';
			$activity_datas['user_id'] = $_SESSION['admin_id'];
			$activity_datas['module_id'] = $id;
			$this->Common_mdl->insert('activity_log', $activity_datas);
		}

		echo $this->db->affected_rows();
	}
	public function archive_leads()
	{
		if (isset($_POST['id'])) {
			$ids = json_decode($_POST['id'], true);
			foreach ($ids as $id) {
				$this->db->query("update leads set old_status=lead_status,lead_status=35 where id=$id and lead_status!=35");
			}
			//$ids=json_decode($_POST[])
			echo $this->db->affected_rows();
		}
	}
	public function unarchive_leads()
	{
		if (isset($_POST['id'])) {
			$ids = json_decode($_POST['id'], true);
			foreach ($ids as $id) {
				//$this->db->query("update leads set old_status=lead_status,lead_status=23 where id=$id and lead_status!=23");
				$this->db->query("update leads set lead_status=@s:=lead_status,lead_status=old_status,old_status=@s where id=$id");
			}
			//$ids=json_decode($_POST[])
			echo $this->db->affected_rows();
		}
	}
	public function delete_source()
	{
		if (isset($_POST['ids'])) {
			$ids = json_decode($_POST['ids'], true);
			$id = implode(",", array_filter($ids));
			$this->db->query("DELETE FROM source WHERE id in ($id)");
			echo $this->db->affected_rows();
		}
	}

	public function delete_status()
	{
		if (isset($_POST['ids'])) {
			$ids = json_decode($_POST['ids'], true);
			// foreach ($ids as $key => $value) {
			//   $sql=$this->db->query('select * from lead_status where id="'.$value.'"')->row_array();
			//   $data['is_delete']=$sql['id'];
			//   $data['is_superadmin_owned']=$sql['id'];
			//   unset($sql['id']);
			//   $this->db->insert('leads_status',$sql);
			//  $id=$this->db->insert_id();
			//  $this->db->where('id',$id);
			//  $this->db->update('lead_status',$data);
			// }
			$id = implode(",", array_filter($ids));
			// echo "<pre>"; print_r($id);
			$this->db->query("DELETE FROM leads_status WHERE id in ($id)");
			echo $this->db->affected_rows();
		}
	}

	public function leads_detailed_tab_profile($id)
	{
		$this->Security_model->chk_login();
		$data['leads'] = $this->Common_mdl->select_record('leads', 'id', $id);
		// $data['leads_status']=$this->Common_mdl->getallrecords('leads_status');
		// $data['leads_status']=$this->Common_mdl->GetAllWithWhere('leads_status','firm_id', $_SESSION['firm_id']);
		$data['leads_status'] = $this->Common_mdl->dynamic_status('leads_status');


		// $data['source']=$this->Common_mdl->getallrecords('source');
		$data['source'] = $this->Common_mdl->dynamic_status('source');

		$data['countries'] = $this->Common_mdl->getallrecords('countries');
		$data['Language'] = $this->Common_mdl->getallrecords('languages');
		$data['staffs'] = $this->Common_mdl->GetAllWithWhere('user', 'role', '6');

		$data['latestrec'] = $this->Common_mdl->orderby('activity_log', 'module', 'Lead', 'desc');

		$this->load->view('leads/leads_detailed_tab_profile', $data);
	}
	/** end 17-04-2018 **/

	public function edit_lead_view($id)
	{
		//$data['leads']=$this->Common_mdl->select_record('leads','id',$id);

		$data['leads'] = $this->db->query("SELECT * FROM leads li where li.id =$id and li.firm_id=" . $_SESSION['firm_id'] . "  order by id desc")->row_array();

		// print_r($data);
		// exit;
		// $data['leads_status']=$this->Common_mdl->GetAllWithWhere('leads_status','firm_id', $_SESSION['firm_id']);
		$data['leads_status'] = $this->Common_mdl->dynamic_status('leads_status');
		//$data['leads_status']=$this->Common_mdl->getallrecords('leads_status');
		//      $data['source']=$this->Common_mdl->getallrecords('source');
		$data['source'] = $this->Common_mdl->dynamic_status('source');
		$data['countries'] = $this->Common_mdl->getallrecords('countries');
		$data['Language'] = $this->Common_mdl->getallrecords('languages');
		$data['staffs'] = $this->Common_mdl->GetAllWithWhere('user', 'role', '6');

		$data['latestrec'] = $this->Common_mdl->orderby('activity_log', 'module', 'Lead', 'desc');
		$sql_val = $this->db->query("SELECT * FROM firm_assignees where module_id = " . $id . " AND module_name='LEADS'")->result_array();

		foreach ($sql_val as $key1 => $value1) {
			$data['asssign_group'][] = $value1['assignees'];
		}


		$data['assign_check'] = $this->Common_mdl->assign_check($id);
		// print_r($data['leads']);
		//  exit;



		if (isset($data['leads']['lead_status']) && $data['leads']['lead_status'] != 60) {
			$this->load->view('leads/edit_lead_view', $data);
		} else {
			redirect('leads');
		}
	}

	public function new_leads()
	{
		$this->load->view('leads/new_lead');
	}

	public function new_lead()
	{
		$this->load->view('leads/new_lead_page');
	}

	public function addLeads()
	{
		//print_r($_POST);die;
		/** my one for user_id **/
		//** user id as a company id **/
		$data['user_id'] = $_SESSION['id'];
		$data['firm_id'] = $_SESSION['firm_id'];
		/** end of user id as company id **/
		/** end of user_id **/
		$data['name'] = $_POST['name'];
		$data['lead_status'] = $_POST['lead_status'];
		// $data['source'] = $_POST['source'];
		//$data['assigned'] = $_POST['assigned'];
		$data['source'] = $_POST['source'];
		if(is_array($_POST['source'])){
			$data['source'] = implode(',', $_POST['source']);
		}
		$team = array();
		$department = array();
		$assign = array();
		//$data['assigned'] = implode(',',$_POST['assigned']);
		/** for assign team and department  11-06-2018 **/
		// if(isset($_POST['assigned']))
		// {
		//     foreach ($_POST['assigned'] as $key => $value) {
		//       $ex_val=explode('_', $value);
		//       if(count($ex_val)>1)
		//       {
		//         if($ex_val[0]=='tm')
		//         {
		//            array_push($team, $ex_val[1]);
		//         }
		//         if($ex_val[0]=='de')
		//         {
		//             array_push($department, $ex_val[1]);
		//         }
		//       }
		//       else
		//       {
		//          array_push($assign, $ex_val[0]);
		//       }

		//     }

		//     $data['assigned'] = implode(',',$assign);
		//     $data['team'] = implode(',',$team);
		//     $data['dept'] = implode(',',$department);

		// }
		// $data['review_manager']=implode(',',$_POST['review_manager']);
		/** end of 11-06-2018 **/

		$data['tags'] = $_POST['tags'];
		$data['position'] = $_POST['position'];
		$data['email_address'] = $_POST['email_address'];
		$data['website'] = $_POST['website'];
		//$data['phone'] = $_POST['phone'];
		$data['phone'] = $_POST['country_code'] . '-' . $_POST['phone'];
		$data['company'] = $_POST['company'];
		$data['address'] = $_POST['address'];
		$data['city'] = $_POST['city'];
		$data['state'] = $_POST['state'];
		$data['country'] = $_POST['country'];
		$data['zip_code'] = $_POST['zip_code'];
		//  $data['default_language'] = (isset($_POST['default_language']))?$_POST['default_language']:'';
		$data['description'] = $_POST['description'];
		$data['contact_date'] = date('Y-m-d');

		if (isset($_POST['public'])) {
			$data['public'] = $_POST['public'];
		} else {
			$data['public'] = '';
		}
		if (isset($_POST['contact_today']) && $_POST['contact_today'] == 'on') {
			$data['contact_today'] = $_POST['contact_today'];
			$data['contact_date'] = date('Y-m-d');
		}
		if (isset($_POST['contact_date']) && $_POST['contact_date'] != '') {
			$originalDate = $_POST['contact_date'];
			$data['contact_date']  = implode('-', array_reverse(explode('-', $_POST['contact_date'])));
		}
		$data['createdTime'] = time();
		$data['old_fields_mail'] = $_POST['contact_today'] . "//" . $data['contact_date'];
		$result = $this->Common_mdl->insert('leads', $data);
		$id = '';
		if ($result) {
			$id = $this->db->insert_id();
			$this->Common_mdl->add_assignto($_POST['assign_role'], $id, 'LEADS');

			$activity_datas['log'] = "New Lead Created";
			$activity_datas['createdTime'] = time();
			$activity_datas['module'] = 'Lead';
			$activity_datas['user_id'] = $_SESSION['admin_id'];
			$activity_datas['module_id'] = $id;
			$this->Common_mdl->insert('activity_log', $activity_datas);
			$this->session->set_flashdata('success', "New Lead Created Successfully!!!");

			//redirect('leads');
		} else {

			$this->session->set_flashdata('error', "Error, Some datas missing!!!");
			//redirect('leads');
		}
		/*** for mail send 15-06-2018  **/
		// if contact today date is today 
		//  echo $id;
		if ($id != '') {
			$get_leads_data = $this->Common_mdl->GetAllWithWheretwo('leads', 'id', $id, 'contact_date', date('Y-m-d'));
			if (count($get_leads_data) > 0) {
				$this->web_lead_email($id);
				$datas['mail'] = 'send';
				$result =  $this->Common_mdl->update('leads', $datas, 'id', $id);
			}
		}

		redirect('leads/leads_detailed_tab/' . $id);

		/** en dof mail send **/


		/*$r_url = $_POST['kanpan'];
        if($r_url=='0'){
          //  redirect('leads');
          redirect('leads/leads_detailed_tab/'.$id);
        }else{
            redirect('leads/kanban');
        }*/
	}

	public function editLeads($id)
	{
		// print_r($_POST['country_code']);
		// exit;
		//$id = $_POST['id'];
		$data['name'] = $_POST['name'];
		$data['lead_status'] = $_POST['lead_status'];
		// $data['source'] = $_POST['source'];
		//$data['assigned'] = $_POST['assigned'];
		$data['source'] = implode(',', $_POST['source']);
		//  $data['assigned'] = implode(',',$_POST['assigned']);
		/** for assign team and department  11-06-2018 **/
		$team = array();
		$department = array();
		$assign = array();
		// if(isset($_POST['assigned']))
		// {
		//     foreach ($_POST['assigned'] as $key => $value) {
		//       $ex_val=explode('_', $value);
		//       if(count($ex_val)>1)
		//       {
		//         if($ex_val[0]=='tm')
		//         {
		//            array_push($team, $ex_val[1]);
		//         }
		//         if($ex_val[0]=='de')
		//         {
		//             array_push($department, $ex_val[1]);
		//         }
		//       }
		//       else
		//       {
		//          array_push($assign, $ex_val[0]);
		//       }

		//     }

		//     $data['assigned'] = implode(',',$assign);
		//     $data['team'] = implode(',',$team);
		//     $data['dept'] = implode(',',$department);

		// }
		/** end of 11-06-2018 **/
		// echo  $data['assigned']."test tets";
		// die;
		$data['tags'] = $_POST['tags'];
		$data['position'] = $_POST['position'];
		$data['email_address'] = $_POST['email_address'];
		$data['website'] = $_POST['website'];
		//$data['phone'] = $_POST['phone'];
		$data['phone'] = $_POST['country_code'] . '-' . $_POST['phone'];
		$data['company'] = $_POST['company'];
		if (isset($_POST['newcompany_name']) && $_POST['newcompany_name'] != '') {
			$data['company'] = $_POST['newcompany_name'];
		}
		/** user id as a company id **/
		//$data['user_id']=$_SESSION['id'];

		/** end of user id as company id **/
		$data['address'] = $_POST['address'];
		$data['city'] = $_POST['city'];
		$data['state'] = $_POST['state'];
		$data['country'] = $_POST['country'];
		$data['zip_code'] = $_POST['zip_code'];
		//$data['default_language'] = (isset($_POST['default_language']))?$_POST['default_language']:'';
		$data['description'] = $_POST['description'];
		$data['contact_date'] = date('Y-m-d');
		// $data['task_status'] = 'notstarted';
		// if(isset($_POST['public'])){
		//     $data['public'] = $_POST['public'];
		// }if(isset($_POST['contact_today']) && $_POST['contact_today']!=''){
		//     $data['contact_today'] = $_POST['contact_today'];
		//     $data['contact_date'] = date('Y-m-d');
		//     $_POST['contact_update_date']='';
		// }


		// if(isset($_POST['contact_update_date']) && $_POST['contact_update_date']!=''){
		//     $data['contact_today'] = '';
		//     $data['contact_date']  = implode('-', array_reverse(explode('-', $_POST['contact_update_date'])));
		// }
		if (isset($_POST['public'])) {
			$data['public'] = $_POST['public'];
		} else {
			$data['public'] = '';
		}
		if (isset($_POST['contact_today']) && $_POST['contact_today'] == 'on') {
			$data['contact_today'] = $_POST['contact_today'];
			$data['contact_date'] = date('Y-m-d');
		}
		if (isset($_POST['contact_update_date']) && $_POST['contact_update_date'] != '') {
			$originalDate = $_POST['contact_update_date'];
			$data['contact_date']  = implode('-', array_reverse(explode('-', $_POST['contact_update_date'])));
		}
		// print_r($_POST);
		// exit;

		/** for customfields **/
		$leads_data = $this->db->query("select * from leads where id=" . $id . " and custom_fields!='' ")->result_array();
		//echo count($leads_data);
		if (count($leads_data)) {
			$myObj = new stdClass();
			$qry_data = $this->db->query("select * from web_lead_template where id=" . $leads_data[0]['form_id'])->result_array();

			$form_data = json_decode($qry_data[0]['form_fields']);

			$custom_fields = $leads_data[0]['custom_fields'];
			$de_custom = json_decode($custom_fields, true);
			$it_val = '';
			$it_label = '';
			foreach ($de_custom as $key => $value) {
				foreach ($form_data as $fdkey => $fdvalue) {
					if ($fdvalue->label == $key) {
						$it_val = $fdvalue->type;
						$it_label = $fdvalue->label;
					}
				}
				if ($it_val != 'file') {
					if (isset($_POST[$key])) {

						$myObj->$key = $_POST[$key];
					}
				} else {
					// print_r($_POST);
					// echo $key;
					$datas['lead_id'] = $id;
					$datas['user_id'] = (isset($_SESSION['id'])) ? $_SESSION['id'] : '0';
					$datas['CreatedTime'] = time();
					if (isset($_FILES[$value]['name']) && ($_FILES[$value]['name'] != '')) {
						// echo "one";
						$images = $this->Common_mdl->do_upload($_FILES[$value], 'uploads/leads/attachments');
						$datass['attachments'] = $images;
						$myObj->$value =  $datas['attachments'];
						$this->Common_mdl->insert('leads_attachments', $datas);
					} else {
						//  echo "two";
						$images = $_POST['hidden_' . $key];
						// echo $images;
						// echo "<br>";
						// echo $_POST['hidden_images'];
						$datass['attachments'] = $images;
					}
					$myObj->$key =  $datass['attachments'];
				}
			}

			$myJSON = json_encode($myObj);
			$data['custom_fields'] = $myJSON;

			//   echo $myJSON;
		}
		/** end of custom_fields**/

		$lead_data = $this->db->query("select * from leads where id=" . $id)->result_array();
		$result =  $this->Common_mdl->update('leads', $data, 'id', $id);

		// convert to customer
		if ($data['lead_status'] == '60' && $lead_data['lead_status'] != '60') {
			// $this->convert_to_customer($id, false);
		}

		$this->Common_mdl->usertask_modify($_POST['assign_role'], $id, 'lead_reminder', 'LEADS_REMINDER');

		$this->Common_mdl->usertask_modify($_POST['assign_role'], $id, 'add_new_task', 'TASK');

		$this->Common_mdl->add_assignto($_POST['assign_role'], $id, 'LEADS', 1);

		/*********** 18-06-2018 **/
		//for email send **/
		/*** for mail send 15-06-2018  **/
		// if contact today date is today 
		if ($id != '') {
			$get_leads_data = $this->Common_mdl->GetAllWithWheretwo('leads', 'id', $id, 'contact_date', date('Y-m-d'));
			if (count($get_leads_data) > 0) {
				$old_fields_mail = $get_leads_data[0]['old_fields_mail'];
				$old_conatct_today = $get_leads_data[0]['contact_today'];
				$old_email = $get_leads_data[0]['mail'];
				$ex_val = explode('//', $old_fields_mail);
				$for_contact_date = $data['contact_date'];
				if ($old_fields_mail != '') {
					//echo $ex_val[1]."-------".$for_contact_date;
					if ($ex_val[1] != $for_contact_date) {
						$this->web_lead_email($id);
						//  echo "ssss";
						$datas['mail'] = 'send';
						$datas['old_fields_mail'] = $old_conatct_today . "//" . $for_contact_date;
						$result =  $this->Common_mdl->update('leads', $datas, 'id', $id);
					}
				} else {
					//  echo "two";
					$this->web_lead_email($id);
					$datas['mail'] = 'send';
					$datas['old_fields_mail'] = $old_conatct_today . "//" . $for_contact_date;
					$result =  $this->Common_mdl->update('leads', $datas, 'id', $id);
					///  echo $result."rer res";
				}
			}
		}

		/** en dof mail send **/
		/** end of 18-06-2018 *******/
		// exit;
		// echo $this->db->last_query();die;
		// if($result)
		// {
		$activity_datas['log'] = "Lead Records Updated";
		$activity_datas['createdTime'] = time();
		$activity_datas['module'] = 'Lead';
		$activity_datas['user_id'] = $_SESSION['admin_id'];
		$activity_datas['module_id'] = $id;

		$this->Common_mdl->insert('activity_log', $activity_datas);

		$this->session->set_flashdata('success', "Lead updated Successfully!!!");
		// redirect('leads');
		// }else{
		//     $this->session->set_flashdata('error',"Error, Some datas missing!!!");
		//    // redirect('leads');
		// } 

		$r_url = $_POST['kanpan'];
		if (isset($_POST['for_edit_page']) && !empty($_POST['for_edit_page'])) {
			// redirect('leads/edit_lead_view/'.$_POST['for_edit_page']);
			redirect('leads');
		} elseif (isset($_POST['for_deatiled_page']) && !empty($_POST['for_deatiled_page'])) {
			// echo "success";
			// $data['leads']=$this->Common_mdl->select_record('leads','id',$id);
			$data['leads'] = $this->db->query("SELECT * FROM leads li where li.id =$id and li.firm_id=" . $_SESSION['firm_id'] . "  order by id desc")->row_array();
			//  $data['leads_status']=$this->Common_mdl->getallrecords('leads_status');
			// $data['leads_status']=$this->Common_mdl->GetAllWithWhere('leads_status','firm_id', $_SESSION['firm_id']);


			$data['leads_status'] = $this->Common_mdl->dynamic_status('leads_status');

			$data['source'] = $this->Common_mdl->dynamic_status('source');
			// $data['source']=$this->Common_mdl->getallrecords('source');
			$data['countries'] = $this->Common_mdl->getallrecords('countries');
			$data['Language'] = $this->Common_mdl->getallrecords('languages');
			$data['staffs'] = $this->Common_mdl->GetAllWithWhere('user', 'role', '6');

			$data['latestrec'] = $this->Common_mdl->orderby('activity_log', 'module', 'Lead', 'desc');

			$this->load->view('leads/leads_detailed_tab_profile', $data);
		} else {
			if ($r_url == '0') {
				redirect('leads');
			} else {
				redirect('leads/kanban');
			}
		}
	}

	function delete($id, $type)
	{
		$this->Common_mdl->delete('leads', 'id', $id);

		$activity_datas['log'] = "Delete Lead";
		$activity_datas['createdTime'] = time();
		$activity_datas['module'] = 'Lead';
		$activity_datas['user_id'] = $_SESSION['admin_id'];
		$activity_datas['module_id'] = $id;
		$this->Common_mdl->insert('activity_log', $activity_datas);


		$this->session->set_flashdata('success', "Lead Deleted Successfully!!!");
		$r_url = $type;
		if ($r_url == '0') {
			redirect('leads');
		} else {
			redirect('leads/kanban');
		}
		redirect('leads');
	}

	public function deleteLeads()
	{
		if (isset($_POST['data_id'])) {
			$id = trim($_POST['data_id']);
			$sql = $this->db->query("DELETE FROM leads WHERE id in ($id)");
			echo $id;
		}
	}

	public function deleteLeadsSource()
	{
		if (isset($_POST['data_id'])) {
			$id = trim($_POST['data_id']);
			$sql = $this->db->query("DELETE FROM source WHERE id in ($id)");
			echo $id;
		}
	}

	public function deleteLeadsStatus()
	{
		if (isset($_POST['data_id'])) {
			$id = trim($_POST['data_id']);
			$sql = $this->db->query("DELETE FROM leads_status WHERE id in ($id)");
			echo $id;
		}
	}

	public function sort_by()
	{
		$sortby_val = $this->input->post('sortby_val');
		// $sql = "select * from leads where user_id=".$_SESSION['id']." order by ";
		/** 16-07-2018 **/
		// $team_num=array();
		// $for_cus_team=$this->db->query("select * from team_assign_staff where FIND_IN_SET('".$_SESSION['id']."',staff_id)")->result_array();
		// foreach ($for_cus_team as $cu_team_key => $cu_team_value) {
		// array_push($team_num,$cu_team_value['team_id']);
		// }
		// if(!empty($team_num) && count($team_num)>0){
		// $res_team_num=implode('|', $team_num);
		// }
		// else
		// { 
		// $res_team_num=0;
		// }

		// $department_num=array();
		// $for_cus_department=$this->db->query("SELECT * FROM `department_assign_team` where CONCAT(',', `team_id`, ',') REGEXP ',($res_team_num),'")->result_array();
		// foreach ($for_cus_department as $cu_dept_key => $cu_dept_value) {
		// array_push($department_num,$cu_dept_value['depart_id']);
		// }
		// if(!empty($department_num) && count($department_num)>0){
		// $res_dept_num=implode('|', $department_num);
		// }
		// else
		// {
		// $res_dept_num=0;
		// }
		/** end of 16-07-2018 **/
		/** for permission 16-07-2018 **/
		$it_session_id = '';
		$get_this_qry = $this->db->query("select * from user where id=" . $_SESSION['id'])->row_array();
		$created_id = $get_this_qry['firm_admin_id'];
		$its_firm_admin_id = $get_this_qry['firm_admin_id'];
		// if($_SESSION['role']==6) // staff
		// {
		// $it_session_id=$_SESSION['id'];
		// }
		// else if($_SESSION['role']==5) //manager
		// {
		// $it_session_id=$created_id;
		// }
		// else if($_SESSION['role']==4)//client
		// {
		// $it_session_id=$created_id;
		// }
		// else if($_SESSION['role']==3) //director
		// {
		// $it_session_id=$created_id;
		// }
		// else if($_SESSION['role']==2) // sub admin
		// {
		// $it_session_id=$created_id;
		// }
		// else
		// {
		$it_session_id = $_SESSION['id'];
		//  }

		/** end of permission 16-07-2018 **/
		$result = array();
		// array_push($result, $it_session_id);
		// $reassign_firm_id=$its_firm_admin_id;
		// $tot_val=1;
		// for($z=0;$z<$tot_val;$z++)
		// {
		// if($reassign_firm_id==0)
		// {
		// $tot_val=0;
		// }
		// else
		// {
		// $get_this_qry=$this->db->query("select * from user where id=".$reassign_firm_id)->row_array();
		// if(!empty($get_this_qry)){
		//  array_push($result, $get_this_qry['id']);
		// $new_created_id=$get_this_qry['firm_admin_id'];
		// $reassign_firm_id=$get_this_qry['firm_admin_id'];
		// }
		// else
		// {
		// $tot_val=0;
		// }

		// }

		// }

		$res2 = array();
		$ques = Get_Assigned_Datas('LEADS');


		$res = (!empty($ques)) ? implode(',', array_filter(array_unique($ques))) : '-1';

		/** end of permission 16-07-2018 **/
		// if($_SESSION['role']==6)
		// {
		// $sql="select * from (SELECT * FROM leads li where li.user_id=".$it_session_id." UNION SELECT * FROM leads lis where lis.user_id!=".$it_session_id." and public='on' 
		// UNION SELECT * FROM leads tls where tls.user_id!=".$it_session_id." and (FIND_IN_SET('".$it_session_id."',assigned) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `dept`, ',') REGEXP ',($res_dept_num),') )  ) x
		// order by ";
		// }
		// else if($_SESSION['role']==4)
		// {

		// $sqls=$this->db->query("select * from client where user_id=".$_SESSION['id']." ")->row_array();
		// $its_id=$sqls['id'];
		// $sql="select * from (SELECT * FROM leads li where li.company=".$its_id."  UNION SELECT * FROM leads lis where lis.user_id in($res) and public='on' ) x order by ";

		// }
		// else if($_SESSION['role']==5 || $_SESSION['role']==3 || $_SESSION['role']==2)
		// {

		// // $res=implode(',', $over_all_res);
		// $sql="select * from (SELECT * FROM leads li where li.user_id in ($res)  
		// UNION 
		// SELECT * FROM leads lis where lis.user_id in($res) and public='on' ) x order by ";
		// }
		// else
		// {

		// $res=implode(',', $over_all_res);

		// $data['leads']=$this->db->query("SELECT * FROM leads li where li.id in ($res) and li.firm_id=".$_SESSION['firm_id']."  UNION
		//   SELECT * FROM leads lis where lis.id in($res) and public='on' order by id desc ")->result_array();

		$sql = "select * from (SELECT * FROM leads li where li.id in ($res) and li.firm_id=" . $_SESSION['firm_id'] . "
        UNION 
        SELECT * FROM leads lis where lis.id in($res) and public='on' ) x order by ";
		// $data['leads']=$this->db->query("SELECT * FROM leads li where li.user_id=".$it_session_id." ")->result_array();
		// }

		if ($sortby_val == 'date_created') {
			$sql .= ' id desc';
		} elseif ($sortby_val == 'kan_ban_order') {
			$sql .= ' lead_status asc';
		} elseif ($sortby_val == 'last_contact') {
			$sql .= ' contact_date desc';
		}
		//  echo $sql;
		$data['leads'] = $this->db->query($sql)->result_array();
		// $data['leads_status']=$this->Common_mdl->getallrecords('leads_status');
		// $data['source']=$this->Common_mdl->getallrecords('source');
		// $data['leads_status']=$this->Common_mdl->GetAllWithWhere('leads_status','firm_id', $_SESSION['firm_id']);


		$data['leads_status'] = $this->Common_mdl->dynamic_status('leads_status');

		$data['source'] = $this->Common_mdl->dynamic_status('source');

		$data['countries'] = $this->Common_mdl->getallrecords('countries');
		$data['Language'] = $this->Common_mdl->getallrecords('languages');
		$data['staffs'] = $this->Common_mdl->GetAllWithWhere('user', 'role', '6');

		$data['latestrec'] = $this->Common_mdl->orderby('activity_log', 'module', 'Lead', 'desc');

		$this->load->view('leads/sortby_rec', $data);
	}



	public function sort_by_kanban()
	{
		// $data['leads_status']=$this->Common_mdl->getallrecords('leads_status');
		// $data['leads_status']=$this->Common_mdl->GetAllWithWhere('leads_status','firm_id', $_SESSION['firm_id']);


		$data['leads_status'] = $this->Common_mdl->dynamic_status('leads_status');
		$data['type'] = 'sort_by';

		$data['sortby_val'] = $this->input->post('sortby_val');
		$this->load->view('leads/sortby_kanban', $data);
	}

	public function kanban()
	{
		$this->Security_model->chk_login();
		$data['leads'] = $this->Common_mdl->getallrecords('leads');
		//$data['leads_status']=$this->Common_mdl->getallrecords('leads_status');
		// $data['leads_status']=$this->Common_mdl->GetAllWithWhere('leads_status','firm_id', $_SESSION['firm_id']);

		$data['leads_status'] = $this->Common_mdl->dynamic_status('leads_status');
		//  $data['source']=$this->Common_mdl->getallrecords('source');
		$data['source'] = $this->Common_mdl->dynamic_status('source');
		$data['countries'] = $this->Common_mdl->getallrecords('countries');
		$data['Language'] = $this->Common_mdl->getallrecords('languages');
		$data['staff_form'] = $this->Common_mdl->GetAllWithWhere('user', 'role', '6');
		$data['latestrec'] = $this->Common_mdl->orderby('activity_log', 'module', 'Lead', 'desc');
		//$data['latestrecs'] = $this->Common_mdl->orderby('activity_log','module','Lead','asc');

		$this->load->view('leads/leads_view_kanban', $data);
	}

	public function import_leads()
	{
		//echo $this->uri->segment(2);die;
		$return_data = array();
		$csvMimes = array('application/vnd.ms-excel', 'text/plain', 'text/csv', 'text/tsv');
		if (!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'], $csvMimes)) {
			//echo 'gggg';


			$csvFile = fopen($_FILES['file']['tmp_name'], 'r');
			//fgetcsv($csvFile);

			$check_mail = array();
			//$check_user_name=array();
			fgetcsv($csvFile);
			while (($line = fgetcsv($csvFile)) !== FALSE) {
				//echo "inside while1";die;
				$check_mail[] = $line[9];
			}

			if (count(array_unique($check_mail)) != count($check_mail)) {
				echo json_encode(['status' => "0", 'file' => "Check The File Dublicate mail Found"]);
				die;
			} else {
				$mail_ids = "'" . implode("','", $check_mail) . "'";
				$check = $this->db->query("select email_address from leads where email_address IN ($mail_ids)")->result_array();
				if (count($check) > 0) {
					echo json_encode(['status' => "0", 'file' => "Mail Already Exists"]);
					die;
				}
			}

			$data['lead_status'] = $_POST['lead_status'];
			// $data['source'] = $_POST['source'];
			// $data['assigned'] = $_POST['assigned'];
			$data['source'] = implode(',', $_POST['source']);
			// $data['assigned'] =  implode(',',$_POST['assigned']);
			/** 12-06-2018 rspt for  **/

			$team = array();
			$department = array();
			$assign = array();
			//$data['assigned'] = implode(',',$_POST['assigned']);
			/** for assign team and department  11-06-2018 **/
			if (isset($_POST['assigned'])) {
				foreach ($_POST['assigned'] as $key => $value) {
					$ex_val = explode('_', $value);
					if (count($ex_val) > 1) {
						if ($ex_val[0] == 'tm') {
							array_push($team, $ex_val[1]);
						}
						if ($ex_val[0] == 'de') {
							array_push($department, $ex_val[1]);
						}
					} else {
						array_push($assign, $ex_val[0]);
					}
				}

				$data['assigned'] = implode(',', $assign);
				$data['team'] = implode(',', $team);
				$data['dept'] = implode(',', $department);
			}
			$data['review_manager'] = implode(',', $_POST['review_manager']);

			/** end of 11-06-2018 **/
			/** end 12-06-2018 **/
			$re_url = $_POST['kanpan'];
			rewind($csvFile);
			fgetcsv($csvFile);
			while (($line = fgetcsv($csvFile)) !== FALSE) {
				/*echo '<pre>';
                print_r($line);*/
				$data['name'] = $line[0];
				$data['company'] = $line[1];
				/** for company added **/


				/** end of company added **/
				/** country **/
				$country = $line[4];
				if (is_numeric($country)) {
					$country_id = $country;
				} else {
					$country_name = $this->Common_mdl->select_record('countries', 'name', $country);
					if (count($country_name) > 0) {
						$country_id = $country_name['id'];
					} else {
						$datas['sortname'] = '';
						$datas['name'] =  $country;
						//  $data['phonecode']=;
						$result = $this->Common_mdl->insert('countries', $datas);
						$country_id = $result;
					}
				}
				/** end of country **/
				$data['position'] = $line[2];
				$data['description'] = $line[3];
				$data['country'] = $line[4];
				$data['zip_code'] = $line[5];
				$data['city'] = $line[6];
				$data['state'] = $line[7];
				$data['address'] = $line[8];
				$data['email_address'] = $line[9];
				$data['website'] = $line[10];
				$data['phone'] = $line[11];
				$data['tags'] = $line[12];
				$data['public'] = $line[13];
				$data['contact_today'] = $line[14];
				$data['contact_date'] = $line[15];
				$data['createdTime'] = time();
				//    $data['default_language'] = 1;
				$data['user_id'] = $_SESSION['id'];

				$result = $this->Common_mdl->insert('leads', $data);
				//echo "leads=";

				$id = $this->db->insert_id();
				$activity_datas['log'] = "Lead Imported";
				$activity_datas['createdTime'] = time();
				$activity_datas['module'] = 'Lead';
				$activity_datas['user_id'] = $_SESSION['admin_id'];
				$activity_datas['module_id'] = $id;
				$this->Common_mdl->insert('activity_log', $activity_datas);

				/*** for mail send 15-06-2018  **/
				// if contact today date is today 
				//  echo $id;
				if ($id != '') {
					$get_leads_data = $this->Common_mdl->GetAllWithWheretwo('leads', 'id', $id, 'contact_date', date('Y-m-d'));
					if (count($get_leads_data) > 0) {
						$this->web_lead_email($id);
						$datas['mail'] = 'send';
						$datas['old_fields_mail'] = $data['contact_today'] . "//" . $data['contact_date'];
						$result =  $this->Common_mdl->update('leads', $datas, 'id', $id);
					}
				}


				/** en dof mail send **/

				//echo $this->db->last_query();
			}
			//die;
			fclose($csvFile);
			$return_data = ['status' => 1];
		} else {
			$return_data = ["status" => "0", "file" => "File Type Doesnt Accepted.."];
		}

		echo json_encode($return_data);

		/* $this->session->set_flashdata('success',"Lead Imported Successfully!!!");

            if($re_url==0){
                redirect('leads');
            }else{
                redirect('leads/kanban');
            }*/
	}

	public function filter_status()
	{


		$ques = Get_Assigned_Datas('LEADS');


		$res = (!empty($ques)) ? implode(',', array_filter(array_unique($ques))) : '-1';
		// $res=implode(',', $over_all_res);
		$data['leads'] = $this->db->query("SELECT * FROM leads li where li.id in ($res) and li.firm_id=" . $_SESSION['firm_id'] . "  UNION
          SELECT * FROM leads lis where lis.id in($res) and public='on' order by id desc ")->result_array();



		$data['leads_status'] = $this->Common_mdl->dynamic_status('leads_status');
		$data['source'] = $this->Common_mdl->dynamic_status('source');
		// $data['source']=$this->Common_mdl->getallrecords('source');
		$data['countries'] = $this->Common_mdl->getallrecords('countries');
		$data['Language'] = $this->Common_mdl->getallrecords('languages');
		$data['staffs'] = $this->Common_mdl->GetAllWithWhere('user', 'role', '6');

		$data['latestrec'] = $this->Common_mdl->orderby('activity_log', 'module', 'Lead', 'desc');
		$data['user_list'] = $this->db->query("select * from user where firm_id=" . $_SESSION['firm_id'] . " and user_type !='FC' and user_type !='SA'")->result_array();

		/** **/
		$this->load->view('leads/sortby_rec', $data);
	}

	public function filter_status_kanban()
	{
		$data['filter_val'] = $this->input->post('filter_val');
		$data['leads_status'] = $this->Common_mdl->GetAllWithWhere('leads_status', 'id', $data['filter_val']);
		$data['sortby_val'] = '';
		$this->load->view('leads/sortby_kanban', $data);
	}

	public function status_update()
	{
		$status_val = $this->input->post('status_val');
		$id = $this->input->post('id');
		if ($status_val == 'lost') {
			$datas['lead_status'] = '3';
		} else {
			$datas['lead_status'] = '2';
		}
		$this->Common_mdl->update('leads', $datas, 'id', $id);
		//echo $this->db->last_query();die;
		$lost_cnt = $this->db->query('select count(*) as i from leads where lead_status="3"')->row_array();
		$junk_cnt = $this->db->query('select count(*) as i from leads where lead_status="2"')->row_array();
		echo $lost_cnt['i'] . '-' . $junk_cnt['i'];
	}

	public function status_update_kanban()
	{
		$status_val = $this->input->post('status_val');
		$id = $this->input->post('id');
		if ($status_val == 'lost') {
			$datas['lead_status'] = '3';
		} else {
			$datas['lead_status'] = '2';
		}
		$this->Common_mdl->update('leads', $datas, 'id', $id);
		//echo $this->db->last_query();die;
		$lost_cnt = $this->db->query('select count(*) as i from leads where lead_status="3"')->row_array();
		$junk_cnt = $this->db->query('select count(*) as i from leads where lead_status="2"')->row_array();
		//echo $lost_cnt['i'].'-'.$junk_cnt['i'];

		//  $leads_status=$this->Common_mdl->getallrecords('leads_status');
		// $leads_status=$this->Common_mdl->GetAllWithWhere('leads_status','firm_id', $_SESSION['firm_id']);

		$leads_status = $this->Common_mdl->dynamic_status('leads_status');
		$data['type'] = 'sort_by';

		$html = '';
		foreach ($leads_status as $ls_value) {
			$html .= '
    <div class="new-row-data1 col-xs-12 col-sm-4">
        <div class="trial-stated-title">
            <h4><i class="fa fa-bars fa-6" aria-hidden="true"></i>' . $ls_value['status_name'] . '</h4>
        </div>';
			$ls_id = $ls_value['id'];
			$sql = "select * from leads where lead_status =" . $ls_id . "  order by id desc";

			$leads_rec = $this->db->query($sql)->result_array();
			//echo $this->db->last_query();
			//$leads_rec = $this->Common_mdl->GetAllWithWhere('leads','lead_status',$ls_id);
			if (!empty($leads_rec)) {
				foreach ($leads_rec as $key => $value) {
					$date = date('Y-m-d H:i:s', $value['createdTime']);
					$getUserProfilepic = $this->Common_mdl->getUserProfilepic($value['assigned']);

					/* if($value['source']==1)
                               {
                                $value['source'] = 'Google';
                               }elseif($value['source']==2)
                               {
                                $value['source'] = 'Facebook';
                               }else{
                                $value['source'] = '';
                               }*/
					$source_name = $this->Common_mdl->GetAllWithWhere('source', 'id', $value['source']);
					$value['source'] = $source_name[0]['source_name'];
					if ($value['source'] != '') {
						$value['source'] = $value['source'];
					} else {
						$value['source'] = '-';
					}

					$html .= '
            <div class="back-set02">
                <div class="user-leads1">
                    <h3 data-target="#profile_lead_' . $value['id'] . '" data-toggle="modal"><img src="' . $getUserProfilepic . '" alt="img">' . "#" . $value['id'] . '-' . $value['name'] . '</h3>
                    <div class="source-google">
                        <div class="source-set1">
                        <span>Source' . $value['source'] . '</span>
                        </div>
                        <div class="source-set1 source-set3 ">
                        <span>Created ' . $this->time_elapsed_string($date) . '</span>
                            <strong><i class="fa fa-file-o fa-6" aria-hidden="true"></i> 0</strong>
                            <strong><i class="fa fa-paperclip fa-6" aria-hidden="true"></i> 0</strong>
                        </div>
                    </div>  
                </div>
            </div>';
				}
			} else {
				$html .= ' <div class="new-row-data1 col-xs-12 col-sm-4"> No records Found </div>';
			}
			$html .= '</div>';
		}

		$return_arr = array(
			"html" => $html,
			"lost_cnt" => $lost_cnt['i'],
			"junk_cnt" => $junk_cnt['i']
		);
		echo json_encode($return_arr);
	}

	public function add_activity_log()
	{
		$datas['log'] = $this->input->post('values');
		$datas['createdTime'] = time();
		$datas['module'] = 'Lead';
		$datas['user_id'] = $_SESSION['admin_id'];
		$datas['module_id'] = $this->input->post('id');
		$this->Common_mdl->insert('activity_log', $datas);
		$latestrecs = $this->db->query("select * from activity_log where module = 'Lead' and module_id = '" . $datas['module_id'] . "' order by id asc")->result_array();
		$rec = '';
		foreach ($latestrecs as $late_key => $late_value) {
			$date = date('Y-m-d H:i:s', $late_value["CreatedTime"]);
			$l_id = $late_value['module_id'];
			$l_name = $this->Common_mdl->select_record('leads', 'id', $l_id);
			$rec .= "<div class='activity-created1'>
            <span>" . $this->time_elapsed_string($date) . "</span>
            <span>" . $l_name['name'] . ' - ' . $late_value['log'] . "</span>
            </div>";
		}
		echo $rec;
	}


	function time_elapsed_string($datetime, $full = false)
	{
		$now = new DateTime;
		$ago = new DateTime($datetime);
		$diff = $now->diff($ago);

		$diff->w = floor($diff->d / 7);
		$diff->d -= $diff->w * 7;

		$string = array(
			'y' => 'year',
			'm' => 'month',
			'w' => 'week',
			'd' => 'day',
			'h' => 'hour',
			'i' => 'minute',
			's' => 'second',
		);
		foreach ($string as $k => &$v) {
			if ($diff->$k) {
				$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
			} else {
				unset($string[$k]);
			}
		}

		if (!$full) $string = array_slice($string, 0, 1);
		return $string ? implode(', ', $string) . ' ago' : 'just now';
	}

	public function lead_status()
	{
		// $data['leads_status']=$this->Common_mdl->getallrecords('leads_status');
		// $data['leads_status']=$this->Common_mdl->GetAllWithWhere('leads_status','firm_id', $_SESSION['firm_id']);


		$data['leads_status'] = $this->Common_mdl->dynamic_status('leads_status');

		$this->load->view('leads/status', $data);
	}

	public function s_delete($id)
	{
		$this->Common_mdl->delete('leads_status', 'id', $id);
		$this->session->set_flashdata('success', "Lead status Deleted Successfully!!!");
		//redirect('leads/lead_status');   
		redirect('leads/lead_settings');   // for settings page
	}
	public function add_status()
	{
		$this->Security_model->chk_login();
		$this->load->view('leads/add_status');
	}

	public function add_new()
	{
		$this->Security_model->chk_login();
		$data['status_name'] = $this->input->post('new_team');
		$data['createdTime'] = time();
		$this->Common_mdl->insert('leads_status', $data);
		$this->session->set_flashdata('success', 'Status Added Successfully');
		//redirect('leads/lead_status');
		redirect('leads/lead_settings');
	}

	public function update_status($id)
	{
		$this->Security_model->chk_login();
		$data['leads'] = $this->Common_mdl->select_record('leads_status', 'id', $id);
		$data['id'] = $id;
		$this->load->view('leads/update_status', $data);
	}

	public function update($id)
	{
		$this->Security_model->chk_login();
		$data['status_name'] = $this->input->post('new_team');
		$this->Common_mdl->update('leads_status', $data, 'id', $id);
		$this->session->set_flashdata('success', 'status Updated Successfully');
		//redirect('department/update_department/'.$id);
		//redirect('leads/lead_status');
		redirect('leads/lead_settings');
	}


	/** Source **/

	public function lead_source()
	{
		//$data['source']=$this->Common_mdl->getallrecords('source');
		$data['source'] = $this->Common_mdl->dynamic_status('source');
		$this->load->view('leads/source', $data);
	}

	public function source_delete($id)
	{
		$this->Common_mdl->delete('source', 'id', $id);
		$this->session->set_flashdata('success', "Source Deleted Successfully!!!");
		//redirect('leads/lead_source'); 
		redirect('leads/lead_settings');
	}
	public function add_source()
	{
		$this->Security_model->chk_login();
		$this->load->view('leads/add_source');
	}

	public function add_new_source()
	{
		$this->Security_model->chk_login();
		$data['source_name'] = $this->input->post('new_team');
		$data['createdTime'] = time();
		$this->Common_mdl->insert('source', $data);
		$this->session->set_flashdata('success', 'Source Added Successfully');

		//redirect('leads/lead_source');
		redirect('leads/lead_settings');
	}

	public function update_source($id)
	{
		$this->Security_model->chk_login();
		$data['source'] = $this->Common_mdl->select_record('source', 'id', $id);
		$data['id'] = $id;
		$this->load->view('leads/update_source', $data);
	}

	public function updatesource($id)
	{
		$this->Security_model->chk_login();
		$data['source_name'] = $this->input->post('new_team');
		$this->Common_mdl->update('source', $data, 'id', $id);

		$this->session->set_flashdata('success', 'source Updated Successfully');
		//redirect('department/update_department/'.$id);
		//redirect('leads/lead_source');
		redirect('leads/lead_settings');
	}

	public function add_note()
	{
		$data['notes'] = $this->input->post('values');
		$data['lead_id'] = $this->input->post('id');
		if ($this->input->post('contact_date') != '') {
			//$data['contact_date'] = $this->input->post('contact_date');
			$data['contact_date']  = implode('-', array_reverse(explode('-', $_POST['contact_date'])));
		}
		$data['user_id'] = $_SESSION['userId'];
		$data['createdTime'] = time();
		$this->Common_mdl->insert('leads_notes', $data);

		$activity_datas['log'] = "Lead Notes Added---" . $data['notes'];
		$activity_datas['createdTime'] = time();
		$activity_datas['module'] = 'Lead';
		$activity_datas['user_id'] = $_SESSION['id'];
		$activity_datas['module_id'] = $this->input->post('id');
		$activity_datas['sub_module'] = 'Lead Notes';
		$this->Common_mdl->insert('activity_log', $activity_datas);

		$rec = $this->get_leads($data['lead_id']);

		echo $rec;
	}

	public function get_leads($l_id)
	{
		$leads_notes = $this->db->query(" select * from leads_notes where lead_id ='" . $l_id . "' order by id desc")->result_array();
		$html = '';
		foreach ($leads_notes as $notes_key => $notes_value) {
			$getUserProfilepic = $this->Common_mdl->getUserProfilepic($notes_value['user_id']);
			if ($notes_value['createdTime'] != '') {
				$d = date('d M Y h:i A', $notes_value['createdTime']);
			} else {
				$d = '';
			}
			$l_name = $this->Common_mdl->select_record('leads', 'id', $notes_value['lead_id']);
			$u_name = $this->Common_mdl->select_record('user', 'id', $notes_value['user_id']);

			$html .= '<div class="box-width-quote1"><div class="gust-toy01">
            <div class="checkbox-profile1">
                <img src="' . $getUserProfilepic . '" alt="User-Profile-Image">
            </div>
            <div class="timeline-lead01">
                <span>Note added: ' . $d . '</span>
                <strong>' . $u_name["crm_name"] . '</strong>
                <p> ' . $notes_value['notes'] . '</p>
            </div>';
			if ($_SESSION['permission']['Leads']['delete'] == '1') {
				$html .= ' <div class="edit-timeline">                
                <a href="#" data-target="#deleteconfirmation" data-toggle="modal" data-id="' . $notes_value['id'] . '" onclick="return delete_data(this,\'delete_note\');"><i class="fa fa-times fa-6" aria-hidden="true"></i></a>
            </div>';
			}
			$html .= ' </div></div>';
		}
		return $html;
	}

	public function delete_note()
	{
		/*$id = explode('-', $this->input->post('note_id'));
        $note_id = $id[0];
        $lead_id = $id[1];*/

		$id = $_POST['id'];
		$data = $this->Common_mdl->select_record('leads_notes', 'id', $id);
		$suc = $this->Common_mdl->delete('leads_notes', 'id', $id);

		if ($suc) {

			$rec = $this->get_leads($data['lead_id']);

			/** for activity logs **/
			$activity_datas['log'] = "Lead Notes Delete---" . $data['notes'];
			$activity_datas['createdTime'] = time();
			$activity_datas['module'] = 'Lead';
			$activity_datas['user_id'] = $_SESSION['admin_id'];
			$activity_datas['module_id'] =  $data['lead_id'];
			$activity_datas['sub_module'] = 'Lead Notes';
			$activity_datas['sub_module_id'] = $id;
			$this->Common_mdl->insert('activity_log', $activity_datas);
			/** end of activity logs **/
		} else {
			$rec = '';
		}
		echo $rec;
	}

	/*12-03-2018** Lakshmipriya **/
	public function proposal($l_id, $rel_type)
	{
		$this->Security_model->chk_login();
		$data['lead_rec'] = $this->Common_mdl->select_record('leads', 'id', $l_id);
		$data['all_leads'] = $this->Common_mdl->getallrecords('leads');
		$data['countries'] = $this->Common_mdl->getallrecords('countries');
		$data['Language'] = $this->Common_mdl->getallrecords('languages');
		$data['staff_form'] = $this->Common_mdl->GetAllWithWhere('user', 'role', '6');
		$this->load->view('leads/proposal', $data);
	}

	public function lead_attachment()
	{
		$l_id = $_POST['id'];
		$files = $_FILES;
		$path = 'uploads/leads/attachments';
		$config = $this->get_upload_options($path);
		$type = explode('.', $files['file']['name']);
		$_FILES['tmp']['name'] = time() . '.' . end($type);
		$_FILES['tmp']['type'] = $files['file']['type'];
		$_FILES['tmp']['tmp_name'] = $files['file']['tmp_name'];
		$_FILES['tmp']['error'] = $files['file']['error'];
		$_FILES['tmp']['size'] = $files['file']['size'];
		if ($this->upload->do_upload('file')) {
			$data = $this->upload->data();

			$datas['attachments'] =  $data['file_name'];
			$datas['lead_id'] = $l_id;
			$datas['user_id'] = $_SESSION['userId'];
			$datas['CreatedTime'] = time();
			$result = $this->Common_mdl->insert('leads_attachments', $datas);
			/** for activity logs **/

			$id = $this->db->insert_id();
			$activity_datas['log'] = "Lead Attached---" . $data['file_name'];
			$activity_datas['createdTime'] = time();
			$activity_datas['module'] = 'Lead';
			$activity_datas['user_id'] = $_SESSION['admin_id'];
			$activity_datas['module_id'] = $l_id;
			$activity_datas['sub_module'] = 'Lead Attachment';
			$activity_datas['sub_module_id'] = $id;
			$this->Common_mdl->insert('activity_log', $activity_datas);
			/** end of activity logs **/


			if ($result) {
				$ht = $this->get_attachments($l_id);
			}
		} else {
			$ht = 'File Did not uploaded..';
		}
		echo $ht;
	}
	public function get_upload_options($path)
	{
		//upload an image options
		$config = array(
			'upload_path' => './' . $path,
			'allowed_types' => "*",
			'overwrite' => FALSE,
		);
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		return $config;
	}
	function get_attachments($l_id)
	{
		$path = 'uploads/leads/attachments';

		$att = $this->Common_mdl->GetAllWithWhere('leads_attachments', 'lead_id', $l_id);
		$ht = '';
		foreach ($att as $att_key => $att_value) {
			// $ht .='<div class="row"><div class="display-block lead-attachment-wrapper"><div class="col-md-10"><div class="pull-left"></div><a href="'. base_url() . $path .'/'.$att_value['attachments'].'" target="_blank">'.$att_value['attachments'].'</a><p class="text-muted"></p></div><div class="col-md-2 text-right"><a href="#" class="text-danger" onclick="delete_lead_attachment('.$att_value['id'].','.$l_id.'); return false;"><i class="fa fa fa-times"></i></a></div><div class="clearfix"></div><hr></div></div>';   
			//  $ht .='<div class="row"><div class="display-block lead-attachment-wrapper"><div class="col-md-10"><div class="pull-left"></div><a href="'. base_url() . $path .'/'.$att_value['attachments'].'" target="_blank">'.$att_value['attachments'].'</a><p class="text-muted"></p></div><div class="col-md-2 text-right"><a href="#" class="text-danger" onclick="delete_lead_attachment('.$att_value['id'].','.$l_id.'); return false;"><i class="fa fa fa-times"></i></a></div><div class="clearfix"></div><hr></div></div>'; 
			$open = 1;
			$close = 2;

			$fn = "'delete_lead_attachment'";
			$ht .= '<div class="data-quote-03">
                <div class="display-block lead-attachment-wrapper">
                <div class="col-md-10">
                <div class="pull-left"></div>
                <a href="' . base_url() . $path . '/' . $att_value['attachments'] . '" target="_blank">' . $att_value['attachments'] . '</a>
                <p class="text-muted"></p>
                </div>';
			if ($_SESSION['permission']['Leads']['delete'] == '1') {
				$ht .= '<div class="col-md-2 text-right">
                <a href="#" data-target="#deleteconfirmation" data-toggle="modal" class="text-danger" data-id="' . $att_value['id'] . '" onclick="return delete_data(this,' . $fn . ')"><i class="fa fa fa-times"></i></a>
                </div>';
			}
			$ht .= '<div class="clearfix"></div>                    
                <hr>
                </div>
                </div>';
		}
		return $ht;
	}
	function delete_lead_attachment()
	{
		$id = $_POST['id'];
		// $l_id = $_POST['l_id'];
		$data = $this->Common_mdl->select_record('leads_attachments', 'id', $id);
		$del = $this->Common_mdl->delete('leads_attachments', 'id', $id);
		if ($del) {
			/** for activity logs **/
			$activity_datas['log'] = "Delete Attached---" . $data['attachments'];
			$activity_datas['createdTime'] = time();
			$activity_datas['module'] = 'Lead';
			$activity_datas['user_id'] = $_SESSION['admin_id'];
			$activity_datas['module_id'] = $data['lead_id'];
			$activity_datas['sub_module'] = 'Lead Attachment';
			$activity_datas['sub_module_id'] = $id;
			$this->Common_mdl->insert('activity_log', $activity_datas);

			/** end of activity logs **/
			$ht = $this->get_attachments($data['lead_id']);  //atchments 

		} else {
			$ht = 'Attachment does not deleted!!!';
		}
		echo $ht;
	}

	public function add_proposal()
	{
		/*echo '<pre>';
        print_r($_POST);*/
		$data['user_id'] = $_SESSION['userId'];
		$data['lead_id'] = $_POST['lead_id'];
		$data['rel_type'] = $_POST['rel_type'];
		$data['subject'] = $_POST['subject'];
		$data['proposal_status'] = $_POST['status'];
		$data['assigned_to'] = $_POST['assigned'];
		$data['related_to'] = $_POST['rel_type'];
		$data['proposal_to'] = $_POST['proposal_to'];
		$data['address'] = $_POST['address'];
		$data['start_date'] = $_POST['cur_date'];
		$data['till_date'] = $_POST['open_till'];
		$data['currency'] = $_POST['currency'];
		$data['city'] = $_POST['city'];
		$data['state'] = $_POST['state'];
		$data['country'] = $_POST['country'];
		$data['zip_code'] = $_POST['zip'];
		$data['email_address'] = $_POST['email'];
		$data['phone'] = $_POST['phone'];
		$data['tags'] = $_POST['tags'];
		$data['allow_comments'] = $_POST['allow_comments'];
		$data['show_qty_as'] = $_POST['show_quantity_as'];
		$data['sub_total'] = $_POST['subtotal'];
		$data['discount_percent'] = $_POST['discount_percent'];
		$data['discount_total'] = $_POST['discount_total'];
		$data['adjustment'] = $_POST['adjustment'];

		$result = $this->Common_mdl->insert('proposal', $data);
		if ($result) {
			$proposal_id = $this->db->insert_id();
			$invoice['proposal_id'] = $proposal_id;
			$invoice['item'] = $_POST['description'];
			$invoice['long_description'] = $_POST['long_description'];
			$invoice['qty'] = $_POST['quantity'];
			$invoice['rate'] = $_POST['rate'];
			$invoice['amount'] = $_POST['unit'];
			$this->Common_mdl->insert('proposal_invoice', $invoice);
		}
	}

	public function add_new_reminder()
	{

		// print_r($_POST);
		// die;
		$data['reminder_date']  = date("Y-m-d", strtotime(implode('-', array_reverse(explode('/', $_POST['reminder_date'])))));
		$data['user_id'] = $_SESSION['userId'];
		$data['lead_id'] = $_POST['lead_id'];
		$data['type'] = $_POST['rel_type'];
		//$data['reminder_date'] = $_POST['reminder_date'];
		$data['description'] = $_POST['reminder_desc'];
		//   $data['assigned_staff_id'] = implode(',',$_POST['staff_id']);//12-06-2018 
		$team = array();
		$department = array();
		$assign = array();
		//$data['assigned'] = implode(',',$_POST['assigned']);
		/** for assign team and department  14-06-2018 **/
		// if(isset($_POST['staff_id']))
		// {
		//     foreach ($_POST['staff_id'] as $key => $value) {
		//       $ex_val=explode('_', $value);
		//       if(count($ex_val)>1)
		//       {
		//         if($ex_val[0]=='tm')
		//         {
		//            array_push($team, $ex_val[1]);
		//         }
		//         if($ex_val[0]=='de')
		//         {
		//             array_push($department, $ex_val[1]);
		//         }
		//       }
		//       else
		//       {
		//          array_push($assign, $ex_val[0]);
		//       }

		//     }

		//     $data['assigned_staff_id'] = implode(',',$assign);
		//     $data['team'] = implode(',',$team);
		//     $data['department'] = implode(',',$department);

		// }
		/** end of 14-06-2018 **/

		$data['email_sent'] = $_POST['email_sent'];
		$data['old_reminder_date'] = $data['reminder_date'];
		$data['CreateTime'] = time();
		$data['subject'] = $_POST['reminder_sub'];
		$data['assigned_staff_id'] = $_POST['assign_role'];
		
		if ($this->Common_mdl->insert('lead_reminder', $data)) {
			/** for activity logs **/
			$id = $this->db->insert_id();
			$this->Common_mdl->add_assignto($_POST['assign_role'], $_POST['lead_id'], 'LEADS_REMINDER');
			// $this->Common_mdl->usertask_modify($_POST['assign_role'],$_POST['lead_id'],'lead_reminder','LEADS_REMINDER');
			$this->Common_mdl->usertask_modify($_POST['assign_role'], $_POST['lead_id'], 'lead_reminder', 'LEADS_REMINDER');

			$this->Common_mdl->usertask_modify($_POST['assign_role'], $_POST['lead_id'], 'add_new_task', 'TASK');
			$this->Common_mdl->add_assignto($_POST['assign_role'], $_POST['lead_id'], 'LEADS', 1);

			$assign_user_id = Get_Module_Assigees('LEADS_REMINDER', $_POST['lead_id']);
			// print_r( $assign_user_id);
			// exit;
			$activity_datas['log'] = "Reminder Imported---" . implode(',', $assign_user_id);
			$activity_datas['createdTime'] = time();
			$activity_datas['module'] = 'Lead';
			$activity_datas['user_id'] = $_SESSION['admin_id'];
			$activity_datas['module_id'] = $_POST['lead_id'];
			$activity_datas['sub_module'] = 'Reminder';
			$activity_datas['sub_module_id'] = $id;
			$this->Common_mdl->insert('activity_log', $activity_datas);
			/** end of activity logs **/
			$data['lead_id'] = $_POST['lead_id'];
			/** for leads mail send for current date **/
			$qry_data = $this->db->query("SELECT * FROM `lead_reminder` where email_sent='yes' and type='Leads' and reminder_date=CURDATE() and id=" . $id . " ")->result_array();
			if (count($qry_data) > 0) {
				$content = 'You Have Assign To Email Reminder for $data["type"]';
				foreach ($qry_data as $key => $value) {
					$this->web_lead_reminder_email($value['id']);
					$this->Common_mdl->Send_Notification('LEADS_REMINDER', 'Leads_Reminder', $value['id'], $content);
				}
			}
			/** end of current date **/

			$data['sql'] = $this->Common_mdl->GetAllWithWhere('lead_reminder', 'lead_id', $_POST['lead_id']);
			$this->load->view('leads/leads_reminder', $data);
		}
	}

	public function edit_reminder()
	{
		$data['reminder_date']  = date("Y-m-d", strtotime(implode('-', array_reverse(explode('/', $_POST['reminder_date'])))));
		$data['type'] = $_POST['rel_type'];
		$data['description'] = $_POST['reminder_desc'];
		$data['email_sent'] = $_POST['email_sent'];

		$this->Common_mdl->add_assignto($_POST['assign_role'], $_POST['lead_id'], 'LEADS_REMINDER', 1);
		// $this->Common_mdl->usertask_modify($_POST['assign_role'],$_POST['lead_id'],'lead_reminder','LEADS_REMINDER');
		$this->Common_mdl->usertask_modify($_POST['assign_role'], $_POST['lead_id'], 'lead_reminder', 'LEADS_REMINDER');

		$this->Common_mdl->usertask_modify($_POST['assign_role'], $_POST['lead_id'], 'add_new_task', 'TASK');
		$this->Common_mdl->add_assignto($_POST['assign_role'], $_POST['lead_id'], 'LEADS', 1);

		if ($this->Common_mdl->update('lead_reminder', $data, 'id', $_POST['id'])) {
			/** ACTIVITY LOGS **/
			if (isset($_POST['assign_role'])) {
				$assign_user_id = Get_Module_Assigees('LEADS_REMINDER', $_POST['lead_id']);
				$activity_datas['log'] = "Reminder Updated---" . implode(',', $assign_user_id);
			} else {
				$activity_datas['log'] = "Reminder Updated---";
			}
			$activity_datas['createdTime'] = time();
			$activity_datas['module'] = 'Lead';
			$activity_datas['user_id'] = $_SESSION['admin_id'];
			$activity_datas['module_id'] = $_POST['lead_id'];
			$activity_datas['sub_module'] = 'Reminder';
			$activity_datas['sub_module_id'] = $_POST['id'];
			$this->Common_mdl->insert('activity_log', $activity_datas);
			/** end of activity logs **/
			/** for leads mail send for current date **/
			$qry_data = $this->db->query("SELECT * FROM `lead_reminder` where email_sent='yes' and type='Leads' and reminder_date=CURDATE() and id=" . $_POST['id'] . " ")->result_array();


			if (count($qry_data) > 0) {
				foreach ($qry_data as $key => $value) {
					$reminder_date = $value['reminder_date'];
					$old_reminder_date = $value['old_reminder_date'];
					if ($reminder_date != $value['old_reminder_date']) {
						$this->web_lead_reminder_email($value['id']);
						$datas['old_reminder_date'] = $reminder_date;
						$datas['mail'] = 'send';
						$this->Common_mdl->update('lead_reminder', $datas, 'id', $_POST['id']);
					}
				}
			}
			/** end of current date **/
			$data['lead_id'] = $_POST['lead_id'];

			$data['sql'] = $this->Common_mdl->GetAllWithWhere('lead_reminder', 'lead_id', $_POST['lead_id']);
			$this->load->view('leads/leads_reminder', $data);
		} else {
			$data['lead_id'] = $_POST['lead_id'];
			$data['sql'] = $this->Common_mdl->GetAllWithWhere('lead_reminder', 'lead_id', $_POST['lead_id']);
			$this->load->view('leads/leads_reminder', $data);
		}
	}

	public function delete_reminder()
	{
		$id = $_POST['id'];
		$reminder_data = $this->Common_mdl->select_record('lead_reminder', 'id', $id);
		$assign_user_id = Get_Module_Assigees('LEADS_REMINDER', $reminder_data['lead_id']);
		if ($this->Common_mdl->delete('lead_reminder', 'id', $id)) {
			/** activity logs **/
			$activity_datas['log'] = "Delete Reminder---" . implode(',', $assign_user_id);
			$activity_datas['createdTime'] = time();
			$activity_datas['module'] = 'Lead';
			$activity_datas['user_id'] = $_SESSION['admin_id'];
			$activity_datas['module_id'] = $reminder_data['lead_id'];
			$activity_datas['sub_module'] = 'Reminder';
			$activity_datas['sub_module_id'] = $id;
			$this->Common_mdl->insert('activity_log', $activity_datas);
			/** end of activity logs **/
			$data['lead_id'] = $reminder_data['lead_id'];
			$data['sql'] = $this->Common_mdl->GetAllWithWhere('lead_reminder', 'lead_id', $reminder_data['lead_id']);
			$this->load->view('leads/leads_reminder', $data);
		}
	}

	public function reminder_excel()
	{

		$filename = 'Reminder_' . date('Ymd') . '.csv';

		$leadid = $_GET['leadid'];
		$sql = 'select id,description,reminder_date,assigned_staff_id from lead_reminder where lead_id=' . $leadid . '';
		$ex_val = Get_Module_Assigees('LEADS_REMINDER', $leadid);

		$i = 1;
		$var_array = array();
		//   print_r($ex_val);
		if (count($ex_val) > 0) {
			foreach ($ex_val as $key => $value) {

				$user_name = $this->Common_mdl->select_record('user', 'id', $value);
				array_push($var_array, $user_name['crm_name']);
			}
		}


		$data['reminder_list'] = $this->db->query($sql)->result_array();

		header("Content-Description: File Transfer");
		header("Content-Disposition: attachment; filename=$filename");
		header("Content-Type: application/csv; ");


		$file = fopen('php://output', 'w');
		$header = array("S.NO", "Description", "Date", "Remind");
		fputcsv($file, $header);
		$i = 0;

		foreach ($data['reminder_list'] as $key => $line) {

			if ($line['id']) {
				$i++;
				$line['id'] = $i;
			}

			//if($line['role']==1){ $line['role']="Super Admin";}
			if ($line['description'] != '') {
				$line['description'] = $line['description'];
			}
			if ($line['reminder_date'] != '') {
				$line['reminder_date'] = $line['reminder_date'];
			}
			if ($line['assigned_staff_id'] != '' || $line['assigned_staff_id'] == '') {
				// $user_name = $this->Common_mdl->select_record('user','id',$line['assigned_staff_id']);
				// $line['assigned_staff_id'] = $user_name['crm_name'];
				$line['assigned_staff_id'] = implode(",", $var_array);
			}

			fputcsv($file, $line);
		}


		fclose($file);
		exit;
	}


	public function reminder_pdf()
	{
		$leadid = $_GET['leadid'];
		$sql = 'select id,description,reminder_date,assigned_staff_id from lead_reminder where lead_id=' . $leadid . '';
		$data['ex_val'] = Get_Module_Assigees('LEADS_REMINDER', $leadid);

		$data['sql'] = $this->db->query($sql)->result_array();
		//$this->load->view('users/task_pdf', $data);
		$this->load->library('pdf');
		$this->pdf->load_view('leads/reminder_pdf', $data);
		$this->pdf->render();
		$this->pdf->stream("reminder.pdf");
	}
	public function reminder_html()
	{
		$leadid = $_GET['leadid'];
		$sql = 'select id,description,reminder_date,assigned_staff_id from lead_reminder where lead_id=' . $leadid . '';
		$data['ex_val'] = Get_Module_Assigees('LEADS_REMINDER', $leadid);
		$data['sql'] = $this->db->query($sql)->result_array();
		$this->load->view("leads/reminder_print", $data);
	}

	public function leads_task()
	{


		$l_name = $this->Common_mdl->select_record('leads', 'id', $_POST['lead_id']);
		$data['company_name'] = $l_name['company'];
		$data['firm_id'] = $_SESSION['firm_id'];
		//  $data['user_id'] = $_SESSION['userId'];
		$data['lead_id'] = $_POST['lead_id'];
		$data['task_status'] = '1';
		$data['public'] = $_POST['public'];
		$data['billable'] = '';
		$data['attach_file'] = $_POST['attach_file'];
		$data['subject'] = $_POST['task_sub'];
		$data['hour_rate'] = $_POST['hour_rate'];
		$data['start_date'] = implode('-', array_reverse(explode('-', $_POST['start_date'])));
		$data['end_date'] = implode('-', array_reverse(explode('-', $_POST['due_date'])));
		$data['priority'] = $_POST['priority'];
		$data['related_to'] = 'leads';
		// $data['lead_to'] = implode(',',$_POST['lead_to']);

		$data['lead_to '] = (isset($_POST['lead_to']) && ($_POST['lead_to'] != '')) ? implode(',', $_POST['lead_to']) : '0';
		// $data['task_status'] = 'notstarted';
		// for 14-06-2018 chng for team,dept
		/*$data['sche_repeats'] = $_POST['repeat'];
        $data['repeat_no'] = $_POST['repeat_no'];
        $data['durations'] = $_POST['durations'];
        $data['ends_on'] = $_POST['ends_on'];*/
		$data['tag'] = $_POST['tags'];
		$data['description'] = $_POST['description'];
		$data['created_date'] = time();
		//  $data['worker'] = $l_name['assigned'];
		/** for team/dept 14-06-2018 **/
		$team = array();
		$department = array();
		$assign = array();
		//$data['assigned'] = implode(',',$_POST['assigned']);
		/** for assign team and department  14-06-2018 **/
		if (isset($_POST['lead_to'])) {
			foreach ($_POST['lead_to'] as $key => $value) {
				$ex_val = explode('_', $value);
				if (count($ex_val) > 1) {
					if ($ex_val[0] == 'tm') {
						array_push($team, $ex_val[1]);
					}
					if ($ex_val[0] == 'de') {
						array_push($department, $ex_val[1]);
					}
				} else {
					array_push($assign, $ex_val[0]);
				}
			}

			$data['worker'] = implode(',', $assign);
			$data['team'] = implode(',', $team);
			$data['department'] = implode(',', $department);
		}
		$data['manager'] = $l_name['review_manager'];
		/** end of 14-06-2018 **/
		/** end of 14-06-2018 **/
		/** for newly added **/
		$data['create_by'] = $_SESSION['id'];

		/* for assigness changes is done in the assigness it reflects all the leads */
		$this->Common_mdl->usertask_modify($_POST['assign_role'], $_POST['lead_id'], 'lead_reminder', 'LEADS_REMINDER');

		$this->Common_mdl->usertask_modify($_POST['assign_role'], $_POST['lead_id'], 'add_new_task', 'TASK');
		$this->Common_mdl->add_assignto($_POST['assign_role'], $_POST['lead_id'], 'LEADS', 1);


		if ($this->Common_mdl->insert('add_new_task', $data)) {
			/** for activity logs data's **/

			$id = $this->db->insert_id();
			$this->Common_mdl->add_assignto($_POST['assign_role'], $id, 'TASK');

			$this->Common_mdl->task_setting($id);

			$this->Task_invoice_model->task_new_mail($id, 'for_add');

			///$this->Common_mdl->createparentDirectory($id);

			$activity_datas['log'] = "Task Created---" . $_POST['task_sub'];
			$activity_datas['createdTime'] = time();
			$activity_datas['module'] = 'Lead';
			$activity_datas['user_id'] = $_SESSION['id'];
			$activity_datas['module_id'] = $_POST['lead_id'];
			$activity_datas['sub_module'] = 'Task';
			$activity_datas['sub_module_id'] = $id;

			$this->Common_mdl->insert('activity_log', $activity_datas);

			/** for invoice create **/

			$invoice_id = '';
			// if((isset($_POST['billable'])&&($_POST['billable']!=''))){
			//         $invoice_id=$this->Task_invoice_model->task_billable_createInvoice($id);
			//         if($invoice_id!='')
			//         {
			//             $datas['invoice_id']=$invoice_id;
			//              $result =  $this->Common_mdl->update('add_new_task',$datas,'id',$id);
			//         }
			//     }
			/** end of invoice create **/
			/** end of activity logs datas **/
			$data['task_sql'] =  $this->db->query("select * from add_new_task where related_to='leads' and lead_id=" . $_POST['lead_id'] . "")->result_array();
			$data['lead_id'] = $_POST['lead_id'];
			$this->load->view('leads/lead_task', $data);
		}
	}

	public function leads_task_image()
	{
		/* print_r($_FILES);
        die;*/
		$files = $_FILES;
		$path = 'uploads/leads/tasks';
		$config = $this->get_upload_options($path);
		$type = explode('.', $files['file']['name']);
		$_FILES['tmp']['name'] = time() . '.' . end($type);
		$_FILES['tmp']['type'] = $files['file']['type'];
		$_FILES['tmp']['tmp_name'] = $files['file']['tmp_name'];
		$_FILES['tmp']['error'] = $files['file']['error'];
		$_FILES['tmp']['size'] = $files['file']['size'];
		if ($this->upload->do_upload('file')) {
			$data = $this->upload->data();
			echo $data['file_name'];
		}
	}

	public function delete_task()
	{
		//$lead_id = $_POST['l_id'];
		$id = $_POST['id'];
		$task_data = $this->Common_mdl->select_record('add_new_task', 'id', $id);
		if ($this->Common_mdl->delete('add_new_task', 'id', $id)) {
			/** for activity logs data **/
			$activity_datas['log'] = "Delete Task---" . $task_data['subject'];
			$activity_datas['createdTime'] = time();
			$activity_datas['module'] = 'Lead';
			$activity_datas['user_id'] = $_SESSION['admin_id'];
			$activity_datas['module_id'] = $task_data['lead_id'];
			$activity_datas['sub_module'] = 'Task';
			$activity_datas['sub_module_id'] = $id;
			$this->Common_mdl->insert('activity_log', $activity_datas);
			/** end of activity logs **/
			$data['lead_id'] = $task_data['lead_id'];
			$data['task_sql'] =  $this->db->query("select * from add_new_task where related_to='leads' and lead_id=" . $task_data['lead_id'] . "")->result_array();
			$this->load->view('leads/lead_task', $data);
		}
	}

	public function l_task_excel()
	{
		$lead_id = $_GET['leadid'];
		$filename = 'Task_' . date('Ymd') . '.csv';

		$leadid = $_GET['leadid'];
		$sql = 'select id,subject,start_date,end_date,worker,tag,priority from add_new_task where lead_id=' . $leadid . ' and related_to="leads"';

		$data['task_list'] = $this->db->query($sql)->result_array();

		header("Content-Description: File Transfer");
		header("Content-Disposition: attachment; filename=$filename");
		header("Content-Type: application/csv; ");


		$file = fopen('php://output', 'w');
		$header = array("S.NO", "Subject", "Start Date", "End Date", "Assignee", "Tag", "Priority");
		fputcsv($file, $header);
		$i = 0;

		foreach ($data['task_list'] as $key => $line) {

			if ($line['id']) {
				$i++;
				$line['id'] = $i;
			}

			//if($line['role']==1){ $line['role']="Super Admin";}
			if ($line['subject'] != '') {
				$line['subject'] = $line['subject'];
			}
			if ($line['start_date'] != '') {
				$line['start_date'] = $line['start_date'];
			}
			if ($line['end_date'] != '') {
				$line['end_date'] = $line['end_date'];
			}
			if ($line['worker'] != '') {
				$user_name = $this->Common_mdl->select_record('user', 'id', $line['worker']);
				$line['worker'] = $user_name['crm_name'];
			}
			if ($line['tag'] != '') {
				$line['tag'] = $line['tag'];
			}
			if ($line['priority'] != '') {
				$line['priority'] = $line['priority'];
			}

			fputcsv($file, $line);
		}


		fclose($file);
		exit;
	}


	public function l_task_pdf()
	{
		$leadid = $_GET['leadid'];
		$sql = 'select id,subject,start_date,end_date,worker,tag,priority from add_new_task where lead_id=' . $leadid . ' and related_to="leads"';

		$data['sql'] = $this->db->query($sql)->result_array();
		//$this->load->view('users/task_pdf', $data);
		$this->load->library('pdf');
		$this->pdf->load_view('leads/l_task_pdf', $data);
		$this->pdf->render();
		$this->pdf->stream("l_task.pdf");
	}
	public function l_task_html()
	{
		$leadid = $_GET['leadid'];
		$sql = 'select id,subject,start_date,end_date,worker,tag,priority from add_new_task where lead_id=' . $leadid . ' and related_to="leads"';
		$data['sql'] = $this->db->query($sql)->result_array();
		$this->load->view("leads/l_task_print", $data);
	}


	public function update_task()
	{
		// print_r($_POST);
		// exit;

		$l_name = $this->Common_mdl->select_record('leads', 'id', $_POST['lead_id']);

		$data['company_name'] = $l_name['company'];
		$data['user_id'] = $_SESSION['userId'];
		//$data['lead_id'] = $_POST['lead_id'];
		$data['public'] = $_POST['public'];
		$data['billable'] = '';
		$data['attach_file'] = $_POST['attach_file'];
		$data['subject'] = $_POST['task_sub'];
		$data['hour_rate'] = $_POST['hour_rate'];
		$data['start_date'] = implode('-', array_reverse(explode('-', $_POST['start_date'])));
		$data['end_date'] = implode('-', array_reverse(explode('-', $_POST['due_date'])));
		$data['priority'] = $_POST['priority'];
		$data['related_to'] = 'leads'; // 14-06-2018
		//     $data['lead_to'] = implode(',',$_POST['lead_to']);//14-06-2018 for team,dept
		//$data['sche_repeats'] = $_POST['repeat'];
		//$data['repeat_no'] = $_POST['repeat_no'];
		//$data['durations'] = $_POST['durations'];
		//$data['ends_on'] = $_POST['ends_on'];
		$data['tag'] = $_POST['tags'];
		$data['description'] = $_POST['description'];
		$data['created_date'] = time();
		/** for team/dept 14-06-2018 **/
		$team = array();
		$department = array();
		$assign = array();
		//$data['assigned'] = implode(',',$_POST['assigned']);
		/** for assign team and department  14-06-2018 **/
		// if(isset($_POST['lead_to']))
		// {
		//     foreach ($_POST['lead_to'] as $key => $value) {
		//       $ex_val=explode('_', $value);
		//       if(count($ex_val)>1)
		//       {
		//         if($ex_val[0]=='tm')
		//         {
		//            array_push($team, $ex_val[1]);
		//         }
		//         if($ex_val[0]=='de')
		//         {
		//             array_push($department, $ex_val[1]);
		//         }
		//       }
		//       else
		//       {
		//          array_push($assign, $ex_val[0]);
		//       }

		//     }

		//     $data['worker'] = implode(',',$assign);
		//     $data['team'] = implode(',',$team);
		//     $data['department'] = implode(',',$department);

		// }
		/** end of 14-06-2018 **/
		/** end of 14-06-2018 **/
		//$data['worker'] = $l_name['assigned'];
		$id = $_POST['id'];
		//$this->Common_mdl->update('add_new_task',$data,'id',$id);
		//echo $this->db->last_query();die;
		/** for newly added **/
		$this->Common_mdl->add_assignto($_POST['assign_role'], $_POST['lead_id'], 'LEADS', 1);

		$data['create_by'] = $_SESSION['id'];
		if ($this->Common_mdl->update('add_new_task', $data, 'id', $id)) {
			// $this->Common_mdl->usertask_modify($_POST['assign_role'],$_POST['lead_id'],'add_new_task','TASK');
			$this->Common_mdl->usertask_modify($_POST['assign_role'], $_POST['lead_id'], 'lead_reminder', 'LEADS_REMINDER');

			$this->Common_mdl->usertask_modify($_POST['assign_role'], $_POST['lead_id'], 'add_new_task', 'TASK');

			$this->Common_mdl->add_assignto($_POST['assign_role'], $_POST['lead_id'], 'LEADS', 1);

			//$this->Common_mdl->add_assignto($_POST['assign_role'],$id,'TASK',1);
			/** for activity logs data **/
			$activity_datas['log'] = "Task Records Updated---" . $_POST['task_sub'];
			$activity_datas['createdTime'] = time();
			$activity_datas['module'] = 'Lead';
			$activity_datas['user_id'] = $_SESSION['admin_id'];
			$activity_datas['module_id'] = $_POST['lead_id'];
			$activity_datas['sub_module'] = 'Task';
			$activity_datas['sub_module_id'] = $id;

			$this->Common_mdl->insert('activity_log', $activity_datas);
			/** end of activity logs data **/
			//echo $this->db->last_query();die;

			/** for task edit option invoice update **/
			$get_task_data = $this->Common_mdl->GetAllWithWhere('add_new_task', 'id', $id);
			/*$invoice_id=$get_task_data[0]['invoice_id'];
//echo $invoice_id."---";
$in=$id;
if($invoice_id!='')
{
   
    if((isset($_POST['billable'])&&($_POST['billable']!=''))){
            $invoice_id=$this->Task_invoice_model->task_billable_createInvoice($in);
           
        }
}
else
{
    $invoice_id='';
    if((isset($_POST['billable'])&&($_POST['billable']!=''))){
            $invoice_id=$this->Task_invoice_model->task_billable_createInvoice($in);
            if($invoice_id!='')
            {
                $for_us_datas['invoice_id']=$invoice_id;
                 $result =  $this->Common_mdl->update('add_new_task',$for_us_datas,'id',$in);
            }
        }
}*/

			/** end of invoice update **/
			$data['lead_id'] = $_POST['lead_id'];
			$data['task_sql'] =  $this->db->query("select * from add_new_task where related_to='leads' and lead_id=" . $_POST['lead_id'] . "")->result_array();

			//  $data['assign_check']=$this->Common_mdl->assign_check($_POST['lead_id']);


			$this->load->view('leads/lead_task', $data);
		}
	}




	/** rspt 17-04-2018 **/

	public function lead_settings()
	{
		$this->Security_model->chk_login();
		// $data['leads_status']=$this->Common_mdl->getallrecords('leads_status');
		// $data['leads_status']=$this->Common_mdl->GetAllWithWhere('leads_status','firm_id', $_SESSION['firm_id']);


		$data['leads_status'] = $this->Common_mdl->dynamic_status('leads_status');
		// $data['source']=$this->Common_mdl->getallrecords('source');
		$data['source'] = $this->Common_mdl->dynamic_status('source');
		$this->load->view('leads/leads_settings', $data);
	}

	public function leads_details_tab()
	{
		// $data['leads_status']=$this->Common_mdl->getallrecords('leads_status');
		// $data['source']=$this->Common_mdl->getallrecords('source');
		// $data['leads_status']=$this->Common_mdl->GetAllWithWhere('leads_status','firm_id', $_SESSION['firm_id']);


		$data['leads_status'] = $this->Common_mdl->dynamic_status('leads_status');
		$data['source'] = $this->Common_mdl->dynamic_status('source');

		$this->load->view('leads/list_detailed_tab', $data);
	}



	/** rspt 18-04-2018 **/

	public function convert_to_customer($lead_id = null, $return_view = true)
	{
		// $id = $_POST['id'];
		// $value = $_POST['status'];
		$id = $lead_id ? $lead_id : $_GET['lead_id'];

		$l_name = $this->Common_mdl->select_record('leads_status', 'status_name', 'customer');
		$status_id = $l_name['id'];
		$data['lead_status'] = $status_id;

		try {
			// if ($this->Common_mdl->update('leads', $data, 'id', $id)) {
			if (1) {
				$v = $this->leads_convert_client($id); // convert to client
				if ($v) {
					$this->leads_convert_task($id); // convert to task
					$this->copy_lead_notes_to_client();
				}
				$result = 1;
			} else {
				$result = 0;
			}
		} catch (\Exception $e) {
			$result = 0;
		}

		if ($return_view) {
			redirect('user?lead_converted=1');
		} else {
			return $result;
		}
	}

	public function convert_to_status()
	{
		$id = $_POST['id'];
		$value = $_POST['status'];

		$lead = $this->Common_mdl->select_record('leads', 'id', $id);

		$l_name = $this->Common_mdl->select_record('leads_status', 'id', $value);
		$status_id = $l_name['id'];
		$data['lead_status'] = $status_id;

		if ($this->Common_mdl->update('leads', $data, 'id', $id)) {

			if ($value == '60' && $lead['lead_status'] != '60') {
				// $this->convert_to_customer($id, false);
			}

			//echo $l_name['status_name'];
			$data['leads'] = $this->Common_mdl->select_record('leads', 'id', $id);
			// $data['leads_status']=$this->Common_mdl->GetAllWithWhere('leads_status','firm_id', $_SESSION['firm_id']);


			$data['leads_status'] = $this->Common_mdl->dynamic_status('leads_status');
			// $data['leads_status']=$this->Common_mdl->getallrecords('leads_status');
			// $data['source']=$this->Common_mdl->getallrecords('source');
			$data['source'] = $this->Common_mdl->dynamic_status('source');
			$data['countries'] = $this->Common_mdl->getallrecords('countries');
			$data['Language'] = $this->Common_mdl->getallrecords('languages');
			$data['staffs'] = $this->Common_mdl->GetAllWithWhere('user', 'role', '6');

			$data['latestrec'] = $this->Common_mdl->orderby('activity_log', 'module', 'Lead', 'desc');

			$this->load->view('leads/leads_detailed_tab_profile', $data);
			//echo "update";
		} else {
			echo "wrong";
		}
	}

	public function leads_deatiled_tab_popup($id)
	{
		$data['leads'] = $this->Common_mdl->select_record('leads', 'id', $id);
		$data['leads_status'] = $this->Common_mdl->getallrecords('leads_status');
		//  $data['source']=$this->Common_mdl->getallrecords('source');
		$data['source'] = $this->Common_mdl->dynamic_status('source');
		$data['countries'] = $this->Common_mdl->getallrecords('countries');
		$data['Language'] = $this->Common_mdl->getallrecords('languages');
		$data['staffs'] = $this->Common_mdl->GetAllWithWhere('user', 'role', '6');

		$data['latestrec'] = $this->Common_mdl->orderby('activity_log', 'module', 'Lead', 'desc');

		$this->load->view('leads/leads_detailed_tab_popup', $data);
	}

	public function reminder_edit_poup($id)
	{
		$data['reminder_id'] = $id;
		$data['staffs'] = $this->Common_mdl->GetAllWithWhere('user', 'role', '6');
		$this->load->view('leads/reminder_edit_popup', $data);
	}

	public function task_edit_popup($id)
	{
		$data['lead_id'] = $id;
		$data['staffs'] = $this->Common_mdl->GetAllWithWhere('user', 'role', '6');
		$this->load->view('leads/task_edit_popup', $data);
	}
	public function get_assigness()
	{
		$data['asssign_group'] = array();
		if (isset($_POST['id']) && isset($_POST['task_name'])) {

			$sql_val = $this->db->query("SELECT * FROM firm_assignees where module_id =" . $_POST['id'] . " AND module_name='" . $_POST['task_name'] . "'")->result_array();

			//  print_r($_POST['id']);

			foreach ($sql_val as $key1 => $value1) {
				$data['asssign_group'][] = $value1['assignees'];
			}

			$data['assign_check'] = $this->Common_mdl->assign_check($_POST['id']);

			echo json_encode($data);
		}
	}


	/** end 18-04-2018 **/

	/** start 21-04-2018 **/
	/** for source in leads **/
	public function leads_source_table()
	{
		//$data['source']=$this->Common_mdl->getallrecords('source');
		$data['source'] = $this->Common_mdl->dynamic_status('source');
		$this->load->view('leads/leads_source_table', $data);
	}
	public function leads_source_editpopup()
	{
		//  $data['source']=$this->Common_mdl->getallrecords('source');
		$data['source'] = $this->Common_mdl->dynamic_status('source');
		$this->load->view('leads/leads_source_editpopup', $data);
	}
	public function leads_source_delete($id)
	{
		$this->Common_mdl->delete('source', 'id', $id);
		//  $data['source']=$this->Common_mdl->getallrecords('source');
		$data['source'] = $this->Common_mdl->dynamic_status('source');
		$this->load->view('leads/leads_source_table', $data);
	}
	public function leads_updatesource($id)
	{
		$this->Security_model->chk_login();
		$data['source_name'] = $this->input->post('new_team');
		$this->Common_mdl->update('source', $data, 'id', $id);
		// $data['source']=$this->Common_mdl->getallrecords('source');
		$data['source'] = $this->Common_mdl->dynamic_status('source');
		$this->load->view('leads/leads_source_table', $data);
		//$this->session->set_flashdata('success', 'source Updated Successfully');
		//redirect('department/update_department/'.$id);
		//redirect('leads/lead_source');
		// redirect('leads/lead_settings');

	}
	public function leads_add_new_source()
	{
		$this->Security_model->chk_login();
		$data['source_name'] = $this->input->post('new_team');
		$data['firm_id'] = $_SESSION['firm_id'];
		$data['createdTime'] = time();
		$this->Common_mdl->insert('source', $data);
		// $this->session->set_flashdata('success', 'Source Added Successfully');
		$data['source'] = $this->Common_mdl->dynamic_status('source');
		$this->load->view('leads/leads_source_table', $data);
		//redirect('leads/lead_source');
		// redirect('leads/lead_settings');

	}

	/** for status in leads **/
	public function leads_status_table()
	{
		//  $data['leads_status']=$this->Common_mdl->getallrecords('leads_status');
		// $data['leads_status']=$this->Common_mdl->GetAllWithWhere('leads_status','firm_id', $_SESSION['firm_id']);


		$data['leads_status'] = $this->Common_mdl->dynamic_status('leads_status');
		$this->load->view('leads/leads_status_table', $data);
	}
	public function leads_status_editpopup()
	{
		//$data['leads_status']=$this->Common_mdl->getallrecords('leads_status');
		// $data['leads_status']=$this->Common_mdl->GetAllWithWhere('leads_status','firm_id', $_SESSION['firm_id']);


		$data['leads_status'] = $this->Common_mdl->dynamic_status('leads_status');
		// $data['leads_status']=$this->db->query('SELECT * FROM leads_status where id in ('.$s_id.')  order by id asc')->result_array();
		$this->load->view('leads/leads_status_editpopup', $data);
	}
	public function leads_status_delete($id)
	{
		$this->Common_mdl->delete('leads_status', 'id', $id);
		//$data['leads_status']=$this->Common_mdl->getallrecords('leads_status');
		// $data['leads_status']=$this->Common_mdl->GetAllWithWhere('leads_status','firm_id', $_SESSION['firm_id']);




		$data['leads_status'] = $this->Common_mdl->dynamic_status('leads_status');
		$this->load->view('leads/leads_status_table', $data);
	}
	public function leads_updatestatus($id)
	{
		$this->Security_model->chk_login();
		$data['status_name'] = $this->input->post('new_team');
		$this->Common_mdl->update('leads_status', $data, 'id', $id);
		//$data['leads_status']=$this->Common_mdl->getallrecords('leads_status');
		// $data['leads_status']=$this->Common_mdl->GetAllWithWhere('leads_status','firm_id', $_SESSION['firm_id']);


		$data['leads_status'] = $this->Common_mdl->dynamic_status('leads_status');
		$this->load->view('leads/leads_status_table', $data);
		//$this->session->set_flashdata('success', 'source Updated Successfully');
		//redirect('department/update_department/'.$id);
		//redirect('leads/lead_source');
		// redirect('leads/lead_settings');

	}
	public function leads_add_new_status()
	{
		$this->Security_model->chk_login();
		$data['status_name'] = $this->input->post('new_team');
		$data['firm_id'] = $_SESSION['firm_id'];
		$data['createdTime'] = time();
		$this->Common_mdl->insert('leads_status', $data);
		// $this->session->set_flashdata('success', 'Source Added Successfully');
		// $data['leads_status']=$this->Common_mdl->GetAllWithWhere('leads_status','firm_id', $_SESSION['firm_id']);


		$data['leads_status'] = $this->Common_mdl->dynamic_status('leads_status');

		$this->load->view('leads/leads_status_table', $data);
		//redirect('leads/lead_source');
		// redirect('leads/lead_settings');

	}
	/** 21-04-2018 **/
	/** 24-04-2018 **/
	public function add_note_with_timeline()
	{
		$data['notes_title'] = $this->input->post('notes_title');
		$data['notes'] = $this->input->post('notes_des');
		$data['data_uid'] = $this->input->post('notes_id');
		$data['lead_id'] = $this->input->post('id');
		$data['notes_date'] = $this->input->post('notes_date');

		//echo $this->input->post('notes_date');


		$result =  $this->db->query("select * from leads_notes where user_id=" . $_SESSION['userId'] . " and lead_id=" . $this->input->post('id') . " and data_uid='" . $this->input->post('notes_id') . "' ")->result_array();
		if (count($result) > 0) {
			// echo json_encode($result);
			// echo json_encode($result[0]['id']);
			$edit_id = $result[0]['id'];
			$this->Common_mdl->update('leads_notes', $data, 'id', $edit_id);
			$activity_datas['log'] = "Lead Notes Updated---" . $data['notes_title'];
			$activity_datas['sub_module_id'] = $edit_id;
		} else {
			$data['user_id'] = $_SESSION['userId'];
			// $data['contact_date']=date('Y-m-d');
			$data['createdTime'] = time();
			$this->Common_mdl->insert('leads_notes', $data);
			$id = $this->db->insert_id();
			$activity_datas['log'] = "Lead Notes Added---" . $data['notes_title'];
			$activity_datas['sub_module_id'] = $id;
		}


		$activity_datas['createdTime'] = time();
		$activity_datas['module'] = 'Lead';
		$activity_datas['user_id'] = $_SESSION['admin_id'];
		$activity_datas['module_id'] = $this->input->post('id');
		$activity_datas['sub_module'] = 'Lead Notes';

		$this->Common_mdl->insert('activity_log', $activity_datas);

		// $result =  $this->db->query("select * from leads_notes where user_id=".$_SESSION['userId']." and lead_id=".$this->input->post('id')." and data_uid='".$this->input->post('notes_id')."' ")->result_array();

		//echo "hai hai";
		// $rec = $this->get_leads($data['lead_id']);

		// echo $rec;
	}
	/** end 24-04-2018 **/

	/** rspt 25-04-2018 **/
	public function delete_note_with_timeline()
	{
		$lead_id = $this->input->post('lead_id');
		$note_uid = $this->input->post('note_uid');
		$result =  $this->db->query("select * from leads_notes where user_id=" . $_SESSION['userId'] . " and lead_id=" . $this->input->post('lead_id') . " and data_uid='" . $this->input->post('note_uid') . "' ")->result_array();
		if (count($result) > 0) {
			// echo json_encode($result);
			// echo json_encode($result[0]['id']);


			$delete_id = $result[0]['id'];
			$data = $this->Common_mdl->select_record('leads_notes', 'id', $delete_id);
			$this->Common_mdl->delete('leads_notes', 'id', $delete_id);
			$activity_datas['log'] = "Lead Notes Delete---" . $data['notes_title'];
			$activity_datas['createdTime'] = time();
			$activity_datas['module'] = 'Lead';
			$activity_datas['user_id'] = $_SESSION['admin_id'];
			$activity_datas['module_id'] = $lead_id;
			$activity_datas['sub_module'] = 'Lead Notes';
			$activity_datas['sub_module_id'] = $delete_id;
			$this->Common_mdl->insert('activity_log', $activity_datas);
		}

		/** for activity logs **/


		/** end of activity logs **/

		echo 'success';
	}
	/** end 25-04-2018 **/

	/** rspt 26-04-2018 **/

	public function leads_notes_contactdate()
	{
		$lead_id = $this->input->post('lead_id');

		$result =  $this->db->query("select * from leads_notes where user_id=" . $_SESSION['userId'] . " and lead_id=" . $this->input->post('lead_id') . "")->result_array();
		if (count($result) > 0) {
			foreach ($result as $key => $value) {
				$update_id = $value['id'];
				$contact_date = implode('-', array_reverse(explode('/', $_POST['contact_date'])));
				$data['contact_date'] = $contact_date;
				$this->Common_mdl->update('leads_notes', $data, 'id', $update_id);
			}
		}
	}
	/** end 26-04-2018 **/

	/** 09-05-2018 **/
	public function update_assignees()
	{
		// print_r($_POST);
		// exit;
		//$_POST['worker'] = explode(',', $_POST['worker']);
		//  $data['assigned']=(isset($_POST['worker'])&&($_POST['worker']!=''))? implode(',',$_POST['worker']):'0';
		$assign = array();
		$team = array();
		$department = array();
		// if(isset($_POST['worker']))
		//     {
		//         foreach ($_POST['worker'] as $key => $value) {
		//           $ex_val=explode('_', $value);
		//           if(count($ex_val)>1)
		//           {
		//             if($ex_val[0]=='tm')
		//             {
		//                array_push($team, $ex_val[1]);
		//             }
		//             if($ex_val[0]=='de')
		//             {
		//                 array_push($department, $ex_val[1]);
		//             }
		//           }
		//           else
		//           {
		//              array_push($assign, $ex_val[0]);
		//           }

		//         }

		//         $data['assigned'] = implode(',',$assign);
		//         $data['team'] = implode(',',$team);
		//         $data['dept'] = implode(',',$department);

		//     }

		$this->Common_mdl->add_assignto($_POST['assign_role'], $_POST['lead_id'], 'LEADS', 1);

		$this->Common_mdl->usertask_modify($_POST['assign_role'], $_POST['lead_id'], 'lead_reminder', 'LEADS_REMINDER');

		$this->Common_mdl->usertask_modify($_POST['assign_role'], $_POST['lead_id'], 'add_new_task', 'TASK');

		$ex_val = Get_Module_Assigees('LEADS', $_POST['lead_id']);

		$i = 1;
		$var_array = array();
		//   print_r($ex_val);
		if (count($ex_val) > 0) {
			foreach ($ex_val as $key => $value) {

				$user_name = $this->Common_mdl->select_record('user', 'id', $value);
				array_push($var_array, $user_name['crm_name']);
			}
		}



		// $update_data=$this->Common_mdl->update('leads',$data,'id',$_POST['lead_id']);
		$img = '';
		//     if(isset($_POST['worker']) && !empty($_POST['worker'])){
		//     foreach ($assign as $key => $value) {
		//         $getUserProfilepic = $this->Common_mdl->getUserProfilepic($value);
		//         $img .= '<img src='.$getUserProfilepic.'>';
		//     }
		// }
		$img .= '<a href="javascript:;" data-toggle="modal" data-target="#adduser_' . $_POST['lead_id'] . '" class="adduser1 user_change"><i class="fa fa-plus"></i></a>';
		$img .= implode(",", $var_array);
		echo $img;
	}
	public function update_assignees_source()
	{
		//$_POST['worker'] = explode(',', $_POST['worker']);
		$data['source'] = (isset($_POST['source']) && ($_POST['source'] != '')) ? implode(',', $_POST['source']) : '0';
		$update_data = $this->Common_mdl->update('leads', $data, 'id', $_POST['lead_id']);
		if ($_POST['source'] != '') {
			foreach ($_POST['source'] as $source_key => $source_value) {
				$source_name = $this->Common_mdl->GetAllWithWhere('source', 'id', $source_value);
				if (!empty($source_name)) {
					$leads_res = $source_name[0]['source_name'];
					if ($leads_res != '') {
						$leads_source[] = $leads_res;
					}
				}
			}
		}
		if (count($leads_source) > 0) {
			$res_source = implode(',', $leads_source);
		} else {
			$res_source = "-";
		}
		$res = '';

		$res .= '<a href="javascript:;" data-toggle="modal" data-target="#adduser_source_' . $_POST['lead_id'] . '" class="adduser1"><i class="fa fa-plus"></i></a> ';
		$res .= $res_source;
		echo $res;
	}

	public function update_assignees_status()
	{

		$this->Security_model->chk_login();
		$data['lead_status'] = (isset($_POST['status']) && ($_POST['status'] != '')) ? $_POST['status'] : '0';
		// echo $_POST['status'];
		// echo $_POST['lead_id'];
		if ($_POST['status'] != 35) {
			$update_data = $this->Common_mdl->update('leads', $data, 'id', $_POST['lead_id']);
		} else {
			$this->db->set('old_status', 'lead_status', false);

			$update_data = $this->Common_mdl->update('leads', $data, 'id', $_POST['lead_id']);
		}

		echo "start";

		$isCustomer = $this->db->query("SELECT * FROM `leads_status` where id=" . $_POST['status'] . " and  LOWER(status_name) like '%customer%'")->row_array();

		print_r($isCustomer);

		if (count($isCustomer) > 0) {
			$v = $this->leads_convert_client($_POST['lead_id']); // convert to client        
			if ($v) $this->leads_convert_task($_POST['lead_id']); // convert to task                    
		}

		echo "convert";



		// echo $_POST['status']."--".$_POST['lead_id']."--".$update_data;
		if (!isset($_POST['refresh'])) {
			//   $this->session->set_flashdata('success',"Lead status updated Successfully!!!");
		}

		$sortby_val = 'date_created';
		//  $sql = "select * from leads order by ";
		//         $team_num=array();
		//          $for_cus_team=$this->db->query("select * from team_assign_staff where FIND_IN_SET('".$_SESSION['id']."',staff_id)")->result_array();
		// foreach ($for_cus_team as $cu_team_key => $cu_team_value) {
		//     array_push($team_num,$cu_team_value['team_id']);
		// }
		// if(!empty($team_num) && count($team_num)>0){
		// $res_team_num=implode('|', $team_num);
		// }
		// else
		// {
		//     $res_team_num=0;
		// }

		//       $department_num=array();
		//       $for_cus_department=$this->db->query("SELECT * FROM `department_assign_team` where CONCAT(',', `team_id`, ',') REGEXP ',($res_team_num),'")->result_array();
		//       foreach ($for_cus_department as $cu_dept_key => $cu_dept_value) {
		//             array_push($department_num,$cu_dept_value['depart_id']);
		//         }
		//     if(!empty($department_num) && count($department_num)>0){
		//       $res_dept_num=implode('|', $department_num);
		//     }
		//     else
		//     {
		//         $res_dept_num=0;
		//     }
		//   /** end of 16-07-2018 **/
		// /** for permission 16-07-2018 **/
		// $it_session_id='';
		// $get_this_qry=$this->db->query("select * from user where id=".$_SESSION['id'])->row_array();
		// $created_id=$get_this_qry['firm_admin_id'];
		// $its_firm_admin_id=$get_this_qry['firm_admin_id'];
		// if($_SESSION['role']==6) // staff
		// {
		// $it_session_id=$_SESSION['id'];
		// }
		// else if($_SESSION['role']==5) //manager
		// {
		// $it_session_id=$created_id;
		// }
		// else if($_SESSION['role']==4)//client
		// {
		// $it_session_id=$created_id;
		// }
		// else if($_SESSION['role']==3) //director
		// {
		// $it_session_id=$created_id;
		// }
		// else if($_SESSION['role']==2) // sub admin
		// {
		//   $it_session_id=$created_id;
		// }
		// else
		// {
		$it_session_id = $_SESSION['id'];
		// }

		/** end of permission 16-07-2018 **/
		//                      $result=array();
		//   array_push($result, $it_session_id);
		//   $reassign_firm_id=$its_firm_admin_id;
		// $tot_val=1;
		//    for($z=0;$z<$tot_val;$z++)
		//    {
		//      if($reassign_firm_id==0)
		//      {
		//        $tot_val=0;
		//      }
		//      else
		//      {
		//          $get_this_qry=$this->db->query("select * from user where id=".$reassign_firm_id)->row_array();
		//          if(!empty($get_this_qry)){
		//             array_push($result, $get_this_qry['id']);
		//          $new_created_id=$get_this_qry['firm_admin_id'];
		//          $reassign_firm_id=$get_this_qry['firm_admin_id'];
		//          }
		//          else
		//          {
		//            $tot_val=0;
		//          }

		//      }

		//    }
		//     $res=(!empty($result))?implode(',',array_filter(array_unique($result))):'';

		$res = array();
		$ques = Get_Assigned_Datas('LEADS');


		$res = (!empty($ques)) ? implode(',', array_filter(array_unique($ques))) : '-1';
		//     if($res!='')
		//     {
		//        $data['leads']=$this->db->query("SELECT * FROM leads li where li.id in ($res) and li.firm_id=".$_SESSION['firm_id']."  UNION
		// SELECT * FROM leads lis where lis.id in($res) and public='on' order by id desc ")->result_array();

		//     }

		// /** end of permission 16-07-2018 **/
		// if($_SESSION['role']==6)
		// {
		//   $sql="select * from (SELECT * FROM leads li where li.user_id=".$it_session_id." UNION SELECT * FROM leads lis where lis.user_id!=".$it_session_id." and public='on' 
		// UNION SELECT * FROM leads tls where tls.user_id!=".$it_session_id." and (FIND_IN_SET('".$it_session_id."',assigned) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `dept`, ',') REGEXP ',($res_dept_num),') )  ) x
		//  order by ";
		// }
		// else if($_SESSION['role']==4)
		// {

		//   $sqls=$this->db->query("select * from client where user_id=".$_SESSION['id']." ")->row_array();
		//   $its_id=$sqls['id'];
		//   $sql="select * from (SELECT * FROM leads li where li.company=".$its_id."  UNION SELECT * FROM leads lis where lis.user_id in($res) and public='on' ) x order by ";

		// }
		// else if($_SESSION['role']==5 || $_SESSION['role']==3 || $_SESSION['role']==2)
		// {

		//  // $res=implode(',', $over_all_res);
		// $sql="select * from (SELECT * FROM leads li where li.user_id in ($res)  
		// UNION 
		// SELECT * FROM leads lis where lis.user_id in($res) and public='on' ) x order by ";
		// }
		// else
		// {

		// $res=implode(',', $over_all_res);
		// $sql="select * from (SELECT * FROM leads li where li.user_id in ($res) 
		// UNION 
		// SELECT * FROM leads lis where lis.user_id in($res) and public='on' ) x order by ";

		$sql = "select * from (SELECT * FROM leads li where li.id in ($res) 
UNION 
SELECT * FROM leads lis where lis.id in($res) and public='on' ) x order by ";

		// $data['leads']=$this->db->query("SELECT * FROM leads li where li.user_id=".$it_session_id." ")->result_array();
		// }
		if ($sortby_val == 'date_created') {
			$sql .= ' id desc';
		} elseif ($sortby_val == 'kan_ban_order') {
			$sql .= ' lead_status asc';
		} elseif ($sortby_val == 'last_contact') {
			$sql .= ' contact_date desc';
		}
		$data['leads'] = $this->db->query($sql)->result_array();
		//  $data['leads_status']=$this->Common_mdl->getallrecords('leads_status');
		// $data['leads_status']=$this->Common_mdl->GetAllWithWhere('leads_status','firm_id', $_SESSION['firm_id']);


		$data['leads_status'] = $this->Common_mdl->dynamic_status('leads_status');
		// $data['source']=$this->Common_mdl->getallrecords('source');
		$data['source'] = $this->Common_mdl->dynamic_status('source');
		$data['countries'] = $this->Common_mdl->getallrecords('countries');
		$data['Language'] = $this->Common_mdl->getallrecords('languages');
		$data['staffs'] = $this->Common_mdl->GetAllWithWhere('user', 'role', '6');

		$data['latestrec'] = $this->Common_mdl->orderby('activity_log', 'module', 'Lead', 'desc');

		$this->load->view('leads/sortby_rec', $data);
	}

	public function leads_status_count()
	{
		// $leads_status=$this->Common_mdl->getallrecords('leads_status');

		// $leads_status=$this->Common_mdl->GetAllWithWhere('leads_status','firm_id', $_SESSION['firm_id']);
		$allstaus = array();

		$leads_status = $this->Common_mdl->dynamic_status('leads_status');

		foreach (array_reverse($leads_status) as $ls_value) {
			$s_id = $ls_value['id'];
			// $sql = $this->db->query('select count(*) as i from leads where lead_status="'.$s_id.'" and user_id='.$_SESSION['id'].' ')->row_array();

			/** for permission 16-07-2018 **/
			$it_session_id = '';
			$get_this_qry = $this->db->query("select * from user where id=" . $_SESSION['id'])->row_array();
			$created_id = $get_this_qry['firm_admin_id'];
			$its_firm_admin_id = $get_this_qry['firm_admin_id'];
			// if($_SESSION['role']==6) // staff
			// {
			// $it_session_id=$_SESSION['id'];
			// }
			// else if($_SESSION['role']==5) //manager
			// {
			// $it_session_id=$created_id;
			// }
			// else if($_SESSION['role']==4)//client
			// {
			// $it_session_id=$created_id;
			// }
			// else if($_SESSION['role']==3) //director
			// {
			// $it_session_id=$created_id;
			// }
			// else if($_SESSION['role']==2) // sub admin
			// {
			//   $it_session_id=$created_id;
			// }
			// else
			// {
			//   $it_session_id=$_SESSION['id'];
			// }

			$res2 = array();


			$ques = Get_Assigned_Datas('LEADS');



			$res = (!empty($ques)) ? implode(',', array_filter(array_unique($ques))) : '-1';

			/** end of permission 16-07-2018 **/
			//                          $sql=$this->db->query("select count(*) as i from (SELECT * FROM leads li where li.user_id=".$it_session_id." and li.lead_status='".$s_id."'
			// UNION
			// SELECT * FROM leads lis where lis.user_id!=".$it_session_id." and lis.public='on' and lis.lead_status='".$s_id."'
			// UNION
			// SELECT * FROM leads tls where tls.user_id!=".$it_session_id." and tls.lead_status='".$s_id."' and (FIND_IN_SET('".$it_session_id."',assigned) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `dept`, ',') REGEXP ',($res_dept_num),') )) x ")->row_array();
			// $result=array();
			//    array_push($result, $it_session_id);
			//    $reassign_firm_id=$its_firm_admin_id;
			//  $tot_val=1;
			//     for($z=0;$z<$tot_val;$z++)
			//     {
			//       if($reassign_firm_id==0)
			//       {
			//         $tot_val=0;
			//       }
			//       else
			//       {
			//           $get_this_qry=$this->db->query("select * from user where id=".$reassign_firm_id)->row_array();
			//           if(!empty($get_this_qry)){
			//              array_push($result, $get_this_qry['id']);
			//           $new_created_id=$get_this_qry['firm_admin_id'];
			//           $reassign_firm_id=$get_this_qry['firm_admin_id'];
			//           }
			//           else
			//           {
			//             $tot_val=0;
			//           }

			//       }

			//     }
			//      $res=(!empty($result))?implode(',',array_filter(array_unique($result))):'';

			// /** end of permission 16-07-2018 **/
			// if($_SESSION['role']==6)
			// {
			//   $sql=$this->db->query("select count(*) as i from (SELECT * FROM leads li where li.user_id=".$it_session_id." and  li.lead_status='".$s_id."'
			// UNION
			// SELECT * FROM leads lis where lis.user_id!=".$it_session_id." and public='on' and  lis.lead_status='".$s_id."'
			// UNION
			// SELECT * FROM leads tls where tls.user_id!=".$it_session_id." and (FIND_IN_SET('".$it_session_id."',assigned) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `dept`, ',') REGEXP ',($res_dept_num),') )  and tls.lead_status='".$s_id."' ) x
			// ")->row_array();
			// }
			// else if($_SESSION['role']==4)
			// {

			//   $sql=$this->db->query("select * from client where user_id=".$_SESSION['id']." ")->row_array();
			//   $its_id=$sql['id'];
			//   $sql=$this->db->query("select count(*) as i from (SELECT * FROM leads li where li.company=".$its_id." and  li.lead_status='".$s_id."' UNION
			// SELECT * FROM leads lis where lis.user_id in($res) and public='on' and lis.lead_status='".$s_id."') x")->row_array();

			// }
			// else if($_SESSION['role']==5 || $_SESSION['role']==3 || $_SESSION['role']==2)
			// {

			//  // $res=implode(',', $over_all_res);
			// $sql=$this->db->query("select count(*) as i from (SELECT * FROM leads li where li.user_id in ($res) and li.lead_status='".$s_id."'
			// UNION
			// SELECT * FROM leads lis where lis.user_id in($res) and public='on' and lis.lead_status='".$s_id."') x ")->row_array();
			// }
			// else
			// {

			// $res=implode(',', $over_all_res);
			$sql = $this->db->query("select count(*) as i from (SELECT * FROM leads li where li.id in ($res) and li.lead_status='" . $s_id . "'
UNION
SELECT * FROM leads lis where lis.id in($res) and public='on' and lis.lead_status='" . $s_id . "') x ")->row_array();
			// $data['leads']=$this->db->query("SELECT * FROM leads li where li.user_id=".$it_session_id." ")->result_array();
			// }

			array_push($allstaus, $sql['i']);
?>

			<div class="junk-lead color-junk2 col-xs-12 col-sm-6 col-md-3 status_filter_new" id="<?php echo $ls_value['id']; ?>" data-id="<?php echo $ls_value['status_name']; ?>" data-searchCol="status_TH">
				<div class="lead-point1">
					<strong id="<?php echo $ls_value['status_name']; ?>"><?php echo $sql['i']; ?>
					</strong>

					<span id="<?php echo $ls_value['id']; ?>" class="status_filter status_filter"><?php echo $ls_value['status_name']; ?></span>

				</div>
			</div>
		<?php } ?>

		<div class="junk-lead  color-junk2 col-xs-12 col-sm-6 col-md-2 status_filter_new quote-colors<?php  ?>  <?php $strlower = strtolower(preg_replace("~[\\\\/:*?'&,<>|]~", '', 'All')); ?>" id="" data-status="All" data-id="" data-searchCol="status_TH">
			<div class="lead-point1">
				<strong id=""><?php echo array_sum($allstaus); ?></strong>
				<span id="" class="status_filter">All</span>
			</div>
		</div>

<?php

	}

	public function kanban_status_dashboad()
	{
		/** **/
		$data['leads'] = $$this->Common_mdl->GetAllWithWhere('leads', 'firm_id', $_SESSION['firm_id']);

		///$data['leads_status']=$this->Common_mdl->getallrecords('leads_status');
		// $data['source']=$this->Common_mdl->getallrecords('source');
		// $data['leads_status']=$this->Common_mdl->GetAllWithWhere('leads_status','firm_id', $_SESSION['firm_id']);


		$data['leads_status'] = $this->Common_mdl->dynamic_status('leads_status');
		$data['source'] = $this->Common_mdl->dynamic_status('source');
		$data['countries'] = $this->Common_mdl->getallrecords('countries');
		$data['Language'] = $this->Common_mdl->getallrecords('languages');
		$data['staffs'] = $this->Common_mdl->GetAllWithWhere('user', 'role', '6');

		$data['latestrec'] = $this->Common_mdl->orderby('activity_log', 'module', 'Lead', 'desc');

		/** **/
		$this->load->view('leads/sortby_rec', $data);
	}

	/** end 09-05-2018 **/

	/* 30.05.2018 */

	public function alltasks_assign()
	{

		$task_id = explode(',', $_POST['task_id']);
		$staff_id = implode(',', $_POST['staff_id']);

		//$id_values=count($task_id); 
		// $worker=array();

		if (!count($task_id) || !count($staff_id)) {
			echo json_encode(["status" => 0]);
			die;
		}
		$assign = array();
		$team = array();
		$department = array();
		foreach ($_POST['staff_id'] as $key => $value) {
			$ex_val = explode('_', $value);
			if (count($ex_val) > 1) {
				if ($ex_val[0] == 'tm') {
					array_push($team, $ex_val[1]);
				}
				if ($ex_val[0] == 'de') {
					array_push($department, $ex_val[1]);
				}
			} else {
				array_push($assign, $ex_val[0]);
			}
		}

		foreach ($task_id as $id) {
			$query = $this->db->query("select * from leads where id='" . $id . "'")->row_array();

			$new_assign = array_diff($assign, explode(',', $query['assigned']));
			$new_team = array_diff($team, explode(',', $query['team']));
			$new_dept = array_diff($department, explode(',', $query['dept']));

			$new_assign = (count($new_assign) > 0 ? $query['assigned'] . "," . implode(',', $new_assign) : $query['assigned']);
			$new_team = (count($new_team) > 0 ? $query['team'] . "," . implode(',', $new_team) : $query['team']);
			$new_dept = (count($new_dept) > 0 ? $query['dept'] . "," . implode(',', $new_dept) : $query['dept']);

			$data['assigned'] = $new_assign;
			$data['team'] = $new_team;
			$data['dept'] = $new_dept;

			$update_data = $this->Common_mdl->update('leads', $data, 'id', $id);
			//  $update=$this->db->query("UPDATE `leads` SET `assigned`='".$worker."' where id='".$task_id[$i]."'");
			//$worker=array();
		}
		$data['status'] = 1;
		echo json_encode($data);
	}
	/* 30.05.2018 */
	/*********************15-06-2018 ****************************/
	/** mail send for contact today **/
	public function leads_contact_date_mail()
	{
		$qry_data = $this->db->query("SELECT * FROM `leads` where contact_today='off' and contact_date=CURDATE()")->result_array();
		if (count($qry_data) > 0) {
			foreach ($qry_data as $key => $value) {
				$this->web_lead_email($value['id']);
			}
		}
	}

	/** end of contact today **/
	/** reminder email function **/
	public function lead_reminder_mail()
	{

		$qry_data = $this->db->query("SELECT * FROM `lead_reminder` where email_sent='yes' and type='Leads' and reminder_date=CURDATE()")->result_array();
		if (count($qry_data) > 0) {
			foreach ($qry_data as $key => $value) {
				$this->web_lead_reminder_email($value['id']);
			}
		}
	}

	public function web_lead_reminder_email($reminder_id = false)
	{
		$get_leads_data = $this->Common_mdl->GetAllWithWhere('lead_reminder', 'id', $reminder_id);
		//$get_lead_name=$get_leads_data[0]['name'];

		$ex_val = Get_Module_Assigees('LEADS_REMINDER', $get_leads_data[0]['lead_id']);

		$this->db->select('name');
		$this->db->where('id', $get_leads_data[0]['lead_id']);
		$q = $this->db->get('leads');
		$data = $q->result_array();

		$get_lead_name = $data[0]['name'];
		$lead_id = $get_leads_data[0]['lead_id'];
		//$get_assigned=$get_leads_data[0]['assigned_staff_id'];
		$get_team = $get_leads_data[0]['team'];
		$get_department = $get_leads_data[0]['department'];
		$get_reminder_des = $get_leads_data[0]['description'];

		if ($ex_val != '') {
			foreach ($ex_val as $key => $value) {
				$get_user_data = $this->Common_mdl->GetAllWithWhere('user', 'id', $value);
				if (count($get_user_data) > 0) {
					$it_name = $get_user_data[0]['crm_name'];
					$it_email = $get_user_data[0]['crm_email_id'];
					if ($it_email != '') {
						$this->weblead_email_reminder($get_lead_name, $lead_id, $it_name, $it_email, 'assign', '', '', 'reminder', $get_reminder_des);
					}
				}
			}
		}
		//  if($get_team!='')
		// {
		//  foreach (explode(',',$get_team) as $tm_key => $tm_value) {

		//     $get_team_data=$this->Common_mdl->GetAllWithWhere('team','id',$tm_value);
		//     if(count($get_team_data)>0){
		//       $team_name=$get_team_data[0]['team'];
		//       $get_teamstaff_data=$this->Common_mdl->GetAllWithWhere('team_assign_staff','team_id',$tm_value);
		//       if(count($get_teamstaff_data)>0){
		//           $staff_ids=$get_teamstaff_data[0]['staff_id'];
		//          if($staff_ids!='')
		//          {
		//              foreach (explode(',',$staff_ids) as $key => $value) {
		//                 $get_user_data=$this->Common_mdl->GetAllWithWhere('user','id',$value);
		//                 if(count($get_user_data)>0){
		//                 //echo $get_user_data[0]['crm_email_id'];
		//                   $it_name=$get_user_data[0]['crm_name'];
		//                   $it_email=$get_user_data[0]['crm_email_id'];
		//                    if($it_email!=''){
		//                     $this->weblead_email_reminder($get_lead_name,$lead_id,$it_name,$it_email,'team',$team_name,'','reminder',$get_reminder_des);
		//                    }
		//               }
		//              }
		//          }
		//       }
		//   }
		//  }

		// }

		// if($get_department!='')
		// {
		//   foreach (explode(',', $get_department) as $de_key => $de_value) {
		//        $get_dept_data=$this->Common_mdl->GetAllWithWhere('department_permission','id',$de_value);
		//   if(count($get_dept_data)>0){
		//        $department_name=$get_dept_data[0]['new_dept'];
		//      //  echo $department_name."--dept--";
		//       $get_deptteam_data=$this->Common_mdl->GetAllWithWhere('department_assign_team','depart_id',$de_value);
		//   if(count($get_deptteam_data)>0){
		//        $departmentteam_id=$get_deptteam_data[0]['team_id'];
		//     //   echo $departmentteam_id;
		//        foreach (explode(',',$departmentteam_id) as $tm_key => $tm_value) {
		//           $get_team_data=$this->Common_mdl->GetAllWithWhere('team','id',$tm_value);
		//   if(count($get_team_data)>0){
		//           $team_name=$get_team_data[0]['team'];

		//           $get_teamstaff_data=$this->Common_mdl->GetAllWithWhere('team_assign_staff','team_id',$tm_value);
		//           if(count($get_teamstaff_data)>0){
		//          $staff_ids=$get_teamstaff_data[0]['staff_id'];
		//          if($staff_ids!='')
		//          {
		//              foreach (explode(',',$staff_ids) as $key => $value) {
		//                 $get_user_data=$this->Common_mdl->GetAllWithWhere('user','id',$value);
		//                 if(count($get_user_data)>0){
		//                // echo $get_user_data[0]['crm_email_id'];
		//                   $it_name=$get_user_data[0]['crm_name'];
		//                   $it_email=$get_user_data[0]['crm_email_id'];
		//                    if($it_email!=''){
		//                     $this->weblead_email_reminder($get_lead_name,$lead_id,$it_name,$it_email,'department',$team_name,$department_name,'reminder',$get_reminder_des);
		//                    }
		//               }
		//              }
		//          }
		//        }
		//        }
		//     }
		//    }
		//    }
		//    }
		// }


	}
	public function weblead_email_reminder($lead_name, $lead_id, $name, $email_id, $field, $team, $depart, $reminder, $des)
	{

		$get_val = array();
		$data2['username'] = $name;

		$EMAIL_TEMPLATE = $this->Common_mdl->getTemplates(32);







		//    $user_id = $query['id'];
		// if($field=='team'){
		//   $email_content="This Lead ".$lead_name." Assign You via This Team ".$team ;
		// }
		// else if($field=='department')
		// {
		//  $email_content="This Lead ".$lead_name." Assign You via This Department ".$depart ;
		// }
		// else
		// {
		//  $email_content="This Lead ".$lead_name." Assign You " ;
		// }
		// $email_subject=' Lead Reminder';
		//  $email_content  = 'Your Reminder - '.$des;

		$url  = '<a href=' . base_url() . 'leads/leads_detailed_tab/' . $lead_id . ' style="display: inline-block;text-align: center;color: #00aeef;text-decoration: none;font-family:;background: #00aeef;color: #fff;font-weight: 600;padding: 5px 10px;border-radius: 3px;">Click Here To view Lead</a>';

		$a1  =   array(
			' ::Message::' => $des,

			'::URL::' => $url,

		);
		if (!empty($EMAIL_TEMPLATE)) {
			$EMAIL_TEMPLATE = end($EMAIL_TEMPLATE);



			//   $data2['username'] = $it_name;
			$email_subject  = html_entity_decode($EMAIL_TEMPLATE['subject']);
			$data2['email_contents']  = html_entity_decode($EMAIL_TEMPLATE['body']);

			$data2['email_contents'] =  strtr($data2['email_contents'], $a1);
			$data2['title'] = html_entity_decode($EMAIL_TEMPLATE['title']);
			//email template
			$body = $this->load->view('remainder_email.php', $data2, TRUE);

			// echo $body;
			// exit;

			$get_val = $this->Common_mdl->select_record('leads', 'id', $lead_id);

			$get_val = (isset($get_val['firm_id']) && $get_val['firm_id'] != '') ? $get_val['firm_id'] : '';


			$send = firm_settings_send_mail($get_val, $it_email, $email_subject, $body);
		}


		//   $data2['title']='You Have a new reminder from this Lead - '.$lead_name.'';
		//   //$data2['link']=base_url();
		//    //email template
		//   $body = $this->load->view('remainder_email.php',$data2, TRUE);
		//    //redirect('login/email_format');

		//   $this->load->library('email');
		//   $this->email->set_mailtype("html");

		//   $get_val=$this->Common_mdl->select_record('leads','id',$lead_id);

		//   $get_val=(isset($get_val ['firm_id']) && $get_val ['firm_id']!='' ) ? $get_val ['firm_id']:'';
		// //  $get_user_data=$this->Common_mdl->select_record('user','id',$get_val)
		//   $get_user_data = $this->Common_mdl->select_record('admin_setting', 'firm_id' , $get_val);

		//   $sender_id=(isset($get_user_data['company_email']) && $get_user_data['company_email']!='') ? $get_user_data['company_email']:'info@remindoo.org';


		//    //$this->mailsettings();
		//    //$this->email->from('info.techleaf@gmail.com');

		//   $this->email->from($sender_id);
		// //   $this->email->to('shanmugapriya.techleaf@gmail.com');$email_id
		//    $this->email->to($email_id);
		//    $this->email->subject($email_subject);
		//    $this->email->message($body);
		//    $send = $this->email->send();
		//    if($send)
		//    {
		//     // echo "email send ";
		//    }
		//    else
		//    {
		//     // echo "email not send ";
		//    }
	}


	// for email send for leads 

	public function web_lead_email($lead_id = false)
	{
		$get_leads_data = $this->Common_mdl->GetAllWithWhere('leads', 'id', $lead_id);
		$get_lead_name = $get_leads_data[0]['name'];
		$get_lead_id = $lead_id;
		$get_assigned = $get_leads_data[0]['assigned'];
		$get_team = $get_leads_data[0]['team'];
		$get_department = $get_leads_data[0]['dept'];

		if ($get_assigned != '') {
			foreach (explode(',', $get_assigned) as $key => $value) {
				$get_user_data = $this->Common_mdl->GetAllWithWhere('user', 'id', $value);
				if (count($get_user_data) > 0) {
					$it_name = $get_user_data[0]['crm_name'];
					$it_email = $get_user_data[0]['crm_email_id'];
					if ($it_email != '') {
						$this->weblead_email($get_lead_name, $lead_id, $it_name, $it_email, 'assign', '', '');
					}
				}
			}
		}
		if ($get_team != '') {
			foreach (explode(',', $get_team) as $tm_key => $tm_value) {

				$get_team_data = $this->Common_mdl->GetAllWithWhere('team', 'id', $tm_value);
				if (count($get_team_data) > 0) {
					$team_name = $get_team_data[0]['team'];
					$get_teamstaff_data = $this->Common_mdl->GetAllWithWhere('team_assign_staff', 'team_id', $tm_value);
					if (count($get_teamstaff_data) > 0) {
						$staff_ids = $get_teamstaff_data[0]['staff_id'];
						if ($staff_ids != '') {
							foreach (explode(',', $staff_ids) as $key => $value) {
								$get_user_data = $this->Common_mdl->GetAllWithWhere('user', 'id', $value);
								if (count($get_user_data) > 0) {
									//echo $get_user_data[0]['crm_email_id'];
									$it_name = $get_user_data[0]['crm_name'];
									$it_email = $get_user_data[0]['crm_email_id'];
									if ($it_email != '') {
										$this->weblead_email($get_lead_name, $lead_id, $it_name, $it_email, 'team', $team_name, '');
									}
								}
							}
						}
					}
				}
			}
		}

		if ($get_department != '') {
			foreach (explode(',', $get_department) as $de_key => $de_value) {
				$get_dept_data = $this->Common_mdl->GetAllWithWhere('department_permission', 'id', $de_value);
				if (count($get_dept_data) > 0) {
					$department_name = $get_dept_data[0]['new_dept'];
					//  echo $department_name."--dept--";
					$get_deptteam_data = $this->Common_mdl->GetAllWithWhere('department_assign_team', 'depart_id', $de_value);
					if (count($get_deptteam_data) > 0) {
						$departmentteam_id = $get_deptteam_data[0]['team_id'];
						//   echo $departmentteam_id;
						foreach (explode(',', $departmentteam_id) as $tm_key => $tm_value) {
							$get_team_data = $this->Common_mdl->GetAllWithWhere('team', 'id', $tm_value);
							if (count($get_team_data) > 0) {
								$team_name = $get_team_data[0]['team'];

								$get_teamstaff_data = $this->Common_mdl->GetAllWithWhere('team_assign_staff', 'team_id', $tm_value);
								if (count($get_teamstaff_data) > 0) {
									$staff_ids = $get_teamstaff_data[0]['staff_id'];
									if ($staff_ids != '') {
										foreach (explode(',', $staff_ids) as $key => $value) {
											$get_user_data = $this->Common_mdl->GetAllWithWhere('user', 'id', $value);
											if (count($get_user_data) > 0) {
												// echo $get_user_data[0]['crm_email_id'];
												$it_name = $get_user_data[0]['crm_name'];
												$it_email = $get_user_data[0]['crm_email_id'];
												if ($it_email != '') {
													$this->weblead_email($get_lead_name, $lead_id, $it_name, $it_email, 'department', $team_name, $department_name);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		//echo "end end";


	}

	public function weblead_email($lead_name, $lead_id, $name, $email_id, $field, $team, $depart)
	{

		$data2['username'] = $name;
		//    $user_id = $query['id'];
		if ($field == 'team') {
			$email_content = "This Lead " . $lead_name . " Assign You via This Team " . $team;
		} else if ($field == 'department') {
			$email_content = "This Lead " . $lead_name . " Assign You via This Department " . $depart;
		} else {
			$email_content = "This Lead " . $lead_name . " Assign You ";
		}

		$firm_data = $this->Common_mdl->select_record('admin_setting', 'firm_id', $_SESSION['firm_id']);

		$send_email = (isset($firm_data['company_email']) && $firm_data['company_email'] != '') ? $firm_data['company_email'] : 'info.techleaf@gmail.com';

		$email_subject  = 'lead Form Notify';
		$data2['email_contents']  = '<a href=' . base_url() . 'leads/leads_detailed_tab/' . $lead_id . ' style="display: inline-block;text-align: center;color: #00aeef;text-decoration: none;font-family:;background: #00aeef;color: #fff;font-weight: 600;padding: 5px 10px;border-radius: 3px;">Click Here To view Lead</a><p>' . $email_content . '</p>';
		$data2['title'] = 'New Lead Assigne to you';
		//$data2['link']=base_url();
		//email template
		$body = $this->load->view('remainder_email.php', $data2, TRUE);
		//redirect('login/email_format');


		$send = firm_settings_send_mail($_SESSION['firm_id'], $email_id, $email_subject, $body);

		// echo $email_id;
		if ($send['result']) {
			echo "email send ";
		} else {
			echo "email not send ";
		}
	}
	/***********************************************************/


	public function addLeads_ajax()
	{
		//print_r($_POST);die;
		/** my one for user_id **/
		//** user id as a company id **/
		$data['user_id'] = $_SESSION['id'];
		$data['firm_id'] = $_SESSION['firm_id'];
		/** end of user id as company id **/
		/** end of user_id **/
		$data['name'] = $_POST['name'];
		$data['lead_status'] = $_POST['lead_status'];
		// $data['source'] = $_POST['source'];
		//$data['assigned'] = $_POST['assigned'];
		$data['source'] = implode(',', $_POST['source']);
		$team = array();
		$department = array();
		$assign = array();
		//$data['assigned'] = implode(',',$_POST['assigned']);
		/** for assign team and department  11-06-2018 **/
		if (isset($_POST['assigned'])) {
			foreach ($_POST['assigned'] as $key => $value) {
				$ex_val = explode('_', $value);
				if (count($ex_val) > 1) {
					if ($ex_val[0] == 'tm') {
						array_push($team, $ex_val[1]);
					}
					if ($ex_val[0] == 'de') {
						array_push($department, $ex_val[1]);
					}
				} else {
					array_push($assign, $ex_val[0]);
				}
			}

			$data['assigned'] = implode(',', $assign);
			$data['team'] = implode(',', $team);
			$data['dept'] = implode(',', $department);
		}
		/** end of 11-06-2018 **/

		$data['tags'] = $_POST['tags'];
		$data['position'] = $_POST['position'];
		$data['email_address'] = $_POST['email_address'];
		$data['website'] = $_POST['website'];
		$data['phone'] = $_POST['phone'];
		$data['company'] = $_POST['company'];
		$data['address'] = $_POST['address'];
		$data['city'] = $_POST['city'];
		$data['state'] = $_POST['state'];
		$data['country'] = $_POST['country'];
		$data['zip_code'] = $_POST['zip_code'];
		//  $data['default_language'] = $_POST['default_language'];
		$data['description'] = $_POST['description'];
		$data['contact_date'] = date('Y-m-d');

		if (isset($_POST['public'])) {
			$data['public'] = $_POST['public'];
		} else {
			$data['public'] = '';
		}
		if (isset($_POST['contact_today']) && $_POST['contact_today'] == 'on') {
			$data['contact_today'] = $_POST['contact_today'];
			$data['contact_date'] = date('Y-m-d');
		}
		if (isset($_POST['contact_date']) && $_POST['contact_date'] != '') {
			$originalDate = $_POST['contact_date'];
			$data['contact_date']  = implode('-', array_reverse(explode('-', $_POST['contact_date'])));
		}
		$data['createdTime'] = time();
		$data['old_fields_mail'] = $_POST['contact_today'] . "//" . $data['contact_date'];
		$result = $this->Common_mdl->insert('leads', $data);
		$id = '';
		if ($result) {
			$id = $this->db->insert_id();

			$activity_datas['log'] = "New Lead Created";
			$activity_datas['createdTime'] = time();
			$activity_datas['module'] = 'Lead';
			$activity_datas['user_id'] = $_SESSION['admin_id'];
			$activity_datas['module_id'] = $id;

			$inserting = $this->Common_mdl->insert('activity_log', $activity_datas);

			$this->session->set_flashdata('success', "New Lead Created Successfully!!!");
			//redirect('leads');
		} else {
			$this->session->set_flashdata('error', "Error, Some datas missing!!!");
			//redirect('leads');
		}
		/*** for mail send 15-06-2018  **/
		// if contact today date is today 
		//  echo $id;
		if ($id != '') {
			$get_leads_data = $this->Common_mdl->GetAllWithWheretwo('leads', 'id', $id, 'contact_date', date('Y-m-d'));
			if (count($get_leads_data) > 0) {
				$this->web_lead_email($id);
				$datas['mail'] = 'send';
				$result =  $this->Common_mdl->update('leads', $datas, 'id', $id);
			}
		}


		/** en dof mail send **/

		// $r_url = $_POST['kanpan'];
		// if($r_url=='0'){

		if ($result) {

			echo  $inserting;
		}
		//  redirect('leads');
		//redirect('leads/leads_detailed_tab/'.$id);
		// }else{
		//     redirect('leads/kanban');
		// }


	}

	/** for leads convert into client in leads detailed tab section **/

	public function leads_convert_client($lead_id)
	{
		$leads_data = $this->db->query('select * from leads where id=' . $lead_id . ' ')->row_array();

		if (is_array($leads_data) && count($leads_data)) {
			$this->lead = $leads_data;
		}

		/** get proposal data **/
		$proposal_data = $this->db->query('select * from proposals where lead_id=' . $lead_id . ' ')->row_array();
		if (is_array($proposal_data) && count($proposal_data) > 0) {
			$proposal_id = $proposal_data['id'];
		} else {
			$proposal_id = '';
		}

		/** end of proposal data **/
		$name = $leads_data['name'];
		$email = $leads_data['email_address'];
		$website = $leads_data['website']; //crm_business_website
		$phoneno = $leads_data['phone']; //crm_phone_number
		$address = $leads_data['address']; //crm_street1
		$city = $leads_data['city'];
		$state = $leads_data['state'];
		$country = $leads_data['country'];
		$zipcode = $leads_data['zip_code'];

		$inser_data = $this->db->insert(
			"user",
			array(
				"crm_name" => $name,
				"crm_last_name" => "",
				"username" => $email,
				"password" => uniqid(),
				"confirm_password" => '',
				"crm_email_id" => $email,
				"crm_phone_number" => $phoneno,
				"crm_date_of_birth" => "",
				"crm_gender" => '',
				"crm_street1" => $address,
				"crm_street2" => "",
				"crm_country" => $country,
				"crm_state" => $state,
				"crm_city" => $city,
				"crm_postal_code" => $zipcode,
				"crm_packages" => "",
				"crm_firm_name" => "",
				"crm_firm_status" => "",
				"crm_profile_pic" => "",
				"role" => "4",
				"status" => 1,
				"create_by" => 0,
				"frozen_freeze" => 0,
				"client_id" => 0,
				"company_roles" => 0,
				"CreatedTime" => time(),
				"autosave_status" => 0,
				'firm_admin_id' => $_SESSION['id'],
				'firm_id' => $leads_data['firm_id'],
				'user_type' => 'FC'
			)
		); // 
		// echo "//".$inser_data."data";
		// echo "<br>";
		$in = $this->db->insert_id();

		// add user id to lead data
		$this->lead['user_id'] = $in;

		$client_contact['client_id'] = $in;
		$client_contact['first_name'] = $leads_data['name'];
		$client_contact['address_line1'] = $leads_data['address'];
		$client_contact['make_primary'] = 1;
		$client_contact['main_email'] = $leads_data['email_address'];
		$this->db->insert("client_contacts", $client_contact);

		if ($inser_data) {

			$insert_client = $this->db->insert("client", array(
				"user_id" => $in,
				"crm_company_name" => $name,
				"crm_first_name" => $name,
				"crm_legal_form" => 'Private Limited company',
				"crm_allocation_holder" => '',
				"crm_company_number" => '',
				"crm_company_url" => '',
				"crm_officers_url" => '',
				"crm_company_name1" => $name,
				"crm_incorporation_date" => '',
				"crm_registered_in" => '',
				"crm_address_line_one" => $address,
				"crm_address_line_two" => '',
				"crm_address_line_three" => '',
				"crm_town_city" => $city,
				"crm_post_code" => $zipcode,
				"crm_company_status" => 'active',
				"crm_company_type" => '',
				"crm_company_sic" => '',
				"crm_letter_sign" => '',
				"crm_business_website" => $website,
				"crm_accounting_system" => '',
				"crm_companies_house_authorisation_code" => '',
				"crm_company_utr" => '',
				"crm_accounts_office_reference" => '',
				"crm_paye_ref_number" => '',
				"crm_vat_number" => '',

				"crm_assign_client_id_verified" => '',
				"crm_assign_type_of_id" => '',
				"crm_assign_proof_of_address" => '',
				"crm_assign_meeting_client" => '',
				"crm_assign_source" => '',
				"crm_refered_by" => '',
				"crm_assign_relationship_client" => '',
				"crm_assign_notes" => '',

				"crm_previous_accountant" => '',
				"crm_other_name_of_firm" => '',
				"crm_other_address" => '',
				"crm_other_contact_no" => '',
				"crm_email" => $email,
				"crm_other_chase_for_info" => '',
				"crm_other_notes" => '',
				"crm_other_internal_notes" => '',
				"crm_other_invite_use" => '',
				"crm_other_crm" => '',
				"crm_other_proposal" => '',
				"crm_other_task" => '',
				"crm_other_send_invit_link" => '',
				"crm_other_username" => '',
				"crm_other_password" => '',
				"crm_other_any_notes" => '',
				"firm_id" => $leads_data['firm_id'],
				"status" => 3, "created_date" => time(), "firm_admin_id" => $_SESSION['id']
			));
			/** 16-08-2018 **/
			$in1 = $this->db->insert_id();

			// adding client id to lead
			$this->lead['client_id'] = $in1;

			$assign_role = $this->Common_mdl->GetAllWithWheretwo('firm_assignees', 'module_id', $lead_id, 'module_name', 'LEADS');
			foreach ($assign_role as $key => $value) {
				$firm_ass_data['firm_id'] = $value['firm_id'];
				$firm_ass_data['assignees'] = $value['assignees'];
				$firm_ass_data['module_name'] = 'CLIENT';
				$firm_ass_data['module_id'] = $in1;
				$this->db->insert('firm_assignees', $firm_ass_data);
			}

			$convert_data['convert_client_user_id'] = $in; // user table id
			$convert_data['company'] = $in1;
			$this->Common_mdl->update('leads', $convert_data, 'id', $lead_id);
			if ($proposal_id != '') {
				$convert_data_proposal['company_id'] = $in1;
				$this->Common_mdl->update('proposals', $convert_data_proposal, 'id', $proposal_id);
			}

			/** end of 16-08-2018 **/
			return $this->db->affected_rows();
		}
	}
	/** endof 16-08-2018 **/
	/** 17-08-2018 **/
	public function leads_convert_task($lead_id)
	{
		$leads_data = $this->db->query('select * from leads where id=' . $lead_id . ' ')->row_array();
		$Date = date("Y-m-d");
		$leads_company = $leads_data['convert_client_user_id'];

		// convert already assigned lead tasks to client tasks
		$leads_tasks = $this->db->query("SELECT * from add_new_task WHERE lead_id = {$lead_id}")->result();

		foreach ($leads_tasks as $key => $task) {
			$client_task = [];
			$client_task['user_id']		 = $leads_company;
			$client_task['firm_id']		 = $task->firm_id;
			$client_task['lead_id']		 = $task->lead_id;
			$client_task['subject']		 = $task->subject;
			$client_task['start_date']	 = $task->start_date;
			$client_task['end_date']	 = $task->end_date;
			$client_task['priority']	 = $task->priority;
			$client_task['related_to']	 = 'service_reminder';
			$client_task['company_name'] = $this->lead['client_id'] ?? $task->company_name;
			$client_task['task_status']	 = $task->task_status;
			$client_task['tag']	 		 = $task->tag;
			$client_task['description']	 = $task->description;
			$client_task['created_date'] = $task->created_date;
			$client_task['create_by']	 = $task->create_by;
			$this->db->insert('add_new_task', $client_task);
			$client_task_id = $this->db->insert_id();

			// convert assignees
			$assignees = $this->db->query("SELECT * from firm_assignees WHERE module_name = 'TASK' AND module_id = {$task->id}")->result();

			foreach ($assignees as $assignee) {
				$client_task_assignee = [];
				$client_task_assignee['firm_id'] = $assignee->firm_id;
				$client_task_assignee['assignees'] = $assignee->assignees;
				$client_task_assignee['module_name'] = 'TASK';
				$client_task_assignee['module_id'] = $client_task_id;
				$this->db->insert('firm_assignees', $client_task_assignee);
			}
		}

		// $leads_company=$leads_data['company'];
		$name = "Create Client -" . $leads_data['name'];
		$task_data['user_id'] = $leads_company;
		$task_data['firm_id'] = $leads_data['firm_id'];
		$task_data['lead_id'] = $lead_id;
		$task_data['public'] = 0;
		$task_data['subject'] = $name;
		$task_data['start_date'] = date('Y-m-d');
		$task_data['end_date'] = date('Y-m-d', strtotime($Date . ' + 2 days'));
		$task_data['priority'] = 'low';
		// $task_data['related_to']='leads';
		$task_data['related_to'] = 'tasks';
		$task_data['company_name'] = $leads_company;
		// $task_data['worker']=$leads_data['assigned'];
		// $task_data['team']=$leads_data['team'];
		// $task_data['department']=$leads_data['dept'];
		$task_data['task_status'] = 'notstarted';

		// $for_old_values['assign']=$leads_data['assigned'];
		// $for_old_values['team']=$leads_data['team'];;
		// $for_old_values['department']=$leads_data['dept'];

		$for_old_values['manager'] = '';
		$task_data['for_old_assign'] = '';
		$task_data['created_date'] = time();
		$task_data['create_by'] = $_SESSION['id'];

		$this->db->insert('add_new_task', $task_data);
		$in = $this->db->insert_id();
		$convert_data['convert_task_id'] = $in; // user table id

		$assign_role = $this->Common_mdl->GetAllWithWheretwo('firm_assignees', 'module_id', $lead_id, 'module_name', 'LEADS');

		foreach ($assign_role as $key => $value) {
			$firm_ass_data['firm_id'] = $value['firm_id'];
			$firm_ass_data['assignees'] = $value['assignees'];
			$firm_ass_data['module_name'] = 'TASK';
			$firm_ass_data['module_id'] = $in;
			$this->db->insert('firm_assignees', $firm_ass_data);
		}

		$this->Common_mdl->update('leads', $convert_data, 'id', $lead_id);
		return $this->db->affected_rows();
	}
	/** end of 17-08-2018 **/





	/** update by Ram on 03-04-2019 **/
	public function get_hidden_data()
	{

		$table_name = $_POST['table_name'];
		$column_setting = Firm_column_settings($table_name);

		echo json_encode($column_setting);
	}



	public function delete_superadmin_status()
	{
		// print_r($_POST['ids']);
		if (isset($_POST['ids'])) {
			$ids = json_decode($_POST['ids'], true);
			foreach ($ids as $key => $value) {
				$sql = $this->db->query('select * from leads_status where id="' . $value . '"')->row_array();
				// print_r($sql);
				// exit;
				$data['is_delete'] = $sql['id'];
				$data['is_superadmin_owned'] = $sql['id'];
				unset($sql['id']);
				$this->db->insert('leads_status', $sql);
				$id = $this->db->insert_id();
				$this->db->where('id', $id);
				$this->db->update('leads_status', $data);
			}
			//  $id=implode(",",array_filter($ids));
			// $this->db->query("DELETE FROM leads_status WHERE id in ($id)");
			//echo $this->db->affected_rows();
			echo 1;
		}
		//redirect('Leads'); 
	}

	public function delete_superadmin_source()
	{
		// print_r($_POST['ids']);
		if (isset($_POST['ids'])) {
			$ids = json_decode($_POST['ids'], true);
			foreach ($ids as $key => $value) {
				$sql = $this->db->query('select * from source where id="' . $value . '"')->row_array();
				// print_r($sql);
				// exit;
				$data['is_delete'] = $sql['id'];
				$data['is_superadmin_owned'] = $sql['id'];
				unset($sql['id']);
				$this->db->insert('source', $sql);
				$id = $this->db->insert_id();
				$this->db->where('id', $id);
				$this->db->update('source', $data);
			}
			//  $id=implode(",",array_filter($ids));
			// $this->db->query("DELETE FROM leads_status WHERE id in ($id)");
			//echo $this->db->affected_rows();
			echo 1;
		}
		//redirect('Leads'); 
	}

	/** end of 03-04-2019 **/


	public function leads_superadmin_updatestatus($id)
	{
		$this->Security_model->chk_login();
		if ($_POST['table'] == 'source') {
			$data['source_name'] = $this->input->post('new_team');
		} else {
			$data['status_name'] = $this->input->post('new_team');
		}

		$sql = $this->db->query('select * from ' . $_POST['table'] . ' where id="' . $id . '"')->row_array();
		// print_r($sql);
		// exit;
		// $data['is_delete']=$sql['id'];
		$data['is_superadmin_owned'] = $sql['id'];
		unset($sql['id']);
		// unset($sql[])
		$this->db->insert($_POST['table'], $sql);
		$id = $this->db->insert_id();
		$this->db->where('id', $id);
		$this->db->update($_POST['table'], $data);


		// $this->Common_mdl->update('leads_status',$data,'id',$id);
		//$data['leads_status']=$this->Common_mdl->getallrecords('leads_status');
		// $data['leads_status']=$this->Common_mdl->GetAllWithWhere('leads_status','firm_id', $_SESSION['firm_id']);

		$data['leads_status'] = $this->Common_mdl->dynamic_status('leads_status');
		$this->load->view('leads/leads_status_table', $data);
		//$this->session->set_flashdata('success', 'source Updated Successfully');
		//redirect('department/update_department/'.$id);
		//redirect('leads/lead_source');
		// redirect('leads/lead_settings');

	}
	public function lead_SearchCompany()
	{
		$this->Security_model->chk_login();

		$searchKey = $_REQUEST['name_startsWith'];

		// $searchKey = $this->input->get('term');
		$alredyExist = $this->db->query("select crm_company_number from client where firm_id =" . $_SESSION['firm_id'])->result_array();

		// print_r( $alredyExist);

		//  $datas = array();
		//       foreach($alredyExist as $key=> $data)
		//       {
		//         //print_r($data);
		//        $name = $data['crm_company_number'];
		//         array_push($datas, $name);            
		//       }           
		//         echo json_encode($datas);

		// exit;

		$alredyExist = array_column($alredyExist, 'crm_company_number');

		$content = $this->CompanyHouse_Model->auto_SearchResults($searchKey, $alredyExist);
		// print_r($content);
		$datas = array();

		if (!empty($content)) {

			foreach ($content as $key => $data) {
				//print_r($data);
				$name = $data['company_name'] . '|' . $data['address_line'] . '|' . $data['description'] . '|' . $data['postal_code'];
				array_push($datas, $name);
			}
		}
		echo json_encode($datas);
		exit();
	}

	public function copy_lead_notes_to_client()
	{
		if (!$this->lead) {
			return;
		}

		$leads_data = $this->lead;

		$lead_notes = $this->db->query('select * from leads_notes where lead_id=' . $leads_data['id'] . ' ')->result();

		if (!is_array($lead_notes) || !$lead_notes) {
			return;
		}

		foreach ($lead_notes as $notes) {
			$data = [];
			$data['user_id'] = $leads_data['user_id'];
			$data['module'] = 'notes';
			$data['notes'] = $notes->notes;
			$data['date'] = date('d-m-Y', $notes->createdTime);
			$this->db->insert('timeline_services_notes_added', $data);
		}

		return $this->db->affected_rows();
	}
}
