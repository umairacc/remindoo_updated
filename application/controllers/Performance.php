<?php
error_reporting(E_STRICT);
class Performance extends CI_Controller 
{
        public $companieshouseAPIkey = "Gm7FPE1X1ZOZ2igIxus8ArN7xMFwOqysr5LqUjpe";
		public function __construct()
		{
			parent::__construct();
			$this->load->model(array('Common_mdl','Report_model','Security_model'));
			$this->load->library('Excel');
			$this->load->helper('comman');
		}


		public function custom()
		{
			$assignee = array();

			if(empty($_POST['assignee']))
			{
				$firm_users = $this->Report_model->getFirmUsers();

				foreach ($firm_users as $key => $value) 
				{
					$assignee[] = $value['id'];
				}
			}
			else
			{
				$assignee = $_POST['assignee'];				
			}

			if(!empty($_POST['filter']) && $_POST['filter'] != 'all')
			{					
				if($_POST['filter']=='week')
				{
					$current_date = date('Y-m-d');
					$week = date('W', strtotime($current_date));
					$year = date('Y', strtotime($current_date));

					$dto = new DateTime();
					$start_date = $dto->setISODate($year, $week, 0)->format('Y-m-d');
					$end_date = $dto->setISODate($year, $week, 6)->format('Y-m-d');
					$end_date = date('Y-m-d', strtotime($end_date . ' +1 day')); 
				}
                else if($_POST['filter']=='three_week')
                {
					$current_date = date('Y-m-d');
					$weeks = strtotime('- 3 week', strtotime( $current_date ));
					$start_date = date('Y-m-d', $weeks); 
					$addweeks = strtotime('+ 3 week', strtotime( $start_date ));
					$end_Week = date('Y-m-d', $addweeks); 
					$end_date = date('Y-m-d', strtotime($end_Week . ' +1 day'));
				}

				else if($_POST['filter']=='month')
				{
					$start_date = date("Y-m-01");
					$month_end = date("Y-m-t"); 
					$end_date = date('Y-m-d', strtotime($month_end . ' +1 day'));
				}

				if($_POST['filter']=='Three_month')
				{
					$end_date = date('Y-m-d',strtotime(date('Y-m-d').'+1 day'));
					$start_date = date('Y-m-d',strtotime('- 3 month', strtotime($end_date)));
				}	

				$filter = 'AND (start_date BETWEEN "'.$start_date.'" AND "'.$end_date.'" OR end_date BETWEEN "'.$start_date.'" AND "'.$end_date.'")';

				$leads_filter = 'AND from_unixtime(createdTime) BETWEEN "'.$start_date.'" AND "'.$end_date.'"';
		    }
		    else if(!empty($_POST['from_date']) && !empty($_POST['to_date']))
			{
				$start_date = $_POST['from_date'];
				$end_date = $_POST['to_date'];
				$filter = 'AND (start_date BETWEEN "'.$start_date.'" AND "'.$end_date.'" OR end_date BETWEEN "'.$start_date.'" AND "'.$end_date.'")';
				$leads_filter = 'AND from_unixtime(createdTime) BETWEEN "'.$start_date.'" AND "'.$end_date.'"';
			}
		    else
		    {
		    	$filter = '';
		    	$leads_filter = '';
		    }		

			if($_POST['option'] == 'Performance')
			{
				if(!empty($_POST['client']))
				{
	                $rec = $this->Report_model->selectRecord('client','id', $_POST['client']);
	                $client = 'AND user_id = "'.$rec['user_id'].'"';
	                $leads_client = 'convert_client_user_id = "'.$rec['user_id'].'"';
				}
				else
				{
					$client = '';
					$Assigned_Leads = Get_Assigned_Datas('LEADS');
					$leads_ids = implode(',', $Assigned_Leads);	
					$leads_client = 'FIND_IN_SET(id,"'.$leads_ids.'")';
				}		

				if(!empty($_POST['service']))
				{
					$service = 'AND related_to_services = "'.$_POST['service'].'"';
				}
				else
				{
					$service = '';
				}	

				$Assigned_Task = Get_Assigned_Datas('TASK');
				$assigned_task = implode(',',$Assigned_Task);
	            
	            $task_list = $this->db->query('SELECT * from add_new_task WHERE FIND_IN_SET(id,"'.$assigned_task.'") AND firm_id = "'.$_SESSION['firm_id'].'" AND task_status IN("notstarted","inprogress","awaiting","archive","complete") '.$filter.' '.$client.' '.$service.' ORDER BY id ASC')->result_array();
				
				     
	            $i = 0;
	            foreach($task_list as $key => $value) 
	            {
	               $task_assignees = Get_Module_Assigees('TASK',$value['id']);
                   $assignees_name = array();
	               $duration = array();             

	           	   foreach($assignee as $key1 => $value1) 
	           	   {
	           	  	  foreach($task_assignees as $key2 => $value2)
	           	  	  {
	           	  	     if($value1 == $value2)
	           	  	     {
	           	  	         $assignees_name[] = ucwords($this->Common_mdl->getUserProfileName($value2));
	           	  	         $time = $this->Common_mdl->get_working_hours($value['id'],$value2);

	           	  	         if($time>0)
	           	  	         {
	           	  	           $duration[$value2] = $time;
	           	  	         }  
	           	  	     }    
	           	  	  }  
	           	   } 

	           	   if(count($assignees_name)>0)
	           	   {
	           	   	  $arr[$i]['id'] = $value['id'];
	           	   	  $arr[$i]['subject'] = $value['subject'];
	           	   	  $arr[$i]['start_date'] = $value['start_date'];
	           	   	  $arr[$i]['end_date'] = $value['end_date'];
	           	   	  $arr[$i]['status'] = $value['task_status'];
	           	   	  $arr[$i]['assignees'] = implode(',', array_unique($assignees_name));
	           	   	  $arr[$i]['hours_worked'] = round(array_sum($duration),2);
	           	   	  $i++; 
	           	   }            	   
	               
	            }			
                
                $data['tasks_list'] = $arr;
				$leads = $this->db->query('SELECT * from leads where '.$leads_client.' '.$leads_filter.' ORDER BY id ASC')->result_array();

				$j = 0;
                
				foreach ($leads as $key3 => $value3) 
				{
					$lead_assignees = Get_Module_Assigees('LEADS',$value3['id']);
                    $assignees_name = array();
                     
                     foreach($assignee as $key1 => $value1) 
                     {
                    	  foreach($lead_assignees as $key2 => $value2)
                    	  {
                    	     if($value1 == $value2)
                    	     {
                                $assignees_name[] = ucwords($this->Common_mdl->getUserProfileName($value2));
                    	     }
                    	  }
                     }

                     if(count($assignees_name)>0)
                     {
                        $record[$j]['id'] = $value3['id'];
                        $record[$j]['name'] = $value3['name'];
                        $record[$j]['lead_status'] = $value3['lead_status'];
                        $record[$j]['assignees'] = implode(',', array_unique($assignees_name));
                        $j++;
                     }	    	
				}
				
				$data['leads'] = $record;
				$this->load->view('reports/performance_report',$data);				
			}
			else
			{
				$ids = implode(',',$assignee);
				$data['user_activity'] = $this->Common_mdl->activitylogfunction($ids,$start_date,$end_date);
				$this->load->view('reports/allactivity',$data);
			}

		}

}
?>