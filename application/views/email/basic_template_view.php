<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width" />
        <!-- Favicon icon -->
        <link rel="icon" href="../email-templates/img/favicon.ico" type="image/x-icon">
        <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
       <style type="text/css">
            @media only screen and (max-width: 550px), screen and (max-device-width: 550px) {
                body[yahoo] .buttonwrapper { background-color: transparent !important; }
                body[yahoo] .button { padding: 0 !important; }
                body[yahoo] .button a { background-color: #9b59b6; padding: 15px 25px !important; }
            }

            @media only screen and (min-device-width: 601px) {
                .content { width: 600px !important; }
                .col387 { width: 387px !important; }
            }
        </style>
    </head>
    <body style="margin: 0; padding: 0;" yahoo="fix">
        <div id="m_5349442688376831455m_-3036873862170083609wrapper" dir="ltr" style="background-color:#f7f7f7;margin:0;padding:70px 0 70px 0;width:100%">
        <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 100%; max-width: 600px;" class="content">
        <tbody style="border:1px solid #eee;">
            <tr>
                <td style="padding: 20px;background-color:<?php echo ( !empty( $background_color ) ? $background_color : 'rgba(0,0,0,0.9)' )?>">
                    <?php echo $header; ?>
                </td>
            </tr>
            <tr>
                <td align="left" bgcolor="#ffffff" style="padding: 20px 40px 0px; color: #555555; font-family: Arial, sans-serif; font-size: 14px; line-height: 30px; text-align: left;">
                    <?php echo $body; ?>
                </td>
            </tr>         
            <tr>
                <td style="padding: 25px;background-color:<?php echo ( !empty( $background_color ) ? $background_color : 'rgba(0,0,0,0.9)' )?>">
                        <?php echo $footer; ?>
                </td>
            </tr>
            </tbody>
        </table>
        </div>
    </body>
</html>