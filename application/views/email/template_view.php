<?php 
$firm_id = (!empty( $firm_id )?$firm_id:'');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

        <title>Guru Able Bootstrap 4 Admin Template by Codedthemes</title>
        <meta name="viewport" content="width=device-width" />
        <!-- Favicon icon -->
        <link rel="icon" href="../email-templates/img/favicon.ico" type="image/x-icon">
        <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
       <style type="text/css">
            @media only screen and (max-width: 550px), screen and (max-device-width: 550px) {
                body[yahoo] .buttonwrapper { background-color: transparent !important; }
                body[yahoo] .button { padding: 0 !important; }
                body[yahoo] .button a { background-color: #9b59b6; padding: 15px 25px !important; }
            }

            @media only screen and (min-device-width: 601px) {
                .content { width: 600px !important; }
                .col387 { width: 387px !important; }
            }
        </style>
    </head>
    <body style="margin: 0; padding: 0;" yahoo="fix">
        <!--[if (gte mso 9)|(IE)]>
        <table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
          <tr>
            <td>
        <![endif]-->
        <div id="m_5349442688376831455m_-3036873862170083609wrapper" dir="ltr" style="background-color:#f7f7f7;margin:0;padding:70px 0 70px 0;width:100%">
        <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 100%; max-width: 600px;" class="content">
        <tbody style="border:1px solid #eee;">
            <tr>
                <td align="center" style="padding: 20px 20px 20px 20px; color: #ffffff; font-family: Arial, sans-serif; font-size: 36px; font-weight: bold;background-color:rgba(0,0,0,0.9);">
                    <!-- <img src="<?php echo base_url()?>assets/images/color_logo.png" alt="Remindoo" style="display:block;" /> -->

                    <?php echo firm_mail_header($firm_id); ?>
                   
                </td>
            </tr>
            <tr>
                <td align="center" bgcolor="#ffffff" style="padding: 40px 20px 0; color: #555555; font-family: Arial, sans-serif; font-size: 24px; line-height: 30px;">
                    <b> Welcome</b><br/>
                </td>
            </tr>
            <tr>

            <td align="left" bgcolor="#ffffff" style="padding: 20px 40px 0px; color: #555555; font-family: Arial, sans-serif; font-size: 14px; line-height: 30px; text-align: left;">

            <?php echo $body; ?>
            </td>
            </tr>

          <!--   <tr>
                <td align="left" bgcolor="#ffffff" style="padding: 20px 40px 0px; color: #555555; font-family: Arial, sans-serif; font-size: 20px; line-height: 30px;">
                    <b style="font-size:15px;">Dear <?php echo $name;?>,</b><br/>
                    <h5 style="margin: 0;font-size: 15px;font-weight: normal;">Here's your Login details,</h5>
                </td>
            </tr>
            <tr>
                <td align="left" bgcolor="#ffffff" style="padding: 20px 40px 40px; color: #555555; font-family: Arial, sans-serif; font-size: 20px; line-height: 30px; border-bottom: 1px solid #f6f6f6;">
                    <b style="margin: 0;font-size: 15px;">User name: <span style="font-weight: normal;padding-left:5px;"><?php echo $user_name;?></span></b><br/>
                    <b style="margin: 0;font-size: 15px;">Password: <span style="font-weight: normal;padding-left:5px;"><?php echo $password; ?></span></b>
                </td>
            </tr> -->
            <tr>
                <td align="center" bgcolor="#353535" style="padding: 25px; color: #fff; font-family: Arial, sans-serif; font-size: 12px; line-height: 18px;">
                    <b style="font-family:'Lato',sans-serif!important;font-size: 15px;font-weight: normal"><!-- Copyright © 2018 Remindoo. -->
                        <?php echo firm_mail_footer($firm_id); ?>
                    </b>
                </td>
            </tr>
            </tbody>
        </table>
        </div>
        <!--[if (gte mso 9)|(IE)]>
                </td>
            </tr>
        </table>
        <![endif]-->
    </body>
</html>