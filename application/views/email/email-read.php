<?php $this->load->view('includes/header');
   $succ = $this->session->flashdata('success');
   $error = $this->session->flashdata('error');
   ?>
<div class="pcoded-content card-removes">
<div class="pcoded-inner-content">
  <div class="main-body">
    <div class="page-wrapper">
      <div class="page-body">
        <div class="card">
          <div class="card-block email-card">
            <div class="row">
              <!-- <div class="col-lg-12 col-xl-3">
                <div class="user-head row">
                  <div class="user-face">
                    <img class="img-fluid" src="../files/assets/images/logo.png" alt="Theme-Logo">
                  </div>
                </div>
              </div> -->
              <div class="col-lg-12 col-xl-12">
                <div class="mail-box-head row">
                  <div class="col-md-12">
                    <form class="f-right">
                      <div class="right-icon-control">
                        <input type="text" class="form-control  search-text" placeholder="Search Friend" id="search-friends-2">
                        <div class="form-icon">
                          <i class="icofont icofont-search"></i>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-xl-3">
                <div class="user-body">
                  <div class="p-20 text-center">
                    <a href="email-compose.html" class="btn btn-danger">Compose</a>
                  </div>
                  <ul class="page-list nav nav-tabs flex-column">
                    <li class="nav-item mail-section">
                      <a class="nav-link" href="email-inbox.html">
                      <i class="icofont icofont-inbox"></i> Inbox
                      <span class="label label-primary f-right">6</span>
                      </a>
                    </li>
                    <li class="nav-item mail-section">
                      <a class="nav-link" href="email-inbox.html">
                      <i class="icofont icofont-star"></i> Starred
                      </a>
                    </li>
                    <li class="nav-item mail-section">
                      <a class="nav-link" href="email-inbox.html">
                      <i class="icofont icofont-file-text"></i> Drafts
                      </a>
                    </li>
                    <li class="nav-item mail-section">
                      <a class="nav-link" href="email-inbox.html">
                      <i class="icofont icofont-paper-plane"></i> Sent Mail
                      </a>
                    </li>
                    <li class="nav-item mail-section">
                      <a class="nav-link" href="email-inbox.html">
                      <i class="icofont icofont-ui-delete"></i> Trash
                      <span class="label label-info f-right">30</span>
                      </a>
                    </li>
                  </ul>
                  <ul class="p-20 label-list">
                    <li>
                      <h5>Labels</h5>
                    </li>
                    <li>
                      <a class="mail-work" href="">Work</a>
                    </li>
                    <li>
                      <a class="mail-design" href="">Design</a>
                    </li>
                    <li>
                      <a class="mail-family" href="">Family</a>
                    </li>
                    <li>
                      <a class="mail-friends" href="">Friends</a>
                    </li>
                    <li>
                      <a class="mail-office" href="">Office</a>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="col-lg-12 col-xl-9">
                <div class="mail-body">
                  <div class="mail-body-content email-read">
                    <div class="card">
                      <div class="card-header">
                        <h5>Here You Have New Opportunity...</h5>
                        <h6 class="f-right">08:23 AM</h6>
                      </div>
                      <div class="card-block">
                        <div class="media m-b-20">
                          <div class="media-left photo-table">
                            <a href="#">
                            <img class="media-object img-radius" src="../files/assets/images/avatar-3.jpg" alt="E-mail User">
                            </a>
                          </div>
                          <div class="media-body photo-contant">
                            <a href="#">
                              <h6 class="user-name txt-primary">John Doe</h6>
                            </a>
                            <a class="user-mail txt-muted" href="#">
                              <h6>From:johndoe1543@gmail.com</h6>
                            </a>
                            <div>
                              <h6 class="email-welcome-txt">Hello Dear...</h6>
                              <p class="email-content">
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus.
                              </p>
                            </div>
                            <div class="m-t-15">
                              <i class="icofont icofont-clip f-20 m-r-10"></i>Attachments <b>(3)</b>
                              <div class="row mail-img">
                                <div class="col-sm-4 col-md-2 col-xs-12">
                                  <a href="#"><img class="card-img-top img-fluid img-thumbnail" src="../files/assets/images/card-block/card1.jpg" alt="Card image cap"></a>
                                </div>
                                <div class="col-sm-4 col-md-2 col-xs-12">
                                  <a href="#"><img class="card-img-top img-fluid img-thumbnail" src="../files/assets/images/card-block/card2.jpg" alt="Card image cap"></a>
                                </div>
                                <div class="col-sm-4 col-md-2 col-xs-12">
                                  <a href="#"><img class="card-img-top img-fluid img-thumbnail" src="../files/assets/images/card-block/card13.jpg" alt="Card image cap"></a>
                                </div>
                              </div>
                              <textarea class="form-control m-t-30 col-xs-12 email-textarea" id="exampleTextarea-1" placeholder="Reply Your Thoughts" rows="4"></textarea>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="styleSelector">
    <div class="selector-toggle"><a href="javascript:void(0)"></a></div>
    <ul>
      <li>
        <p class="selector-title main-title st-main-title"><b>Guru </b>able Customizer</p>
        <span class="text-muted">Live customizer with tons of options</span>
      </li>
      <li>
        <p class="selector-title">Main layouts</p>
      </li>
      <li>
        <div class="theme-color"><a href="#" class="navbar-theme" navbar-theme="themelight1"><span class="head"></span><span class="cont"></span></a><a href="#" class="navbar-theme" navbar-theme="theme1"><span class="head"></span><span class="cont"></span></a><a href="#" class="Layout-type" layout-type="light"><span class="head"></span><span class="cont"></span></a><a href="#" class="Layout-type" layout-type="dark"><span class="head"></span><span class="cont"></span></a><a href="#" class="Layout-type" layout-type="img"><span class="head"></span><span class="cont"></span></a></div>
      </li>
    </ul>
    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: calc(100vh - 515px);">
      <div class="style-cont m-t-10" style="overflow: hidden; width: auto; height: calc(100vh - 515px);">
        <ul class="nav nav-tabs  tabs" role="tablist">
          <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#sel-layout" role="tab">Layouts</a></li>
          <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#sel-sidebar-setting" role="tab">Sidebar Settings</a></li>
        </ul>
        <div class="tab-content tabs">
          <div class="tab-pane active" id="sel-layout" role="tabpanel">
            <ul>
              <li class="theme-option">
                <div class="checkbox-fade fade-in-primary"><label><input type="checkbox" value="false" id="theme-layout" name="vertical-item-border"><span class="cr"><i class="cr-icon icofont icofont-ui-check txt-success"></i></span><span>Box Layout - with patterns</span></label></div>
              </li>
              <li class="theme-option d-none" id="bg-pattern-visiblity">
                <div class="theme-color"><a href="#" class="themebg-pattern small" themebg-pattern="pattern1">&nbsp;</a><a href="#" class="themebg-pattern small" themebg-pattern="pattern2">&nbsp;</a><a href="#" class="themebg-pattern small" themebg-pattern="pattern3">&nbsp;</a><a href="#" class="themebg-pattern small" themebg-pattern="pattern4">&nbsp;</a><a href="#" class="themebg-pattern small" themebg-pattern="pattern5">&nbsp;</a><a href="#" class="themebg-pattern small" themebg-pattern="pattern6">&nbsp;</a></div>
              </li>
              <li class="theme-option">
                <div class="checkbox-fade fade-in-primary"><label><input type="checkbox" value="false" id="sidebar-position" name="sidebar-position" checked=""><span class="cr"><i class="cr-icon icofont icofont-ui-check txt-success"></i></span><span>Fixed Sidebar Position</span></label></div>
              </li>
              <li class="theme-option">
                <div class="checkbox-fade fade-in-primary"><label><input type="checkbox" value="false" id="header-position" name="header-position" checked=""><span class="cr"><i class="cr-icon icofont icofont-ui-check txt-success"></i></span><span>Fixed Header Position</span></label></div>
              </li>
            </ul>
          </div>
          <div class="tab-pane" id="sel-sidebar-setting" role="tabpanel">
            <ul>
              <li class="theme-option">
                <p class="sub-title drp-title">Menu Type</p>
                <div class="form-radio" id="menu-effect">
                  <div class="radio radiofill radio-primary radio-inline"><label><input type="radio" name="radio" value="st1" onclick="handlemenutype(this.value)"><i class="helper"></i><span class="micon st1"><i class="ti-home"></i><b>D</b></span></label></div>
                  <div class="radio radiofill radio-success radio-inline"><label><input type="radio" name="radio" value="st2" onclick="handlemenutype(this.value)" checked="true"><i class="helper"></i><span class="micon st2"><i class="ti-home"></i><b>D</b></span></label></div>
                  <div class="radio radiofill radio-warning radio-inline"><label><input type="radio" name="radio" value="st3" onclick="handlemenutype(this.value)"><i class="helper"></i><span class="micon st3"><i class="ti-home"></i><b>D</b></span></label></div>
                  <div class="radio radiofill radio-danger radio-inline"><label><input type="radio" name="radio" value="st4" onclick="handlemenutype(this.value)"><i class="helper"></i><span class="micon st4"><i class="ti-home"></i><b>D</b></span></label></div>
                  <div class="radio radiofill radio-primary radio-inline"><label><input type="radio" name="radio" value="st5" onclick="handlemenutype(this.value)"><i class="helper"></i><span class="micon st5"><i class="ti-home"></i><b>D</b></span></label></div>
                </div>
              </li>
              <li class="theme-option">
                <p class="sub-title drp-title">SideBar Effect</p>
                <select id="vertical-menu-effect" class="form-control minimal">
                  <option name="vertical-menu-effect" value="shrink">shrink</option>
                  <option name="vertical-menu-effect" value="overlay">overlay</option>
                  <option name="vertical-menu-effect" value="push">Push</option>
                </select>
              </li>
              <li class="theme-option">
                <p class="sub-title drp-title">Hide/Show Border</p>
                <select id="vertical-border-style" class="form-control minimal">
                  <option name="vertical-border-style" value="solid">Style 1</option>
                  <option name="vertical-border-style" value="dotted">Style 2</option>
                  <option name="vertical-border-style" value="dashed">Style 3</option>
                  <option name="vertical-border-style" value="none">No Border</option>
                </select>
              </li>
              <li class="theme-option">
                <p class="sub-title drp-title">Drop-Down Icon</p>
                <select id="vertical-dropdown-icon" class="form-control minimal">
                  <option name="vertical-dropdown-icon" value="style1">Style 1</option>
                  <option name="vertical-dropdown-icon" value="style2">style 2</option>
                  <option name="vertical-dropdown-icon" value="style3">style 3</option>
                </select>
              </li>
              <li class="theme-option">
                <p class="sub-title drp-title">Sub Menu Drop-down Icon</p>
                <select id="vertical-subitem-icon" class="form-control minimal">
                  <option name="vertical-subitem-icon" value="style1">Style 1</option>
                  <option name="vertical-subitem-icon" value="style2">style 2</option>
                  <option name="vertical-subitem-icon" value="style3">style 3</option>
                  <option name="vertical-subitem-icon" value="style4">style 4</option>
                  <option name="vertical-subitem-icon" value="style5">style 5</option>
                  <option name="vertical-subitem-icon" value="style6">style 6</option>
                </select>
              </li>
            </ul>
          </div>
          <ul>
            <li>
              <p class="selector-title">Header Brand color</p>
            </li>
            <li class="theme-option">
              <div class="theme-color"><a href="#" class="logo-theme" logo-theme="theme1"><span class="head"></span><span class="cont"></span></a><a href="#" class="logo-theme" logo-theme="theme2"><span class="head"></span><span class="cont"></span></a><a href="#" class="logo-theme" logo-theme="theme3"><span class="head"></span><span class="cont"></span></a><a href="#" class="logo-theme" logo-theme="theme4"><span class="head"></span><span class="cont"></span></a><a href="#" class="logo-theme" logo-theme="theme5"><span class="head"></span><span class="cont"></span></a></div>
            </li>
            <li>
              <p class="selector-title">Header color</p>
            </li>
            <li class="theme-option">
              <div class="theme-color"><a href="#" class="header-theme" header-theme="theme1"><span class="head"></span><span class="cont"></span></a><a href="#" class="header-theme" header-theme="theme2"><span class="head"></span><span class="cont"></span></a><a href="#" class="header-theme" header-theme="theme3"><span class="head"></span><span class="cont"></span></a><a href="#" class="header-theme" header-theme="theme4"><span class="head"></span><span class="cont"></span></a><a href="#" class="header-theme" header-theme="theme5"><span class="head"></span><span class="cont"></span></a><a href="#" class="header-theme" header-theme="theme6"><span class="head"></span><span class="cont"></span></a></div>
            </li>
            <li>
              <p class="selector-title">Navbar image</p>
            </li>
            <li class="theme-option">
              <div class="theme-color"><a href="#" class="navbg-pattern image" navbg-pattern="img1">&nbsp;</a><a href="#" class="navbg-pattern image" navbg-pattern="img2">&nbsp;</a><a href="#" class="navbg-pattern image" navbg-pattern="img3">&nbsp;</a><a href="#" class="navbg-pattern image" navbg-pattern="img4">&nbsp;</a><a href="#" class="navbg-pattern image" navbg-pattern="img5">&nbsp;</a></div>
            </li>
            <li>
              <p class="selector-title">Active link color</p>
            </li>
            <li class="theme-option">
              <div class="theme-color"><a href="#" class="active-item-theme small" active-item-theme="theme1">&nbsp;</a><a href="#" class="active-item-theme small" active-item-theme="theme2">&nbsp;</a><a href="#" class="active-item-theme small" active-item-theme="theme3">&nbsp;</a><a href="#" class="active-item-theme small" active-item-theme="theme4">&nbsp;</a><a href="#" class="active-item-theme small" active-item-theme="theme5">&nbsp;</a><a href="#" class="active-item-theme small" active-item-theme="theme6">&nbsp;</a><a href="#" class="active-item-theme small" active-item-theme="theme7">&nbsp;</a><a href="#" class="active-item-theme small" active-item-theme="theme8">&nbsp;</a><a href="#" class="active-item-theme small" active-item-theme="theme9">&nbsp;</a><a href="#" class="active-item-theme small" active-item-theme="theme10">&nbsp;</a><a href="#" class="active-item-theme small" active-item-theme="theme11">&nbsp;</a><a href="#" class="active-item-theme small" active-item-theme="theme12">&nbsp;</a></div>
            </li>
            <li>
              <p class="selector-title">Menu Caption Color</p>
            </li>
            <li class="theme-option">
              <div class="theme-color"><a href="#" class="leftheader-theme small" lheader-theme="theme1">&nbsp;</a><a href="#" class="leftheader-theme small" lheader-theme="theme2">&nbsp;</a><a href="#" class="leftheader-theme small" lheader-theme="theme3">&nbsp;</a><a href="#" class="leftheader-theme small" lheader-theme="theme4">&nbsp;</a><a href="#" class="leftheader-theme small" lheader-theme="theme5">&nbsp;</a><a href="#" class="leftheader-theme small" lheader-theme="theme6">&nbsp;</a></div>
            </li>
          </ul>
        </div>
      </div>
      <div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px; height: 33.29px;"></div>
      <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
    </div>
    <ul>
      <li><a href="https://codedthemes.com/item/guru-able-lite-free-admin-template/" target="_blank" class="btn btn-success btn-block m-r-15 m-t-10 m-b-10">Free ! Download Lite Version</a><a href="http://html.codedthemes.com/guru-able/doc" target="_blank" class="btn btn-primary btn-block m-r-15 m-t-5 m-b-10">Online Documentation</a></li>
      <li class="text-center"><span class="text-center f-18 m-t-15 m-b-15 d-block">Thank you for sharing !</span><a href="https://www.facebook.com/codedthemes" target="_blank" class="btn btn-facebook soc-icon m-b-20"><i class="icofont icofont-social-facebook"></i></a><a href="https://twitter.com/codedthemes" target="_blank" class="btn btn-twitter soc-icon m-l-20 m-b-20"><i class="icofont icofont-social-twitter"></i></a></li>
    </ul>
  </div>
</div>
</div>
<?php $this->load->view('includes/footer');?>