<?php
 function GetAssignees_SelectBox_Content($defult_select=0,$disable_id=0,$id=0,$data_value=1)
    { 
      $CI =& get_instance(); 
      $CHILDS = '';

          $DB_DATA = $CI->db->query("select * from roles_section where firm_id = ".$_SESSION['firm_id']." and  parent_id=".$id." ")->result_array();  

        
          foreach( $DB_DATA as $val )
          {
            
              $Extraclass = "";

              if($defult_select!=0 && $defult_select==$val['id'] )
              {
                $Extraclass .= ' active';
              }
              if($disable_id!=0 && $disable_id==$val['id'] )
              {
                $Extraclass .= ' disabled';
              }
              
            $CHILDS.= '<a class="dropdown-item remove_member '.$Extraclass.'" data-value="'.$val['id'].'" data-level="'.$data_value.'" href="#"><i class="fa fa-dot-circle-o"></i>'.$val['role'].'</a>';

            $CHILDS.= GetAssignees_SelectBox_Content($defult_select,$disable_id, $val['id'] , $data_value+1 );
          }
          return $CHILDS;
    }

function GetAssignees_SelectBox( $id=0 , $data_value='')
{ 
  $CI =& get_instance();  
  $CHILDS = [];

      $DB_DATA = $CI->db->query("select * from organisation_tree where firm_id = ".$_SESSION['firm_id']." and  parent_id=".$id." ")->result_array();  

    
      foreach( $DB_DATA as $val )
      {
        $CHILD_DATA = [];

        $user_info = $CI->db->query("select crm_name from user where id=".$val['user_id'])->row_array();
        
         if(empty( $data_value ))
          {
            $value = $val['id'];
          }
          else
          {
            $value = $data_value.':'.$val['id'];
          }
            $CHILD_DATA['id'] = $value.'_'.$val['user_id'];
            $CHILD_DATA['title'] = $user_info['crm_name'];

            $hasChild = GetAssignees_SelectBox( $val['id'] , $value );

        if( !empty( $hasChild ) )
        {
          $CHILD_DATA['subs']= $hasChild;
        }
        $CHILDS[]= $CHILD_DATA;
      }
      return $CHILDS;
}
function Get_Assigned_Datas( $Module ,$user_id = 0 )
{
  $CI =& get_instance();
  
  if(!$user_id)
  {
    $user_id = $_SESSION['userId'];
  } 

  $ids = $CI->db->query("select id from organisation_tree where user_id=".$user_id." ")->result_array();
  $Module_ids = [];
  
  foreach ($ids as $key => $value)
  {
    $data = $CI->db->query("select module_id from firm_assignees where module_name='".$Module."' and (CONCAT(':',assignees,':') REGEXP ':".$value['id'].":') ")->result_array();

    $Module_ids=array_merge($Module_ids,array_column($data, 'module_id'));
    //print_r(array_column($data, 'module_id'));
  }
  return array_unique( $Module_ids );
}

function Get_Module_Assigees( $Module , $Module_id )
{
  $CI =& get_instance();
  $Return = [];
  $data = $CI->db->query("select assignees from firm_assignees where module_name='".$Module."' and module_id=".$Module_id)->result_array();
  $data = array_column($data,'assignees');
  $data =explode(':', implode(':',$data) );
  $data = implode(',',$data);
  if(!empty($data))
  {
    $Return = $CI->db->query("select user_id from organisation_tree where id IN (".$data.") ")->result_array();
    $Return = array_unique( array_column( $Return,'user_id') );
  }
  return $Return;
}

function delete_OrgChart_Node($id)
  { 
    $CI =& get_instance();

    if(!empty( $id ))
    {
      $nodeData = $CI->db->query("select * from organisation_tree where firm_id=".$_SESSION['firm_id']." and id= ".$id)->row_array();

      $CI->db->update('organisation_tree',['parent_id'=>$nodeData['parent_id']],"parent_id=".$nodeData['id']);

      $CI->db->query("delete from organisation_tree where id =".$id);

      $data = $CI->db->query("select * from firm_assignees where  (CONCAT(':',assignees,':') REGEXP ':".$id.":') ")->result_array();

      foreach ($data as $key => $value) 
      {
        $assignee = explode(':', $value['assignees'] );
        $key = array_search( $id , $assignee );
        unset($assignee[ $key ] );
        $assignee = implode(':',$assignee);
        $CI->db->update('firm_assignees',['assignees'=>$assignee],"id=".$value['id']);
      }
      return  $CI->db->affected_rows();
    }
  }

function find_underlevel_users($role,$user_id)
  {	$break=0;
    do{
      if($break==1000)break;
      $CI =& get_instance();
      $data=$CI->db->query("select * from assign_member where assign_to_id IN ($user_id)")->result_array();
      //echo "<pre>";print_r($data);die;
      $t='';
      $atr='';
      foreach ($data as $v) 
      {
        if($role==($v['assign_to_roll']+1))
        {
          $t.=($t=='')?$v['members_id']:",".$v['members_id'];
        }
        else 
        {
          $atr.=($atr==''?$v['members_id']:",".$v['members_id']);
        }  
      }
      $user_id=$atr;
     $break++;
   }while($t=='');
  
  return $CI->db->query("select * from user where id IN ($t)")->result_array();
  }


function dateDiffrents( $D1 , $D2 )
{
    $d1 = date_create( $D1 );
    $d2 = date_create( $D2 );
    $interval=date_diff( $d1 ,$d2 );
          
    return intval( $interval->format('%R%a') );
}

function Change_Date_Format( $date , $C_formate = 'Y-m-d' , $R_formate = 'd-m-Y' )
{
  $date = date_create_from_format( $C_formate , trim ( $date ) );
  if( $date )
  {
    return date_format( $date , $R_formate );
  }
  return $date;

}
function Firm_column_settings($table)

{
  $CI =& get_instance();
  $firm_id = "firm".'0';
  
  $column_setting = $CI->db->query("select name,visible_".$firm_id.",order_".$firm_id.",cusname_".$firm_id." from firm_column_settings where table_name='".$table."' order by order_".$firm_id." ASC")->result_array();

    
  $hidden_coulmns = array_keys( array_column( $column_setting , 'visible_'.$firm_id , 'name' ) , 0 );

  $column_order = array_column( $column_setting , 'name' );

  $hidden_coulmns = array_map(function ($a){return trim( $a ).'_TH';}, $hidden_coulmns );

  $column_order = array_map(function ($a){return trim( $a ).'_TH';}, $column_order );

  $return['hidden'] = json_encode( $hidden_coulmns );

  $return['order'] = json_encode( $column_order );

  return $return;
}

function SuperAdmin_column_settings($table)

{
  $CI =& get_instance();

  
  $column_setting = $CI->db->query("select name,visible,orders,cusname from super_admin_column_settings where table_name='".$table."' order by orders ASC")->result_array();

    
  $hidden_coulmns = array_keys( array_column( $column_setting , 'visible' , 'name' ) , 0 );

  $column_order = array_column( $column_setting , 'name' );

  $hidden_coulmns = array_map(function ($a){return trim( $a ).'_TH';}, $hidden_coulmns );

  $column_order = array_map(function ($a){return trim( $a ).'_TH';}, $column_order );

  $return['hidden'] = json_encode( $hidden_coulmns );

  $return['order'] = json_encode( $column_order );

  return $return;
}

  
 
function getMenuURL( $id )
{ 
  $CI =& get_instance();
  
  $url="javascript::void(0)";

  if ( !empty( $id ) )
    {
      
      $url = $CI->db->query(" SELECT url FROM `module_mangement` where module_mangement_id = $id ")->row_array();
      
      if ( strpos( $url['url'] , "#" ) === 0 )        
      {
        $url = $url['url'];
      
      }
      else
      {
        $url = base_url().$url['url'];
      }

    }

    return $url;
}

function getHeader( $PARENT_LI , $CHILD_UL , $CHILD_LI )
{ 
  $CI =& get_instance();

  $menu_data = '';
  $Menus = GetMenus();
  foreach ($Menus as $datas)
  {
    
    $url =  getMenuURL( $datas['href'] );
    
    $ParentLi = str_replace('{URL}', $url , $PARENT_LI );  
    $ParentLi = str_replace('{ICON_PATH}', $datas['icon'] , $ParentLi );  
    $ParentLi = str_replace('{MENU_LABEL}',  $datas['text']  , $ParentLi );  

    $childCnt ='';

    if( isset( $datas['children'] ) )
    {
      $childCnt =  recursive_menu( $datas['children'] , $CHILD_UL , $CHILD_LI );
    }

//echo htmlspecialchars($ParentLi)."<br>";
//echo  htmlspecialchars($childCnt)."child<br>";

    $ParentLi = str_replace('{CHILD_CONTENT}', $childCnt , $ParentLi ); 




    $menu_data.=$ParentLi; 
  }

 // echo htmlspecialchars( $menu_data ); 

  return $menu_data;

}


function recursive_menu( $Child_menu , $CHILD_UL , $CHILD_LI )
{
  
  $CI =& get_instance();

   $url="javascript::void(0)";

   $menu_data = '';

  foreach ($Child_menu as $datas)
  {
                          
    $url = getMenuURL( $datas['href'] );

    $ChildLi = str_replace('{URL}', $url , $CHILD_LI );  
    $ChildLi = str_replace('{MENU_LABEL}',  $datas['text']  , $ChildLi );  

    $childCnt ='';

     if( isset( $datas['children'] ) )
    {
      $childCnt = recursive_menu( $datas['children'] , $CHILD_UL , $CHILD_LI );
    }

    $ChildLi = str_replace('{CHILD_CONTENT}', $childCnt , $ChildLi ); 

    $menu_data.= $ChildLi; 

  }
    
  $menu_data = str_replace('{CHILD_CONTENT}', $menu_data , $CHILD_UL );  
   
  return  $menu_data;


}

 
   function PermissionCheck( $data , $sectionSettings=0)
  { 
      $CI =& get_instance();
      $rolePermission = 1;

      if( !empty( $data['href'] ) )
      {
        $modualData = $CI->db->query("select * from module_mangement where module_mangement_id = ".$data['href'] )->row_array();
        
        $Module = trim( $modualData['module'] );

        $PermissionType = trim( $modualData['required_premission'] ); 

        $result = [];
        $needl = ",";

        if( substr_count( $PermissionType ,'||') )
        {
            $needl = "||";
        }
        else if( substr_count( $PermissionType ,'&&') )
        {
            $needl = "&&";
        }

        $PermissionType = explode( $needl , $PermissionType );              

        //print_r($modualData);
    

        foreach ($PermissionType as $perVal) 
        {
            $result[] = $_SESSION['permission'][ $Module ][ $perVal ]; 
        }

        $result = array_keys( $result , 1);

        if( ( $needl=="," || $needl=="||" ) && count($result) )
        {
            $rolePermission = 1;
        }
        else if ( $needl=="&&" &&  count($PermissionType) == count($result) )
        {
            $rolePermission = 1;                
        }
        else 
        {
            $rolePermission = 0;
        }

          //echo $Module."----".$rolePermission."---";print_r($result);
      }

      return $rolePermission;        
  }

  function subMenus($subMenus )
  {

      $CI =& get_instance();
      // $subMenus  = $CI->db->query("select * from menu_management where parent_id =$id ORDER BY menu_management_id ASC ")->result_array();   
   if( !empty($subMenus) )
      {           
          $pre = 0;
          $submenu  = [];
          foreach ($subMenus as $data)
          {
              $PermissionCheck = PermissionCheck( $data );

              if( !$PermissionCheck ) continue;
              $pre = 1;

              $result = subMenus( $data['children'] );

              if( !$result )
              {
                  $submenu[] = $data;
              }
              else if( $result!= 1 )
              {
                  $data['children'] = $result;
                  $submenu[] = $data; 
              }

          }
          
          if ($pre) return $submenu;
          else return 1;
      }
      else 
      {
          return 0;
      }
  }
  
  function GetMenus()
  {

      $CI =& get_instance();
      $getMenu  = $CI->db->query("select * from menu_manage where  firm_id = ".$_SESSION['firm_id']." ")->row_array();

      $mainMenus=json_decode($getMenu['menu_details'],true);


      // print_r($mainMenus);
      // exit();

      $menu = [];

      foreach($mainMenus as $key=> $data)
      {

          $PermissionCheck = PermissionCheck( $data );
          
          if ( !$PermissionCheck ) continue;

          $result = subMenus( $data['children']  );

          if( !$result ) 
          {
              $menu[] = $data;
          }
          else if( $result != 1 )
          {
              $data['children'] = $result;
              $menu[] = $data;
          }
          
      }
      return $menu;
      
  }

  function firm_mail_header( $firm_id ='' ) {
    $CI =& get_instance();
    if( $firm_id =='' )
    {
      $firm_id = $_SESSION['firm_id'];
    }
    $output = $CI->db->query('SELECT header1 FROM admin_setting WHERE firm_id = '.$firm_id)->row_array();
    return $output['header1'];
  }

  function firm_mail_footer( $firm_id ='' ) {
    $CI =& get_instance();

    if( $firm_id =='' )
    {
      $firm_id = $_SESSION['firm_id'];
    }

    $output = $CI->db->query('SELECT footer1 FROM admin_setting WHERE firm_id = '.$firm_id)->row_array();
    return $output['footer1'];
  }

  function email_header($firm_id) 
  {
    $CI =& get_instance();
    $output = $CI->db->query('SELECT header1 FROM admin_setting WHERE firm_id = '.$firm_id.'')->row_array();
    return $output['header1'];
  }

  function email_footer($firm_id) 
  {
    $CI =& get_instance();
    $output = $CI->db->query('SELECT footer1 FROM admin_setting WHERE firm_id = '.$firm_id.'')->row_array();
    return $output['footer1'];
  }

?>
