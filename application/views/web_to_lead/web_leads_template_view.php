<?php $this->load->view('includes/header');
   $succ = $this->session->flashdata('success');
   ?>
<style> 
   .columns > .editor {
   float: left;
   width: 70%;
   position: relative;
   z-index: 1;
   }
   .columns > .contacts {
   float: right;
   width: 30%;
   box-sizing: border-box;
   padding: 0 0 0 20px;
   }
   #contactList {
   list-style-type: none;
   margin: 0 !important;
   padding: 0;
   }
   #contactList li {
   background: #FAFAFA;
   margin-bottom: 1px;
   height: 36px;
   line-height: 30px;
   cursor: pointer;
   }
   #contactList li:nth-child(2n) {
   background: #F3F3F3;
   }
   #contactList li:hover {
   background: #FFFDE3;
   border-left: 5px solid #DCDAC1;
   margin-left: -5px;
   }
   div#myTemplate {
   z-index: 1041;
   }
   div.edit_template {
   z-index: 1041;
   }
   .my-error-class {
   border-color:1px red;
   }
   .dropdown-content {
    display: none;
    position: absolute;
    background-color: #fff;
    min-width: 86px;
    overflow: auto;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    left: -92px;
    width: 150px;
}
 tfoot {
    display: table-header-group;
}

</style>
<div class="modal-alertsuccess alert  succ popup_info_box" style="display:none;">
  <div class="newupdate_alert">
         <a href="#" class="close">×</a>
         <div class="pop-realted1">
         <div class="position-alert1">
              </div>
         </div>
         </div>
   </div>

<!-- <iframe src="http://remindoo.org/CRMTool/web_to_lead/web_leadform/ghdfgh" height="1000" width="1000" class="card" style="background: #fff !important; margin: 0 auto; float: none; display: block;border: none; box-shadow: 0 1px 11px 0 rgba(0, 0, 0, 0.12); padding: 20px;width: 700;" ></iframe>  -->
<div class="pcoded-content pcodedteam-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <!--start-->
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card propose-template">
                        <!-- admin start-->
                        <div class="modal-alertsuccess alert alert-success" style="display:none;">
                           <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                           <div class="pop-realted1">
                              <div class="position-alert1">
                                 YOU HAVE SUCCESSFULLY ADDED TEMPLATE
                              </div>
                           </div>
                        </div>
                        <div class="modal-alertsuccess alert alert-success-update" style="display:none;">
                           <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                           <div class="pop-realted1">
                              <div class="position-alert1">
                                 YOU HAVE SUCCESSFULLY UPDATED                              
                              </div>
                           </div>
                        </div>
                        <div class="modal-alertsuccess alert alert-danger" style="display:none;">
                           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                           <div class="pop-realted1">
                              <div class="position-alert1">
                                 Please Fill All the required fills.
                              </div>
                           </div>
                        </div>
                        <!-- List -->
                        <form action="<?php echo base_url(); ?>web_to_lead/web_leads_delete" class="require-validation" method="post">
                           <div class="all_user-section floating_set">
                              <div class="deadline-crm1 floating_set">
                                 <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard client-top-nav">
                                    <li class="nav-item">
                                       <a href="#" class="nav-link active"><!-- Web Leads Template -->Template List</a>
                                    </li>
									 <li class="nav-item">
                                       <a href="<?php echo base_url(); ?>web_to_lead" class="nav-link"><!-- Add Lead to Web -->New Template</a>
                                    </li>
                                 </ul>
                                 <div class="count-value1 pull-right ">
                                       <?php

                                       // print_r($_SESSION['permission']);
                                       // exit;

                                        if($_SESSION['permission']['Webtolead']['create']=='1'){?> 
                                    <a class="btn btn-primary" href="<?php echo base_url().'Web_to_lead';?>">
                                      <i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> Add New Template</a>  
                                      <?php } ?>      
                                    <!-- <button class="btn btn-primary" type="submit" name="submit"><i class="fa fa-trash fa-6" aria-hidden="true"></i> Delete</button> -->
                                    <button id="deleteTriger" class="deleteTri" style="display:none;">
                                      <i class="fa fa-trash fa-6 deleteUser" aria-hidden="true"></i> Delete</button>     
                                    <button type="button" id="deleteInvoice_rec" class="deleteInvoice_rec del-tsk12 f-right" style="display:none;"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true"></i>Delete</button>                        
                                 </div>
                              </div>
                              <div class="floating_set">
                                 <div class="tab-content <?php if($_SESSION['permission']['Webtolead']['view']!='1'){?> permission_deined <?php } ?>">
                                    <div id="allusers" class="tab-pane fade in active">
                                       <div id="task"></div>
                                       <div class="client_section3 table-responsive ">
                                          <div class="status_succ"></div>
                                          <div class="all-usera1 data-padding1 ">
                                             <div class="">
                                                <table class="table client_table1 text-center display nowrap printableArea" id="display_service1" cellspacing="0" width="100%">
                                                   <thead>
                                                      <tr class="text-uppercase">
                                                         <th style="display: none">
                                                            <label class="custom_checkbox1">                                                                
                                                               <input type="checkbox" id="select_all_invoice">
                                                               <i></i>
                                                            </label>
                                                           <!--  </div> -->
                                                         </th>
                                                         <th>S.no</th>
                                                         <th>Template Form ID</th>
                                                         <th style="display: none;">Template Form ID</th>
                                                         <th>HTML</th>
                                                         <th class="action_TH">Action</th>
                                                      </tr>
                                                   </thead>

                                                   <tbody>
                                                      <?php
                                                         $i=1;
                                                         foreach($result as $value){ ?>
                                                      <tr>
                                                         
                                                         <td style="display: none">
                                                            <label class="custom_checkbox1">
                                                                
                                                               <input type="checkbox" class="invoice_checkbox" data-invoice-id="<?php echo $value['id'];?>">
                                                              <i></i>
                                                               </label>
                                                            <!-- </div> -->
                                                         </td>
                                                         <td><?php echo $i; ?></td>
                                                         <!--   <td><a href="<?php echo base_url(); ?>web_to_lead/web_lead_form/<?php echo $value['form_id'];?>">
                                                            <?php echo $value['form_id'];?>
                                                            </a>
                                                            </td> -->
                                                         <td><a href="<?php echo base_url(); ?>web_to_lead/web_lead_form/<?php echo $value['id'];?>">
                                                            <?php echo $value['form_id'];?>
                                                            </a>
                                                         </td>

                                                           <td style="display: none;">
                                                            <?php echo $value['form_id'];?>
                                                          
                                                         </td>
                                                         <td id="iframes_<?php echo $value['id'];?>" class="frame_widths">
                                                          <span>
                                                        
                                                         <?php //echo $value['createdTime']; 
                                                            echo '&lt;iframe src="'.base_url().'web_to_lead/web_lead_form/'.$value['id'].'" height="1000" width="1000" class="card" style="background: #fff !important;
                                                            margin: 0 auto;
                                                            float: none;
                                                            display: block;border: none;
                                                            box-shadow: 0 1px 11px 0 rgba(0, 0, 0, 0.12);
                                                            padding: 20px;width: 700;"
                                                            &gt;&lt;/iframe&gt;';
                                                            ?></span></td>
                                                         <td>
                                                         <p class="action_01">
                                                            <div class="dropdown">
                                                               <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownLeadsMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="dropdwn-action"></span><span class="dropdwn-action"></span><span class="dropdwn-action"></span></button>
                                                               <div class="dropdown-menu" aria-labelledby="dropdownLeadsMenuButton">
                                                                  <?php if($_SESSION['permission']['Webtolead']['edit']=='1'){ ?>

                                                                  <a href="<?php echo base_url(); ?>web_to_lead/web_leads_update_contents/<?php echo $value['id']; ?>" class="greencolor">
                                                                  <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
                                                                  <?php } ?>

                                                                  <a href="javascript:void(0);" class="pinkclr copy_content" onclick="copyToClipboard('#iframes_<?php echo $value['id'] ?>','<?php echo $value['id'];?>')" data-id="<?php echo $value['id'];?>"><i class="fa fa-clipboard" aria-hidden="true"></i></a> 

                                                                  
                                                                  <?php if($_SESSION['permission']['Webtolead']['delete']=='1' && $value['default_template']!='1'){?> 
                                                                  <a href="javascript:void(0);" onclick="delete_data(this)"  data-toggle="modal" data-target="#deleteconfirmation" data-id="<?=$value['id']?>">
                                                                  <i class="icofont icofont-ui-delete" aria-hidden="true" ></i>
                                                                  </a>
                                                                  <?php } ?>
                                                               </div>
                                                            </div>
                                                         </p>
                                                          </td>   


                                           <!-- popup clipboard -->
                                       <!-- <div class="modal-alertsuccess alert alert-success" id="popupfor_clipboard<?php echo $value['id'];?>" style="display:none;">
                                                            <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                                                            <div class="pop-realted1">
                                                               <div class="position-alert1">
                                                                 Iframe for  <?php echo $value['form_id'];?> is Copied
                                                               </div>
                                                            </div>
                                                         </div> -->
                                           <!-- end of clip board -->
                                                      </tr>
                                                      <?php $i++; }  ?>
                                                   </tbody>
                                                </table>
                                                <input type="hidden" class="rows_selected" id="select_invoice_count" >              
                                             </div>
                                          </div>
                                          <!-- List -->           
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </form>
                        <!-- Page body end -->
                     </div>
                  </div>
                  <!-- Main-body end -->
                  <div id="styleSelector">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Email Template Popup -->
<div class="modal fade new-adds-company" id="myTemplate" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Template</h4>
         </div>
         <div class="modal-body">
            <form id="proposal_template" class="require-validation" method="post">
               <?php include('proposal_mail_content.php'); ?>
            </form>
         </div>
         <div class="modal-footer">
            <button id="submit">Save</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
 <div class="modal fade" id="deleteconfirmation" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
        <input type="hidden"  id="delete_id" value=""/>
<!--         <input type="hidden" id="delete_fun" value=""/>
 -->
          <p> Are you sure want to delete ?</p>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default"  onclick="delete_action()">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
      </div>

    </div>
  </div>
<!-- ajax loader -->
<!-- <div class="LoadingImage" ></div>
<style>
   .LoadingImage {
   display : none;
   position : fixed;
   z-index: 100;
   background-image : url('<?php echo site_url()?>assets/images/ajax-loader.gif');
   background-color:#666;
   opacity : 0.4;
   background-repeat : no-repeat;
   background-position : center;
   left : 0;
   bottom : 0;
   right : 0;
   top : 0;
   }
</style> -->
<!-- ajax loader end-->
<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<script src="https://cdn.ckeditor.com/4.9.0/standard-all/ckeditor.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> 
<script src="<?php echo base_url();?>assets/js/user_page/materialize.js"></script>
<script>
   // $(document).ready(function() {
   //     $('#display_service1').DataTable({
   //       "scrollX": true
   //   });
   //   });
     
</script>
<script type="text/javascript">
function delete_data(obj)
{
  $("#delete_id").val($(obj).attr("data-id"));
}
function delete_action()
{
  $("#deleteconfirmation").modal("hide");
  var id=$("#delete_id").val();
  if(!id){console.log("delete id undefined.");return;}
  $.ajax({
          type: "POST",
          url: "<?php echo base_url().'Web_to_lead/web_leadsDelete';?>",
          cache: false,
          data: {'id':id},
          beforeSend: function() {
            $(".LoadingImage").show();
          },success:function(res)
          {
            $(".LoadingImage").hide();
            if(parseInt(res))
            {
              $(".popup_info_box .position-alert1").html("delete Successfully.");
              $(".position-alert1").show();
               setTimeout(function(){location.reload();},1500)
            }
          },
        });
}
   $(document).ready(function(){



/*$(".archive_update").click(function(){
    var id=this.id;
            $.ajax({
                    url: 'http://remindoo.uk/web_to_lead/archive_update',
                    type : 'POST',
                    data : { 'id':id},                    
                      beforeSend: function() {
                        $(".LoadingImage").show();
                      },
                      success: function(data) {
                        //alert(data);
                        if(data==1){
                         $("#my_Modal_"+id).hide();
                         $(".LoadingImage").hide();
                         location.reload();
                        }
                      }

                    });
          });

*/      $( "#proposal_template" ).validate({
          errorClass: "my-error-class",
        rules: {
          template_title: "required",  
          template_content: "required",  
          template_type: "required"
        },
        messages: {
          template_title: "Please enter your Title",
          template_content: "Please enter your Title",
          template_type: "Please Select Template Type"
        },        
      });
   });
   CKEDITOR.replace( 'editor1', {
   extraPlugins: 'divarea,hcard,sourcedialog,justify'
   });
   'use strict';
   var CONTACTS = [
   { name: 'Username' },
   { name: 'Client Name' },
   { name: 'Accountant Name' },
   { name: 'Task Number' },
   { name: 'Task Name' },
   { name: ' Task Due Date'},
   { name: ' Task Client' },
   { name: 'Firm Name' },
   { name: 'accountant number' },
   { name: 'invoice amount' },
   { name: 'firm name' },
   { name: 'invoice due date' },      
   ];
   CKEDITOR.disableAutoInline = true;   
   CKEDITOR.plugins.add( 'hcard', {
   requires: 'widget',
   init: function( editor ) {
    editor.widgets.add( 'hcard', {
      allowedContent: 'span(!h-card); a[href](!u-email,!p-name); span(!p-tel)',
      requiredContent: 'span(h-card)',
      pathName: 'hcard',
      upcast: function( el ) {
        return el.name == 'span' && el.hasClass( 'h-card' );
      }
    } );        
    editor.addFeature( editor.widgets.registered.hcard );       
    editor.on( 'paste', function( evt ) {
      var contact = evt.data.dataTransfer.getData( 'contact' );
      if ( !contact ) {
        return;
      }
      evt.data.dataValue =            
        '<span class="content h-card">' +
          '['+contact.name+']'  +
        '</span>';
    } );
   }
   });
   CKEDITOR.on( 'instanceReady', function() {     
   CKEDITOR.document.getById( 'contactList' ).on( 'dragstart', function( evt ) {      
    var target = evt.data.getTarget().getAscendant( 'div', true );
    
    CKEDITOR.plugins.clipboard.initDragDataTransfer( evt );
    var dataTransfer = evt.data.dataTransfer;       
    dataTransfer.setData( 'contact', CONTACTS[ target.data( 'contact' ) ] );  
    dataTransfer.setData( 'text/html', target.getText() );        
   } );
   } );
   $("#template_title").keyup(function(){
   $("#template_title").removeAttr("style");
   });
   $("#template_type").change(function(){
   $("#template_type").removeAttr("style");
   });
   $("#submit").click(function(){     
    // var template_content=$(".cke_wysiwyg_div > p").text();
    var template_content1=$(".cke_wysiwyg_div").html();
    $('div[data-cke-hidden-sel="1"]').remove();
    var template_content=$(".cke_wysiwyg_div").html();
    var  template_type=$("#template_type").val();
    var  template_title=$("#template_title").val();
    if(template_content==''){
      $("#error").show();   
      setTimeout(function() {
        $("#error").hide(); 
      }, 2000);     
    }
    if(template_type=='none'){;
      $("#template_type").css({"border-color": "red", 
      "border-width":"1px", 
      "border-style":"solid"});
    }
    if(template_title==''){
      $("#template_title").css({"border-color": "red", 
      "border-width":"1px", 
      "border-style":"solid"});
    }
   if(template_content!='' && template_type!='none' && template_title!=''){ 
    var formData={'template_content':template_content,'template_title':template_title,'template_type':template_type};       
     $.ajax({
                    url: '<?php echo base_url();?>proposal/insert_template/',
                    type : 'POST',
                    data : formData,                    
           beforeSend: function() {
                 $(".loading-image").show();
              },
              success: function(msg) {
                $(".loading-image").hide();
                  if(msg=='0'){
                      $('.alert-danger').show();
                  }else{
                  setTimeout(function() { 
                    $('.alert-success').show();
                     location.reload();  
                               }, 1000);                    
                }
              }
   
                  });
   }else{
    $('.alert-danger').show();
     setTimeout(function() { 
      $('.alert-danger').hide();
    }, 1000); 
   }
   });
   $('.test').change(function() {  
   var task_status = $(this).val();
   var id=this.id;
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>proposal/update_status",
      data:  'id='+id+"&task_status="+task_status, 
        beforeSend: function() {
          $(".loading-image").show();
        },
        success: function(data){     
           $(".loading-image").hide();        
          $(".alert-success-update").show();
          setTimeout(function() { $(".alert-success-update").hide(); 
          location.reload(); }, 5000);
        }
    });  
   });
</script> 
<script type="text/javascript">
   $('#timepicker1').timepicker();
   
</script> 
<script type="text/javascript">
   $(document).ready(function() {
    $("#select_all_invoice").change(function(){
      if(this.checked){
        $(".invoice_checkbox").each(function(){
          this.checked=true;
          $(".deleteInvoice_rec").show();
        })              
      }else{
        $(".invoice_checkbox").each(function(){
          this.checked=false;
          $(".deleteInvoice_rec").hide();
        })              
      }
      $("#select_invoice_count").val($("input.invoice_checkbox:checked").length+" Selected");
    });
   
    $(".invoice_checkbox").click(function () {
      if ($(this).is(":checked")){
        var isAllChecked = 0;
   
        $(".invoice_checkbox").each(function(){
          if(!this.checked)
             isAllChecked = 1;
           $(".deleteInvoice_rec").show();
        })              
        if(isAllChecked == 0){ $("#select_all_invoice").prop("checked", true); }     
      }else {
        $("#select_all_invoice").prop("checked", false);
      }
      $("#select_invoice_count").val($("input.invoice_checkbox:checked").length+" Selected");
    });
   
   $(document).on('click', '#deleteInvoice_rec', function() {
    var invoice = [];
    $(".invoice_checkbox:checked").each(function() {
      invoice.push($(this).data('invoice-id'));
    });
   
    if(invoice.length <=0) {
       $('.alert-danger-check').show();
    } else {
      $('#delete_all_invoice'+invoice).show();
      $('.delete_yes').click(function() {
         var selected_invoice_values = invoice.join(",");
   
        $.ajax({
          type: "POST",
          url: "<?php echo base_url().'Web_to_lead/web_leadsDelete';?>",
          cache: false,
          data: 'data_ids='+selected_invoice_values,
          beforeSend: function() {
            $(".LoadingImage").show();
          },
          success: function(data) {
            $(".LoadingImage").hide();
            var invoice_ids = data.split(",");
            for (var i=0; i < invoice_ids.length; i++ ) { 
            $("#"+invoice_ids[i]).remove(); } 
            $(".alert-success-delete").show();
            setTimeout(function() { 
            location.reload(); }, 500);     
          }
        });
      });
    }
   });    
   
   
   });
   
   $(document).ready(function(){
    var check=0;
    var check1=0;
    var numCols = $('#display_service1 thead th').length;   
    //alert(numCols);
        var table10 = $('#display_service1').DataTable({
        "pageLength": "<?php echo get_firm_page_length() ?>",
        "dom": '<"top"fl>rt<"bottom"ip><"clear">',
         order: [[0, 'asc']],
          columnDefs: 
          [
            {"orderable": false,"targets": ['source_chk_TH','action_TH']}
          ],
       initComplete: function () { 
                  // var q=1;
                  //    $('#display_service1 tfoot th').find('.filter_check').each( function(){
                  //    // alert('ok');
                  //      $(this).attr('id',"wb_"+q);
                  //      q++;
                  //    });
                  // for(i=1;i<numCols;i++){ 

                  //       if(i==2){
                  //       check=1;          
                  //       i=Number(i) + 1;
                  //       var select = $("#wb_2"); 
                  //       }else{
                  //       var select = $("#wb_"+i); 
                  //       }
                  //     this.api().columns([i]).every( function () {
                  //       var column = this;
                  //       column.data().unique().sort().each( function ( d, j ) {    
                  //         console.log(d);         
                  //         select.append( '<option value="'+d+'">'+d+'</option>' )
                  //       });
                  //     }); 

                  //       if(check=='1'){                 
                  //      i=Number(i) - 1;
                  //      check=0;
                  //   }
           
                  //    $("#wb_"+i).formSelect();  
                  // }
        }
    });
      // for(j=1;j<numCols;j++){  
      //     $('#wb_'+j).on('change', function(){ 
      //       var result=$(this).attr('id').split('_');      
      //        var c=result[1];
      //         var search = [];              
      //         $.each($('#wb_'+c+ ' option:selected'), function(){                
      //             search.push($(this).val());
      //         });      
      //         search = search.join('|');    
      //           if(c==2){               
      //           c=Number(c) + 1;
      //         }         

      //         table10.column(c).search(search, true, false).draw();  
      //     });
      //  }
$(".popup_info_box .close").click(function(){$(".popup_info_box").hide();});

$('input[type="radio"]').parents(".form-group").find(".radio-group").addClass("radio_button01");

});
   
</script>
<script type="text/javascript">
   /** for clipboard **/

function copyToClipboard(element,id) {
   //alert('cop');
  var $temp = $("<input>");
  $("body").append($temp);
  $temp.val($.trim($(element).text())).select();
  document.execCommand("copy");
  $temp.remove();

              $(".popup_info_box .position-alert1").html("Iframe Is Copied Successfully");
//  $('#popupfor_clipboard'+id).show();
$(".popup_info_box").show();
           /* setTimeout(function() { 
           //  $('#popupfor_clipboard'+id).hide(); 

  $(".popup_info_box").hide();

           }, 500); */
}
   /** end of clipboard **/

</script>
</body>
</html>