<?php

$this->load->view('includes/header');
$succ = $this->session->flashdata('success');
$uri_seg_menu = $this->uri->segment('1');
$uri_seg_menu1 = $this->uri->segment('2');
$uri_seg_menu2 = $this->uri->segment('3');
//echo $uri_seg_menu."---".$uri_seg_menu1."--".$uri_seg_menu2;
if ($uri_seg_menu == 'web_to_lead' && $uri_seg_menu1 == '') {
	$active_class32 = "head-active";
	$active_class34 = "head-active";
	$active_class3 = "head-active";
}
?>
<style>
	.copy-button {
		display: none !important;
	}

	button#frmb-0-view-data {
		display: none;
	}

	.vld_hidden,
	.vald_rules-wrap {
		display: none !important;
	}


	.frm-holder .dropdown-main ul li {
		padding: 5px 10px !important;
		line-height: initial;
	}
</style>

<link href="<?php echo base_url(); ?>assets/css/jquery.filer.css" type="text/css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/css/jquery.filer-dragdropbox-theme.css" type="text/css" rel="stylesheet" />

<!-- Tree Select -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/tree_select/style.css?ver=2">
<!-- -->
<input type="hidden" name="get_id" id="get_id" value="<?php echo $value['id'] ?>">
<?php
	$team = $this->db->query("select * from team where create_by=" . $_SESSION['id'] . " ")->result_array();
	$department = $this->db->query("select * from department_permission where create_by=" . $_SESSION['id'] . " ")->result_array();
?>

<div class="modal-alertsuccess alert  succ popup_info_box" style="display:none;">
	<div class="newupdate_alert">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
		<div class="pop-realted1">
			<div class="position-alert1">
			</div>
		</div>
	</div>
</div>

<div class="pcoded-content new-formbuilder1 ">
	<div class="lead-post-01">

		<!-- <div class="realign_leads floating_set">
		<div class="pull-left">
		
	</div> -->

	</div>

	<div class="public_builder floating_set public_template_builder">
		<div class="deadline-crm1 floating_set we_lead_cls">
			<ol class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard client-top-nav">
				<li class="nav-item">
					<a class="nav-link" href="https://remindoo.uk/web_to_lead/web_leads_view">Template List</a>
				</li>
				<li class="nav-item">
					<a class="nav-link active" href="<?php echo base_url(); ?>web_leads_view">New Template</a>
				</li>
			</ol>

			<ol class="nav nav-tabs1 all_user1 md-tabs pull-left u-dashboard">
				<li class="nav-item form_info">
					<a class="nav-link form_info active" data-toggle="tab" onclick="previous_btn()" href="#allusers">Form Information & Setup</a>
				</li>
				<li class="nav-item form_builder">
					<a class="nav-link form_builder" data-toggle="tab" onclick="nxt_btn()" href="#tab_form_build"> Form Builder</a>
				</li>
				<!-- <li class="nav-item ">
                        <a class="nav-link" data-toggle="tab" href="#newinactive">Integration Code</a>
                      </li> -->
			</ol>

			<div class="Footer common-clienttab pull-right">
				<div class="divleft">
					<a class="nav-link  active" data-toggle="tab" href="#allusers" style="padding: 0px"> <button class="signed-change2" type="button" value="Previous Tab" style="display:none" text="Previous Tab">Previous
						</button></a>
				</div>
				<div class="divright">
					<a class="nav-link " data-toggle="tab" href="#tab_form_build">
						<button class="signed-change3" type="button" value="Next Tab" text="Next Tab">Next
						</button>
					</a>
				</div>
			</div>

		</div>

		<div class="form-build-action  floating_set">
			<div class="tab-content <?php if ($_SESSION['permission']['Webtolead']['create'] != '1') { ?> permission_deined <?php } ?>">

				<div id="tab_form_build" class="tab-pane fade">
					<div id="build-wrap"></div>
				</div> <!-- 1tab -->

				<div class="tab-pane fade in active" id="allusers">
					<div class="public-bill01 build-integrate">
						<div class="add_new_post04">
							<div class="row">
								<div class="space-equal-data col-sm-12 space_cls temp-upper-data">
									<div class=" form-group col-sm-6">
										<label>Form Name</label>
										<input type="text" name="form_name" id="form_name" class="form-control clr-check-client" placeholder="Enter Name" value="<?php if (isset($value['form_id']) && $value['form_id'] != '') {
																																					echo $value['form_id'];
																																				} ?>">
										<span id="err_name" style="color:red; display:none">Please Enter Your Form Name</span>
										<span style="color:red" id="form_id_error"></span>
									</div>

									<div class="form-group col-sm-6">
										<label>Language</label>
										<div class="dropdown-sin-2">
											<select name="default_language" id="default_language" class="clr-check-select-cus">
												<?php foreach ($Language as $l_key => $l_value) { ?>
													<option value="<?php echo $l_value['id']; ?>" <?php if (isset($value['language']) && $value['language'] == $l_value['id']) {
																										echo 'selected="selected"';
																									} ?>><?php echo $l_value['name']; ?></option>
												<?php } ?>
											</select></div>
									</div>
									<div class="form-group col-sm-6">
										<label>Source</label>
										<div class="dropdown-sin-2">
											<select name="source[]" id="source" multiple="multiple" class="clr-check-select-cus">
												<?php
												$i = 0;
												foreach ($source as $lsource_value) { ?>
													<!--   <option value='<?php /*echo $lsource_value['id'];?>' <?php if(isset($value['lead_source']) && $value['lead_source']==$lsource_value['id']){ echo 'selected="selected"'; }?>><?php echo $lsource_value['source_name']; */ ?></option> -->
													<option value='<?php echo $lsource_value['id']; ?>' <?php if (isset($value['lead_source']) && (in_array($lsource_value['id'], explode(',', $value['lead_source'])))) {
																											echo 'selected="selected"';
																										}
																										if (!isset($value['lead_source']) && ($i == 0)) {
																											echo 'selected="selected"';
																										}  ?>><?php echo $lsource_value['source_name']; ?></option>
												<?php
													$i++;
												} ?>
												<!-- <option value='1'>Google</option>
				                  <option value-'2'>Facebook</option> -->
											</select>
										</div>
										<span id="err_source" style="color:red; display:none">This field is Required</span>
									</div>

									<div class="form-group col-sm-6">
										<label> Status</label>
										<div class="dropdown-sin-2">
											<select name="lead_status" id="lead_status" class="clr-check-select-cus">
												<?php foreach ($leads_status as $ls_value) { ?>
													<option value='<?php echo $ls_value['id']; ?>' <?php if (isset($value['lead_status']) && $value['lead_status'] == $ls_value['id']) {
																										echo 'selected="selected"';
																									}  ?>><?php echo $ls_value['status_name']; ?></option>
												<?php } ?>
											</select>
										</div>
										<span id="err_status" style="color:red; display:none">This field is Required</span>
									</div>
									<div class="form-group col-sm-6">
										<label>Responsible (Assignee)</label>
										<input type="text" class="tree_select" id="tree_select" name="assignees[]" placeholder="Select">
										<input type="hidden" id="assign_role" name="assign_role">
										<span id="err_responsible" style="color:red; display:none">This field is Required</span>
									</div>
								</div> <!-- col-sm-6 -->

								<div class="space-equal-data col-sm-6 pad_none">

									<!-- <div class="form-group">
							<label>Notification settings</label>
							<div class="checkbox-color checkbox-primary">
							<input type="checkbox" name="radio" id="repeat11"><label for="repeat11">Auto mark as public</label>
							</div>
							</div> -->
									<?php //echo $value['notify_to']."testtest";
									?>
									<!-- 		<div class="form-group form-radio">
							<div class="radio radio-inline">
							<label><input type="radio" name="member_notify" value="specific_staff" <?php if (isset($value['notify_to']) && $value['notify_to'] == 'specific_staff') {
																										echo 'checked="checked"';
																									} ?> ><i class="helper"></i>Specific Staff Members </label>
							</div>
							
							<div class="radio radio-inline">
							<label><input type="radio" name="member_notify" value="specific_roles" <?php if (isset($value['notify_to']) && $value['notify_to'] == 'specific_roles') {
																										echo 'checked="checked"';
																									} ?> ><i class="helper"></i>Staff members with roles</label>
							</div>
							
							<div class="radio radio-inline">
							<label><input type="radio" name="member_notify" value="responsible_person" <?php if (isset($value['notify_to']) && $value['notify_to'] == 'responsible_person') {
																											echo 'checked="checked"';
																										}
																										if (!isset($value['notify_to'])) {
																											echo 'checked="checked"';
																										} ?> ><i class="helper"></i>Responsible person </label>
							</div>
							
							</div> -->

									<div class="form-group" <?php if (isset($value['notify_to']) && $value['notify_to'] == 'specific_staff') { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?> id="notify_staff_members">
										<label> Staff Members to Notify</label>
										<div class="dropdown-sin-2">
											<select name="staff_members" id="staff_members" multiple="multiple" placeholder="Select Staff Members">
												<?php
												if (isset($staff_form) && $staff_form != '') {
													foreach ($staff_form as $valuess) {
												?>
														<option value="<?php echo $valuess['id']; ?>" <?php /* if(isset($value['notify_user_id']) && $value['notify_user_id']==$valuess['id']){ echo 'selected="selected"'; }?>><?php echo $valuess["crm_name"]; */
																										?> <?php if (isset($value['notify_user_id']) && in_array($valuess['id'], explode(',', $value['notify_user_id']))) {
															echo 'selected="selected"';
														} ?>><?php echo $valuess["crm_name"]; ?>
														</option>
												<?php
													}
												}
												?>
											</select>
										</div>
									</div>

									<div class="form-group" <?php if (isset($value['notify_to']) && $value['notify_to'] == 'specific_roles') { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?> id="notify_role">
										<label> Roles to Notify</label>
										<div class="dropdown-sin-2">
											<select name="notify_assigned" id="notify_assigned" multiple="multiple" placeholder="Select Roles to Notify">
												<?php
												if (isset($roles)) {
													foreach ($roles as $r_value) {
												?>
														<option value="<?php echo $r_value['id']; ?>" <?php /*if(isset($value['notify_role_id']) && $value['notify_role_id']==$r_value['id']){ echo 'selected="selected"'; }?> ><?php echo $r_value["role"]; */ ?> <?php if (isset($value['notify_role_id']) && in_array($r_value['id'], explode(',', $value['notify_role_id']))) {
																																																																		echo 'selected="selected"';
																																																																	} ?>><?php echo $r_value["role"]; ?>
														</option>
												<?php
													}
												}
												?>
											</select>
										</div>
									</div>

									<input type="hidden" name="form_rec_id" id="form_rec_id" value="<?php if (isset($value['id']) && $value['id'] != '') {
																										echo $value['id'];
																									} else {
																										echo '';
																									} ?>">


									<div class="form-group create-btn new_task-bts1">
										<!--  <input type="submit" name="add_form_info" id="add_form_info" class="btn-primary" value="SAVE"> -->

									</div>
								</div> <!-- col-sm-6 -->
							</div>
						</div>
					</div>

				</div><!-- 2tab -->


				<script src="<?php echo base_url(); ?>assets/form-builder/vendor.js?v=1.9.6"></script>
				<?php $this->load->view('includes/session_timeout'); ?>
				<?php $this->load->view('includes/footer'); ?>
				<link href='<?php echo base_url(); ?>assets/form-builder/form-builder.min.css' rel='stylesheet' />

				<script type="text/javascript">
					$(document).ready(function() {
						$('.access-wrap').removeClass('checkbox-color checkbox-primary');
					});
				</script>

				<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
				<!-- 
<script src="http://remindoo.org/perfex_crm/assets/plugins/jquery/jquery-migrate.min.js"></script>

<script src="http://remindoo.org/perfex_crm/assets/plugins/app-build/moment.min.js"></script>
<script src='http://remindoo.org/perfex_crm/assets/plugins/app-build/bootstrap-select.min.js?v=1.9.6'></script>
<script src="http://remindoo.org/perfex_crm/assets/plugins/tinymce/tinymce.min.js?v=1.9.6"></script>
<script src='http://remindoo.org/perfex_crm/assets/plugins/jquery-validation/jquery.validate.min.js?v=1.9.6'></script>
<script src="http://remindoo.org/perfex_crm/assets/js/main.min.js?v=1.9.6"></script> -->
				<script src="<?php echo base_url(); ?>assets/form-builder/form-builder.js"></script>

				<!-- <script src="<?php echo base_url(); ?>assets/js/jquery.dropdown.js"></script> -->
				<script src="<?php echo base_url(); ?>assets/js/mock.js"></script>

				<script>
					var buildWrap = document.getElementById('build-wrap');
					<?php if (!isset($value['form_json'])) { ?>
						var formData = "[\n {\n  \"type\": \"header\",\n  \"subtype\": \"h1\",\n  \"label\": \"Header\",\n  \"className\": \"header\"\n },\n {\n  \"type\": \"text\",\n  \"required\": true,\n  \"label\": \"Name\",\n  \"subtype\": \"text\",\n  \"className\": \"form-control\",\n  \"name\": \"name\"\n },\n {\n  \"type\": \"file\",\n  \"label\": \"File Upload\",\n  \"className\": \"form-control\",\n  \"name\": \"file-input\"\n },\n {\n  \"type\": \"text\",\n  \"label\": \"Position\",\n  \"subtype\": \"text\",\n  \"className\": \"form-control\",\n  \"name\": \"title\"\n },\n {\n  \"type\": \"text\",\n  \"label\": \"Email Address\",\n  \"className\": \"form-control\",\n  \"name\": \"email\"\n },\n {\n  \"type\": \"text\",\n  \"label\": \"Phone\",\n  \"subtype\": \"text\",\n  \"className\": \"form-control\",\n  \"name\": \"phonenumber\"\n },\n {\n  \"type\": \"text\",\n  \"label\": \"Company\",\n  \"subtype\": \"text\",\n  \"className\": \"form-control\",\n  \"name\": \"company\"\n },\n {\n  \"type\": \"textarea\",\n  \"label\": \"Address\",\n  \"className\": \"form-control\",\n  \"name\": \"address\"\n },\n {\n  \"type\": \"text\",\n  \"label\": \"City\",\n  \"subtype\": \"text\",\n  \"className\": \"form-control\",\n  \"name\": \"city\"\n },\n {\n  \"type\": \"text\",\n  \"label\": \"State\",\n  \"subtype\": \"text\",\n  \"className\": \"form-control\",\n  \"name\": \"state\"\n },\n {\n  \"type\": \"select\",\n  \"label\": \"Country\",\n  \"className\": \"form-control\",\n  \"name\": \"country\",\n  \"values\": [\n   {\n    \"label\": \"Afghanistan\",\n    \"value\": \"1\"\n   },\n   {\n    \"label\": \"Aland Islands\",\n    \"value\": \"2\"\n   },\n   {\n    \"label\": \"Albania\",\n    \"value\": \"3\"\n   },\n   {\n    \"label\": \"Algeria\",\n    \"value\": \"4\"\n   },\n   {\n    \"label\": \"American Samoa\",\n    \"value\": \"5\"\n   },\n   {\n    \"label\": \"Andorra\",\n    \"value\": \"6\"\n   },\n   {\n    \"label\": \"Angola\",\n    \"value\": \"7\"\n   },\n   {\n    \"label\": \"Anguilla\",\n    \"value\": \"8\"\n   },\n   {\n    \"label\": \"Antarctica\",\n    \"value\": \"9\"\n   },\n   {\n    \"label\": \"Antigua and Barbuda\",\n    \"value\": \"10\"\n   },\n   {\n    \"label\": \"Argentina\",\n    \"value\": \"11\"\n   },\n   {\n    \"label\": \"Armenia\",\n    \"value\": \"12\"\n   },\n   {\n    \"label\": \"Aruba\",\n    \"value\": \"13\"\n   },\n   {\n    \"label\": \"Australia\",\n    \"value\": \"14\"\n   },\n   {\n    \"label\": \"Austria\",\n    \"value\": \"15\"\n   },\n   {\n    \"label\": \"Azerbaijan\",\n    \"value\": \"16\"\n   },\n   {\n    \"label\": \"Bahamas\",\n    \"value\": \"17\"\n   },\n   {\n    \"label\": \"Bahrain\",\n    \"value\": \"18\"\n   },\n   {\n    \"label\": \"Bangladesh\",\n    \"value\": \"19\"\n   },\n   {\n    \"label\": \"Barbados\",\n    \"value\": \"20\"\n   },\n   {\n    \"label\": \"Belarus\",\n    \"value\": \"21\"\n   },\n   {\n    \"label\": \"Belgium\",\n    \"value\": \"22\"\n   },\n   {\n    \"label\": \"Belize\",\n    \"value\": \"23\"\n   },\n   {\n    \"label\": \"Benin\",\n    \"value\": \"24\"\n   },\n   {\n    \"label\": \"Bermuda\",\n    \"value\": \"25\"\n   },\n   {\n    \"label\": \"Bhutan\",\n    \"value\": \"26\"\n   },\n   {\n    \"label\": \"Bolivia\",\n    \"value\": \"27\"\n   },\n   {\n    \"label\": \"Bonaire, Sint Eustatius and Saba\",\n    \"value\": \"28\"\n   },\n   {\n    \"label\": \"Bosnia and Herzegovina\",\n    \"value\": \"29\"\n   },\n   {\n    \"label\": \"Botswana\",\n    \"value\": \"30\"\n   },\n   {\n    \"label\": \"Bouvet Island\",\n    \"value\": \"31\"\n   },\n   {\n    \"label\": \"Brazil\",\n    \"value\": \"32\"\n   },\n   {\n    \"label\": \"British Indian Ocean Territory\",\n    \"value\": \"33\"\n   },\n   {\n    \"label\": \"Brunei\",\n    \"value\": \"34\"\n   },\n   {\n    \"label\": \"Bulgaria\",\n    \"value\": \"35\"\n   },\n   {\n    \"label\": \"Burkina Faso\",\n    \"value\": \"36\"\n   },\n   {\n    \"label\": \"Burundi\",\n    \"value\": \"37\"\n   },\n   {\n    \"label\": \"Cambodia\",\n    \"value\": \"38\"\n   },\n   {\n    \"label\": \"Cameroon\",\n    \"value\": \"39\"\n   },\n   {\n    \"label\": \"Canada\",\n    \"value\": \"40\"\n   },\n   {\n    \"label\": \"Cape Verde\",\n    \"value\": \"41\"\n   },\n   {\n    \"label\": \"Cayman Islands\",\n    \"value\": \"42\"\n   },\n   {\n    \"label\": \"Central African Republic\",\n    \"value\": \"43\"\n   },\n   {\n    \"label\": \"Chad\",\n    \"value\": \"44\"\n   },\n   {\n    \"label\": \"Chile\",\n    \"value\": \"45\"\n   },\n   {\n    \"label\": \"China\",\n    \"value\": \"46\"\n   },\n   {\n    \"label\": \"Christmas Island\",\n    \"value\": \"47\"\n   },\n   {\n    \"label\": \"Cocos (Keeling) Islands\",\n    \"value\": \"48\"\n   },\n   {\n    \"label\": \"Colombia\",\n    \"value\": \"49\"\n   },\n   {\n    \"label\": \"Comoros\",\n    \"value\": \"50\"\n   },\n   {\n    \"label\": \"Congo\",\n    \"value\": \"51\"\n   },\n   {\n    \"label\": \"Cook Islands\",\n    \"value\": \"52\"\n   },\n   {\n    \"label\": \"Costa Rica\",\n    \"value\": \"53\"\n   },\n   {\n    \"label\": \"Cote d`ivoire (Ivory Coast)\",\n    \"value\": \"54\"\n   },\n   {\n    \"label\": \"Croatia\",\n    \"value\": \"55\"\n   },\n   {\n    \"label\": \"Cuba\",\n    \"value\": \"56\"\n   },\n   {\n    \"label\": \"Curacao\",\n    \"value\": \"57\"\n   },\n   {\n    \"label\": \"Cyprus\",\n    \"value\": \"58\"\n   },\n   {\n    \"label\": \"Czech Republic\",\n    \"value\": \"59\"\n   },\n   {\n    \"label\": \"Democratic Republic of the Congo\",\n    \"value\": \"60\"\n   },\n   {\n    \"label\": \"Denmark\",\n    \"value\": \"61\"\n   },\n   {\n    \"label\": \"Djibouti\",\n    \"value\": \"62\"\n   },\n   {\n    \"label\": \"Dominica\",\n    \"value\": \"63\"\n   },\n   {\n    \"label\": \"Dominican Republic\",\n    \"value\": \"64\"\n   },\n   {\n    \"label\": \"Ecuador\",\n    \"value\": \"65\"\n   },\n   {\n    \"label\": \"Egypt\",\n    \"value\": \"66\"\n   },\n   {\n    \"label\": \"El Salvador\",\n    \"value\": \"67\"\n   },\n   {\n    \"label\": \"Equatorial Guinea\",\n    \"value\": \"68\"\n   },\n   {\n    \"label\": \"Eritrea\",\n    \"value\": \"69\"\n   },\n   {\n    \"label\": \"Estonia\",\n    \"value\": \"70\"\n   },\n   {\n    \"label\": \"Ethiopia\",\n    \"value\": \"71\"\n   },\n   {\n    \"label\": \"Falkland Islands (Malvinas)\",\n    \"value\": \"72\"\n   },\n   {\n    \"label\": \"Faroe Islands\",\n    \"value\": \"73\"\n   },\n   {\n    \"label\": \"Fiji\",\n    \"value\": \"74\"\n   },\n   {\n    \"label\": \"Finland\",\n    \"value\": \"75\"\n   },\n   {\n    \"label\": \"France\",\n    \"value\": \"76\"\n   },\n   {\n    \"label\": \"French Guiana\",\n    \"value\": \"77\"\n   },\n   {\n    \"label\": \"French Polynesia\",\n    \"value\": \"78\"\n   },\n   {\n    \"label\": \"French Southern Territories\",\n    \"value\": \"79\"\n   },\n   {\n    \"label\": \"Gabon\",\n    \"value\": \"80\"\n   },\n   {\n    \"label\": \"Gambia\",\n    \"value\": \"81\"\n   },\n   {\n    \"label\": \"Georgia\",\n    \"value\": \"82\"\n   },\n   {\n    \"label\": \"Germany\",\n    \"value\": \"83\"\n   },\n   {\n    \"label\": \"Ghana\",\n    \"value\": \"84\"\n   },\n   {\n    \"label\": \"Gibraltar\",\n    \"value\": \"85\"\n   },\n   {\n    \"label\": \"Greece\",\n    \"value\": \"86\"\n   },\n   {\n    \"label\": \"Greenland\",\n    \"value\": \"87\"\n   },\n   {\n    \"label\": \"Grenada\",\n    \"value\": \"88\"\n   },\n   {\n    \"label\": \"Guadaloupe\",\n    \"value\": \"89\"\n   },\n   {\n    \"label\": \"Guam\",\n    \"value\": \"90\"\n   },\n   {\n    \"label\": \"Guatemala\",\n    \"value\": \"91\"\n   },\n   {\n    \"label\": \"Guernsey\",\n    \"value\": \"92\"\n   },\n   {\n    \"label\": \"Guinea\",\n    \"value\": \"93\"\n   },\n   {\n    \"label\": \"Guinea-Bissau\",\n    \"value\": \"94\"\n   },\n   {\n    \"label\": \"Guyana\",\n    \"value\": \"95\"\n   },\n   {\n    \"label\": \"Haiti\",\n    \"value\": \"96\"\n   },\n   {\n    \"label\": \"Heard Island and McDonald Islands\",\n    \"value\": \"97\"\n   },\n   {\n    \"label\": \"Honduras\",\n    \"value\": \"98\"\n   },\n   {\n    \"label\": \"Hong Kong\",\n    \"value\": \"99\"\n   },\n   {\n    \"label\": \"Hungary\",\n    \"value\": \"100\"\n   },\n   {\n    \"label\": \"Iceland\",\n    \"value\": \"101\"\n   },\n   {\n    \"label\": \"India\",\n    \"value\": \"102\"\n   },\n   {\n    \"label\": \"Indonesia\",\n    \"value\": \"103\"\n   },\n   {\n    \"label\": \"Iran\",\n    \"value\": \"104\"\n   },\n   {\n    \"label\": \"Iraq\",\n    \"value\": \"105\"\n   },\n   {\n    \"label\": \"Ireland\",\n    \"value\": \"106\"\n   },\n   {\n    \"label\": \"Isle of Man\",\n    \"value\": \"107\"\n   },\n   {\n    \"label\": \"Israel\",\n    \"value\": \"108\"\n   },\n   {\n    \"label\": \"Italy\",\n    \"value\": \"109\"\n   },\n   {\n    \"label\": \"Jamaica\",\n    \"value\": \"110\"\n   },\n   {\n    \"label\": \"Japan\",\n    \"value\": \"111\"\n   },\n   {\n    \"label\": \"Jersey\",\n    \"value\": \"112\"\n   },\n   {\n    \"label\": \"Jordan\",\n    \"value\": \"113\"\n   },\n   {\n    \"label\": \"Kazakhstan\",\n    \"value\": \"114\"\n   },\n   {\n    \"label\": \"Kenya\",\n    \"value\": \"115\"\n   },\n   {\n    \"label\": \"Kiribati\",\n    \"value\": \"116\"\n   },\n   {\n    \"label\": \"Kosovo\",\n    \"value\": \"117\"\n   },\n   {\n    \"label\": \"Kuwait\",\n    \"value\": \"118\"\n   },\n   {\n    \"label\": \"Kyrgyzstan\",\n    \"value\": \"119\"\n   },\n   {\n    \"label\": \"Laos\",\n    \"value\": \"120\"\n   },\n   {\n    \"label\": \"Latvia\",\n    \"value\": \"121\"\n   },\n   {\n    \"label\": \"Lebanon\",\n    \"value\": \"122\"\n   },\n   {\n    \"label\": \"Lesotho\",\n    \"value\": \"123\"\n   },\n   {\n    \"label\": \"Liberia\",\n    \"value\": \"124\"\n   },\n   {\n    \"label\": \"Libya\",\n    \"value\": \"125\"\n   },\n   {\n    \"label\": \"Liechtenstein\",\n    \"value\": \"126\"\n   },\n   {\n    \"label\": \"Lithuania\",\n    \"value\": \"127\"\n   },\n   {\n    \"label\": \"Luxembourg\",\n    \"value\": \"128\"\n   },\n   {\n    \"label\": \"Macao\",\n    \"value\": \"129\"\n   },\n   {\n    \"label\": \"Macedonia\",\n    \"value\": \"130\"\n   },\n   {\n    \"label\": \"Madagascar\",\n    \"value\": \"131\"\n   },\n   {\n    \"label\": \"Malawi\",\n    \"value\": \"132\"\n   },\n   {\n    \"label\": \"Malaysia\",\n    \"value\": \"133\"\n   },\n   {\n    \"label\": \"Maldives\",\n    \"value\": \"134\"\n   },\n   {\n    \"label\": \"Mali\",\n    \"value\": \"135\"\n   },\n   {\n    \"label\": \"Malta\",\n    \"value\": \"136\"\n   },\n   {\n    \"label\": \"Marshall Islands\",\n    \"value\": \"137\"\n   },\n   {\n    \"label\": \"Martinique\",\n    \"value\": \"138\"\n   },\n   {\n    \"label\": \"Mauritania\",\n    \"value\": \"139\"\n   },\n   {\n    \"label\": \"Mauritius\",\n    \"value\": \"140\"\n   },\n   {\n    \"label\": \"Mayotte\",\n    \"value\": \"141\"\n   },\n   {\n    \"label\": \"Mexico\",\n    \"value\": \"142\"\n   },\n   {\n    \"label\": \"Micronesia\",\n    \"value\": \"143\"\n   },\n   {\n    \"label\": \"Moldava\",\n    \"value\": \"144\"\n   },\n   {\n    \"label\": \"Monaco\",\n    \"value\": \"145\"\n   },\n   {\n    \"label\": \"Mongolia\",\n    \"value\": \"146\"\n   },\n   {\n    \"label\": \"Montenegro\",\n    \"value\": \"147\"\n   },\n   {\n    \"label\": \"Montserrat\",\n    \"value\": \"148\"\n   },\n   {\n    \"label\": \"Morocco\",\n    \"value\": \"149\"\n   },\n   {\n    \"label\": \"Mozambique\",\n    \"value\": \"150\"\n   },\n   {\n    \"label\": \"Myanmar (Burma)\",\n    \"value\": \"151\"\n   },\n   {\n    \"label\": \"Namibia\",\n    \"value\": \"152\"\n   },\n   {\n    \"label\": \"Nauru\",\n    \"value\": \"153\"\n   },\n   {\n    \"label\": \"Nepal\",\n    \"value\": \"154\"\n   },\n   {\n    \"label\": \"Netherlands\",\n    \"value\": \"155\"\n   },\n   {\n    \"label\": \"New Caledonia\",\n    \"value\": \"156\"\n   },\n   {\n    \"label\": \"New Zealand\",\n    \"value\": \"157\"\n   },\n   {\n    \"label\": \"Nicaragua\",\n    \"value\": \"158\"\n   },\n   {\n    \"label\": \"Niger\",\n    \"value\": \"159\"\n   },\n   {\n    \"label\": \"Nigeria\",\n    \"value\": \"160\"\n   },\n   {\n    \"label\": \"Niue\",\n    \"value\": \"161\"\n   },\n   {\n    \"label\": \"Norfolk Island\",\n    \"value\": \"162\"\n   },\n   {\n    \"label\": \"North Korea\",\n    \"value\": \"163\"\n   },\n   {\n    \"label\": \"Northern Mariana Islands\",\n    \"value\": \"164\"\n   },\n   {\n    \"label\": \"Norway\",\n    \"value\": \"165\"\n   },\n   {\n    \"label\": \"Oman\",\n    \"value\": \"166\"\n   },\n   {\n    \"label\": \"Pakistan\",\n    \"value\": \"167\"\n   },\n   {\n    \"label\": \"Palau\",\n    \"value\": \"168\"\n   },\n   {\n    \"label\": \"Palestine\",\n    \"value\": \"169\"\n   },\n   {\n    \"label\": \"Panama\",\n    \"value\": \"170\"\n   },\n   {\n    \"label\": \"Papua New Guinea\",\n    \"value\": \"171\"\n   },\n   {\n    \"label\": \"Paraguay\",\n    \"value\": \"172\"\n   },\n   {\n    \"label\": \"Peru\",\n    \"value\": \"173\"\n   },\n   {\n    \"label\": \"Phillipines\",\n    \"value\": \"174\"\n   },\n   {\n    \"label\": \"Pitcairn\",\n    \"value\": \"175\"\n   },\n   {\n    \"label\": \"Poland\",\n    \"value\": \"176\"\n   },\n   {\n    \"label\": \"Portugal\",\n    \"value\": \"177\"\n   },\n   {\n    \"label\": \"Puerto Rico\",\n    \"value\": \"178\"\n   },\n   {\n    \"label\": \"Qatar\",\n    \"value\": \"179\"\n   },\n   {\n    \"label\": \"Reunion\",\n    \"value\": \"180\"\n   },\n   {\n    \"label\": \"Romania\",\n    \"value\": \"181\"\n   },\n   {\n    \"label\": \"Russia\",\n    \"value\": \"182\"\n   },\n   {\n    \"label\": \"Rwanda\",\n    \"value\": \"183\"\n   },\n   {\n    \"label\": \"Saint Barthelemy\",\n    \"value\": \"184\"\n   },\n   {\n    \"label\": \"Saint Helena\",\n    \"value\": \"185\"\n   },\n   {\n    \"label\": \"Saint Kitts and Nevis\",\n    \"value\": \"186\"\n   },\n   {\n    \"label\": \"Saint Lucia\",\n    \"value\": \"187\"\n   },\n   {\n    \"label\": \"Saint Martin\",\n    \"value\": \"188\"\n   },\n   {\n    \"label\": \"Saint Pierre and Miquelon\",\n    \"value\": \"189\"\n   },\n   {\n    \"label\": \"Saint Vincent and the Grenadines\",\n    \"value\": \"190\"\n   },\n   {\n    \"label\": \"Samoa\",\n    \"value\": \"191\"\n   },\n   {\n    \"label\": \"San Marino\",\n    \"value\": \"192\"\n   },\n   {\n    \"label\": \"Sao Tome and Principe\",\n    \"value\": \"193\"\n   },\n   {\n    \"label\": \"Saudi Arabia\",\n    \"value\": \"194\"\n   },\n   {\n    \"label\": \"Senegal\",\n    \"value\": \"195\"\n   },\n   {\n    \"label\": \"Serbia\",\n    \"value\": \"196\"\n   },\n   {\n    \"label\": \"Seychelles\",\n    \"value\": \"197\"\n   },\n   {\n    \"label\": \"Sierra Leone\",\n    \"value\": \"198\"\n   },\n   {\n    \"label\": \"Singapore\",\n    \"value\": \"199\"\n   },\n   {\n    \"label\": \"Sint Maarten\",\n    \"value\": \"200\"\n   },\n   {\n    \"label\": \"Slovakia\",\n    \"value\": \"201\"\n   },\n   {\n    \"label\": \"Slovenia\",\n    \"value\": \"202\"\n   },\n   {\n    \"label\": \"Solomon Islands\",\n    \"value\": \"203\"\n   },\n   {\n    \"label\": \"Somalia\",\n    \"value\": \"204\"\n   },\n   {\n    \"label\": \"South Africa\",\n    \"value\": \"205\"\n   },\n   {\n    \"label\": \"South Georgia and the South Sandwich Islands\",\n    \"value\": \"206\"\n   },\n   {\n    \"label\": \"South Korea\",\n    \"value\": \"207\"\n   },\n   {\n    \"label\": \"South Sudan\",\n    \"value\": \"208\"\n   },\n   {\n    \"label\": \"Spain\",\n    \"value\": \"209\"\n   },\n   {\n    \"label\": \"Sri Lanka\",\n    \"value\": \"210\"\n   },\n   {\n    \"label\": \"Sudan\",\n    \"value\": \"211\"\n   },\n   {\n    \"label\": \"Suriname\",\n    \"value\": \"212\"\n   },\n   {\n    \"label\": \"Svalbard and Jan Mayen\",\n    \"value\": \"213\"\n   },\n   {\n    \"label\": \"Swaziland\",\n    \"value\": \"214\"\n   },\n   {\n    \"label\": \"Sweden\",\n    \"value\": \"215\"\n   },\n   {\n    \"label\": \"Switzerland\",\n    \"value\": \"216\"\n   },\n   {\n    \"label\": \"Syria\",\n    \"value\": \"217\"\n   },\n   {\n    \"label\": \"Taiwan\",\n    \"value\": \"218\"\n   },\n   {\n    \"label\": \"Tajikistan\",\n    \"value\": \"219\"\n   },\n   {\n    \"label\": \"Tanzania\",\n    \"value\": \"220\"\n   },\n   {\n    \"label\": \"Thailand\",\n    \"value\": \"221\"\n   },\n   {\n    \"label\": \"Timor-Leste (East Timor)\",\n    \"value\": \"222\"\n   },\n   {\n    \"label\": \"Togo\",\n    \"value\": \"223\"\n   },\n   {\n    \"label\": \"Tokelau\",\n    \"value\": \"224\"\n   },\n   {\n    \"label\": \"Tonga\",\n    \"value\": \"225\"\n   },\n   {\n    \"label\": \"Trinidad and Tobago\",\n    \"value\": \"226\"\n   },\n   {\n    \"label\": \"Tunisia\",\n    \"value\": \"227\"\n   },\n   {\n    \"label\": \"Turkey\",\n    \"value\": \"228\"\n   },\n   {\n    \"label\": \"Turkmenistan\",\n    \"value\": \"229\"\n   },\n   {\n    \"label\": \"Turks and Caicos Islands\",\n    \"value\": \"230\"\n   },\n   {\n    \"label\": \"Tuvalu\",\n    \"value\": \"231\"\n   },\n   {\n    \"label\": \"Uganda\",\n    \"value\": \"232\"\n   },\n   {\n    \"label\": \"Ukraine\",\n    \"value\": \"233\"\n   },\n   {\n    \"label\": \"United Arab Emirates\",\n    \"value\": \"234\"\n   },\n   {\n    \"label\": \"United Kingdom\",\n    \"value\": \"235\"\n   },\n   {\n    \"label\": \"United States\",\n    \"value\": \"236\"\n   },\n   {\n    \"label\": \"United States Minor Outlying Islands\",\n    \"value\": \"237\"\n   },\n   {\n    \"label\": \"Uruguay\",\n    \"value\": \"238\"\n   },\n   {\n    \"label\": \"Uzbekistan\",\n    \"value\": \"239\"\n   },\n   {\n    \"label\": \"Vanuatu\",\n    \"value\": \"240\"\n   },\n   {\n    \"label\": \"Vatican City\",\n    \"value\": \"241\"\n   },\n   {\n    \"label\": \"Venezuela\",\n    \"value\": \"242\"\n   },\n   {\n    \"label\": \"Vietnam\",\n    \"value\": \"243\"\n   },\n   {\n    \"label\": \"Virgin Islands, British\",\n    \"value\": \"244\"\n   },\n   {\n    \"label\": \"Virgin Islands, US\",\n    \"value\": \"245\"\n   },\n   {\n    \"label\": \"Wallis and Futuna\",\n    \"value\": \"246\"\n   },\n   {\n    \"label\": \"Western Sahara\",\n    \"value\": \"247\"\n   },\n   {\n    \"label\": \"Yemen\",\n    \"value\": \"248\"\n   },\n   {\n    \"label\": \"Zambia\",\n    \"value\": \"249\"\n   },\n   {\n    \"label\": \"Zimbabwe\",\n    \"value\": \"250\"\n   }\n  ]\n },\n {\n  \"type\": \"text\",\n  \"label\": \"Zip Code\",\n  \"subtype\": \"text\",\n  \"className\": \"form-control\",\n  \"name\": \"zip\"\n },\n {\n  \"type\": \"textarea\",\n  \"label\": \"Description\",\n  \"className\": \"form-control\",\n  \"name\": \"description\"\n },\n {\n  \"type\": \"text\",\n  \"label\": \"Website\",\n  \"subtype\": \"text\",\n  \"className\": \"form-control\",\n  \"name\": \"website\"\n }\n]";

					<?php } else { ?>
						var formData = <?php echo $value['form_json']; ?>;
					<?php } ?>
					//console.log(formData);
				</script>

				<script>
					var fbOptions = {
						dataType: 'json'
					};

					if (formData && formData.length) {
						fbOptions.formData = formData;
					}

					//fbOptions.disableFields = ['autocomplete', 'button', 'checkbox', 'checkbox-group', 'date', 'hidden', 'number', 'radio-group', 'select', 'text', 'textarea'];

					fbOptions.disableFields = ['autocomplete', 'button', 'checkbox', 'checkbox-group', 'date', 'hidden', 'number'];

					fbOptions.controlPosition = 'left';
					fbOptions.controlOrder = [
						'header',
						'paragraph',
						'file',
					];

					fbOptions.inputSets = [];

					var db_fields = [{
						"label": "Name",
						"name": "name",
						"fields": [{
							"type": "text",
							"label": "Name",
							"className": "form-control",
							"name": "name",
							"required": true
						}]
					}, {
						"label": "Position",
						"name": "title",
						"fields": [{
							"type": "text",
							"label": "Position",
							"className": "form-control",
							"name": "title"
						}]
					}, {
						"label": "Email Address",
						"name": "email",
						"fields": [{
							"type": "text",
							"label": "Email Address",
							"className": "form-control",
							"name": "email"
						}]
					}, {
						"label": "Phone",
						"name": "phonenumber",
						"fields": [{
							"type": "text",
							"label": "Phone",
							"className": "form-control",
							"name": "phonenumber"
						}]
					}, {
						"label": "Company",
						"name": "company",
						"fields": [{
							"type": "text",
							"label": "Company",
							"className": "form-control",
							"name": "company"
						}]
					}, {
						"label": "Address",
						"name": "address",
						"fields": [{
							"type": "textarea",
							"label": "Address",
							"className": "form-control",
							"name": "address"
						}]
					}, {
						"label": "City",
						"name": "city",
						"fields": [{
							"type": "text",
							"label": "City",
							"className": "form-control",
							"name": "city"
						}]
					}, {
						"label": "State",
						"name": "state",
						"fields": [{
							"type": "text",
							"label": "State",
							"className": "form-control",
							"name": "state"
						}]
					}, {
						"label": "Country",
						"name": "country",
						"fields": [{
							"type": "select",
							"label": "Country",
							"multiple": "true",
							"className": "form-control",
							"name": "country",
							"values": [{
								"label": "Afghanistan",
								"value": 1,
								"selected": false
							}, {
								"label": "Aland Islands",
								"value": 2,
								"selected": false
							}, {
								"label": "Albania",
								"value": 3,
								"selected": false
							}, {
								"label": "Algeria",
								"value": 4,
								"selected": false
							}, {
								"label": "American Samoa",
								"value": 5,
								"selected": false
							}, {
								"label": "Andorra",
								"value": 6,
								"selected": false
							}, {
								"label": "Angola",
								"value": 7,
								"selected": false
							}, {
								"label": "Anguilla",
								"value": 8,
								"selected": false
							}, {
								"label": "Antarctica",
								"value": 9,
								"selected": false
							}, {
								"label": "Antigua and Barbuda",
								"value": 10,
								"selected": false
							}, {
								"label": "Argentina",
								"value": 11,
								"selected": false
							}, {
								"label": "Armenia",
								"value": 12,
								"selected": false
							}, {
								"label": "Aruba",
								"value": 13,
								"selected": false
							}, {
								"label": "Australia",
								"value": 14,
								"selected": false
							}, {
								"label": "Austria",
								"value": 15,
								"selected": false
							}, {
								"label": "Azerbaijan",
								"value": 16,
								"selected": false
							}, {
								"label": "Bahamas",
								"value": 17,
								"selected": false
							}, {
								"label": "Bahrain",
								"value": 18,
								"selected": false
							}, {
								"label": "Bangladesh",
								"value": 19,
								"selected": false
							}, {
								"label": "Barbados",
								"value": 20,
								"selected": false
							}, {
								"label": "Belarus",
								"value": 21,
								"selected": false
							}, {
								"label": "Belgium",
								"value": 22,
								"selected": false
							}, {
								"label": "Belize",
								"value": 23,
								"selected": false
							}, {
								"label": "Benin",
								"value": 24,
								"selected": false
							}, {
								"label": "Bermuda",
								"value": 25,
								"selected": false
							}, {
								"label": "Bhutan",
								"value": 26,
								"selected": false
							}, {
								"label": "Bolivia",
								"value": 27,
								"selected": false
							}, {
								"label": "Bonaire, Sint Eustatius and Saba",
								"value": 28,
								"selected": false
							}, {
								"label": "Bosnia and Herzegovina",
								"value": 29,
								"selected": false
							}, {
								"label": "Botswana",
								"value": 30,
								"selected": false
							}, {
								"label": "Bouvet Island",
								"value": 31,
								"selected": false
							}, {
								"label": "Brazil",
								"value": 32,
								"selected": false
							}, {
								"label": "British Indian Ocean Territory",
								"value": 33,
								"selected": false
							}, {
								"label": "Brunei",
								"value": 34,
								"selected": false
							}, {
								"label": "Bulgaria",
								"value": 35,
								"selected": false
							}, {
								"label": "Burkina Faso",
								"value": 36,
								"selected": false
							}, {
								"label": "Burundi",
								"value": 37,
								"selected": false
							}, {
								"label": "Cambodia",
								"value": 38,
								"selected": false
							}, {
								"label": "Cameroon",
								"value": 39,
								"selected": false
							}, {
								"label": "Canada",
								"value": 40,
								"selected": false
							}, {
								"label": "Cape Verde",
								"value": 41,
								"selected": false
							}, {
								"label": "Cayman Islands",
								"value": 42,
								"selected": false
							}, {
								"label": "Central African Republic",
								"value": 43,
								"selected": false
							}, {
								"label": "Chad",
								"value": 44,
								"selected": false
							}, {
								"label": "Chile",
								"value": 45,
								"selected": false
							}, {
								"label": "China",
								"value": 46,
								"selected": false
							}, {
								"label": "Christmas Island",
								"value": 47,
								"selected": false
							}, {
								"label": "Cocos (Keeling) Islands",
								"value": 48,
								"selected": false
							}, {
								"label": "Colombia",
								"value": 49,
								"selected": false
							}, {
								"label": "Comoros",
								"value": 50,
								"selected": false
							}, {
								"label": "Congo",
								"value": 51,
								"selected": false
							}, {
								"label": "Cook Islands",
								"value": 52,
								"selected": false
							}, {
								"label": "Costa Rica",
								"value": 53,
								"selected": false
							}, {
								"label": "Cote d`ivoire (Ivory Coast)",
								"value": 54,
								"selected": false
							}, {
								"label": "Croatia",
								"value": 55,
								"selected": false
							}, {
								"label": "Cuba",
								"value": 56,
								"selected": false
							}, {
								"label": "Curacao",
								"value": 57,
								"selected": false
							}, {
								"label": "Cyprus",
								"value": 58,
								"selected": false
							}, {
								"label": "Czech Republic",
								"value": 59,
								"selected": false
							}, {
								"label": "Democratic Republic of the Congo",
								"value": 60,
								"selected": false
							}, {
								"label": "Denmark",
								"value": 61,
								"selected": false
							}, {
								"label": "Djibouti",
								"value": 62,
								"selected": false
							}, {
								"label": "Dominica",
								"value": 63,
								"selected": false
							}, {
								"label": "Dominican Republic",
								"value": 64,
								"selected": false
							}, {
								"label": "Ecuador",
								"value": 65,
								"selected": false
							}, {
								"label": "Egypt",
								"value": 66,
								"selected": false
							}, {
								"label": "El Salvador",
								"value": 67,
								"selected": false
							}, {
								"label": "Equatorial Guinea",
								"value": 68,
								"selected": false
							}, {
								"label": "Eritrea",
								"value": 69,
								"selected": false
							}, {
								"label": "Estonia",
								"value": 70,
								"selected": false
							}, {
								"label": "Ethiopia",
								"value": 71,
								"selected": false
							}, {
								"label": "Falkland Islands (Malvinas)",
								"value": 72,
								"selected": false
							}, {
								"label": "Faroe Islands",
								"value": 73,
								"selected": false
							}, {
								"label": "Fiji",
								"value": 74,
								"selected": false
							}, {
								"label": "Finland",
								"value": 75,
								"selected": false
							}, {
								"label": "France",
								"value": 76,
								"selected": false
							}, {
								"label": "French Guiana",
								"value": 77,
								"selected": false
							}, {
								"label": "French Polynesia",
								"value": 78,
								"selected": false
							}, {
								"label": "French Southern Territories",
								"value": 79,
								"selected": false
							}, {
								"label": "Gabon",
								"value": 80,
								"selected": false
							}, {
								"label": "Gambia",
								"value": 81,
								"selected": false
							}, {
								"label": "Georgia",
								"value": 82,
								"selected": false
							}, {
								"label": "Germany",
								"value": 83,
								"selected": false
							}, {
								"label": "Ghana",
								"value": 84,
								"selected": false
							}, {
								"label": "Gibraltar",
								"value": 85,
								"selected": false
							}, {
								"label": "Greece",
								"value": 86,
								"selected": false
							}, {
								"label": "Greenland",
								"value": 87,
								"selected": false
							}, {
								"label": "Grenada",
								"value": 88,
								"selected": false
							}, {
								"label": "Guadaloupe",
								"value": 89,
								"selected": false
							}, {
								"label": "Guam",
								"value": 90,
								"selected": false
							}, {
								"label": "Guatemala",
								"value": 91,
								"selected": false
							}, {
								"label": "Guernsey",
								"value": 92,
								"selected": false
							}, {
								"label": "Guinea",
								"value": 93,
								"selected": false
							}, {
								"label": "Guinea-Bissau",
								"value": 94,
								"selected": false
							}, {
								"label": "Guyana",
								"value": 95,
								"selected": false
							}, {
								"label": "Haiti",
								"value": 96,
								"selected": false
							}, {
								"label": "Heard Island and McDonald Islands",
								"value": 97,
								"selected": false
							}, {
								"label": "Honduras",
								"value": 98,
								"selected": false
							}, {
								"label": "Hong Kong",
								"value": 99,
								"selected": false
							}, {
								"label": "Hungary",
								"value": 100,
								"selected": false
							}, {
								"label": "Iceland",
								"value": 101,
								"selected": false
							}, {
								"label": "India",
								"value": 102,
								"selected": false
							}, {
								"label": "Indonesia",
								"value": 103,
								"selected": false
							}, {
								"label": "Iran",
								"value": 104,
								"selected": false
							}, {
								"label": "Iraq",
								"value": 105,
								"selected": false
							}, {
								"label": "Ireland",
								"value": 106,
								"selected": false
							}, {
								"label": "Isle of Man",
								"value": 107,
								"selected": false
							}, {
								"label": "Israel",
								"value": 108,
								"selected": false
							}, {
								"label": "Italy",
								"value": 109,
								"selected": false
							}, {
								"label": "Jamaica",
								"value": 110,
								"selected": false
							}, {
								"label": "Japan",
								"value": 111,
								"selected": false
							}, {
								"label": "Jersey",
								"value": 112,
								"selected": false
							}, {
								"label": "Jordan",
								"value": 113,
								"selected": false
							}, {
								"label": "Kazakhstan",
								"value": 114,
								"selected": false
							}, {
								"label": "Kenya",
								"value": 115,
								"selected": false
							}, {
								"label": "Kiribati",
								"value": 116,
								"selected": false
							}, {
								"label": "Kosovo",
								"value": 117,
								"selected": false
							}, {
								"label": "Kuwait",
								"value": 118,
								"selected": false
							}, {
								"label": "Kyrgyzstan",
								"value": 119,
								"selected": false
							}, {
								"label": "Laos",
								"value": 120,
								"selected": false
							}, {
								"label": "Latvia",
								"value": 121,
								"selected": false
							}, {
								"label": "Lebanon",
								"value": 122,
								"selected": false
							}, {
								"label": "Lesotho",
								"value": 123,
								"selected": false
							}, {
								"label": "Liberia",
								"value": 124,
								"selected": false
							}, {
								"label": "Libya",
								"value": 125,
								"selected": false
							}, {
								"label": "Liechtenstein",
								"value": 126,
								"selected": false
							}, {
								"label": "Lithuania",
								"value": 127,
								"selected": false
							}, {
								"label": "Luxembourg",
								"value": 128,
								"selected": false
							}, {
								"label": "Macao",
								"value": 129,
								"selected": false
							}, {
								"label": "Macedonia",
								"value": 130,
								"selected": false
							}, {
								"label": "Madagascar",
								"value": 131,
								"selected": false
							}, {
								"label": "Malawi",
								"value": 132,
								"selected": false
							}, {
								"label": "Malaysia",
								"value": 133,
								"selected": false
							}, {
								"label": "Maldives",
								"value": 134,
								"selected": false
							}, {
								"label": "Mali",
								"value": 135,
								"selected": false
							}, {
								"label": "Malta",
								"value": 136,
								"selected": false
							}, {
								"label": "Marshall Islands",
								"value": 137,
								"selected": false
							}, {
								"label": "Martinique",
								"value": 138,
								"selected": false
							}, {
								"label": "Mauritania",
								"value": 139,
								"selected": false
							}, {
								"label": "Mauritius",
								"value": 140,
								"selected": false
							}, {
								"label": "Mayotte",
								"value": 141,
								"selected": false
							}, {
								"label": "Mexico",
								"value": 142,
								"selected": false
							}, {
								"label": "Micronesia",
								"value": 143,
								"selected": false
							}, {
								"label": "Moldava",
								"value": 144,
								"selected": false
							}, {
								"label": "Monaco",
								"value": 145,
								"selected": false
							}, {
								"label": "Mongolia",
								"value": 146,
								"selected": false
							}, {
								"label": "Montenegro",
								"value": 147,
								"selected": false
							}, {
								"label": "Montserrat",
								"value": 148,
								"selected": false
							}, {
								"label": "Morocco",
								"value": 149,
								"selected": false
							}, {
								"label": "Mozambique",
								"value": 150,
								"selected": false
							}, {
								"label": "Myanmar (Burma)",
								"value": 151,
								"selected": false
							}, {
								"label": "Namibia",
								"value": 152,
								"selected": false
							}, {
								"label": "Nauru",
								"value": 153,
								"selected": false
							}, {
								"label": "Nepal",
								"value": 154,
								"selected": false
							}, {
								"label": "Netherlands",
								"value": 155,
								"selected": false
							}, {
								"label": "New Caledonia",
								"value": 156,
								"selected": false
							}, {
								"label": "New Zealand",
								"value": 157,
								"selected": false
							}, {
								"label": "Nicaragua",
								"value": 158,
								"selected": false
							}, {
								"label": "Niger",
								"value": 159,
								"selected": false
							}, {
								"label": "Nigeria",
								"value": 160,
								"selected": false
							}, {
								"label": "Niue",
								"value": 161,
								"selected": false
							}, {
								"label": "Norfolk Island",
								"value": 162,
								"selected": false
							}, {
								"label": "North Korea",
								"value": 163,
								"selected": false
							}, {
								"label": "Northern Mariana Islands",
								"value": 164,
								"selected": false
							}, {
								"label": "Norway",
								"value": 165,
								"selected": false
							}, {
								"label": "Oman",
								"value": 166,
								"selected": false
							}, {
								"label": "Pakistan",
								"value": 167,
								"selected": false
							}, {
								"label": "Palau",
								"value": 168,
								"selected": false
							}, {
								"label": "Palestine",
								"value": 169,
								"selected": false
							}, {
								"label": "Panama",
								"value": 170,
								"selected": false
							}, {
								"label": "Papua New Guinea",
								"value": 171,
								"selected": false
							}, {
								"label": "Paraguay",
								"value": 172,
								"selected": false
							}, {
								"label": "Peru",
								"value": 173,
								"selected": false
							}, {
								"label": "Phillipines",
								"value": 174,
								"selected": false
							}, {
								"label": "Pitcairn",
								"value": 175,
								"selected": false
							}, {
								"label": "Poland",
								"value": 176,
								"selected": false
							}, {
								"label": "Portugal",
								"value": 177,
								"selected": false
							}, {
								"label": "Puerto Rico",
								"value": 178,
								"selected": false
							}, {
								"label": "Qatar",
								"value": 179,
								"selected": false
							}, {
								"label": "Reunion",
								"value": 180,
								"selected": false
							}, {
								"label": "Romania",
								"value": 181,
								"selected": false
							}, {
								"label": "Russia",
								"value": 182,
								"selected": false
							}, {
								"label": "Rwanda",
								"value": 183,
								"selected": false
							}, {
								"label": "Saint Barthelemy",
								"value": 184,
								"selected": false
							}, {
								"label": "Saint Helena",
								"value": 185,
								"selected": false
							}, {
								"label": "Saint Kitts and Nevis",
								"value": 186,
								"selected": false
							}, {
								"label": "Saint Lucia",
								"value": 187,
								"selected": false
							}, {
								"label": "Saint Martin",
								"value": 188,
								"selected": false
							}, {
								"label": "Saint Pierre and Miquelon",
								"value": 189,
								"selected": false
							}, {
								"label": "Saint Vincent and the Grenadines",
								"value": 190,
								"selected": false
							}, {
								"label": "Samoa",
								"value": 191,
								"selected": false
							}, {
								"label": "San Marino",
								"value": 192,
								"selected": false
							}, {
								"label": "Sao Tome and Principe",
								"value": 193,
								"selected": false
							}, {
								"label": "Saudi Arabia",
								"value": 194,
								"selected": false
							}, {
								"label": "Senegal",
								"value": 195,
								"selected": false
							}, {
								"label": "Serbia",
								"value": 196,
								"selected": false
							}, {
								"label": "Seychelles",
								"value": 197,
								"selected": false
							}, {
								"label": "Sierra Leone",
								"value": 198,
								"selected": false
							}, {
								"label": "Singapore",
								"value": 199,
								"selected": false
							}, {
								"label": "Sint Maarten",
								"value": 200,
								"selected": false
							}, {
								"label": "Slovakia",
								"value": 201,
								"selected": false
							}, {
								"label": "Slovenia",
								"value": 202,
								"selected": false
							}, {
								"label": "Solomon Islands",
								"value": 203,
								"selected": false
							}, {
								"label": "Somalia",
								"value": 204,
								"selected": false
							}, {
								"label": "South Africa",
								"value": 205,
								"selected": false
							}, {
								"label": "South Georgia and the South Sandwich Islands",
								"value": 206,
								"selected": false
							}, {
								"label": "South Korea",
								"value": 207,
								"selected": false
							}, {
								"label": "South Sudan",
								"value": 208,
								"selected": false
							}, {
								"label": "Spain",
								"value": 209,
								"selected": false
							}, {
								"label": "Sri Lanka",
								"value": 210,
								"selected": false
							}, {
								"label": "Sudan",
								"value": 211,
								"selected": false
							}, {
								"label": "Suriname",
								"value": 212,
								"selected": false
							}, {
								"label": "Svalbard and Jan Mayen",
								"value": 213,
								"selected": false
							}, {
								"label": "Swaziland",
								"value": 214,
								"selected": false
							}, {
								"label": "Sweden",
								"value": 215,
								"selected": false
							}, {
								"label": "Switzerland",
								"value": 216,
								"selected": false
							}, {
								"label": "Syria",
								"value": 217,
								"selected": false
							}, {
								"label": "Taiwan",
								"value": 218,
								"selected": false
							}, {
								"label": "Tajikistan",
								"value": 219,
								"selected": false
							}, {
								"label": "Tanzania",
								"value": 220,
								"selected": false
							}, {
								"label": "Thailand",
								"value": 221,
								"selected": false
							}, {
								"label": "Timor-Leste (East Timor)",
								"value": 222,
								"selected": false
							}, {
								"label": "Togo",
								"value": 223,
								"selected": false
							}, {
								"label": "Tokelau",
								"value": 224,
								"selected": false
							}, {
								"label": "Tonga",
								"value": 225,
								"selected": false
							}, {
								"label": "Trinidad and Tobago",
								"value": 226,
								"selected": false
							}, {
								"label": "Tunisia",
								"value": 227,
								"selected": false
							}, {
								"label": "Turkey",
								"value": 228,
								"selected": false
							}, {
								"label": "Turkmenistan",
								"value": 229,
								"selected": false
							}, {
								"label": "Turks and Caicos Islands",
								"value": 230,
								"selected": false
							}, {
								"label": "Tuvalu",
								"value": 231,
								"selected": false
							}, {
								"label": "Uganda",
								"value": 232,
								"selected": false
							}, {
								"label": "Ukraine",
								"value": 233,
								"selected": false
							}, {
								"label": "United Arab Emirates",
								"value": 234,
								"selected": false
							}, {
								"label": "United Kingdom",
								"value": 235,
								"selected": false
							}, {
								"label": "United States",
								"value": 236,
								"selected": false
							}, {
								"label": "United States Minor Outlying Islands",
								"value": 237,
								"selected": false
							}, {
								"label": "Uruguay",
								"value": 238,
								"selected": false
							}, {
								"label": "Uzbekistan",
								"value": 239,
								"selected": false
							}, {
								"label": "Vanuatu",
								"value": 240,
								"selected": false
							}, {
								"label": "Vatican City",
								"value": 241,
								"selected": false
							}, {
								"label": "Venezuela",
								"value": 242,
								"selected": false
							}, {
								"label": "Vietnam",
								"value": 243,
								"selected": false
							}, {
								"label": "Virgin Islands, British",
								"value": 244,
								"selected": false
							}, {
								"label": "Virgin Islands, US",
								"value": 245,
								"selected": false
							}, {
								"label": "Wallis and Futuna",
								"value": 246,
								"selected": false
							}, {
								"label": "Western Sahara",
								"value": 247,
								"selected": false
							}, {
								"label": "Yemen",
								"value": 248,
								"selected": false
							}, {
								"label": "Zambia",
								"value": 249,
								"selected": false
							}, {
								"label": "Zimbabwe",
								"value": 250,
								"selected": false
							}]
						}]
					}, {
						"label": "Zip Code",
						"name": "zip",
						"fields": [{
							"type": "text",
							"label": "Zip Code",
							"className": "form-control",
							"name": "zip"
						}]
					}, {
						"label": "Description",
						"name": "description",
						"fields": [{
							"type": "textarea",
							"label": "Description",
							"className": "form-control",
							"name": "description"
						}]
					}, {
						"label": "Website",
						"name": "website",
						"fields": [{
							"type": "text",
							"label": "Website",
							"className": "form-control",
							"name": "website"
						}]
					}];
					var cfields = [];
					$.each(db_fields, function(i, f) {
						fbOptions.inputSets.push(f);
					});
					if (cfields && cfields.length) {
						$.each(cfields, function(i, f) {
							fbOptions.inputSets.push(f);
						});
					}
					fbOptions.typeUserEvents = {
						'text': {
							onadd: function(fId) {
								do_form_field_restrictions(fId, 'input');
								//hide close button for if make primary it nessary fro contect details
								var name = $(fId).find('.name-wrap input').val();
								if (name == "company" || name == "email" || name == "name") {
									$(fId).find(".field-actions .delete-confirm").hide();
								}
								//
							},
						},
						'email': {
							onadd: function(fId) {
								do_form_field_restrictions(fId, 'input');
							},
						},
						'color': {
							onadd: function(fId) {
								do_form_field_restrictions(fId, 'input');
							},
						},
						'date': {
							onadd: function(fId) {
								do_form_field_restrictions(fId, 'input');
							},
						},
						'datetime': {
							onadd: function(fId) {
								do_form_field_restrictions(fId, 'datetime');
							},
						},
						'select': {
							onadd: function(fId) {
								do_form_field_restrictions(fId, 'select');
							},
						},
						'file': {
							onadd: function(fId) {
								do_form_field_restrictions(fId, 'file');
								//alert($(fId).attr("class")+"dddd"+$(fId).attr("id"));
								// add class fro design porpose
								$(fId).find(".prev-holder div").addClass("custom_upload");

								// set file upload field name to be always file-input

								$(fId).find('.name-wrap .input-wrap input').val('file-input');
							},
						},
						'textarea': {
							onadd: function(fId) {
								do_form_field_restrictions(fId, 'textarea');
							},
						},
						'checkbox-group': {
							onadd: function(fId) {
								do_form_field_restrictions(fId, 'checkbox-group');
							},
						},
						'radio-group': {
							onadd: function(fId) {
								do_form_field_restrictions(fId, 'radio-group');
							},
						},
					}

					/*var rule=JSON.parse('{"valdtype": {"label" : "validation","multiple" : "true","class":"validation-rule","onchange":"mychang(this)","options": {"required:true": "Required","alphanumeric:true":"Alpha Numeric","lettersonly:true":"Letter","email:true": "Email validation","digits:true": "Digits","number:true":"Numbers Only","nowhitespace:true":"No White space","dateITA:true":"Date validation d/m/y"}},"vald_rules": {"label": "vld_hidden"}}');*/
					var rule = JSON.parse('{"valdtype": {"label" : "validation","multiple" : "true","class":"validation-rule","options": {"required": "Required","alphanumeric":"Alpha Numeric","lettersonly":"Letter","email": "Email validation","digits": "Digits","number":"Numbers Only","nowhitespace":"No White space","dateITA":"Date validation d/m/y"}},"vald_rules": {"label": "vld_hidden"}}');
					//var rule=JSON.parse('{"valdtype" : {"label" : "validation"}}');
					//console.log(rule);
					fbOptions.typeUserAttrs = {
						text: rule,
						email: rule,
						textarea: rule
					};

					function mychang(obj) {
						alert("test inside");
						var rule = $(obj).parent().parent().parent().find(".fld-vald_rules");
						rule.attr("value", $(obj).val().join());
					}

					$(function() {
						$('body').on('click', '.del-button', function() {
							var _field = $(this).parents('li.form-field');
							var _preview_name;
							var s = $('.cb-wrap .ui-sortable');
							if (_field.find('.prev-holder input').length > 0) {
								_preview_name = _field.find('.prev-holder input').attr('name');
							} else if (_field.find('.prev-holder datetime').length > 0) {
								_preview_name = _field.find('.prev-holder datetime').attr('name');
							} else if (_field.find('.prev-holder textarea').length > 0) {
								_preview_name = _field.find('.prev-holder textarea').attr('name');
							} else if (_field.find('.prev-holder select').length > 0) {
								_preview_name = _field.find('.prev-holder select').attr('name');
							}

							var pos = _preview_name.lastIndexOf('-');
							_preview_name = _preview_name.substr(0, pos);
							if (_preview_name != 'file-input') {
								$('li[type="' + _preview_name + '"]').removeClass('disabled')
							} else {
								setTimeout(function() {
									s.find('li').eq(2).removeClass('disabled');
								}, 50);
							}
							setTimeout(function() {
								s.sortable({
									cancel: '.disabled'
								});
								s.sortable('refresh');
							}, 80);
						});

						$('body').on('blur', '.form-field:not([type="header"],[type="paragraph"],[type="checkbox-group"]) input[name="className"]',
							function() {
								var className = $(this).val();
								if (className.indexOf('form-control') == -1) {
									className = className.trim();
									className += ' form-control';
									className = className.trim();
									$(this).val(className);
								}
							});

						$('body').on('focus', '.name-wrap input', function() {
							$(this).blur();
						});

					})
					//  var radio_count=0;
					function do_form_field_restrictions(fId, type) {
						var _field = $(fId);

						var _preview_name;
						var s = $('.cb-wrap .ui-sortable');
						$('.access-wrap').html('');
						$('.multiple-wrap').html('');

						if (type == 'checkbox-group') {
							_preview_name = _field.find('input[type="checkbox"]').eq(0).attr('name');
						} else if (type == 'radio-group') {
							_preview_name = _field.find('input[type="radio"]').eq(0).attr('name');

							// radio_count=radio_count+1;
							//	radio_count++;
							//var new_length=_field.find('.access-wrap').length+1;
							//s.find('access-wrap').eq(2).addClass('disabled');
							$('.access-wrap').addClass('disabled');

							// $(this).find('.radio-group-field').find('.fld-name').val('radio-group_'+radio_count);

						} else if (type == 'file') {
							//  alert(JSON.stringify(_field));

							setTimeout(function() {
								//   s.find('li').eq(2).addClass('disabled');
							}, 50);

						} else if (type == 'datetime') {
							_preview_name = _field.find('.name-wrap input').val();
							setTimeout(function() {
								_field.find('.prev-holder datetime').html('');
							}, 50);
							_field.find('.prev-hold input[type="' + type + '"]').html('');
						} else {
							_preview_name = _field.find(type).attr('name');
						}

						if (type == 'datetime') {
							$('[type="' + _preview_name + '"]:not(.form-field)').addClass('disabled');
						} else if (type != 'file') {
							var pos = _preview_name.lastIndexOf('-');
							_preview_name = _preview_name.substr(0, pos);
							if (_preview_name == 'textbox') {} else if (_preview_name == 'textarea') {} else {
								$('[type="' + _preview_name + '"]:not(.form-field)').addClass('disabled');
							}
						}

						$('.frmb-control li[type="' + _preview_name + '"]').removeClass('text-danger');

						if (typeof(mustRequiredFields) != 'undefined' && $.inArray(_preview_name, mustRequiredFields) != -1) {
							_field.find('.required-wrap input[type="checkbox"]').prop('disabled', true);
						}

						setTimeout(function() {
							s.sortable({
								cancel: '.disabled'
							});
							s.sortable('refresh');
						}, 80);
					}
				</script>

				<script>
					$(function() {
						var formBuilder = $(buildWrap).formBuilder(fbOptions).data("formBuilder");

						//console.log(formBuilder);
						$('#allow_duplicate').on('change', function() {
							$('.duplicate-settings-wrapper').toggleClass('hide');
						});

						$('#track_duplicate_field,#track_duplicate_field_and').on('change', function() {

							var selector = ($(this).hasClass('track_duplicate_field') ? 'track_duplicate_field_and' : 'track_duplicate_field')
							$('#' + selector + ' option').removeAttr('disabled', true);
							$('#' + selector + ' option[value="' + $(this).val() + '"]').attr('disabled', true);
							$('#' + selector + '').selectpicker('refresh');
						});

						$('body').on('click', '.form-builder-save', function() {
							//var form_id=$("#template_form_id").val();
							add_form_info(formBuilder);
						});

						/* _validate_form('#form_info',{
						   name:'required',
						   lead_source: 'required',
						   lead_status: 'required',
						   language:'required',
						   success_submit_msg:'required',
						   submit_btn_name:'required',
						   responsible: {
						     required: {
						       depends:function(element){
						         return ($('input[name="notify_type"]:checked').val() == 'assigned') ? true : false
						       }
						     }
						   }
						 });*/
					});

					$(document).ready(function() {
						/** 02-04-2018 rspt **/
						/* $('.pull-left').append('<div class="form-action for_form_ids"><label class="field-label">Form Id</label><span class="required-asterisk" style="display:inline"> *</span><input type="text" class="form-control" name="template_form_id" id="template_form_id" placeholder="Enter Your Form ID" value="<?php echo $value['form_id']; ?>"><span style="color:red" id="form_id_error"></span></div>');*/
						/** end 02-04-2018 **/
						// console.log('test');
					});
				</script>

				<script>
					$(document).ready(function() {

						$('.deadline-crm1 li a').click(function() {
							$('.deadline-crm1 li a').removeClass("active");
							$('.deadline-crm1 li').removeClass("active");
							$(this).addClass("active");
						});

						$('.valdtype-wrap .input-wrap select').wrap("<div class='dropdown-sin-2'></div>");

						$(document).find('.space_cls .dropdown-sin-2').dropdown({
							input: '<input type="text" maxLength="20" placeholder="Search">'
						});

						$(document).find(".space_cls select .fld-valdtype").on("change", function() {
							var valid = $(this).val();
							$.each(valid, function(i, val) {
								valid[i] = val + ":true";
							});

							$(this).closest(".form-elements").find("input.fld-vald_rules").attr("value", valid.join());
						});

						/*$(document).find(".valdtype-wrap .dropdown-main ul li").click(function(){
							alert("inside");
							var valid=[];
							$(this).parent().find(".dropdown-chose").each(function(){
								valid.push($(this).attr("data-value"));
							});	
							alert(valid.join());
							$(this).closest(".form-elements").find("input.fld-vald_rules").val(valid.join());
						});*/

					});



					$("input[name='member_notify']").click(function() {
						var radioValue = $("input[name='member_notify']:checked").val();
						//alert(radioValue);
						if (radioValue == "specific_staff") {
							$("#notify_staff_members").css('display', 'block');
							$("#notify_role").css('display', 'none');
						} else if (radioValue == "specific_roles") {
							$("#notify_role").css('display', 'block');
							$("#notify_staff_members").css('display', 'none');
						} else {
							$("#notify_role").css('display', 'none');
							$("#notify_staff_members").css('display', 'none');
						}
					});
					/* formBuilder.promise.then(function(fb) {
					    console.log(fb.formData);
					  });*/

					function add_form_info(formBuilder) { //alert(JSON.parse(formBuilder.formData))
						//console.log(formBuilder.formData);

						var form_name = $("#form_name").val();
						var form_rec_id = $("#form_rec_id").val();
						var source = $("#source").val();
						//alert(source);
						//	var lead_status = $("#lead_status option:selected").val();
						//var lead_status = $("#lead_status").val();
						var assigned = $("#tree_select").val();
						var Assignees = [];
						//alert(assigned);

						if (assigned == '' || form_name == '') {

							if (form_name == '') {
								$('#err_responsible').css('display', 'none');
								$('#err_source').css('display', 'none');

								$(".form_builder").removeClass("active");
								$(".tab-pane").removeClass("active");
								$("#tab_form_build").removeClass("active");
								$("#allusers").addClass("active");
								$('#err_name').css('display', 'block');

								$('#form_name').change(function() {
									$('#err_name').hide();
								});
							}
							if (assigned == '') {
								$('#err_responsible').css('display', 'none');
								$('#err_source').css('display', 'none');

								$(".form_builder").removeClass("active");
								$(".tab-pane").removeClass("active");
								$("#tab_form_build").removeClass("active");
								$("#allusers").addClass("active");
								$('#err_responsible').css('display', 'block');

								$('#err_responsible').change(function() {
									$('#err_responsible').hide();
								});
							}

						} else {
							if (source != '' && assigned != '') {
								$(".LoadingImage").show();
								$('#err_name').css('display', 'none');
								var default_language = $("#default_language option:selected").val();
								//var source = $("#source option:selected").val();
								var source = $("#source").val();
								var lead_status = $("#lead_status option:selected").val();
								//var assigned = $("#assigned option:selected").val();
								var assigned = $("#tree_select").val();
								//alert(assigned);
								// var member_notify = $("input[name='member_notify']:checked").val();
								// if(member_notify=="specific_staff")
								// {
								// 	//var notify_user_id = $("#staff_members option:selected").val();
								// 	var notify_user_id = $("#staff_members").val();
								// 	var notify_role_id ='';
								// }else if(member_notify=="specific_roles")
								// {
								// 	//var notify_role_id = $("#notify_assigned option:selected").val();
								// 	var notify_role_id = $("#notify_assigned").val();
								// 	var notify_user_id ='';
								// }else
								// {
								// 	var notify_user_id = $("#assigned option:selected").val();
								// 	var notify_role_id ='';
								// }
								// alert(notify_user_id);
								// alert(notify_role_id);
								//return false;
								var data = {};
								data['form_name'] = form_name;
								data['default_language'] = default_language;
								data['source'] = source;
								data['lead_status'] = lead_status;
								// data['assigned'] = assigned;
								// data['member_notify'] = member_notify;
								// data['notify_role_id'] = notify_role_id;
								// data['notify_user_id'] = notify_user_id;
								data['form_rec_id'] = form_rec_id;

								$('.comboTreeItemTitle input[type="checkbox"]:checked').each(function() {
									var id = $(this).closest('.comboTreeItemTitle').attr('data-id');
									var id = id.split('_');
									Assignees.push(id[0]);
								});
								data['assign_role'] = Assignees.join(',');
								$.ajax({
									url: '<?php echo base_url(); ?>web_to_lead/add_form_info/',
									type: 'POST',
									data: data,
									success: function(res) {
										//save_form(res);
										var form_id = $("#form_name").val();
										var get_id = res;

										$.post('<?php echo base_url() . "web_to_lead/save_form_data"; ?>', {
											formjson: formBuilder.formData,
											formData: JSON.parse(formBuilder.formData),
											form_id: form_id,
											get_id: get_id
										}).done(function(response) {
											//console.log(response);
											//alert(JSON.stringify(response));
											if (parseInt(response)) {
												$(".LoadingImage").hide();
												$(".popup_info_box  .position-alert1").html("Template Success Fully Created");
												$(".popup_info_box").show();
												window.location.href = '<?php echo base_url() ?>' + 'web_to_lead/web_leads_view/';
											}
										});
									}
								});
							} else {
								if (source == '') {
									$('#err_source').css('display', 'block');
								} else {
									$('#err_source').css('display', 'none');
								}
								if (assigned == '') {

									$('#err_responsible').css('display', 'block');
								} else {
									$('#err_responsible').css('display', 'none');
								}
							}
						}

					}

					/** rspt 25-05-2018 **/
					/** clear all button enable all fields **/
					$(document).on('click', '.yes', function() {
						$('.ui-sortable-handle').each(function() {
							$(this).removeClass('disabled');
						});
						//alert('test');
					});

					/** end of 25-05-2018 **/
				</script>


				<script src="<?php echo base_url(); ?>assets/tree_select/comboTreePlugin.js"></script>
				<script src="<?php echo base_url(); ?>assets/tree_select/icontains.js"></script>

				<script type="text/javascript">
					/** 29-08-2018 **/

					var tree_select = <?php echo json_encode(GetAssignees_SelectBox()); ?>;

					$('.tree_select').comboTree({
						source: tree_select,
						isMultiple: true
					});

					var arr = <?php echo json_encode($assign_data); ?>;

					$('.comboTreeItemTitle').click(function() {
						var id = $(this).attr('data-id');
						var id = id.split('_');

						$('.comboTreeItemTitle').each(function() {
							var id1 = $(this).attr('data-id');
							var id1 = id1.split('_');
							//console.log(id[1]+"=="+id1[1]);
							if (id[1] == id1[1]) {
								$(this).toggleClass('disabled');
							}
						});
						$(this).removeClass('disabled');
					});

					$('.comboTreeItemTitle').each(function() {
						var id1 = $(this).attr('data-id');
						var id = id1.split('_');
						if (jQuery.inArray(id[0], arr) !== -1) {
							$(this).find("input").trigger('click');
						}
					});
				</script>

				<script type="text/javascript">
					$(document).ready(function() {
						$('input[type="file').parent().addClass('custom_upload');
					})

					$('.signed-change3').on('click', function() {
						$('li.nav-item.form_builder a').addClass('active');
						// $(this).addClass('current');
						$('li.nav-item.form_info a').removeClass('active');
						$('.signed-change3').css('display', 'none');
						$('.signed-change3').closest('a').css({
							"min-width": "auto",
							"padding": "0px"
						});
						$('.signed-change2').css('display', 'block');
					});

					$('.signed-change2').on('click', function() {
						$('.signed-change3').css('display', 'block');
						$('.signed-change2').css('display', 'none');
						$('li.nav-item.form_info a').addClass('active');
						// $(this).addClass('current');
						$('li.nav-item.form_builder a').removeClass('active');
					});

					function previous_btn() {
						$('.signed-change3').css('display', 'block');
						$('.signed-change2').css('display', 'none');

					}

					function nxt_btn() {
						$('.signed-change3').css('display', 'none');
						$('.signed-change3').closest('a').css({
							"min-width": "auto",
							"padding": "0px"
						});
						$('.signed-change2').css('display', 'block');
					}
				</script>
				<!-- li.nav-item.form_info a -->

			</div> <!-- tab-content -->
		</div>
	</div>
</div>
</div> <!-- pcoded-content -->

<script>
	$(document).ready(function(){
		$(".clr-check-client").keyup(function(){
			var clr = $(this).val();
			if(clr.length >= 3){
				$(this).addClass("clr-check-client-outline");
			}else{
				$(this).removeClass("clr-check-client-outline");
			}
		});

		$(".clr-check-client").each(function(i){
			if($(this).val() !== ""){
			$(this).addClass("clr-check-client-outline");
			}  
		})

		$(".clr-check-select-cus").change(function(){
			$(this).next().addClass("clr-check-select-cus-wrap");
		})

		$(".clr-check-select-cus").each(function(){
			if($(this).val() !== ""){
				$(this).next().addClass("clr-check-select-cus-wrap");
			}  
		})
	});

</script>

<script type="text/javascript">
	$(document).ready(function() {
		$('input[type="radio"]').parents(".form-group").find(".radio-group").addClass("radio_button01");
	})
</script>