<?php
if(!empty($data)){  
  // echo '<pre>';
  // print_r($data);
  // echo '</pre>';
  // die;
  foreach($data as $val){ ?>
    <div class="analytics-card analytics-card-wrapper">
      <div>
        <!-- <a href="/user/task_list?filter_by=<?php echo strtolower($val['STAFF_NAME']); ?>" target="_blank"> -->
        <a href="/user/task_list?filter_by=<?php echo strtolower($val['STAFF_ID']); ?>&un=<?php echo base64_encode($val['STAFF_NAME']); ?>" target="_blank">
          <?php echo $val['STAFF_NAME'] ?>
        </a>
      </div>
      <div>             
        <div class="c100 p<?php echo ($val['CONTRIBUTION'] <= 100) ? $val['CONTRIBUTION'] : '100'; ?>">
            <span><?php echo $val['CONTRIBUTION'] ?>%</span>
            <div class="slice">
                <div class="bar"></div>
                <div class="fill"></div>
            </div>
        </div>                    
      </div>
      <div>
        <?php echo $val['TIME_SPENT']; ?>
        <span> hours</span>
        <span>
          <button type="button" class="btn btn-info btn-lg mdl-task-details" data-toggle="modal" data-target="#task_details" data-details='<?php echo $val['TASK']; ?>'>View Details</button>
        </span>
      </div>
    </div><?php
  }
}else{ ?>
  <div class="analytics-card analytics-no-card-wrapper">
    <div class="no-data-found" colspan="6">No data found</div>
  </div><?php
} ?>