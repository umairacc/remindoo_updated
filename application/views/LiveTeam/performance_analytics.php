<div class="tabset tab-performance-analytics">
  <input type="radio" class="tab-set" name="tabset" id="tab1" aria-controls="today" checked>
  <label for="tab1" class="tab-perf active">Logged Today</label>
  <input type="radio" class="tab-set" name="tabset" id="tab2" aria-controls="yesterday">
  <label for="tab2" class="tab-perf">Yesterday</label>
  <input type="radio" class="tab-set" name="tabset" id="tab3" aria-controls="this_week">
  <label for="tab3" class="tab-perf">This Week</label>
  <input type="radio" class="tab-set" name="tabset" id="tab4" aria-controls="last_week">
  <label for="tab4" class="tab-perf">Last Week</label>
  <input type="radio" class="tab-set" name="tabset" id="tab5" aria-controls="this_month">
  <label for="tab5" class="tab-perf">This Month</label>
  <input type="radio" class="tab-set" name="tabset" id="tab6" aria-controls="last_month">
  <label for="tab6" class="tab-perf">Last Month</label>

  <input type="hidden" id="active_tab" value="tab1">

  <div class="tab-panels">
    <section id="today" class="tab-panel"><?php $this->load->view('LiveTeam/analytics_card', ['data' => $data['today']]); ?></section>
    <section id="yesterday" class="tab-panel"><?php $this->load->view('LiveTeam/analytics_card', ['data' => $data['yesterday']]); ?></section>
    <section id="this_week" class="tab-panel"><?php $this->load->view('LiveTeam/analytics_card', ['data' => $data['this_week']]); ?></section>
    <section id="last_week" class="tab-panel"><?php $this->load->view('LiveTeam/analytics_card', ['data' => $data['last_week']]); ?></section>
    <section id="this_month" class="tab-panel"><?php $this->load->view('LiveTeam/analytics_card', ['data' => $data['this_month']]); ?></section>
    <section id="last_month" class="tab-panel"><?php $this->load->view('LiveTeam/analytics_card', ['data' => $data['last_month']]); ?></section>
  </div>  
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $(".tab-perf").click(function () {
        if(!$(this).hasClass('active'))
        {
            $(".tab-perf.active").removeClass("active");
            $(this).addClass("active");        
        }
    });
  });
</script>

