<table id="customers">
    <thead>
        <tr>
            <th>Staff</th>
            <th>Task Created On</th>
            <th>Last Timer Started At</th>
            <th>Time Spent</th>
            <th>Client</th>
            <th>Service</th>
            <th>Chargeable</th>
        </tr>
    </thead>
    <?php
        if(!empty($data)){
                foreach($data as $val){ 
                if(date('Y-m-d', $val['TIMER_STARTED']) == date('Y-m-d')){ ?>
                        <tr>
                            <td><?php echo $val['STAFF_NAME']; ?></td>
                            <td><?php echo date('Y-m-d', $val['CREATED_AT']); ?></td>
                            <td><?php echo date('Y-m-d H:i:s', $val['TIMER_STARTED']); ?></td>
                            <td>
                                <span class="<?php echo ($val['TIMER_STATUS'] == 1) ? 'active-member' : ''; ?>">
                                    <?php echo $this->Task_creating_model->unixTime2text($val['TIME_SPENT']); ?>
                                </span>
                            </td>            
                            <td style="width: 250px"><?php echo $val['CUSTOMER_NAME']; ?></td>
                            <td style="width: 320px"><?php echo $val['SERVICE_NAME']; ?></td>
                            <td><?php echo ($val['CHARGEABLE'] == 'Billable') ? 'Yes' : 'No'; ?></td>                        
                        </tr>
                <?php }
        }
    }else{ ?>
        <tr>
            <td class="no-data-found" colspan="2">No data found</td>
        </tr><?php
    } ?>  
</table> 