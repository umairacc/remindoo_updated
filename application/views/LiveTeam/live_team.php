<?php $this->load->view('includes/header'); ?>
<link href="<?php echo base_url();?>assets/css/custom/live-team.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/css/custom/circle.css" rel="stylesheet" type="text/css" />
<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" type="text/css" /> -->

<div class="pcoded-content card-removes push-left" id="task_list_view">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body rem-tasks live-team-wrapper">
                    <div class="row">                        
                        <div class="col-sm-12">
                            <span class="team-loader">
                                <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                                <span class="sr-only">Loading...</span>
                            </span>
                            <h2 style="margin: 20px 0">Live Team</h2>                            
                            <!-- <div id="live_team_report"></div>                             -->
                            <h2 style="margin: 20px 0">Performance Analytics</h2>
                            <div id="performance_analytics"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="loading_status" value=0>

<div id="task_details" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-live">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Task Details</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('includes/footer'); ?>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script src="<?php echo base_url();?>assets/js/custom/live_team.js" type="text/javascript"></script>

