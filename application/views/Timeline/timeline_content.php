<?php
// $result = $this->db->query('select * from user where id="' . $_SESSION['id'] . '"')->row_array();

// function getUserNameByID($db_obj, $user_id){
// 		$result = $db_obj->query('select * from user where id="' . $user_id . '"')->row_array();
// 		return $result['crm_name'];
// }

foreach ($source as $key => $source_value) {
	?>
	<div class="cd-timeline-block">
		<div class="cd-timeline-icon bg-primary">
			<i class="icofont icofont-tasks-alt"></i>
		</div>
		<div class="cd-timeline-content card_main">
			<div class="p-20">
				<?php 
					if($source_value['pinned_to_top'])
					{
						$pin_flag = '<span><i class="fa fa-bookmark-o" aria-hidden="true" title="Pinned Card"></i></span>'
				?>
					<span class="oval-timeline-icon"><?php echo $pin_flag ?></span>
				<?php } ?>
				<div class="btn-group dropdown-split-primary" style="float: right;">
					<button type="button" class="btn btn-primary  dropdown-toggle dropdown-toggle-split waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</button>
					<div class="dropdown-menu">
						<?php if ($_SESSION['permission']['Client_Section']['edit'] == '1' && $source_value['type'] != 'log') { ?>
							<span class="notes_edit">
								<a href="javascript:void(0);" data-id="<?php echo $source_value['id']; ?>" class="client_timeline_note_edit">Edit</a>
							</span>
						<?php } ?>

						<?php if ($_SESSION['permission']['Client_Section']['delete'] == '1' && $source_value['type'] != 'log') { ?>
							<span class="notes_edit">
								<a href="javascript:void(0);" data-id="<?php echo $source_value['id']; ?>" class="client_timeline_note_delete">Delete</a>
							</span>
						<?php } ?>

						<span class="notes_edit">
							<a href="<?php echo base_url(); ?>user/new_task/<?php echo $source_value['type'] . '_' . $source_value['id']; ?>" target="_blank">Create Task</a>
						</span>
						<?php
						  if($source_value['pinned_to_top'])
						  {
						  	$pin_flag = '<span class="oval">PINNED</span>';
						  	?>
						  	<span class="notes_edit">
								<a href="#" class="pin_to_top" data-id="<?php echo $source_value['id'];?>" data-section="<?php echo $source_value['section'];?>" data-activity="unpin">Unpin</a>
							</span>
						  	<?php
						  }
						  else
						  {
						  	$pin_flag = '';
						  	?>
						  	<span class="notes_edit">
								<a href="#" class="pin_to_top" data-id="<?php echo $source_value['id'];?>" data-section="<?php echo $source_value['section'];?>" data-activity="pin">Pin To Top</a>
							</span>
						  	<?php
						  }	
						?>

					</div>
				</div>

				<div class="timeline-details">
					<?php
						if ($source_value['section'] != '')  echo "<b class='text-primary'>Section : " . $source_value['section'] . "</b></br>";
						if ($source_value['service'] != '')  echo "<b class='text-success'>Service : " . $source_value['service'] . "</b></br>";
						if ($source_value['date'] != '')     echo "<b class='text-info'>Selected Date : " . $source_value['date'] . "</b>";
					?>
					<div class="log">
						<?php echo $source_value['log'] ?>
					</div>
				</div>

			</div>
			<div class="note-date-timeline">
				<span class="cd-date" style="text-align: left;">
					Added by: 
				</span>
				<span class="cd-date"> <?php echo date('F d, Y, h:i', $source_value['created_time']); ?></span>
			</div>
		</div>
	</div>
<?php } ?>