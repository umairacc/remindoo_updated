<style type="text/css">
  .notes_edit{
    float: right;
  }
</style>

<?php 
$team = $this->db->query("select * from team where create_by=".$_SESSION['id']." ")->result_array();

$department=$this->db->query("select * from department_permission where create_by=".$_SESSION['id']." ")->result_array();
?>
  <?php
   $client_services=array("VAT"=>"VAT","Payroll"=>"Payroll","Accounts"=>"Accounts","Confirmation_statement"=>"Confirmation statement","Company_Tax_Return"=>"Company Tax Return","Personal_Tax_Return"=>"Personal Tax Return","WorkPlace_Pension"=>"WorkPlace Pension - AE","CIS_Contractor"=>"CIS - Contractor","CIS_Sub_Contractor"=>"CIS - Sub Contractor","P11D"=>"P11D","Bookkeeping"=>"Bookkeeping","Management_Accounts"=>"Management Accounts","Investigation_Insurance"=>"Investigation Insurance","Registered_Address"=>"Registered Address","Tax_Advice"=>"Tax Advice","Tax_Investigation"=>"Tax Investigation"); ?>
   <div id="tabs221">
     <input type="hidden" name="timeline_client_id" id="timeline_client_id" value="<?php echo $client_id; ?>">
                <ul>
                  <li class="active"><a href="#tabs-1" class="for_tabs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>new note</a></li>
                  <li><a href="#tabs-2" class="for_tabs"><i class="fa fa-envelope" aria-hidden="true"></i>email</a></li>
                  <li><a href="#tabs-3" class="for_tabs"><i class="fa fa-phone" aria-hidden="true"></i>call</a></li>
                  <li><a href="#tabs-7" class="for_tabs"><i class="fa fa-envelope" aria-hidden="true"></i>SMS</a></li>
                  <li><a href="#tabs-13" class="for_tabs"><i class="fa fa-envelope" aria-hidden="true"></i>Meeting</a></li>
                  <li><a href="#tabs-4" class="for_tabs"><i class="fa fa-plus" aria-hidden="true"></i>log activity</a></li>
                  <li><a href="#tabs-11" class="for_tabs for_allactivity_section_ajax"><i class="fa fa-plus" aria-hidden="true"></i>All activity</a></li>
                  <li><a href="#tabs-5" class="for_tabs"><i class="fa fa-file-text-o" aria-hidden="true"></i>create task</a></li>
                  <li><a href="#tabs-6" class="for_tabs"><i class="fa fa-clock-o" aria-hidden="true"></i>schedule</a></li>
                  <!-- <li class="visible-schedule"><a href="#tabs-7">Add Event</a></li> -->
                </ul>
                <div id="tabs-1">
                  <div class="first-tag">
                
                    <textarea id="editor1" placeholder="Start typing to leave a note...">
                    
                    </textarea>
                    <span class="notes_errormsg error_msg" style="color: red;"></span>
                    &nbsp;
                    <!-- Services type -->
                    <div class="form-group col-sm-6 for_services_type">
                    <label>Services Type</label>
                         <div class="dropdown-sin-12 lead-form-st">
                                       <select name="services_type" id="services_type" class="form-control fields" placeholder="Select Services Type">
                                          <option value="">--Select Service Type--</option>
                                          <?php
                                      foreach ($client_services as $clse_key => $clse_value) {
                                         ?>
                                          <option value="<?php echo $clse_key;?>"><?php echo $clse_value; ?></option>
                                       <?php } ?>
                                       </select>
                         </div>
                          <span class="services_type_errormsg error_msg" style="color: red;"></span>
                         </div>
                        
                    <!-- end of services type -->
                   <!--  <span class="save-btn ">
                    <div><button class="btn btn-primary notes_section_insert">save</button><div></span> -->
                    <input type="hidden" name="update_notes_id" id="update_notes_id">
                    <div class="timeline_button timeline_add" style="float: right;">
                        <input type="button" class="btn btn-primary notes_section_insert" name="save" id="save" value="Create">
                    </div>
                    <div class="timeline_button timeline_update" style="float: right;display: none;">
                        <input type="button" class="btn btn-primary notes_section_update" name="save" id="save" value="Update">
                        <input type="button" class="btn btn-primary notes_section_cancel" name="save" id="save" value="Cancel">
                    </div>


                  </div>
                  <div id="notes_section">
                         <div class="card ser-timeline">
                            <div class="card-header">
                            </div>
                            <div class="card-header">
                               <div class="col-12">
                                  <input class="form-control form-control-lg Note-search_timeline" type="text" id="notes_section" placeholder="Search">
                               </div>
                            </div>
                            <div class="card-block">
                               <div class="main-timeline">
                                  <div class="cd-timeline cd-container overall_notes_section" id="search_for_activity">
                                     <!-- from db data loop start -->
                                     <!-- updated date -->
                                     <?php
                                     $notes_section_data=$this->db->query("select * from timeline_services_notes_added where user_id=".$client_id." and module='notes' order by id desc ")->result_array();
                                      foreach ($notes_section_data as $notes_key => $notes_value) {
                                      ?>
                                     <div class="cd-timeline-block notes_section">
                                        <div class="cd-timeline-icon bg-primary">
                                           <i class="icofont icofont-ui-file"></i>
                                        </div>
                                        <div class="cd-timeline-content card_main">
                                           <div class="p-0">
                                           
                                      

                                      <div class="btn-group dropdown-split-primary">
                                                                            <button type="button" class="btn btn-primary  dropdown-toggle dropdown-toggle-split waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                Actions   
                                                                              </button>
                                                                  <div class="dropdown-menu">
                                                                  <span class="notes_edit"><a href="javascript:void(0);" onclick="return confirm_delete_timeline('<?php echo $notes_value['id'];?>');"  >Delete</a></span>
                                                                  <span class="notes_edit"><a href="javascript:void(0);" onclick="topFunction(<?php echo $notes_value['id']; ?>,'notes')">Edit</a></span>
                                                                   <span class="notes_edit"><a href="javascript:void(0);" onclick="archiveFunction(<?php echo $notes_value['id']; ?>)">Archive</a></span>
                                                                  </div>
                                                                        </div>


                                           </div>
                                           <!-- notes delete section -->       
 <div class="modal-alertsuccess alert alert-success" id="delete_notes<?php echo $notes_value['id'];?>" style="display:none;">
         <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
         <div class="pop-realted1">
            <div class="position-alert1">
               Are you sure want to delete <b><a href="javascript:void(0);" class="notes_delete_function" data-id="<?php echo $notes_value['id'];?>"> Yes </a></b> OR <b><a href="#" id="close">No</a></b>
            </div>
         </div>                             
 </div>
 <!-- end of notes delete section -->
                                           <div class="p-20">
                                              <h6><?php echo ($client_details[0]['crm_company_name'])?$client_details[0]['crm_company_name']:$this->Common_mdl->get_crm_name($client_details[0]['user_id']);?> - <?php echo $client_details[0]['crm_legal_form'];?></h6>
                                              <div class="timeline-details">
                                                 <!-- <p class="m-t-0"><?php //echo  ?></p> -->
                                                 <?php echo $notes_value['notes']; ?>
                                              </div>
                                              <a href="<?php echo base_url(); ?>user/new_task/<?php echo $notes_value['id']; ?>" class="btn btn-primary" target="_blank">Create Task</a>
                                               <a href="<?php echo base_url(); ?>user/archive_task/<?php echo $notes_value['id']; ?>" class="btn btn-primary" target="_blank">Archive Task</a>
                                           </div>
                                           <span class="cd-date"><?php if(isset($notes_value['created_time'])){ echo date('F j, Y, g:i a',$notes_value['created_time']); } ?></span>
                                           <span class="cd-details">Notes For <?php echo $client_services[$notes_value['services_type']]; ?></span>
                                        </div>
                                     </div>
                                     <?php } ?>
                                     <!-- end of updated date -->
                                    
                                  </div>
                                  <div class="cd-timeline cd-container overall_for_notes_section" id="search_for_activity" style="display: none;">
                                  </div>
                               </div>
                            </div>
                         </div>
                         <!-- end -->
                      </div>
                </div>
                <div id="tabs-2">
                  <div class="first-tag">

                    <textarea id="editor2" placeholder="Start typing to leave a note...">
                    
                    </textarea>
                    <span class="emailnote_errormsg error_msg" style="color: red;"></span>
                    <span class="save-btn">
                    <a href="javascript:void(0);" class="timeline_email_to_client btn btn-primary" target="_blank" >Email The Client</a>
                    <!-- <button type="button" class="timeline_email_to_client btn btn-primary">Email the client</button> --></span>
                  </div>

                   <div class="card ser-timeline">
                            <div class="card-header">
                            </div>
                            <div class="card-header">
                               <div class="col-12">
                                  <input class="form-control form-control-lg Email-search_timeline" type="text" id="email_section" placeholder="Search">
                               </div>
                            </div>
                            <div class="card-block">
                               <div class="main-timeline">
                                  <div class="cd-timeline cd-container overall_email_section" id="search_for_activity">
                                     <!-- from db data loop start -->
                                     <!-- updated date -->
                                     <?php
                                     $email_data=$this->db->query("select * from timeline_services_notes_added where user_id=".$client_id." and module='email' order by id desc ")->result_array();
                                      foreach ($email_data as $email_key => $email_value) {
                                      ?>
                                     <div class="cd-timeline-block email_section">
                                        <div class="cd-timeline-icon bg-primary">
                                         <i class="fa fa-envelope" aria-hidden="true"></i>
                                        </div>
                                        <div class="cd-timeline-content card_main">
                                           <div class="p-0">
                                            <div class="btn-group dropdown-split-primary">
                                            <button type="button" class="btn btn-primary  dropdown-toggle dropdown-toggle-split waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Actions   
                                            </button>
                                            <div class="dropdown-menu">
                                            <span class="notes_edit"><a href="javascript:void(0);" onclick="return confirm_delete_timeline('<?php echo $email_value['id'];?>');"  >Delete</a></span>
                                            <span class="notes_edit"><a href="javascript:void(0);" onclick="topFunction(<?php echo $email_value['id']; ?>,'calllog')">Edit</a></span>

                                              <span class="notes_edit"><a href="javascript:void(0);" onclick="archiveFunction(<?php echo $email_value['id']; ?>)">Archive</a></span>
                                            </div>
                                            </div>
                                           </div>
                                           <!-- notes delete section -->       
 <div class="modal-alertsuccess alert alert-success" id="delete_notes<?php echo $calllog_value['id'];?>" style="display:none;">
         <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
         <div class="pop-realted1">
            <div class="position-alert1">
               Are you sure want to delete <b><a href="javascript:void(0);" class="calllog_delete_function" data-id="<?php echo $email_value['id'];?>"> Yes </a></b> OR <b><a href="#" id="close">No</a></b>
            </div>
         </div>                             
 </div>
 <!-- end of notes delete section -->
                                           <div class="p-20">
                                              <h6><?php echo ($client_details[0]['crm_company_name'])?$client_details[0]['crm_company_name']:$this->Common_mdl->get_crm_name($client_details[0]['user_id']);?> - <?php echo $client_details[0]['crm_legal_form'];?></h6>
                                              <div class="timeline-details">
                                                 <!-- <p class="m-t-0"><?php //echo  ?></p> -->
                                                 <?php echo $email_value['notes']; ?>
                                              </div>
                                           </div>
                                           <span class="cd-date"><?php if(isset($email_value['created_time'])){ echo date('F j, Y, g:i a',$email_value['created_time']); } ?></span>
                                           <span class="cd-details">Email log Activity</span>
                                        </div>
                                     </div>
                                     <?php } ?>
                                     <!-- end of updated date -->
                                    
                                  </div>
                                  <div class="cd-timeline cd-container overall_for_email_section" id="search_for_activity" style="display: none;">
                                  </div>
                               </div>
                            </div>
                         </div>
                </div>
           
                <!-- call log activity -->
                <div id="tabs-3">
                  <div class="first-tag">
                 <!--  <input type="text" name="time" id="time" value="<?php $date=date("g:ia"); echo $date;  ?>" > -->
                    <textarea id="editor21" placeholder="Start typing to leave a note...">
                    
                    </textarea>
                    <span class="calllog_errormsg error_msg" style="color: red;"></span>



                    <input type="text" class="datepicker"  name="call_date" id="call_date" value="">          
                  
                    <!-- Services type -->
                                     
                    <!-- end of services type -->
                   <!--  <span class="save-btn ">
                    <div><button class="btn btn-primary notes_section_insert">save</button><div></span> -->
                    <input type="hidden" name="update_calllog_id" id="update_calllog_id">
                    <div class="timeline_button timelinecall_add" style="float: right;">
                        <input type="button" class="btn btn-primary calllog_section_insert" name="save" id="save" value="Create">
                    </div>
                    <div class="timeline_button timelinecall_update" style="float: right;display: none;">
                        <input type="button" class="btn btn-primary calllog_section_update" name="save" id="save" value="Update">
                        <input type="button" class="btn btn-primary calllog_section_cancel" name="save" id="save" value="Cancel">
                    </div>


                  </div>
                  <div id="calllog_section">
                         <div class="card ser-timeline">
                            <div class="card-header">
                            </div>
                            <div class="card-header">
                               <div class="col-12">
                                  <input class="form-control form-control-lg Calllog-search_timeline" type="text" id="calllog_section" placeholder="Search">
                               </div>
                            </div>
                            <div class="card-block">
                               <div class="main-timeline">
                                  <div class="cd-timeline cd-container overall_calllog_section" id="search_for_activity">
                                     <!-- from db data loop start -->
                                     <!-- updated date -->
                                     <?php
                                     $calllog_data=$this->db->query("select * from timeline_services_notes_added where user_id=".$client_id." and module='calllog' order by id desc ")->result_array();
                                      foreach ($calllog_data as $calllog_key => $calllog_value) {
                                      ?>
                                     <div class="cd-timeline-block calllog_section">
                                        <div class="cd-timeline-icon bg-primary">
                                           <i class="icofont icofont-phone-circle"></i>
                                        </div>
                                        <div class="cd-timeline-content card_main">
                                           <div class="p-0">
                                           
                                      

                                      <div class="btn-group dropdown-split-primary">
                                                                            <button type="button" class="btn btn-primary  dropdown-toggle dropdown-toggle-split waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                Actions   
                                                                              </button>
                                                                            <div class="dropdown-menu">
                                                                              <span class="notes_edit"><a href="javascript:void(0);" onclick="return confirm_delete_timeline('<?php echo $calllog_value['id'];?>');"  >Delete</a></span>
                                           <span class="notes_edit"><a href="javascript:void(0);" onclick="topFunction(<?php echo $calllog_value['id']; ?>,'calllog')">Edit</a></span>

                                            <span class="notes_edit"><a href="javascript:void(0);" onclick="archiveFunction(<?php echo $calllog_value['id']; ?>)">Archive</a></span>
                                                                            </div>
                                                                        </div>


                                           </div>
                                           <!-- notes delete section -->       
 <div class="modal-alertsuccess alert alert-success" id="delete_notes<?php echo $calllog_value['id'];?>" style="display:none;">
         <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
         <div class="pop-realted1">
            <div class="position-alert1">
               Are you sure want to delete <b><a href="javascript:void(0);" class="calllog_delete_function" data-id="<?php echo $calllog_value['id'];?>"> Yes </a></b> OR <b><a href="#" id="close">No</a></b>
            </div>
         </div>                             
 </div>
 <!-- end of notes delete section -->
                                           <div class="p-20">
                                              <h6><?php echo ($client_details[0]['crm_company_name'])?$client_details[0]['crm_company_name']:$this->Common_mdl->get_crm_name($client_details[0]['user_id']);?> - <?php echo $client_details[0]['crm_legal_form'];?></h6>
                                              <div class="timeline-details">
                                                 <!-- <p class="m-t-0"><?php //echo  ?></p> -->
                                                 <?php echo $calllog_value['notes']; ?>
                                              </div>
                                           </div>
                                           <span class="cd-date"><?php if(isset($calllog_value['created_time'])){ echo date('F j, Y, g:i a',$calllog_value['created_time']); } ?></span>
                                           <span class="cd-details">Call log Activity</span>
                                        </div>
                                     </div>
                                     <?php } ?>
                                     <!-- end of updated date -->
                                    
                                  </div>
                                  <div class="cd-timeline cd-container overall_for_calllog_section" id="search_for_activity" style="display: none;">
                                  </div>
                               </div>
                            </div>
                         </div>
                         <!-- end -->
                      </div>
                </div>
                <!-- end of log activity -->



                <!--- Start Sms Tab -->
                  <div id="tabs-7">
                  <div class="first-tag">
                 <!--  <input type="text" name="time" id="time" value="<?php $date=date("g:ia"); echo $date;  ?>" > -->
                    <textarea id="editor51" placeholder="Start typing to leave a note...">
                    
                    </textarea>
                    <span class="sms_errormsg error_msg" style="color: red;"></span>




                    <?php $mobile_number=$this->db->query("select mobile from client_contacts where client_id=".$_SESSION['id']." and make_primary='1'")->row_array();                    
                    ?>
                  <input type="hidden" name="mobile_number" id="mobile_number" value="<?php echo $mobile_number['mobile']; ?>">
                    <!-- Services type -->
                                     
                    <!-- end of services type -->
                   <!--  <span class="save-btn ">
                    <div><button class="btn btn-primary notes_section_insert">save</button><div></span> -->
                    <input type="hidden" name="update_sms_id" id="update_sms_id">
                    <div class="timeline_button timelinesms_add" style="float: right;">
                        <input type="button" class="btn btn-primary sms_section_insert" name="save" id="save" value="Create">
                    </div>
                    <div class="timeline_button timelinesms_update" style="float: right;display: none;">
                        <input type="button" class="btn btn-primary sms_section_update" name="save" id="save" value="Update">
                        <input type="button" class="btn btn-primary sms_section_cancel" name="save" id="save" value="Cancel">
                    </div>


                  </div>
                  <div id="sms_section">
                         <div class="card ser-timeline">
                            <div class="card-header">
                            </div>
                            <div class="card-header">
                               <div class="col-12">
                                  <input class="form-control form-control-lg Sms-search_timeline" type="text" id="sms_section" placeholder="Search">
                               </div>
                            </div>
                            <div class="card-block">
                               <div class="main-timeline">
                                  <div class="cd-timeline cd-container overall_sms_section" id="search_for_activity">
                                     <!-- from db data loop start -->
                                     <!-- updated date -->
                                     <?php
                                     $sms_data=$this->db->query("select * from timeline_services_notes_added where user_id=".$client_id." and module='smslog' order by id desc ")->result_array();
                                      foreach ($sms_data as $sms_key => $sms_value) {
                                      ?>
                                     <div class="cd-timeline-block sms_section">
                                        <div class="cd-timeline-icon bg-primary">
                                           <i class="fa fa-envelope" aria-hidden="true"></i>
                                        </div>
                                        <div class="cd-timeline-content card_main">
                                           <div class="p-0">

                                      <div class="btn-group dropdown-split-primary">
                                                                            <button type="button" class="btn btn-primary  dropdown-toggle dropdown-toggle-split waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                Actions   
                                                                              </button>
                                                                            <div class="dropdown-menu">
                                                                              <span class="notes_edit"><a href="javascript:void(0);" onclick="return confirm_delete_timeline('<?php echo $sms_value['id'];?>');"  >Delete</a></span>
                                           <span class="notes_edit"><a href="javascript:void(0);" onclick="topFunction(<?php echo $sms_value['id']; ?>,'smslog')">Edit</a></span>

                                            <span class="notes_edit"><a href="javascript:void(0);" onclick="archiveFunction(<?php echo $sms_value['id']; ?>)">Archive</a></span>
                                                                            </div>
                                                                        </div>


                                           </div>
                                           <!-- notes delete section -->       
 <div class="modal-alertsuccess alert alert-success" id="delete_notes<?php echo $sms_value['id'];?>" style="display:none;">
         <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
         <div class="pop-realted1">
            <div class="position-alert1">
               Are you sure want to delete <b><a href="javascript:void(0);" class="sms_delete_function" data-id="<?php echo $sms_value['id'];?>"> Yes </a></b> OR <b><a href="#" id="close">No</a></b>
            </div>
         </div>                             
 </div>
 <!-- end of notes delete section -->
                                           <div class="p-20">
                                              <h6><?php echo ($client_details[0]['crm_company_name'])?$client_details[0]['crm_company_name']:$this->Common_mdl->get_crm_name($client_details[0]['user_id']);?> - <?php echo $client_details[0]['crm_legal_form'];?></h6>
                                              <div class="timeline-details">
                                                 <!-- <p class="m-t-0"><?php //echo  ?></p> -->
                                                 <?php echo $sms_value['notes']; ?>
                                              </div>
                                           </div>
                                           <span class="cd-date"><?php if(isset($sms_value['created_time'])){ echo date('F j, Y, g:i a',$sms_value['created_time']); } ?></span>
                                           <span class="cd-details">Sms log Activity</span>
                                        </div>
                                     </div>
                                     <?php } ?>
                                     <!-- end of updated date -->
                                    
                                  </div>
                                  <div class="cd-timeline cd-container overall_for_sms_section" id="search_for_activity" style="display: none;">
                                  </div>
                               </div>
                            </div>
                         </div>
                         <!-- end -->
                      </div>
                </div>

                <!-- End Sms Tab   -->


                <!-- Start Metting Tab-->
                   <div id="tabs-13">
                  <div class="first-tag">
                 <!--  <input type="text" name="time" id="time" value="<?php $date=date("g:ia"); echo $date;  ?>" > -->
                    <textarea id="editor61" placeholder="Start typing to leave a note...">                    
                    </textarea>
                    <span class="meeting_errormsg error_msg" style="color: red;"></span>  


                    <input type="text" class="datepicker"  name="meeting_date" id="meeting_date" value="">               
                    <!-- Services type -->
                                     
                    <!-- end of services type -->
                   <!--  <span class="save-btn ">
                    <div><button class="btn btn-primary notes_section_insert">save</button><div></span> -->
                    <input type="hidden" name="update_meeting_id" id="update_meeting_id">
                    <div class="timeline_button timelinesms_add" style="float: right;">
                        <input type="button" class="btn btn-primary meeting_section_insert" name="save" id="save" value="Create">
                    </div>
                    <div class="timeline_button timelinesms_update" style="float: right;display: none;">
                        <input type="button" class="btn btn-primary meeting_section_update" name="save" id="save" value="Update">
                        <input type="button" class="btn btn-primary meeting_section_cancel" name="save" id="save" value="Cancel">
                    </div>


                  </div>
                  <div id="meeting_section">
                         <div class="card ser-timeline">
                            <div class="card-header">
                            </div>
                            <div class="card-header">
                               <div class="col-12">
                                  <input class="form-control form-control-lg meeting-search_timeline" type="text" id="meeting_section" placeholder="Search">
                               </div>
                            </div>
                            <div class="card-block">
                               <div class="main-timeline">
                                  <div class="cd-timeline cd-container overall_meeting_section" id="search_for_activity">
                                     <!-- from db data loop start -->
                                     <!-- updated date -->
                                     <?php
                                     $meeting_data=$this->db->query("select * from timeline_services_notes_added where user_id=".$client_id." and module='meeting' order by id desc ")->result_array();
                                      foreach ($meeting_data as $meeting_key => $meeting_value) {
                                      ?>
                                     <div class="cd-timeline-block meeting_section">
                                        <div class="cd-timeline-icon bg-primary">
                                           <i class="fa fa-envelope" aria-hidden="true"></i>
                                        </div>
                                        <div class="cd-timeline-content card_main">
                                           <div class="p-0">

                                      <div class="btn-group dropdown-split-primary">
                                                                            <button type="button" class="btn btn-primary  dropdown-toggle dropdown-toggle-split waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                Actions   
                                                                              </button>
                                                                            <div class="dropdown-menu">
                                                                              <span class="notes_edit"><a href="javascript:void(0);" onclick="return confirm_delete_timeline('<?php echo $meeting_value['id'];?>');"  >Delete</a></span>
                                           <span class="notes_edit"><a href="javascript:void(0);" onclick="topFunction(<?php echo $meeting_value['id']; ?>,'meeting')">Edit</a></span>

                                            <span class="notes_edit"><a href="javascript:void(0);" onclick="archiveFunction(<?php echo $meeting_value['id']; ?>)">Archive</a></span>
                                                                            </div>
                                                                        </div>


                                           </div>
                                           <!-- notes delete section -->       
 <div class="modal-alertsuccess alert alert-success" id="delete_notes<?php echo $meeting_value['id'];?>" style="display:none;">
         <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
         <div class="pop-realted1">
            <div class="position-alert1">
               Are you sure want to delete <b><a href="javascript:void(0);" class="meeting_delete_function" data-id="<?php echo $meeting['id'];?>"> Yes </a></b> OR <b><a href="#" id="close">No</a></b>
            </div>
         </div>                             
 </div>
 <!-- end of notes delete section -->
                                           <div class="p-20">
                                              <h6><?php echo ($client_details[0]['crm_company_name'])?$client_details[0]['crm_company_name']:$this->Common_mdl->get_crm_name($client_details[0]['user_id']);?> - <?php echo $client_details[0]['crm_legal_form'];?></h6>
                                              <div class="timeline-details">
                                                 <!-- <p class="m-t-0"><?php //echo  ?></p> -->
                                                 <?php echo $meeting_value['notes']; ?>-<?php echo $meeting_value['date']; ?>
                                              </div>
                                           </div>
                                           <span class="cd-date"><?php if(isset($meeting_value['created_time'])){ echo date('F j, Y, g:i a',$meeting_value['created_time']); } ?></span>
                                           <span class="cd-details">Meeting log Activity</span>
                                        </div>
                                     </div>
                                     <?php } ?>
                                     <!-- end of updated date -->
                                    
                                  </div>
                                  <div class="cd-timeline cd-container overall_for_meeting_section" id="search_for_activity" style="display: none;">
                                  </div>
                               </div>
                            </div>
                         </div>
                         <!-- end -->
                      </div>
                </div>


                <!-- End Metting Tab -->
                 <div id="tabs-4">
                  <div class="first-tag">
                    <textarea id="editor22" placeholder="Start typing to leave a note...">
                   
                    </textarea>
                    <span class="log_errormsg error_msg" style="color: red;"></span>
                    <!-- <span class="save-btn"><button class="btn btn-primary">log activity</button></span> -->
                    <input type="hidden" name="update_log_id" id="update_log_id">
                    <div class="timeline_button timelinelog_add" style="float: right;">
                        <input type="button" class="btn btn-primary log_section_insert" name="save" id="save" value="Add log activity">
                    </div>
                    <div class="timeline_button timelinelog_update" style="float: right;display: none;">
                        <input type="button" class="btn btn-primary log_section_update" name="save" id="save" value="Update log activity">
                        <input type="button" class="btn btn-primary log_section_cancel" data-action="logsection" name="save" id="save" value="Cancel">
                    </div>

                  </div>
                  <div id="log_section">
                         <div class="card ser-timeline">
                            <div class="card-header">
                            </div>
                            <div class="card-header">
                               <div class="col-12">
                                  <input class="form-control form-control-lg log-search_timeline" type="text" id="log_section" placeholder="Search">
                               </div>
                            </div>
                            <div class="card-block">
                               <div class="main-timeline">
                                  <div class="cd-timeline cd-container overall_log_section" id="search_for_activity">
                                     <!-- from db data loop start -->
                                     <!-- updated date -->
                                     <?php
                                     $log_data=$this->db->query("select * from timeline_services_notes_added where user_id=".$client_id." and module='logactivity' order by id desc ")->result_array();
                                      foreach ($log_data as $calllog_key => $calllog_value) {
                                      ?>
                                     <div class="cd-timeline-block log_section">
                                        <div class="cd-timeline-icon bg-primary">
                                           <i class="icofont icofont-ui-file"></i>
                                        </div>
                                        <div class="cd-timeline-content card_main">
                                           <div class="p-0">
                                           
                                      

                                      <div class="btn-group dropdown-split-primary">
                                                                            <button type="button" class="btn btn-primary  dropdown-toggle dropdown-toggle-split waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                Actions   
                                                                              </button>
                                                                            <div class="dropdown-menu">
                                                                              <span class="notes_edit"><a href="javascript:void(0);" onclick="return confirm_delete_timeline('<?php echo $calllog_value['id'];?>');"  >Delete</a></span>
                                           <span class="notes_edit"><a href="javascript:void(0);" onclick="topFunction(<?php echo $calllog_value['id']; ?>,'log')">Edit</a></span>

                                           <span class="notes_edit"><a href="javascript:void(0);" onclick="archiveFunction(<?php echo $calllog_value['id']; ?>)">Archive</a></span>
                                                                            </div>
                                                                        </div>


                                           </div>
                                           <!-- notes delete section -->       
 <div class="modal-alertsuccess alert alert-success" id="delete_notes<?php echo $calllog_value['id'];?>" style="display:none;">
         <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
         <div class="pop-realted1">
            <div class="position-alert1">
               Are you sure want to delete <b><a href="javascript:void(0);" class="log_delete_function" data-action="logsection" data-id="<?php echo $calllog_value['id'];?>"> Yes </a></b> OR <b><a href="#" id="close">No</a></b>
            </div>
         </div>                             
 </div>
 <!-- end of notes delete section -->
                                           <div class="p-20">
                                              <h6><?php echo ($client_details[0]['crm_company_name'])?$client_details[0]['crm_company_name']:$this->Common_mdl->get_crm_name($client_details[0]['user_id']);?> - <?php echo $client_details[0]['crm_legal_form'];?></h6>
                                              <div class="timeline-details">
                                                 <!-- <p class="m-t-0"><?php //echo  ?></p> -->
                                                 <?php echo $calllog_value['notes']; ?>
                                              </div>
                                           </div>
                                           <span class="cd-date"><?php if(isset($calllog_value['created_time'])){ echo date('F j, Y, g:i a',$calllog_value['created_time']); } ?></span>
                                           <span class="cd-details">log Activity</span>
                                        </div>
                                     </div>
                                     <?php } ?>
                                     <!-- end of updated date -->
                                    
                                  </div>
                                  <div class="cd-timeline cd-container overall_for_log_section" id="search_for_activity" style="display: none;">
                                  </div>
                               </div>
                            </div>
                         </div>
                         <!-- end -->
                      </div>
                </div>
                <!-- for allactivity logs -->
<div id="tabs-11">
<!-- content from ajax data --> 
                    <div id="allactivitylog_sectionsss"> <div class="card ser-timeline">
                           <div class="card-header">
                               <div class="col-md-12"> 
                                <div class="col-md-2"> 
                                <button type="button" class="btn btn-primary" id="notes" onclick="trigger(this)" style="
                                font-size: 10px;">All Section</button></div>                          
                               <div class="col-md-2 filter_timeline_list">
                                <!-- for filter options 18-09-2018 -->
 <div class="button-group">
        <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" style="
                                font-size: 10px;">Filter Timeline</button>
<ul class="dropdown-menu">
  <li style="display: none;"><a href="javascript:void(0);" class="small" data-value="option1" tabIndex="-1"><input type="checkbox" name="filter_timeline[]" id="filter_timeline_notesactivity" class="filter_timeline_checkboxs notes_click" value="notes"/>&nbsp;Notes Activity</a></li>
  <li style="display: none;"><a href="javascript:void(0);" class="small" data-value="option2" tabIndex="-1"><input type="checkbox" name="filter_timeline[]" id="filter_timeline_emailactivity" class="filter_timeline_checkboxs email_click" value="email"/>&nbsp;Email Activity</a></li>
  <li style="display: none;"><a href="javascript:void(0);" class="small" data-value="option3" tabIndex="-1"><input type="checkbox" name="filter_timeline[]" id="filter_timeline_callactivity" class="filter_timeline_checkboxs call_click" value="calllog" />&nbsp;Call Activity</a></li>
  <li style="display: none;"><a href="javascript:void(0);" class="small" data-value="option4" tabIndex="-1"><input type="checkbox" name="filter_timeline[]" id="filter_timeline_logactivity" class="filter_timeline_checkboxs log_click" value="logactivity" />&nbsp;Log Activity</a></li>
  <li style="display: none;"><a href="javascript:void(0);" class="small" data-value="option5" tabIndex="-1"><input type="checkbox" name="filter_timeline[]" id="filter_timeline_taskactivity" class="filter_timeline_checkboxs task_click" value="newtask" />&nbsp;Create Task Notes</a></li>
  <li style="display: none;"><a href="javascript:void(0);" class="small" data-value="option5" tabIndex="-1"><input type="checkbox" name="filter_timeline[]" id="filter_timeline_scheduleactivity" class="filter_timeline_checkboxs schedule_click" value="schedule" />&nbsp;Schedule Activity</a></li>

   <li style="display: none;"><a href="javascript:void(0);" class="small" data-value="option5" tabIndex="-1"><input type="checkbox" name="filter_timeline[]" id="filter_timeline_smsactivity" class="filter_timeline_checkboxs sms_click" value="schedule" />&nbsp;Schedule Activity</a></li>

  <li><a href="javascript:void(0);" class="small" data-value="option6" tabIndex="-1">Client Services&nbsp;</a></li>

 <!--  <li><a href="#" class="small" data-value="option6" tabIndex="-1"><input type="checkbox"/>&nbsp;Confirmation Statement</a></li> -->
 <?php foreach ($client_services as $filt_key => $filt_value) {
    ?>
    <li><a href="javascript:void(0);" class="small" data-value="option6" tabIndex="-1"><input type="checkbox" name="filter_timeline[]" id="filter_timeline_<?php echo $filt_key; ?>" class="filter_timeline_checkboxs" value="<?php echo $filt_key; ?>" />&nbsp;<?php echo $filt_value; ?></a></li>
    <?php
 } ?>
</ul>
  </div>  
  </div>
<div class="col-md-2"> <button type="button" class="btn btn-primary" id="notes" onclick="trigger_function(this)" style="
font-size: 10px;">Notes Section (<?php echo count($notes_section_data); ?>)</button></div>
<div class="col-md-2">
<button type="button" class="btn btn-primary" id="email" onclick="trigger_function(this)" style="
font-size: 10px;">Email Section (<?php echo count($email_data); ?>)</button>
</div> 
<div class="col-md-2">
<button type="button" class="btn btn-primary" id="calllog" onclick="trigger_function(this)" style="
font-size: 10px;">Call Section (<?php echo count($calllog_data); ?>)</button>
</div> 

<div class="col-md-2">
<button type="button" class="btn btn-primary" id="smslog" onclick="trigger_function(this)" style="
font-size: 10px;">Sms Section (<?php echo count($calllog_data); ?>)</button>
</div> 

<div class="col-md-2">
<button type="button" class="btn btn-primary" id="logactivity" onclick="trigger_function(this)" style="
font-size: 10px;">Log Section (<?php echo count($log_data);?>) </button>
</div> 

<div class="col-md-2">
<button type="button" class="btn btn-primary" id="newtask" onclick="trigger_function(this)" style="
font-size: 10px;">Task Section (<?php echo count($tasks_section_data); ?>) </button>
</div> 

<div class="col-md-2">
<button type="button" class="btn btn-primary" id="schedule" onclick="trigger_function(this)" style="
font-size: 10px;">Schedule Section (<?php echo count($schedulelog_data); ?>)</button>
</div>   <div class="col-md-2">
 <input class="allactivitylog-search_timeline" type="text" id="allactivitylog_section" placeholder="Search">
</div> </div> </div>   <div class="card-block" id="allactivitylog_sections_info">
                            </div>              
                    </div>
</div></div>
                <!-- end of all activity logs -->
                <div id="tabs-5">
                  <div class="first-tag">
                    <textarea id="editor23" placeholder="Start typing to leave a note...">
                     
                    </textarea>
                     <span class="createtask_note_errormsg error_msg" style="color: red;"></span>
                    <span class="save-btn"><button class="btn btn-primary create_task_withnotes">create a task</button></span>
                  </div>


                  <div id="notes_section">
                         <div class="card ser-timeline">
                            <div class="card-header">
                            </div>
                            <div class="card-header">
                               <div class="col-12">
                                  <input class="form-control form-control-lg task-search_timeline" type="text" id="tasks_section" placeholder="Search">
                               </div>
                            </div>
                            <div class="card-block">
                               <div class="main-timeline">
                                  <div class="cd-timeline cd-container overall_tasks_section" id="search_for_activity">
                                     <!-- from db data loop start -->
                                     <!-- updated date -->
                                     <?php
                                     $tasks_section_data=$this->db->query("select * from add_new_task where user_id=".$client_id." and timeline_task_id!='0' order by id desc ")->result_array();
                                      foreach ($tasks_section_data as $notes_key => $notes_value) {
                                            $explode_worker=explode(',',$notes_value['worker']);
                                            $explode_team=explode(',',$value['team']);
                                            $explode_department=explode(',',$value['department']);
                                      ?>
                                     <div class="cd-timeline-block tasks_section">
                                        <div class="cd-timeline-icon bg-primary">
                                           <i class="icofont icofont-ui-file"></i>
                                        </div>
                                        <div class="cd-timeline-content card_main">
                                           <div class="p-0">
                                           
                                      

                                      <div class="btn-group dropdown-split-primary">
                                                                            <button type="button" class="btn btn-primary  dropdown-toggle dropdown-toggle-split waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                Actions   
                                                                              </button>
                                                                            <div class="dropdown-menu">
                                                                              <span class="notes_edit"><a href="javascript:void(0);" onclick="return alltask_delete('<?php echo $notes_value['id'];?>');"  >Delete</a></span>
                                                                              <span class="notes_edit"><a href="<?php echo base_url().'user/update_task/'.$notes_value['id'];?>" target="_blank">Edit</a></span>
                                                                            <span class="notes_edit">
                                                                            <a href="javascript:;" id="<?php echo $notes_value['id'];?>" class="archieve_click" onclick="archieve_click(this)" data-toggle="modal" data-target="#my_Modal">
                                                                           Archive</a></span>
                                                                            <span class="notes_edit">
                                                                            <a href="javascript:;" data-toggle="modal" data-target="#adduser_<?php echo $notes_value['id'];?>" class="adduser1 per_assigne_<?php echo $notes_value['id']; ?>">Forward</a>
                                                                            </span>
                                                                            </div>
                                                                        </div>

                                                                  <div class="modal-alertsuccess alert alert-success" id="delete_alltask<?php echo $notes_value['id'];?>" style="display:none;">
                                                                  <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                                                                  <div class="pop-realted1">
                                                                  <div class="position-alert1">
                                                                  Are you sure want to delete <b><a href="<?php echo base_url().'user/t_delete/'.$notes_value['id'];?>"> Yes </a></b> OR <b><a href="#" id="close">No</a></b></div>
                                                                  </div></div> 


                                                                   <div id="adduser_<?php echo $notes_value['id'];?>" class="modal fade" role="dialog">
                                                      <div class="modal-dialog">
                                                         <!-- Modal content-->
                                                         <div class="modal-content">
                                                            <div class="modal-header">
                                                               <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                               <h4 class="modal-title" style="text-align-last: center">Assign to</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                               <!--  <label>assign to </label> -->
                                                               <div class="dropdown-sin-2 lead-form-st">
                                                                  <select multiple placeholder="select" name="workers[]" id="workers<?php echo $notes_value['id'];?>" class="workers">
                                                                   <!--   <?php foreach($staffs as $s_key => $s_val){ ?>
                                                                      <option value="<?php echo $s_val['id'];?>"  <?php if(in_array( $s_val['id'] ,$explode_worker )) {?> selected="selected"<?php } ?> ><?php echo $s_val['crm_name'];?></option>
                                                                     <?php } ?> -->
                                                           <?php if(count($staff_form)){ ?>
                                                                  <option disabled>Staff</option>
                                                                     <?php foreach($staff_form as $s_key => $s_val){ ?>
                                                                      <option value="<?php echo $s_val['id'];?>"  <?php if(in_array( $s_val['id'] ,$explode_worker )) {?> selected="selected"<?php } ?> ><?php echo $s_val['crm_name'];?></option>
                                                                     <?php } 
                                                                     } ?>
                                                              <?php if(count($team)){ ?>
                                                                  <option disabled>Team</option>
                                                                     <?php foreach($team as $s_key => $s_val){ ?>
                                                                      <option value="tm_<?php echo $s_val['id'];?>"  <?php if(in_array( $s_val['id'] ,$explode_team )) {?> selected="selected"<?php } ?> ><?php echo $s_val['team'];?></option>
                                                                     <?php } 
                                                                     } ?>
                                                                  <?php if(count($department)){ ?>
                                                                  <option disabled>Department</option>
                                                                     <?php foreach($department as $s_key => $s_val){ ?>
                                                                      <option value="de_<?php echo $s_val['id'];?>"  <?php if(in_array( $s_val['id'] ,$explode_department )) { ?> selected="selected"<?php } ?> ><?php echo $s_val['new_dept'];?></option>
                                                                     <?php } 
                                                                     } ?>
                                                                  </select>
                                                               </div>
                                                            </div>
                                                            <div class="modal-footer profileEdit">
                                                               <input type="hidden" name="hidden">
 <a href="javascript:;" data-dismiss="modal"  id="acompany_name"  data-id="<?php echo $notes_value['id'];?>" class="save_assign_staff" >save</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                           </div>
                                           <!-- notes delete section -->       
 <div class="modal-alertsuccess alert alert-success" id="delete_notes<?php echo $notes_value['id'];?>" style="display:none;">
         <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
         <div class="pop-realted1">
            <div class="position-alert1">
               Are you sure want to delete <b><a href="javascript:void(0);" class="notes_delete_function" data-id="<?php echo $notes_value['id'];?>"> Yes </a></b> OR <b><a href="#" id="close">No</a></b>
            </div>
         </div>                             
 </div>
 <!-- end of notes delete section -->
                                           <div class="p-20">
                                              <h6><?php echo ($notes_value['subject']); ?></h6>
                                              <div class="timeline-details">
                                                 <!-- <p class="m-t-0"><?php //echo  ?></p> -->
                                                 <?php echo 'Start Date:'.$notes_value['start_date'].'End date:'.$notes_value['end_date'] ?>
                                              </div>
                                           </div>
                                          
                                        </div>
                                     </div>
                                     <?php } ?>
                                     <!-- end of updated date -->
                                    
                                  </div>
                                  <div class="cd-timeline cd-container overall_for_tasks_section" id="search_for_activity" style="display: none;">
                                  </div>
                               </div>
                            </div>
                         </div>
                         <!-- end -->
                      </div>
                </div>
                <div id="tabs-6">
                  <div class="first-tag">
                    <textarea id="editor24" placeholder="Start typing to leave a note...">
                    
                    </textarea>
                    <span class="schedule_note_errormsg error_msg" style="color: red;"></span>
                   <!--  <span class="save-btn sche-clickevent"><button class="btn btn-primary">schedule</button></span> -->
                    <input type="hidden" name="update_schedule_id" id="update_schedule_id">
                    <div class="timeline_button timelineschedule_add" style="float: right;">
                        <input type="button" class="btn btn-primary schedule_section_insert" name="save" id="save" value="Schedule">
                    </div>
                    <div class="timeline_button timelineschedule_update" style="float: right;display: none;">
                        <input type="button" class="btn btn-primary schedule_section_update" name="save" id="save" value="Update Schedule">
                        <input type="button" class="btn btn-primary schedule_section_cancel" data-action="schedule" name="save" id="save" value="Cancel">
                    </div>
                  </div>
                  <!-- for timeline for schedule -->
                  <div id="schedule_section">
                         <div class="card ser-timeline">
                            <div class="card-header">
                            </div>
                            <div class="card-header">
                               <div class="col-12">
                                  <input class="form-control form-control-lg Calllog-search_timeline" type="text" id="schedule_section" placeholder="Search">
                               </div>
                            </div>
                            <div class="card-block">
                               <div class="main-timeline">
                                  <div class="cd-timeline cd-container overall_schedule_section" id="search_for_activity">
                                     <!-- from db data loop start -->
                                     <!-- updated date -->
                                     <?php
                                     $schedulelog_data=$this->db->query("select * from timeline_services_notes_added where user_id=".$client_id." and module='schedule' order by id desc ")->result_array();
                                      foreach ($schedulelog_data as $calllog_key => $calllog_value) {
                                      ?>
                                     <div class="cd-timeline-block schedule_section">
                                        <div class="cd-timeline-icon bg-primary">
                                           <i class="icofont icofont-clock-time"></i>
                                        </div>
                                        <div class="cd-timeline-content card_main">
                                           <div class="p-0">
                                           
                                      

                                      <div class="btn-group dropdown-split-primary">
                                        <button type="button" class="btn btn-primary  dropdown-toggle dropdown-toggle-split waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Actions   
                                          </button>
                                        <div class="dropdown-menu">
                                          <span class="notes_edit"><a href="javascript:void(0);" onclick="return confirm_delete_timeline('<?php echo $calllog_value['id'];?>');"  >Delete</a></span>
                                           <span class="notes_edit"><a href="javascript:void(0);" onclick="topFunction(<?php echo $calllog_value['id']; ?>,'schedule')">Edit</a></span>

                                           <span class="notes_edit"><a href="javascript:void(0);" onclick="archiveFunction(<?php echo $calllog_value['id']; ?>)">Archive</a></span>
                                                                            </div>
                                                                        </div>


                                           </div>
                                           <!-- notes delete section -->       
 <div class="modal-alertsuccess alert alert-success" id="delete_notes<?php echo $calllog_value['id'];?>" style="display:none;">
         <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
         <div class="pop-realted1">
            <div class="position-alert1">
               Are you sure want to delete <b><a href="javascript:void(0);" class="schedule_delete_function" data-action="schedule" data-id="<?php echo $calllog_value['id'];?>"> Yes </a></b> OR <b><a href="#" id="close">No</a></b>
            </div>
         </div>                             
 </div>
 <!-- end of notes delete section -->
                                           <div class="p-20">
                                              <h6><?php echo ($client_details[0]['crm_company_name'])?$client_details[0]['crm_company_name']:$this->Common_mdl->get_crm_name($client_details[0]['user_id']);?> - <?php echo $client_details[0]['crm_legal_form'];?></h6>
                                              <div class="timeline-details">
                                                 <!-- <p class="m-t-0"><?php //echo  ?></p> -->
                                                 <?php if($calllog_value['comments_for_reference']!=''){
                                                 ?><p>Your Schedule was On <b><?php echo date('d-m-Y',strtotime($calllog_value['comments_for_reference'])); ?></b></p><?php 
                                                  } ?>
                                                 
                                                 <br>
                                                 <?php echo $calllog_value['notes']; ?>
                                              </div>
                                           </div>
                                           <span class="cd-date"><?php if(isset($calllog_value['created_time'])){ echo date('F j, Y, g:i a',$calllog_value['created_time']); } ?></span>
                                           <span class="cd-details">Schedule Activity</span>
                                        </div>
                                     </div>
                                     <?php } ?>
                                     <!-- end of updated date -->
                                    
                                  </div>
                                  <div class="cd-timeline cd-container overall_for_schedule_section" id="search_for_activity" style="display: none;">
                                  </div>
                               </div>
                            </div>
                         </div>
                         <!-- end -->
                      </div>
                  <!-- timeline schedule -->
                </div>


              </div>

              <div class="modal fade" id="my_Modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
        <input type="hidden" name="archive_task_id" id="archive_task_id" value="">
          <p>Do you want to archive this task?</p>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default archive_task" >yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

<script type="text/javascript">
  $(document).ready(function(){
      $(document).on("click", ".sche-clickevent", function () {
          $('.visible-schedule').fadeIn(200);
      });
  });

  function alltask_delete(id)
{
   $('#delete_alltask'+id).show();
   return false;
}  

function archieve_click(val){
//  alert('ok');
//  alert($(val).attr('id'));
  $("#archive_task_id").val($(val).attr('id'));
  //alert($("#archive_task_id").val());
  //alert($("#my_Modal").attr('class'));
 $("#my_Modal").modal({backdrop: 'static', keyboard: false,show: true });  
}






</script>