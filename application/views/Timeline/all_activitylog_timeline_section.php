<?php
$filter_array =(!empty( $filter_values ) ?explode(',',$filter_values):[]); 

$services_type_con = ( !empty( $services_type ) ? " AND FIND_IN_SET (services_type,'".$services_type."')" : '' );

?>
<div class="cd-timeline cd-container overall_allactivitylog_section" id="search_for_activity">
<div id="services"> 

<?php 
$calllog_data=$this->db->query("select * from timeline_services_notes_added where user_id=".$client_id." and (module!='') ".$services_type_con." order by id desc ")->result_array();

foreach ($calllog_data as $calllog_key => $calllog_value) 
{ 
$display="display:";
if($filter_values=='' || ( count( $filter_array ) > 0 && in_array( $calllog_value['module'] , $filter_array ) ))
{
//   echo $display."--rspt--".$calllog_value['module'];
?>
<div class="cd-timeline-block allactivitylog_section">

<?php if($calllog_value['module']=='notes'){ ?>
<div class="cd-timeline-icon bg-primary">
<i class="icofont icofont-ui-file"></i>
</div>
<?php } ?>

<?php if($calllog_value['module']=='calllog'){ ?>
<div class="cd-timeline-icon bg-primary">
<i class="icofont icofont-phone-circle"></i>
</div>
<?php } ?>

<?php if($calllog_value['module']=='logactivity'){ ?>
<div class="cd-timeline-icon bg-primary">
<i class="icofont icofont-ui-file"></i>
</div>
<?php } ?>

<?php if($calllog_value['module']=='email'){ ?>
<div class="cd-timeline-icon bg-primary">
<i class="icofont icofont-email"></i>
</div>
<?php } ?>

<?php if($calllog_value['module']=='newtask'){ ?>
<div class="cd-timeline-icon bg-primary">
<i class="icofont icofont-notepad"></i>
</div>
<?php } ?>

<?php if($calllog_value['module']=='schedule'){ ?>
<div class="cd-timeline-icon bg-primary">
<i class="icofont icofont-clock-time"></i>
</div>
<?php } ?>

<?php if($calllog_value['module']=='smslog'){ ?>
<div class="cd-timeline-icon bg-primary">
<i class="fa fa-envelope" aria-hidden="true"></i>
</div>
<?php } ?>

<?php if($calllog_value['module']=='meeting'){ ?>
<div class="cd-timeline-icon bg-primary">
<i class="fa fa-handshake-o" aria-hidden="true"></i>
</div>
<?php } ?>

<div class="cd-timeline-content card_main">
<div class="p-0">
<?php if($_SESSION['permission']['Client_Section']['edit'] == '1' || $_SESSION['permission']['Client_Section']['delete'] == '1'){ ?>
<div class="btn-group dropdown-split-primary">
<button type="button" class="btn btn-primary  dropdown-toggle dropdown-toggle-split waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</button>
<div class="dropdown-menu">
<?php if($_SESSION['permission']['Client_Section']['delete'] == '1'){ ?>
<span class="notes_edit">
<a href="javascript:void(0);" data-id="<?php echo $calllog_value['id'];?>" class="confirm_delete_timeline">Delete</a>
</span>
<?php } ?>

<?php if($_SESSION['permission']['Client_Section']['edit'] == '1'){ ?>
<span class="notes_edit"><a href="javascript:void(0);" onclick="topFunction('<?php echo $calllog_value['id']; ?>','<?php echo $calllog_value['module'];?>')">Edit</a>
</span> 

<span class="notes_edit"><a href="javascript:void(0);" onclick="archiveFunction('<?php echo $calllog_value['id']; ?>')">Archive</a>
</span>

<?php } ?>
</div>
</div>
<?php } ?>
</div>

<div class="p-20">
<h6><?php echo $client_details[0]['crm_company_name'] ."-". $client_details[0]['crm_legal_form'];?></h6>
<div class="timeline-details">
<!-- <p class="m-t-0"><?php //echo  ?></p> -->
<?php if($calllog_value['module']=='schedule' && $calllog_value['comments_for_reference']!='')
{
?>
<p>Your Schedule was On <b><?php echo date('d-m-Y',strtotime($calllog_value['comments_for_reference'])); ?></b>
</p>
<?php } ?>    
<br>
<?php echo $calllog_value['notes']; ?>

<?php if($_SESSION['permission']['Task']['create'] == '1'){ ?>
<div class="timeline-Task-div">
<a href="<?php echo base_url(); ?>user/new_task/<?php echo $calllog_value['id']; ?>" class="btn btn-primary" target="_blank">Create Task</a>
</div>
<?php } ?>
</div>
</div>

<span class="cd-date"><?php if(isset($calllog_value['created_time'])){ echo date('F j, Y, g:i a',$calllog_value['created_time']); } ?></span>
<!-- <span class="cd-details">log Activity</span>-->
<?php if($calllog_value['module']=='notes'){ ?>
<span class="cd-details">Notes For <?php echo $calllog_value['services_type']; ?></span>
<?php } ?>
<?php if($calllog_value['module']=='calllog'){ ?>
<span class="cd-details">Call log Activity</span>
<?php } ?>
<?php if($calllog_value['module']=='logactivity'){ ?>
<span class="cd-details">log Activity</span>
<?php } ?>
<?php if($calllog_value['module']=='email'){ ?>
<span class="cd-details">Email Notes Activity</span>
<?php } ?>
<?php if($calllog_value['module']=='newtask'){ ?>
<span class="cd-details">Task Notes Activity</span>
<?php } ?>
<?php if($calllog_value['module']=='schedule'){ ?>
<span class="cd-details">Schedule Activity</span>
<?php } ?>
<?php if($calllog_value['module']=='smslog'){ ?>
<span class="cd-details">Sms Activity</span>
<?php } ?>
<?php if($calllog_value['module']=='meeting'){ ?>
<span class="cd-details">Meeting Activity</span>
<?php } ?>
</div>
</div>
<?php } 
} // condition
?>
<!-- end of updated date -->
<!-- client timelien services section -->
<?php 
/* if($filter_values=='')
{
$client_services = array("allactivitylog_section"=>"ALL");

$services = $this->Common_mdl->getClientServices( $client_id ); 
$firm_enabled_services = array();
function firm_enabled($val)
{          
return $val['service_name'];
}

$firm_enabled_services = array_map('firm_enabled',$services); 
}
else
{   
$client_services = $this->Common_mdl->getClientServices($client_id);
}
*/


if( $filter_values=='' )
{
$service_ids = array_keys( $this->Common_mdl->get_FirmEnabled_Services() );

$service_type_con = '';
if( !empty( $services_type ) )
{

$services_type = explode( "," , $services_type );

$service_type_con = "'".implode("','", $services_type )."'";
$service_type_con = " AND service_name IN (".$service_type_con.") ";
}
    

$services    = $this->db->query("SELECT * FROM service_lists WHERE id IN (". implode(',', array_filter( $service_ids ) ) .") ".$service_type_con)->result_array();
$services    = array_column( $services , 'service_name' );

}
           
$key="allactivitylog_section";
$getCompanyvalue = $client_details[0];
if( in_array("VAT", $services ) )
{


$getusername = $this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);

(isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = '';

if($jsnvat!='' && $getCompanyvalue['crm_vat_due_date']!='')
{  

$activity_log = $this->db->query("select * from activity_log where module='client' and module_id=".$getCompanyvalue['user_id']." and sub_module='VAT' order by id desc limit 1 ")->row_array();
$due=explode("-",$getCompanyvalue['crm_vat_due_date']);
$mkdue=mktime(0,0,0,$due[1],$due[2],$due[0]);
?>
<div class="cd-timeline cd-container <?php echo $key; ?> sorting" id="<?=$mkdue?>" data-id="rsptrspt">
<div class="cd-timeline-block <?php echo $key; ?>">
<div class="cd-timeline-icon bg-primary">
<i class="icofont icofont-ui-file"></i>
</div>
<div class="cd-timeline-content card_main">
<div class="p-20">
<h6><?php echo $getusername['crm_name'];?> - <?php echo $getCompanyvalue['crm_legal_form'];?></h6>
<div class="timeline-details">
<a href="#">
<i class="icofont icofont-ui-calendar"></i>
<span>Due Date : <?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_vat_due_date'])): ''); ?></span>
</a>
<!-- shown created user name -->
<a href="#">
<i class="icofont icofont-ui-user"></i>
<span>
<?php   
if(count($activity_log)>0)
{
echo "Updated: ".($activity_log['user_id'] !='' ?  $this->Common_mdl->get_field_value('user','crm_name','id',$activity_log['user_id']): ''); 
}
?>
</span>
</a>
<!--end of created user name 25-08-2018-->
<p class="m-t-0">VAT Number : <?php echo $getCompanyvalue['crm_vat_number'];?></p>
<p class="m-t-0">VAT Registration Date : <?php echo (strtotime($getCompanyvalue['crm_vat_date_of_registration']) !='' ?  date('F j, Y, g:i', strtotime($getCompanyvalue['crm_vat_date_of_registration'])): ''); ?>              
</p>
<!-- for notes section -->
<p class="m-t-0">
Notes:<?php echo (isset($getCompanyvalue['crm_vat_notes']) && $getCompanyvalue['crm_vat_notes']!='0')?$getCompanyvalue['crm_vat_notes']:'-';  ?>
</p>
<!-- end of notes -->

<!-- for services updated from activity log section -->
<p class="m-t-0">
<?php 

if(count($activity_log)>0)
{
echo "Updated Date:".($activity_log['CreatedTime'] !='' ?  date('F j, Y, g:i', $activity_log['CreatedTime']): ''); 
}
else
{
if(isset($getCompanyvalue['created_date'])){ echo "Updated Date:".date('F j, Y, g:i',$getCompanyvalue['created_date']); }  
}
?>
</p>
<!-- end of activity log -->
<!-- for notifiaction -->
<p class="m-t-20">
<p>
<label>Services :</label><?php   (isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = '';
($jsnvat!='') ? $vat = "On" : $vat = "Off"; echo $vat;?></p>
<p><label>Reminder :</label><?php   (isset(json_decode($getCompanyvalue['vat'])->reminder) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->reminder : $jsnvat = '';
($jsnvat!='') ? $vat = "On" : $vat = "Off"; echo $vat; ?></p>
<p><label>Text :</label><?php   (isset(json_decode($getCompanyvalue['vat'])->text) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->text : $jsnvat = '';
($jsnvat!='') ? $vat = "On" : $vat = "Off"; echo $vat;?></p>
<p><label>Invoice :</label><?php  echo (isset(json_decode($getCompanyvalue['vat'])->invoice) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->invoice : $jsnvat = '';
($jsnvat!='') ? $vat = "On" : $vat = "Off"; echo $vat;?></p>
</p>
<!-- end of notification -->
</div>
</div>
<span class="cd-date">
<?php
if(count($activity_log)>0)
{

echo ($activity_log['CreatedTime'] !='' ?  date('F j, Y, g:i', $activity_log['CreatedTime']): '');
}
else
{
if(isset($getCompanyvalue['created_date']))
{
echo date('F j, Y, g:i',$getCompanyvalue['created_date']); 
} 
}
?>
</span>
<span class="cd-details"><?php echo $getCompanyvalue['crm_company_name'];?> Vat Info </span>
</div>
</div>
</div>

<?php
} // services condition

?><?php
}









if( in_array('Payroll', $services ) )
{
// $value_val="";
// foreach ($client_details as $getCompanykey => $getCompanyvalue) {
(isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpayroll =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpayroll = '';
if($jsnpayroll!='' && $getCompanyvalue['crm_rti_deadline']!='')
{
$activity_log=$this->db->query("select * from activity_log where module='client' and module_id=".$getCompanyvalue['user_id']." and sub_module='Payroll' order by id desc limit 1 ")->row_array();
$getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
$due=explode("-",$getCompanyvalue['crm_rti_deadline']);
$mkdue=mktime(0,0,0,$due[1],$due[2],$due[0]);
?>
<div class="cd-timeline cd-container <?php echo $key; ?> sorting" id="<?=$mkdue?>" data-id="rsptrspt" >

<div class="cd-timeline-block <?php echo $key; ?>">
<div class="cd-timeline-icon bg-primary">
<i class="icofont icofont-ui-file"></i>
</div>
<div class="cd-timeline-content card_main">
<div class="p-20">
<h6><?php echo $getusername['crm_name'];?> - <?php echo $getCompanyvalue['crm_legal_form'];?></h6>
<div class="timeline-details">
<a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_rti_deadline'])): ''); ?></span> </a>
<!-- shown created user name -->
<a href="#">
<i class="icofont icofont-ui-user"></i><span>
<?php 

if(count($activity_log)>0)
{
?>
Updated:<?php echo ($activity_log['user_id'] !='' ?  $this->Common_mdl->get_field_value('user','crm_name','id',$activity_log['user_id']): ''); ?>
<?php }?>
</span>
</a>
<!--end of created user name 25-08-2018-->
<p class="m-t-0"> First Pay Date: <?php echo (strtotime($getCompanyvalue['crm_first_pay_date']) !='' ?  date('F j, Y, g:i', strtotime($getCompanyvalue['crm_first_pay_date'])): ''); ?></p>
<p class="m-t-0">Payroll Run Date : <?php echo (strtotime($getCompanyvalue['crm_payroll_run_date']) !='' ?  date('F j, Y, g:i', strtotime($getCompanyvalue['crm_payroll_run_date'])): ''); ?></p>
<!-- for notes section -->

<!-- end of notes -->
<!-- for services updated from activity log section -->
<p class="m-t-0">
<?php 

if(count($activity_log)>0)
{
?>
Updated Date:<?php echo ($activity_log['CreatedTime'] !='' ?  date('F j, Y, g:i', $activity_log['CreatedTime']): ''); ?>
<?php }?>
</p>
<!-- end of activity log -->
<!-- for notifications -->
<p class="m-t-20">
<p><label>Services :</label><?php   (isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpayroll =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpayroll = '';
($jsnpayroll!='') ? $payroll = "On" : $payroll = "Off"; echo $payroll;?></p>
<p><label>Reminder :</label><?php   (isset(json_decode($getCompanyvalue['payroll'])->reminder) && $getCompanyvalue['payroll'] != '') ? $jsnpayroll =  json_decode($getCompanyvalue['payroll'])->reminder : $jsnpayroll = '';
($jsnpayroll!='') ? $payroll = "On" : $payroll = "Off"; echo $payroll; ?></p>
<p><label>Text :</label><?php   (isset(json_decode($getCompanyvalue['payroll'])->text) && $getCompanyvalue['payroll'] != '') ? $jsnpayroll =  json_decode($getCompanyvalue['payroll'])->text : $jsnpayroll = '';
($jsnpayroll!='') ? $payroll = "On" : $payroll = "Off"; echo $payroll;?></p>
<p><label>Invoice :</label><?php  echo (isset(json_decode($getCompanyvalue['payroll'])->invoice) && $getCompanyvalue['payroll'] != '') ? $jsnpayroll =  json_decode($getCompanyvalue['payroll'])->invoice : $jsnpayroll = '';
($jsnpayroll!='') ? $payroll = "On" : $payroll = "Off"; echo $payroll;?></p>
</p>
<!-- end of notification -->
</div>
</div>
<span class="cd-date"><?php //if(isset($getusername['CreatedTime'])){ echo date('F j, Y, g:i',$getusername['CreatedTime']); } 
if(count($activity_log)>0)
{
?>
<?php echo ($activity_log['CreatedTime'] !='' ?  date('F j, Y, g:i', $activity_log['CreatedTime']): ''); ?>
<?php }else{
?>
<?php if(isset($getCompanyvalue['created_date'])){ echo date('F j, Y, g:i',$getCompanyvalue['created_date']); } 
}?></span>
<span class="cd-details"><?php echo $getCompanyvalue['crm_company_name'];?> Payroll Info </span>
</div>
</div></div>
<?php
}// services tab check
}





if( in_array('Accounts', $services) )
{
(isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnaccounts =  json_decode($getCompanyvalue['accounts'])->tab : $jsnaccounts = '';
if($jsnaccounts!='' && $getCompanyvalue['crm_ch_accounts_next_due']!=''){

$activity_log=$this->db->query("select * from activity_log where module='client' and module_id=".$getCompanyvalue['user_id']." and sub_module='Accounts' order by id desc limit 1 ")->row_array();

$getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);

$due=explode("-",$getCompanyvalue['crm_ch_accounts_next_due']);
$mkdue=mktime(0,0,0,$due[1],$due[2],$due[0]);
?>
<div class="cd-timeline cd-container <?php echo $key; ?> sorting" id="<?=$mkdue?>" data-id="rsptrspt" >
<div class="cd-timeline-block <?php echo $key; ?>">
<div class="cd-timeline-icon bg-primary">
<i class="icofont icofont-ui-file"></i>
</div>
<div class="cd-timeline-content card_main">
<div class="p-20">
<h6><?php echo $getusername['crm_name'];?> - <?php echo $getCompanyvalue['crm_legal_form'];?></h6>
<div class="timeline-details">
<a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])): ''); ?></span> </a>
<!-- shown created user name -->
<a href="#">
<i class="icofont icofont-ui-user"></i><span>
<?php 

if(count($activity_log)>0)
{
  ?>
  Updated:<?php echo ($activity_log['user_id'] !='' ?  $this->Common_mdl->get_field_value('user','crm_name','id',$activity_log['user_id']): ''); 
  ?>
<?php } ?>
</span>
</a>
<!--end of created user name 25-08-2018-->
<p class="m-t-0"> Next Account Madeup: <?php echo (strtotime($getCompanyvalue['crm_ch_yearend']) !='' ?  date('F j, Y, g:i', strtotime($getCompanyvalue['crm_ch_yearend'])): ''); ?></p>
 <!-- for notes section -->
<p class="m-t-0">
  Notes:<?php echo (isset($getCompanyvalue['crm_accounts_notes']) && $getCompanyvalue['crm_accounts_notes']!='0')?$getCompanyvalue['crm_accounts_notes']:'-';  ?>
</p>
<!-- end of notes -->
<!-- for services updated from activity log section -->
<p class="m-t-0">
<?php 

if(count($activity_log)>0)
{
?>
Updated Date:<?php echo ($activity_log['CreatedTime'] !='' ?  date('F j, Y, g:i', $activity_log['CreatedTime']): ''); ?>
<?php } ?>
</p>
<!-- end of activity log -->
<!-- for notifications -->
<p class="m-t-20">
<p><label>Services :</label><?php   (isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnaccounts =  json_decode($getCompanyvalue['accounts'])->tab : $jsnaccounts = '';
  ($jsnaccounts!='') ? $accounts = "On" : $accounts = "Off"; echo $accounts;?></p>
<p><label>Reminder :</label><?php   (isset(json_decode($getCompanyvalue['accounts'])->reminder) && $getCompanyvalue['accounts'] != '') ? $jsnaccounts =  json_decode($getCompanyvalue['accounts'])->reminder : $jsnaccounts = '';
  ($jsnaccounts!='') ? $accounts = "On" : $accounts = "Off"; echo $accounts; ?></p>
<p><label>Text :</label><?php   (isset(json_decode($getCompanyvalue['accounts'])->text) && $getCompanyvalue['accounts'] != '') ? $jsnaccounts =  json_decode($getCompanyvalue['accounts'])->text : $jsnaccounts = '';
  ($jsnaccounts!='') ? $accounts = "On" : $accounts = "Off"; echo $accounts;?></p>
<p><label>Invoice :</label><?php  echo (isset(json_decode($getCompanyvalue['accounts'])->invoice) && $getCompanyvalue['accounts'] != '') ? $jsnaccounts =  json_decode($getCompanyvalue['accounts'])->invoice : $jsnaccounts = '';
  ($jsnaccounts!='') ? $accounts = "On" : $accounts = "Off"; echo $accounts;?></p>
</p>
<!-- end of notification -->
</div>
</div>
<span class="cd-date"><?php //if(isset($getusername['CreatedTime'])){ echo date('F j, Y, g:i',$getusername['CreatedTime']); } 
if(count($activity_log)>0)
{
?>
<?php echo ($activity_log['CreatedTime'] !='' ?  date('F j, Y, g:i', $activity_log['CreatedTime']): ''); ?>
<?php }else{
?>
<?php if(isset($getCompanyvalue['created_date'])){ echo date('F j, Y, g:i',$getCompanyvalue['created_date']); } 
}?></span>
<span class="cd-details"><?php echo $getCompanyvalue['crm_company_name'];?> Account Info </span>
</div>
</div></div>
<?php
} // services check

}



if( in_array('Confirmation statement', $services) )
{
// foreach ($client_details as $getCompanykey => $getCompanyvalue) { 
(isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnconf_statement =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnconf_statement = '';

if($jsnconf_statement!='' && $getCompanyvalue['crm_confirmation_statement_due_date']!='')
{
$activity_log=$this->db->query("select * from activity_log where module='client' and module_id=".$getCompanyvalue['user_id']." and sub_module='Confirmation statement' order by id desc limit 1 ")->row_array();

$getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
$due=explode("-",$getCompanyvalue['crm_confirmation_statement_due_date']);
$mkdue=mktime(0,0,0,$due[1],$due[2],$due[0]);
?>
<div class="cd-timeline cd-container <?php echo $key; ?> sorting" id="<?=$mkdue?>" data-id="rsptrspt" >
<div class="cd-timeline-block <?php echo $key; ?>">
<div class="cd-timeline-icon bg-primary">
<i class="icofont icofont-ui-file"></i>
</div>
<div class="cd-timeline-content card_main">
<div class="p-20">
<h6><?php echo $getusername['crm_name'];?> - <?php echo $getCompanyvalue['crm_legal_form'];?></h6>
<div class="timeline-details">
<a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])): ''); ?></span> </a>
<!-- shown created user name -->
<a href="#">
<i class="icofont icofont-ui-user"></i><span>
<?php 

if(count($activity_log)>0)
{

?>Updated:<?php echo ($activity_log['user_id'] !='' ?  $this->Common_mdl->get_field_value('user','crm_name','id',$activity_log['user_id']): ''); ?>
<?php }
?>
</span>
</a>
<!--end of created user name 25-08-2018-->
<p class="m-t-0"> Next Statement Date: <?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_date']) !='' ?  date('F j, Y, g:i', strtotime($getCompanyvalue['crm_confirmation_statement_date'])): ''); ?></p>
<!-- for notes section -->
<p class="m-t-0">
  Notes:<?php echo (isset($getCompanyvalue['crm_confirmation_notes']) && $getCompanyvalue['crm_confirmation_notes']!='0')?$getCompanyvalue['crm_confirmation_notes']:'-';  ?>
</p>
<!-- end of notes -->
<!-- for services updated from activity log section -->
<p class="m-t-0">
<?php 
$activity_log=$this->db->query("select * from activity_log where module='client' and module_id=".$getCompanyvalue['user_id']." and sub_module='".$value_val."' order by id desc limit 1 ")->row_array();
if(count($activity_log)>0)
{
?>
Updated Date:<?php echo ($activity_log['CreatedTime'] !='' ?  date('F j, Y, g:i', $activity_log['CreatedTime']): ''); ?>
<?php }else{
?>
Updated Date:<?php if(isset($getCompanyvalue['created_date'])){ echo date('F j, Y, g:i',$getCompanyvalue['created_date']); } 
} ?>
</p>
<!-- end of activity log -->
<!-- for notifications -->
<p class="m-t-20">
<p><label>Services :</label><?php   (isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnconf_statement =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnconf_statement = '';
  ($jsnconf_statement!='') ? $conf_statement = "On" : $conf_statement = "Off"; echo $conf_statement;?></p>
<p><label>Reminder :</label><?php   (isset(json_decode($getCompanyvalue['conf_statement'])->reminder) && $getCompanyvalue['conf_statement'] != '') ? $jsnconf_statement =  json_decode($getCompanyvalue['conf_statement'])->reminder : $jsnconf_statement = '';
  ($jsnconf_statement!='') ? $conf_statement = "On" : $conf_statement = "Off"; echo $conf_statement; ?></p>
<p><label>Text :</label><?php   (isset(json_decode($getCompanyvalue['conf_statement'])->text) && $getCompanyvalue['conf_statement'] != '') ? $jsnconf_statement =  json_decode($getCompanyvalue['conf_statement'])->text : $jsnconf_statement = '';
  ($jsnconf_statement!='') ? $conf_statement = "On" : $conf_statement = "Off"; echo $conf_statement;?></p>
<p><label>Invoice :</label><?php  echo (isset(json_decode($getCompanyvalue['conf_statement'])->invoice) && $getCompanyvalue['conf_statement'] != '') ? $jsnconf_statement =  json_decode($getCompanyvalue['conf_statement'])->invoice : $jsnconf_statement = '';
  ($jsnconf_statement!='') ? $conf_statement = "On" : $conf_statement = "Off"; echo $conf_statement;?></p>
</p>
<!-- end of notification -->
</div>
</div>
<span class="cd-date"><?php //if(isset($getusername['CreatedTime'])){ echo date('F j, Y, g:i',$getusername['CreatedTime']); } 
if(count($activity_log)>0)
{
?>
<?php echo ($activity_log['CreatedTime'] !='' ?  date('F j, Y, g:i', $activity_log['CreatedTime']): ''); ?>
<?php }else{
?>
<?php if(isset($getCompanyvalue['created_date'])){ echo date('F j, Y, g:i',$getCompanyvalue['created_date']); } 
}
?></span>
<span class="cd-details"><?php echo $getCompanyvalue['crm_company_name'];?> Confirmation statement Info </span>
</div>
</div></div>
<?php } // services check
// }
}




if( in_array( 'Company Tax Return', $services ) )
{
 
(isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsncompany_tax_return =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsncompany_tax_return = '';
if($jsncompany_tax_return!='' && $getCompanyvalue['crm_accounts_due_date_hmrc']!=''){

$activity_log=$this->db->query("select * from activity_log where module='client' and module_id=".$getCompanyvalue['user_id']." and sub_module='Company Tax Return' order by id desc limit 1 ")->row_array();

$getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
$due=explode("-",$getCompanyvalue['crm_accounts_due_date_hmrc']);
$mkdue=mktime(0,0,0,$due[1],$due[2],$due[0]);
?>
<div class="cd-timeline cd-container <?php echo $key; ?> sorting" id="<?=$mkdue?>" data-id="rsptrspt" >
<div class="cd-timeline-block <?php echo $key; ?>">
<div class="cd-timeline-icon bg-primary">
<i class="icofont icofont-ui-file"></i>
</div>
<div class="cd-timeline-content card_main">
<div class="p-20">
<h6><?php echo $getusername['crm_name'];?> - <?php echo $getCompanyvalue['crm_legal_form'];?></h6>
<div class="timeline-details">
<a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php echo (strtotime($getCompanyvalue['crm_accounts_due_date_hmrc']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_accounts_due_date_hmrc'])): ''); ?></span> </a>
<!--  <a href="#">
  <i class="icofont icofont-ui-user"></i><span>John Doe</span>crm_vat_date_of_registration
  </a> -->
<!-- shown created user name -->
<a href="#">
<i class="icofont icofont-ui-user"></i><span>
<?php 

if(count($activity_log)>0)
{

?>Updated:<?php echo ($activity_log['user_id'] !='' ?  $this->Common_mdl->get_field_value('user','crm_name','id',$activity_log['user_id']): ''); ?>
<?php } ?>

</span>
</a>
<!--end of created user name 25-08-2018-->
<p class="m-t-0"> Tax Payment Date to HMRC: <?php echo (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) !='' ?  date('F j, Y, g:i', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])): ''); ?></p>
<!-- for notifications -->
<!-- for services updated from activity log section -->
<p class="m-t-0">
<?php 


if(count($activity_log)>0)
{
?>
Updated Date:<?php echo ($activity_log['CreatedTime'] !='' ?  date('F j, Y, g:i', $activity_log['CreatedTime']): ''); ?>
<?php }else{
?>
Updated Date:<?php if(isset($getCompanyvalue['created_date'])){ echo date('F j, Y, g:i',$getCompanyvalue['created_date']); }
} ?>
</p>
<!-- end of activity log -->
<p class="m-t-20">
<p><label>Services :</label><?php   (isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsncompany_tax_return =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsncompany_tax_return = '';
  ($jsncompany_tax_return!='') ? $company_tax_return = "On" : $company_tax_return = "Off"; echo $company_tax_return;?></p>
<p><label>Reminder :</label><?php   (isset(json_decode($getCompanyvalue['company_tax_return'])->reminder) && $getCompanyvalue['company_tax_return'] != '') ? $jsncompany_tax_return =  json_decode($getCompanyvalue['company_tax_return'])->reminder : $jsncompany_tax_return = '';
  ($jsncompany_tax_return!='') ? $company_tax_return = "On" : $company_tax_return = "Off"; echo $company_tax_return; ?></p>
<p><label>Text :</label><?php   (isset(json_decode($getCompanyvalue['company_tax_return'])->text) && $getCompanyvalue['company_tax_return'] != '') ? $jsncompany_tax_return =  json_decode($getCompanyvalue['company_tax_return'])->text : $jsncompany_tax_return = '';
  ($jsncompany_tax_return!='') ? $company_tax_return = "On" : $company_tax_return = "Off"; echo $company_tax_return;?></p>
<p><label>Invoice :</label><?php  echo (isset(json_decode($getCompanyvalue['company_tax_return'])->invoice) && $getCompanyvalue['company_tax_return'] != '') ? $jsncompany_tax_return =  json_decode($getCompanyvalue['company_tax_return'])->invoice : $jsncompany_tax_return = '';
  ($jsncompany_tax_return!='') ? $company_tax_return = "On" : $company_tax_return = "Off"; echo $company_tax_return;?></p>
</p>
<!-- end of notification -->
</div>
</div>
<span class="cd-date"><?php //if(isset($getusername['CreatedTime'])){ echo date('F j, Y, g:i',$getusername['CreatedTime']); } 
if(count($activity_log)>0)
{
?>
<?php echo ($activity_log['CreatedTime'] !='' ?  date('F j, Y, g:i', $activity_log['CreatedTime']): ''); ?>
<?php }else{
?>
<?php if(isset($getCompanyvalue['created_date'])){ echo date('F j, Y, g:i',$getCompanyvalue['created_date']); } 
}?></span>
<span class="cd-details"><?php echo $getCompanyvalue['crm_company_name'];?> Company Tax Return Info </span>
</div>
</div></div>
<?php } // services check

}


if( in_array('Personal Tax Return', $services) )
{

$activity_log=$this->db->query("select * from activity_log where module='client' and module_id=".$getCompanyvalue['user_id']." and sub_module='Personal Tax Return' order by id desc limit 1 ")->row_array();
 
(isset(json_decode($getCompanyvalue['personal_tax_return'])->tab) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnpersonal_tax_return =  json_decode($getCompanyvalue['personal_tax_return'])->tab : $jsnpersonal_tax_return = '';
if($jsnpersonal_tax_return!='' && $getCompanyvalue['crm_personal_due_date_return']!=''){

$getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
$due=explode("-",$getCompanyvalue['crm_personal_due_date_return']);
$mkdue=mktime(0,0,0,$due[1],$due[2],$due[0]);
?>
<div class="cd-timeline cd-container <?php echo $key; ?> sorting" id="<?=$mkdue?>" data-id="rsptrspt" >
<div class="cd-timeline-block <?php echo $key; ?>">
<div class="cd-timeline-icon bg-primary">
<i class="icofont icofont-ui-file"></i>
</div>
<div class="cd-timeline-content card_main">
<div class="p-20">
<h6><?php echo $getusername['crm_name'];?> - <?php echo $getCompanyvalue['crm_legal_form'];?></h6>
<div class="timeline-details">
<a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_personal_due_date_return'])): ''); ?></span> </a>
<!--  <a href="#">
  <i class="icofont icofont-ui-user"></i><span>John Doe</span>crm_vat_date_of_registration
  </a> -->
<!-- shown created user name -->
<a href="#">
<i class="icofont icofont-ui-user"></i><span>
<?php 

if(count($activity_log)>0)
{

?>Updated:<?php echo ($activity_log['user_id'] !='' ?  $this->Common_mdl->get_field_value('user','crm_name','id',$activity_log['user_id']): ''); ?>
<?php
}
?>
</span>
</a>
<!--end of created user name 25-08-2018-->
<p class="m-t-0"> Tax Return Date: <?php echo (strtotime($getCompanyvalue['crm_personal_tax_return_date']) !='' ?  date('F j, Y, g:i', strtotime($getCompanyvalue['crm_personal_tax_return_date'])): ''); ?></p>
<p class="m-t-0"> Due Date online filing: <?php echo (strtotime($getCompanyvalue['crm_personal_due_date_online']) !='' ?  date('F j, Y, g:i', strtotime($getCompanyvalue['crm_personal_due_date_online'])): ''); ?></p>
<!-- for notes section -->
<p class="m-t-0">
  Notes:<?php echo (isset($getCompanyvalue['crm_personal_notes']) && $getCompanyvalue['crm_personal_notes']!='0')?$getCompanyvalue['crm_personal_notes']:'-';  ?>
</p>
<!-- end of notes -->
<!-- for services updated from activity log section -->
<p class="m-t-0">
<?php 
$activity_log=$this->db->query("select * from activity_log where module='client' and module_id=".$getCompanyvalue['user_id']." and sub_module='".$value_val."' order by id desc limit 1 ")->row_array();
if(count($activity_log)>0)
{
?>
Updated Date:<?php echo ($activity_log['CreatedTime'] !='' ?  date('F j, Y, g:i', $activity_log['CreatedTime']): ''); ?>
<?php }else{
?>
Updated Date:<?php if(isset($getCompanyvalue['created_date'])){ echo date('F j, Y, g:i',$getCompanyvalue['created_date']); }
} ?>
</p>
<!-- end of activity log -->
<!-- for notifications -->
<p class="m-t-20">
<p><label>Services :</label><?php   (isset(json_decode($getCompanyvalue['personal_tax_return'])->tab) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnpersonal_tax_return =  json_decode($getCompanyvalue['personal_tax_return'])->tab : $jsnpersonal_tax_return = '';
  ($jsnpersonal_tax_return!='') ? $personal_tax_return = "On" : $personal_tax_return = "Off"; echo $personal_tax_return;?></p>
<p><label>Reminder :</label><?php   (isset(json_decode($getCompanyvalue['personal_tax_return'])->reminder) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnpersonal_tax_return =  json_decode($getCompanyvalue['personal_tax_return'])->reminder : $jsnpersonal_tax_return = '';
  ($jsnpersonal_tax_return!='') ? $personal_tax_return = "On" : $personal_tax_return = "Off"; echo $personal_tax_return; ?></p>
<p><label>Text :</label><?php   (isset(json_decode($getCompanyvalue['personal_tax_return'])->text) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnpersonal_tax_return =  json_decode($getCompanyvalue['personal_tax_return'])->text : $jsnpersonal_tax_return = '';
  ($jsnpersonal_tax_return!='') ? $personal_tax_return = "On" : $personal_tax_return = "Off"; echo $personal_tax_return;?></p>
<p><label>Invoice :</label><?php  echo (isset(json_decode($getCompanyvalue['personal_tax_return'])->invoice) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnpersonal_tax_return =  json_decode($getCompanyvalue['personal_tax_return'])->invoice : $jsnpersonal_tax_return = '';
  ($jsnpersonal_tax_return!='') ? $personal_tax_return = "On" : $personal_tax_return = "Off"; echo $personal_tax_return;?></p>
</p>
<!-- end of notification -->
</div>
</div>
<span class="cd-date"><?php //if(isset($getusername['CreatedTime'])){ echo date('F j, Y, g:i',$getusername['CreatedTime']); }
if(count($activity_log)>0)
{
?>
<?php echo ($activity_log['CreatedTime'] !='' ?  date('F j, Y, g:i', $activity_log['CreatedTime']): ''); ?>
<?php }else{
?>
<?php if(isset($getCompanyvalue['created_date'])){ echo date('F j, Y, g:i',$getCompanyvalue['created_date']); } 
} ?></span>
<span class="cd-details"><?php echo $getCompanyvalue['crm_company_name'];?> Personal Tax Return Info </span>
</div>
</div></div>
<?php } // services check

}


if(in_array('WorkPlace Pension - AE', $services))
{
 
(isset(json_decode($getCompanyvalue['workplace'])->tab) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->tab : $jsnworkplace = '';
if($jsnworkplace!='' && $getCompanyvalue['crm_pension_subm_due_date']!=''){
$activity_log=$this->db->query("select * from activity_log where module='client' and module_id=".$getCompanyvalue['user_id']." and sub_module='WorkPlace Pension - AE' order by id desc limit 1 ")->row_array();
$getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
$due=explode("-",$getCompanyvalue['crm_pension_subm_due_date']);
$mkdue=mktime(0,0,0,$due[1],$due[2],$due[0]);
?>
<div class="cd-timeline cd-container <?php echo $key; ?> sorting" id="<?=$mkdue?>" data-id="rsptrspt" >
<div class="cd-timeline-block <?php echo $key; ?>">
<div class="cd-timeline-icon bg-primary">
<i class="icofont icofont-ui-file"></i>
</div>
<div class="cd-timeline-content card_main">
<div class="p-20">
<h6><?php echo $getusername['crm_name'];?> - <?php echo $getCompanyvalue['crm_legal_form'];?></h6>
<div class="timeline-details">
<a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_pension_subm_due_date'])): ''); ?></span> </a>
<!-- shown created user name -->
<a href="#">
<i class="icofont icofont-ui-user"></i><span>
<?php 

if(count($activity_log)>0)
{

?>Updated:<?php echo ($activity_log['user_id'] !='' ?  $this->Common_mdl->get_field_value('user','crm_name','id',$activity_log['user_id']): ''); ?>
<?php }?>
</span>
</a>
<!--end of created user name 25-08-2018-->
<p class="m-t-0"> Staging Date: <?php echo (strtotime($getCompanyvalue['crm_staging_date']) !='' ?  date('F j, Y, g:i', strtotime($getCompanyvalue['crm_staging_date'])): ''); ?></p>
<p class="m-t-0"> Pension ID: <?php echo ($getCompanyvalue['crm_pension_id'] !='' ? $getCompanyvalue['crm_pension_id']: '-'); ?></p>
<p class="m-t-0"> Declaration of Compliance Due: <?php echo (strtotime($getCompanyvalue['crm_declaration_of_compliance_due_date']) !='' ?  date('F j, Y, g:i', strtotime($getCompanyvalue['crm_declaration_of_compliance_due_date'])): ''); ?></p>
<p class="m-t-0"> Pension Provider Name: <?php echo ($getCompanyvalue['crm_paye_pension_provider'] !='' ? $getCompanyvalue['crm_paye_pension_provider']: '-'); ?></p>
<p class="m-t-0"> Employer Contribution Percentage: <?php echo ($getCompanyvalue['crm_employer_contri_percentage'] !='' ? $getCompanyvalue['crm_employer_contri_percentage']: '-'); ?></p>
<p class="m-t-0"> Employee Contribution Percentage: <?php echo ($getCompanyvalue['crm_employee_contri_percentage'] !='' ? $getCompanyvalue['crm_employee_contri_percentage']: '-'); ?></p>
<!-- for notes section -->
<p class="m-t-0">
  Notes:<?php echo (isset($getCompanyvalue['crm_pension_notes']) && $getCompanyvalue['crm_pension_notes']!='0')?$getCompanyvalue['crm_pension_notes']:'-';  ?>
</p>
<!-- end of notes -->
<!-- for services updated from activity log section -->
<p class="m-t-0">
<?php 

if(count($activity_log)>0)
{
?>
Updated Date:<?php echo ($activity_log['CreatedTime'] !='' ?  date('F j, Y, g:i', $activity_log['CreatedTime']): ''); ?>
<?php }else{
?>
Updated Date:<?php if(isset($getCompanyvalue['created_date'])){ echo date('F j, Y, g:i',$getCompanyvalue['created_date']); } 
} ?>
</p>
<!-- end of activity log -->

<!-- for notifications -->
<p class="m-t-20">
<p><label>Services :</label><?php   (isset(json_decode($getCompanyvalue['workplace'])->tab) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->tab : $jsnworkplace = '';
  ($jsnworkplace!='') ? $workplace = "On" : $workplace = "Off"; echo $workplace;?></p>
<p><label>Reminder :</label><?php   (isset(json_decode($getCompanyvalue['workplace'])->reminder) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->reminder : $jsnworkplace = '';
  ($jsnworkplace!='') ? $workplace = "On" : $workplace = "Off"; echo $workplace; ?></p>
<p><label>Text :</label><?php   (isset(json_decode($getCompanyvalue['workplace'])->text) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->text : $jsnworkplace = '';
  ($jsnworkplace!='') ? $workplace = "On" : $workplace = "Off"; echo $workplace;?></p>
<p><label>Invoice :</label><?php  echo (isset(json_decode($getCompanyvalue['workplace'])->invoice) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->invoice : $jsnworkplace = '';
  ($jsnworkplace!='') ? $workplace = "On" : $workplace = "Off"; echo $workplace;?></p>
</p>
<!-- end of notification -->
</div>
</div>
<span class="cd-date"><?php //if(isset($getusername['CreatedTime'])){ echo date('F j, Y, g:i',$getusername['CreatedTime']); }
if(count($activity_log)>0)
{
?>
<?php echo ($activity_log['CreatedTime'] !='' ?  date('F j, Y, g:i', $activity_log['CreatedTime']): ''); ?>
<?php }else{
?>
<?php if(isset($getCompanyvalue['created_date'])){ echo date('F j, Y, g:i',$getCompanyvalue['created_date']); } 
} ?></span>
<span class="cd-details"><?php echo $getCompanyvalue['crm_company_name'];?> WorkPlace Pension - AE Info </span>
</div>
</div></div>
<?php } // services check

}


if( in_array('CIS - Contractor', $services) )
{ 
(isset(json_decode($getCompanyvalue['cis'])->tab) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->tab : $jsncis = '';
if($jsncis!='' && $getCompanyvalue['crm_pension_subm_due_date']!='')
{
$activity_log=$this->db->query("select * from activity_log where module='client' and module_id=".$getCompanyvalue['user_id']." and sub_module='CIS - Contractor' order by id desc limit 1 ")->row_array();
$getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
$due=explode("-",$getCompanyvalue['crm_pension_subm_due_date']);
$mkdue=mktime(0,0,0,$due[1],$due[2],$due[0]);
?>
<div class="cd-timeline cd-container <?php echo $key; ?> sorting" id="<?=$mkdue?>" data-id="rsptrspt" >
<div class="cd-timeline-block <?php echo $key; ?>">
<div class="cd-timeline-icon bg-primary">
<i class="icofont icofont-ui-file"></i>
</div>
<div class="cd-timeline-content card_main">
<div class="p-20">
<h6><?php echo $getusername['crm_name'];?> - <?php echo $getCompanyvalue['crm_legal_form'];?></h6>
<div class="timeline-details">
<a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php //echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_pension_subm_due_date'])): ''); ?></span> </a>
<!-- shown created user name -->
<a href="#">
<i class="icofont icofont-ui-user"></i><span>
<?php 

if(count($activity_log)>0)
{

?>Updated:<?php echo ($activity_log['user_id'] !='' ?  $this->Common_mdl->get_field_value('user','crm_name','id',$activity_log['user_id']): ''); ?>
<?php }?>
</span>
</a>
<!--end of created user name 25-08-2018-->
<p class="m-t-0"> C.I.S Contractor : <?php echo ($getCompanyvalue['crm_cis_contractor'] !='' ? $getCompanyvalue['crm_cis_contractor']: '-'); ?></p>
<p class="m-t-0"> Start Date: <?php echo (strtotime($getCompanyvalue['crm_cis_contractor_start_date']) !='' ?  date('F j, Y, g:i', strtotime($getCompanyvalue['crm_cis_contractor_start_date'])): ''); ?></p>
<p class="m-t-0"> C.I.S Scheme: <?php echo ($getCompanyvalue['crm_cis_scheme_notes'] !='' ? $getCompanyvalue['crm_cis_scheme_notes']: '-'); ?></p>
 <!-- for notes section -->
<p class="m-t-0">
  Notes:<?php echo (isset($getCompanyvalue['crm_cis_scheme_notes']) && $getCompanyvalue['crm_cis_scheme_notes']!='0')?$getCompanyvalue['crm_cis_scheme_notes']:'-';  ?>
</p>
<!-- end of notes -->
<!-- for services updated from activity log section -->
<p class="m-t-0">
<?php 

if(count($activity_log)>0)
{
?>
Updated Date:<?php echo ($activity_log['CreatedTime'] !='' ?  date('F j, Y, g:i', $activity_log['CreatedTime']): ''); ?>
<?php }else{
?>
Updated Date:<?php if(isset($getCompanyvalue['created_date'])){ echo date('F j, Y, g:i',$getCompanyvalue['created_date']); }
} ?>
</p>
<!-- end of activity log -->
<!-- for notifications -->
<p class="m-t-20">
<p><label>Services :</label><?php   (isset(json_decode($getCompanyvalue['cis'])->tab) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->tab : $jsncis = '';
  ($jsncis!='') ? $cis = "On" : $cis = "Off"; echo $cis;?></p>
<p><label>Reminder :</label><?php   (isset(json_decode($getCompanyvalue['cis'])->reminder) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->reminder : $jsncis = '';
  ($jsncis!='') ? $cis = "On" : $cis = "Off"; echo $cis; ?></p>
<p><label>Text :</label><?php   (isset(json_decode($getCompanyvalue['cis'])->text) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->text : $jsncis = '';
  ($jsncis!='') ? $cis = "On" : $cis = "Off"; echo $cis;?></p>
<p><label>Invoice :</label><?php  echo (isset(json_decode($getCompanyvalue['cis'])->invoice) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->invoice : $jsncis = '';
  ($jsncis!='') ? $cis = "On" : $cis = "Off"; echo $cis;?></p>
</p>
<!-- end of notification -->
</div>
</div>
<span class="cd-date"><?php //if(isset($getusername['CreatedTime'])){ echo date('F j, Y, g:i',$getusername['CreatedTime']); }
if(count($activity_log)>0)
{
?>
<?php echo ($activity_log['CreatedTime'] !='' ?  date('F j, Y, g:i', $activity_log['CreatedTime']): ''); ?>
<?php }else{
?>
<?php if(isset($getCompanyvalue['created_date'])){ echo date('F j, Y, g:i',$getCompanyvalue['created_date']); } 
} ?></span>
<span class="cd-details"><?php echo $getCompanyvalue['crm_company_name'];?> CIS - Contractor Info </span>
</div>
</div>
</div>
<?php 
} // services check

}

if( in_array('CIS - Sub Contractor', $services) ){
 
(isset(json_decode($getCompanyvalue['cissub'])->tab) && $getCompanyvalue['cissub'] != '') ? $jsncissub =  json_decode($getCompanyvalue['cissub'])->tab : $jsncissub = '';
if($jsncissub!='' && $getCompanyvalue['crm_pension_subm_due_date']!=''){
$activity_log=$this->db->query("select * from activity_log where module='client' and module_id=".$getCompanyvalue['user_id']." and sub_module='CIS - Sub Contractor' order by id desc limit 1 ")->row_array();
$getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
$due=explode("-",$getCompanyvalue['crm_pension_subm_due_date']);
$mkdue=mktime(0,0,0,$due[1],$due[2],$due[0]);
?>
<div class="cd-timeline cd-container <?php echo $key; ?> sorting" id="<?=$mkdue?>" data-id="rsptrspt" >
<div class="cd-timeline-block <?php echo $key; ?>">
<div class="cd-timeline-icon bg-primary">
<i class="icofont icofont-ui-file"></i>
</div>
<div class="cd-timeline-content card_main">
<div class="p-20">
<h6><?php echo $getusername['crm_name'];?> - <?php echo $getCompanyvalue['crm_legal_form'];?></h6>
<div class="timeline-details">
<a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php //echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_pension_subm_due_date'])): ''); ?></span> </a>
<!-- shown created user name -->
<a href="#">
<i class="icofont icofont-ui-user"></i><span>
<?php 

if(count($activity_log)>0)
{

?>Updated:<?php echo ($activity_log['user_id'] !='' ?  $this->Common_mdl->get_field_value('user','crm_name','id',$activity_log['user_id']): ''); ?>
<?php }?>
</span>
</a>
<!--end of created user name 25-08-2018-->
<p class="m-t-0"> C.I.S Sub Contractor : <?php echo ($getCompanyvalue['crm_cis_subcontractor'] !='' ? $getCompanyvalue['crm_cis_subcontractor']: '-'); ?></p>
<p class="m-t-0"> Start Date: <?php echo (strtotime($getCompanyvalue['crm_cis_subcontractor_start_date']) !='' ?  date('F j, Y, g:i', strtotime($getCompanyvalue['crm_cis_subcontractor_start_date'])): ''); ?></p>
 <!-- for notes section -->
<p class="m-t-0">
  Notes:<?php echo (isset($getCompanyvalue['crm_cis_subcontractor_scheme_notes']) && $getCompanyvalue['crm_cis_subcontractor_scheme_notes']!='0')?$getCompanyvalue['crm_cis_subcontractor_scheme_notes']:'-';  ?>
</p>
<!-- end of notes -->
<!-- for services updated from activity log section -->
<p class="m-t-0">
<?php 

if(count($activity_log)>0)
{
?>
<?php echo ($activity_log['CreatedTime'] !='' ?  date('F j, Y, g:i', $activity_log['CreatedTime']): ''); ?>
<?php }else{
?>
<?php if(isset($getCompanyvalue['created_date'])){ echo date('F j, Y, g:i',$getCompanyvalue['created_date']); } 
} ?>
</p>
<!-- end of activity log -->
<!-- <p class="m-t-0"> C.I.S Scheme: <?php echo ($getCompanyvalue['crm_cis_subcontractor_scheme_notes'] !='' ? $getCompanyvalue['crm_cis_subcontractor_scheme_notes']: '-'); ?></p> -->
<!-- for notifications -->
<p class="m-t-20">
<p><label>Services :</label><?php   (isset(json_decode($getCompanyvalue['cissub'])->tab) && $getCompanyvalue['cissub'] != '') ? $jsncissub =  json_decode($getCompanyvalue['cissub'])->tab : $jsncissub = '';
  ($jsncissub!='') ? $cissub = "On" : $cissub = "Off"; echo $cissub;?></p>
<p><label>Reminder :</label><?php   (isset(json_decode($getCompanyvalue['cissub'])->reminder) && $getCompanyvalue['cissub'] != '') ? $jsncissub =  json_decode($getCompanyvalue['cissub'])->reminder : $jsncissub = '';
  ($jsncissub!='') ? $cissub = "On" : $cissub = "Off"; echo $cissub; ?></p>
<p><label>Text :</label><?php   (isset(json_decode($getCompanyvalue['cissub'])->text) && $getCompanyvalue['cissub'] != '') ? $jsncissub =  json_decode($getCompanyvalue['cissub'])->text : $jsncissub = '';
  ($jsncissub!='') ? $cissub = "On" : $cissub = "Off"; echo $cissub;?></p>
<p><label>Invoice :</label><?php  echo (isset(json_decode($getCompanyvalue['cissub'])->invoice) && $getCompanyvalue['cissub'] != '') ? $jsncissub =  json_decode($getCompanyvalue['cissub'])->invoice : $jsncissub = '';
  ($jsncissub!='') ? $cissub = "On" : $cissub = "Off"; echo $cissub;?></p>
</p>
<!-- end of notification -->
</div>
</div>
<span class="cd-date"><?php //if(isset($getusername['CreatedTime'])){ echo date('F j, Y, g:i',$getusername['CreatedTime']); } 
if(count($activity_log)>0)
{
?>
<?php echo ($activity_log['CreatedTime'] !='' ?  date('F j, Y, g:i', $activity_log['CreatedTime']): ''); ?>
<?php }else{
?>
<?php if(isset($getCompanyvalue['created_date'])){ echo date('F j, Y, g:i',$getCompanyvalue['created_date']); } 
}?></span>
<span class="cd-details"><?php echo $getCompanyvalue['crm_company_name'];?> CIS - Sub Contractor Info </span>
</div>
</div></div>
<?php } // services check

}
if( in_array('P11D', $services) ){

(isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = '';
if($jsnp11d!='' && $getCompanyvalue['crm_next_p11d_return_due']!=''){

$activity_log=$this->db->query("select * from activity_log where module='client' and module_id=".$getCompanyvalue['user_id']." and sub_module='P11D' order by id desc limit 1 ")->row_array();
$getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
$due=explode("-",$getCompanyvalue['crm_next_p11d_return_due']);
$mkdue=mktime(0,0,0,$due[1],$due[2],$due[0]);
?>
<div class="cd-timeline cd-container <?php echo $key; ?> sorting" id="<?=$mkdue?>" data-id="rsptrspt" >
<div class="cd-timeline-block <?php echo $key; ?>">
<div class="cd-timeline-icon bg-primary">
<i class="icofont icofont-ui-file"></i>
</div>
<div class="cd-timeline-content card_main">
<div class="p-20">
<h6><?php echo $getusername['crm_name'];?> - <?php echo $getCompanyvalue['crm_legal_form'];?></h6>
<div class="timeline-details">
<a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php echo (strtotime($getCompanyvalue['crm_next_p11d_return_due']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_next_p11d_return_due'])): ''); ?></span> </a>
<!-- shown created user name -->
<a href="#">
<i class="icofont icofont-ui-user"></i><span>
<?php 

if(count($activity_log)>0)
{

?>Updated:<?php echo ($activity_log['user_id'] !='' ?  $this->Common_mdl->get_field_value('user','crm_name','id',$activity_log['user_id']): ''); ?>
<?php }?>
</span>
</a>
<!--end of created user name 25-08-2018-->
<p class="m-t-0"> Start Date: <?php echo (strtotime($getCompanyvalue['crm_p11d_latest_action_date']) !='' ?  date('F j, Y, g:i', strtotime($getCompanyvalue['crm_p11d_latest_action_date'])): ''); ?></p>
<p class="m-t-0"> First Benefit Pay Date: <?php echo (strtotime($getCompanyvalue['crm_latest_p11d_submitted']) !='' ?  date('F j, Y, g:i', strtotime($getCompanyvalue['crm_latest_p11d_submitted'])): ''); ?></p>
<p class="m-t-0"> p11d_paye_scheme_ceased: <?php echo (strtotime($getCompanyvalue['crm_p11d_records_received']) !='' ?  date('F j, Y, g:i', strtotime($getCompanyvalue['crm_p11d_records_received'])): ''); ?></p>
<!-- for notifications -->
 <!-- for notes section -->
<p class="m-t-0">
  Notes:<?php echo (isset($getCompanyvalue['crm_p11d_notes']) && $getCompanyvalue['crm_p11d_notes']!='0')?$getCompanyvalue['crm_p11d_notes']:'-';  ?>
</p>
<!-- end of notes -->
<!-- for services updated from activity log section -->
<p class="m-t-0">
<?php 

if(count($activity_log)>0)
{
?>
Updated Date:<?php echo ($activity_log['CreatedTime'] !='' ?  date('F j, Y, g:i', $activity_log['CreatedTime']): ''); ?>
<?php }else{
?>
Updated Date:<?php if(isset($getCompanyvalue['created_date'])){ echo date('F j, Y, g:i',$getCompanyvalue['created_date']); }
} ?>
</p>
<!-- end of activity log -->
<p class="m-t-20">
<p><label>Services :</label><?php   (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = '';
  ($jsnp11d!='') ? $p11d = "On" : $p11d = "Off"; echo $p11d;?></p>
<p><label>Reminder :</label><?php   (isset(json_decode($getCompanyvalue['p11d'])->reminder) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->reminder : $jsnp11d = '';
  ($jsnp11d!='') ? $p11d = "On" : $p11d = "Off"; echo $p11d; ?></p>
<p><label>Text :</label><?php   (isset(json_decode($getCompanyvalue['p11d'])->text) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->text : $jsnp11d = '';
  ($jsnp11d!='') ? $p11d = "On" : $p11d = "Off"; echo $p11d;?></p>
<p><label>Invoice :</label><?php  echo (isset(json_decode($getCompanyvalue['p11d'])->invoice) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->invoice : $jsnp11d = '';
  ($jsnp11d!='') ? $p11d = "On" : $p11d = "Off"; echo $p11d;?></p>
</p>
<!-- end of notification -->        
</div>
</div>
<span class="cd-date"><?php //if(isset($getusername['CreatedTime'])){ echo date('F j, Y, g:i',$getusername['CreatedTime']); } 
if(count($activity_log)>0)
{
?>
<?php echo ($activity_log['CreatedTime'] !='' ?  date('F j, Y, g:i', $activity_log['CreatedTime']): ''); ?>
<?php }else{
?>
<?php if(isset($getCompanyvalue['created_date'])){ echo date('F j, Y, g:i',$getCompanyvalue['created_date']); } 
}?></span>
<span class="cd-details"><?php echo $getCompanyvalue['crm_company_name'];?> P11D Info </span>
</div>
</div></div>
<?php }

}




if( in_array('Bookkeeping', $services) )
{
 
(isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = '';
if($jsnp11d!='' && $getCompanyvalue['crm_next_booking_date']!=''){

$activity_log=$this->db->query("select * from activity_log where module='client' and module_id=".$getCompanyvalue['user_id']." and sub_module='Bookkeeping' order by id desc limit 1 ")->row_array();
$getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
$due=explode("-",$getCompanyvalue['crm_next_booking_date']);
$mkdue=mktime(0,0,0,$due[1],$due[2],$due[0]);
?>
<div class="cd-timeline cd-container <?php echo $key; ?> sorting" id="<?=$mkdue?>" data-id="rsptrspt" >
<div class="cd-timeline-block <?php echo $key; ?>">
<div class="cd-timeline-icon bg-primary">
<i class="icofont icofont-ui-file"></i>
</div>
<div class="cd-timeline-content card_main">
<div class="p-20">
<h6><?php echo $getusername['crm_name'];?> - <?php echo $getCompanyvalue['crm_legal_form'];?></h6>
<div class="timeline-details">
<a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php echo (strtotime($getCompanyvalue['crm_next_booking_date']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_next_booking_date'])): ''); ?></span> </a>
<!-- shown created user name -->
<a href="#">
<i class="icofont icofont-ui-user"></i><span>
<?php 

if(count($activity_log)>0)
{

?>Updated:<?php echo ($activity_log['user_id'] !='' ?  $this->Common_mdl->get_field_value('user','crm_name','id',$activity_log['user_id']): ''); ?>
<?php }?>
</span>
</a>
<!--end of created user name 25-08-2018-->
<p class="m-t-0"> Bookkeepeing to Done: <?php echo ($getCompanyvalue['crm_bookkeeping'] !='' ? $getCompanyvalue['crm_bookkeeping']: '-'); ?></p>
<p class="m-t-0"> Method of Bookkeeping: <?php echo ($getCompanyvalue['crm_method_bookkeeping'] !='' ? $getCompanyvalue['crm_method_bookkeeping']: '-'); ?></p>
<p class="m-t-0"> How Client will provide records: <?php echo ($getCompanyvalue['crm_client_provide_record'] !='' ? $getCompanyvalue['crm_client_provide_record']: '-'); ?></p>
<!-- for notifications -->
<!-- for services updated from activity log section -->
<p class="m-t-0">
<?php 

if(count($activity_log)>0)
{
?>
Updated Date:<?php echo ($activity_log['CreatedTime'] !='' ?  date('F j, Y, g:i', $activity_log['CreatedTime']): ''); ?>
<?php }else{
?>
Updated Date:<?php if(isset($getCompanyvalue['created_date'])){ echo date('F j, Y, g:i',$getCompanyvalue['created_date']); }
} ?>
</p>
<!-- end of activity log -->
<p class="m-t-20">
<p><label>Services :</label><?php   (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = '';
  ($jsnp11d!='') ? $p11d = "On" : $p11d = "Off"; echo $p11d;?></p>
<p><label>Reminder :</label><?php   (isset(json_decode($getCompanyvalue['p11d'])->reminder) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->reminder : $jsnp11d = '';
  ($jsnp11d!='') ? $p11d = "On" : $p11d = "Off"; echo $p11d; ?></p>
<p><label>Text :</label><?php   (isset(json_decode($getCompanyvalue['p11d'])->text) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->text : $jsnp11d = '';
  ($jsnp11d!='') ? $p11d = "On" : $p11d = "Off"; echo $p11d;?></p>
<p><label>Invoice :</label><?php  echo (isset(json_decode($getCompanyvalue['p11d'])->invoice) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->invoice : $jsnp11d = '';
  ($jsnp11d!='') ? $p11d = "On" : $p11d = "Off"; echo $p11d;?></p>
</p>
<!-- end of notification -->  
</div>
</div>
<span class="cd-date"><?php //if(isset($getusername['CreatedTime'])){ echo date('F j, Y, g:i',$getusername['CreatedTime']); }
if(count($activity_log)>0)
{
?>
<?php echo ($activity_log['CreatedTime'] !='' ?  date('F j, Y, g:i', $activity_log['CreatedTime']): ''); ?>
<?php }else{
?>
<?php if(isset($getCompanyvalue['created_date'])){ echo date('F j, Y, g:i',$getCompanyvalue['created_date']); } 
} ?></span>
<span class="cd-details"><?php echo $getCompanyvalue['crm_company_name'];?> Bookkeeping Info </span>
</div>
</div></div>
<?php }

}
if( in_array('Management Accounts', $services) )
{
 
(isset(json_decode($getCompanyvalue['management'])->tab) && $getCompanyvalue['management'] != '') ? $jsnmanagement =  json_decode($getCompanyvalue['management'])->tab : $jsnmanagement = '';
if($jsnmanagement!='' && $getCompanyvalue['crm_next_manage_acc_date']!=''){

$activity_log=$this->db->query("select * from activity_log where module='client' and module_id=".$getCompanyvalue['user_id']." and sub_module='Management Accounts' order by id desc limit 1 ")->row_array();
$getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
$due=explode("-",$getCompanyvalue['crm_next_manage_acc_date']);
$mkdue=mktime(0,0,0,$due[1],$due[2],$due[0]);
?>
<div class="cd-timeline cd-container <?php echo $key; ?> sorting" id="<?=$mkdue?>" data-id="rsptrspt" >
<div class="cd-timeline-block <?php echo $key; ?>">
<div class="cd-timeline-icon bg-primary">
<i class="icofont icofont-ui-file"></i>
</div>
<div class="cd-timeline-content card_main">
<div class="p-20">
<h6><?php echo $getusername['crm_name'];?> - <?php echo $getCompanyvalue['crm_legal_form'];?></h6>
<div class="timeline-details">
<a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php echo (strtotime($getCompanyvalue['crm_next_manage_acc_date']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_next_manage_acc_date'])): ''); ?></span> </a>
<!-- shown created user name -->
<a href="#">
<i class="icofont icofont-ui-user"></i><span>
<?php 

if(count($activity_log)>0)
{

?>Updated:<?php echo ($activity_log['user_id'] !='' ?  $this->Common_mdl->get_field_value('user','crm_name','id',$activity_log['user_id']): ''); ?>
<?php }?>
</span>
</a>
<!--end of created user name 25-08-2018-->
<p class="m-t-0"> Management Accounts Frequency: <?php echo ($getCompanyvalue['crm_manage_acc_fre'] !='' ? $getCompanyvalue['crm_manage_acc_fre']: '-'); ?></p>
<p class="m-t-0"> Method of Bookkeeping: <?php echo ($getCompanyvalue['crm_manage_method_bookkeeping'] !='' ? $getCompanyvalue['crm_manage_method_bookkeeping']: '-'); ?></p>
<p class="m-t-0"> How Client will provide records: <?php echo ($getCompanyvalue['crm_manage_client_provide_record'] !='' ? $getCompanyvalue['crm_manage_client_provide_record']: '-'); ?></p>
<!-- for notifications -->
 <!-- for notes section -->
<p class="m-t-0">
  Notes:<?php echo (isset($getCompanyvalue['crm_manage_notes']) && $getCompanyvalue['crm_manage_notes']!='0')?$getCompanyvalue['crm_manage_notes']:'-';  ?>
</p>
<!-- for services updated from activity log section -->
<p class="m-t-0">
<?php 

if(count($activity_log)>0)
{
?>
Updated Date:<?php echo ($activity_log['CreatedTime'] !='' ?  date('F j, Y, g:i', $activity_log['CreatedTime']): ''); ?>
<?php }else{
?>
Updated Date:<?php if(isset($getCompanyvalue['created_date'])){ echo date('F j, Y, g:i',$getCompanyvalue['created_date']); }
} ?>
</p>
<!-- end of activity log -->
<!-- end of notes -->
<p class="m-t-20">
<p><label>Services :</label><?php   (isset(json_decode($getCompanyvalue['management'])->tab) && $getCompanyvalue['management'] != '') ? $jsnmanagement =  json_decode($getCompanyvalue['management'])->tab : $jsnmanagement = '';
  ($jsnmanagement!='') ? $management = "On" : $management = "Off"; echo $management;?></p>
<p><label>Reminder :</label><?php   (isset(json_decode($getCompanyvalue['management'])->reminder) && $getCompanyvalue['management'] != '') ? $jsnmanagement =  json_decode($getCompanyvalue['management'])->reminder : $jsnmanagement = '';
  ($jsnmanagement!='') ? $management = "On" : $management = "Off"; echo $management; ?></p>
<p><label>Text :</label><?php   (isset(json_decode($getCompanyvalue['management'])->text) && $getCompanyvalue['management'] != '') ? $jsnmanagement =  json_decode($getCompanyvalue['management'])->text : $jsnmanagement = '';
  ($jsnmanagement!='') ? $management = "On" : $management = "Off"; echo $management;?></p>
<p><label>Invoice :</label><?php  echo (isset(json_decode($getCompanyvalue['management'])->invoice) && $getCompanyvalue['management'] != '') ? $jsnmanagement =  json_decode($getCompanyvalue['management'])->invoice : $jsnmanagement = '';
  ($jsnmanagement!='') ? $management = "On" : $management = "Off"; echo $management;?></p>
</p>
<!-- end of notification -->  
</div>
</div>
<span class="cd-date"><?php //if(isset($getusername['CreatedTime'])){ echo date('F j, Y, g:i',$getusername['CreatedTime']); } 
if(count($activity_log)>0)
{
?>
<?php echo ($activity_log['CreatedTime'] !='' ?  date('F j, Y, g:i', $activity_log['CreatedTime']): ''); ?>
<?php }else{
?>
<?php if(isset($getCompanyvalue['created_date'])){ echo date('F j, Y, g:i',$getCompanyvalue['created_date']); } 
}?></span>
<span class="cd-details"><?php echo $getCompanyvalue['crm_company_name'];?> Management Accounts Info </span>
</div>
</div></div>
<?php } // services check

}
if( in_array('Investigation Insurance', $services) ){
 
(isset(json_decode($getCompanyvalue['investgate'])->tab) && $getCompanyvalue['investgate'] != '') ? $jsninvestgate =  json_decode($getCompanyvalue['investgate'])->tab : $jsninvestgate = '';
if($jsninvestgate!='' && $getCompanyvalue['crm_insurance_renew_date']!=''){
$activity_log=$this->db->query("select * from activity_log where module='client' and module_id=".$getCompanyvalue['user_id']." and sub_module='Investigation Insurance' order by id desc limit 1 ")->row_array();
$getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
$due=explode("-",$getCompanyvalue['crm_insurance_renew_date']);
$mkdue=mktime(0,0,0,$due[1],$due[2],$due[0]);
?>
<div class="cd-timeline cd-container <?php echo $key; ?> sorting" id="<?=$mkdue?>" data-id="rsptrspt" >
<div class="cd-timeline-block <?php echo $key; ?>">
<div class="cd-timeline-icon bg-primary">
<i class="icofont icofont-ui-file"></i>
</div>
<div class="cd-timeline-content card_main">
<div class="p-20">
<h6><?php echo $getusername['crm_name'];?> - <?php echo $getCompanyvalue['crm_legal_form'];?></h6>
<div class="timeline-details">
<a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php echo (strtotime($getCompanyvalue['crm_insurance_renew_date']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_insurance_renew_date'])): ''); ?></span> </a>
<!-- shown created user name -->
<a href="#">
<i class="icofont icofont-ui-user"></i>
<span>
<?php 

if(count($activity_log)>0)
{

?>Updated:<?php echo ($activity_log['user_id'] !='' ?  $this->Common_mdl->get_field_value('user','crm_name','id',$activity_log['user_id']): ''); ?>
<?php }?>
</span>
</a>
<!--end of created user name 25-08-2018-->
<p class="m-t-0"> Start Date: <?php echo (strtotime($getCompanyvalue['crm_invesitgation_insurance']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_invesitgation_insurance'])): ''); ?></p>
<p class="m-t-0"> Insurance Provider: <?php echo ($getCompanyvalue['crm_insurance_provider'] !='' ? $getCompanyvalue['crm_insurance_provider']: '-'); ?></p>
<p class="m-t-0"> History of Previous Claims: <?php echo ($getCompanyvalue['crm_claims_note'] !='' ? $getCompanyvalue['crm_claims_note']: '-'); ?></p>
<!-- for notifications -->
<!-- for services updated from activity log section -->
<p class="m-t-0">
<?php 

if(count($activity_log)>0)
{
?>
Updated Date:<?php echo ($activity_log['CreatedTime'] !='' ?  date('F j, Y, g:i', $activity_log['CreatedTime']): ''); ?>
<?php }else{
?>
Updated Date:<?php if(isset($getCompanyvalue['created_date'])){ echo date('F j, Y, g:i',$getCompanyvalue['created_date']); }
} ?>
</p>
<!-- end of activity log -->
<p class="m-t-20">
<p><label>Services :</label><?php   (isset(json_decode($getCompanyvalue['investgate'])->tab) && $getCompanyvalue['investgate'] != '') ? $jsninvestgate =  json_decode($getCompanyvalue['investgate'])->tab : $jsninvestgate = '';
  ($jsninvestgate!='') ? $investgate = "On" : $investgate = "Off"; echo $investgate;?></p>
<p><label>Reminder :</label><?php   (isset(json_decode($getCompanyvalue['investgate'])->reminder) && $getCompanyvalue['investgate'] != '') ? $jsninvestgate =  json_decode($getCompanyvalue['investgate'])->reminder : $jsninvestgate = '';
  ($jsninvestgate!='') ? $investgate = "On" : $investgate = "Off"; echo $investgate; ?></p>
<p><label>Text :</label><?php   (isset(json_decode($getCompanyvalue['investgate'])->text) && $getCompanyvalue['investgate'] != '') ? $jsninvestgate =  json_decode($getCompanyvalue['investgate'])->text : $jsninvestgate = '';
  ($jsninvestgate!='') ? $investgate = "On" : $investgate = "Off"; echo $investgate;?></p>
<p><label>Invoice :</label><?php  echo (isset(json_decode($getCompanyvalue['investgate'])->invoice) && $getCompanyvalue['investgate'] != '') ? $jsninvestgate =  json_decode($getCompanyvalue['investgate'])->invoice : $jsninvestgate = '';
  ($jsninvestgate!='') ? $investgate = "On" : $investgate = "Off"; echo $investgate;?></p>
</p>
<!-- end of notification -->  
</div>
</div>
<span class="cd-date"><?php //if(isset($getusername['CreatedTime'])){ echo date('F j, Y, g:i',$getusername['CreatedTime']); } 
if(count($activity_log)>0)
{
?>
<?php echo ($activity_log['CreatedTime'] !='' ?  date('F j, Y, g:i', $activity_log['CreatedTime']): ''); ?>
<?php }else{
?>
<?php if(isset($getCompanyvalue['created_date'])){ echo date('F j, Y, g:i',$getCompanyvalue['created_date']); } 
}?></span>
<span class="cd-details"><?php echo $getCompanyvalue['crm_company_name'];?> Investigation Insurance Info </span>
</div>
</div>
</div>
<?php } // services check

}
if( in_array('Registered Address', $services) ){
 
(isset(json_decode($getCompanyvalue['registered'])->tab) && $getCompanyvalue['registered'] != '') ? $jsnregistered =  json_decode($getCompanyvalue['registered'])->tab : $jsnregistered = '';
if($jsnregistered!='' && $getCompanyvalue['crm_registered_renew_date']!=''){
$activity_log=$this->db->query("select * from activity_log where module='client' and module_id=".$getCompanyvalue['user_id']." and sub_module='Registered Address' order by id desc limit 1 ")->row_array();
$getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
$due=explode("-",$getCompanyvalue['crm_registered_renew_date']);
$mkdue=mktime(0,0,0,$due[1],$due[2],$due[0]);
?>
<div class="cd-timeline cd-container <?php echo $key; ?> sorting" id="<?=$mkdue?>" data-id="rsptrspt" >
<div class="cd-timeline-block <?php echo $key; ?>">
<div class="cd-timeline-icon bg-primary">
<i class="icofont icofont-ui-file"></i>
</div>
<div class="cd-timeline-content card_main">
<div class="p-20">
<h6><?php echo $getusername['crm_name'];?> - <?php echo $getCompanyvalue['crm_legal_form'];?></h6>
<div class="timeline-details">
<a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php echo (strtotime($getCompanyvalue['crm_registered_renew_date']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_registered_renew_date'])): ''); ?></span> </a>
<!-- shown created user name -->
<a href="#">
<i class="icofont icofont-ui-user"></i><span>
<?php 

if(count($activity_log)>0)
{

?>Updated:<?php echo ($activity_log['user_id'] !='' ?  $this->Common_mdl->get_field_value('user','crm_name','id',$activity_log['user_id']): ''); ?>
<?php }?>
</span>
</a>
<!--end of created user name 25-08-2018-->
<p class="m-t-0"> Start Date: <?php echo (strtotime($getCompanyvalue['crm_registered_start_date']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_registered_start_date'])): ''); ?></p>
<p class="m-t-0"> Registered office in Use: <?php echo ($getCompanyvalue['crm_registered_office_inuse'] !='' ? $getCompanyvalue['crm_registered_office_inuse']: '-'); ?></p>
<p class="m-t-0"> History of Previous Claims: <?php echo ($getCompanyvalue['crm_registered_claims_note'] !='' ? $getCompanyvalue['crm_registered_claims_note']: '-'); ?></p>
<!-- for notifications -->
<!-- for services updated from activity log section -->
<p class="m-t-0">
<?php 

if(count($activity_log)>0)
{
?>
Updated Date:<?php echo ($activity_log['CreatedTime'] !='' ?  date('F j, Y, g:i', $activity_log['CreatedTime']): ''); ?>
<?php }else{
?>
Updated Date:<?php if(isset($getCompanyvalue['created_date'])){ echo date('F j, Y, g:i',$getCompanyvalue['created_date']); } 
} ?>
</p>
<!-- end of activity log -->
<p class="m-t-20">
<p><label>Services :</label><?php   (isset(json_decode($getCompanyvalue['registered'])->tab) && $getCompanyvalue['registered'] != '') ? $jsnregistered =  json_decode($getCompanyvalue['registered'])->tab : $jsnregistered = '';
  ($jsnregistered!='') ? $registered = "On" : $registered = "Off"; echo $registered;?></p>
<p><label>Reminder :</label><?php   (isset(json_decode($getCompanyvalue['registered'])->reminder) && $getCompanyvalue['registered'] != '') ? $jsnregistered =  json_decode($getCompanyvalue['registered'])->reminder : $jsnregistered = '';
  ($jsnregistered!='') ? $registered = "On" : $registered = "Off"; echo $registered; ?></p>
<p><label>Text :</label><?php   (isset(json_decode($getCompanyvalue['registered'])->text) && $getCompanyvalue['registered'] != '') ? $jsnregistered =  json_decode($getCompanyvalue['registered'])->text : $jsnregistered = '';
  ($jsnregistered!='') ? $registered = "On" : $registered = "Off"; echo $registered;?></p>
<p><label>Invoice :</label><?php  echo (isset(json_decode($getCompanyvalue['registered'])->invoice) && $getCompanyvalue['registered'] != '') ? $jsnregistered =  json_decode($getCompanyvalue['registered'])->invoice : $jsnregistered = '';
  ($jsnregistered!='') ? $registered = "On" : $registered = "Off"; echo $registered;?></p>
</p>
<!-- end of notification --> 
</div>
</div>
<span class="cd-date"><?php //if(isset($getusername['CreatedTime'])){ echo date('F j, Y, g:i',$getusername['CreatedTime']); } 
if(count($activity_log)>0)
{
?>
<?php echo ($activity_log['CreatedTime'] !='' ?  date('F j, Y, g:i', $activity_log['CreatedTime']): ''); ?>
<?php }else{
?>
<?php if(isset($getCompanyvalue['created_date'])){ echo date('F j, Y, g:i',$getCompanyvalue['created_date']); } 
}?></span>
<span class="cd-details"><?php echo $getCompanyvalue['crm_company_name'];?> Registered Address Info </span>
</div>
</div>
</div>
<?php } // services check

}
if( in_array('Tax Advice', $services) ){
 
(isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxadvice =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxadvice = '';
if($jsntaxadvice!='' && $getCompanyvalue['crm_investigation_end_date']!=''){
$activity_log=$this->db->query("select * from activity_log where module='client' and module_id=".$getCompanyvalue['user_id']." and sub_module='Tax Advice' order by id desc limit 1 ")->row_array();
$getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
$due=explode("-",$getCompanyvalue['crm_investigation_end_date']);
$mkdue=mktime(0,0,0,$due[1],$due[2],$due[0]);
?>
<div class="cd-timeline cd-container <?php echo $key; ?> sorting" id="<?=$mkdue?>" data-id="rsptrspt" >
<div class="cd-timeline-block <?php echo $key; ?>">
<div class="cd-timeline-icon bg-primary">
<i class="icofont icofont-ui-file"></i>
</div>
<div class="cd-timeline-content card_main">
<div class="p-20">
<h6><?php echo $getusername['crm_name'];?> - <?php echo $getCompanyvalue['crm_legal_form'];?></h6>
<div class="timeline-details">
<a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></span> </a>
<!-- shown created user name -->
<a href="#">
<i class="icofont icofont-ui-user"></i><span>
<?php 

if(count($activity_log)>0)
{

?>Updated:<?php echo ($activity_log['user_id'] !='' ?  $this->Common_mdl->get_field_value('user','crm_name','id',$activity_log['user_id']): ''); ?>
<?php }?>
</span>
</a>
<!--end of created user name 25-08-2018-->
<p class="m-t-0"> Start Date: <?php echo (strtotime($getCompanyvalue['crm_investigation_start_date']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_investigation_start_date'])): ''); ?></p>
<p class="m-t-0"> Notes: <?php echo ($getCompanyvalue['crm_investigation_note'] !='' ? $getCompanyvalue['crm_investigation_note']: '-'); ?></p>
<!-- for notifications -->
<!-- for services updated from activity log section -->
<p class="m-t-0">
<?php 

if(count($activity_log)>0)
{
?>
Updated Date:<?php echo ($activity_log['CreatedTime'] !='' ?  date('F j, Y, g:i', $activity_log['CreatedTime']): ''); ?>
<?php }else{
?>
Updated Date:<?php if(isset($getCompanyvalue['created_date'])){ echo date('F j, Y, g:i',$getCompanyvalue['created_date']); }
} ?>
</p>
<!-- end of activity log -->
<p class="m-t-20">
<p><label>Services :</label><?php   (isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxadvice =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxadvice = '';
  ($jsntaxadvice!='') ? $taxadvice = "On" : $taxadvice = "Off"; echo $taxadvice;?></p>
<p><label>Reminder :</label><?php   (isset(json_decode($getCompanyvalue['taxadvice'])->reminder) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxadvice =  json_decode($getCompanyvalue['taxadvice'])->reminder : $jsntaxadvice = '';
  ($jsntaxadvice!='') ? $taxadvice = "On" : $taxadvice = "Off"; echo $taxadvice; ?></p>
<p><label>Text :</label><?php   (isset(json_decode($getCompanyvalue['taxadvice'])->text) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxadvice =  json_decode($getCompanyvalue['taxadvice'])->text : $jsntaxadvice = '';
  ($jsntaxadvice!='') ? $taxadvice = "On" : $taxadvice = "Off"; echo $taxadvice;?></p>
<p><label>Invoice :</label><?php  echo (isset(json_decode($getCompanyvalue['taxadvice'])->invoice) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxadvice =  json_decode($getCompanyvalue['taxadvice'])->invoice : $jsntaxadvice = '';
  ($jsntaxadvice!='') ? $taxadvice = "On" : $taxadvice = "Off"; echo $taxadvice;?></p>
</p>
<!-- end of notification --> 
</div>
</div>
<span class="cd-date"><?php //if(isset($getusername['CreatedTime'])){ echo date('F j, Y, g:i',$getusername['CreatedTime']); }
if(count($activity_log)>0)
{
?>
<?php echo ($activity_log['CreatedTime'] !='' ?  date('F j, Y, g:i', $activity_log['CreatedTime']): ''); ?>
<?php }else{
?>
<?php if(isset($getCompanyvalue['created_date'])){ echo date('F j, Y, g:i',$getCompanyvalue['created_date']); } 
} ?></span>
<span class="cd-details"><?php echo $getCompanyvalue['crm_company_name'];?> Tax Advice Info </span>
</div>
</div>
</div>
<?php } //services check

}
if(in_array('Tax Investigation', $services))
{
 
(isset(json_decode($getCompanyvalue['taxinvest'])->tab) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->tab : $jsntaxinvest = '';
if($jsntaxinvest!='' && $getCompanyvalue['crm_investigation_end_date']!=''){
$activity_log=$this->db->query("select * from activity_log where module='client' and module_id=".$getCompanyvalue['user_id']." and sub_module='Tax Investigation' order by id desc limit 1 ")->row_array();
$getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
$due=explode("-",$getCompanyvalue['crm_investigation_end_date']);
$mkdue=mktime(0,0,0,$due[1],$due[2],$due[0]);
?>
<div class="cd-timeline cd-container <?php echo $key; ?> sorting" id="<?=$mkdue?>" data-id="rsptrspt" >
<div class="cd-timeline-block <?php echo $key; ?>">
<div class="cd-timeline-icon bg-primary">
<i class="icofont icofont-ui-file"></i>
</div>
<div class="cd-timeline-content card_main">
<div class="p-20">
<h6><?php echo $getusername['crm_name'];?> - <?php echo $getCompanyvalue['crm_legal_form'];?></h6>
<div class="timeline-details">
<a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></span> </a>
<!-- shown created user name -->
<a href="#">
<i class="icofont icofont-ui-user"></i><span>
<?php 

if(isset($activity_log) && count($activity_log)>0)
{

?>Updated:<?php echo ($activity_log['user_id'] !='' ?  $this->Common_mdl->get_field_value('user','crm_name','id',$activity_log['user_id']): ''); ?>
<?php } ?>
</span>
</a>
<!--end of created user name 25-08-2018-->
<p class="m-t-0"> Start Date: <?php echo (strtotime($getCompanyvalue['crm_investigation_start_date']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_investigation_start_date'])): ''); ?></p>
<p class="m-t-0"> Notes: <?php echo ($getCompanyvalue['crm_investigation_note'] !='' ? $getCompanyvalue['crm_investigation_note']: '-'); ?></p>
<!-- for notifications -->
<!-- for services updated from activity log section -->
<p class="m-t-0">
<?php 

if(isset($activity_log) && count($activity_log)>0)
{
?>
Updated Date:<?php echo ($activity_log['CreatedTime'] !='' ?  date('F j, Y, g:i', $activity_log['CreatedTime']): ''); ?>
<?php }else{
?>
Updated Date:<?php if(isset($getCompanyvalue['created_date'])){ echo date('F j, Y, g:i',$getCompanyvalue['created_date']); } 
} ?>
</p>
<!-- end of activity log -->
<p class="m-t-20">
<p><label>Services :</label><?php   (isset(json_decode($getCompanyvalue['taxinvest'])->tab) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->tab : $jsntaxinvest = '';
  ($jsntaxinvest!='') ? $taxinvest = "On" : $taxinvest = "Off"; echo $taxinvest;?></p>
<p><label>Reminder :</label><?php   (isset(json_decode($getCompanyvalue['taxinvest'])->reminder) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->reminder : $jsntaxinvest = '';
  ($jsntaxinvest!='') ? $taxinvest = "On" : $taxinvest = "Off"; echo $taxinvest; ?></p>
<p><label>Text :</label><?php   (isset(json_decode($getCompanyvalue['taxinvest'])->text) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->text : $jsntaxinvest = '';
  ($jsntaxinvest!='') ? $taxinvest = "On" : $taxinvest = "Off"; echo $taxinvest;?></p>
<p><label>Invoice :</label><?php  echo (isset(json_decode($getCompanyvalue['taxinvest'])->invoice) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->invoice : $jsntaxinvest = '';
  ($jsntaxinvest!='') ? $taxinvest = "On" : $taxinvest = "Off"; echo $taxinvest;?></p>
</p>
<!-- end of notification --> 
</div>
</div>
<span class="cd-date"><?php //if(isset($getusername['CreatedTime'])){ echo date('F j, Y, g:i',$getusername['CreatedTime']); } 
if(isset($activity_log) && count($activity_log)>0)
{
?>
<?php echo ($activity_log['CreatedTime'] !='' ?  date('F j, Y, g:i', $activity_log['CreatedTime']): ''); ?>
<?php }else{
?>
<?php if(isset($getCompanyvalue['created_date'])){ echo date('F j, Y, g:i',$getCompanyvalue['created_date']); } 
}?></span>
<span class="cd-details"><?php echo $getCompanyvalue['crm_company_name'];?> Tax Investigation Info</span>
</div>
</div>
</div>

<?php } // services check
}
?>
</div><!--#service-->
<?php 
/** for company created data **/
/** end of company created **/
?>  
<!-- <?php if($value!=''){ ?>       
</div>
<?php } ?>     -->
            
<?php

// }


$data = $this->Common_mdl->GetAllWithWheretwo('activity_log',' module','client','module_id',$client_id);
$client_details = $client_details[0];
foreach (array_reverse($data) as $key => $value)
{
$CreatedTime = !empty( $value['CreatedTime'] )? $value['CreatedTime']:0;
?>

<div class="cd-timeline-block sorting" id="<?php echo $CreatedTime;?>">
<div class="cd-timeline-icon bg-primary">
<i class="icofont icofont-ui-file"></i>
</div>
<div class="cd-timeline-content card_main">
<div class="p-20">
<h6>
<?php echo $client_details['crm_company_name'];?> - 
<?php echo $client_details['crm_legal_form'];?>
</h6>
<div class="timeline-details">
<p class="m-t-0">
<?php echo $value['log'];?>                                                              
</p>
</div>
</div>
<span class="cd-date"><?php if(isset($value['CreatedTime'])){ echo date('F j, Y, g:i',$value['CreatedTime']); } ?></span>
<span class="cd-details"><?php echo $client_details['crm_company_name'];?></span>
</div>
</div>

<?php    
}
?>                                  
</div>
<div class="cd-timeline cd-container overall_for_allactivitylog_section" id="search_for_activity" style="display: none;">
</div>
</div>

