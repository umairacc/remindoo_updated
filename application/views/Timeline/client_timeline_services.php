<style type="text/css">
	.notes_edit {
		float: right;
	}
</style>
<?php
$client_services = $this->Common_mdl->getClientServices($client_id);

//$client_services = $this->db->query("SELECT * FROM service_lists")->result_array();

/*$All_NotesCount = $this->db->query('SELECT count(*) as num,module FROM `timeline_services_notes_added` where user_id='.$client_id.' GROUP by module')->result_array();

   $All_NotesCount = array_map(function($a){ return ($a=='')?0:$a; },array_column( $All_NotesCount ,'num','module'));*/
?>
<div id="tabs221" class="timeline-notes_active">
	<input type="hidden" name="timeline_client_id" id="timeline_client_id" value="<?php echo $client_id; ?>">
	<ul>
		<li><a href="" class="for_tabs for_allactivity_section_ajax">Client Timeline</a></li>
		<!--   <li><a href="#tabs-5" class="for_tabs"><i class="fa fa-file-text-o" aria-hidden="true"></i>create task</a></li>
      <li class="active"><a href="#tabs-1" class="for_tabs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>new note</a></li>
      <li><a href="#tabs-2" class="for_tabs"><i class="fa fa-envelope" aria-hidden="true"></i>email</a></li>
      <li><a href="#tabs-3" class="for_tabs"><i class="fa fa-phone" aria-hidden="true"></i>call</a></li>
      <li><a href="#tabs-7" class="for_tabs"><i class="fa fa-envelope" aria-hidden="true"></i>SMS</a></li>
      <li><a href="#tabs-13" class="for_tabs"><i class="fa fa-envelope" aria-hidden="true"></i>Meeting</a></li>
      <li><a href="#tabs-4" class="for_tabs"><i class="fa fa-plus" aria-hidden="true"></i>log activity</a></li>
      <li><a href="#tabs-6" class="for_tabs"><i class="fa fa-clock-o" aria-hidden="true"></i>schedule</a></li> -->
		<!-- <li class="visible-schedule"><a href="#tabs-7">Add Event</a></li> -->
	</ul>
	<!--add notes Modal start -->
	<div id="note_popup" class="modal fade" role="dialog" data-backdrop="static">
		<div class="modal-dialog modal-dialog-timeline">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header modal-header-timeline">
					<button type="button" class="close" data-dismiss="modal"></button>
					<h4 class="modal-title">Create a Note</h4>
				</div>
				<div class="modal-body modal-body-timeline">
					<form id="timeline_notes" method="post" name="form">
						<div class="editor_note">
							<textarea name="feed_msg" id="notes_msg" class="notes_text_box" placeholder="Type your text here..."></textarea>
							<span class="notes_errormsg error_msg" style="color: red;"></span>
						</div>
						<div class="deadline-crm1 floating_set" id="trig_acts">
							<ol class="all_user1 md-tabs pull-left u-dashboard">
								<li class="nav-item notes">
									<a class="active" data-id="addNotes_tab" href="javascript:void(0)" data-value="notes">Add Notes
									</a>
								</li>
								<li class="nav-item calllog">
									<a data-id="call_tab" href="javascript:void(0)" data-value="calllog"> Call
									</a>
								</li>
								<li class="nav-item email">
									<a data-id="email_tab" href="javascript:void(0)" data-value="email">Email
									</a>
								</li>
								<li class="nav-item smslog">
									<a data-id="sms_tab" href="javascript:void(0)" data-value="smslog">SMS
									</a>
								</li>
								<li class="nav-item form_builder logactivity">
									<a data-id="log_activity_tab" href="javascript:void(0)" data-value="logactivity">Log Activity
									</a>
								</li>
								<li class="nav-item form_builder meeting">
									<a data-id="meeting_tab" href="javascript:void(0)" data-value="meeting">Meeting
									</a>
								</li>
								<!-- <li class="nav-item form_builder">
									<a class="nav-link" data-toggle="tab" href="#task_create_tab">Create Task</a>
								</li> -->
								<li class="nav-item form_builder schedule">
									<a data-id="schedule_tab" href="javascript:void(0)" data-value="schedule">Schedule
									</a>
								</li>
							</ol>
						</div>
						<div class="form-group col-sm-6">
							<label>Services Type</label>
							<div class="dropdown-sin-12 lead-form-st">
								<select name="services_type" id="services_type" class="form-control fields" placeholder="Select Services Type">
									<option value="">--Select Service Type--</option>
									<?php
									foreach ($client_services as $clse_key => $clse_value) {
									?>
										<option value="<?php echo trim($clse_value['service_name']); ?>"><?php echo trim($clse_value['service_name']); ?></option>
									<?php } ?>
								</select>
							</div>
							<span class="services_type_errormsg error_msg" style="color: red;"></span>
						</div>
						<div class="form-group col-sm-6">
							<label>Date</label>
							<input type="text" class="datepicker" name="date" id="call_date1" value="<?php echo date("d-m-y"); ?>">
						</div>

						<input type="hidden" name="notes_section" value="notes">
						<input type="hidden" name="notes_id" value="">
						<input type="hidden" name="user_id" value="<?php echo $client_id; ?>">
						<div class="form-group form-group-save">
							<button class="btn btn-success pull-right" type="submit">Save</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!--add notes modal close -->

	<div id="tabs-11">
		<!-- content from ajax data -->
		<div id="allactivitylog_sectionsss">
			<div class="card ser-timeline">
				<div class="card-header">
					<div class="col-md-12">
						<div class="col-md-2">
							<button type="button" class="btn btn-primary show_all_activity applied_filter" style="
                  font-size: 10px;">ALL ACTIVITY</button>
						</div>
						<div class="col-md-2 filter_timeline_list">
							<!-- for filter options 18-09-2018 -->
							<div class="button-group">
								<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" style="font-size: 10px;">Services</button>
								<ul class="dropdown-menu">
									<li><a href="javascript:void(0);" class="small" data-value="option6" tabIndex="-1">Client Services&nbsp;</a></li>
									<?php foreach ($client_services as $filt_key => $filt_value) {
									?>
										<li>
											<a href="javascript:void(0);" class="small" data-value="option6" tabIndex="-1">
												<label class="custom_checkbox1">
													<input type="checkbox" name="filter_timeline[]" id="filter_timeline_<?php echo $filt_value['service_name']; ?>" class="service_filter" value="<?php echo $filt_value['service_name']; ?>" />
													<i></i>
												</label>
												&nbsp;<?php echo $filt_value['service_name']; ?>
											</a>
										</li>
									<?php
									} ?>
								</ul>
							</div>
						</div>

						<div class="col-md-2">
							<button type="button" class="btn btn-primary section_filter" data-id="notes" style="font-size: 10px;">Notes<p></p>
							</button>
						</div>
						<div class="col-md-2">
							<button type="button" class="btn btn-primary section_filter" data-id="email" style="font-size: 10px;">Email<p></p>
							</button>
						</div>
						<div class="col-md-2">
							<button type="button" class="btn btn-primary section_filter" data-id="calllog" style="font-size: 10px;">Call<p></p>
							</button>
						</div>
						<div class="col-md-2">
							<button type="button" class="btn btn-primary section_filter" data-id="smslog" style="font-size: 10px;">Sms<p></p>
						</div>
						<div class="col-md-2">
							<button type="button" class="btn btn-primary section_filter" data-id="logactivity" style="font-size: 10px;">Log<p></p>
							</button>
						</div>
						<div class="col-md-2">
							<button type="button" class="btn btn-primary section_filter" data-id="meeting" style="font-size: 10px;">Metting<p> </p>
							</button>
						</div>
						<div class="col-md-2">
							<button type="button" class="btn btn-primary section_filter" data-id="schedule" style="font-size: 10px;">Schedule<p></p>
							</button>
						</div>
						<div class="col-md-5 full-activity01 pull-right">
							<input class="allactivitylog-search_timeline" type="text" id="allactivitylog_section" placeholder="Search">
							<?php if ($_SESSION['permission']['Client_Section']['create'] == '1') { ?>
								<a href="#" data-toggle="modal" data-target="#note_popup" id="show_add_service_notes_popup">Create a Note</a>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="card-block" id="allactivitylog_sections_info">
					<div class="cd-timeline cd-container overall_allactivitylog_section" id="search_for_activity">
					</div>
					<div class="pageination timelinepage"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="my_Modal" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Confirmation</h4>
			</div>
			<div class="modal-body">
				<input type="hidden" name="archive_task_id" id="archive_task_id" value="">
				<p>Do you want to archive this task?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default archive_task">yes</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	/*$(document).ready(function(){
      $(document).on("click", ".sche-clickevent", function () {
          $('.visible-schedule').fadeIn(200);
      });
  });*/
	/*
	  function alltask_delete(id)
	{
	   $('#delete_alltask'+id).show();
	   return false;
	}  

	function archieve_click(val){
	//  alert('ok');
	//  alert($(val).attr('id'));
	  $("#archive_task_id").val($(val).attr('id'));
	  //alert($("#archive_task_id").val());
	  //alert($("#my_Modal").attr('class'));
	 $("#my_Modal").modal({backdrop: 'static', keyboard: false,show: true });  
	}*/
</script>