<?php 
$class_name='';
$search_event='';
if($module=='notes')
{ 
$class_name='notes_section';
$search_event='Note';
$delete_class='notes_delete_function';
$edit_section='notes';
}
else if($module=='calllog')
{ 
$class_name='calllog_section';
$search_event='Calllog';
$delete_class='calllog_delete_function';
$edit_section='calllog';
}
else if($module=='logactivity')
{
$class_name='log_section';
$search_event='log';
$delete_class='log_delete_function';
$edit_section='log';
}
else if($module=='schedule')
{
$class_name='schedule_section';
$search_event='schedule';
$delete_class='schedule_delete_function';
$edit_section='schedule';
}

else if($module=='smslog')
{
$class_name='sms_section';
$search_event='Sms';
$delete_class='sms_delete_function';
$edit_section='smslog';
}

else if($module=='meeting')
{
$class_name='meeting_section';
$search_event='meeting';
$delete_class='meeting_delete_function';
$edit_section='meeting';
}
?>
<div class="card ser-timeline">
<div class="card-header">
</div>
<div class="card-header">
<div class="col-12">
<input class="form-control form-control-lg <?php echo $search_event; ?>-search_timeline" type="text" id="<?php echo $class_name; ?>" placeholder="Search">
</div>
</div>
<div class="card-block">
<div class="main-timeline">
<div class="cd-timeline cd-container overall_<?php echo $class_name; ?>" id="search_for_activity">
<!-- from db data loop start -->
<!-- updated date -->
<?php
$notes_section_data=$this->db->query("select * from timeline_services_notes_added where user_id=".$client_id." and module='".$module."' order by id desc ")->result_array();
foreach ($notes_section_data as $notes_key => $notes_value) {
?>
<div class="cd-timeline-block <?php echo $class_name; ?>">
<?php if($module=='notes'){ ?>
<div class="cd-timeline-icon bg-primary">
<i class="icofont icofont-ui-file"></i>
</div>
<?php } ?>
<?php if($module=='calllog'){ ?>
<div class="cd-timeline-icon bg-primary">
<i class="icofont icofont-phone-circle"></i>
</div>
<?php } ?>
<?php if($module=='logactivity'){ ?>
<div class="cd-timeline-icon bg-primary">
<i class="icofont icofont-ui-file"></i>
</div>
<?php } ?>
<?php if($module=='schedule'){ ?>
<div class="cd-timeline-icon bg-primary">
<i class="icofont icofont-clock-time"></i>
</div>
<?php } ?>
<!--<div class="cd-timeline-icon bg-primary">
<i class="icofont icofont-ui-file"></i>
</div>-->
<div class="cd-timeline-content card_main">
<div class="p-0">
<!--   <span class="notes_edit"><a href="javascript:void(0);" onclick="return confirm_delete('<?php echo $notes_value['id'];?>');"  >Delete</a></span><span class="notes_edit"> - </span><span class="notes_edit"><a href="javascript:void(0);" onclick="topFunction(<?php echo $notes_value['id']; ?>,'<?php echo $module; ?>')">Edit</a></span> -->
<?php if($_SESSION['permission']['Client_Section']['edit'] == '1' || $_SESSION['permission']['Client_Section']['delete'] == '1'){ ?>
<div class="btn-group dropdown-split-primary">
<button type="button" class="btn btn-primary  dropdown-toggle dropdown-toggle-split waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
Actions   
</button>
<div class="dropdown-menu">
<?php if($_SESSION['permission']['Client_Section']['delete'] == '1'){ ?>
<span class="notes_edit">
<a href="javascript:void(0);" data-id="<?php echo $notes_value['id'];?>" class="confirm_delete_timeline"  >Delete</a></span><?php } ?>
<?php if($_SESSION['permission']['Client_Section']['edit'] == '1'){ ?>
<span class="notes_edit"><a href="javascript:void(0);" onclick="topFunction(<?php echo $notes_value['id']; ?>,'<?php echo $edit_section;?>')">Edit</a></span><?php } ?>
</div>
</div>
<?php } ?>

</div>

<div class="p-20">
<h6><?php echo ($client_details[0]['crm_company_name'])?$client_details[0]['crm_company_name']:$this->Common_mdl->get_crm_name($client_details[0]['user_id']);?> - <?php echo $client_details[0]['crm_legal_form'];?></h6>
<div class="timeline-details">
<!-- <p class="m-t-0"><?php //echo  ?></p> -->
<?php echo $notes_value['notes']; ?>
</div>
</div>
<span class="cd-date"><?php if(isset($notes_value['created_time'])){ echo date('F j, Y, g:i a',$notes_value['created_time']); } ?></span>
<?php if($module=='notes'){ ?>
<span class="cd-details">Notes For <?php echo $client_services[$notes_value['services_type']]; ?></span>
<?php } ?>
<?php if($module=='calllog'){ ?>
<span class="cd-details">Call log Activity</span>
<?php } ?>
<?php if($module=='logactivity'){ ?>
<span class="cd-details">log Activity</span>
<?php } ?>
<?php if($module=='schedule'){ ?>
<span class="cd-details">Schedule Activity</span>
<?php } ?>
</div>
</div>
<?php } ?>
<!-- end of updated date -->

</div>
<div class="cd-timeline cd-container overall_for_<?php echo $class_name; ?>" id="search_for_activity" style="display: none;">
</div>
</div>
</div>
</div>
<!-- end -->