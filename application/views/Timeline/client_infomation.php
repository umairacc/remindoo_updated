<?php //print_r($contactRec);die; ?>
<?php $this->load->view('includes/header');?>


<style type="text/css">
  .client-details-tab01 #details .accordion-panel {
    width: 60%;
}
 .client-details-tab01 #details .accordion-panel1 {
    width: 40%;
    float: right;
}
.left-primary.right-primary {
    width: 30%;
    vertical-align: top;
    margin-left: -5px;
    padding-left: 0;
}
.make_a_primary, .make_a_primary .main-contact {
    float: left;
    position: relative;
    width: 60%;
    display: block;
    padding-left: 0;
}
.make_a_primary1, .make_a_primary1 .main-contact {
    float: left;
    position: relative;
    width: 40%;
    display: block;
    padding-left: 0;
}</style>
<?php
$res_suc=$this->session->flashdata('email_url_redirect');
?>
<link href="<?php echo base_url();?>assets/css/jquery.filer.css" type="text/css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/timepicki.css" rel="stylesheet">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/fullcalendar12.css"> 
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/fullcalendar.print.css" media='print'>
<section class="client-details-view hidden-user01 floating_set card-removes">
  <div class="modal-alertsuccess alert alert-success" style="display:none;"
    ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Your client was updated successfully.</div>  
  <div class="modal-alertsuccess alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Please fill the required fields.</div>
    <div class="information-tab floating_set">
      <div class="space-required">
    <div class="deadline-crm1 floating_set">
        <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url().'user';?>">All Clients</a>
            <div class="slide"></div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url().'client/addnewclient';?>">Add new client</a>
            <div class="slide"></div>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-popup-open="popup-1" href="#">Email all</a>
            <div class="slide"></div>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-popup-open="popup-2" href="#">Give feedback</a>
            <div class="slide"></div>
          </li>
        </ul>
      </div>
    </div>
    <div class="space-required">
      <div class="document-center client-infom-1 floating_set">
        <div class="Companies_House floating_set">
          <div class="pull-left">
<?php $cmy_role = $client[0]['status'];
//echo $cmy_role;
      if($cmy_role == 0) {
        $cmy_status = 'Manual'; 
      } else if($cmy_role == 1) {
        $cmy_status = 'Company House';
      }else if($cmy_role==2)
      {
        $cmy_status = 'Import';
      }
       else if($cmy_role==3){
        $cmy_status = 'From Lead';
      } ?>

<?php
 if(isset($client[0]['crm_company_name']) && $client[0]['crm_company_name']!=''){ 
  $client_name=$client[0]['crm_company_name'];
 }else{
    $client_name=$this->Common_mdl->get_crm_name($client[0]['user_id']);
 }
  echo '<h2>'.$client_name.' - '.$cmy_status.'</h2>';  ?>             
          </div>
          <div class="Footer common-clienttab pull-right">
            <div class="change-client-bts1 client_view_saveedit_button">        
            <a href="<?php echo base_url().'Client/client_info/'.$client[0]['user_id']; ?>" class="signed-change1">EDIT</a>
            </div>
            <div class="divleft">
              <button  class="signed-change2"  type="button" value="Previous Tab" text="Previous Tab" <?php /* if($res_suc==''){ ?> style="display: none;" <?php } */ ?> >Previous 
              </button>
            </div>
            <div class="divright">
              <button  class="signed-change3"  type="button" value="Next Tab"  text="Next Tab">Next
              </button>
            </div>
          </div>
        </div>
        <div class="addnewclient_pages1 floating_set">
          <ol class="nav nav-tabs all_user1 md-tabs floating_set" id="tabs">
          <?php
          $shown_next='next';
           if(isset($client[0]['crm_legal_form']) && ($client[0]['crm_legal_form']=='Private Limited company' || $client[0]['crm_legal_form']=='Public Limited company' || $client[0]['crm_legal_form']=='Limited Liability Partnership')) { 
            $shown_next='';
            ?>
            <li class="nav-item it0 bbs" value="0"><a class="nav-link <?php if($res_suc==''){ echo "active";} ?>" data-toggle="tab" 
              href="#details">Basic Details</a></li>
               <?php } ?>  
            <!-- <li class="nav-item it1" value="1"><a class="nav-link" data-toggle="tab" href="#contacts">Contacts</a></li> --> 
            <li class="nav-item it1" value="1"><a class="nav-link <?php if($shown_next!='' && $res_suc==''){ echo "active"; } ?> " data-toggle="tab" href="#contacts1"> Contact</a></li>
            <li class="nav-item it2 hide" value="2"><a class="nav-link <?php if($res_suc!='' ){ echo "active";} ?>" data-toggle="tab" href="#service"> Service</a></li>
            <li class="nav-item it2 hide" value="2"><a class="nav-link " data-toggle="tab" href="#assignto"> Assign to</a></li>
            <!-- 27-08-2018 -->
             <li class="nav-item it2 hide" value="2"><a class="nav-link " data-toggle="tab" href="#amlchecks"> AML Checks</a></li>
            <!-- end of 27-08-2018 -->
            <li class="nav-item it2 hide" value="2"><a class="nav-link" data-toggle="tab" href="#other"> Other</a></li>
            <li class="nav-item it2 hide" value="2"><a class="nav-link" data-toggle="tab" href="#referral"> Referral</a></li>
            <li class="nav-item it2 hide" value="2"><a class="nav-link" data-toggle="tab" href="#task"> Task</a></li>
            <li class="nav-item it2 hide" value="2"><a class="nav-link" data-toggle="tab" href="#leads"> Leads</a></li>
            <li class="nav-item it2 hide" value="2"><a class="nav-link" data-toggle="tab" href="#proposals"> Proposals</a></li>
            <li class="nav-item it2 hide" value="2"><a class="nav-link" data-toggle="tab" href="#invoice"> Invoice</a></li>
            <!-- <li class="nav-item it2 hide" value="2"><a class="nav-link" data-toggle="tab" href="#invoices">Invoices</a></li>
              <li class="nav-item it2 hide" value="2"><a class="nav-link" data-toggle="tab" href="#timeline">Timeline</a></li> -->
          <!--   <li class="nav-item it2 hide" value="2"><a class="nav-link" data-toggle="tab" href="#notes"> Notes</a></li> -->
          <!--   <li class="nav-item it2 hide" value="2"><a class="nav-link" data-toggle="tab" href="#timelineservices"> Timeline Services</a></li> -->
            <li class="nav-item it2 hide" value="2"><a class="nav-link" data-toggle="tab" href="#newtimelineservices"> Timeline Services</a></li>
<!--             <li class="nav-item it2 hide" value="2"><a class="nav-link" data-toggle="tab" href="#documents"><strong>8.</strong>Documents</a></li>
 -->            <!-- <li class="nav-item it2 hide" value="2"><a class="nav-link" data-toggle="tab" href="#deadlines">Deadlines</a></li>
              <li class="nav-item it2 hide" value="2"><a class="nav-link" data-toggle="tab" href="#referrals">Referrals</a></li> -->
            <?php 
              //$jsnCst = json_decode($client[0]['conf_statement']);
              (isset(json_decode($client[0]['conf_statement'])->tab) && $client[0]['conf_statement'] != '') ? $jsnCst =  json_decode($client[0]['conf_statement'])->tab : $jsnCst = '';
              
              ($jsnCst!='') ? $cst = "block" : $cst = "none";
              
              ?>
            <li class="nav-item it7 hide services_tabs_sec" value="2" style="display: <?php echo $cst;?>"><a class="nav-link" data-toggle="tab" href="#confirmation-statement"> Confirmation Statement</a></li>
            <?php 
              (isset(json_decode($client[0]['accounts'])->tab) && $client[0]['accounts'] != '') ? $jsnAcc =  json_decode($client[0]['accounts'])->tab : $jsnAcc = '';
              
              //$jsnacc = json_decode($client[0]['accounts']);
              ($jsnAcc !='') ? $acc = "block" : $acc = "none";
              
              ?>
            <li class="nav-item it6 hide services_tabs_sec" value="2" style="display: <?php echo $acc;?>;"><a class="nav-link" data-toggle="tab" href="#accounts"> Accounts</a></li>
            <?php 
              //$jsnpertax = json_decode($client[0]['personal_tax_return']);
              (isset(json_decode($client[0]['personal_tax_return'])->tab) && $client[0]['personal_tax_return'] != '') ? $jsnpertax =  json_decode($client[0]['conf_statement'])->tab : $jsnpertax = '';
              ($jsnpertax!='') ? $pertax = "block" : $pertax = "none";
              
              ?>
            <li class="nav-item it12 hide services_tabs_sec" value="2" style="display: <?php echo $pertax;?>;"><a class="nav-link" data-toggle="tab" href="#personal-tax-returns">  Personal Tax Return</a></li>
            <?php 
              //$jsnpay = json_decode($client[0]['payroll']);
              (isset(json_decode($client[0]['payroll'])->tab) && $client[0]['payroll'] != '') ? $jsnpay =  json_decode($client[0]['payroll'])->tab : $jsnpay = '';
              ($jsnpay!='') ? $pay = "block" : $pay = "none";
              
              ?>
            <li class="nav-item it11 hide services_tabs_sec" value="2" style="display: <?php echo $pay;?>;"><a class="nav-link" data-toggle="tab" href="#payroll">  Payroll</a></li>
            <?php 
              //$jsnvat = json_decode($client[0]['vat']);
              (isset(json_decode($client[0]['vat'])->tab) && $client[0]['vat'] != '') ? $jsnvat =  json_decode($client[0]['vat'])->tab : $jsnvat = '';
              ($jsnvat!='') ? $vat = "block" : $vat = "none";
              
              ?>
            <li class="nav-item it8 hide services_tabs_sec" value="2" style="display: <?php echo $vat;?>;"><a class="nav-link" data-toggle="tab" href="#vat-Returns" >  VAT Returns</a></li>
            <?php 
              //$jsnmanagement = json_decode($client[0]['management']);
              (isset(json_decode($client[0]['management'])->tab) && $client[0]['management'] != '') ? $jsnmanagement =  json_decode($client[0]['management'])->tab : $jsnmanagement = '';
              ($jsnmanagement!='') ? $management = "block" : $management = "none";
              
              ?>
            <li class="nav-item it13 hide services_tabs_sec" value="2" style="display: <?php echo $management;?>;"><a class="nav-link" data-toggle="tab" href="#management-account"> Management Accounts</a></li>
            <?php 
            /** 12-08-2018**/
            $investgate_block='';$investgate_none='';
              //$jsninvestgate = json_decode($client[0]['investgate']);
              (isset(json_decode($client[0]['investgate'])->tab) && $client[0]['investgate'] != '') ? $jsninvestgate =  json_decode($client[0]['investgate'])->tab : $jsninvestgate = '';
              ($jsninvestgate!='') ? $investgate_block = "block" : $investgate_none = "none";

                (isset(json_decode($client[0]['registered'])->tab) && $client[0]['registered'] != '') ? $jsninvestgate =  json_decode($client[0]['registered'])->tab : $jsninvestgate = '';
              ($jsninvestgate!='') ? $investgate_block = "block" : $investgate_none = "none";

                (isset(json_decode($client[0]['taxadvice'])->tab) && $client[0]['taxadvice'] != '') ? $jsninvestgate =  json_decode($client[0]['taxadvice'])->tab : $jsninvestgate = '';
              ($jsninvestgate!='') ? $investgate_block = "block" : $investgate_none = "none";

              if($investgate_block!='')
              {
                $investgate="block";
              }
              else
              {
                $investgate="none";
              }
              ?>
            <li class="nav-item it14 hide services_tabs_sec" value="2" style="display: <?php echo $investgate;?>;"><a class="nav-link" data-toggle="tab" href="#investigation-insurance">  Investigation Insurance</a></li>
          </ol>
        </div>
        <!-- tab close --> 
      </div>
    </div>
      <?php
        $rel_id=( isset($client) ? $client[0]['user_id'] : false);       
        ?>
      <div class="management_section client-details-tab01 data-mang-06  floating_set realign">
      
        <div class="tab-content card">
          <div id="detailss" class="tab-pane fade">
            <div class="search-client-details01 floating_set">
              <div class="pull-left detail-pull-left">
                <span>Details</span>
              </div>
              <div class="pull-right edit-tag1">   
                <a href="#"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
              </div>
            </div>
            <div id="accordion" role="tablist" aria-multiselectable="true">
              <div class="accordion-panel">
                <div class="box-division03">
                  <div class="accordion-heading" role="tab" id="headingOne">
                    <h3 class="card-title accordion-title">
                      <a class="accordion-msg" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Basic Info</a>
                    </h3>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse in show" role="tabpanel" aria-labelledby="headingOne">
                    <div class="basic-info-client1">
                      <table class="client-detail-table01">
                        <tr>
                          <th>Client ID</th>
                          <td><?php if(isset($user[0]['client_id'])){ echo $user[0]['client_id']; } ?></td>
                          <th>Account Office Reference</th>
                          <td><?php if(isset($client[0]['crm_accounts_office_reference'])){ echo $client[0]['crm_accounts_office_reference']; } ?></td>
                        </tr>
                        <tr>
                          <th>Client Type</th>
                          <td><?php if(isset($client[0]['crm_company_type'])){ echo $client[0]['crm_company_type']; } ?></td>
                          <th>PAYE Reference Number</th>
                          <td><?php if(isset($client[0]['crm_employers_reference'])){ echo $client[0]['crm_employers_reference']; } ?></td>
                        </tr>
                        <tr>
                          <th>Company Name</th>
                          <td><?php if(isset($client[0]['crm_company_name'])){ echo $client[0]['crm_company_name']; } ?></td>
                        </tr>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <!-- accordion1 -->
              <div class="accordion-panel">
                <div class="box-division03">
                  <div class="accordion-heading" role="tab" id="headingTwo">
                    <h3 class="card-title accordion-title">
                      <a class="accordion-msg" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseOne">Address</a>
                    </h3>
                  </div>
                  <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="basic-info-client1">
                      <table class="client-detail-table01">
                        <tr>
                          <th>Address Line 1</th>
                          <td><?php if(isset($company['registered_office_address']['address_line_1'])){ echo $company['registered_office_address']['address_line_1']; }  ?></td>
                          <th>Post Code</th>
                          <td><?php if(isset($company['registered_office_address']['postal_code'])){ echo $company['registered_office_address']['postal_code']; }  ?></td>
                        </tr>
                        <tr>
                          <th>Address Line 2</th>
                          <td><?php if(isset($company['registered_office_address']['address_line_2'])){ echo $company['registered_office_address']['address_line_2']; }  ?></td>
                          <th>country</th>
                          <td><?php if(isset($company['registered_office_address']['country'])){ echo $company['registered_office_address']['country']; }  ?></td>
                        </tr>
                        <tr>
                          <th>Town/City</th>
                          <td><?php if(isset($company['registered_office_address']['locality'])){ echo $company['registered_office_address']['locality']; }  ?></td>
                          <th>Region</th>
                          <td><?php if(isset($company['registered_office_address']['region'])){ echo $company['registered_office_address']['region']; }  ?></td>
                        </tr>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <!-- accordion1 -->
              <div class="accordion-panel">
                <div class="box-division03">
                  <div class="accordion-heading" role="tab" id="headingOne">
                    <h3 class="card-title accordion-title">
                      <a class="accordion-msg" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Contact Info</a>
                    </h3>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse in show" role="tabpanel" aria-labelledby="headingOne">
                    <div class="basic-info-client1">
                      <table class="client-detail-table01">
                        <tr>
                          <th>Phone</th>
                          <td><?php if(isset($client[0]['crm_telephone_number'])){ echo $client[0]['crm_telephone_number']; } ?></td>
                          <th>Website</th>
                          <td><?php if(isset($client[0]['crm_website'])){ echo $client[0]['crm_website']; } ?></td>
                        </tr>
                        <tr>
                          <th>Email</th>
                          <td><?php if(isset($client[0]['crm_email'])){ echo $client[0]['crm_email']; } ?></td>
                        </tr>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <!-- accordion1 -->
              <div class="accordion-panel">
                <div class="box-division03">
                  <div class="accordion-heading" role="tab" id="headingOne">
                    <h3 class="card-title accordion-title">
                      <a class="accordion-msg" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Business Info</a>
                    </h3>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse in show" role="tabpanel" aria-labelledby="headingOne">
                    <div class="basic-info-client1">
                      <table class="client-detail-table01">
                        <tr>
                          <th>Business Start Date</th>
                          <td><?php if(isset($company['date_of_creation'])){ echo $company['date_of_creation']; } ?></td>
                          <th>VAT Scheme</th>
                          <td></td>
                        </tr>
                        <tr>
                          <th>Book Start Date</th>
                          <td></td>
                          <th>VAT Submit Type</th>
                          <td></td>
                        </tr>
                        <tr>
                          <th>Year End Date</th>
                          <td><?php if(isset($company['accounts']['accounting_reference_date'])){ echo $company['accounts']['accounting_reference_date']['day'].'/'.$company['accounts']['accounting_reference_date']['month']; } ?></td>
                          <th>VAT Reg.No</th>
                          <td><?php if(isset($client[0]['vat_number'])){ echo $client[0]['vat_number']; } ?></td>
                        </tr>
                        <tr>
                          <th>Company Reg.No</th>
                          <td><?php if(isset($company['company_number'])){ echo $company['company_number']; } ?></td>
                          <th>VAT Reg.Date</th>
                          <td><?php if(isset($client[0]['crm_vat_date_of_registration'])){ echo $client[0]['crm_vat_date_of_registration']; } ?></td>
                        </tr>
                        <tr>
                          <th>UTR No</th>
                          <td><?php if(isset($client[0]['crm_company_utr'])){ echo $client[0]['crm_company_utr']; } ?></td>
                          <th>Company Status</th>
                          <td><?php if(isset($company['company_status'])){ echo $company['company_status']; } ?></td>
                        </tr>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <!-- accordion1 -->
            </div>
          </div>
          <!-- 1tab -->
          <div id="details" class="accord-proposal1 tab-pane fade in  <?php if($shown_next==''){ if($res_suc==''){ echo "active";} } ?>">
      <div class="masonry-container floating_set">
<div class="grid-sizer"></div>
            <div class="accordion-panel">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Important Information</a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1" id="important_details_section">

                  <?php if(isset($client[0]['crm_company_name']) && $client[0]['crm_company_name']!=''){  ?>
                    <div class="form-group row name_fields sorting" id="<?php echo $this->Common_mdl->get_order_details(1); ?>">
                      <?php $company_roles=(isset($client[0]['status'])&&$client[0]['status']==1)? 'readonly' :''?>
                      
                      <label class="col-sm-4 col-form-label">Company Name </label>
                      <div class="col-sm-8">
                        <!-- <input type="text" name="company_name" id="company_name" placeholder="" value="<?php if(isset($client[0]['crm_company_name'])){ echo $client[0]['crm_company_name']; } ?>" class="fields" <?php echo $company_roles;?>> -->
                        <span class="small-info"><?php if(isset($client[0]['crm_company_name'])){ echo $client[0]['crm_company_name']; } ?></span>
                      </div>
                    </div>
                    <?php } ?>


                      <?php if(isset($client[0]['crm_company_number']) && $client[0]['crm_company_number']!=''){ ?>
                    <div class="form-group row name_fields sorting" id="<?php echo $this->Common_mdl->get_order_details(2); ?>">
                      <label class="col-sm-4 col-form-label">Company Number</label>
                      <div class="col-sm-8">
                        <!-- <input type="text" name="company_number" id="company_number" placeholder="" value="<?php if(isset($company['company_number'])){ echo $company['company_number']; } ?>" class="fields" <?php echo $company_roles;?>> -->
                        <span class="small-info"><?php if(isset($client[0]['crm_company_number'])){ echo $client[0]['crm_company_number']; } ?></span>
                      </div>
                    </div>
                    <?php } ?>

                      <?php if(isset($client[0]['crm_company_url']) && $client[0]['crm_company_url']!=''){ ?>
                     <div class="form-group row name_fields sorting" id="<?php echo $this->Common_mdl->get_order_details(15); ?>">
                      <label class="col-sm-4 col-form-label">Company URL</label>
                      <div class="col-sm-8">
                        <span class="small-info"><?php if(isset($client[0]['crm_company_url'])){ echo $client[0]['crm_company_url']; } ?></span>
                      </div>
                    </div>
                    <?php } ?>


                      <?php if(isset($client[0]['crm_officers_url']) && $client[0]['crm_officers_url']!=''){ ?>
                     <div class="form-group row name_fields sorting" id="<?php echo $this->Common_mdl->get_order_details(16); ?>">
                      <label class="col-sm-4 col-form-label">Officers URL</label>
                      <div class="col-sm-8">
                        <span class="small-info"><?php if(isset($client[0]['crm_officers_url'])){ echo $client[0]['crm_officers_url']; } ?></span>
                      </div>
                    </div>
                    <?php } ?>

                    <?php if(isset($client[0]['crm_incorporation_date']) && $client[0]['crm_incorporation_date']!=''){ ?>
                    <div class="form-group row name_fields sorting" id="<?php echo $this->Common_mdl->get_order_details(3); ?>">
                      <label class="col-sm-4 col-form-label">Incorporation Date</label>
                      <div class="col-sm-8">
                       <span class="small-info"><?php if(isset($client[0]['crm_incorporation_date'])){ echo $client[0]['crm_incorporation_date']; } ?></span>
                      </div>
                    </div>
                    <?php } ?>

                      <?php if(isset($client[0]['crm_registered_in']) && $client[0]['crm_registered_in']!=''){ ?>
                    <div class="form-group row name_fields sorting" id="<?php echo $this->Common_mdl->get_order_details(4); ?>">
                      <label class="col-sm-4 col-form-label">Registered In</label>
                      <div class="col-sm-8">
                        <!-- <input type="text" name="registered_in" id="registered_in" placeholder="" value="<?php if(isset($company['jurisdiction'])){ echo $company['jurisdiction']; } ?>" class="fields" <?php echo $company_roles;?>> -->
                        <span class="small-info"><?php if(isset($client[0]['crm_registered_in'])){ echo $client[0]['crm_registered_in']; } ?></span>
                      </div>
                    </div>

                    <?php } ?>

                      <?php if(isset($client[0]['crm_address_line_one']) && $client[0]['crm_address_line_one']!=''){ ?>
                    <div class="form-group row name_fields sorting" id="<?php echo $this->Common_mdl->get_order_details(5); ?>">
                      <label class="col-sm-4 col-form-label">Address Line 1</label>
                      <div class="col-sm-8">
                        <!-- <input type="text" name="address_line_one" id="address_line_one" placeholder="" value="<?php if(isset($company['registered_office_address']['address_line_1'])){ echo $company['registered_office_address']['address_line_1']; }  ?>" class="fields" <?php echo $company_roles;?>> -->
                        <p class="for-address"><?php if(isset($client[0]['crm_address_line_one'])){ echo $client[0]['crm_address_line_one']; }  ?></p>
                      </div>
                    </div>
                    <?php } ?>

                    <?php if(isset($client[0]['crm_address_line_two']) && $client[0]['crm_address_line_two']!=''){ ?>

                    <div class="form-group row name_fields sorting"  id="<?php echo $this->Common_mdl->get_order_details(6); ?>">
                      <label class="col-sm-4 col-form-label">Address Line 2</label>
                      <div class="col-sm-8">
                        <!-- <input type="text" name="address_line_two" id="address_line_two" placeholder="" value="<?php if(isset($company['registered_office_address']['address_line_2'])){ echo $company['registered_office_address']['address_line_2']; }  ?>" class="fields" <?php echo $company_roles;?>> -->
                        <p class="for-address"><?php if(isset($client[0]['crm_address_line_two'])){ echo $client[0]['crm_address_line_two']; }  ?></p>
                      </div>
                    </div>

                    <?php } ?>

                    <?php if(isset($client[0]['crm_address_line_three']) && $client[0]['crm_address_line_three']!=''){ ?>
                    <div class="form-group row name_fields sorting" id="<?php echo $this->Common_mdl->get_order_details(7); ?>">
                      <label class="col-sm-4 col-form-label">Address Line 3</label>
                      <div class="col-sm-8">
                        <!-- <input type="text" name="address_line_three" id="address_line_three" placeholder="" value="<?php if(isset($company['registered_office_address']['address_line_3'])){ echo $company['registered_office_address']['address_line_3']; }  ?>" class="fields" <?php echo $company_roles;?>> -->
                        <p class="for-address"><?php if(isset($client[0]['crm_address_line_three'])){ echo $client[0]['crm_address_line_three']; }  ?></p>
                      </div>
                    </div>
                    <?php } ?>

                    <?php if(isset($client[0]['crm_town_city']) && $client[0]['crm_town_city']!=''){ ?>
                    <div class="form-group row name_fields sorting" id="<?php echo $this->Common_mdl->get_order_details(8); ?>">
                      <label class="col-sm-4 col-form-label">Town/City</label>
                      <div class="col-sm-8">
                        <!-- <input type="text" name="crm_town_city" id="crm_town_city" placeholder="" value="<?php if(isset($company['registered_office_address']['locality'])){ echo $company['registered_office_address']['locality']; }  ?>" class="fields" <?php echo $company_roles;?>> -->
                        <span class="small-info"><?php if(isset($client[0]['crm_town_city'])){ echo $client[0]['crm_town_city']; }  ?></span>
                      </div>
                    </div>
                    <?php } ?>

                    <?php if(isset($client[0]['crm_post_code']) && $client[0]['crm_post_code']!=''){ ?>
                    <div class="form-group row name_fields sorting" id="<?php echo $this->Common_mdl->get_order_details(9); ?>">
                      <label class="col-sm-4 col-form-label">Post Code</label>
                      <div class="col-sm-8">
                        <!-- <input type="text" name="crm_post_code" id="crm_post_code" placeholder="" value="<?php if(isset($company['registered_office_address']['postal_code'])){ echo $company['registered_office_address']['postal_code']; }  ?>" class="fields" <?php echo $company_roles;?>> -->
                        <span class="small-info"><?php if(isset($client[0]['crm_post_code'])){ echo $client[0]['crm_post_code']; }  ?></span>
                      </div>
                    </div>
                    <?php } ?>
                    <?php if(isset($client[0]['crm_company_status']) && $client[0]['crm_company_status']!=''){ ?>
                    <div class="form-group row name_fields sorting" id="<?php echo $this->Common_mdl->get_order_details(10); ?>">
                      <label class="col-sm-4 col-form-label">Company Status</label>
                      <div class="col-sm-8">
                        <!-- <input type="text" name="company_status" id="company_status" placeholder="" value="<?php if(isset($company['company_status'])){ echo $company['company_status']; } ?>" class="fields" <?php echo $company_roles;?>> -->
                        <span class="small-info"><?php if(isset($client[0]['crm_company_status'])){ echo $client[0]['crm_company_status']; } ?></span>
                      </div>
                    </div>
                    <?php } ?>
                    <?php if(isset($client[0]['crm_company_type']) && $client[0]['crm_company_type']!=''){ ?>
                    <div class="form-group row name_fields sorting" id="<?php echo $this->Common_mdl->get_order_details(11); ?>">
                      <label class="col-sm-4 col-form-label">Company Type</label>
                      <div class="col-sm-8">
                        <!-- <input type="text" name="company_type" id="company_type" placeholder="" value="<?php if(isset($company['type'])){ echo $company['type']; } ?>" class="fields" <?php echo $company_roles;?>> -->
                        <span class="small-info"><?php if(isset($client[0]['crm_company_type'])){ echo $client[0]['crm_company_type']; } ?></span>
                      </div>
                    </div>
                    <?php } ?>

                    <?php if(isset($client[0]['crm_company_sic']) && ($client[0]['crm_company_sic'][0]!='') ){ ?>

                    <div class="form-group row name_fields sorting" id="<?php echo $this->Common_mdl->get_order_details(12); ?>">
                      <label class="col-sm-4 col-form-label">Company S.I.C</label>
                      <div class="col-sm-8">
                        <!-- <input type="text" name="company_sic" id="company_sic" placeholder="" value="<?php if(isset($company['sic_codes']) && ($company['sic_codes'][0]!='') ){
                          $code=$this->db->query('SELECT * FROM sic_code WHERE COL1="'.$company['sic_codes'][0].'"')->row_array();
                          echo $code['COL1'].'-'.$code['COL2'];
                          
                                      }
                                          ?>" class="fields" <?php echo $company_roles;?>> -->
                                          <span class="small-info"><?php if(isset($client[0]['crm_company_sic']) && ($client[0]['crm_company_sic'][0]!='') ){
                          $code=$this->db->query('SELECT * FROM sic_code WHERE COL1="'.$client[0]['crm_company_sic'][0].'"')->row_array();
                          //echo $code['COL1'].'-'.$code['COL2'];
                          echo $client[0]['crm_company_sic'];
                          
                                      }
                                          ?></span>
                      </div>
                    </div>

                    <?php } ?>
                    <?php if(isset($client[0]['crm_letter_sign']) && $client[0]['crm_letter_sign']!=''){ ?>

                    <div class="form-group row name_fields sorting" id="<?php echo $this->Common_mdl->get_order_details(13); ?>">
                      <label class="col-sm-4 col-form-label">Letter of Enagagement Sign Date</label>
                      <div class="col-sm-8">
                        <!-- <input type="text" name="letter_sign" id="letter_sign" placeholder="" value="<?php if(isset($client[0]['crm_letter_sign'])){ echo $client[0]['crm_letter_sign']; } ?>" class="fields datepicker"> -->
                        <span class="small-info"><?php if(isset($client[0]['crm_letter_sign'])){ echo $client[0]['crm_letter_sign']; } ?></span>
                      </div>
                    </div>
                    <?php } ?>
                    <?php
                      $crm_business_website=( isset($client[0]['crm_business_website']) ? $client[0]['crm_business_website'] : false);  
                      $crm_business= render_custom_fields_edit( 'business_website',$rel_id,$crm_business_website);  
                      if($crm_business!='') {
                       $s = $this->Common_mdl->getCustomFieldRec($rel_id,'business_website');
                     // echo '<pre>';
                     // print_r($s);
                     //   echo '</pre>'; 
                       //exit;
                        if($s){
                          foreach ($s as $keys => $values) {
                            if($keys == ''){ $keys = '-'; }
                            if($values == ''){ $values = '-'; }
                          ?>
                          <div class="form-group row name_fields sorting" id="<?php echo $this->Common_mdl->get_order_details(14); ?>">
                            <label class="col-sm-4 col-form-label"><?php echo $keys;?></label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php echo $values;?></span>
                              </div>
                          </div>
                    <?php
                          }
                        }
                       } ?>
                    <?php
                      $crm_accounting_system=( isset($client[0]['crm_accounting_system']) ? $client[0]['crm_accounting_system'] : false);   
                      $crm_accounting=render_custom_fields_edit( 'accounting_system',$rel_id,$crm_accounting_system);  
                       if($crm_accounting!='') {
                      $s_accounting_system = $this->Common_mdl->getCustomFieldRec($rel_id,'accounting_system');
                     
                        if($s_accounting_system){
                          foreach ($s_accounting_system as $key_as => $value_as) {
                            if($key_as == ''){ $key_as = '-'; }
                            if($value_as == ''){ $value_as = '-'; }
                          ?>
                          <div class="form-group row name_fields sorting" id="<?php echo $this->Common_mdl->get_order_details(17); ?>">
                            <label class="col-sm-4 col-form-label"><?php echo $key_as;?></label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php echo $value_as;?></span>
                              </div>
                          </div>
                    <?php
                          }
                        } } ?>



                      <div class="form-group row name_fields sorting" id="<?php echo $this->Common_mdl->get_order_details(178); ?>">
                      <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(178); ?></label>
                      <div class="col-sm-8">
                      <?php if(isset($client[0]['crm_hashtag']) && $client[0]['crm_hashtag']!=''){ echo $client[0]['crm_hashtag'];  } ?>                     

                      </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>


            <div class="accordion-panel1">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Basic Details Important Information</a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1" id="important_details_section">

                  <?php if(isset($client[0]['crm_company_name']) && $client[0]['crm_company_name']!=''){  ?>
                    <div class="form-group row name_fields sorting" id="<?php echo $this->Common_mdl->get_order_details(1); ?>">
                      <?php $company_roles=(isset($client[0]['status'])&&$client[0]['status']==1)? 'readonly' :''?>
                      
                      <label class="col-sm-4 col-form-label">Company Name </label>
                      <div class="col-sm-8">
                        <!-- <input type="text" name="company_name" id="company_name" placeholder="" value="<?php if(isset($client[0]['crm_company_name'])){ echo $client[0]['crm_company_name']; } ?>" class="fields" <?php echo $company_roles;?>> -->
                        <span class="small-info"><?php if(isset($client[0]['crm_company_name'])){ echo $client[0]['crm_company_name']; } ?></span>
                      </div>
                    </div>
                    <?php } ?>


                      <?php if(isset($client[0]['crm_company_number']) && $client[0]['crm_company_number']!=''){ ?>
                    <div class="form-group row name_fields sorting" id="<?php echo $this->Common_mdl->get_order_details(2); ?>">
                      <label class="col-sm-4 col-form-label">Company Number</label>
                      <div class="col-sm-8">
                        <!-- <input type="text" name="company_number" id="company_number" placeholder="" value="<?php if(isset($company['company_number'])){ echo $company['company_number']; } ?>" class="fields" <?php echo $company_roles;?>> -->
                        <span class="small-info"><?php if(isset($client[0]['crm_company_number'])){ echo $client[0]['crm_company_number']; } ?></span>
                      </div>
                    </div>
                    <?php } ?>
                     <?php if(isset($client[0]['crm_address_line_one']) && $client[0]['crm_address_line_one']!=''){ ?>
                    <div class="form-group row name_fields sorting" id="<?php echo $this->Common_mdl->get_order_details(5); ?>">
                      <label class="col-sm-4 col-form-label">Address Line 1</label>
                      <div class="col-sm-8">
                        <!-- <input type="text" name="address_line_one" id="address_line_one" placeholder="" value="<?php if(isset($company['registered_office_address']['address_line_1'])){ echo $company['registered_office_address']['address_line_1']; }  ?>" class="fields" <?php echo $company_roles;?>> -->
                        <p class="for-address"><?php if(isset($client[0]['crm_address_line_one'])){ echo $client[0]['crm_address_line_one']; }  ?></p>
                      </div>
                    </div>
                    <?php } ?>

                    <?php if(isset($client[0]['crm_address_line_two']) && $client[0]['crm_address_line_two']!=''){ ?>

                    <div class="form-group row name_fields sorting"  id="<?php echo $this->Common_mdl->get_order_details(6); ?>">
                      <label class="col-sm-4 col-form-label">Address Line 2</label>
                      <div class="col-sm-8">
                        <!-- <input type="text" name="address_line_two" id="address_line_two" placeholder="" value="<?php if(isset($company['registered_office_address']['address_line_2'])){ echo $company['registered_office_address']['address_line_2']; }  ?>" class="fields" <?php echo $company_roles;?>> -->
                        <p class="for-address"><?php if(isset($client[0]['crm_address_line_two'])){ echo $client[0]['crm_address_line_two']; }  ?></p>
                      </div>
                    </div>
                    <?php } ?>
                    <?php if(isset($client[0]['crm_company_status']) && $client[0]['crm_company_status']!=''){ ?>
                    <div class="form-group row name_fields sorting" id="<?php echo $this->Common_mdl->get_order_details(10); ?>">
                      <label class="col-sm-4 col-form-label">Company Status</label>
                      <div class="col-sm-8">
                        <!-- <input type="text" name="company_status" id="company_status" placeholder="" value="<?php if(isset($company['company_status'])){ echo $company['company_status']; } ?>" class="fields" <?php echo $company_roles;?>> -->
                        <span class="small-info"><?php if(isset($client[0]['crm_company_status'])){ echo $client[0]['crm_company_status']; } ?></span>
                      </div>
                    </div>
                    <?php } ?>
                    <?php if(isset($client[0]['crm_company_type']) && $client[0]['crm_company_type']!=''){ ?>
                    <div class="form-group row name_fields sorting" id="<?php echo $this->Common_mdl->get_order_details(11); ?>">
                      <label class="col-sm-4 col-form-label">Company Type</label>
                      <div class="col-sm-8">
                        <!-- <input type="text" name="company_type" id="company_type" placeholder="" value="<?php if(isset($company['type'])){ echo $company['type']; } ?>" class="fields" <?php echo $company_roles;?>> -->
                        <span class="small-info"><?php if(isset($client[0]['crm_company_type'])){ echo $client[0]['crm_company_type']; } ?></span>
                      </div>
                    </div>
                    <?php } ?>

                    <?php if(isset($client[0]['crm_ch_accounts_next_due']) && ($client[0]['crm_ch_accounts_next_due']!='') ){ ?>
<div class="form-group row name_fields  accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(95); ?>">
<label class="col-sm-4 col-form-label">Accounts Due Date - Companies House</label>
<div class="col-sm-8">
<!-- <input type="text" name="accounts_next_due" id="accounts_next_due" placeholder="" value="<?php if(isset($company['accounts']['next_due'])){ echo $company['accounts']['next_due']; }  ?>" class="fields datepicker"> -->
<span class="small-info"><?php if(isset($client[0]['crm_ch_accounts_next_due']) && ($client[0]['crm_ch_accounts_next_due']!='') ){ echo date("d-m-Y", strtotime($client[0]['crm_ch_accounts_next_due']));}?></span>
</div>
</div>
<?php } ?>


<?php if(isset($client[0]['crm_personal_tax_return_date']) && ($client[0]['crm_personal_tax_return_date']!='') ){ ?>
<div class="form-group row name_fields personal_sort" id="<?php echo $this->Common_mdl->get_order_details(33); ?>">
<label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Tax Return Date</label>
<div class="col-sm-8">                      
<span class="small-info"><?php if(isset($client[0]['crm_personal_tax_return_date']) && ($client[0]['crm_personal_tax_return_date']!='') ){ echo $client[0]['crm_personal_tax_return_date'];}?></span>
</div>
</div>
<?php } ?>

<?php if(isset($client[0]['crm_personal_due_date_return']) && ($client[0]['crm_personal_due_date_return']!='') ){ ?>
<div class="form-group row name_fields personal_sort" id="<?php echo $this->Common_mdl->get_order_details(34); ?>">
<label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(34); ?></label>
<div class="col-sm-8">                      
<span class="small-info"><?php if(isset($client[0]['crm_personal_due_date_return']) && ($client[0]['crm_personal_due_date_return']!='') ){ echo $client[0]['crm_personal_due_date_return'];}?></span>
</div>
</div>
<?php } ?>


<?php if(isset($client[0]['crm_confirmation_statement_due_date']) && ($client[0]['crm_confirmation_statement_due_date']!='') ){  ?>  
<div class="form-group row name_fields sorting_conf" id="<?php echo $this->Common_mdl->get_order_details(24); ?>">
<label class="col-sm-4 col-form-label">Confirmation statement Due By</label>
<div class="col-sm-8 edit-field-popup1">
<?php if(isset($client[0]['crm_confirmation_statement_due_date']) && ($client[0]['crm_confirmation_statement_due_date']!='') ){ echo date("d-m-Y", strtotime($client[0]['crm_confirmation_statement_due_date']));}?>
</div>
</div>
<?php } ?>


<?php if(isset($client[0]['crm_vat_due_date']) && ($client[0]['crm_vat_due_date']!='') ){ ?>
<div class="form-group row help_icon date_birth vat_sort" id="<?php echo $this->Common_mdl->get_order_details(143); ?>">
<label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>VAT Due Date</label>
<div class="col-sm-8">
<?php if(isset($client[0]['crm_vat_due_date']) && ($client[0]['crm_vat_due_date']!='') ){ echo $client[0]['crm_vat_due_date'];}?>
</div>
</div>
<?php } ?>


<?php if(isset($client[0]['crm_rti_deadline']) && ($client[0]['crm_rti_deadline']!='') ){ ?>
<div class="form-group row  date_birth payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(112); ?>">
<label class="col-sm-4 col-form-label">RTI Due/Deadline Date</label>
<div class="col-sm-8">
<?php if(isset($client[0]['crm_rti_deadline']) && ($client[0]['crm_rti_deadline']!='') ){ echo $client[0]['crm_rti_deadline'];}?>
</div>
</div>
<?php } ?>

<?php if(isset($client[0]['crm_pension_subm_due_date']) && ($client[0]['crm_pension_subm_due_date']!='') ){ ?>
<div class="form-group row  date_birth workplace" id="<?php echo $this->Common_mdl->get_order_details(120); ?>">
<label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Pension Submission Due date</label>
<div class="col-sm-8">
<?php if(isset($client[0]['crm_pension_subm_due_date']) && ($client[0]['crm_pension_subm_due_date']!='') ){ echo $client[0]['crm_pension_subm_due_date'];}?>
</div>
</div>
<?php } ?>


<?php if(isset($client[0]['crm_next_p11d_return_due']) && ($client[0]['crm_next_p11d_return_due']!='') ){  ?>
<div class="form-group row date_birth p11d" id="<?php echo $this->Common_mdl->get_order_details(135); ?>">
<label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>P11D Due date</label>
<div class="col-sm-8">
<?php if(isset($client[0]['crm_next_p11d_return_due']) && ($client[0]['crm_next_p11d_return_due']!='') ){ echo $client[0]['crm_next_p11d_return_due'];}?>
</div>
</div>
<?php } ?>

<?php if(isset($client[0]['crm_next_manage_acc_date']) && ($client[0]['crm_next_manage_acc_date']!='') ){ ?><
<div class="form-group row help_icon date_birth management" id="<?php echo $this->Common_mdl->get_order_details(54); ?>">
<label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Next Management Accounts Due date</label>
<div class="col-sm-8">

<span class="small-info"><?php if(isset($client[0]['crm_next_manage_acc_date']) && ($client[0]['crm_next_manage_acc_date']!='') ){ echo $client[0]['crm_next_manage_acc_date'];}?></span>
</div>
</div>
<?php } ?>

<?php if(isset($client[0]['crm_next_booking_date']) && ($client[0]['crm_next_booking_date']!='') ){ ?>
<div class="form-group row help_icon date_birth management" id="<?php echo $this->Common_mdl->get_order_details(50); ?>">
<label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Next Bookkeeping Date</label>
<div class="col-sm-8">
<?php if(isset($client[0]['crm_next_booking_date']) && ($client[0]['crm_next_booking_date']!='') ){ echo $client[0]['crm_next_booking_date'];}?>
</div>
</div>
<?php } ?>


<?php if(isset($client[0]['crm_insurance_renew_date']) && ($client[0]['crm_insurance_renew_date']!='') ){ ?>
<div class="form-group row help_icon date_birth invest" id="<?php echo $this->Common_mdl->get_order_details(59); ?>">
<label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Insurance Renew Date</label>
<div class="col-sm-8">

<span class="small-info"><?php if(isset($client[0]['crm_insurance_renew_date']) && ($client[0]['crm_insurance_renew_date']!='') ){ echo $client[0]['crm_insurance_renew_date'];}?></span>
</div>
</div>
<?php } ?>

<?php if(isset($client[0]['crm_registered_renew_date']) && ($client[0]['crm_registered_renew_date']!='') ){ ?>
<div class="form-group row help_icon date_birth invest" id="<?php echo $this->Common_mdl->get_order_details(63); ?>">
<label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Registered Office Renew Date</label>
<div class="col-sm-8">

<span class="small-info"><?php if(isset($client[0]['crm_registered_renew_date']) && ($client[0]['crm_registered_renew_date']!='') ){ echo $client[0]['crm_registered_renew_date'];}?></span>
</div>
</div>
<?php } ?>

<?php if(isset($client[0]['crm_investigation_end_date']) && ($client[0]['crm_investigation_end_date']!='') ){ ?>
<div class="form-group row help_icon date_birth">
<label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>End Date</label>
<div class="col-sm-8">

<span class="small-info"><?php if(isset($client[0]['crm_investigation_end_date']) && ($client[0]['crm_investigation_end_date']!='') ){ echo $client[0]['crm_investigation_end_date'];}?></span>
</div>
</div>
<?php } ?>

                   
                 
                    
                  </div>
                </div>
              </div>
            </div>

      </div>
            <!-- accordion-panel -->
          </div>
          <!-- VAT Returns -->
          <div id="contacts" class="tab-pane fade">
            <?php if(isset($officers)) {
              foreach($content as $key => $value){ 
               ?>
            <div class="accordion-panel">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg"><?php echo $value['name']; ?></a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1">
                    <table class="client-detail-table01">
                      <tr>
                        <th>Basic Info</th>
                        <th>Contact Details</th>
                      </tr>
                      <tr>
                        <th>contact type</th>
                        <td><?php echo $value['officer_role']; ?></td>
                        <th>Address1</th>
                        <td><?php echo $value['address']['address_line_1']; ?></td>
                      </tr>
                      <tr>
                        <th>First name</th>
                        <td><?php echo $value['name_elements']['forename']; ?></td>
                        <th>Down/City</th>
                        <td><?php echo $value['address']['locality']; ?></td>
                      </tr>
                      <tr>
                        <th>Last Name</th>
                        <td><?php echo $value['name_elements']['surname']; ?></td>
                        <th>Post Code</th>
                        <td><?php echo $value['address']['postal_code']; ?></td>
                      </tr>
                      <tr>
                        <th>Nationality</th>
                        <td><?php echo $value['nationality']; ?></td>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <!-- accordion1 -->
            <?php   } } ?>
            <!-- <div class="accordion-panel">
              <div class="accordion-heading" role="tab" id="headingOne">
               <h3 class="card-title accordion-title">
               <a class="accordion-msg">History</a></h3>
              </div>
              <div id="collapse" class="panel-collapse">
               <div class="basic-info-client1">
                  <table class="client-detail-table01">
                     <tr>
                        <th>Date</th>
                        <th>Action</th>
                        <th>Category</th>
                        <th>Description</th>
                        <th>Imported</th>
                     </tr>
                     <?php if(isset($array_rec3['items'])){ 
                foreach($array_rec3['items'] as $key => $value){
                ?>
                     <tr>
                  
                        <td><?php echo $value['date']; ?></td>
                        <td><?php echo $value['action_date']; ?></td>
                        <td><?php echo $value['category']; ?></td>
                        <td><a href="<?php echo 'https://beta.companieshouse.gov.uk'.$value['links']['self'].'/document?format=pdf&download=0'; ?>"  target="_blank"><?php echo $value['description']; ?></a></td>
                        <td></td>
                     </tr>
                     
                     <?php } } ?>
                  </table> 
               </div>         
              </div>
              </div> -->  <!-- accordion1 -->
          </div>
          <div id="contacts1" class="tab-pane fade <?php  if($shown_next=='next' && $res_suc==''){ echo "active"; } ?>">
          
            <div id="contactss"></div>
           
            <?php 
              $i=1;
              
              /*echo "<pre>";
              print_r($contactRec);die;*/
              foreach ($contactRec as $contactRec_key => $contactRec_value) {
              
                        $title = $contactRec_value['title'];
                        $first_name = $contactRec_value['first_name'];
                        $middle_name = $contactRec_value['middle_name'];
                        $surname = $contactRec_value['surname']; 
                        $preferred_name = $contactRec_value['preferred_name']; 
                        $mobile = $contactRec_value['mobile']; 
                        $main_email = $contactRec_value['main_email']; 
                        $nationality = $contactRec_value['nationality']; 
                        $psc = $contactRec_value['psc']; 
                        $shareholder = $contactRec_value['shareholder']; 
                        $ni_number = $contactRec_value['ni_number']; 
                        $contact_type = $contactRec_value['contact_type']; 
                        $address_line1 = $contactRec_value['address_line1']; 
                        $address_line2 = $contactRec_value['address_line2']; 
                        $town_city = $contactRec_value['town_city']; 
                        $post_code = $contactRec_value['post_code']; 
                        $landline = $contactRec_value['landline']; 
                        $work_email   = $contactRec_value['work_email']; 
                        $date_of_birth   = $contactRec_value['date_of_birth']; 
                        $nature_of_control   = $contactRec_value['nature_of_control']; 
                        $marital_status   = $contactRec_value['marital_status']; 
                        $utr_number   = $contactRec_value['utr_number']; 
                        $id   = $contactRec_value['id']; 
                        $client_id   = $contactRec_value['client_id']; 
                        $make_primary   = $contactRec_value['make_primary']; 
              
                        $contact_names = $this->Common_mdl->numToOrdinalWord($i).' Contact ';
                        $name=ucfirst($contactRec_value['first_name']).' '.ucfirst($contactRec_value['surname']);
                    ?>
          <div class="space-required">
            <div class="make_a_primary make-mdl-01">
              <div class="client-info-circle1 floating_set">
                <div class="accordion-panel remove<?php echo $id;?> contactcc ">
                  <div class="accordion-heading" role="tab" id="headingOne">
                    <h3 class="card-title accordion-title">
                      <a class="accordion-msg"><?php echo $name;?></a>
                      <!-- <a href="#" data-toggle="modal" data-target="#modalcontact<?php echo $id;?>"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a> -->
                    </h3>
                  </div>
                  <!-- <div class="data-primary-03 floating_set form-radio">
                    <?php if($make_primary=='0'){?>
                    <span class="make_primary" onclick="return make_a_primary(<?php echo $id.','.$client_id;?>);">Make a Primary Contact</span>
                    <?php } ?>
                  </div> -->
                  <div class="primary-info03 floating_set">
                    <div id="collapse" class="panel-collapse">
                      <div class="basic-info-client1">
                        <div class="left-primary remove-space-set1">
                        <?php if(isset($title) && $title!='') { ?>
                          <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Title</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php if(isset($title)) { echo $title; }?></span>
                              </div> 
                          </div>
                          <?php } ?>


                      <?php if(isset($first_name) && $first_name!='') { ?>
                            <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">First Name</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php echo $first_name;?></span>
                              </div>
                           </div>
                           <?php } ?>

                            <?php if(isset($middle_name) && $middle_name!='') { ?>
                            <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Middle Name</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php echo $middle_name;?></span>
                              </div>
                           </div>
                           <?php } ?>
                            <?php if(isset($surname) && $surname!='') { ?>
                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Surname</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php echo $surname;?></span>
                              </div>
                           </div>
                           <?php } ?>
                             <?php if(isset($preferred_name) && $preferred_name!='') { ?>
                          <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Prefered Name</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php echo $preferred_name;?></span>
                              </div>
                           </div>
                           <?php } ?>
                            <?php if(isset($mobile) && $mobile!='') { ?>
                      <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Mobile</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php echo $mobile;?></span>
                              </div>
                              </div>
                              <?php } ?>

                               <?php if(isset($main_email) && $mani_email!='') { ?>
                            <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Main E-Mail address</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php echo $main_email;?></span>
                              </div>
                           </div>
                           <?php } ?>
                            <?php if(isset($nationality) && $nationality!='') { ?>

                            <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Nationality</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php echo $nationality;?></span>
                              </div>
                           </div>
                           <?php } ?> 
                            <?php if(isset($psc) && $psc!='') { ?>
                        <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">PSC</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php echo $psc;?></span>
                              </div>
                           </div>
                           <?php } ?>

                            <?php if(isset($shareholder) && $shareholder!='') { ?>
                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Shareholder</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php echo $shareholder;?></span>
                              </div>
                           </div>
                           <?php } ?>
                            <?php if(isset($ni_number) && $ni_number!='') { ?>
                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">National Insurance Number</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php echo $ni_number;?></span>
                              </div>
                           </div>
                           <?php } ?>

                            <?php if(isset($contactRec_value['country_of_residence']) && $contactRec_value['country_of_residence']!='') { ?>
                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Country Of Residence</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php if(isset($contactRec_value['country_of_residence']) && ($contactRec_value['country_of_residence']!='') ){ echo $contactRec_value['country_of_residence'];}?></span>
                              </div>
                           </div>
                     
                        <?php } ?>
                        </div>
                         <div class="left-primary right-primary">
                         <?php if(isset($contact_type) && $contact_type!='') { ?>
                       
                        
                            <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Contact Type</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php echo $contact_type;?></span>
                              </div>
                           </div>
                           <?php } ?>
                            <?php if(isset($contactRec_value['other_custom']) && $contactRec_value['other_custom']!='') { ?>
                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Other(Custom)</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php if(isset($contactRec_value['other_custom']) && ($contactRec_value['other_custom']!='') ){ echo $contactRec_value['other_custom'];}?></span>
                              </div>
                           </div> 
                           <?php } ?>
                            <?php if(isset($address_line1) && $address_line1!='') { ?>

                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Address Line1</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php  echo $address_line1;?></span>
                              </div>
                           </div>
                           <?php } ?>
                            <?php if(isset($address_line2) && $address_line2!='') { ?>
                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Address line2</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php  echo $address_line2;?></span>
                              </div>
                           </div>
                           <?php } ?>
                            <?php if(isset($town_city) && $town_city!='') { ?>
                          <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Town/City</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php  echo $town_city;?></span>
                              </div>
                           </div>
                           <?php } ?>
                            <?php if(isset($post_code) && $post_code!='') { ?>
                         <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Post Code</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php  echo $post_code;?></span>
                              </div>
                           </div> 

                        <?php } ?>

                       
                         
                          <?php 
                            $land=json_decode($contactRec_value['landline']);
                            //print_r( $land);
                            if(!empty($land)){
                            foreach($land as $key =>$val) {
                            ?>
                          
                             <?php if(isset($val) && $val!='') { ?>
                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Landline</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php  echo $val;?></span>
                              </div>
                           </div>

                          <?php }} } ?>
                         
                          <?php 
                            $work=json_decode($contactRec_value['work_email']);
                                                    if(!empty($land)){
                            foreach($work as $key =>$val) {
                            ?>
                             <?php if(isset($val) && $val!='') { ?>
                          
                          <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Work email</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php  echo $val;?></span>
                              </div>
                           </div>

                          <?php } } } ?>
                             <?php if(isset($date_of_birth) && $date_of_birth!='') { ?>

                        <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Date of Birth</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php  echo $date_of_birth;?></span>
                              </div>
                           </div>
                           <?php } ?>

                             <?php if(isset($nature_of_control) && $nature_of_control!='') { ?>

                              <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Nature of Control</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php  echo $nature_of_control;?></span>
                              </div>
                           </div>
                           <?php } ?>
                             <?php if(isset($marital_statusl) && $marital_status!='') { ?>
                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Marital Status</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php  echo $marital_status;?></span>
                              </div>
                           </div>
                           <?php } ?>
                             <?php if(isset($utr_number) && $utr_number!='') { ?>
                          <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Utr number</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php  echo $utr_number;?></span>
                              </div>
                           </div>
                           <?php } ?>

                           <?php if(isset($contactRec_value['occupation']) && ($contactRec_value['occupation']!='') ){  ?>
                        <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Occupation</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php if(isset($contactRec_value['occupation']) && ($contactRec_value['occupation']!='') ){ echo $contactRec_value['occupation'];}?></span>
                              </div>
                           </div>
                           <?php } ?>
                            <?php if(isset($contactRec_value['appointed_on']) && ($contactRec_value['appointed_on']!='') ){  ?>
                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Appointed On</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php if(isset($contactRec_value['appointed_on']) && ($contactRec_value['appointed_on']!='') ){ echo $contactRec_value['appointed_on'];}?></span>
                              </div>
                           </div>
                           <?php } ?>  
                       
                        </div>
                      </div>
                      <!-- <div class="sav-btn">
                        <span id="succ<?php echo $id;?>" class="succ" style="color:green; display:none;">Contact Updated successfully!!!</span>
                        <input type="button" value="save" class="contactupdate" id="perferred_name<?php echo $id;?>"  data-id="<?php echo $id;?>">
                        </div> -->
                    </div>
                  </div>
                </div>
              </div>
            </div>

             <?php
                  if($make_primary=='1'){ ?>
            <div class="make_a_primary1 make-mdl-01">
              <div class="client-info-circle1 floating_set">
                <div class="accordion-panel remove<?php echo $id;?> contactcc ">
                  <div class="accordion-heading" role="tab" id="headingOne">
                    <h3 class="card-title accordion-title">
                      <a class="accordion-msg"><?php echo $name;?>-Primary Contact</a>
                    
                    </h3>
                  </div>
                  <!-- <div class="data-primary-03 floating_set form-radio">
                    <?php if($make_primary=='0'){?>
                    <span class="make_primary" onclick="return make_a_primary(<?php echo $id.','.$client_id;?>);">Make a Primary Contact</span>
                    <?php } ?>
                  </div> -->

                 
                  <div class="primary-info03 floating_set">
                    <div id="collapse" class="panel-collapse">
                      <div class="basic-info-client1">
                      
                        <?php if(isset($title) && $title!='') { ?>
                          <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Title</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php if(isset($title)) { echo $title; }?></span>
                              </div> 
                          </div>
                          <?php } ?>


                      <?php if(isset($first_name) && $first_name!='') { ?>
                            <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">First Name</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php echo $first_name;?></span>
                              </div>
                           </div>
                           <?php } ?>                           
                          
                            <?php if(isset($mobile) && $mobile!='') { ?>
                      <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Mobile</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php echo $mobile;?></span>
                              </div>
                              </div>
                              <?php } ?>

                               <?php if(isset($main_email) && $mani_email!='') { ?>
                            <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Main E-Mail address</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php echo $main_email;?></span>
                              </div>
                           </div>
                           <?php } ?>
                            <?php if(isset($nationality) && $nationality!='') { ?>

                            <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Nationality</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php echo $nationality;?></span>
                              </div>
                           </div>
                           <?php } ?> 
                          

                            <?php if(isset($shareholder) && $shareholder!='') { ?>
                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Shareholder</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php echo $shareholder;?></span>
                              </div>
                           </div>
                           <?php } ?>
                            <?php if(isset($ni_number) && $ni_number!='') { ?>
                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">National Insurance Number</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php echo $ni_number;?></span>
                              </div>
                           </div>
                           <?php } ?>

                         
                       <!--  </div> -->
                        
                         <?php if(isset($contact_type) && $contact_type!='') { ?>
                       
                        
                            <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Contact Type</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php echo $contact_type;?></span>
                              </div>
                           </div>
                           <?php } ?>                 
                      
                         
                      
                           

                       
                         
                          <?php 
                            $land=json_decode($contactRec_value['landline']);
                            //print_r( $land);
                            if(!empty($land)){
                            foreach($land as $key =>$val) {
                            ?>
                          
                             <?php if(isset($val) && $val!='') { ?>
                           <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Landline</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php  echo $val;?></span>
                              </div>
                           </div>

                          <?php }} } ?>
                         
                          <?php 
                            $work=json_decode($contactRec_value['work_email']);
                                                    if(!empty($land)){
                            foreach($work as $key =>$val) {
                            ?>
                             <?php if(isset($val) && $val!='') { ?>
                          
                          <div class="form-group row name_fields">
                              <label class="col-sm-4 col-form-label">Work email</label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php  echo $val;?></span>
                              </div>
                           </div>

                          <?php } } } ?>
                         

                        

                          
                          
                       
                     <!--    </div> -->
                      </div>
                      <!-- <div class="sav-btn">
                        <span id="succ<?php echo $id;?>" class="succ" style="color:green; display:none;">Contact Updated successfully!!!</span>
                        <input type="button" value="save" class="contactupdate" id="perferred_name<?php echo $id;?>"  data-id="<?php echo $id;?>">
                        </div> -->
                    </div>
                  </div>
                
                </div>
              </div>
            </div>
  <?php }else{ "There is no Primary Contacts"; } ?>






      </div>
            <input type="hidden" name="make_primary[]" id="make_primary[]" value="<?php echo $make_primary;?>">
            <?php $i++;  } ?> 
            <input type="hidden" name="incre" id="incre" value="<?php echo $i-1;?>">
            <!--                                   </form>
              -->
            <div class="contact_form"></div>
            <input type="hidden" name="append_cnt" id="append_cnt" value="">
          </div>
          <!-- contact1 -->
     
          <div id="service" class="tab-pane fade  <?php if($res_suc!=''){ echo "active";}  ?>">
            <div class="space-required">
      <div class="basic-available-data">
              <div class="basic-available-data1">
                <table class="client-detail-table01 service-table-client">
                  <thead>
                    <tr>
                      <th>Services</th>
                      <th>( Services )Enable/Disable</th>
                      <th>( Reminders )Enable/Disable</th>
                      <th>( Text )Enable/Disable</th>
                      <th>( Invoice Notifications )Enable/Disable</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <?php 
                        //!empty(json_decode($client[0]['conf_statement'])['tab']) ? $cst = "checked" : $cst = "";
                        
                        //$jsnCst = json_decode($client[0]['conf_statement']);
                        (isset(json_decode($client[0]['conf_statement'])->tab) && $client[0]['conf_statement'] != '') ? $jsnCst =  json_decode($client[0]['conf_statement'])->tab : $jsnCst = '';
                        ($jsnCst!='') ? $cst = "checked='checked'" : $cst = "";
                        //print_r($jsnCst);
                        //echo $cst;
                        (isset(json_decode($client[0]['conf_statement'])->reminder) && $client[0]['conf_statement'] != '') ? $jsnCstre =  json_decode($client[0]['conf_statement'])->reminder : $jsnCstre = '';
                        ($jsnCstre!='') ? $cstre = "checked='checked'" : $cstre = "";
                        (isset(json_decode($client[0]['conf_statement'])->text) && $client[0]['conf_statement'] != '') ? $jsnCsttext =  json_decode($client[0]['conf_statement'])->text : $jsnCsttext = '';
                        ($jsnCsttext!='') ? $csttext = "checked='checked'" : $csttext = "";
                        
                        ($jsnCsttext!='') ? $onet = "" : $onet = "display:none;";
                        
                        (isset(json_decode($client[0]['conf_statement'])->invoice) && $client[0]['conf_statement'] != '') ? $jsnCstinvoice =  json_decode($client[0]['conf_statement'])->invoice : $jsnCstinvoice = '';
                        ($jsnCstinvoice!='') ? $cstinvoice = "checked='checked'" : $cstinvoice = "";
                        ?>
                      <td>Confirmation Statement</td>
                      <td class="switching"><input type="checkbox" class="js-small f-right services" name="confirm[tab]" data-id="cons" <?php if($client[0]['blocked_status']==0){ echo $cst; } ?> readonly></td>
                      <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="confirm[reminder]" <?php if($client[0]['blocked_status']==0 && $client[0]['reminder_block']==0){ echo $cstre; } ?> data-id="enableconf" readonly></td>
                      <td class="swi2 textwitch"><input type="checkbox" class="js-small f-right text" name="confirm[text]" <?php if($client[0]['blocked_status']==0){ echo $csttext; } ?> data-id="onet" readonly></td>
                      <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="confirm[invoice]" <?php if($client[0]['blocked_status']==0){ echo $cstinvoice; } ?> readonly></td>
                    </tr>
                    <tr>
                      <?php 
                        (isset(json_decode($client[0]['accounts'])->tab) && $client[0]['accounts'] != '') ? $jsnAcc =  json_decode($client[0]['accounts'])->tab : $jsnAcc = '';
                        //$jsnacc = json_decode($client[0]['accounts']);
                        ($jsnAcc!='') ? $acc = "checked='checked'" : $acc = "";
                        
                        (isset(json_decode($client[0]['accounts'])->reminder) && $client[0]['accounts'] != '') ? $jsnAccre =  json_decode($client[0]['accounts'])->reminder : $jsnAccre = '';
                        ($jsnAccre!='') ? $accre = "checked='checked'" : $accre = "";
                        (isset(json_decode($client[0]['accounts'])->text) && $client[0]['accounts'] != '') ? $jsnAcctext =  json_decode($client[0]['accounts'])->text : $jsnAcctext = '';
                        ($jsnAcctext!='') ? $acctext = "checked='checked'" : $acctext = "";
                        ($jsnAcctext!='') ? $twot = "" : $twot = "display:none;";
                        
                        (isset(json_decode($client[0]['accounts'])->invoice) && $client[0]['accounts'] != '') ? $jsnAccinvoice =  json_decode($client[0]['accounts'])->invoice : $jsnAccinvoice = '';
                        ($jsnAccinvoice!='') ? $accinvoice = "checked='checked'" : $accinvoice = "";
                        ?>
                      <td>Accounts</td>
                      <td class="switching"><input type="checkbox" class="js-small f-right services" name="accounts[tab]" data-id="accounts" <?php if($client[0]['blocked_status']==0){ echo $acc; } ?> readonly></td>
                      <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="accounts[reminder]" <?php if($client[0]['blocked_status']==0 && $client[0]['reminder_block']==0){ echo $accre; } ?> data-id="enableacc" readonly></td>
                      <td class="swi2 textwitch"> <input type="checkbox" class="js-small f-right text" name="accounts[text]" <?php if($client[0]['blocked_status']==0){ echo $acctext; } ?> data-id="twot" readonly></td>
                      <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="accounts[invoice]" <?php if($client[0]['blocked_status']==0){ echo $accinvoice; } ?> readonly></td>
                    </tr>
                    <tr>
                      <?php 
                        //$jsntax = json_decode($client[0]['company_tax_return']);
                        (isset(json_decode($client[0]['company_tax_return'])->tab) && $client[0]['company_tax_return'] != '') ? $jsntax =  json_decode($client[0]['company_tax_return'])->tab : $jsntax = '';
                        ($jsntax!='') ? $tax = "checked='checked'" : $tax = "";
                        ($jsntax!='') ? $taxtab = "" : $taxtab = "display:none;";
                        
                        (isset(json_decode($client[0]['company_tax_return'])->reminder) && $client[0]['company_tax_return'] != '') ? $jsntaxre =  json_decode($client[0]['company_tax_return'])->reminder : $jsntaxre = '';
                        ($jsntaxre!='') ? $taxre = "checked='checked'" : $taxre = "";
                        ($jsntaxre!='') ? $taxrecom = "" : $taxrecom = "display:none;";
                        
                        (isset(json_decode($client[0]['company_tax_return'])->text) && $client[0]['company_tax_return'] != '') ? $jsntaxtext =  json_decode($client[0]['company_tax_return'])->text : $jsntaxtext = '';
                        ($jsntaxtext!='') ? $taxtext = "checked='checked'" : $taxtext = "";
                        ($jsntaxtext!='') ? $threet = "" : $threet = "display:none;";
                        
                        (isset(json_decode($client[0]['company_tax_return'])->invoice) && $client[0]['company_tax_return'] != '') ? $jsntaxinvoice =  json_decode($client[0]['company_tax_return'])->invoice : $jsntaxinvoice = '';
                        ($jsntaxinvoice!='') ? $taxinvoice = "checked='checked'" : $taxinvoice = "";
                        
                        ?>
                      <td>Company Tax Return</td>
                      <td class="switching"><input type="checkbox" class="js-small f-right services" name="companytax[tab]" data-id="companytax" <?php if($client[0]['blocked_status']==0){ echo $tax; }?> readonly></td>
                      <td class="swi1 splwitch"> <input type="checkbox" class="js-small f-right reminder" name="companytax[reminder]" <?php if($client[0]['blocked_status']==0 && $client[0]['reminder_block']==0){ echo $taxre; } ?> data-id="enabletax" readonly></td>
                      <td class="swi2 textwitch"><input type="checkbox" class="js-small f-right text" name="companytax[text]" <?php if($client[0]['blocked_status']==0){ echo $taxtext; } ?> data-id="threet" readonly></td>
                      <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="companytax[invoice]" 
                      <?php if($client[0]['blocked_status']==0){  echo $taxinvoice; }?> readonly></td>
                    </tr>
                    <tr>
                      <?php 
                        //$jsnpertax = json_decode($client[0]['personal_tax_return']);
                        (isset(json_decode($client[0]['personal_tax_return'])->tab) && $client[0]['personal_tax_return'] != '') ? $jsnpertax =  json_decode($client[0]['personal_tax_return'])->tab : $jsnpertax = '';
                        ($jsnpertax!='') ? $pertax = "checked='checked'" : $pertax = "";
                        
                        (isset(json_decode($client[0]['personal_tax_return'])->reminder) && $client[0]['personal_tax_return'] != '') ? $jsnpertaxre =  json_decode($client[0]['personal_tax_return'])->reminder : $jsnpertaxre = '';
                        ($jsnpertaxre!='') ? $pertaxre = "checked='checked'" : $pertaxre = "";
                        ($jsnpertaxre!='') ? $three = "" : $three = "display:none;";
                        
                        (isset(json_decode($client[0]['personal_tax_return'])->text) && $client[0]['personal_tax_return'] != '') ? $jsnpertaxtext =  json_decode($client[0]['personal_tax_return'])->text : $jsnpertaxtext = '';
                        ($jsnpertaxtext!='') ? $pertaxtext = "checked='checked'" : $pertaxtext = "";
                        ($jsnpertaxtext!='') ? $fourt = "" : $fourt = "display:none;";
                        
                        (isset(json_decode($client[0]['personal_tax_return'])->invoice) && $client[0]['personal_tax_return'] != '') ? $jsnpertaxinvoice =  json_decode($client[0]['personal_tax_return'])->invoice : $jsnpertaxinvoice = '';
                        ($jsnpertaxinvoice!='') ? $pertaxinvoice = "checked='checked'" : $pertaxinvoice = "";
                        
                        ?>
                      <td>Personal Tax Return</td>
                      <td class="switching"><input type="checkbox" class="js-small f-right services" name="personaltax[tab]" data-id="personaltax" <?php if($client[0]['blocked_status']==0){ echo $pertax; } ?> readonly></td>
                      <td class="swi1 splwitch"><input type="checkbox" class="js-small f-right reminder" name="personaltax[reminder]" <?php if($client[0]['blocked_status']==0 && $client[0]['reminder_block']==0){ echo $pertaxre; }?> data-id="enablepertax" readonly></td>
                      <td class="swi2 textwitch"><input type="checkbox" class="js-small f-right text" name="personaltax[text]" <?php if($client[0]['blocked_status']==0){ echo $pertaxtext; }?> data-id="fourt" readonly></td>
                      <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="personaltax[invoice]" <?php if($client[0]['blocked_status']==0){ echo $pertaxinvoice; }?> readonly></td>
                    </tr>
                    <tr>
                      <?php 
                        //$jsnpay = json_decode($client[0]['payroll']);
                        (isset(json_decode($client[0]['payroll'])->tab) && $client[0]['payroll'] != '') ? $jsnpay =  json_decode($client[0]['payroll'])->tab : $jsnpay = '';
                        ($jsnpay!='') ? $pay = "checked='checked'" : $pay = "";
                        
                        (isset(json_decode($client[0]['payroll'])->reminder) && $client[0]['payroll'] != '') ? $jsnpayre =  json_decode($client[0]['payroll'])->reminder : $jsnpayre = '';
                        ($jsnpayre!='') ? $payre = "checked='checked'" : $payre = "";
                        ($jsnpayre!='') ? $four = "" : $four = "display:none;";
                        (isset(json_decode($client[0]['payroll'])->text) && $client[0]['payroll'] != '') ? $jsnpaytext =  json_decode($client[0]['payroll'])->text : $jsnpaytext = '';
                        ($jsnpaytext!='') ? $paytext = "checked='checked'" : $paytext = "";
                        ($jsnpaytext!='') ? $fivet = "" : $fivet = "display:none;";
                        
                        (isset(json_decode($client[0]['payroll'])->invoice) && $client[0]['payroll'] != '') ? $jsnpayinvoice =  json_decode($client[0]['payroll'])->invoice : $jsnpayinvoice = '';
                        ($jsnpayinvoice!='') ? $payinvoice = "checked='checked'" : $payinvoice = "";
                        
                        ?>
                      <td>Payroll</td>
                      <td class="switching"><input type="checkbox" class="js-small f-right services" name="payroll[tab]" data-id="payroll" <?php if($client[0]['blocked_status']==0){ echo $pay; } ?> readonly></td>
                      <td class="swi1 splwitch"><input type="checkbox" class="js-small f-right reminder" name="payroll[reminder]" <?php if($client[0]['blocked_status']==0 && $client[0]['reminder_block']==0){ echo $payre; }?> data-id="enablepay" readonly></td>
                      <td class="swi2 textwitch"><input type="checkbox" class="js-small f-right text" name="payroll[text]" <?php  if($client[0]['blocked_status']==0){ echo $paytext; }?> data-id="fivet" readonly></td>
                      <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="payroll[invoice]" <?php if($client[0]['blocked_status']==0){ echo $payinvoice; }?> readonly></td>
                    </tr>
                    <tr>
                      <?php 
                        //$jsnwork = json_decode($client[0]['workplace']);
                        (isset(json_decode($client[0]['workplace'])->tab) && $client[0]['workplace'] != '') ? $jsnwork =  json_decode($client[0]['workplace'])->tab : $jsnwork = '';
                        ($jsnwork!='') ? $work = "checked='checked'" : $work = "";
                        ($jsnwork!='') ? $worktab = "" : $worktab = "display:none;";
                        
                        (isset(json_decode($client[0]['workplace'])->reminder) && $client[0]['workplace'] != '') ? $jsnworkre =  json_decode($client[0]['workplace'])->reminder : $jsnworkre = '';
                        ($jsnworkre!='') ? $workre = "checked='checked'" : $workre = "";
                        ($jsnworkre!='') ? $five = "" : $five = "display:none;";
                        
                        (isset(json_decode($client[0]['workplace'])->text) && $client[0]['workplace'] != '') ? $jsnworktext =  json_decode($client[0]['workplace'])->text : $jsnworktext = '';
                        ($jsnworktext!='') ? $worktext = "checked='checked'" : $worktext = "";
                        ($jsnworktext!='') ? $sixt = "" : $sixt = "display:none;";
                        
                        (isset(json_decode($client[0]['workplace'])->invoice) && $client[0]['workplace'] != '') ? $jsnworkinvoice =  json_decode($client[0]['workplace'])->invoice : $jsnworkinvoice = '';
                        ($jsnworkinvoice!='') ? $workinvoice = "checked='checked'" : $workinvoice = "";
                        
                        ?>
                      <td>WorkPlace Pension - AE</td>
                      <td class="switching"><input type="checkbox" class="js-small f-right services" name="workplace[tab]" data-id="workplace" <?php if($client[0]['blocked_status']==0){ echo $work; } ?> readonly></td>
                      <td class="swi1 splwitch"><input type="checkbox" class="js-small f-right reminder" name="workplace[reminder]" <?php if($client[0]['blocked_status']==0 && $client[0]['reminder_block']==0){ echo $workre; } ?> data-id="enablework" readonly></td>
                      <td class="swi2 textwitch"><input type="checkbox" class="js-small f-right text" name="workplace[text]" <?php if($client[0]['blocked_status']==0){ echo $worktext; }?> data-id="sixt" readonly></td>
                      <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="workplace[invoice]" <?php if($client[0]['blocked_status']==0){ echo $workinvoice; } ?> readonly></td>
                    </tr>
                    <tr>
                      <?php 
                        //$jsnvat = json_decode($client[0]['vat']);
                        (isset(json_decode($client[0]['vat'])->tab) && $client[0]['vat'] != '') ? $jsnvat =  json_decode($client[0]['vat'])->tab : $jsnvat = '';
                        ($jsnvat!='') ? $vat = "checked='checked'" : $vat = "";
                        
                        (isset(json_decode($client[0]['vat'])->reminder) && $client[0]['vat'] != '') ? $jsnvatre =  json_decode($client[0]['vat'])->reminder : $jsnvatre = '';
                        ($jsnvatre!='') ? $vatre = "checked='checked'" : $vatre = "";
                        ($jsnvatre!='') ? $six = "" : $six = "display:none;";
                        
                        (isset(json_decode($client[0]['vat'])->text) && $client[0]['vat'] != '') ? $jsnvattext =  json_decode($client[0]['vat'])->text : $jsnvattext = '';
                        ($jsnvattext!='') ? $vattext = "checked='checked'" : $vattext = "";
                        ($jsnvattext!='') ? $sevent = "" : $sevent = "display:none;";
                        
                        (isset(json_decode($client[0]['vat'])->invoice) && $client[0]['vat'] != '') ? $jsnvatinvoice =  json_decode($client[0]['vat'])->invoice : $jsnvatinvoice = '';
                        ($jsnvatinvoice!='') ? $vatinvoice = "checked='checked'" : $vatinvoice = "";
                        ?>
                      <td>VAT Returns</td>
                      <td class="switching"><input type="checkbox" class="js-small f-right services" name="vat[tab]" data-id="vat" <?php  if($client[0]['blocked_status']==0){ echo $vat; } ?> readonly></td>
                      <td class="swi1 splwitch"><input type="checkbox" class="js-small f-right reminder" name="vat[reminder]" <?php if($client[0]['blocked_status']==0 && $client[0]['reminder_block']==0){ echo $vatre; }?> data-id="enablevat" readonly></td>
                      <td><input type="checkbox" class="js-small f-right" name="vat[text]" <?php if($client[0]['blocked_status']==0){ echo $vattext; } ?> data-id="sevent" readonly></td>
                      <td><input type="checkbox" class="js-small f-right" name="vat[invoice]" <?php if($client[0]['blocked_status']==0){ echo $vatinvoice; } ?> readonly></td>
                    </tr>
                    <tr>
                      <?php 
                        //$jsncontra = json_decode($client[0]['cis']);
                        (isset(json_decode($client[0]['cis'])->tab) && $client[0]['cis'] != '') ? $jsncontra =  json_decode($client[0]['cis'])->tab : $jsncontra = '';
                        ($jsncontra!='') ? $contra = "checked='checked'" : $contra = "";
                        ($jsncontra!='') ? $contratab = "" : $contratab = "display:none;";
                        
                        (isset(json_decode($client[0]['cis'])->reminder) && $client[0]['cis'] != '') ? $jsncontrare =  json_decode($client[0]['cis'])->reminder : $jsncontrare = '';
                        ($jsncontrare!='') ? $contrare = "checked='checked'" : $contrare = "";
                        ($jsncontrare!='') ? $seven = "" : $seven = "display:none;";
                        
                        (isset(json_decode($client[0]['cis'])->text) && $client[0]['cis'] != '') ? $jsncontratext =  json_decode($client[0]['cis'])->text : $jsncontratext = '';
                        ($jsncontratext!='') ? $contratext = "checked='checked'" : $contratext = "";
                        ($jsncontratext!='') ? $eightt = "" : $eightt = "display:none;";
                        
                        (isset(json_decode($client[0]['cis'])->invoice) && $client[0]['cis'] != '') ? $jsncontrainvoice =  json_decode($client[0]['cis'])->invoice : $jsncontrainvoice = '';
                        ($jsncontrainvoice!='') ? $contrainvoice = "checked='checked'" : $contrainvoice = "";
                        
                        ?>
                      <td>CIS - Contractor</td>
                      <td class="switching"><input type="checkbox" class="js-small f-right services" name="cis[tab]" data-id="cis" <?php if($client[0]['blocked_status']==0){ echo $contra; }?> readonly></td>
                      <td class="swi1 splwitch"><input type="checkbox" class="js-small f-right reminder" name="cis[reminder]" <?php if($client[0]['blocked_status']==0 && $client[0]['reminder_block']==0){ echo $contrare; } ?> data-id="enablecis" readonly></td>
                      <td class="swi2 textwitch"><input type="checkbox" class="js-small f-right text" name="cis[text]" <?php if($client[0]['blocked_status']==0){ echo $contratext; } ?> data-id="eightt" readonly></td>
                      <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="cis[invoice]" <?php if($client[0]['blocked_status']==0){ echo $contrainvoice; } ?> readonly></td>
                    </tr>
                    <tr>
                      <?php 
                        //$jsncontrasub = json_decode($client[0]['cissub']);
                        (isset(json_decode($client[0]['cissub'])->tab) && $client[0]['cissub'] != '') ? $jsncontrasub =  json_decode($client[0]['cissub'])->tab : $jsncontrasub = '';
                        ($jsncontrasub!='') ? $contrasub = "checked='checked'" : $contrasub = "";
                        ($jsncontrasub!='') ? $contrasubtab = "" : $contrasubtab = "display:none;";
                        
                        (isset(json_decode($client[0]['cissub'])->reminder) && $client[0]['cissub'] != '') ? $jsncontrasubre =  json_decode($client[0]['cissub'])->reminder : $jsncontrasubre = '';
                        ($jsncontrasubre!='') ? $contrasubre = "checked='checked'" : $contrasubre = "";
                        ($jsncontrasubre!='') ? $eight = "" : $eight = "display:none;";
                        
                        (isset(json_decode($client[0]['cissub'])->text) && $client[0]['cissub'] != '') ? $jsncontrasubtext =  json_decode($client[0]['cissub'])->text : $jsncontrasubtext = '';
                        ($jsncontrasubtext!='') ? $contrasubtext = "checked='checked'" : $contrasubtext = "";
                        ($jsncontrasubtext!='') ? $ninet = "" : $ninet = "display:none;";
                        
                        (isset(json_decode($client[0]['cissub'])->invoice) && $client[0]['cissub'] != '') ? $jsncontrasubinvoice =  json_decode($client[0]['cissub'])->invoice : $jsncontrasubinvoice = '';
                        ($jsncontrasubinvoice!='') ? $contrasubinvoice = "checked='checked'" : $contrasubinvoice = "";
                        
                        ?>
                      <td>CIS - Sub Contractor</td>
                      <td class="switching"><input type="checkbox" class="js-small f-right services" name="cissub[tab]" data-id="cissub" <?php if($client[0]['blocked_status']==0){ echo $contrasub; } ?> readonly></td>
                      <td class="swi1 splwitch"><input type="checkbox" class="js-small f-right reminder" name="cissub[reminder]" <?php if($client[0]['blocked_status']==0 && $client[0]['reminder_block']==0){ echo $contrasubre; } ?> data-id="enablecissub" readonly></td>
                      <td class="swi2 textwitch"> <input type="checkbox" class="js-small f-right text" name="cissub[text]" <?php if($client[0]['blocked_status']==0){ echo $contrasubtext; } ?> data-id="ninet" readonly></td>
                      <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="cissub[invoice]" <?php if($client[0]['blocked_status']==0){ echo $contrasubinvoice; }?> readonly></td>
                    </tr>
                    <tr>
                      <?php 
                        //$jsncissub = json_decode($client[0]['cissub']);
                        (isset(json_decode($client[0]['p11d'])->tab) && $client[0]['p11d'] != '') ? $jsnp11d =  json_decode($client[0]['p11d'])->tab : $jsnp11d = '';
                        ($jsnp11d!='') ? $p11d = "checked='checked'" : $p11d = "";
                        ($jsnp11d!='') ? $p11dtab = "" : $p11dtab = "display:none;";
                        
                        (isset(json_decode($client[0]['p11d'])->reminder) && $client[0]['p11d'] != '') ? $jsnp11dre =  json_decode($client[0]['p11d'])->reminder : $jsnp11dre = '';
                        ($jsnp11dre!='') ? $p11dre = "checked='checked'" : $p11dre = "";
                        ($jsnp11dre!='') ? $nine = "" : $nine = "display:none;";
                        
                        (isset(json_decode($client[0]['p11d'])->text) && $client[0]['p11d'] != '') ? $jsnp11dtext =  json_decode($client[0]['p11d'])->text : $jsnp11dtext = '';
                        ($jsnp11dtext!='') ? $p11dtext = "checked='checked'" : $p11dtext = "";
                        ($jsnp11dtext!='') ? $tent = "" : $tent = "display:none;";
                        
                        (isset(json_decode($client[0]['p11d'])->invoice) && $client[0]['p11d'] != '') ? $jsnp11dinvoice =  json_decode($client[0]['p11d'])->invoice : $jsnp11dinvoice = '';
                        ($jsnp11dinvoice!='') ? $p11dinvoice = "checked='checked'" : $p11dinvoice = "";
                        ?>
                      <td>P11D</td>
                      <td class="switching"><input type="checkbox" class="js-small f-right services" name="p11d[tab]" data-id="p11d" <?php if($client[0]['blocked_status']==0){ echo $p11d; } ?> readonly></td>
                      <td class="swi1 splwitch"><input type="checkbox" class="js-small f-right reminder" name="p11d[reminder]" <?php if($client[0]['blocked_status']==0 && $client[0]['reminder_block']==0){ echo $p11dre; } ?> data-id="enableplld" readonly></td>
                      <td class="swi2 textwitch"><input type="checkbox" class="js-small f-right text" name="p11d[text]" <?php if($client[0]['blocked_status']==0){ echo $p11dtext; }?> data-id="tent" readonly></td>
                      <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="p11d[invoice]" <?php if($client[0]['blocked_status']==0){ echo $p11dinvoice; }?> readonly></td>
                    </tr>
                    <tr>
                      <?php 
                        //$jsnbookkeep = json_decode($client[0]['bookkeep']);
                        (isset(json_decode($client[0]['bookkeep'])->tab) && $client[0]['bookkeep'] != '') ? $jsnbookkeep =  json_decode($client[0]['bookkeep'])->tab : $jsnbookkeep = '';
                        ($jsnbookkeep!='') ? $bookkeep = "checked='checked'" : $bookkeep = "";
                        ($jsnbookkeep!='') ? $bookkeeptab = "" : $bookkeeptab = "display:none;";
                        
                        (isset(json_decode($client[0]['bookkeep'])->reminder) && $client[0]['bookkeep'] != '') ? $jsnbookkeepre =  json_decode($client[0]['bookkeep'])->reminder : $jsnbookkeepre = '';
                        ($jsnbookkeepre!='') ? $bookkeepre = "checked='checked'" : $bookkeepre = "";
                        ($jsnbookkeepre!='') ? $ten = "" : $ten = "display:none;";
                        
                        
                        (isset(json_decode($client[0]['bookkeep'])->text) && $client[0]['bookkeep'] != '') ? $jsnbookkeeptext =  json_decode($client[0]['bookkeep'])->text : $jsnbookkeeptext = '';
                        ($jsnbookkeeptext!='') ? $bookkeeptext = "checked='checked'" : $bookkeeptext = "";
                        
                        ($jsnbookkeeptext!='') ? $elevent = "" : $elevent = "display:none;";
                        
                        (isset(json_decode($client[0]['bookkeep'])->invoice) && $client[0]['bookkeep'] != '') ? $jsnbookkeepinvoice =  json_decode($client[0]['bookkeep'])->invoice : $jsnbookkeepinvoice = '';
                        ($jsnbookkeepinvoice!='') ? $bookkeepinvoice = "checked='checked'" : $bookkeepinvoice = "";
                        
                        ?>
                      <td>Bookkeeping</td>
                      <td class="switching"><input type="checkbox" class="js-small f-right services" name="bookkeep[tab]" data-id="bookkeep" <?php if($client[0]['blocked_status']==0){ echo $bookkeep; } ?> readonly></td>
                      <td class="swi1 splwitch"><input type="checkbox" class="js-small f-right reminder" name="bookkeep[reminder]" <?php if($client[0]['blocked_status']==0 && $client[0]['reminder_block']==0) { echo $bookkeepre; } ?> data-id="enablebook" readonly></td>
                      </td>
                      <td class="swi2 textwitch"><input type="checkbox" class="js-small f-right text" name="bookkeep[text]" <?php if($client[0]['blocked_status']==0){ echo $bookkeeptext; } ?> data-id="elevent" readonly></td>
                      <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="bookkeep[invoice]" <?php if($client[0]['blocked_status']==0){ echo $bookkeepinvoice; }?> readonly></td>
                    </tr>
                    <tr>
                      <?php 
                        //$jsnmanagement = json_decode($client[0]['management']);
                        (isset(json_decode($client[0]['management'])->tab) && $client[0]['management'] != '') ? $jsnmanagement =  json_decode($client[0]['management'])->tab : $jsnmanagement = '';
                        ($jsnmanagement!='') ? $management = "checked='checked'" : $management = "";
                        
                        (isset(json_decode($client[0]['management'])->reminder) && $client[0]['management'] != '') ? $jsnmanagementre =  json_decode($client[0]['management'])->reminder : $jsnmanagementre = '';
                        ($jsnmanagementre!='') ? $managementre = "checked='checked'" : $managementre = "";
                        ($jsnmanagementre!='') ? $eleven = "" : $eleven = "display:none;";
                        
                        (isset(json_decode($client[0]['management'])->text) && $client[0]['management'] != '') ? $jsnmanagementtext =  json_decode($client[0]['management'])->text : $jsnmanagementtext = '';
                        ($jsnmanagementtext!='') ? $managementtext = "checked='checked'" : $managementtext = "";
                        
                        ($jsnmanagementtext!='') ? $twelvet = "" : $twelvet = "display:none;";
                        
                        (isset(json_decode($client[0]['management'])->invoice) && $client[0]['management'] != '') ? $jsnmanagementinvoice =  json_decode($client[0]['management'])->invoice : $jsnmanagementinvoice = '';
                        ($jsnmanagementinvoice!='') ? $managementinvoice = "checked='checked'" : $managementinvoice = "";
                        
                        ?>
                      <td>Management Accounts</td>
                      <td class="switching"><input type="checkbox" class="js-small f-right services" name="management[tab]" data-id="management" <?php if($client[0]['blocked_status']==0){ echo $management; } ?> readonly></td>
                      <td class="swi1 splwitch"><input type="checkbox" class="js-small f-right reminder" name="management[reminder]" <?php if($client[0]['blocked_status']==0 && $client[0]['reminder_block']==0){ echo $managementre; } ?> data-id="enablemanagement" readonly></td>
                      <td class="swi2 textwitch"><input type="checkbox" class="js-small f-right text" name="management[text]" <?php if($client[0]['blocked_status']==0){ echo $managementtext; } ?> data-id="twelvet" readonly></td>
                      <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="management[invoice]" <?php if($client[0]['blocked_status']==0){  echo $managementinvoice; }?> readonly></td>
                    </tr>
                    <!-- *******  *** -->     
                    <tr>
                      <?php 
                        //$jsninvestgate = json_decode($client[0]['investgate']);
                        (isset(json_decode($client[0]['investgate'])->tab) && $client[0]['investgate'] != '') ? $jsninvestgate =  json_decode($client[0]['investgate'])->tab : $jsninvestgate = '';
                        ($jsninvestgate!='') ? $investgate = "checked='checked'" : $investgate = "";
                        
                        (isset(json_decode($client[0]['investgate'])->reminder) && $client[0]['investgate'] != '') ? $jsninvestgatere =  json_decode($client[0]['investgate'])->reminder : $jsninvestgatere = '';
                        ($jsninvestgatere!='') ? $investgatere = "checked='checked'" : $investgatere = "";
                        ($jsninvestgatere!='') ? $twelve = "" : $twelve = "display:none;";
                        
                        (isset(json_decode($client[0]['investgate'])->text) && $client[0]['investgate'] != '') ? $jsninvestgatetext =  json_decode($client[0]['investgate'])->text : $jsninvestgatetext = '';
                        ($jsninvestgatetext!='') ? $investgatetext = "checked='checked'" : $investgatetext = "";
                        ($jsninvestgatetext!='') ? $thirteent = "" : $thirteent = "display:none;";
                        
                        (isset(json_decode($client[0]['investgate'])->invoice) && $client[0]['investgate'] != '') ? $jsninvestgateinvoice =  json_decode($client[0]['investgate'])->invoice : $jsninvestgateinvoice = '';
                        ($jsninvestgateinvoice!='') ? $investgateinvoice = "checked='checked'" : $investgateinvoice = "";
                        
                        ?>
                      <td>Investigation Insurance</td>
                      <td class="switching"><input type="checkbox" class="js-small f-right services" name="investgate[tab]" data-id="investgate" <?php if($client[0]['blocked_status']==0){ echo $investgate; } ?> readonly></td>
                      <td class="swi1 splwitch"><input type="checkbox" class="js-small f-right reminder" name="investgate[reminder]" <?php if($client[0]['blocked_status']==0 && $client[0]['reminder_block']==0){ echo $investgatere; } ?> data-id="enableinvest" readonly></td>
                      <td class="swi2 textwitch"><input type="checkbox" class="js-small f-right text" name="investgate[text]" <?php if($client[0]['blocked_status']==0){echo $investgatetext; }?> data-id="thirteent" readonly></td>
                      <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="investgate[invoice]" <?php if($client[0]['blocked_status']==0){ echo $investgateinvoice; }?> readonly></td>
                    </tr>
                    <tr>
                      <?php 
                        //$jsnregistered = json_decode($client[0]['registered']);
                        (isset(json_decode($client[0]['registered'])->tab) && $client[0]['registered'] != '') ? $jsnregistered =  json_decode($client[0]['registered'])->tab : $jsnregistered = '';
                        ($jsnregistered!='') ? $registered = "checked='checked'" : $registered = "";
                        ($jsnregistered!='') ? $registeredtab = "" : $registeredtab = "display:none;";
                        
                        (isset(json_decode($client[0]['registered'])->reminder) && $client[0]['registered'] != '') ? $jsnregisteredre =  json_decode($client[0]['registered'])->reminder : $jsnregisteredre = '';
                        ($jsnregisteredre!='') ? $registeredre = "checked='checked'" : $registeredre = "";
                        ($jsnregisteredre!='') ? $thirteen = "" : $thirteen = "display:none;";
                        
                        (isset(json_decode($client[0]['registered'])->text) && $client[0]['registered'] != '') ? $jsnregisteredtext =  json_decode($client[0]['registered'])->text : $jsnregisteredtext = '';
                        ($jsnregisteredtext!='') ? $registeredtext = "checked='checked'" : $registeredtext = "";
                        ($jsnregisteredtext!='') ? $fourteent = "" : $fourteent = "display:none;";
                        
                        (isset(json_decode($client[0]['registered'])->invoice) && $client[0]['registered'] != '') ? $jsnregisteredinvoice =  json_decode($client[0]['registered'])->invoice : $jsnregisteredinvoice = '';
                        ($jsnregisteredinvoice!='') ? $registeredinvoice = "checked='checked'" : $registeredinvoice = "";
                        
                        ?>
                      <td>Registered Address</td>
                      <td class="switching"><input type="checkbox" class="js-small f-right services" name="registered[tab]" data-id="registered" <?php if($client[0]['blocked_status']==0){ echo $registered; } ?> readonly></td>
                      <td class="swi1 splwitch"> <input type="checkbox" class="js-small f-right reminder" name="registered[reminder]" <?php if($client[0]['blocked_status']==0 && $client[0]['reminder_block']==0){ echo $registeredre; } ?> data-id="enablereg" readonly></td>
                      <td class="swi2 textwitch"><input type="checkbox" class="js-small f-right text" name="registered[text]" <?php if($client[0]['blocked_status']==0){ echo $registeredtext; } ?> data-id="fourteent" readonly></td>
                      <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="registered[invoice]" <?php if($client[0]['blocked_status']==0){ echo $registeredinvoice; }?> readonly></td>
                    </tr>
                    <tr>
                      <?php 
                        //$jsntaxadvice = json_decode($client[0]['taxadvice']);
                        (isset(json_decode($client[0]['taxadvice'])->tab) && $client[0]['taxadvice'] != '') ? $jsntaxadvice =  json_decode($client[0]['taxadvice'])->tab : $jsntaxadvice = '';
                        ($jsntaxadvice!='') ? $taxadvice = "checked='checked'" : $taxadvice = "";
                        ($jsntaxadvice!='') ? $taxadvicetab = "" : $taxadvicetab = "display:none;";
                        
                        (isset(json_decode($client[0]['taxadvice'])->reminder) && $client[0]['taxadvice'] != '') ? $jsntaxadvicere =  json_decode($client[0]['taxadvice'])->reminder : $jsntaxadvicere = '';
                        ($jsntaxadvicere!='') ? $taxadvicere = "checked='checked'" : $taxadvicere = "";
                        ($jsntaxadvicere!='') ? $fourteen = "" : $fourteen = "display:none;";
                        
                        (isset(json_decode($client[0]['taxadvice'])->text) && $client[0]['taxadvice'] != '') ? $jsntaxadvicetext =  json_decode($client[0]['taxadvice'])->text : $jsntaxadvicetext = '';
                        ($jsntaxadvicetext!='') ? $taxadvicetext = "checked='checked'" : $taxadvicetext = "";
                        ($jsntaxadvicetext!='') ? $fifteent = "" : $fifteent = "display:none;";
                        
                        (isset(json_decode($client[0]['taxadvice'])->invoice) && $client[0]['taxadvice'] != '') ? $jsntaxadviceinvoice =  json_decode($client[0]['taxadvice'])->invoice : $jsntaxadviceinvoice = '';
                        ($jsntaxadviceinvoice!='') ? $taxadviceinvoice = "checked='checked'" : $taxadviceinvoice = "";
                        
                        ?>
                      <td>Tax Advice/Investigation</td>
                      <td class="switching"><input type="checkbox" class="js-small f-right services" name="taxadvice[tab]" data-id="taxadvice" <?php if($client[0]['blocked_status']==0){ echo $taxadvice; }?> readonly></td>
                      <td class="swi1 splwitch"> <input type="checkbox" class="js-small f-right reminder" name="taxadvice[reminder]" <?php if($client[0]['blocked_status']==0 && $client[0]['reminder_block']==0){ echo $taxadvicere; } ?> data-id="enabletaxad" readonly></td>
                      <td class="swi2 textwitch"><input type="checkbox" class="js-small f-right text" name="taxadvice[text]" <?php if($client[0]['blocked_status']==0){ echo $taxadvicetext; } ?> data-id="fifteent" readonly></td>
                      <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="taxadvice[invoice]" <?php if($client[0]['blocked_status']==0){ echo $taxadviceinvoice; }?> readonly></td>
                    </tr>
                    <tr style="display: none;">
                      <?php 
                        //$jsninves = json_decode($client[0]['taxinvest']);
                        (isset(json_decode($client[0]['taxinvest'])->tab) && $client[0]['taxinvest'] != '') ? $jsninves =  json_decode($client[0]['taxinvest'])->tab : $jsninves = '';
                        ($jsninves!='') ? $inves = "checked='checked'" : $inves = "";
                        ($jsninves!='') ? $investab = "" : $investab = "display:none;";
                        
                        (isset(json_decode($client[0]['taxinvest'])->reminder) && $client[0]['taxinvest'] != '') ? $jsninvesre =  json_decode($client[0]['taxinvest'])->reminder : $jsninvesre = '';
                        ($jsninvesre!='') ? $invesre = "checked='checked'" : $invesre = "";
                        ($jsninvesre!='') ? $fifteen = "" : $fifteen = "display:none;";
                        
                        (isset(json_decode($client[0]['taxinvest'])->text) && $client[0]['taxinvest'] != '') ? $jsninvestext =  json_decode($client[0]['taxinvest'])->text : $jsninvestext = '';
                        ($jsninvestext!='') ? $investext = "checked='checked'" : $investext = "";
                        ($jsninvestext!='') ? $sixteent = "" : $sixteent = "display:none;";
                        
                        (isset(json_decode($client[0]['taxinvest'])->invoice) && $client[0]['taxinvest'] != '') ? $jsninvesinvoice =  json_decode($client[0]['taxinvest'])->invoice : $jsninvesinvoice = '';
                        ($jsninvesinvoice!='') ? $invesinvoice = "checked='checked'" : $invesinvoice = "";
                        ?>
                      <td>Tax Investigation</td>
                      <td class="switching"><input type="checkbox" class="js-small f-right services" name="taxinvest[tab]" data-id="taxinvest" <?php if($client[0]['blocked_status']==0){ echo $inves; }?> readonly></td>
                      <td class="swi1 splwitch"><input type="checkbox" class="js-small f-right reminder" name="taxinvest[reminder]" <?php if($client[0]['blocked_status']==0 && $client[0]['reminder_block']==0){ echo $invesre; }?> data-id="enabletaxinves" readonly></td>
                      <td class="swi2 textwitch"><input type="checkbox" class="js-small f-right text" name="taxinvest[text]" <?php if($client[0]['blocked_status']==0){ echo $investext; } ?> data-id="sixteent" readonly></td>
                      <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="taxinvest[invoice]" <?php if($client[0]['blocked_status']==0){ echo $invesinvoice; }?> readonly></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
      </div>
          </div>
          <!-- service -->
          <div id="assignto" class="tab-pane fade">
          <!--   <div class="main-pane-border1 main-pane-border03">
              <div class="pane-border1">
                <div class="form-group row name_fields">
                  <label class="col-sm-4 col-form-label">Select Responsible Type</label>
                  <div class="col-sm-8">
                    <select name="select_responsible_type" id="select_responsible_type">
                      <option value="">--select--</option>
                      <option value="staff" <?php if($client[0]['select_responsible_type']=='staff'){ echo 'selected'; }?>>Staff</option>
                      <option value="team"  <?php if($client[0]['select_responsible_type']=='team'){ echo 'selected'; }?>>Team</option>
                      <option value="departments"  <?php if($client[0]['select_responsible_type']=='departments'){ echo 'selected'; }?>>Departments</option>
                      <option value="members"  <?php if($client[0]['select_responsible_type']=='members'){ echo 'selected'; }?>>Members</option>
                    </select>
                  </div>
                </div>
              </div>
            </div> -->
            <?php  
              /*if($client[0]['select_responsible_type']=='staff'){ $css = "display:block"; } else { $css ="display:none;"; }
              if($client[0]['select_responsible_type']=='team'){ $team_css = "display:block"; } else { $team_css ="display:none;"; }
              if($client[0]['select_responsible_type']=='departments'){ $department_css = "display:block"; } else { $department_css ="display:none;"; }
              if($client[0]['select_responsible_type']=='members'){ $members_css = "display:block"; } else { $members_css ="display:none;"; }*/

                 if($client[0]['select_responsible_type']=='staff' || $client[0]['select_responsible_type']=='team' || $client[0]['select_responsible_type']=='departments' || $client[0]['select_responsible_type']=='members' )
                        {
                          $members_css = $department_css = $team_css = $css = "display:block";
                        }else{
                          $members_css = $department_css = $team_css = $css = "display:none";
                        }
              ?>
        <div class="masonry-container floating_set">
<div class="grid-sizer"></div>
            <div class="accordion-panel responsible_team_tbl" style="<?php echo $team_css;?>">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Responsible Team</a>
                  </h3>
                </div>
                <div class="basic-info-client1">
                  <div class="basic-border1">
                    <table class="table table-hover table-bordered">
                      <thead>
                        <tr>
                          <th>Team</th>
                          <th>Allocation office</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                          $k =1;
                          if(!empty($responsible_team)){
                          foreach ($responsible_team as $responsible_teamkey => $responsible_teamvalue) {
                           $team_n = $this->Common_mdl->select_record('team','id',$responsible_teamvalue['team']);
                          ?>
                        <tr class="success ykpackrowfirst ykpackrow">
                         
                          <td><?php echo $team_n['team'];?>
                           
                          </td>
                          <td><?php if(isset($responsible_teamvalue['allocation_holder']) && ($responsible_teamvalue['allocation_holder']!='') ){ echo $responsible_teamvalue['allocation_holder'];}?>
                          </td>
                          <?php //if($k>1){?>
                          <!-- <td><button type="button" class="btn btn-danger yk_pack_delrow">Delete</button></td> -->
                          <?php //} ?>                                       
                        </tr>
                        <?php $k++; }}?>                                             
                       
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <!-- accordion-panel -->         
            <!-- ********************* -->
            <div class="accordion-panel responsible_department_tbl" style="<?php echo $department_css;?>">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Responsible Department</a>
                  </h3>
                </div>
                <div class="basic-info-client1">
                  <div class="basic-border1">
                    <table class="table table-hover table-bordered">
                      <thead>
                        <tr>
                          <th>Department</th>
                          <th>Allocation office</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                          $k =1;
                          if(!empty($responsible_department)){
                          foreach ($responsible_department as $responsible_departkey => $responsible_departvalue) {
                           $dept_n = $this->Common_mdl->select_record('department_permission','id',$responsible_departvalue['depart']);
                           $dept_name = $dept_n['new_dept'];
                           if($dept_name==''){
                            $dept_name = '-';
                           }
                          ?>
                        <tr class="success ykpackrowfirst ykpackrow_depart">
                          <td><?php echo $dept_name;?>
                            <!-- <input type="text" name="depart[]" id="depart" placeholder="" value="<?php if(isset($responsible_departvalue['depart']) && ($responsible_departvalue['depart']!='') ){ echo $responsible_departvalue['depart'];}?>" class="fields"> -->
                            <!-- <select name="depart" id="depart" class="fields">
                              <option value=''>--select--</option>
                              <?php foreach ($deptlist as $deptlistkey => $deptlistvalue) { ?>
                              <option value="<?php echo $deptlistvalue['id'];?>" <?php if(isset($responsible_departvalue['depart']) && ($responsible_departvalue['depart']==$deptlistvalue['id']) ){ echo 'selected';}?>><?php echo $deptlistvalue['department'];?></option>
                              <?php } ?>
                            </select> -->
                          </td>
                          <td><?php if(isset($responsible_departvalue['allocation_holder']) && ($responsible_departvalue['allocation_holder']!='') ){ echo $responsible_departvalue['allocation_holder'];}?>
                           <!--  <input type="text" name="allocation_holder_dept[]" id="allocation_holder_dept" placeholder="" value="<?php if(isset($responsible_departvalue['allocation_holder']) && ($responsible_departvalue['allocation_holder']!='') ){ echo $responsible_departvalue['allocation_holder'];}?>" class="fields"> -->
                          </td>
                          <?php //if($k>1){?>
                         
                          <?php //} ?>                                       
                        </tr>
                        <?php $k++; }}?>                                             
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <!-- accordion-panel -->         
            <!-- ********************* -->
            <div class="accordion-panel responsible_user_tbl" style="<?php echo $css;?>">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Responsible Staff</a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1">
                    <div class="basic-border1">
                      <!--    <div class="form-group row name_fields">
                        <label class="col-sm-4 col-form-label">Manager/Reviewer</label>
                        <div class="col-sm-8">
                            <input type="text" name="manager_reviewer" id="manager_reviewer" placeholder="" value="<?php if(isset($client[0]['crm_assign_manager_reviewer']) && ($client[0]['crm_assign_manager_reviewer']!='') ){ echo $client[0]['crm_assign_manager_reviewer'];}?>" class="fields">
                         </div>
                         </div> 
                         <div class="form-group row name_fields">
                        <label class="col-sm-4 col-form-label">Managed By</label>
                        <div class="col-sm-8">
                            <input type="text" name="managed" id="managed" placeholder="" value="<?php if(isset($client[0]['crm_assign_managed']) && ($client[0]['crm_assign_managed']!='') ){ echo $client[0]['crm_assign_managed'];}?>" class="fields">
                         </div>
                         </div>  -->
                      <table class="table table-hover table-bordered">
                        <thead>
                          <tr>
                           <!--  <th>Manager/Reviewer</th> -->
                            <th>Staff</th>
                            <th>Managed By</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                            $j =1;
                            if(!empty($responsible_user)){
                            foreach ($responsible_user as $responsible_userkey => $responsible_uservalue) {
                              
                            ?>
                          <tr class="success ykpackrowfirsts ykpackrow_user ">
                            <?php //print_r($staff_form);die;?>
                            <td>
                              <!--  <input type="text" name="manager_reviewer[]" id="manager_reviewer" placeholder="" value="<?php if(isset($responsible_uservalue['manager_reviewer']) && ($responsible_uservalue['manager_reviewer']!='') ){ echo $responsible_uservalue['manager_reviewer'];}?>" class="fields"> -->
                              <div class="dropdown-sin-2">
                                <!-- <select style="display:block" name="manager_reviewer" id="manager_reviewer"  placeholder="Select">
                                  <?php foreach($staff_form as $managers) {
                                    ?>
                                  <option value="<?php echo $managers['id']?>" <?php if(isset($responsible_uservalue['manager_reviewer']) && ($responsible_uservalue['manager_reviewer']==$managers['id']) ){ echo 'selected'; } ?>><?php echo $managers['crm_name'];?></option>
                                  <?php } ?>
                                </select> -->
                                   <?php foreach($staff_form as $managers) {
                                     if(isset($responsible_uservalue['manager_reviewer']) && ($responsible_uservalue['manager_reviewer']==$managers['id']) ){ 
                                      //echo 'selected'; ?>
                                        <span class="small-info"><?php echo $managers['crm_name'];?>
                                    <?php }
                                   }
                                    ?>
                              <!--   <span class="small-info"><?php echo $managers['crm_name'];?> --></span>
                              </div>
                            </td>
                            <td>
                              <!-- <input type="text" name="assign_managed[]" id="assign_managed" placeholder="" value="<?php if(isset($responsible_uservalue['assign_managed']) && ($responsible_uservalue['assign_managed']!='') ){ echo $responsible_uservalue['assign_managed'];}?>" class="fields"> -->
                              <div class="dropdown-sin-2">
                                <!-- <select style="display:block" name="assign_managed" id="assign_managed"  placeholder="Select">
                                  <?php foreach($managed_by as $managedby) {
                                    ?>
                                  <option value="<?php echo $managedby['id']?>"  <?php if(isset($responsible_uservalue['assign_managed']) && ($responsible_uservalue['assign_managed']==$managedby['id']) ){ echo 'selected'; } ?>><?php echo $managedby['crm_name'];?></option>
                                  <?php } ?>
                                </select> -->
                                <?php foreach($managed_by as $managedby) {
                                  if(isset($responsible_uservalue['assign_managed']) && ($responsible_uservalue['assign_managed']==$managedby['id']) ){
                                   ?>
                                    <span class="small-info"><?php echo $managedby['crm_name'];?></span>
                                   <?php 
                                 }
                                }
                                    ?>
                               <!--  <span class="small-info"><?php echo $managedby['crm_name'];?></span> -->
                              </div>
                            </td>
                            <?php //if($k>1){?>
                            
                            <?php //} ?>                                       
                          </tr>
                          <?php $j++; } } ?>                                           
                          
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- accordion-panel -->   
            <!-- ********************* -->
            <!-- already had in style="<?php echo $members_css;?>" -->
            <div class="accordion-panel responsible_member_tbl" style="display: none;">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Responsible Users</a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1">
                    <div class="basic-border1">
                      <table class="table table-hover table-bordered">
                        <thead>
                          <tr>
                            <th>Manager/Reviewer</th>
                            <th>Managed By</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                            $j =1;
                            if(!empty($responsible_member)){
                            foreach ($responsible_member as $responsible_memberkey => $responsible_membervalue) {
                              
                            ?>
                          <tr class="success ykpackrowfirsts ykpackrow_member ">
                            <?php //print_r($staff_form);die;?>
                            <td>
                              <!--  <input type="text" name="manager_reviewer[]" id="manager_reviewer" placeholder="" value="<?php if(isset($responsible_uservalue['manager_reviewer']) && ($responsible_uservalue['manager_reviewer']!='') ){ echo $responsible_uservalue['manager_reviewer'];}?>" class="fields"> -->
                              <div class="dropdown-sin-2">
                                <!-- <select style="display:block" name="manager_reviewer_member" id="manager_reviewer_member"  placeholder="Select">
                                  <?php //foreach($staff_form as $managers) {
                                    foreach ($referby as $referbykey => $referbyvalue) {
                                                                                        
                                                                    ?>
                                  <option value="<?php echo $referbyvalue['crm_first_name'];?>" <?php if(isset($responsible_membervalue['manager_reviewer']) && ($responsible_membervalue['manager_reviewer']==$referbyvalue['crm_first_name']) ){ echo 'selected'; } ?>><?php echo $referbyvalue['crm_first_name'];?></option>
                                  <?php } ?>
                                </select> -->
                                <span class="small-info"><?php  if(isset($responsible_membervalue['manager_reviewer']) && ($responsible_membervalue['manager_reviewer']!='') ){ echo $responsible_membervalue['manager_reviewer']; } ?></span>
                              </div>
                            </td>
                            <td>
                              <!-- <input type="text" name="assign_managed[]" id="assign_managed" placeholder="" value="<?php if(isset($responsible_uservalue['assign_managed']) && ($responsible_uservalue['assign_managed']!='') ){ echo $responsible_uservalue['assign_managed'];}?>" class="fields"> -->
                              <div class="dropdown-sin-2">
                                <!-- <select style="display:block" name="assign_managed_member" id="assign_managed_member"  placeholder="Select">
                                  <?php foreach($managed_by as $managedby) {
                                    ?>
                                  <option value="<?php echo $managedby['id']?>"  <?php if(isset($responsible_membervalue['assign_managed']) && ($responsible_membervalue['assign_managed']==$managedby['id']) ){ echo 'selected'; } ?>><?php echo $managedby['crm_name'];?></option>
                                  <?php } ?>
                                                                   
                                  </select> -->
                                <!-- <input type="text" name="assign_managed_member[]" id="assign_managed_member" placeholder="" value="<?php if(isset($responsible_membervalue['assign_managed']) && ($responsible_membervalue['assign_managed']!='') ){ echo $responsible_membervalue['assign_managed'];}?>" class="fields"> -->
                                  <?php foreach($managed_by as $managedby) {
                                    if(isset($responsible_membervalue['assign_managed']) && ($responsible_membervalue['assign_managed']==$managedby['id']) ){ ?>
                                    <span class="small-info"><?php echo $managedby['crm_name'];?></span>
                                    <?php }
                                  }
                                    ?>
                              <!--   <span class="small-info"><?php if(isset($responsible_membervalue['assign_managed']) && ($responsible_membervalue['assign_managed']!='') ){ echo $responsible_membervalue['assign_managed'];}?></span> -->
                              </div>
                            </td>
                            <?php //if($k>1){?>
                        
                            <?php //} ?>                                       
                          </tr>
                          <?php $j++; } } ?>                                           
                        
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        
           <!--  <div class="accordion-panel">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Notes if any other</a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1">
                    <?php
                      $crm_assign_notes=( isset($client[0]['crm_assign_notes']) ? $client[0]['crm_assign_notes'] : false);   
                      $crm_assign_note=render_custom_fields_edit( 'assign_notes',$rel_id,$crm_assign_notes);  
                       if($crm_assign_note!='') {
                        //echo $crm_assign_note;

                          $s_an = $this->Common_mdl->getCustomFieldRec($rel_id,'assign_notes');
                     
                        if($s_an){
                          foreach ($s_an as $key_an => $value_an) {
                            if($key_an == ''){ $key_an = '-'; }
                            if($value_an == ''){ $value_an = '-'; }
                          ?>
                          <div class="form-group row name_fields">
                            <label class="col-sm-4 col-form-label"><?php echo $key_an;?></label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php echo $value_an;?></span>
                              </div>
                          </div>
                    <?php
                          }
                        }


                       }  ?>
                    <?php
                     // echo render_custom_fields_one( 'assign_to',$rel_id);

                       $s_assign = $this->Common_mdl->getCustomFieldRec($rel_id,'assign_to');
                     
                        if($s_assign){
                          foreach ($s_assign as $key_as => $value_as) {
                            if($key_as == ''){ $key_as = '-'; }
                            if($value_as == ''){ $value_as = '-'; }
                          ?>
                          <div class="form-group row name_fields">
                            <label class="col-sm-4 col-form-label"><?php echo $key_as;?></label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php echo $value_as;?></span>
                              </div>
                          </div>
                    <?php
                          }
                        }

                          ?>
                  </div>
                </div>
              </div>
            </div> -->

             <div class="accordion-panel" style="display: none;">
           <div class="box-division03">
                        <div class="accordion-heading" role="tab" id="headingOne">
                           <h3 class="card-title accordion-title">
                              <a class="accordion-msg">Notes if any other</a>
                           </h3>
                        </div>
                        <div id="collapse" class="panel-collapse">
                           <div class="basic-info-client1">
                              <?php
                                 $crm_assign_notes=( isset($client[0]['crm_assign_notes']) ? $client[0]['crm_assign_notes'] : false);   
                                 $crm_assign_note=render_custom_fields_edit( 'assign_notes',$rel_id,$crm_assign_notes);  
                                  if($crm_assign_note!='') {
                                    echo $crm_assign_note;
                                  } else {
                                 ?>
                         <?php if(isset($client[0]['crm_assign_notes']) && $client[0]['crm_assign_notes']!='') { ?>
                              <div class="form-group row ">
                                 <label class="col-sm-4 col-form-label">Notes</label>
                                 <div class="col-sm-8"><?php if(isset($client[0]['crm_assign_notes']) && ($client[0]['crm_assign_notes']!='') ){ echo $client[0]['crm_assign_notes'];}?>
                                 </div>
                              </div>
                              <?php } ?>
                              <?php } ?>
                              <?php
                                 //echo render_custom_fields_one( 'assign_to',$rel_id);  ?>
                           </div>
                        </div>
            </div>
                     </div>
            <!-- accordion-panel -->  
</div>      
          </div>
          <!-- assignto -->
          <!-- amlchecks -->
          <div id="amlchecks" class="tab-pane fade">
        
            <?php  
            
                 if($client[0]['select_responsible_type']=='staff' || $client[0]['select_responsible_type']=='team' || $client[0]['select_responsible_type']=='departments' || $client[0]['select_responsible_type']=='members' )
                        {
                          $members_css = $department_css = $team_css = $css = "display:block";
                        }else{
                          $members_css = $department_css = $team_css = $css = "display:none";
                        }
              ?>
        
            
            <div class="accordion-panel">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Anti Money Laundering Checks     </a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1">
                    <?php if(isset($client[0]['crm_assign_client_id_verified']) && ($client[0]['crm_assign_client_id_verified']=='on') ){?>
                    <div class="form-group row radio_bts ">
                      <label class="col-sm-4 col-form-label">Client ID Verified</label>
                      <div class="col-sm-8">
                        <input type="checkbox" class="js-small f-right fields" name="client_id_verified" id="client_id_verified" <?php if(isset($client[0]['crm_assign_client_id_verified']) && ($client[0]['crm_assign_client_id_verified']=='on') ){?> checked="checked"<?php } ?> readonly>
                      </div>
                    </div>
                    <?php } ?>
                    <?php if(isset($client[0]['crm_assign_type_of_id']) && ($client[0]['crm_assign_type_of_id']!='') ){?>
                    <div class="form-group row assign_cus_type">
                      <label class="col-sm-4 col-form-label">Type of ID Provided</label>
                      <div class="col-sm-8">
                      
                        <span class="small-info"><?php if(isset($client[0]['crm_assign_type_of_id'])) { echo $client[0]['crm_assign_type_of_id']; } ?></span>
                      </div>
                    </div>
                    <?php } ?>
                     <!-- 29-08-2018 for multiple image upload option -->
                     <?php if(isset($client[0]['proof_attach_file']) && ($client[0]['proof_attach_file']=='on') ){?>
                                       <div class="form-group row" >
                                             <label class="col-sm-4 col-form-label">Attachment</label>
                                             <div class="col-sm-8">
                                               <!--  <input type="file" name="proof_attach_file[]" id="proof_attach_file" multiple="multiple" > -->
                                             </div>
                                      
                                       <div class="attach-files showtm f-right">
                                       <?php 
                                       if(isset($client[0]['proof_attach_file'])){
                                          ?>
                                       <div class="jFiler-items jFiler-row">
                                           <ul class="jFiler-items-list jFiler-items-grid">
                                          <?php
                                          $ex_attach=array_filter(explode(',', $client[0]['proof_attach_file']));
                                       foreach ($ex_attach as $attach_key => $attach_value) {
                                             $replace_val=str_replace(base_url(),'',$attach_value);
                                             $ext = explode(".", $replace_val);
                                           
                                             $res=$this->Common_mdl->geturl_image_or_not($ext[1]);
                                            // echo $res;
                                             if($res=='image'){
                                                ?>
                                             <li class="jFiler-item for_img_<?php echo $attach_key; ?>" data-jfiler-index="3" style="">
                                               <input type="hidden" name="already_upload_img[]" value="<?php echo $attach_value; ?>" >
                                                 <div class="jFiler-item-container">
                                                    <div class="jFiler-item-inner">
                                                       <div class="jFiler-item-thumb">
                                                          <div class="jFiler-item-status"></div>
                                                          <div class="jFiler-item-info">                                        
                                                                                         
                                                          </div>
                                                          <div class="jFiler-item-thumb-image"><img src="<?php echo $attach_value;?>" draggable="false"></div>
                                                       
                                                    </div>
                                                   <!-- <div class="jFiler-item-assets jFiler-row">
                                                          <ul class="list-inline pull-left">
                                                             <li>
                                                                <div class="jFiler-jProgressBar" style="display: none;">
                                                                   <div class="bar"></div>
                                                                </div>
                                                               
                                                             </li>
                                                          </ul>
                                                          <ul class="list-inline pull-right">
                                                             <li><a class="icon-jfi-trash jFiler-item-trash-action remove_file" data-id="<?php echo $attach_key ?>"></a></li>
                                                          </ul>
                                                       </div>-->
                                                 </div>
                                              </li>
                                                <?php
                                                } // if image
                                                else{ ?>
                                              <li class="jFiler-item jFiler-no-thumbnail for_img_<?php echo $attach_key; ?>" data-jfiler-index="2" style="">
                                               <input type="hidden" name="already_upload_img[]" value="<?php echo $attach_value; ?>" >
                                                 <div class="jFiler-item-container">
                                                    <div class="jFiler-item-inner">
                                                       <div class="jFiler-item-thumb">
                                                          <div class="jFiler-item-status"></div>
                                                          <a href="<?php echo $attach_value; ?>" target="_blank" ><div class="jFiler-item-info">                                                               </div></a>
                                                          <div class="jFiler-item-thumb-image"><span class="jFiler-icon-file f-file f-file-ext-odt" style="background-color: rgb(63, 79, 211);"><?php echo $attach_value; ?></span></div>
                                                       </div>
                                                       <!-- <div class="jFiler-item-assets jFiler-row">
                                                          <ul class="list-inline pull-left">
                                                             <li>
                                                                <div class="jFiler-jProgressBar" style="display: none;">
                                                                   <div class="bar"></div>
                                                                </div>
                                                               
                                                             </li>
                                                          </ul>
                                                          <ul class="list-inline pull-right">
                                                             <li><a class="icon-jfi-trash jFiler-item-trash-action remove_file" data-id="<?php echo $attach_key ?>" ></a></li>
                                                          </ul>
                                                       </div>-->
                                                    </div>
                                                 </div>
                                              </li>
                                             <?php } //else end

                                               } // foreach
                                               ?>
                                               </ul>
                                               </div>
                                               <?php
                                          }
                                       ?>
                                      </div>
                                       </div>

                                       <?php } ?>
                                 <!-- en dof image upload for type of ids -->
                      <?php
                            $new_arr=array();
                            $all_val=(isset($client[0]['crm_assign_type_of_id']) && $client[0]['crm_assign_type_of_id']!='')? explode(',',$client[0]['crm_assign_type_of_id']):$new_arr; 
                      ?>
                    <div class="form-group row name_fields spanassign" <?php if(in_array('Other_Custom',$all_val) && (isset($client[0]['crm_assign_client_id_verified']) && ($client[0]['crm_assign_client_id_verified']=='on'))) { }else { ?> style="display:none" <?php } ?> >
                      <label class="col-sm-4 col-form-label">Other Custom</label>
                      <div class="col-sm-8">
                       <!--  <input type="text" name="assign_other_custom" id="assign_other_custom" placeholder="" value="<?php if(isset($client[0]['crm_assign_other_custom']) && ($client[0]['crm_assign_other_custom']!='') ){ echo $client[0]['crm_assign_other_custom'];}?>" class="fields"> -->
                         <span class="small-info"><?php if(isset($client[0]['crm_assign_other_custom']) && ($client[0]['crm_assign_other_custom']!='') ){ echo $client[0]['crm_assign_other_custom'];}?></span>
                      </div>
                    </div>
                    <?php if(isset($client[0]['crm_assign_proof_of_address']) && ($client[0]['crm_assign_proof_of_address']=='on') ){?>
                    <div class="form-group row radio_bts ">
                      <label class="col-sm-4 col-form-label">Proof of Address</label>
                      <div class="col-sm-8">
                        <input type="checkbox" class="js-small f-right fields" name="proof_of_address" id="proof_of_address" <?php if(isset($client[0]['crm_assign_proof_of_address']) && ($client[0]['crm_assign_proof_of_address']=='on') ){?> checked="checked"<?php } ?> readonly>
                      </div>
                    </div>
                    <?php } ?>
                    <?php if(isset($client[0]['crm_assign_meeting_client']) && ($client[0]['crm_assign_meeting_client']=='on') ){?>
                    <div class="form-group row radio_bts ">
                      <label class="col-sm-4 col-form-label">Meeting with the Client</label>
                      <div class="col-sm-8">
                        <input type="checkbox" class="js-small f-right fields" name="meeting_client" id="meeting_client" <?php if(isset($client[0]['crm_assign_meeting_client']) && ($client[0]['crm_assign_meeting_client']=='on') ){?> checked="checked"<?php } ?> readonly>
                      </div>
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
           </div>      
         
          <!-- end of amlchecks -->   
          <div id="other" class="tab-pane fade">
      <div class="masonry-container floating_set">
      <div class="grid-sizer"></div>
            <div class="accordion-panel">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Other</a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1" id="others_section">

                     <?php if(isset($client[0]['crm_previous_accountant']) && ($client[0]['crm_previous_accountant']!='') ){?>
                    <div class="form-group row radio_bts others_details" id="<?php echo $this->Common_mdl->get_order_details(78); ?>">
                      <label class="col-sm-4 col-form-label">Previous Accounts</label>
                      <div class="col-sm-8">
                        <input type="checkbox" class="js-small f-right fields" name="previous_account" id="previous_account" value="on" <?php if(isset($client[0]['crm_previous_accountant']) && ($client[0]['crm_previous_accountant']=='on') ){?> checked="checked"<?php } ?> readonly data-id="preacc">
                      </div>
                    </div>
                    <?php } ?>
                    <?php 
                     (isset($client[0]['crm_previous_accountant']) && $client[0]['crm_previous_accountant'] == 'on') ? $pre =  "block" : $pre = 'none';                     
                      
                      ?>
                    <div id="enable_preacc" style="display:<?php echo $pre;?>;">

                    <?php if(isset($client[0]['crm_other_name_of_firm']) && ($client[0]['crm_other_name_of_firm']!='') ){ ?>
                      <div class="form-group row name_fields">
                        <label class="col-sm-4 col-form-label">Name of the Firm</label>
                        <div class="col-sm-8">
                        
                          <span class="small-info"><?php if(isset($client[0]['crm_other_name_of_firm']) && ($client[0]['crm_other_name_of_firm']!='') ){ echo $client[0]['crm_other_name_of_firm'];}?></span>
                        </div>
                      </div>
                      <?php } ?>

                      <?php if(isset($client[0]['crm_other_address']) && ($client[0]['crm_other_address']!='') ){ ?>
                      <div class="form-group row ">
                        <label class="col-sm-4 col-form-label">Address</label>
                        <div class="col-sm-8">
                        
                          <p class="for-address"><?php if(isset($client[0]['crm_other_address']) && ($client[0]['crm_other_address']!='') ){ echo $client[0]['crm_other_address'];}?></p>
                        </div>
                      </div>
                    <?php } ?>
                    <!-- preacc close-->
                    <?php if(isset($user[0]['crm_phone_number']) && ($user[0]['crm_phone_number']!='') ){  ?>
                    <div class="form-group row name_fields others_details" id="<?php echo $this->Common_mdl->get_order_details(79); ?>">
                      <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(79); ?></label>
                      <div class="col-sm-8">
                     
                        <span class="small-info"><?php if(isset($user[0]['crm_phone_number']) && ($user[0]['crm_phone_number']!='') ){ echo $user[0]['crm_phone_number'];}?></span>
                      </div>
                    </div>
                    <?php } ?>

                    <?php if(isset($user[0]['crm_email']) && ($user[0]['crm_email']!='') ){  ?>
                    <div class="form-group row name_fields others_details" id="<?php echo $this->Common_mdl->get_order_details(80); ?>">
                      <label class="col-sm-4 col-form-label">Email Address</label>
                      <div class="col-sm-8">
                        <!-- <input type="email" name="emailid" id="emailid" placeholder="" value="<?php if(isset($client[0]['crm_email']) && ($client[0]['crm_email']!='') ){ echo $client[0]['crm_email'];}?>" class="fields"> -->
                        <span class="small-info"><?php if(isset($client[0]['crm_email']) && ($client[0]['crm_email']!='') ){ echo $client[0]['crm_email'];}?></span>
                      </div>
                    </div>
                    <?php } ?>

                    <?php if(isset($user[0]['crm_other_chase_for_info']) && ($user[0]['crm_other_chase_for_info']!='') ){  ?>
                    <div class="form-group row radio_bts  others_details" id="<?php echo $this->Common_mdl->get_order_details(81); ?>">
                      <label class="col-sm-4 col-form-label">Chase for Information</label>
                      <div class="col-sm-8">
                        <input type="checkbox" class="js-small f-right fields" name="chase_for_info" id="chase_for_info" <?php if(isset($client[0]['crm_other_chase_for_info']) && ($client[0]['crm_other_chase_for_info']=='on') ){?> checked="checked"<?php } ?> readonly>
                      </div>
                    </div>
                   
                    <?php } ?><!-- previous account hide -->
                     </div>
                    <?php
                      $crm_other_notes=( isset($client[0]['crm_other_notes']) ? $client[0]['crm_other_notes'] : false);   
                      $crm_other_note= render_custom_fields_edit( 'other_notes',$rel_id,$crm_other_notes);  
                      if($crm_other_note!='') {
                     $s_notes = $this->Common_mdl->getCustomFieldRec($rel_id,'other_notes');                     
                        if($s_notes){
                          foreach ($s_notes as $key_on => $value_on) {
                            if($key_on == ''){ $key_on = '-'; }
                            if($value_on == ''){ $value_on = '-'; }
                          ?>
                          <div class="form-group row name_fields>
                            <label class="col-sm-4 col-form-label"><?php echo $key_on;?></label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php echo $value_on;?></span>
                              </div>
                          </div>
                    <?php
                          } }    
                       }else{
                      ?>
                    <div class="form-group row " <?php if(isset($client[0]['crm_other_chase_for_info']) && ($client[0]['crm_other_chase_for_info']=='on') ){?> <?php }else{ ?> style="display: none;" <?php } ?>>
                      <label class="col-sm-4 col-form-label">Notes</label>
                      <div class="col-sm-8">
                        <!-- <textarea rows="4" name="other_notes" id="other_notes" class="form-control fields"><?php if(isset($client[0]['crm_other_notes']) && ($client[0]['crm_other_notes']!='') ){ echo $client[0]['crm_other_notes'];}?></textarea> -->
                        <p class="for-address"><?php if(isset($client[0]['crm_other_notes']) && ($client[0]['crm_other_notes']!='') ){ echo $client[0]['crm_other_notes'];}?></p>
                      </div>
                    </div>
                    <?php } ?>
                
                 
            <!-- accordion-panel -->
            <div class="accordion-panel">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Additional Information - Internal Notes</a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1">
                  <?php if(isset($client[0]['crm_other_internal_notes']) && ($client[0]['crm_other_internal_notes']!='') ){ ?>
                    <div class="form-group row ">
                      <label class="col-sm-4 col-form-label">Notes</label>
                      <div class="col-sm-8">                      
                        <p class="for-address"><?php if(isset($client[0]['crm_other_internal_notes']) && ($client[0]['crm_other_internal_notes']!='') ){ echo $client[0]['crm_other_internal_notes'];}?></p>
                      </div>
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
            <!-- accordion-panel -->
                       <!-- accordion-panel -->
            <div class="accordion-panel">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg" style="display: inline-block;">Client Login
 <input type="checkbox" class="js-small f-right fields" name="show_login" id="show_login" 
 data-id="show_login">
                                                      </a>
                                                   </h3>

                </div>
                <div id="collapse" class="panel-collapse show_login" style="display: none">
                  <div class="basic-info-client1">
                      <!-- 30-08-2018 -->
                      <?php if(isset($user[0]['username']) && ($user[0]['username']!='') ){ ?>
                    <div class="form-group row name_fields">
                      <label class="col-sm-4 col-form-label">Username</label>
                      <div class="col-sm-8">                        
                        <span class="small-info"><?php if(isset($user[0]['username']) && ($user[0]['username']!='') ){ echo $user[0]['username'];}?></span>
                      </div>
                    </div>
                    <?php } ?>
                    <!-- end of 30-08-2018 -->
                  </div>
                </div>
              </div>
            </div>
            <!-- accordion-panel -->


            <div class="accordion-panel">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Client Source</a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1">
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Source</label>
                      <div class="col-sm-8">
                        <!-- <select name="source" id="source" class="form-control fields">
                          <option value="" selected="selected">--Select--</option>
                          <option value="Google" <?php if(isset($client[0]['crm_assign_source']) && $client[0]['crm_assign_source']=='Google') {?> selected="selected"<?php } ?>>Google</option>
                          <option value="FB" <?php if(isset($client[0]['crm_assign_source']) && $client[0]['crm_assign_source']=='FB') {?> selected="selected"<?php } ?>>FB</option>
                          <option value="Website" <?php if(isset($client[0]['crm_assign_source']) && $client[0]['crm_assign_source']=='Website') {?> selected="selected"<?php } ?>>Website</option>
                          <option value="Existing Client" <?php if(isset($client[0]['crm_assign_source']) && $client[0]['crm_assign_source']=='Existing Client') {?> selected="selected"<?php } ?>>Existing Client</option>
                        </select> -->
                        <span class="small-info"><?php if(isset($client[0]['crm_assign_source'])){ echo $client[0]['crm_assign_source']; } ?></span>
                      </div>
                    </div>
                    <?php if(isset($client[0]['crm_assign_source']) && $client[0]['crm_assign_source']=='Existing Client') { ?>
                    <div class="form-group row name_fields">
                      <label class="col-sm-4 col-form-label">Refered by</label>
                      <div class="col-sm-8">
                      
                        <span class="small-info">  <?php foreach ($referby as $referbykey => $referbyvalue) {  if(isset($client[0]['crm_refered_by']) && $client[0]['crm_refered_by']==$referbyvalue['id']) { echo $referbyvalue['crm_first_name']; } } ?></span>
                      </div>
                    </div>
                    <?php } ?>
                    <div class="form-group row name_fields">
                      <label class="col-sm-4 col-form-label">Relationship to Client</label>
                      <div class="col-sm-8">
                        <!--  <input type="text" name="refer_exist_client" id="refer_exist_client" placeholder="" value="" class="fields"> -->
                        <!-- <select name="refer_exist_client" id="refer_exist_client" class="fields">
                          <option value=''>--Select one--</option>
                          <?php foreach ($referby as $referbykey => $referbyvalue) {
                            # code...
                            ?>
                          <option value="<?php echo $referbyvalue['crm_first_name'];?>" <?php if(isset($client[0]['crm_refered_by']) && $client[0]['crm_refered_by']==$referbyvalue['crm_first_name']) {?> selected="selected"<?php } ?>><?php echo $referbyvalue['crm_first_name'];?></option>
                          <?php } ?>
                        </select> -->
                        <span class="small-info"><?php if(isset($client[0]['crm_assign_relationship_client']) && ($client[0]['crm_assign_relationship_client']!='') ){ echo $client[0]['crm_assign_relationship_client'];}?></span>
                      </div>
                    </div>
                    <?php
                      $crm_assign_relationship_client=( isset($client[0]['crm_assign_relationship_client']) ? $client[0]['crm_assign_relationship_client'] : false);   
                      $crm_assign_relationship=render_custom_fields_edit( 'relationship_client',$rel_id,$crm_assign_relationship_client);  
                       if($crm_assign_relationship!='') {
                       // echo $crm_assign_relationship;

                          $s_rel = $this->Common_mdl->getCustomFieldRec($rel_id,'relationship_client');
                     
                        if($s_rel){
                          foreach ($s_rel as $key_rel => $value_rel) {
                            if($key_rel == ''){ $key_rel = '-'; }
                            if($value_rel == ''){ $value_rel = '-'; }
                          ?>
                          <div class="form-group row name_fields">
                            <label class="col-sm-4 col-form-label"><?php echo $key_rel;?></label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php echo $value_rel;?></span>
                              </div>
                          </div>
                    <?php
                          }
                        }

                       } ?>
                  </div>
                </div>
              </div>
            </div>
            <!-- accordion-panel -->  
          
            <div class="accordion-panel" style="display: none;">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Notes if any other</a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1">
                  <?php if(isset($client[0]['crm_other_any_notes']) && ($client[0]['crm_other_any_notes']!='') ){  ?>
                    <div class="form-group row ">
                      <label class="col-sm-4 col-form-label">Notes</label>
                      <div class="col-sm-8">
                       <!--  <textarea rows="4" name="other_any_notes" id="other_any_notes" class="form-control fields"><?php if(isset($client[0]['crm_other_any_notes']) && ($client[0]['crm_other_any_notes']!='') ){ echo $client[0]['crm_other_any_notes'];}?></textarea> -->
                       <p class="for-address"><?php if(isset($client[0]['crm_other_any_notes']) && ($client[0]['crm_other_any_notes']!='') ){ echo $client[0]['crm_other_any_notes'];}?></p>
                      </div>
                    </div>
                    <?php } ?>
                    <?php
                      //echo render_custom_fields_one( 'other',$rel_id);
                       $s_other = $this->Common_mdl->getCustomFieldRec($rel_id,'other');
                     
                        if($s_other){
                          foreach ($s_other as $key_other => $value_other) {
                            if($key_other == ''){ $key_other = '-'; }
                            if($value_other == ''){ $value_other = '-'; }
                          ?>
                          <div class="form-group row name_fields">
                            <label class="col-sm-4 col-form-label"><?php echo $key_other;?></label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php echo $value_other;?></span>
                              </div>
                          </div>
                    <?php
                          }
                        }
                    ?>
                  </div>
                  </div>
                  </div>
                  </div>



















                    <!--- End --> 
                </div>
              </div>
            </div>
            <!-- accordion-panel -->
      </div>
          </div>
          </div>
          

          <!-- other -->
          <!-- referral tab section -->
             <div id="referral" class="tab-pane fade">
      <div class="masonry-container floating_set">
      <div class="grid-sizer"></div>
        <div class="accordion-panel">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Invite Client</a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1">
                  <?php if(isset($client[0]['crm_other_invite_use']) && ($client[0]['crm_other_invite_use']!='') ){  ?>
                    <div class="form-group row radio_bts ">
                      <label class="col-sm-4 col-form-label">Invite to use our system</label>
                      <div class="col-sm-8">
                        <input type="checkbox" class="js-small f-right fields" name="invite_use" id="invite_use" <?php if(isset($client[0]['crm_other_invite_use']) && ($client[0]['crm_other_invite_use']=='on') ){?> checked="checked"<?php } ?> readonly>
                      </div>
                    </div>
                    <?php } ?>
                    <div style="display: none;"> <!-- hided div -->
                     <?php if(isset($client[0]['crm_other_crm']) && ($client[0]['crm_other_crm']!='') ){  ?>
                    <div class="form-group row radio_bts ">
                      <label class="col-sm-4 col-form-label">crm</label>
                      <div class="col-sm-8">
                        <input type="checkbox" class="js-small f-right fields" name="other_crm" id="other_crm" <?php if(isset($client[0]['crm_other_crm']) && ($client[0]['crm_other_crm']=='on') ){?> checked="checked"<?php } ?> readonly>
                      </div>
                    </div>
                    <?php } ?>
                     <?php if(isset($client[0]['crm_other_proposal']) && ($client[0]['crm_other_proposal']!='') ){  ?>
                    <div class="form-group row radio_bts ">
                      <label class="col-sm-4 col-form-label">Proposal</label>
                      <div class="col-sm-8">
                        <input type="checkbox" class="js-small f-right fields" name="other_proposal" id="other_proposal" <?php if(isset($client[0]['crm_other_proposal']) && ($client[0]['crm_other_proposal']=='on') ){?> checked="checked"<?php } ?> readonly>
                      </div>
                    </div>
                    <?php } ?>
                      <?php if(isset($client[0]['crm_other_task']) && ($client[0]['crm_other_task']!='') ){  ?>
                    <div class="form-group row radio_bts ">
                      <label class="col-sm-4 col-form-label">Tasks</label>
                      <div class="col-sm-8">
                        <input type="checkbox" class="js-small f-right fields" name="other_task" id="other_task" <?php if(isset($client[0]['crm_other_task']) && ($client[0]['crm_other_task']=='on') ){?> checked="checked"<?php } ?> readonly> 
                      </div>
                    </div>
                    <?php } ?>
                    </div><!-- hide div -->

                  <?php if(isset($client[0]['crm_other_send_invit_link']) && ($client[0]['crm_other_send_invit_link']!='') ){  ?>
                    <div class="form-group row name_fields">
                      <label class="col-sm-4 col-form-label">Send invitation link</label>
                      <div class="col-sm-8">
                        <!-- <input type="text" name="send_invit_link" id="send_invit_link" placeholder="" value="<?php if(isset($client[0]['crm_other_send_invit_link']) && ($client[0]['crm_other_send_invit_link']!='') ){ echo $client[0]['crm_other_send_invit_link'];}?>" class="fields"> -->
                         <p class="for-address"><?php if(isset($client[0]['crm_other_send_invit_link']) && ($client[0]['crm_other_send_invit_link']!='') ){ echo $client[0]['crm_other_send_invit_link'];}?></p>
                      </div>
                    </div>
                    <?php } ?>
                <!--     <div class="form-group row name_fields">
                      <label class="col-sm-4 col-form-label">Username</label>
                      <div class="col-sm-8">
                        
                        <span class="small-info"><?php if(isset($user[0]['username']) && ($user[0]['username']!='') ){ echo $user[0]['username'];}?></span>
                      </div>
                    </div> -->
                  <!--   <div class="form-group row name_fields">
                      <label class="col-sm-4 col-form-label">Password</label>
                      <div class="col-sm-8">
                        <input type="password" name="password" id="password" placeholder="" value="<?php if(isset($user[0]['confirm_password']) && ($user[0]['confirm_password']!='') ){ echo $user[0]['confirm_password'];}?>" class="fields">
                      </div>
                    </div> -->
                  </div>
                </div>
              </div>
            </div>
            <!-- accordion-panel -->
      </div>
      </div>
          <!-- en dof referral tab section -->
          <div id="confirmation-statement" class="tab-pane fade">
        <div class="masonry-container floating_set">
        <div class="grid-sizer"></div>
            <div class="accordion-panel">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Important Information</a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1">

                   <?php if(isset($client[0]['crm_companies_house_authorisation_code']) && ($client[0]['crm_companies_house_authorisation_code']!='') ){  ?> 
                    <div class="form-group row name_fields">
                      <label class="col-sm-4 col-form-label">Authentication Code</label>
                      <div class="col-sm-8">
                        <!-- <input type="text" name="confirmation_auth_code" id="confirmation_auth_code" placeholder="" value="<?php if(isset($client[0]['crm_confirmation_auth_code']) && ($client[0]['crm_confirmation_auth_code']!='') ){ echo $client[0]['crm_confirmation_auth_code'];}?>" class="fields"> -->
                        <span class="small-info"><?php if(isset($client[0]['crm_companies_house_authorisation_code'])){ echo $client[0]['crm_companies_house_authorisation_code']; } ?></span>
                      </div>
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
            <!-- accordion-panel -->
            <div class="accordion-panel">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Confirmation Statement</a>
                  </h3>
                </div>
               


                <div id="collapse" class="panel-collapse">
                           <div class="basic-info-client1" id="box1">
                              <?php 
                                 (isset(json_decode($client[0]['conf_statement'])->reminder) && $client[0]['conf_statement'] != '') ? $one =  json_decode($client[0]['conf_statement'])->reminder : $one = '';
                                 ($one!='') ? $one_cs = "" : $one_cs = "display:none;";
                                 
                                 ?> 
                                  <?php if(isset($client[0]['crm_confirmation_statement_date']) && ($client[0]['crm_confirmation_statement_date']!='') ){  ?>                                    
                              <div class="form-group row name_fields sorting_conf" id="<?php echo $this->Common_mdl->get_order_details(23); ?>">
                                 <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Next Statement Date</label>
                                 <div class="col-sm-8 edit-field-popup1">
                                   <?php 
                                    if(isset($client[0]['crm_confirmation_statement_date']) && ($client[0]['crm_confirmation_statement_date']!='') ){ echo date("d-m-Y", strtotime($client[0]['crm_confirmation_statement_date'])) ;}?>
                                 </div>
                              </div>
                              <?php } ?>
                             <?php if(isset($client[0]['crm_confirmation_statement_due_date']) && ($client[0]['crm_confirmation_statement_due_date']!='') ){  ?>  
                              <div class="form-group row name_fields sorting_conf" id="<?php echo $this->Common_mdl->get_order_details(24); ?>">
                                 <label class="col-sm-4 col-form-label">Due By</label>
                                 <div class="col-sm-8 edit-field-popup1">
                                    <?php if(isset($client[0]['crm_confirmation_statement_due_date']) && ($client[0]['crm_confirmation_statement_due_date']!='') ){ echo date("d-m-Y", strtotime($client[0]['crm_confirmation_statement_due_date']));}?>
                                 </div>
                              </div>
                              <?php } ?>
                           </div>
                        </div>

              </div>
            </div>
            <!-- accordion-panel -->
            <div class="accordion-panel enable_conf" style="<?php echo $one_cs;?>">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Confirmation Reminders</a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1" id="box2">
                    <div class="form-group row name_fields">
                     <!--  <label class="col-sm-4 col-form-label">Next Reminder Date</label> -->
                      <div class="col-sm-8">
                        <input type="hidden" name="confirmation_next_reminder" id="confirmation_next_reminder" placeholder="" value="<?php if(isset($client[0]['crm_confirmation_email_remainder']) && ($client[0]['crm_confirmation_email_remainder']!='') ){ echo $client[0]['crm_confirmation_email_remainder'];}?>" class="fields datepicker">
                        <a name="add_custom_reminder_link" id="add_custom_reminder_link" href="<?php echo base_url();?>User/reminder_settings/4" target="_blank">As per firm setting</a>
                        <input type="checkbox" class="js-small f-right fields" name="confirmation_next_reminder_date" id="confirmation_next_reminder_date"  checked="checked" onchange="check_checkbox();" readonly>
                      </div>
                    </div>
                       <?php if(isset($client[0]['crm_confirmation_create_task_reminder']) && ($client[0]['crm_confirmation_statement_due_date']!='') ){  ?>  
                    <div class="form-group row radio_bts sorting_conf" id="<?php echo $this->Common_mdl->get_order_details(26); ?>">
                      <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                      <div class="col-sm-8">
                        <input type="checkbox" class="js-small f-right fields" name="create_task_reminder" id="create_task_reminder" <?php if(isset($client[0]['crm_confirmation_create_task_reminder']) && ($client[0]['crm_confirmation_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> readonly>
                      </div>
                    </div>
                    <?php } ?>
                    <div class="form-group row radio_bts sorting_conf" id="<?php echo $this->Common_mdl->get_order_details(27); ?>" style="">
                      <label class="col-sm-4 col-form-label" id="add_custom_reminder_label" name="add_custom_reminder_label">Add Custom Reminder</label>
                      <div class="col-sm-8">
                        <!-- <input type="checkbox" class="js-small f-right fields" name="add_custom_reminder" id="add_custom_reminder" <?php if(isset($client[0]['crm_confirmation_add_custom_reminder']) && ($client[0]['crm_confirmation_add_custom_reminder']=='on') ){?> checked="checked"<?php } ?>> -->
                        <!-- <textarea rows="3" placeholder="" name="add_custom_reminder" id="add_custom_reminder" class="fields"><?php if(isset($client[0]['crm_confirmation_add_custom_reminder']) && ($client[0]['crm_confirmation_add_custom_reminder']!='') ){ echo $client[0]['crm_confirmation_add_custom_reminder'];}?></textarea> -->
                        <a name="add_custom_reminder" id="add_custom_reminder" href="<?php echo base_url();?>User/reminder_settings/4" target="_blank">Click to add Custom Reminder</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- accordion-panel -->
            <div class="accordion-panel">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg"></a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1" id="box3">


                  <?php if(isset($client[0]['crm_confirmation_officers']) && ($client[0]['crm_confirmation_officers']!='') ){  ?>
                    <div class="form-group row sorting_conf" id="<?php echo $this->Common_mdl->get_order_details(19); ?>">
                      <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(19); ?></label>
                      <div class="col-sm-8">
                        <!-- <textarea rows="3" placeholder="" name="confirmation_officers" id="confirmation_officers" class="fields"><?php if(isset($client[0]['crm_confirmation_officers']) && ($client[0]['crm_confirmation_officers']!='') ){ echo $client[0]['crm_confirmation_officers'];}?></textarea> -->
                        <p class="for-address"><?php if(isset($client[0]['crm_confirmation_officers']) && ($client[0]['crm_confirmation_officers']!='') ){ echo $client[0]['crm_confirmation_officers'];}?></p>
                      </div>
                    </div>
                    <?php } ?>
                    <?php if(isset($client[0]['crm_share_capital']) && ($client[0]['crm_share_capital']!='') ){ ?>

                    <div class="form-group row sorting_conf" id="<?php echo $this->Common_mdl->get_order_details(20); ?>">
                      <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(20); ?></label>
                      <div class="col-sm-8">
                        <!-- <textarea rows="3" placeholder="" name="share_capital" id="share_capital" class="fields"><?php if(isset($client[0]['crm_share_capital']) && ($client[0]['crm_share_capital']!='') ){ echo $client[0]['crm_share_capital'];}?></textarea> -->
                        <p class="for-address"><?php if(isset($client[0]['crm_share_capital']) && ($client[0]['crm_share_capital']!='') ){ echo $client[0]['crm_share_capital'];}?></p>
                      </div>
                    </div>
                    <?php } ?>

                    <?php if(isset($client[0]['crm_shareholders']) && ($client[0]['crm_shareholders']!='') ){ ?>
                    <div class="form-group row sorting_conf" id="<?php echo $this->Common_mdl->get_order_details(21); ?>">
                      <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(21); ?></label>
                      <div class="col-sm-8">
                        <!-- <textarea rows="3" placeholder="" name="shareholders" id="shareholders" class="fields"><?php if(isset($client[0]['crm_shareholders']) && ($client[0]['crm_shareholders']!='') ){ echo $client[0]['crm_shareholders'];}?></textarea> -->
                        <p class="for-address"><?php if(isset($client[0]['crm_shareholders']) && ($client[0]['crm_shareholders']!='') ){ echo $client[0]['crm_shareholders'];}?></p>
                      </div>
                    </div>
                    <?php } ?>

                    <?php if(isset($client[0]['crm_people_with_significant_control']) && ($client[0]['crm_people_with_significant_control']!='') ){  ?>
                    <div class="form-group row sorting_conf" id="<?php echo $this->Common_mdl->get_order_details(22); ?>">
                      <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(22); ?></label>
                      <div class="col-sm-8">
                        <!-- <textarea rows="3" placeholder="" name="people_with_significant_control" id="people_with_significant_control" class="fields"><?php if(isset($client[0]['crm_people_with_significant_control']) && ($client[0]['crm_people_with_significant_control']!='') ){ echo $client[0]['crm_people_with_significant_control'];}?></textarea> -->
                        <p class="for-address"><?php if(isset($client[0]['crm_people_with_significant_control']) && ($client[0]['crm_people_with_significant_control']!='') ){ echo $client[0]['crm_people_with_significant_control'];}?></p>
                      </div>
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
            <!-- accordion-panel -->
            <div class="accordion-panel">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Notes if any other            </a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1">
                  <?php if(isset($client[0]['crm_confirmation_notes']) && ($client[0]['crm_confirmation_notes']!='') ){ ?>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(28); ?></label>
                      <div class="col-sm-8">
                        <!-- <textarea rows="3" placeholder="" name="confirmation_notes" id="confirmation_notes" class="fields"><?php if(isset($client[0]['crm_confirmation_notes']) && ($client[0]['crm_confirmation_notes']!='') ){ echo $client[0]['crm_confirmation_notes'];}?></textarea> -->
                        <p class="for-address"><?php if(isset($client[0]['crm_confirmation_notes']) && ($client[0]['crm_confirmation_notes']!='') ){ echo $client[0]['crm_confirmation_notes'];}?></p>
                      </div>
                    </div>
                    <?php } ?>             
                  </div>
                </div>
              </div>
            </div>
      </div>
            <!-- accordion-panel -->
          </div>
          <!-- confirmation-statement -->
          <div id="accounts" class="tab-pane fade">
            <div class="accordion-panel">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Important Information</a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1" id="accounts_box">
                  <?php if(isset($client[0]['crm_companies_house_authorisation_code']) && $client[0]['crm_companies_house_authorisation_code']!=''){  ?>
                    <div class="form-group row name_fields accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(91); ?>">
                      <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(91); ?></label>
                      <div class="col-sm-8">                       
                       <span class="small-info"><?php if(isset($client[0]['crm_companies_house_authorisation_code'])){ echo $client[0]['crm_companies_house_authorisation_code']; } ?></span>
                      </div>
                    </div>
                    <?php } ?>

                    <?php if(isset($client[0]['crm_accounts_utr_number']) && ($client[0]['crm_accounts_utr_number']!='') ){  ?>
                    <div class="form-group row name_fields accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(92); ?>">
                      <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(92); ?></label>
                      <div class="col-sm-8">
                        <!-- <input type="number" name="accounts_utr_number" id="accounts_utr_number" placeholder="" value="<?php if(isset($client[0]['crm_accounts_utr_number']) && ($client[0]['crm_accounts_utr_number']!='') ){ echo $client[0]['crm_accounts_utr_number'];}?>" class="fields"> -->
                        <span class="small-info"><?php if(isset($client[0]['crm_accounts_utr_number']) && ($client[0]['crm_accounts_utr_number']!='') ){ echo $client[0]['crm_accounts_utr_number'];}?></span>
                      </div>
                    </div>
                    <?php } ?>

                    <?php if(isset($client[0]['crm_companies_house_email_remainder']) && ($client[0]['crm_companies_house_email_remainder']=='on') ){?> 

                    <div class="form-group row radio_bts accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(93); ?>">
                      <label class="col-sm-4 col-form-label">Companies House Reminders</label>
                      <div class="col-sm-8">
                        <input type="checkbox" class="js-small f-right fields" name="company_house_reminder" id="company_house_reminder" <?php if(isset($client[0]['crm_companies_house_email_remainder']) && ($client[0]['crm_companies_house_email_remainder']=='on') ){?> checked="checked"<?php } ?> readonly>
                      </div>
                    </div>
                    <?php }  ?>
                  </div>
                </div>
              </div>
            </div>
            <!-- accordion-panel -->
            <div class="accordion-panel">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Accounts</a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1" id="accounts_box1">
                    <?php 
                      (isset(json_decode($client[0]['accounts'])->reminder) && $client[0]['accounts'] != '') ? $two =  json_decode($client[0]['accounts'])->reminder : $two = '';
                      
                      ($two !='') ? $two_acc = "" : $two_acc = "display:none;";
                      
                      ?>
                      <?php if(isset($client[0]['crm_ch_yearend']) && ($client[0]['crm_ch_yearend']!='') ){ ?>
                    <div class="form-group row name_fields  accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(94); ?>" >
                      <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Next accounts made up to</label>
                      <div class="col-sm-8">
                        <!-- <input type="text" name="accounts_next_made_up_to" id="accounts_next_made_up_to" placeholder="" value="<?php if(isset($company['accounts']['next_made_up_to'])){ echo $company['accounts']['next_made_up_to']; }  ?>" class="fields datepicker"> -->
                        <span class="small-info"><?php if(isset($client[0]['crm_ch_yearend']) && ($client[0]['crm_ch_yearend']!='') ){ echo date("d-m-Y", strtotime($client[0]['crm_ch_yearend']));}?></span>
                      </div>
                    </div>
                    <?php } ?>
                    <?php if(isset($client[0]['crm_ch_accounts_next_due']) && ($client[0]['crm_ch_accounts_next_due']!='') ){ ?>
                    <div class="form-group row name_fields  accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(95); ?>">
                      <label class="col-sm-4 col-form-label">Accounts Due Date - Companies House</label>
                      <div class="col-sm-8">
                        <!-- <input type="text" name="accounts_next_due" id="accounts_next_due" placeholder="" value="<?php if(isset($company['accounts']['next_due'])){ echo $company['accounts']['next_due']; }  ?>" class="fields datepicker"> -->
                        <span class="small-info"><?php if(isset($client[0]['crm_ch_accounts_next_due']) && ($client[0]['crm_ch_accounts_next_due']!='') ){ echo date("d-m-Y", strtotime($client[0]['crm_ch_accounts_next_due']));}?></span>
                      </div>
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
            <!-- accordion-panel -->
            <div class="accordion-panel enable_acc" style="<?php echo $two_acc?>">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Accounts Reminders</a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1" id="accounts_box2">
                    <div class="form-group row name_fields">
                     <!--  <label class="col-sm-4 col-form-label">Next Reminder Date</label> -->
                      <div class="col-sm-8">                 
                        <a id="accounts_custom_reminder_link" name="accounts_custom_reminder_link" href="<?php echo base_url();?>User/reminder_settings/3" target="_blank">As per firm setting</a>
                        <input type="checkbox" class="js-small f-right fields" name="accounts_next_reminder_date" id="accounts_next_reminder_date"  checked="checked" onchange="check_checkbox1();" readonly>
                      </div>
                    </div>
                    <div class="form-group row radio_bts accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(97); ?>">
                      <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                      <div class="col-sm-8">
                        <input type="checkbox" class="js-small f-right fields" name="accounts_create_task_reminder" id="accounts_create_task_reminder" <?php if(isset($client[0]['crm_accounts_create_task_reminder']) && ($client[0]['crm_accounts_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> readonly>
                      </div>
                    </div>
                    <div class="form-group row radio_bts accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(98); ?>" style="">
                      <label id="accounts_custom_reminder_label" name="accounts_custom_reminder_label" class="col-sm-4 col-form-label">Add Custom Reminder</label>
                      <div class="col-sm-8">
                        <!-- <input type="checkbox" class="js-small f-right fields" name="accounts_custom_reminder" id="accounts_custom_reminder" <?php if(isset($client[0]['crm_accounts_custom_reminder']) && ($client[0]['crm_accounts_custom_reminder']=='on') ){?> checked="checked"<?php } ?>> -->
                        <!-- <textarea rows="4" name="accounts_custom_reminder" id="accounts_custom_reminder" class="form-control fields"><?php if(isset($client[0]['crm_accounts_custom_reminder']) && ($client[0]['crm_accounts_custom_reminder']!='') ){ echo $client[0]['crm_accounts_custom_reminder'];}?></textarea> -->
                        <a name="accounts_custom_reminder" id="accounts_custom_reminder" href="<?php echo base_url();?>User/reminder_settings/3" target="_blank">Click to add Custom Reminder</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- accordion-panel -->
            <div class="companytax" style="<?php echo $taxtab;?>">
              <div class="box-division03">
                <div class="accordion-panel">
                  <div class="accordion-heading" role="tab" id="headingOne">
                    <h3 class="card-title accordion-title">
                      <a class="accordion-msg">Tax Return   </a>
                    </h3>
                  </div>
                <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1" id="accounts_box3">
                              <?php if(isset($client[0]['crm_ch_yearend']) && $client[0]['crm_ch_yearend']!=''){ ?>
                                 <div class="form-group row name_fields accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(95); ?>">
                                    <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Accounts Due Date - HMRC</label>
                                    <div class="col-sm-8">
                                     <?php echo date("F j, Y", strtotime("+1 years", strtotime($client[0]['crm_ch_yearend'])));
                                          ?>
                                    </div>
                                 </div>
                                 <?php } ?>

                                <?php if(isset($client[0]['crm_ch_accounts_next_due']) && $client[0]['crm_ch_accounts_next_due']!=''){ ?>
                                 <div class="form-group row name_fields accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(100); ?>">
                                    <label class="col-sm-4 col-form-label">Tax Payment Date to HMRC</label>
                                    <div class="col-sm-8"><?php echo date("F j, Y", strtotime("+1 days", strtotime($client[0]['crm_ch_accounts_next_due'])));?>
                                    </div>
                                 </div>
                                 <?php } ?>
                              </div>
                           </div>
                </div>
              </div>
              <!-- accordion-panel -->
              <div class="accordion-panel enable_tax" style="<?php echo $taxrecom;?>">
                <div class="box-division03">
                  <div class="accordion-heading" role="tab" id="headingOne">
                    <h3 class="card-title accordion-title">
                      <a class="accordion-msg">company Tax Reminders</a>
                    </h3>
                  </div>
                  <div id="collapse" class="panel-collapse">
                    <div class="basic-info-client1"  id="accounts_box4">
                      <div class="form-group row name_fields">
                       <!--  <label class="col-sm-4 col-form-label">Next Reminder Date</label> -->
                        <div class="col-sm-8">
                          <!-- <input type="hidden" name="company_next_reminder_date" id="company_next_reminder_date" placeholder="" value="<?php if(isset($client[0]['crm_company_next_reminder_date']) && ($client[0]['crm_company_next_reminder_date']!='') ){ echo $client[0]['crm_company_next_reminder_date'];}?>" class="fields datepicker"> -->
                          <a id="company_custom_reminder_link" name="company_custom_reminder_link" href="<?php echo base_url();?>User/reminder_settings/5" target="_blank">As per firm setting</a>
                          <input type="checkbox" class="js-small f-right fields" name="company_next_reminder_date" id="company_next_reminder_date"  checked="checked" onchange="check_checkbox2();" readonly>
                        </div>
                      </div>
                      <div class="form-group row radio_bts accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(102); ?>">
                        <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                        <div class="col-sm-8">
                          <input type="checkbox" class="js-small f-right fields" name="company_create_task_reminder" id="company_create_task_reminder" <?php if(isset($client[0]['crm_company_create_task_reminder']) && ($client[0]['crm_company_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> readonly>
                        </div>
                      </div>
                      <div class="form-group row radio_bts accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(103); ?>" style="">
                        <label id="company_custom_reminder_label" name="company_custom_reminder_label" class="col-sm-4 col-form-label">Add Custom Reminder</label>
                        <div class="col-sm-8">
                          <!-- <input type="checkbox" class="js-small f-right fields" name="accounts_custom_reminder" id="accounts_custom_reminder" <?php if(isset($client[0]['crm_accounts_custom_reminder']) && ($client[0]['crm_accounts_custom_reminder']=='on') ){?> checked="checked"<?php } ?>> -->
                          <!-- <textarea rows="4" name="company_custom_reminder" id="company_custom_reminder" class="form-control fields"><?php if(isset($client[0]['crm_company_custom_reminder']) && ($client[0]['crm_company_custom_reminder']!='') ){ echo $client[0]['crm_company_custom_reminder'];}?></textarea> -->
                          <a name="company_custom_reminder" id="company_custom_reminder" href="<?php echo base_url();?>User/reminder_settings/5" target="_blank">Click to add Custom Reminder</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- accordion-panel -->
            </div>
            <!-- companytax-->
            <div class="accordion-panel">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Notes if any other</a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1">
                  <?php if(isset($client[0]['crm_accounts_notes']) && $client[0]['crm_accounts_notes']!=''){ ?>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(104); ?></label>
                      <div class="col-sm-8">
                        <!-- <textarea rows="3" placeholder="" name="accounts_notes" id="accounts_notes" class="fields"><?php if(isset($client[0]['crm_accounts_notes']) && ($client[0]['crm_accounts_notes']!='') ){ echo $client[0]['crm_accounts_notes'];}?></textarea> -->
                        <p class="for-address"><?php if(isset($client[0]['crm_accounts_notes']) && ($client[0]['crm_accounts_notes']!='') ){ echo $client[0]['crm_accounts_notes'];}?></p>
                      </div>
                    </div>
                    <?php } ?>                   
                  </div>
                </div>
              </div>
            </div>
            <!-- accordion-panel -->
          </div>
          <!-- accounts -->
          <div id="personal-tax-returns" class="tab-pane fade">
      <div class="masonry-container floating_set">
<div class="grid-sizer"></div>
            <div class="accordion-panel">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Important Information            </a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1" id="personal_box">
                  <?php if(isset($client[0]['crm_personal_utr_number']) && ($client[0]['crm_personal_utr_number']!='') ){ ?>    
                    <div class="form-group row personal_sort" id="<?php echo $this->Common_mdl->get_order_details(29); ?>">
                      <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(29); ?></label>
                      <div class="col-sm-8">
                        <!-- <input type="number" name="personal_utr_number" id="personal_utr_number" class="form-control fields" value="<?php if(isset($client[0]['crm_personal_utr_number']) && ($client[0]['crm_personal_utr_number']!='') ){ echo $client[0]['crm_personal_utr_number'];}?>"> -->
                        <span class="small-info"><?php if(isset($client[0]['crm_personal_utr_number']) && ($client[0]['crm_personal_utr_number']!='') ){ echo $client[0]['crm_personal_utr_number'];}?></span>
                      </div>
                    </div>
                    <?php } ?>

                    <?php if(isset($client[0]['crm_ni_number']) && ($client[0]['crm_ni_number']!='') ){ ?>
                    <div class="form-group row personal_sort" id="<?php echo $this->Common_mdl->get_order_details(30); ?>">
                      <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(30); ?></label>
                      <div class="col-sm-8">
                        <!-- <input type="number" name="ni_number" id="ni_number" class="form-control fields" value="<?php if(isset($client[0]['crm_ni_number']) && ($client[0]['crm_ni_number']!='') ){ echo $client[0]['crm_ni_number'];}?>"> -->
                        <span class="small-info"><?php if(isset($client[0]['crm_ni_number']) && ($client[0]['crm_ni_number']!='') ){ echo $client[0]['crm_ni_number'];}?></span>
                      </div>
                    </div>
                    <?php } ?>
                    <?php if(isset($client[0]['crm_property_income']) && ($client[0]['crm_property_income']=='on') ){?>
                    <div class="form-group row radio_bts personal_sort" id="<?php echo $this->Common_mdl->get_order_details(31); ?>">
                      <label class="col-sm-4 col-form-label">Property Income</label>
                      <div class="col-sm-8">
                        <input type="checkbox" class="js-small f-right fields" name="property_income" id="property_income" <?php if(isset($client[0]['crm_property_income']) && ($client[0]['crm_property_income']=='on') ){?> checked="checked"<?php } ?> readonly>
                      </div>
                    </div>
                    <?php } ?>
                    <?php if(isset($client[0]['crm_additional_income']) && ($client[0]['crm_additional_income']=='on') ){?>                      
                    <div class="form-group row radio_bts personal_sort" id="<?php echo $this->Common_mdl->get_order_details(32); ?>">
                      <label class="col-sm-4 col-form-label">Additional Income</label>
                      <div class="col-sm-8">
                        <input type="checkbox" class="js-small f-right fields" name="additional_income" id="additional_income" <?php if(isset($client[0]['crm_additional_income']) && ($client[0]['crm_additional_income']=='on') ){?> checked="checked"<?php } ?> readonly>
                      </div>
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
            <!-- accordion-panel -->
            <div class="accordion-panel">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Personal Tax Return</a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1" id="personal_box1">
                  <?php if(isset($client[0]['crm_personal_tax_return_date']) && ($client[0]['crm_personal_tax_return_date']!='') ){ ?>
                    <div class="form-group row name_fields personal_sort" id="<?php echo $this->Common_mdl->get_order_details(33); ?>">
                      <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Tax Return Date</label>
                      <div class="col-sm-8">                      
                        <span class="small-info"><?php if(isset($client[0]['crm_personal_tax_return_date']) && ($client[0]['crm_personal_tax_return_date']!='') ){ echo $client[0]['crm_personal_tax_return_date'];}?></span>
                      </div>
                    </div>
                    <?php } ?>
                    <?php if(isset($client[0]['crm_personal_due_date_return']) && ($client[0]['crm_personal_due_date_return']!='') ){ ?>
                    <div class="form-group row name_fields personal_sort" id="<?php echo $this->Common_mdl->get_order_details(34); ?>">
                      <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(34); ?></label>
                      <div class="col-sm-8">                      
                        <span class="small-info"><?php if(isset($client[0]['crm_personal_due_date_return']) && ($client[0]['crm_personal_due_date_return']!='') ){ echo $client[0]['crm_personal_due_date_return'];}?></span>
                      </div>
                    </div>
                    <?php } ?>
                    <?php if(isset($client[0]['crm_personal_due_date_online']) && ($client[0]['crm_personal_due_date_online']!='') ){ ?>
                    <div class="form-group row name_fields personal_sort" id="<?php echo $this->Common_mdl->get_order_details(35); ?>">
                      <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(35); ?></label>
                      <div class="col-sm-8">                     
                       <span class="small-info"><?php if(isset($client[0]['crm_personal_due_date_online']) && ($client[0]['crm_personal_due_date_online']!='') ){ echo $client[0]['crm_personal_due_date_online'];}?></span>
                      </div>
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
            <!-- accordion-panel -->
            <div class="accordion-panel enable_pertax" style="<?php echo $three;?>">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Personal tax Reminders</a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1" id="personal_box2">
                    <div class="form-group row name_fields">                     
                      <div class="col-sm-8">                       
                        <a id="personal_custom_reminder_link" name="personal_custom_reminder_link" href="<?php echo base_url();?>User/reminder_settings/6" target="_blank">As per firm setting</a>
                        <input type="checkbox" class="js-small f-right fields" name="Personal_next_reminder_date" id="Personal_next_reminder_date"  checked="checked" onchange="check_checkbox3();" readonly>
                      </div>
                    </div>
                     <?php if(isset($client[0]['crm_personal_task_reminder']) && ($client[0]['crm_personal_task_reminder']=='on') ){?>
                    <div class="form-group row radio_bts personal_sort" id="<?php echo $this->Common_mdl->get_order_details(37); ?>">
                      <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                      <div class="col-sm-8">
                        <input type="checkbox" class="js-small f-right fields" name="personal_task_reminder" id="personal_task_reminder" <?php if(isset($client[0]['crm_personal_task_reminder']) && ($client[0]['crm_personal_task_reminder']=='on') ){?> checked="checked"<?php } ?> readonly>
                      </div>
                    </div>
                    <?php } ?>
                    <div class="form-group row radio_bts personal_sort" id="<?php echo $this->Common_mdl->get_order_details(38); ?>" style="">
                      <label id="personal_custom_reminder_label" name="personal_custom_reminder_label" class="col-sm-4 col-form-label">Add Custom Reminder</label>
                      <div class="col-sm-8">                      
                        <a name="personal_custom_reminder" id="personal_custom_reminder" href="<?php echo base_url();?>User/reminder_settings/6" target="_blank">Click to add Custom Reminder</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- accordion-panel -->
            <div class="accordion-panel">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Notes if any other</a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1">
                  <?php if(isset($client[0]['crm_personal_notes']) && ($client[0]['crm_personal_notes']!='') ){  ?>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(39); ?></label>
                      <div class="col-sm-8">                     
                       <p class="for-address"><?php if(isset($client[0]['crm_personal_notes']) && ($client[0]['crm_personal_notes']!='') ){ echo $client[0]['crm_personal_notes'];}?></p>
                      </div>
                    </div>
                  <?php } ?>  
                  </div>
                </div>
              </div>
            </div>
            <!-- accordion-panel -->
      </div>
          </div>
          <!-- Personal Tax Return -->
          <div id="payroll" class="tab-pane fade">
            <div class="accordion-panel">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Important Information            </a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1" id="payroll_box">
                  <?php if(isset($client[0]['crm_payroll_acco_off_ref_no']) && ($client[0]['crm_payroll_acco_off_ref_no']!='') ){ ?>
                    <div class="form-group row name_fields  payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(105); ?>">
                      <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(105); ?></label>
                      <div class="col-sm-8">                       
                        <span class="small-info"><?php if(isset($client[0]['crm_payroll_acco_off_ref_no']) && ($client[0]['crm_payroll_acco_off_ref_no']!='') ){ echo $client[0]['crm_payroll_acco_off_ref_no'];}?></span>
                      </div>
                    </div>
                    <?php } ?>
                    <?php if(isset($client[0]['crm_paye_off_ref_no']) && ($client[0]['crm_paye_off_ref_no']!='') ){ ?>
                    <div class="form-group row name_fields payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(106); ?>">
                      <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(106); ?></label>
                      <div class="col-sm-8">                    
                       <span class="small-info"><?php if(isset($client[0]['crm_paye_off_ref_no']) && ($client[0]['crm_paye_off_ref_no']!='') ){ echo $client[0]['crm_paye_off_ref_no'];}?></span>
                      </div>
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
            <!-- accordion-panel --> 
          <div class="accordion-panel">
           <div class="box-division03">
                        <div class="accordion-heading" role="tab" id="headingOne">
                           <h3 class="card-title accordion-title">
                              <a class="accordion-msg">Payroll</a>
                           </h3>
                        </div>
                        <div id="collapse" class="panel-collapse">
            
                           <div class="basic-info-client1" id="payroll_box1">

                             <?php if(isset($client[0]['crm_payroll_reg_date']) && ($client[0]['crm_payroll_reg_date']!='') ){ ?>
                              <div class="form-group row date_birth payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(107); ?>">
                                 <label class="col-sm-4 col-form-label">Payroll Registration Date</label>
                                 <div class="col-sm-8">
                                   <?php if(isset($client[0]['crm_payroll_reg_date']) && ($client[0]['crm_payroll_reg_date']!='') ){ echo $client[0]['crm_payroll_reg_date'];}?>
                                 </div>
                              </div>
                              <?php } ?>
                               <?php if(isset($client[0]['crm_no_of_employees']) && ($client[0]['crm_no_of_employees']!='') ){ ?>
                              <div class="form-group row payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(108); ?>">
                                 <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(108); ?></label>
                                 <div class="col-sm-8">
                                    <?php if(isset($client[0]['crm_no_of_employees']) && ($client[0]['crm_no_of_employees']!='') ){ echo $client[0]['crm_no_of_employees'];}?>                                 </div>
                              </div>
                              <?php } ?>
                               <?php if(isset($client[0]['crm_first_pay_date']) && ($client[0]['crm_first_pay_date']!='') ){ ?>
                              <div class="form-group row  date_birth payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(109); ?>">
                                 <label class="col-sm-4 col-form-label">First Pay Date</label>
                                 <div class="col-sm-8">
                                    <?php if(isset($client[0]['crm_first_pay_date']) && ($client[0]['crm_first_pay_date']!='') ){ echo $client[0]['crm_first_pay_date'];}?>
                                 </div>
                              </div>
                              <?php } ?>
                               <?php if(isset($client[0]['crm_payroll_run']) && $client[0]['crm_payroll_run']!=''){ ?>
                              <div class="form-group row payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(110); ?>">
                                 <label class="col-sm-4 col-form-label">Payroll Run</label>
                                 <div class="col-sm-8">
                                    <?php if(isset($client[0]['crm_payroll_run'])){ echo $client[0]['crm_payroll_run']; }?>
                                 </div>
                              </div>
                              <?php } ?>
                              <?php if(isset($client[0]['crm_payroll_run_date']) && ($client[0]['crm_payroll_run_date']!='') ){  ?>
                              <div class="form-group row  date_birth payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(111); ?>">
                                 <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Payroll Run Date</label>
                                 <div class="col-sm-8">
                                    <?php if(isset($client[0]['crm_payroll_run_date']) && ($client[0]['crm_payroll_run_date']!='') ){ echo $client[0]['crm_payroll_run_date'];}?>
                                 </div>
                              </div>
                              <?php } ?>
                               <?php if(isset($client[0]['crm_rti_deadline']) && ($client[0]['crm_rti_deadline']!='') ){ ?>
                              <div class="form-group row  date_birth payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(112); ?>">
                                 <label class="col-sm-4 col-form-label">RTI Due/Deadline Date</label>
                                 <div class="col-sm-8">
                                   <?php if(isset($client[0]['crm_rti_deadline']) && ($client[0]['crm_rti_deadline']!='') ){ echo $client[0]['crm_rti_deadline'];}?>
                                 </div>
                              </div>
                              <?php } ?>
                              <?php if(isset($client[0]['crm_previous_year_require']) && ($client[0]['crm_previous_year_require']=='on') ){?>
                              <div class="form-group row radio_bts payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(113); ?>">
                                 <label class="col-sm-4 col-form-label">Previous Year Requires</label>
                                 <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right fields" name="previous_year_require" id="previous_year_require" <?php if(isset($client[0]['crm_previous_year_require']) && ($client[0]['crm_previous_year_require']=='on') ){?> checked="checked"<?php } ?> data-id="preyear" readonly>
                                 </div>
                              </div>
                              <?php } ?>
                              <?php 
                                 (isset($client[0]['crm_previous_year_require']) && $client[0]['crm_previous_year_require'] == 'on') ? $preyear =  "block" : $preyear = 'none';
                                 ?>
                              <div id="enable_preyear" style="display:<?php echo $preyear;?>;">
                             <?php if(isset($client[0]['crm_payroll_if_yes']) && $client[0]['crm_payroll_if_yes']!=''){ ?>
                                 <div class="form-group row name_fields">
                                    <label class="col-sm-4 col-form-label">If Yes</label>
                                    <div class="col-sm-8">
                                       <!-- <input type="text" name="payroll_if_yes" id="payroll_if_yes" placeholder="How many years" value="<?php if(isset($client[0]['crm_payroll_if_yes']) && ($client[0]['crm_payroll_if_yes']!='') ){ echo $client[0]['crm_payroll_if_yes'];}?>" class="fields"> -->
                                       <?php  if(isset($client[0]['crm_payroll_if_yes'])){ echo $client[0]['crm_payroll_if_yes'];}?>
                                    </div>
                                 </div>
                                 <?php } ?>
                              </div>
                              <?php if(isset($client[0]['crm_paye_scheme_ceased']) && ($client[0]['crm_paye_scheme_ceased']!='') ){ ?>
                              <div class="form-group row  date_birth payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(114); ?>">
                                 <label class="col-sm-4 col-form-label">PAYE Scheme Ceased</label>
                                 <div class="col-sm-8">
                                   <?php if(isset($client[0]['crm_paye_scheme_ceased']) && ($client[0]['crm_paye_scheme_ceased']!='') ){ echo $client[0]['crm_paye_scheme_ceased'];}?>
                                 </div>
                              </div>
                              <?php } ?>
                           </div>
                        </div>
                     </div>
           </div>


            <!-- accordion-panel -->
            <div class="accordion-panel enable_pay" style="<?php echo $four;?>">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Payroll Reminders</a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1" id="payroll_box2">
                    <div class="form-group row  date_birth">
                     <!--  <label class="col-sm-4 col-form-label">Next Reminder Date</label> -->
                      <div class="col-sm-8">
                        <!-- <input type="hidden" name="payroll_next_reminder_date" id="payroll_next_reminder_date" placeholder="How many years" value="<?php if(isset($client[0]['crm_payroll_next_reminder_date']) && ($client[0]['crm_payroll_next_reminder_date']!='') ){ echo $client[0]['crm_payroll_next_reminder_date'];}?>" class="fields datepicker"> -->
                        <a id="payroll_add_custom_reminder_link" name="payroll_add_custom_reminder_link" href="<?php echo base_url();?>User/reminder_settings/7" target="_blank">As per firm setting</a>
                        <input type="checkbox" class="js-small f-right fields" name="Payroll_next_reminder_date" id="Payroll_next_reminder_date"  checked="checked" onchange="check_checkbox4();" readonly>
                      </div>
                    </div>
                    <div class="form-group row radio_bts payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(116); ?>">
                      <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                      <div class="col-sm-8">
                        <input type="checkbox" class="js-small f-right fields" name="payroll_create_task_reminder" id="payroll_create_task_reminder" <?php if(isset($client[0]['crm_payroll_create_task_reminder']) && ($client[0]['crm_payroll_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> readonly>
                      </div>
                    </div>
                    <div class="form-group row radio_bts payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(117); ?>" style="">
                      <label id="payroll_add_custom_reminder_label" name="payroll_add_custom_reminder_label" class="col-sm-4 col-form-label">Add Custom Reminder</label>
                      <div class="col-sm-8">
                        <!-- <input type="checkbox" class="js-small f-right fields" name="payroll_add_custom_reminder" id="payroll_add_custom_reminder" <?php if(isset($client[0]['crm_payroll_add_custom_reminder']) && ($client[0]['crm_payroll_add_custom_reminder']=='on') ){?> checked="checked"<?php } ?>> -->
                        <!--   <textarea rows="4" name="payroll_add_custom_reminder" id="payroll_add_custom_reminder" class="form-control fields"><?php if(isset($client[0]['crm_payroll_add_custom_reminder']) && ($client[0]['crm_payroll_add_custom_reminder']!='') ){ echo $client[0]['crm_payroll_add_custom_reminder'];}?></textarea> -->
                        <a name="payroll_add_custom_reminder" id="payroll_add_custom_reminder" href="<?php echo base_url();?>User/reminder_settings/7" target="_blank">Click to add Custom Reminder</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- accordion-panel -->
             <div class='worktab' style="<?php echo $worktab;?>">
                        <div class="accordion-panel">
            <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">WorkPlace Pension - AE        </a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1" id="workplace_sec">
                               <?php if(isset($client[0]['crm_staging_date']) && ($client[0]['crm_staging_date']!='') ){ ?>
                                 <div class="form-group row  date_birth workplace" id="<?php echo $this->Common_mdl->get_order_details(118); ?>">
                                    <label class="col-sm-4 col-form-label">Staging Date</label>
                                    <div class="col-sm-8">
                                      <?php if(isset($client[0]['crm_staging_date']) && ($client[0]['crm_staging_date']!='') ){ echo $client[0]['crm_staging_date'];}?>
                                    </div>
                                 </div>
                                 <?php } ?>
                                 <?php if(isset($client[0]['crm_pension_id']) && ($client[0]['crm_pension_id']!='') ){  ?>
                                 <div class="form-group row workplace" id="<?php echo $this->Common_mdl->get_order_details(119); ?>">
                                    <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(119); ?></label>
                                    <div class="col-sm-8">
                                       <?php if(isset($client[0]['crm_pension_id']) && ($client[0]['crm_pension_id']!='') ){ echo $client[0]['crm_pension_id'];}?>
                                    </div>
                                 </div>
                                 <?php } ?>
                                  <?php if(isset($client[0]['crm_pension_subm_due_date']) && ($client[0]['crm_pension_subm_due_date']!='') ){ ?>
                                 <div class="form-group row  date_birth workplace" id="<?php echo $this->Common_mdl->get_order_details(120); ?>">
                                    <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Pension Submission Due date</label>
                                    <div class="col-sm-8">
                                       <?php if(isset($client[0]['crm_pension_subm_due_date']) && ($client[0]['crm_pension_subm_due_date']!='') ){ echo $client[0]['crm_pension_subm_due_date'];}?>
                                    </div>
                                 </div>
                                 <?php } ?>
                                  <?php if(isset($client[0]['crm_postponement_date']) && ($client[0]['crm_postponement_date']!='') ){ ?>
                                 <div class="form-group row  date_birth workplace" id="<?php echo $this->Common_mdl->get_order_details(121); ?>">
                                    <label class="col-sm-4 col-form-label">Defer/Postpone Upto</label>
                                    <div class="col-sm-8">
                                       <?php if(isset($client[0]['crm_postponement_date']) && ($client[0]['crm_postponement_date']!='') ){ echo $client[0]['crm_postponement_date'];}?>
                                    </div>
                                 </div>
                                 <?php } ?>
                                   <?php if(isset($client[0]['crm_the_pensions_regulator_opt_out_date']) && ($client[0]['crm_the_pensions_regulator_opt_out_date']!='') ){ ?>
                                 <div class="form-group row  date_birth">
                                    <label class="col-sm-4 col-form-label">The Pensions Regulator Opt Out Date</label>
                                    <div class="col-sm-8">
                                       <?php if(isset($client[0]['crm_the_pensions_regulator_opt_out_date']) && ($client[0]['crm_the_pensions_regulator_opt_out_date']!='') ){ echo $client[0]['crm_the_pensions_regulator_opt_out_date'];}?>
                                    </div>
                                 </div>
                                 <?php } ?>
                                 <?php if(isset($client[0]['crm_re_enrolment_date']) && ($client[0]['crm_re_enrolment_date']!='') ){ ?>
                                 <div class="form-group row  date_birth workplace" id="<?php echo $this->Common_mdl->get_order_details(122); ?>">
                                    <label class="col-sm-4 col-form-label">Re-Enrolment Date</label>
                                    <div class="col-sm-8">
                                      <?php if(isset($client[0]['crm_re_enrolment_date']) && ($client[0]['crm_re_enrolment_date']!='') ){ echo $client[0]['crm_re_enrolment_date'];}?>
                                    </div>
                                 </div>
                                 <?php } ?>
                                  <?php if(isset($client[0]['crm_declaration_of_compliance_due_date']) && ($client[0]['crm_declaration_of_compliance_due_date']!='') ){ ?>
                                 <div class="form-group row  date_birth  workplace" id="<?php echo $this->Common_mdl->get_order_details(124); ?>">
                                    <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Declaration of Compliance Due</label>
                                    <div class="col-sm-8">
                                      <?php if(isset($client[0]['crm_declaration_of_compliance_due_date']) && ($client[0]['crm_declaration_of_compliance_due_date']!='') ){ echo $client[0]['crm_declaration_of_compliance_due_date'];}?>
                                    </div>
                                 </div>
                                 <?php } ?>
                                  <?php if(isset($client[0]['crm_declaration_of_compliance_submission']) && ($client[0]['crm_declaration_of_compliance_submission']!='') ){ ?>
                                 <div class="form-group row  date_birth">
                                    <label class="col-sm-4 col-form-label">Declaration of Compliance last filed</label>
                                    <div class="col-sm-8">
                                        <?php if(isset($client[0]['crm_declaration_of_compliance_submission']) && ($client[0]['crm_declaration_of_compliance_submission']!='') ){ echo $client[0]['crm_declaration_of_compliance_submission'];}?>                                    </div>
                                 </div>
                                 <?php } ?>
                                 <?php if(isset($client[0]['crm_paye_pension_provider']) && ($client[0]['crm_paye_pension_provider']!='') ){ ?>

                                 <div class="form-group row workplace" id="<?php echo $this->Common_mdl->get_order_details(125); ?>">
                                    <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(125); ?></label>
                                    <div class="col-sm-8">
                                      <?php if(isset($client[0]['crm_paye_pension_provider']) && ($client[0]['crm_paye_pension_provider']!='') ){ echo $client[0]['crm_paye_pension_provider'];}?>
                                    </div>
                                 </div>
                                 <?php } ?>
                                  <?php if(isset($client[0]['crm_pension_id']) && ($client[0]['crm_pension_id']!='') ){ ?>
                                 <div class="form-group row workplace" id="<?php echo $this->Common_mdl->get_order_details(126); ?>">
                                    <label class="col-sm-4 col-form-label">Pension Provider User ID</label>
                                    <div class="col-sm-8">
                                    <?php if(isset($client[0]['crm_pension_id']) && ($client[0]['crm_pension_id']!='') ){ echo $client[0]['crm_pension_id'];}?>
                                    </div>
                                 </div>
                                 <?php } ?>
                                   <?php if(isset($client[0]['crm_paye_pension_provider_password']) && ($client[0]['crm_paye_pension_provider_password']!='') ){ ?>
                                 <div class="form-group row workplace" id="<?php echo $this->Common_mdl->get_order_details(126); ?>">
                                    <label class="col-sm-4 col-form-label">Pension Provider Password</label>
                                    <div class="col-sm-8">
                                       <?php if(isset($client[0]['crm_paye_pension_provider_password']) && ($client[0]['crm_paye_pension_provider_password']!='') ){ echo $client[0]['crm_paye_pension_provider_password'];}?>
                                    </div>
                                 </div>
                                 <?php } ?>
                                   <?php if(isset($client[0]['crm_employer_contri_percentage']) && ($client[0]['crm_employer_contri_percentage']!='') ){ ?>
                                 <div class="form-group row workplace" id="<?php echo $this->Common_mdl->get_order_details(127); ?>">
                                    <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(127); ?></label>
                                    <div class="col-sm-8">
                                      <?php if(isset($client[0]['crm_employer_contri_percentage']) && ($client[0]['crm_employer_contri_percentage']!='') ){ echo $client[0]['crm_employer_contri_percentage'];}?>
                                    </div>
                                 </div>
                                 <?php } ?>

                                 <?php if(isset($client[0]['crm_employee_contri_percentage']) && ($client[0]['crm_employee_contri_percentage']!='') ){ ?>
                                 <div class="form-group row workplace" id="<?php echo $this->Common_mdl->get_order_details(128); ?>">
                                    <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(128); ?></label>
                                    <div class="col-sm-8">
                                      <?php if(isset($client[0]['crm_employee_contri_percentage']) && ($client[0]['crm_employee_contri_percentage']!='') ){ echo $client[0]['crm_employee_contri_percentage'];}?>
                                    </div>
                                 </div>
                                 <?php } ?>
                                  <?php if(isset($client[0]['crm_pension_notes']) && ($client[0]['crm_pension_notes']!='') ){ ?>
                                 <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Other Notes</label>
                                    <div class="col-sm-8">
                                      <?php if(isset($client[0]['crm_pension_notes']) && ($client[0]['crm_pension_notes']!='') ){ echo $client[0]['crm_pension_notes'];}?>
                                    </div>
                                 </div>
                                 <?php } ?>
                              </div>
                           </div>
               </div>
                        </div>
                        <!-- accordion-panel -->
                        <div class="accordion-panel enable_work" style="<?php echo $five;?>">
            <div class="box-division03">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">Pension Reminders</a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1" id="workplace_sec1">
                                 <div class="form-group row  date_birth">
                                    <label class="col-sm-4 col-form-label"><a id="pension_create_task_reminder_link" name="pension_create_task_reminder_link" href="<?php echo base_url();?>User/reminder_settings/8" target="_blank">As per firm setting</a></label>
                                    <div class="col-sm-8">
                                       <!-- <input type="hidden" name="pension_next_reminder_date" id="pension_next_reminder_date" placeholder="How many years" value="<?php if(isset($client[0]['crm_pension_next_reminder_date']) && ($client[0]['crm_pension_next_reminder_date']!='') ){ echo $client[0]['crm_pension_next_reminder_date'];}?>" class="fields datepicker"> -->
                                       
                                        <input type="checkbox" class="js-small f-right fields" name="Pension_next_reminder_date" id="Pension_next_reminder_date"  onchange="check_checkbox5();" <?php if(isset($client[0]['crm_pension_next_reminder_date']) && ($client[0]['crm_pension_next_reminder_date']=='on') ){?> checked="checked" <?php } ?> readonly>
                                    </div>
                                 </div>
                                 <div class="form-group row radio_bts workplace" id="<?php echo $this->Common_mdl->get_order_details(130); ?>">
                                    <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" class="js-small f-right fields" name="pension_create_task_reminder" id="pension_create_task_reminder" <?php if(isset($client[0]['crm_pension_create_task_reminder']) && ($client[0]['crm_pension_create_task_reminder']=='on') ){?> checked="checked" <?php } ?> readonly>
                                    </div>
                                 </div>
                                 <div class="form-group row radio_bts workplace" id="<?php echo $this->Common_mdl->get_order_details(131); ?>" style="">
                                    <label id="pension_create_task_reminder_label" name="pension_create_task_reminder_label" class="col-sm-4 col-form-label">Add Custom Reminder</label>
                                    <div class="col-sm-8" id="pension_add_custom_reminder">
                                      <?php if(isset($client[0]['crm_pension_add_custom_reminder']) && ($client[0]['crm_pension_add_custom_reminder']!='') ){ echo $client[0]['crm_pension_add_custom_reminder'];}?>
                                       <!-- <textarea rows="4" name="pension_add_custom_reminder" id="pension_add_custom_reminder" class="form-control fields"><?php if(isset($client[0]['crm_pension_add_custom_reminder']) && ($client[0]['crm_pension_add_custom_reminder']!='') ){ echo $client[0]['crm_pension_add_custom_reminder'];}?></textarea> -->
                                       <!-- <a name="pension_add_custom_reminder" id="pension_add_custom_reminder" href="<?php echo base_url();?>User/reminder_settings/8" target="_blank">Click to add Custom Reminder</a> -->
                                    </div>
                                 </div>
                                  <?php $cus_temp = $this->db->query('select * from reminder_setting where user_id='.$client[0]['user_id'].' and service_id=8')->result_array();
                            if(isset($cus_temp)){
                              foreach ($cus_temp as $key => $value) {
                              
                             ?>
                              <div class="form-group row name_fields" style="">
                              <label class="col-sm-4 col-form-label"><?php echo $value['subject']; ?></label>
                              <div class="col-sm-8">
                              <a href="javascript:;" class="date-editop" data-toggle="modal" data-target="#edit-reminder<?php echo $value['id']; ?>">edit</a>
                              </div>
                              </div>

                              <?php } } ?>
                              </div>
                           </div>
                        </div>
            </div>
                        <!-- accordion-panel -->
                     </div>
            <!--worktab close-->         
            <div class="accordion-panel inpn"  style="display: none">
              <div class="accordion-heading" role="tab" id="headingOne">
                <h3 class="card-title accordion-title">
                  <a class="accordion-msg">Pension Invoice</a>
                </h3>
              </div>
              <div id="collapse" class="panel-collapse">
                <div class="basic-info-client1">
                  <div class="form-group row name_fields">
                    <label class="col-sm-4 col-form-label">Invoice</label>
                    <div class="col-sm-8">                     
                      <span class="small-info">E-mail</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="accordion-panel indl"  style="display: none">
              <div class="accordion-heading" role="tab" id="headingOne">
                <h3 class="card-title accordion-title">
                  <a class="accordion-msg">Declration Invoice</a>
                </h3>
              </div>
              <div id="collapse" class="panel-collapse">
                <div class="basic-info-client1">
                  <div class="form-group row name_fields">
                    <label class="col-sm-4 col-form-label">Invoice</label>
                    <div class="col-sm-8">
                      <!-- <select name="invoice_dl" id="invoice_dl">
                        <option value="">Select</option>
                        <option value="E-Mail">E-Mail</option>
                        <option value="SMS">SMS</option>
                        <option value="Phone">Phone</option>
                      </select> -->
                      <span class="small-info">E-mail</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="contratab" style="<?php echo $contratab;?>">
              <div class="accordion-panel">
                <div class="box-division03">
                  <div class="accordion-heading" role="tab" id="headingOne">
                    <h3 class="card-title accordion-title">
                      <a class="accordion-msg">  CIS Contractor/Sub Contractor         </a>
                    </h3>
                  </div>
                    <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1">
                                 <div class="form-group row name_fields">
                                    <label class="col-sm-4 col-form-label">C.I.S Contractor</label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" class="js-small f-right fields" name="cis_contractor" id="cis_contractor" <?php if(isset($client[0]['crm_cis_contractor']) && ($client[0]['crm_cis_contractor']=='on') ){?> checked="checked"<?php } ?> readonly>
                                    </div>
                                 </div>
                                  <?php if(isset($client[0]['crm_cis_contractor_start_date']) && ($client[0]['crm_cis_contractor_start_date']!='') ){  ?>
                                 <div class="form-group row  date_birth">
                                    <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Start Date</label>
                                    <div class="col-sm-8">
                                       <?php if(isset($client[0]['crm_cis_contractor_start_date']) && ($client[0]['crm_cis_contractor_start_date']!='') ){ echo $client[0]['crm_cis_contractor_start_date'];}?>
                                    </div>
                                 </div>
                                 <?php } ?>
                                    <?php if(isset($client[0]['crm_cis_scheme_notes']) && ($client[0]['crm_cis_scheme_notes']!='') ){ ?>
                                 <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">C.I.S Scheme</label>
                                    <div class="col-sm-8">
                                       <?php if(isset($client[0]['crm_cis_scheme_notes']) && ($client[0]['crm_cis_scheme_notes']!='') ){ echo $client[0]['crm_cis_scheme_notes'];}?>
                                    </div>
                                 </div>
                                 <?php } ?>
                                 <?php if(isset($client[0]['crm_cis_subcontractor']) && ($client[0]['crm_cis_subcontractor']=='on') ){?>
                                 <div class="form-group row name_fields">
                                    <label class="col-sm-4 col-form-label">CIS Subcontractor</label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" name="cis_subcontractor" id="cis_subcontractor" class="js-small f-right fields" <?php if(isset($client[0]['crm_cis_subcontractor']) && ($client[0]['crm_cis_subcontractor']=='on') ){?> checked="checked"<?php } ?> readonly>
                                    </div>
                                 </div>
                                 <?php } ?>
                                 <?php if(isset($client[0]['crm_cis_subcontractor_start_date']) && ($client[0]['crm_cis_subcontractor_start_date']!='') ){ ?>
                                 <div class="form-group row  date_birth">
                                    <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Start Date</label>
                                    <div class="col-sm-8">
                                      <?php if(isset($client[0]['crm_cis_subcontractor_start_date']) && ($client[0]['crm_cis_subcontractor_start_date']!='') ){ echo $client[0]['crm_cis_subcontractor_start_date'];}?>
                                    </div>
                                 </div>
                                 <?php } ?>
                                 <?php if(isset($client[0]['crm_cis_subcontractor_scheme_notes']) && ($client[0]['crm_cis_subcontractor_scheme_notes']!='') ){ ?>
                                 <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">C.I.S Scheme</label>
                                    <div class="col-sm-8">
                                     <?php if(isset($client[0]['crm_cis_subcontractor_scheme_notes']) && ($client[0]['crm_cis_subcontractor_scheme_notes']!='') ){ echo $client[0]['crm_cis_subcontractor_scheme_notes'];}?>
                                    </div>
                                 </div>
                                 <?php } ?>
                              </div>
                           </div>
                </div>
              </div>
              <!-- accordion-panel -->
              <div class="accordion-panel enable_cis"  style="<?php echo $seven;?>">
                <div class="box-division03">
                  <div class="accordion-heading" role="tab" id="headingOne">
                    <h3 class="card-title accordion-title">
                      <a class="accordion-msg">CIS Reminders</a>
                    </h3>
                  </div>
                  <div id="collapse" class="panel-collapse">
                    <div class="basic-info-client1">
                      <div class="form-group row  date_birth">
                       <!--  <label class="col-sm-4 col-form-label">Next Reminder Date</label> -->
                        <div class="col-sm-8">
                          <!--  <input type="hidden" name="cis_next_reminder_date" id="cis_next_reminder_date" placeholder="How many years" value="<?php if(isset($client[0]['crm_cis_next_reminder_date']) && ($client[0]['crm_cis_next_reminder_date']!='') ){ echo $client[0]['crm_cis_next_reminder_date'];}?>" class="fields datepicker"> -->
                          <a id="cis_add_custom_reminder_link" name="cis_add_custom_reminder_link" href="<?php echo base_url();?>User/reminder_settings/10" target="_blank">As per firm setting</a>
                          <input type="checkbox" class="js-small f-right fields" name="Cis_next_reminder_date" id="Cis_next_reminder_date"  checked="checked" onchange="check_checkbox6();" readonly>
                          <!--ggggg-->
                        </div>
                      </div>
                      <div class="form-group row radio_bts eightt" style="<?php echo $eight;?>">
                        <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                        <div class="col-sm-8">
                          <input type="checkbox" class="js-small f-right fields" name="cis_create_task_reminder" id="cis_create_task_reminder" <?php if(isset($client[0]['crm_cis_create_task_reminder']) && ($client[0]['crm_cis_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> readonlyc>
                        </div>
                      </div>
                      <div class="form-group row radio_bts">
                        <label id="cis_add_custom_reminder_label" name="cis_add_custom_reminder_label" class="col-sm-4 col-form-label">Add Custom Reminder</label>
                        <div class="col-sm-8">
                          <!-- <input type="checkbox" class="js-small f-right fields" name="cis_add_custom_reminder" id="cis_add_custom_reminder" <?php if(isset($client[0]['crm_cis_add_custom_reminder']) && ($client[0]['crm_cis_add_custom_reminder']=='on') ){?> checked="checked"<?php } ?>> -->
                          <!-- <textarea rows="4" name="cis_add_custom_reminder" id="cis_add_custom_reminder" class="form-control fields"><?php if(isset($client[0]['crm_cis_add_custom_reminder']) && ($client[0]['crm_cis_add_custom_reminder']!='') ){ echo $client[0]['crm_cis_add_custom_reminder'];}?></textarea> -->
                          <a name="cis_add_custom_reminder" id="cis_add_custom_reminder" href="<?php echo base_url();?>User/reminder_settings/10" target="_blank">Click to add Custom Reminder</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- accordion-panel -->
            </div>
            <!-- contratab close-->
            <div class="accordion-panel enable_pay" style="<?php echo $four;?>">
      <div class="box-division03">
              <div class="accordion-heading" role="tab" id="headingOne">
                <h3 class="card-title accordion-title">
                  <a class="accordion-msg">CIS SUB Reminders</a>
                </h3>
              </div>
              <div id="collapse" class="panel-collapse">
                <div class="basic-info-client1">
                  <div class="form-group row  date_birth">
                  <!--   <label class="col-sm-4 col-form-label">Next Reminder Date</label> -->
                    <div class="col-sm-8">
                      <!--  <input type="text" name="payroll_next_reminder_date" id="payroll_next_reminder_date" placeholder="How many years" value="<?php //if(isset($client[0]['crm_payroll_next_reminder_date']) && ($client[0]['crm_payroll_next_reminder_date']!='') ){ echo $client[0]['crm_payroll_next_reminder_date'];}?>" class="fields datepicker">-->
                      <a id="cissub_add_custom_reminder_link" name="cissub_add_custom_reminder_link" target="_blank" href="<?php echo base_url(); ?>User/reminder_settings/11">CIS-SUB next reminder date</a>
                      <input type="checkbox" class="js-small f-right fields" name="cissub_next_reminder_date" id="cissub_next_reminder_date"  checked="checked" onchange="check_checkbox12();" readonly>
                      <!--mmmmm-->
                    </div>
                  </div>
                  <div class="form-group row radio_bts ">
                    <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                    <div class="col-sm-8">
                      <input type="checkbox" class="js-small f-right fields" name="cissub_create_task_reminder" id="cissub_create_task_reminder" <?php if(isset($client[0]['crm_cissub_create_task_reminder']) && ($client[0]['crm_cissub_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> readonly>
                    </div>
                  </div>
                  <div class="form-group row radio_bts" style="">
                    <label id="cissub_add_custom_reminder_label" name="cissub_add_custom_reminder_label" class="col-sm-4 col-form-label">Add Custom Reminder</label>
                    <div class="col-sm-8">
                      <!-- <input type="checkbox" class="js-small f-right fields" name="payroll_add_custom_reminder" id="payroll_add_custom_reminder" <?php if(isset($client[0]['crm_cissub_add_custom_reminder']) && ($client[0]['crm_cissub_add_custom_reminder']=='on') ){?> checked="checked"<?php } ?>> -->
                      <!--  <textarea rows="4" name="cissub_add_custom_reminder" id="cissub_add_custom_reminder" class="form-control fields"><?php if(isset($client[0]['crm_cissub_add_custom_reminder']) && ($client[0]['crm_cissub_add_custom_reminder']!='') ){ echo $client[0]['crm_cissub_add_custom_reminder'];}?></textarea> -->
                      <a name="cissub_add_custom_reminder" id="cissub_add_custom_reminder" href="<?php echo base_url();?>User/reminder_settings/11" target="_blank">Click to add Custom Reminder</a>
                    </div>
                  </div>
                </div>
              </div>
        </div>
            </div>
            <div class="accordion-panel incis"  style="display: none">
      <div class="box-division03">
              <div class="accordion-heading" role="tab" id="headingOne">
                <h3 class="card-title accordion-title">
                  <a class="accordion-msg">CIS Invoice</a>
                </h3>
              </div>
              <div id="collapse" class="panel-collapse">
                <div class="basic-info-client1">
                  <div class="form-group row name_fields">
                    <label class="col-sm-4 col-form-label">Invoice</label>
                    <div class="col-sm-8">
                     <!--  <select name="invoice_cis" id="invoice_cis">
                        <option value="">Select</option>
                        <option value="E-Mail">E-Mail</option>
                        <option value="SMS">SMS</option>
                        <option value="Phone">Phone</option>
                      </select> -->
                      <span class="small-info">E-mail</span>
                    </div>
                  </div>
                </div>
              </div>
        </div>
            </div>
            <div class="p11dtab" style="<?php echo $p11dtab?>">
              <div class="accordion-panel">
                <div class="box-division03">
                  <div class="accordion-heading" role="tab" id="headingOne">
                    <h3 class="card-title accordion-title">
                      <a class="accordion-msg">P11D</a>
                    </h3>
                  </div>
                   <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1" id="p11d_sec">
                              <?php if(isset($client[0]['crm_p11d_latest_action_date']) && ($client[0]['crm_p11d_latest_action_date']!='') ){ ?>
                                 <div class="form-group row  date_birth p11d" id="<?php echo $this->Common_mdl->get_order_details(132); ?>">
                                    <label class="col-sm-4 col-form-label">P11D Start Date</label>
                                    <div class="col-sm-8">
                                       <?php if(isset($client[0]['crm_p11d_latest_action_date']) && ($client[0]['crm_p11d_latest_action_date']!='') ){ echo $client[0]['crm_p11d_latest_action_date'];}?>
                                    </div>
                                 </div>
                                 <?php } ?>
                                 <?php if(isset($client[0]['crm_p11d_latest_action']) && ($client[0]['crm_p11d_latest_action']!='') ){  ?>
                                 <div class="form-group row name_fields  p11d" id="<?php echo $this->Common_mdl->get_order_details(133); ?>">
                                    <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(133); ?></label>
                                    <div class="col-sm-8">
                                      <?php if(isset($client[0]['crm_p11d_latest_action']) && ($client[0]['crm_p11d_latest_action']!='') ){ echo $client[0]['crm_p11d_latest_action'];}?>                                    </div>
                                 </div>
                                 <?php } ?>
                                 <?php if(isset($client[0]['crm_latest_p11d_submitted']) && ($client[0]['crm_latest_p11d_submitted']!='') ){ ?>
                                 <div class="form-group row date_birth p11d" id="<?php echo $this->Common_mdl->get_order_details(134); ?>">
                                    <label class="col-sm-4 col-form-label">First Benefit Pay Date</label>
                                    <div class="col-sm-8">
                                       <?php if(isset($client[0]['crm_latest_p11d_submitted']) && ($client[0]['crm_latest_p11d_submitted']!='') ){ echo $client[0]['crm_latest_p11d_submitted'];}?>
                                    </div>
                                 </div>
                                 <?php } ?>
                                  <?php if(isset($client[0]['crm_next_p11d_return_due']) && ($client[0]['crm_next_p11d_return_due']!='') ){  ?>
                                 <div class="form-group row date_birth p11d" id="<?php echo $this->Common_mdl->get_order_details(135); ?>">
                                    <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>P11D Due date</label>
                                    <div class="col-sm-8">
                                      <?php if(isset($client[0]['crm_next_p11d_return_due']) && ($client[0]['crm_next_p11d_return_due']!='') ){ echo $client[0]['crm_next_p11d_return_due'];}?>
                                    </div>
                                 </div>
                                 <?php } ?>
                                 <?php if(isset($client[0]['crm_p11d_previous_year_require']) && ($client[0]['crm_p11d_previous_year_require']=='on') ){?>
                                 <div class="form-group row radio_bts p11d" id="<?php echo $this->Common_mdl->get_order_details(136); ?>">
                                    <label class="col-sm-4 col-form-label">Previous Year Requires</label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" class="js-small f-right fields" name="p11d_previous_year_require" id="p11d_previous_year_require" <?php if(isset($client[0]['crm_p11d_previous_year_require']) && ($client[0]['crm_p11d_previous_year_require']=='on') ){?> checked="checked"<?php } ?> readonly>
                                    </div>
                                 </div>
                                 <?php } ?>
                                  <?php if(isset($client[0]['crm_p11d_payroll_if_yes']) && ($client[0]['crm_p11d_payroll_if_yes']!='') ){ ?>
                                 <div class="form-group row name_fields p11d" id="<?php echo $this->Common_mdl->get_order_details(137); ?>">
                                    <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(137); ?></label>
                                    <div class="col-sm-8">
                                       <?php if(isset($client[0]['crm_p11d_payroll_if_yes']) && ($client[0]['crm_p11d_payroll_if_yes']!='') ){ echo $client[0]['crm_p11d_payroll_if_yes'];}?>
                                    </div>
                                 </div>
                                 <?php } ?>
                                 <?php if(isset($client[0]['crm_p11d_records_received']) && ($client[0]['crm_p11d_records_received']!='') ){ ?>
                                 <div class="form-group row  date_birth p11d" id="<?php echo $this->Common_mdl->get_order_details(138); ?>">
                                    <label class="col-sm-4 col-form-label">PAYE Scheme Ceased Date</label>
                                    <div class="col-sm-8">
                                       <?php if(isset($client[0]['crm_p11d_records_received']) && ($client[0]['crm_p11d_records_received']!='') ){ echo $client[0]['crm_p11d_records_received'];}?>
                                    </div>
                                 </div>
                                 <?php } ?>
                              </div>
                           </div>
                </div>
              </div>
              <!-- accordion-panel -->
              <div class="accordion-panel enable_plld" style="<?php echo $nine;?>">
                <div class="box-division03">
                  <div class="accordion-heading" role="tab" id="headingOne">
                    <h3 class="card-title accordion-title">
                      <a class="accordion-msg">P11D Reminders</a>
                    </h3>
                  </div>
                  <div id="collapse" class="panel-collapse">
                    <div class="basic-info-client1" id="p11d_sec1">
                      <div class="form-group row  date_birth">
                        <!-- <label class="col-sm-4 col-form-label">Next Reminder Date</label> -->
                        <div class="col-sm-8">
                          <!-- <input type="hidden" name="p11d_next_reminder_date" id="p11d_next_reminder_date" placeholder="" value="<?php if(isset($client[0]['crm_p11d_records_received']) && ($client[0]['crm_p11d_records_received']!='') ){ echo $client[0]['crm_p11d_records_received'];}?>" class="fields datepicker"> -->
                          <a id="p11d_add_custom_reminder_link" name="p11d_add_custom_reminder_link" href="<?php echo base_url();?>User/reminder_settings/12" target="_blank">As per firm setting setting</a>
                          <input type="checkbox" class="js-small f-right fields" name="P11d_next_reminder_date" id="P11d_next_reminder_date"  checked="checked" onchange="check_checkbox7();" readonly>
                          <!--hhhhh-->
                        </div>
                      </div>
                      <div class="form-group row radio_bts ">
                        <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                        <div class="col-sm-8">
                          <input type="checkbox" class="js-small f-right fields" name="p11d_create_task_reminder" id="p11d_create_task_reminder" <?php if(isset($client[0]['crm_p11d_create_task_reminder']) && ($client[0]['crm_p11d_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> readonly> 
                        </div>
                      </div>
                      <div class="form-group row radio_bts" style="">
                        <label id="p11d_add_custom_reminder_label" name="p11d_add_custom_reminder_label" class="col-sm-4 col-form-label">Add Custom Reminder</label>
                        <div class="col-sm-8">
                          <!-- <input type="checkbox" class="js-small f-right fields" name="p11d_add_custom_reminder" id="p11d_add_custom_reminder" <?php if(isset($client[0]['crm_p11d_add_custom_reminder']) && ($client[0]['crm_p11d_add_custom_reminder']=='on') ){?> checked="checked"<?php } ?>> -->
                          <!-- <textarea rows="4" name="p11d_add_custom_reminder" id="p11d_add_custom_reminder" class="form-control fields"><?php if(isset($client[0]['crm_p11d_add_custom_reminder']) && ($client[0]['crm_p11d_add_custom_reminder']!='') ){ echo $client[0]['crm_p11d_add_custom_reminder'];}?></textarea> -->
                          <a name="p11d_add_custom_reminder" id="p11d_add_custom_reminder" href="<?php echo base_url();?>User/reminder_settings/12" target="_blank">Click to add Custom Reminder</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- accordion-panel -->
            </div>
            <!-- p11dtab close -->
            <div class="accordion-panel incis_sub" style="display: none">
              <div class="accordion-heading" role="tab" id="headingOne">
                <h3 class="card-title accordion-title">
                  <a class="accordion-msg">CIS_SUB Invoice</a>
                </h3>
              </div>
              <div id="collapse" class="panel-collapse">
                <div class="basic-info-client1">
                  <div class="form-group row name_fields">
                    <label class="col-sm-4 col-form-label">Invoice</label>
                    <div class="col-sm-8">
                      <!-- <select name="invoice_cissub" id="invoice_cissub">
                        <option value="">Select</option>
                        <option value="E-Mail">E-Mail</option>
                        <option value="SMS">SMS</option>
                        <option value="Phone">Phone</option>
                      </select> -->
                      <span class="small-info">E-mail</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="accordion-panel inp11d"  style="display: none">
              <div class="accordion-heading" role="tab" id="headingOne">
                <h3 class="card-title accordion-title">
                  <a class="accordion-msg">P11d Invoice</a>
                </h3>
              </div>
              <div id="collapse" class="panel-collapse">
                <div class="basic-info-client1">
                  <div class="form-group row name_fields">
                    <label class="col-sm-4 col-form-label">Invoice</label>
                    <div class="col-sm-8">
                      <!-- <select name="invoice_p11d" id="invoice_p11d">
                        <option value="">Select</option>
                        <option value="E-Mail">E-Mail</option>
                        <option value="SMS">SMS</option>
                        <option value="Phone">Phone</option>
                      </select> -->
                      <span class="small-info">E-mail</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="accordion-panel">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Notes if any other</a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1">
                  <?php if(isset($client[0]['crm_p11d_notes']) && ($client[0]['crm_p11d_notes']!='') ){ ?>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(139); ?></label>
                      <div class="col-sm-8">
                        <!-- <textarea rows="3" placeholder="" name="p11d_notes" id="p11d_notes" class="fields"><?php if(isset($client[0]['crm_p11d_notes']) && ($client[0]['crm_p11d_notes']!='') ){ echo $client[0]['crm_p11d_notes'];}?></textarea> -->
                        <p class="for-address"><?php if(isset($client[0]['crm_p11d_notes']) && ($client[0]['crm_p11d_notes']!='') ){ echo $client[0]['crm_p11d_notes'];}?></p>
                      </div>
                    </div>
                    <?php } ?>
                  
                  </div>
                </div>
              </div>
            </div>
            <!-- accordion-panel -->
          </div>
          <!-- Payroll -->
          <div id="vat-Returns" class="tab-pane fade">
            <div class="accordion-panel">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Important Information</a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1">
                  <?php if(isset($client[0]['crm_vat_number']) && $client[0]['crm_vat_number']!=''){ ?>
                    <div class="form-group row name_fields">
                      <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(40); ?></label>
                      <div class="col-sm-8">
                       <!--  <input type="number" name="vat_number_one" id="vat_number_one" placeholder="" value="<?php if(isset($client[0]['crm_vat_number'])){ echo $client[0]['crm_vat_number']; } ?>" class="fields"> -->
                       <span class="small-info"><?php if(isset($client[0]['crm_vat_number'])){ echo $client[0]['crm_vat_number']; } ?></span>
                      </div>
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
            <!-- accordion-panel -->
            <div class="accordion-panel">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">VAT</a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                           <div class="basic-info-client1" id="vat_box">

                            <?php if(isset($client[0]['crm_vat_date_of_registration']) && ($client[0]['crm_vat_date_of_registration']!='') ){ ?>
                              <div class="form-group row  date_birth vat_sort" id="<?php echo $this->Common_mdl->get_order_details(41); ?>">
                                 <label class="col-sm-4 col-form-label">VAT Registration Date</label>
                                 <div class="col-sm-8">
                                    <?php if(isset($client[0]['crm_vat_date_of_registration']) && ($client[0]['crm_vat_date_of_registration']!='') ){ echo $client[0]['crm_vat_date_of_registration'];}?>
                                 </div>
                              </div>
                              <?php } ?>
                               <?php if(isset($client[0]['crm_vat_frequency']) && $client[0]['crm_vat_frequency']!=''){ ?>
                              <div class="form-group row vat_sort" id="<?php echo $this->Common_mdl->get_order_details(42); ?>">
                                 <label class="col-sm-4 col-form-label">VAT Frequency</label>
                                 <div class="col-sm-8">
                                    <?php if(isset($client[0]['crm_vat_frequency'])){ echo $client[0]['crm_vat_frequency']; }?>
                                 </div>
                              </div>
                              <?php } ?>
                               <?php if(isset($client[0]['crm_vat_quater_end_date']) && ($client[0]['crm_vat_quater_end_date']!='') ){ ?>
                              <div class="form-group row help_icon date_birth  vat_sort" id="<?php echo $this->Common_mdl->get_order_details(141); ?>">
                                 <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>VAT Quarter End Date</label>
                                 <div class="col-sm-8">
                                    <?php if(isset($client[0]['crm_vat_quater_end_date']) && ($client[0]['crm_vat_quater_end_date']!='') ){ echo $client[0]['crm_vat_quater_end_date'];}?>
                                 </div>
                              </div>
                              <?php } ?>
                              <?php if(isset($client[0]['crm_vat_quarters']) && $client[0]['crm_vat_quarters']!=''){ ?>
                              <div class="form-group row vat_sort" id="<?php echo $this->Common_mdl->get_order_details(142); ?>">
                                 <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>VAT Quarters</label>
                                 <div class="col-sm-8">
                                   <?php if(isset($client[0]['crm_vat_quarters'])){ echo $client[0]['crm_vat_quarters']; }?> 
                                 </div>
                              </div>
                              <?php } ?>
                               <?php if(isset($client[0]['crm_vat_due_date']) && ($client[0]['crm_vat_due_date']!='') ){ ?>
                              <div class="form-group row help_icon date_birth vat_sort" id="<?php echo $this->Common_mdl->get_order_details(143); ?>">
                                 <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>VAT Due Date</label>
                                 <div class="col-sm-8">
                                    <?php if(isset($client[0]['crm_vat_due_date']) && ($client[0]['crm_vat_due_date']!='') ){ echo $client[0]['crm_vat_due_date'];}?>
                                 </div>
                              </div>
                              <?php } ?>
                               <?php if(isset($client[0]['crm_vat_scheme']) && $client[0]['crm_vat_scheme']!='') { ?>
                              <div class="form-group row vat_sort" id="<?php echo $this->Common_mdl->get_order_details(144); ?>">
                                 <label class="col-sm-4 col-form-label">VAT Scheme</label>
                                 <div class="col-sm-8">
                                   <?php if(isset($client[0]['crm_vat_scheme'])) { echo $client[0]['crm_vat_scheme']; } ?>
                                 </div>
                              </div>
                              <?php } ?>
                               <?php if(isset($client[0]['crm_flat_rate_category']) && ($client[0]['crm_flat_rate_category']!='') ){ ?>
                              <div class="form-group row vat_sort" id="<?php echo $this->Common_mdl->get_order_details(43); ?>">
                                 <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(43); ?></label>
                                 <div class="col-sm-8">
                                   <?php if(isset($client[0]['crm_flat_rate_category']) && ($client[0]['crm_flat_rate_category']!='') ){ echo $client[0]['crm_flat_rate_category'];}?>
                                 </div>
                              </div>
                              <?php } ?>
                              <?php if(isset($client[0]['crm_flat_rate_percentage']) && ($client[0]['crm_flat_rate_percentage']!='') ){ ?>
                              <div class="form-group row vat_sort" id="<?php echo $this->Common_mdl->get_order_details(44); ?>">
                                 <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(44); ?></label>
                                 <div class="col-sm-8">
                                   <?php if(isset($client[0]['crm_flat_rate_percentage']) && ($client[0]['crm_flat_rate_percentage']!='') ){ echo $client[0]['crm_flat_rate_percentage'];}?>
                                 </div>
                              </div>
                              <?php } ?>
                              <?php if(isset($client[0]['crm_direct_debit']) && ($client[0]['crm_direct_debit']=='on') ){?>
                              <div class="form-group row name_fields vat_sort" id="<?php echo $this->Common_mdl->get_order_details(45); ?>">
                                 <label class="col-sm-4 col-form-label">Direct Debit with HMRC</label>
                                 <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right fields" name="direct_debit" id="direct_debit" <?php if(isset($client[0]['crm_direct_debit']) && ($client[0]['crm_direct_debit']=='on') ){?> checked="checked"<?php } ?> readonly>
                                 </div>
                              </div>
                              <?php } ?>  
                               <?php if(isset($client[0]['crm_annual_accounting_scheme']) && ($client[0]['crm_annual_accounting_scheme']=='on') ){?>
                              <div class="form-group row name_fields vat_sort" id="<?php echo $this->Common_mdl->get_order_details(46); ?>">
                                 <label class="col-sm-4 col-form-label">Annual Accounting Scheme</label>
                                 <div class="col-sm-8">
                                    <input type="checkbox" class="js-small f-right fields" name="annual_accounting_scheme" id="annual_accounting_scheme" <?php if(isset($client[0]['crm_annual_accounting_scheme']) && ($client[0]['crm_annual_accounting_scheme']=='on') ){?> checked="checked"<?php } ?> readonly>
                                 </div>
                              </div>
                              <?php } ?>
                              <?php if(isset($client[0]['crm_box5_of_last_quarter_submitted']) && $client[0]['crm_box5_of_last_quarter_submitted']!=''){ ?>
                              <div class="form-group row vat_sort" id="<?php echo $this->Common_mdl->get_order_details(47); ?>">
                                 <label class="col-sm-4 col-form-label">Box 5 Figure of Last Quarter</label>
                                 <div class="col-sm-8">
                                   <?php if(isset($client[0]['crm_box5_of_last_quarter_submitted'])){ echo $client[0]['crm_box5_of_last_quarter_submitted']; } ?>
                                 </div>
                              </div>
                              <?php } ?>
                               <?php if(isset($client[0]['crm_vat_address']) && ($client[0]['crm_vat_address']!='') ){ ?>
                              <div class="form-group row vat_sort" id="<?php echo $this->Common_mdl->get_order_details(48); ?>">
                                 <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(48); ?></label>
                                 <div class="col-sm-8">
                                 <?php if(isset($client[0]['crm_vat_address']) && ($client[0]['crm_vat_address']!='') ){ echo $client[0]['crm_vat_address'];}?>                       
                               </div>
                              </div>
                              <?php } ?>
                           </div>
                        </div>
              </div>
            </div>
            <!-- accordion-panel -->
            <div class="accordion-panel enable_vat" style="<?php echo $six;?>">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">VAT Reminders</a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1" id="vat_box1">
                    <div class="form-group row  date_birth">
                     <!--  <label class="col-sm-4 col-form-label">Next Reminder Date</label> -->
                      <div class="col-sm-8">
                        <!-- <input type="hidden" name="vat_next_reminder_date" id="vat_next_reminder_date" placeholder="" value="<?php if(isset($client[0]['crm_vat_next_reminder_date']) && ($client[0]['crm_vat_next_reminder_date']!='') ){ echo $client[0]['crm_vat_next_reminder_date'];}?>" class="fields datepicker"> -->
                        <a id="vat_add_custom_reminder_link" name="vat_add_custom_reminder_link" href="<?php echo base_url();?>User/reminder_settings/15" target="_blank">Tax Return reminder setting</a>
                        <input type="checkbox" class="js-small f-right fields" name="Vat_next_reminder_date" id="Vat_next_reminder_date"  checked="checked" onchange="check_checkbox10();" readonly>
                      </div>
                    </div>
                    <div class="form-group row radio_bts vat_sort" id="<?php echo $this->Common_mdl->get_order_details(145); ?>">
                      <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                      <div class="col-sm-8">
                        <input type="checkbox" class="js-small f-right fields" name="vat_create_task_reminder" id="vat_create_task_reminder" <?php if(isset($client[0]['crm_vat_create_task_reminder']) && ($client[0]['crm_vat_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> readonly>
                      </div>
                    </div>
                    <div class="form-group row radio_bts vat_sort" id="<?php echo $this->Common_mdl->get_order_details(146); ?>" style="">
                      <label id="vat_add_custom_reminder_label" name="vat_add_custom_reminder_label" class="col-sm-4 col-form-label">Add Custom Reminder</label>
                      <div class="col-sm-8">
                        <!-- <input type="checkbox" class="js-small f-right fields" name="vat_add_custom_reminder" id="vat_add_custom_reminder" <?php if(isset($client[0]['crm_vat_add_custom_reminder']) && ($client[0]['crm_vat_add_custom_reminder']=='on') ){?> checked="checked"<?php } ?>> -->
                        <!-- <textarea rows="3" placeholder="" name="vat_add_custom_reminder" id="vat_add_custom_reminder" class="fields"><?php if(isset($client[0]['crm_vat_add_custom_reminder']) && ($client[0]['crm_vat_add_custom_reminder']!='') ){ echo $client[0]['crm_vat_add_custom_reminder'];}?></textarea> -->
                        <a name="vat_add_custom_reminder" id="vat_add_custom_reminder" href="<?php echo base_url();?>User/reminder_settings/15" target="_blank">Click to add Custom Reminder</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- accordion-panel -->
            <div class="accordion-panel invt"  style="display: none">
              <div class="accordion-heading" role="tab" id="headingOne">
                <h3 class="card-title accordion-title">
                  <a class="accordion-msg">Invoice</a>
                </h3>
              </div>
              <div id="collapse" class="panel-collapse">
                <div class="basic-info-client1">
                  <div class="form-group row name_fields">
                    <label class="col-sm-4 col-form-label">Invoice</label>
                    <div class="col-sm-8">
                      <!-- <select name="invoice" id="invoice">
                        <option value="">Select</option>
                        <option value="E-Mail">E-Mail</option>
                        <option value="SMS">SMS</option>
                        <option value="Phone">Phone</option>
                      </select> -->
                      <span class="small-info">Email</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="accordion-panel">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Notes if any other</a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1">
                  <?php if(isset($client[0]['crm_vat_notes']) && ($client[0]['crm_vat_notes']!='') ){ ?>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Notes</label>
                      <div class="col-sm-8">
                        <!-- <textarea rows="3" placeholder="" name="vat_notes" id="vat_notes" class="fields"><?php if(isset($client[0]['crm_vat_notes']) && ($client[0]['crm_vat_notes']!='') ){ echo $client[0]['crm_vat_notes'];}?></textarea> -->
                        <p class="for-address"><?php if(isset($client[0]['crm_vat_notes']) && ($client[0]['crm_vat_notes']!='') ){ echo $client[0]['crm_vat_notes'];}?></p>
                      </div>
                    </div>
                  <?php } ?>
                  
                  </div>
                </div>
              </div>
            </div>
            <!-- accordion-panel -->
          </div>
          <!-- VAT Returns -->
          <div id="management-account" class="tab-pane fade">
            <div class="bookkeeptab" style="<?php echo $bookkeeptab;?>">
              <div class="accordion-panel">
                <div class="box-division03">
                  <div class="accordion-heading" role="tab" id="headingOne">
                    <h3 class="card-title accordion-title">
                      <a class="accordion-msg">Bookkeeping</a>
                    </h3>
                  </div>
                 <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1" id="management_box1">
                                 <div class="form-group row management" id="<?php echo $this->Common_mdl->get_order_details(49); ?>">
                                    <label class="col-sm-4 col-form-label">Bookkeepeing to Done</label>
                                    <div class="col-sm-8">
                                      <?php if(isset($client[0]['crm_bookkeeping'])){ echo $client[0]['crm_bookkeeping']; }?> 
                                    </div>
                                 </div>
                                 <?php if(isset($client[0]['crm_next_booking_date']) && ($client[0]['crm_next_booking_date']!='') ){ ?>
                                 <div class="form-group row help_icon date_birth management" id="<?php echo $this->Common_mdl->get_order_details(50); ?>">
                                    <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Next Bookkeeping Date</label>
                                    <div class="col-sm-8">
                                      <?php if(isset($client[0]['crm_next_booking_date']) && ($client[0]['crm_next_booking_date']!='') ){ echo $client[0]['crm_next_booking_date'];}?>
                                    </div>
                                 </div>
                                 <?php } ?>
                                  <?php if(isset($client[0]['crm_method_bookkeeping']) && $client[0]['crm_method_bookkeeping']!=''){ ?>
                                 <div class="form-group row management" id="<?php echo $this->Common_mdl->get_order_details(51); ?>">
                                    <label class="col-sm-4 col-form-label">Method of Bookkeeping</label>
                                    <div class="col-sm-8">
                                      <?php if(isset($client[0]['crm_method_bookkeeping'])){ echo $client[0]['crm_method_bookkeeping']; }?>
                                    </div>
                                 </div>
                                 <?php } ?>
                                 <?php if(isset($client[0]['crm_client_provide_record']) && $client[0]['crm_client_provide_record']!='') { ?>
                                 <div class="form-group row management" id="<?php echo $this->Common_mdl->get_order_details(52); ?>">
                                    <label class="col-sm-4 col-form-label">How Client will provide records</label>
                                    <div class="col-sm-8">
                                       <?php if(isset($client[0]['crm_client_provide_record'])) { echo $client[0]['crm_client_provide_record']; }?> 
                                    </div>
                                 </div>
                                 <?php } ?>
                              </div>
                </div>
                </div>
              </div>
              <!-- accordion-panel -->
              <div class="accordion-panel enable_book" style="<?php echo $ten;?>">
                <div class="box-division03">
                  <div class="accordion-heading" role="tab" id="headingOne">
                    <h3 class="card-title accordion-title">
                      <a class="accordion-msg">Bookkeep Reminders</a>
                    </h3>
                  </div>
                  <div id="collapse" class="panel-collapse">
                    <div class="basic-info-client1">
                      <div class="form-group row  date_birth">
                       <!--  <label class="col-sm-4 col-form-label">Next Reminder Date</label> -->
                        <div class="col-sm-8">
                          <!-- <input type="hidden" name="bookkeep_next_reminder_date" id="bookkeep_next_reminder_date" placeholder="" value="<?php if(isset($client[0]['crm_bookkeep_next_reminder_date']) && ($client[0]['crm_bookkeep_next_reminder_date']!='') ){ echo $client[0]['crm_bookkeep_next_reminder_date'];}?>" class="fields datepicker"> -->
                          <a id="bookkeep_add_custom_reminder_link" name="bookkeep_add_custom_reminder_link" href="<?php echo base_url();?>User/reminder_settings/13" target="_blank">As per firm setting</a>
                          <input type="checkbox" class="js-small f-right fields" name="bookkeep_next_reminder_date" id="bookkeep_next_reminder_date"  checked="checked" onchange="check_checkbox8();" readonly>
                          <!---iiiii-->
                        </div>
                      </div>
                      <div class="form-group row radio_bts ">
                        <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                        <div class="col-sm-8">
                          <input type="checkbox" class="js-small f-right fields" name="bookkeep_create_task_reminder" id="bookkeep_create_task_reminder" <?php if(isset($client[0]['crm_bookkeep_create_task_reminder']) && ($client[0]['crm_bookkeep_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> readonly>
                        </div>
                      </div>
                      <div class="form-group row radio_bts" style="">
                        <label id="bookkeep_add_custom_reminder_label" name="bookkeep_add_custom_reminder_label" class="col-sm-4 col-form-label">Add Custom Reminder</label>
                        <div class="col-sm-8">
                          <!-- <input type="checkbox" class="js-small f-right fields" name="bookkeep_add_custom_reminder" id="bookkeep_add_custom_reminder" <?php if(isset($client[0]['crm_bookkeep_add_custom_reminder']) && ($client[0]['crm_bookkeep_add_custom_reminder']=='on') ){?> checked="checked"<?php } ?>> -->
                          <!-- <textarea rows="3" placeholder="" name="bookkeep_add_custom_reminder" id="bookkeep_add_custom_reminder" class="fields"><?php if(isset($client[0]['crm_bookkeep_add_custom_reminder']) && ($client[0]['crm_bookkeep_add_custom_reminder']!='') ){ echo $client[0]['crm_bookkeep_add_custom_reminder'];}?></textarea> -->
                          <a name="bookkeep_add_custom_reminder" id="bookkeep_add_custom_reminder" href="<?php echo base_url();?>User/reminder_settings/13" target="_blank">Click to add Custom Reminder</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="accordion-panel inbook" style="<?php $bookkeepinvoice;?>">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Invoice</a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1">
                    <div class="form-group row name_fields">
                      <label class="col-sm-4 col-form-label">Invoice</label>
                      <div class="col-sm-8">
                        <!-- <select name="invoice_book" id="invoice_book">
                          <option value="">Select</option>
                          <option value="E-Mail">E-Mail</option>
                          <option value="SMS">SMS</option>
                          <option value="Phone">Phone</option>
                        </select> -->
                        <span class="small-info">Email</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- accordion-panel -->
            </div>
            <!-- bookkeep tab close-->
            <div class="accordion-panel">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Management Accounts</a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1" id="management_account">


                  <?php if(isset($client[0]['crm_manage_acc_fre']) && $client[0]['crm_manage_acc_fre']!='') { ?>
                    <div class="form-group row management" id="<?php echo $this->Common_mdl->get_order_details(53); ?>">
                      <label class="col-sm-4 col-form-label">Management Accounts Frequency</label>
                      <div class="col-sm-8">
                      
                        <span class="small-info"><?php if(isset($client[0]['crm_manage_acc_fre'])) { echo $client[0]['crm_manage_acc_fre'];}?></span>
                      </div>
                    </div>
                    <?php } ?>
                    <?php if(isset($client[0]['crm_next_manage_acc_date']) && ($client[0]['crm_next_manage_acc_date']!='') ){ ?><
                    <div class="form-group row help_icon date_birth management" id="<?php echo $this->Common_mdl->get_order_details(54); ?>">
                      <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Next Management Accounts Due date</label>
                      <div class="col-sm-8">
                       
                        <span class="small-info"><?php if(isset($client[0]['crm_next_manage_acc_date']) && ($client[0]['crm_next_manage_acc_date']!='') ){ echo $client[0]['crm_next_manage_acc_date'];}?></span>
                      </div>
                    </div>
                    <?php } ?>
                    <?php if(isset($client[0]['crm_manage_method_bookkeeping']) && $client[0]['crm_manage_method_bookkeeping']!='') { ?>
                    <div class="form-group row management" id="<?php echo $this->Common_mdl->get_order_details(55); ?>">
                      <label class="col-sm-4 col-form-label">Method of Bookkeeping</label>
                      <div class="col-sm-8">
                        
                        <span class="small-info"><?php if(isset($client[0]['crm_manage_method_bookkeeping'])) { echo $client[0]['crm_manage_method_bookkeeping'];}?></span>
                      </div>
                    </div>
                    <?php } ?>

                    <?php if(isset($client[0]['crm_manage_client_provide_record']) && $client[0]['crm_manage_client_provide_record']!='') { ?>
                    <div class="form-group row management" id="<?php echo $this->Common_mdl->get_order_details(56); ?>">
                      <label class="col-sm-4 col-form-label">How Client will provide records</label>
                      <div class="col-sm-8">
                       
                        <span class="small-info"><?php if(isset($client[0]['crm_manage_client_provide_record']) ) { echo $client[0]['crm_manage_client_provide_record'];}?></span>
                      </div>
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
            <!-- accordion-panel -->
            <div class="accordion-panel enable_management" style="<?php echo $eleven;?>">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Management Reminders</a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1" id="management_account1">
                    <div class="form-group row  date_birth">
                     <!--  <label class="col-sm-4 col-form-label">Next Reminder Date</label> -->
                      <div class="col-sm-8">
                        <input type="hidden" name="manage_next_reminder_date" id="manage_next_reminder_date" placeholder="" value="<?php if(isset($client[0]['crm_manage_next_reminder_date']) && ($client[0]['crm_manage_next_reminder_date']!='') ){ echo $client[0]['crm_manage_next_reminder_date'];}?>" class="fields datepicker">
                        <a id="manage_add_custom_reminder_link" name="manage_add_custom_reminder_link" href="<?php echo base_url();?>User/reminder_settings/14" target="_blank">As per firm setting</a>
                        <input type="checkbox" class="js-small f-right fields" name="manage_next_reminder_date" id="manage_next_reminder_date"  checked="checked" onchange="check_checkbox9();" readonly>
                        <!--jjjjj-->
                      </div>
                    </div>
                    <div class="form-group row radio_bts management" id="<?php echo $this->Common_mdl->get_order_details(147); ?>">
                      <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                      <div class="col-sm-8">
                        <input type="checkbox" class="js-small f-right fields" name="manage_create_task_reminder" id="manage_create_task_reminder" <?php if(isset($client[0]['crm_manage_create_task_reminder']) && ($client[0]['crm_manage_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> readonly>
                      </div>
                    </div>
                    <div class="form-group row radio_bts management" id="<?php echo $this->Common_mdl->get_order_details(148); ?>"  style="">
                      <label id="manage_add_custom_reminder_label" name="manage_add_custom_reminder_label" class="col-sm-4 col-form-label">Add Custom Reminder</label>
                      <div class="col-sm-8">
                        <!-- <input type="checkbox" class="js-small f-right fields" name="manage_add_custom_reminder" id="manage_add_custom_reminder" <?php if(isset($client[0]['crm_manage_add_custom_reminder']) && ($client[0]['crm_manage_add_custom_reminder']=='on') ){?> checked="checked"<?php } ?>> -->
                        <!-- <textarea rows="3" placeholder="" name="manage_add_custom_reminder" id="manage_add_custom_reminder" class="fields"><?php if(isset($client[0]['crm_manage_add_custom_reminder']) && ($client[0]['crm_manage_add_custom_reminder']!='') ){ echo $client[0]['crm_manage_add_custom_reminder'];}?></textarea> -->
                        <a name="manage_add_custom_reminder" id="manage_add_custom_reminder" href="<?php echo base_url();?>User/reminder_settings/14" target="_blank">Click to add Custom Reminder</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- accordion-panel -->
            <div class="accordion-panel inma"  style="display: none">
              <div class="accordion-heading" role="tab" id="headingOne">
                <h3 class="card-title accordion-title">
                  <a class="accordion-msg">Invoice</a>
                </h3>
              </div>
              <div id="collapse" class="panel-collapse">
                <div class="basic-info-client1">
                  <div class="form-group row name_fields">
                    <label class="col-sm-4 col-form-label">Invoice</label>
                    <div class="col-sm-8">
                      <!-- <select name="invoice_ma" id="invoice_ma">
                        <option value="">Select</option>
                        <option value="E-Mail">E-Mail</option>
                        <option value="SMS">SMS</option>
                        <option value="Phone">Phone</option>
                      </select> -->
                      <span class="small-info">Email</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="accordion-panel">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Notes if any other            </a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1">
                  <?php if(isset($client[0]['crm_manage_notes']) && ($client[0]['crm_manage_notes']!='') ){  ?>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Notes</label>
                      <div class="col-sm-8">
                       
                        <p class="for-address"><?php if(isset($client[0]['crm_manage_notes']) && ($client[0]['crm_manage_notes']!='') ){ echo $client[0]['crm_manage_notes'];}?></p>
                      </div>
                    </div>
                    <?php } ?>
                   
                  </div>
                </div>
              </div>
            </div>
            <!-- accordion-panel -->
          </div>
          <!-- Management Accounts --> 
          <div id="investigation-insurance" class="tab-pane fade">
            <div class="accordion-panel">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Investigation Insurance          </a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1" id="invest-box">
                  <?php if(isset($client[0]['crm_invesitgation_insurance']) && ($client[0]['crm_invesitgation_insurance']!='') ){ ?>
                    <div class="form-group row help_icon date_birth invest" id="<?php echo $this->Common_mdl->get_order_details(58); ?>">
                      <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Insurance Start Date</label>
                      <div class="col-sm-8">
                        
                       <span class="small-info"><?php if(isset($client[0]['crm_invesitgation_insurance']) && ($client[0]['crm_invesitgation_insurance']!='') ){ echo $client[0]['crm_invesitgation_insurance'];}?></span>
                      </div>
                    </div>
                    <?php } ?>
                    <?php if(isset($client[0]['crm_insurance_renew_date']) && ($client[0]['crm_insurance_renew_date']!='') ){ ?>
                    <div class="form-group row help_icon date_birth invest" id="<?php echo $this->Common_mdl->get_order_details(59); ?>">
                      <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Insurance Renew Date</label>
                      <div class="col-sm-8">
                       
                        <span class="small-info"><?php if(isset($client[0]['crm_insurance_renew_date']) && ($client[0]['crm_insurance_renew_date']!='') ){ echo $client[0]['crm_insurance_renew_date'];}?></span>
                      </div>
                    </div>
                    <?php } ?>
                    <?php if(isset($client[0]['crm_insurance_provider'])  && $client[0]['crm_insurance_provider']!=''){?>
                    <div class="form-group row invest" id="<?php echo $this->Common_mdl->get_order_details(60); ?>">
                      <label class="col-sm-4 col-form-label">Insurance Provider</label>
                      <div class="col-sm-8">
                       
                        <span class="small-info"><?php if(isset($client[0]['crm_insurance_provider']) ) { echo $client[0]['crm_insurance_provider'];}?></span>
                      </div>
                    </div>
                    <?php } ?>
                    <?php if(isset($client[0]['crm_claims_note']) && ($client[0]['crm_claims_note']!='') ){ ?>
                    <div class="form-group row invest" id="<?php echo $this->Common_mdl->get_order_details(140); ?>">
                      <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(140); ?></label>
                      <div class="col-sm-8">
                       
                        <p class="for-address"><?php if(isset($client[0]['crm_claims_note']) && ($client[0]['crm_claims_note']!='') ){ echo $client[0]['crm_claims_note'];}?></p>
                      </div>
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
            <!-- accordion-panel -->
            <div class="accordion-panel enable_invest" style="<?php echo $twelve;?>">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Investigation Insurance Reminders</a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1" id="invest_box1">
                    <div class="form-group row  date_birth">
                      <!-- <label class="col-sm-4 col-form-label">Next Reminder Date</label> -->
                      <div class="col-sm-8">
                        <!-- <input type="hidden" name="insurance_next_reminder_date" id="insurance_next_reminder_date" placeholder="" value="<?php if(isset($client[0]['crm_insurance_next_reminder_date']) && ($client[0]['crm_insurance_next_reminder_date']!='') ){ echo $client[0]['crm_insurance_next_reminder_date'];}?>" class="fields datepicker"> -->
                        <a id="insurance_add_custom_reminder_link" name="insurance_add_custom_reminder_link" href="<?php echo base_url(); ?>User/reminder_settings/9" target="_blank">As per firm setting</a>
                        <input type="checkbox" class="js-small f-right fields" name="insurance_next_reminder_date" id="insurance_next_reminder_date"  checked="checked" onchange="check_checkbox13();" readonly>
                      </div>
                    </div>
                    <div class="form-group row radio_bts thirteent" style="<?php echo $thirteent; ?>">
                      <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                      <div class="col-sm-8">
                        <input type="checkbox" class="js-small f-right fields" name="insurance_create_task_reminder" id="insurance_create_task_reminder" <?php if(isset($client[0]['crm_insurance_create_task_reminder']) && ($client[0]['crm_insurance_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> readonly>
                      </div>
                    </div>
                    <div class="form-group row radio_bts" style="">
                      <label class="col-sm-4 col-form-label" name="insurance_add_custom_reminder_label" id="insurance_add_custom_reminder_label">Add Custom Reminder</label>
                      <div class="col-sm-8">
                        <!--  <input type="checkbox" class="js-small f-right fields" name="insurance_add_custom_reminder" id="insurance_add_custom_reminder" <?php if(isset($client[0]['crm_insurance_add_custom_reminder']) && ($client[0]['crm_insurance_add_custom_reminder']=='on') ){?> checked="checked"<?php } ?>> -->
                        <!-- <textarea rows="3" placeholder="" name="insurance_add_custom_reminder" id="insurance_add_custom_reminder" class="fields"><?php if(isset($client[0]['crm_insurance_add_custom_reminder']) && ($client[0]['crm_insurance_add_custom_reminder']!='') ){ echo $client[0]['crm_insurance_add_custom_reminder'];}?></textarea> -->
                        <a name="insurance_add_custom_reminder" id="insurance_add_custom_reminder" href="<?php echo base_url(); ?>User/reminder_settings/9" target="_blank">Click to add Custom Reminder</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="accordion-panel invoice_insurance" style="display: none">
              <div class="accordion-heading" role="tab" id="headingOne">
                <h3 class="card-title accordion-title">
                  <a class="accordion-msg">Insurance Invoice</a>
                </h3>
              </div>
              <div id="collapse" class="panel-collapse">
                <div class="basic-info-client1">
                  <div class="form-group row name_fields">
                    <label class="col-sm-4 col-form-label">Invoice</label>
                    <div class="col-sm-8">
                      <!-- <select name="invoice_insuance_data" id="invoice_insuance_data">
                        <option value="E-Mail">E-Mail</option>
                        <option value="SMS">SMS</option>
                        <option value="Phone">Phone</option>
                      </select> -->
                      <span class="small-info">Email</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- accordion-panel -->
            <div class="registeredtab" style="<?php echo $registeredtab;?>">
              <div class="accordion-panel">
                <div class="box-division03">
                  <div class="accordion-heading" role="tab" id="headingOne">
                    <h3 class="card-title accordion-title">
                      <a class="accordion-msg">Registered Office</a>
                    </h3>
                  </div>
                  <div id="collapse" class="panel-collapse">
                    <div class="basic-info-client1" id="invest_box2">
                    <?php if(isset($client[0]['crm_registered_start_date']) && ($client[0]['crm_registered_start_date']!='') ){ ?>
                      <div class="form-group row help_icon date_birth invest" id="<?php echo $this->Common_mdl->get_order_details(62); ?>">
                        <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Registered Office Start Date</label>
                        <div class="col-sm-8">
                         <span class="small-info"><?php if(isset($client[0]['crm_registered_start_date']) && ($client[0]['crm_registered_start_date']!='') ){ echo $client[0]['crm_registered_start_date'];}?></span>
                        </div>
                      </div>
                      <?php } ?>
                      <?php if(isset($client[0]['crm_registered_renew_date']) && ($client[0]['crm_registered_renew_date']!='') ){ ?>
                      <div class="form-group row help_icon date_birth invest" id="<?php echo $this->Common_mdl->get_order_details(63); ?>">
                        <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Registered Office Renew Date</label>
                        <div class="col-sm-8">
                        
                         <span class="small-info"><?php if(isset($client[0]['crm_registered_renew_date']) && ($client[0]['crm_registered_renew_date']!='') ){ echo $client[0]['crm_registered_renew_date'];}?></span>
                        </div>
                      </div>
                      <?php } ?>
                      <?php if(isset($client[0]['crm_registered_office_inuse']) && ($client[0]['crm_registered_office_inuse']!='') ){ ?>
                      <div class="form-group row invest" id="<?php echo $this->Common_mdl->get_order_details(64); ?>">
                        <label class="col-sm-4 col-form-label">Registered office in Use</label>
                        <div class="col-sm-8">
                          
                          <span class="small-info"><?php if(isset($client[0]['crm_registered_office_inuse']) && ($client[0]['crm_registered_office_inuse']!='') ){ echo $client[0]['crm_registered_office_inuse'];}?></span>
                        </div>
                      </div>
                      <?php } ?>
                      <?php if(isset($client[0]['crm_registered_claims_note']) && ($client[0]['crm_registered_claims_note']!='') ){ ?>
                      <div class="form-group row invest" id="<?php echo $this->Common_mdl->get_order_details(65); ?>">
                        <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(65); ?></label>
                        <div class="col-sm-8">
                        
                         <p class="for-address"><?php if(isset($client[0]['crm_registered_claims_note']) && ($client[0]['crm_registered_claims_note']!='') ){ echo $client[0]['crm_registered_claims_note'];}?></p>
                        </div>
                      </div>
                      <?php } ?>
                    </div>
                  </div>
                </div>
              </div>
              <!-- accordion-panel -->
              <div class="accordion-panel enable_reg" style="<?php echo $thirteen;?>">
                <div class="box-division03">
                  <div class="accordion-heading" role="tab" id="headingOne">
                    <h3 class="card-title accordion-title">
                      <a class="accordion-msg">Registered Reminders</a>
                    </h3>
                  </div>
                  <div id="collapse" class="panel-collapse">
                    <div class="basic-info-client1">
                      <div class="form-group row  date_birth">
                       <!--  <label class="col-sm-4 col-form-label">Next Reminder Date</label> -->
                        <div class="col-sm-8">
                          <!--  <input type="hidden" name="registered_next_reminder_date" id="registered_next_reminder_date" placeholder="" value="<?php if(isset($client[0]['crm_registered_next_reminder_date']) && ($client[0]['crm_registered_next_reminder_date']!='') ){ echo $client[0]['crm_registered_next_reminder_date'];}?>" class="fields datepicker"> -->
                          <a id="registered_add_custom_reminder_link" name="registered_add_custom_reminder_link" href="<?php echo base_url(); ?>User/reminder_settings/12" target="_blank">As per firm setting</a>
                          <input type="checkbox" class="js-small f-right fields" name="registered_next_reminder_date" id="registered_next_reminder_date"  checked="checked" onchange="check_checkbox14();" readonly>
                        </div>
                      </div>
                      <div class="form-group row radio_bts ">
                        <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                        <div class="col-sm-8">
                          <input type="checkbox" class="js-small f-right fields" name="registered_create_task_reminder" id="registered_create_task_reminder" <?php if(isset($client[0]['crm_registered_create_task_reminder']) && ($client[0]['crm_registered_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> readonly>
                        </div>
                      </div>
                      <div class="form-group row radio_bts" style="">
                        <label class="col-sm-4 col-form-label" id="registered_add_custom_reminder_label" name="registered_add_custom_reminder_label">Add Custom Reminder</label>
                        <div class="col-sm-8">
                          <!-- <input type="checkbox" class="js-small f-right fields" name="registered_add_custom_reminder" id="registered_add_custom_reminder" <?php if(isset($client[0]['crm_registered_add_custom_reminder']) && ($client[0]['crm_registered_add_custom_reminder']=='on') ){?> checked="checked"<?php } ?>> -->
                          <!-- <textarea rows="3" placeholder="" name="registered_add_custom_reminder" id="registered_add_custom_reminder" class="fields"><?php if(isset($client[0]['crm_registered_add_custom_reminder']) && ($client[0]['crm_registered_add_custom_reminder']!='') ){ echo $client[0]['crm_registered_add_custom_reminder'];}?></textarea> -->
                          <a name="registered_add_custom_reminder" id="registered_add_custom_reminder" href="<?php echo base_url(); ?>User/reminder_settings/9" target="_blank">Click to add Custom Reminder</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- accordion-panel -->
              <div class="accordion-panel invoice_regaddr" style="display: none">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Register Address Invoice</a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1">
                    <div class="form-group row name_fields">
                      <label class="col-sm-4 col-form-label">Invoice</label>
                      <div class="col-sm-8">
                        <!-- <select name="invoice_regaddr_data" id="invoice_regaddr_data">
                          <option value="">Select</option>
                          <option value="E-Mail">E-Mail</option>
                          <option value="SMS">SMS</option>
                          <option value="Phone">Phone</option>
                        </select> -->
                        <span class="small-info">Email</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- registeredtab close-->
            <div class="taxadvicetab" style="<?php echo $taxadvicetab;?>">
              <div class="accordion-panel">
                <div class="box-division03">
                  <div class="accordion-heading" role="tab" id="headingOne">
                    <h3 class="card-title accordion-title">
                      <a class="accordion-msg">Tax Advice/Investigation</a>
                    </h3>
                  </div>
                  <div id="collapse" class="panel-collapse">
                    <div class="basic-info-client1">
                    <?php if(isset($client[0]['crm_investigation_start_date']) && ($client[0]['crm_investigation_start_date']!='') ){ ?>
                      <div class="form-group row help_icon date_birth">
                        <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Start Date</label>
                        <div class="col-sm-8">
                         
                          <span class="small-info"><?php if(isset($client[0]['crm_investigation_start_date']) && ($client[0]['crm_investigation_start_date']!='') ){ echo $client[0]['crm_investigation_start_date'];}?></span>
                        </div>
                      </div>
                      <?php } ?>
                      <?php if(isset($client[0]['crm_investigation_end_date']) && ($client[0]['crm_investigation_end_date']!='') ){ ?>
                      <div class="form-group row help_icon date_birth">
                        <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>End Date</label>
                        <div class="col-sm-8">
                         
                         <span class="small-info"><?php if(isset($client[0]['crm_investigation_end_date']) && ($client[0]['crm_investigation_end_date']!='') ){ echo $client[0]['crm_investigation_end_date'];}?></span>
                        </div>
                      </div>
                      <?php } ?>
                    <?php if(isset($client[0]['crm_investigation_note']) && ($client[0]['crm_investigation_note']!='') ){  ?>
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(68); ?></label>
                        <div class="col-sm-8">
                         
                          <p class="for-address"><?php if(isset($client[0]['crm_investigation_note']) && ($client[0]['crm_investigation_note']!='') ){ echo $client[0]['crm_investigation_note'];}?></p>
                        </div>
                      </div>
                      <?php } ?>
                    </div>
                  </div>
                </div>
              </div>
              <!-- accordion-panel -->
              <div class="accordion-panel enable_taxad" style="<?php echo $fourteen; ?>">
                <div class="box-division03">
                  <div class="accordion-heading" role="tab" id="headingOne">
                    <h3 class="card-title accordion-title">
                      <a class="accordion-msg">Tax advice/investigation Reminders</a>
                    </h3>
                  </div>
                  <div id="collapse" class="panel-collapse">
                    <div class="basic-info-client1">
                      <div class="form-group row  date_birth">
                        <!-- <label class="col-sm-4 col-form-label">Next Reminder Date</label> -->
                        <div class="col-sm-8">
                          <!-- <input type="hidden" name="investigation_next_reminder_date" id="investigation_next_reminder_date" placeholder="" value="<?php if(isset($client[0]['crm_investigation_next_reminder_date']) && ($client[0]['crm_investigation_next_reminder_date']!='') ){ echo $client[0]['crm_investigation_next_reminder_date'];}?>" class="fields datepicker"> -->
                          <a id="investigation_add_custom_reminder_link" name="investigation_add_custom_reminder_link" href="<?php echo base_url(); ?>User/reminder_settings/12" target="_blank">As per firm setting</a>
                          <input type="checkbox" class="js-small f-right fields" name="investigation_next_reminder_date" id="investigation_next_reminder_date"  checked="checked" onchange="check_checkbox15();" readonly>
                        </div>
                      </div>
                      <div class="form-group row radio_bts ">
                        <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                        <div class="col-sm-8">
                          <input type="checkbox" class="js-small f-right fields" name="investigation_create_task_reminder" id="investigation_create_task_reminder" <?php if(isset($client[0]['crm_investigation_create_task_reminder']) && ($client[0]['crm_investigation_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> readonly>
                        </div>
                      </div>
                      <div class="form-group row radio_bts" style="">
                        <label class="col-sm-4 col-form-label" id="investigation_add_custom_reminder_label" name="investigation_add_custom_reminder_label">Add Custom Reminder</label>
                        <div class="col-sm-8">
                          <!-- <input type="checkbox" class="js-small f-right fields" name="investigation_add_custom_reminder" id="investigation_add_custom_reminder" <?php if(isset($client[0]['crm_investigation_add_custom_reminder']) && ($client[0]['crm_investigation_add_custom_reminder']=='on') ){?> checked="checked"<?php } ?>> -->
                          <!--  <textarea rows="3" placeholder="" name="investigation_add_custom_reminder" id="investigation_add_custom_reminder" class="fields"><?php if(isset($client[0]['crm_investigation_add_custom_reminder']) && ($client[0]['crm_investigation_add_custom_reminder']!='') ){ echo $client[0]['crm_investigation_add_custom_reminder'];}?></textarea> -->
                          <a name="investigation_add_custom_reminder" id="investigation_add_custom_reminder" href="<?php echo base_url(); ?>User/reminder_settings/9" target="_blank">Click to add Custom Reminder</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- accordion-panel -->
            </div>
            <!-- taxadvicetab-->
            <div class="accordion-panel invoice_taxadvice" style="display: none">
              <div class="accordion-heading" role="tab" id="headingOne">
                <h3 class="card-title accordion-title">
                  <a class="accordion-msg">Tax advice Invoice</a>
                </h3>
              </div>
              <div id="collapse" class="panel-collapse">
                <div class="basic-info-client1">
                  <div class="form-group row name_fields">
                    <label class="col-sm-4 col-form-label">Invoice</label>
                    <div class="col-sm-8">
                      <!-- <select name="invoice_taxad_data" id="invoice_taxad_data">
                        <option value="">Select</option>
                        <option value="E-Mail">E-Mail</option>
                        <option value="SMS">SMS</option>
                        <option value="Phone">Phone</option>
                      </select> -->
                      <span class="small-info">Email</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="accordion-panel invoice_taxinvest" style="display: none">
              <div class="accordion-heading" role="tab" id="headingOne">
                <h3 class="card-title accordion-title">
                  <a class="accordion-msg">Tax Invest Invoice</a>
                </h3>
              </div>
              <div id="collapse" class="panel-collapse">
                <div class="basic-info-client1">
                  <div class="form-group row name_fields">
                    <label class="col-sm-4 col-form-label">Invoice</label>
                    <div class="col-sm-8">
                      <!-- <select name="invoice_taxinvest_data" id="invoice_taxinvest_data">
                        <option value="">Select</option>
                        <option value="E-Mail">E-Mail</option>
                        <option value="SMS">SMS</option>
                        <option value="Phone">Phone</option>
                      </select> -->
                      <span class="small-info">Email</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="accordion-panel">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Notes if any other</a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1">
                  <?php if(isset($client[0]['crm_tax_investigation_note']) && ($client[0]['crm_tax_investigation_note']!='') ){  ?>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Notes</label>
                      <div class="col-sm-8">
                       
                        <p class="for-address"><?php if(isset($client[0]['crm_tax_investigation_note']) && ($client[0]['crm_tax_investigation_note']!='') ){ echo $client[0]['crm_tax_investigation_note'];}?></p>
                      </div>
                    </div>
                    <?php } ?>
                    <?php
                      //echo render_custom_fields_one( 'investigation',$rel_id);    
                    $s_inv = $this->Common_mdl->getCustomFieldRec($rel_id,'investigation');
                     
                        if($s_inv){
                          foreach ($s_inv as $key_inv => $value_inv) {
                            if($key_inv == ''){ $key_inv = '-'; }
                            if($value_inv == ''){ $value_inv = '-'; }
                          ?>
                          <div class="form-group row name_fields">
                            <label class="col-sm-4 col-form-label"><?php echo $key_inv;?></label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php echo $value_inv;?></span>
                              </div>
                          </div>
                    <?php
                          }
                        }?>
                  </div>
                </div>
              </div>
            </div>
            <!-- accordion-panel -->
            <!-- <div class="floation_set text-right accordion_ups">
              <input type="submit" class="add_acc_client" value="Save & Exit" id="submit"/>
              </div> -->
          </div>
          <!-- Investigation Insurance -->   
          <div id="notes" class="tab-pane fade" style="display: none;">
            <div class="accordion-panel">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Additional Information - Internal Notes</a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1">
                    <div class="form-group row ">
                      <label class="col-sm-4 col-form-label">Notes</label>
                      <div class="col-sm-8">
                       <p class="for-address"><?php if(isset($client[0]['crm_other_internal_notes']) && ($client[0]['crm_other_internal_notes']!='') ){ echo $client[0]['crm_other_internal_notes'];}?></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- accordion-panel -->
          </div>
          <!-- Notes tab close -->
          <!-- time line services tab -->
            <!-- time line services tab -->
               <div id="timelineservices" class="tab-pane fade">
                  <?php 
                     $data['client_details'] = $client;
                     $data['client_id']=$client[0]['user_id'];
                     $this->load->view('Timeline/timeline_services_second',$data); 
                             ?>
               </div>
               <!-- end of timeline services tab -->
          <!-- end of timeline services tab -->

          <div id="newtimelineservices" class="tab-pane fade">
          <div class="modal-alertsuccess  alert alert-success-reminder succs" style="display: none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><div class="pop-realted1"><div class="position-alert1 timeline_success_messages"></div></div></div>
                   <?php 
                    $client_services=array("VAT"=>"VAT","Payroll"=>"Payroll","Accounts"=>"Accounts","Confirmation_statement"=>"Confirmation statement","Company_Tax_Return"=>"Company Tax Return","Personal_Tax_Return"=>"Personal Tax Return","WorkPlace_Pension"=>"WorkPlace Pension - AE","CIS_Contractor"=>"CIS - Contractor","CIS_Sub_Contractor"=>"CIS - Sub Contractor","P11D"=>"P11D","Bookkeeping"=>"Bookkeeping","Management_Accounts"=>"Management Accounts","Investigation_Insurance"=>"Investigation Insurance","Registered_Address"=>"Registered Address","Tax_Advice"=>"Tax Advice","Tax_Investigation"=>"Tax Investigation");
                    ?>
                    <div class="client_timeline_services">
                    <?php
                     $data['client_details'] = $client;
                     $data['client_id']=$client[0]['user_id'];
                     $this->load->view('Timeline/client_timeline_services',$data); 
                    ?>
                    </div>
           
          </div>

          <div id="documents" class="tab-pane fade">
            <!-- <div class="accordion-panel">
              <div class="box-division03">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Documents</a>
                  </h3>
                </div>
                <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1">
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">upload Document</label>
                      <div class="col-sm-8">
                        <input type="file" name="document" id="document" class="fields">
                        <span class="small-infor">test</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div> -->
            <!-- accordion-panel -->
          </div>
          <!-- documents tab close -->
          <div id="task" class="tab-pane fade">
      <div class="space-required">
            <div class="all_user-section2 floating_set">
              <div class="all-usera1 user-dashboard-section1">
                <div class="client_section3 table-responsive ">
                  <table class="table client_table1 text-center display nowrap" id="alltask" cellspacing="0" width="100%">
                    <thead>
                      <tr class="text-uppercase">
                       <!--  <th>
                          <div class="checkbox-fade fade-in-primary">
                            <label>
                            <input type="checkbox"  id="bulkDelete"  />
                            <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                            </label>
                          </div>
                        </th> -->
                        <th>Task Created</th>
                        <th>Task Name</th>
                        <!-- <th>CRM-Username</th> -->
                        <th>Start Date</th>
                        <th>Due Date</th>
                        <th>Status</th>
                        <th>Priority</th>
                        <th>Tag</th>
                        <th>Assignto</th>
                       <!--  <th>Actions</th> -->
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($task_list as $key => $value) {
                        if($value['worker']=='')
                        {
                        $value['worker'] = 0;
                        }
                        $staff=$this->db->query("SELECT * FROM staff_form WHERE user_id in (".$value['worker'].")")->result_array();
                        if($value['task_status']=='notstarted')
                        {
                        $stat = 'Not Started';
                        }else if($value['task_status']=='inprogress')
                        {
                        $stat = 'In Progress';
                        }else if($value['task_status']=='awaiting')
                        {
                        $stat = 'Awaiting for a feedback';
                        }else if($value['task_status']=='testing')
                        {
                        $stat = 'Testing';
                        }else if($value['task_status']='complete')
                        {
                        $stat = 'Complete';
                        }
                        $exp_tag = explode(',', $value['tag']);
                        
                        ?>
                      <tr id="<?php echo $value['id']; ?>">
                      <!--   <td>
                          <div class="checkbox-fade fade-in-primary">
                            <label>
                            <input type='checkbox'  class='deleteRow' value="<?php echo $value['id'];?>"  />
                            <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                            </label>
                          </div>
                        </td> -->
                        <td><?php echo date('Y-m-d H:i:s', $value['created_date']);?></td>
                        <td><a href="<?php echo base_url().'user/task_details/'.$value['id']; ?>"><?php echo ucfirst($value['subject']);?></a></td>
                        <td><?php echo $value['start_date'];?></td>
                        <td><?php echo $value['end_date'];?></td>
                        <td><?php echo $stat;?></td>
                        <td>
                          <?php echo $value['priority'];?>
                        </td>
                        <td>
                          <?php  foreach ($exp_tag as $exp_tag_key => $exp_tag_value) {
                            echo $tagName = $this->Common_mdl->getTag($exp_tag_value).' ';
                            }?>
                        </td>
                        <td class="user_imgs">
                          <?php
                            foreach($staff as $key => $val){     
                            ?>
                          <img src="<?php echo base_url().'uploads/'.$val['profile'];?>" alt="img">
                          <?php } ?>
                        </td>
                        <!--<td>
                          <p class="action_01">
                            <?php if($role!='Staff'){?>  
                            <a href="<?php echo base_url().'user/delete/'.$value['id'];?>" onclick="return confirm('Are you sure want to delete');"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>
                            <?php } ?>
                            <a href="<?php echo base_url().'user/update_task/'.$value['id'];?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                          
                          </p>
                        </td>-->
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div></div>
          </div>
          <div id="leads" class="tab-pane fade">
      <div class="space-required">
            <div class="all_user-section2 floating_set">
              <div class="all-usera1 user-dashboard-section1">
                <div class="client_section3 table-responsive ">
                  <table class="table client_table1 text-center display nowrap" id="allleads" cellspacing="0" width="100%">
                   <thead>
                           <tr class="text-uppercase">
                              <th>SNO</th>
                              <th>Name</th>
                              <th>Company</th>
                              <th>Email</th>
                              <th>Phone</th>
                              <th>Tags</th>
                              <th>Assigned</th>
                              <th>status</th>
                              <th>source</th>
                              <th>contact Date</th>
                             <!--  <th>Action</th> -->
                           </tr>
                        </thead>
                   <tbody>
                           <?php $i =1 ; foreach ($leads as $key => $value) {
                              $getUserProfilepic = $this->Common_mdl->getUserProfilepic($value['assigned']); 
                              /*if($value['lead_status']==1)
                              {
                                $value['lead_status'] = 'Customer';
                              }else{
                                $value['lead_status'] = '';
                              }*/
                               $status_name = $this->Common_mdl->GetAllWithWhere('leads_status','id',$value['lead_status']);
                               if(!empty($status_name))
                               {
                                $statusname = $status_name[0]['status_name'];
                               }else{
                                $statusname = '-';
                               }
                              
                              /* if($value['source']==1)
                              {
                                $value['source'] = 'Google';
                              }elseif($value['source']==2)
                              {
                                $value['source'] = 'Facebook';
                              }else{
                                $value['source'] = '';
                              }*/
                              
                              $source_name = $this->Common_mdl->GetAllWithWhere('source','id',$value['source']);
                               if(!empty($source_name))
                               {
                              $value['source'] = $source_name[0]['source_name'];
                              /*if($value['source']!='')
                              {
                                $value['source'] = $value['source'];*/
                              }else{
                                $value['source'] = '-';
                              }
                              
                              ?>
                           <tr>
                              <td><?php echo $i;?></td>
                              <td>
                                 <!-- <a href="#" data-toggle="modal" data-target="#profile_lead_<?php echo $value['id'];?>"><?php echo $value['name'];?></a> -->
                                 <a href="<?php echo base_url().'leads/leads_detailed_tab/'.$value['id'];?>" ><?php echo $value['name'];?></a>
                              </td>
                              <!-- <td><?php echo $value['company'];?></td> -->
                              <td>
                              <?php 
                              //echo $value['company'];
                              if(is_numeric($value['company']))
                              {
                                 $client_query=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and id='".$value['company']."' order by id desc ")->result_array();
                                 echo $client_query[0]['crm_company_name'];
                              }
                              else
                              {
                                 echo $value['company'];
                              }
                                
                              ?>
                              </td>
                              <td><?php echo $value['email_address'];?></td>
                              <td><?php echo $value['phone'];?></td>
                              <td><?php echo $value['tags'];?></td>
                              <td><img src="<?php echo $getUserProfilepic;?>" alt="img" height="50" width="50"></td>
                              <td class="lead_status_<?php echo $value['id'];?>"><?php echo $statusname;?></td>
                              <td><?php echo $value['source'];?></td>
                              <td><?php echo $value['contact_date'];?></td>
                             <!-- <td>
                                 <p class="action_01">
                                    <a href="<?php echo base_url().'leads/delete/'.$value['id'].'/0';?>" onclick="return confirm('Are you sure want to delete');"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>
                                  
                                    <a href="<?php echo base_url().'leads/edit_lead_view/'.$value['id'].'';?>" ><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                                   
                                 </p>
                              </td>-->
                           </tr>
                           <?php $i++; } ?>
                        </tbody>
                  </table>
                </div>
              </div>
            </div></div>
          </div>



          <div id="proposals" class="tab-pane fade">
      <div class="space-required">
            <div class="all_user-section2 floating_set">
              <div class="all-usera1 user-dashboard-section1">
                <div class="client_section3 table-responsive ">
                  <table class="table client_table1 text-center display nowrap" id="allproposal" cellspacing="0" width="100%">
                    <thead>
                      <tr class="text-uppercase">
                        <!-- <th>
                          <div class="checkbox-fade fade-in-primary">
                            <label>
                            <input type="checkbox"  id="bulkDelete"  />
                            <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                            </label>
                          </div>
                        </th> -->
                        <th>S.no</th>
                        <th>Proposal Name</th>                     
                        <th>Created Date</th>                       
                        <th>Status</th>                       
                       <!--  <th>Actions</th> -->
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                      $i=1;
                      foreach ($proposals as $key => $value) {
                 $random_string=generateRandomString('100');
                 $link=base_url().'proposal_page/step_proposal/'.$random_string.'---'.$value['id'];
                       ?>
                        
                      <tr id="<?php echo $value['id']; ?>">
                        <!-- <td>
                          <div class="checkbox-fade fade-in-primary">
                            <label>
                            <input type='checkbox'  class='deleteRow' value="<?php echo $value['id'];?>"  />
                            <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                            </label>
                          </div>
                        </td> -->
                        <td><?php echo $i; ?>
                        <td><a href="<?php echo $link; ?>"><?php echo $value['proposal_name']; ?></a></td>
                        <td><?php echo $value['created_at'];?></td>
                        <td><?php echo $value['status'];?></td>
                        <!--<td>
                          <p class="action_01">
                           
                          
                            <a href="#" onclick="return confirm_delete('<?php echo $value['id'];?>');"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>    
                            <?php $random_string=generateRandomString('100');?>
                            <a href="<?php echo base_url().'proposal_page/copy_proposal/'.$random_string.'---'.$value['id'];?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                        
                          </p>
                        </td>-->
                      </tr>
            <div class="modal-alertsuccess alert alert-success" id="delete_proposal<?php echo $value['id'];?>" style="display:none;">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                  <div class="pop-realted1">
                  <div class="position-alert1">
                  Are you sure want to delete <b><a href="<?php echo base_url().'client/delete_proposal/'.$value['id'];?>"> Yes </a></b> OR <b><a href="#" id="close">No</a></b></div>
                  </div></div>          
                      <?php $i++; } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div></div>
          </div>

           <div id="invoice" class="tab-pane fade">
      <div class="space-required">
            <div class="all_user-section2 floating_set">
              <div class="all-usera1 user-dashboard-section1">
                <div class="client_section3 table-responsive ">
                           <table class="table client_table1 text-center display nowrap printableArea" id="home_table" cellspacing="0" width="100%">
                   <thead>
                     <tr class="text-uppercase">
                     <!--   <th><div class="checkbox-fade fade-in-primary">
                          <label>
                          <input type="checkbox" id="select_all_invoice">
                          <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                          </label>
                     </div></th> -->
                       <th>Client Email</th>
                       <th>Date</th>
                       <th>Due Date</th>
                       <th>Invoice Number</th>
                       <th>Reference</th>
                       <!-- <th>Action</th> -->
                     </tr>
                   </thead>
                   <tbody>
                    
                  <?php 
                    if($invoice_details)
                    {  
                      foreach ($invoice_details as $value) { ?>
                      <tr class="client_info" id="client_info">  
                      <!--   <td>
                          <div class="checkbox-fade fade-in-primary">
                             <label>
                             <input type="checkbox" class="invoice_checkbox" data-invoice-id="<?php echo $value['client_id'];?>">
                             <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                             </label>
                          </div>
                       </td> -->
                        <td><a href="<?php echo base_url().'invoice/EditInvoice/'.$value['client_id'] ?>?preview=page"><?php echo $this->Common_mdl->get_field_value('user','crm_email_id','id',$value['client_email']);?></a></td> 
                        <td><?php echo date("M, d, Y", $value['invoice_date']);?></td> 
                        <td><?php if($value['invoice_duedate'] == 'Expired') {
                          echo $value['invoice_duedate'];
                        } else {
                          echo date("M, d, Y", $value['invoice_duedate']);  
                        } ?></td> 
                        <td><?php echo $value['invoice_no'];?></td> 
                        <td><?php echo $value['reference'];?></td> 
                       <!-- <td>
                          <p class="action_01 ticket_list">
                                     
                           
                              <a href="#" onclick="return confirm_delete('<?php echo $value['client_id'];?>');"><i class="fa fa-trash fa-6 delete_invoice" id="delete_invoice" aria-hidden="true" ></i></a>
                              <a href="<?php echo base_url().'invoice/EditInvoice/'.$value['client_id'];?>" class="edit_invoice"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                          </p>
                    <input type="hidden" class="clientID" value="<?php echo $value['client_id'];?>">
                    <input type="hidden" class="clientEmail" value="<?php echo $value['client_email'];?>"> 
                    <input type="hidden" class="invoiceDate" value="<?php echo $value['invoice_date'];?>">
                    <input type="hidden" class="invoiceNo" value="<?php echo $value['invoice_no'];?>"> 


                        </td>-->

                  <div class="modal-alertsuccess alert alert-success" id="delete_crm<?php echo $value['client_id'];?>" style="display:none;">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                  <div class="pop-realted1">
                  <div class="position-alert1">
                  Are you sure want to delete <b><a href="<?php echo base_url().'invoice/DeleteInvoice/'.$value['client_id'];?>"> Yes </a></b> OR <b><a href="#" id="close">No</a></b></div>
                  </div></div>      

                  <div class="modal-alertsuccess alert alert-success" id="delete_all_invoice<?php echo $value['client_id'];?>" style="display:none;">
                       <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                       <div class="pop-realted1">
                       <div class="position-alert1">
                       Are you sure want to delete <b><a href="#" class="delete_yes"> Yes </a></b> OR <b><a href="#" id="close">No</a></b></div>
                       </div></div>
                      </tr>   
                  <?php  } } else { ?> 
                    <!--   <tr class="for-norecords">
                        <td colspan="5"><h5>No records to display</h5></td>
                      </tr> -->
                  <?php } ?>
                     

                   </tbody>
                 </table>
                </div>
              </div>
            </div></div>
          </div>
      
       <div class="space-required">
          <div class="comp_cre"> 
            <span><?php if(isset($client[0]['created_date'])){ echo 'Company Created'.' '.date('F j, Y, g:i',$client[0]['created_date']); } ?></span>
            <span><?php if(isset($user[0]['CreatedTime'])){ echo ';Updated'.' '.date('F j, Y, g:i',$user[0]['CreatedTime']); } ?></span>
   
           <!--  <span><?php if(isset($user[0]['CreatedTime'])){ echo 'Company Created'.' '.date('F j, Y, g:i',$user[0]['CreatedTime']); } ?></span>
            <span><?php if(isset($client[0]['created_date'])){ echo ';Updated'.' '.date('F j, Y, g:i',$client[0]['created_date']); } ?></span> -->
          </div>
      </div>
      
          <!-- task tab tab close -->
        </div>
        <!-- tabcontent -->
      </div>
      <div class="floation_set text-right accordion_ups">
        <input type="hidden" name="user_id" id="user_id" value="<?php if(isset($client[0]['user_id']) && ($client[0]['user_id']!='') ){ echo $client[0]['user_id'];}?>" class="fields">
        <input type="hidden" name="client_id" id="client_id" value="<?php if(isset($user[0]['client_id']) && ($user[0]['client_id']!='') ){ echo $user[0]['client_id'];}?>" class="fields">
        <input type="hidden" id="company_house" name="company_house" value="<?php if(isset($user[0]['company_roles']) && ($user[0]['company_roles']!='') ){ echo $user[0]['company_roles'];}?>" class="fields">
        <!-- <input type="submit" class="add_acc_client" value="Save & Exit" id="submit"/> -->
      </div>
    </div>
    <!-- information-tab -->
<!--   </form> -->
</section>
<!-- client-details-view -->
<?php  
  foreach ($contactRec as $contactRec_key => $contactRec_value) { 
  $id   = $contactRec_value['id']; 
      ?>
<div id="modalcontact<?php echo $id;?>" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="text-align-last: center">contact Delete</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure want to delete?</p>
      </div>
      <div class="modal-footer">
        <a href="#" class="delcontact" data-value="<?php echo $id;?>">Delete</a>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?php  } ?>
<!--modal 2-->
<div class="modal fade all_layout_modals" id="exist_person" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Select Existing Person</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="main_contacts" id="main_contacts" style="display:block">
          <?php 
            echo '<h3>'.$company['company_name'].'</h3>';
            
            $array = array('First','Second','Third','Fourth','Fifth','Sixth','Seventh','Eight','Nineth','Tenth');
            $i=0;
            foreach ($officers as $key => $value) {
            
            
            
            if($value['officer_role'] == 'director' && !isset($value['resigned_on'])) {
            $dateofbirth=(isset($value['date_of_birth']['year'])&&$value['date_of_birth']['year']!='') ? $value['date_of_birth']['year'] : '';
            $month=(isset($value['date_of_birth']['month'])&&$value['date_of_birth']['month']!='') ? date("F", mktime(0, 0, 0, $value['date_of_birth']['month'], 10)) : '';
            $concat=$dateofbirth.' '.$month;
            ?>
          <div class="trading_tables">
            <div class="trading_rate">
              <p>
                <span><strong><?php echo $value['name'];?></strong></span>
                <span><?php echo 'Born'.$dateofbirth;?></span>
                <span class="fields" id="usecontact<?php echo $array[$i];?>"><a href="#" onclick="return getmaincontact(<?php echo "'".$company['company_number']."'".','."'".$value['links']['officer']['appointments']."'".','."'".$array[$i]."'".','."'".$concat."'"?>);">Use as Contact</a></span>
              </p>
            </div>
          </div>
          <?php 
            $i++;
            }
            
            }   
            ?>
        </div>
      </div>
      <!-- modalbody -->
    </div>
  </div>
</div>
<!-- modal-close -->
<!-- modal 2-->
<!-- new person add contact delete modal popup-->
<div id="modalnewperson" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="text-align-last: center">contact Delete</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure want to delete?</p>
      </div>
      <div class="modal-footer">
        <a href="#" class="delnewperson" data-value="">Delete</a>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!--modal close-->


<!-- email all -->
<div class="popup" data-popup="popup-1">
<div class="popup-inner">
<div class="head">
<h3>Email Form</h3>
<p></p>
</div>

 <form action="<?php echo base_url();?>User/sendMail" id="form" method="post" name="form"> 
<label>Your content</label>
<!-- <textarea name="msg" id="msg" placeholder="Type your text here..."></textarea> -->
 <textarea id="editor4" name="editor4" placeholder="Type your text here..." ></textarea>
<div id="error_msg" style="color:red"></div>
<input id="send" name="submit" type="submit" value="Send E-mail">

<div class="feedback-submit  text-right">
  <div class="feed-submit close-feedback1">
  <a data-popup-close="popup-1" href="#">Close</a>
</div>
</div>

<a class="popup-close" data-popup-close="popup-1" href="#">x</a>
</form>
</div>
</div>
<!-- end email all -->
<!-- feedback all -->
<div class="popup" data-popup="popup-2">
   <div class="popup-inner">
      <div class="head">
         <h3>Feedback Form</h3>
         <p></p>
      </div>
      <form action="<?php echo base_url();?>User/feedMail" id="feed_form" method="post" name="feed_form">
         <style>
            input.error {
            border: 1px dotted red;
            }
            label.error{
            width: 100%;
            color: red;
            font-style: italic;
            margin-left: 120px;
            margin-bottom: 5px;
            }
         </style>
         <input type="hidden" name="feed_id" id="feed_id">
         <input type="hidden" name="feed_email" id="feed_email">
         <input type="hidden" name="feed_user" id="feed_user">
         <label><b>Choose Your Name</b></label><br>
         <select name="control" id="control" style=" width: 625px;">
            <option value="">Select</option>
            <?php foreach($feedback as $row1) { ?>
            <option value="<?=$row1->id?>"><?=$row1->username?></option>
            <?php } ?>
         </select>
         <br><br>
         <br>
         <label><b>Your content</b></label>
         <textarea name="feed_msg" id="feed_msg" placeholder="Type your text here..."></textarea>
         <br><br>
         <div class="feedback-submit">
            <div class="feed-submit text-left">
               <input id="feed" name="submit" type="submit" value="submit">
            </div>
            <div class="feed-submit text-right">
               <a data-popup-close="popup-2" href="#">Close</a>
            </div>
         </div>
         <a class="popup-close" data-popup-close="popup-2" href="#">x</a>
      </form>
   </div>
</div>
<!-- end fedd-->
<div class="add_custom_fields_section" style="display: none;">
<?php   echo render_custom_fields_one( 'other',$rel_id); ?>
<?php   echo render_custom_fields_one( 'confirmation',$rel_id);  ?>
<?php   echo render_custom_fields_one( 'accounts',$rel_id);   ?>
<?php   echo render_custom_fields_one( 'personal_tax',$rel_id);  ?>
<?php   echo render_custom_fields_one( 'investigation',$rel_id); ?>
<?php   echo render_custom_fields_one( 'assign_to',$rel_id); ?>
<?php   echo render_custom_fields_one( 'payroll',$rel_id);  ?>
<?php   echo render_custom_fields_one( 'vat',$rel_id);  ?>
<?php   echo render_custom_fields_one( 'management',$rel_id);  ?>
<?php   echo render_custom_fields_one( 'customers',$rel_id);  ?>
</div>
<?php $this->load->view('includes/footer');?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/email.css">
<link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="http:/resources/demos/style.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/client_page.js"></script>
<script type="text/javascript" src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script src="<?php echo base_url();?>assets/js/pcoded.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/moment.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/js/fullcalendar.min.js"></script>
<!-- <script type="text/javascript" src="<?php echo base_url();?>assets/js/calendar.js"></script> -->
<script type="text/javascript">
   CKEDITOR.replace('editor4');
</script>
<script>
  $( function() {
  /*  $( "#datepicker1,#datepicker2,#datepicker3,#datepicker4,#datepicker5,#datepicker6,#datepicker7,#datepicker8,#datepicker9,#datepicker10,#datepicker11,#datepicker12,#datepicker13,#datepicker14,#datepicker15,#datepicker16,#datepicker17,#datepicker18,#datepicker19,#datepicker20,#datepicker21,#datepicker22,#datepicker23,#datepicker24,#datepicker25,#datepicker26,#datepicker27,#datepicker28,#datepicker29,#datepicker30,#datepicker31,#datepicker32,#datepicker33,#datepicker34,#datepicker35,#datepicker36,#datepicker37,#datepicker38,#datepicker39,#letter_sign,#confirmation_next_reminder,#accounts_next_reminder_date,#personal_next_reminder_date,#datepicker,.datepicker,#personal_tax_return_date,#p11d_due_date").datepicker({ dateFormat: "yyyy-mm-dd" });*/
  $(".datepicker").datepicker({dateFormat: 'd MM, y'});
  } );
</script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript">

 $(document).ready(function() {

  var check_value='<?php echo $client[0]['blocked_status']; ?>';
  //alert(check_value);
  if(check_value==1){
    $( ".services" ).each(function(  ) {
    var oop=$(this).val();
  //  alert(oop);
    if(oop=='on'){
      $(".LoadingImage").show();
       $(this).parent('.switching').find('.services').each(function() { 
     //  alert('ok');  
        $(this).prop('checked', $(this).prop(''));        
        });
      $(this).parent('.switching').find('.switchery').each(function() {
        $(this).trigger('click'); 
         $(this).trigger('change');
         $(this).addClass('disabled','disabled');
        });
          setTimeout(function(){  $(".LoadingImage").hide(); }, 3000);     
    }
});
  }

 });

  $(document).ready(function() {

  var check_reminder='<?php echo $client[0]['reminder_block']; ?>';
 // alert(check_reminder);
  if(check_reminder==1){
    $( ".reminder" ).each(function(  ) {
    var oop=$(this).val();
    //alert(oop);
    if(oop=='on'){
      $(".LoadingImage").show();
       $(this).parent('.splwitch').find('.reminder').each(function() {   
        $(this).prop('checked', $(this).prop(''));        
        });
      $(this).parent('.splwitch').find('.switchery').each(function() {
        $(this).trigger('click'); 
         $(this).addClass("disabled");
         $(this).trigger('change');
         $(this).attr('disabled', 'disabled'); 
        }); 
setTimeout(function(){  $(".LoadingImage").hide(); }, 3000);     
    }
});
  }

 });




 $( document ).ready(function() {



          CKEDITOR.editorConfig = function (config) {
          config.language = 'es';
          config.uiColor = '#fff';
          config.height = 300;
          config.toolbarCanCollapse = true;
          config.toolbarLocation = 'bottom';
          };

             CKEDITOR.replace( 'editor21', {
            toolbarLocation: 'bottom',
            // Remove some plugins that would conflict with the bottom
            // toolbar position.
            removePlugins: 'elementspath,resize',
            toolbarGroups: [
        {"name":"links","groups":["links"]}
      ],
          } ); 


                  CKEDITOR.replace( 'editor51', {
            toolbarLocation: 'bottom',
            // Remove some plugins that would conflict with the bottom
            // toolbar position.
            removePlugins: 'elementspath,resize',
            toolbarGroups: [
        {"name":"links","groups":["links"]}
      ],
          } ); 

                   CKEDITOR.replace( 'editor61', {
            toolbarLocation: 'bottom',
            // Remove some plugins that would conflict with the bottom
            // toolbar position.
            removePlugins: 'elementspath,resize',
            toolbarGroups: [
        {"name":"links","groups":["links"]}
      ],
          } ); 



             CKEDITOR.replace( 'editor24', {
            toolbarLocation: 'bottom',
            // Remove some plugins that would conflict with the bottom
            // toolbar position.
            removePlugins: 'elementspath,resize',
            toolbarGroups: [
        {"name":"links","groups":["links"]}
      ],
          } );   

               CKEDITOR.replace( 'editor22', {
            toolbarLocation: 'bottom',
            // Remove some plugins that would conflict with the bottom
            // toolbar position.
            removePlugins: 'elementspath,resize',
            toolbarGroups: [
        {"name":"links","groups":["links"]}
      ],
          } );  

           CKEDITOR.replace( 'editor23', {
            toolbarLocation: 'bottom',
            // Remove some plugins that would conflict with the bottom
            // toolbar position.
            removePlugins: 'elementspath,resize',
            toolbarGroups: [
        {"name":"links","groups":["links"]}
      ],
          } ); 

           CKEDITOR.replace( 'editor2', {
            toolbarLocation: 'bottom',
            // Remove some plugins that would conflict with the bottom
            // toolbar position.
            removePlugins: 'elementspath,resize',
            toolbarGroups: [
        {"name":"links","groups":["links"]}
      ],
          } );  
          
           CKEDITOR.replace( 'editor1', {
            toolbarLocation: 'bottom',
            // Remove some plugins that would conflict with the bottom
            // toolbar position.
            removePlugins: 'elementspath,resize',
            toolbarGroups: [
        {"name":"links","groups":["links"]}
      ],
          } ); 

          CKEDITOR.replace( 'editor2', {
            toolbarLocation: 'bottom',
            // Remove some plugins that would conflict with the bottom
            // toolbar position.
            removePlugins: 'elementspath,resize',
            toolbarGroups: [
        {"name":"links","groups":["links"]}
      ],
          } );  

       

        

          CKEDITOR.replace( 'editor23', {
            toolbarLocation: 'bottom',
            // Remove some plugins that would conflict with the bottom
            // toolbar position.
            removePlugins: 'elementspath,resize',
            toolbarGroups: [
        {"name":"links","groups":["links"]}
      ],
          } ); 

               
            

        } );


  $(document).ready(function(){
     function arr(name){
      var values = $("input[name='"+name+"[]']").map(function(){return $(this).val();}).get();
      return values;
     }
  
  
    function contact_add(id){
   var incre=$('#incre').val();
   //alert(incre);
   
          //var fields = $( "#contact_info" ).serializeArray();
  
  //alert(getFormData('contact_info'));
  
     //$(".contact_add").click(function(){
      //alert('contact_add');
  
        var data={};
  
        var title=[];
  $("select[name=title]").each(function(){
      title.push($(this).val());
  }); 
  
        //data['title'] = arr('title'); 
        data['title'] = title; 
  
        //$("input[name='title[]']").map(function(){return $(this).val();}).get();
        data['first_name'] = arr('first_name');
        data['middle_name'] = arr('middle_name');
        data['last_name'] = arr('last_name');
        data['surname'] = arr('surname');
        data['preferred_name'] = arr('preferred_name');
        data['mobile'] = arr('mobile');
        data['main_email'] = arr('main_email');
        //data['work_email'] = arr('work_email');
        data['nationality'] = arr('nationality');
        data['psc'] = arr('psc');
        //data['shareholder'] = $("input[name='shareholder[]']:selec"). val();
        var usertype=[];
  $("select[name=shareholder]").each(function(){
      usertype.push($(this).val());
  }); 
  
      var marital=[];
  $("select[name=marital_status]").each(function(){
      marital.push($(this).val());
  }); 
  
     var contacttype=[];
  $("select[name=contact_type]").each(function(){
      contacttype.push($(this).val());
  }); 
        data['shareholder'] = usertype;
        data['ni_number'] = arr('ni_number');
        data['content_type'] = arr('content_type');
        data['address_line1'] = arr('address_line1');
        data['address_line2'] = arr('address_line2');
        data['town_city'] = arr('town_city');
        data['post_code'] = arr('post_code');
        for (var i = 1; i <=incre; i++) {
     
        data['landline'+i] = arr('landline'+i);
        data['work_email'+i] = arr('work_email'+i);
    }
        data['date_of_birth'] = arr('date_of_birth');
        data['nature_of_control'] = arr('nature_of_control');
        data['marital_status'] = marital;
        data['contact_type'] = contacttype;
        data['utr_number'] = arr('utr_number');
        data['occupation'] = arr('occupation');
        data['appointed_on'] = arr('appointed_on');
        data['country_of_residence'] = arr('country_of_residence');
        data['make_primary'] = arr('make_primary');
        data['other_custom'] = arr('other_custom');
        data['client_id'] = id;
  
        var cnt = $("#append_cnt").val();
  
          if(cnt==''){
              cnt = 1;
          }else{
              cnt = parseInt(cnt)+1;
          }
          
          $("#append_cnt").val(cnt);
  
  
  
      data['cnt'] =cnt;
      $.ajax({
      url: '<?php echo base_url();?>client/update_client_contacts/',
      type : 'POST',
      data : data,
      success: function(data) {
  //alert('ggg');
         //$(".LoadingImage").hide();
            // location.reload();
                                             add_assignto(id);
  
  
      },
      });
     //});
  }
  
  function add_assignto(clientId)
  {
   var data = {};
  
  
          var team=[];
  $("select[name=team]").each(function(){
      team.push($(this).val());
  }); 
   data['team'] = team;
  
      data['allocation_holder'] = arr('allocation_holder');
      data['clientId'] = clientId;
      $.ajax({
      url: '<?php echo base_url();?>client/add_assignto/',
      type: "POST",
      data: data,
      success: function(data)  
      {
            add_responsibleuser(clientId);
      }
    });
  }
  
  function add_responsibleuser(clientId)
  {
   var data = {};
   /*data['assign_managed'] = arr('assign_managed');
      data['manager_reviewer'] = arr('manager_reviewer');
  */
          var assign_managed=[];
  $("select[name=assign_managed]").each(function(){
      assign_managed.push($(this).val());
  }); 
  
      var manager_reviewer=[];
  $("select[name=manager_reviewer]").each(function(){
      manager_reviewer.push($(this).val());
  }); 
  
   data['assign_managed'] = assign_managed;
      data['manager_reviewer'] = manager_reviewer;
  
      data['clientId'] = clientId;
      $.ajax({
      url: '<?php echo base_url();?>client/add_responsibleuser/',
      type: "POST",
      data: data,
      success: function(data)  
      {
         add_assigntodepart(clientId);
          //location.reload();  
         // window.location.href="<?php echo base_url().'user'?>"
      }
    });
  }
  
  function add_assigntodepart(clientId)
  {
   var data = {};
  
  
      var depart=[];
  $("select[name=depart]").each(function(){
      depart.push($(this).val());
  }); 
   data['depart'] = depart;
      data['allocation_holder'] = arr('allocation_holder_dept');
      data['clientId'] = clientId;
      $.ajax({
      url: '<?php echo base_url();?>client/add_assigntodepart/',
      type: "POST",
      data: data,
      success: function(data)  
      {
            add_responsiblemember(clientId);
      }
    });
  }
  
  function add_responsiblemember(clientId)
  {
   var data = {};
   /*data['assign_managed'] = arr('assign_managed');
      data['manager_reviewer'] = arr('manager_reviewer');
  */
          /*var assign_managed_member=[];
  $("select[name=assign_managed_member]").each(function(){
      assign_managed_member.push($(this).val());
  });*/ 
  
      var manager_reviewer_member=[];
  $("select[name=manager_reviewer_member]").each(function(){
      manager_reviewer_member.push($(this).val());
  }); 
  
   //data['assign_managed_member'] = assign_managed_member;
   data['assign_managed_member'] = arr('assign_managed_member');
      data['manager_reviewer_member'] = manager_reviewer_member;
  
      data['clientId'] = clientId;
      $.ajax({
      url: '<?php echo base_url();?>client/add_responsiblemember/',
      type: "POST",
      data: data,
      success: function(data)  
      {
         
          //location.reload();  
          window.location.href="<?php echo base_url().'user'?>";
      }
    });
  }
  
  $("#insert_form").validate({
  
         ignore: false,
     // errorClass: "error text-warning",
      //validClass: "success text-success",
      /*highlight: function (element, errorClass) {
          //alert('em');
         // $(element).fadeOut(function () {
             // $(element).fadeIn();
          //});
      },*/
                          rules: {
                          company_name: {required: true},
                         // company_number: {required: true},
                  
                          },
                          errorElement: "span", 
                          errorClass: "field-error",                             
                           messages: {
                            company_name: "Give company name",
                            //company_number: "Give company number",
                            
                           },
  
                          
                          submitHandler: function(form) {
                              var formData = new FormData($("#insert_form")[0]);
  
                              $(".LoadingImage").show();
                              $.ajax({
                                  url: '<?php echo base_url();?>client/updates_client/',
                                  dataType : 'json',
                                  type : 'POST',
                                  data : formData,
                                  contentType : false,
                                  processData : false,
                                  success: function(data) {
                                    var Contact_id = $('#user_id').val();
  
                                    contact_add(Contact_id);
                                    //add_assignto(Contact_id);
                                      if(data == '1'){
                                         // $('#insert_form')[0].reset();
                                          $('.alert-success').show();
                                          $('.alert-danger').hide();
                                      }
                                      else if(data == '0'){
                                         // alert('failed');
                                          $('.alert-danger').show();
                                          $('.alert-success').hide();
                                      }
                                  $(".LoadingImage").hide();
                                  },
                                  error: function() {
                                       $(".LoadingImage").hide();
                                      alert('faileddd');}
                              });
  
                              return false;
                          } ,
                           /* invalidHandler: function(e,validator) {
          for (var i=0;i<validator.errorList.length;i++){   
              $(validator.errorList[i].element).parents('.panel-collapse.collapse').collapse('show');
          }
      }*/
       invalidHandler: function(e, validator) {
             if(validator.errorList.length)
          $('#tabs a[href="#' + jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
  
          }
  
                      });
  
  });
</script>

<script type="text/javascript">
   $(function() {
   //----- OPEN
   $('[data-popup-open]').on('click', function(e)  {
   var targeted_popup_class = jQuery(this).attr('data-popup-open');
   $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
   e.preventDefault();
   });
   //----- CLOSE
   $('[data-popup-close]').on('click', function(e)  {
   var targeted_popup_class = jQuery(this).attr('data-popup-close');
   $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
   e.preventDefault();
   });
   });
</script>
<script type="text/javascript">
   $('#feed').click(function(){
   
   $("#feed_form").validate({
     rules: {
         control: {
             required: true,
             
         },
         feed_msg: {
             required: true,
            
         },
     
      
     },
     messages: {
         control: "Please select username",
        
         feed_msg: {
             required: "Please Enter your message",
         },
              
          },
     });
   });
   
</script>

<script type="text/javascript">
  $('#send').click(function(){
   var value = CKEDITOR.instances['editor4'].getData(); 
   if(value=='')
   {
   document.getElementById("error_msg").innerHTML = "Enter Message";
   return false;
   }
   else
   {
   return true;
   }
   });



</script>


<script type="text/javascript">
$(document).ready(function(){
   $('input.js-small.f-right').each(function(){
      $(this).addClass('rsptrspt');
      $(this).trigger('change');
     // $(this).removeClass('rsptrspt');
   // console.log('trigger change');
   });
    $('#myReminders').modal('hide');
});

  $(document).on('change','.js-small',function(){
   if ($(this).hasClass('rsptrspt')){
      console.log('hd');
    }
    else{
      console.log('not');
    } 
    
   //$(this).closest('div').find('.field').prop('disabled', !$(this).is(':checked'));
   
   var check= $(this).data('id');
 
   var id=$(this).attr('id');
   //alert(id);
   //  alert(check);
   //alert($(this).is(':checked'));
   //alert($(this).val());
  

      /** start 27-08-2018 **/
           var check1=$(this).attr('id');
     /** 10-09-2018 **/
      /** 10-09-2018 **/
     
      if(check=="show_login")
     {
      if($(this).is(':checked'))$(".show_login").css("display","block");
      else $(".show_login").css("display","none");
     }//ajith
     
                    // task investigation 
     if($(this).is(':checked')&&(check1=='investigation_create_task_reminder')){
       $('.investigation_add_custom_reminder_label').css('display','');
     }
     else if(check1=='investigation_create_task_reminder')
     {
       $('.investigation_add_custom_reminder_label').css('display','none');
     }
               // cissub 
     if($(this).is(':checked')&&(check1=='cissub_create_task_reminder')){
       $('.cissub_add_custom_reminder_label').css('display','');
     }
     else if(check1=='cissub_create_task_reminder')
     {
       $('.cissub_add_custom_reminder_label').css('display','none');
     }
          // register 
     if($(this).is(':checked')&&(check1=='registered_create_task_reminder')){
       $('.registered_add_custom_reminder_label').css('display','');
     }
     else if(check1=='registered_create_task_reminder')
     {
       $('.registered_add_custom_reminder_label').css('display','none');
     }
     // bookkeep 
     if($(this).is(':checked')&&(check1=='bookkeep_create_task_reminder')){
       $('.bookkeep_add_custom_reminder_label').css('display','');
     }
     else if(check1=='bookkeep_create_task_reminder')
     {
       $('.bookkeep_add_custom_reminder_label').css('display','none');
     }
     // managemnt 
     if($(this).is(':checked')&&(check1=='manage_create_task_reminder')){
       $('.manage_add_custom_reminder_label').css('display','');
     }
     else if(check1=='manage_create_task_reminder')
     {
       $('.manage_add_custom_reminder_label').css('display','none');
     }
     // p11d 
     if($(this).is(':checked')&&(check1=='p11d_create_task_reminder')){
       $('.p11d_add_custom_reminder_label').css('display','');
     }
     else if(check1=='p11d_create_task_reminder')
     {
       $('.p11d_add_custom_reminder_label').css('display','none');
     }
      // cis
     if($(this).is(':checked')&&(check1=='cis_create_task_reminder')){
       $('.cis_add_custom_reminder_label').css('display','');
     }
     else if(check1=='cis_create_task_reminder')
     {
       $('.cis_add_custom_reminder_label').css('display','none');
     }
     // workplace pension ae
     if($(this).is(':checked')&&(check1=='pension_create_task_reminder')){
       $('.pension_create_task_reminder_label').css('display','');
     }
     else if(check1=='pension_create_task_reminder')
     {
       $('.pension_create_task_reminder_label').css('display','none');
     }
      // confirmation statement
     if($(this).is(':checked')&&(check1=='create_task_reminder')){
       $('.custom_remain').css('display','');
     }
     else if(check1=='create_task_reminder')
     {
       $('.custom_remain').css('display','none');
     }
      // PayRole
     if($(this).is(':checked')&&(check1=='payroll_create_task_reminder')){
       $('.payroll_add_custom_reminder_label').css('display','');
     }
     else if(check1=='payroll_create_task_reminder')
     {
       $('.payroll_add_custom_reminder_label').css('display','none');
     }
     // vat
     if($(this).is(':checked')&&(check1=='vat_create_task_reminder')){
       $('.vat_add_custom_reminder_label').css('display','');
     }
     else if(check1=='vat_create_task_reminder')
     {
       $('.vat_add_custom_reminder_label').css('display','none');
     }
     // personal tax
     if($(this).is(':checked')&&(check1=='personal_task_reminder')){
       $('.personal_custom_reminder_label').css('display','');
     }
     else if(check1=='personal_task_reminder')
     {
       $('.personal_custom_reminder_label').css('display','none');
     }
     //accounts
     if($(this).is(':checked')&&(check1=='accounts_create_task_reminder')){
       $('.accounts_custom_reminder_label').css('display','');
     }
     else if(check1=='accounts_create_task_reminder')
     {
       $('.accounts_custom_reminder_label').css('display','none');
     }
            /** 07-09-2018 **/
      if($(this).is(':checked')&&(check1=='insurance_create_task_reminder')){
       $('.insurance_add_custom_reminder_label').css('display','');
     }
     else if(check1=='insurance_create_task_reminder')
     {
       $('.insurance_add_custom_reminder_label').css('display','none');
     }
     // company tax return
     if($(this).is(':checked')&&(check1=='company_create_task_reminder')){
       $('.company_custom_reminder_label').css('display','');
     }
     else if(check1=='company_create_task_reminder')
     {
       $('.company_custom_reminder_label').css('display','none');
     }
     /** en dof 07-09-2018 **/
      if($(this).is(':checked')&&(check1=='client_id_verified')){ // amlcheck checkbox change enabled 
        $('.client_id_verified_data').css('display','');
         var person=$('#person').val();
        //alert(person);
        if(person!=''){
            result = person.toString().split(',');
            if($.inArray("Other_Custom",result)!=-1)
            {
            $('.for_other_custom_choose').css('display','');
            }
            else{
            $('.for_other_custom_choose').css('display','none');
            }
         }
      }
      else if(check1=='client_id_verified')
      {
         $('.client_id_verified_data').css('display','none');
         $('.for_other_custom_choose').css('display','none'); // for for other custom textbox hide
      }
    /** end of 27-08-2018 **/
   if($(this).is(':checked')&&(check=='cons')){
   //alert('confirmation statement');
   var cnt_of_other = $("#cnt_of_other").val();
   var new_cnt = parseInt(cnt_of_other)+1;
   $("#cnt_of_other").val(new_cnt);
   $("#old_cnt").hide();
   $("#other_tab_cnt").html(new_cnt);
    
   $(".li_append_acc").html(' <li class="nav-item it7 hide" value="2"  style="display: none;"><a class="nav-link" data-toggle="tab" href="#accounts"><strong>'+cnt_of_other+'</strong>.Accounts</a></li> ');
   
   localStorage.setItem("service", "confirmation");
         
      var data=$("#confirmation-statement").html();      
      $(".body-popup-content").html('');
      $(".body-popup-content").append(data);   
       if ($(this).hasClass('rsptrspt')){}else{     
      checking();
      }
   
   
   
   
   
    $(".it7").attr("style", "display:block");
    $(".it7").removeClass("hide");
   // $(".it7 a").click();
   } else if(check=='cons') {
   //alert('confirmation statement1');
   var cnt_of_other = $("#cnt_of_other").val();
   var new_cnt = parseInt(cnt_of_other)-1;
   $("#cnt_of_other").val(new_cnt);
   $("#old_cnt").hide();
   $("#other_tab_cnt").html(new_cnt);
   
    $(".it7").attr("style", "display:none");
    $(".it7").addClass("hide");
    $(".li_append_acc").html('');
   }
   
   
   if($(this).is(':checked')&&(check=='accounts')){
   
   var cnt_of_other = $("#cnt_of_other").val();
   var new_cnt = parseInt(cnt_of_other)+1;
   $("#cnt_of_other").val(new_cnt);
   $("#old_cnt").hide();
   $("#other_tab_cnt").html(new_cnt);
   $('.accounts_tab_section').css('display',''); // 10-09-2018
    
   $(".li_append_acc").html(' <li class="nav-item it6 hide" value="2"  style="display: none;"><a class="nav-link" data-toggle="tab" href="#accounts"><strong>'+cnt_of_other+'</strong>.Accounts</a></li> ');
   
    localStorage.setItem("service", "accounts");
        
      var data=$("#accounts").html();      
      $(".body-popup-content").html('');
      $(".body-popup-content").append(data);
       if ($(this).hasClass('rsptrspt')){}else{
     checking();
     }
    $(".it6").attr("style", "display:block");
    $(".it6").removeClass("hide");
   // $(".it6 a").click();
   } else if(check=='accounts') {
   
   var cnt_of_other = $("#cnt_of_other").val();
   var new_cnt = parseInt(cnt_of_other)-1;
   $("#cnt_of_other").val(new_cnt);
   $("#old_cnt").hide();
   $("#other_tab_cnt").html(new_cnt);
    $('.accounts_tab_section').css('display','none'); // 10-09-2018
      if($(".companytax").css('display') === 'none') { // 10-09-2018
          $(".it6").attr("style", "display:none");
          $(".it6").addClass("hide");
          $(".li_append_acc").html('');
      }
      else
      {
          $(".it6").attr("style", "display:block");
          $(".it6").removeClass("hide");
      
      }
   
    // $(".it6").attr("style", "display:none");
    // $(".it6").addClass("hide");
    // $(".li_append_acc").html('');
   }
   
   if($(this).is(':checked')&&(check=='payroll')){
   
   var cnt_of_other = $("#cnt_of_other").val();
   var new_cnt = parseInt(cnt_of_other)+1;
   $("#cnt_of_other").val(new_cnt);
   $("#old_cnt").hide();
   $("#other_tab_cnt").html(new_cnt);
   $('.for_payroll_section').css('display',''); // 10-09-2018
   
   $(".li_append_pay").html(' <li class="nav-item it11 hide" value="2" style="display: none;"><a class="nav-link" data-toggle="tab" href="#payroll"><strong>'+cnt_of_other+'</strong>.Payroll</a></li> ');
   
    localStorage.setItem("service", "payroll");
         
      var data=$("#payroll").html();      
      $(".body-popup-content").html('');
      $(".body-popup-content").append(data);
        if ($(this).hasClass('rsptrspt')){}else{
        checking();
     }
    $(".it11").attr("style", "display:block");
    $(".it11").removeClass("hide");
   // $(".it11 a").click();
   } else if(check=='payroll') {
   
   var cnt_of_other = $("#cnt_of_other").val();
   var new_cnt = parseInt(cnt_of_other)-1;
   $("#cnt_of_other").val(new_cnt);
   $("#old_cnt").hide();
   $("#other_tab_cnt").html(new_cnt);
     $('.for_payroll_section').css('display','none'); // 10-09-2018
      if($(".worktab").css('css') === 'none' && ($('.p11dtab').css('display')==='none') && ($(".contratab").css('display')==='none')) { // 10-09-2018
             $(".it11").attr("style", "display:none");
             $(".it11").addClass("hide");
             $(".li_append_pay").html('');
      }
      else
      {

      }
    // $(".it11").attr("style", "display:none");
    // $(".it11").addClass("hide");
    // $(".li_append_pay").html('');
   }
   
   if($(this).is(':checked')&&(check=='vat')){
   
   var cnt_of_other = $("#cnt_of_other").val();
   var new_cnt = parseInt(cnt_of_other)+1;
   $("#cnt_of_other").val(new_cnt);
   $("#old_cnt").hide();
   $("#other_tab_cnt").html(new_cnt);
   
   $(".li_append_vat").html('<li class="nav-item it8 hide" value="2" style="display: none;"><a class="nav-link" data-toggle="tab" href="#vat-Returns" ><strong>'+cnt_of_other+'</strong>.VAT Returns</a></li> ');
   
   localStorage.setItem("service", "vat");
    
      var data=$("#vat-Returns").html();      
      $(".body-popup-content").html('');
      $(".body-popup-content").append(data);
        if ($(this).hasClass('rsptrspt')){}else{
       checking();
    }
    $(".it8").attr("style", "display:block");
    $(".it8").removeClass("hide");
   // $(".it8 a").click();
   } else if(check=='vat') {
   var cnt_of_other = $("#cnt_of_other").val();
   var new_cnt = parseInt(cnt_of_other)-1;
   $("#cnt_of_other").val(new_cnt);
   $("#old_cnt").hide();
   $("#other_tab_cnt").html(new_cnt);
    $(".it8").attr("style", "display:none");
    $(".it8").addClass("hide");
    $(".li_append_vat").html('');
   }
   
   
   if($(this).is(':checked')&&(check=='personaltax')){
   
   var cnt_of_other = $("#cnt_of_other").val();
   var new_cnt = parseInt(cnt_of_other)+1;
   $("#cnt_of_other").val(new_cnt);
   $("#old_cnt").hide();
   $("#other_tab_cnt").html(new_cnt);
   
   $(".li_append_per").html(' <li class="nav-item it12 hide" value="2" style="display: none;"><a class="nav-link" data-toggle="tab" href="#personal-tax-returns"><strong>'+cnt_of_other+'</strong>.Personal Tax Return</a></li> ');
   
   
   // if(localStorage.getItem("service")!='accounts'){
   //       localStorage.clear();
      localStorage.setItem("service", "personaltax");         
      var data=$("#personal-tax-returns").html();      
      $(".body-popup-content").html('');
      $(".body-popup-content").append(data);
   // }
     if ($(this).hasClass('rsptrspt')){}else{
       checking();
    }
    $(".it12").attr("style", "display:block");
    $(".it12").removeClass("hide");
   // $(".it12 a").click();
   } else if(check=='personaltax') {
   
   var cnt_of_other = $("#cnt_of_other").val();
   var new_cnt = parseInt(cnt_of_other)-1;
   $("#cnt_of_other").val(new_cnt);
   $("#old_cnt").hide();
   $("#other_tab_cnt").html(new_cnt);
   
    $(".it12").attr("style", "display:none");
    $(".it12").addClass("hide");
    $(".li_append_per").html('');
   }
   
   if($(this).is(':checked')&&(check=='management')){
   
   var cnt_of_other = $("#cnt_of_other").val();
   var new_cnt = parseInt(cnt_of_other)+1;
   $("#cnt_of_other").val(new_cnt);
   $("#old_cnt").hide();
   $("#other_tab_cnt").html(new_cnt);
   $('.for_management_account').css('display','');

   $(".li_append_man").html('<li class="nav-item it13 hide" value="2" style="display: none;"><a class="nav-link" data-toggle="tab" href="#management-account"><strong>'+cnt_of_other+'</strong>.Management Accounts</a></li> ');
   
   localStorage.setItem("service", "management");
   
      var data=$("#management-account").html();      
      $(".body-popup-content").html('');
      $(".body-popup-content").append(data);
        if ($(this).hasClass('rsptrspt')){}else{
        checking();
     }
    $(".it13").attr("style", "display:block");
    $(".it13").removeClass("hide");
   //    $(".it13 a").click();
   } else if(check=='management') {
   var cnt_of_other = $("#cnt_of_other").val();
   var new_cnt = parseInt(cnt_of_other)-1;
   $("#cnt_of_other").val(new_cnt);

   $("#old_cnt").hide();
   $("#other_tab_cnt").html(new_cnt);
     $('.for_management_account').css('display','none');

      if ($(".inbook").css('css') === 'none') { // 10-09-2018
            $(".it13").attr("style", "display:none");
          $(".it13").addClass("hide");
          $(".li_append_man").html('');
      }
   
    // $(".it13").attr("style", "display:none");
    // $(".it13").addClass("hide");
    // $(".li_append_man").html('');
   }
   
   if($(this).is(':checked')&&(check=='investgate')){
   
   var cnt_of_other = $("#cnt_of_other").val();
   var new_cnt = parseInt(cnt_of_other)+1;
   $("#cnt_of_other").val(new_cnt);
   $("#old_cnt").hide();
   $("#other_tab_cnt").html(new_cnt);

   
   $('.li_append_inv').html('<li class="nav-item it14 hide" value="2" style="display: none;"><a class="nav-link" data-toggle="tab" href="#investigation-insurance"><strong>'+cnt_of_other+'</strong>.Investigation Insurance</a></li>');
   
    localStorage.setItem("service", "investgate");
     
      var data=$("#investigation-insurance").html();      
      $(".body-popup-content").html('');
      $(".body-popup-content").append(data);
        if ($(this).hasClass('rsptrspt')){}else{
       checking();
    }
    $(".it14").attr("style", "display:block");
    $(".it14").removeClass("hide");
   // $(".it14 a").click();
   } else if(check=='investgate') {
   
   var cnt_of_other = $("#cnt_of_other").val();
   var new_cnt = parseInt(cnt_of_other)-1;
   $("#cnt_of_other").val(new_cnt);
   $("#old_cnt").hide();
   $("#other_tab_cnt").html(new_cnt);
      if($('.registeredtab').css('display')==='none' && $('.taxadvicetab').css('display')==='none')
      {
       $(".it14").attr("style", "display:none");
       $(".it14").addClass("hide");
       $('.li_append_inv').html('');
      }
   }
   
   if($(this).is(':checked')&&(check=='preacc')){
    $("#enable_preacc").attr("style", "display:block");
    /** 30-08-2018 **/
       var clickCheckbox = document.querySelector('#chase_for_info');
       if(clickCheckbox.checked) // true
       {
         $(".for_other_notes").css('display','');
       }
    /** end of 30-08-2018 **/
   } else if(check=='preacc') {
    $("#enable_preacc").attr("style", "display:none");
    /** 30-08-2018 **/
    $(".for_other_notes").css('display','none');
    /** end of 30-08-2018 **/
   }
    /** for other tab hide and show 30-08-2018 **/
   if($(this).is(':checked')&&(check=='chase_for_info')){
      $(".for_other_notes").css('display','');
   } else if(check=='chase_for_info') {
      $(".for_other_notes").css('display','none');
   }
   /** end of 30-08-2018 **/
   //company tax section enable/disable
   if($(this).is(':checked')&&(check=='companytax')){
   
   var cnt_of_other = $("#cnt_of_other").val();
   var new_cnt = parseInt(cnt_of_other)-1;
   $("#cnt_of_other").val(new_cnt);
   $("#old_cnt").hide();
   $("#other_tab_cnt").html(new_cnt);
    $(".li_append_acc").html(' <li class="nav-item it6 hide" value="2"  style="display: none;"><a class="nav-link" data-toggle="tab" href="#accounts"><strong>'+cnt_of_other+'</strong>.Accounts</a></li> ');
   
   
     if(localStorage.getItem("service")!='accounts'){
         localStorage.clear();
         localStorage.setItem("service", "companytax");
         $(".companytax").attr("style", "display:block");
         var data=$("#accounts").html();      
         $(".body-popup-content").html('');
         $(".body-popup-content").append(data);
   }
   else{
         localStorage.clear();
         localStorage.setItem("service", "companytax");
         $(".companytax").attr("style", "display:block");
   }
     if ($(this).hasClass('rsptrspt')){}else{
       checking();
    }
   $(".it6").attr("style", "display:block");
   $(".it6").removeClass("hide");
   // $(".it6 a").click();
   } else if(check=='companytax') {
   
   var cnt_of_other = $("#cnt_of_other").val();
   var new_cnt = parseInt(cnt_of_other)-1;
   $("#cnt_of_other").val(new_cnt);
   $("#old_cnt").hide();
   $("#other_tab_cnt").html(new_cnt);
   
    $(".companytax").attr("style", "display:none");


     if($(this).is(':checked')&&(check=='accounts')){
     }else{
       // alert('**********************');
       $(".it6").attr("style", "display:none");
     }


     if ($(".accounts_tab_section").css('display') === 'hidden') { // 10-09-2018
      $(".it6").addClass("hide");
      $(".li_append_acc").html('');
    }
   
    // $(".it6").addClass("hide");
    // $(".li_append_acc").html('');
   
   }
   
   //workplace pension section enable/disable
   if($(this).is(':checked')&&(check=='workplace')){
   
   var cnt_of_other = $("#cnt_of_other").val();
   var new_cnt = parseInt(cnt_of_other)+1;
   $("#cnt_of_other").val(new_cnt);
   $("#old_cnt").hide();
   $("#other_tab_cnt").html(new_cnt);
   
   $(".li_append_pay").html(' <li class="nav-item it11 hide" value="2" style="display: none;"><a class="nav-link" data-toggle="tab" href="#payroll"><strong>'+cnt_of_other+'</strong>.Payroll</a></li> ');
      if(localStorage.getItem("service")!='payroll'){
            localStorage.clear();
            localStorage.setItem("service", "workplace");
            $(".worktab").attr("style", "display:block");
            var data=$("#payroll").html();      
            $(".body-popup-content").html('');
            $(".body-popup-content").append(data);
      }
      else{
           localStorage.setItem("service", "workplace");
            $(".worktab").attr("style", "display:block");
            var data=$("#payroll").html();      
            $(".body-popup-content").html('');
            $(".body-popup-content").append(data);
      }

  if ($(this).hasClass('rsptrspt')){}else{
       checking();
    }
   $(".it11").attr("style", "display:block");
   $(".it11").removeClass("hide");
   // $(".it11 a").click();
   } else if(check=='workplace') {
   
   var cnt_of_other = $("#cnt_of_other").val();
   var new_cnt = parseInt(cnt_of_other)-1;
   $("#cnt_of_other").val(new_cnt);
   $("#old_cnt").hide();
   $("#other_tab_cnt").html(new_cnt);
     
   $(".worktab").attr("style", "display:none");

   if($(this).is(':checked')&&(check=='payroll')){

    }else{
       $(".it11").attr("style", "display:none");
    }



    if (($(".for_payroll_section").css('display') === 'none') && ($(".contratab").css('display')==='none') && ($('.p11dtab').css('display')==='none')) { // 10-09-2018
      $(".it11").addClass("hide");
        $(".li_append_pay").html('');
    }
   
   // $(".it11").addClass("hide");
   // $(".li_append_pay").html('');
   
   }
   
   //cis/subcis  section enable/disable
   if($(this).is(':checked')&&(check=='cis' || check=='cissub')){
   
   var cnt_of_other = $("#cnt_of_other").val();
   var new_cnt = parseInt(cnt_of_other)+1;
   $("#cnt_of_other").val(new_cnt);
   $("#old_cnt").hide();
   $("#other_tab_cnt").html(new_cnt);
   
   $(".li_append_pay").html(' <li class="nav-item it11 hide" value="2" style="display: none;"><a class="nav-link" data-toggle="tab" href="#payroll"><strong>'+cnt_of_other+'</strong>.Payroll</a></li> ');
   
   if(localStorage.getItem("service")!='payroll'){
         localStorage.clear();
         localStorage.setItem("service", "cis");
         $(".contratab").attr("style", "display:block");          
         var data=$("#payroll").html();      
         $(".body-popup-content").html('');
         $(".body-popup-content").append(data);
   }
     if ($(this).hasClass('rsptrspt')){}else{
       checking();
    }
   $(".it11").attr("style", "display:block");
   $(".it11").removeClass("hide");
   // $(".it11 a").click();
   } else if(check=='cis' || check=='cissub') {
   
   var cnt_of_other = $("#cnt_of_other").val();
   var new_cnt = parseInt(cnt_of_other)-1;
   $("#cnt_of_other").val(new_cnt);
   $("#old_cnt").hide();
   $("#other_tab_cnt").html(new_cnt);
   
    $(".contratab").attr("style", "display:none");

     if($(this).is(':checked')&&(check=='payroll')){

    }else{
       $(".it11").attr("style", "display:none");
    }



   if (($(".for_payroll_section").css('display') === 'none') && ($(".worktab").css('display')==='none') && ($('.p11dtab').css('display')==='none')) { // 10-09-2018
      $(".it11").addClass("hide");
        $(".li_append_pay").html('');
    }
   
   // $(".it11").addClass("hide");
   // $(".li_append_pay").html('');
   }
   
   //p11d  section enable/disable
   if($(this).is(':checked')&&(check=='p11d')){
   
   var cnt_of_other = $("#cnt_of_other").val();
   var new_cnt = parseInt(cnt_of_other)+1;
   $("#cnt_of_other").val(new_cnt);
   $("#old_cnt").hide();
   $("#other_tab_cnt").html(new_cnt);
   
   $(".li_append_pay").html(' <li class="nav-item it11 hide" value="2" style="display: none;"><a class="nav-link" data-toggle="tab" href="#payroll"><strong>'+cnt_of_other+'</strong>.Payroll</a></li> '); 
   if(localStorage.getItem("service")!='payroll'){
      localStorage.clear();     
      localStorage.setItem("service", "p11d");
      $(".p11dtab").attr("style", "display:block");           
      var data=$("#payroll").html();      
      $(".body-popup-content").html('');
      $(".body-popup-content").append(data);
   }
   if ($(this).hasClass('rsptrspt')){}else{
      checking();
   }
    $(".it11").attr("style", "display:block");
    $(".it11").removeClass("hide");
   // $(".it11 a").click();
   } else if(check=='p11d') {
   
   var cnt_of_other = $("#cnt_of_other").val();
   var new_cnt = parseInt(cnt_of_other)-1;
   $("#cnt_of_other").val(new_cnt);
   $("#old_cnt").hide();
   $("#other_tab_cnt").html(new_cnt);
   
    $(".p11dtab").attr("style", "display:none");

     if($(this).is(':checked')&&(check=='payroll')){

    }else{
       $(".it11").attr("style", "display:none");
    }


    if(($(".for_payroll_section").css('display') === 'none') && ($(".worktab").css('display')==='none') && ($('.contratab').css('display')==='none')) {
         $(".it11").addClass("hide");
         $(".li_append_pay").html('');
    }
   
    // $(".it11").addClass("hide");
    // $(".li_append_pay").html('');
   
   }
   
   //bookkeep section enable/disable
   if($(this).is(':checked')&&(check=='bookkeep')){
   var cnt_of_other = $("#cnt_of_other").val();
   var new_cnt = parseInt(cnt_of_other)+1;
   $("#cnt_of_other").val(new_cnt);
   $("#old_cnt").hide();
   $("#other_tab_cnt").html(new_cnt);
   
   $(".li_append_man").html('<li class="nav-item it13 hide" value="2" style="display: none;"><a class="nav-link" data-toggle="tab" href="#management-account"><strong>'+cnt_of_other+'</strong>.Management Accounts</a></li> ');
   if(localStorage.getItem("service")!='management'){
         localStorage.clear();
         localStorage.setItem("service", "bookkeep");
         $(".bookkeeptab").attr("style", "display:block");
         var data=$("#management-account").html();      
         $(".body-popup-content").html('');
         $(".body-popup-content").append(data);
      }
      else{
         localStorage.setItem("service", "bookkeep");
         $(".bookkeeptab").attr("style", "display:block");
         var data=$("#management-account").html();      
         $(".body-popup-content").html('');
         $(".body-popup-content").append(data);
      }
      if ($(this).hasClass('rsptrspt')){}else{
        checking();
     }
     // $('#myReminders').modal();
   $(".it13").attr("style", "display:block");
   $(".it13").removeClass("hide");
   //$(".it13 a").click();
   } else if(check=='bookkeep') {
   
   var cnt_of_other = $("#cnt_of_other").val();
   var new_cnt = parseInt(cnt_of_other)-1;
   $("#cnt_of_other").val(new_cnt);
   $("#old_cnt").hide();
   $("#other_tab_cnt").html(new_cnt);
   
    $(".bookkeeptab").attr("style", "display:none");

    if($(this).is(':checked')&&(check=='management')){

       }else{
        $(".it13").attr("style", "display:none");
       }
   
    $(".it13").addClass("hide");
    $(".li_append_man").html('');
   
   }
   
   //registered section enable/disable
   if($(this).is(':checked')&&(check=='registered')){
   
   var cnt_of_other = $("#cnt_of_other").val();
   var new_cnt = parseInt(cnt_of_other)+1;
   $("#cnt_of_other").val(new_cnt);
   $("#old_cnt").hide();
   $("#other_tab_cnt").html(new_cnt);
   
   $('.li_append_inv').html('<li class="nav-item it14 hide" value="2" style="display: none;"><a class="nav-link" data-toggle="tab" href="#investigation-insurance"><strong>'+cnt_of_other+'</strong>.Investigation Insurance</a></li>');
   if(localStorage.getItem("service")!='investgate'){
         localStorage.clear();
         localStorage.setItem("service", "registered");
         $(".registeredtab").attr("style", "display:block");          
         var data=$("#investigation-insurance").html();      
         $(".body-popup-content").html('');
         $(".body-popup-content").append(data);
   }
   else{
         localStorage.setItem("service", "registered");
         $(".registeredtab").attr("style", "display:block");          
         var data=$("#investigation-insurance").html();      
         $(".body-popup-content").html('');
         $(".body-popup-content").append(data);
   }
     if ($(this).hasClass('rsptrspt')){}else{
       checking();
    }
    $(".it14").attr("style", "display:block");
   // $(".it14").removeClass("hide");
   //$(".it14 a").click();
   } else if(check=='registered') {
   var cnt_of_other = $("#cnt_of_other").val();
   var new_cnt = parseInt(cnt_of_other)-1;
   $("#cnt_of_other").val(new_cnt);
   $("#old_cnt").hide();
   $("#other_tab_cnt").html(new_cnt); 
   
   $(".registeredtab").attr("style", "display:none");

    if($(this).is(':checked')&&(check=='investgate')){
     }else{
       $(".it14").attr("style", "display:none");
     }


      if($('.for_investigation').css('display')==='none' && $('.taxadvicetab').css('display')==='none')
   {
   
   $(".it14").addClass("hide");
   $('.li_append_inv').html('');
   }
   
   }
   
   //Tax Advice/Investigation section enable/disable
   if($(this).is(':checked')&&(check=='taxadvice' || check=='taxinvest')){
   var cnt_of_other = $("#cnt_of_other").val();
   var new_cnt = parseInt(cnt_of_other)+1;
   $("#cnt_of_other").val(new_cnt);
   $("#old_cnt").hide();
   $("#other_tab_cnt").html(new_cnt);
   
   $('.li_append_inv').html('<li class="nav-item it14 hide" value="2" style="display: none;"><a class="nav-link" data-toggle="tab" href="#investigation-insurance"><strong>'+cnt_of_other+'</strong>.Investigation Insurance</a></li>');
   
   if(localStorage.getItem("service")!='investgate'){
      localStorage.clear();
      localStorage.setItem("service", "taxadvice");
      $(".taxadvicetab").attr("style", "display:block");
      var data=$("#investigation-insurance").html();      
      $(".body-popup-content").html('');
      $(".body-popup-content").append(data);
   }
     if ($(this).hasClass('rsptrspt')){}else{
       checking();
    }
   $(".it14").attr("style", "display:block");
   $(".it14").removeClass("hide");
   //$(".it14 a").click();
   } else if(check=='taxadvice' || check=='taxinvest') {
   
    var cnt_of_other = $("#cnt_of_other").val();
   var new_cnt = parseInt(cnt_of_other)-1;
   $("#cnt_of_other").val(new_cnt);
   $("#old_cnt").hide();
   $("#other_tab_cnt").html(new_cnt);
     
    $(".taxadvicetab").attr("style", "display:none");
    if($(this).is(':checked')&&(check=='investgate')){
     }else{
       $(".it14").attr("style", "display:none");
     }
    $(".it14").addClass("hide");
    $('.li_append_inv').html('');
   
   }
   
   //Previous Year Requires
   if($(this).is(':checked')&&(check=='preyear')){
   
    $("#enable_preyear").attr("style", "display:block");
   } else if(check=='preyear') {
   
    $("#enable_preyear").attr("style", "display:none");
   
   }
   
   // confirmation remainder
   if($(this).is(':checked')&&(check=='enableconf')){
      /* Suganya */  
      $(".enable_conf").css("display", "block");    
   
   console.log(localStorage.getItem("service"));
    if (($('#service th:nth-child(2) input:checked').length > 0) || localStorage.getItem("service")!='confirmation'){
      localStorage.clear();
      var data=$("#confirmation-statement").html(); 
      $(".body-popup-content").html('');  
      $(".body-popup-content").append(data);
      }
      else{
      var data=$("#confirmation-statement").html(); 
      $(".body-popup-content").html('');  
      $(".body-popup-content").append(data);
      }       
   if($(this).hasClass('rsptrspt')){
      
    }else{    
      $('#myReminders').modal({backdrop: 'static', keyboard: false}); 
     }

      $('#myReminders').find("#confirmation_auth_code").val($('#auth_code').val());      
      /* Suganya */     
   } else if(check=='enableconf') {    
   
    $(".enable_conf").attr("style", "display:none");      
   }
   
   $('#myReminders').on('hidden.bs.modal', function () {
   // do something…
    // $("body").css({"overflow":"auto"});
   })
   
   //$('.datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
   
   // Accounts remainder
   if($(this).is(':checked')&&(check=='enableacc')){      
      /* Suganya */
    $(".enable_acc").css("display", "block");
    $(".enable_accdue").css("display", "block");
     $('#myReminders .accounts_auth_code').val($('#auth_code').val());      
   
     console.log(localStorage.getItem("service"));
    if (($('#service th:nth-child(2) input:checked').length > 0) || localStorage.getItem("service")!='accounts'){
   
      localStorage.clear();
      var data=$("#accounts").html();  
      $(".body-popup-content").html('');
      $(".body-popup-content").append(data);
      }
   // console.log(localStorage.getItem("service"));
   if (($('#service th:nth-child(2) input:checked').length > 0)){
       localStorage.setItem("service", "accounts");
   }
   // console.log(localStorage.getItem("service"));      
      if($(this).hasClass('rsptrspt')){
      
    }else{
     $('#myReminders').modal({backdrop: 'static', keyboard: false});
      $('#myReminders .accounts_auth_code').val($('#auth_code').val());
   }
      /*Suganya */
    
   } else if(check=='enableacc') {
   
    $(".enable_acc").attr("style", "display:none");
    $(".enable_accdue").attr("style", "display:none");  
   }
   // company tax remainder
   if($(this).is(':checked')&&(check=='enabletax')){     
    /* Suganya */
      $(".enable_tax").css("display", "block");
    if(localStorage.getItem("service")!='accounts'){
       if (($('#service th:nth-child(2) input:checked').length > 0) || localStorage.getItem("service")!='companytax'){
         localStorage.clear();
            var data=$("#accounts").html(); 
            $(".body-popup-content").html('');       
            $(".body-popup-content").append(data);
         }
   }
      if($(this).hasClass('rsptrspt')){
      
    }else{
     $('#myReminders').modal({backdrop: 'static', keyboard: false});
     }
      /*Suganya */ 
   } else if(check=='enabletax') {        
    $(".enable_tax").attr("style", "display:none");      
   }
   
   // Personal Tax Return remainder
   if($(this).is(':checked')&&(check=='enablepertax')){
   /* Suganya */
    $(".enable_pertax").css("display", "block");
      console.log(localStorage.getItem("service"));
   
   // if(localStorage.getItem("service")!='accounts'){
      if (($('#service th:nth-child(2) input:checked').length > 0) || localStorage.getItem("service")!='personaltax'){
         localStorage.clear();
       // $("body").css({"overflow":"hidden"}); 
      var data=$("#personal-tax-returns").html();  
      $(".body-popup-content").html('');        
      $(".body-popup-content").append(data);
      }
   // }   
      if($(this).hasClass('rsptrspt')){
      
    }else{
     $('#myReminders').modal({backdrop: 'static', keyboard: false});
  }
      /*Suganya */  
   } else if(check=='enablepertax') {       
    $(".enable_pertax").attr("style", "display:none");
    
   }
   // payroll remainder
   if($(this).is(':checked')&&(check=='enablepay')){
    /* Suganya */
    $('.enable_pay').addClass('rsptrspt');
    $('.enable_pay').css('display','');
    // $(".enable_pay").attr("display", "block");
       if (($('#service th:nth-child(2) input:checked').length > 0) || localStorage.getItem("service")!='payroll'){
      localStorage.clear();
      var data=$("#payroll").html();    
   $(".body-popup-content").html('');       
   //  $(".it11 a").click();   
   $(".body-popup-content").append(data);
   }
   
   console.log(localStorage.getItem("service"));
   if (($('#service th:nth-child(2) input:checked').length > 0)){      
          localStorage.setItem("service", "payroll");          
   }
   
    console.log(localStorage.getItem("service"));
       if($(this).hasClass('rsptrspt')){
      
    }else{
     $('#myReminders').modal({backdrop: 'static', keyboard: false});
     }
      /*Suganya */     
   
   } else if(check=='enablepay') {        
    $(".enable_pay").attr("style", "display:none");       
   }
   // WorkPlace Pension - AE remainder
   if($(this).is(':checked')&&(check=='enablework')){
   /* Suganya */
   $(".enable_work").css("display", "block");
   $(".enable_workdue").css("display", "block");
   
    if(localStorage.getItem("service")!='payroll'){
       if (($('#service th:nth-child(2) input:checked').length > 0 ) || localStorage.getItem("service")!='workplace'){
         localStorage.clear();
      var data=$("#payroll").html();   
      $(".body-popup-content").html('');       
      $(".body-popup-content").append(data);
      }
    }
      if($(this).hasClass('rsptrspt')){
      
    }else{
     $('#myReminders').modal({backdrop: 'static', keyboard: false});
     }
      /*Suganya */    
   } else if(check=='enablework') {       
    $(".enable_work").attr("style", "display:none");
    $(".enable_workdue").attr("style", "display:none");       
   }
   // C.I.S Contractor remainder
   if($(this).is(':checked')&&(check=='enablecis')){
       /* Suganya */
      $(".enable_cis").css("display", "block");
        console.log(localStorage.getItem("service"));
   if(localStorage.getItem("service")!='payroll'){
      if (($('#service th:nth-child(2) input:checked').length > 0 ) || localStorage.getItem("service")!='cis'){
      localStorage.clear();
         var data=$("#payroll").html();   
   $(".body-popup-content").html('');
   //  $(".it11 a").click();    
   $(".body-popup-content").append(data);
   }
   }    
      if($(this).hasClass('rsptrspt')){
      
    }else{ 
     $('#myReminders').modal({backdrop: 'static', keyboard: false});
     }
      /*Suganya */    
   } else if(check=='enablecis') {        
    $(".enable_cis").attr("style", "display:none");       
   }
   // C.I.S Sub Contractor remainder
   if($(this).is(':checked')&&(check=='enablecissub')){
    /* Suganya */
     $(".enable_cissub").css("display", "block");
      if(localStorage.getItem("service")!='payroll'){
         if (($('#service th:nth-child(2) input:checked').length > 0 ) || localStorage.getItem("service")!='cis'){
         localStorage.clear();
         var data=$("#payroll").html();  
         $(".body-popup-content").html('');      
         //  $(".it11 a").click();    
         $(".body-popup-content").append(data);
         }
      }
      if($(this).hasClass('rsptrspt')){
      
    }else{
     $('#myReminders').modal({backdrop: 'static', keyboard: false});
     }
      /*Suganya */      
   } else if(check=='enablecissub') {       
    $(".enable_cissub").attr("style", "display:none");
    
   }
   // P11D remainder
   if($(this).is(':checked')&&(check=='enableplld')){
    /* Suganya */
      $(".enable_plld").css("display", "block");
   if(localStorage.getItem("service")!='payroll'){
      if (($('#service th:nth-child(2) input:checked').length > 0 ) || localStorage.getItem("service")!='p11d'){
      localStorage.clear();
      var data=$("#payroll").html();     
      $(".body-popup-content").html('');        
      //  $(".it11 a").click();  
      $(".body-popup-content").append(data);
   }
   }
        if($(this).hasClass('rsptrspt')){
      console.log('ss');
    }else{
      console.log('zzz');
     $('#myReminders').modal({backdrop: 'static', keyboard: false});
     }
      /*Suganya */     
   
   } else if(check=='enableplld') {
   
    $(".enable_plld").attr("style", "display:none");
    
   }
   // VAT Quarters remainder
   if($(this).is(':checked')&&(check=='enablevat')){  
    /* Suganya */
     $(".enable_vat").css("display", "block");
        if (($('#service th:nth-child(2) input:checked').length > 0) || localStorage.getItem("service")!='vat'){
      localStorage.clear();
          var data=$("#vat-Returns").html(); 
   $(".body-popup-content").html('');       
   //  $(".it8 a").click();    
   $(".body-popup-content").append(data);
   }
      if($(this).hasClass('rsptrspt')){
      
    }else{
     $('#myReminders').modal({backdrop: 'static', keyboard: false});
     }
      /*Suganya */     
   
   } else if(check=='enablevat') {        
    $(".enable_vat").attr("style", "display:none");       
   }
   // Bookkeeping remainder
   if($(this).is(':checked')&&(check=='enablebook')){
   /* Suganya */
    $(".enable_book").css("display", "block");
     if(localStorage.getItem("service")!='management'){
     if (($('#service th:nth-child(2) input:checked').length > 0 ) || localStorage.getItem("service")!='bookkeep'){
      localStorage.clear();
         var data=$("#management-account").html();  
         $(".body-popup-content").html('');        
         // $(".it13 a").click();     
         $(".body-popup-content").append(data);
   }
   }      
      if($(this).hasClass('rsptrspt')){
      
    }else{
     $('#myReminders').modal({backdrop: 'static', keyboard: false});
     }
          /* Suganya */      
   
   } else if(check=='enablebook') {       
    $(".enable_book").attr("style", "display:none");       
   }
   // Management Accounts remainder
   if($(this).is(':checked')&&(check=='enablemanagement')){
    /* Suganya */
     $(".enable_management").css("display", "block");
   
      if (($('#service th:nth-child(2) input:checked').length > 0 ) || localStorage.getItem("service")!='management'){
      localStorage.clear();
         var data=$("#management-account").html();   
   $(".body-popup-content").html('');       
   //  $(".it13 a").click();   
   $(".body-popup-content").append(data);
   }      
    console.log(localStorage.getItem("service"));
   if (($('#service th:nth-child(2) input:checked').length > 0)){      
          localStorage.setItem("service", "management");           
   }
    console.log(localStorage.getItem("service"));
       if($(this).hasClass('rsptrspt')){
      
    }else{
     $('#myReminders').modal({backdrop: 'static', keyboard: false});
     }
   /* Suganya */    
   } else if(check=='enablemanagement') {       
    $(".enable_management").attr("style", "display:none");       
   }
   
   // Investigation insurance remainder
   if($(this).is(':checked')&&(check=='enableinvest')){
    /* Suganya */
    $(".enable_invest").css("display", "block");
      if (($('#service th:nth-child(2) input:checked').length > 0 ) || localStorage.getItem("service")!='investgate'){
      localStorage.clear();
          var data=$("#investigation-insurance").html(); 
   $(".body-popup-content").html('');         
   //   $(".it14 a").click();     
   $(".body-popup-content").append(data);
   }
    console.log(localStorage.getItem("service"));
   if (($('#service th:nth-child(2) input:checked').length > 0)){       
          localStorage.setItem("service", "investgate");           
   }
   console.log(localStorage.getItem("service"));
      if($(this).hasClass('rsptrspt')){
      
    }else{
    $('#myReminders').modal({backdrop: 'static', keyboard: false});
    }
   /* Suganya */    
   } else if(check=='enableinvest') {       
    $(".enable_invest").attr("style", "display:none");       
   }
   
   // registration add remainder
   if($(this).is(':checked')&&(check=='enablereg')){
    /* Suganya */
     $(".enable_reg").css("display", "block");
      console.log(localStorage.getItem("service"));
   if(localStorage.getItem("service")!='investgate'){
      if (($('#service th:nth-child(2) input:checked').length > 0 ) || localStorage.getItem("service")!='registered'){
      localStorage.clear();
     var data=$("#investigation-insurance").html();   
     $(".body-popup-content").html('');       
   // $(".it14 a").click();        
     $(".body-popup-content").append(data);
   
   }
   }
      if($(this).hasClass('rsptrspt')){
      
    }else{
     $('#myReminders').modal({backdrop: 'static', keyboard: false});
  }
   /* Suganya */      
       
   } else if(check=='enablereg') {        
    $(".enable_reg").attr("style", "display:none");       
   }
   // tax advice add remainder
   if($(this).is(':checked')&&(check=='enabletaxad')){
   
    /* Suganya */
    $(".enable_taxad").css("display", "block");
     console.log(localStorage.getItem("service"));
   if(localStorage.getItem("service")!='investgate'){
     if (($('#service th:nth-child(2) input:checked').length > 0 ) || localStorage.getItem("service")!='taxadvice'){
         localStorage.clear();
         var data=$("#investigation-insurance").html();   
         $(".body-popup-content").html('');          
         //  $(".it14 a").click();   
         $(".body-popup-content").append(data);
   }
   
   }  
      if($(this).hasClass('rsptrspt')){
       console.log('taxadvice');
    }else{ 
     console.log('tafsdfsdxadvice');   
     $('#myReminders').modal({backdrop: 'static', keyboard: false});
  }
   /* Suganya */      
   } else if(check=='enabletaxad') {
   
    $(".enable_taxad").attr("style", "display:none");
    
   }
   
   
   if($(this).is(':checked')&&(check=='enabletaxinves')){
    /* Suganya */
    $(".enable_taxad").css("display", "block");
   
    console.log(localStorage.getItem("service"));
   if(localStorage.getItem("service")!='investgate'){
    if (($('#service th:nth-child(2) input:checked').length > 0 ) || localStorage.getItem("service")!='taxadvice'){
      localStorage.clear();
         var data=$("#investigation-insurance").html();   
         $(".body-popup-content").html('');          
         //  $(".it14 a").click();   
         $(".body-popup-content").append(data);
   }
   }
      if($(this).hasClass('rsptrspt')){
      console.log('taxadvice');
    }else{
      console.log('adasdasds');
     $('#myReminders').modal('show');
      }
   /* Suganya */    
   
   } else if(check=='enabletaxinves') {       
   // $("body").css("overflow-y", "auto");  
    $(".enable_taxad").attr("style", "display:none");       
   }

   $(this).removeClass('rsptrspt');
   
   //onet remainder
   if($(this).is(':checked')&&(check=='onet')){
   
    $(".onet").attr("style", "");
   
   } else if(check=='onet') {
   
    $(".onet").attr("style", "display:none");
    
   }
   //twot remainder
   if($(this).is(':checked')&&(check=='twot')){
   
    $(".twot").attr("style", "");
   
   } else if(check=='twot') {
   
    $(".twot").attr("style", "display:none");
    
   }
   
   //threet remainder
   if($(this).is(':checked')&&(check=='threet')){
   
    $(".threet").attr("style", "");
   
   } else if(check=='threet') {
   
    $(".threet").attr("style", "display:none");
    
   }
   
   //fourt remainder
   if($(this).is(':checked')&&(check=='fourt')){
   
    $(".fourt").attr("style", "");
   
   } else if(check=='fourt') {
   
    $(".fourt").attr("style", "display:none");
    
   }
   //fivet remainder
   if($(this).is(':checked')&&(check=='fivet')){
   
    $(".fivet").attr("style", "");
   
   } else if(check=='fivet') {
   
    $(".fivet").attr("style", "display:none");  
   }
   
   //sixt remainder
   if($(this).is(':checked')&&(check=='sixt')){
   
    $(".sixt").attr("style", "");
   
   } else if(check=='sixt') {
   
    $(".sixt").attr("style", "display:none");  
   }
   
   //sevent remainder
   if($(this).is(':checked')&&(check=='sevent')){
   
    $(".sevent").attr("style", "");
   
   } else if(check=='sevent') {
   
    $(".sevent").attr("style", "display:none");  
   }
   
   //eightt remainder
   if($(this).is(':checked')&&(check=='eightt')){
   
    $(".eightt").attr("style", "");
   
   } else if(check=='eightt') {
   
    $(".eightt").attr("style", "display:none");  
   }
   //ninet remainder
   if($(this).is(':checked')&&(check=='ninet')){
   
    $(".ninet").attr("style", "");
   
   } else if(check=='ninet') {
   
    $(".ninet").attr("style", "display:none");  
   }
   
   //tent remainder
   if($(this).is(':checked')&&(check=='tent')){
   
    $(".tent").attr("style", "");
   
   } else if(check=='tent') {
   
    $(".tent").attr("style", "display:none");  
   }
   
   //elevent remainder
   if($(this).is(':checked')&&(check=='elevent')){
   
    $(".elevent").attr("style", "");
   
   } else if(check=='elevent') {
   
    $(".elevent").attr("style", "display:none");  
   }
   
   //twelvet remainder
   if($(this).is(':checked')&&(check=='twelvet')){
   
    $(".twelvet").attr("style", "");
   
   } else if(check=='twelvet') {
   
    $(".twelvet").attr("style", "display:none");  
   }
   
   //thirteent remainder
   if($(this).is(':checked')&&(check=='thirteent')){
   
    $(".thirteent").attr("style", "");
   
   } else if(check=='thirteent') {
   
    $(".thirteent").attr("style", "display:none");  
   }
   
   //fourteent remainder
   if($(this).is(':checked')&&(check=='fourteent')){
   
    $(".fourteent").attr("style", "");
   
   } else if(check=='fourteent') {
   
    $(".fourteent").attr("style", "display:none");  
   }
   
   //fifteent remainder
   if($(this).is(':checked')&&(check=='fifteent')){
   
    $(".fifteent").attr("style", "");
   
   } else if(check=='fifteent') {
   
    $(".fifteent").attr("style", "display:none");  
   }
   
   //sixteent remainder
   if($(this).is(':checked')&&(check=='sixteent')){
   
    $(".sixteent").attr("style", "");
   
   } else if(check=='sixteent') {
   
    $(".sixteent").attr("style", "display:none");  
   }
   
   //confirmation Statement Invoice
   if($(this).is(':checked')&&(check=='incs')){
   
   $(".incs").attr("style", "");
   
   } else if(check=='incs') {
   
   $(".incs").attr("style", "display:none");  
   }
   
   //Accounts Invoice
   if($(this).is(':checked')&&(check=='inas')){
   
   $(".inas").attr("style", "");
   
   } else if(check=='inas') {
   
   $(".inas").attr("style", "display:none");  
   }
   //company Invoice
   if($(this).is(':checked')&&(check=='inct')){
   
   $(".inct").attr("style", "");
   
   } else if(check=='inct') {
   
   $(".inct").attr("style", "display:none");  
   }
   //Personal Tax return Invoice
   if($(this).is(':checked')&&(check=='inpt')){
   
   $(".inpt").attr("style", "");
   
   } else if(check=='inpt') {
   
   $(".inpt").attr("style", "display:none");  
   }
   //Payroll Invoice
   if($(this).is(':checked')&&(check=='inpr')){
   
   $(".inpr").attr("style", "");
   
   } else if(check=='inpr') {
   
   $(".inpr").attr("style", "display:none");  
   }
   
   //Pension and declration return Invoice
   if($(this).is(':checked')&&(check=='inpn')){
   
   $(".inpn").attr("style", "");
   $(".indl").attr("style", "");
   
   } else if(check=='inpn') {
   
   $(".inpn").attr("style", "display:none");  
   $(".indl").attr("style", "display:none");  
   }
   
   //CIS return Invoice
   
   if($(this).is(':checked')&&(check=='incis')){
   
   $(".incis").attr("style", "");
   
   } else if(check=='incis') {
   
   $(".incis").attr("style", "display:none");  
   
   }
   //CIS_SUB return Invoice
   
   if($(this).is(':checked')&&(check=='incis_sub')){
   
   $(".incis_sub").attr("style", "");
   
   } else if(check=='incis_sub') {
   
   $(".incis_sub").attr("style", "display:none");  
   
   }
   //P11d return Invoice
   if($(this).is(':checked')&&(check=='inp11d')){
   
   $(".inp11d").attr("style", "");
   
   } else if(check=='inp11d') {
   
   $(".inp11d").attr("style", "display:none");  
   
   }
   //VAT return Invoice
   if($(this).is(':checked')&&(check=='invt')){
   
   $(".invt").attr("style", "");
   
   } else if(check=='invt') {
   
   $(".invt").attr("style", "display:none");  
   
   }
   
   //bookkeep return Invoice
   if($(this).is(':checked')&&(check=='inbook')){
   
   $(".inbook").attr("style", "");
   
   } else if(check=='inbook') {
   
   $(".inbook").attr("style", "display:none");  
   
   }
   //Management return Invoice
   if($(this).is(':checked')&&(check=='inma')){
   
   $(".inma").attr("style", "");
   
   } else if(check=='inma') {
   
   $(".inma").attr("style", "display:none");  
   
   } 
   
   //Investigation Insurance Invoice
   if($(this).is(':checked')&&(check=='ininves')){
   
   $(".ininves").attr("style", "");
   
   } else if(check=='ininves') {
   
   $(".ininves").attr("style", "display:none");  
   
   }
   //Registered Address Invoice
   if($(this).is(':checked')&&(check=='inreg')){
   
   $(".inreg").attr("style", "");
   
   } else if(check=='inreg') {
   
   $(".inreg").attr("style", "display:none");  
   
   }
   //Tax Advice Invoice
   if($(this).is(':checked')&&(check=='intaxadv')){
   
   $(".intaxadv").attr("style", "");
   
   } else if(check=='intaxadv') {
   
   $(".intaxadv").attr("style", "display:none");  
   
   }
   //Tax Investigation Invoice
   if($(this).is(':checked')&&(check=='intaxinves')){
   
   $(".intaxinves").attr("style", "");
   
   } else if(check=='intaxinves') {
   
   $(".intaxinves").attr("style", "display:none");  
   
   }
   
   // accounts reminder
   
   if($(this).is(':checked')&&(check=='accounts_cus')){
   
   $('#add-reminder').modal('toggle');
   $('#service_id').val('3');
   
   } else if(check=='accounts_cus') {
   
   $('#add-reminder').modal('hide');
   $('#service_id').val('');
   
   }
   
   // confirmation reminder
   
   if($(this).is(':checked')&&(check=='confirm_cus')){
   
   $('#add-reminder').modal('toggle');
   $('#service_id').val('4');
   
   } else if(check=='confirm_cus') {
   
   $('#add-reminder').modal('hide');
   $('#service_id').val('');
   
   }
   // company tax reminder
   
   if($(this).is(':checked')&&(check=='company_cus')){
   
   $('#add-reminder').modal('toggle');
   $('#service_id').val('5');
   
   } else if(check=='company_cus') {
   
   $('#add-reminder').modal('hide');
   $('#service_id').val('');
   
   }
   
   // personal tax reminder
   
   if($(this).is(':checked')&&(check=='personal_cus')){
   
   $('#add-reminder').modal('toggle');
   $('#service_id').val('6');
   
   } else if(check=='personal_cus') {
   
   $('#add-reminder').modal('hide');
   $('#service_id').val('');
   
   }
   // payroll reminder
   
   if($(this).is(':checked')&&(check=='payroll_cus')){
   
   $('#add-reminder').modal('toggle');
   $('#service_id').val('7');
   
   } else if(check=='payroll_cus') {
   
   $('#add-reminder').modal('hide');
   $('#service_id').val('');
   
   }
   // workplace pension reminder
   
   if($(this).is(':checked')&&(check=='pension_cus')){
   
   $('#add-reminder').modal('toggle');
   $('#service_id').val('8');
   
   } else if(check=='pension_cus') {
   
   $('#add-reminder').modal('hide');
   $('#service_id').val('');
   
   }
   // investigation insurance reminder
   
   if($(this).is(':checked')&&(check=='insurance_cus')){
   
   $('#add-reminder').modal('toggle');
   $('#service_id').val('9');
   
   } else if(check=='insurance_cus') {
   
   $('#add-reminder').modal('hide');
   $('#service_id').val('');
   
   }
   
   // Registered reminder
   
   if($(this).is(':checked')&&(check=='registered_cus')){
   
   $('#add-reminder').modal('toggle');
   $('#service_id').val('9');
   
   } else if(check=='registered_cus') {
   
   $('#add-reminder').modal('hide');
   $('#service_id').val('');
   
   }
   
   // Tax advice/investigation reminder
   
   if($(this).is(':checked')&&(check=='investigation_cus')){
   
   $('#add-reminder').modal('toggle');
   $('#service_id').val('9');
   
   } else if(check=='investigation_cus') {
   
   $('#add-reminder').modal('hide');
   $('#service_id').val('');
   
   }
   
   // cis reminder
   
   if($(this).is(':checked')&&(check=='cis_cus')){
   
   $('#add-reminder').modal('toggle');
   $('#service_id').val('10');
   
   } else if(check=='cis_cus') {
   
   $('#add-reminder').modal('hide');
   $('#service_id').val('');
   
   }
   // cissub_cus reminder
   if($(this).is(':checked')&&(check=='cissub_cus')){
   
   $('#add-reminder').modal('toggle');
   $('#service_id').val('11');
   
   } else if(check=='cissub_cus') {
   
   $('#add-reminder').modal('hide');
   $('#service_id').val('');
   
   }
   // p11d_cus reminder
   if($(this).is(':checked')&&(check=='p11d_cus')){
   
   $('#add-reminder').modal('toggle');
   $('#service_id').val('12');
   
   } else if(check=='p11d_cus') {
   
   $('#add-reminder').modal('hide');
   $('#service_id').val('');
   
   }
   // bookkeep_cus reminder
   if($(this).is(':checked')&&(check=='bookkeep_cus')){
   
   $('#add-reminder').modal('toggle');
   $('#service_id').val('13');
   
   } else if(check=='bookkeep_cus') {
   
   $('#add-reminder').modal('hide');
   $('#service_id').val('');
   
   }
   
   // manage_cus reminder
   
   if($(this).is(':checked')&&(check=='manage_cus')){
   
   $('#add-reminder').modal('toggle');
   $('#service_id').val('14');
   
   } else if(check=='manage_cus') {
   
   $('#add-reminder').modal('hide');
   $('#service_id').val('');
   
   }
   
   //vat_cus reminder
   
   if($(this).is(':checked')&&(check=='vat_cus')){
   
   $('#add-reminder').modal('toggle');
   $('#service_id').val('15');
   
   } else if(check=='vat_cus') {
   
   $('#add-reminder').modal('hide');
   $('#service_id').val('');
   
   }
   
   //$('.body-popup-content .datepicker, .body-popup-content .dob_picker').removeClass('hasDatepicker');
   // $('.body-popup-content .datepicker, .body-popup-content .dob_picker').removeAttr('id');
   //$('.body-popup-content .datepicker, .body-popup-content .dob_picker').datepicker({ inline: true, dateFormat: 'dd-mm-yy' }).val();
   $('.body-popup-content .datepicker-13, .body-popup-content .dob_picker').removeClass('hasDatepicker');
   $('.body-popup-content .datepicker-13, .body-popup-content .dob_picker').datepicker({ 
   dateFormat: 'yy-mm-dd',
   onSelect:function(selectedDate){
             //    alert($(this).attr('name'));
                     $("input[name="+$(this).attr('name')+"]").each(function() {
                        $(this).attr('value',selectedDate);
                       });
                    }, });
   
   // $(document).on('click', '.body-popup-content .datepicker, .body-popup-content .dob_picker', function () {
   
   //    $('.body-popup-content .datepicker, .body-popup-content .dob_picker').datepicker({ inline: true, dateFormat: 'dd-mm-yy' }).val();
   // });
   
   
   var $grid = $('.body-popup-content  .masonry-container').masonry({
   itemSelector: '.body-popup-content  .accordion-panel',
   percentPosition: true,
   columnWidth: '.body-popup-content  .grid-sizer' 
   });
   
   setTimeout(function(){ $grid.masonry('layout'); }, 600);
   
   $(document).on( 'click', '.body-popup-content .switchery', function() {
     setTimeout(function(){ $grid.masonry('layout'); }, 600);
    });
   
   /*var elemstate = document.querySelector('.body-popup-content  .js-small');
   var switcheryy = new Switchery(elemstate, { color: '#1abc9c', jackColor: '#fff' });
   switcheryy.enable();    */
   
   $('.body-popup-content .switchery').remove();
   var elem = Array.prototype.slice.call(document.querySelectorAll('.body-popup-content .js-small'));
   
        elem.forEach(function(html) {
            var switchery = new Switchery(html, {
                color: '#1abc9c',
                jackColor: '#fff',
                size: 'small'
            });
        });
   });
   
  $(document).ready(function(){
  
  /*  $( "#datepicker" ).datepicker({
      dateFormat: 'yy-mm-dd'
  });*/
   $(document).on('change','.js-smallss',function(){
 //  $('.js-small').change(function() {
      
    //$(this).closest('div').find('.field').prop('disabled', !$(this).is(':checked'));
  
    var check= $(this).data('id');
    //alert(check);
   //alert($(this).is(':checked'));
   //alert($(this).val());
   if($(this).is(':checked')&&(check=='accounts')){
      $(".it6").attr("style", "display:block");
      $(".it6").removeClass("hide");
   } else if(check=='accounts') {
      $(".it6").attr("style", "display:none");
      $(".it6").addClass("hide");
   }
  
  if($(this).is(':checked')&&(check=='payroll')){
      $(".it11").attr("style", "display:block");
      $(".it11").removeClass("hide");
   } else if(check=='paye') {
      $(".it11").attr("style", "display:none");
      $(".it11").addClass("hide");
   }
  
  if($(this).is(':checked')&&(check=='vat')){
      $(".it8").attr("style", "display:block");
      $(".it8").removeClass("hide");
   } else if(check=='vat') {
      $(".it8").attr("style", "display:none");
      $(".it8").addClass("hide");
   }
  
  
   if($(this).is(':checked')&&(check=='cons')){
      $(".it7").attr("style", "display:block");
      $(".it7").removeClass("hide");
   } else if(check=='cons') {
      $(".it7").attr("style", "display:none");
      $(".it7").addClass("hide");
   }
   
   if($(this).is(':checked')&&(check=='personaltax')){
      $(".it12").attr("style", "display:block");
      $(".it12").removeClass("hide");
   } else if(check=='personaltax') {
      $(".it12").attr("style", "display:none");
      $(".it12").addClass("hide");
   }
  
    if($(this).is(':checked')&&(check=='management')){
      $(".it13").attr("style", "display:block");
      $(".it13").removeClass("hide");
   } else if(check=='management') {
      $(".it13").attr("style", "display:none");
      $(".it13").addClass("hide");
   }
  
    if($(this).is(':checked')&&(check=='investgate')){
      $(".it14").attr("style", "display:block");
      $(".it14").removeClass("hide");
   } else if(check=='investgate') {
      $(".it14").attr("style", "display:none");
      $(".it14").addClass("hide");
   }
  
  if($(this).is(':checked')&&(check=='preacc')){
      $("#enable_preacc").attr("style", "display:block");
   } else if(check=='preacc') {
      $("#enable_preacc").attr("style", "display:none");
     
   }
   //company tax section enable/disable
   if($(this).is(':checked')&&(check=='companytax')){
      
      $(".companytax").attr("style", "display:block");
   } else if(check=='companytax') {
         
      $(".companytax").attr("style", "display:none");
     
   }
  
   //workplace pension section enable/disable
   if($(this).is(':checked')&&(check=='workplace')){
      
      $(".worktab").attr("style", "display:block");
   } else if(check=='workplace') {
         
      $(".worktab").attr("style", "display:none");
     
   }
  
   //cis/subcis  section enable/disable
   if($(this).is(':checked')&&(check=='cis')){
      
      $(".contratab").attr("style", "display:block");
   } else if(check=='cis') {
         
      $(".contratab").attr("style", "display:none");
     
   }
  
   //p11d  section enable/disable
   if($(this).is(':checked')&&(check=='p11d')){
      
      $(".p11dtab").attr("style", "display:block");
   } else if(check=='p11d') {
         
      $(".p11dtab").attr("style", "display:none");
     
   }
  
   //bookkeep section enable/disable
   if($(this).is(':checked')&&(check=='bookkeep')){
      
      $(".bookkeeptab").attr("style", "display:block");
   } else if(check=='bookkeep') {
         
      $(".bookkeeptab").attr("style", "display:none");
     
   }
  
   //registered section enable/disable
   if($(this).is(':checked')&&(check=='registered')){
      
      $(".registeredtab").attr("style", "display:block");
   } else if(check=='registered') {
         
      $(".registeredtab").attr("style", "display:none");
     
   }
  
   //Tax Advice/Investigation section enable/disable
   if($(this).is(':checked')&&(check=='taxadvice')){
      
      $(".taxadvicetab").attr("style", "display:block");
   } else if(check=='taxadvice') {
         
      $(".taxadvicetab").attr("style", "display:none");
     
   }
  
  //Previous Year Requires
   if($(this).is(':checked')&&(check=='preyear')){
      
      $("#enable_preyear").attr("style", "display:block");
   } else if(check=='preyear') {
         
      $("#enable_preyear").attr("style", "display:none");
     
   }
  
   // confirmation remainder
   if($(this).is(':checked')&&(check=='enableconf')){
      
      $(".enable_conf").attr("style", "");
   } else if(check=='enableconf') {
         
      $(".enable_conf").attr("style", "display:none");
     
   }
  
   // Accounts remainder
   if($(this).is(':checked')&&(check=='enableacc')){
      
      $(".enable_acc").attr("style", "");
      $(".enable_accdue").attr("style", "");
   } else if(check=='enableacc') {
         
      $(".enable_acc").attr("style", "display:none");
      $(".enable_accdue").attr("style", "display:none");  
   }
   // company tax remainder
   if($(this).is(':checked')&&(check=='enabletax')){
      
      $(".enable_tax").attr("style", "");
     
   } else if(check=='enabletax') {
         
      $(".enable_tax").attr("style", "display:none");
     
   }
  
   // Personal Tax Return remainder
   if($(this).is(':checked')&&(check=='enablepertax')){
      
      $(".enable_pertax").attr("style", "");
     
   } else if(check=='enablepertax') {
         
      $(".enable_pertax").attr("style", "display:none");
      
   }
   // payroll remainder
   if($(this).is(':checked')&&(check=='enablepay')){
      
      $(".enable_pay").attr("style", "");
     
   } else if(check=='enablepay') {
         
      $(".enable_pay").attr("style", "display:none");
      
   }
   // WorkPlace Pension - AE remainder
   if($(this).is(':checked')&&(check=='enablework')){
      
      $(".enable_work").attr("style", "");
      $(".enable_workdue").attr("style", "");
     
   } else if(check=='enablework') {
         
      $(".enable_work").attr("style", "display:none");
      $(".enable_workdue").attr("style", "display:none");
      
   }
   // C.I.S Contractor remainder
   if($(this).is(':checked')&&(check=='enablecis')){
      
      $(".enable_cis").attr("style", "");
     
   } else if(check=='enablecis') {
         
      $(".enable_cis").attr("style", "display:none");
      
   }
  // C.I.S Sub Contractor remainder
   if($(this).is(':checked')&&(check=='enablecissub')){
      
      $(".enable_cissub").attr("style", "");
     
   } else if(check=='enablecissub') {
         
      $(".enable_cissub").attr("style", "display:none");
      
   }
   // P11D remainder
   if($(this).is(':checked')&&(check=='enableplld')){
      
      $(".enable_plld").attr("style", "");
     
   } else if(check=='enableplld') {
         
      $(".enable_plld").attr("style", "display:none");
      
   }
  // VAT Quarters remainder
   if($(this).is(':checked')&&(check=='enablevat')){
      
      $(".enable_vat").attr("style", "");
     
   } else if(check=='enablevat') {
         
      $(".enable_vat").attr("style", "display:none");
      
   }
   // Bookkeeping remainder
   if($(this).is(':checked')&&(check=='enablebook')){
      
      $(".enable_book").attr("style", "");
     
   } else if(check=='enablebook') {
         
      $(".enable_book").attr("style", "display:none");
      
   }
   // Management Accounts remainder
   if($(this).is(':checked')&&(check=='enablemanagement')){
      
      $(".enable_management").attr("style", "");
     
   } else if(check=='enablemanagement') {
         
      $(".enable_management").attr("style", "display:none");
      
   }
  
   // Investigation insurance remainder
   if($(this).is(':checked')&&(check=='enableinvest')){
      
      $(".enable_invest").attr("style", "");
     
   } else if(check=='enableinvest') {
         
      $(".enable_invest").attr("style", "display:none");
      
   }
  
   // registration add remainder
   if($(this).is(':checked')&&(check=='enablereg')){
      
      $(".enable_reg").attr("style", "");
     
   } else if(check=='enablereg') {
         
      $(".enable_reg").attr("style", "display:none");
      
   }
   // tax advice add remainder
   if($(this).is(':checked')&&(check=='enabletaxad')){
      
      $(".enable_taxad").attr("style", "");
     
   } else if(check=='enabletaxad') {
         
      $(".enable_taxad").attr("style", "display:none");
      
   }
  
  //onet remainder
   if($(this).is(':checked')&&(check=='onet')){
      
      $(".onet").attr("style", "");
     
   } else if(check=='onet') {
         
      $(".onet").attr("style", "display:none");
      
   }
   //twot remainder
   if($(this).is(':checked')&&(check=='twot')){
      
      $(".twot").attr("style", "");
     
   } else if(check=='twot') {
         
      $(".twot").attr("style", "display:none");
      
   }
  
  //threet remainder
   if($(this).is(':checked')&&(check=='threet')){
      
      $(".threet").attr("style", "");
     
   } else if(check=='threet') {
         
      $(".threet").attr("style", "display:none");
      
   }
  
   //fourt remainder
   if($(this).is(':checked')&&(check=='fourt')){
      
      $(".fourt").attr("style", "");
     
   } else if(check=='fourt') {
         
      $(".fourt").attr("style", "display:none");
      
   }
   //fivet remainder
   if($(this).is(':checked')&&(check=='fivet')){
      
      $(".fivet").attr("style", "");
     
   } else if(check=='fivet') {
         
      $(".fivet").attr("style", "display:none");  
   }
  
    //sixt remainder
   if($(this).is(':checked')&&(check=='sixt')){
      
      $(".sixt").attr("style", "");
     
   } else if(check=='sixt') {
         
      $(".sixt").attr("style", "display:none");  
   }
  
   //sevent remainder
   if($(this).is(':checked')&&(check=='sevent')){
      
      $(".sevent").attr("style", "");
     
   } else if(check=='sevent') {
         
      $(".sevent").attr("style", "display:none");  
   }
  
  //eightt remainder
   if($(this).is(':checked')&&(check=='eightt')){
      
      $(".eightt").attr("style", "");
     
   } else if(check=='eightt') {
         
      $(".eightt").attr("style", "display:none");  
   }
   //ninet remainder
   if($(this).is(':checked')&&(check=='ninet')){
      
      $(".ninet").attr("style", "");
     
   } else if(check=='ninet') {
         
      $(".ninet").attr("style", "display:none");  
   }
  
   //tent remainder
   if($(this).is(':checked')&&(check=='tent')){
      
      $(".tent").attr("style", "");
     
   } else if(check=='tent') {
         
      $(".tent").attr("style", "display:none");  
   }
  
   //elevent remainder
   if($(this).is(':checked')&&(check=='elevent')){
      
      $(".elevent").attr("style", "");
     
   } else if(check=='elevent') {
         
      $(".elevent").attr("style", "display:none");  
   }
  
   //twelvet remainder
   if($(this).is(':checked')&&(check=='twelvet')){
      
      $(".twelvet").attr("style", "");
     
   } else if(check=='twelvet') {
         
      $(".twelvet").attr("style", "display:none");  
   }
  
   //thirteent remainder
   if($(this).is(':checked')&&(check=='thirteent')){
      
      $(".thirteent").attr("style", "");
     
   } else if(check=='thirteent') {
         
      $(".thirteent").attr("style", "display:none");  
   }
  
   //fourteent remainder
   if($(this).is(':checked')&&(check=='fourteent')){
      
      $(".fourteent").attr("style", "");
     
   } else if(check=='fourteent') {
         
      $(".fourteent").attr("style", "display:none");  
   }
  
   //fifteent remainder
   if($(this).is(':checked')&&(check=='fifteent')){
      
      $(".fifteent").attr("style", "");
     
   } else if(check=='fifteent') {
         
      $(".fifteent").attr("style", "display:none");  
   }
  
   //sixteent remainder
   if($(this).is(':checked')&&(check=='sixteent')){
      
      $(".sixteent").attr("style", "");
     
   } else if(check=='sixteent') {
         
      $(".sixteent").attr("style", "display:none");  
   }
  
  });
  
  
  
  $('.contactupdate').click(function(){
         $(".LoadingImage").show();
  
  var Contact_id = $(this).data("id");
  /*var formData = new FormData($("#contact_info"+Contact_id));
  alert("#contact_info"+Contact_id);*/
  /*var datas = $("#contact_info"+Contact_id).serialize();
  alert(datas);
  */
  /*  var formElement = document.getElementById("#contact_info"+Contact_id);
          var formData = new FormData(formElement);*/
        // alert(formData);
  
        /* var rec = getFormData('contact_info'+Contact_id);
         alert(JSON.stringify(rec))*/
  //alert(Contact_id);
  var data = {};
  
  data['title'] = $("#title"+Contact_id).val();
  data['first_name'] = $("#first_name"+Contact_id).val();
  data['middle_name'] = $("#middle_name"+Contact_id).val();
  data['surname'] = $("#surname"+Contact_id).val();
  data['preferred_name'] = $("#preferred_name"+Contact_id).val();
  data['mobile'] = $("#mobile"+Contact_id).val();
  data['main_email'] = $("#main_email"+Contact_id).val();
  data['nationality'] = $("#nationality"+Contact_id).val();
  data['psc'] = $("#psc"+Contact_id).val();
  data['ni_number'] = $("#ni_number"+Contact_id).val();
  data['address_line1'] = $("#address_line1"+Contact_id).val();
  data['address_line2'] = $("#address_line2"+Contact_id).val();
  data['town_city'] = $("#town_city"+Contact_id).val();
  data['post_code'] = $("#post_code"+Contact_id).val();
  data['landline'] = $("#landline"+Contact_id).val();
  data['work_email'] = $("#work_email"+Contact_id).val();
  data['date_of_birth'] = $("#date_of_birth"+Contact_id).val();
  data['nature_of_control'] = $("#nature_of_control"+Contact_id).val();
  data['marital_status'] = $("#marital_status"+Contact_id).val();
  data['utr_number'] = $("#utr_number"+Contact_id).val();
  data['shareholder'] = $("#shareholder"+Contact_id).val();
  data['contact_type'] = $("#contact_type"+Contact_id).val();
  
  data['Contact_id'] = Contact_id;
    $.ajax({
     url: '<?php echo base_url();?>client/updates_clientcontact/',
      type: "POST",
      data: data,
      success: function(data)  
      {
         $('.succ').hide();
         $("#succ"+Contact_id).show();
         $(".LoadingImage").hide();
  
      }
    });
  //alert(JSON.stringify($("#title"+Contact_id)));
  /*$.ajax({
                                  url: '<?php echo base_url();?>client/updates_clientcontact/',
                                  dataType : 'json',
                                  type : 'POST',
                                  data : data,
                                  contentType : false,
                                  processData : false,
                                  success: function(data) {
                                  alert('ffff');
                                  },
                                  error: function() {
                                       $(".LoadingImage").hide();
                                      alert('failed');}
                              });*/
  });
  
  
  function getFormData(formId) {
      return $('#' + formId).serializeArray().reduce(function (obj, item) {
          var name = item.name,
              value = item.value;
  
          if (obj.hasOwnProperty(name)) {
              if (typeof obj[name] == "string") {
                  obj[name] = [obj[name]];
                  obj[name].push(value);
              } else {
                  obj[name].push(value);
              }
          } else {
              obj[name] = value;
          }
          return obj;
      }, {});
  }
  
  
  $(".delcontact").on('click',function(e){
    e.preventDefault();
   id = $(this).data('value');
  //alert(id);
    $.ajax({
        type:"POST",
        url:"<?php echo base_url().'client/delete_contact/'?>",
        data:"id="+id,
        cache: false,  
        success:
        function(data)  
        {
  
        $(".remove"+id).remove();
        $('#modalcontact'+id).modal('hide');
        return false;
        
        }
      });
  return false;
  });
  // delete new person
  $(".delnewperson").on('click',function(e){
    e.preventDefault();
   //id = $(this).data('value');
   $(".removenew").remove();
   $('#modalnewperson').modal('hide');
   return false;
  });
  $("#select_responsible_type").change(function(){
  //alert($(this).val());
  var rec = $(this).val();
  if(rec=='staff')
  {
   $('.responsible_user_tbl').show();
   $('.responsible_team_tbl').hide();
   $('.responsible_department_tbl').hide();
 //  $('.responsible_member_tbl').hide();
  
  }else if(rec=='team')
  {
   $('.responsible_team_tbl').show();
   $('.responsible_user_tbl').hide();
   $('.responsible_department_tbl').hide();
  // $('.responsible_member_tbl').hide();
  }else if(rec=='departments')
  {
   $('.responsible_department_tbl').show();
   $('.responsible_user_tbl').hide();
   $('.responsible_team_tbl').hide();
  // $('.responsible_member_tbl').hide();
  }else if(rec=='members')
  {
   //$('.responsible_member_tbl').show();
   $('.responsible_user_tbl').hide();
   $('.responsible_team_tbl').hide();
   $('.responsible_department_tbl').hide();
  
  }
  else{
      $('.responsible_user_tbl').hide();
      $('.responsible_team_tbl').hide();
      $('.responsible_department_tbl').hide();
    //  $('.responsible_member_tbl').hide();
  }
  });
  $("#person").change(function(){
     var person = $(':selected',this).val();
     var client_id = $('#user_id').val();
     // alert(legal_form);
     if(person=='opt1'){
     // $('#exist_person').modal('show');
     $('.all_layout_modals').modal('show');
     }else if(person=='opt2')
     {
      var cnt = $("#append_cnt").val();
  
              if(cnt==''){
              cnt = 1;
             }else{
              cnt = parseInt(cnt)+1;
             }
          $("#append_cnt").val(cnt);
  
          var incre=$("#incre").val();
          if(incre==''){
              incre = 1;
             }else{
              incre = parseInt(incre)+1;
             }
          $("#incre").val(incre);
          
          //alert(incre);
         $.ajax({
            url: '<?php echo base_url();?>client/new_contact_clientinfo/',
            type: 'post',
            data:{ 'cnt':cnt, 'incre':incre},
            //data: "person="+person+"&client_id="+client_id,
            success: function( data ){
                $('.contact_form').append(data);
                },
                error: function( errorThrown ){
                    console.log( errorThrown );
                }
            });
     }
     
     }); 
  // Other custom label
  $(document).on('change','.othercus',function(e){
  //$(".othercus").change(function(){
     var custom = $(':selected',this).val();
     var client_id = $('#user_id').val();
      //alert(custom);
      
   if(custom=='Other')
     {
    // $('.contact_type').next('span').show();
    $(this).closest('.contact_type').next('.spnMulti').show();
   } else {
  $('.contact_type').next('span').hide();
   }
  
     
     }); 
  
  //Assign Other custom label
  $(document).on('change','.assign_cus',function(e){
  //$(".othercus").change(function(){
     var custom = $(':selected',this).val();
     var client_id = $('#user_id').val();
      //alert(custom);
      
   if(custom=='Other_Custom')
     {
    // $('.contact_type').next('span').show();
    $(this).closest('.assign_cus_type').next('.spanassign').show();
   } else {
  $(this).closest('.assign_cus_type').next('.spanassign').hide();
   }
  
     
     }); 
  
  });
  
</script>
<script>
  /* $( "#searchCompany" ).autocomplete({
         source: function(request, response) {
             //console.info(request, 'request');
             //console.info(response, 'response');
  
             $.ajax({
                 //q: request.term,
                 url: "<?=site_url('client/SearchCompany')?>",
                 data: { term: $("#searchCompany").val()},
                 dataType: "json",
                 type: "POST",
                 success: function(data) {
                     //alert(JSON.stringify(data.searchrec));
                     //add(data.searchrec);
                     //console.log(data);
                     $(".ui-autocomplete").css('display','block');
                     response(data.searchrec);
                 }
             
             });
         },
         minLength: 2
     });*/
  $("#searchCompany").keyup(function(){
      var currentRequest = null;    
      var term = $(this).val();
      //var term = $(this).val().replace(/ +?/g, '');
      //var term = $(this).val($(this).val().replace(/ +?/g, ''));
      $("#searchresult").show();
      $('#selectcompany').hide();
       $(".LoadingImage").show();
      currentRequest = $.ajax({
         url: '<?php echo base_url();?>client/SearchCompany/',
         type: 'post',
         data: { 'term':term },
         beforeSend : function()    {           
         if(currentRequest != null) {
             currentRequest.abort();
         }
     },
         success: function( data ){
             $("#searchresult").html(data);
              $(".LoadingImage").hide();
             },
             error: function( errorThrown ){
                 console.log( errorThrown );
             }
         });
    });
  var xhr = null;
  function getCompanyRec(companyNo)
  {
     $(".LoadingImage").show();
      if( xhr != null ) {
                 xhr.abort();
                 xhr = null;
         }
      
     xhr = $.ajax({
         url: '<?php echo base_url();?>client/CompanyDetails/',
         type: 'post',
         data: { 'companyNo':companyNo },
         success: function( data ){
             //alert(data);
             $("#searchresult").hide();
             $('#companyprofile').show();
             $("#companyprofile").html(data);
             $(".LoadingImage").hide();
             
             },
             error: function( errorThrown ){
                 console.log( errorThrown );
             }
         });}
  
  
     function getCompanyView(companyNo)
  {
     //$('.modal-search').hide();
     $(".LoadingImage").show();
    var user_id= $("#user_id").val();
      if( xhr != null ) {
                 xhr.abort();
                 xhr = null;
         }
  
      
     xhr = $.ajax({
         url: '<?php echo base_url();?>client/selectcompany/',
         type: 'post',
         dataType: 'JSON',
         data: { 'companyNo':companyNo,'user_id': user_id},
         success: function( data ){
             //alert(data);
             $("#searchresult").hide();
             $('#companyprofile').hide();
             $('#selectcompany').show();
             $("#selectcompany").html(data.html);
             $('.main_contacts').html(data.html);
  // append form field value
  $("#company_house").val('1');
  $("#company_name").val(data.company_name);
  $("#company_name1").val(data.company_name);
  $("#company_number").val(data.company_number);
  $("#companynumber").val(data.company_number);
  $("#company_url").val(data.company_url);
  $("#company_url_anchor").attr("href",data.company_url);
  $("#officers_url").val(data.officers_url);
  $("#officers_url_anchor").attr("href",data.officers_url);
  $("#company_status").val(data.company_status);
  $("#company_type").val(data.company_type);
  //$("#company_sic").append(data.company_sic);
  $("#company_sic").val(data.company_sic);
  $("#sic_codes").val(data.sic_codes);
  
  $("#register_address").val(data.address1+"\n"+data.address2+"\n"+data.locality+"\n"+data.postal);
  $("#allocation_holder").val(data.allocation_holder);
  $("#date_of_creation").val(data.date_of_creation);
  $("#period_end_on").val(data.period_end_on);
  $("#next_made_up_to").val(data.next_made_up_to);
  $("#next_due").val(data.next_due);
  $("#confirm_next_made_up_to").val(data.confirm_next_made_up_to);
  $("#confirm_next_due").val(data.confirm_next_due);
  
  $("#tradingas").val(data.company_name);
  $("#business_details_nature_of_business").val(data.nature_business);
  $("#user_id").val(data.user_id);
  
  $(".LoadingImage").hide();
             
             },
             error: function( errorThrown ){
                 console.log( errorThrown );
             }
         });}
  
     function getmaincontact(companyNo,appointments,i,birth)
  {
     $(".LoadingImage").show();
     var user_id= $("#user_id").val();
      if( xhr != null ) {
                 xhr.abort();
                 xhr = null;
         }
             var cnt = $("#append_cnt").val();
  
           if(cnt==''){
           cnt = 1;
          }else{
           cnt = parseInt(cnt)+1;
          }
       $("#append_cnt").val(cnt);
  
       var incre=$("#incre").val();
       if(incre==''){
           incre = 1;
          }else{
           incre = parseInt(incre)+1;
          }
       $("#incre").val(incre);
      
     xhr = $.ajax({
         url: '<?php echo base_url();?>client/maincontact_info/',
         type: 'post',
         
         data: { 'companyNo':companyNo,'appointments': appointments,'user_id': user_id,'cnt':cnt,'birth':birth,'incre':incre},
         success: function( data ){
           $("#usecontact"+i).html('<span class="succ_contact"><a href="#">Added</a></span>');
             //$("#default-Modal,.modal-backdrop.show").hide();
               // $('#default-Modal').modal('hide');
                // $('#exist_person').modal('hide');
                $('.all_layout_modal').modal('hide');
  $('.contact_form').append(data);
  $(".LoadingImage").hide();
  
  //$('.contactexist').click(function(){
  
  
  
             
             },
             error: function( errorThrown ){
                 console.log( errorThrown );
             }
  
         });
  }
  
  
  
  function formSubmit ( formID ) {
  
  $(".LoadingImage").show();
  
  
  //var formId = $(this).closest("form").attr('id');
  
  var formData = $("#contact_exist"+formID).serialize();
  
  var Contact_id = $('#user_id').val();
  
  var data = {};
  data['Contact_id'] = Contact_id;
  data['formdata'] = formData;
  $.ajax({
  url: '<?php echo base_url();?>client/insert_clientcontact/'+Contact_id,
   type: "POST",
   data: data,
   success: function(data)  
   {
      $('.succ').hide();
      $("#succ").show();
  $(".LoadingImage").hide();
  
   }
  });
  
  //});
  }
  
  function make_a_primary(id,clientId)
  {
  $(".LoadingImage").show();
  
  //alert(id);
  var data = {};
  data['contact_id'] = id;
  data['clientId'] = clientId;
  
  $.ajax({
  url: '<?php echo base_url();?>client/update_primary_contact/',
   type: "POST",
   data: data,
   success: function(data)  
   {
        $("#contactss").html(data);
            $(".LoadingImage").hide();
            $(".contactcc").hide();
  
         // location.reload();
  
      //alert('success');
   }
  });
  
  }
  
  /**************************************************
  PACK ADD ROW 
  ***************************************************/   
  
  $('.yk_pack_addrow').click(function(e)
  {
  e.preventDefault();
  
  
  /*data = '<tr class="success ykpackrow"><td><select name="team[]" id="team"  placeholder="Select"><?php foreach($teamlist as $teamkey => $teamval) {?><option value="<?php echo $teamval["id"];?>"><?php echo $teamval["team"];?></option><?php } ?></select></td><td><input type="text" name="allocation_holder[]" id="allocation_holder" ></td><td><button type="button" class="btn btn-danger yk_pack_delrow">Delete</button></td><tr>';
  $(this).parents('.pack_add_row_wrpr_member').before(data);*/
  
  
  
  $(this).parents('.pack_add_row_wrpr').before('<tr class="success ykpackrow"><td><select name="team" id="team"  placeholder="Select"><option value="">--select--</option><?php foreach($teamlist as $teamkey => $teamval) {?><option value="<?php echo $teamval["id"];?>"><?php echo $teamval["team"];?></option><?php } ?></select></td><td><input type="text" name="allocation_holder[]" id="allocation_holder" ></td><td><button type="button" class="btn btn-danger yk_pack_delrow">Delete</button></td><tr>');
  
  
  });
  
  /**************************************************
  PACK REMOVE ROW 
  ***************************************************/   
  
  $(document).on('click','.yk_pack_delrow',function(e)
  {
  $(this).parents('.ykpackrow').remove();
  
  });
  
  /**************************************************
  depart ADD ROW 
  ***************************************************/   
  
  $('.yk_pack_addrow_depart').click(function(e)
  {
  e.preventDefault();
  
  $(this).parents('.pack_add_row_wrpr_depart').before('<tr class="success ykpackrow_depart"><td><select name="depart" id="depart" class="fields"><option value="">--select--</option><?php foreach ($deptlist as $deptlistkey => $deptlistvalue) { ?><option value="<?php echo $deptlistvalue["id"];?>" ><?php echo $deptlistvalue["department"];?></option><?php } ?></select></td><td><input type="text" name="allocation_holder_dept[]" id="allocation_holder_dept" ></td><td><button type="button" class="btn btn-danger yk_pack_delrow_depart">Delete</button></td></tr>');
  
  
  });
  
  /**************************************************
  depart REMOVE ROW 
  ***************************************************/   
  
  $(document).on('click','.yk_pack_delrow_depart',function(e)
  {
  $(this).parents('.ykpackrow_depart').remove();
  
  });
  
  
  /**************************************************
  PACK ADD ROW 
  ***************************************************/   
  
  $('.yk_pack_addrow_user').click(function(e)
  {
  e.preventDefault();
  /*var rec = '<div class="dropdown-sin-2">
                             <select style="display:none" name="manager_reviewer[]" id="manager_reviewer" multiple placeholder="Select">'
  <?php foreach($staff_form as $managers) {
    ?>
                                '<option value="<?php echo $managers['id']?>"><?php echo $managers['crm_name'];?></option>'
                                <?php } ?>
                                
                            '</select>
                          </div> ';*/
                          //var rec = '';
                          //  data = $('.append_responsible_staff').html();
                          data='<tr class="success ykpackrow_user append_responsible_staff "><td><div class="dropdown-sin-2"><select style="display:block" name="manager_reviewer" id="manager_reviewer"  placeholder="Select"><?php foreach($staff_form as $managers) {?><option value="<?php echo $managers["id"]?>"><?php echo $managers["crm_name"];?></option><?php } ?></select></div></td><td><div class="dropdown-sin-2"><select style="display:block" name="assign_managed" id="assign_managed"  placeholder="Select"><?php foreach($managed_by as $managedby) {?><option value="<?php echo $managedby["id"]?>"><?php echo $managedby["crm_name"];?></option><?php } ?></select></div></td><td><button type="button" class="btn btn-danger yk_pack_delrow_user">Delete</button></td></tr>';
  $(this).parents('.pack_add_row_wrpr_user').before(data);
  
  
  });
  
  /**************************************************
  PACK REMOVE ROW 
  ***************************************************/   
  
  $(document).on('click','.yk_pack_delrow_user',function(e)
  {
  $(this).parents('.ykpackrow_user').remove();
  
  });
  
  /**************************************************
  member ADD ROW 
  ***************************************************/   
  
  $('.yk_pack_addrow_member').click(function(e)
  {
  e.preventDefault();
  
                           data = '<tr class="success ykpackrowfirsts ykpackrow_member"><td><div class="dropdown-sin-2"><select style="display:block" name="manager_reviewer_member" id="manager_reviewer_member"  placeholder="Select"><?php foreach($referby as $referbykey => $referbyvalue) {?><option value="<?php echo $referbyvalue["crm_first_name"];?>"><?php echo $referbyvalue["crm_first_name"];?></option><?php } ?></select></div></td><td><div class="dropdown-sin-2"><input type="text" name="assign_managed_member[]" id="assign_managed_member" placeholder="" value="" class="fields"></div></td><td><button type="button" class="btn btn-danger yk_pack_delrow_member">Delete</button></td><tr>';
  $(this).parents('.pack_add_row_wrpr_member').before(data);
  
  
  });
  
  /**************************************************
  member REMOVE ROW 
  ***************************************************/   
  
  $(document).on('click','.yk_pack_delrow_member',function(e)
  {
  $(this).parents('.ykpackrow_member').remove();
  
  });
  
  
  /**************************************************
  landline ADD ROW 
  ***************************************************/   
  
  //$('.yk_pack_addrow_landline').click(function(e)
  $(document).on('click','.yk_pack_addrow_landline',function(e)
  {
  e.preventDefault();
  var id=$(this).data('id');
  $(this).parents('.pack_add_row_wrpr_landline').before('<span class="primary-inner success ykpackrow_landline"><label>landline</label><input type="text" class="text-info" name="landline'+id+'[]" id="landline" value=""><a href="javascript:;" class="btn btn-danger yk_pack_delrow_landline"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true"></i></a></span>');
  
  
  });
  
  /**************************************************
  landline REMOVE ROW 
  ***************************************************/   
  
  $(document).on('click','.yk_pack_delrow_landline',function(e)
  {
  $(this).parents('.ykpackrow_landline').remove();
  
  });
  
  /**************************************************
  email ADD ROW 
  ***************************************************/   
  
  //$('.yk_pack_addrow_email').click(function(e)
  $(document).on('click','.yk_pack_addrow_email',function(e)
  {
  e.preventDefault();
  var id=$(this).data('id');
  $(this).parents('.pack_add_row_wrpr_email').before('<span class="primary-inner success ykpackrow_email"><label>Work email</label><input type="text" class="text-info" name="work_email'+id+'[]" id="work_email"  value=""><a href="javascript:;" class="btn btn-danger yk_pack_delrow_email"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true"></i></a></span>');
  
  
  });
  
  /**************************************************
  email REMOVE ROW 
  ***************************************************/   
  
  $(document).on('click','.yk_pack_delrow_email',function(e)
  {
  $(this).parents('.ykpackrow_email').remove();
  
  });
  $('.dropdown-sin-21').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });

  $('.dropdown-sin-2').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });

</script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
  $("#alltask").dataTable({
        "iDisplayLength": 10,
   //  "scrollX": true,
     });

   $("#allleads").dataTable({
        "iDisplayLength": 10,
    // "scrollX": true,
     });

    $("#allproposal").dataTable({
        "iDisplayLength": 10,
     //"scrollX": true,
     });
    $('#home_table').dataTable({
        "iDisplayLength": 10,
     //"scrollX": true,
     });
  });
</script>
<script type="text/javascript">
  function check_checkbox()
         {
  
                 if($('[name="confirmation_next_reminder_date"]:checked').length < 1)
                 {
                   $("#add_custom_reminder").fadeIn(2000);
                 $("#add_custom_reminder_label").fadeIn(2000);              
                 $("#add_custom_reminder_link").hide();
                 }else
                 {
                    $("#add_custom_reminder").hide();
                    $("#add_custom_reminder_label").hide();
                    $("#add_custom_reminder_link").fadeIn(2000);
                 }
  
   
        }
         function check_checkbox1()
         {
  
                 if($('[name="accounts_next_reminder_date"]:checked').length < 1)
                 {
                   $("#accounts_custom_reminder").fadeIn(2000);
                 $("#accounts_custom_reminder_label").fadeIn(2000);
                 $("#accounts_custom_reminder_link").hide();             
  
                 }else
                 {
                    $("#accounts_custom_reminder").hide();
                    $("#accounts_custom_reminder_label").hide();
                    $("#accounts_custom_reminder_link").fadeIn(2000);
                 }
  
   
        }
         function check_checkbox2()
         {
  
                 if($('[name="company_next_reminder_date"]:checked').length < 1)
                 {
                    //alert("fdg");
                   $("#company_custom_reminder").fadeIn(2000);
                 $("#company_custom_reminder_label").fadeIn(2000);
                 $("#company_custom_reminder_link").hide();              
  
                 }else
                 {
                    $("#company_custom_reminder").hide();
                    $("#company_custom_reminder_label").hide();
                    $("#company_custom_reminder_link").fadeIn(2000);
                 }
  
   
        }
        function check_checkbox3()
         {
  
                 if($('[name="Personal_next_reminder_date"]:checked').length < 1)
                 {
                   $("#personal_custom_reminder").fadeIn(2000);
                 $("#personal_custom_reminder_label").fadeIn(2000);
                 $("#personal_custom_reminder_link").hide();             
  
                 }else
                 {
                    $("#personal_custom_reminder").hide();
                    $("#personal_custom_reminder_label").hide();
                    $("#personal_custom_reminder_link").fadeIn(2000);
                 }
  
   
        }
        function check_checkbox4()
         {
  
                 if($('[name="Payroll_next_reminder_date"]:checked').length < 1)
                 {
                   $("#payroll_add_custom_reminder").fadeIn(2000);
                 $("#payroll_add_custom_reminder_label").fadeIn(2000);
                 $("#payroll_add_custom_reminder_link").hide();             
  
                 }else
                 {
                    $("#payroll_add_custom_reminder").hide();
                    $("#payroll_add_custom_reminder_label").hide();
                    $("#payroll_add_custom_reminder_link").fadeIn(2000);
                 }
  
   
        }
        function check_checkbox5()
         {
  
                 if($('[name="Pension_next_reminder_date"]:checked').length < 1)
                 {
                   $("#pension_add_custom_reminder").fadeIn(2000);
                 $("#pension_create_task_reminder_label").fadeIn(2000);
                 $("#pension_create_task_reminder_link").hide();            
  
                 }else
                 {
                    $("#pension_add_custom_reminder").hide();
                    $("#pension_create_task_reminder_label").hide();
                    $("#pension_create_task_reminder_link").fadeIn(2000);
                 }
  
   
        }
        function check_checkbox6()
         {
  
                 if($('[name="Cis_next_reminder_date"]:checked').length < 1)
                 {
                   $("#cis_add_custom_reminder").fadeIn(2000);
                 $("#cis_add_custom_reminder_label").fadeIn(2000);
                 $("#cis_add_custom_reminder_link").hide();              
  
                 }else
                 {
                    $("#cis_add_custom_reminder").hide();
                    $("#cis_add_custom_reminder_label").hide();
                    $("#cis_add_custom_reminder_link").fadeIn(2000);
                 }
  
   
        }
        function check_checkbox7()
         {
  
                 if($('[name="P11d_next_reminder_date"]:checked').length < 1)
                 {
                   $("#p11d_add_custom_reminder").fadeIn(2000);
                 $("#p11d_add_custom_reminder_label").fadeIn(2000);
                 $("#p11d_add_custom_reminder_link").hide();             
  
                 }else
                 {
                    $("#p11d_add_custom_reminder").hide();
                    $("#p11d_add_custom_reminder_label").hide();
                    $("#p11d_add_custom_reminder_link").fadeIn(2000);
                 }
  
   
        }
        function check_checkbox8()
         {
  
                 if($('[name="bookkeep_next_reminder_date"]:checked').length < 1)
                 {
                   $("#bookkeep_add_custom_reminder").fadeIn(2000);
                 $("#bookkeep_add_custom_reminder_label").fadeIn(2000);
                 $("#bookkeep_add_custom_reminder_link").hide();            
  
                 }else
                 {
                    $("#bookkeep_add_custom_reminder").hide();
                    $("#bookkeep_add_custom_reminder_label").hide();
                    $("#bookkeep_add_custom_reminder_link").fadeIn(2000);
                 }
  
   
        }
        function check_checkbox9()
         {
  
                 if($('[name="manage_next_reminder_date"]:checked').length < 1)
                 {
                   $("#manage_add_custom_reminder").fadeIn(2000);
                 $("#manage_add_custom_reminder_label").fadeIn(2000);
                 $("#manage_add_custom_reminder_link").hide();              
  
                 }else
                 {
                    $("#manage_add_custom_reminder").hide();
                    $("#manage_add_custom_reminder_label").hide();
                    $("#manage_add_custom_reminder_link").fadeIn(2000);
                 }
  
   
        }
        function check_checkbox10()
         {
  
                 if($('[name="Vat_next_reminder_date"]:checked').length < 1)
                 {
                   $("#vat_add_custom_reminder").fadeIn(2000);
                 $("#vat_add_custom_reminder_label").fadeIn(2000);
                 $("#vat_add_custom_reminder_link").hide();              
  
                 }else
                 {
                    $("#vat_add_custom_reminder").hide();
                    $("#vat_add_custom_reminder_label").hide();
                    $("#vat_add_custom_reminder_link").fadeIn(2000);
                 }
  
   
        }
        function check_checkbox11()
         {
  
                 if($('[name="Declatrion_next_reminder_date"]:checked').length < 1)
                 {
                   $("#Declration_add_custom_reminder").fadeIn(2000);
                 $("#Declration_add_custom_reminder_label").fadeIn(2000);
                 $("#Declration_add_custom_reminder_link").hide();             
  
                 }else
                 {
                    $("#Declration_add_custom_reminder").hide();
                    $("#Declration_add_custom_reminder_label").hide();
                    $("#Declration_add_custom_reminder_link").fadeIn(2000);
                 }
  
   
        }
        function check_checkbox12()
         {
  
                 if($('[name="cissub_next_reminder_date"]:checked').length < 1)
                 {
                   $("#cissub_add_custom_reminder").fadeIn(2000);
                 $("#cissub_add_custom_reminder_label").fadeIn(2000);
                 $("#cissub_add_custom_reminder_link").hide();              
  
                 }else
                 {
                    $("#cissub_add_custom_reminder").hide();
                    $("#cissub_add_custom_reminder_label").hide();
                    $("#cissub_add_custom_reminder_link").fadeIn(2000);
                 }
  
   
        }
  
        function check_checkbox13()
         {
  
                 if($('[name="insurance_next_reminder_date"]:checked').length < 1)
                 {
                   $("#insurance_add_custom_reminder").fadeIn(2000);
                 $("#insurance_add_custom_reminder_label").fadeIn(2000);
                 $("#insurance_add_custom_reminder_link").hide();              
  
                 }else
                 {
                    $("#insurance_add_custom_reminder").hide();
                    $("#insurance_add_custom_reminder_label").hide();
                    $("#insurance_add_custom_reminder_link").fadeIn(2000);
                 }
  
   
        }
        function check_checkbox14()
         {
  
                 if($('[name="registered_next_reminder_date"]:checked').length < 1)
                 {
                   $("#registered_add_custom_reminder").fadeIn(2000);
                 $("#registered_add_custom_reminder_label").fadeIn(2000);
                 $("#registered_add_custom_reminder_link").hide();              
  
                 }else
                 {
                    $("#registered_add_custom_reminder").hide();
                    $("#registered_add_custom_reminder_label").hide();
                    $("#registered_add_custom_reminder_link").fadeIn(2000);
                 }
  
   
        }
        
        function check_checkbox15()
         {
  
                 if($('[name="investigation_next_reminder_date"]:checked').length < 1)
                 {
                   $("#investigation_add_custom_reminder").fadeIn(2000);
                 $("#investigation_add_custom_reminder_label").fadeIn(2000);
                 $("#investigation_add_custom_reminder_link").hide();              
  
                 }else
                 {
                    $("#investigation_add_custom_reminder").hide();
                    $("#investigation_add_custom_reminder_label").hide();
                    $("#investigation_add_custom_reminder_link").fadeIn(2000);
                 }
  
   
        }
     
     
</script>
<script type="text/javascript">
  $( document ).ready(function() {
  
      $("#add_custom_reminder").hide();
      $("#add_custom_reminder_label").hide();
  
      $("#accounts_custom_reminder").hide();
      $("#accounts_custom_reminder_label").hide();
  
      $("#company_custom_reminder").hide();
      $("#company_custom_reminder_label").hide();
  
      $("#personal_custom_reminder").hide();
      $("#personal_custom_reminder_label").hide();
  
      $("#payroll_add_custom_reminder").hide();
      $("#payroll_add_custom_reminder_label").hide();
  
      $("#pension_add_custom_reminder").hide();
      $("#pension_create_task_reminder_label").hide();
  
      $("#cis_add_custom_reminder").hide();
      $("#cis_add_custom_reminder_label").hide();
  
      $("#p11d_add_custom_reminder").hide();
      $("#p11d_add_custom_reminder_label").hide();
  
      $("#bookkeep_add_custom_reminder").hide();
      $("#bookkeep_add_custom_reminder_label").hide();
  
      $("#manage_add_custom_reminder").hide();
      $("#manage_add_custom_reminder_label").hide();
  
      $("#vat_add_custom_reminder").hide();
      $("#vat_add_custom_reminder_label").hide();
  
       $("#Declration_add_custom_reminder").hide();
      $("#Declration_add_custom_reminder_label").hide();
  
       $("#cissub_add_custom_reminder").hide();
      $("#cissub_add_custom_reminder_label").hide();
  
      $("#insurance_add_custom_reminder").hide();
      $("#insurance_add_custom_reminder_label").hide();
  
      $("#registered_add_custom_reminder").hide();
      $("#registered_add_custom_reminder_label").hide();
  
      $("#investigation_add_custom_reminder").hide();
      $("#investigation_add_custom_reminder_label").hide();
  
     });
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.1.8/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/masonry/3.3.1/masonry.pkgd.min.js"></script>

<script>
    
$(document).ready(function(){
    var $grid = $('.masonry-container').masonry({
    itemSelector: '.accordion-panel',
    percentPosition: true,
    columnWidth: '.grid-sizer'
    });

    $(document).on('click', '.add_acc_client', function(){
             $('.accordion-panel').css("transform", "");
           setTimeout(function(){ 
           
              $grid11.masonry('layout'); 
            
   
           }, 600);
         });
         
          $(document).on('click', '.switchery', function(){
   
           setTimeout(function(){ 
              $grid11.masonry('reloadItems');
              $grid11.masonry('layout'); 
            $('.accordion-panel').css("transform", "");
   
           }, 600);  
          });


        $(document).on( 'click', 'ol .nav-item a', function() {
          setTimeout(function(){ $grid.masonry('layout'); }, 500);
        });

        $(document).on( 'change', '.main-pane-border1', function() {
          setTimeout(function(){ $grid.masonry('layout'); }, 500);
        });

      setTimeout(function(){ $grid.masonry('layout'); }, 600);  
   
      $(document).on( 'click', '.body-popup-content .switchery', function() {
        setTimeout(function(){ $grid.masonry('layout'); }, 600);
       });
   
      $(document).on( 'click', '.switchery', function() {
      $('.accordion-panel').css("transform", "");
      });
       $(document).on( 'change', '#select_responsible_type', function() {
      $('.accordion-panel').css("transform", "");
      });
      $(document).on( 'click', '.addnewclient_pages1 nav li a', function() {
      $('.accordion-panel').css("transform", "");
      });
   
      $(document).on( 'click', 'td.splwitch .switchery', function() {
   
        $('.body-popup-content .accordion-panel').css("transform", "");
  
   
         var $grid = $('.body-popup-content  .masonry-container').masonry({
         itemSelector: '.body-popup-content  .accordion-panel',
         percentPosition: true,
         columnWidth: '.body-popup-content  .grid-sizer' 
       });
   
         $grid.masonry('reloadItems');
             
         setTimeout(function(){ $grid.masonry('layout'); }, 600);  
   
       });

});
</script> 
<script type="text/javascript">
  function confirm_delete(id) 
  {
    $('#delete_proposal'+id).show();
    return false;
  }

  $(document).on('click','#close',function(e)
   {
   $('.alert-success').hide();
   return false;
   });
</script>
<!-- for timeline services -->
<script type="text/javascript">
   //$('.Note-search_timeline').on('keyup',function(){
    $(document).on('keyup','.Note-search_timeline',function(){
       //alert('121');
       var txt=$(this).val();
       var data_id=$(this).attr('id');
   //alert(data_id);
   $('.overall_'+data_id).hide();
   $('.overall_for_'+data_id).html('');
   $('.overall_for_'+data_id).css('display','block');
   
         $('.'+data_id).each(function(){
              if($(this).text().toUpperCase().indexOf(txt.toUpperCase()) != -1){
                   $(this).show();
                  console.log($(this).html());
                  $('.overall_for_'+data_id).append(' <div class="cd-timeline-block for_<?php echo $key; ?>">'+$(this).html()+'</div>');
                  //$(this).unwrap();
   
              }
              else
              {
                $(this).hide();
           //  $(this).wrap( "<!-- -->" );
              // $("<!-- ").insertAfter($(this));
             //  $(this).html('<!--'+$(this).html()+'-->');
              }
           });
   });


        $(document).on('keyup','.task-search_timeline',function(){
      alert('121');
       var txt=$(this).val();
       var data_id=$(this).attr('id');
   alert(data_id);
   $('.overall_'+data_id).hide();
   $('.overall_for_'+data_id).html('');
   $('.overall_for_'+data_id).css('display','block');
   
         $('.'+data_id).each(function(){
              if($(this).text().toUpperCase().indexOf(txt.toUpperCase()) != -1){
                   $(this).show();
                  console.log($(this).html());
                  $('.overall_for_'+data_id).append(' <div class="cd-timeline-block for_<?php echo $key; ?>">'+$(this).html()+'</div>');
                  //$(this).unwrap();
   
              }
              else
              {
                $(this).hide();
           //  $(this).wrap( "<!-- -->" );
              // $("<!-- ").insertAfter($(this));
             //  $(this).html('<!--'+$(this).html()+'-->');
              }
           });
   });




    
      $(document).on('keyup','.Calllog-search_timeline',function(){
       //alert('121');
       var txt=$(this).val();

      // alert(txt);
       var data_id=$(this).attr('id');
 // alert(data_id);
   $('.overall_'+data_id).hide();
   $('.overall_for_'+data_id).html('');
   $('.overall_for_'+data_id).css('display','block');
   
         $('.'+data_id).each(function(){
              if($(this).text().toUpperCase().indexOf(txt.toUpperCase()) != -1){
                   $(this).show();
                  console.log($(this).html());
                  $('.overall_for_'+data_id).append(' <div class="cd-timeline-block for_<?php echo $key; ?>">'+$(this).html()+'</div>');
                  //$(this).unwrap();
   
              }
              else
              {
                $(this).hide();
           //  $(this).wrap( "<!-- -->" );
              // $("<!-- ").insertAfter($(this));
             //  $(this).html('<!--'+$(this).html()+'-->');
              }
           });
   });


       $(document).on('keyup','.Sms-search_timeline',function(){
     //  alert('121');
       var txt=$(this).val();

    //   alert(txt);
       var data_id=$(this).attr('id');
 // alert(data_id);
   $('.overall_'+data_id).hide();
   $('.overall_for_'+data_id).html('');
   $('.overall_for_'+data_id).css('display','block');
   
         $('.'+data_id).each(function(){
              if($(this).text().toUpperCase().indexOf(txt.toUpperCase()) != -1){
                   $(this).show();
                  console.log($(this).html());
                  $('.overall_for_'+data_id).append(' <div class="cd-timeline-block for_<?php echo $key; ?>">'+$(this).html()+'</div>');
                  //$(this).unwrap();
   
              }
              else
              {
                $(this).hide();
           //  $(this).wrap( "<!-- -->" );
              // $("<!-- ").insertAfter($(this));
             //  $(this).html('<!--'+$(this).html()+'-->');
              }
           });
   });


       $(document).on('keyup','.meeting-search_timeline',function(){
     //  alert('121');
       var txt=$(this).val();

       alert(txt);
       var data_id=$(this).attr('id');
  alert(data_id);
   $('.overall_'+data_id).hide();
   $('.overall_for_'+data_id).html('');
   $('.overall_for_'+data_id).css('display','block');
   
         $('.'+data_id).each(function(){
              if($(this).text().toUpperCase().indexOf(txt.toUpperCase()) != -1){
                   $(this).show();
                  console.log($(this).html());
                  $('.overall_for_'+data_id).append(' <div class="cd-timeline-block for_<?php echo $key; ?>">'+$(this).html()+'</div>');
                  //$(this).unwrap();
   
              }
              else
              {
                $(this).hide();
           //  $(this).wrap( "<!-- -->" );
              // $("<!-- ").insertAfter($(this));
             //  $(this).html('<!--'+$(this).html()+'-->');
              }
           });
   });






         $(document).on('keyup','.Email-search_timeline',function(){
      // alert('121');
       var txt=$(this).val();

    //   alert(txt);
       var data_id=$(this).attr('id');
 // alert(data_id);
   $('.overall_'+data_id).hide();
   $('.overall_for_'+data_id).html('');
   $('.overall_for_'+data_id).css('display','block');
   
         $('.'+data_id).each(function(){
              if($(this).text().toUpperCase().indexOf(txt.toUpperCase()) != -1){
                   $(this).show();
                  console.log($(this).html());
                  $('.overall_for_'+data_id).append(' <div class="cd-timeline-block for_<?php echo $key; ?>">'+$(this).html()+'</div>');
                  //$(this).unwrap();
   
              }
              else
              {
                $(this).hide();
           //  $(this).wrap( "<!-- -->" );
              // $("<!-- ").insertAfter($(this));
             //  $(this).html('<!--'+$(this).html()+'-->');
              }
           });
   
   
   
   
   });

      $(document).on('keyup','.log-search_timeline',function(){
       //alert('121');
       var txt=$(this).val();
       var data_id=$(this).attr('id');
   //alert(data_id);
   $('.overall_'+data_id).hide();
   $('.overall_for_'+data_id).html('');
   $('.overall_for_'+data_id).css('display','block');
   
         $('.'+data_id).each(function(){
              if($(this).text().toUpperCase().indexOf(txt.toUpperCase()) != -1){
                   $(this).show();
                  console.log($(this).html());
                  $('.overall_for_'+data_id).append(' <div class="cd-timeline-block for_<?php echo $key; ?>">'+$(this).html()+'</div>');
                  //$(this).unwrap();
   
              }
              else
              {
                $(this).hide();
           //  $(this).wrap( "<!-- -->" );
              // $("<!-- ").insertAfter($(this));
             //  $(this).html('<!--'+$(this).html()+'-->');
              }
           });
   
   
   
   
   });
        $(document).on('keyup','.log-search_timeline',function(){
       //alert('121');
       var txt=$(this).val();
       var data_id=$(this).attr('id');
   //alert(data_id);
   $('.overall_'+data_id).hide();
   $('.overall_for_'+data_id).html('');
   $('.overall_for_'+data_id).css('display','block');
   
         $('.'+data_id).each(function(){
              if($(this).text().toUpperCase().indexOf(txt.toUpperCase()) != -1){
                   $(this).show();
                  console.log($(this).html());
                  $('.overall_for_'+data_id).append(' <div class="cd-timeline-block for_<?php echo $key; ?>">'+$(this).html()+'</div>');
                  //$(this).unwrap();
   
              }
              else
              {
                $(this).hide();
           //  $(this).wrap( "<!-- -->" );
              // $("<!-- ").insertAfter($(this));
             //  $(this).html('<!--'+$(this).html()+'-->');
              }
           });
   
   
   
   
   });
               $(document).on('keyup','.allactivitylog-search_timeline',function(){
       //alert('121');
       var txt=$(this).val();
       var data_id=$(this).attr('id');
   //alert(data_id);
   $('.overall_'+data_id).hide();
   $('.overall_for_'+data_id).html('');
   $('.overall_for_'+data_id).css('display','block');
   
         $('.'+data_id).each(function(){
              if($(this).text().toUpperCase().indexOf(txt.toUpperCase()) != -1){
                   //$(this).show();
                  console.log($(this).html());
                  $('.overall_for_'+data_id).append(' <div class="cd-timeline-block for_<?php echo $key; ?>">'+$(this).html()+'</div>');
                  //$(this).unwrap();
   
              }
              else
              {
              //  $(this).hide();
           //  $(this).wrap( "<!-- -->" );
              // $("<!-- ").insertAfter($(this));
             //  $(this).html('<!--'+$(this).html()+'-->');
              }
           });
   
   
   
   
   });

$(document).ready(function() {
    $( "#tabs221" ).tabs();
  });


 $( document ).ready(function() {
// $ ('#tabs-7').click(function(){
//     alert('hi');
//                 var date = new Date();
//                 var d = date.getDate();
//                 var m = date.getMonth();
//                 var y = date.getFullYear();
//                 $('#calendar').fullCalendar({
//                 header: {
//                 left: 'prev,next today',
//                 center: 'title',
//                 right: 'month,agendaWeek,agendaDay'
//                 },
//                 editable: true,
//                 theme: true
//         });

//   });

});
/** 05-09-2018 **/
/** for timeline services **/
    $('.dropdown-sin-12').dropdown({
    // limitCount: 5,
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });
    /** end of timeline services **/
    /** notes section add and edit option **/
$(document).on('click','.notes_section_insert,.notes_section_update',function(){
  var user_id=$('#timeline_client_id').val();
var notes=CKEDITOR.instances['editor1'].getData();
var services_type=$('#services_type').val();
var update_notes_id=$("#update_notes_id").val();
  if(notes == '')
  {
     $('.notes_errormsg').html('Enter Your Notes');
  }
  else
  {
    $('.notes_errormsg').html('');
  }
  if(services_type=='')
  {
     $('.services_type_errormsg').html('Select Services Type ');
  }
  else
  {
     $('.services_type_errormsg').html('');
  }
  if(notes!='' && services_type!='')
  {
         $.ajax({
                              url: '<?php echo base_url();?>client/client_timeline_notes_add/',
                              dataType : 'json',
                              type : 'POST',
                              data : {'notes':notes,'services_type':services_type,'user_id':user_id,'update_notes_id':update_notes_id},
                              success: function(data) {
                                
                                $('.for_services_type').html('');
                                CKEDITOR.instances['editor1'].setData('');
                                $('.for_services_type').html('<label>Services Type</label><div class="dropdown-sin-12 lead-form-st"><select name="services_type" id="services_type" class="form-control fields" placeholder="Select Services Type"><option value="">--Select Service Type--</option>   <?php foreach ($client_services as $clse_key => $clse_value) {
                                         ?><option value="<?php echo $clse_key;?>"><?php echo $clse_value; ?></option><?php } ?></select></div><span class="services_type_errormsg error_msg" style="color: red;"></span>');
                                $('.dropdown-sin-12').dropdown({
                                        input: '<input type="text" maxLength="20" placeholder="Search">'
                                   });
                                $('.alert-success-reminder').css('display','block');
                                if(update_notes_id!=''){  $('.timeline_success_messages').html('Notes Updated successfully'); }
                                else{  $('.timeline_success_messages').html('Notes Added successfully'); }
                               
                                setTimeout(function() { 
                                         $('.alert-success-reminder').css('display','none');
                                             }, 2000); 
                                $('#notes_section').load("<?php echo base_url(); ?>client/jquery_append_client_timeline_services/"+user_id+"/notes");
                                $('.timeline_add').css('display','');
                                $('.timeline_update').css('display','none');
                                 },
                            });
  }
  return false;
});

/** calllog section add and update action **/
$(document).on('click','.calllog_section_insert,.calllog_section_update',function(){
  var user_id=$('#timeline_client_id').val();
var notes=CKEDITOR.instances['editor21'].getData();
//var services_type=$('#services_type').val();
var update_calllog_id=$("#update_calllog_id").val();
  if(notes == '')
  {
    $('.calllog_errormsg').html('Enter Your Notes');
  }
  else
  {    
    $('.calllog_errormsg').html('');
         $.ajax({
                  url: '<?php echo base_url();?>client/client_timeline_calllog_add/',
                  dataType : 'json',
                  type : 'POST',
                  data : {'notes':notes,'user_id':user_id,'update_calllog_id':update_calllog_id},
                  success: function(data) {
                   //alert('zzz');
                    CKEDITOR.instances['editor21'].setData('');
                    
                    $('.alert-success-reminder').css('display','block');
                    if(update_calllog_id!=''){  $('.timeline_success_messages').html('Notes Updated successfully'); }
                    else{  $('.timeline_success_messages').html('Notes Added successfully'); }
                   /** 15-09-2018  **/
                       $('.timelinecall_add').css('display','');
                       $('.timelinecall_update').css('display','none');
                      // $('#update_notes_id').val(note_id);
                   /** end of 15-09-2018 **/
                    setTimeout(function() { 
                    $('.alert-success-reminder').css('display','none');
                                 }, 2000); 
                    $('#calllog_section').load("<?php echo base_url(); ?>client/jquery_append_client_timeline_services/"+user_id+"/calllog");

                     },
                });
  }
  return false;
});


$(document).on('click','.sms_section_insert,.sms_section_update',function(){
  var user_id=$('#timeline_client_id').val();
var notes=CKEDITOR.instances['editor51'].getData();
//var services_type=$('#services_type').val();
var update_calllog_id=$("#update_sms_id").val();

var mobile_number=$("#mobile_number").val();
  if(notes == '')
  {
    $('.sms_errormsg').html('Enter Your Notes');
  }
  else
  {    
    $('.sms_errormsg').html('');
         $.ajax({
                  url: '<?php echo base_url();?>client/client_timeline_sms_add/',
                  dataType : 'json',
                  type : 'POST',
                  data : {'notes':notes,'user_id':user_id,'update_calllog_id':update_calllog_id,'mobile_number':mobile_number},
                  success: function(data) {
                   //alert('zzz');
                    CKEDITOR.instances['editor51'].setData('');
                    
                    $('.alert-success-reminder').css('display','block');
                    if(update_calllog_id!=''){  $('.timeline_success_messages').html('Notes Updated successfully'); }
                    else{  $('.timeline_success_messages').html('Notes Added successfully'); }
                   /** 15-09-2018  **/
                       $('.timelinesms_add').css('display','');
                       $('.timelinesms_update').css('display','none');
                      // $('#update_notes_id').val(note_id);
                   /** end of 15-09-2018 **/
                    setTimeout(function() { 
                    $('.alert-success-reminder').css('display','none');
                                 }, 2000); 
                    $('#calllog_section').load("<?php echo base_url(); ?>client/jquery_append_client_timeline_services/"+user_id+"/calllog");

                     },
                });
  }
  return false;
});



$(document).on('click','.meeting_section_insert,.meeting_section_update',function(){
  var user_id=$('#timeline_client_id').val();
var notes=CKEDITOR.instances['editor61'].getData();
//var services_type=$('#services_type').val();
var update_calllog_id=$("#update_sms_id").val();

var meeting_date=$("#meeting_date").val();
  if(notes == '')
  {
    $('.meeting_errormsg').html('Enter Your Notes');
  }
  else
  {    
    $('.meeting_errormsg').html('');
         $.ajax({
                  url: '<?php echo base_url();?>client/client_timeline_meeting_add/',
                  dataType : 'json',
                  type : 'POST',
                  data : {'notes':notes,'user_id':user_id,'update_calllog_id':update_calllog_id,'meeting_date':meeting_date},
                  success: function(data) {
                   //alert('zzz');
                    CKEDITOR.instances['editor61'].setData('');
                    
                    $('.alert-success-reminder').css('display','block');
                    if(update_calllog_id!=''){  $('.timeline_success_messages').html('Notes Updated successfully'); }
                    else{  $('.timeline_success_messages').html('Notes Added successfully'); }
                   /** 15-09-2018  **/
                       $('.timelinemeeting_add').css('display','');
                       $('.timelinemeeting_update').css('display','none');
                      // $('#update_notes_id').val(note_id);
                   /** end of 15-09-2018 **/
                    setTimeout(function() { 
                    $('.alert-success-reminder').css('display','none');
                                 }, 2000); 
                    $('#calllog_section').load("<?php echo base_url(); ?>client/jquery_append_client_timeline_services/"+user_id+"/calllog");

                     },
                });
  }
  return false;
});
/** end of calllog add and update **/
/**17-09-2018 **/
/** calllog section add and update action **/
$(document).on('click','.log_section_insert,.log_section_update',function(){
  var user_id=$('#timeline_client_id').val();
var notes=CKEDITOR.instances['editor22'].getData();
//var services_type=$('#services_type').val();
var update_log_id=$("#update_log_id").val();
  if(notes == '')
  {
    $('.log_errormsg').html('Enter Your Notes');
  }
  else
  {    
    $('.log_errormsg').html('');
         $.ajax({
                  url: '<?php echo base_url();?>client/client_timeline_log_add/',
                  dataType : 'json',
                  type : 'POST',
                  data : {'notes':notes,'user_id':user_id,'update_log_id':update_log_id},
                  success: function(data) {
                   //alert('zzz');
                    CKEDITOR.instances['editor22'].setData('');
                    var zz=CKEDITOR.instances['editor22'].getData();
                  //  alert(zz);
                    
                    $('.alert-success-reminder').css('display','block');
                    if(update_log_id!=''){  $('.timeline_success_messages').html('Notes Updated successfully'); }
                    else{  $('.timeline_success_messages').html('Notes Added successfully'); }
                   /** 15-09-2018  **/
                       $('.timelinelog_add').css('display','');
                       $('.timelinelog_update').css('display','none');
                      // $('#update_notes_id').val(note_id);
                   /** end of 15-09-2018 **/
                    setTimeout(function() { 
                    $('.alert-success-reminder').css('display','none');
                                 }, 2000); 
                    $('#log_section').load("<?php echo base_url(); ?>client/jquery_append_client_timeline_services/"+user_id+"/logactivity");

                     },
                });
  }
  return false;
});
/** end of calllog add and update **/
/** 19-09-2018 **/
$(document).on('click','.schedule_section_insert,.schedule_section_update',function(){
var user_id=$('#timeline_client_id').val();
var notes=CKEDITOR.instances['editor24'].getData();
//var services_type=$('#services_type').val();
var update_schedule_id=$("#update_schedule_id").val();
  if(notes == '')
  {
    $('.schedule_note_errormsg').html('Enter Your Notes');
  }
  else
  {    
    $('.schedule_note_errormsg').html('');
         $.ajax({
                  url: '<?php echo base_url();?>client/client_timeline_schedule_add/',
                  dataType : 'json',
                  type : 'POST',
                  data : {'notes':notes,'user_id':user_id,'update_schedule_id':update_schedule_id},
                  success: function(data) {
                   //alert('zzz');
                    CKEDITOR.instances['editor24'].setData('');
                    var zz=CKEDITOR.instances['editor24'].getData();
                 //   alert(zz);
                    
                    $('.alert-success-reminder').css('display','block');
                    if(update_schedule_id!=''){  $('.timeline_success_messages').html('Schedule Updated successfully'); }
                    else{  $('.timeline_success_messages').html('Schedule Added successfully'); }
                   /** 15-09-2018  **/
                       $('.timelineschedule_add').css('display','');
                       $('.timelineschedule_update').css('display','none');
                      // $('#update_notes_id').val(note_id);
                   /** end of 15-09-2018 **/
                    setTimeout(function() { 
                    $('.alert-success-reminder').css('display','none');
                                 }, 2000); 
                    $('#schedule_section').load("<?php echo base_url(); ?>client/jquery_append_client_timeline_services/"+user_id+"/schedule");
/** for open calender section **/
 if(update_schedule_id!=''){
  window.open('<?php echo base_url(); ?>calendar/index/'+update_schedule_id, '_blank');
  }
  else
  {
window.open('<?php echo base_url(); ?>calendar/index/'+data, '_blank');
  }
/** end of calender _section **/ 
                     },
                });
  }
  return false;
});
/** end of 19-09-2018 **/
/** end 17-09-2018 **/

$(document).ready(function(){
  CKEDITOR.instances.editor22.on('change', function() { 
     $('.log_errormsg').html('');
});
CKEDITOR.instances.editor21.on('change', function() { 
     $('.calllog_errormsg').html('');
});
CKEDITOR.instances.editor1.on('change', function() { 
     $('.notes_errormsg').html('');
});
CKEDITOR.instances.editor2.on('change', function() { 
     $('.emailnote_errormsg').html('');
});
CKEDITOR.instances.editor23.on('change', function() { 
     $('.createtask_note_errormsg').html('');
});
CKEDITOR.instances.editor24.on('change', function() { 
     $('.schedule_note_errormsg').html('');
});
 
$('.dropdown-sin-12').click(function(){
  $('.services_type_errormsg').html('');
});

});



$('#notes_sections').validate(
            {
                ignore: [],
              debug: false,
                rules: { 

                    editor1:{
                         required: function() 
                        {
                         CKEDITOR.instances.editor1.updateElement();
                        },

                       //  minlength:10
                    }
                },
                messages:
                    {
                    editor1:{
                        required:"Please enter Text",
                       // minlength:"Please enter 10 characters"
                    }
                },
                submitHandler: function(form) {
                  
                  return false;
                 }
            });

</script>
<script>
// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction(note_id,action) {
    document.body.scrollTop = 3;
    document.documentElement.scrollTop = 3;
                      $.ajax({
                              url: '<?php echo base_url();?>client/get_client_timeline_notes_add/',
                              dataType : 'json',
                              type : 'POST',
                              data : {'note_id':note_id},
                              success: function(data) {
                              var notes=data.notes;
                              var services_type=data.services_type;
                              if(action=='notes'){
                                CKEDITOR.instances['editor1'].setData(notes);
                                $('.for_services_type').html('<label>Services Type</label><div class="dropdown-sin-12 lead-form-st"><select name="services_type" id="services_type" class="form-control fields" placeholder="Select Services Type"><option value="">--Select Service Type--</option>   <?php foreach ($client_services as $clse_key => $clse_value) {
                                         ?><option value="<?php echo $clse_key;?>"><?php echo $clse_value; ?></option><?php } ?></select></div><span class="services_type_errormsg error_msg" style="color: red;"></span>');
                                 $('#services_type').val(services_type);
                                $('.dropdown-sin-12').dropdown({
                                        input: '<input type="text" maxLength="20" placeholder="Search">'
                                   });
                                /** hide add button shown update button **/
                                 $('.timeline_add').css('display','none');
                                 $('.timeline_update').css('display','');
                                 $('#update_notes_id').val(note_id);
                                 }
                                 else if(action=='calllog')
                                 {
                                      CKEDITOR.instances['editor21'].setData(notes);
                                      $('.timelinecall_add').css('display','none');
                                      $('.timelinecall_update').css('display','');
                                      $('#update_calllog_id').val(note_id);
                                 }
                                  else if(action=='smslog')
                                 {
                                
                                      CKEDITOR.instances['editor51'].setData(notes);
                                      $('.timelinecall_add').css('display','none');
                                      $('.timelinecall_update').css('display','');
                                      $('#update_sms_id').val(note_id);
                                 }

                                  else if(action=='meeting')
                                 {
                                
                                      CKEDITOR.instances['editor61'].setData(notes);
                                      $('.timelinemeeting_add').css('display','none');
                                      $('.timelinemeeting_update').css('display','');
                                      $('#update_meeting_id').val(note_id);
                                 }
                                 else if(action=='log')
                                 {
                                      CKEDITOR.instances['editor22'].setData(notes);
                                      $('.timelinelog_add').css('display','none');
                                      $('.timelinelog_update').css('display','');
                                      $('#update_log_id').val(note_id);
                                 }
                                else if(action=='schedule')
                                 {
                                      CKEDITOR.instances['editor24'].setData(notes);
                                      $('.timelineschedule_add').css('display','none');
                                      $('.timelineschedule_update').css('display','');
                                      $('#update_schedule_id').val(note_id);
                                 }
                               /** end of button **/
                          
                                }
                            });

  }
  /** notes update cancel section **/
  //$('.notes_section_cancel').click(function(){
    $(document).on('click','.notes_section_cancel',function(){
      $('.for_services_type').html('');
      CKEDITOR.instances['editor1'].setData('');
      $('.for_services_type').html('<label>Services Type</label><div class="dropdown-sin-12 lead-form-st"><select name="services_type" id="services_type" class="form-control fields" placeholder="Select Services Type"><option value="">--Select Service Type--</option>   <?php foreach ($client_services as $clse_key => $clse_value) {
               ?><option value="<?php echo $clse_key;?>"><?php echo $clse_value; ?></option><?php } ?></select></div><span class="services_type_errormsg error_msg" style="color: red;"></span>');
      $('.dropdown-sin-12').dropdown({
              input: '<input type="text" maxLength="20" placeholder="Search">'
         });
       /** hide add button shown update button **/
      $('.timeline_add').css('display','');
      $('.timeline_update').css('display','none');
       $('#update_notes_id').val('');
      /** end of button **/
  });
  /** cancel section **/
    /** calllog update cancel section **/
    $(document).on('click','.calllog_section_cancel',function(){
   
      CKEDITOR.instances['editor21'].setData('');
    
       /** hide add button shown update button **/
      $('.timelinecall_add').css('display','');
      $('.timelinecall_update').css('display','none');
       $('#update_calllog_id').val('');
      /** end of button **/
  });
  /** cancel section **/
  /** calllog update cancel section **/
    $(document).on('click','.log_section_cancel,.schedule_section_cancel',function(){
       /** hide add button shown update button **/
      var action=$(this).attr('data-action');
      if(action=='logsection'){
       CKEDITOR.instances['editor22'].setData('');
       $('.timelinelog_add').css('display','');
       $('.timelinelog_update').css('display','none');
       $('#update_log_id').val('');
      }
      else
      {    
       CKEDITOR.instances['editor24'].setData('');
       $('.timelineschedule_add').css('display','');
       $('.timelineschedule_update').css('display','none');
       $('#update_schedule_id').val(''); 
      }
      // $('.timelinelog_add').css('display','');
      // $('.timelinelog_update').css('display','none');
      //  $('#update_log_id').val('');
      /** end of button **/
  });
  /** cancel section **/
  /** delete popup shown **/
  function confirm_delete_timeline(id)
   {
   $('#delete_notes'+id).show();
   return false;
   }
  /** end of delete popup **/

    $(document).on('click','.notes_delete_function',function(){
    var user_id=$('#timeline_client_id').val();
    var note_id=$(this).attr('data-id');
   $.ajax({
                              url: '<?php echo base_url();?>client/delete_timeline_notes/',
                              dataType : 'json',
                              type : 'POST',
                              data : {'note_id':note_id},
                              success: function(data) {
                            // alert(data);
                            $('#delete_notes'+note_id).hide();
                 $('.alert-success-reminder').css('display','block');
                 $('.timeline_success_messages').html('Notes Deleted successfully'); 
                 setTimeout(function() { 
                           $('.alert-success-reminder').css('display','none');
                  }, 2000); 
                $('#notes_section').load("<?php echo base_url(); ?>client/jquery_append_client_timeline_services/"+user_id+"/notes");
                                }
                            });
 });

    /** 14-09-2018 **/
    /** call log delete **/
$(document).on('click','.calllog_delete_function',function(){
    var user_id=$('#timeline_client_id').val();
    var note_id=$(this).attr('data-id');
   $.ajax({
                              url: '<?php echo base_url();?>client/delete_timeline_notes/',
                              dataType : 'json',
                              type : 'POST',
                              data : {'note_id':note_id},
                              success: function(data) {
                            // alert(data);
                            $('#delete_notes'+note_id).hide();
                            $('#calllog_section').load("<?php echo base_url(); ?>client/jquery_append_client_timeline_services/"+user_id+"/calllog");
                 $('.alert-success-reminder').css('display','block');
                 $('.timeline_success_messages').html('Notes Deleted successfully'); 
                 setTimeout(function() { 
                           $('.alert-success-reminder').css('display','none');
                  }, 2000); 
                $('#calllog_section').load("<?php echo base_url(); ?>client/jquery_append_client_timeline_services/"+user_id+"/calllog");

                
                                }
                            });
 });
        /** end of call log edit **/
            /**  log activity 17-09-2018 delete **/
$(document).on('click','.log_delete_function',function(){
    var user_id=$('#timeline_client_id').val();
    var note_id=$(this).attr('data-id');
    var action=$(this).attr('data-action');
   $.ajax({
                              url: '<?php echo base_url();?>client/delete_timeline_notes/',
                              dataType : 'json',
                              type : 'POST',
                              data : {'note_id':note_id},
                              success: function(data) {
                            // alert(data);
                            $('#delete_notes'+note_id).hide();
                            if(action=='logsection'){
                               $('#log_section').load("<?php echo base_url(); ?>client/jquery_append_client_timeline_services/"+user_id+"/logactivity");
                            }
                            if(action=='schedule')
                            {
                             $('#schedule_section').load("<?php echo base_url(); ?>client/jquery_append_client_timeline_services/"+user_id+"/schedule");
                            }
                            // $('#log_section').load("<?php echo base_url(); ?>client/jquery_append_client_timeline_services/"+user_id+"/logactivity");
                 $('.alert-success-reminder').css('display','block');
                 $('.timeline_success_messages').html('Notes Deleted successfully'); 
                 setTimeout(function() { 
                           $('.alert-success-reminder').css('display','none');
                  }, 2000); 
            
                
                                }
                            });
 });
        /** end of call log edit **/
/** email content for timeline services **/
$(document).on('click','.timeline_email_to_client',function(){

    var user_id=$('#timeline_client_id').val();
    var notes=CKEDITOR.instances['editor2'].getData();
    //window.open('<?php echo base_url(); ?>/email/compose', '_blank');
  if(notes == '')
  {
     $('.emailnote_errormsg').html('Enter Your Notes');
  }
  else
  {
    $('.emailnote_errormsg').html('');
 
     $.ajax({
            url: '<?php echo base_url();?>client/client_timeline_email_add/',
            dataType : 'json',
            type : 'POST',
            data : {'user_id':user_id,'notes':notes},
            success: function(data) {
             // alert(data);
              CKEDITOR.instances['editor2'].setData('');
              window.open('<?php echo base_url(); ?>email/compose/'+data, '_blank');
            },
          });
  }

});

/** end of email contact **/

    /** end of  14-09-2018 **/
  /** 15-09-2018 **/
$(document).on('click','.create_task_withnotes',function(){
//alert('zz');
  var notes=CKEDITOR.instances['editor23'].getData();
  var user_id=$('#timeline_client_id').val();
  if(notes=='')
  {
     $('.createtask_note_errormsg').html('Enter a Note');
  }  
  else
  {
   //  window.open('<?php echo base_url(); ?>user/new_task/'+data, '_blank');
    $.ajax({
            url: '<?php echo base_url();?>client/client_timeline_task_add/',
            dataType : 'json',
            type : 'POST',
            data : {'user_id':user_id,'notes':notes},
            success: function(data) {
             // alert(data);
              CKEDITOR.instances['editor23'].setData('');
              window.open('<?php echo base_url(); ?>user/new_task/'+data, '_blank');
            },
          });
  }

});
  /** end of 15-09-2018 **/

  /** 18-09-2018 for allactivity section ajax **/
$(document).on('click','.for_allactivity_section_ajax',function(){
      var user_id=$('#timeline_client_id').val();
    
        $('.LoadingImage').show();
    //$('#allactivitylog_sections').load("<?php echo base_url(); ?>client/jquery_append_client_timeline_services/"+user_id+"/allactivity");
     $.ajax({url:"<?php echo base_url(); ?>client/jquery_append_client_timeline_services/"+user_id+"/allactivity",success: function(result){

      $('#allactivitylog_sections').append(result);
var mylist = $('#allactivitylog_sections').find('#services');
var listitems =$('#allactivitylog_sections').find('#services').children('.sorting').get();
listitems.sort(function(a, b) {
   var compA = $(a).attr('id').toUpperCase();
   var compB = $(b).attr('id').toUpperCase();
   return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems, function(idx, itm) {
   mylist.append(itm);
   //alert(itm);
});

//console.log(res);
// $('#allactivitylog_sections').append(res);
//console.log(result);
//$('#allactivitylog_sections').append($(result));
 setTimeout(function() { 
                          $('.LoadingImage').hide();
                  }, 2000); 
    }

  });
    //$('.LoadingImage').hide();                
             
});
  /** end of allactivity section ajax **/
  /** 18-09-2018 for filter option dropdown **/
$(document).on('click','.filter_timeline_checkboxs',function(){
  var values = new Array();
  var user_id=$('#timeline_client_id').val();
  $.each($("input[name='filter_timeline[]']:checked"), function() {
    values.push($(this).val());
   });
  
  var all_values=values.join(',');
  console.log(all_values);

   $('.LoadingImage').show();
   $.ajax({
        url: "<?php echo base_url(); ?>client/jquery_append_client_timeline_services_allactivity",
        type: "post",
        data: {'user_id':user_id,'filter_values':all_values} ,
       success: function (result) {
          //alert('sss');
      $('#allactivitylog_sections').append(result);

var mylist = $('#allactivitylog_sections').find('#services');
var listitems = $('#allactivitylog_sections').find('#services').children('.sorting').get();
listitems.sort(function(a, b) {
   var compA = $(a).attr('id').toUpperCase();
   var compB = $(b).attr('id').toUpperCase();
   return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems, function(idx, itm) {
   mylist.append(itm);
});
          $('.LoadingImage').hide();
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }


    });
  //  $('#allactivitylog_sections').load("<?php echo base_url(); ?>client/jquery_append_client_timeline_services_allactivity/"+user_id+"/"+values);
     // setTimeout(function() { 
   //                        $('.LoadingImage').hide();
   //                }, 1000); 

  //console.log('rspt rspt');
});

/** 22-09-2018 **/
  $(document).on('click','ol.nav.nav-tabs.all_user1.md-tabs li.nav-item',function(){
      //alert($(this).find('a').attr('href'));
      var attr_id=$(this).find('a').attr('href');
      $('.tab-content').each(function(){
        $(this).find('.tab-pane').removeClass('active');
        console.log($(this).find('.tab-pane').attr('class'));
      });
      $(attr_id).addClass('active');
  });



  $( document ).ready(function() {
var mylist = $('#details').find('#important_details_section');
var listitems = $('#details').find('#important_details_section').children('.sorting').get();
listitems.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems, function(idx, itm) { 
    mylist.append(itm);
});
var mylist1 = $('#confirmation-statement').find('#box1');
var listitems1 = $('#confirmation-statement').find('#box1').children('.sorting_conf').get();
listitems1.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems1, function(idx, itm) {
    mylist1.append(itm);
})
var mylist2 = $('#confirmation-statement').find('#box2');
var listitems2 = $('#confirmation-statement').find('#box2').children('.sorting_conf').get();
listitems2.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems2, function(idx, itm) { 
    mylist2.append(itm);
})
var mylist3 = $('#confirmation-statement').find('#box3');
var listitems3 = $('#confirmation-statement').find('#box3').children('.sorting_conf').get();
listitems3.sort(function(a, b) { 
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems3, function(idx, itm) { 
    mylist3.append(itm);
})
var mylist4 = $('#personal-tax-returns').find('#personal_box');
var listitems4 = $('#personal-tax-returns').find('#personal_box').children('.personal_sort').get();
listitems4.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems4, function(idx, itm) { 
    mylist4.append(itm);
})
var mylist4 = $('#personal-tax-returns').find('#personal_box1');
var listitems4 = $('#personal-tax-returns').find('#personal_box1').children('.personal_sort').get();
listitems4.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems4, function(idx, itm) { 
    mylist4.append(itm);
})
var mylist4 = $('#personal-tax-returns').find('#personal_box2');
var listitems4 = $('#personal-tax-returns').find('#personal_box2').children('.personal_sort').get();
listitems4.sort(function(a, b) { 
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems4, function(idx, itm) {
    mylist4.append(itm);
})
var mylist5 = $('#vat-Returns').find('#vat_box');
var listitems5 = $('#vat-Returns').find('#vat_box').children('.vat_sort').get();
listitems5.sort(function(a, b) { 
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems5, function(idx, itm) {
  // console.log(idx);
    mylist5.append(itm);
})
var mylist6 = $('#vat-Returns').find('#vat_box1');
var listitems6 = $('#vat-Returns').find('#vat_box1').children('.vat_sort').get();
listitems6.sort(function(a, b) {  
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems6, function(idx, itm) { 
    mylist6.append(itm);
})
var mylist7 = $('#management-account').find('#management_account');
var listitems7 = $('#management-account').find('#management_account').children('.management').get();
listitems7.sort(function(a, b) {  
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems7, function(idx, itm) {
    mylist7.append(itm);
})
var mylist8 = $('#management-account').find('#management_account1');
var listitems8 = $('#management-account').find('#management_account1').children('.management').get();
listitems8.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems8, function(idx, itm) {
    mylist8.append(itm);
})
var mylist9 = $('#management-account').find('#management_box1');
var listitems9 = $('#management-account').find('#management_box1').children('.management').get();
listitems9.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems9, function(idx, itm) {
    mylist9.append(itm);
})
var mylist10 = $('#management-account').find('#management_box2');
var listitems10 = $('#management-account').find('#management_box2').children('.management').get();
listitems10.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems10, function(idx, itm) {
    mylist10.append(itm);
})

var mylist11 = $('#investigation-insurance').find('#invest-box');
var listitems11 = $('#investigation-insurance').find('#invest-box').children('.invest').get();
listitems11.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems11, function(idx, itm) {
    mylist11.append(itm);
})
var mylist13 = $('#investigation-insurance').find('#invest_box2');
var listitems13 = $('#investigation-insurance').find('#invest_box2').children('.invest').get();
listitems13.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems13, function(idx, itm) {
 //  console.log(idx);
    mylist13.append(itm);
})
var mylist12 = $('#other').find('#others_section');
var listitems12 = $('#other').find('#others_section').children('.others_details').get();
listitems12.sort(function(a, b) { 
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems12, function(idx, itm) {
  // console.log(idx);
    mylist12.append(itm);
})
var mylist14 = $('#other').find('#other_details1');
var listitems14 = $('#other').find('#other_details1').children('.others_details').get();
listitems14.sort(function(a, b) { 
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems14, function(idx, itm) {
 //  console.log(idx);
    mylist14.append(itm);
})
var mylist15 = $('#referral').find('#referals');
var listitems15 = $('#referral').find('#referals').children('.referal_details').get();
listitems15.sort(function(a, b) { 
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems15, function(idx, itm) {
  // console.log(idx);
    mylist15.append(itm);
})
/* Accounts */
var mylist16 = $('#accounts').find('#accounts_box');
var listitems16 = $('#accounts').find('#accounts_box').children('.accounts_sec').get();
listitems16.sort(function(a, b) { 
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems16, function(idx, itm) {
 //  console.log(idx);
    mylist16.append(itm);
})
var mylist17 = $('#accounts').find('#accounts_box1');
var listitems17 = $('#accounts').find('#accounts_box1').children('.accounts_sec').get();
listitems17.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems17, function(idx, itm) {
 //  console.log(idx);
    mylist17.append(itm);
})
var mylist18 = $('#accounts').find('#accounts_box2');
var listitems18 = $('#accounts').find('#accounts_box2').children('.accounts_sec').get();
listitems18.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems18, function(idx, itm) {
 //  console.log(idx);
    mylist18.append(itm);
})
var mylist19 = $('#accounts').find('#accounts_box3');
var listitems19 = $('#accounts').find('#accounts_box3').children('.accounts_sec').get();
listitems16.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems19, function(idx, itm) {
  // console.log(idx);
    mylist19.append(itm);
})

var mylist20 = $('#accounts').find('#accounts_box4');
var listitems20 = $('#accounts').find('#accounts_box4').children('.accounts_sec').get();
listitems20.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems20, function(idx, itm) {
  // console.log(idx);
    mylist20.append(itm);
})
var mylist21 = $('#payroll').find('#payroll_box');
var listitems21 = $('#payroll').find('#payroll_box').children('.payroll_sec').get();
listitems21.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems21, function(idx, itm) {
 //  console.log(idx);
    mylist21.append(itm);
})

var mylist22 = $('#payroll').find('#payroll_box1');
var listitems22 = $('#payroll').find('#payroll_box1').children('.payroll_sec').get();
listitems22.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems22, function(idx, itm) {
 //  console.log(idx);
    mylist22.append(itm);
})
var mylist23 = $('#payroll').find('#payroll_box2');
var listitems23 = $('#payroll').find('#payroll_box2').children('.payroll_sec').get();
listitems23.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems23, function(idx, itm) {
 //  console.log(idx);
    mylist23.append(itm);
})
var mylist24 = $('#payroll').find('#workplace_sec');
var listitems24 = $('#payroll').find('#workplace_sec').children('.workplace').get();
listitems24.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems24, function(idx, itm) {
  // console.log(idx);
    mylist24.append(itm);
})
var mylist25 = $('#payroll').find('#workplace_sec1');
var listitems25 = $('#payroll').find('#workplace_sec1').children('.workplace').get();
listitems25.sort(function(a, b) { 
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems25, function(idx, itm) {
//console.log(idx);
    mylist25.append(itm);
})
var mylist26 = $('#payroll').find('#p11d_sec');
var listitems26 = $('#payroll').find('#p11d_sec').children('.p11d').get();
listitems26.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems26, function(idx, itm) {
  // console.log(idx);
    mylist26.append(itm);
})


var listitems27 = $('#payroll').find('#p11d_sec1').children('.p11d').get();
listitems27.sort(function(a, b) {  
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems27, function(idx, itm) {
  // console.log(idx);
    mylist27.append(itm);
})
});



$( document ).ready(function() { 



  $('.add_custom_fields_section .form-control').prop('disabled', true);
//console.log( $(".add_custom_fields_section").html());

$( "div.add_custom_fields_section div.form-group").each(function( index ) {
  console.log( index + ": " + $( this ).attr('class')+ "ID :" +  $( this ).attr('id') );
  var goal=$( this ).attr('id');
  if ( $( this ).hasClass( "personal_sort" ) ) { 
        append_text_section('personal-tax-returns','personal_sort',index,goal);
   }

    if ( $( this ).hasClass( "sorting" ) ) { 
        append_text_section('details','sorting',index,goal);
   }

   if ( $( this ).hasClass( "accounts_sec" ) ) {          
         append_text_section('accounts','accounts_sec',index,goal); 
   }

   if ( $( this ).hasClass( "workplace" ) ) {  
         append_text_section('payroll','workplace',index,goal);    
   }

   if ( $( this ).hasClass( "p11d" ) ) {   
         append_text_section('payroll','p11d',index,goal);     
   }

   if ( $( this ).hasClass( "payroll_sec" ) ) {        
          append_text_section('payroll','payroll_sec',index,goal);      
   }

   if ( $( this ).hasClass( "vat_sort" ) ) {       
         append_text_section('vat-Returns','vat_sort',index,goal);     
   }

   if ( $( this ).hasClass( "management" ) ) {        
         append_text_section('management-account','management',index,goal);      
   }

   if ( $( this ).hasClass( "invest" ) ) {        
         append_text_section('investigation-insurance','invest',index,goal);     
   }

   if ( $( this ).hasClass( "others_details" ) ) {        
        append_text_section('other','others_details',index,goal);   
   }

    if ( $( this ).hasClass( "sorting_conf" ) ) {        
        append_text_section('confirmation-statement','sorting_conf',index,goal);   
   }
});

   });



function append_text_section(parent,value,index,goal){
console.log(parent);
console.log(value);
    console.log($('#'+parent+' .'+value).html());
         var myarray_parents=[];
         var myarray = [];
         $('#'+parent+' .'+value).each(function(){
            var condition=$(this).parents().attr('class');     
            if(condition!='add_custom_fields_section'){
                  myarray.push($(this).attr('id')); 
                  myarray_parents.push($(this).parents().attr('id'));                 
           }      
         });

         console.log(myarray);
         console.log(myarray_parents);
          var closest = null;  
              values = myarray;

              userNum = goal; //This is coming from user input

               smallestDiff = Math.abs(userNum - values[0]);
               closest = 0; //index of the current closest number
               for (i = 1; i < values.length; i++) {
                  currentDiff = Math.abs(userNum - values[i]);
                  if (currentDiff < smallestDiff) {
                     smallestDiff = currentDiff;
                     closest = i;
                  }
               }      

            var search_value=values[closest];    
            console.log(search_value);         
               var position=jQuery.inArray( search_value, myarray );
               var id_value=myarray[position];                
               var parent_value=myarray_parents[position]; 
               var next_index=index +1;
               console.log(id_value);              
               var consume=$("#"+parent_value).find("#"+id_value);  
               console.log(consume);
               console.log($("div.add_custom_fields_section  .form-group:nth-child(1)").html());
               $("div.add_custom_fields_section  .form-group:nth-child(1)").insertAfter(consume);  
}



$(".archive_task").click(function(){
//alert('ok');
var id=$("#archive_task_id").val();
 $.ajax({
                    url: '<?php echo base_url(); ?>user/archive_update',
                    type : 'POST',
                    data : { 'id':id},                    
                      beforeSend: function() {
                        $(".LoadingImage").show();
                      },
                      success: function(data) {
                        if(data==1){
                         $("#my_Modal").hide();
                         $(".LoadingImage").hide();
                         location.reload();
                        }
                      }

                    });

});



$(document).on("click",".save_assign_staff",function(){

 // alert('ok');
var id = $(this).attr("data-id");
var data = {};
var countries =$("#workers"+id ).val();

alert(countries);
/* alert($("#workers"+id ).val());
$.each($(".workers option:selected"), function(){            
  countries.push($(this).val());
});*/
data['task_id'] = id;
data['worker'] = countries;
$(".LoadingImage").show();
$.ajax({
     type: "POST",
     url: "<?php echo base_url();?>user/update_assignees/",
     data: data,
     success: function(response) {
        // alert(response); die();
        $(".LoadingImage").hide();
     $('#task_'+id).html(response);
     $('.task_'+id).html(response);
        $('.dashboard_success_message').show();
       setTimeout(function(){ $('.dashboard_success_message').hide(); }, 2000);
     },
  });
});


function archiveFunction(id){
  $.ajax({
     type: "POST",
     url: "<?php echo base_url();?>user/update_archive/",
     data: {'id':id},
      beforeSend: function() {
                        $(".LoadingImage").show();
                      },
     success: function(response) {
      if(response==1){
            $(".LoadingImage").hide();
        // alert(response); die();
      }
     //    $(".LoadingImage").hide();
     // $('#task_'+id).html(response);
     // $('.task_'+id).html(response);
     //    $('.dashboard_success_message').show();
     //   setTimeout(function(){ $('.dashboard_success_message').hide(); }, 2000);
     },
  });
}

function trigger_function(trigger){
 var all_values=$(trigger).attr('id');
 //alert(all_values);
  var user_id=$('#timeline_client_id').val();
  // $('.'+name).trigger('click');

     $.ajax({
        url: "<?php echo base_url(); ?>client/jquery_append_client_timeline_services_allactivity",
        type: "post",
        data: {'user_id':user_id,'filter_values':all_values} ,
         beforeSend: function() {
                        $(".LoadingImage").show();
                      },
        success: function (response) {
          //alert('sss');
          $('#allactivitylog_sections').html(response);
          $('.LoadingImage').hide();
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }


    });
}


function trigger(ggg){
  $(".for_allactivity_section_ajax").trigger('click');
}

   // function staff_click(staff){
   //  alert($(staff).attr("data-id"))
   // }
  /** end of 22-09-2018 **/
  /** endof filter option **/
</script>
<!-- end of services -->