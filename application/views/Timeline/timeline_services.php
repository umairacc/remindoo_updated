<?php $this->load->view('includes/header');
error_reporting(-1);
   ?>
<style>
.deadline-crm1 .nav-tabs li:first-child a {
    padding-left: 10px;
    color: #777777;
    font-family: Roboto Condensed;
    font-weight: 400;
    font-size: 14px;
}
</style>
<!-- <link href="http://remindoo.org/CRMTool/assets/css/jquery.filer.css" type="text/css" rel="stylesheet" /> -->
<link href="<?php echo base_url()?>/assets/css/timeline-style.css" type="text/css" rel="stylesheet" />
<!-- <link href="http://remindoo.org/CRMTool/assets/css/jquery.filer-dragdropbox-theme.css" type="text/css" rel="stylesheet" /> -->
<div class="pcoded-content card-removes">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card">
                        <!-- admin start-->
                        <div class="client_section col-xs-12 floating_set">
                           <div class="all_user-section floating_set">
                              <div class="deadline-crm1 floating_set">
                                   <ul class="nav nav-tabs1 all_user1 md-tabs pull-left u-dashboard">
                                    <li><a href="javascript:;" class="active">
                                    <img class="themicond1" src="<?php echo base_url();?>assets/images/new_update14.png" alt="themeicon" />  
                                 services timeline</a></li>
                                   </ul>
                              </div>
                              <div class="floating_set time-service ">
                                 <div class="addnewclient_pages1 floating_set bg_new1">
                              <ul class="nav nav-tabs all_user1 md-tabs floating_set">
                              <?php 
                                 $services = $this->Common_mdl->getServicelist();
                                 $other_services = array_map('Get_Other_Services', $services);
                                 
                                 function Get_Other_Services($arr)
                                 {
                                    if($arr['id'] > 16)
                                    {
                                       return $arr;
                                    }
                                 }
                                      
                                       $client_services = array('ALL');

                                       foreach($services as $key => $value) 
                                       {
                                          $client_services[] = $value['service_name'];
                                       }

                                       $settings = $this->Common_mdl->select_record("admin_setting",'firm_id',$_SESSION['firm_id']);
                                       $settings_services = json_decode( $settings['services'] ,true);

                                       if(!empty( $settings_services ))
                                       {
                                          $disabled = array_diff($settings_services, [1]);
                                          $client_services = array_diff_key($client_services, $disabled);
                                       }
                                       
                                       //$client_services = $this->Common_mdl->getClientServices($client_id);
                                       $i=0;
                                       foreach ($client_services as $key => $value)
                                       {

                                        ?>
                                          <li class="nav-item">
                                             <a class="nav-link <?php if($i==0){ echo " active"; }?>" data-toggle="tab" href="#tab_<?php echo $i;?>"><?php echo $value;?></a>
                                             <div class="slide"></div>
                                          </li>
                                    <?php  
                                       $i++;                                
                                       }
                                       ?>
                                    <!--      <li class="nav-item">
                                       <a class="nav-link active" data-toggle="tab" href="#alltasks">All Tasks</a>
                                       <div class="slide"></div>
                                       </li> -->
                                 </ul>
                              </div>
                                 
   <div class="tab-content">
      <?php $i=0;
         foreach ($client_services as $key => $value) {  ?>
      <div id="tab_<?php echo $i;?>" class="tab-pane fade <?php if($i==0){ echo " in active"; }?>">
         <!-- start -->
         <!-- for new one -->
         <div class="card ser-timeline">
            <div class="time-border">
            <div class="time-frame">
                  <h2><?php echo $value;?></h2>
                  <div class="time-inputs">
                      <input class="form-control form-control-lg Note-search_timeline searchclsnew" type="text" id="<?php echo str_replace(' ', '', $value); ?>" placeholder="Search"  >
                  </div>
             </div>

            <!--search  end -->
            <div class="card-block <?php if($_SESSION['permission']['Services_Timeline']['view']!='1'){ ?> permission_deined <?php } ?>">
               <div class="main-timeline">
                  <div class="cd-timeline cd-container overall_<?php echo str_replace(' ', '', $value); ?>" id="search_for_activity" >
                     <!-- from db data loop start -->
                     <?php 
                    
                     /** 21-08-2018 **/
                        if($value=="VAT" || $value=="ALL"){
                        foreach ($client_details as $getCompanykey => $getCompanyvalue) {
                          if($getCompanyvalue['blocked_status']==0){
                           $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                           ?>
                     <div class="cd-timeline-block <?php echo str_replace(' ', '', $value); ?>">
                        <div class="cd-timeline-icon bg-primary">
                           <i class="icofont icofont-ui-file"></i>
                        </div>
                        <div class="cd-timeline-content card_main">
                           <div class="p-20">
                              <h6>
                                 <a href="<?php echo base_url().'client/client_infomation/'.$getCompanyvalue['user_id']?>" target="_blank">
                                 <?php echo $getusername['crm_name'];?> - <?php echo $getCompanyvalue['crm_legal_form'];?>                            </a>                                      
                                 </h6>
                              <div class="timeline-details">
                                 <a href="#">
                                    <span>
                                    Vat Quarter: <?php echo ( !empty($getCompanyvalue['crm_vat_quarters']) )?$getCompanyvalue['crm_vat_quarters']:'-';  ?>
                                    </span>
                                 </a>
                              </br>
                                 <a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_vat_due_date'])): ''); ?></span> </a>
                                 <!--  <a href="#">
                                    <i class="icofont icofont-ui-user"></i><span>John Doe</span>crm_vat_date_of_registration
                                    </a> -->
                                 <p class="m-t-0">VAT Number : <?php echo $getCompanyvalue['crm_vat_number_one'];?></p>
                                 <p class="m-t-0">VAT Registration Date : <?php echo (strtotime($getCompanyvalue['crm_vat_date_of_registration']) !='' ?  date('F j, Y, g:i', strtotime($getCompanyvalue['crm_vat_date_of_registration'])): ''); ?></p>
                           <!-- for notes section -->
                                 <!-- end of notes -->
                                 <!-- for notifiaction -->
                                 <p class="m-t-20">
                                 <p><label>Services :</label><?php   (isset(json_decode($getCompanyvalue['vat'])->tab) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->tab : $jsnvat = '';
                                    ($jsnvat!='') ? $vat = "On" : $vat = "Off"; echo $vat;?></p>
                                 <p><label>Reminder :</label><?php   (isset(json_decode($getCompanyvalue['vat'])->reminder) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->reminder : $jsnvat = '';
                                    ($jsnvat!='') ? $vat = "On" : $vat = "Off"; echo $vat; ?></p>
                                 <p><label>Text :</label><?php   (isset(json_decode($getCompanyvalue['vat'])->text) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->text : $jsnvat = '';
                                    ($jsnvat!='') ? $vat = "On" : $vat = "Off"; echo $vat;?></p>
                                 <p><label>Invoice :</label><?php  echo (isset(json_decode($getCompanyvalue['vat'])->invoice) && $getCompanyvalue['vat'] != '') ? $jsnvat =  json_decode($getCompanyvalue['vat'])->invoice : $jsnvat = '';
                                    ($jsnvat!='') ? $vat = "On" : $vat = "Off"; echo $vat;?></p>
                                 </p>
                                 <!-- end of notification -->
                              </div>
                           </div>
                              <!-- <span class="cd-date"><?php if(isset($getusername['CreatedTime'])){ echo date('F j, Y, g:i',$getusername['CreatedTime']); } ?></span> -->
                              <span class="cd-details"><?php echo $getCompanyvalue['crm_company_name'];?> Vat Info </span>
                        </div>
                     </div>
                     <?php }
                        ?>
                     <?php
                        }} 
                        
                        if($value=="Payroll" || $value=="ALL"){
                           foreach ($client_details as $getCompanykey => $getCompanyvalue) { 
                                 if($getCompanyvalue['blocked_status']==0){
                            $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                            ?>
                     <div class="cd-timeline-block <?php echo str_replace(' ', '', $value); ?>">
                        <div class="cd-timeline-icon bg-primary">
                           <i class="icofont icofont-ui-file"></i>
                        </div>
                        <div class="cd-timeline-content card_main">
                           <div class="p-20">
                              <h6><a href="<?php echo base_url().'client/client_infomation/'.$getCompanyvalue['user_id']?>" target="_blank">
                                 <?php echo $getusername['crm_name'];?> - <?php echo $getCompanyvalue['crm_legal_form'];?>                            </a></h6>
                              <div class="timeline-details">
                                 <a href="#">
                                    <span>
                                       Payroll Run : <?php echo $getCompanyvalue['crm_payroll_run'];?>
                                    </span>
                                 </a>
                                 </br>
                                 <a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_rti_deadline'])): ''); ?></span> </a>
                                 <!--  <a href="#">
                                    <i class="icofont icofont-ui-user"></i><span>John Doe</span>crm_vat_date_of_registration
                                    </a> -->
                                 <p class="m-t-0"> First Pay Date: <?php echo (strtotime($getCompanyvalue['crm_first_pay_date']) !='' ?  date('F j, Y, g:i', strtotime($getCompanyvalue['crm_first_pay_date'])): ''); ?></p>
                                 <p class="m-t-0">Payroll Run Date : <?php echo (strtotime($getCompanyvalue['crm_payroll_run_date']) !='' ?  date('F j, Y, g:i', strtotime($getCompanyvalue['crm_payroll_run_date'])): ''); ?></p>
                                 <!-- for notifications -->
                                 <p class="m-t-20">
                                 <p><label>Services :</label><?php   (isset(json_decode($getCompanyvalue['payroll'])->tab) && $getCompanyvalue['payroll'] != '') ? $jsnpayroll =  json_decode($getCompanyvalue['payroll'])->tab : $jsnpayroll = '';
                                    ($jsnpayroll!='') ? $payroll = "On" : $payroll = "Off"; echo $payroll;?></p>
                                 <p><label>Reminder :</label><?php   (isset(json_decode($getCompanyvalue['payroll'])->reminder) && $getCompanyvalue['payroll'] != '') ? $jsnpayroll =  json_decode($getCompanyvalue['payroll'])->reminder : $jsnpayroll = '';
                                    ($jsnpayroll!='') ? $payroll = "On" : $payroll = "Off"; echo $payroll; ?></p>
                                 <p><label>Text :</label><?php   (isset(json_decode($getCompanyvalue['payroll'])->text) && $getCompanyvalue['payroll'] != '') ? $jsnpayroll =  json_decode($getCompanyvalue['payroll'])->text : $jsnpayroll = '';
                                    ($jsnpayroll!='') ? $payroll = "On" : $payroll = "Off"; echo $payroll;?></p>
                                 <p><label>Invoice :</label><?php  echo (isset(json_decode($getCompanyvalue['payroll'])->invoice) && $getCompanyvalue['payroll'] != '') ? $jsnpayroll =  json_decode($getCompanyvalue['payroll'])->invoice : $jsnpayroll = '';
                                    ($jsnpayroll!='') ? $payroll = "On" : $payroll = "Off"; echo $payroll;?></p>
                                 </p>
                                 <!-- end of notification -->
                              </div>
                           </div>
                           <span class="cd-date"><?php if(isset($getusername['CreatedTime'])){ echo date('F j, Y, g:i',$getusername['CreatedTime']); } ?></span>
                           <span class="cd-details"><?php echo $getCompanyvalue['crm_company_name'];?> Payroll Info </span>
                        </div>
                     </div>
                     <?php }
                        }}
                        if($value=="Accounts" || $value=="ALL" ){
                        foreach ($client_details as $getCompanykey => $getCompanyvalue) { 
                              if($getCompanyvalue['blocked_status']==0){
                        $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                        ?>
                     <div class="cd-timeline-block <?php echo str_replace(' ', '', $value); ?>">
                        <div class="cd-timeline-icon bg-primary">
                           <i class="icofont icofont-ui-file"></i>
                        </div>
                        <div class="cd-timeline-content card_main">
                           <div class="p-20">
                              <h6><a href="<?php echo base_url().'client/client_infomation/'.$getCompanyvalue['user_id']?>" target="_blank"><?php echo $getusername['crm_name'];?> - <?php echo $getCompanyvalue['crm_legal_form'];?></a></h6>
                              <div class="timeline-details">
                                 <a href="#">
                                    <i class="icofont icofont-ui-calendar"></i>
                                    <span>Due Date : <?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])): ''); ?></span>
                                 </a>
                                 <!--  <a href="#">
                                    <i class="icofont icofont-ui-user"></i><span>John Doe</span>crm_vat_date_of_registration
                                    </a> -->
                                 <a href="#"> 
                                    <span>
                                    Next Account Madeup: <?php echo (strtotime($getCompanyvalue['crm_hmrc_yearend']) !='' ?  date('F j, Y, g:i', strtotime($getCompanyvalue['crm_hmrc_yearend'])): ''); ?>
                                    </span>
                                 </a>

                              
                                 <!-- for notifications -->
                                 <p class="m-t-20">
                                 <p><label>Services :</label><?php   (isset(json_decode($getCompanyvalue['accounts'])->tab) && $getCompanyvalue['accounts'] != '') ? $jsnaccounts =  json_decode($getCompanyvalue['accounts'])->tab : $jsnaccounts = '';
                                    ($jsnaccounts!='') ? $accounts = "On" : $accounts = "Off"; echo $accounts;?></p>
                                 <p><label>Reminder :</label><?php   (isset(json_decode($getCompanyvalue['accounts'])->reminder) && $getCompanyvalue['accounts'] != '') ? $jsnaccounts =  json_decode($getCompanyvalue['accounts'])->reminder : $jsnaccounts = '';
                                    ($jsnaccounts!='') ? $accounts = "On" : $accounts = "Off"; echo $accounts; ?></p>
                                 <p><label>Text :</label><?php   (isset(json_decode($getCompanyvalue['accounts'])->text) && $getCompanyvalue['accounts'] != '') ? $jsnaccounts =  json_decode($getCompanyvalue['accounts'])->text : $jsnaccounts = '';
                                    ($jsnaccounts!='') ? $accounts = "On" : $accounts = "Off"; echo $accounts;?></p>
                                 <p><label>Invoice :</label><?php  echo (isset(json_decode($getCompanyvalue['accounts'])->invoice) && $getCompanyvalue['accounts'] != '') ? $jsnaccounts =  json_decode($getCompanyvalue['accounts'])->invoice : $jsnaccounts = '';
                                    ($jsnaccounts!='') ? $accounts = "On" : $accounts = "Off"; echo $accounts;?></p>
                                 </p>
                                 <!-- end of notification -->
                              </div>
                           </div>
                           <span class="cd-date"><?php if(isset($getusername['CreatedTime'])){ echo date('F j, Y, g:i',$getusername['CreatedTime']); } ?></span>
                           <span class="cd-details"><?php echo $getCompanyvalue['crm_company_name'];?> Account Info </span>
                        </div>
                     </div>
                     <?php }
                        } }
                        if($value=="Confirmation Statement" || $value=="ALL"){
                        foreach ($client_details as $getCompanykey => $getCompanyvalue) { 
                              if($getCompanyvalue['blocked_status']==0){
                        $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                        ?>
                     <div class="cd-timeline-block <?php echo str_replace(' ', '', $value); ?>">
                        <div class="cd-timeline-icon bg-primary">
                           <i class="icofont icofont-ui-file"></i>
                        </div>
                        <div class="cd-timeline-content card_main">
                           <div class="p-20">
                              <h6><a href="<?php echo base_url().'client/client_infomation/'.$getCompanyvalue['user_id']?>" target="_blank"><?php echo $getusername['crm_name'];?> - <?php echo $getCompanyvalue['crm_legal_form'];?></a></h6>
                              <div class="timeline-details">
                                 <a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])): ''); ?></span> </a>
                                 <!--  <a href="#">
                                    <i class="icofont icofont-ui-user"></i><span>John Doe</span>crm_vat_date_of_registration
                                    </a> -->
                                    </br>
                                 <a href="#">
                                    <span>
                                    Next Statement Date:  <?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_date']) !='' ?  date('F j, Y, g:i', strtotime($getCompanyvalue['crm_confirmation_statement_date'])): ''); ?>
                                       
                                    </span>
                                 </a>
                                 
                                 <!-- for notifications -->
                                 <p class="m-t-20">
                                 <p><label>Services :</label><?php   (isset(json_decode($getCompanyvalue['conf_statement'])->tab) && $getCompanyvalue['conf_statement'] != '') ? $jsnconf_statement =  json_decode($getCompanyvalue['conf_statement'])->tab : $jsnconf_statement = '';
                                    ($jsnconf_statement!='') ? $conf_statement = "On" : $conf_statement = "Off"; echo $conf_statement;?></p>
                                 <p><label>Reminder :</label><?php   (isset(json_decode($getCompanyvalue['conf_statement'])->reminder) && $getCompanyvalue['conf_statement'] != '') ? $jsnconf_statement =  json_decode($getCompanyvalue['conf_statement'])->reminder : $jsnconf_statement = '';
                                    ($jsnconf_statement!='') ? $conf_statement = "On" : $conf_statement = "Off"; echo $conf_statement; ?></p>
                                 <p><label>Text :</label><?php   (isset(json_decode($getCompanyvalue['conf_statement'])->text) && $getCompanyvalue['conf_statement'] != '') ? $jsnconf_statement =  json_decode($getCompanyvalue['conf_statement'])->text : $jsnconf_statement = '';
                                    ($jsnconf_statement!='') ? $conf_statement = "On" : $conf_statement = "Off"; echo $conf_statement;?></p>
                                 <p><label>Invoice :</label><?php  echo (isset(json_decode($getCompanyvalue['conf_statement'])->invoice) && $getCompanyvalue['conf_statement'] != '') ? $jsnconf_statement =  json_decode($getCompanyvalue['conf_statement'])->invoice : $jsnconf_statement = '';
                                    ($jsnconf_statement!='') ? $conf_statement = "On" : $conf_statement = "Off"; echo $conf_statement;?></p>
                                 </p>
                                 <!-- end of notification -->
                              </div>
                           </div>
                           <span class="cd-date"><?php if(isset($getusername['CreatedTime'])){ echo date('F j, Y, g:i',$getusername['CreatedTime']); } ?></span>
                           <span class="cd-details"><?php echo $getCompanyvalue['crm_company_name'];?> Confirmation statement Info </span>
                        </div>
                     </div>
                     <?php }
                        } }
                        if($value=="Company Tax Return" || $value=="ALL" ){
                        foreach ($client_details as $getCompanykey => $getCompanyvalue) { 
                              if($getCompanyvalue['blocked_status']==0){
                        $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                        ?>
                     <div class="cd-timeline-block <?php echo str_replace(' ', '', $value); ?>">
                        <div class="cd-timeline-icon bg-primary">
                           <i class="icofont icofont-ui-file"></i>
                        </div>
                        <div class="cd-timeline-content card_main">
                           <div class="p-20">
                              <h6><a href="<?php echo base_url().'client/client_infomation/'.$getCompanyvalue['user_id']?>" target="_blank"><?php echo $getusername['crm_name'];?> - <?php echo $getCompanyvalue['crm_legal_form'];?></a></h6>
                              <div class="timeline-details">
                                 <a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php echo (strtotime($getCompanyvalue['crm_accounts_due_date_hmrc']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_accounts_due_date_hmrc'])): ''); ?></span> </a>
                                 <!--  <a href="#">
                                    <i class="icofont icofont-ui-user"></i><span>John Doe</span>crm_vat_date_of_registration
                                    </a> -->
                                 <p class="m-t-0"> Tax Payment Date to HMRC: <?php echo (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) !='' ?  date('F j, Y, g:i', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])): ''); ?></p>
                                 <!-- for notifications -->
                                 <p class="m-t-20">
                                 <p><label>Services :</label><?php   (isset(json_decode($getCompanyvalue['company_tax_return'])->tab) && $getCompanyvalue['company_tax_return'] != '') ? $jsncompany_tax_return =  json_decode($getCompanyvalue['company_tax_return'])->tab : $jsncompany_tax_return = '';
                                    ($jsncompany_tax_return!='') ? $company_tax_return = "On" : $company_tax_return = "Off"; echo $company_tax_return;?></p>
                                 <p><label>Reminder :</label><?php   (isset(json_decode($getCompanyvalue['company_tax_return'])->reminder) && $getCompanyvalue['company_tax_return'] != '') ? $jsncompany_tax_return =  json_decode($getCompanyvalue['company_tax_return'])->reminder : $jsncompany_tax_return = '';
                                    ($jsncompany_tax_return!='') ? $company_tax_return = "On" : $company_tax_return = "Off"; echo $company_tax_return; ?></p>
                                 <p><label>Text :</label><?php   (isset(json_decode($getCompanyvalue['company_tax_return'])->text) && $getCompanyvalue['company_tax_return'] != '') ? $jsncompany_tax_return =  json_decode($getCompanyvalue['company_tax_return'])->text : $jsncompany_tax_return = '';
                                    ($jsncompany_tax_return!='') ? $company_tax_return = "On" : $company_tax_return = "Off"; echo $company_tax_return;?></p>
                                 <p><label>Invoice :</label><?php  echo (isset(json_decode($getCompanyvalue['company_tax_return'])->invoice) && $getCompanyvalue['company_tax_return'] != '') ? $jsncompany_tax_return =  json_decode($getCompanyvalue['company_tax_return'])->invoice : $jsncompany_tax_return = '';
                                    ($jsncompany_tax_return!='') ? $company_tax_return = "On" : $company_tax_return = "Off"; echo $company_tax_return;?></p>
                                 </p>
                                 <!-- end of notification -->
                              </div>
                           </div>
                           <span class="cd-date"><?php if(isset($getusername['CreatedTime'])){ echo date('F j, Y, g:i',$getusername['CreatedTime']); } ?></span>
                           <span class="cd-details"><?php echo $getCompanyvalue['crm_company_name'];?> Company Tax Return Info </span>
                        </div>
                     </div>
                     <?php }
                        } }
                        if($value=="Personal Tax Return" || $value=="ALL"){
                        foreach ($client_details as $getCompanykey => $getCompanyvalue) { 
                              if($getCompanyvalue['blocked_status']==0){
                        $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                        ?>
                     <div class="cd-timeline-block <?php echo str_replace(' ', '', $value); ?>">
                        <div class="cd-timeline-icon bg-primary">
                           <i class="icofont icofont-ui-file"></i>
                        </div>
                        <div class="cd-timeline-content card_main">
                           <div class="p-20">
                              <h6><?php echo $getusername['crm_name'];?> - <?php echo $getCompanyvalue['crm_legal_form'];?></h6>
                              <div class="timeline-details">
                                 <a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_personal_due_date_return'])): ''); ?></span> </a>
                                 <!--  <a href="#">
                                    <i class="icofont icofont-ui-user"></i><span>John Doe</span>crm_vat_date_of_registration
                                    </a> -->
                                    <br>
                                 <a href="#"> <i class="icofont icofont-ui-calendar"></i><span> Tax Return Date: <?php echo (strtotime($getCompanyvalue['crm_personal_tax_return_date']) !='' ?  date('F j, Y, g:i', strtotime($getCompanyvalue['crm_personal_tax_return_date'])): ''); ?></span> </a>

                                 <p class="m-t-0"> Due Date online filing: <?php echo (strtotime($getCompanyvalue['crm_personal_due_date_online']) !='' ?  date('F j, Y, g:i', strtotime($getCompanyvalue['crm_personal_due_date_online'])): ''); ?></p>
                       
                                 <!-- for notifications -->
                                 <p class="m-t-20">
                                 <p><label>Services :</label><?php   (isset(json_decode($getCompanyvalue['personal_tax_return'])->tab) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnpersonal_tax_return =  json_decode($getCompanyvalue['personal_tax_return'])->tab : $jsnpersonal_tax_return = '';
                                    ($jsnpersonal_tax_return!='') ? $personal_tax_return = "On" : $personal_tax_return = "Off"; echo $personal_tax_return;?></p>
                                 <p><label>Reminder :</label><?php   (isset(json_decode($getCompanyvalue['personal_tax_return'])->reminder) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnpersonal_tax_return =  json_decode($getCompanyvalue['personal_tax_return'])->reminder : $jsnpersonal_tax_return = '';
                                    ($jsnpersonal_tax_return!='') ? $personal_tax_return = "On" : $personal_tax_return = "Off"; echo $personal_tax_return; ?></p>
                                 <p><label>Text :</label><?php   (isset(json_decode($getCompanyvalue['personal_tax_return'])->text) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnpersonal_tax_return =  json_decode($getCompanyvalue['personal_tax_return'])->text : $jsnpersonal_tax_return = '';
                                    ($jsnpersonal_tax_return!='') ? $personal_tax_return = "On" : $personal_tax_return = "Off"; echo $personal_tax_return;?></p>
                                 <p><label>Invoice :</label><?php  echo (isset(json_decode($getCompanyvalue['personal_tax_return'])->invoice) && $getCompanyvalue['personal_tax_return'] != '') ? $jsnpersonal_tax_return =  json_decode($getCompanyvalue['personal_tax_return'])->invoice : $jsnpersonal_tax_return = '';
                                    ($jsnpersonal_tax_return!='') ? $personal_tax_return = "On" : $personal_tax_return = "Off"; echo $personal_tax_return;?></p>
                                 </p>
                                 <!-- end of notification -->
                              </div>
                           </div>
                           <span class="cd-date"><?php if(isset($getusername['CreatedTime'])){ echo date('F j, Y, g:i',$getusername['CreatedTime']); } ?></span>
                           <span class="cd-details"><?php echo $getCompanyvalue['crm_company_name'];?> Personal Tax Return Info </span>
                        </div>
                     </div>
                     <?php }
                        } }
                        if($value=="WorkPlace Pension - AE" || $value=="ALL"){
                        foreach ($client_details as $getCompanykey => $getCompanyvalue) { 
                              if($getCompanyvalue['blocked_status']==0){
                        $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                        ?>
                     <div class="cd-timeline-block <?php echo str_replace(' ', '', $value); ?>">
                        <div class="cd-timeline-icon bg-primary">
                           <i class="icofont icofont-ui-file"></i>
                        </div>
                        <div class="cd-timeline-content card_main">
                           <div class="p-20">
                              <h6><a href="<?php echo base_url().'client/client_infomation/'.$getCompanyvalue['user_id']?>" target="_blank"><?php echo $getusername['crm_name'];?> - <?php echo $getCompanyvalue['crm_legal_form'];?></a></h6>
                              <div class="timeline-details">
                                 <a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_pension_subm_due_date'])): ''); ?></span> </a>
                                 <!--  <a href="#">
                                    <i class="icofont icofont-ui-user"></i><span>John Doe</span>crm_vat_date_of_registration
                                    </a> -->
                                 <p class="m-t-0"> Staging Date: <?php echo (strtotime($getCompanyvalue['crm_staging_date']) !='' ?  date('F j, Y, g:i', strtotime($getCompanyvalue['crm_staging_date'])): ''); ?></p>
                                 <p class="m-t-0"> Pension ID: <?php echo ($getCompanyvalue['crm_pension_id'] !='' ? $getCompanyvalue['crm_pension_id']: '-'); ?></p>
                                 <p class="m-t-0"> Declaration of Compliance Due: <?php echo (strtotime($getCompanyvalue['crm_declaration_of_compliance_due_date']) !='' ?  date('F j, Y, g:i', strtotime($getCompanyvalue['crm_declaration_of_compliance_due_date'])): ''); ?></p>
                                 <p class="m-t-0"> Pension Provider Name: <?php echo ($getCompanyvalue['crm_paye_pension_provider'] !='' ? $getCompanyvalue['crm_paye_pension_provider']: '-'); ?></p>
                                 <p class="m-t-0"> Employer Contribution Percentage: <?php echo ($getCompanyvalue['crm_employer_contri_percentage'] !='' ? $getCompanyvalue['crm_employer_contri_percentage']: '-'); ?></p>
                                 <p class="m-t-0"> Employee Contribution Percentage: <?php echo ($getCompanyvalue['crm_employee_contri_percentage'] !='' ? $getCompanyvalue['crm_employee_contri_percentage']: '-'); ?></p>
                                 <!-- for notifications -->
                                 <p class="m-t-20">
                                 <p><label>Services :</label><?php   (isset(json_decode($getCompanyvalue['workplace'])->tab) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->tab : $jsnworkplace = '';
                                    ($jsnworkplace!='') ? $workplace = "On" : $workplace = "Off"; echo $workplace;?></p>
                                 <p><label>Reminder :</label><?php   (isset(json_decode($getCompanyvalue['workplace'])->reminder) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->reminder : $jsnworkplace = '';
                                    ($jsnworkplace!='') ? $workplace = "On" : $workplace = "Off"; echo $workplace; ?></p>
                                 <p><label>Text :</label><?php   (isset(json_decode($getCompanyvalue['workplace'])->text) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->text : $jsnworkplace = '';
                                    ($jsnworkplace!='') ? $workplace = "On" : $workplace = "Off"; echo $workplace;?></p>
                                 <p><label>Invoice :</label><?php  echo (isset(json_decode($getCompanyvalue['workplace'])->invoice) && $getCompanyvalue['workplace'] != '') ? $jsnworkplace =  json_decode($getCompanyvalue['workplace'])->invoice : $jsnworkplace = '';
                                    ($jsnworkplace!='') ? $workplace = "On" : $workplace = "Off"; echo $workplace;?></p>
                                 </p>
                                 <!-- end of notification -->
                              </div>
                           </div>
                           <span class="cd-date"><?php if(isset($getusername['CreatedTime'])){ echo date('F j, Y, g:i',$getusername['CreatedTime']); } ?></span>
                           <span class="cd-details"><?php echo $getCompanyvalue['crm_company_name'];?> WorkPlace Pension - AE Info </span>
                        </div>
                     </div>
                     <?php }
                        } }
                        if($value=="CIS - Contractor" || $value=="ALL"){
                        foreach ($client_details as $getCompanykey => $getCompanyvalue) { 
                              if($getCompanyvalue['blocked_status']==0){
                        $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                        ?>
                     <div class="cd-timeline-block <?php echo str_replace(' ', '', $value); ?>">
                        <div class="cd-timeline-icon bg-primary">
                           <i class="icofont icofont-ui-file"></i>
                        </div>
                        <div class="cd-timeline-content card_main">
                           <div class="p-20">
                              <h6><a href="<?php echo base_url().'client/client_infomation/'.$getCompanyvalue['user_id']?>" target="_blank"><?php echo $getusername['crm_name'];?> - <?php echo $getCompanyvalue['crm_legal_form'];?></a></h6>
                              <div class="timeline-details">
                                 <a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php //echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_pension_subm_due_date'])): ''); ?></span> </a>
                                  <a href="#">
                                    <span>
                                       C.I.S Frequency : <?php echo $getCompanyvalue['crm_cis_frequency'];?> 
                                    </span>
                                  </a>
                                  <br>
                                 <a href="#">
                                    <span>
                                     Start Date: <?php echo (strtotime($getCompanyvalue['crm_cis_contractor_start_date']) !='' ?  date('F j, Y, g:i', strtotime($getCompanyvalue['crm_cis_contractor_start_date'])): ''); ?>
                                    </span>
                                 </a>
                                 <p class="m-t-0"> C.I.S Contractor : <?php echo ($getCompanyvalue['crm_cis_contractor'] !='' ? $getCompanyvalue['crm_cis_contractor']: '-'); ?></p>

                                 <p class="m-t-0"> C.I.S Scheme: <?php echo ($getCompanyvalue['crm_cis_scheme_notes'] !='' ? $getCompanyvalue['crm_cis_scheme_notes']: '-'); ?></p>
                                 <!-- for notifications -->
                                 <p class="m-t-20">
                                 <p><label>Services :</label><?php   (isset(json_decode($getCompanyvalue['cis'])->tab) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->tab : $jsncis = '';
                                    ($jsncis!='') ? $cis = "On" : $cis = "Off"; echo $cis;?></p>
                                 <p><label>Reminder :</label><?php   (isset(json_decode($getCompanyvalue['cis'])->reminder) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->reminder : $jsncis = '';
                                    ($jsncis!='') ? $cis = "On" : $cis = "Off"; echo $cis; ?></p>
                                 <p><label>Text :</label><?php   (isset(json_decode($getCompanyvalue['cis'])->text) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->text : $jsncis = '';
                                    ($jsncis!='') ? $cis = "On" : $cis = "Off"; echo $cis;?></p>
                                 <p><label>Invoice :</label><?php  echo (isset(json_decode($getCompanyvalue['cis'])->invoice) && $getCompanyvalue['cis'] != '') ? $jsncis =  json_decode($getCompanyvalue['cis'])->invoice : $jsncis = '';
                                    ($jsncis!='') ? $cis = "On" : $cis = "Off"; echo $cis;?></p>
                                 </p>
                                 <!-- end of notification -->
                              </div>
                           </div>
                           <span class="cd-date"><?php if(isset($getusername['CreatedTime'])){ echo date('F j, Y, g:i',$getusername['CreatedTime']); } ?></span>
                           <span class="cd-details"><?php echo $getCompanyvalue['crm_company_name'];?> CIS - Contractor Info </span>
                        </div>
                     </div>
                     <?php }
                        } }
                        if($value=="CIS - Sub Contractor" || $value=="ALL"){
                        foreach ($client_details as $getCompanykey => $getCompanyvalue) { 
                              if($getCompanyvalue['blocked_status']==0){
                        $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                        ?>
                     <div class="cd-timeline-block <?php echo str_replace(' ', '', $value); ?>">
                        <div class="cd-timeline-icon bg-primary">
                           <i class="icofont icofont-ui-file"></i>
                        </div>
                        <div class="cd-timeline-content card_main">
                           <div class="p-20">
                              <h6><a href="<?php echo base_url().'client/client_infomation/'.$getCompanyvalue['user_id']?>" target="_blank"><?php echo $getusername['crm_name'];?> - <?php echo $getCompanyvalue['crm_legal_form'];?></a></h6>
                              <div class="timeline-details">
                                 <a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php //echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_pension_subm_due_date'])): ''); ?></span> </a>
                                 <!--  <a href="#">
                                    <i class="icofont icofont-ui-user"></i><span>John Doe</span>crm_vat_date_of_registration
                                    </a> -->
                                 <p class="m-t-0"> C.I.S Sub Contractor : <?php echo ($getCompanyvalue['crm_cis_subcontractor'] !='' ? $getCompanyvalue['crm_cis_subcontractor']: '-'); ?></p>
                                 <p class="m-t-0"> Start Date: <?php echo (strtotime($getCompanyvalue['crm_cis_subcontractor_start_date']) !='' ?  date('F j, Y, g:i', strtotime($getCompanyvalue['crm_cis_subcontractor_start_date'])): ''); ?></p>
                               
                                 <!-- for notifications -->
                                 <p class="m-t-20">
                                 <p><label>Services :</label><?php   (isset(json_decode($getCompanyvalue['cissub'])->tab) && $getCompanyvalue['cissub'] != '') ? $jsncissub =  json_decode($getCompanyvalue['cissub'])->tab : $jsncissub = '';
                                    ($jsncissub!='') ? $cissub = "On" : $cissub = "Off"; echo $cissub;?></p>
                                 <p><label>Reminder :</label><?php   (isset(json_decode($getCompanyvalue['cissub'])->reminder) && $getCompanyvalue['cissub'] != '') ? $jsncissub =  json_decode($getCompanyvalue['cissub'])->reminder : $jsncissub = '';
                                    ($jsncissub!='') ? $cissub = "On" : $cissub = "Off"; echo $cissub; ?></p>
                                 <p><label>Text :</label><?php   (isset(json_decode($getCompanyvalue['cissub'])->text) && $getCompanyvalue['cissub'] != '') ? $jsncissub =  json_decode($getCompanyvalue['cissub'])->text : $jsncissub = '';
                                    ($jsncissub!='') ? $cissub = "On" : $cissub = "Off"; echo $cissub;?></p>
                                 <p><label>Invoice :</label><?php  echo (isset(json_decode($getCompanyvalue['cissub'])->invoice) && $getCompanyvalue['cissub'] != '') ? $jsncissub =  json_decode($getCompanyvalue['cissub'])->invoice : $jsncissub = '';
                                    ($jsncissub!='') ? $cissub = "On" : $cissub = "Off"; echo $cissub;?></p>
                                 </p>
                                 <!-- end of notification -->
                              </div>
                           </div>
                           <span class="cd-date"><?php if(isset($getusername['CreatedTime'])){ echo date('F j, Y, g:i',$getusername['CreatedTime']); } ?></span>
                           <span class="cd-details"><?php echo $getCompanyvalue['crm_company_name'];?> CIS - Sub Contractor Info </span>
                        </div>
                     </div>
                     <?php }
                        } }
                        if($value=="P11D" || $value=="ALL" ){
                        foreach ($client_details as $getCompanykey => $getCompanyvalue) { 
                              if($getCompanyvalue['blocked_status']==0){
                        $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                        ?>
                     <div class="cd-timeline-block <?php echo str_replace(' ', '', $value); ?>">
                        <div class="cd-timeline-icon bg-primary">
                           <i class="icofont icofont-ui-file"></i>
                        </div>
                        <div class="cd-timeline-content card_main">
                           <div class="p-20">
                              <h6><a href="<?php echo base_url().'client/client_infomation/'.$getCompanyvalue['user_id']?>" target="_blank"><?php echo $getusername['crm_name'];?> - <?php echo $getCompanyvalue['crm_legal_form'];?></a></h6>
                              <div class="timeline-details">
                                 <a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php echo (strtotime($getCompanyvalue['crm_next_p11d_return_due']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_next_p11d_return_due'])): ''); ?></span> </a>
                                 <!--  <a href="#">
                                    <i class="icofont icofont-ui-user"></i><span>John Doe</span>crm_vat_date_of_registration
                                    </a> -->
                                 <p class="m-t-0"> Start Date: <?php echo (strtotime($getCompanyvalue['crm_p11d_latest_action_date']) !='' ?  date('F j, Y, g:i', strtotime($getCompanyvalue['crm_p11d_latest_action_date'])): ''); ?></p>
                                 <p class="m-t-0"> First Benefit Pay Date: <?php echo (strtotime($getCompanyvalue['crm_latest_p11d_submitted']) !='' ?  date('F j, Y, g:i', strtotime($getCompanyvalue['crm_latest_p11d_submitted'])): ''); ?></p>
                                 <p class="m-t-0"> p11d_paye_scheme_ceased: <?php echo (strtotime($getCompanyvalue['crm_p11d_records_received']) !='' ?  date('F j, Y, g:i', strtotime($getCompanyvalue['crm_p11d_records_received'])): ''); ?></p>
                                 <!-- for notifications -->
                                
                                 <p class="m-t-20">
                                 <p><label>Services :</label><?php   (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = '';
                                    ($jsnp11d!='') ? $p11d = "On" : $p11d = "Off"; echo $p11d;?></p>
                                 <p><label>Reminder :</label><?php   (isset(json_decode($getCompanyvalue['p11d'])->reminder) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->reminder : $jsnp11d = '';
                                    ($jsnp11d!='') ? $p11d = "On" : $p11d = "Off"; echo $p11d; ?></p>
                                 <p><label>Text :</label><?php   (isset(json_decode($getCompanyvalue['p11d'])->text) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->text : $jsnp11d = '';
                                    ($jsnp11d!='') ? $p11d = "On" : $p11d = "Off"; echo $p11d;?></p>
                                 <p><label>Invoice :</label><?php  echo (isset(json_decode($getCompanyvalue['p11d'])->invoice) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->invoice : $jsnp11d = '';
                                    ($jsnp11d!='') ? $p11d = "On" : $p11d = "Off"; echo $p11d;?></p>
                                 </p>
                                 <!-- end of notification -->        
                              </div>
                           </div>
                           <span class="cd-date"><?php if(isset($getusername['CreatedTime'])){ echo date('F j, Y, g:i',$getusername['CreatedTime']); } ?></span>
                           <span class="cd-details"><?php echo $getCompanyvalue['crm_company_name'];?> P11D Info </span>
                        </div>
                     </div>
                     <?php }
                        } }
                        
                        if($value=="Bookkeeping" || $value=="ALL"){
                        foreach ($client_details as $getCompanykey => $getCompanyvalue) { 
                              if($getCompanyvalue['blocked_status']==0){
                        $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                        ?>
                     <div class="cd-timeline-block <?php echo str_replace(' ', '', $value); ?>">
                        <div class="cd-timeline-icon bg-primary">
                           <i class="icofont icofont-ui-file"></i>
                        </div>
                        <div class="cd-timeline-content card_main">
                           <div class="p-20">
                              <h6><a href="<?php echo base_url().'client/client_infomation/'.$getCompanyvalue['user_id']?>" target="_blank"><?php echo $getusername['crm_name'];?> - <?php echo $getCompanyvalue['crm_legal_form'];?></a></h6>
                              <div class="timeline-details">
                                 <a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php echo (strtotime($getCompanyvalue['crm_next_booking_date']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_next_booking_date'])): ''); ?></span> </a>
                              </br>
                                 <a href="#">
                                    <span>
                                    Bookkeepeing to Done: <?php echo ($getCompanyvalue['crm_bookkeeping'] !='' ? $getCompanyvalue['crm_bookkeeping']: '-'); ?>
                                    </span>
                                 </a>

                                 <p class="m-t-0"> Method of Bookkeeping: <?php echo ($getCompanyvalue['crm_method_bookkeeping'] !='' ? $getCompanyvalue['crm_method_bookkeeping']: '-'); ?></p>
                                 <p class="m-t-0"> How Client will provide records: <?php echo ($getCompanyvalue['crm_client_provide_record'] !='' ? $getCompanyvalue['crm_client_provide_record']: '-'); ?></p>
                                 <!-- for notifications -->
                                 <p class="m-t-20">
                                 <p><label>Services :</label><?php   (isset(json_decode($getCompanyvalue['p11d'])->tab) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->tab : $jsnp11d = '';
                                    ($jsnp11d!='') ? $p11d = "On" : $p11d = "Off"; echo $p11d;?></p>
                                 <p><label>Reminder :</label><?php   (isset(json_decode($getCompanyvalue['p11d'])->reminder) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->reminder : $jsnp11d = '';
                                    ($jsnp11d!='') ? $p11d = "On" : $p11d = "Off"; echo $p11d; ?></p>
                                 <p><label>Text :</label><?php   (isset(json_decode($getCompanyvalue['p11d'])->text) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->text : $jsnp11d = '';
                                    ($jsnp11d!='') ? $p11d = "On" : $p11d = "Off"; echo $p11d;?></p>
                                 <p><label>Invoice :</label><?php  echo (isset(json_decode($getCompanyvalue['p11d'])->invoice) && $getCompanyvalue['p11d'] != '') ? $jsnp11d =  json_decode($getCompanyvalue['p11d'])->invoice : $jsnp11d = '';
                                    ($jsnp11d!='') ? $p11d = "On" : $p11d = "Off"; echo $p11d;?></p>
                                 </p>
                                 <!-- end of notification -->  
                              </div>
                           </div>
                           <span class="cd-date"><?php if(isset($getusername['CreatedTime'])){ echo date('F j, Y, g:i',$getusername['CreatedTime']); } ?></span>
                           <span class="cd-details"><?php echo $getCompanyvalue['crm_company_name'];?> Bookkeeping Info </span>
                        </div>
                     </div>
                     <?php }
                        } }
                        if($value=="Management Accounts" || $value=="ALL" ){
                        foreach ($client_details as $getCompanykey => $getCompanyvalue) { 
                              if($getCompanyvalue['blocked_status']==0){
                        $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                        ?>
                     <div class="cd-timeline-block <?php echo str_replace(' ', '', $value); ?>">
                        <div class="cd-timeline-icon bg-primary">
                           <i class="icofont icofont-ui-file"></i>
                        </div>
                        <div class="cd-timeline-content card_main">
                           <div class="p-20">
                              <h6><a href="<?php echo base_url().'client/client_infomation/'.$getCompanyvalue['user_id']?>" target="_blank"><?php echo $getusername['crm_name'];?> - <?php echo $getCompanyvalue['crm_legal_form'];?></a></h6>
                              <div class="timeline-details">
                                 <a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php echo (strtotime($getCompanyvalue['crm_next_manage_acc_date']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_next_manage_acc_date'])): ''); ?></span> </a>
                              </br>
                              <a href="#"> 
                                 <span>
                                  Management Accounts Frequency: <?php echo ($getCompanyvalue['crm_manage_acc_fre'] !='' ? $getCompanyvalue['crm_manage_acc_fre']: '-'); ?>
                                 </span>
                              </a>
                                 <p class="m-t-0"> Method of Bookkeeping: <?php echo ($getCompanyvalue['crm_manage_method_bookkeeping'] !='' ? $getCompanyvalue['crm_manage_method_bookkeeping']: '-'); ?></p>
                                 <p class="m-t-0"> How Client will provide records: <?php echo ($getCompanyvalue['crm_manage_client_provide_record'] !='' ? $getCompanyvalue['crm_manage_client_provide_record']: '-'); ?></p>
                                 <!-- for notifications -->
                               <!-- for notes section -->
                                 <p class="m-t-0">
                                    Notes:<?php echo (isset($getCompanyvalue['crm_manage_notes']) && $getCompanyvalue['crm_manage_notes']!='0')?$getCompanyvalue['crm_manage_notes']:'-';  ?>
                                 </p>
                                 <!-- end of notes -->

                                 <p class="m-t-20">
                                 <p><label>Services :</label><?php   (isset(json_decode($getCompanyvalue['management'])->tab) && $getCompanyvalue['management'] != '') ? $jsnmanagement =  json_decode($getCompanyvalue['management'])->tab : $jsnmanagement = '';
                                    ($jsnmanagement!='') ? $management = "On" : $management = "Off"; echo $management;?></p>
                                 <p><label>Reminder :</label><?php   (isset(json_decode($getCompanyvalue['management'])->reminder) && $getCompanyvalue['management'] != '') ? $jsnmanagement =  json_decode($getCompanyvalue['management'])->reminder : $jsnmanagement = '';
                                    ($jsnmanagement!='') ? $management = "On" : $management = "Off"; echo $management; ?></p>
                                 <p><label>Text :</label><?php   (isset(json_decode($getCompanyvalue['management'])->text) && $getCompanyvalue['management'] != '') ? $jsnmanagement =  json_decode($getCompanyvalue['management'])->text : $jsnmanagement = '';
                                    ($jsnmanagement!='') ? $management = "On" : $management = "Off"; echo $management;?></p>
                                 <p><label>Invoice :</label><?php  echo (isset(json_decode($getCompanyvalue['management'])->invoice) && $getCompanyvalue['management'] != '') ? $jsnmanagement =  json_decode($getCompanyvalue['management'])->invoice : $jsnmanagement = '';
                                    ($jsnmanagement!='') ? $management = "On" : $management = "Off"; echo $management;?></p>
                                 </p>
                                 <!-- end of notification -->  
                              </div>
                           </div>
                           <span class="cd-date"><?php if(isset($getusername['CreatedTime'])){ echo date('F j, Y, g:i',$getusername['CreatedTime']); } ?></span>
                           <span class="cd-details"><?php echo $getCompanyvalue['crm_company_name'];?> Management Accounts Info </span>
                        </div>
                     </div>
                     <?php }
                        } }
                          if($value=="Investigation Insurance" || $value=="ALL" ){
                        foreach ($client_details as $getCompanykey => $getCompanyvalue) { 
                              if($getCompanyvalue['blocked_status']==0){
                        $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                        ?>
                     <div class="cd-timeline-block <?php echo str_replace(' ', '', $value); ?>">
                        <div class="cd-timeline-icon bg-primary">
                           <i class="icofont icofont-ui-file"></i>
                        </div>
                        <div class="cd-timeline-content card_main">
                           <div class="p-20">
                              <h6><a href="<?php echo base_url().'client/client_infomation/'.$getCompanyvalue['user_id']?>" target="_blank"><?php echo $getusername['crm_name'];?> - <?php echo $getCompanyvalue['crm_legal_form'];?></a></h6>
                              <div class="timeline-details">
                                 <a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php echo (strtotime($getCompanyvalue['crm_insurance_renew_date']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_insurance_renew_date'])): ''); ?></span> </a>
                                 <p class="m-t-0"> Start Date: <?php echo (strtotime($getCompanyvalue['crm_invesitgation_insurance']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_invesitgation_insurance'])): ''); ?></p>
                                 <p class="m-t-0"> Insurance Provider: <?php echo ($getCompanyvalue['crm_insurance_provider'] !='' ? $getCompanyvalue['crm_insurance_provider']: '-'); ?></p>
                                 <p class="m-t-0"> History of Previous Claims: <?php echo ($getCompanyvalue['crm_claims_note'] !='' ? $getCompanyvalue['crm_claims_note']: '-'); ?></p>
                                 <!-- for notifications -->
                                 <p class="m-t-20">
                                 <p><label>Services :</label><?php   (isset(json_decode($getCompanyvalue['investgate'])->tab) && $getCompanyvalue['investgate'] != '') ? $jsninvestgate =  json_decode($getCompanyvalue['investgate'])->tab : $jsninvestgate = '';
                                    ($jsninvestgate!='') ? $investgate = "On" : $investgate = "Off"; echo $investgate;?></p>
                                 <p><label>Reminder :</label><?php   (isset(json_decode($getCompanyvalue['investgate'])->reminder) && $getCompanyvalue['investgate'] != '') ? $jsninvestgate =  json_decode($getCompanyvalue['investgate'])->reminder : $jsninvestgate = '';
                                    ($jsninvestgate!='') ? $investgate = "On" : $investgate = "Off"; echo $investgate; ?></p>
                                 <p><label>Text :</label><?php   (isset(json_decode($getCompanyvalue['investgate'])->text) && $getCompanyvalue['investgate'] != '') ? $jsninvestgate =  json_decode($getCompanyvalue['investgate'])->text : $jsninvestgate = '';
                                    ($jsninvestgate!='') ? $investgate = "On" : $investgate = "Off"; echo $investgate;?></p>
                                 <p><label>Invoice :</label><?php  echo (isset(json_decode($getCompanyvalue['investgate'])->invoice) && $getCompanyvalue['investgate'] != '') ? $jsninvestgate =  json_decode($getCompanyvalue['investgate'])->invoice : $jsninvestgate = '';
                                    ($jsninvestgate!='') ? $investgate = "On" : $investgate = "Off"; echo $investgate;?></p>
                                 </p>
                                 <!-- end of notification -->  
                              </div>
                           </div>
                           <span class="cd-date"><?php if(isset($getusername['CreatedTime'])){ echo date('F j, Y, g:i',$getusername['CreatedTime']); } ?></span>
                           <span class="cd-details"><?php echo $getCompanyvalue['crm_company_name'];?> Investigation Insurance Info </span>
                        </div>
                     </div>
                     <?php }
                        } }
                        if($value=="Registered Address" || $value=="ALL"){
                        foreach ($client_details as $getCompanykey => $getCompanyvalue) { 
                              if($getCompanyvalue['blocked_status']==0){
                        $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                        ?>
                     <div class="cd-timeline-block <?php echo str_replace(' ', '', $value); ?>">
                        <div class="cd-timeline-icon bg-primary">
                           <i class="icofont icofont-ui-file"></i>
                        </div>
                        <div class="cd-timeline-content card_main">
                           <div class="p-20">
                              <h6><a href="<?php echo base_url().'client/client_infomation/'.$getCompanyvalue['user_id']?>" target="_blank"><?php echo $getusername['crm_name'];?> - <?php echo $getCompanyvalue['crm_legal_form'];?></a></h6>
                              <div class="timeline-details">
                                 <a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php echo (strtotime($getCompanyvalue['crm_registered_renew_date']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_registered_renew_date'])): ''); ?></span> </a>
                                 <p class="m-t-0"> Start Date: <?php echo (strtotime($getCompanyvalue['crm_registered_start_date']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_registered_start_date'])): ''); ?></p>
                                 <p class="m-t-0"> Registered office in Use: <?php echo ($getCompanyvalue['crm_registered_office_inuse'] !='' ? $getCompanyvalue['crm_registered_office_inuse']: '-'); ?></p>
                                 <p class="m-t-0"> History of Previous Claims: <?php echo ($getCompanyvalue['crm_registered_claims_note'] !='' ? $getCompanyvalue['crm_registered_claims_note']: '-'); ?></p>
                                 <!-- for notifications -->
                                 <p class="m-t-20">
                                 <p><label>Services :</label><?php   (isset(json_decode($getCompanyvalue['registered'])->tab) && $getCompanyvalue['registered'] != '') ? $jsnregistered =  json_decode($getCompanyvalue['registered'])->tab : $jsnregistered = '';
                                    ($jsnregistered!='') ? $registered = "On" : $registered = "Off"; echo $registered;?></p>
                                 <p><label>Reminder :</label><?php   (isset(json_decode($getCompanyvalue['registered'])->reminder) && $getCompanyvalue['registered'] != '') ? $jsnregistered =  json_decode($getCompanyvalue['registered'])->reminder : $jsnregistered = '';
                                    ($jsnregistered!='') ? $registered = "On" : $registered = "Off"; echo $registered; ?></p>
                                 <p><label>Text :</label><?php   (isset(json_decode($getCompanyvalue['registered'])->text) && $getCompanyvalue['registered'] != '') ? $jsnregistered =  json_decode($getCompanyvalue['registered'])->text : $jsnregistered = '';
                                    ($jsnregistered!='') ? $registered = "On" : $registered = "Off"; echo $registered;?></p>
                                 <p><label>Invoice :</label><?php  echo (isset(json_decode($getCompanyvalue['registered'])->invoice) && $getCompanyvalue['registered'] != '') ? $jsnregistered =  json_decode($getCompanyvalue['registered'])->invoice : $jsnregistered = '';
                                    ($jsnregistered!='') ? $registered = "On" : $registered = "Off"; echo $registered;?></p>
                                 </p>
                                 <!-- end of notification --> 
                              </div>
                           </div>
                           <span class="cd-date"><?php if(isset($getusername['CreatedTime'])){ echo date('F j, Y, g:i',$getusername['CreatedTime']); } ?></span>
                           <span class="cd-details"><?php echo $getCompanyvalue['crm_company_name'];?> Registered Address Info </span>
                        </div>
                     </div>
                     <?php }
                        } }
                        if($value=="Tax Investigation/Tax Advice" || $value=="ALL" || $value == 'Tax Advice'){
                        foreach ($client_details as $getCompanykey => $getCompanyvalue) { 
                              if($getCompanyvalue['blocked_status']==0){
                        $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                        ?>
                     <div class="cd-timeline-block <?php echo str_replace(' ', '', $value); ?>">
                        <div class="cd-timeline-icon bg-primary">
                           <i class="icofont icofont-ui-file"></i>
                        </div>
                        <div class="cd-timeline-content card_main">
                           <div class="p-20">
                              <h6><a href="<?php echo base_url().'client/client_infomation/'.$getCompanyvalue['user_id']?>" target="_blank"><?php echo $getusername['crm_name'];?> - <?php echo $getCompanyvalue['crm_legal_form'];?></a></h6>
                              <div class="timeline-details">
                                 <a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></span> </a>
                                 <p class="m-t-0"> Start Date: <?php echo (strtotime($getCompanyvalue['crm_investigation_start_date']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_investigation_start_date'])): ''); ?></p>
                                 <p class="m-t-0"> Notes: <?php echo ($getCompanyvalue['crm_investigation_note'] !='' ? $getCompanyvalue['crm_investigation_note']: '-'); ?></p>
                                 <!-- for notifications -->
                                 <p class="m-t-20">
                                 <p><label>Services :</label><?php   (isset(json_decode($getCompanyvalue['taxadvice'])->tab) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxadvice =  json_decode($getCompanyvalue['taxadvice'])->tab : $jsntaxadvice = '';
                                    ($jsntaxadvice!='') ? $taxadvice = "On" : $taxadvice = "Off"; echo $taxadvice;?></p>
                                 <p><label>Reminder :</label><?php   (isset(json_decode($getCompanyvalue['taxadvice'])->reminder) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxadvice =  json_decode($getCompanyvalue['taxadvice'])->reminder : $jsntaxadvice = '';
                                    ($jsntaxadvice!='') ? $taxadvice = "On" : $taxadvice = "Off"; echo $taxadvice; ?></p>
                                 <p><label>Text :</label><?php   (isset(json_decode($getCompanyvalue['taxadvice'])->text) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxadvice =  json_decode($getCompanyvalue['taxadvice'])->text : $jsntaxadvice = '';
                                    ($jsntaxadvice!='') ? $taxadvice = "On" : $taxadvice = "Off"; echo $taxadvice;?></p>
                                 <p><label>Invoice :</label><?php  echo (isset(json_decode($getCompanyvalue['taxadvice'])->invoice) && $getCompanyvalue['taxadvice'] != '') ? $jsntaxadvice =  json_decode($getCompanyvalue['taxadvice'])->invoice : $jsntaxadvice = '';
                                    ($jsntaxadvice!='') ? $taxadvice = "On" : $taxadvice = "Off"; echo $taxadvice;?></p>
                                 </p>
                                 <!-- end of notification --> 
                              </div>
                           </div>
                           <span class="cd-date"><?php if(isset($getusername['CreatedTime'])){ echo date('F j, Y, g:i',$getusername['CreatedTime']); } ?></span>
                           <span class="cd-details"><?php echo $getCompanyvalue['crm_company_name'];?> Tax Advice Info </span>
                        </div>
                     </div>
                     <?php }
                        }
                     }

                     ?>

                      <?php  

                      foreach ($other_services as $key1 => $value1) 
                      {
                          if(isset($value1['id']))
                          {
                             if($value == $value1['service_name'] || $value=="ALL")
                             {
                                 foreach ($client_details as $getCompanykey => $getCompanyvalue) 
                                 { 
                                    if($getCompanyvalue['blocked_status']==0)
                                    {
                                       $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                      ?>
                     <div class="cd-timeline-block <?php echo str_replace(' ', '', $value); ?>">
                        <div class="cd-timeline-icon bg-primary">
                           <i class="icofont icofont-ui-file"></i>
                        </div>
                        <div class="cd-timeline-content card_main">
                           <div class="p-20">
                              <h6><a href="<?php echo base_url().'client/client_infomation/'.$getCompanyvalue['user_id']?>" target="_blank"><?php echo $getusername['crm_name'];?> - <?php echo $getCompanyvalue['crm_legal_form'];?></a></h6>
                              <div class="timeline-details">
                                 <a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php echo (strtotime($getCompanyvalue['crm_'.$value1['services_subnames'].'_statement_date']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_'.$value1['services_subnames'].'_statement_date'])): ''); ?></span> </a>
                                 <p class="m-t-0"> Start Date: <?php echo (strtotime($getCompanyvalue['crm_'.$value1['services_subnames'].'_statement_date']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_'.$value1['services_subnames'].'_statement_date'])): ''); ?></p>
                                
                                 <!-- for notifications -->
                                 <p class="m-t-20">
                                 <p><label>Services :</label><?php (isset(json_decode($getCompanyvalue[$value1['services_subnames']])->tab) && $getCompanyvalue[$value1['services_subnames']] != '') ? $tab =  json_decode($getCompanyvalue[$value1['services_subnames']])->tab : $tab = '';
                                    ($tab!='') ? $tab = "On" : $tab = "Off"; echo $tab;?></p>
                                 <p><label>Reminder :</label><?php (isset(json_decode($getCompanyvalue[$value1['services_subnames']])->reminder) && $getCompanyvalue[$value1['services_subnames']] != '') ? $reminder =  json_decode($getCompanyvalue[$value1['services_subnames']])->reminder : $reminder = '';
                                    ($reminder!='') ? $reminder = "On" : $reminder = "Off"; echo $reminder;?></p>
                                 <p><label>Text :</label><?php (isset(json_decode($getCompanyvalue[$value1['services_subnames']])->text) && $getCompanyvalue[$value1['services_subnames']] != '') ? $text =  json_decode($getCompanyvalue[$value1['services_subnames']])->text : $text = '';
                                    ($text!='') ? $text = "On" : $text = "Off"; echo $text;?></p>
                                 <p><label>Invoice :</label><?php (isset(json_decode($getCompanyvalue[$value1['services_subnames']])->invoice) && $getCompanyvalue[$value1['services_subnames']] != '') ? $invoice =  json_decode($getCompanyvalue[$value1['services_subnames']])->invoice : $invoice = '';
                                    ($invoice!='') ? $invoice = "On" : $invoice = "Off"; echo $invoice;?></p>
                                 </p>
                                 <!-- end of notification --> 
                              </div>
                           </div>
                           <span class="cd-date"><?php if(isset($getusername['CreatedTime'])){ echo date('F j, Y, g:i',$getusername['CreatedTime']); } ?></span>
                           <span class="cd-details"><?php echo $getCompanyvalue['crm_company_name'];?> <?php echo $value1['service_name']; ?> Info </span>
                        </div>
                     </div>
                     <?php }}}}} ?>
                       <!-- <?php
                        if($value=="Tax Investigation" || $value=="ALL"){
                        foreach ($client_details as $getCompanykey => $getCompanyvalue) { 
                              if($getCompanyvalue['blocked_status']==0){
                        $getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
                        ?>
                     <div class="cd-timeline-block <?php echo $value; ?>">
                        <div class="cd-timeline-icon bg-primary">
                           <i class="icofont icofont-ui-file"></i>
                        </div>
                        <div class="cd-timeline-content card_main">
                           <div class="p-20">
                              <h6><a href="<?php echo base_url().'client/client_infomation/'.$getCompanyvalue['user_id']?>" target="_blank"><?php echo $getusername['crm_name'];?> - <?php echo $getCompanyvalue['crm_legal_form'];?></a></h6>
                              <div class="timeline-details">
                                 <a href="#"> <i class="icofont icofont-ui-calendar"></i><span>Due Date : <?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></span> </a>
                                 <p class="m-t-0"> Start Date: <?php echo (strtotime($getCompanyvalue['crm_investigation_start_date']) !='' ?  date('d-m-Y', strtotime($getCompanyvalue['crm_investigation_start_date'])): ''); ?></p>
                                
                                 <p class="m-t-20">
                                 <p><label>Services :</label><?php   (isset(json_decode($getCompanyvalue['taxinvest'])->tab) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->tab : $jsntaxinvest = '';
                                    ($jsntaxinvest!='') ? $taxinvest = "On" : $taxinvest = "Off"; echo $taxinvest;?></p>
                                 <p><label>Reminder :</label><?php   (isset(json_decode($getCompanyvalue['taxinvest'])->reminder) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->reminder : $jsntaxinvest = '';
                                    ($jsntaxinvest!='') ? $taxinvest = "On" : $taxinvest = "Off"; echo $taxinvest; ?></p>
                                 <p><label>Text :</label><?php   (isset(json_decode($getCompanyvalue['taxinvest'])->text) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->text : $jsntaxinvest = '';
                                    ($jsntaxinvest!='') ? $taxinvest = "On" : $taxinvest = "Off"; echo $taxinvest;?></p>
                                 <p><label>Invoice :</label><?php  echo (isset(json_decode($getCompanyvalue['taxinvest'])->invoice) && $getCompanyvalue['taxinvest'] != '') ? $jsntaxinvest =  json_decode($getCompanyvalue['taxinvest'])->invoice : $jsntaxinvest = '';
                                    ($jsntaxinvest!='') ? $taxinvest = "On" : $taxinvest = "Off"; echo $taxinvest;?></p>
                                 </p>
                              </div>
                           </div>
                           <span class="cd-date"><?php if(isset($getusername['CreatedTime'])){ echo date('F j, Y, g:i',$getusername['CreatedTime']); } ?></span>
                           <span class="cd-details"><?php echo $getCompanyvalue['crm_company_name'];?> Tax Investigation Info</span>
                        </div>
                     </div>
                     <?php }
                        }
                     }
                        ?>     -->                
                  </div>
                  <!-- for over all -->
                  <!-- for search append concept -->                
                  <div class="cd-timeline cd-container overall_for_<?php echo str_replace(' ', '', $value); ?>" id="search_for_activity" style="display: none;">
                  </div>
                  <!-- enf of search append concept -->         
               </div>
            </div>
  </div>
         </div>
         <!-- end -->
      </div>
      <?php
         $i++;
          } ?>
      <div id="inprogress" class="tab-pane fade">
      </div>
      <div id="starting" class="tab-pane fade">
      </div>
      <div id="notstarting" class="tab-pane fade">
      </div>
   </div>
                              </div>
                           </div>
                           <!-- Register your self card end -->
                        </div>
                     </div>
                  </div>
                  <!-- Page body end -->
               </div>
            </div>
            <!-- Main-body end -->
            <div id="styleSelector">
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>
<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<!-- <script type="text/javascript" src="http://remindoo.org/CRMTool/assets/js/store.min.js"></script> -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/moment/js/moment.js"></script>
<script type="text/javascript">
   $('.Note-search_timeline').on('keyup',function(){ 
       //alert('121');
       var txt=$(this).val();
       var data_id=$(this).attr('id'); 
   //alert(data_id);
   $('.overall_'+data_id).hide();
   $('.overall_for_'+data_id).html('');
   $('.overall_for_'+data_id).css('display','block');
   
         $('.'+data_id).each(function(){ 
              if($(this).text().toUpperCase().indexOf(txt.toUpperCase()) != -1)
              {
                  $(this).show();                 
                  $('.overall_for_'+data_id).append(' <div class="cd-timeline-block for_'+data_id+'">'+$(this).html()+'</div>');   
              }
              else
              {
                  $(this).hide();
                  //  $(this).wrap( "<!-- -->" );
                  // $("<!-- ").insertAfter($(this));
                  //  $(this).html('<!--'+$(this).html()+'-->');
              }
           });
   
   
   
   
   });
</script>