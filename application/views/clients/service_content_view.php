  <ul class="accordion-views">
  <!--Account  openn-->
  <li class="show-block" style="<?php echo ($tab_on['accounts']['tab']!==0 ? 'display: block' : 'display:none'); ?>">
    <a href="#" class="toggle">Accounts</a>
    <div id="accounts" class="masonry-container floating_set inner-views LISTEN_CONTENT">
      <div class="grid-sizer"></div>
      <div class="accordion-panel">
        <div class="box-division03 info-box-div">
          <div class="accordion-heading" role="tab" id="headingOne">
          <h3 class="card-title accordion-title">
            <a class="accordion-msg">Important Information</a>
          </h3>
          </div>
          <div id="collapse" class="panel-collapse">
            <div class="basic-info-client1 CONTENT_IMPORTANT_INFORMATION" id="accounts_box">
              <div class="form-group row name_fields accounts_sec" data-id="<?php echo $Setting_Order['crm_companies_house_authorisation_code']; ?>"
                <?php echo $Setting_Delete['crm_companies_house_authorisation_code']; ?>>
                <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_companies_house_authorisation_code']; ?></label>
                <div class="col-sm-8">
                   <input type="text" name="auth_code" id="accounts_auth_code" placeholder="" value="<?php if(isset($client['crm_companies_house_authorisation_code']) && ($client['crm_companies_house_authorisation_code']!='') ){ echo $client['crm_companies_house_authorisation_code'];}?>" class="fields accounts_auth_code clr-check-client" ><!-- remove readonly data -->
                </div>
              </div>
              <div class="form-group row name_fields accounts_sec" data-id="<?php echo $Setting_Order['crm_accounts_utr_number']; ?>" <?php echo $Setting_Delete['crm_accounts_utr_number']; ?>>
                <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_accounts_utr_number']; ?></label>
                <div class="col-sm-8">
                   <input type="number" name="accounts_utr_number" id="accounts_utr_number" placeholder="" value="<?php if(isset($client['crm_accounts_utr_number']) && ($client['crm_accounts_utr_number']!='') ){ echo $client['crm_accounts_utr_number'];}?>" class="fields clr-check-select">
                </div>
              </div>
              <div class="form-group row radio_bts accounts_sec" data-id="<?php echo $Setting_Order['crm_companies_house_email_remainder']; ?>" <?php echo $Setting_Delete['crm_companies_house_email_remainder']; ?>>
                <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_companies_house_email_remainder']; ?></label>
                <div class="col-sm-8" id="che_ck">
                   <input type="checkbox" class="js-small f-right fields clr-check-client" name="company_house_reminder" id="company_house_reminder" <?php if(isset($client['crm_companies_house_email_remainder']) && ($client['crm_companies_house_email_remainder']=='on') ){?> checked="checked"<?php } ?> value="on">
                   <!--  <button id="checked">Check</button> -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- accordion-panel -->
      <div class="accordion-panel accounts_tab_section" >
        <div class="box-division03 info-box-div">
          <div class="accordion-heading" role="tab" id="headingOne">
          <h3 class="card-title accordion-title">
          <a class="accordion-msg">Accounts</a>
          </h3>
          </div>
          <div id="collapse" class="panel-collapse ">
            <div class="basic-info-client1 CONTENT_ACCOUNTS" id="accounts_box1">

              <div class="form-group row name_fields date_birth accounts_sec" data-id="<?php echo $Setting_Order['crm_hmrc_yearend']; ?>">
                <label class="col-sm-4 col-form-label">
                  <?php echo $Setting_Label['crm_hmrc_yearend']; ?>
                  <span class="Hilight_Required_Feilds">*</span>
                </label>
                  
                <div class="col-sm-8 edit-field-popup1">
                   <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>  <input type="text" name="accounts_next_made_up_to" id="accounts_next_made_up_to" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_hmrc_yearend']) && ($client['crm_hmrc_yearend']!='') ){ echo Change_Date_Format( $client['crm_hmrc_yearend'] );}?>" class="fields edit_classname datepicker LISTEN_CHANGES clr-check-select" >
                   <?php if(!empty($client) && $client['status']==1){ ?>
                   <div class="edit-button-confim">
                      <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                   </div>
                   <?php } ?>
                </div>
              </div>
              <div class="form-group row name_fields date_birth accounts_sec" data-id="<?php echo $Setting_Order['crm_accounts_last_made_up_to_date']; ?>" <?php echo $Setting_Delete['crm_accounts_last_made_up_to_date']; ?>>
               <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_accounts_last_made_up_to_date']; ?></label>
               <div class="col-sm-8 edit-field-popup1">
                  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span><input type="text" name="accounts_last_made_up_to" id="accounts_last_made_up_to" placeholder="dd-mm-yyyy" value="<?php if(!empty($client['crm_accounts_last_made_up_to_date'])){ echo Change_Date_Format( $client['crm_accounts_last_made_up_to_date'] ); }  ?>" class="fields edit_classname datepicker clr-check-select" >
                  <!-- <div class="edit-button-confim" style="<?php ?>">
                     <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                  </div> -->
               </div>
              </div>
              <div class="form-group row name_fields date_birth accounts_sec" data-id="<?php echo $Setting_Order['crm_ch_accounts_next_due']; ?>" <?php echo $Setting_Delete['crm_ch_accounts_next_due']; ?>>
                <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_ch_accounts_next_due']; ?></label>
                <div class="col-sm-8 edit-field-popup1">
                   <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>   <input type="text" name="accounts_next_due" id="accounts_next_due" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_ch_accounts_next_due']) && ($client['crm_ch_accounts_next_due']!='') ){ echo date("d-m-Y", strtotime($client['crm_ch_accounts_next_due']));}?>" class="fields edit_classname datepicker clr-check-select" >
                   <?php if(!empty($client) && $client['status']==1){ ?>
                   <div class="edit-button-confim">
                      <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                   </div>
                   <?php } ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <?php 
      $accounts_reminder = ($tab_on['accounts']['reminder']!==0?"display:block":"display:none");
      ?>
      <!-- accordion-panel -->
      <div class="accordion-panel enable_acc" style="<?php echo $accounts_reminder?>">
        <div class="box-division03 info-box-div">
          <div class="accordion-heading" role="tab" id="headingOne">
          <h3 class="card-title accordion-title">
          <a class="accordion-msg">Accounts Reminders</a>
          </h3>
          </div>
          <div id="collapse" class="panel-collapse">
            <div class="basic-info-client1 CONTENT_ACCOUNTS_REMINDERS" id="accounts_box2">
              <div class="form-group row name_fields radio_bts accounts_sec" data-id="<?php echo $Setting_Order['crm_accounts_next_reminder_date']; ?>"
                <?php echo $Setting_Delete['crm_accounts_next_reminder_date']; ?>>
                <label class="col-sm-4 col-form-label">
                  <a id="accounts_custom_reminder_link" name="accounts_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/2" target="_blank">
                    <?php echo $Setting_Label['crm_accounts_next_reminder_date']; ?>
                  </a>
                </label>
                <div class="col-sm-8">
                   <!-- <input type="hidden" name="accounts_next_reminder_date" id="accounts_next_reminder_date" placeholder="" value="<?php if(isset($client['crm_accounts_next_reminder_date']) && ($client['crm_accounts_next_reminder_date']!='') ){ echo $client['crm_accounts_next_reminder_date'];}?>" class="fields datepicker"> -->

                   <input type="checkbox" class="js-small f-right fields as_per_firm LISTEN_CHANGES clr-check-client" name="accounts_next_reminder_date" id="accounts_next_reminder_date"  <?php if(isset($client['crm_accounts_next_reminder_date']) && ($client['crm_accounts_next_reminder_date']!='') ){ echo "checked"; } ?> >
                </div>
              </div>
              
              <?php $acc_cus_reminder = (!empty($client['crm_accounts_custom_reminder'])?$client['crm_accounts_custom_reminder']:'');?>
              <div class="form-group row radio_bts accounts_custom_reminder_label accounts_sec" data-id="<?php echo $Setting_Order['crm_accounts_custom_reminder']; ?>" <?php echo $Setting_Delete['crm_accounts_custom_reminder']; ?>>
                <label id="accounts_custom_reminder_label" name="accounts_custom_reminder_label" class="col-sm-4 col-form-label">
                  <?php echo $Setting_Label['crm_accounts_custom_reminder']; ?>                    
                </label>
                <div class="col-sm-8 " id="accounts_custom_reminder">
                   <input type="checkbox" class="js-small f-right fields cus_reminder LISTEN_CHANGES clr-check-client" name="accounts_custom_reminder" id="" data-id="accounts_cus" data-serid="2" value="<?php echo $acc_cus_reminder;?>" <?php if($acc_cus_reminder!=''){ ?>  checked <?php } ?> >

                   <!-- <textarea rows="4" name="accounts_custom_reminder" id="accounts_custom_reminder" class="form-control fields"><?php if(isset($client['crm_accounts_custom_reminder']) && ($client['crm_accounts_custom_reminder']!='') ){ echo $client['crm_accounts_custom_reminder'];}?></textarea> -->
                   <!-- <a name="accounts_custom_reminder" id="accounts_custom_reminder" href="<?php echo base_url();?>user/Service_reminder_settings/3" target="_blank">Click to add Custom Reminder</a> -->
                </div>
              </div>
            <?php 
              if(!empty($client))
              {
                $cus_temp = $this->db->query('select * from custom_service_reminder where client_id='.$client['id'].' and firm_id ='.$_SESSION['firm_id'].' and service_id=2')->result_array();
                foreach ($cus_temp as $key => $value)
                {
                
               ?>
            <div class="form-group row name_fields reminder_list" style="">
              <label class="col-sm-4 col-form-label cus_reminder_subject_<?php echo $value['id']; ?>"><?php echo $value['subject']; ?></label>
              <div class="col-sm-8">
                 <a href="javascript:;" class="date-editop" data-toggle="modal" id="cus_reminder_<?php echo $value['id']; ?>">Edit</a>
              </div>
            </div>
            <?php 
            } 
            } ?>
            </div>
          </div>
        </div>
      </div>
      <input type="hidden" class="UPDATE_LISTENED" name="is_service_data_edited[2]" value="0">
    </div>
    </li>
<!-- accounts close-->
  
<!-- bookkeeping open -->
    <li class="show-block" style="<?php echo ($tab_on['bookkeep']['tab']!==0  ? "display: block" :"display:none;"); ?>">
      <a href="#" class="toggle">Bookkeeping</a>
      <div id="bookkeeptab" class="masonry-container floating_set inner-views LISTEN_CONTENT">
        <div class="grid-sizer"></div>
      <div class="accordion-panel">
         <div class="box-division03 info-box-div">
            <div class="accordion-heading" role="tab" id="headingOne">
               <h3 class="card-title accordion-title">
                  <a class="accordion-msg">Bookkeeping</a>
               </h3>
            </div>
            <div id="collapse" class="panel-collapse">
               <div class="basic-info-client1 CONTENT_BOOKKEEPING" id="bookkeep_box1">
                  <div class="form-group row bookkeep_sort" data-id="<?php echo $Setting_Order['crm_bookkeeping']; ?>" <?php echo $Setting_Delete['crm_bookkeeping']; ?>>
                     <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_bookkeeping']; ?>
                       <span class="Hilight_Required_Feilds">*</span>
                     </label>
                     <div class="col-sm-8">
                        <select name="bookkeeping" id="bookkeeping" class="form-control fields required LISTEN_CHANGES clr-check-select">
                           <option value="">--Select--</option>
                           <option value="Weekly" <?php if(isset($client['crm_bookkeeping']) && $client['crm_bookkeeping']=='Weekly') {?> selected="selected"<?php } ?>>Weekly</option>
                           <option value="Monthly" <?php if(isset($client['crm_bookkeeping']) && $client['crm_bookkeeping']=='Monthly') {?> selected="selected"<?php } ?>>Monthly</option>
                           <option value="Quarterly" <?php if(isset($client['crm_bookkeeping']) && $client['crm_bookkeeping']=='Quarterly') {?> selected="selected"<?php } ?>>Quarterly</option>
                           <option value="Annually" <?php if(isset($client['crm_bookkeeping']) && $client['crm_bookkeeping']=='Annually') {?> selected="selected"<?php } ?>>Annually</option>
                        </select>
                     </div>
                  </div>
                  <div class="form-group row help_icon date_birth bookkeep_sort" data-id="<?php echo $Setting_Order['crm_next_booking_date']; ?>">
                       <label class="col-sm-4 col-form-label">
                        <?php echo $Setting_Label['crm_next_booking_date']; ?>
                          
                          <span class="Hilight_Required_Feilds">*</span>
                        </label>
                     <div class="col-sm-8">
                        <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                        <input class="form-control clr-check-select datepicker-13 fields LISTEN_CHANGES" name="next_booking_date" id="datepicker26" type="text" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_next_booking_date']) && ($client['crm_next_booking_date']!='') ){ echo Change_Date_Format($client['crm_next_booking_date']);}?>"/>
                     </div>
                  </div>
                  <div class="form-group row bookkeep_sort" data-id="<?php echo $Setting_Order['crm_method_bookkeeping']; ?>" <?php echo $Setting_Delete['crm_method_bookkeeping']; ?>>
                     <label class="col-sm-4 col-form-label">
                      <?php echo $Setting_Label['crm_method_bookkeeping']; ?>                        
                      </label>
                     <div class="col-sm-8">
                        <select name="method_bookkeeping" id="method_bookkeeping" class="form-control fields clr-check-select">
                           <option value="">--Select--</option>
                           <option value="Excel" <?php if(isset($client['crm_method_bookkeeping']) && $client['crm_method_bookkeeping']=='Excel') {?> selected="selected"<?php } ?>>Excel</option>
                           <option value="Software" <?php if(isset($client['crm_method_bookkeeping']) && $client['crm_method_bookkeeping']=='Software') {?> selected="selected"<?php } ?>>Software</option>
                        </select>
                     </div>
                  </div>
                  <div class="form-group row bookkeep_sort" data-id="<?php echo $Setting_Order['crm_client_provide_record']; ?>" <?php echo $Setting_Delete['crm_client_provide_record']; ?>>
                     <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_client_provide_record']; ?>  </label>
                     <div class="col-sm-8">
                        <select name="client_provide_record" id="client_provide_record" class="form-control fields clr-check-select">
                           <option value="">--Select--</option>
                           <option value="Email/DropBox" <?php if(isset($client['crm_client_provide_record']) && $client['crm_client_provide_record']=='Email/DropBox') {?> selected="selected"<?php } ?>>Email/DropBox</option>
                           <option value="Google/Online Portal" <?php if(isset($client['crm_client_provide_record']) && $client['crm_client_provide_record']=='Google/Online Portal') {?> selected="selected"<?php } ?>>Google/Online Portal</option>
                        </select>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- accordion-panel -->

      <div class="accordion-panel enable_book" style="<?php echo ( $tab_on['bookkeep']['reminder']!==0  ? "display: block" :"display:none;");?>">
         <div class="box-division03 info-box-div">
            <div class="accordion-heading" role="tab" id="headingOne">
               <h3 class="card-title accordion-title">
                  <a class="accordion-msg">Bookkeep Reminders</a>
               </h3>
            </div>
            <div id="collapse" class="panel-collapse">
               <div class="basic-info-client1 CONTENT_BOOKKEEPING_REMINDERS" id="bookkeep_box2">
                  <div class="form-group row  date_birth radio_bts bookkeep_sort" data-id="<?php echo $Setting_Order['crm_bookkeep_next_reminder_date']; ?>" <?php echo $Setting_Delete['crm_bookkeep_next_reminder_date']; ?>>
                     <label class="col-sm-4 col-form-label"><a id="bookkeep_add_custom_reminder_link" name="bookkeep_add_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/10" target="_blank"><?php echo $Setting_Label['crm_bookkeep_next_reminder_date']; ?> </a></label>
                     <div class="col-sm-8">
                        <!-- <input type="hidden" name="bookkeep_next_reminder_date" id="bookkeep_next_reminder_date" placeholder="" value="<?php if(isset($client['crm_bookkeep_next_reminder_date']) && ($client['crm_bookkeep_next_reminder_date']!='') ){ echo $client['crm_bookkeep_next_reminder_date'];}?>" class="fields datepicker"> -->
                        <input type="checkbox" class="js-small f-right fields as_per_firm LISTEN_CHANGES clr-check-client" name="bookkeep_next_reminder_date" id="bookkeep_next_reminder_date"  <?php if(isset($client['crm_bookkeep_next_reminder_date']) && ($client['crm_bookkeep_next_reminder_date']!='') ){ echo "checked"; } ?> >
                        <!---iiiii-->
                     </div>
                  </div>
                  <!-- <div class="form-group row radio_bts management" data-id="<?php echo $Setting_Order['crm_manage_create_task_reminder']; ?>">
                     <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                     <div class="col-sm-8">
                        <input type="checkbox" class="js-small f-right fields" name="bookkeep_create_task_reminder" id="bookkeep_create_task_reminder" <?php if(isset($client['crm_bookkeep_create_task_reminder']) && ($client['crm_bookkeep_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> value="on" >
                     </div>
                  </div> -->
                  <?php $bookkeep_cus_reminder  = (!empty($client['crm_bookkeep_add_custom_reminder'])?$client['crm_bookkeep_add_custom_reminder']:'');?>
                  <div class="form-group row radio_bts bookkeep_add_custom_reminder_label bookkeep_sort" data-id="<?php echo $Setting_Order['crm_bookkeep_add_custom_reminder']; ?>"  <?php echo $Setting_Delete['crm_bookkeep_add_custom_reminder']; ?>>
                     <label id="bookkeep_add_custom_reminder_label" name="bookkeep_add_custom_reminder_label" class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_bookkeep_add_custom_reminder']; ?></label>
                     <div class="col-sm-8" id="bookkeep_add_custom_reminder">
                        <input type="checkbox" class="js-small f-right fields cus_reminder LISTEN_CHANGES clr-check-client" name="bookkeep_add_custom_reminder" id="" data-id="bookkeep_cus" data-serid="10" value="<?php echo $bookkeep_cus_reminder;?>" <?php if($bookkeep_cus_reminder!=''){ ?> checked <?php } ?>>
                        <!-- <textarea rows="3" placeholder="" name="bookkeep_add_custom_reminder" id="bookkeep_add_custom_reminder" class="fields"><?php if(isset($client['crm_bookkeep_add_custom_reminder']) && ($client['crm_bookkeep_add_custom_reminder']!='') ){ echo $client['crm_bookkeep_add_custom_reminder'];}?></textarea> -->
                        <!-- <a name="bookkeep_add_custom_reminder" id="bookkeep_add_custom_reminder" href="<?php echo base_url();?>user/Service_reminder_settings/13" target="_blank">Click to add Custom Reminder</a> -->
                     </div>
                  </div>
                  <?php 
                     if(!empty($client))
                     {
                      $cus_temp = $this->db->query('select id,subject from  custom_service_reminder where client_id='.$client['id'].' and firm_id='.$_SESSION['firm_id'].' and service_id=10')->result_array();
                       foreach ($cus_temp as $key => $value)
                       {
                      ?>
                  <div class="form-group row name_fields" style="">
                     <label class="col-sm-4 col-form-label cus_reminder_subject_<?php echo $value['id']; ?>"><?php echo $value['subject']; ?></label>
                     <div class="col-sm-8">
                        <a href="javascript:;" class="date-editop" data-toggle="modal" id="cus_reminder_<?php echo $value['id']; ?>">Edit</a>
                     </div>
                  </div>
                  <?php } 
                    } ?>
               </div>
            </div>
         </div>
      </div>
      <!-- accordion-panel -->
      <input type="hidden" class="UPDATE_LISTENED" name="is_service_data_edited[10]" value="0">
   </div>
    </li>
<!-- bookkeeping close -->
                              
  <!-- confirm tab start-->
  <li class="show-block" style="<?php echo ($tab_on['conf_statement']['tab']!==0 ? 'display: block' : 'display:none');?>">
     <a href="#" class="toggle">  Confirmation Statement</a>
      <div id="confirmation-statement" class="masonry-container floating_set inner-views LISTEN_CONTENT">
                     <div class="grid-sizer"></div>
                     <div class="accordion-panel">
                        <div class="box-division03 info-box-div">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">Important Information</a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1 CONTENT_IMPORTANT_INFORMATION" id="box">
                                 <div class="form-group row name_fields sorting_conf" data-id="<?php echo $Setting_Order['crm_companies_house_authorisation_code']; ?>" <?php echo $Setting_Delete['crm_companies_house_authorisation_code']; ?>>
                                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_companies_house_authorisation_code']; ?></label>
                                    <div class="col-sm-8">
                                       <input type="text" name="auth_code" id="confirmation_auth_code" placeholder="" value="<?php if(isset($client['crm_companies_house_authorisation_code']) && ($client['crm_companies_house_authorisation_code']!='') ){ echo $client['crm_companies_house_authorisation_code'];}?>" class="fields confirmation_auth_code clr-check-client" > <!-- readonly -->
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- accordion-panel -->
                     <div class="accordion-panel">
                        <div class="box-division03 info-box-div">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">Confirmation Statement</a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1 CONTENT_CONFIRMATION_STATEMENT" id="box1">      
                                 <div class="form-group row name_fields date_birth sorting_conf" data-id="<?php echo $Setting_Order['crm_confirmation_statement_date']; ?>">
                                    <label class="col-sm-4 col-form-label">
                                    <?php echo $Setting_Label['crm_confirmation_statement_date']; ?>
                                    <span class="Hilight_Required_Feilds">*</span>
                                  </label>
                                    <div class="col-sm-8 edit-field-popup1">
                                       <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span><input type="text" name="confirmation_next_made_up_to" id="confirmation_next_made_up_to" placeholder="dd-mm-yyyy" value="<?php 
                                          if(isset($client['crm_confirmation_statement_date']) && ($client['crm_confirmation_statement_date']!='') ){ echo Change_Date_Format( $client['crm_confirmation_statement_date'] );}?>" class="fields edit_classname datepicker LISTEN_CHANGES clr-check-select" >
                                       <?php if(!empty($client) && $client['status']==1){ ?>
                                       <div class="edit-button-confim">
                                          <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                       </div>
                                       <?php } ?>
                                    </div>
                                 </div>
                                 <div class="form-group row name_fields date_birth sorting_conf" data-id="<?php echo $Setting_Order['crm_confirmation_statement_due_date']; ?>" <?php echo $Setting_Delete['crm_confirmation_statement_due_date']; ?>>
                                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_confirmation_statement_due_date']; ?></label>
                                    <div class="col-sm-8 edit-field-popup1">
                                       <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>  <input type="text" name="confirmation_next_due" id="confirmation_next_due" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_confirmation_statement_due_date']) && ($client['crm_confirmation_statement_due_date']!='') ){ echo Change_Date_Format($client['crm_confirmation_statement_due_date']);}?>" class="fields edit_classname datepicker clr-check-select" >
                                       <?php if(!empty($client) && $client['status']==1){ ?>
                                       <div class="edit-button-confim">
                                          <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                       </div>
                                       <?php } ?>
                                    </div>
                                 </div>
                                  <div class="form-group row name_fields date_birth sorting_conf" data-id="<?php echo $Setting_Order['crm_confirmation_statement_last_made_up_to_date']; ?>" <?php echo $Setting_Delete['crm_confirmation_statement_last_made_up_to_date']; ?>>
                             <label class="col-sm-4 col-form-label"> 
                              <?php echo $Setting_Label['crm_confirmation_statement_last_made_up_to_date']; ?>
                              </label>
                             <div class="col-sm-8 edit-field-popup1">
                                <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                <input type="text" name="statement_last_made_up_to_date" id="statement_last_made_up_to_date" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_confirmation_statement_last_made_up_to_date'])){
                                 echo Change_Date_Format( $client['crm_confirmation_statement_last_made_up_to_date']); }  ?>" class="fields edit_classname datepicker clr-check-select" >
                                <!-- <div class="edit-button-confim" style="<?php ?>">
                                   <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                </div> -->
                             </div>
                          </div>
                              </div>
                           </div>
                        </div>
                     </div>
                    
                     <!--- Conformation Reminder Section -->
                     <div class="accordion-panel enable_conf" style="<?php echo ($tab_on['conf_statement']['reminder']!==0 ?"":'display:none');?>">
                        <div class="box-division03 info-box-div">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg">Confirmation Reminders</a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1 CONTENT_CONFIRMATION_REMINDERS" id="box2">
                                 <div class="form-group row name_fields date_birth radio_bts sorting_conf" data-id="<?php echo $Setting_Order['crm_confirmation_email_remainder']; ?>" <?php echo $Setting_Delete['crm_confirmation_email_remainder']; ?>>
                                    <label class="col-sm-4 col-form-label"><a name="add_custom_reminder_link" id="add_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/1" target="_blank"><?php echo $Setting_Label['crm_confirmation_email_remainder']; ?></a></label>
                                    <div class="col-sm-8">
                                       <input type="hidden" name="confirmation_next_reminder_test" id="confirmation_next_reminder_test" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_confirmation_email_remainder']) && ($client['crm_confirmation_email_remainder']!='') ){ echo $client['crm_confirmation_email_remainder'];}?>" class="fields datepicker-13 clr-check-select">
                                      <!--  <input type="checkbox" class="js-small f-right fields" name="confirmation_next_reminder_date" id="confirmation_next_reminder_date"  checked="checked" onchange="check_checkbox();"> -->
                                          <input type="checkbox" class="js-small f-right fields as_per_firm LISTEN_CHANGES clr-check-client" name="confirmation_next_reminder" id="confirmation_next_reminder_date" <?php if(isset($client['crm_confirmation_email_remainder']) && ($client['crm_confirmation_email_remainder']!='') ){ echo "checked"; } ?>>
                                    </div>
                                 </div>
                                <!--  <div class="form-group row radio_bts sorting_conf" data-id="<?php echo $Setting_Order['crm_confirmation_create_task_reminder']; ?>">
                                    <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                                    <div class="col-sm-8">
                                       <input type="checkbox" class="js-small f-right fields" name="create_task_reminder" id="create_task_reminder" <?php if(isset($client['crm_confirmation_create_task_reminder']) && ($client['crm_confirmation_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> value='on' >
                                    </div>
                                 </div> -->
                                 <?php
                                  $cs_cus_reminder = (!empty($client['crm_confirmation_add_custom_reminder'])?$client['crm_confirmation_add_custom_reminder']:'');
                                 ?>
                                 <div class="form-group row radio_bts custom_remain add_custom_reminder_label sorting_conf" data-id="<?php echo $Setting_Order['crm_confirmation_add_custom_reminder']; ?>" <?php echo $Setting_Delete['crm_confirmation_add_custom_reminder'];?> >
                                    <label class="col-sm-4 col-form-label" id="add_custom_reminder_label" name="add_custom_reminder_label"><?php echo $Setting_Label['crm_confirmation_add_custom_reminder'];?> </label>
                                    <div class="col-sm-8 " id="add_custom_reminder">

                                       <input type="checkbox" class="js-small f-right fields cus_reminder LISTEN_CHANGES clr-check-client" 
                                        name="add_custom_reminder" id="" data-id="confirm_cus" data-serid="1"
                                       value="<?php echo $cs_cus_reminder;?>" <?php if($cs_cus_reminder!=''){ ?>checked <?php } ?>>
                                       <!-- <textarea rows="3" placeholder="" name="add_custom_reminder" id="add_custom_reminder" class="fields"><?php if(isset($client['crm_confirmation_add_custom_reminder']) && ($client['crm_confirmation_add_custom_reminder']!='') ){ echo $client['crm_confirmation_add_custom_reminder'];}?></textarea> -->
                                       <!--  <a name="add_custom_reminder" id="add_custom_reminder" href="<?php echo base_url();?>user/Service_reminder_settings/4" target="_blank">Click to add Custom Reminder</a> -->
                                    </div>
                                 </div>

                                 <?php 

                                    if(!empty($client))
                                    {
                                      $cus_temp = $this->db->query('select id,subject from  custom_service_reminder where client_id='.$client['id'].' and firm_id='.$_SESSION['firm_id'].' and service_id=1 ')->result_array();
                                  foreach ($cus_temp as $key => $value)
                                  {
                                  ?>
                                 <div class="form-group row name_fields" style="">
                                    <label class="col-sm-4 col-form-label cus_reminder_subject_<?php echo $value['id']; ?>"><?php echo $value['subject']; ?></label>
                                    <div class="col-sm-8">
                                       <a href="javascript:;" class="date-editop" data-toggle="modal" id="cus_reminder_<?php echo $value['id']; ?>">Edit</a>
                                    </div>
                                 </div>
                                 <?php
                                  }
                                  } ?>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- accordion-panel -->
                     <!-- invoice confirmation-->
                     
                     <!-- end invoice confirmation-->
                     <div class="accordion-panel">
                        <div class="box-division03 info-box-div">
                           <div class="accordion-heading" role="tab" id="headingOne">
                              <h3 class="card-title accordion-title">
                                 <a class="accordion-msg"></a>
                              </h3>
                           </div>
                           <div id="collapse" class="panel-collapse">
                              <div class="basic-info-client1 CONTENT_OTHERS" id="box3">
                                 <div class="form-group row sorting_conf" data-id="<?php echo $Setting_Order['crm_confirmation_officers']; ?>" <?php echo $Setting_Delete['crm_confirmation_officers']; ?>>
                                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_confirmation_officers']; ?></label>
                                    <div class="col-sm-8">
                                       <textarea rows="3" placeholder="" name="confirmation_officers" id="confirmation_officers" class="fields clr-check-client"><?php if(isset($client['crm_confirmation_officers']) && ($client['crm_confirmation_officers']!='') ){ echo $client['crm_confirmation_officers'];}?></textarea>
                                    </div>
                                 </div>
                                 <div class="form-group row sorting_conf" data-id="<?php echo $Setting_Order['crm_share_capital']; ?>" <?php echo $Setting_Delete['crm_share_capital']; ?>>
                                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_share_capital']; ?></label>
                                    <div class="col-sm-8">
                                       <textarea rows="3" placeholder="" name="share_capital" id="share_capital" class="fields clr-check-client"><?php if(isset($client['crm_share_capital']) && ($client['crm_share_capital']!='') ){ echo $client['crm_share_capital'];}?></textarea>
                                    </div>
                                 </div>
                                 <div class="form-group row sorting_conf" data-id="<?php echo $Setting_Order['crm_shareholders']; ?>" <?php echo $Setting_Delete['crm_shareholders']; ?>>
                                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_shareholders']; ?></label>
                                    <div class="col-sm-8">
                                       <textarea rows="3" placeholder="" name="shareholders" id="shareholders" class="fields clr-check-client"><?php if(isset($client['crm_shareholders']) && ($client['crm_shareholders']!='') ){ echo $client['crm_shareholders'];}?></textarea>
                                    </div>
                                 </div>
                                 <div class="form-group row sorting_conf" data-id="<?php echo $Setting_Order['crm_people_with_significant_control']; ?>" <?php echo $Setting_Delete['crm_people_with_significant_control']; ?>>
                                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_people_with_significant_control']; ?></label>
                                    <div class="col-sm-8">
                                       <textarea rows="3" placeholder="" name="people_with_significant_control" id="people_with_significant_control" class="fields clr-check-client"><?php if(isset($client['crm_people_with_significant_control']) && ($client['crm_people_with_significant_control']!='') ){ echo $client['crm_people_with_significant_control'];}?></textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                    <input type="hidden" class="UPDATE_LISTENED" name="is_service_data_edited[1]" value="0">
                  </div>
     </li>
  <!-- confirm statement close -->

  <!-- company tax return open-->

    <li class="show-block" style="<?php echo ( $tab_on['company_tax_return']['tab']!==0  ? "display: block" :"display:none;");?>">
    <a href="#" class="toggle">Company Tax Return</a>
   <!-- end accounts invoice -->
   <div id="companytax" class="masonry-container floating_set inner-views LISTEN_CONTENT">
   <div class="grid-sizer"></div>
      <div class="accordion-panel">
         <div class="box-division03 info-box-div">
            <div class="accordion-heading" role="tab" id="headingOne">
               <h3 class="card-title accordion-title">
                  <a class="accordion-msg">Company Tax Return </a>
               </h3>
            </div>
            <div id="collapse" class="panel-collapse">
               <div class="basic-info-client1 CONTENT_COMPANY_TAX_RETURN" id="companytax_box">
                  <div class="form-group row name_fields date_birth company_tax_return_sec" data-id="<?php echo $Setting_Order['crm_accounts_due_date_hmrc']; ?>">
                     <label class="col-sm-4 col-form-label">
                      <?php echo $Setting_Label['crm_accounts_due_date_hmrc']; ?>
                     <span class="Hilight_Required_Feilds">*</span>
                      </label>
                     <div class="col-sm-8">
                        <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>  <input type="text" name="accounts_due_date_hmrc" id="accounts_due_date_hmrc" placeholder="dd-mm-yyyy" value="<?php if(!empty($client['crm_accounts_due_date_hmrc'])){ echo date("d-m-Y", strtotime($client['crm_accounts_due_date_hmrc'])); } 
                           ?>" class="fields datepicker-13 LISTEN_CHANGES clr-check-select">
                     </div>
                  </div>
                  <div class="form-group row name_fields date_birth company_tax_return_sec" data-id="<?php echo $Setting_Order['crm_accounts_tax_date_hmrc']; ?>" <?php echo $Setting_Delete['crm_accounts_tax_date_hmrc']; ?>>
                     <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_accounts_tax_date_hmrc']; ?></label>
                     <div class="col-sm-8">
                        <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span> <input type="text" name="accounts_tax_date_hmrc" id="accounts_tax_date_hmrc" placeholder="dd-mm-yyyy" value="<?php if(!empty($client['crm_accounts_tax_date_hmrc'])){ echo Change_Date_Format( $client['crm_accounts_tax_date_hmrc'] ); } ?>" class="fields datepicker-13 clr-check-select">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
      <!-- accordion-panel -->
      <div class="accordion-panel enable_tax" style="<?php echo ( $tab_on['company_tax_return']['reminder']!==0 ? "display: block" :"display:none;");?>">
         <div class="box-division03 info-box-div">
            <div class="accordion-heading" role="tab" id="headingOne">
               <h3 class="card-title accordion-title">
                  <a class="accordion-msg">Company Tax Reminders</a>
               </h3>
            </div>
            <div id="collapse" class="panel-collapse">
               <div class="basic-info-client1 CONTENT_COMPANY_TAX_REMINDER" id="companytax_box1">
                  <div class="form-group row radio_bts name_fields company_tax_return_sec" data-id="<?php echo $Setting_Order['crm_company_next_reminder_date']; ?>" <?php echo $Setting_Delete['crm_company_next_reminder_date']; ?>>
                     <label class="col-sm-4 col-form-label"><a id="company_custom_reminder_link" name="company_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/3" target="_blank">
                     <?php echo $Setting_Label['crm_company_next_reminder_date']; ?>
                   </a></label>
                     <div class="col-sm-8">
                        <!-- <input type="hidden" name="company_next_reminder_date" id="company_next_reminder_date" placeholder="" value="<?php if(isset($client['crm_company_next_reminder_date']) && ($client['crm_company_next_reminder_date']!='') ){ echo $client['crm_company_next_reminder_date'];}?>" class="fields datepicker"> -->

                        <input type="checkbox" class="js-small f-right fields as_per_firm LISTEN_CHANGES clr-check-client" name="company_next_reminder_date" id="company_next_reminder_date"   <?php if(isset($client['crm_company_next_reminder_date']) && ($client['crm_company_next_reminder_date']!='') ){ echo "checked"; } ?> >
                     </div>
                  </div>
                 <!--  <div class="form-group row radio_bts accounts_sec" data-id="<?php echo $Setting_Order['crm_company_create_task_reminder']; ?>">
                     <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                     <div class="col-sm-8">
                        <input type="checkbox" class="js-small f-right fields" name="company_create_task_reminder" id="company_create_task_reminder" <?php if(isset($client['crm_company_create_task_reminder']) && ($client['crm_company_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> value="on" >
                     </div>
                  </div> -->
                  <?php $com_ret_cus_reminder = (!empty($client['crm_company_custom_reminder'])?$client['crm_company_custom_reminder']:''); ?>

                  <div class="form-group row radio_bts company_tax_return_sec company_custom_reminder_label" data-id="<?php echo $Setting_Order['crm_company_custom_reminder']; ?>" <?php echo $Setting_Delete['crm_company_custom_reminder']; ?>>
                     <label id="company_custom_reminder_label" name="company_custom_reminder_label" class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_company_custom_reminder']; ?></label>
                     <div class="col-sm-8" id="company_custom_reminder">
                        <input type="checkbox" class="js-small f-right fields cus_reminder LISTEN_CHANGES clr-check-client" name="company_custom_reminder" id="" data-id="company_cus" data-serid="3" value="<?php echo $com_ret_cus_reminder; ?>" <?php if($com_ret_cus_reminder!=''){ ?> checked <?php } ?> >

                        <!-- <textarea rows="4" name="company_custom_reminder" id="company_custom_reminder" class="form-control fields"><?php if(isset($client['crm_company_custom_reminder']) && ($client['crm_company_custom_reminder']!='') ){ echo $client['crm_company_custom_reminder'];}?></textarea> -->
                        <!-- <a name="company_custom_reminder" id="company_custom_reminder" href="<?php echo base_url();?>user/Service_reminder_settings/5" target="_blank">Click to add Custom Reminder</a> -->
                     </div>
                  </div>
                  <?php 
                     if(!empty($client))
                     {
                      $cus_temp = $this->db->query('select id,subject from  custom_service_reminder where client_id='.$client['id'].' and firm_id='.$_SESSION['firm_id'].' and service_id=3')->result_array();
                       foreach ($cus_temp as $key => $value) {
                       
                      ?>
                  <div class="form-group row name_fields" style="">
                     <label class="col-sm-4 col-form-label cus_reminder_subject_<?php echo $value['id']; ?>"><?php echo $value['subject']; ?></label>
                     <div class="col-sm-8">
                        <a href="javascript:;" class="date-editop" data-toggle="modal" id="cus_reminder_<?php echo $value['id']; ?>">Edit</a>
                     </div>
                  </div>
                  <?php } } ?>
               </div>
            </div>
         </div>
      </div>
      <input type="hidden" class="UPDATE_LISTENED" name="is_service_data_edited[3]" value="0">
   </div>
  </li>
  <!-- company tax return Close-->
 

  <!-- CIS/CIS SUB OPEN -->
  <li class="show-block" style="<?php echo ( $tab_on['cis']['tab']!==0   ? "display: block" :"display:none;");?>">
  <a href="#" class="toggle">CIS Contractor</a>
  <div id="contratab" class="masonry-container floating_set inner-views LISTEN_CONTENT">
   <div class="grid-sizer"></div>
    <div class="accordion-panel">
       <div class="box-division03 info-box-div">
          <div class="accordion-heading" role="tab" id="headingOne">
             <h3 class="card-title accordion-title">
                <a class="accordion-msg"> CIS Contractor</a>
             </h3>
          </div>
          <div id="collapse" class="panel-collapse">
             <div class="basic-info-client1 CONTENT_CIS_CONTRACTOR" id="contratab_box">
              <div class="form-group row radio_bts name_fields contratab_sort" data-id="<?php echo $Setting_Order['crm_cis_contractor']; ?>"
                <?php echo $Setting_Delete['crm_cis_contractor']; ?> >
                   <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_cis_contractor']; ?></label>
                   <div class="col-sm-8">
                      <input type="checkbox" class="js-small f-right fields clr-check-client" name="cis_contractor" id="cis_contractor" <?php if(isset($client['crm_cis_contractor']) && ($client['crm_cis_contractor']=='on') ){?> checked="checked"<?php } ?>>
                   </div>
                </div> 
                <div class="form-group row  date_birth contratab_sort" data-id="<?php echo $Setting_Order['crm_cis_contractor_start_date']; ?>"
                 >
                   <label class="col-sm-4 col-form-label">
                   <?php echo $Setting_Label['crm_cis_contractor_start_date']; ?>
                   <span class="Hilight_Required_Feilds">*</span>
                  </label>
                   <div class="col-sm-8">
                      <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                      <input class="form-control clr-check-select datepicker-13 fields LISTEN_CHANGES" type="text" name="cis_contractor_start_date" id="datepicker14" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_cis_contractor_start_date']) && ($client['crm_cis_contractor_start_date']!='') ){ echo Change_Date_Format( $client['crm_cis_contractor_start_date'] );}?>"/>
                   </div>
                </div>
                <div class="form-group row contratab_sort" data-id="<?php echo $Setting_Order['crm_cis_scheme_notes']; ?>" <?php echo $Setting_Delete['crm_cis_scheme_notes']; ?>>
                   <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_cis_scheme_notes']; ?></label>
                   <div class="col-sm-8">
                      <textarea rows="3" placeholder="" name="cis_scheme_notes" id="cis_scheme_notes" class="fields clr-check-client"><?php if(isset($client['crm_cis_scheme_notes']) && ($client['crm_cis_scheme_notes']!='') ){ echo $client['crm_cis_scheme_notes'];}?></textarea>
                   </div>
                </div>
                <div class="form-group row bookkeep_sort" data-id="<?php echo $Setting_Order['crm_cis_frequency']; ?>" <?php echo $Setting_Delete['crm_cis_frequency']; ?>>
                     <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_cis_frequency']; ?>
                       <span class="Hilight_Required_Feilds">*</span>
                     </label>
                     <div class="col-sm-8">
                        <select name="crm_cis_frequency" id="crm_cis_frequency" class="form-control fields required LISTEN_CHANGES clr-check-select">
                           <option value="">--Select--</option>
                           <option value="Weekly" <?php if(isset($client['crm_cis_frequency']) && $client['crm_cis_frequency']=='Weekly') {?> selected="selected"<?php } ?>>Weekly</option>
                           <option value="Monthly" <?php if(isset($client['crm_cis_frequency']) && $client['crm_cis_frequency']=='Monthly') {?> selected="selected"<?php } ?>>Monthly</option>
                           <option value="Quarterly" <?php if(isset($client['crm_cis_frequency']) && $client['crm_cis_frequency']=='Quarterly') {?> selected="selected"<?php } ?>>Quarterly</option>
                           <option value="Annually" <?php if(isset($client['crm_cis_frequency']) && $client['crm_cis_frequency']=='Annually') {?> selected="selected"<?php } ?>>Annually</option>
                        </select>
                     </div>
                  </div>
             </div>
          </div>
       </div>
    </div>
   
  <div class="accordion-panel enable_cis"  style="<?php echo ( $tab_on['cis']['reminder']!==0 ? "display: block" :"display:none;");?>">
     <div class="box-division03 info-box-div">
        <div class="accordion-heading" role="tab" id="headingOne">
           <h3 class="card-title accordion-title">
              <a class="accordion-msg">CIS Reminders</a>
           </h3>
        </div>
        <div id="collapse" class="panel-collapse">
           <div class="basic-info-client1 CONTENT_CIS_REMINDERS" id="contratab_box2">
              <div class="form-group row  date_birth radio_bts contratab_sort" data-id="<?php echo $Setting_Order['crm_cis_next_reminder_date']; ?>"
                <?php echo $Setting_Delete['crm_cis_next_reminder_date']; ?>>
                 <label class="col-sm-4 col-form-label"><a id="cis_add_custom_reminder_link" name="cis_add_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/8" target="_blank"><?php echo $Setting_Label['crm_cis_next_reminder_date']; ?></a></label>
                 <div class="col-sm-8">
                    <!--  <input type="hidden" name="cis_next_reminder_date" id="cis_next_reminder_date" placeholder="How many years" value="<?php if(isset($client['crm_cis_next_reminder_date']) && ($client['crm_cis_next_reminder_date']!='') ){ echo $client['crm_cis_next_reminder_date'];}?>" class="fields datepicker"> -->
                    <input type="checkbox" class="js-small f-right fields as_per_firm LISTEN_CHANGES clr-check-client" name="cis_next_reminder_date" id="Cis_next_reminder_date" <?php if(isset($client['crm_cis_next_reminder_date']) && ($client['crm_cis_next_reminder_date']!='') ){ echo "checked"; } ?>>
                    <!--ggggg-->
                 </div>
              </div>
              <!-- <div class="form-group row radio_bts eightt" style="<?php //echo $eight;?>">
                 <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                 <div class="col-sm-8">
                    <input type="checkbox" class="js-small f-right fields" name="cis_create_task_reminder" id="cis_create_task_reminder" <?php if(isset($client['crm_cis_create_task_reminder']) && ($client['crm_cis_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> value="on" >
                 </div>
              </div> -->
              <?php $cis_cus_reminder = (!empty($client['crm_cis_add_custom_reminder'])?$client['crm_cis_add_custom_reminder']:''); ?>

              <div class="form-group row radio_bts cis_add_custom_reminder_label contratab_sort" data-id="<?php echo $Setting_Order['crm_cis_add_custom_reminder'];?>"
                <?php echo $Setting_Delete['crm_cis_add_custom_reminder'];?>>
                 <label id="cis_add_custom_reminder_label" name="cis_add_custom_reminder_label" class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_cis_add_custom_reminder'];?></label>
                 <div class="col-sm-8" id="cis_add_custom_reminder">
                    <input type="checkbox" class="js-small f-right fields cus_reminder LISTEN_CHANGES clr-check-client" name="cis_add_custom_reminder" id="" data-id="cis_cus" data-serid="8" value="<?php echo $cis_cus_reminder;?>" <?php if($cis_cus_reminder!=''){ ?> checked <?php } ?> >
                    <!-- <textarea rows="4" name="cis_add_custom_reminder" id="cis_add_custom_reminder" class="form-control fields"><?php if(isset($client['crm_cis_add_custom_reminder']) && ($client['crm_cis_add_custom_reminder']!='') ){ echo $client['crm_cis_add_custom_reminder'];}?></textarea> -->
                    <!-- <a name="cis_add_custom_reminder" id="cis_add_custom_reminder" href="<?php echo base_url();?>user/Service_reminder_settings/10" target="_blank">Click to add Custom Reminder</a> -->
                 </div>
              </div>
              <?php 
                 if(!empty($client))
                 {
                  $cus_temp = $this->db->query('select id,subject from  custom_service_reminder where client_id='.$client['id'].' and firm_id='.$_SESSION['firm_id'].' and service_id=8')->result_array();
                   foreach ($cus_temp as $key => $value) {                   
                  ?>
              <div class="form-group row name_fields" style="">
                 <label class="col-sm-4 col-form-label cus_reminder_subject_<?php echo $value['id']; ?>"><?php echo $value['subject']; ?></label>
                 <div class="col-sm-8">
                    <a href="javascript:;" class="date-editop" data-toggle="modal" id="cus_reminder_<?php echo $value['id']; ?>">Edit</a>
                 </div>
              </div>
              <?php } 
              } ?>
           </div>
        </div>
     </div>
  </div>
  <!-- accordion-panel -->
      <input type="hidden" class="UPDATE_LISTENED" name="is_service_data_edited[8]" value="0">
  </div>
  
                  
   
  </li>
<!-- CIS/CIS SUB CLOSE -->

 <!-- CIS SUB OPEN -->
<li class="show-block" style="<?php echo ($tab_on['cissub']['tab']!==0  ? "display: block" :"display:none;");?>">
  <a href="#" class="toggle">CIS Sub Contractor</a>
  <div id="sub_contratab" class="masonry-container floating_set inner-views LISTEN_CONTENT">
   <div class="grid-sizer"></div>
    <div class="accordion-panel">
       <div class="box-division03 info-box-div">
          <div class="accordion-heading" role="tab" id="headingOne">
             <h3 class="card-title accordion-title">
                <a class="accordion-msg"> CIS Sub Contractor</a>
             </h3>
          </div>
          <div id="collapse" class="panel-collapse">
             <div class="basic-info-client1 CIS_Sub_contractor" id="contratab_box3">
              
                <div class="form-group row name_fields radio_bts sub_contratab_sort"  data-id="<?php echo $Setting_Order['crm_cis_subcontractor']; ?>" <?php echo $Setting_Delete['crm_cis_subcontractor']; ?>>
                   <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_cis_subcontractor']; ?></label>
                   <div class="col-sm-8">
                      <input type="checkbox" name="cis_subcontractor" id="cis_subcontractor" class="js-small f-right fields clr-check-client" <?php if(isset($client['crm_cis_subcontractor']) && ($client['crm_cis_subcontractor']=='on') ){?> checked="checked"<?php } ?>>
                   </div>
                </div>
                <div class="form-group row  date_birth sub_contratab_sort" data-id="<?php echo $Setting_Order['crm_cis_subcontractor_start_date']; ?>">
                   <label class="col-sm-4 col-form-label" >
                   <?php echo $Setting_Label['crm_cis_subcontractor_start_date']; ?>
                   <span class="Hilight_Required_Feilds">*</span>
                  </label>
                   <div class="col-sm-8">
                      <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                      <input class="form-control clr-check-select datepicker-13 fields LISTEN_CHANGES" type="text" name="cis_subcontractor_start_date" id="datepicker15" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_cis_subcontractor_start_date']) && ($client['crm_cis_subcontractor_start_date']!='') ){ echo Change_Date_Format( $client['crm_cis_subcontractor_start_date'] );}?>"/>
                   </div>
                </div>
                  <div class="form-group row  date_birth sub_contratab_sort" data-id="<?php echo $Setting_Order['crm_cis_subcontractor_frequency']; ?>">
                   <label class="col-sm-4 col-form-label" >
                   <?php echo $Setting_Label['crm_cis_subcontractor_frequency']; ?>
                   <span class="Hilight_Required_Feilds">*</span>
                  </label>
                   <div class="col-sm-8">                      
                        <select name="crm_cis_subcontractor_frequency" id="crm_cis_subcontractor_frequency" class="form-control fields required LISTEN_CHANGES clr-check-select">
                           <option value="">--Select--</option>
                           <option value="Weekly" <?php if(!empty($client['crm_cis_subcontractor_frequency']) && $client['crm_cis_subcontractor_frequency']=='Weekly') {?> selected="selected"<?php } ?>>Weekly</option>
                           <option value="Monthly" <?php if(!empty($client['crm_cis_subcontractor_frequency']) && $client['crm_cis_subcontractor_frequency']=='Monthly') {?> selected="selected"<?php } ?>>Monthly</option>
                           <option value="Quarterly" <?php if(!empty($client['crm_cis_subcontractor_frequency']) && $client['crm_cis_subcontractor_frequency']=='Quarterly') {?> selected="selected"<?php } ?>>Quarterly</option>
                           <option value="Annually" <?php if(!empty($client['crm_cis_subcontractor_frequency']) && $client['crm_cis_subcontractor_frequency']=='Annually') {?> selected="selected"<?php } ?>>Annually</option>
                        </select>
                   </div>
                </div>
                <div class="form-group row sub_contratab_sort" data-id="<?php echo $Setting_Order['crm_cis_subcontractor_scheme_notes']; ?>" <?php echo $Setting_Delete['crm_cis_subcontractor_scheme_notes']; ?>>
                   <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_cis_subcontractor_scheme_notes']; ?></label>
                   <div class="col-sm-8">
                      <textarea rows="3" placeholder="" name="cis_subcontractor_scheme_notes" id="cis_subcontractor_scheme_notes" class="fields clr-check-client"><?php if(isset($client['crm_cis_subcontractor_scheme_notes']) && ($client['crm_cis_subcontractor_scheme_notes']!='') ){ echo $client['crm_cis_subcontractor_scheme_notes'];}?></textarea>
                   </div>
                </div>
             </div>
          </div>
       </div>
    </div>
   <div class="accordion-panel  enable_cissub" style="<?php echo ($tab_on['cissub']['reminder']!==0 ? "display: block" :"display:none;");?>">
      <div class="box-division03 info-box-div">
         <div class="accordion-heading" role="tab" id="headingOne">
            <h3 class="card-title accordion-title">
               <a class="accordion-msg">CIS SUB Reminders</a>
            </h3>
         </div>
         <div id="collapse" class="panel-collapse">
            <div class="basic-info-client1 CONTENT_CIS_SUB_REMINDERS" id="contratab_box1">
               <div class="form-group row  date_birth radio_bts sub_contratab_sort" data-id="<?php echo $Setting_Order['crm_cissub_next_reminder_date']; ?>"
                <?php echo $Setting_Delete['crm_cissub_next_reminder_date']; ?>>
                  <label class="col-sm-4 col-form-label"><a id="cissub_add_custom_reminder_link" name="cissub_add_custom_reminder_link" target="_blank" href="<?php echo base_url(); ?>user/Service_reminder_settings/9"><?php echo $Setting_Label['crm_cissub_next_reminder_date']; ?></a></label>
                  <div class="col-sm-8">
                     <!--  <input type="text" name="payroll_next_reminder_date" id="payroll_next_reminder_date" placeholder="How many years" value="<?php //if(isset($client['crm_payroll_next_reminder_date']) && ($client['crm_payroll_next_reminder_date']!='') ){ echo $client['crm_payroll_next_reminder_date'];}?>" class="fields datepicker">-->
                     <input type="checkbox" class="js-small f-right fields as_per_firm LISTEN_CHANGES clr-check-client" name="cissub_next_reminder_date" id="cissub_next_reminder_date" <?php if(isset($client['crm_cissub_next_reminder_date']) && ($client['crm_cissub_next_reminder_date']=='on')){ ?> checked="checked" <?php } ?>>
                     <!--mmmmm-->
                  </div>
               </div>
              <!--  <div class="form-group row radio_bts ">
                  <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                  <div class="col-sm-8">
                     <input type="checkbox" class="js-small f-right fields" name="cissub_create_task_reminder" id="cissub_create_task_reminder" <?php if(isset($client['crm_cissub_create_task_reminder']) && ($client['crm_cissub_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> value="on" >
                  </div>
               </div> -->
               <?php $cis_sub_cus_reminder = (!empty($client['crm_cissub_add_custom_reminder'])?$client['crm_cissub_add_custom_reminder']:''); ?>

               <div class="form-group row radio_bts cissub_add_custom_reminder_label sub_contratab_sort" data-id="<?php echo $Setting_Order['crm_cissub_add_custom_reminder']; ?>"
                <?php echo $Setting_Delete['crm_cissub_add_custom_reminder']; ?>>
                  <label id="cissub_add_custom_reminder_label" name="cissub_add_custom_reminder_label" class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_cissub_add_custom_reminder']; ?></label>
                  <div class="col-sm-8" id="cissub_add_custom_reminder">
                     <input type="checkbox" class="js-small f-right fields cus_reminder LISTEN_CHANGES clr-check-client" name="cissub_add_custom_reminder" id="" data-id="cissub_cus" data-serid="9" value="<?php echo $cis_sub_cus_reminder;?>" <?php if($cis_sub_cus_reminder!=''){ ?> checked <?php } ?> >
                     <!--  <textarea rows="4" name="cissub_add_custom_reminder" id="cissub_add_custom_reminder" class="form-control fields"><?php if(isset($client['crm_cissub_add_custom_reminder']) && ($client['crm_cissub_add_custom_reminder']!='') ){ echo $client['crm_cissub_add_custom_reminder'];}?></textarea> -->
                     <!-- <a name="cissub_add_custom_reminder" id="cissub_add_custom_reminder" href="<?php echo base_url();?>user/Service_reminder_settings/11" target="_blank">Click to add Custom Reminder</a> -->
                  </div>
               </div>
               <?php 
                  if(!empty($client))
                  {
                    $cus_temp = $this->db->query('select id,subject from  custom_service_reminder where client_id='.$client['id'].' and firm_id='.$_SESSION['firm_id'].' and service_id=9')->result_array();
                    foreach ($cus_temp as $key => $value) {
                    
                   ?>
               <div class="form-group row name_fields" style="">
                  <label class="col-sm-4 col-form-label cus_reminder_subject_<?php echo $value['id']; ?>"><?php echo $value['subject']; ?></label>
                  <div class="col-sm-8">
                     <a href="javascript:;" class="date-editop" data-toggle="modal" id="cus_reminder_<?php echo $value['id']; ?>">Edit</a>
                  </div>
               </div>
               <?php } } ?>
            </div>
         </div>
      </div>
   </div>
      <input type="hidden" class="UPDATE_LISTENED" name="is_service_data_edited[9]" value="0">    
  </div>
  
                  
   
  </li>
  <!--  CIS SUB OPEN -->

<!-- Investigation Insurance open -->
 <li class="show-block" style="<?php echo ( $tab_on['investgate']['tab']!==0 ? "display: block" :"display:none;"); ?>">
   <a href="#" class="toggle">Tax Investigation Insurance</a>
   <div  id="investigation-insurance" class="masonry-container floating_set inner-views LISTEN_CONTENT">
   <div class="grid-sizer"></div>
   <div class="accordion-panel">
      <div class="box-division03 info-box-div">
         <div class="accordion-heading" role="tab" id="headingOne">
            <h3 class="card-title accordion-title">
               <a class="accordion-msg">Tax Investigation Insurance</a>
            </h3>
         </div>
         <div id="collapse" class="panel-collapse">
            <div class="basic-info-client1 CONTENT_INVESTIGATION_INSURANCE" id="invest-box">
               <div class="form-group row help_icon date_birth invest_sort" data-id="<?php echo $Setting_Order['crm_invesitgation_insurance']; ?>"
                <?php echo $Setting_Delete['crm_invesitgation_insurance']; ?>>
                  <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_invesitgation_insurance']; ?></label>
                  <div class="col-sm-8">
                     <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                     <input class="form-control clr-check-select datepicker-13 fields" name="insurance_start_date" id="datepicker30" type="text" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_invesitgation_insurance']) && ($client['crm_invesitgation_insurance']!='') ){ echo Change_Date_Format( $client['crm_invesitgation_insurance'] );}?>"/>
                  </div>
               </div>
               <div class="form-group row help_icon date_birth invest_sort" data-id="<?php echo $Setting_Order['crm_insurance_renew_date']; ?>"
                >
                      <label class="col-sm-4 col-form-label">
                        <?php echo $Setting_Label['crm_insurance_renew_date']; ?>
                          <span class="Hilight_Required_Feilds">*</span>
                        </label>
                                                                           <div class="col-sm-8">
                     <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                     <input class="form-control clr-check-select datepicker-13 fields LISTEN_CHANGES" name="insurance_renew_date" id="datepicker31" type="text" placeholder="dd-mm-yyyy" value="<?php if(!empty($client['crm_insurance_renew_date']) ){ echo Change_Date_Format( $client['crm_insurance_renew_date'] );}?>"/>
                  </div>
               </div>
               <div class="form-group row invest_sort" data-id="<?php echo $Setting_Order['crm_insurance_provider']; ?>" <?php echo $Setting_Delete['crm_insurance_provider']; ?>>
                  <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_insurance_provider']; ?></label>
                  <div class="col-sm-8">
                     <select name="insurance_provider" id="insurance_provider" class="form-control fields clr-check-select">
                        <option value="">--Select--</option>
                        <option value="Excel" <?php if(isset($client['crm_insurance_provider']) && $client['crm_insurance_provider']=='Excel') {?> selected="selected"<?php } ?>>Excel</option>
                        <option value="Software" <?php if(isset($client['crm_insurance_provider']) && $client['crm_insurance_provider']=='Software') {?> selected="selected"<?php } ?>>Software</option>
                     </select>
                  </div>
               </div>
               <div class="form-group row invest_sort" data-id="<?php echo $Setting_Order['crm_claims_note']; ?>" <?php echo $Setting_Delete['crm_claims_note']; ?>>
                  <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_claims_note']; ?></label>
                  <div class="col-sm-8">
                     <textarea rows="3" placeholder="" name="claims_note" id="claims_note" class="fields clr-check-client"><?php if(isset($client['crm_claims_note']) && ($client['crm_claims_note']!='') ){ echo $client['crm_claims_note'];}?></textarea>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- accordion-panel -->
   <div class="accordion-panel enable_invest" style="<?php echo ($tab_on['investgate']['reminder']!==0  ? "display: block" :"display:none;");?>">
      <div class="box-division03 info-box-div">
         <div class="accordion-heading" role="tab" id="headingOne">
            <h3 class="card-title accordion-title">
               <a class="accordion-msg">Tax Investigation Insurance Reminders</a>
            </h3>
         </div>
         <div id="collapse" class="panel-collapse">
            <div class="basic-info-client1 CONTENT_INVESTIGATION_INSURANCE_REMINDER"  id="invest_box1">
               <div class="form-group row  date_birth radio_bts invest_sort" data-id="<?php echo $Setting_Order['crm_insurance_next_reminder_date']; ?>" <?php echo $Setting_Delete['crm_insurance_next_reminder_date']; ?>>
                  <label class="col-sm-4 col-form-label"><a id="insurance_add_custom_reminder_link" name="insurance_add_custom_reminder_link" href="<?php echo base_url(); ?>user/Service_reminder_settings/13" target="_blank"><?php echo $Setting_Label['crm_insurance_next_reminder_date']; ?></a></label>
                  <div class="col-sm-8">
                     <!-- <input type="hidden" name="insurance_next_reminder_date" id="insurance_next_reminder_date" placeholder="" value="<?php if(isset($client['crm_insurance_next_reminder_date']) && ($client['crm_insurance_next_reminder_date']!='') ){ echo $client['crm_insurance_next_reminder_date'];}?>" class="fields datepicker"> -->
                     <input type="checkbox" class="js-small f-right fields as_per_firm LISTEN_CHANGES clr-check-select" name="insurance_next_reminder_date" id="insurance_next_reminder_date"   <?php if(isset($client['crm_insurance_next_reminder_date']) && ($client['crm_insurance_next_reminder_date']!='') ){ echo "checked"; } ?>>
                  </div>
               </div>
               <!-- <div class="form-group row radio_bts thirteent" style="<?php //echo $thirteent; ?>">
                  <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                  <div class="col-sm-8">
                     <input type="checkbox" class="js-small f-right fields" name="insurance_create_task_reminder" id="insurance_create_task_reminder" <?php if(isset($client['crm_insurance_create_task_reminder']) && ($client['crm_insurance_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> value="on" >
                  </div>
               </div> -->
               <?php $invs_ins_cus_reminder = (!empty($client['crm_insurance_add_custom_reminder'])?$client['crm_insurance_add_custom_reminder']:'');?>
               <div class="form-group row radio_bts insurance_add_custom_reminder_label invest_sort" data-id="<?php echo $Setting_Order['crm_insurance_add_custom_reminder']; ?>" <?php echo $Setting_Delete['crm_insurance_add_custom_reminder']; ?>>
                  <label class="col-sm-4 col-form-label" name="insurance_add_custom_reminder_label" id="insurance_add_custom_reminder_label"><?php echo $Setting_Label['crm_insurance_add_custom_reminder']; ?></label>
                  <div class="col-sm-8" id="insurance_add_custom_reminder">
                     <input type="checkbox" class="js-small f-right fields cus_reminder LISTEN_CHANGES clr-check-client" name="insurance_add_custom_reminder" id="" data-id="insurance_cus" data-serid="13" value="<?php echo $invs_ins_cus_reminder; ?>" <?php if($invs_ins_cus_reminder!=''){ ?> checked <?php } ?>>
                     <!-- <textarea rows="3" placeholder="" name="insurance_add_custom_reminder" id="insurance_add_custom_reminder" class="fields"><?php if(isset($client['crm_insurance_add_custom_reminder']) && ($client['crm_insurance_add_custom_reminder']!='') ){ echo $client['crm_insurance_add_custom_reminder'];}?></textarea> -->
                     <!-- <a name="insurance_add_custom_reminder" id="insurance_add_custom_reminder" href="<?php echo base_url(); ?>user/Service_reminder_settings/9" target="_blank">Click to add Custom Reminder</a> -->
                  </div>
               </div>
               <?php 
                  if(!empty($client)){
                     $cus_temp = $this->db->query('select id,subject from  custom_service_reminder where client_id='.$client['id'].' and firm_id='.$_SESSION['firm_id'].' and service_id=13')->result_array();
                    foreach ($cus_temp as $key => $value) {
                    
                   ?>
               <div class="form-group row name_fields" style="">
                  <label class="col-sm-4 col-form-label cus_reminder_subject_<?php echo $value['id']; ?>"><?php echo $value['subject']; ?></label>
                  <div class="col-sm-8">
                     <a href="javascript:;" class="date-editop" data-toggle="modal" id="cus_reminder_<?php echo $value['id']; ?>">Edit</a>
                  </div>
               </div>
               <?php } } ?>
            </div>
         </div>
      </div>
   </div>
      <input type="hidden" class="UPDATE_LISTENED" name="is_service_data_edited[13]" value="0">    
   </div>
</li>                  
<!-- Investigation Insurance close -->



<!-- management-a/c tab start -->
<li class="show-block" style="<?php echo ( $tab_on['management']['tab']!==0 ? "display: block" :"display:none;");?>">
<a href="#" class="toggle">  Management Accounts  </a>                            
  <div id="management-account" class="masonry-container floating_set inner-views LISTEN_CONTENT">
   <div class="grid-sizer"></div>
   
   <div class="accordion-panel for_management_account">
      <div class="box-division03 info-box-div">
         <div class="accordion-heading" role="tab" id="headingOne">
            <h3 class="card-title accordion-title">
               <a class="accordion-msg">Management Accounts</a>
            </h3>
         </div>
         <div id="collapse" class="panel-collapse">
            <div class="basic-info-client1 CONTENT_MANAGEMENT_ACCOUNTS" id="management_account">
               <div class="form-group row management_sort" data-id="<?php echo $Setting_Order['crm_manage_acc_fre']; ?>" 
                <?php echo $Setting_Delete['crm_manage_acc_fre']; ?>>
                  <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_manage_acc_fre']; ?>
                    <span class="Hilight_Required_Feilds">*</span>
                  </label>
                  <div class="col-sm-8">
                     <select name="manage_acc_fre" id="manage_acc_fre" class="form-control fields required clr-check-select">
                        <option value="">--Select--</option>
                        <option value="Weekly" <?php if(isset($client['crm_manage_acc_fre']) && $client['crm_manage_acc_fre']=='Weekly') {?> selected="selected"<?php } ?>>Weekly</option>
                        <option value="Monthly" <?php if(isset($client['crm_manage_acc_fre']) && $client['crm_manage_acc_fre']=='Monthly') {?> selected="selected"<?php } ?>>Monthly</option>
                        <option value="Quarterly" <?php if(isset($client['crm_manage_acc_fre']) && $client['crm_manage_acc_fre']=='Quarterly') {?> selected="selected"<?php } ?>>Quarterly</option>
                        <option value="Annually" <?php if(isset($client['crm_manage_acc_fre']) && $client['crm_manage_acc_fre']=='Annually') {?> selected="selected"<?php } ?>>Annually</option>
                     </select>
                  </div>
               </div>
               <div class="form-group row help_icon date_birth management_sort" data-id="<?php echo $Setting_Order['crm_next_manage_acc_date']; ?>" >
                  <label class="col-sm-4 col-form-label">
                   <?php echo $Setting_Label['crm_next_manage_acc_date']; ?>
                     
                  <span class="Hilight_Required_Feilds">*</span>
                   </label>
                  <div class="col-sm-8">
                     <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                     <input class="form-control clr-check-select datepicker-13 fields LISTEN_CHANGES" name="next_manage_acc_date" id="datepicker28" type="text" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_next_manage_acc_date']) && ($client['crm_next_manage_acc_date']!='') ){ echo Change_Date_Format( $client['crm_next_manage_acc_date'] );}?>"/>
                  </div>
               </div>
               <div class="form-group row management_sort" data-id="<?php echo $Setting_Order['crm_manage_method_bookkeeping']; ?>" <?php echo $Setting_Delete['crm_manage_method_bookkeeping']; ?>>
                  <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_manage_method_bookkeeping']; ?></label>
                  <div class="col-sm-8">
                     <select name="manage_method_bookkeeping" id="manage_method_bookkeeping" class="form-control fields clr-check-select">
                        <option value="">--Select--</option>
                        <option value="Excel" <?php if(isset($client['crm_manage_method_bookkeeping']) && $client['crm_manage_method_bookkeeping']=='Excel') {?> selected="selected"<?php } ?>>Excel</option>
                        <option value="Software" <?php if(isset($client['crm_manage_method_bookkeeping']) && $client['crm_manage_method_bookkeeping']=='Software') {?> selected="selected"<?php } ?>>Software</option>
                     </select>
                  </div>
               </div>
               <div class="form-group row management_sort" data-id="<?php echo $Setting_Order['crm_manage_client_provide_record']; ?>"
                <?php echo $Setting_Delete['crm_manage_client_provide_record']; ?>>
                  <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_manage_client_provide_record']; ?></label>
                  <div class="col-sm-8">
                     <select name="manage_client_provide_record" id="manage_client_provide_record" class="form-control fields clr-check-select">
                        <option value="">--Select--</option>
                        <option value="Email/DropBox" <?php if(isset($client['crm_manage_client_provide_record']) && $client['crm_manage_client_provide_record']=='Email/DropBox') {?> selected="selected"<?php } ?>>Email/DropBox</option>
                        <option value="Google/Online Portal" <?php if(isset($client['crm_manage_client_provide_record']) && $client['crm_manage_client_provide_record']=='Google/Online Portal') {?> selected="selected"<?php } ?>>Google/Online Portal</option>
                     </select>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- accordion-panel -->
   <div class="accordion-panel enable_management" style="<?php echo ( $tab_on['management']['reminder']!==0 ? "display: block" :"display:none;");?>">
      <div class="box-division03 info-box-div">
         <div class="accordion-heading" role="tab" id="headingOne">
            <h3 class="card-title accordion-title">
               <a class="accordion-msg">Management Reminders</a>
            </h3>
         </div>
         <div id="collapse" class="panel-collapse">
            <div class="basic-info-client1 CONTENT_MANAGEMENT_ACCOUNTS_REMINDERS" id="management_account1">
               <div class="form-group row  date_birth radio_bts management_sort" data-id="<?php echo $Setting_Order['crm_manage_next_reminder_date']; ?>"
                <?php echo $Setting_Delete['crm_manage_next_reminder_date']; ?>>
                  <label class="col-sm-4 col-form-label"><a id="manage_add_custom_reminder_link" name="manage_add_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/12" target="_blank"><?php echo $Setting_Label['crm_manage_next_reminder_date']; ?></a></label>
                  <div class="col-sm-8">
                   <!--   <input type="hidden" name="manage_next_reminder_date" id="manage_next_reminder_date" placeholder="" value="<?php if(isset($client['crm_manage_next_reminder_date']) && ($client['crm_manage_next_reminder_date']!='') ){ echo $client['crm_manage_next_reminder_date'];}?>" class="fields datepicker-13"> -->
                     <input type="checkbox" class="js-small f-right fields as_per_firm LISTEN_CHANGES clr-check-client" name="manage_next_reminder_date" id="manage_next_reminder_date" <?php if(isset($client['crm_manage_next_reminder_date']) && ($client['crm_manage_next_reminder_date']!='') ){ echo "checked"; } ?> >
                     <!--jjjjj-->
                  </div>
               </div>
               <!-- <div class="form-group row radio_bts management" data-id="<?php echo $Setting_Order['crm_manage_create_task_reminder']; ?>">
                  <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                  <div class="col-sm-8">
                     <input type="checkbox" class="js-small f-right fields" name="manage_create_task_reminder" id="manage_create_task_reminder" <?php if(isset($client['crm_manage_create_task_reminder']) && ($client['crm_manage_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> value="on">
                  </div>
               </div> -->
               <?php $management_cus_reminder = (!empty($client['crm_manage_add_custom_reminder'])?$client['crm_manage_add_custom_reminder']:''); ?>
               <div class="form-group row radio_bts manage_add_custom_reminder_label  management_sort" data-id="<?php echo $Setting_Order['crm_manage_add_custom_reminder']; ?>" <?php echo $Setting_Delete['crm_manage_add_custom_reminder']; ?>>
                  <label id="manage_add_custom_reminder_label" name="manage_add_custom_reminder_label" class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_manage_add_custom_reminder']; ?></label>
                  <div class="col-sm-8" id="manage_add_custom_reminder">
                     <input type="checkbox" class="js-small f-right fields cus_reminder LISTEN_CHANGES clr-check-client" name="manage_add_custom_reminder" id="" data-id="manage_cus" data-serid="12" value="<?php echo $management_cus_reminder?>"  <?php if($management_cus_reminder!=''){ ?> checked <?php } ?> >
                     <!-- <textarea rows="3" placeholder="" name="manage_add_custom_reminder" id="manage_add_custom_reminder" class="fields"><?php if(isset($client['crm_manage_add_custom_reminder']) && ($client['crm_manage_add_custom_reminder']!='') ){ echo $client['crm_manage_add_custom_reminder'];}?></textarea> -->
                     <!-- <a name="manage_add_custom_reminder" id="manage_add_custom_reminder" href="<?php echo base_url();?>user/Service_reminder_settings/14" target="_blank">Click to add Custom Reminder</a> -->
                  </div>
               </div>
               <?php 
                  if(!empty($client))
                  {
                    $cus_temp = $this->db->query('select id,subject from  custom_service_reminder where client_id='.$client['id'].' and firm_id='.$_SESSION['firm_id'].' and service_id=12')->result_array();
                    foreach ($cus_temp as $key => $value) {
                    
                   ?>
               <div class="form-group row name_fields" style="">
                  <label class="col-sm-4 col-form-label cus_reminder_subject_<?php echo $value['id']; ?>"><?php echo $value['subject']; ?></label>
                  <div class="col-sm-8">
                     <a href="javascript:;" class="date-editop" data-toggle="modal" id="cus_reminder_<?php echo $value['id']; ?>">Edit</a>
                  </div>
               </div>
               <?php } } ?>
            </div>
         </div>
      </div>
   </div>  
   <!-- accordion-panel -->
      <input type="hidden" class="UPDATE_LISTENED" name="is_service_data_edited[12]" value="0">       
  </div>
  <!-- Management Accounts -->
</li>
<!-- management-a/c tab end -->

<!-- payroll tab start -->
<li class="show-block" style="<?php echo ($tab_on['payroll']['tab']!==0?"display: block":"display: none");?>">
<a href="#" class="toggle">  Payroll</a>
<!-- Pay Roll -->              
  <div id="payroll" class="masonry-container floating_set inner-views LISTEN_CONTENT">
     <div class="grid-sizer"></div>
     <div class="accordion-panel">
        <div class="box-division03 info-box-div">
           <div class="accordion-heading" role="tab" id="headingOne">
              <h3 class="card-title accordion-title">
                 <a class="accordion-msg">Important Information</a>
              </h3>
           </div>
           <div id="collapse" class="panel-collapse">
              <div class="basic-info-client1 CONTENT_IMPORTANT_INFORMATION" id="payroll_box">
                 <div class="form-group row name_fields payroll_sec" data-id="<?php echo $Setting_Order['crm_payroll_acco_off_ref_no']; ?>" <?php echo $Setting_Delete['crm_payroll_acco_off_ref_no']; ?>>
                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_payroll_acco_off_ref_no']; ?></label>
                    <div class="col-sm-8">
                       <input type="number" name="payroll_acco_off_ref_no" id="payroll_acco_off_ref_no" placeholder="" value="<?php if(isset($client['crm_payroll_acco_off_ref_no']) && ($client['crm_payroll_acco_off_ref_no']!='') ){ echo $client['crm_payroll_acco_off_ref_no'];}?>" class="fields clr-check-client">
                    </div>
                 </div>
                 <div class="form-group row name_fields payroll_sec" data-id="<?php echo $Setting_Order['crm_paye_off_ref_no']; ?>" <?php echo $Setting_Delete['crm_paye_off_ref_no']; ?>>
                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_paye_off_ref_no']; ?></label>
                    <div class="col-sm-8">
                       <input type="number" name="paye_off_ref_no" id="paye_off_ref_no" placeholder="" value="<?php if(isset($client['crm_paye_off_ref_no']) && ($client['crm_paye_off_ref_no']!='') ){ echo $client['crm_paye_off_ref_no'];}?>" class="fields clr-check-client">
                    </div>
                 </div>
              </div>
           </div>
        </div>
     </div>
     <!-- accordion-panel -->
     <div class="accordion-panel for_payroll_section">
        <div class="box-division03 info-box-div">
           <div class="accordion-heading" role="tab" id="headingOne">
              <h3 class="card-title accordion-title">
                 <a class="accordion-msg">Payroll</a>
              </h3>
           </div>
           <div id="collapse" class="panel-collapse">
              <div class="basic-info-client1 CONTENT_PAYROLL" id="payroll_box1">
                 <div class="form-group row date_birth payroll_sec" data-id="<?php echo $Setting_Order['crm_payroll_reg_date']; ?>" <?php echo $Setting_Delete['crm_payroll_reg_date']; ?>>
                    <label class="col-sm-4 col-form-label">
                      <?php echo $Setting_Label['crm_payroll_reg_date']; ?>
                    </label>
                    <div class="col-sm-8">
                       <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                       <input type="text" name="payroll_reg_date" id="datepicker1" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_payroll_reg_date']) && ($client['crm_payroll_reg_date']!='') ){ echo Change_Date_Format( $client['crm_payroll_reg_date'] );}?>" class="fields datepicker-13 clr-check-select">
                    </div>
                 </div>
                 <div class="form-group row payroll_sec" data-id="<?php echo $Setting_Order['crm_no_of_employees']; ?>"
                  <?php echo $Setting_Delete['crm_no_of_employees']; ?>
                  >
                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_no_of_employees']; ?></label>
                    <div class="col-sm-8">
                       <input type="number" name="no_of_employees" id="no_of_employees" value="<?php if(isset($client['crm_no_of_employees']) && ($client['crm_no_of_employees']!='') ){ echo $client['crm_no_of_employees'];}?>" class="fields clr-check-select">
                    </div>
                 </div>
                 <div class="form-group row  date_birth payroll_sec" data-id="<?php echo $Setting_Order['crm_first_pay_date']; ?>" <?php echo $Setting_Delete['crm_first_pay_date']; ?>>
                    <label class="col-sm-4 col-form-label "><?php echo $Setting_Label['crm_first_pay_date']; ?></label>
                    <div class="col-sm-8">
                       <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                       <input class="fields datepicker-13 clr-check-select" type="text" name="first_pay_date" id="datepicker2" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_first_pay_date']) && ($client['crm_first_pay_date']!='') ){ echo Change_Date_Format( $client['crm_first_pay_date']);}?>"/>
                    </div>
                 </div>
                 <div class="form-group row payroll_sec" data-id="<?php echo $Setting_Order['crm_payroll_run']; ?>"
                  <?php echo $Setting_Delete['crm_payroll_run']; ?>>
                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_payroll_run']; ?>
                      <span class="Hilight_Required_Feilds">*</span>
                    </label>
                    <div class="col-sm-8">
                       <select name="payroll_run" id="payroll_run" class="form-control fields required LISTEN_CHANGES clr-check-select">
                          <option value="">--Select--</option>
                          <option value="Monthly" <?php if(isset($client['crm_payroll_run']) && $client['crm_payroll_run']=='Monthly') {?> selected="selected"<?php } ?>>Monthly</option>
                          <option value="Weekly" <?php if(isset($client['crm_payroll_run']) && $client['crm_payroll_run']=='Weekly') {?> selected="selected"<?php } ?>>Weekly</option>
                          <option value="Forthnightly" <?php if(isset($client['crm_payroll_run']) && $client['crm_payroll_run']=='Forthnightly') {?> selected="selected"<?php } ?>>Forthnightly</option>
                       </select>
                    </div>
                 </div>
                 <div class="form-group row  date_birth payroll_sec" data-id="<?php echo $Setting_Order['crm_payroll_run_date']; ?>">
                    <label class="col-sm-4 col-form-label">
                      <?php echo $Setting_Label['crm_payroll_run_date']; ?>
                    <span class="Hilight_Required_Feilds">*</span>
                    </label>
                    <div class="col-sm-8">
                       <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                       <input class="datepicker-13 fields LISTEN_CHANGES clr-check-select" type="text" name="payroll_run_date" id="datepicker3" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_payroll_run_date']) && ($client['crm_payroll_run_date']!='') ){ echo Change_Date_Format( $client['crm_payroll_run_date'] );}?>"/>
                    </div>
                 </div>
                 <div class="form-group row  date_birth payroll_sec" data-id="<?php echo $Setting_Order['crm_rti_deadline']; ?>" <?php echo $Setting_Delete['crm_rti_deadline']; ?>>
                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_rti_deadline']; ?></label>
                    <div class="col-sm-8">
                       <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                       <input class="fields datepicker-13 clr-check-select" type="text" name="rti_deadline" id="datepicker4" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_rti_deadline']) && ($client['crm_rti_deadline']!='') ){ echo Change_Date_Format( $client['crm_rti_deadline'] );}?>"/>
                    </div>
                 </div>
                 <div class="form-group row radio_bts payroll_sec" data-id="<?php echo $Setting_Order['crm_previous_year_require']; ?>" <?php echo $Setting_Delete['crm_previous_year_require']; ?>>
                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_previous_year_require']; ?></label>
                    <div class="col-sm-8">
                       <input type="checkbox" class="js-small f-right fields clr-check-client" name="previous_year_require" id="previous_year_require" <?php if(isset($client['crm_previous_year_require']) && ($client['crm_previous_year_require']=='on') ){?> checked="checked"<?php } ?> data-id="preyear">
                    </div>
                 </div>
                 <?php 
                    (isset($client['crm_previous_year_require']) && $client['crm_previous_year_require'] == 'on') ? $preyear =  "block" : $preyear = 'none';
                    
                    
                    ?>
                 <div id="enable_preyear" style="display:<?php echo $preyear;?>;" data-id="<?php echo $Setting_Order['crm_payroll_if_yes']; ?>" <?php echo $Setting_Delete['crm_payroll_if_yes']; ?>>
                    <div class="form-group row name_fields  payroll_sec">
                       <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_payroll_if_yes']; ?></label>
                       <div class="col-sm-8">
                          <!-- <input type="text" name="payroll_if_yes" id="payroll_if_yes" placeholder="How many years" value="<?php if(isset($client['crm_payroll_if_yes']) && ($client['crm_payroll_if_yes']!='') ){ echo $client['crm_payroll_if_yes'];}?>" class="fields"> -->
                          <select name="payroll_if_yes" id="payroll_if_yes" class="form-control fields clr-check-select">
                             <option value="" selected="selected">--Select--</option>
                             <option value="01" <?php if(isset($client['crm_payroll_if_yes']) && $client['crm_payroll_if_yes']=='01') {?> selected="selected"<?php } ?>>01</option>
                             <option value="02" <?php if(isset($client['crm_payroll_if_yes']) && $client['crm_payroll_if_yes']=='02') {?> selected="selected"<?php } ?>>02</option>
                             <option value="03" <?php if(isset($client['crm_payroll_if_yes']) && $client['crm_payroll_if_yes']=='03') {?> selected="selected"<?php } ?>>03</option>
                             <option value="04" <?php if(isset($client['crm_payroll_if_yes']) && $client['crm_payroll_if_yes']=='04') {?> selected="selected"<?php } ?>>04</option>
                             <option value="05" <?php if(isset($client['crm_payroll_if_yes']) && $client['crm_payroll_if_yes']=='05') {?> selected="selected"<?php } ?>>05</option>
                             <option value="06" <?php if(isset($client['crm_payroll_if_yes']) && $client['crm_payroll_if_yes']=='06') {?> selected="selected"<?php } ?>>06</option>
                             <option value="07" <?php if(isset($client['crm_payroll_if_yes']) && $client['crm_payroll_if_yes']=='07') {?> selected="selected"<?php } ?>>07</option>
                             <option value="08" <?php if(isset($client['crm_payroll_if_yes']) && $client['crm_payroll_if_yes']=='08') {?> selected="selected"<?php } ?>>08</option>
                             <option value="09" <?php if(isset($client['crm_payroll_if_yes']) && $client['crm_payroll_if_yes']=='09') {?> selected="selected"<?php } ?>>09</option>
                             <option value="10" <?php if(isset($client['crm_payroll_if_yes']) && $client['crm_payroll_if_yes']=='10') {?> selected="selected"<?php } ?>>10</option>
                             <option value="11" <?php if(isset($client['crm_payroll_if_yes']) && $client['crm_payroll_if_yes']=='11') {?> selected="selected"<?php } ?>>11</option>
                             <option value="12" <?php if(isset($client['crm_payroll_if_yes']) && $client['crm_payroll_if_yes']=='12') {?> selected="selected"<?php } ?>>12</option>
                             <option value="13" <?php if(isset($client['crm_payroll_if_yes']) && $client['crm_payroll_if_yes']=='13') {?> selected="selected"<?php } ?>>13</option>
                             <option value="14" <?php if(isset($client['crm_payroll_if_yes']) && $client['crm_payroll_if_yes']=='14') {?> selected="selected"<?php } ?>>14</option>
                             <option value="15" <?php if(isset($client['crm_payroll_if_yes']) && $client['crm_payroll_if_yes']=='15') {?> selected="selected"<?php } ?>>15</option>
                          </select>
                       </div>
                    </div>
                 </div>
                 <div class="form-group row  date_birth payroll_sec" data-id="<?php echo $Setting_Order['crm_paye_scheme_ceased']; ?>" <?php echo $Setting_Delete['crm_paye_scheme_ceased']; ?>>
                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_paye_scheme_ceased']; ?></label>
                    <div class="col-sm-8">
                       <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                       <input class="form-control clr-check-select datepicker-13 fields" type="text" name="paye_scheme_ceased" id="datepicker5" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_paye_scheme_ceased']) && ($client['crm_paye_scheme_ceased']!='') ){ echo Change_Date_Format( $client['crm_paye_scheme_ceased'] );}?>"/>
                    </div>
                 </div>
              </div>
           </div>
        </div>
     </div>
     <!-- accordion-panel -->
     <div class="accordion-panel enable_pay" style="<?php echo ($tab_on['payroll']['reminder']!==0 ? "display: block" :"display:none;");?>">
        <div class="box-division03 info-box-div">
           <div class="accordion-heading" role="tab" id="headingOne">
              <h3 class="card-title accordion-title">
                 <a class="accordion-msg">Payroll Reminders</a>
              </h3>
           </div>
           <div id="collapse" class="panel-collapse">
              <div class="basic-info-client1 CONTENT_PAYROLL_REMINDERS"  id="payroll_box2">
                 <div class="form-group row  radio_bts date_birth payroll_sec" data-id="<?php echo $Setting_Order['crm_payroll_next_reminder_date']; ?>" <?php echo $Setting_Delete['crm_payroll_next_reminder_date']; ?>>
                    <label class="col-sm-4 col-form-label"><a id="payroll_add_custom_reminder_link" name="payroll_add_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/6" target="_blank"><?php echo $Setting_Label['crm_payroll_next_reminder_date']; ?></a></label>
                    <div class="col-sm-8">
                       <!-- <input type="hidden" name="payroll_next_reminder_date" id="payroll_next_reminder_date" placeholder="How many years" value="<?php if(isset($client['crm_payroll_next_reminder_date']) && ($client['crm_payroll_next_reminder_date']!='') ){ echo $client['crm_payroll_next_reminder_date'];}?>" class="fields datepicker"> -->
                       <input type="checkbox" class="js-small f-right fields as_per_firm LISTEN_CHANGES clr-check-client" name="payroll_next_reminder_date" id="Payroll_next_reminder_date" <?php if(isset($client['crm_payroll_next_reminder_date']) && ($client['crm_payroll_next_reminder_date']!='') ){ echo "checked"; } ?> >
                    </div>
                 </div>
                <!--  <div class="form-group row radio_bts payroll_sec" data-id="<?php echo $Setting_Order['crm_payroll_create_task_reminder']; ?>">
                    <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                    <div class="col-sm-8">
                       <input type="checkbox" class="js-small f-right fields" name="payroll_create_task_reminder" id="payroll_create_task_reminder" <?php if(isset($client['crm_payroll_create_task_reminder']) && ($client['crm_payroll_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> value="on" >
                    </div>
                 </div> -->
                 <?php $payroll_cus_reminder = (!empty($client['crm_payroll_add_custom_reminder'])?$client['crm_payroll_add_custom_reminder']:''); ?>
                 <div class="form-group row radio_bts payroll_add_custom_reminder_label payroll_sec" data-id="<?php echo $Setting_Order['crm_payroll_add_custom_reminder']; ?>" <?php echo $Setting_Delete['crm_payroll_add_custom_reminder']; ?> >
                    <label id="payroll_add_custom_reminder_label" name="payroll_add_custom_reminder_label" class="col-sm-4 col-form-label">
                      <?php echo $Setting_Label['crm_payroll_add_custom_reminder']; ?>                        
                      </label>
                    <div class="col-sm-8" id="payroll_add_custom_reminder">
                       <input type="checkbox" class="js-small f-right fields cus_reminder LISTEN_CHANGES clr-check-client" name="payroll_add_custom_reminder" id="" data-id="payroll_cus" data-serid="6" value="<?php echo $payroll_cus_reminder;?>" <?php if($payroll_cus_reminder!=''){ ?> checked <?php } ?> >

                       <!--   <textarea rows="4" name="payroll_add_custom_reminder" id="payroll_add_custom_reminder" class="form-control fields"><?php if(isset($client['crm_payroll_add_custom_reminder']) && ($client['crm_payroll_add_custom_reminder']!='') ){ echo $client['crm_payroll_add_custom_reminder'];}?></textarea> -->
                       <!-- <a name="payroll_add_custom_reminder" id="payroll_add_custom_reminder" href="<?php echo base_url();?>user/Service_reminder_settings/7" target="_blank">Click to add Custom Reminder</a> -->
                    </div>
                 </div>
                 <?php 
                    if(!empty($client))
                    {
                      foreach ($cus_temp as $key => $value) {
                      $cus_temp = $this->db->query('select id,subject from  custom_service_reminder where client_id='.$client['id'].' and firm_id='.$_SESSION['firm_id'].' and service_id=6')->result_array();
                     ?>
                 <div class="form-group row name_fields" style="">
                    <label class="col-sm-4 col-form-label cus_reminder_subject_<?php echo $value['id']; ?>"><?php echo $value['subject']; ?></label>
                    <div class="col-sm-8">
                       <a href="javascript:;" class="date-editop" data-toggle="modal" id="cus_reminder_<?php echo $value['id']; ?>">Edit</a>
                    </div>
                 </div>
                 <?php } } ?>
              </div>
           </div>
        </div>
     </div>
     <!-- accordion-panel -->
      <input type="hidden" class="UPDATE_LISTENED" name="is_service_data_edited[6]" value="0">                   
  </div>
</li>
<!-- payroll tab close -->



<!-- p11d open -->
<li class="show-block" style="<?php echo ($tab_on['p11d']['tab']!==0 ? 'display: block' : 'display:none'); ?>" >
<a href="#" class="toggle"> P11D</a>
   <div id="p11dtab" class="masonry-container floating_set inner-views LISTEN_CONTENT">
        <div class="grid-sizer"></div>
      <div class="accordion-panel">
         <div class="box-division03 info-box-div">
            <div class="accordion-heading" role="tab" id="headingOne">
               <h3 class="card-title accordion-title">
                  <a class="accordion-msg">P11D</a>
               </h3>
            </div>
            <div id="collapse" class="panel-collapse">
               <div class="basic-info-client1 CONTENT_P11D" id="p11d_sec">
                  <div class="form-group row  date_birth p11d_sort" data-id="<?php echo $Setting_Order['crm_p11d_latest_action_date']; ?>" 
                    <?php echo $Setting_Delete['crm_p11d_latest_action_date']; ?>>
                     <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_p11d_latest_action_date']; ?></label>
                     <div class="col-sm-8">
                        <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                        <input class="form-control clr-check-select datepicker-13 fields" type="text" name="p11d_start_date" id="datepicker17" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_p11d_latest_action_date']) && ($client['crm_p11d_latest_action_date']!='') ){ echo Change_Date_Format( $client['crm_p11d_latest_action_date'] );}?>"/>
                     </div>
                  </div>
                  <div class="form-group row name_fields p11d_sort" data-id="<?php echo $Setting_Order['crm_p11d_latest_action']; ?>"
                    <?php echo $Setting_Delete['crm_p11d_latest_action']; ?>>
                     <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_p11d_latest_action']; ?></label>
                     <div class="col-sm-8">
                        <input type="number" name="p11d_todo" id="p11d_todo" placeholder="" value="<?php if(isset($client['crm_p11d_latest_action']) && ($client['crm_p11d_latest_action']!='') ){ echo $client['crm_p11d_latest_action'];}?>" class="fields clr-check-select">
                     </div>
                  </div>
                  <div class="form-group row date_birth p11d_sort" data-id="<?php echo $Setting_Order['crm_latest_p11d_submitted']; ?>"
                    <?php echo $Setting_Delete['crm_latest_p11d_submitted']; ?>>
                     <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_latest_p11d_submitted']; ?></label>
                     <div class="col-sm-8">
                        <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                        <input class="form-control clr-check-select datepicker-13 fields" type="text" name="p11d_first_benefit_date" id="datepicker18" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_latest_p11d_submitted']) && ($client['crm_latest_p11d_submitted']!='') ){ echo Change_Date_Format( $client['crm_latest_p11d_submitted']);}?>"/>
                     </div>
                  </div>
                  <div class="form-group row date_birth p11d_sort" data-id="<?php echo $Setting_Order['crm_next_p11d_return_due']; ?>">
                     <label class="col-sm-4 col-form-label">
                      <?php echo $Setting_Label['crm_next_p11d_return_due']; ?>
                      <span class="Hilight_Required_Feilds">*</span>
                      </label>
                     <div class="col-sm-8">
                        <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                        <input type="text" name="p11d_due_date" id="p11d_due_date" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_next_p11d_return_due']) && ($client['crm_next_p11d_return_due']!='') ){ echo Change_Date_Format( $client['crm_next_p11d_return_due']);}?>" class="datepicker-13 fields LISTEN_CHANGES clr-check-client">
                     </div>
                  </div>
                  <div class="form-group row radio_bts p11d_sort" data-id="<?php echo $Setting_Order['crm_p11d_previous_year_require']; ?>"
                    <?php echo $Setting_Delete['crm_p11d_previous_year_require']; ?>>
                     <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_p11d_previous_year_require']; ?></label>
                     <div class="col-sm-8">
                        <input type="checkbox" class="js-small f-right fields clr-check-client" name="p11d_previous_year_require" id="p11d_previous_year_require" <?php if(isset($client['crm_p11d_previous_year_require']) && ($client['crm_p11d_previous_year_require']=='on') ){?> checked="checked"<?php } ?>>
                     </div>
                  </div>
                  <div class="form-group row name_fields p11d_sort" data-id="<?php echo $Setting_Order['crm_p11d_payroll_if_yes']; ?>"
                    <?php echo $Setting_Delete['crm_p11d_payroll_if_yes']; ?>>
                     <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_p11d_payroll_if_yes']; ?></label>
                     <div class="col-sm-8">
                        <input type="text" name="p11d_payroll_if_yes" id="p11d_payroll_if_yes" placeholder="How many years" value="<?php if(isset($client['crm_p11d_payroll_if_yes']) && ($client['crm_p11d_payroll_if_yes']!='') ){ echo $client['crm_p11d_payroll_if_yes'];}?>" class="fields clr-check-client">
                     </div>
                  </div>
                  <div class="form-group row  date_birth p11d_sort" data-id="<?php echo $Setting_Order['crm_p11d_records_received']; ?>"
                    <?php echo $Setting_Delete['crm_p11d_records_received']; ?>>
                     <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_p11d_records_received']; ?></label>
                     <div class="col-sm-8">
                        <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                        <input class="form-control clr-check-select datepicker-13 fields" type="text" name="p11d_paye_scheme_ceased" id="datepicker20" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_p11d_records_received']) && ($client['crm_p11d_records_received']!='') ){ echo Change_Date_Format( $client['crm_p11d_records_received'] );}?>"/>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- accordion-panel -->
      <div class="accordion-panel enable_plld" style="<?php echo ($tab_on['p11d']['reminder']!==0 ? "display: block" :"display:none;");?>">
         <div class="box-division03 info-box-div">
            <div class="accordion-heading" role="tab" id="headingOne">
               <h3 class="card-title accordion-title">
                  <a class="accordion-msg">P11D Reminders</a>
               </h3>
            </div>
            <div id="collapse" class="panel-collapse">
               <div class="basic-info-client1 CONTENT_P11D_REMINDERS" id="p11d_sec1">
                  <div class="form-group row  date_birth radio_bts p11d_sort" data-id="<?php echo $Setting_Order['crm_p11d_next_reminder_date']; ?>"
                    <?php echo $Setting_Delete['crm_p11d_next_reminder_date']; ?>>
                     <label class="col-sm-4 col-form-label"><a id="p11d_add_custom_reminder_link" name="p11d_add_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/11" target="_blank"><?php echo $Setting_Label['crm_p11d_next_reminder_date']; ?></a></label>
                     <div class="col-sm-8">
                        <!-- <input type="hidden" name="p11d_next_reminder_date" id="p11d_next_reminder_date" placeholder="" value="<?php if(isset($client['crm_p11d_records_received']) && ($client['crm_p11d_records_received']!='') ){ echo $client['crm_p11d_records_received'];}?>" class="fields datepicker"> -->
                        <input type="checkbox" class="js-small f-right fields as_per_firm LISTEN_CHANGES clr-check-client" name="p11d_next_reminder_date" id="P11d_next_reminder_date"  <?php if(isset($client['crm_p11d_next_reminder_date']) && ($client['crm_p11d_next_reminder_date']!='') ){ echo "checked"; } ?> >
                        <!--hhhhh-->
                     </div>
                  </div>
                  <!-- <div class="form-group row radio_bts ">
                     <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                     <div class="col-sm-8">
                        <input type="checkbox" class="js-small f-right fields" name="p11d_create_task_reminder" id="p11d_create_task_reminder" <?php if(isset($client['crm_p11d_create_task_reminder']) && ($client['crm_p11d_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> value="on" >
                     </div>
                  </div> -->
                  <?php $p11d_sub_cus_reminder = (!empty($client['crm_p11d_add_custom_reminder'])?$client['crm_p11d_add_custom_reminder']:''); ?>
                  <div class="form-group row radio_bts p11d_add_custom_reminder_label p11d_sort" data-id="<?php echo $Setting_Order['crm_p11d_add_custom_reminder']; ?>"
                    <?php echo $Setting_Delete['crm_p11d_add_custom_reminder']; ?>>
                     <label id="p11d_add_custom_reminder_label" name="p11d_add_custom_reminder_label" class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_p11d_add_custom_reminder']; ?></label>
                     <div class="col-sm-8" id="p11d_add_custom_reminder">
                        <input type="checkbox" class="js-small f-right fields cus_reminder LISTEN_CHANGES clr-check-client" name="p11d_add_custom_reminder" id="" data-id="p11d_cus" data-serid="11" value="<?php echo $p11d_sub_cus_reminder;?>" <?php if($p11d_sub_cus_reminder!=''){ ?> checked <?php } ?>  >
                        <!-- <textarea rows="4" name="p11d_add_custom_reminder" id="p11d_add_custom_reminder" class="form-control fields"><?php if(isset($client['crm_p11d_add_custom_reminder']) && ($client['crm_p11d_add_custom_reminder']!='') ){ echo $client['crm_p11d_add_custom_reminder'];}?></textarea> -->
                        <!-- <a name="p11d_add_custom_reminder" id="p11d_add_custom_reminder" href="<?php echo base_url();?>user/Service_reminder_settings/12" target="_blank">Click to add Custom Reminder</a> -->
                     </div>
                  </div>
                  <?php 
                     if(!empty($client))
                     {
                        $cus_temp = $this->db->query('select id,subject from  custom_service_reminder where client_id='.$client['id'].' and firm_id='.$_SESSION['firm_id'].' and service_id=11')->result_array();
                       foreach ($cus_temp as $key => $value) {
                       
                      ?>
                  <div class="form-group row name_fields" style="">
                     <label class="col-sm-4 col-form-label cus_reminder_subject_<?php echo $value['id']; ?>"><?php echo $value['subject']; ?></label>
                     <div class="col-sm-8">
                        <a href="javascript:;" class="date-editop" data-toggle="modal" id="cus_reminder_<?php echo $value['id']; ?>">Edit</a>
                     </div>
                  </div>
                  <?php } } ?>
               </div>
            </div>
         </div>
      </div>
      <!-- accordion-panel -->
      <input type="hidden" class="UPDATE_LISTENED" name="is_service_data_edited[11]" value="0">      
   </div>
   <!-- p11dtab close -->
  </li>
<!-- p11d close -->

<li class="show-block" style="<?php echo ($tab_on['registered']['tab']!==0  ? "display: block" :"display:none;")?>">
<a href="#" class="toggle">Registered Office</a>
  <div  id="registeredtab" class="masonry-container floating_set inner-views LISTEN_CONTENT">
     <div class="grid-sizer"></div>

        <div class="accordion-panel">
           <div class="box-division03 info-box-div">
              <div class="accordion-heading" role="tab" id="headingOne">
                 <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Registered Office</a>
                 </h3>
              </div>
              <div id="collapse" class="panel-collapse">
                 <div class="basic-info-client1 CONTENT_REGISTERED_OFFICE" id="registeredtab_box">
                    <div class="form-group row help_icon date_birth registered_sort" data-id="<?php echo $Setting_Order['crm_registered_start_date']; ?>" <?php echo $Setting_Delete['crm_registered_start_date']; ?>>
                       <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_registered_start_date']; ?></label>
                       <div class="col-sm-8">
                          <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                          <input class="form-control clr-check-select datepicker-13 fields" name="registered_start_date" id="datepicker33" type="text" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_registered_start_date']) && ($client['crm_registered_start_date']!='') ){ echo Change_Date_Format( $client['crm_registered_start_date'] );}?>"/>
                       </div>
                    </div>
                    <div class="form-group row help_icon date_birth registered_sort" data-id="<?php echo $Setting_Order['crm_registered_renew_date']; ?>">
                       <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_registered_renew_date']; ?>  
                       <span class="Hilight_Required_Feilds">*</span>
                       </label>
                       <div class="col-sm-8">
                          <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                          <input class="form-control clr-check-select datepicker-13 fields LISTEN_CHANGES" name="registered_renew_date" id="datepicker34" type="text" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_registered_renew_date']) && ($client['crm_registered_renew_date']!='') ){ echo Change_Date_Format( $client['crm_registered_renew_date'] );}?>"/>
                       </div>
                    </div>
                    <div class="form-group row registered_sort" data-id="<?php echo $Setting_Order['crm_registered_office_inuse']; ?>" <?php echo $Setting_Delete['crm_registered_office_inuse']; ?>>
                       <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_registered_office_inuse']; ?></label>
                       <div class="col-sm-8">
                          <input type="text" name="registered_office_inuse" id="registered_office_inuse" placeholder="" value="<?php if(isset($client['crm_registered_office_inuse']) && ($client['crm_registered_office_inuse']!='') ){ echo $client['crm_registered_office_inuse'];}?>" class="fields clr-check-client">
                       </div>
                    </div>
                    <div class="form-group row registered_sort" data-id="<?php echo $Setting_Order['crm_registered_claims_note']; ?>" <?php echo $Setting_Delete['crm_registered_claims_note']; ?>>
                       <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_registered_claims_note']; ?></label>
                       <div class="col-sm-8">
                          <textarea rows="3" placeholder="" name="registered_claims_note" id="registered_claims_note" class="fields clr-check-client"><?php if(isset($client['crm_registered_claims_note']) && ($client['crm_registered_claims_note']!='') ){ echo $client['crm_registered_claims_note'];}?></textarea>
                       </div>
                    </div>
                 </div>
              </div>
           </div>
        </div>
        <!-- accordion-panel -->
        <div class="accordion-panel enable_reg" style="<?php echo  ($tab_on['registered']['reminder']!==0 ? "display: block" :"display:none;"); ?>">
           <div class="box-division03 info-box-div">
              <div class="accordion-heading" role="tab" id="headingOne">
                 <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Registered Reminders</a>
                 </h3>
              </div>
              <div id="collapse" class="panel-collapse">
                 <div class="basic-info-client1 CONTENT_REGISTERED_OFFICE_REMINDERS" id="registeredtab_box1">
                    <div class="form-group row  date_birth radio_bts registered_sort" data-id="<?php echo $Setting_Order['crm_registered_next_reminder_date']; ?>" <?php echo $Setting_Delete['crm_registered_next_reminder_date']; ?>>
                       <label class="col-sm-4 col-form-label"><a id="registered_add_custom_reminder_link" name="registered_add_custom_reminder_link" href="<?php echo base_url(); ?>user/Service_reminder_settings/14" target="_blank"><?php echo $Setting_Label['crm_registered_next_reminder_date']; ?></a></label>
                       <div class="col-sm-8">
                          <!--  <input type="hidden" name="registered_next_reminder_date" id="registered_next_reminder_date" placeholder="" value="<?php if(isset($client['crm_registered_next_reminder_date']) && ($client['crm_registered_next_reminder_date']!='') ){ echo $client['crm_registered_next_reminder_date'];}?>" class="fields datepicker"> -->
                          <input type="checkbox" class="js-small f-right fields as_per_firm LISTEN_CHANGES clr-check-client" name="registered_next_reminder_date" id="registered_next_reminder_date" <?php if(isset($client['crm_registered_next_reminder_date']) && ($client['crm_registered_next_reminder_date']!='') ){ echo "checked"; } ?> >
                       </div>
                    </div>
                    <!-- <div class="form-group row radio_bts ">
                       <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                       <div class="col-sm-8">
                          <input type="checkbox" class="js-small f-right fields" name="registered_create_task_reminder" id="registered_create_task_reminder" <?php if(isset($client['crm_registered_create_task_reminder'])  ){?> checked="checked"<?php } ?> value="on" >
                       </div>
                    </div> -->
                    <?php $res_cus_reminder = (!empty($client['crm_registered_add_custom_reminder'])?$client['crm_registered_add_custom_reminder']:'');?>
                    <div class="form-group row radio_bts registered_add_custom_reminder_label registered_sort" data-id="<?php echo $Setting_Order['crm_registered_add_custom_reminder']; ?>" <?php echo $Setting_Delete['crm_registered_add_custom_reminder']; ?>>
                       <label class="col-sm-4 col-form-label" id="registered_add_custom_reminder_label" name="registered_add_custom_reminder_label"><?php echo $Setting_Label['crm_registered_add_custom_reminder']; ?></label>
                       <div class="col-sm-8" id="registered_add_custom_reminder">
                          <input type="checkbox" class="js-small f-right fields cus_reminder LISTEN_CHANGES clr-check-client" name="registered_add_custom_reminder" id="" data-id="registered_cus" data-serid="14" value="<?php  echo $res_cus_reminder;?>" <?php if($res_cus_reminder!=''){ ?> checked <?php } ?>  >
                          <!-- <textarea rows="3" placeholder="" name="registered_add_custom_reminder" id="registered_add_custom_reminder" class="fields"><?php if(isset($client['crm_registered_add_custom_reminder']) && ($client['crm_registered_add_custom_reminder']!='') ){ echo $client['crm_registered_add_custom_reminder'];}?></textarea> -->
                          <!-- <a name="registered_add_custom_reminder" id="registered_add_custom_reminder" href="<?php echo base_url(); ?>user/Service_reminder_settings/9" target="_blank">Click to add Custom Reminder</a> -->
                       </div>
                    </div>
                    <?php 
                       if(!empty($client))
                       {
                          $cus_temp = $this->db->query('select id,subject from  custom_service_reminder where client_id='.$client['id'].' and firm_id='.$_SESSION['firm_id'].' and service_id=14')->result_array();
                         foreach ($cus_temp as $key => $value) {
                         
                        ?>
                    <div class="form-group row name_fields" style="">
                       <label class="col-sm-4 col-form-label cus_reminder_subject_<?php echo $value['id']; ?>"><?php echo $value['subject']; ?></label>
                       <div class="col-sm-8">
                          <a href="javascript:;" class="date-editop" data-toggle="modal" id="cus_reminder_<?php echo $value['id']; ?>">Edit</a>
                       </div>
                    </div>
                    <?php } } ?>
                 </div>
              </div>
           </div>
        </div>
        <!-- accordion-panel -->
      <input type="hidden" class="UPDATE_LISTENED" name="is_service_data_edited[14]" value="0">             
     </div>
     <!-- registeredtab close-->
</li>                     
<!-- taxadvicetab open -->
<li class="show-block" style="<?php echo ($tab_on['taxadvice']['tab']!==0  ? "display: block" :"display:none;");?>">
<a href="#" class="toggle"> Tax Advice/Investigation</a>
<div  id="taxadvicetab" class="masonry-container floating_set inner-views LISTEN_CONTENT">
   <div class="grid-sizer"></div>
   <div class="accordion-panel">
   <div class="box-division03 info-box-div">
      <div class="accordion-heading" role="tab" id="headingOne">
         <h3 class="card-title accordion-title">
            <a class="accordion-msg">Tax Advice/Investigation</a>
         </h3>
      </div>
      <div id="collapse" class="panel-collapse">
         <div class="basic-info-client1 CONTENT_TAX_ADVICE-INVESTIGATION" id="taxadvice_box">
            <div class="form-group row help_icon date_birth taxadvice_sort" data-id="<?php echo $Setting_Order['crm_investigation_start_date']; ?>" <?php echo $Setting_Delete['crm_investigation_start_date']; ?>>
               <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_investigation_start_date']; ?></label>
               <div class="col-sm-8">
                  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                  <input class="form-control clr-check-select datepicker-13 fields" name="investigation_start_date" id="datepicker36" type="text" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_investigation_start_date']) && ($client['crm_investigation_start_date']!='') ){ echo Change_Date_Format( $client['crm_investigation_start_date'] );}?>"/>
               </div>
            </div>
            <div class="form-group row help_icon date_birth taxadvice_sort" data-id="<?php echo $Setting_Order['crm_investigation_end_date']; ?>" >
               <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_investigation_end_date']; ?>
               <span class="Hilight_Required_Feilds">*</span>
               </label>
               <div class="col-sm-8">
                  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                  <input class="form-control clr-check-select datepicker-13 fields LISTEN_CHANGES" name="investigation_end_date" id="datepicker37" type="text" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_investigation_end_date']) && ($client['crm_investigation_end_date']!='') ){ echo Change_Date_Format( $client['crm_investigation_end_date'] );}?>"/>
               </div>
            </div>
            <div class="form-group row taxadvice_sort" data-id="<?php echo $Setting_Order['crm_investigation_note']; ?>" <?php echo $Setting_Delete['crm_investigation_note']; ?>>
               <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_investigation_note']; ?></label>
               <div class="col-sm-8">
                  <textarea rows="3" placeholder="" name="investigation_note" id="investigation_note" class="fields clr-check-client"><?php if(isset($client['crm_investigation_note']) && ($client['crm_investigation_note']!='') ){ echo $client['crm_investigation_note'];}?></textarea>
               </div>
            </div>
         </div>
      </div>
   </div>
   </div>
   <!-- accordion-panel -->
   <div class="accordion-panel enable_taxad" style="<?php echo ($tab_on['taxadvice']['reminder']!==0  ? "display: block" :"display:none;");?>">
   <div class="box-division03 info-box-div">
      <div class="accordion-heading" role="tab" id="headingOne">
         <h3 class="card-title accordion-title">
            <a class="accordion-msg">Tax advice/investigation Reminders</a>
         </h3>
      </div>
      <div id="collapse" class="panel-collapse">
         <div class="basic-info-client1 CONTENT_TAX_ADVICE-INVESTIGATION_REMINDERS" id="taxadvice_box1">
            <div class="form-group row  date_birth radio_bts taxadvice_sort" data-id="<?php echo $Setting_Order['crm_investigation_next_reminder_date']; ?>" <?php echo $Setting_Delete['crm_investigation_next_reminder_date']; ?>>
               <label class="col-sm-4 col-form-label"><a id="investigation_add_custom_reminder_link" name="investigation_add_custom_reminder_link" href="<?php echo base_url(); ?>user/Service_reminder_settings/15" target="_blank"><?php echo $Setting_Label['crm_investigation_next_reminder_date']; ?></a></label>
               <div class="col-sm-8">
                  <!-- <input type="hidden" name="investigation_next_reminder_date" id="investigation_next_reminder_date" placeholder="" value="<?php if(isset($client['crm_investigation_next_reminder_date']) && ($client['crm_investigation_next_reminder_date']!='') ){ echo $client['crm_investigation_next_reminder_date'];}?>" class="fields datepicker"> -->
                  <input type="checkbox" class="js-small f-right fields as_per_firm LISTEN_CHANGES clr-check-client" name="investigation_next_reminder_date" id="investigation_next_reminder_date"  <?php if(isset($client['crm_investigation_next_reminder_date']) && ($client['crm_investigation_next_reminder_date']!='') ){ echo "checked"; } ?>>
               </div>
            </div>
            <!-- <div class="form-group row radio_bts ">
               <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
               <div class="col-sm-8">
                  <input type="checkbox" class="js-small f-right fields" name="investigation_create_task_reminder" id="investigation_create_task_reminder" <?php if(isset($client['crm_investigation_create_task_reminder']) && ($client['crm_investigation_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> value="on" >
               </div>
            </div> -->
            <?php $investigation_cus_reminder = (!empty($client['crm_investigation_add_custom_reminder'])?$client['crm_investigation_add_custom_reminder']:''); ?>
            <div class="form-group row radio_bts investigation_add_custom_reminder_label taxadvice_sort" data-id="<?php echo $Setting_Order['crm_investigation_add_custom_reminder']; ?>" <?php echo $Setting_Delete['crm_investigation_add_custom_reminder']; ?>>
               <label class="col-sm-4 col-form-label" id="investigation_add_custom_reminder_label" name="investigation_add_custom_reminder_label"><?php echo $Setting_Label['crm_investigation_add_custom_reminder']; ?></label>
               <div class="col-sm-8" id="investigation_add_custom_reminder">
                  <input type="checkbox" class="js-small f-right fields cus_reminder LISTEN_CHANGES clr-check-client" name="investigation_add_custom_reminder" id="" data-id="investigation_cus" data-serid="15" value="<?php echo $investigation_cus_reminder;?>" <?php if($investigation_cus_reminder!=''){ ?> checked <?php } ?> >
                  <!--  <textarea rows="3" placeholder="" name="investigation_add_custom_reminder" id="investigation_add_custom_reminder" class="fields"><?php if(isset($client['crm_investigation_add_custom_reminder']) && ($client['crm_investigation_add_custom_reminder']!='') ){ echo $client['crm_investigation_add_custom_reminder'];}?></textarea> -->
                  <!-- <a name="investigation_add_custom_reminder" id="investigation_add_custom_reminder" href="<?php echo base_url(); ?>user/Service_reminder_settings/9" target="_blank">Click to add Custom Reminder</a> -->
               </div>
            </div>
            <?php 
               if(!empty($client)){
                $cus_temp = $this->db->query('select id,subject from  custom_service_reminder where client_id='.$client['id'].' and firm_id='.$_SESSION['firm_id'].' and service_id=15')->result_array();
                 foreach ($cus_temp as $key => $value) {
                 
                ?>
            <div class="form-group row name_fields" style="">
               <label class="col-sm-4 col-form-label cus_reminder_subject_<?php echo $value['id']; ?>"><?php echo $value['subject']; ?></label>
               <div class="col-sm-8">
                  <a href="javascript:;" class="date-editop" data-toggle="modal" id="cus_reminder_<?php echo $value['id']; ?>">Edit</a>
               </div>
            </div>
            <?php } } ?>
         </div>
      </div>
   </div>
   </div>
   <!-- accordion-panel -->
      <input type="hidden" class="UPDATE_LISTENED" name="is_service_data_edited[15]" value="0">                
   </div>
</li>   
   <!-- taxadvicetab close-->

 <!-- vat returns tab start -->
 <li class="show-block" style="<?php echo ($tab_on['vat']['tab']!==0?'display: block':'display: none;'); ?>">
 <a href="#" class="toggle"> VAT Returns </a>


  <div id="vat-Returns" class="masonry-container floating_set inner-views LISTEN_CONTENT">
     <div class="grid-sizer"></div>
     <div class="accordion-panel">
        <div class="box-division03 info-box-div">
           <div class="accordion-heading" role="tab" id="headingOne">
              <h3 class="card-title accordion-title">
                 <a class="accordion-msg">Important Information</a>
              </h3>
           </div>
           <div id="collapse" class="panel-collapse">
              <div class="basic-info-client1 CONTENT_IMPORTANT_INFORMATION" id="vat_box0">
                 <div class="form-group row name_fields vat_sort" data-id="<?php echo $Setting_Order['crm_vat_number_one']; ?>" <?php echo $Setting_Delete['crm_vat_number_one']; ?>>
                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_vat_number_one']; ?></label>
                    <div class="col-sm-8">
                       <input type="number" name="vat_number_one" id="vat_number_one" placeholder="" value="<?php if(isset($client['crm_vat_number_one'])){ echo $client['crm_vat_number_one']; } ?>" class="fields clr-check-select">
                    </div>
                 </div>
              </div>
           </div>
        </div>
     </div>
     <!-- accordion-panel -->
     <div class="accordion-panel">
        <div class="box-division03 info-box-div">
           <div class="accordion-heading" role="tab" id="headingOne">
              <h3 class="card-title accordion-title">
                 <a class="accordion-msg">VAT</a>
              </h3>
           </div>
           <div id="collapse" class="panel-collapse">
              <div class="basic-info-client1 CONTENT_VAT" id="vat_box">
                 <div class="form-group row  date_birth vat_sort" data-id="<?php echo $Setting_Order['crm_vat_date_of_registration']; ?>"
                  <?php echo $Setting_Delete['crm_vat_date_of_registration']; ?>>
                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_vat_date_of_registration']; ?></label>
                    <div class="col-sm-8">
                       <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                       <input class="form-control clr-check-select datepicker-13 fields" type="text" name="vat_registration_date" id="datepicker21" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_vat_date_of_registration']) && ($client['crm_vat_date_of_registration']!='') ){ echo Change_Date_Format( $client['crm_vat_date_of_registration'] );}?>"/>
                    </div>
                 </div>
                 <div class="form-group row vat_sort" data-id="<?php echo $Setting_Order['crm_vat_frequency']; ?>" <?php echo $Setting_Delete['crm_vat_frequency']; ?>>
                    <label class="col-sm-4 col-form-label">
                      <?php echo $Setting_Label['crm_vat_frequency']; ?>
                    </label>
                    <div class="col-sm-8">
                       <select name="vat_frequency" id="vat_frequency" class="form-control fields clr-check-select">
                          <option value="" selected="selected">--Select--</option>
                          <option value="Monthly" <?php if(isset($client['crm_vat_frequency']) && $client['crm_vat_frequency']=='Monthly') {?> selected="selected"<?php } ?>>Monthly</option>
                          <option value="quarterly" <?php if(isset($client['crm_vat_frequency']) && $client['crm_vat_frequency']=='quarterly') {?> selected="selected"<?php } ?>>Quarterly</option>
                          <option value="Yearly" <?php if(isset($client['crm_vat_frequency']) && $client['crm_vat_frequency']=='Yearly') {?> selected="selected"<?php } ?>>Yearly</option>
                       </select>
                    </div>
                 </div>
                 <div class="form-group row help_icon date_birth vat_sort" data-id="<?php echo $Setting_Order['crm_vat_quater_end_date']; ?>" <?php echo $Setting_Delete['crm_vat_quater_end_date']; ?>>
                    <label class="col-sm-4 col-form-label">
                      <?php echo $Setting_Label['crm_vat_quater_end_date']; ?> 
                       <span class="Hilight_Required_Feilds">*</span>                    
                    </label>

                    <div class="col-sm-8">
                       <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                       <input class="form-control clr-check-select datepicker-13 fields LISTEN_CHANGES" name="vat_quater_end_date" id="datepicker22" type="text" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_vat_quater_end_date']) && ($client['crm_vat_quater_end_date']!='') ){ echo Change_Date_Format( $client['crm_vat_quater_end_date'] );}?>"/>
                    </div>
                 </div>
                 <div class="form-group row vat_sort" data-id="<?php echo $Setting_Order['crm_vat_quarters']; ?>">
                    <label class="col-sm-4 col-form-label">
                      <?php echo $Setting_Label['crm_vat_quarters']; ?>
                        
                    <span class="Hilight_Required_Feilds">*</span>
                      </label>
                    <div class="col-sm-8">
                       <select name="vat_quarters" id="vat_quarters" class="form-control fields LISTEN_CHANGES clr-check-select">
                          <option value="">--Select--</option>
                          <option value="Jan/Apr/Jul/Oct" <?php if(isset($client['crm_vat_quarters']) && $client['crm_vat_quarters']=='Jan/Apr/Jul/Oct') {?> selected="selected"<?php } ?>>Jan/Apr/Jul/Oct</option>
                          <option value="Feb/May/Aug/Nov" <?php if(isset($client['crm_vat_quarters']) && $client['crm_vat_quarters']=='Feb/May/Aug/Nov') {?> selected="selected"<?php } ?>>Feb/May/Aug/Nov</option>
                          <option value="Mar/Jun/Sep/Dec" <?php if(isset($client['crm_vat_quarters']) && $client['crm_vat_quarters']=='Mar/Jun/Sep/Dec') {?> selected="selected"<?php } ?>>Mar/Jun/Sep/Dec</option>
                          <option value="Monthly" <?php if(isset($client['crm_vat_quarters']) && $client['crm_vat_quarters']=='Monthly') {?> selected="selected"<?php } ?>>Monthly</option>
                          <option value="Annual End Jan" <?php if(isset($client['crm_vat_quarters']) && $client['crm_vat_quarters']=='Annual End Jan') {?> selected="selected"<?php } ?>>Annual End Jan</option>
                          <option value="Annual End Feb" <?php if(isset($client['crm_vat_quarters']) && $client['crm_vat_quarters']=='Annual End Feb') {?> selected="selected"<?php } ?>>Annual End Feb</option>
                          <option value="Annual End Mar" <?php if(isset($client['crm_vat_quarters']) && $client['crm_vat_quarters']=='Monthly') {?> selected="selected"<?php } ?>>Annual End Mar</option>
                          <option value="Annual End Apr" <?php if(isset($client['crm_vat_quarters']) && $client['crm_vat_quarters']=='Annual End Apr') {?> selected="selected"<?php } ?>>Annual End Apr</option>
                          <option value="Annual End May" <?php if(isset($client['crm_vat_quarters']) && $client['crm_vat_quarters']=='Annual End May') {?> selected="selected"<?php } ?>>Annual End May</option>
                          <option value="Annual End Jun" <?php if(isset($client['crm_vat_quarters']) && $client['crm_vat_quarters']=='Annual End Jun') {?> selected="selected"<?php } ?>>Annual End Jun</option>
                          <option value="Annual End Jul" <?php if(isset($client['crm_vat_quarters']) && $client['crm_vat_quarters']=='Annual End Jul') {?> selected="selected"<?php } ?>>Annual End Jul</option>
                          <option value="Annual End Aug" <?php if(isset($client['crm_vat_quarters']) && $client['crm_vat_quarters']=='Annual End Aug') {?> selected="selected"<?php } ?>>Annual End Aug</option>
                          <option value="Annual End Sep" <?php if(isset($client['crm_vat_quarters']) && $client['crm_vat_quarters']=='Annual End Sep') {?> selected="selected"<?php } ?>>Annual End Sep</option>
                          <option value="Annual End Oct" <?php if(isset($client['crm_vat_quarters']) && $client['crm_vat_quarters']=='Annual End Oct') {?> selected="selected"<?php } ?>>Annual End Oct</option>
                          <option value="Annual End Nov" <?php if(isset($client['crm_vat_quarters']) && $client['crm_vat_quarters']=='Annual End Nov') {?> selected="selected"<?php } ?>>Annual End Nov</option>
                          <option value="Annual End Dec" <?php if(isset($client['crm_vat_quarters']) && $client['crm_vat_quarters']=='Annual End Dec') {?> selected="selected"<?php } ?>>Annual End Dec</option>
                       </select>
                    </div>
                 </div>
                 <div class="form-group row help_icon date_birth vat_sort" data-id="<?php echo $Setting_Order['crm_last_vat_return_filed_upto']; ?>" <?php echo $Setting_Delete['crm_last_vat_return_filed_upto']; ?>>
                    <label class="col-sm-4 col-form-label">
                      <?php echo $Setting_Label['crm_last_vat_return_filed_upto']; ?>                      
                    </label>
                    <div class="col-sm-8">
                       <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                       <input class="form-control clr-check-select datepicker-13 fields" name="crm_last_vat_return_filed_upto" id="datepicker24" type="text" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_last_vat_return_filed_upto']) && ($client['crm_last_vat_return_filed_upto']!='') ){ echo Change_Date_Format( $client['crm_last_vat_return_filed_upto'] );}?>"/>
                    </div>
                 </div>
                 <div class="form-group row vat_sort" data-id="<?php echo $Setting_Order['crm_vat_scheme']; ?>"
                  <?php echo $Setting_Delete['crm_vat_scheme']; ?>>
                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_vat_scheme']; ?></label>
                    <div class="col-sm-8">
                       <select name="vat_scheme" id="vat_scheme" class="form-control fields clr-check-select">
                          <option value="">--Select--</option>
                          <option value="Standard Rate" <?php if(isset($client['crm_vat_scheme']) && $client['crm_vat_scheme']=='Standard Rate') {?> selected="selected"<?php } ?>>Standard Rate</option>
                          <option value="Flat Rate Scheme" <?php if(isset($client['crm_vat_scheme']) && $client['crm_vat_scheme']=='Flat Rate Scheme') {?> selected="selected"<?php } ?>>Flat Rate Scheme</option>
                          <option value="Marginal Scheme" <?php if(isset($client['crm_vat_scheme']) && $client['crm_vat_scheme']=='Marginal Scheme') {?> selected="selected"<?php } ?>>Marginal Scheme</option>
                       </select>
                    </div>
                 </div>
                 <div class="form-group row vat_sort" data-id="<?php echo $Setting_Order['crm_flat_rate_category']; ?>" <?php echo $Setting_Delete['crm_flat_rate_category']; ?>>
                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_flat_rate_category']; ?></label>
                    <div class="col-sm-8">
                       <input type="text" name="flat_rate_category" id="flat_rate_category" placeholder="" value="<?php if(isset($client['crm_flat_rate_category']) && ($client['crm_flat_rate_category']!='') ){ echo $client['crm_flat_rate_category'];}?>" class="fields clr-check-client">
                    </div>
                 </div>
                 <div class="form-group row vat_sort" data-id="<?php echo $Setting_Order['crm_flat_rate_percentage']; ?>"
                  <?php echo $Setting_Delete['crm_flat_rate_percentage']; ?>>
                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_flat_rate_percentage']; ?></label>
                    <div class="col-sm-8">
                       <input type="number" name="flat_rate_percentage" id="flat_rate_percentage" placeholder="" value="<?php if(isset($client['crm_flat_rate_percentage']) && ($client['crm_flat_rate_percentage']!='') ){ echo $client['crm_flat_rate_percentage'];}?>" class="fields clr-check-select">
                    </div>
                 </div>
                 <div class="form-group row name_fields vat_sort radio_bts" data-id="<?php echo $Setting_Order['crm_direct_debit']; ?>" <?php echo $Setting_Delete['crm_direct_debit']; ?>>
                    <label class="col-sm-4 col-form-label">
                      <?php echo $Setting_Label['crm_direct_debit']; ?>
                    </label>
                    <div class="col-sm-8">
                       <input type="checkbox" class="js-small f-right fields clr-check-client" name="direct_debit" id="direct_debit" <?php if(isset($client['crm_direct_debit']) && ($client['crm_direct_debit']=='on') ){?> checked="checked"<?php } ?>>
                    </div>
                 </div>
                 <div class="form-group row name_fields vat_sort radio_bts" data-id="<?php echo $Setting_Order['crm_annual_accounting_scheme']; ?>" <?php echo $Setting_Delete['crm_annual_accounting_scheme']; ?>>
                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_annual_accounting_scheme']; ?></label>
                    <div class="col-sm-8">
                       <input type="checkbox" class="js-small f-right fields clr-check-client" name="annual_accounting_scheme" id="annual_accounting_scheme" <?php if(isset($client['crm_annual_accounting_scheme']) && ($client['crm_annual_accounting_scheme']=='on') ){?> checked="checked"<?php } ?>>
                    </div>
                 </div>
                 <div class="form-group row vat_sort" data-id="<?php echo $Setting_Order['crm_box5_of_last_quarter_submitted']; ?>" 
                  <?php echo $Setting_Delete['crm_box5_of_last_quarter_submitted']; ?>>
                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_box5_of_last_quarter_submitted']; ?></label>
                    <div class="col-sm-8">
                       <!-- <input type="text" name="box5_of_last_quarter_submitted" id="box5_of_last_quarter_submitted" value="<?php if(isset($client['crm_box5_of_last_quarter_submitted']) && ($client['crm_box5_of_last_quarter_submitted']!='') ){ echo $client['crm_box5_of_last_quarter_submitted'];}?>" class="fields"> -->
                       <select name="box5_of_last_quarter_submitted" id="box5_of_last_quarter_submitted" class="form-control fields clr-check-select">
                          <option value="">--Select--</option>
                          <option value="01" <?php if(isset($client['crm_box5_of_last_quarter_submitted']) && $client['crm_box5_of_last_quarter_submitted']=='01') {?> selected="selected"<?php } ?>>01</option>
                          <option value="02" <?php if(isset($client['crm_box5_of_last_quarter_submitted']) && $client['crm_box5_of_last_quarter_submitted']=='02') {?> selected="selected"<?php } ?>>02</option>
                          <option value="03" <?php if(isset($client['crm_box5_of_last_quarter_submitted']) && $client['crm_box5_of_last_quarter_submitted']=='03') {?> selected="selected"<?php } ?>>03</option>
                          <option value="04" <?php if(isset($client['crm_box5_of_last_quarter_submitted']) && $client['crm_box5_of_last_quarter_submitted']=='04') {?> selected="selected"<?php } ?>>04</option>
                          <option value="05" <?php if(isset($client['crm_box5_of_last_quarter_submitted']) && $client['crm_box5_of_last_quarter_submitted']=='05') {?> selected="selected"<?php } ?>>05</option>
                          <option value="06" <?php if(isset($client['crm_box5_of_last_quarter_submitted']) && $client['crm_box5_of_last_quarter_submitted']=='06') {?> selected="selected"<?php } ?>>06</option>
                          <option value="07" <?php if(isset($client['crm_box5_of_last_quarter_submitted']) && $client['crm_box5_of_last_quarter_submitted']=='07') {?> selected="selected"<?php } ?>>07</option>
                          <option value="08" <?php if(isset($client['crm_box5_of_last_quarter_submitted']) && $client['crm_box5_of_last_quarter_submitted']=='08') {?> selected="selected"<?php } ?>>08</option>
                          <option value="09" <?php if(isset($client['crm_box5_of_last_quarter_submitted']) && $client['crm_box5_of_last_quarter_submitted']=='09') {?> selected="selected"<?php } ?>>09</option>
                          <option value="10" <?php if(isset($client['crm_box5_of_last_quarter_submitted']) && $client['crm_box5_of_last_quarter_submitted']=='10') {?> selected="selected"<?php } ?>>10</option>
                          <option value="11" <?php if(isset($client['crm_box5_of_last_quarter_submitted']) && $client['crm_box5_of_last_quarter_submitted']=='11') {?> selected="selected"<?php } ?>>11</option>
                          <option value="12" <?php if(isset($client['crm_box5_of_last_quarter_submitted']) && $client['crm_box5_of_last_quarter_submitted']=='12') {?> selected="selected"<?php } ?>>12</option>
                       </select>
                    </div>
                 </div>
                 <div class="form-group row vat_sort" data-id="<?php echo $Setting_Order['crm_vat_address']; ?>"
                  <?php echo $Setting_Delete['crm_vat_address']; ?>>
                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_vat_address']; ?></label>
                    <div class="col-sm-8">
                       <textarea rows="3" placeholder="" class="fields clr-check-client" name="vat_address" id="vat_address"><?php if(isset($client['crm_vat_address']) && ($client['crm_vat_address']!='') ){ echo $client['crm_vat_address'];}?></textarea>
                    </div>
                 </div>
              </div>
           </div>
        </div>
     </div>
     <!-- accordion-panel -->
     <div class="accordion-panel enable_vat" style="<?php echo ($tab_on['vat']['reminder']!==0?'display: block':'display: none;');?>">
        <div class="box-division03 info-box-div">
           <div class="accordion-heading" role="tab" id="headingOne">
              <h3 class="card-title accordion-title">
                 <a class="accordion-msg">VAT Reminders</a>
              </h3>
           </div>
           <div id="collapse" class="panel-collapse">
              <div class="basic-info-client1 CONTENT_VAT_REMINDERS" id="vat_box1">
                 <div class="form-group row  date_birth radio_bts vat_sort" data-id="<?php echo $Setting_Order['crm_vat_next_reminder_date']; ?>" <?php echo $Setting_Delete['crm_vat_next_reminder_date']; ?>>
                    <label class="col-sm-4 col-form-label"><a id="vat_add_custom_reminder_link" name="vat_add_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/5" target="_blank"><?php echo $Setting_Label['crm_vat_next_reminder_date']; ?></a></label>
                    <div class="col-sm-8">
                       <!-- <input type="hidden" name="vat_next_reminder_date" id="vat_next_reminder_date" placeholder="" value="<?php if(isset($client['crm_vat_next_reminder_date']) && ($client['crm_vat_next_reminder_date']!='') ){ echo $client['crm_vat_next_reminder_date'];}?>" class="fields datepicker"> -->
                       <input type="checkbox" class="js-small f-right fields as_per_firm LISTEN_CHANGES clr-check-client" name="vat_next_reminder_date" id="vat_next_reminder_date"  <?php if(isset($client['crm_vat_next_reminder_date']) && ($client['crm_vat_next_reminder_date']=='on') ){?> checked="checked"<?php } ?>>
                    </div>
                 </div>
        
                 <?php $vat_cus_reminder = (!empty($client['crm_vat_add_custom_reminder'])?$client['crm_vat_add_custom_reminder']:'');?>

                 <div class="form-group row radio_bts vat_add_custom_reminder_label  vat_sort" data-id="<?php echo $Setting_Order['crm_vat_add_custom_reminder']; ?>" <?php echo $Setting_Delete['crm_vat_add_custom_reminder']; ?>>
                    <label id="vat_add_custom_reminder_label" name="vat_add_custom_reminder_label" class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_vat_add_custom_reminder']; ?></label>
                    <div class="col-sm-8" id="vat_add_custom_reminder">
                       <input type="checkbox" class="js-small f-right fields cus_reminder LISTEN_CHANGES clr-check-client" name="vat_add_custom_reminder" id="" data-id="vat_cus" data-serid="5" value="<?php echo $vat_cus_reminder?>" <?php if($vat_cus_reminder!=''){ ?> checked <?php } ?>>
                       <!-- <textarea rows="3" placeholder="" name="vat_add_custom_reminder" id="vat_add_custom_reminder" class="fields"><?php if(isset($client['crm_vat_add_custom_reminder']) && ($client['crm_vat_add_custom_reminder']!='') ){ echo $client['crm_vat_add_custom_reminder'];}?></textarea> -->
                       <!-- <a name="vat_add_custom_reminder" id="vat_add_custom_reminder" href="<?php echo base_url();?>user/Service_reminder_settings/15" target="_blank">Click to add Custom Reminder</a> -->
                    </div>
                 </div>
                 <?php 
                    if(!empty($client))
                    {
                      $cus_temp = $this->db->query('select id,subject from  custom_service_reminder where client_id='.$client['id'].' and firm_id='.$_SESSION['firm_id'].' and service_id=5')->result_array();
                      foreach ($cus_temp as $key => $value) {
                      
                     ?>
                 <div class="form-group row name_fields" style="">
                    <label class="col-sm-4 col-form-label cus_reminder_subject_<?php echo $value['id']; ?>"><?php echo $value['subject']; ?></label>
                    <div class="col-sm-8">
                       <a href="javascript:;" class="date-editop" data-toggle="modal" id="cus_reminder_<?php echo $value['id']; ?>">Edit</a>
                    </div>
                 </div>
                 <?php } } ?>
              </div>
           </div>
        </div>
     </div>
      <input type="hidden" class="UPDATE_LISTENED" name="is_service_data_edited[5]" value="0">                   
  </div>
<!-- VAT Returns -->
</li>
 <!-- vat returns tab end -->

<!-- vat returns tab start -->
 <li class="show-block" style="<?php echo ($tab_on['workplace']['tab']!==0?'display: block':'display: none;'); ?>">
 <a href="#" class="toggle"> WorkPlace Pension - AE</a>
   <div id="worktab" class="masonry-container floating_set inner-views LISTEN_CONTENT">
     <div class="grid-sizer"></div>
   <div class="accordion-panel">
      <div class="box-division03 info-box-div">
         <div class="accordion-heading" role="tab" id="headingOne">
            <h3 class="card-title accordion-title">
               <a class="accordion-msg">WorkPlace Pension - AE       </a>
            </h3>
         </div>
         <div id="collapse" class="panel-collapse">
            <div class="basic-info-client1 CONTENT_WORKPLACE_PENSION_AE" id="workplace_sec">
               <div class="form-group row  date_birth workplace_sort" data-id="<?php echo $Setting_Order['crm_staging_date']; ?>" <?php echo $Setting_Delete['crm_staging_date']; ?>>
                  <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_staging_date']; ?></label>
                  <div class="col-sm-8">
                     <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                     <input class="form-control clr-check-select datepicker-13 fields" type="text" name="staging_date" id="datepicker6" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_staging_date']) && ($client['crm_staging_date']!='') ){ echo Change_Date_Format( $client['crm_staging_date'] );}?>"/>
                  </div>
               </div>
               <div class="form-group row workplace_sort" data-id="<?php echo $Setting_Order['crm_pension_id']; ?>"
                <?php echo $Setting_Delete['crm_pension_id']; ?>>
                  <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_pension_id']; ?></label>
                  <div class="col-sm-8">
                     <input type="text" name="pension_id" id="pension_id" value="<?php if(isset($client['crm_pension_id']) && ($client['crm_pension_id']!='') ){ echo $client['crm_pension_id'];}?>" class="fields clr-check-client">
                  </div>
               </div>
               <div class="form-group row  date_birth workplace_sort" data-id="<?php echo $Setting_Order['crm_pension_subm_due_date']; ?>">
                  <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_pension_subm_due_date']; ?>
                    
                                       <span class="Hilight_Required_Feilds">*</span>
                  </label>
                  <div class="col-sm-8">
                     <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                     <input class="form-control clr-check-select datepicker-13 fields LISTEN_CHANGES" type="text" name="pension_subm_due_date" id="datepicker7" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_pension_subm_due_date']) && ($client['crm_pension_subm_due_date']!='') ){ echo Change_Date_Format( $client['crm_pension_subm_due_date'] );}?>"/>
                  </div>
               </div>
               <div class="form-group row  date_birth workplace_sort" data-id="<?php echo $Setting_Order['crm_postponement_date']; ?>"
                <?php echo $Setting_Delete['crm_postponement_date']; ?>>
                  <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_postponement_date']; ?></label>
                  <div class="col-sm-8">
                     <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                     <input class="form-control clr-check-select datepicker-13 fields" type="text" name="defer_post_upto" id="datepicker8" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_postponement_date']) && ($client['crm_postponement_date']!='') ){ echo Change_Date_Format( $client['crm_postponement_date'] );}?>"/>
                  </div>
               </div>
               <div class="form-group row  date_birth workplace_sort" data-id="<?php echo $Setting_Order['crm_the_pensions_regulator_opt_out_date']; ?>" <?php echo $Setting_Delete['crm_the_pensions_regulator_opt_out_date']; ?>>
                  <label class="col-sm-4 col-form-label">
                    <?php echo $Setting_Label['crm_the_pensions_regulator_opt_out_date']; ?>
                  </label>
                  <div class="col-sm-8">
                     <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                     <input class="form-control clr-check-select datepicker-13 fields" type="text" name="pension_regulator_date" id="datepicker9" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_the_pensions_regulator_opt_out_date']) && ($client['crm_the_pensions_regulator_opt_out_date']!='') ){ echo Change_Date_Format ( $client['crm_the_pensions_regulator_opt_out_date'] );}?>"/>
                  </div>
               </div>
               <div class="form-group row  date_birth workplace_sort" data-id="<?php echo $Setting_Order['crm_re_enrolment_date']; ?>" <?php echo $Setting_Delete['crm_re_enrolment_date']; ?>>
                  <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_re_enrolment_date']; ?></label>
                  <div class="col-sm-8">
                     <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                     <input class="form-control clr-check-select datepicker-13 fields" type="text" name="paye_re_enrolment_date" id="datepicker10" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_re_enrolment_date']) && ($client['crm_re_enrolment_date']!='') ){ echo Change_Date_Format( $client['crm_re_enrolment_date'] );}?>"/>
                  </div>
               </div>
               <div class="form-group row  date_birth workplace_sort" data-id="<?php echo $Setting_Order['crm_declaration_of_compliance_due_date']; ?>" <?php echo $Setting_Delete['crm_declaration_of_compliance_due_date']; ?>>
                  <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_declaration_of_compliance_due_date']; ?></label>
                  <div class="col-sm-8">
                     <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                     <input class="form-control clr-check-select datepicker-13 fields" type="text" name="declaration_of_compliance_due_date" id="datepicker11" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_declaration_of_compliance_due_date']) && ($client['crm_declaration_of_compliance_due_date']!='') ){ echo Change_Date_Format( $client['crm_declaration_of_compliance_due_date'] );}?>"/>
                  </div>
               </div>
               <div class="form-group row  date_birth workplace_sort" data-id="<?php echo $Setting_Order['crm_declaration_of_compliance_submission']; ?>" <?php echo $Setting_Delete['crm_declaration_of_compliance_submission']; ?>>
                  <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_declaration_of_compliance_submission']; ?></label>
                  <div class="col-sm-8">
                     <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                     <input class="form-control clr-check-select datepicker-13 fields" type="text" name="declaration_of_compliance_last_filed" id="datepicker12" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_declaration_of_compliance_submission']) && ($client['crm_declaration_of_compliance_submission']!='') ){ echo Change_Date_Format( $client['crm_declaration_of_compliance_submission'] );}?>"/>
                  </div>
               </div>
               <div class="form-group row workplace_sort" data-id="<?php echo $Setting_Order['crm_paye_pension_provider']; ?>"
                <?php echo $Setting_Delete['crm_paye_pension_provider']; ?>>
                  <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_paye_pension_provider']; ?></label>
                  <div class="col-sm-8">
                     <input type="text" name="paye_pension_provider" id="paye_pension_provider" value="<?php if(isset($client['crm_paye_pension_provider']) && ($client['crm_paye_pension_provider']!='') ){ echo $client['crm_paye_pension_provider'];}?>" class="fields clr-check-client">
                  </div>
               </div>
              <!--  <div class="form-group row workplace_sort" data-id="<?php echo $Setting_Order['crm_pension_id']; ?>"
                <?php echo $Setting_Delete['crm_pension_id']; ?>>
                  <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_pension_id']; ?></label>
                  <div class="col-sm-8">
                     <input type="text" name="paye_pension_provider_userid" id="paye_pension_provider_userid" value="<?php if(isset($client['crm_pension_id']) && ($client['crm_pension_id']!='') ){ echo $client['crm_pension_id'];}?>" class="fields">
                  </div>
               </div> -->
               <div class="form-group row workplace_sort" data-id="<?php echo $Setting_Order['crm_paye_pension_provider_password']; ?>" <?php echo $Setting_Delete['crm_paye_pension_provider_password']; ?>>
                  <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_paye_pension_provider_password']; ?></label>
                  <div class="col-sm-8">
                     <input type="password" name="paye_pension_provider_password" id="paye_pension_provider_password" value="<?php if(isset($client['crm_paye_pension_provider_password']) && ($client['crm_paye_pension_provider_password']!='') ){ echo $client['crm_paye_pension_provider_password'];}?>" class="fields clr-check-client">
                  </div>
               </div>
               <div class="form-group row workplace_sort" data-id="<?php echo $Setting_Order['crm_employer_contri_percentage']; ?>"
                <?php echo $Setting_Delete['crm_employer_contri_percentage']; ?>>
                  <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_employer_contri_percentage']; ?></label>
                  <div class="col-sm-8">
                     <input type="number" name="employer_contri_percentage" id="employer_contri_percentage" value="<?php if(isset($client['crm_employer_contri_percentage']) && ($client['crm_employer_contri_percentage']!='') ){ echo $client['crm_employer_contri_percentage'];}?>" class="fields clr-check-select">
                  </div>
               </div>
               <div class="form-group row workplace_sort" data-id="<?php echo $Setting_Order['crm_employee_contri_percentage']; ?>"
                <?php echo $Setting_Delete['crm_employee_contri_percentage']; ?>>
                  <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_employee_contri_percentage']; ?></label>
                  <div class="col-sm-8">
                     <input type="number" name="employee_contri_percentage" id="employee_contri_percentage" value="<?php if(isset($client['crm_employee_contri_percentage']) && ($client['crm_employee_contri_percentage']!='') ){ echo $client['crm_employee_contri_percentage'];}?>" class="fields clr-check-select">
                  </div>
               </div>
               <div class="form-group row workplace_sort" data-id="<?php echo $Setting_Order['crm_pension_notes']; ?>"
                <?php echo $Setting_Delete['crm_pension_notes']; ?>>
                  <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_pension_notes']; ?></label>
                  <div class="col-sm-8">
                     <textarea rows="3" placeholder="" name="pension_notes" id="pension_notes" class="fields clr-check-client"><?php if(isset($client['crm_pension_notes']) && ($client['crm_pension_notes']!='') ){ echo $client['crm_pension_notes'];}?></textarea>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- accordion-panel -->
   <div class="accordion-panel enable_work" style="<?php echo ($tab_on['workplace']['reminder']!==0?'display: block':'display: none;');?>">
      <div class="box-division03 info-box-div">
         <div class="accordion-heading" role="tab" id="headingOne">
            <h3 class="card-title accordion-title">
               <a class="accordion-msg">Pension Reminders</a>
            </h3>
         </div>
         <div id="collapse" class="panel-collapse">
            <div class="basic-info-client1 CONTENT_WORKPLACE_PENSION_AE_REMINDERS" id="workplace_sec1">
               <div class="form-group row radio_bts date_birth workplace_sort" data-id="<?php echo $Setting_Order['crm_pension_next_reminder_date']; ?>"
                <?php echo $Setting_Delete['crm_pension_next_reminder_date']; ?>>
                  <label class="col-sm-4 col-form-label"><a id="pension_create_task_reminder_link" name="pension_create_task_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/7" target="_blank"><?php echo $Setting_Label['crm_pension_next_reminder_date']; ?></a></label>
                  <div class="col-sm-8">
                     <!-- <input type="hidden" name="pension_next_reminder_date" id="pension_next_reminder_date" placeholder="How many years" value="<?php if(isset($client['crm_pension_next_reminder_date']) && ($client['crm_pension_next_reminder_date']!='') ){ echo $client['crm_pension_next_reminder_date'];}?>" class="fields datepicker"> -->
                     <input type="checkbox" class="js-small f-right fields as_per_firm LISTEN_CHANGES clr-check-client" name="pension_next_reminder_date" id="Pension_next_reminder_date"   <?php if(isset($client['crm_pension_next_reminder_date']) && ($client['crm_pension_next_reminder_date']!='') ){ echo "checked"; } ?> >
                  </div>
               </div>
              <!--  <div class="form-group row radio_bts workplace" data-id="<?php echo $Setting_Order['crm_pension_create_task_reminder']; ?>">
                  <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                  <div class="col-sm-8">
                     <input type="checkbox" class="js-small f-right fields" name="pension_create_task_reminder" id="pension_create_task_reminder" <?php if(isset($client['crm_pension_create_task_reminder']) && ($client['crm_pension_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> value="on" >
                  </div>
               </div> -->
               <?php $work_pension_cus_reminder = (!empty($client['crm_pension_add_custom_reminder'])?$client['crm_pension_add_custom_reminder']:''); ?>

               <div class="form-group row radio_bts pension_create_task_reminder_label workplace_sort" data-id="<?php echo $Setting_Order['crm_pension_add_custom_reminder']; ?>" <?php echo $Setting_Delete['crm_pension_add_custom_reminder']; ?>>
                  <label id="pension_create_task_reminder_label" name="pension_create_task_reminder_label" class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_pension_add_custom_reminder']; ?></label>
                  <div class="col-sm-8" id="pension_add_custom_reminder">
                     <input type="checkbox" class="js-small f-right fields cus_reminder LISTEN_CHANGES clr-check-client" name="pension_add_custom_reminder" id="" data-id="pension_cus" data-serid="7" value="<?php echo $work_pension_cus_reminder;?>" <?php if($work_pension_cus_reminder!=''){ ?> checked <?php } ?> >
                     <!-- <textarea rows="4" name="pension_add_custom_reminder" id="pension_add_custom_reminder" class="form-control fields"><?php if(isset($client['crm_pension_add_custom_reminder']) && ($client['crm_pension_add_custom_reminder']!='') ){ echo $client['crm_pension_add_custom_reminder'];}?></textarea> -->
                     <!-- <a name="pension_add_custom_reminder" id="pension_add_custom_reminder" href="<?php echo base_url();?>user/Service_reminder_settings/8" target="_blank">Click to add Custom Reminder</a> -->
                  </div>
               </div>
               <?php 
                  if(!empty($client)){
                     $cus_temp = $this->db->query('select id,subject from  custom_service_reminder where client_id='.$client['id'].' and firm_id='.$_SESSION['firm_id'].' and service_id=7')->result_array();
                    foreach ($cus_temp as $key => $value) {
                    
                   ?>
               <div class="form-group row name_fields" style="">
                  <label class="col-sm-4 col-form-label cus_reminder_subject_<?php echo $value['id']; ?>"><?php echo $value['subject']; ?></label>
                  <div class="col-sm-8">
                     <a href="javascript:;" class="date-editop" data-toggle="modal" id="cus_reminder_<?php echo $value['id']; ?>">Edit</a>
                  </div>
               </div>
               <?php } } ?>
            </div>
         </div>
      </div>
   </div>
   <!-- accordion-panel -->
      <input type="hidden" class="UPDATE_LISTENED " name="is_service_data_edited[7]" value="0">                      
</div>
<!--worktab close-->
</li>
<?php 
  $cnt = 1;
  foreach ($contactRec as $key => $contactRec_val) {
    $ser_reminder                 =   ( $contactRec_val['has_ptr_reminder'] == 1 ?"block":"none" );
    $has_property_income          =   ( $contactRec_val['property_income']==1?"checked='checked'":'' );
    $has_additional_income        =   ( $contactRec_val['additional_income']==1?"checked='checked'":'' );
    $has_additional_income_note   =   ( $contactRec_val['additional_income']==1?"block":'none' );
    $is_as_per_firm               =   ( $contactRec_val['is_ptr_as_per_firm_reminder']==1?"checked='checked'":'' );               
    $is_cus_reminder              =   ( $contactRec_val['is_ptr_cus_reminder']!=0?"checked='checked'":'' );               
  ?>
<li class="show-block" style="display:<?php echo ($contactRec_val['has_ptr_service'] == 1?'block':'none');?>" >
<a href="#" class="toggle">Personal Tax Return 
    <b class="ccp_name_<?php echo $cnt;?>"> - <?php echo $contactRec_val['first_name'].' '.$contactRec_val['last_name'];?></b>
      </a>
<!-- personal-tax-returns-->               
  <div id="ccp_<?php echo $cnt;?>_personal_tax_returns" class="ccp_ptr_cnt masonry-container floating_set inner-views LISTEN_CONTENT">
     <div class="grid-sizer"></div>
     <div class="accordion-panel">
        <div class="box-division03 info-box-div">
           <div class="accordion-heading" role="tab" id="headingOne">
              <h3 class="card-title accordion-title">
                 <a class="accordion-msg">Important Information</a>
              </h3>
           </div>
           <div id="collapse" class="panel-collapse">
              <div class="basic-info-client1 CONTENT_IMPORTANT_INFORMATION" id="personal_box">
                  <input type="hidden" class="client_contacts_id"  value="<?php echo $contactRec_val['id']; ?>">
                 <div class="form-group row personal_sort" data-id="<?php echo $Setting_Order['crm_personal_utr_number']; ?>"
                   <?php echo $Setting_Delete['crm_personal_utr_number']; ?>>
                     <label class="col-sm-4 col-form-label">
                        <?php echo $Setting_Label['crm_personal_utr_number']; ?>                        
                     </label>
                    <div class="col-sm-8">
                       <input type="text" name="ccp_personal_utr_number[<?php echo $cnt;?>]" class="form-control fields clr-check-client" value="<?php echo $contactRec_val['utr_number']; ?>">
                    </div>
                 </div>
                 <div class="form-group row personal_sort" data-id="<?php echo $Setting_Order['crm_ni_number']; ?>"
                  <?php echo $Setting_Delete['crm_ni_number']; ?>>
                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_ni_number']; ?></label>
                    <div class="col-sm-8">
                       <input type="text" name="ccp_ni_number[<?php echo $cnt;?>]" class="form-control fields clr-check-client" value="<?php echo $contactRec_val['ni_number']; ?>">
                    </div>
                 </div>
                 <div class="form-group row radio_bts personal_sort" data-id="<?php echo $Setting_Order['crm_property_income']; ?>" <?php echo $Setting_Delete['crm_property_income']; ?>>
                    <label class="col-sm-4 col-form-label">
                      <?php echo $Setting_Label['crm_property_income']; ?>
                    </label>
                    <div class="col-sm-8">
                       <input type="checkbox" class="js-small f-right fields clr-check-client" name="ccp_property_income[<?php echo $cnt;?>]" data-id="property_income" value="1" <?php echo $has_property_income;?> >
                    </div>
                 </div>
                 <div class="form-group row radio_bts personal_sort" data-id="<?php echo $Setting_Order['crm_additional_income']; ?>" <?php echo $Setting_Delete['crm_additional_income']; ?>>
                    <label class="col-sm-4 col-form-label">
                      <?php echo $Setting_Label['crm_additional_income']; ?>
                    </label>
                    <div class="col-sm-8">
                       <input type="checkbox" class="js-small f-right fields clr-check-client" name="ccp_additional_income[<?php echo $cnt;?>]" value="1"
                        <?php echo $has_additional_income;?> >
                    </div>
                 </div>

                  <div class="form-group row text-box1 personal_sort property_income_notes" data-id="<?php echo $Setting_Order['property_income_notes']; ?>" style="display:<?php echo $has_additional_income_note;?> ">
                   <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['property_income_notes']; ?> </label>
                   <div class="col-sm-8">
                      <textarea rows="3" placeholder="" name="ccp_property_income_notes[<?php echo $cnt;?>]"  class="fields clr-check-client"><?php echo                $contactRec_val['property_income_notes'];?>                              
                     </textarea>
                   </div>
                  </div>
              </div>
           </div>
        </div>
     </div>
     <!-- accordion-panel -->
     <div class="accordion-panel">
        <div class="box-division03 info-box-div">
           <div class="accordion-heading" role="tab" id="headingOne">
              <h3 class="card-title accordion-title">
                 <a class="accordion-msg">Personal Tax Return</a>
              </h3>
           </div>
           <div id="collapse" class="panel-collapse">
              <div class="basic-info-client1 CONTENT_PERSONAL_TAX_RETURN" id="personal_box1">
                 <div class="form-group row name_fields date_birth personal_sort" data-id="<?php echo $Setting_Order['crm_personal_tax_return_date']; ?>">
                    <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_personal_tax_return_date']; ?>                                      
                    <span class="Hilight_Required_Feilds">*</span>
                    </label>
                    <div class="col-sm-8">
                       <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>  <input type="text" name="ccp_personal_tax_return_date[<?php echo $cnt;?>]"  placeholder="dd-mm-yyyy"  class="datepicker-13 fields clr-check-select LISTEN_CHANGES" data-cntid="<?php echo $cnt;?>" value="<?php echo Change_Date_Format($contactRec_val['personal_tax_return_date']); ?>">
                    </div>
                 </div>
                 <div class="form-group row name_fields date_birth personal_sort" data-id="<?php echo $Setting_Order['crm_personal_due_date_return']; ?>" <?php echo $Setting_Delete['crm_personal_due_date_return']; ?>>
                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_personal_due_date_return']; ?></label>
                    <div class="col-sm-8">
                       <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>  <input type="text" name="ccp_personal_due_date_return[<?php echo $cnt;?>]" placeholder="dd-mm-yyyy"  class="fields clr-check-select datepicker-13" value="<?php echo Change_Date_Format($contactRec_val['personal_due_date_return']); ?>">
                    </div>
                 </div>
                 <div class="form-group row name_fields date_birth personal_sort" data-id="<?php echo $Setting_Order['crm_personal_due_date_online']; ?>" <?php echo $Setting_Delete['crm_personal_due_date_online']; ?>>
                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_personal_due_date_online']; ?></label>
                    <div class="col-sm-8">
                       <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>   <input type="text" name="ccp_personal_due_date_online[<?php echo $cnt;?>]" placeholder="dd-mm-yyyy" class="fields clr-check-select datepicker-13" value="<?php echo Change_Date_Format($contactRec_val['personal_due_date_online']); ?>">
                    </div>
                 </div>
              </div>
           </div>
        </div>
     </div>
     <!-- accordion-panel -->
     <div class="accordion-panel" style="display:<?php echo $ser_reminder;?>" id="ccp_<?php echo $cnt?>_reminder">
        <div class="box-division03 info-box-div">
           <div class="accordion-heading" role="tab" id="headingOne">
              <h3 class="card-title accordion-title">
                 <a class="accordion-msg">Personal tax Reminders</a>
              </h3>
           </div>
           <div id="collapse" class="panel-collapse">
              <div class="basic-info-client1 CONTENT_PERSONAL_TAX_RETURN_REMINDERS" id="personal_box2">

                 <div class="form-group row name_fields radio_bts personal_sort" data-id="<?php echo $Setting_Order['crm_personal_next_reminder_date']; ?>" <?php echo $Setting_Delete['crm_personal_next_reminder_date']; ?>>
                    <label class="col-sm-4 col-form-label">
                        <a id="personal_custom_reminder_link" name="personal_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/4" target="_blank"><?php echo $Setting_Label['crm_personal_next_reminder_date']; ?></a>
                    </label>
                    <div class="col-sm-8">                   
                       <input type="checkbox" class="js-small f-right fields as_per_firm LISTEN_CHANGES clr-check-client" name="ccp_ptr_as_per_firm_reminder[<?php echo $cnt;?>]" value="1" <?php echo $is_as_per_firm;?>>
                    </div>
                 </div>

                 <div class="form-group row radio_bts personal_custom_reminder_label personal_sort" data-id="<?php echo $Setting_Order['crm_personal_custom_reminder']; ?>" <?php echo $Setting_Delete['crm_personal_custom_reminder']; ?>>

                    <label id="personal_custom_reminder_label" name="ccp_personal_custom_reminder_label[<?php echo $cnt;?>]" class="col-sm-4 col-form-label">
                      <?php echo $Setting_Label['crm_personal_custom_reminder']; ?>                      
                    </label>

                    <div class="col-sm-8" id="personal_custom_reminder">
                       <input type="checkbox" class="js-small f-right fields cus_reminder LISTEN_CHANGES clr-check-client" name="ccp_ptr_cus_reminder[<?php echo $cnt;?>]"  data-id="personal_cus" data-serid="4" value="<?php echo $contactRec_val['is_ptr_cus_reminder'];?>" <?php echo $is_cus_reminder;?>>
                    </div>
                 </div>
                    <?php 
                  if(!empty($client)){
                     $cus_temp = $this->db->query('select id,subject from  custom_service_reminder where client_id='.$client['id'].' and firm_id='.$_SESSION['firm_id'].' and service_id=4 and client_contacts_id='.$contactRec_val['id'])->result_array();
                    foreach ($cus_temp as $key => $value) {
                    
                   ?>
                 <div class="form-group row name_fields" style="">
                    <label class="col-sm-4 col-form-label cus_reminder_subject_<?php echo $value['id']; ?>"><?php echo $value['subject']; ?></label>
                    <div class="col-sm-8">
                       <a href="javascript:;" class="date-editop" data-toggle="modal" id="cus_reminder_<?php echo $value['id']; ?>">Edit</a>
                    </div>
                 </div>
               <?php } } ?>
              </div>
           </div>
        </div>
     </div>
      <input type="hidden" class="UPDATE_LISTENED clr-check-client" name="is_service_data_edited[4_<?php echo $contactRec_val['id'];?>]" value="0">

      <div class="SATR_custom_fields" style="display: none;">
        <?php 
          $_POST['cnt']       = $cnt;
          $user_id_conact_id  = $uri_seg."--".$contactRec_val['id'];
          echo render_custom_fields_one( '4' , $user_id_conact_id );
          unset( $_POST['cnt'] );
        ?>
      </div>
  </div>              
<!-- Personal Tax Return -->
</li>

  <?php
  $cnt++;
}
?>
</ul>