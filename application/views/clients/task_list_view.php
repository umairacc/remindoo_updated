<?php $this->load->view('includes/header');
   $role = $this->Common_mdl->getRole($_SESSION['id']);
   function convertToHoursMins($time, $format = '%02d:%02d') {
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
   }
   
   ?>
<style>
   /*span.newonoff {
   background: #4680ff;
   padding: 6px 15px;
   display: inline-block;
   color: #fff;
   border-radius: 5px;
   }*/
   span.demo {
   padding: 0 10px;
   }
   button.btn.btn-info.btn-lg.newonoff {
   padding: 3px 10px;
   height: initial;
   font-size: 15px;
   border-radius: 5px;
   }
   img.user_imgs {
   width: 38px;
   height: 38px;
   border-radius: 50%;
   display: inline-block;
   }
   a.adduser1 {
   background: #38b87c;
   color: #fff;
   padding: 5px 7px 4px;
   border-radius: 6px;
   vertical-align: middle;
   }
   #adduser label {
   text-transform: capitalize;
   font-size: 14px;
   }
  span.hours-left {
    background: #f76480;
    color: #fff;
    padding: 6px 10px;
    border-radius: 3px;
    margin-right: 15px;
}
   .dropdown12 button.btn.btn-primary {
   background: #ccc;
   color: #555;
   font-size: 13px;
   padding: 4px 10px 4px;
   margin-top: -2px;
   border-color:transparent;
   }
   select + .dropdown:hover .dropdown-menu
   {
   display: none;
   }
   select + .dropdown12 .dropdown-menu
   {
   padding: 0 !important;
   }
   select + .dropdown12 .dropdown-menu a {
   color: #000 !important;
   padding: 7px 10px !important;
   border: none !important;
   font-size: 14px;
   }
   select + .dropdown12 .dropdown-menu li {
   border: none !important;
   padding: 0px !important;
   }
   select + .dropdown12 .dropdown-menu a:hover {
   background:#4c7ffe ;
   color: #fff !important;
   }
   span.created-date {
   color: gray;
   padding-left: 10px;
   font-size: 13px;
   font-weight: 600;
   }
   span.created-date i.fa.fa-clock-o {
   padding: 0 5px;
   }
   .dropdown12 span.caret {
   right: -2px;
   z-index: 99999;
   border-top: 4px solid #555;
   border-left: 4px solid transparent;
   border-right: 4px solid transparent;
   border-bottom: 4px solid transparent;
   top: 12px;
   position: relative;
   }
   span.timer {
   padding: 0 10px;
   }
   body {
   font-family:"Arial", Helvetica, sans-serif;
   text-align: center;
   }
   #controls{
   font-size: 12px;
   }
   #time {
   font-size: 150%;
   }
</style>
<link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/responsive.dataTables.css">
<link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/buttons.dataTables.min.css?ver=2">
<link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/progress-circle.css">
<div class="pcoded-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card">
                        <!-- admin start-->
                        <div class="client_section col-xs-12 floating_set">
                           <!-- <div class="all-clients floating_set">
                              <div class="tab_section_01 floating_set">
                              <ul class="client_tab">
                               <li class="active"><a href="<?php echo  base_url()?>user">All Clients</a></li>
                               <li><a href="<?php echo  base_url()?>client/addnewclient">Add new client</a></li>
                              </ul> 
                              
                              <ul class="client_tab">
                              <li><a href="#">Email All</a></li>
                              <li><a href="#">filters</a></li>
                              </ul> 
                              </div> 
                              
                              <div class="text-left search_section1 floating_set">
                              <div class="search_box01">
                              
                              <div class="pcoded-search">
                              
                                                           <span class="searchbar-toggle">  </span>
                              
                                                           <div class="pcoded-search-box ">
                              
                                                               <input type="text" placeholder="Search">
                              
                                                               <span class="search-icon"><i class="ti-search" aria-hidden="true"></i></span>
                              
                                                           </div>
                              
                                                       </div>
                              
                              </div>
                              </div>
                              
                              
                              
                              </div> <!-- all-clients -->
                           <div class="all_user-section floating_set">
                              <div class="deadline-crm1 floating_set">
                                 <ul class="nav nav-tabs all_user1 md-tabs pull-left">
                                    <li class="nav-item">
                                       <a class="nav-link active" data-toggle="tab" href="#alltasks">All Tasks</a>
                                       <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" data-toggle="tab" href="#inprogress">In Progress</a>
                                       <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" data-toggle="tab" href="#starting">Started</a>
                                       <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" data-toggle="tab" href="#notstarting">Not Started</a>
                                       <div class="slide"></div>
                                    </li>
                                 </ul>
                                 <?php $this->load->view('users/task_summary');?>
                              </div>
                              <div class="all_user-section2 floating_set">
                                 <div class="tab-content">
                                    <div id="alltasks" class="tab-pane fade in active">
                                       <!--  <div class="count-value1 floating_set">
                                          <select id="dropdown2" >
                                             <option value="">--All--</option>
                                             <option value="notstarted">Not started</option>
                                             <option value="inprogress">In Progress</option>
                                             <option value="awaiting">Awaiting Feedback</option>
                                             <option value="testing">Testing</option>
                                             <option value="complete">Complete</option>
                                             <option value="today_task">Today's Task</option>
                                             <option value="due_date_passed">Due Date Passed</option>
                                             <option value="upcoming_task">Upcoming Task</option>
                                             <option value="assinged_to_me">Task Assigned to me</option>
                                             <option value="not_assigned">Not assigned</option>
                                          </select>
                                          <button id="deleteTriger" class="deleteTri" style="display:none;">Delete</button>
                                          </div> -->
                                       <div class="client_section3  floating_set">
                                          <div id="status_succ"></div>
                                          <div class="all-usera1 table-responsive user-dashboard-section1">
                                             <!--         <div class="dt-responsive table-responsive">
                                                <div id="time">
                                                   <span id="hours">00</span> :
                                                   <span id="minutes">00</span> :
                                                   <span id="seconds">00</span> ::
                                                   <span id="milliseconds">000</span>
                                                </div>
                                                <div id="controls">
                                                   <button id="start_pause_resume">Start</button>
                                                   <button id="reset">Reset</button>
                                                </div> -->
                                             <!-- <div class="workout-timer" data-repetitions="-1" data-sound="bell_ring">
                                                <div class="workout-timer__counter" data-counter></div>
                                                <div class="workout-timer__play-pause" data-control="play-pause" data-paused="true"></div>
                                                <div class="workout-timer__reset" data-control="reset"></div>
                                                <div class="workout-timer__repetitions" data-repetitions>Repetitions</div>
                                                <div class="workout-timer__volume" data-control="volume"></div>
                                                </div> 
                                                
                                                <div class="workout-timer" data-repetitions="-1" data-sound="bell_ring">
                                                <div class="workout-timer__counter" data-counter></div>
                                                <div class="workout-timer__play-pause" data-control="play-pause" data-paused="true"></div>
                                                <div class="workout-timer__reset" data-control="reset"></div>
                                                <div class="workout-timer__repetitions" data-repetitions>Repetitions</div>
                                                <div class="workout-timer__volume" data-control="volume"></div> 
                                                </div>
                                                <div class="workout-timer" data-repetitions="-1" data-sound="bell_ring">
                                                <div class="workout-timer__counter" data-counter></div>
                                                <div class="workout-timer__play-pause" data-control="play-pause" data-paused="true"></div>
                                                <div class="workout-timer__reset" data-control="reset"></div>
                                                <div class="workout-timer__repetitions" data-repetitions>Repetitions</div>
                                                <div class="workout-timer__volume" data-control="volume"></div> 
                                                </div>
                                                <div class="workout-timer" data-repetitions="-1" data-sound="bell_ring">
                                                <div class="workout-timer__counter" data-counter></div>
                                                <div class="workout-timer__play-pause" data-control="play-pause" data-paused="true"></div>
                                                <div class="workout-timer__reset" data-control="reset"></div>
                                                <div class="workout-timer__repetitions" data-repetitions>Repetitions</div>
                                                <div class="workout-timer__volume" data-control="volume"></div> 
                                                </div>
                                                -->
                                             <table class="table client_table1 text-center display nowrap" id="alltask" cellspacing="0" width="100%">
                                                <thead>
                                                   <tr class="text-uppercase">
                                                      <th>
                                                         <!-- <div class="checkbox-fade fade-in-primary">
                                                            <label>
                                                            <input type="checkbox"  id="bulkDelete"  />
                                                            <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                                            </label>
                                                            </div> -->
                                                      </th>
                                                      <th>Timer</th>
                                                      <th>Task Name</th>
                                                      <!-- <th>CRM-Username</th> -->
                                                      <th>Start Date</th>
                                                      <th>Due Date</th>
                                                      <th>Status</th>
                                                      <th>Priority</th>
                                                      <th>Tag</th>
                                                      <th>Assignto</th>
                                                      <th>Actions</th>
                                                      <th>Due</th>
                                                   </tr>
                                                </thead>
                                                <tbody>
                                                   <?php foreach ($task_list as $key => $value) {
                                                      error_reporting(0);
                                                      // $value['end_date'] = date('d-m-Y');
                                                      if($value['start_date']!='' && $value['end_date']!=''){    
                                                      $start  = date_create(implode('-', array_reverse(explode('-', $value['start_date']))));
                                                      $end  = date_create(implode('-', array_reverse(explode('-', $value['end_date']))));
                                                      
                                                      //$end  = date_create(); // Current time and date
                                                      $diff    = date_diff ( $start, $end );
                                                      
                                                      $y =  $diff->y;
                                                      $m =  $diff->m;
                                                      $d =  $diff->d;
                                                      $h =  $diff->h;
                                                      $min =  $diff->i;
                                                      $sec =  $diff->s;
                                                      }
                                                      else{
                                                      $y =  0;
                                                      $m =  0;
                                                      $d =  0;
                                                      $h =  0;
                                                      $min = 0;
                                                      $sec =  0;
                                                      }
                                                      
                                                      $date_now = date("Y-m-d"); // this format is string comparable
                                                      $d_rec = implode('-', array_reverse(explode('-', $value['end_date'])));
                                                      if ($date_now > $d_rec) {
                                                      //echo 'priya';
                                                      $d_val = "EXPIRED";
                                                      }else{
                                                      $d_val = $y .' years '. $m .' months '. $d .' days '. $h .' Hours '. $min .' min '. $sec .' sec ';
                                                      }
                                                      
                                                      
                                                      if($value['worker']=='')
                                                      {
                                                      $value['worker'] = 0;
                                                      }
                                                      $staff=$this->db->query("SELECT * FROM staff_form WHERE user_id in (".$value['worker'].")")->result_array();
                                                      if($value['task_status']=='notstarted')
                                                      {
                                                        $percent = 0;
                                                      $stat = 'Not Started';
                                                      } if($value['task_status']=='inprogress')
                                                      {
                                                        $percent = 25;
                                                      $stat = 'In Progress';
                                                      } if($value['task_status']=='awaiting')
                                                      {
                                                        $percent = 50;
                                                      $stat = 'Awaiting for a feedback';
                                                      } if($value['task_status']=='testing')
                                                      {
                                                        $percent = 75;
                                                      $stat = 'Testing';
                                                      } if($value['task_status']=='complete')
                                                      {
                                                        $percent = 100;
                                                      $stat = 'Complete';
                                                      }
                                                      $exp_tag = explode(',', $value['tag']);
                                                      $explode_worker=explode(',',$value['worker']);
                                                      ?>
                                                   <tr id="<?php echo $value['id']; ?>">
                                                      <td>
                                                         <!-- <div class="checkbox-fade fade-in-primary">
                                                            <label>
                                                            <input type='checkbox'  class='deleteRow' value="<?php echo $value['id'];?>"  />
                                                            <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                                            </label>
                                                            </div> -->
                                                      </td>
                                                      <td>
                                                         <!--      <div class="workout-timer" data-repetitions="-1" data-sound="bell_ring">
                                                            <div class="workout-timer__counter" data-counter></div>
                                                            <div class="workout-timer__play-pause" data-control="play-pause" data-paused="true"></div>
                                                            <div class="workout-timer__reset" data-control="reset"></div>
                                                            <div class="workout-timer__repetitions" data-repetitions>Repetitions</div>
                                                            <div class="workout-timer__volume" data-control="volume"></div>
                                                            </div>  -->
                                                        <!--  <div class="workout-timer" data-repetitions="-1" data-sound="bell_ring" data-id="<?php echo $value['id'];?>" v>
                                                            <div class="workout-timer__counter" data-counter data-id="<?php echo $value['id'];?>" id="counter_<?php echo $value['id'];?>"></div>
                                                            <div class="workout-timer__play-pause" data-control="play-pause" data-paused="true" data-id="<?php echo $value['id'];?>" id="<?php echo $value['id'];?>"></div>
                                                            <div class="workout-timer__reset" data-control="reset" data-id="<?php echo $value['id'];?>" id="<?php echo $value['id'];?>"></div> -->
                                                            <!-- <div class="workout-timer__repetitions" data-repetitions data-id="<?php echo $value['id'];?>" id="<?php echo $value['id'];?>">Repetitions</div>
                                                               <div class="workout-timer__volume" data-control="volume" data-id="<?php echo $value['id'];?>" id="<?php echo $value['id'];?>"></div>  -->
                                                         </div>
                                                         <?php echo date('Y-m-d H:i:s', $value['created_date']);?>
                                                      </td>
                                                      <td><?php echo ucfirst($value['subject']);?></td>
                                                      <td><?php echo $value['start_date'];?></td>
                                                      <td><?php echo $value['end_date'];?></td>
                                                      <td><?php echo $stat;?></td>
                                                      <td>
                                                         <?php echo $value['priority'];?>
                                                      </td>
                                                      <td>
                                                         <?php  foreach ($exp_tag as $exp_tag_key => $exp_tag_value) {
                                                            echo $tagName = $this->Common_mdl->getTag($exp_tag_value).' ';
                                                            }?>
                                                      </td>
                                                      <td class="user_imgs" id="task_<?php echo $value['id'];?>">
                                                         <?php
                                                            foreach($explode_worker as $key => $val){     
                                                                    $getUserProfilepic = $this->Common_mdl->getUserProfilepic($val);
                                                            ?>
                                                         <img src="<?php echo $getUserProfilepic;?>" alt="img">
                                                         <?php } ?>
                                                         <a href="javascript:;" data-toggle="modal" data-target="#adduser_<?php echo $value['id'];?>" class="adduser1"><i class="fa fa-plus"></i></a>
                                                      </td>
                                                      <td>
                                                         <?php //echo $value['worker'];?>
                                                         <p class="action_01">
                                                            <?php if($role!='Staff'){?>   
                                                            <a href="<?php echo base_url().'user/t_delete/'.$value['id'];?>" onclick="return confirm('Are you sure want to delete');"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>
                                                            <?php } ?>
                                                            <a href="<?php echo base_url().'user/update_task/'.$value['id'];?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                                                            <!-- <a href="#"><i class="fa fa-inbox fa-6" aria-hidden="true"></i></a> -->
                                                         </p>
                                                      </td>
                                                      <td>
                                                         <span class="hours-left"><?php
                                                         //echo $value['timer_status'];
                                                          if($value['timer_status']!=''){ echo convertToHoursMins($value['timer_status']); }else{ echo '0'; }?> hours<?php //echo $value['task_status'];?></span>
                                                         <!--  <select>  <option>Normal</option>
                                                            <option>Normal</option> </select> -->
                                                         <select name="task_status" id="task_status" class="task_status" data-id="<?php echo $value['id'];?>">
                                                            <option value="">Select Status</option>
                                                            <option value="notstarted" <?php if($value['task_status']=='notstarted'){ echo 'selected="selected"'; }?>>Not started</option>
                                                            <option value="inprogress" <?php if($value['task_status']=='inprogress'){ echo 'selected="selected"'; }?>>In Progress</option>
                                                            <option value="awaiting" <?php if($value['task_status']=='awaiting'){ echo 'selected="selected"'; }?>>Awaiting Feedback</option>
                                                            <option value="testing" <?php if($value['task_status']=='testing'){ echo 'selected="selected"'; }?>>Testing</option>
                                                            <option value="complete" <?php if($value['task_status']=='complete'){ echo 'selected="selected"'; }?>>Complete</option>
                                                         </select>
                                                         <select name="test_status" id="test_status" class="close-test-04 test_status" data-id="<?php echo $value['id'];?>">
                                                            <option value="open" <?php if($value['test_status']=='open'){ echo 'selected="selected"'; }?>>Open</option>
                                                            <option value="onhold" <?php if($value['test_status']=='onhold'){ echo 'selected="selected"'; }?>>On Hold</option>
                                                            <option value="resolved" <?php if($value['test_status']=='resolved'){ echo 'selected="selected"'; }?>>Resolved</option>
                                                            <option value="closed" <?php if($value['test_status']=='closed'){ echo 'selected="selected"'; }?>>Closed</option>
                                                            <option value="duplicate" <?php if($value['test_status']=='duplicate'){ echo 'selected="selected"'; }?>>Duplicate</option>
                                                            <option value="invalid" <?php if($value['test_status']=='invalid'){ echo 'selected="selected"'; }?>>Invalid</option>
                                                            <option value="wontfix" <?php if($value['test_status']=='wontfix'){ echo 'selected="selected"'; }?>>Wontfix</option>
                                                         </select>
                                                         <!-- <span class="dropdown12">
                                                            <button class="btn btn-primary" type="button" data-toggle="dropdown">Open
                                                            <span class="caret"></span></button>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="#">Open</a></li>
                                                                <li><a href="#">On hold</a></li>
                                                                <li><a href="#">Resolved</a></li>
                                                                <li><a href="#">Closed</a></li>
                                                                <li><a href="#">Duplicate</a></li>
                                                                <li><a href="#">Invalid</a></li>
                                                                <li><a href="#">Wontfix</a></li>
                                                              </ul>
                                                            </span> -->
                                                         <span class="created-date">
                                                         <i class="fa fa-clock-o"></i>Created <?php echo $value['start_date'];?></span>
                                                         <span class="timer"><?php echo $d_val;?> </span>
                                                         <span class="demo" id="demo_<?php echo $value['id'];?>"></span>
                                                        
                                                         <!--    <div id="time_<?php echo $value['id'];?>">
                                                            <span id="hours_<?php echo $value['id'];?>">00</span> :
                                                            <span id="minutes_<?php echo $value['id'];?>">00</span> :
                                                            <span id="seconds_<?php echo $value['id'];?>">00</span> ::
                                                            <span id="milliseconds_<?php echo $value['id'];?>">000</span>
                                                            </div>
                                                            <div id="controls" class="start-time-06">
                                                            <button id="start_pause_resume_<?php echo $value['id'];?>">Start</button>
                                                            <button id="reset_<?php echo $value['id'];?>">Reset</button>
                                                            </div> -->
                                                          <div class="workout-timer" data-repetitions="-1" data-sound="bell_ring" data-id="<?php echo $value['id'];?>" v>
                                                            <div class="workout-timer__counter" data-counter data-id="<?php echo $value['id'];?>" id="counter_<?php echo $value['id'];?>"></div>
                                                            <div class="workout-timer__play-pause" data-control="play-pause" data-paused="true" data-id="<?php echo $value['id'];?>" id="<?php echo $value['id'];?>"></div>
                                                            <div class="workout-timer__reset" data-control="reset" data-id="<?php echo $value['id'];?>" id="<?php echo $value['id'];?>"></div>
                                                            <!-- <div class="workout-timer__repetitions" data-repetitions data-id="<?php echo $value['id'];?>" id="<?php echo $value['id'];?>">Repetitions</div>
                                                               <div class="workout-timer__volume" data-control="volume" data-id="<?php echo $value['id'];?>" id="<?php echo $value['id'];?>"></div>  -->
                                                         </div>
														  <div class="progress-circle progress-<?php echo $percent;?>"><span><?php echo $percent;?></span></div>
                                                      </td>
                                                   </tr>
                                                   <div id="adduser_<?php echo $value['id'];?>" class="modal fade" role="dialog">
                                                      <div class="modal-dialog">
                                                         <!-- Modal content-->
                                                         <div class="modal-content">
                                                            <div class="modal-header">
                                                               <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                               <h4 class="modal-title" style="text-align-last: center">Assign to</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                               <!--  <label>assign to </label> -->
                                                               <div class="dropdown-sin-2">
                                                                  <select multiple placeholder="select" name="workers[]" id="workers<?php echo $value['id'];?>" class="workers">
                                                                     <?php foreach($staffs as $s_key => $s_val){ ?>
                                                                      <option value="<?php echo $s_val['id'];?>"  <?php if(in_array( $s_val['id'] ,$explode_worker )) {?> selected="selected"<?php } ?> ><?php echo $s_val['crm_name'];?></option>
                                                                     <?php } ?>
                                                                  </select>
                                                               </div>
                                                            </div>
                                                            <div class="modal-footer profileEdit">
                                                               <input type="hidden" name="hidden">
                                                               <a href="javascript:void();" id="acompany_name" data-dismiss="modal" data-id="<?php echo $value['id'];?>" class="save_assign_staff">save</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <?php 
                                                      } ?>
                                                   <!-- <tr>
                                                      <td></td>
                                                      </tr> -->
                                                </tbody>
                                             </table>
                                             
                                          </div>
                                       </div>
                                    </div>
                                 
                                 <!-- home-->
                                 <div id="inprogress" class="tab-pane fade">
                                    <!-- <?php $this->load->view('users/task_summary');?> -->
                                    <div class="count-value1 floating_set">
                                       <!--<ul class="pull-left">
                                          <li><select><option>1</option><option>2</option><option>3</option></select></li>
                                           <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>
                                          <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i><span>selected</span></a></li>
                                          <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>
                                          <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>  
                                          <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>
                                          </ul> 
                                          
                                          <ul class="pull-right">
                                          <form>
                                             <label>Search all columns</label>
                                             <input type="text" name="text">
                                          </form>
                                          </ul> -->
                                    </div>
                                    <div class="user-dashboard-section1 client_section3 table-responsive floating_set">
                                       <table class="table client_table1 text-center" id="inprogresses">
                                          <thead>
                                             <tr class="text-uppercase">
                                                <th>Timer</th>
                                                <th>Task Name</th>
                                                <!-- <th>CRM-Username</th> -->
                                                <th>Start Date</th>
                                                <th>Due Date</th>
                                                <th>Status</th>
                                                <th>Priority</th>
                                                <th>Assignto</th>
                                                <th>Actions</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             <?php 
                                                foreach ($task_list as $key => $value) {
                                                if($value['task_status']=='inprogress') {
                                                if($value['worker']=='')
                                                {
                                                   $value['worker'] = 0;
                                                }
                                                $staff=$this->db->query("SELECT * FROM staff_form WHERE user_id in (".$value['worker'].")")->result_array();
                                                
                                                ?>
                                             <tr>
                                                <td><?php echo date('Y-m-d H:i:s', $value['created_date']);?></td>
                                                <td><?php echo ucfirst($value['subject']);?></td>
                                                <td><?php echo $value['start_date'];?></td>
                                                <td><?php echo $value['end_date'];?></td>
                                                <td><?php echo "Inprogress";?></td>
                                                <td>
                                                   <?php echo $value['priority'];?>
                                                </td>
                                                <td class="user_imgs">
                                                   <?php
                                                      foreach($staff as $key => $val){     
                                                      ?>
                                                   <img src="<?php echo base_url().'uploads/'.$val['profile'];?>" alt="img">
                                                   <?php } ?>
                                                   <a href="javascript:;" data-toggle="modal" data-target="#adduser" class="adduser1"><i class="fa fa-plus"></i></a>
                                                </td>
                                                <td>
                                                   <p class="action_01">
                                                      <?php if($role!='Staff'){?>   
                                                      <a href="<?php echo base_url().'user/delete/'.$value['id'];?>" onclick="return confirm('Are you sure want to delete');"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>
                                                      <?php } ?>
                                                      <a href="<?php echo base_url().'user/update_task'.$value['id'];?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                                                      <!-- <a href="#"><i class="fa fa-inbox fa-6" aria-hidden="true"></i></a> -->
                                                   </p>
                                                </td>
                                             </tr>
                                             <?php } }
                                                ?>
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                                 <!-- new_user -->
                                 <div id="starting" class="tab-pane fade">
                                    <!-- <?php $this->load->view('users/task_summary');?> -->
                                    <div class="count-value1 floating_set">
                                       <!--<ul class="pull-left">
                                          <li><select><option>1</option><option>2</option><option>3</option></select></li>
                                          <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>
                                          <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i><span>selected</span></a></li>
                                          <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>
                                          <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>  
                                          <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>
                                          </ul> 
                                          
                                          <ul class="pull-right">
                                          <form>
                                             <label>Search all columns</label>
                                             <input type="text" name="text">
                                          </form>
                                          </ul> -->
                                    </div>
                                    <div class="user-dashboard-section1 client_section3 table-responsive floating_set">
                                       <table class="table client_table1 text-center" id="started">
                                          <thead>
                                             <tr class="text-uppercase">
                                                <th>Timer</th>
                                                <th>Task Name</th>
                                                <!-- <th>CRM-Username</th> -->
                                                <th>Start Date</th>
                                                <th>Due Date</th>
                                                <th>Status</th>
                                                <th>Priority</th>
                                                <th>Assignto</th>
                                                <th>Actions</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             <?php foreach ($task_list as $key => $value) {
                                                if($value['task_status']=='started') { 
                                                   if($value['worker']=='')
                                                   {
                                                      $value['worker'] = 0;
                                                   }
                                                $staff=$this->db->query("SELECT * FROM staff_form WHERE user_id in (".$value['worker'].")")->result_array();
                                                
                                                 ?>
                                             <tr>
                                                <td><?php echo date('Y-m-d H:i:s', $value['created_date']);?></td>
                                                <td><?php echo ucfirst($value['subject']);?></td>
                                                <td><?php echo $value['start_date'];?></td>
                                                <td><?php echo $value['end_date'];?></td>
                                                <td><?php echo "Started";?></td>
                                                <td>
                                                   <?php echo $value['priority'];?>
                                                </td>
                                                <td class="user_imgs">
                                                   <?php
                                                      foreach($staff as $key => $val){     
                                                      ?>
                                                   <img src="<?php echo base_url().'uploads/'.$val['profile'];?>" alt="img">
                                                   <?php } ?>
                                                   <a href="javascript:;" data-toggle="modal" data-target="#adduser" class="adduser1"><i class="fa fa-plus"></i></a>
                                                </td>
                                                <td>
                                                   <p class="action_01">
                                                      <?php if($role!='Staff'){?>   
                                                      <a href="<?php echo base_url().'user/delete/'.$value['id'];?>" onclick="return confirm('Are you sure want to delete');"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>
                                                      <?php } ?>
                                                      <a href="<?php echo base_url().'user/update_task'.$value['id'];?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                                                      <!-- <a href="#"><i class="fa fa-inbox fa-6" aria-hidden="true"></i></a> -->
                                                   </p>
                                                </td>
                                             </tr>
                                             <?php } } ?>
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                                 <!-- inactive_user -->
                                 <div id="notstarting" class="tab-pane fade">
                                    <!-- <?php $this->load->view('users/task_summary');?>-->
                                    <div class="count-value1 floating_set">
                                       <!--<ul class="pull-left">
                                          <li><select><option>1</option><option>2</option><option>3</option></select></li>
                                           <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>
                                          <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i><span>selected</span></a></li>
                                          <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>
                                          <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>  
                                          <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>
                                          </ul> 
                                          
                                          <ul class="pull-right">
                                          <form>
                                             <label>Search all columns</label>
                                             <input type="text" name="text">
                                          </form>
                                          </ul>--> 
                                    </div>
                                    <div class="user-dashboard-section1 client_section3 table-responsive floating_set">
                                       <div id="suspent_succ"></div>
                                       <table class="table client_table1 text-center" id="notstarted">
                                          <thead>
                                             <tr class="text-uppercase">
                                                <th>Timer</th>
                                                <th>Task Name</th>
                                                <!-- <th>CRM-Username</th> -->
                                                <th>Start Date</th>
                                                <th>Due Date</th>
                                                <th>Status</th>
                                                <th>Priority</th>
                                                <th>Assignto</th>
                                                <th>Actions</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             <?php foreach ($task_list as $key => $value) {
                                                if($value['task_status']=='notstarted') { 
                                                   if($value['worker']=='')
                                                   {
                                                      $value['worker'] = 0;
                                                   }
                                                $staff=$this->db->query("SELECT * FROM staff_form WHERE user_id in (".$value['worker'].")")->result_array();
                                                
                                                 ?>
                                             <tr>
                                                <td><?php echo date('Y-m-d H:i:s', $value['created_date']);?></td>
                                                <td><?php echo ucfirst($value['subject']);?></td>
                                                <td><?php echo $value['start_date'];?></td>
                                                <td><?php echo $value['end_date'];?></td>
                                                <td><?php echo "Not Started";?></td>
                                                <td>
                                                   <?php echo $value['priority'];?>
                                                </td>
                                                <td class="user_imgs">
                                                   <?php
                                                      foreach($staff as $key => $val){     
                                                      ?>
                                                   <img src="<?php echo base_url().'uploads/'.$val['profile'];?>" alt="img">
                                                   <?php } ?>
                                                   <a href="javascript:;" data-toggle="modal" data-target="#adduser" class="adduser1"><i class="fa fa-plus"></i></a>
                                                </td>
                                                <td>
                                                   <p class="action_01">
                                                      <?php if($role!='Staff'){?>      
                                                      <a href="<?php echo base_url().'user/delete/'.$value['id'];?>" onclick="return confirm('Are you sure want to delete');"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>
                                                      <?php } ?>
                                                      <a href="<?php echo base_url().'user/update_task'.$value['id'];?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                                                      <!-- <a href="#"><i class="fa fa-inbox fa-6" aria-hidden="true"></i></a> -->
                                                   </p>
                                                </td>
                                             </tr>
                                             <?php } } ?>
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                                 <!-- frozen -->
                              </div>
                           </div>
                           <!-- admin close -->
                        </div>
                        <!-- Register your self card end -->
                     </div>
                  </div>
               </div>
               <!-- Page body end -->
            </div>
         </div>
         <!-- Main-body end -->
         <div id="styleSelector">
         </div>
      </div>
   </div>
</div>
</div>
</div>
</div>
<!-- Warning Section Starts -->
<!-- Older IE warning message -->
<!--[if lt IE 10]>
<div class="ie-warning">
   <h1>Warning!!</h1>
   <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
   <div class="iew-container">
      <ul class="iew-download">
         <li>
            <a href="http://www.google.com/chrome/">
               <img src="assets/images/browser/chrome.png" alt="Chrome">
               <div>Chrome</div>
            </a>
         </li>
         <li>
            <a href="https://www.mozilla.org/en-US/firefox/new/">
               <img src="assets/images/browser/firefox.png" alt="Firefox">
               <div>Firefox</div>
            </a>
         </li>
         <li>
            <a href="http://www.opera.com">
               <img src="assets/images/browser/opera.png" alt="Opera">
               <div>Opera</div>
            </a>
         </li>
         <li>
            <a href="https://www.apple.com/safari/">
               <img src="assets/images/browser/safari.png" alt="Safari">
               <div>Safari</div>
            </a>
         </li>
         <li>
            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
               <img src="assets/images/browser/ie.png" alt="">
               <div>IE (9 & above)</div>
            </a>
         </li>
      </ul>
   </div>
   <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->
<!-- Warning Section Ends -->
<?php
   $data = $this->db->query("SELECT * FROM update_client")->result_array();
   foreach($data as $row) { ?>
<!-- Modal -->
<div id="myModal<?php echo $row['user_id'];?>" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">CRM-Updated fields</h4>
         </div>
         <div class="modal-body">
            <?php if(isset($row['company_name'])&&($row['company_name']!='0')){?>
            <p>Company Name: <span><?php echo $row['company_name'];?></span></p>
            <?php } ?> 
            <?php if(isset($row['company_url'])&&($row['company_url']!='0')){?>
            <p>Company URL: <span><?php echo $row['company_url'];?></span></p>
            <?php } ?>
            <?php if(isset($row['officers_url'])&&($row['officers_url']!='0')){?>
            <p>Officers URL: <span><?php echo $row['officers_url'];?></span></p>
            <?php } ?>
            <?php if(isset($row['incorporation_date'])&&($row['incorporation_date']!='0')){?>
            <p>Incorporation Date: <span><?php echo $row['incorporation_date'];?></span></p>
            <?php } ?>
            <?php if(isset($row['register_address'])&&($row['register_address']!='0')){?>
            <p>Registered Address: <span><?php echo $row['register_address'];?></span></p>
            <?php }  ?>
            <?php if(isset($row['company_status'])&&($row['company_status']!='0')){?>
            <p>Company Status: <span><?php echo $row['company_status'];?></span></p>
            <?php }  ?>
            <?php if(isset($row['company_type'])&&($row['company_type']!='0')){?>
            <p>Company Type: <span><?php echo $row['company_type'];?></span></p>
            <?php } ?>
            <?php if(isset($row['accounts_periodend'])&&($row['accounts_periodend']!='0')){?>
            <p>Accounts Period End: <span><?php echo $row['accounts_periodend'];?></span></p>
            <?php } ?>
            <?php if(isset($row['hmrc_yearend'])&&($row['hmrc_yearend']!='0')){?>
            <p>HMRC Year End: <span><?php echo $row['hmrc_yearend'];?></span></p>
            <?php } ?>
            <?php if(isset($row['ch_accounts_next_due'])&&($row['ch_accounts_next_due']!='0')){?>
            <p>CH Accounts Next Due: <span><?php echo $row['ch_accounts_next_due'];?></span></p>
            <?php } ?>
            <?php if(isset($row['confirmation_statement_date'])&&($row['confirmation_statement_date']!='0')){?>
            <p>Confirmation Statement Date: <span><?php echo $row['confirmation_statement_date'];?></span></p>
            <?php } ?>
            <?php if(isset($row['confirmation_statement_due_date'])&&($row['confirmation_statement_due_date']!='0')){?>
            <p>Confirmation Statement Due: <span><?php echo $row['confirmation_statement_due_date'];?></span></p>
            <?php } ?>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!-- /.modal -->
<?php } ?>
<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<!-- <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap.js"></script> -->
<!-- <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>  -->
<script src="<?php echo base_url();?>js/jquery.workout-timer.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="http://remindoo.org/CRMTool/assets/js/dataTables.responsive.min.js"></script>
<script>
   //alert(timer_fun());
   
   //alert(<?php echo $d; ?>);
   /*var i = <?php echo $value['id'];?>;
   //var d = <?php echo $d;?>;
   //alert(d);
   // Set the date we're counting down to
   //function timer_fun(){
   var countDownDate = new Date("Sep 4, 2019 15:37:25").getTime();
   
   // Update the count down every 1 second
   var x = setInterval(function() {
   
       // Get todays date and time
       var now = new Date().getTime();
       
       // Find the distance between now an the count down date
       var distance = countDownDate - now;
       console.log(countDownDate);
       // Time calculations for days, hours, minutes and seconds
       var days = Math.floor(distance / (1000 * 60 * 60 * 24));
       var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
       var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
       var seconds = Math.floor((distance % (1000 * 60)) / 1000);
       
       // Output the result in an element with id="demo"
       //document.getElementById("demo").innerHTML = days + "d " + hours + "h "
     var result = humanise(days) + hours + "h "
       + minutes + "m " + seconds + "s ";
       // console.log(result);
       //alert('demo_<?php echo $value["id"];?>');
       //$("#demo_<?php echo $value['id'];?>").html(result_<?php echo $key;?>);
   
       $('#demo_12').html(result);
   //alert(result);
       return countDownDate;
   
       
       // If the count down is over, write some text 
       if (distance < 0) {
           clearInterval(x);
           document.getElementById("demo").innerHTML = "EXPIRED";
       }
   }, 1000);*/
   //}
   
         function humanise (diff) {
   
   // The string we're working with to create the representation 
   var str = '';
   // Map lengths of `diff` to different time periods 
   var values = [[' year', 365], [' month', 30], [' day', 1]];
   // Iterate over the values... 
   for (var i=0;i<values.length;i++) {
   var amount = Math.floor(diff / values[i][1]);
   // ... and find the largest time value that fits into the diff 
   if (amount >= 1) { 
   // If we match, add to the string ('s' is for pluralization) 
   str += amount + values[i][0] + (amount > 1 ? 's' : '') + ' '; 
   // and subtract from the diff 
   diff -= amount * values[i][1];
   }
   else
   {
   str += amount + values[i][0]+' ';
   }
   
   } return str; } 
     
</script>
<?php
   /*  foreach ($task_list as $key => $value) {
      # code...
   
     $s_date = $value['start_date'];
     $d = date("F j, Y G:i:s",strtotime($s_date));
   //echo $d;die;
   }*/
   ?><script></script>
<script>
   //    window.onload = function() { 
   //    $(".chartContainer").CanvasJSChart({  
   //       legend :{ 
   //          verticalAlign: "center", 
   //          horizontalAlign: "right" 
   //       }, 
   //       data: [ 
   //       { 
   //          type: "pie", 
   //          showInLegend: true, 
   //          toolTipContent: "{label} <br/> {y} %", 
   //          indexLabel: "{y} %", 
   //          dataPoints: [ 
   //             { label: "Samsung",  y: 30.3, legendText: "Samsung"}, 
   //             { label: "Apple",    y: 19.1, legendText: "Apple"  }, 
   //             { label: "Huawei",   y: 4.0,  legendText: "Huawei" }, 
   //             { label: "LG",       y: 3.8,  legendText: "LG Electronics"}, 
   //             { label: "Lenovo",   y: 3.2,  legendText: "Lenovo" }, 
   //             { label: "Others",   y: 39.6, legendText: "Others" } 
   //          ] 
   //       } 
   //       ] 
   //    }); 
   // }
   
   
      $(document).ready(function() {
   
       humanise();
   
      
      
        var tabletask1 =$("#alltask").dataTable({
           "iDisplayLength": 10,
        "scrollX": true,
        "dom": '<"toolbar-table">lfrtip',
        responsive: true
        });
      
        // var tabletask2 =$("#inprogresses").dataTable({
        //     "dom": '<"toolbar-table">lfrtip'
        //  });
      
        // var tabletask3 =$("#started").dataTable({
        //     "dom": '<"toolbar-table">lfrtip'
        //  });
      
        // var tabletask4 =$("#notstarted").dataTable({
        //     "dom": '<"toolbar-table">lfrtip',
      
        //  });
      
        $("div.toolbar-table").html('<div class="filter-task1"><h2>Filter:</h2><ul><li><img src="<?php echo  base_url()?>assets/images/by-date2.png" alt="data"><select id="statuswise_filter" class="statuswise_filter"><option value="">By Status</option> <option value="notstarted">Not started</option><option value="inprogress">In Progress</option><option value="awaiting">Awaiting Feedback</option><option value="testing">Testing</option><option value="complete">Complete</option></select></li><li><img src="<?php echo  base_url()?>assets/images/by-date3.png" alt="data"><select id="prioritywise_filter" class="prioritywise_filter"><option value="">By Priority</option><option value="low">Low</option><option value="medium">Medium</option><option value="high">High</option><option value="super_urgent">Super Urgent</option></select></li><li><select id="export_report" class="export_report"><option value="">Export</option><option value="excel">Excel</option><option value="pdf">PDF</option><option value="html">HTML</option></select></li></ul></div>');
      
      
      $("#inprogresses").dataTable({
           "iDisplayLength": 10,
        });
      $("#notstarted").dataTable({
           "iDisplayLength": 10,
        });
      $("#started").dataTable({
           "iDisplayLength": 10,
        });
      
         $(document).on('change','.task_status',function () {
            //$(".task_status").change(function(){
       var rec_id = $(this).data('id');
       var stat = $(this).val();
          $.ajax({
            url: '<?php echo base_url();?>user/task_statusChange/',
            type: 'post',
            data: { 'rec_id':rec_id,'status':stat },
            timeout: 3000,
            success: function( data ){
               //alert('ggg');
                $("#status_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! Task status have been changed successfully...</span></div></div></div>').hide().fadeIn(1500, function() { $('#status_succ'); });
                setTimeout(resetAll,3000);
               /* if(stat=='3'){
                  
                   //$this.closest('td').next('td').html('Active');
                   $('#frozen'+rec_id).html('Frozen');
      
                } else {
                   $this.closest('td').next('td').html('Inactive');
                }*/
                },
                error: function( errorThrown ){
                    console.log( errorThrown );
                }
            });
   });
   
   
         $(document).on('change','.test_status',function () {
            //$(".task_status").change(function(){
       var rec_id = $(this).data('id');
       var stat = $(this).val();
          $.ajax({
            url: '<?php echo base_url();?>user/test_statuschange/',
            type: 'post',
            data: { 'rec_id':rec_id,'status':stat },
            timeout: 3000,
            success: function( data ){
               //alert('ggg');
                $("#status_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! Status have been changed successfully...</span></div></div></div>').hide().fadeIn(1500, function() { $('#status_succ'); });
                setTimeout(resetAll,3000);
               /* if(stat=='3'){
                  
                   //$this.closest('td').next('td').html('Active');
                   $('#frozen'+rec_id).html('Frozen');
      
                } else {
                   $this.closest('td').next('td').html('Inactive');
                }*/
                },
                error: function( errorThrown ){
                    console.log( errorThrown );
                }
            });
   });
   
      //$(".status").change(function(e){
      $('#alluser').on('change','.status',function () {
      //e.preventDefault();
      
      
       var rec_id = $(this).data('id');
       var stat = $(this).val();
      
      $.ajax({
            url: '<?php echo base_url();?>user/statusChange/',
            type: 'post',
            data: { 'rec_id':rec_id,'status':stat },
            timeout: 3000,
            success: function( data ){
               //alert('ggg');
                $("#status_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! User status have been changed successfully...</span></div></div></div>').hide().fadeIn(1500, function() { $('#status_succ'); });
                setTimeout(resetAll,3000);
                if(stat=='3'){
                  
                   //$this.closest('td').next('td').html('Active');
                   $('#frozen'+rec_id).html('Frozen');
      
                } else {
                   $this.closest('td').next('td').html('Inactive');
                }
                },
                error: function( errorThrown ){
                    console.log( errorThrown );
                }
            });
       });
      
      // payment/non payment status
      $('.suspent').click(function() {
        if($(this).is(':checked'))
            var stat = '1';
        else
            var stat = '0';
        var rec_id = $(this).val();
        var $this = $(this);
         $.ajax({
            url: '<?php echo base_url();?>user/suspentChange/',
            type: 'post',
            data: { 'rec_id':rec_id,'status':stat },
            success: function( data ){
               //alert('ggg');
                $("#suspent_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! User status have been changed successfully...</span></div></div></div>');
                if(stat=='1'){
                  
                   $this.closest('td').next('td').html('Payment');
      
                } else {
                   $this.closest('td').next('td').html('Non payment');
                }
                },
                error: function( errorThrown ){
                    console.log( errorThrown );
                }
            });
      });
      });
      
</script>
<script>
   $(document).ready(function() {
    var table =  $('#alltask').DataTable();
     
      $('#dropdown2').on('change', function () {
                    // table.columns(5).search( this.value ).draw();
                    var filterstatus = $(this).val();
                    $.ajax({
                  type: "POST",
                  url: "<?php echo base_url();?>tasksummary/taskFilter",
                  data: {filterstatus:filterstatus},
                  success: function(response) {
   
                     $(".all-usera1").html(response);
    
                  },
                  //async:false,
               });
                 });
   
      /*$('#dropdown1').on('change', function () {
                    // table.columns(2).search( this.value ).draw();
                    
                 } );*/
   
                
   });
   
</script>
<script>
   $(document).ready(function() {
      $('.dropdown-sin-2').dropdown({
      limitCount: 5,
      input: '<input type="text" maxLength="20" placeholder="Search">'
    });
   $("#bulkDelete").on('click',function() { // bulk checked
         var status = this.checked;
   
         $(".deleteRow").each( function() {
            $(this).prop("checked",status);
   
         });
        if(status==true)
        {
          $('#deleteTriger').show();
        } else {
          $('#deleteTriger').hide();
        }
      });
   
   $(".deleteRow").on('click',function() { // bulk checked
   
        var status = this.checked;
   
        if(status==true)
        {
          $('#deleteTriger').show();
        } else {
          $('#deleteTriger').hide();
        }
      });
      
      $('#deleteTriger').on("click", function(event){ // triggering delete one by one
       
         if( $('.deleteRow:checked').length > 0 ){  // at-least one checkbox checked
            var ids = [];
            $('.deleteRow').each(function(){
               if($(this).is(':checked')) { 
                  ids.push($(this).val());
               }
            });
            var ids_string = ids.toString();  // array to string conversion 
            $.ajax({
               type: "POST",
               url: "<?php echo base_url();?>user/task_delete/",
               data: {data_ids:ids_string},
               success: function(response) {
                  //dataTable.draw(); // redrawing datatable
                  var emp_ids = response.split(",");
   for (var i=0; i < emp_ids.length; i++ ) {
   
      $("#"+emp_ids[i]).remove(); 
   }
   
               },
               //async:false,
            });
         }
      });
   
      /*$("#datewise_filter").change(function(){
         alert($(this).val());
      });*/
     /* $("#prioritywise_filter").change(function(){
         alert($(this).val());
      });
      $("#statuswise_filter").change(function(){
         alert($(this).val());
      });
      $("#export_report").change(function(){
         alert($(this).val());
      });*/
   $(  document ).on('change', ".statuswise_filter",function(){
   //$("#statuswise_filter").change(function(){
      //alert($(this).val());
      var data = {};
      data['status'] = $(this).val();
      data['priority'] = $('.prioritywise_filter').val();
      get_result(data);
      return false; 
    });
   $(  document ).on('change', ".prioritywise_filter",function(){
   //$("#prioritywise_filter").change(function(){
      var data = {};
      data['priority'] = $(this).val();
      data['status'] = $('.statuswise_filter').val();
      get_result(data);
      return false;
    });
   
   $(  document ).on('change', ".export_report",function(){
   //$("#prioritywise_filter").change(function(){
      var data = {};
      data['priority'] = $('.prioritywise_filter').val();
      data['status'] = $('.statuswise_filter').val();
      data['d_type'] = $(this).val();
      file_download(data);
      return false;
    });
   
   function file_download(data)
   {
   var type = data.d_type;
   var status = data.status;
   var priority = data.priority;
   if(type=='excel')
   {
   window.location.href="<?php echo base_url().'user/task_excel?status="+status+"&priority="+priority+"'?>";
   }else if(type=='pdf')
   {
   window.location.href="<?php echo base_url().'user/task_pdf?status="+status+"&priority="+priority+"'?>";
   }else if(type=='html')
   {
   window.open(
   "<?php echo base_url().'user/task_html?status="+status+"&priority="+priority+"'?>",
   '_blank' // <- This is what makes it open in a new window.
   );
   // window.location.href="<?php echo base_url().'user/task_html?status="+status+"&priority="+priority+"'?>";
   }
   }
   
    function get_result(data){
      $(".LoadingImage").show();
      var pri = data.priority;
      var sta = data.status;
      //alert(sta);
      var ns = '';
      var ip = '';
      var af = '';
      var ts = '';
      var cm = '';
      if(sta == 'notstarted')
      {
         ns = 'selected="selected"';
      }if(sta == 'inprogress')
      {
         ip = 'selected="selected"';
      }if(sta == 'awaiting')
      {
         af = 'selected="selected"';
      }if(sta == 'testing')
      {
         ts = 'selected="selected"';
      }if(sta == 'complete')
      {
         cm = 'selected="selected"';
      }
   
      var lw = '';
      var md = '';
      var hi = '';
      var su = '';
   
      if(pri == 'low')
      {
         lw = 'selected="selected"';
      }if(pri == 'medium')
      {
         md = 'selected="selected"';
      }if(pri == 'high')
      {
         hi = 'selected="selected"';
      }if(pri == 'super_urgent')
      {
         su = 'selected="selected"';
      }
   
      $.ajax({
      url: '<?php echo base_url();?>user/filter_task/',
      type: "POST",
      data: data,
      success: function(data)  
      { 
        $(".LoadingImage").hide();
        $(".all-usera1").html(data);
   
        var tabletask1 =$("#alltask").dataTable({
          "iDisplayLength": 10,
          "scrollX": true,
          "dom": '<"toolbar-table">lfrtip',
          responsive: true
        });
         $("div.toolbar-table").html('<div class="filter-task1"><h2>Filter:</h2><ul><li><img src="<?php echo  base_url()?>assets/images/by-date2.png" alt="data"><select id="statuswise_filter" class="statuswise_filter"><option value="">By Status</option> <option value="notstarted" ' +ns+ ' >Not started</option><option value="inprogress" '+ip+' >In Progress</option><option value="awaiting"'+af+'>Awaiting Feedback</option><option value="testing" '+ts+'>Testing</option><option value="complete" '+cm+'>Complete</option></select></li><li><img src="<?php echo  base_url()?>assets/images/by-date3.png" alt="data"><select id="prioritywise_filter" class="prioritywise_filter"><option value="">By Priority</option><option value="low" '+lw+'>Low</option><option value="medium" '+md+'>Medium</option><option value="high" '+hi+'>High</option><option value="super_urgent" '+su+'>Super Urgent</option></select></li><li><select id="export_report" class="export_report"><option value="">Export</option><option value="excel">Excel</option><option value="pdf">PDF</option><option value="html">HTML</option></select></li></ul></div>');
      }
      });
    }
   
   
   });
   
   $(".save_assign_staff").click(function(){
      alert("0");
      var id = $(this).attr("data-id");
      var data = {};
       var countries =$("#workers"+id ).val();
      /* alert($("#workers"+id ).val());
        $.each($(".workers option:selected"), function(){            
            countries.push($(this).val());
        });*/
      data['task_id'] = id;
      data['worker'] = countries;
   
   
        $.ajax({
               type: "POST",
               url: "<?php echo base_url();?>user/update_assignees/",
               data: data,
               success: function(response) {
               $('#task_'+id).html(response);
               },
            });
   });
   
   
   
   $(function() {
    
    var hours = minutes = seconds = milliseconds = 0;
    var prev_hours = prev_minutes = prev_seconds = prev_milliseconds = undefined;
    var timeUpdate;
   
    // Start/Pause/Resume button onClick
    $("#start_pause_resume").button().click(function(){
        // Start button
        if($(this).text() == "Start"){  // check button label
            $(this).html("<span class='ui-button-text'>Pause</span>");
            updateTime(0,0,0,0);
        }
    // Pause button
        else if($(this).text() == "Pause"){
            clearInterval(timeUpdate);
            $(this).html("<span class='ui-button-text'>Resume</span>");
        }
    // Resume button    
        else if($(this).text() == "Resume"){
            prev_hours = parseInt($("#hours").html());
            prev_minutes = parseInt($("#minutes").html());
            prev_seconds = parseInt($("#seconds").html());
            prev_milliseconds = parseInt($("#milliseconds").html());
            
            updateTime(prev_hours, prev_minutes, prev_seconds, prev_milliseconds);
            
            $(this).html("<span class='ui-button-text'>Pause</span>");
        }
    });
    
    // Reset button onClick
    $("#reset").button().click(function(){
        if(timeUpdate) clearInterval(timeUpdate);
        setStopwatch(0,0,0,0);
        $("#start_pause_resume").html("<span class='ui-button-text'>Start</span>");      
    });
    
    // Update time in stopwatch periodically - every 25ms
    function updateTime(prev_hours, prev_minutes, prev_seconds, prev_milliseconds){
        var startTime = new Date();    // fetch current time
        
        timeUpdate = setInterval(function () {
            var timeElapsed = new Date().getTime() - startTime.getTime();    // calculate the time elapsed in milliseconds
            
            // calculate hours                
            hours = parseInt(timeElapsed / 1000 / 60 / 60) + prev_hours;
            
            // calculate minutes
            minutes = parseInt(timeElapsed / 1000 / 60) + prev_minutes;
            if (minutes > 60) minutes %= 60;
            
            // calculate seconds
            seconds = parseInt(timeElapsed / 1000) + prev_seconds;
            if (seconds > 60) seconds %= 60;
            
            // calculate milliseconds 
            milliseconds = timeElapsed + prev_milliseconds;
            if (milliseconds > 1000) milliseconds %= 1000;
            
            // set the stopwatch
            setStopwatch(hours, minutes, seconds, milliseconds);
            
        }, 25); // update time in stopwatch after every 25ms
        
    }
    
    // Set the time in stopwatch
    function setStopwatch(hours, minutes, seconds, milliseconds){
        $("#hours").html(prependZero(hours, 2));
        $("#minutes").html(prependZero(minutes, 2));
        $("#seconds").html(prependZero(seconds, 2));
        $("#milliseconds").html(prependZero(milliseconds, 3));
    }
    
    // Prepend zeros to the digits in stopwatch
    function prependZero(time, length) {
        time = new String(time);    // stringify time
        return new Array(Math.max(length - time.length + 1, 0)).join("0") + time;
    }
   });
   
   
   function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
   }
   
   // window.onclick = function(event) {
   // if (!event.target.matches('.dropbtn')) {
   
   //  var dropdowns = document.getElementsByClassName("dropdown-content");
   //  var i;
   //  for (i = 0; i < dropdowns.length; i++) {
   //    var openDropdown = dropdowns[i];
   //    if (openDropdown.classList.contains('show')) {
   //      openDropdown.classList.remove('show');
   //    }
   //  }
   // }
   // }
   
   // Set the date we're counting down to
   var countDownDate = new Date("Sep 4, 2017 15:37:25").getTime();
   
   // Update the count down every 1 second
   var x = setInterval(function() {
   
    // Get todays date and time
    var now = new Date().getTime();
    
    // Find the distance between now an the count down date
    var distance = countDownDate - now;
    //alert(distance);
    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
    // Output the result in an element with id="demo"
    //document.getElementById("demo").innerHTML = days + "d " + hours + "h "
   var result = humanise(days) + hours + "h "
    + minutes + "m " + seconds + "s ";
    // console.log(result);
    $(".demos").html(result);
   
   
    
    // If the count down is over, write some text 
    if (distance < 0) {
     // alert('fff');
      //  clearInterval(x);
        $(".demos").html('EXPIRED');
        //document.getElementById("demo").innerHTML = "EXPIRED";
    }
   }, 1000);
</script>
<!--  <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"/> -->
<link rel="stylesheet" href="<?php echo base_url();?>css/jquery.workout-timer.css" />
<!-- Example-page styles -->
<style>
   body {
   margin-bottom: 100px;
   background-color:#ECF0F1;
   }
   .event-log {
   width: 100%;
   margin: 20px 0;
   }
   .code-example {
   margin: 20px 0;
   }
</style>
<!-- Syntax highlighting -->
<!--   <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/highlight.js/8.8.0/styles/default.min.css"> -->
<!-- <script src="http://cdnjs.cloudflare.com/ajax/libs/highlight.js/8.8.0/highlight.min.js"></script>-->
<script>
   /*
       // Register sound effects
       $.WorkoutTimer.registerSound({
         path: "audio/",
         name: "bell_ring"
       });
   
       $.WorkoutTimer.registerSound({
         path: "audio/",
         name: "door_bell"
       });
   */
       // Init timers
       $(document).ready(function(){
        // alert('test');
         $('.workout-timer').workoutTimer();
       })
   $(document).on('click','.paginate_button',function(){
             $('.workout-timer').workoutTimer();
  
   });$(document).on('click','.sorting_1',function(){
             $('.workout-timer').workoutTimer();
  
   });
       $(document).on('click', '.workout-timer__play-pause', function(){
         //$('.workout-timer').workoutTimer();
         var id = $(this).attr("data-id");
         var txt = $("#counter_"+id).html();
         var data = {};
         data['id'] = id;
         data['time'] = txt;
             $.ajax({
                  type: "POST",
                  url: "<?php echo base_url();?>user/update_timer/",
                  data: data,
                  success: function(response) {
                 // $('#task_'+id).html(response);
                  },
               });
        /* alert(id);
         alert(txt);*/
       });
   
   
   
      /* var $eventLog = $('#event-log');
       var logEvent = function(eventType, eventTarget, timer) {
         var newLog = '"' + eventType + '" event triggered on #' + eventTarget.attr('id') + '\r\n';
         $eventLog.val( newLog.concat( $eventLog.val() ) );
       };
   
       $('.workout-timer-events').workoutTimer({
         onStart: logEvent,
         onRestart: logEvent,
         onPause: logEvent,
         onRoundComplete: logEvent,
         onComplete: logEvent
       });*/
     
</script>
<script>
   // Syntax highlighting
   $('pre code').each(function(i, block) {
     hljs.highlightBlock(block);
   });
   
   
</script><script type="text/javascript">
   var _gaq = _gaq || [];
   _gaq.push(['_setAccount', 'UA-36251023-1']);
   _gaq.push(['_setDomainName', 'jqueryscript.net']);
   _gaq.push(['_trackPageview']);
   
   (function() {
     var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
     ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
     var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
   })();
   
</script>
<script type="text/javascript">
   $(document).ready(function(){
       
         
         $('.dropdown-display').click(function{
           alert('hi');
             $('.dropdown-main').slideToggle(300);
         })
   })
</script>