<?php $this->load->view('includes/header');?>
 <style>
    .floatleft {
    float:left;
    }
   .floatright {
    float:right;
    }
    #next{
        background-color: #4680FF;
        text-align: center;
        width: 75px;
        border-radius: 3px;
        margin-left: 5px;
        color: white;
        font-size: bold;
        font-weight: 300;
    }
    .prv{
        background-color: #4680FF;
        text-align: center;
        width: 75px;
        border-radius: 3px;
        margin-right: 5px;
        color: white;
        font-size: bold;
        font-weight: 300;
    }
</style>                   

                    <div class="pcoded-content">

                        <div class="pcoded-inner-content">

                            <!-- Main-body start -->

                            <div class="main-body">

                                <div class="page-wrapper">

                                    <!-- Page body start -->

                                    <div class="page-body">

                                        <div class="row">

                                            <div class="col-sm-12">

                                                <!-- Register your self card start -->

                                                <div class="card">
    <!-- admin start-->
    <?php 
    /*echo "<pre>";
    print_r($client);echo "</pre>";*/ ?>
    <div class="alert alert-success" style="display:none;"
            ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Your client was successfully.</div>
            <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Please fill the required fields.</div>
            <?php 
            $error = $this->session->userdata('error');
            if($error){ echo $error; }
            $success = $this->session->userdata('success');
            if($success){ echo $success; }
            ?>
   <form id="insert_form" class="validation" method="post" action=""> 
    <div class="addnewclient_pages1 floating_set">
    
    <ul class="nav nav-tabs all_user1 md-tabs floating_set" id="tabs">
<li class="nav-item it0 bbs" value="0"><a class="nav-link active" data-toggle="tab" href="#required_information">Required information</a></li>
<li class="nav-item it1" value="1"><a class="nav-link" data-toggle="tab" href="#main_Contact">Main Contact</a></li> 
<li class="nav-item it2 hide" value="2" style="display: none;"><a class="nav-link" data-toggle="tab" href="#business">Business</a></li> 
<li class="nav-item it3" value="3"><a class="nav-link" data-toggle="tab" href="#company">Basic Details</a></li> 
<li class="nav-item it4" value="4"><a class="nav-link" data-toggle="tab" href="#income">Income</a></li> 
<li class="nav-item it5" value="5"><a class="nav-link" data-toggle="tab" href="#services">Services</a></li> 
<li class="nav-item it6 hide" value="6" style="display: none;"><a class="nav-link" data-toggle="tab" href="#account">Account & Returns</a></li> 
<li class="nav-item it11 hide" value="11" style="display: none;"><a class="nav-link" data-toggle="tab" href="#pay">PAYE Details</a></li>
<li class="nav-item it8 hide" value="8" style="display: none;"><a class="nav-link" data-toggle="tab" href="#vat">VAT</a></li> 
<li class="nav-item it7 hide" value="7" style="display: none;"><a class="nav-link" data-toggle="tab" href="#confirmation">Confirmation Statement</a></li>
<li class="nav-item it9" value="9"><a class="nav-link" data-toggle="tab" href="#registration">Registration</a></li> 
<li class="nav-item it10" value="10"><a class="nav-link" data-toggle="tab" href="#other">Other Details</a></li> 

</ul>   
<!-- tab close -->
</div>
            
        


<input type="hidden" id="tabnext"  name="tabnext" value="0" />

    <div class="management_section floating_set">

    
    <div class="card-block accordion-block">
    
    <!-- tab start -->
        <div class="tab-content">
        <div id="required_information" class="tab-pane fade in active">
            
        <div class="management_form1">  
            <!-- <div class="form-titles"><a href="#" class="waves-effect" data-toggle="modal" data-target="#default-Modal"><h2>View on Companies House</h2></a></div> -->
      <!--  <form> -->

            <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Name</label>
            <div class="col-sm-8">
                <input type="text" name="company_name" id="company_name" placeholder="Accotax Limited" value="<?php if(isset($client[0]['crm_company_name']) && ($client[0]['crm_company_name']!='') ){ echo $client[0]['crm_company_name'];}?>" class="fields">
             </div>
             </div>   

            <div class="form-group row">
            <label class="col-sm-4 col-form-label">Legal Form</label>
            <div class="col-sm-8">
            <select name="legal_form" class="form-control" id="legal_form">
            <option value="Private Limited company" <?php if(isset($client[0]['crm_legal_form']) && $client[0]['crm_legal_form']=='Private Limited company') {?> selected="selected"<?php } ?> data-id="1">Private Limited company</option>
            <option value="Public Limited company" <?php if(isset($client[0]['crm_legal_form']) && $client[0]['crm_legal_form']=='Public Limited company') {?> selected="selected"<?php } ?> data-id="1" >Public Limited company</option>
            <option value="Limited Liability Partnership" <?php if(isset($client[0]['crm_legal_form']) && $client[0]['crm_legal_form']=='Limited Liability Partnership') {?> selected="selected"<?php } ?> data-id="1">Limited Liability Partnership</option>
            <option value="Partnership" <?php if(isset($client[0]['crm_legal_form']) && $client[0]['crm_legal_form']=='Partnership') {?> selected="selected"<?php } ?> data-id="2">Partnership</option>
            <option value="Self Assessment" <?php if(isset($client[0]['crm_legal_form']) && $client[0]['crm_legal_form']=='Self Assessment') {?> selected="selected"<?php } ?> data-id="2">Self Assessment</option>
            <option value="Trust" <?php if(isset($client[0]['crm_legal_form']) && $client[0]['crm_legal_form']=='Trust') {?> selected="selected"<?php } ?> data-id="1">Trust</option>
            <option value="Charity" <?php if(isset($client[0]['crm_legal_form']) && $client[0]['crm_legal_form']=='Charity') {?> selected="selected"<?php } ?> data-id="1">Charity</option>
            <option value="Other" <?php if(isset($client[0]['crm_legal_form']) && $client[0]['crm_legal_form']=='Other') {?> selected="selected"<?php } ?> data-id="1">Other</option>
             </select>
            </div>
            </div>

             <div class="form-group row">
            <label class="col-sm-4 col-form-label">Allocation Holder</label>
            <div class="col-sm-8">
            <input type="text" name="allocation_holder" id="allocation_holder" placeholder="Allocation holder" value="<?php if(isset($client[0]['crm_allocation_holder']) && ($client[0]['crm_allocation_holder']!='') ){ echo $client[0]['crm_allocation_holder'];}?>" class="fields">
            </div>
            </div>

    </div>

<div class="button-group">
        <div class="floatleft">
           <button type="button" class="btn btn-primary btnNext th">Next</button>
        </div>
      </div>

    </div>  <!-- 1tab close-->
      
    <div id="business" class="tab-pane fade">
      <div class="management_form1 management_accordion">  
       <!-- <form>-->
                <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Trading As</label>
            <div class="col-sm-8">
                <input type="text" name="tradingas" id="tradingas" placeholder="" value="<?php if(isset($client[0]['crm_tradingas']) && ($client[0]['crm_tradingas']!='') ){ echo $client[0]['crm_tradingas'];}?>" class="fields">
             </div>
             </div>   

             <div class="form-group row date_birth">
            <label class="col-sm-4 col-form-label">Commenced Trading</label>
            <div class="col-sm-8">
                <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                <input class="form-control datepicker fields" type="text" name="commenced_trading" id="commenced_trading" placeholder="Select your date" value="<?php if(isset($client[0]['crm_commenced_trading']) && ($client[0]['crm_commenced_trading']!='') ){ echo $client[0]['crm_commenced_trading'];}?>"/>
             </div>
             </div>  

             <div class="form-group row date_birth">
            <label class="col-sm-4 col-form-label">Registered for SA</label>
            <div class="col-sm-8">
                <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                <input class="form-control datepicker fields" type="text" name="registered_for_sa" id="registered_for_sa" placeholder="Select your date" value="<?php if(isset($client[0]['crm_registered_for_sa']) && ($client[0]['crm_registered_for_sa']!='') ){ echo $client[0]['crm_registered_for_sa'];}?>" />
             </div>
             </div>  

             <div class="form-group row turnover">
            <label class="col-sm-4 col-form-label">Turnover</label>
            <div class="col-sm-8">
            <span><?php 
echo (isset($currency_set[0]['currency'])&&($currency_set[0]['currency']!=''))? $currency_set[0]['currency']:'€'; ?></span>
            <input type="text" name="business_details_turnover" id="business_details_turnover" placeholder="" value="<?php if(isset($client[0]['crm_business_details_turnover']) && ($client[0]['crm_business_details_turnover']!='') ){ echo $client[0]['crm_business_details_turnover'];}?>" class="fields">
            </div>
            </div>

            <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Nature of Business</label>
            <div class="col-sm-8">
                <input type="text" name="business_details_nature_of_business" id="business_details_nature_of_business" placeholder="" value="<?php if(isset($client[0]['crm_business_details_nature_of_business']) && ($client[0]['crm_business_details_nature_of_business']!='') ){ echo $client[0]['crm_business_details_nature_of_business'];}?>" class="fields">
             </div>
             </div> 
        <!-- </form>-->
      </div>

      <div class="button-group">
            <div class="floatleft">
               <button type="button" class="btn btn-primary btnNext">Next</button>
           </div>

           <div class="floatright">
               <button type="button" class="btn btn-primary btnPrevious">Previous</button>
           </div>
         </div>

        </div> <!-- 2tab close -->



    <!-- 3tab start -->
   
    <div id="main_Contact" class="tab-pane fade">
      <div class="management_form1 management_accordion">  
       <!-- <form>-->
             <div class="form-group row">
            <label class="col-sm-4 col-form-label">Person</label>
            <div class="col-sm-8">
            <select name="person" id="person" class="form-control fields">
            <!-- <option value="opt1" <?php if(isset($client[0]['crm_person']) && $client[0]['crm_person']=='opt1') {?> selected="selected"<?php } ?>>Select Existing Person</option> -->
            <option value="opt2" <?php if(isset($client[0]['crm_person']) && $client[0]['crm_person']=='opt2') {?> selected="selected"<?php } ?>>Select New Person</option>
            <!-- <option value="opt3" <?php if(isset($client[0]['crm_person']) && $client[0]['crm_person']=='opt3') {?> selected="selected"<?php } ?>>Select others</option> -->
             </select>
            </div>
            </div>

             <div class="form-group row">
            <label class="col-sm-4 col-form-label">Title</label>
            <div class="col-sm-8">
            <select name="title" id="title" class="form-control fields">
            <option value="Mr" <?php if(isset($client[0]['crm_title']) && $client[0]['crm_title']=='Mr') {?> selected="selected"<?php } ?>>Mr</option>
            <option value="Ms" <?php if(isset($client[0]['crm_title']) && $client[0]['crm_title']=='Ms') {?> selected="selected"<?php } ?>>Ms</option>
            <option value="Mrs" <?php if(isset($client[0]['crm_title']) && $client[0]['crm_title']=='Mrs') {?> selected="selected"<?php } ?>>Mrs</option>
            <option value="Miss" <?php if(isset($client[0]['crm_title']) && $client[0]['crm_title']=='Miss') {?> selected="selected"<?php } ?>>Miss</option>
             </select>
            </div>
            </div>

            <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">First Name</label>
            <div class="col-sm-8">
                <input type="text" name="first_name" id="first_name" placeholder="" value="<?php if(isset($client[0]['crm_first_name']) && ($client[0]['crm_first_name']!='') ){ echo $client[0]['crm_first_name'];}?>" class="fields">
             </div>
             </div>  

             <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Middle Name</label>
            <div class="col-sm-8">
                <input type="text" name="middle_name" id="middle_name" placeholder="" value="<?php if(isset($client[0]['crm_middle_name']) && ($client[0]['crm_middle_name']!='') ){ echo $client[0]['crm_middle_name'];}?>" class="fields">
             </div>
             </div>   

             <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Last Name</label>
            <div class="col-sm-8">
                <input type="text" name="last_name" id="last_name" placeholder="" value="<?php if(isset($client[0]['crm_last_name']) && ($client[0]['crm_last_name']!='') ){ echo $client[0]['crm_last_name'];}?>" class="fields">
             </div>
             </div>  

             <div class="form-group row radio_bts ">
            <label class="col-sm-4 col-form-label">Create Self Assessment Client</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right">
                <div class="turnover">
                         <span><?php 

echo (isset($currency_set[0]['currency'])&&($currency_set[0]['currency']!=''))? $currency_set[0]['currency']:'€'; ?>

                         </span>
            <input type="text" name="create_self_assessment_client" id="create_self_assessment_client" class="field fields" disabled placeholder="" value="<?php if(isset($client[0]['crm_create_self_assessment_client']) && ($client[0]['crm_create_self_assessment_client']!='') ){ echo $client[0]['crm_create_self_assessment_client'];}?>"> 
        </div>
             </div>
             </div>  

             <div class="form-group row ">
            <label class="col-sm-4 col-form-label">Client Does Self Assessment</label>
            <div class="col-sm-8">
                <input type="checkbox" name="client_does_self_assessment" id="client_does_self_assessment" class="js-small f-right fields" value="<?php if(isset($client[0]['crm_client_does_self_assessment']) && ($client[0]['crm_client_does_self_assessment']!='') ){ echo $client[0]['crm_client_does_self_assessment'];}?>">
             </div>
             </div>  

              <div class="form-group row help_icon">
            <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Perferred Name</label>
            <div class="col-sm-8">
                <input type="text" name="name" id="name" placeholder="" value="<?php if(isset($client[0]['crm_preferred_name']) && ($client[0]['crm_preferred_name']!='') ){ echo $client[0]['crm_preferred_name'];}?>" class="fields">
             </div>
             </div>  

             <div class="form-group row date_birth">
            <label class="col-sm-4 col-form-label">Date of Birth</label>
            <div class="col-sm-8">
                <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                <input class="form-control datepicker fields" type="text" name="dob" id="dob" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_date_of_birth']) && ($client[0]['crm_date_of_birth']!='') ){ echo $client[0]['crm_date_of_birth'];}?>"/>
             </div>
             </div>  

             <div class="form-group row ">
            <label class="col-sm-4 col-form-label">Email</label>
            <div class="col-sm-8">
                <input type="mail" name="email_id" id="email_id" class="form-control fields" value="<?php if(isset($client[0]['crm_email']) && ($client[0]['crm_email']!='') ){ echo $client[0]['crm_email'];}?>">
             </div>
             </div> 

             <div class="form-group row ">
            <label class="col-sm-4 col-form-label">Postal Address</label>
            <div class="col-sm-8">
                <textarea rows="4" name="postal_address" id="postal_address" class="form-control fields"><?php if(isset($client[0]['crm_postal_address']) && ($client[0]['crm_postal_address']!='') ){ echo $client[0]['crm_postal_address'];}?></textarea>
             </div>
             </div>  

              <div class="form-group row ">
            <label class="col-sm-4 col-form-label">Telephone Number</label>
            <div class="col-sm-8">
                <input type="tel" name="telephone_number" id="telephone_number" class="form-control fields" value="<?php if(isset($client[0]['crm_telephone_number']) && ($client[0]['crm_telephone_number']!='') ){ echo $client[0]['crm_telephone_number'];}?>">
             </div>
             </div> 

             <div class="form-group row ">
            <label class="col-sm-4 col-form-label">Mobile Number</label>
            <div class="col-sm-8">
                <input type="tel" name="mobile_number" id="mobile_number" class="form-control fields" value="<?php if(isset($client[0]['crm_mobile_number']) && ($client[0]['crm_mobile_number']!='') ){ echo $client[0]['crm_mobile_number'];}?>">
             </div>
             </div> 

             <div class="form-group row ">
            <label class="col-sm-4 col-form-label">NI Number</label>
            <div class="col-sm-8">
                <input type="tel" name="ni_number" id="ni_number" class="form-control fields" value="<?php if(isset($client[0]['crm_ni_number']) && ($client[0]['crm_ni_number']!='') ){ echo $client[0]['crm_ni_number'];}?>">
             </div>
             </div> 

             <div class="form-group row ">
            <label class="col-sm-4 col-form-label">Personal UTR Number</label>
            <div class="col-sm-8">
                <input type="tel" name="personal_utr_number" id="personal_utr_number" class="form-control fields" value="<?php if(isset($client[0]['crm_personal_utr_number']) && ($client[0]['crm_personal_utr_number']!='') ){ echo $client[0]['crm_personal_utr_number'];}?>">
             </div>
             </div> 

              <div class="form-group row date_birth">
            <label class="col-sm-4 col-form-label">Terms Signed</label>
            <div class="col-sm-8">
                <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                <input class="form-control datepicker fields" name="terms_signed" id="terms_signed" type="text" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_terms_signed']) && ($client[0]['crm_terms_signed']!='') ){ echo $client[0]['crm_terms_signed'];}?>"/>
             </div>
             </div>  

             <div class="form-group row radio_bts ">
            <label class="col-sm-4 col-form-label">Phone ID Verified</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right fields" name="photo_id_verified" id="photo_id_verified" <?php if(isset($client[0]['crm_photo_id_verified']) && $client[0]['crm_photo_id_verified']=='on') {?> checked="checked"<?php } ?>>
             </div>
             </div>  

             <div class="form-group row">
            <label class="col-sm-4 col-form-label">Address Verified</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right fields" name="address_verified" id="address_verified" <?php if(isset($client[0]['crm_address_verified']) && $client[0]['crm_address_verified']=='on') {?> checked="checked"<?php } ?>>
             </div>
             </div> 

       <!-- </form>-->
      </div>

<div class="button-group">
            <div class="floatleft">
               <button type="button" class="btn btn-primary btnNext">Next</button>
           </div>

           <div class="floatright">
               <button type="button" class="btn btn-primary btnPrevious">Previous</button>
           </div>
         </div>

    </div> <!-- accordion-panel -->

<!-- 3tab close -->
    
        <div id="income" class="tab-pane fade">
            <div class="management_form1 management_accordion">  
        <!--<form>-->

            <div class="form-group row">
            <label class="col-sm-4 col-form-label">Previous</label>
            <div class="col-sm-8">
                <textarea rows="4" class="fields" name="previous" id="previous"><?php if(isset($client[0]['crm_previous']) && ($client[0]['crm_previous']!='') ){ echo $client[0]['crm_previous'];}?></textarea>
             </div>
             </div> 

             <div class="form-group row">
            <label class="col-sm-4 col-form-label">Current</label>
            <div class="col-sm-8">
                <textarea rows="4" class="fields" name="current" id="current"><?php if(isset($client[0]['crm_current']) && ($client[0]['crm_current']!='') ){ echo $client[0]['crm_current'];}?></textarea>
             </div>
             </div>

             <div class="form-group row">
            <label class="col-sm-4 col-form-label">IR 35 Notes</label>
            <div class="col-sm-8">
                <textarea rows="4" class="fields" name="ir_35_notes" id="ir_35_notes"><?php if(isset($client[0]['crm_ir_35_notes']) && ($client[0]['crm_ir_35_notes']!='') ){ echo $client[0]['crm_ir_35_notes'];}?></textarea>
             </div>
             </div>  



       <!-- </form>-->
      </div> 

<div class="button-group">
            <div class="floatleft">
               <button type="button" class="btn btn-primary btnNext">Next</button>
           </div>

           <div class="floatright">
               <button type="button" class="btn btn-primary btnPrevious">Previous</button>
           </div>
         </div>

    </div> <!-- accordion-panel -->  
<!-- 4tab close -->


<!-- 5tab start -->
    <div id="other" class="tab-pane fade">
      <div class="management_form1 management_accordion">  
       <!-- <form>-->

            <div class="form-group row">
            <label class="col-sm-4 col-form-label">Previous Accountant</label>
            <div class="col-sm-8">
                <textarea rows="4" name="previous_accountant" id="previous_accountant" class="fields"><?php if(isset($client[0]['crm_previous_accountant']) && ($client[0]['crm_previous_accountant']!='') ){ echo $client[0]['crm_previous_accountant'];}?></textarea>
             </div>
             </div> 

             <div class="form-group row">
            <label class="col-sm-4 col-form-label">Referred By</label>
            <div class="col-sm-8">
                <select name="refered_by" id="refered_by" class="fields">
                                                    <option value=''></option>

                    <?php foreach ($referby as $referbykey => $referbyvalue) {
                        # code...
                    ?>
                     <option value="<?php echo $referbyvalue['crm_first_name'];?>" <?php if(isset($client[0]['crm_refered_by']) && $client[0]['crm_refered_by']==$referbyvalue['crm_first_name']) {?> selected="selected"<?php } ?>><?php echo $referbyvalue['crm_first_name'];?></option>
                    <?php } ?>
               <!--  <option value="opt1" <?php if(isset($client[0]['crm_refered_by']) && $client[0]['crm_refered_by']=='opt1') {?> selected="selected"<?php } ?>>opt1</option>
            <option value="opt2" <?php if(isset($client[0]['crm_refered_by']) && $client[0]['crm_refered_by']=='opt2') {?> selected="selected"<?php } ?>>opt2</option>
            <option value="opt3" <?php if(isset($client[0]['crm_refered_by']) && $client[0]['crm_refered_by']=='opt3') {?> selected="selected"<?php } ?>>opt3</option> -->
        </select>
             </div>
             </div>

             <div class="form-group row">
            <label class="col-sm-4 col-form-label">Allocated Office</label>
            <div class="col-sm-8">
                <input type="text" name="allocated_office" id="allocated_office" value="<?php if(isset($client[0]['crm_allocated_office']) && ($client[0]['crm_allocated_office']!='') ){ echo $client[0]['crm_allocated_office'];}?>" class="fields">
             </div>
             </div>

             <div class="form-group row date_birth">
            <label class="col-sm-4 col-form-label">Initial Contact</label>
            <div class="col-sm-8">
                <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                <input class="form-control datepicker fields" type="text" placeholder="dd-mm-yyyy" name="inital_contact" id="inital_contact" value="<?php if(isset($client[0]['crm_inital_contact']) && ($client[0]['crm_inital_contact']!='') ){ echo $client[0]['crm_inital_contact'];}?>"/>
             </div>
             </div> 

              <div class="form-group row date_birth">
            <label class="col-sm-4 col-form-label">Quote Email Sent</label>
            <div class="col-sm-8">
                <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
         <input class="form-control datepicker fields" type="text" name="quote_email_sent" id="quote_email_sent" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_quote_email_sent']) && ($client[0]['crm_quote_email_sent']!='') ){ echo $client[0]['crm_quote_email_sent'];}?>"/>
             </div>
             </div> 

              <div class="form-group row date_birth">
            <label class="col-sm-4 col-form-label">Welcome Email</label>
            <div class="col-sm-8">
                <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                <input class="form-control datepicker fields" type="text" name="welcome_email_sent" id="welcome_email_sent" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_welcome_email_sent']) && ($client[0]['crm_welcome_email_sent']!='') ){ echo $client[0]['crm_welcome_email_sent'];}?>"/>
             </div>
             </div> 

               <div class="form-group row">
            <label class="col-sm-4 col-form-label">Internal Reference</label>
            <div class="col-sm-8">
                <!-- <input type="text" name="internal_reference" id="internal_reference" value="<?php if(isset($client[0]['internal_reference']) && ($client[0]['internal_reference']!='') ){ echo $client[0]['internal_reference'];}?>"> -->
                <select name="internal_reference" id="internal_reference" class="fields">
                    <option <?php if(isset($client[0]['crm_internal_reference']) && $client[0]['crm_internal_reference']=='blank') {?> selected="selected"<?php } ?>>Blank</option>
                    <option <?php if(isset($client[0]['crm_internal_reference']) && $client[0]['crm_internal_reference']=='google') {?> selected="selected"<?php } ?>>Google</option>
                    <option <?php if(isset($client[0]['crm_internal_reference']) && $client[0]['crm_internal_reference']=='directmail') {?> selected="selected"<?php } ?>>Direct Mail</option>
                     <option <?php if(isset($client[0]['crm_internal_reference']) && $client[0]['crm_internal_reference']=='physicalsign') {?> selected="selected"<?php } ?>>Physical Sign</option>
                      <option <?php if(isset($client[0]['crm_internal_reference']) && $client[0]['crm_internal_reference']=='looklocal') {?> selected="selected"<?php } ?>>LookLocal</option>
                      <option <?php if(isset($client[0]['crm_internal_reference']) && $client[0]['crm_internal_reference']=='yell') {?> selected="selected"<?php } ?>>Yell</option>
                      <option <?php if(isset($client[0]['crm_internal_reference']) && $client[0]['crm_internal_reference']=='postoffice') {?> selected="selected"<?php } ?>>Post Office</option>
                      <option <?php if(isset($client[0]['crm_internal_reference']) && $client[0]['crm_internal_reference']=='wordofmouth') {?> selected="selected"<?php } ?>>Word Of Mouth</option>
                </select>
             </div>
             </div>

               <div class="form-group row">
            <label class="col-sm-4 col-form-label">Profession</label>
            <div class="col-sm-8">
                <input type="text" name="profession" id="profession" value="<?php if(isset($client[0]['crm_profession']) && ($client[0]['crm_profession']!='') ){ echo $client[0]['crm_profession'];}?>" class="fields">
             </div>
             </div>

               <div class="form-group row">
            <label class="col-sm-4 col-form-label">Website</label>
            <div class="col-sm-8">
                <input type="text" name="website" id="website" value="<?php if(isset($client[0]['crm_website']) && ($client[0]['crm_website']!='') ){ echo $client[0]['crm_website'];}?>" class="fields"> 
             </div>
             </div>

               <div class="form-group row">
            <label class="col-sm-4 col-form-label">Accounting System</label>
            <div class="col-sm-8">
                <select name="accounting_system" id="accounting_system" class="fields">
                    <option <?php if(isset($client[0]['crm_accounting_system']) && $client[0]['crm_accounting_system']=='Cashbook') {?> selected="selected"<?php } ?>>Cashbook</option>
                    <option <?php if(isset($client[0]['crm_accounting_system']) && $client[0]['crm_accounting_system']=='Assistanted') {?> selected="selected"<?php } ?>>Assisted</option>
                    <option <?php if(isset($client[0]['crm_accounting_system']) && $client[0]['crm_accounting_system']=='Sage') {?> selected="selected"<?php } ?>>Sage</option>
                     <option <?php if(isset($client[0]['crm_accounting_system']) && $client[0]['crm_accounting_system']=='Xero') {?> selected="selected"<?php } ?>>Xero</option>
                      <option <?php if(isset($client[0]['crm_accounting_system']) && $client[0]['crm_accounting_system']=='Quickbooks') {?> selected="selected"<?php } ?>>Quickbooks</option>
                      <option <?php if(isset($client[0]['crm_accounting_system']) && $client[0]['crm_accounting_system']=='Free Agent') {?> selected="selected"<?php } ?>>Free Agent</option>
                      <option <?php if(isset($client[0]['crm_accounting_system']) && $client[0]['crm_accounting_system']=='Excel') {?> selected="selected"<?php } ?>>Excel</option>
                      <option <?php if(isset($client[0]['crm_accounting_system']) && $client[0]['crm_accounting_system']=='Paper') {?> selected="selected"<?php } ?>>Paper</option>
                </select>
             </div>
             </div>

              <div class="form-group row">
            <label class="col-sm-4 col-form-label">Notes</label>
            <div class="col-sm-8">
                <textarea rows="4" name="notes" id="notes" class="fields"><?php if(isset($client[0]['crm_notes']) && ($client[0]['crm_notes']!='') ){ echo $client[0]['crm_notes'];}?></textarea>
             </div>
             </div>  
             <div class="form-group row">
            <label class="col-sm-4 col-form-label">Urgent</label>
            <div class="col-sm-8">
                <textarea rows="4" name="other_urgent" id="other_urgent" class="fields"><?php if(isset($client[0]['crm_other_urgent']) && ($client[0]['crm_other_urgent']!='') ){ echo $client[0]['crm_other_urgent'];}?></textarea>
             </div>
             </div> 
              
             <div class="form-group row">
            <label class="col-sm-4 col-form-label">Profile Image</label>
            <div class="col-sm-8">
            <input type="file" name="profile_image" id="profile_image">
            <input type="hidden" name="image_name" id="image_name" value="<?php if(isset($user[0]['crm_profile_pic']) && ($user[0]['crm_profile_pic']!='') ){ echo $user[0]['crm_profile_pic'];}?>" class="fields">
             </div>
             </div> 
             <div class="form-group row name_fields radio_button02">
            <label class="col-sm-4 col-form-label">Gender</label>
            <div class="col-sm-8 gender-01">
               <p><input type="radio" class="fields" id="test1" name="gender" value="male" <?php if(isset($user[0]['crm_gender']) && ($user[0]['crm_gender']=='male') ){?> checked="checked"<?php } ?>><label for="test1">Male</label></p>
                <p><input type="radio" class="fields" id="test2" name="gender" value="female" <?php if(isset($user[0]['crm_gender']) && ($user[0]['crm_gender']=='female') ){?> checked="checked"<?php } ?>><label for="test2">Female</label></p>
             </div>
             </div>
             <div class="form-group row">
            <label class="col-sm-4 col-form-label">Packages</label>
            <div class="col-sm-8">
                 <input type="text" name="packages" id="pakages" placeholder="Packages" value="<?php if(isset($user[0]['crm_packages']) && ($user[0]['crm_packages']!='') ){ echo $user[0]['crm_packages'];}?>" class="fields">
             </div>
             </div>
              <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Password</label>
            <div class="col-sm-8">
                <input type="password" name="password" id="password" placeholder="password" value="<?php if(isset($user[0]['confirm_password']) && ($user[0]['confirm_password']!='') ){ echo $user[0]['confirm_password'];}?>" class="fields">
             </div>
             </div>
              <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Confirm Passowrd</label>
            <div class="col-sm-8">
                <input type="password" name="confirm_password" id="confirm_password" placeholder="confirm password" value="<?php if(isset($user[0]['confirm_password']) && ($user[0]['confirm_password']!='') ){ echo $user[0]['confirm_password'];}?>" class="fields">
             </div>
             </div>
       <!--</form>-->
      </div>
      <div class="floation_set text-right accordion_ups">
       <!--  <a href="javascript:void()" id="accordion_close">
            <img src="<?php echo base_url();?>assets/images/acc_up.png" alt="img">
            <img src="<?php echo base_url();?>assets/images/acc_down.png" alt="img">
        </a> -->
        <!-- <a class="add_acc_client" href="javascript:void()">Add Client</a> -->
        <input type="hidden" name="user_id" id="user_id" value="<?php if(isset($client[0]['user_id']) && ($client[0]['user_id']!='') ){ echo $client[0]['user_id'];}?>">
         <input type="hidden" name="client_id" id="client_id" value="<?php if(isset($user[0]['client_id']) && ($user[0]['client_id']!='') ){ echo $user[0]['client_id'];}?>">
         <input type="hidden" id="company_house" name="company_house" value="">
        <input type="submit" class="add_acc_client" value="<?php echo (isset($client[0]['user_id']) && ($client[0]['user_id']!='') ) ? 'Update Client':'Add Client';?>" id="submit"/>
    </div>  

<div class="button-group">
            <div class="floatleft">
               <button type="button" class="btn btn-primary btnNext">Next</button>
           </div>

           <div class="floatright">
               <button type="button" class="btn btn-primary btnPrevious">Previous</button>
           </div>
         </div>

    </div> <!-- accordion-panel -->  
<!-- 5tab close -->

    <div id="services" class="tab-pane fade">
      <div class="management_form1 management_accordion">  
       <!-- <form> -->
             <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Accounts</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" <?php if(isset($client[0]['crm_accounts']) && ($client[0]['crm_accounts']!='') ){?> checked="checked"<?php } ?> data-id="accounts" >
                <div class="turnover">
                <span><?php 
echo (isset($currency_set[0]['currency'])&&($currency_set[0]['currency']!=''))? $currency_set[0]['currency']:'€'; ?></span>
                <input type="text" name="accounts" id="accounts" placeholder="" class="field fields" disabled value="<?php if(isset($client[0]['crm_accounts']) && ($client[0]['crm_accounts']!='') ){ echo $client[0]['crm_accounts'];}?>">
                </div>
             </div>
             </div>

              <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Bookkeeping</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" <?php if(isset($client[0]['crm_bookkeeping']) && ($client[0]['crm_bookkeeping']!='') ){?> checked="checked"<?php } ?>>
                <div class="turnover">
                <span><?php 
echo (isset($currency_set[0]['currency'])&&($currency_set[0]['currency']!=''))? $currency_set[0]['currency']:'€'; ?></span>
                <input type="text" class="field fields" disabled name="bookkeeping" id="bookkeeping" placeholder="" value="<?php if(isset($client[0]['crm_bookkeeping']) && ($client[0]['crm_bookkeeping']!='') ){ echo $client[0]['crm_bookkeeping'];}?>">
                </div>
             </div>
             </div>

            

              <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>CT600 Return</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" <?php if(isset($client[0]['crm_ct600_return']) && ($client[0]['crm_ct600_return']!='') ){?> checked="checked"<?php } ?>>
                <div class="turnover">
                <span><?php 
echo (isset($currency_set[0]['currency'])&&($currency_set[0]['currency']!=''))? $currency_set[0]['currency']:'€'; ?></span>
                <input type="text" name="ct600_return" id="ct600_return" class="field fields" disabled placeholder="" value="<?php if(isset($client[0]['crm_ct600_return']) && ($client[0]['crm_ct600_return']!='') ){ echo $client[0]['crm_ct600_return'];}?>">
                </div>
             </div>
             </div>

              <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Payroll</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" <?php if(isset($client[0]['crm_payroll']) && ($client[0]['crm_payroll']!='') ){?> checked="checked"<?php } ?> data-id="paye">
                <div class="turnover">
                <span><?php 
echo (isset($currency_set[0]['currency'])&&($currency_set[0]['currency']!=''))? $currency_set[0]['currency']:'€'; ?></span>
                <input type="text" name="payroll" id="payroll" placeholder="" class="field fields" disabled value="<?php if(isset($client[0]['crm_payroll']) && ($client[0]['crm_payroll']!='') ){ echo $client[0]['crm_payroll'];}?>">
                </div>
             </div>
             </div>

              <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Auto-Enrollement</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" <?php if(isset($client[0]['crm_auto_enrolment']) && ($client[0]['crm_auto_enrolment']!='') ){?> checked="checked"<?php } ?>>
                <div class="turnover">
                <span><?php 
echo (isset($currency_set[0]['currency'])&&($currency_set[0]['currency']!=''))? $currency_set[0]['currency']:'€'; ?></span>
                <input type="text" name="auto_enrolment" id="auto_enrolment" placeholder="" class="field fields" disabled value="<?php if(isset($client[0]['crm_auto_enrolment']) && ($client[0]['crm_auto_enrolment']!='') ){ echo $client[0]['crm_auto_enrolment'];}?>">
                </div>
             </div>
             </div>

              <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>VAT Returns</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" <?php if(isset($client[0]['crm_vat_returns']) && ($client[0]['crm_vat_returns']!='') ){?> checked="checked"<?php } ?> data-id="vat">
                <div class="turnover">
                <span><?php 
echo (isset($currency_set[0]['currency'])&&($currency_set[0]['currency']!=''))? $currency_set[0]['currency']:'€'; ?></span>
                <input type="text" name="vat_returns" id="vat_returns" placeholder="" class="field fields" disabled value="<?php if(isset($client[0]['crm_vat_returns']) && ($client[0]['crm_vat_returns']!='') ){ echo $client[0]['crm_vat_returns'];}?>">
                </div>
             </div>
             </div>

              <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Confirmation Statement</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" <?php if(isset($client[0]['crm_confirmation_statement']) && ($client[0]['crm_confirmation_statement']!='') ){?> checked="checked"<?php } ?> data-id="cons">
                <div class="turnover">
                <span><?php 
echo (isset($currency_set[0]['currency'])&&($currency_set[0]['currency']!=''))? $currency_set[0]['currency']:'€'; ?></span>
                <input type="text" name="confirmation_statement" id="confirmation_statement" placeholder="" class="field fields" disabled value="<?php if(isset($client[0]['crm_confirmation_statement']) && ($client[0]['crm_confirmation_statement']!='') ){ echo $client[0]['crm_confirmation_statement'];}?>">
                </div>
             </div>
             </div>

              <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>CIS</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" <?php if(isset($client[0]['crm_cis']) && ($client[0]['crm_cis']!='') ){?> checked="checked"<?php } ?>>
                <div class="turnover">
                <span><?php 
echo (isset($currency_set[0]['currency'])&&($currency_set[0]['currency']!=''))? $currency_set[0]['currency']:'€'; ?></span>
                <input type="text" name="cis" id="cis" placeholder="" class="field fields" disabled value="<?php if(isset($client[0]['crm_cis']) && ($client[0]['crm_cis']!='') ){ echo $client[0]['crm_cis'];}?>">
                </div>
             </div>
             </div>

              <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>P11D</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" <?php if(isset($client[0]['crm_p11d']) && ($client[0]['crm_p11d']!='') ){?> checked="checked"<?php } ?>>
                <div class="turnover">
                <span><?php 
echo (isset($currency_set[0]['currency'])&&($currency_set[0]['currency']!=''))? $currency_set[0]['currency']:'€'; ?></span>
                <input type="text" name="p11d" id="p11d" placeholder="" class="field fields" disabled value="<?php if(isset($client[0]['crm_p11d']) && ($client[0]['crm_p11d']!='') ){ echo $client[0]['crm_p11d'];}?>">
                </div>
             </div>
             </div>

              <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Investigation Insurance</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" <?php if(isset($client[0]['crm_invesitgation_insurance']) && ($client[0]['crm_invesitgation_insurance']!='') ){?> checked="checked"<?php } ?>>
                <div class="turnover">
                <span><?php 
echo (isset($currency_set[0]['currency'])&&($currency_set[0]['currency']!=''))? $currency_set[0]['currency']:'€'; ?></span>
                <input type="text" name="invesitgation_insurance" id="invesitgation_insurance" placeholder="" class="field fields" disabled value="<?php if(isset($client[0]['crm_invesitgation_insurance']) && ($client[0]['crm_invesitgation_insurance']!='') ){ echo $client[0]['crm_invesitgation_insurance'];}?>">
                </div>
             </div>
             </div>

              <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Registered Address</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" <?php if(isset($client[0]['crm_services_registered_address']) && ($client[0]['crm_services_registered_address']!='') ){?> checked="checked"<?php } ?>>
                <div class="turnover">
                <span><?php 
echo (isset($currency_set[0]['currency'])&&($currency_set[0]['currency']!=''))? $currency_set[0]['currency']:'€'; ?></span>
                <input type="text" name="services_registered_address" id="services_registered_address" placeholder="" class="field fields" disabled value="<?php if(isset($client[0]['crm_services_registered_address']) && ($client[0]['crm_services_registered_address']!='') ){ echo $client[0]['crm_services_registered_address'];}?>">
                </div>
             </div>
             </div>

              <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Bill Payment</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" <?php if(isset($client[0]['crm_bill_payment']) && ($client[0]['crm_bill_payment']!='') ){?> checked="checked"<?php } ?>>
                <div class="turnover">
                <span><?php 
echo (isset($currency_set[0]['currency'])&&($currency_set[0]['currency']!=''))? $currency_set[0]['currency']:'€'; ?></span>
                <input type="text" name="bill_payment" id="bill_payment" placeholder="" class="field fields" disabled value="<?php if(isset($client[0]['crm_bill_payment']) && ($client[0]['crm_bill_payment']!='') ){ echo $client[0]['crm_bill_payment'];}?>">
                </div>
             </div>
             </div>

              <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Advice</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" <?php if(isset($client[0]['crm_advice']) && ($client[0]['crm_advice']!='') ){?> checked="checked"<?php } ?>>
                <div class="turnover">
                <span><?php 
echo (isset($currency_set[0]['currency'])&&($currency_set[0]['currency']!=''))? $currency_set[0]['currency']:'€'; ?></span>
                <input type="text" name="advice" id="advice" placeholder="" class="field fields" disabled value="<?php if(isset($client[0]['crm_advice']) && ($client[0]['crm_advice']!='') ){ echo $client[0]['crm_advice'];}?>">
                </div>
             </div>
             </div>

             <div class="combing_pricing floation_set">
                <h3>Combing pricing</h3>

                 <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Monthly Charge</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" <?php if(isset($client[0]['crm_monthly_charge']) && ($client[0]['crm_monthly_charge']!='') ){?> checked="checked"<?php } ?>>
                <div class="turnover">
                <span><?php 
echo (isset($currency_set[0]['currency'])&&($currency_set[0]['currency']!=''))? $currency_set[0]['currency']:'€'; ?></span>
                <input type="text" name="monthly_charge" id="monthly_charge" placeholder="" class="field fields" disabled value="<?php if(isset($client[0]['crm_monthly_charge']) && ($client[0]['crm_monthly_charge']!='') ){ echo $client[0]['crm_monthly_charge'];}?>">
                </div>
             </div>
             </div>

              <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Annual Charge</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" <?php if(isset($client[0]['crm_annual_charge']) && ($client[0]['crm_annual_charge']!='') ){?> checked="checked"<?php } ?>>
                <div class="turnover">
                <span><?php 
echo (isset($currency_set[0]['currency'])&&($currency_set[0]['currency']!=''))? $currency_set[0]['currency']:'€'; ?></span>
                <input type="text" name="annual_charge" id="annual_charge" placeholder="" class="field fields" disabled value="<?php if(isset($client[0]['crm_annual_charge']) && ($client[0]['crm_annual_charge']!='') ){ echo $client[0]['crm_annual_charge'];}?>">
                </div>
             </div>
             </div>
         </div>

        
             
             
           


       <!-- </form>-->
      </div>

<div class="button-group">
            <div class="floatleft">
               <button type="button" class="btn btn-primary btnNext">Next</button>
           </div>

           <div class="floatright">
               <button type="button" class="btn btn-primary btnPrevious">Previous</button>
           </div>
         </div>

  </div>
<!-- 6tab close -->


        <!-- 7tab start -->
    <div id="account" class="tab-pane fade">
      <div class="management_form1 management_accordion">  
        <!--<form>-->

            <div class="form-group row help_icon date_birth">
            <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Accounts Period End</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" type="text" name="accounts_periodend" id="accounts_periodend" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_accounts_periodend']) && ($client[0]['crm_accounts_periodend']!='') ){ echo $client[0]['crm_accounts_periodend'];}?>" />
            </div>
            </div>

             <div class="form-group row help_icon date_birth">
            <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>CH Year End(Companies House)</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" type="text" name="period_end_on" id="period_end_on" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_ch_yearend']) && ($client[0]['crm_ch_yearend']!='') ){ echo $client[0]['crm_ch_yearend'];}?>"/>
            </div>
            </div>

             <div class="form-group row help_icon date_birth">
            <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>HMRC Year End</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" type="text" name="next_made_up_to" id="next_made_up_to" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_hmrc_yearend']) && ($client[0]['crm_hmrc_yearend']!='') ){ echo $client[0]['crm_hmrc_yearend'];}?>" />
            </div>
            </div>

             <div class="form-group row help_icon date_birth">
            <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>CH Accounts Next Due</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" type="text" name="next_due" id="next_due" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_ch_accounts_next_due']) && ($client[0]['crm_ch_accounts_next_due']!='') ){ echo $client[0]['crm_ch_accounts_next_due'];}?>" />
            </div>
            </div>

             <div class="form-group row help_icon date_birth">
            <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>CT600 Due</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" type="text" name="ct600_due" id="ct600_due" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_ct600_due']) && ($client[0]['crm_ct600_due']!='') ){ echo $client[0]['crm_ct600_due'];}?>" />
            </div>
            </div>

            <div class="form-group row help_icon">
            <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>Companies House Email Reminder</label>
            <div class="col-sm-8">
            <input type="checkbox" class="js-small f-right fields" name="companies_house_email_remainder" id="companies_house_email_remainder" <?php if(isset($client[0]['crm_companies_house_email_remainder']) && ($client[0]['crm_companies_house_email_remainder']!='') ){?> checked="checked"<?php } ?> >
            </div>
            </div>

            <div class="form-group row">
            <label class="col-sm-4 col-form-label">Latest Action</label>
            <div class="col-sm-8">
            <select name="latest_action" id="latest_action" class="form-control fields">
            <option value="opt1" <?php if(isset($client[0]['crm_latest_action']) && $client[0]['crm_latest_action']=='opt1') {?> selected="selected"<?php } ?>>Select One Value Only</option>
            <option value="opt2" <?php if(isset($client[0]['crm_latest_action']) && $client[0]['crm_latest_action']=='opt2') {?> selected="selected"<?php } ?>>Type 2</option>
            <option value="opt3" <?php if(isset($client[0]['crm_latest_action']) && $client[0]['crm_latest_action']=='opt3') {?> selected="selected"<?php } ?>>Type 3</option>
            </select>
            </div>
            </div>

             <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Latest Action Date</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" name="latest_action_date" id="latest_action_date" type="text" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_latest_action_date']) && ($client[0]['crm_latest_action_date']!='') ){ echo $client[0]['crm_latest_action_date'];}?>" />
            </div>
            </div>

             <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Records Received</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" name="records_received_date" id="records_received_date" type="text" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_records_received_date']) && ($client[0]['crm_records_received_date']!='') ){ echo $client[0]['crm_records_received_date'];}?>" />
            </div>
            </div>

        <!--</form>-->
      </div>

<div class="button-group">
            <div class="floatleft">
               <button type="button" class="btn btn-primary btnNext">Next</button>
           </div>

           <div class="floatright">
               <button type="button" class="btn btn-primary btnPrevious">Previous</button>
           </div>
         </div>

  </div>  
<!-- 7tab close -->
    
        <!-- 8tab start -->
    
    <div id="confirmation" class="tab-pane fade">
      <div class="management_form1 management_accordion">  
       <!-- <form>-->

            <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Confirmation Statement Date</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" type="text" name="confirm_next_made_up_to" id="confirm_next_made_up_to" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_confirmation_statement_date']) && ($client[0]['crm_confirmation_statement_date']!='') ){ echo $client[0]['crm_confirmation_statement_date'];}?>" />
            </div>
            </div>

            <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Confirmation Statement Due</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" type="text" name="confirm_next_due" id="confirm_next_due" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_confirmation_statement_due_date']) && ($client[0]['crm_confirmation_statement_due_date']!='') ){ echo $client[0]['crm_confirmation_statement_due_date'];}?>"/>
            </div>
            </div>

            <div class="form-group row">
            <label class="col-sm-4 col-form-label">Latest Action</label>
            <div class="col-sm-8">
            <select name="confirmation_latest_action" id="confirmation_latest_action" class="form-control fields">
            <option value="opt1" <?php if(isset($client[0]['crm_confirmation_latest_action']) && $client[0]['crm_confirmation_latest_action']=='opt1') {?> selected="selected"<?php } ?>>Select One Value Only</option>
            <option value="opt2" <?php if(isset($client[0]['crm_confirmation_latest_action']) && $client[0]['crm_confirmation_latest_action']=='opt2') {?> selected="selected"<?php } ?>>Type 2</option>
            <option value="opt3" <?php if(isset($client[0]['crm_confirmation_latest_action']) && $client[0]['crm_confirmation_latest_action']=='opt3') {?> selected="selected"<?php } ?>>Type 3</option>
            </select>
            </div>
            </div>

             <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Latest Action Date</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
    <input class="form-control datepicker fields" type="text" name="confirmation_latest_action_date" id="confirmation_latest_action_date" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_confirmation_latest_action_date']) && ($client[0]['crm_confirmation_latest_action_date']!='') ){ echo $client[0]['crm_confirmation_latest_action_date'];}?>"/>
            </div>
            </div>

             <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Records Received</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" type="text" name="confirmation_records_received_date" id="confirmation_records_received_date" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_confirmation_records_received_date']) && ($client[0]['crm_confirmation_records_received_date']!='') ){ echo $client[0]['crm_confirmation_records_received_date'];}?>"/>
            </div>
            </div>

            <div class="form-group row">
            <label class="col-sm-4 col-form-label">Officers</label>
            <div class="col-sm-8">
            <textarea rows="3" placeholder="" name="confirmation_officers" id="confirmation_officers" class="fields"><?php if(isset($client[0]['crm_confirmation_officers']) && ($client[0]['crm_confirmation_officers']!='') ){ echo $client[0]['crm_confirmation_officers'];}?></textarea>
            </div>
            </div>

            <div class="form-group row">
            <label class="col-sm-4 col-form-label">Share Capital</label>
            <div class="col-sm-8">
            <textarea rows="3" placeholder="" name="share_capital" id="share_capital" class="fields"><?php if(isset($client[0]['crm_share_capital']) && ($client[0]['crm_share_capital']!='') ){ echo $client[0]['crm_share_capital'];}?></textarea>
            </div>
            </div>

            <div class="form-group row">
            <label class="col-sm-4 col-form-label">Shareholders</label>
            <div class="col-sm-8">
            <textarea rows="3" placeholder="" name="shareholders" id="shareholders" class="fields"><?php if(isset($client[0]['crm_shareholders']) && ($client[0]['crm_shareholders']!='') ){ echo $client[0]['crm_shareholders'];}?></textarea>
            </div>
            </div>

            <div class="form-group row">
            <label class="col-sm-4 col-form-label">People with Significant Control</label>
            <div class="col-sm-8">
        <textarea rows="3" class="fields" placeholder="" name="people_with_significant_control" id="people_with_significant_control"><?php if(isset($client[0]['crm_people_with_significant_control']) && ($client[0]['crm_people_with_significant_control']!='') ){ echo $client[0]['crm_people_with_significant_control'];}?></textarea>
            </div>
            </div>

       <!-- </form>-->
      </div>
  </div>
<!-- 8tab close -->
    
    <div id="vat" class="tab-pane fade">
      <div class="management_form1 management_accordion">  
       <!-- <form>-->

                <div class="form-group row">
<label class="col-sm-4 col-form-label">VAT Quarters</label>
<div class="col-sm-8">
<select name="vat_quarters" id="vat_quarters" class="form-control fields">
<option value="opt1" <?php if(isset($client[0]['crm_vat_quarters']) && $client[0]['crm_vat_quarters']=='opt1') {?> selected="selected"<?php } ?>>Select One Value Only</option>
<option value="opt2" <?php if(isset($client[0]['crm_vat_quarters']) && $client[0]['crm_vat_quarters']=='opt2') {?> selected="selected"<?php } ?>>Type 2</option>
<option value="opt3" <?php if(isset($client[0]['crm_vat_quarters']) && $client[0]['crm_vat_quarters']=='opt3') {?> selected="selected"<?php } ?>>Type 3</option>
</select>
</div>
</div>
        
        <div class="form-group row help_icon date_birth">
            <label class="col-sm-4 col-form-label"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i>VAT Quarter End</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" name="vat_quater_end_date" id="vat_quater_end_date" type="text" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_vat_quater_end_date']) && ($client[0]['crm_vat_quater_end_date']!='') ){ echo $client[0]['crm_vat_quater_end_date'];}?>"/>
            </div>
            </div>

            <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Next Return Date</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
    <input class="form-control datepicker fields" type="text" name="next_return_due_date" id="next_return_due_date" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_next_return_due_date']) && ($client[0]['crm_next_return_due_date']!='') ){ echo $client[0]['crm_next_return_due_date'];}?>"/>
            </div>
            </div>

             <div class="form-group row">
<label class="col-sm-4 col-form-label">Latest Action</label>
<div class="col-sm-8">
<select name="vat_latest_action" id="vat_latest_action" class="form-control fields">
<option value="opt1" <?php if(isset($client[0]['crm_vat_latest_action']) && $client[0]['crm_vat_latest_action']=='opt1') {?> selected="selected"<?php } ?>>Select One Value Only</option>
<option value="opt2" <?php if(isset($client[0]['crm_vat_latest_action']) && $client[0]['crm_vat_latest_action']=='opt2') {?> selected="selected"<?php } ?>>Type 2</option>
<option value="opt3" <?php if(isset($client[0]['crm_vat_latest_action']) && $client[0]['crm_vat_latest_action']=='opt3') {?> selected="selected"<?php } ?>>Type 3</option>
</select>
</div>
</div>  

        <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Latest Action Date</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" type="text" name="vat_latest_action_date" id="vat_latest_action_date" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_vat_latest_action_date']) && ($client[0]['crm_vat_latest_action_date']!='') ){ echo $client[0]['crm_vat_latest_action_date'];}?>" />
            </div>
            </div>


            <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Records Received</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
<input class="form-control datepicker fields" type="text" name="vat_records_received_date" id="vat_records_received_date" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_vat_records_received_date']) && ($client[0]['crm_vat_records_received_date']!='') ){ echo $client[0]['crm_vat_records_received_date'];}?>" />
            </div>
            </div>


             <div class="form-group row">
<label class="col-sm-4 col-form-label">VAT Member State</label>
<div class="col-sm-8">
<select name="vat_member_state" id="vat_member_state" class="form-control fields">
<option value="opt1" <?php if(isset($client[0]['crm_vat_member_state']) && $client[0]['crm_vat_member_state']=='opt1') {?> selected="selected"<?php } ?>>Select One Value Only</option>
<option value="opt2" <?php if(isset($client[0]['crm_vat_member_state']) && $client[0]['crm_vat_member_state']=='opt2') {?> selected="selected"<?php } ?>>Type 2</option>
<option value="opt3" <?php if(isset($client[0]['crm_vat_member_state']) && $client[0]['crm_vat_member_state']=='opt3') {?> selected="selected"<?php } ?>>Type 3</option>
</select>
</div>
</div>  

    <div class="form-group row name_fields">
<label class="col-sm-4 col-form-label">VAT Number</label>
<div class="col-sm-8">
<input type="text" name="vat_number" id="vat_number" placeholder="543545" value="<?php if(isset($client[0]['crm_vat_number']) && ($client[0]['crm_vat_number']!='') ){ echo $client[0]['crm_vat_number'];}?>" class="fields">
</div>
</div>   

<div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Date of Registration</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" type="text" name="vat_date_of_registration" id="vat_date_of_registration" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_vat_date_of_registration']) && ($client[0]['crm_vat_date_of_registration']!='') ){ echo $client[0]['crm_vat_date_of_registration'];}?>"/>
            </div>
            </div>

            <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Effective Date</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" type="text" name="vat_effective_date" id="vat_effective_date" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_vat_effective_date']) && ($client[0]['crm_vat_effective_date']!='') ){ echo $client[0]['crm_vat_effective_date'];}?>" />
            </div>
            </div>

             <div class="form-group row turnover">
            <label class="col-sm-4 col-form-label">Estimated Turnover</label>
            <div class="col-sm-8">
            <span><?php 
echo (isset($currency_set[0]['currency'])&&($currency_set[0]['currency']!=''))? $currency_set[0]['currency']:'€'; ?></span>
            <input type="text" name="vat_estimated_turnover" id="vat_estimated_turnover" placeholder="" value="<?php if(isset($client[0]['crm_vat_estimated_turnover']) && ($client[0]['crm_vat_estimated_turnover']!='') ){ echo $client[0]['crm_vat_estimated_turnover'];}?>" class="fields">
            </div>
            </div>

            <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Transfer of Going Concern</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right fields" name="transfer_of_going_concern" id="transfer_of_going_concern" <?php if(isset($client[0]['crm_transfer_of_going_concern']) && ($client[0]['crm_transfer_of_going_concern']=='on') ){?> checked="checked"<?php } ?>>
             </div>
             </div> 

               <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Involved in Any Other Business</label>
            <div class="col-sm-8">
   <input type="checkbox" class="js-small f-right fields" name="involved_in_any_other_business" id="involved_in_any_other_business" <?php if(isset($client[0]['crm_involved_in_any_other_business']) && ($client[0]['crm_involved_in_any_other_business']=='on') ){?> checked="checked"<?php } ?>>
             </div>
             </div> 

               <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Flat Rate</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right fields" name="flat_rate" id="flat_rate" <?php if(isset($client[0]['crm_flat_rate']) && ($client[0]['crm_flat_rate']=='on') ){?> checked="checked"<?php } ?> >
             </div>
             </div> 


               <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Direct Debit</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right fields" name="direct_debit" id="direct_debit" <?php if(isset($client[0]['crm_direct_debit']) && ($client[0]['crm_direct_debit']=='on') ){?> checked="checked"<?php } ?>>
             </div>
             </div> 
        
              <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Annual Accounting Scheme</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right fields" name="annual_accounting_scheme" id="annual_accounting_scheme" <?php if(isset($client[0]['crm_annual_accounting_scheme']) && ($client[0]['crm_annual_accounting_scheme']=='on') ){?> checked="checked"<?php } ?>>
             </div>
             </div> 

             <div class="form-group row">
<label class="col-sm-4 col-form-label">Flat Rate Category</label>
<div class="col-sm-8">
<select name="flat_rate_category" id="flat_rate_category" class="form-control fields">
<option value="opt1" <?php if(isset($client[0]['crm_flat_rate_category']) && $client[0]['crm_flat_rate_category']=='advertising') {?> selected="selected"<?php } ?>>Advertising</option>
<option value="opt2" <?php if(isset($client[0]['crm_flat_rate_category']) && $client[0]['crm_flat_rate_category']=='accountancy') {?> selected="selected"<?php } ?>>Accountancy</option>
<option value="opt3" <?php if(isset($client[0]['crm_flat_rate_category']) && $client[0]['crm_flat_rate_category']=='agricultural') {?> selected="selected"<?php } ?>>Agricultural services</option>
<option value="opt3" <?php if(isset($client[0]['crm_flat_rate_category']) && $client[0]['crm_flat_rate_category']=='entertainment') {?> selected="selected"<?php } ?>>Entertainment</option>
</select>
</div>
</div>

    <div class="form-group row">
<label class="col-sm-4 col-form-label">Month of Last Quarter Submitted</label>
<div class="col-sm-8">
<select name="month_of_last_quarter_submitted" id="month_of_last_quarter_submitted" class="form-control fields">
<option value="opt1" <?php if(isset($client[0]['crm_month_of_last_quarter_submitted']) && $client[0]['crm_month_of_last_quarter_submitted']=='opt1') {?> selected="selected"<?php } ?>>Select One Value Only</option>
<option value="opt2" <?php if(isset($client[0]['crm_month_of_last_quarter_submitted']) && $client[0]['crm_month_of_last_quarter_submitted']=='opt2') {?> selected="selected"<?php } ?>>Type 2</option>
<option value="opt3" <?php if(isset($client[0]['crm_month_of_last_quarter_submitted']) && $client[0]['crm_month_of_last_quarter_submitted']=='opt3') {?> selected="selected"<?php } ?>>Type 3</option>
</select>
</div>
</div>
    
    <div class="form-group row">
<label class="col-sm-4 col-form-label">Box 5 of Last Quarter Submitted </label>
<div class="col-sm-8">
<input type="text" name="box5_of_last_quarter_submitted" id="box5_of_last_quarter_submitted" value="<?php if(isset($client[0]['crm_box5_of_last_quarter_submitted']) && ($client[0]['crm_box5_of_last_quarter_submitted']!='') ){ echo $client[0]['crm_box5_of_last_quarter_submitted'];}?>" class="fields">
</div>
</div>

      <div class="form-group row">
            <label class="col-sm-4 col-form-label">VAT Address</label>
            <div class="col-sm-8">
            <textarea rows="3" class="fields" placeholder="" name="vat_address" id="vat_address"><?php if(isset($client[0]['vat_address']) && ($client[0]['crm_vat_address']!='') ){ echo $client[0]['crm_vat_address'];}?></textarea>
            </div>
            </div>


              <div class="form-group row">
            <label class="col-sm-4 col-form-label">Notes</label>
            <div class="col-sm-8">
            <textarea rows="3" class="fields" placeholder="" name="vat_notes" id="vat_notes"><?php if(isset($client[0]['crm_vat_notes']) && ($client[0]['crm_vat_notes']!='') ){ echo $client[0]['vat_notes'];}?></textarea>
            </div>
            </div>
    


        <!--</form>-->
      </div>

<div class="button-group">
            <div class="floatleft">
               <button type="button" class="btn btn-primary btnNext">Next</button>
           </div>

           <div class="floatright">
               <button type="button" class="btn btn-primary btnPrevious">Previous</button>
           </div>
         </div>

  </div>
 
<!-- 9tab close -->
    
        <!-- 10tab start -->
    <!-- paye details -->
    <div id="pay" class="tab-pane fade">
      <div class="management_form1 management_accordion">  
        <!--<form>-->

             <div class="form-group row">
<label class="col-sm-4 col-form-label">Employers Reference</label>
<div class="col-sm-8">
<input type="text" name="employers_reference" id="employers_reference" value="<?php if(isset($client[0]['crm_employers_reference']) && ($client[0]['crm_employers_reference']!='') ){ echo $client[0]['crm_employers_reference'];}?>" class="fields">
</div>
</div>
        
         <div class="form-group row">
<label class="col-sm-4 col-form-label">Accounts Office Reference </label>
<div class="col-sm-8">
<input type="text" name="accounts_office_reference" id="accounts_office_reference" value="<?php if(isset($client[0]['crm_accounts_office_reference']) && ($client[0]['crm_accounts_office_reference']!='') ){ echo $client[0]['crm_accounts_office_reference'];}?>" class="fields">
</div>
</div>

    
 <div class="form-group row">
<label class="col-sm-4 col-form-label">Years Required</label>
<div class="col-sm-8">
<!-- <select name="years_required" id="years_required" class="form-control fields">
<option value="opt1" <?php if(isset($client[0]['crm_years_required']) && $client[0]['crm_years_required']=='opt1') {?> selected="selected"<?php } ?>>Select One Value Only</option>
<option value="opt2" <?php if(isset($client[0]['crm_years_required']) && $client[0]['crm_years_required']=='opt2') {?> selected="selected"<?php } ?>>Type 2</option>
<option value="opt3" <?php if(isset($client[0]['crm_years_required']) && $client[0]['crm_years_required']=='opt3') {?> selected="selected"<?php } ?>>Type 3</option>
</select> -->
<input type="text" name="years_required" id="years_required" value="<?php if(isset($client[0]['crm_years_required']) && ($client[0]['crm_accounts_office_reference']!='') ){ echo $client[0]['crm_accounts_office_reference'];}?>" class="fields">
</div>
</div>


 <div class="form-group row">
<label class="col-sm-4 col-form-label">Annual/Monthly Submissions</label>
<div class="col-sm-8">
    <input type="text" name="annual_or_monthly_submissions" id="annual_or_monthly_submissions" value="<?php if(isset($client[0]['crm_annual_or_monthly_submissions']) && ($client[0]['crm_annual_or_monthly_submissions']!='') ){ echo $client[0]['crm_annual_or_monthly_submissions'];}?>" class="fields">

<!-- <select name="annual_or_monthly_submissions" id="annual_or_monthly_submissions" class="form-control fields">
<option value="opt1" <?php if(isset($client[0]['crm_annual_or_monthly_submissions']) && $client[0]['crm_annual_or_monthly_submissions']=='opt1') {?> selected="selected"<?php } ?>>Select One Value Only</option>
<option value="opt2" <?php if(isset($client[0]['crm_annual_or_monthly_submissions']) && $client[0]['crm_annual_or_monthly_submissions']=='opt2') {?> selected="selected"<?php } ?>>Type 2</option>
<option value="opt3" <?php if(isset($client[0]['crm_annual_or_monthly_submissions']) && $client[0]['crm_annual_or_monthly_submissions']=='opt3') {?> selected="selected"<?php } ?>>Type 3</option>
</select> -->
</div>
</div>


 <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Irregular Monthly Pay</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right fields" name="irregular_montly_pay" id="irregular_montly_pay" <?php if(isset($client[0]['crm_irregular_montly_pay']) && ($client[0]['crm_irregular_montly_pay']=='on') ){?> checked="checked"<?php } ?>> 
             </div>
             </div> 


 <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Nil EPS</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right fields" name="nil_eps" id="nil_eps" <?php if(isset($client[0]['crm_nil_eps']) && ($client[0]['crm_nil_eps']=='on') ){?> checked="checked"<?php } ?>> 
             </div>
             </div> 

              <div class="form-group row">
<label class="col-sm-4 col-form-label">Number of Employees</label>
<div class="col-sm-8">
<input type="text" name="no_of_employees" id="no_of_employees" value="<?php if(isset($client[0]['crm_no_of_employees']) && ($client[0]['crm_no_of_employees']!='') ){ echo $client[0]['crm_no_of_employees'];}?>" class="fields">
</div>
</div>
    
     <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">First Pay Date</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" type="text" name="first_pay_date" id="first_pay_date" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_first_pay_date']) && ($client[0]['crm_first_pay_date']!='') ){ echo $client[0]['crm_first_pay_date'];}?>"/>
            </div>
            </div>

             <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">RTI Deadline</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" type="text" name="rti_deadline" id="rti_deadline" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_rti_deadline']) && ($client[0]['crm_rti_deadline']!='') ){ echo $client[0]['crm_rti_deadline'];}?>"/>
            </div>
            </div>

             <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">PAYE Scheme Ceased</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" type="text" name="paye_scheme_ceased" id="paye_scheme_ceased" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_paye_scheme_ceased']) && ($client[0]['crm_paye_scheme_ceased']!='') ){ echo $client[0]['crm_paye_scheme_ceased'];}?>"/>
            </div>
            </div>

             <div class="form-group row">
<label class="col-sm-4 col-form-label">PAYE Latest Action</label>
<div class="col-sm-8">
<select name="paye_latest_action" id="paye_latest_action" class="form-control fields">
<option value="opt1" <?php if(isset($client[0]['crm_paye_latest_action']) && $client[0]['crm_paye_latest_action']=='opt1') {?> selected="selected"<?php } ?>>Select One Value Only</option>
<option value="opt2" <?php if(isset($client[0]['crm_paye_latest_action']) && $client[0]['crm_paye_latest_action']=='opt2') {?> selected="selected"<?php } ?>>Type 2</option>
<option value="opt3" <?php if(isset($client[0]['crm_paye_latest_action']) && $client[0]['crm_paye_latest_action']=='opt3') {?> selected="selected"<?php } ?>>Type 3</option>
</select>
</div>
</div>

 <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">PAYE Latest Action Date</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" type="text" name="paye_latest_action_date" id="paye_latest_action_date" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_paye_latest_action_date']) && ($client[0]['crm_paye_latest_action_date']!='') ){ echo $client[0]['crm_paye_latest_action_date'];}?>"/>
            </div>
            </div>

             <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">PAYE Records Received</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" type="text" name="paye_records_received" id="paye_records_received" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_paye_latest_action_date']) && ($client[0]['crm_paye_latest_action_date']!='') ){ echo $client[0]['crm_paye_latest_action_date'];}?>"/>
            </div>
            </div>

             <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Next P11D Return Due</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" type="text" name="next_p11d_return_due" id="next_p11d_return_due" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_next_p11d_return_due']) && ($client[0]['crm_next_p11d_return_due']!='') ){ echo $client[0]['crm_next_p11d_return_due'];}?>"/>
            </div>
            </div>

             <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Latest P11D Submitted</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" type="text" name="latest_p11d_submitted" id="latest_p11d_submitted" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_latest_p11d_submitted']) && ($client[0]['crm_latest_p11d_submitted']!='') ){ echo $client[0]['crm_latest_p11d_submitted'];}?>"/>
            </div>
            </div>

             <div class="form-group row">
<label class="col-sm-4 col-form-label">P11D Latest Action</label>
<div class="col-sm-8">
<select name="p11d_latest_action" id="p11d_latest_action" class="form-control fields">
<option value="opt1" <?php if(isset($client[0]['crm_p11d_latest_action']) && $client[0]['crm_p11d_latest_action']=='opt1') {?> selected="selected"<?php } ?>>Select One Value Only</option>
<option value="opt2" <?php if(isset($client[0]['crm_p11d_latest_action']) && $client[0]['crm_p11d_latest_action']=='opt2') {?> selected="selected"<?php } ?>>Type 2</option>
<option value="opt3" <?php if(isset($client[0]['crm_p11d_latest_action']) && $client[0]['crm_p11d_latest_action']=='opt3') {?> selected="selected"<?php } ?>>Type 3</option>
</select>
</div>
</div>
    

    <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">P11D Latest Action Date</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" type="text" name="p11d_latest_action_date" id="p11d_latest_action_date" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_p11d_latest_action_date']) && ($client[0]['crm_p11d_latest_action_date']!='') ){ echo $client[0]['crm_p11d_latest_action_date'];}?>"/>
            </div>
            </div>

            <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">P11D Records Received</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" type="text" name="p11d_records_received" id="p11d_records_received" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_p11d_records_received']) && ($client[0]['crm_p11d_records_received']!='') ){ echo $client[0]['crm_p11d_records_received'];}?>"/>
            </div>
            </div>

             <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">CIS Contractor</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right fields" name="cis_contractor" id="cis_contractor" <?php if(isset($client[0]['crm_cis_contractor']) && ($client[0]['crm_cis_contractor']=='on') ){?> checked="checked"<?php } ?>>
             </div>
             </div>

             <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">CIS Subcontractor</label>
            <div class="col-sm-8">
                <input type="checkbox" name="cis_subcontractor" id="cis_subcontractor" class="js-small f-right fields" <?php if(isset($client[0]['crm_cis_subcontractor']) && ($client[0]['crm_cis_subcontractor']=='on') ){?> checked="checked"<?php } ?>>
             </div>
             </div> 

             <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">CIS Deadline</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" type="text" name="cis_deadline" id="cis_deadline" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_cis_deadline']) && ($client[0]['crm_cis_deadline']!='') ){ echo $client[0]['crm_cis_deadline'];}?>" />
            </div>
            </div>

            <div class="form-group row">
            <label class="col-sm-4 col-form-label">Auto-Enrollement Latest Action</label>
            <div class="col-sm-8">
            <select name="auto_enrolment_latest_action" id="auto_enrolment_latest_action" class="form-control fields">
            <option value="opt1" <?php if(isset($client[0]['crm_auto_enrolment_latest_action']) && $client[0]['crm_auto_enrolment_latest_action']=='opt1') {?> selected="selected"<?php } ?>>Select One Value Only</option>
            <option value="opt2" <?php if(isset($client[0]['crm_auto_enrolment_latest_action']) && $client[0]['crm_auto_enrolment_latest_action']=='opt2') {?> selected="selected"<?php } ?>>Type 2</option>
            <option value="opt3" <?php if(isset($client[0]['crm_auto_enrolment_latest_action']) && $client[0]['crm_auto_enrolment_latest_action']=='opt3') {?> selected="selected"<?php } ?>>Type 3</option>
            </select>
            </div>
            </div>


            <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Auto-Enrollement Latest Action Date</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" type="text" name="auto_enrolment_latest_action_date" id="auto_enrolment_latest_action_date" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_auto_enrolment_latest_action_date']) && ($client[0]['crm_auto_enrolment_latest_action_date']!='') ){ echo $client[0]['crm_auto_enrolment_latest_action_date'];}?>" />
            </div>
            </div>

            <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Auto-Enrollement Record Received</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" type="text" name="auto_enrolment_records_received" id="auto_enrolment_records_received" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_auto_enrolment_records_received']) && ($client[0]['crm_auto_enrolment_records_received']!='') ){ echo $client[0]['crm_auto_enrolment_records_received'];}?>"/>
            </div>
            </div>

            <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Auto-Enrollement Staging</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" type="text" name="auto_enrolment_staging" id="auto_enrolment_staging" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_auto_enrolment_staging']) && ($client[0]['crm_auto_enrolment_staging']!='') ){ echo $client[0]['crm_auto_enrolment_staging'];}?>"/>
            </div>
            </div>

            <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Postponement Date</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" type="text" name="postponement_date" id="postponement_date" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_postponement_date']) && ($client[0]['crm_postponement_date']!='') ){ echo $client[0]['crm_postponement_date'];}?>"/>
            </div>
            </div>

            <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">The Pensions Regulator Opt Out Date</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" type="text" name="the_pensions_regulator_opt_out_date" id="the_pensions_regulator_opt_out_date" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_the_pensions_regulator_opt_out_date']) && ($client[0]['crm_the_pensions_regulator_opt_out_date']!='') ){ echo $client[0]['crm_the_pensions_regulator_opt_out_date'];}?>"/>
            </div>
            </div>

           <!--  <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Re-Enrolment Date</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker" type="text" name="re_enrolment_date" id="re_enrolment_date" placeholder="dd/mm/yyy" value="<?php if(isset($client[0]['re_enrolment_date']) && ($client[0]['re_enrolment_date']!='') ){ echo $client[0]['re_enrolment_date'];}?>" />
            </div>
            </div>

             <div class="form-group row">
<label class="col-sm-4 col-form-label">Pension Provider </label>
<div class="col-sm-8">
<input type="text" name="pension_provider" id="pension_provider" value="<?php if(isset($client[0]['pension_provider']) && ($client[0]['pension_provider']!='') ){ echo $client[0]['pension_provider'];}?>"> 
</div>
</div>   -->

<div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Re-Enrolment Date</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" type="text" name="paye_re_enrolment_date" id="paye_re_enrolment_date" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_paye_re_enrolment_date']) && ($client[0]['crm_paye_re_enrolment_date']!='') ){ echo $client[0]['crm_paye_re_enrolment_date'];}?>"/>
            </div>
            </div>

              <div class="form-group row">
<label class="col-sm-4 col-form-label">Pension Provider</label>
<div class="col-sm-8">
<input type="text" name="paye_pension_provider" id="paye_pension_provider" value="<?php if(isset($client[0]['crm_paye_pension_provider']) && ($client[0]['crm_paye_pension_provider']!='') ){ echo $client[0]['crm_paye_pension_provider'];}?>" class="fields">
</div>
</div>  

              <div class="form-group row">
<label class="col-sm-4 col-form-label">Pension ID </label>
<div class="col-sm-8">
<input type="text" name="pension_id" id="pension_id" value="<?php if(isset($client[0]['crm_pension_id']) && ($client[0]['crm_pension_id']!='') ){ echo $client[0]['crm_pension_id'];}?>" class="fields">
</div>
</div>  
    
        <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Declaration of Compliance Due</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" type="text" name="declaration_of_compliance_due_date" id="declaration_of_compliance_due_date" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_declaration_of_compliance_due_date']) && ($client[0]['crm_declaration_of_compliance_due_date']!='') ){ echo $client[0]['crm_declaration_of_compliance_due_date'];}?>"/>
            </div>
            </div>

              <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Declaration of Compliance Submission</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" type="text" placeholder="dd-mm-yyyy" name="declaration_of_compliance_submission" id="declaration_of_compliance_submission" value="<?php if(isset($client[0]['crm_declaration_of_compliance_submission']) && ($client[0]['crm_declaration_of_compliance_submission']!='') ){ echo $client[0]['crm_declaration_of_compliance_submission'];}?>"/>
            </div>
            </div>

              <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">Pension Deadline</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" type="text" id="pension_deadline" name="pension_deadline" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_pension_deadline']) && ($client[0]['crm_pension_deadline']!='') ){ echo $client[0]['crm_pension_deadline'];}?>" />
            </div>
            </div>

            <div class="form-group row">
<label class="col-sm-4 col-form-label">Notes</label>
<div class="col-sm-8">
<textarea rows="3" placeholder="" name="note" id="note" class="fields"><?php if(isset($client[0]['crm_note']) && ($client[0]['crm_note']!='') ){ echo $client[0]['crm_note'];}?></textarea>
</div>
</div>

        <!--</form>-->
      </div>

<div class="button-group">
            <div class="floatleft">
               <button type="button" class="btn btn-primary btnNext">Next</button>
           </div>

           <div class="floatright">
               <button type="button" class="btn btn-primary btnPrevious">Previous</button>
           </div>
         </div>


  </div> 
<!-- 10tab close -->


    <!-- 11tab start -->
    <div id="registration" class="tab-pane fade">
      <div class="management_form1 management_accordion">  
        <!--<form>-->

              <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-4 col-form-label">Registration Fee Paid</label>
            <div class="col-sm-8">
                <input type="checkbox" class="js-small f-right" <?php if(isset($client[0]['crm_registration_fee_paid']) && ($client[0]['crm_registration_fee_paid']!='0') ){ ?>checked="checked"<?php } ?>>
                <div class="turnover">
                <span><?php 
echo (isset($currency_set[0]['currency'])&&($currency_set[0]['currency']!=''))? $currency_set[0]['currency']:'€'; ?></span>
                <input type="text" name="registration_fee_paid" id="registration_fee_paid" class="field fields" disabled placeholder="" value="<?php if(isset($client[0]['crm_registration_fee_paid']) && ($client[0]['crm_registration_fee_paid']!='') ){ echo $client[0]['crm_registration_fee_paid'];}?>">
                </div>
             </div>
             </div>

             <div class="form-group row  date_birth">
            <label class="col-sm-4 col-form-label">64-8 Registration</label>
            <div class="col-sm-8">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input class="form-control datepicker fields" type="text" name="64_8_registration" id="64_8_registration" placeholder="dd-mm-yyyy" value="<?php if(isset($client[0]['crm_64_8_registration']) && ($client[0]['crm_64_8_registration']!='') ){ echo $client[0]['crm_64_8_registration'];}?>"/>
            </div>
            </div>

        <!--</form>-->
      </div>


      <div class="button-group">
            <div class="floatleft">
               <button type="button" class="btn btn-primary btnNext">Next</button>
           </div>

           <div class="floatright">
               <button type="button" class="btn btn-primary btnPrevious">Previous</button>
           </div>
         </div>


  </div>
<!-- 11tab close -->

<!-- 12tab start -->
    <div id="company" class="tab-pane fade">
      <div class="management_form1 management_accordion">  
       <!--<form>-->

            <div class="form-group row">
            <label class="col-sm-4 col-form-label">Company Number</label>
            <div class="col-sm-8">
            <input type="number" name="company_number" id="company_number" placeholder="07725439" value="<?php if(isset($client[0]['crm_company_number']) && ($client[0]['crm_company_number']!='') ){ echo $client[0]['crm_company_number'];}?>" class="fields" >
             </div>
             </div>  

            <div class="form-group row">
            <label class="col-sm-4 col-form-label">Company URL</label>
            <div class="col-sm-8">
            <!-- <a href="#" id="company_url_anchor">Click Here</a> -->
            <input type="text" class="fields" name="company_url" id="company_url" value="<?php if(isset($client[0]['crm_company_url']) && ($client[0]['crm_company_url']!='') ){ echo $client[0]['crm_company_url'];}?>" placeholder="www.google.com">
             </div>
             </div> 

            <div class="form-group row">
            <label class="col-sm-4 col-form-label">Officers URL</label>
            <div class="col-sm-8">
           <!--  <a href="#" id="officers_url_anchor">Click Here</a> -->
            <input type="text" name="officers_url" id="officers_url" value="<?php if(isset($client[0]['crm_officers_url']) && ($client[0]['crm_officers_url']!='') ){ echo $client[0]['crm_officers_url'];}?>" class="fields" placeholder="www.google.com">
             </div>
             </div> 

            <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Company Name</label>
            <div class="col-sm-8">
                <input type="text" name="company_name1" id="company_name1" placeholder="Accotax Limited" value="<?php if(isset($client[0]['crm_company_name1']) && ($client[0]['crm_company_name1']!='') ){ echo $client[0]['crm_company_name1'];}?>" class="fields">
             </div>
             </div>   

            <div class="form-group row">
            <label class="col-sm-4 col-form-label">Registered Address</label>
            <div class="col-sm-8">
            <textarea rows="3" placeholder="c/o Accotax Limited 12 London Road Modern Surney" name="register_address" id="register_address" class="fields"><?php if(isset($client[0]['crm_register_address']) && ($client[0]['crm_register_address']!='') ){ echo $client[0]['crm_register_address'];}?></textarea>
            </div>
            </div>
            
             <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Company Status</label>
            <div class="col-sm-8">
                <input type="text" name="company_status" id="company_status" placeholder="active" value="<?php if(isset($client[0]['crm_company_status']) && ($client[0]['crm_company_status']!='') ){ echo $client[0]['crm_company_status'];}?>" class="fields">
             </div>
             </div>

             <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Business Type</label>
            <div class="col-sm-8">
                <input type="text" name="company_type" id="company_type" placeholder="Private Limited Company" value="<?php if(isset($client[0]['crm_company_type']) && ($client[0]['crm_company_type']!='') ){ echo $client[0]['crm_company_type'];}?>" class="fields">
             </div>
             </div>

             <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Company SIC</label>
            <div class="col-sm-8">
               <input type="text" name="company_sic" class="fields" id="company_sic" placeholder="999999-Dormant Company" value="<?php if(isset($client[0]['crm_company_sic']) && ($client[0]['crm_company_sic']!='0') ){
$codes=explode(',',$client[0]['crm_sic_codes']);

$code=$this->db->query('SELECT * FROM sic_code WHERE COL1="'.$codes[0].'"')->row_array();
echo $code['COL1'];

            }
                ?>"> 
           <!--  <select name="company_sic" id="company_sic" class="form-control" >

            <?php if(isset($client[0]['crm_company_sic']) && ($client[0]['crm_company_sic']!='0') ){
$codes=explode(',',$client[0]['crm_sic_codes']);
foreach($codes as $key => $val){
$code=$this->db->query('SELECT * FROM sic_code WHERE COL1="'.$val.'"')->row_array();
echo '<option value='.$code['COL1'].'>'.$code['COL1'].'-'.$code['COL2'].'</option>';

}

            }
                ?>

            </select> -->
            <input type="hidden" name="sic_codes" id="sic_codes" value="">
             </div>
             </div>
        
             
              <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label"> Authentication Code</label>
            <div class="col-sm-8">
                <input type="text" class="fields" name="companies_house_authorisation_code" id="companies_house_authorisation_code" placeholder="autentication code" value="<?php if(isset($client[0]['crm_companies_house_authorisation_code']) && ($client[0]['crm_companies_house_authorisation_code']!='') ){ echo $client[0]['crm_companies_house_authorisation_code'];}?>">
             </div>
             </div>
              <div class="form-group row name_fields">
            <label class="col-sm-4 col-form-label">Company UTR</label>
            <div class="col-sm-8">
                <input type="text" class="fields" name="company_utr" id="company_utr" placeholder="" value="<?php if(isset($client[0]['crm_company_utr']) && ($client[0]['crm_company_utr']!='') ){ echo $client[0]['crm_company_utr'];}?>">
             </div>
             </div> 
             

             <div class="form-group row date_birth">
            <label class="col-sm-4 col-form-label">Incorporation Date</label>
            <div class="col-sm-8">
                <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                <input class="form-control datepicker fields" type="text" name="date_of_creation" id="date_of_creation" placeholder="Incorporation date" value="<?php if(isset($client[0]['crm_incorporation_date']) && ($client[0]['crm_incorporation_date']!='') ){ echo $client[0]['crm_incorporation_date'];}?>"/>
             </div>
             </div>   


            <div class="form-group row turnover">
            <label class="col-sm-4 col-form-label">Managed By</label>
            <div class="col-sm-8">
            <select name="managed" class="form-control fields" id="managed">
                <?php foreach($Managed_by_person as $person_key => $person_value){?>
            <option value="<?php echo $person_value['id'];?>" <?php if(isset($client[0]['crm_managed']) && $client[0]['crm_managed']==$person_value['id']) {?> selected="selected"<?php } ?>><?php echo $person_value['name'];?></option>
            <?php } ?>
            <!-- <option value="opt2" <?php if(isset($client[0]['crm_managed']) && $client[0]['crm_managed']=='opt2') {?> selected="selected"<?php } ?>>Type 2</option>
            <option value="opt3" <?php if(isset($client[0]['crm_managed']) && $client[0]['crm_managed']=='opt3') {?> selected="selected"<?php } ?>>Type 3</option> -->
            </select>
            </div>
            </div> 
 
       
      </div>

<div class="button-group">
            <div class="floatleft">
               <button type="button" class="btn btn-primary btnNext">Next</button>
           </div>

           <div class="floatright">
               <button type="button" class="btn btn-primary btnPrevious">Previous</button>
           </div>
         </div>

  </div>
<!-- 12tab close -->

    </div>  <!-- tab-content -->
    
</div>


</div> <!-- managementclose -->



 </form>  

<input type="hidden" id="userurl" name="userurl" value="<?php echo base_url() ?>Client/register_user_exists">
<div class="modal fade all_layout_modal" id="default-Modal" tabindex="-1" role="dialog">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Search Companines House</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
        </div>
        <div class="modal-body">
        <div class="input-group input-group-button input-group-primary modal-search">
<input type="text" class="form-control" placeholder="Search here..." id="searchCompany">
<button class="btn btn-primary input-group-addon" id="basic-addon1">
<i class="ti-search" aria-hidden="true"></i>
</button>
</div>

<div class="british_gas1 trading_limited" id="searchresult">
 </div>    

<div class="british_view" style="display:none" id="companyprofile">  
<!-- <div class="br_company_profile">
<h3>british gas trading limited</h3>
<div class="company_numbers">
<span>Company Number</span>    
<p>03078711</p>
</div>
<div class="company_numbers">
<span>Company Status</span>    
<p>Active</p>
</div>    
<div class="company_numbers">
<span>Incorporated Date</span>    
<p>06/07/1995</p>
</div>
<div class="company_numbers">
<span>Accounts Reference Date</span>    
<p>31/12</p>
</div>
<div class="company_numbers">
<span>Charges</span>    
<p><strong>1</strong></p>
</div>
<div class="company_numbers">
<span>Current Directors</span>    
<p>R.O.Y. International PO Box 13056 ISL-61130 TEL-AVIV ISRAEL</p>
</div>
<div class="company_numbers">
<span>Registered Directors</span>    
<p>The Israel Philatelic Service 12 Shderot Yerushalayim 68021 Tel Aviv - Yafo ISRAEL</p>
</div>
<div class="company_chooses">
<a href="#">select company</a>
<a href="#">View on Companies House</a>
</div>
</div> -->
</div>  

<div class="main_contacts" id="selectcompany" style="display:none">     
<h3>british gas trading limited</h3>
<div class="trading_tables"> 
<div class="trading_rate">
<p>
<span><strong>BARBARO,Gab</strong></span>
<span>Born 1971</span>
<span><a href="#">Use as Main Contact</a></span>
</p> 
</div>
</div>  
</div>  






</div> <!-- modalbody -->
</div>
</div>
</div> <!-- modal-close -->










<!-- admin close-->


</div>

</div>

</div>

</div>

                                    <!-- Page body end -->

                                </div>

                            </div>

                            <!-- Main-body end -->



                            <div id="styleSelector">



                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>


<!-- ajax loader -->

<div class="LoadingImage" ></div>

<style>
.LoadingImage {
    display : none;
   position : fixed;
   z-index: 100;
   background-image : url('<?php echo site_url()?>assets/images/ajax-loader.gif');
   background-color:#666;
   opacity : 0.4;
   background-repeat : no-repeat;
   background-position : center;
   left : 0;
   bottom : 0;
   right : 0;
   top : 0;
 
}

</style>
<!-- ajax loader end-->


<?php //$this->load->view('includes/session_timeout');?>
    <!-- Warning Section Starts -->

    <!-- Older IE warning message -->

    <!--[if lt IE 10]>

<div class="ie-warning">

    <h1>Warning!!</h1>

    <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>

    <div class="iew-container">

        <ul class="iew-download">

            <li>

                <a href="http://www.google.com/chrome/">

                    <img src="assets/images/browser/chrome.png" alt="Chrome">

                    <div>Chrome</div>

                </a>

            </li>

            <li>

                <a href="https://www.mozilla.org/en-US/firefox/new/">

                    <img src="assets/images/browser/firefox.png" alt="Firefox">

                    <div>Firefox</div>

                </a>

            </li>

            <li>

                <a href="http://www.opera.com">

                    <img src="assets/images/browser/opera.png" alt="Opera">

                    <div>Opera</div>

                </a>

            </li>

            <li>

                <a href="https://www.apple.com/safari/">

                    <img src="assets/images/browser/safari.png" alt="Safari">

                    <div>Safari</div>

                </a>

            </li>

            <li>

                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">

                    <img src="assets/images/browser/ie.png" alt="">

                    <div>IE (9 & above)</div>

                </a>

            </li>

        </ul>

    </div>

    <p>Sorry for the inconvenience!</p>

</div>

<![endif]-->

    <!-- Warning Section Ends -->

    <!-- Required Jquery -->

    <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-ui/js/jquery-ui.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>bower_components/popper.js/js/popper.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap/js/bootstrap.min.js"></script>

    <!-- j-pro js -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.ui.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.maskedinput.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.j-pro.js"></script>

    <!-- jquery slimscroll js -->

    <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>

    <!-- modernizr js -->

    <script type="text/javascript" src="<?php echo base_url();?>bower_components/switchery/js/switchery.min.js"></script>

    

    <script type="text/javascript" src="<?php echo base_url();?>bower_components/modernizr/js/css-scrollbars.js"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <!-- Bootstrap date-time-picker js -->
<!--     <script type="text/javascript" src="assets/pages/advance-elements/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="assets/pages/advance-elements/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
    <script type="text/javascript" src="bower_components/datedropper/js/datedropper.min.js"></script> -->




    <!-- Custom js -->

    




    



    

    <script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/pages/advance-elements/custom-picker.js"></script>


    <script src="<?php echo base_url();?>assets/js/pcoded.min.js"></script>

    <script src="<?php echo base_url();?>assets/js/demo-12.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/common_script.js"></script>

    

  <script>

    $( document ).ready(function() {

         var today = new Date();
        $('#terms_signed').datepicker({
            dateFormat: 'dd-mm-yy',
            autoclose:true,
            endDate: "today",
            maxDate: today
        }).on('changeDate', function (ev) {
                $(this).datepicker('hide');
            });


        $('#terms_signed').keyup(function () {
            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9^-]/g, '');
            }
        });

          $('#dob').datepicker({
            dateFormat: 'dd-mm-yy',
            autoclose:true,
            endDate: "today",
            maxDate: today
        }).on('changeDate', function (ev) {
                $(this).datepicker('hide');
            });


        $('#dob').keyup(function () {
            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9^-]/g, '');
            }
        });



          var today = new Date();
        $('#date_of_creation').datepicker({
            dateFormat: 'dd-mm-yy',
            autoclose:true,
            endDate: "today",
            maxDate: today
        }).on('changeDate', function (ev) {
                $(this).datepicker('hide');
            });


        $('#date_of_creation').keyup(function () {
            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9^-]/g, '');
            }
        });

    
        var date = $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();

        // Multiple swithces
        var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));

        elem.forEach(function(html) {
            var switchery = new Switchery(html, {
                color: '#1abc9c',
                jackColor: '#fff',
                size: 'small'
            });
        });

        $('#accordion_close').on('click', function(){
                $('#accordion').slideToggle(300);
                $(this).toggleClass('accordion_down');
        });

$("#legal_form").change(function(){
   var legal_form = $(':selected',this).data('id');
  // alert(legal_form);
   if(legal_form=='2'){
    $(".it2").attr("style", "display:block");
    $(".it2").removeClass("hide");
   } else{
    $(".it2").attr("style", "display:none");
    $(".it2").addClass("hide");
   }

   });
});
    </script>


    <script>
 /* $( "#searchCompany" ).autocomplete({
        source: function(request, response) {
            //console.info(request, 'request');
            //console.info(response, 'response');

            $.ajax({
                //q: request.term,
                url: "<?=site_url('client/SearchCompany')?>",
                data: { term: $("#searchCompany").val()},
                dataType: "json",
                type: "POST",
                success: function(data) {
                    //alert(JSON.stringify(data.searchrec));
                    //add(data.searchrec);
                    //console.log(data);
                    $(".ui-autocomplete").css('display','block');
                    response(data.searchrec);
                }
            
            });
        },
        minLength: 2
    });*/
$("#searchCompany").keyup(function(){
     var currentRequest = null;    
     var term = $(this).val();
     $("#searchresult").show();
     $('#selectcompany').hide();
      $(".LoadingImage").show();
     currentRequest = $.ajax({
        url: '<?php echo base_url();?>client/SearchCompany/',
        type: 'post',
        data: { 'term':term },
        beforeSend : function()    {           
        if(currentRequest != null) {
            currentRequest.abort();
        }
    },
        success: function( data ){
            $("#searchresult").html(data);
             $(".LoadingImage").hide();
            },
            error: function( errorThrown ){
                console.log( errorThrown );
            }
        });
   });
var xhr = null;
function getCompanyRec(companyNo)
{
    $(".LoadingImage").show();
     if( xhr != null ) {
                xhr.abort();
                xhr = null;
        }
     
    xhr = $.ajax({
        url: '<?php echo base_url();?>client/CompanyDetails/',
        type: 'post',
        data: { 'companyNo':companyNo },
        success: function( data ){
            //alert(data);
            $("#searchresult").hide();
            $('#companyprofile').show();
            $("#companyprofile").html(data);
            $(".LoadingImage").hide();
            
            },
            error: function( errorThrown ){
                console.log( errorThrown );
            }
        });}


    function getCompanyView(companyNo)
{
    $(".LoadingImage").show();
     if( xhr != null ) {
                xhr.abort();
                xhr = null;
        }
     
    xhr = $.ajax({
        url: '<?php echo base_url();?>client/selectcompany/',
        type: 'post',
        dataType: 'JSON',
        data: { 'companyNo':companyNo },
        success: function( data ){
            //alert(data);
            $("#searchresult").hide();
            $('#companyprofile').hide();
            $('#selectcompany').show();
            $("#selectcompany").html(data.html);
// append form field value
$("#company_house").val('1');
$("#company_name").val(data.company_name);
$("#company_name1").val(data.company_name);
$("#company_number").val(data.company_number);
$("#company_url").val(data.company_url);
$("#company_url_anchor").attr("href",data.company_url);
$("#officers_url").val(data.officers_url);
$("#officers_url_anchor").attr("href",data.officers_url);
$("#company_status").val(data.company_status);
$("#company_type").val(data.company_type);
$("#company_sic").append(data.company_sic);
$("#sic_codes").val(data.sic_codes);

$("#register_address").val(data.address1+"\n"+data.address2+"\n"+data.locality+"\n"+data.postal);
$("#allocation_holder").val(data.allocation_holder);
$("#date_of_creation").val(data.date_of_creation);
$("#period_end_on").val(data.period_end_on);
$("#next_made_up_to").val(data.next_made_up_to);
$("#next_due").val(data.next_due);
$("#confirm_next_made_up_to").val(data.confirm_next_made_up_to);
$("#confirm_next_due").val(data.confirm_next_due);

$("#tradingas").val(data.company_name);
$("#business_details_nature_of_business").val(data.nature_business);

$(".LoadingImage").hide();
            
            },
            error: function( errorThrown ){
                console.log( errorThrown );
            }
        });}

    function getmaincontact(companyNo,appointments)
{
    $(".LoadingImage").show();
     if( xhr != null ) {
                xhr.abort();
                xhr = null;
        }
     
    xhr = $.ajax({
        url: '<?php echo base_url();?>client/maincontact/',
        type: 'post',
        dataType: 'JSON',
        data: { 'companyNo':companyNo,'appointments': appointments },
        success: function( data ){
          
            //$("#default-Modal,.modal-backdrop.show").hide();
               $('#default-Modal').modal('hide');

// append form field value

$("#first_name").val(data.first_name);
$("#middle_name").val(data.middle_name);
$("#last_name").val(data.last_name);
$(".LoadingImage").hide();

            
            },
            error: function( errorThrown ){
                console.log( errorThrown );
            }
        });}
</script>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
/*    jQuery.validator.addMethod("noSpace", function(value, element) { 
  return value.indexOf(" ") < 0 && value != ""; 
}, "No space please and don't leave it empty");*/
$("#insert_form").validate({

       ignore: false,
   // errorClass: "error text-warning",
    //validClass: "success text-success",
    /*highlight: function (element, errorClass) {
        //alert('em');
       // $(element).fadeOut(function () {
           // $(element).fadeIn();
        //});
    },*/
                        rules: {
                        company_name: {required: true},
                        company_number: {required: true},
                        company_status: {required: true},
                        name:{required: true},
                    email_id:{
 required: true,
  email: true,
},
                        /*profile_image: {required: true, accept: "jpg|jpeg|png|gif"},*/
                         profile_image: {
    required: true,
  accept: "jpg|jpeg|png|gif"
},
                        /*user_name: {required: true},
                        emailid: {required: true,email:true},*/
                        gender:{required: true},
                        packages:{required: true},
                        password:{required: true,minlength : 5},
                        company_number:{required: true,maxlength : 10},
                        confirm_password : { minlength : 5,equalTo : "#password"},
                        },
                        errorElement: "span" , 
                        errorClass: "field-error",                             
                         messages: {
                          company_name: "Give company name",
                          company_number: "Give company number",
                          company_status: "Give company status",
                          name: { required: "Please enter your User Name"},
                          email_id:{
    email:"Please enter a valid email address", 
    required:"Please enter a email address"},
/*emailid:{
       required:"Please enter a email address",
       email:"Please enter a valid email address"
   
   },
user_name:"Enter a Username",*/
profile_image: {required: 'Required!', accept: 'Not an image!'},
gender:"Select gender",
packages:"Enter a package",
password: {required: "Please enter your password "},

                         },

                        
                        submitHandler: function(form) {
                            var formData = new FormData($("#insert_form")[0]);
                            $(".LoadingImage").show();
                            $.ajax({
                                url: '<?php echo base_url();?>client/insert_manualclient/',
                                dataType : 'json',
                                type : 'POST',
                                data : formData,
                                contentType : false,
                                processData : false,
                                success: function(data) {
                                    if(data == '1'){
                                        $('#insert_form')[0].reset();
                                        $('.alert-success').show();
                                        $('.alert-danger').hide();
                                        $(location).attr('href', '<?php echo base_url();?>user');
                                    }
                                    else if(data == '0'){
                                       // alert('failed');
                                        $('.alert-danger').show();
                                        $('.alert-success').hide();
                                    }
                                $(".LoadingImage").hide();
                                },
                                error: function() {
                                     $(".LoadingImage").hide();
                                    alert('faileddd');}
                            });

                            return false;
                        } ,
                         /* invalidHandler: function(e,validator) {
        for (var i=0;i<validator.errorList.length;i++){   
            $(validator.errorList[i].element).parents('.panel-collapse.collapse').collapse('show');
        }
    }*/
     invalidHandler: function(e, validator) {
           if(validator.errorList.length)
        $('#tabs a[href="#' + jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')

        }

                    });

});
</script>
<script type="text/javascript">
$(document).ready(function(){
 $('.js-small').change(function() {
    
  $(this).closest('div').find('.field').prop('disabled', !$(this).is(':checked'));
 var check= $(this).data('id');
 //alert($(this).is(':checked'));
 //alert($(this).val());
 if($(this).is(':checked')&&(check=='accounts')){
    $(".it6").attr("style", "display:block");
    $(".it6").removeClass("hide");
 } else if(check=='accounts') {
    $(".it6").attr("style", "display:none");
    $(".it6").addClass("hide");
 }

if($(this).is(':checked')&&(check=='paye')){
    $(".it11").attr("style", "display:block");
    $(".it11").removeClass("hide");
 } else if(check=='paye') {
    $(".it11").attr("style", "display:none");
    $(".it11").addClass("hide");
 }

if($(this).is(':checked')&&(check=='vat')){
    $(".it8").attr("style", "display:block");
    $(".it8").removeClass("hide");
 } else if(check=='vat') {
    $(".it8").attr("style", "display:none");
    $(".it8").addClass("hide");
 }


 if($(this).is(':checked')&&(check=='cons')){
    $(".it7").attr("style", "display:block");
    $(".it7").removeClass("hide");
 } else if(check=='cons') {
    $(".it7").attr("style", "display:none");
    $(".it7").addClass("hide");
 }

});
});

</script>
<script>
$(document).ready(function(){
    $('.step-tabs').on('click', function(){
        $('html,body').animate({scrollTop: $(this).offset().top}, 800);
    }); 
});


$('.btnNext').click(function(){         
          $('.nav-tabs > .bbs').nextAll('li').not(".hide").first().find('a').trigger('click');

    }
);


  $('.btnPrevious').click(function(){
  $('.nav-tabs > .bbs').prevAll('li').not(".hide").first().find('a').trigger('click');
});
</script>



<script>
    $(document).ready(function() {
    $(".nav-tabs a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("bbs");
        $(this).parent().siblings().removeClass("bbs");
    });

     $(".th").click(function(event) {

         $("#ss").addClass("bbs");
    });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
$(".fields").blur(function(){
var currentRequest = null; 
     var term = $(this).val();
    
     currentRequest = $.ajax({
        url: '<?php echo base_url();?>client/addmanual_client/',
        type: 'post',
        data: $("form").serialize(),
        beforeSend : function()    {           
        if(currentRequest != null) {
            currentRequest.abort();
        }
    },
        success: function( data ){
            $("#user_id").val(data);
             //$(".LoadingImage").hide();
            },
            error: function( errorThrown ){
                console.log( errorThrown );
            }
        });
   });
   });
</script>

</body>



</html>

