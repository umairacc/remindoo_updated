<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//generate random string
function generateRandomString( $length = false) {
	if( $length == '' ) $length = 10;
    $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
function render_custom_fields_one($belongs_to, $rel_id = false, $where = array())
{
    $CI =& get_instance();
    
    $firm_id = $CI->session->userdata('firm_id');

    $cus = $CI->FirmFields_model->Customised_Fields( $firm_id );

    /*echo "my cus";
    print_r( $cus );*/
    $CI->db->select("GROUP_CONCAT(field_propriety) as ids");
    $CI->db->where('field_type', 1 );
    $CI->db->where_in('firm_id', ['0' , $firm_id] );
    if( !empty( $cus['customised_ids'] ) )
    {
        $CI->db->where_not_in('id' , explode( ',' , $cus['customised_ids'] ) );
    }
    $result = $CI->db->get("client_fields_management")->row_array();

   /* echo "permited ids";
    print_r( $result );*/
    if( !empty( $result['ids'] ) )
    {
        $CI->db->where_in( 'id' , explode( ',' , $result['ids'] ) );
    }
    $CI->db->where('active', 1);
    $CI->db->where('section_name', $belongs_to);

    if (is_array($where) && count($where) > 0 || is_string($where) && $where != '') {
        $CI->db->where($where);
    }
    //$CI->db->order_by('field_order', 'asc');
    $fields      = $CI->db->get('tblcustomfields')->result_array();
    $fields_html = '';

    if (count($fields)) {
       // $fields_html .= '<div class="row">';
        foreach ($fields as $field) {


    $CI =& get_instance();  
    $CI->db->where('field_type', 1);
    $CI->db->where('field_propriety', $field['id']);
    if (is_array($where) && count($where) > 0 || is_string($where) && $where != '') {
        $CI->db->where($where);
    }
    $CI->db->order_by('id', 'asc');
    $order_id = $CI->db->get('client_fields_management')->row_array();
           
            $field['name'] = $field['name'];

            $value = '';
            if ($field['bs_column'] == '' || $field['bs_column'] == 0) {
                $field['bs_column'] = 12;
            }

            //$fields_html .= '<div class="col-md-' . $field['bs_column'] . '">';
            
            if ($rel_id !== false) {
                $value = get_custom_field_value_one($rel_id, $field['id'], $belongs_to, false);
            }

            $_input_attrs = array();
            if ($field['required'] == 1) {
                $_input_attrs['data-custom-field-required'] = true;
                $_input_attrs['data-rule-required'] = "true";
            }

            if ($field['disalow_client_to_edit'] == 1) {
                $_input_attrs['disabled'] = true;
            }
            $_input_attrs['data-section'] = $field['section_name'];
            $_input_attrs['data-fieldid'] = $field['id'];

                //section name
               /* if($field['firm_name']=='imp_details'){
                $cont='sorting';
                }else if($field['firm_name']=='work_place'){
                $cont='workplace';
                }else if($field['firm_name']=='P11d'){
                $cont='p11d';
                }else if($field['firm_name']=='conf_stmt'){
                $cont='sorting_conf';
                }else if($field['firm_name']=='payroll'){
                $cont='payroll_sec';
                }else if($field['firm_name']=='accounts'){ 
                $cont='accounts_sec'; 
                }else if($field['firm_name']=='others'){
                $cont='others_details'; 
                }else if($field['firm_name']=='investigation_insurance'){
                $cont='invest';
                }else if($field['firm_name']=='management_account'){ 
                $cont='management';
                }else if($field['firm_name']=='vat_return'){
                $cont='vat_sort';
                }else{ $cont='personal_sort';
                } */
                $cont = $field['section_name'];
                $div_attr = array(  'data-id'      => $order_id['cnt_order'],
                                    'data-section' => $field['section_name'],
                                    'data-content' => $field['sub_content']
                                );


            $field_name = ucfirst($field['name']);
            if ($field['type'] == 'input' || $field['type'] == 'number' || $field['type'] == 'url' || $field['type'] == 'email')
            {
                
                $t = $field['type'] == 'input' ? 'text' : $field['type'];
                $fields_html .= render_input_one('custom_fields[' . $field['section_name'] . '][' . $field['id'] . ']', $field_name, $value, $t, $_input_attrs,$div_attr,$cont);
            }
             elseif ($field['type'] == 'date_picker') {
                $fields_html .= render_date_input_one('custom_fields[' . $field['section_name'] . '][' . $field['id'] . ']', $field_name, $value, $_input_attrs,$div_attr,$cont);
            }  elseif ($field['type'] == 'textarea') {
                $fields_html .= render_textarea_one('custom_fields[' . $field['section_name'] . '][' . $field['id'] . ']', $field_name, $value, $_input_attrs,$div_attr,$cont);
            }elseif ($field['type'] == 'select' || $field['type'] == 'multiselect') {
                $_select_attrs = array();
                $select_attrs  = '';
                $select_name = 'custom_fields[' . $field['section_name'] . '][' . $field['id'] . ']';
                if ($field['required'] == 1) {
                    $_select_attrs['data-custom-field-required'] = true;
                    $_select_attrs['data-rule-required'] = "true";
                }
                if ($field['disalow_client_to_edit'] == 1 ) {
                    $_select_attrs['disabled'] = true;
                }
                $_select_attrs['data-section'] = $field['section_name'];
                $_select_attrs['data-fieldid'] = $field['id'];
                if($field['type'] == 'multiselect'){
                    $_select_attrs['multiple'] = true;
                    $select_name .= '[]';
                }
                foreach ($_select_attrs as $key => $val) {
                    $select_attrs .= $key . '=' . '"' . $val . '" ';
                }
                $div_attrs ='';
                foreach ($div_attr as $key => $val) {
                    $div_attrs .= $key . '=' . '"' . $val . '" ';
                }

                $fields_html .= '<div class="form-group row '.$cont.'" data-id="'.$order_id['cnt_order'].'" ' . $div_attrs . '>';
                $fields_html .= '<label class="col-sm-4 col-form-label" for="custom_fields[' . $field['section_name'] . '][' . $field['id'] . ']">' . $field_name . '</label>';
                $fields_html .= '<div class="col-sm-8"><select ' . $select_attrs . ' name="'.$select_name.'" class="selectpicker form-control" data-width="100%" data-none-selected-text="Nothing selected"  data-live-search="true">';
                $fields_html .= '<option value="">--select--</option>';
                $options = explode(',', $field['options']);
                
                if($field['type'] == 'multiselect'){
                    $value   = explode(',', $value);
                }
                foreach ($options as $option) {
                    $option   = trim($option);
                    if($option != ''){
                        $selected = '';
                        if($field['type'] == 'select'){
                            if ($option == $value) {
                                $selected = ' selected';
                            }
                        } else {
                           foreach ($value as $v) {
                            $v = trim($v);
                            if ($v == $option) {
                                $selected = ' selected';
                            }
                        }
                    }
                    $fields_html .= '<option value="' . $option . '"' . $selected . '' . set_select('custom_fields[' . $field['section_name'] . '][' . $field['id'] . ']', $option) . '>' . $option . '</option>';
                    }
                }
                $fields_html .= '</select>';
                $fields_html .= '</div></div>';
            } elseif ($field['type'] == 'checkbox') {
                $div_attrs ='';
                foreach ($div_attr as $key => $val)
                {
                   $div_attrs .= $key . '=' . '"' . $val . '" ';
                }
                $fields_html .= '<div class="form-group '.$cont.'" ' . $div_attrs .'>';
                $fields_html .= '<br /><label class="control-label" for="custom_fields[' . $field['section_name'] . '][' . $field['id'] . '][]">' . $field_name . '</label>' . ($field['display_inline'] == 1 ? ' <br />': '');
                $options = explode(',', $field['options']);
                $value   = explode(',', $value);

                foreach ($options as $option) {
                    $checked = '';
                    // Replace double quotes with single.
                    $option  = htmlentities($option);
                    $option  = trim($option);
                    foreach ($value as $v) {
                        $v = trim($v);
                        if ($v == $option) {
                            $checked = 'checked';
                        }
                    }

                    $_chk_attrs                 = array();
                    $chk_attrs                  = '';
                    $_chk_attrs['data-section'] = $field['section_name'];
                    $_chk_attrs['data-fieldid'] = $field['id'];

                    if ($field['required'] == 1) {
                        $_chk_attrs['data-custom-field-required'] = true;
                         $_chk_attrs['data-rule-required'] = "true";
                    }

                    if ($field['disalow_client_to_edit'] == 1) {
                        $_chk_attrs['disabled'] = true;
                    }
                    foreach ($_chk_attrs as $key => $val) {
                        $chk_attrs .= $key . '=' . '"' . $val . '" ';
                    }
                    

                    $fields_html .= '<div class="checkbox'.($field['display_inline'] == 1 ? ' checkbox-inline': '').'">';
                    $fields_html .= '<input class="custom_field_checkbox" ' . $chk_attrs . ' ' . set_checkbox('custom_fields[' . $field['section_name'] . '][' . $field['id'] . '][]', $option) . ' ' . $checked . ' value="' . $option . '" id="cfc_' . $field['id'] . '_' . slug_it($option) . '" type="checkbox" name="custom_fields[' . $field['section_name'] . '][' . $field['id'] . '][]">';

                    $fields_html .= '<label for="cfc_' . $field['id'] . '_' . slug_it($option) . '">' . $option . '</label>';
                    $fields_html .= '<input type="hidden" name="custom_fields[' . $field['section_name'] . '][' . $field['id'] . '][]" value="cfk_hidden">';
                    $fields_html .= '</div>';
                }
                $fields_html .= '</div>';
            } /*elseif ($field['type'] == 'link') {
                $fields_html .= '<div class="form-group cf-hyperlink" data-section="' . $field['section_name'] . '" data-field-id="' . $field['id'] . '" data-value="' . htmlspecialchars($value) . '" data-field-name="' . htmlspecialchars($field_name) . '">';
                $fields_html .= '<label class="control-label" for="custom_fields[' . $field['section_name'] . '][' . $field['id'] . ']">' . $field_name . '</label></br>';

                $fields_html .= '<a id="custom_fields_' . $field['section_name'] . '_' . $field['id'] . '_popover" type="button" href="javascript:">' . _l('cf_translate_input_link_tip') . '</a>';

                $fields_html .= '<input type="hidden" ' . ($field['required'] == 1 ? 'data-custom-field-required="1"' : '') . ' value="" id="custom_fields[' . $field['section_name'] . '][' . $field['id'] . ']" name="custom_fields[' . $field['section_name'] . '][' . $field['id'] . ']">';

                $field_template = '';
                $field_template .= '<div id="custom_fields_' . $field['section_name'] . '_' . $field['id'] . '_popover-content" class="hide cfh-field-popover-template"><div class="form-group">';
                $field_template .= '<div class="row"><div class="col-md-12"><label class="control-label" for="custom_fields_' . $field['section_name'] . '_' . $field['id'] . '_title">' . _l('cf_translate_input_link_title') . '</label>';
                $field_template .= '<input type="text"' . ($field['disalow_client_to_edit'] == 1 && is_client_logged_in() ? " disabled=\"true\" " : ' ') . 'id="custom_fields_' . $field['section_name'] . '_' . $field['id'] . '_title" value="" class="form-control">';
                $field_template .= '</div>';
                $field_template .= '</div>';
                $field_template .= '</div>';
                $field_template .= '<div class="form-group">';
                $field_template .= '<div class="row">';
                $field_template .= '<div class="col-md-12">';
                $field_template .= '<label class="control-label" for="custom_fields_' . $field['section_name'] . '_' . $field['id'] . '_link">' . _l('cf_translate_input_link_url') . '</label>';
                $field_template .= '<div class="input-group"><input type="text"' . ($field['disalow_client_to_edit'] == 1 && is_client_logged_in() ? " disabled=\"true\" " : ' ') . 'id="custom_fields_' . $field['section_name'] . '_' . $field['id'] . '_link" value="" class="form-control"><span class="input-group-addon"><a href="#" id="cf_hyperlink_open_'.$field['id'].'" target="_blank"><i class="fa fa-globe"></i></a></span></div>';
                $field_template .= '</div>';
                $field_template .= '</div>';
                $field_template .= '</div>';
                $field_template .= '<div class="row">';
                $field_template .= '<div class="col-md-6">';
                $field_template .= '<button type="button" id="custom_fields_' . $field['section_name'] . '_' . $field['id'] . '_btn-cancel" class="btn btn-default btn-md pull-left" value="">' . _l('cancel') . '</button>';
                $field_template .= '</div>';
                $field_template .= '<div class="col-md-6">';
                $field_template .= '<button type="button" id="custom_fields_' . $field['section_name'] . '_' . $field['id'] . '_btn-save" class="btn btn-info btn-md pull-right" value="">' . _l('apply') . '</button>';
                $field_template .= '</div>';
                $field_template .= '</div>';
                $fields_html .= '<script>';
                $fields_html .= 'cfh_popover_templates[\'' . $field['id'] . '\'] = \'' . $field_template . '\';';
                $fields_html .= '</script>';
                $fields_html .= '</div>';
            }*/

            $name = 'custom_fields[' . $field['section_name'] . '][' . $field['id'] . ']';
            /*if ($field['type'] == 'checkbox' || $field['type'] == 'multiselect') {
                $name .= '[]';
            }*/

            $fields_html .= form_error($name);
            // Close column
           // $fields_html .= '</div>';
        }
        // close row
       // $fields_html .= '</div>';
    }

    return $fields_html;
}
function render_custom_fields_edit($belongs_to, $rel_id = false,$con=false, $where = array())
{
    $CI =& get_instance();
    $CI->db->where('active', 1);
    $CI->db->where('section_name', $belongs_to);
    if (is_array($where) && count($where) > 0 || is_string($where) && $where != '') {
        $CI->db->where($where);
    }
    $CI->db->order_by('field_order', 'asc');
    $fields      = $CI->db->get('tblcustomfields')->result_array();
    $fields_html = '';

    if (count($fields)) {
       // $fields_html .= '<div class="row">';
        foreach ($fields as $field) {
           
            $field['name'] = $field['name'];

            $value = '';
            if ($field['bs_column'] == '' || $field['bs_column'] == 0) {
                $field['bs_column'] = 12;
            }

            //$fields_html .= '<div class="col-md-' . $field['bs_column'] . '">';
            
            if ($rel_id !== false) {
                $value = get_custom_field_value_edit($rel_id, $field['id'], $belongs_to, false);
            }

            $_input_attrs = array();
            if ($field['required'] == 1) {
                $_input_attrs['data-custom-field-required'] = true;
                $_input_attrs['data-rule-required'] = "true";
            }

            if ($field['disalow_client_to_edit'] == 1) {
                $_input_attrs['disabled'] = true;
            }
            $_input_attrs['data-section'] = $field['section_name'];
            $_input_attrs['data-fieldid'] = $field['id'];

            $field_name = ucfirst($field['name']);
            if ($field['type'] == 'input' || $field['type'] == 'number') {
            
                $t = $field['type'] == 'input' ? 'text' : 'number';
                $fields_html .= render_input_edit($field['section_name'], $field_name, $value, $t, $_input_attrs,$con);
            } elseif ($field['type'] == 'date_picker') {
                $fields_html .= render_date_input_edit($field['section_name'], $field_name, _d_one($value), $_input_attrs,$con);
            }  elseif ($field['type'] == 'textarea') {
                $fields_html .= render_textarea_edit($field['section_name'], $field_name, $value, $_input_attrs,$con);
            }elseif ($field['type'] == 'select' || $field['type'] == 'multiselect') {
                $_select_attrs = array();
                $select_attrs  = '';
                $select_name =$field['section_name'];
                if ($field['required'] == 1) {
                    $_select_attrs['data-custom-field-required'] = true;
                    $_select_attrs['data-rule-required'] = "true";
                }
                if ($field['disalow_client_to_edit'] == 1 ) {
                    $_select_attrs['disabled'] = true;
                }
                $_select_attrs['data-section'] = $field['section_name'];
                $_select_attrs['data-fieldid'] = $field['id'];
                if($field['type'] == 'multiselect'){
                    $_select_attrs['multiple'] = true;
                    $select_name .= '[]';
                }
                foreach ($_select_attrs as $key => $val) {
                    $select_attrs .= $key . '=' . '"' . $val . '" ';
                }

                $fields_html .= '<div class="form-group">';
                $fields_html .= '<label>' . $field_name . '</label>';
                $fields_html .= '<select ' . $select_attrs . ' name="'.$select_name.'" class="selectpicker form-control" data-width="100%" data-none-selected-text="Nothing selected"  data-live-search="true">';
                $fields_html .= '<option value=""></option>';
                $options = explode(',', $field['options']);
                if($field['type'] == 'multiselect'){
                    $value   = explode(',', $value);
                }
                foreach ($options as $option) {
                    $option   = trim($option);
                    if($option != ''){
                        $selected = '';
                        if($field['type'] == 'select'){
                            if ($option == $value) {
                                $selected = ' selected';
                            }
                        } else {
                           foreach ($value as $v) {
                            $v = trim($v);
                            if ($v == $option) {
                                $selected = ' selected';
                            }
                        }
                    }
                    $fields_html .= '<option value="' . $option . '"' . $selected . '' . set_select('[' . $field['section_name'] . ']', $option) . '>' . $option . '</option>';
                    }
                }
                $fields_html .= '</select>';
                $fields_html .= '</div>';
            } elseif ($field['type'] == 'checkbox') {
                $fields_html .= '<div class="form-group chk">';
                $fields_html .= '<br /><label class="col-sm-4 col-form-label" for="custom_fields[' . $field['section_name'] . '][' . $field['id'] . '][]">' . $field_name . '</label>' . ($field['display_inline'] == 1 ? ' <br />': '');
                $options = explode(',', $field['options']);
                $value   = explode(',', $value);

                foreach ($options as $option) {
                    $checked = '';
                    // Replace double quotes with single.
                    $option  = htmlentities($option);
                    $option  = trim($option);
                    foreach ($value as $v) {
                        $v = trim($v);
                        if ($v == $option) {
                            $checked = 'checked';
                        }
                    }

                    $_chk_attrs                 = array();
                    $chk_attrs                  = '';
                    $_chk_attrs['data-section'] = $field['section_name'];
                    $_chk_attrs['data-fieldid'] = $field['id'];

                    if ($field['required'] == 1) {
                        $_chk_attrs['data-custom-field-required'] = true;
                        $_chk_attrs['data-rule-required'] = "true";
                    }

                    if ($field['disalow_client_to_edit'] == 1) {
                        $_chk_attrs['disabled'] = true;
                    }
                    foreach ($_chk_attrs as $key => $val) {
                        $chk_attrs .= $key . '=' . '"' . $val . '" ';
                    }

                    $fields_html .= '<div class="checkbox'.($field['display_inline'] == 1 ? ' checkbox-inline': '').'">';
                    $fields_html .= '<input class="custom_field_checkbox" ' . $chk_attrs . ' ' . set_checkbox('custom_fields[' . $field['section_name'] . '][' . $field['id'] . '][]', $option) . ' ' . $checked . ' value="' . $option . '" id="cfc_' . $field['id'] . '_' . slug_it($option) . '" type="checkbox" name="custom_fields[' . $field['section_name'] . '][' . $field['id'] . '][]">';

                    $fields_html .= '<label for="cfc_' . $field['id'] . '_' . slug_it($option) . '">' . $option . '</label>';
                    $fields_html .= '<input type="hidden" name="[' . $field['section_name'] . ']" value="cfk_hidden">';
                    $fields_html .= '</div>';
                }
                $fields_html .= '</div>';
            } 

            $name = $field['section_name'];
            /*if ($field['type'] == 'checkbox' || $field['type'] == 'multiselect') {
                $name .= '[]';
            }*/

            $fields_html .= form_error($name);
            // Close column
           // $fields_html .= '</div>';
        }
        // close row
        //$fields_html .= '</div>';
    }

    return $fields_html;
}
/**
 * Get custom fields
 * @param  [type]  $field_to
 * @param  array   $where
 * @param  boolean $exclude_only_admin
 * @return array
 */
function get_custom_fields_one($field_to, $where = array(), $exclude_only_admin = false)
{
    $is_admin = is_admin();
    $CI =& get_instance();
    $CI->db->where('section_name', $field_to);
    if (count($where) > 0) {
        $CI->db->where($where);
    }
    if (!$is_admin || $exclude_only_admin == true) {
        $CI->db->where('only_admin', 0);
    }
    $CI->db->where('active', 1);
    $CI->db->order_by('field_order', 'asc');

    $results = $CI->db->get('tblcustomfields')->result_array();

    foreach ($results as $key => $result) {
        $results[$key]['name'] = _l('cf_translate_' . $result['slug'], '', false) != 'cf_translate_' . $result['slug'] ? _l('cf_translate_' . $result['slug'], '', false) : $result['name'];
    }

    return $results;
}

function get_custom_field_value_one($rel_id, $field_id, $field_to, $format = true)
{
    $CI =& get_instance();
    $CI->db->where('relid', $rel_id);
    $CI->db->where('fieldid', $field_id);
    $CI->db->where('section_name', $field_to);
    $row    = $CI->db->get('tblcustomfieldsvalues')->row();
    $result = '';
    if ($row) {
        $result = $row->value;
        if ($format == true) {
            $CI->db->where('id', $field_id);
            $_row = $CI->db->get('tblcustomfields')->row();
            if ($_row->type == 'date_picker') {
                $result = _d($result);
            } elseif ($_row->type == 'date_picker_time') {
                $result = _dt($result);
            }
        }
    }

    return $result;
}
function get_custom_field_value_edit($rel_id, $field_id, $field_to, $format = true)
{
    $CI =& get_instance();
    $CI->db->where('relid', $rel_id);
    $CI->db->where('fieldid', $field_id);
    $CI->db->where('section_name', $field_to);
    $row    = $CI->db->get('tblcustomfieldsvalues')->row();
    $result = '';
    if ($row) {
        $result = $row->value;
        if ($format == true) {
            $CI->db->where('section_name', $field_to);
            $_row = $CI->db->get('tblcustomfields')->row();
            if ($_row->type == 'date_picker') {
                $result = _d($result);
            } elseif ($_row->type == 'date_picker_time') {
                $result = _dt($result);
            }
        }
    }

    return $result;
}


function render_input_one($name, $label = '', $value = '', $type = 'text', $input_attrs = array(), $form_group_attr = array(), $form_group_class = '', $input_class = '')
{
    $input            = '';
    $_form_group_attr = '';
    $_input_attrs     = '';


    foreach ($input_attrs as $key => $val) {
        // tooltips
        /*if ($key == 'title') {
            $val = _l($val);
        }*/
        //echo $key.'==='.$val;
        $_input_attrs .= $key . '=' . '"' . $val . '" ';
    }

    $_input_attrs = rtrim($_input_attrs);

    $form_group_attr['app-field-wrapper'] = $name;


    

    foreach ($form_group_attr as $key => $val) {
        // tooltips
        /*if ($key == 'title') {
            $val = _l($val);
        }*/
        $_form_group_attr .= $key . '=' . '"' . $val . '" ';
    }





    $_form_group_attr = rtrim($_form_group_attr);

    if (!empty($form_group_class)) {
        $form_group_class = ' ' . $form_group_class;
    }
    if (!empty($input_class)) {
        $input_class = ' ' . $input_class;
    }
    $input .= '<div class="form-group row name_fields' . $form_group_class . '" ' . $_form_group_attr . '>';
    if ($label != '') {
        $input .= '<label for="' . $name . '" class="col-sm-4 col-form-label">' .$label. '</label>';
    }
    $input .='<div class="col-sm-8">';
    $input .= '<input type="' . $type . '" id="' . $name . '" name="' . $name . '" class="form-control' . $input_class . '" ' . $_input_attrs . ' value="' . set_value($name, $value) . '">';
    $input .= '</div></div>';
    return $input;
 
}
function render_input_edit($name,$label = '', $value = '', $type = 'text', $input_attrs = array(),$con,$form_group_attr = array(), $form_group_class = '', $input_class = '')
{
    $input            = '';
    $_form_group_attr = '';
    $_input_attrs     = '';

foreach ($input_attrs as $key => $val) {
        // tooltips
        /*if ($key == 'title') {
            $val = _l($val);
        }*/
        $_input_attrs .= $key . '=' . '"' . $val . '" ';
    }

    $_input_attrs = rtrim($_input_attrs);

    $input .= '<div class="form-group row name_fields">';
    if ($label != '') {
        $input .= '<label class="col-sm-4 col-form-label">' .$label. '</label>';
    }
    $input .='<div class="col-sm-8">';
    $input .= '<input type="' . $type . '" id="' . $name . '" name="' . $name . '" class="fields" ' . $_input_attrs . ' value="' . $con . '">';
    $input .= '</div>';
    $input .= '</div>';
    return $input;
 
}

function render_date_input_one($name, $label = '', $value = '', $input_attrs = array(), $form_group_attr = array(), $form_group_class = '', $input_class = '')
{
    $input            = '';
    $_form_group_attr = '';
    $_input_attrs     = '';
    foreach ($input_attrs as $key => $val) {
        
        $_input_attrs .= $key . '=' . '"' . $val . '" ';
    }

    $_input_attrs = rtrim($_input_attrs);

    $form_group_attr['app-field-wrapper'] = $name;

    foreach ($form_group_attr as $key => $val) {
       
        $_form_group_attr .= $key . '=' . '"' . $val . '" ';
    }

    $_form_group_attr = rtrim($_form_group_attr);

    if (!empty($form_group_class)) {
        $form_group_class = ' ' . $form_group_class;
    }
    if (!empty($input_class)) {
        $input_class = ' ' . $input_class;
    }
    $input .= '<div class="form-group row' . $form_group_class . '" ' . $_form_group_attr . '>';
    if ($label != '') {
        $input .= '<label for="' . $name . '" class="col-sm-4 col-form-label">' .$label. '</label>';
    }
    //$input .= '<div class="input-group date">';
    $input .='<div class="col-sm-8 cus-date"> 
                <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span>                                                        </span>';
    $input .= '<input type="text" id="' . $name . '" name="' . $name . '" class="form-control dob_picker clr-check-select' . $input_class . '" ' . $_input_attrs . ' data-filed-custom="custom" value="' . set_value($name,$value) . '">';
    /*$input .= '<div class="input-group-addon">
    <i class="fa fa-calendar calendar-icon"></i>
</div>';*/
   // $input .= '</div>';
    $input .= '</div>';
    $input .= '</div>';
    return $input;
}

function render_date_input_edit($name, $label = '', $value = '', $input_attrs = array(),$con, $form_group_attr = array(), $form_group_class = '', $input_class = '')
{
    $input            = '';
    $_form_group_attr = '';
    $_input_attrs     = '';
    foreach ($input_attrs as $key => $val) {
        
        $_input_attrs .= $key . '=' . '"' . $val . '" ';
    }

    $_input_attrs = rtrim($_input_attrs);

    $form_group_attr['app-field-wrapper'] = $name;

    foreach ($form_group_attr as $key => $val) {
       
        $_form_group_attr .= $key . '=' . '"' . $val . '" ';
    }

    $_form_group_attr = rtrim($_form_group_attr);

    if (!empty($form_group_class)) {
        $form_group_class = ' ' . $form_group_class;
    }
    if (!empty($input_class)) {
        $input_class = ' ' . $input_class;
    }
    $input .= '<div class="form-group row name_fields">';
    if ($label != '') {
        $input .= '<label class="col-sm-4 col-form-label">' .$label. '</label>';
    }
   // $input .= '<div class="input-group date">';
    $input .='<div class="col-sm-8 cus-date"> <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>';
    $input .= '<input type="text" id="' . $name . '" name="' . $name . '" class="form-control datepicker fields clr-check-select" ' . $_input_attrs . ' data-filed-custom="custom" value="' . $con . '">';
    /*$input .= '<div class="input-group-addon">
    <i class="fa fa-calendar calendar-icon"></i>
</div>';*/
    $input .= '</div>';
    $input .= '</div>';
    return $input;
}
/**
 * Format date to selected dateformat
 * @param  date $date Valid date
 * @return date/string
 */
function _d_one($date)
{
    if ($date == '' || is_null($date) || $date == '0000-00-00') {
        return '';
    }
    if (strpos($date, ' ') !== false) {
        return _dt($date);
    }
    $format = '%Y-%m-%d';
    $date   = strftime($format, strtotime($date));

    return $date;
}

/**
 * Format datetime to selected datetime format
 * @param  datetime $date datetime date
 * @return datetime/string
 */
function _dt_one($date, $is_timesheet = false)
{
    if ($date == '' || is_null($date) || $date == '0000-00-00 00:00:00') {
        return '';
    }
    $format = '%Y-%m-%d';
    $hour12 = (24 == 24 ? false : true);

    if ($is_timesheet == false) {
        $date = strtotime($date);
    }

    if ($hour12 == false) {
        $tf = '%H:%M:%S';
        if ($is_timesheet == true) {
            $tf = '%H:%M';
        }
        $date   = strftime($format . ' ' . $tf, $date);
    } else {
        $date = date('Y-m-d'. ' g:i A', $date);
    }

    return  $date;
}
/**
 * Render textarea for admin area
 * @param  [type] $name             textarea name
 * @param  string $label            textarea label
 * @param  string $value            default value
 * @param  array  $textarea_attrs      textarea attributes
 * @param  array  $form_group_attr  <div class="form-group"> div wrapper html attributes
 * @param  string $form_group_class form group div wrapper additional class
 * @param  string $textarea_class      <textarea> additional class
 * @return string
 */
function render_textarea_one($name, $label = '', $value = '', $textarea_attrs = array(), $form_group_attr = array(), $form_group_class = '', $textarea_class = '')
{

    $textarea         = '';
    $_form_group_attr = '';
    $_textarea_attrs  = '';
    if (!isset($textarea_attrs['rows'])) {
        $textarea_attrs['rows'] = 4;
    }

    if(isset($textarea_attrs['class'])){
        $textarea_class .= ' '. $textarea_attrs['class'];
        unset($textarea_attrs['class']);
    }

    foreach ($textarea_attrs as $key => $val) {
        
        $_textarea_attrs .= $key . '=' . '"' . $val . '" ';
    }

    $_textarea_attrs = rtrim($_textarea_attrs);

    $form_group_attr['app-field-wrapper'] = $name;

    foreach ($form_group_attr as $key => $val) {
      
        $_form_group_attr .= $key . '=' . '"' . $val . '" ';
    }

    $_form_group_attr = rtrim($_form_group_attr);

    if (!empty($textarea_class)) {
        $textarea_class = trim($textarea_class);
        $textarea_class = ' ' . $textarea_class;
    }
    if (!empty($form_group_class)) {
        $form_group_class = ' ' . $form_group_class;
    }
    $textarea .= '<div class="form-group row' . $form_group_class . '" ' . $_form_group_attr . '>';
    if ($label != '') {
        $textarea .= '<label for="' . $name . '" class="col-sm-4 col-form-label">' . $label . '</label>';
    }

    $v = clear_textarea_breaks($value);
    if (strpos($textarea_class, 'tinymce') !== false) {
        $v = $value;
    }
    $textarea .='<div class="col-sm-8">';
    $textarea .= '<textarea id="' . $name . '" name="' . $name . '" class="form-control' . $textarea_class . '" ' . $_textarea_attrs . '>' . set_value($name, $v) . '</textarea>';

    $textarea .= '</div></div>';
    return $textarea;
}
function render_textarea_edit($name, $label = '', $value = '', $textarea_attrs = array(),$con,$form_group_attr = array(), $form_group_class = '', $textarea_class = '')
{

    $textarea         = '';
    $_form_group_attr = '';
    $_textarea_attrs  = '';
    if (!isset($textarea_attrs['rows'])) {
        $textarea_attrs['rows'] = 4;
    }

    if(isset($textarea_attrs['class'])){
        $textarea_class .= ' '. $textarea_attrs['class'];
        unset($textarea_attrs['class']);
    }

    foreach ($textarea_attrs as $key => $val) {
        
        $_textarea_attrs .= $key . '=' . '"' . $val . '" ';
    }

    $_textarea_attrs = rtrim($_textarea_attrs);

    $form_group_attr['app-field-wrapper'] = $name;

    foreach ($form_group_attr as $key => $val) {
      
        $_form_group_attr .= $key . '=' . '"' . $val . '" ';
    }

    $_form_group_attr = rtrim($_form_group_attr);

    if (!empty($textarea_class)) {
        $textarea_class = trim($textarea_class);
        $textarea_class = ' ' . $textarea_class;
    }
    if (!empty($form_group_class)) {
        $form_group_class = ' ' . $form_group_class;
    }
    $textarea .= '<div class="form-group row">';
    if ($label != '') {
        $textarea .= '<label class="col-sm-4 col-form-label">' . $label . '</label>';
    }

    $v = clear_textarea_breaks($value);
    if (strpos($textarea_class, 'tinymce') !== false) {
        $v = $value;
    }
    $textarea .='<div class="col-sm-8">';
    $textarea .= '<textarea id="' . $name . '" name="' . $name . '" class="form-control fields" ' . $_textarea_attrs . '>' . $con . '</textarea>';

    $textarea .= '</div>';
    $textarea .= '</div>';
    return $textarea;
}
/**
 * Check for custom fields, update on $_POST
 * @param  mixed $rel_id        the main ID from the table
 * @param  array $custom_fields all custom fields with id and values
 * @return boolean
 */
function handle_custom_fields_post_one($rel_id, $custom_fields)
{
    $affectedRows = 0;
    $CI =& get_instance();

    foreach ($custom_fields as $key => $fields) {

        foreach ($fields as $field_id => $field_value) {
            

            $CI->db->where('relid', $rel_id);
            $CI->db->where('fieldid', $field_id);
            $CI->db->where('section_name', $key);
            $row = $CI->db->get('tblcustomfieldsvalues')->row();

            // Make necessary checkings for fields
            $CI->db->where('id', $field_id);
            $field_checker = $CI->db->get('tblcustomfields')->row();
            if ($field_checker->type == 'date_picker') {
                $field_value = $field_value;
            } elseif($field_checker->type == 'date_picker_time'){
                $field_value =$field_value;
            } elseif ($field_checker->type == 'textarea') {
                $field_value = nl2br($field_value);
            } elseif ($field_checker->type == 'checkbox' || $field_checker->type == 'multiselect') {
                if ($field_checker->disalow_client_to_edit == 1 ) {
                    continue;
                }
                if (is_array($field_value)) {
                    $v = 0;
                    foreach ($field_value as $chk) {
                        if ($chk == 'cfk_hidden') {
                            unset($field_value[$v]);
                        }
                        $v++;
                    }
                    $field_value = implode(', ', $field_value);
                }
            }
            if ($row) {
                $CI->db->where('id', $row->id);
                $CI->db->update('tblcustomfieldsvalues', array(
                    'value' => $field_value
                ));
                if ($CI->db->affected_rows() > 0) {
                    $affectedRows++;
                }
            } else {
                if ($field_value != '') {
                    $CI->db->insert('tblcustomfieldsvalues', array(
                        'relid' => $rel_id,
                        'fieldid' => $field_id,
                        'section_name' => $key,
                        'value' => $field_value
                    ));
                    $insert_id = $CI->db->insert_id();
                    if ($insert_id) {
                        $affectedRows++;
                    }
                }
            }
        }
    }
    if ($affectedRows > 0) {
        return true;
    }

    return false;
}
?>