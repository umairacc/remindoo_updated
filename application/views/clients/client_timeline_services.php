<style type="text/css">
	.notes_edit {
		float: right;
	}
</style>
<?php $client_services = $this->Common_mdl->getServicelist(); ?>
<div id="tabs221">
	<input type="hidden" name="timeline_client_id" id="timeline_client_id" value="<?php echo $client_id; ?>">
	<ul>
		<li class="active"><a href="#tabs-1" class="for_tabs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>new note</a></li>
		<li><a href="#tabs-2" class="for_tabs"><i class="fa fa-envelope" aria-hidden="true"></i>email</a></li>
		<li><a href="#tabs-3" class="for_tabs"><i class="fa fa-phone" aria-hidden="true"></i>call</a></li>
		<li><a href="#tabs-4" class="for_tabs"><i class="fa fa-plus" aria-hidden="true"></i>log activity</a></li>
		<li><a href="#tabs-11" class="for_tabs for_allactivity_section_ajax"><i class="fa fa-plus" aria-hidden="true"></i>All activity</a></li>
		<li><a href="#tabs-5" class="for_tabs"><i class="fa fa-file-text-o" aria-hidden="true"></i>create task</a></li>
		<li><a href="#tabs-6" class="for_tabs"><i class="fa fa-clock-o" aria-hidden="true"></i>schedule</a></li>
		<!-- <li class="visible-schedule"><a href="#tabs-7">Add Event</a></li> -->
	</ul>
	<div id="tabs-1">
		<div class="first-tag">

			<textarea id="editor1" placeholder="Start typing to leave a note...">

            </textarea>
			<span class="notes_errormsg error_msg" style="color: red;"></span>
			&nbsp;
			<!-- Services type -->
			<div class="form-group col-sm-6 for_services_type">
				<label>Services Type</label>
				<div class="dropdown-sin-12 lead-form-st">
					<select name="services_type" id="services_type" class="form-control fields" placeholder="Select Services Type">
						<option value="">--Select Service Type--</option>
						<?php
						foreach ($client_services as $clse_key => $clse_value) {
						?>
							<option value="<?php echo $clse_value; ?>"><?php echo $clse_value; ?></option>
						<?php } ?>
					</select>
				</div>
				<span class="services_type_errormsg error_msg" style="color: red;"></span>
			</div>

			<!-- end of services type -->
			<!--  <span class="save-btn ">
                    <div><button class="btn btn-primary notes_section_insert">save</button><div></span> -->
			<input type="hidden" name="update_notes_id" id="update_notes_id">
			<div class="timeline_button timeline_add" style="float: right;">
				<input type="button" class="btn btn-primary notes_section_insert" name="save" id="save" value="Create">
			</div>
			<div class="timeline_button timeline_update" style="float: right;display: none;">
				<input type="button" class="btn btn-primary notes_section_update" name="save" id="save" value="Update">
				<input type="button" class="btn btn-primary notes_section_cancel" name="save" id="save" value="Cancel">
			</div>
		</div>

		<div id="notes_section">
			<div class="card ser-timeline">
				<div class="card-header">
				</div>
				<div class="card-header">
					<div class="col-12">
						<input class="form-control form-control-lg Note-search_timeline" type="text" id="notes_section" placeholder="Search">
					</div>
				</div>
				<div class="card-block">
					<div class="main-timeline">
						<div class="cd-timeline cd-container overall_notes_section" id="search_for_activity">
							<!-- from db data loop start -->
							<!-- updated date -->
							<?php
							$notes_section_data = $this->db->query("select * from timeline_services_notes_added where user_id=" . $client_id . " and module='notes' order by id desc ")->result_array();
							foreach ($notes_section_data as $notes_key => $notes_value) {
							?>
								<div class="cd-timeline-block notes_section">
									<div class="cd-timeline-icon bg-primary">
										<i class="icofont icofont-ui-file"></i>
									</div>
									<div class="cd-timeline-content card_main">
										<div class="p-0">
											<div class="btn-group dropdown-split-primary">
												<button type="button" class="btn btn-primary  dropdown-toggle dropdown-toggle-split waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													Actions
												</button>
												<div class="dropdown-menu">
													<span class="notes_edit"><a href="javascript:void(0);" onclick="return confirm_delete_timeline('<?php echo $notes_value['id']; ?>');">Delete</a></span>
													<span class="notes_edit"><a href="javascript:void(0);" onclick="topFunction(<?php echo $notes_value['id']; ?>,'notes')">Edit</a></span>
												</div>
											</div>
										</div>
										<!-- notes delete section -->
										<div class="modal-alertsuccess alert alert-success" id="delete_notes<?php echo $notes_value['id']; ?>" style="display:none;">
											<a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
											<div class="pop-realted1">
												<div class="position-alert1">
													Are you sure want to delete <b><a href="javascript:void(0);" class="notes_delete_function" data-id="<?php echo $notes_value['id']; ?>"> Yes </a></b> OR <b><a href="#" id="close">No</a></b>
												</div>
											</div>
										</div>
										<!-- end of notes delete section -->
										<div class="p-20">
											<h6><?php echo ($client_details[0]['crm_company_name']) ? $client_details[0]['crm_company_name'] : $this->Common_mdl->get_crm_name($client_details[0]['user_id']); ?> - <?php echo $client_details[0]['crm_legal_form']; ?></h6>
											<div class="timeline-details">
												<!-- <p class="m-t-0"><?php //echo  
																		?></p> -->
												<?php echo $notes_value['notes']; ?>
											</div>
										</div>
										<span class="cd-date"><?php if (isset($notes_value['created_time'])) {
																	echo date('F j, Y, g:i a', $notes_value['created_time']);
																} ?></span>
										<span class="cd-details">Notes For <?php echo $client_services[$notes_value['services_type']]; ?></span>
									</div>
								</div>
							<?php } ?>
							<!-- end of updated date -->

						</div>
						<div class="cd-timeline cd-container overall_for_notes_section" id="search_for_activity" style="display: none;">
						</div>
					</div>
				</div>
			</div>
			<!-- end -->
		</div>
	</div>
	<div id="tabs-2">
		<div class="first-tag">

			<textarea id="editor2" placeholder="Start typing to leave a note...">

            </textarea>
			<span class="emailnote_errormsg error_msg" style="color: red;"></span>
			<span class="save-btn">
				<a href="javascript:void(0);" class="timeline_email_to_client btn btn-primary" target="_blank">Email The Client</a>
				<!-- <button type="button" class="timeline_email_to_client btn btn-primary">Email the client</button> --></span>
		</div>
	</div>
	<!--  <div id="tabs-3">
                  <div class="first-tag">
                    <textarea id="editor21" placeholder="Start typing to leave a note...">
                      &lt;Start typing to leave a note...&lt;
                    </textarea>
                    <span class="save-btn"><button class="btn btn-primary">Email the client</button></span>
                  </div>
                </div> -->
	<!-- call log activity -->
	<div id="tabs-3">
		<div class="first-tag">

			<textarea id="editor21" placeholder="Start typing to leave a note...">

            </textarea>
			<span class="calllog_errormsg error_msg" style="color: red;"></span>

			<!-- Services type -->

			<!-- end of services type -->
			<!--  <span class="save-btn ">
                    <div><button class="btn btn-primary notes_section_insert">save</button><div></span> -->
			<input type="hidden" name="update_calllog_id" id="update_calllog_id">
			<div class="timeline_button timelinecall_add" style="float: right;">
				<input type="button" class="btn btn-primary calllog_section_insert" name="save" id="save" value="Create">
			</div>
			<div class="timeline_button timelinecall_update" style="float: right;display: none;">
				<input type="button" class="btn btn-primary calllog_section_update" name="save" id="save" value="Update">
				<input type="button" class="btn btn-primary calllog_section_cancel" name="save" id="save" value="Cancel">
			</div>
		</div>

		<div id="calllog_section">
			<div class="card ser-timeline">
				<div class="card-header">
				</div>
				<div class="card-header">
					<div class="col-12">
						<input class="form-control form-control-lg Calllog-search_timeline" type="text" id="calllog_section" placeholder="Search">
					</div>
				</div>
				<div class="card-block">
					<div class="main-timeline">
						<div class="cd-timeline cd-container overall_calllog_section" id="search_for_activity">
							<!-- from db data loop start -->
							<!-- updated date -->
							<?php
							$calllog_data = $this->db->query("select * from timeline_services_notes_added where user_id=" . $client_id . " and module='calllog' order by id desc ")->result_array();
							foreach ($calllog_data as $calllog_key => $calllog_value) {
							?>
								<div class="cd-timeline-block calllog_section">
									<div class="cd-timeline-icon bg-primary">
										<i class="icofont icofont-phone-circle"></i>
									</div>
									<div class="cd-timeline-content card_main">
										<div class="p-0">
											<div class="btn-group dropdown-split-primary">
												<button type="button" class="btn btn-primary  dropdown-toggle dropdown-toggle-split waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													Actions
												</button>
												<div class="dropdown-menu">
													<span class="notes_edit"><a href="javascript:void(0);" onclick="return confirm_delete_timeline('<?php echo $calllog_value['id']; ?>');">Delete</a></span>
													<span class="notes_edit"><a href="javascript:void(0);" onclick="topFunction(<?php echo $calllog_value['id']; ?>,'calllog')">Edit</a></span>
												</div>
											</div>
										</div>
										<!-- notes delete section -->
										<div class="modal-alertsuccess alert alert-success" id="delete_notes<?php echo $calllog_value['id']; ?>" style="display:none;">
											<a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
											<div class="pop-realted1">
												<div class="position-alert1">
													Are you sure want to delete <b><a href="javascript:void(0);" class="calllog_delete_function" data-id="<?php echo $calllog_value['id']; ?>"> Yes </a></b> OR <b><a href="#" id="close">No</a></b>
												</div>
											</div>
										</div>
										<!-- end of notes delete section -->
										<div class="p-20">
											<h6><?php echo ($client_details[0]['crm_company_name']) ? $client_details[0]['crm_company_name'] : $this->Common_mdl->get_crm_name($client_details[0]['user_id']); ?> - <?php echo $client_details[0]['crm_legal_form']; ?></h6>
											<div class="timeline-details">
												<!-- <p class="m-t-0"><?php //echo  
																		?></p> -->
												<?php echo $calllog_value['notes']; ?>
											</div>
										</div>
										<span class="cd-date"><?php if (isset($calllog_value['created_time'])) {
																	echo date('F j, Y, g:i a', $calllog_value['created_time']);
																} ?></span>
										<span class="cd-details">Call log Activity</span>
									</div>
								</div>
							<?php } ?>
							<!-- end of updated date -->

						</div>
						<div class="cd-timeline cd-container overall_for_calllog_section" id="search_for_activity" style="display: none;">
						</div>
					</div>
				</div>
			</div>
			<!-- end -->
		</div>
	</div>
	<!-- end of log activity -->
	<div id="tabs-4">
		<div class="first-tag">
			<textarea id="editor22" placeholder="Start typing to leave a note...">

            </textarea>
			<span class="log_errormsg error_msg" style="color: red;"></span>
			<!-- <span class="save-btn"><button class="btn btn-primary">log activity</button></span> -->
			<input type="hidden" name="update_log_id" id="update_log_id">
			<div class="timeline_button timelinelog_add" style="float: right;">
				<input type="button" class="btn btn-primary log_section_insert" name="save" id="save" value="Add log activity">
			</div>
			<div class="timeline_button timelinelog_update" style="float: right;display: none;">
				<input type="button" class="btn btn-primary log_section_update" name="save" id="save" value="Update log activity">
				<input type="button" class="btn btn-primary log_section_cancel" data-action="logsection" name="save" id="save" value="Cancel">
			</div>
		</div>
		<div id="log_section">
			<div class="card ser-timeline">
				<div class="card-header">
				</div>
				<div class="card-header">
					<div class="col-12">
						<input class="form-control form-control-lg log-search_timeline" type="text" id="log_section" placeholder="Search">
					</div>
				</div>
				<div class="card-block">
					<div class="main-timeline">
						<div class="cd-timeline cd-container overall_log_section" id="search_for_activity">
							<!-- from db data loop start -->
							<!-- updated date -->
							<?php
							$calllog_data = $this->db->query("select * from timeline_services_notes_added where user_id=" . $client_id . " and module='logactivity' order by id desc ")->result_array();
							foreach ($calllog_data as $calllog_key => $calllog_value) {
							?>
								<div class="cd-timeline-block log_section">
									<div class="cd-timeline-icon bg-primary">
										<i class="icofont icofont-ui-file"></i>
									</div>
									<div class="cd-timeline-content card_main">
										<div class="p-0">
											<div class="btn-group dropdown-split-primary">
												<button type="button" class="btn btn-primary  dropdown-toggle dropdown-toggle-split waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													Actions
												</button>
												<div class="dropdown-menu">
													<span class="notes_edit"><a href="javascript:void(0);" onclick="return confirm_delete_timeline('<?php echo $calllog_value['id']; ?>');">Delete</a></span>
													<span class="notes_edit"><a href="javascript:void(0);" onclick="topFunction(<?php echo $calllog_value['id']; ?>,'log')">Edit</a></span>
												</div>
											</div>
										</div>
										<!-- notes delete section -->
										<div class="modal-alertsuccess alert alert-success" id="delete_notes<?php echo $calllog_value['id']; ?>" style="display:none;">
											<a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
											<div class="pop-realted1">
												<div class="position-alert1">
													Are you sure want to delete <b><a href="javascript:void(0);" class="log_delete_function" data-action="logsection" data-id="<?php echo $calllog_value['id']; ?>"> Yes </a></b> OR <b><a href="#" id="close">No</a></b>
												</div>
											</div>
										</div>
										<!-- end of notes delete section -->
										<div class="p-20">
											<h6><?php echo ($client_details[0]['crm_company_name']) ? $client_details[0]['crm_company_name'] : $this->Common_mdl->get_crm_name($client_details[0]['user_id']); ?> - <?php echo $client_details[0]['crm_legal_form']; ?></h6>
											<div class="timeline-details">
												<!-- <p class="m-t-0"><?php //echo  
																		?></p> -->
												<?php echo $calllog_value['notes']; ?>
											</div>
										</div>
										<span class="cd-date"><?php if (isset($calllog_value['created_time'])) {
																	echo date('F j, Y, g:i a', $calllog_value['created_time']);
																} ?></span>
										<span class="cd-details">log Activity</span>
									</div>
								</div>
							<?php } ?>
							<!-- end of updated date -->

						</div>
						<div class="cd-timeline cd-container overall_for_log_section" id="search_for_activity" style="display: none;">
						</div>
					</div>
				</div>
			</div>
			<!-- end -->
		</div>
	</div>
	<!-- for allactivity logs -->
	<div id="tabs-11">
		<!-- content from ajax data -->
		<div id="allactivitylog_sectionsss">
			<div class="card ser-timeline">
				<div class="card-header">
					<div class="col-12">
						<input class="form-control form-control-lg allactivitylog-search_timeline" type="text" id="allactivitylog_section" placeholder="Search">
					</div>
					<div class="col-12 filter_timeline_list">
						<!-- for filter options 18-09-2018 -->
						<div class="button-group">
							<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">Filter Timeline</button>
							<ul class="dropdown-menu">
								<li><a href="javascript:void(0);" class="small" data-value="option1" tabIndex="-1"><input type="checkbox" name="filter_timeline[]" id="filter_timeline_notesactivity" class="filter_timeline_checkboxs" value="notes" />&nbsp;Notes Activity</a></li>
								<li><a href="javascript:void(0);" class="small" data-value="option2" tabIndex="-1"><input type="checkbox" name="filter_timeline[]" id="filter_timeline_emailactivity" class="filter_timeline_checkboxs" value="email" />&nbsp;Email Activity</a></li>
								<li><a href="javascript:void(0);" class="small" data-value="option3" tabIndex="-1"><input type="checkbox" name="filter_timeline[]" id="filter_timeline_callactivity" class="filter_timeline_checkboxs" value="calllog" />&nbsp;Call Activity</a></li>
								<li><a href="javascript:void(0);" class="small" data-value="option4" tabIndex="-1"><input type="checkbox" name="filter_timeline[]" id="filter_timeline_logactivity" class="filter_timeline_checkboxs" value="logactivity" />&nbsp;Log Activity</a></li>
								<li><a href="javascript:void(0);" class="small" data-value="option5" tabIndex="-1"><input type="checkbox" name="filter_timeline[]" id="filter_timeline_taskactivity" class="filter_timeline_checkboxs" value="newtask" />&nbsp;Create Task Notes</a></li>
								<li><a href="javascript:void(0);" class="small" data-value="option5" tabIndex="-1"><input type="checkbox" name="filter_timeline[]" id="filter_timeline_scheduleactivity" class="filter_timeline_checkboxs" value="schedule" />&nbsp;Schedule Activity</a></li>
								<li><a href="javascript:void(0);" class="small" data-value="option6" tabIndex="-1">Client Services&nbsp;</a></li>

								<!--  <li><a href="#" class="small" data-value="option6" tabIndex="-1"><input type="checkbox"/>&nbsp;Confirmation Statement</a></li> -->
								<?php foreach ($client_services as $filt_key => $filt_value) {
								?>
									<li><a href="javascript:void(0);" class="small" data-value="option6" tabIndex="-1"><input type="checkbox" name="filter_timeline[]" id="filter_timeline_<?php echo $filt_value; ?>" class="filter_timeline_checkboxs" value="<?php echo $filt_value; ?>" />&nbsp;<?php echo $filt_value; ?></a></li>
								<?php
								} ?>
							</ul>
						</div>
						<!-- end filter options -->
					</div>
				</div>
				<div class="card-block" id="allactivitylog_sections">
				</div>
			</div>
		</div>
	</div>
	<!-- end of all activity logs -->
	<div id="tabs-5">
		<div class="first-tag">
			<textarea id="editor23" placeholder="Start typing to leave a note...">
				&lt;Start typing to leave a note...&lt;
			</textarea>
			<span class="createtask_note_errormsg error_msg" style="color: red;"></span>
			<span class="save-btn"><button class="btn btn-primary create_task_withnotes">create a task</button></span>
		</div>
	</div>
	<div id="tabs-6">
		<div class="first-tag">
			<textarea id="editor24" placeholder="Start typing to leave a note...">

            </textarea>
			<span class="schedule_note_errormsg error_msg" style="color: red;"></span>
			<!--  <span class="save-btn sche-clickevent"><button class="btn btn-primary">schedule</button></span> -->
			<input type="hidden" name="update_schedule_id" id="update_schedule_id">
			<div class="timeline_button timelineschedule_add" style="float: right;">
				<input type="button" class="btn btn-primary schedule_section_insert" name="save" id="save" value="Schedule">
			</div>
			<div class="timeline_button timelineschedule_update" style="float: right;display: none;">
				<input type="button" class="btn btn-primary schedule_section_update" name="save" id="save" value="Update Schedule">
				<input type="button" class="btn btn-primary schedule_section_cancel" data-action="schedule" name="save" id="save" value="Cancel">
			</div>
		</div>
		<!-- for timeline for schedule -->
		<div id="schedule_section">
			<div class="card ser-timeline">
				<div class="card-header">
				</div>
				<div class="card-header">
					<div class="col-12">
						<input class="form-control form-control-lg Calllog-search_timeline" type="text" id="schedule_section" placeholder="Search">
					</div>
				</div>
				<div class="card-block">
					<div class="main-timeline">
						<div class="cd-timeline cd-container overall_schedule_section" id="search_for_activity">
							<!-- from db data loop start -->
							<!-- updated date -->
							<?php
							$calllog_data = $this->db->query("select * from timeline_services_notes_added where user_id=" . $client_id . " and module='schedule' order by id desc ")->result_array();
							foreach ($calllog_data as $calllog_key => $calllog_value) {
							?>
								<div class="cd-timeline-block schedule_section">
									<div class="cd-timeline-icon bg-primary">
										<i class="icofont icofont-clock-time"></i>
									</div>
									<div class="cd-timeline-content card_main">
										<div class="p-0">
											<div class="btn-group dropdown-split-primary">
												<button type="button" class="btn btn-primary  dropdown-toggle dropdown-toggle-split waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													Actions
												</button>
												<div class="dropdown-menu">
													<span class="notes_edit"><a href="javascript:void(0);" onclick="return confirm_delete_timeline('<?php echo $calllog_value['id']; ?>');">Delete</a></span>
													<span class="notes_edit"><a href="javascript:void(0);" onclick="topFunction(<?php echo $calllog_value['id']; ?>,'schedule')">Edit</a></span>
												</div>
											</div>
										</div>
										<!-- notes delete section -->
										<div class="modal-alertsuccess alert alert-success" id="delete_notes<?php echo $calllog_value['id']; ?>" style="display:none;">
											<a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
											<div class="pop-realted1">
												<div class="position-alert1">
													Are you sure want to delete <b><a href="javascript:void(0);" class="schedule_delete_function" data-action="schedule" data-id="<?php echo $calllog_value['id']; ?>"> Yes </a></b> OR <b><a href="#" id="close">No</a></b>
												</div>
											</div>
										</div>
										<!-- end of notes delete section -->
										<div class="p-20">
											<h6><?php echo ($client_details[0]['crm_company_name']) ? $client_details[0]['crm_company_name'] : $this->Common_mdl->get_crm_name($client_details[0]['user_id']); ?> - <?php echo $client_details[0]['crm_legal_form']; ?></h6>
											<div class="timeline-details">
												<!-- <p class="m-t-0"><?php //echo  
																		?></p> -->
												<?php if ($calllog_value['comments_for_reference'] != '') {
												?><p>Your Schedule was On <b><?php echo date('d-m-Y', strtotime($calllog_value['comments_for_reference'])); ?></b></p><?php
																																									} ?>

												<br>
												<?php echo $calllog_value['notes']; ?>
											</div>
										</div>
										<span class="cd-date"><?php if (isset($calllog_value['created_time'])) {
																	echo date('F j, Y, g:i a', $calllog_value['created_time']);
																} ?></span>
										<span class="cd-details">Schedule Activity</span>
									</div>
								</div>
							<?php } ?>
							<!-- end of updated date -->

						</div>
						<div class="cd-timeline cd-container overall_for_schedule_section" id="search_for_activity" style="display: none;">
						</div>
					</div>
				</div>
			</div>
			<!-- end -->
		</div>
		<!-- timeline schedule -->
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$(document).on("click", ".sche-clickevent", function() {
			$('.visible-schedule').fadeIn(200);
		});
	});
</script>