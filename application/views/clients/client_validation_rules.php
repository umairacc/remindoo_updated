<script>

function validateEmail($email)
{
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return emailReg.test( $email );
}

function phonenumber(inputtxt)
{
 console.log(inputtxt);

 var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;

  if(phoneno.test(inputtxt.value))
  {
    return true;
  }
  else
  {    
    return false;
  }
}
var check_username = 
{
   url:'<?php echo base_url();?>firm/check_username/',
   type : 'post',
   data:{
      <?php if(!empty($uri_seg)){?> id:function(){return $('#user_id').val();} <?php } ?>      
   },
  beforeSend:function(){
  $('#save_exit.sv_ex').addClass('disabled');
  },
  complete:function()
  {
    $('#save_exit.sv_ex').removeClass('disabled');
  }
};
var check_company_numper = {
  url:'<?php echo base_url();?>client/check_company_numberExist/',
  type : 'post',
  data:{<?php if(!empty($uri_seg)){?> id:function(){return $('#user_id').val();} <?php } ?>},
  beforeSend:function(){
    $('#save_exit.sv_ex').addClass('disabled');
  },
  complete:function()
  {
    $('#save_exit.sv_ex').removeClass('disabled');
  }
};

var trigger_contact_person_validation = function(){

   $(document).find('input[name^="work_email"] , input[name^="main_email"]').each(function(){
       $(this).rules("add", 
             {
                 email:true,
                 messages: {
                     email:"Enter Valid Email Address."
                 }
             });
       $(this).keyup(function(){ $(this).valid(); });
    });

   $(document).find('input[name^="landline"] ,input[name^="mobile_number"] ').each(function(){
    $(this).rules("add", 
          {
              required:false,
              digits:true,
                                 
          });
    $(this).keyup(function(){ $(this).valid(); });
   }); 

   $(document).find('input[name^="first_name"] ').each(function(){
    $(this).rules("add", 
          {
              required:true,
              messages: {
                  required: "First Name is required.",
              }
          });
    $(this).keyup(function(){ $(this).valid(); });
   });

   $(document).find('input[name^="date_of_birth"] ').each(function(){
     $(this).rules("add", 
           {
               required:false,
               check_date:true
           });
     $(this).keyup(function(){ $(this).valid(); });
   });
   $(document).find('input[name^="main_email"] ').each(function(){

       var isprimary = $(this).closest('div.make_a_primary').find('input[name="make_primary"]').prop('checked');
       
       console.log("is isprimary"+isprimary.length);
       
         $(this).rules("add", 
               {
                   required: true,
                   email:true,
                   messages:
                   {
                       required: "Mail required.",
                   }
               });
       if( isprimary ) $(this).keyup(function(){ $(this).valid(); });

     });

      $(document).find('input[name^="ccp_personal_tax_return_date"] ').each(function(){
        var cntid = $(this).attr('data-cntid');
        $(this).rules("add", 
              {
                  required:{
                     depends:function()
                     {
                        if( $("input[name='ccp_ptr_service["+cntid+"]']").is(':checked') )
                        {
                           return true;
                        }
                        else
                        {
                           return false;
                        }
                     }
                  }, messages: {
                     required:"Next Tax Return Period End Date Required"
                  }
              });
        $(this).keyup(function(){ $(this).valid(); });
      });

   };
   var check_firm_type_for_validation = function(element)
  {
      var selectVal=$("#legal_form").val();
      if((selectVal == "Private Limited company") || (selectVal == "Public Limited company") || (selectVal == "Limited Liability Partnership")) {
         return true;
       }
       else
       {
         return false;
       }
  };
   var check_previous_tab_open = function(element){
                               var clickCheckbox = document.querySelector('#previous_account');
                               if(clickCheckbox.checked) // true
                               {
                                 return true;
                               }
                               else
                               {
                                 return false;
                               }
                            };
   var  is_Login_required = function(element)
   {
         // console.log($("input[name='show_login']").prop("checked")+"login required");
        if($("input[name='invite_use']").prop("checked"))
        {
         return true;
        }
        else
        {
         return false;
        }
   };

  var is_cs_enabled = function()
  {
    if($("input[name='confirm[tab]']").prop("checked"))
    {
     return true;
    }
    else
    {
     return false;
    }     
  };            

  var is_acc_enabled = function()
  {
    if($("input[name='accounts[tab]']").prop("checked"))
    {
     return true;
    }
    else
    {
      return false;
    }
  };
  var is_company_tax_enabled = function()
  {
     if($("input[name='companytax[tab]']").prop("checked"))
     {
     return true;
     }
     else
     {
     return false;
     }
  };
  var is_ptr_enabled  = function()
  {
    if( $("input[name^='ccp_ptr_service']:checked").length )
    {
       return true;
    }
    else
    {
       return false;
    }  
  };  
  var is_vat_enabled  = function()
  {
     if($("input[name='vat[tab]']").prop("checked"))
     {
     return true;
     }
     else
     {
     return false;
     }  
  };
  var is_payroll_enabled = function()
  { 
     if($("input[name='payroll[tab]']").prop("checked"))
     {
     return true;
     }
     else
     {
     return false;
     }
  };
  var is_pension_subm_enabled = function()
  {
     if($("input[name='workplace[tab]']").prop("checked"))
     {
     return true;
     }
     else
     {
     return false;
     }
  };
  var is_cis_con_enabled = function()
  { 
     if($("input[name='cis[tab]']").prop("checked"))
     {
     return true;
     }
     else
     {
     return false;
     }
  };
  var is_cis_sub_enabled = function()
  { 
    if($("input[name='cissub[tab]']").prop("checked"))
    {
     return true;
    }
    else
    {
     return false;
    }
  };
  var is_p11d_enabled = function()
  {
     if($("input[name='p11d[tab]']").prop("checked"))
     {
     return true;
     }
     else
     {
     return false;
     }
  };
  var is_next_manage_acc_enabled = function()
  {
     if($("input[name='management[tab]']").prop("checked"))
     {
     return true;
     }
     else
     {
     return false;
     }     
  };
  var is_bookkping_enabled = function()
  { 
     if($("input[name='bookkeep[tab]']").prop("checked"))
     {
     return true;
     }
     else
     {
     return false;
     }
  };
  var is_insurance_enbaled = function()
  {
    if($("input[name='investgate[tab]']").prop("checked"))
    {
     return true;
    }
     else
    {
     return false;
    }
  };
  var is_registered_enabled = function()
  {
      if($("input[name='registered[tab]']").prop("checked"))
      {
       return true;
      }
       else
      {
       return false;
      }
  };
  var is_investigation_enabled = function()
  {
    if( $("input[name='taxadvice[tab]']").prop("checked") )
    {       
      return true;
    }
    else
    {
      return false;
    }
  };
   var Rules = {  
      company_name   : {required: true},
      company_number : {required :check_firm_type_for_validation,remote:check_company_numper},
      company_status : {required: true},
      company_url    : {url: true},
      officers_url   : {url: true},
      append_cnt     : {min:1},
      make_primary   : {required:true},
      assignees_values : {required:true}, 
      emailid :
      {
           email:
           {
              depends: check_previous_tab_open
           } 
      }, 
       cun_code:
       {
         required:
         {
            depends:function()
            {
               if( $('input[name="pn_no_rec"]').val() != '' )
               {
                  return true;
               }
               else
               {
                  return false;
               }
            }
         }
      },
       pn_no_rec:
       {
         digits:true,
       },
       user_name:
       { 
         required:
         {
            depends: is_Login_required
         },
         remote:
         {
            param:check_username,
            depends: is_Login_required
         }
      },
       password:
       {
         required:
         {
            depends: is_Login_required
         },
         minlength :
         {
            param:5,
            depends: is_Login_required
         }
       },
       confirm_password :
      {
            required:
            {
               depends: is_Login_required
            },
            minlength :
            {
               param:5,
               depends: is_Login_required
            },
            equalTo :
            {  param:"#password",
               depends: is_Login_required
            }
       },
      confirmation_next_made_up_to:
      {
         required:
         {
            depends: is_cs_enabled
         },         
      },
     /* 'service_manager[1]':{
         required:
         {
            depends: is_cs_enabled
         },         
      },
      'service_assignee[1]':{
         required:
         {
            depends: is_cs_enabled
         },         
      },*/
      accounts_next_made_up_to:
      {  
         required:
         {
            depends: is_acc_enabled
         },         
      },      
    /*  'service_manager[2]':
      {  
         required:
         {
            depends: is_acc_enabled
         },         
      },
      'service_assignee[2]':
      {  
         required:
         {
            depends: is_acc_enabled
         },         
      },*/
      accounts_due_date_hmrc:
      {  
         required:
         {
            depends: is_company_tax_enabled
         },
      },
      /*'service_manager[3]':
      {  
         required:
         {
            depends: is_company_tax_enabled
         },         
      },
      'service_assignee[3]':
      {  
         required:
         {
            depends: is_company_tax_enabled
         },         
      },*/
    /*
      already setup trigger_contact_person_validation() 
       personal_tax_return_date:
      {  
         required:
         {
            depends: is_ptr_enabled
         },
      },*/
     /*  'service_manager[4]':
      {  
         required:
         {
            depends: is_ptr_enabled
         },         
      },
      'service_assignee[4]':
      {  
         required:
         {
            depends: is_ptr_enabled
         },         
      },*/
      vat_quarters:
      {  
         required:
         {
            depends: is_vat_enabled     
         },
      },
      vat_quater_end_date:
      {  
         required:
         {
            depends: is_vat_enabled    
         },
      },
     /*  'service_manager[5]':
      {  
         required:
         {
            depends: is_vat_enabled
         },         
      },
      'service_assignee[5]':
      {  
         required:
         {
            depends: is_vat_enabled
         },         
      },*/
      payroll_run_date:
      {  
         required:
         {
            depends: is_payroll_enabled             
         },
      },
      payroll_run:
      {  
         required:
         {
            depends: is_payroll_enabled            
         },
      },
     /* 'service_manager[6]':
      {  
         required:
         {
            depends: is_payroll_enabled
         },         
      },
      'service_assignee[6]':
      {  
         required:
         {
            depends: is_payroll_enabled
         },         
      },*/
      pension_subm_due_date:
      {  
         required:
         {
            depends: is_pension_subm_enabled
         },
      },
     /* 'service_manager[7]':
      {  
         required:
         {
            depends: is_pension_subm_enabled
         },         
      },
      'service_assignee[7]':
      {  
         required:
         {
            depends: is_pension_subm_enabled
         },         
      },*/
      cis_contractor_start_date:
      {  
        required:
        {
            depends: is_cis_con_enabled
        },
      },
      crm_cis_frequency:
      {  
         required:
         {
            depends: is_cis_con_enabled
         },
      },
      /*'service_manager[8]':
      {  
        required:
        {
            depends: is_cis_con_enabled
        },         
      },
      'service_assignee[8]':
      {  
        required:
        {  
          depends: is_cis_con_enabled
        },         
      },   */  
      cis_subcontractor_start_date:
      {  
         required:
         {
            depends: is_cis_sub_enabled
         },
      },
      crm_cis_subcontractor_frequency:
      {  
         required:
         {
            depends: is_cis_sub_enabled
         },
      },
      /* 'service_manager[9]':
      {  
         required:
         {
            depends: is_cis_sub_enabled
         },         
      },
      'service_assignee[9]':
      {  
         required:
         {
            depends: is_cis_sub_enabled
         },         
      },*/
      p11d_due_date:
      {  
         required:
         {
            depends: is_p11d_enabled
         },
      },
      /* 'service_manager[11]':
      {  
         required:
         {
            depends: is_p11d_enabled
         },         
      },
      'service_assignee[11]':
      {  
         required:
         {
            depends: is_p11d_enabled
         },         
      },*/
      next_manage_acc_date:
      {  
         required:
         {
            depends: is_next_manage_acc_enabled
         },
      },
      manage_acc_fre:
      {  
         required:
         {
            depends: is_next_manage_acc_enabled
         }
         ,
      },
      /* 'service_manager[12]':
      {  
         required:
         {
            depends: is_next_manage_acc_enabled
         },         
      },
      'service_assignee[12]':
      {  
         required:
         {
            depends: is_next_manage_acc_enabled
         },         
      },*/
      next_booking_date:
      {  
         required:
         {
            depends: is_bookkping_enabled
         },
      },
      bookkeeping:
      {  
         required:
         {
            depends: is_bookkping_enabled
         }, 
      },
      /*'service_manager[10]':
      {  
         required:
         {
            depends: is_bookkping_enabled
         },         
      },
      'service_assignee[10]':
      {  
         required:
         {
            depends: is_bookkping_enabled
         },         
      },*/
      insurance_renew_date:
      {  
         required:
         {
            depends: is_insurance_enbaled
         },
      },
      /*'service_manager[13]':
      {  
         required:
         {
            depends: is_insurance_enbaled
         },         
      },
      'service_assignee[13]':
      {  
         required:
         {
            depends: is_insurance_enbaled
         },         
      },*/
      registered_renew_date:
      {  
         required:
         {
            depends: is_registered_enabled
         },
      },
      /*'service_manager[14]':
      {  
         required:
         {
            depends: is_registered_enabled
         },         
      },
      'service_assignee[14]':
      {  
         required:
         {
            depends: is_registered_enabled
         },         
      },*/
      investigation_end_date:
      {  
         required:
         {
            depends: is_investigation_enabled
         },
      },
      /*'service_manager[15]':
      {  
         required:
         {
            depends: is_investigation_enabled
         },         
      },
      'service_assignee[15]':
      {  
        required:
        {
          depends: is_investigation_enabled
        },         
      }, */ 
   };

   var Message = 
   {
      company_name:
      {
         required:"Company Name Required."
      },
      company_number:
      {
         required:"Company Number Required.",
         remote:"Company Number Already Exists."
      },
      company_status:
      {
         required:"Company Status Required."
      },
      append_cnt:"Please Add Contact Persons.",
      make_primary:
      {
         required:"Select any one Contact as Primary."
      },
      assignees_values:
      {
         required:"Please Select Any Assignee To The Client."
      },
      user_name:
      {  
         required:"User Name Required.",
         remote:"User Name Already Exists."
      },
      password:
      {  required:"Password Required.",
         minlength: "Password Must Be Greater Than 5."
      },
      confirm_password:
      {
         required:"Confirm Password Required.",
         minlength: "Confirm Password Must Be Greater Than 5.", 
         equalTo: "Confirm Password Dosen't Match With Password."
      },
      confirmation_next_made_up_to:
      {
         required:"Confirmation Date Required."
      },
      'service_manager[1]':
      {
         required: "CS Manager Required."
      },
      'service_assignee[1]':
      {
         required: "CS Assignee Required."
      },
      accounts_due_date_hmrc:
      {
         required:"Tax Return Date Required."
      },
      accounts_next_made_up_to:
      {
         required:"Accounts Date Required."
      },
      'service_manager[2]':
      {  
         required: " Accounts Manager Required."
      },
      'service_assignee[2]':
      {  
         required:"Accounts Assignee Required."
      },
      personal_tax_return_date:
      {
         required:"Personal Tax Return Date Required."
      },
      'service_manager[4]':
      {  
         required:"Personal Tax Return Manager Required."        
      },
      'service_assignee[4]':
      {  
         required:"Personal Tax Return Assignee Required."       
      },
      cis_contractor_start_date:
      {
         required:"CIS Date Required."
      },
      crm_cis_frequency:
      {
         required:"CIS Frequency Required."
      },
      'service_manager[8]':
      {  
        required:"CIS Manager Required."
      },
      'service_assignee[8]':
      {  
        required:"CIS Assignee Required."
      },   
      cis_subcontractor_start_date:
      {
         required:"CIS Sub-Contractor Start  Date Required."
      },
      crm_cis_subcontractor_frequency:
      {
         required:"CIS Sub-Contractor Frequency Required."
      },
      'service_manager[9]':
      {  
         required:"CIS Sub-Contractor Manager Required."
      },
      'service_assignee[9]':
      {  
         required:"CIS Sub-Contractor Assignee Required."
      },
      payroll_run_date:
      {
        required:"Payroll Date Required." 
      },
      payroll_run:
      {
        required:"Payroll Frequency Required." 
      },
      'service_manager[6]':
      {  
         required:"Payroll Manager Required."       
      },
      'service_assignee[6]':
      {  
         required:"Payroll Assignee Required."
      },
      pension_subm_due_date:
      {
        required:"Work Pension Submission Date Required." 
      },
       'service_manager[7]':
      {  
         required:"Work Pension Submission Manager Required."      
      },
      'service_assignee[7]':
      {  
         required:"Work Pension Submission Assignee Required."    
      },
      p11d_due_date:
      {
        required:"P11D Date Required." 
      },
      'service_manager[11]':
      {  
         required:"P11D Manager Required."          
      },
      'service_assignee[11]':
      {  
         required:"P11D Assignee Required."         
      },
      vat_quarters:
      {
        required:"Vat Quarters Required." 
      },
      vat_quater_end_date:
      {
         required:"Vat Quarters End Date Required."   
      },
      'service_manager[5]':
      {  
         required:"VAT Manager Required."       
      },
      'service_assignee[5]':
      {  
         required:"VAT Assignee Required."          
      },
      next_booking_date:
      {
        required:"Next Bookkeeping Date Required." 
      },
      bookkeeping:
      {
       required:"Bookkeeping Frequency Required." 
      },
        'service_manager[10]':
      {   
         required:"Bookkeeping Manager Required."        
      },
      'service_assignee[10]':
      {  
         required:"Bookkeeping Assignee Required."       
      },
      next_manage_acc_date:
      {
        required:"Next Management Date Required." 
      },
      manage_acc_fre:
      {
        required:"Management Frequency Required." 
      },
      'service_manager[12]':
      {  
         required:"Management Account Manager Required."         
      },
      'service_assignee[12]':
      {  
         required:"Management Account Assignee Required."         
      },
      insurance_renew_date:
      {
        required:"Insurance Renew Date Required." 
      },
      'service_manager[13]':
      {  
         required:"Insurance Renew Manager Required."        
      },
      'service_assignee[13]':
      {  
         required:"Insurance Renew Assignee Required."     
      },
      registered_renew_date:
      {
        required:"Registered Office Renew Date Required." 
      },
      'service_manager[14]':
      {  
         required:"Registered Office Renew Manager Required."         
      },
      'service_assignee[14]':
      {  
         required:"Registered Office Renew Assignee Required.",         
      },
      investigation_end_date:
      {
        required:"Tax Advice/Investigation End Date Required." 
      },
      'service_manager[15]':
      {  
         required:"Tax Advice/Investigation Manager Required."         
      },
      'service_assignee[15]':
      {  
        required:"Tax Advice/Investigation Assignee Required."         
      }, 
   };
</script>