<?php 
$j =1;
   //foreach ($rec as $key => $value) {
$contact_names = $this->Common_mdl->numToOrdinalWord($cnt).' Contact ';

$value['title']             = $title;
$value['first_name']        = $first_name;
$value['surname']           = $surname;
$value['preferred_name']    = $preferred_name;
$value['address_line1']     = $address_line1;
$value['address_line2']     = $address_line2;
$value['premises']          = $premises;
$value['region']            = $region;
$value['country']           = $country;
$value['locality']          = $locality;
$value['post_code']         = $post_code;
$value['nationality']       = $nationality;
$value['occupation']        = $occupation;
$value['appointed_on']      = $appointed_on;
$value['country_of_residence'] = $country_of_residence;
$value['date_of_birth'] = $date_of_birth;
$value['psc'] = $psc;
$value['nature_of_control'] = $nature_of_control;
$value['contact_from']=$contact_from;
$cnt;
$contact_id = ( !empty( $contact_id ) ? $contact_id :'' );

//$value['created_date'] = time();

    ?>
<!--      <input type="hidden" name="contact_id[]" id="contact_id" value="<?php //echo $value['id'];?>">
 -->

 <div class="space-required new_update1 <?php echo $contact_from; ?>">
   <div class="update-data01 make_a_primary common_div_remove-<?php echo $cnt; ?>" id="common_div_remove-<?php echo $cnt; ?>">
 
      <input type="hidden" class="contact_table_id" name="contact_table_id[<?php echo $cnt; ?>]"  value="<?php echo $contact_id;?>">

      <div class="main-contact append_contact ">
         <span class="h4"><!-- <?php echo $contact_names;?> --> Contact Person details</span>
         <div class="dead-primary1 for_row_count-<?php echo $cnt; ?>">
         <!-- sync with company details button -->
         <div class="radio radio-inline">
               <a href="javascript:void(0)" class="sync_with_company active" data-id="sync_with_company<?php echo $cnt;?>" ><span>SYNC WITH COMPANY</span></a> 
            </div>
         <input type="hidden" name="make_primary_loop[<?php echo $cnt; ?>]" id="make_primary_loop" value="<?php echo $cnt; ?>">
         <div class="radio radio-inline">
            <label>
               <i class="helper" style="display: none;"></i>
               <a href="javascript:void(0)" class="make_primary_section"  data-id="make_primary<?php echo $cnt;?>" ><span>Make a primary</span></a>
               <input type="radio" name="make_primary" id="make_primary<?php echo $cnt;?>" value="<?php echo $cnt;?>">

               <!--  <?php if(isset($value['contact_from']) && ($value['contact_from']!='') ){ ?>
               <a href="javascript:void(0)" class="Edit_contact"  data-id="edit<?php echo $cnt;?>" ><span>Edit</span></a>
               <?php } ?> -->
            </label>
         </div>
      </div>

   </div>

      <div class="primary-info addnewclient CONTENT_CONTACT_PERSON">

            <span class="primary-inner  <?php if(isset($title) && $title!=''){ ?>light-color_company<?php }?> contact_sort" data-id="<?php echo $Setting_Order['contact_title'];?>" <?php echo $Setting_Delete['contact_title'];?> >
            <label><?php echo $Setting_Label['contact_title'];?></label>
            <!-- <input type="text" class="text-info title"  name="title[]" id="title" value="<?php echo $title;?>"> -->
            <select class="text-info title clr-check-select"  name="title[<?php echo $cnt; ?>]">
               
               <option value="Mr" <?php if(isset($title) && $title=='Mr') {?> selected="selected"<?php } ?>>Mr</option>
               <option value="Mrs" <?php if(isset($title) && $title=='Mrs') {?> selected="selected"<?php } ?>>Mrs</option>
               <option value="Miss" <?php if(isset($title) && $title=='Miss') {?> selected="selected"<?php } ?>>Miss</option>
               <option value="Dr" <?php if(isset($title) && $title=='Dr') {?> selected="selected"<?php } ?>>Dr</option>
               <option value="Ms" <?php if(isset($title) && $title=='Ms') {?> selected="selected"<?php } ?>>Ms</option>
               <option value="Prof" <?php if(isset($title) && $title=='Prof') {?> selected="selected"<?php } ?>>Prof</option>
            </select>
            </span>

            <span class="primary-inner   contact_sort <?php 
            if(isset($value['first_name']) && ($value['first_name']!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_first_name'];?>" <?php echo $Setting_Delete['contact_first_name'];?>>
            <label><?php echo $Setting_Label['contact_first_name'];?>
            <span class="Hilight_Required_Feilds">*</span>
            </label>
            <input type="text" name="first_name[<?php echo $cnt;?>]"  placeholder="" value="<?php if(isset($value['first_name']) && ($value['first_name']!='') ){ echo $value['first_name'];}?>" class="text-info clr-check-client" data-cntid="<?php echo $cnt;?>" onchange="update_ccp_name_to_content(this)">
            </span>                           

            <span class="primary-inner contact_sort <?php  if(isset($value['middle_name']) && ($value['middle_name']!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_middle_name'];?>" <?php echo $Setting_Delete['contact_middle_name'];?>>
            <label><?php echo $Setting_Label['contact_middle_name'];?></label>
            <input type="text" name="middle_name[<?php echo $cnt; ?>]"  placeholder="" value="<?php if(isset($value['middle_name']) && ($value['middle_name']!='') ){ echo $value['middle_name'];}?>" class="text-info clr-check-client">
            </span>

            <span class="primary-inner contact_sort <?php  if(isset($value['last_name']) && ($value['last_name']!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_last_name'];?>" <?php echo $Setting_Delete['contact_last_name'];?>>
            <label><?php echo $Setting_Label['contact_last_name'];?></label>
            <input type="text" name="last_name[<?php echo $cnt; ?>]" placeholder="" value="<?php if(isset($value['last_name']) && ($value['last_name']!='') ){ echo $value['last_name'];}?>" class="text-info clr-check-client" data-cntid="<?php echo $cnt;?>" onchange="update_ccp_name_to_content(this)">
            </span>

            <span class="primary-inner   contact_sort <?php if(isset($value['surname']) && ($value['surname']!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_surname'];?>" <?php echo $Setting_Delete['contact_surname'];?>>
            <label><?php echo $Setting_Label['contact_surname'];?></label>
            <input type="text" name="surname[<?php echo $cnt; ?>]"  placeholder="" value="<?php if(isset($value['surname']) && ($value['surname']!='') ){ echo $value['surname'];}?>" class="text-info clr-check-client">
            </span>

            <span class="primary-inner contact_sort <?php if(isset($value['preferred_name']) && ($value['preferred_name']!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_preferred_name'];?>" <?php echo $Setting_Delete['contact_preferred_name'];?>>
            <label><?php echo $Setting_Label['contact_preferred_name'];?></label>
            <input type="text" name="preferred_name[<?php echo $cnt; ?>]" placeholder="" value="<?php if(isset($value['preferred_name']) && ($value['preferred_name']!='') ){ echo $value['preferred_name'];}?>" class="text-info clr-check-client">
            </span>                              

            <span class="primary-inner  contact_sort  <?php  if(isset($value['mobile']) && ($value['mobile']!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_mobile'];?>" <?php echo $Setting_Delete['contact_mobile'];?>>
            <label> <?php echo $Setting_Label['contact_mobile'];?></label>
            <input type="text" name="mobile_number[<?php echo $cnt;?>]"  pattern="\d{3}[\-]\d{3}[\-]\d{4}" class="text-info clr-check-client" value="<?php if(isset($value['mobile']) && ($value['mobile']!='') ){ echo $value['mobile'];}?>">
            </span>

            <span class="primary-inner contact_sort  <?php  if(isset($value['main_email']) && ($value['main_email']!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_main_email'];?>" <?php echo $Setting_Delete['contact_main_email'];?>>
            <label><?php echo $Setting_Label['contact_main_email'];?></label>
            <input type="email" name="main_email[<?php echo $cnt;?>]"  class="text-info main_email clr-check-client" value="<?php if(isset($value['main_email']) && ($value['main_email']!='') ){ echo $value['main_email'];}?>">
            </span>

            <span class="primary-inner contact_sort  <?php  if(isset($value['nationality']) && ($value['nationality']!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_nationality'];?>" <?php echo $Setting_Delete['contact_nationality'];?>>
            <label><?php echo $Setting_Label['contact_nationality'];?></label>
            <input type="text" name="nationality[<?php echo $cnt; ?>]" id="nationality" class="text-info clr-check-client" value="<?php if(isset($value['nationality']) && ($value['nationality']!='') ){ echo $value['nationality'];}?>">
            </span>

            <span class="primary-inner contact_sort <?php  if(isset($value['psc']) && ($value['psc']!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_psc'];?>" <?php echo $Setting_Delete['contact_psc'];?>>
            <label><?php echo $Setting_Label['contact_psc'];?></label>
            <input type="text" name="psc[<?php echo $cnt; ?>]" class="text-info clr-check-client" value="<?php if(isset($value['psc']) && ($value['psc']!='') ){ echo $value['psc'];}?>">
            </span>

            <span class="primary-inner contact_sort"  data-id="<?php echo $Setting_Order['contact_shareholder'];?>" <?php echo $Setting_Delete['contact_shareholder'];?>>
            <label><?php echo $Setting_Label['contact_shareholder'];?></label>                                              
            <select name="shareholder[<?php echo $cnt; ?>]" class="clr-check-select" >
            <option value="yes">Yes</option>
            <option value="no">No</option>
            </select>
            </span>

            <span class="primary-inner contact_sort <?php  if(isset($value['ni_number']) && ($value['ni_number']!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_ni_number'];?>" <?php echo $Setting_Delete['contact_ni_number'];?>>
            <label><?php echo $Setting_Label['contact_ni_number'];?></label>
            <input type="text" name="ccp_ni_number[<?php echo $cnt; ?>]"  class="text-info clr-check-client" value="<?php if(isset($value['ni_number']) && ($value['ni_number']!='') ){ echo $value['ni_number'];}?>">
            </span>

            <span class="primary-inner  contact_sort <?php if(isset($value['country_of_residence']) && ($value['country_of_residence']!='') ){ ?>light-color_company<?php } ?>"  data-id="<?php echo $Setting_Order['contact_country_of_residence'];?>" <?php echo $Setting_Delete['contact_country_of_residence'];?>>
            <label><?php echo $Setting_Label['contact_country_of_residence'];?></label>
            <input type="text" name="country_of_residence[<?php echo $cnt; ?>]"  class="text-info clr-check-client" value="<?php if(isset($value['country_of_residence']) && ($value['country_of_residence']!='') ){ echo $value['country_of_residence'];}?>">
            </span>

            <span class="primary-inner contact_type contact_sort" data-id="<?php echo $Setting_Order['contact_contact_type'];?>"
                                       <?php echo $Setting_Delete['contact_contact_type'];?>>
            <label><?php echo $Setting_Label['contact_contact_type'];?></label>
            <select name="contact_type" id="contact_type[<?php echo $cnt; ?>]" class="othercus clr-check-select">
            <option value="Director" selected="selected">Director</option>
            <option value="Director/Shareholder">Director/Shareholder</option>
            <option value="Shareholder">Shareholder</option>
            <option value="Accountant">Accountant</option>
            <option value="Bookkeeper">Bookkeeper</option>
            <option value="Other">Other(Custom)</option>
            </select>
            </span>

            <span class="primary-inner spnMulti contact_sort  <?php  if(isset($value['other_custom']) && ($value['other_custom']!='') ){ ?>light-color_company<?php } ?>"  style="display:none" data-id="<?php echo $Setting_Order['contact_other_custom'];?>" <?php echo $Setting_Delete['contact_other_custom'];?> >
            <label><?php echo $Setting_Label['contact_other_custom'];?></label>
            <input type="text" class="text-info clr-check-client" name="other_custom[<?php echo $cnt; ?>]"   value="<?php if(isset($value['other_custom']) && ($value['other_custom']!='') ){ echo $value['other_custom'];}?>">
            </span>
   
            <span class="primary-inner contact_sort <?php if(isset($value['address_line1']) && ($value['address_line1']!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_address_line1'];?>" <?php echo $Setting_Delete['contact_address_line1'];?>>
            <label> <?php echo $Setting_Label['contact_address_line1'];?></label>
            <input type="text" name="address_line1[<?php echo $cnt; ?>]"  class="text-info clr-check-client" value="<?php if(isset($value['address_line1']) && ($value['address_line1']!='') ){ echo $value['address_line1'];}?>">
            </span>
      
            <span class="primary-inner contact_sort <?php if(isset($value['address_line2']) && ($value['address_line2']!='') ){ ?>light-color_company<?php } ?>"  data-id="<?php echo $Setting_Order['contact_address_line2'];?>" <?php echo $Setting_Delete['contact_address_line2'];?>>
            <label><?php echo $Setting_Label['contact_address_line2'];?></label>
            <input type="text" name="address_line2[<?php echo $cnt; ?>]" class=" text-info clr-check-client" value="<?php if(isset($value['address_line2']) && ($value['address_line2']!='') ){ echo $value['address_line2'];}?>">
            </span> 
            <span class="primary-inner contact_sort <?php  if(isset($value['town_city']) && ($value['town_city']!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_town_city'];?>" <?php echo $Setting_Delete['contact_town_city'];?>>
            <label><?php echo $Setting_Label['contact_town_city'];?></label>
             <input type="text" name="town_city[<?php echo $cnt; ?>]"  class=" text-info clr-check-client" value="<?php if(isset($value['town_city']) && ($value['town_city']!='') ){ echo $value['town_city'];}?>">
            </span>
       
            <span class="primary-inner contact_sort <?php if(isset($value['post_code']) && ($value['post_code']!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_post_code'];?>" <?php echo $Setting_Delete['contact_post_code'];?>>
            <label><?php echo $Setting_Label['contact_post_code'];?></label>
            <input type="text" name="post_code[<?php echo $cnt; ?>]"  class="text-info clr-check-client" value="<?php if(isset($value['post_code']) && ($value['post_code']!='') ){ echo $value['post_code'];}?>">
            </span>
                  
            <span class="primary-inner contact_sort" data-id="<?php echo $Setting_Order['contact_landline'];?>"
                                          <?php echo $Setting_Delete['contact_landline'];?>>
            <div class="update-primary cmn-land-append1 pack_add_row_wrpr_landline">

            <label><?php echo $Setting_Label['contact_landline'];?></label>
            <div class="remove_one_row">
            <select name="pre_landline[<?php echo $cnt;?>][0]" class="clr-check-select" >
               <option value="mobile">Mobile</option>
               <option value="work">Work</option>
               <option value="home">Home</option>
               <option value="main">Main</option>
               <option value="workfax">Work Fax</option>
               <option value="homefax">Home Fax</option>
            </select>

            <div class="land-spaces  <?php if(isset($value['landline']) && ($value['landline']!='') ){ ?>light-color_company<?php } ?>">  
            <input type="text" class="text-info clr-check-client" name="landline[<?php echo $cnt;?>][0]"  value="<?php if(isset($value['landline']) && ($value['landline']!='') ){ echo $value['landline'];}?>">
            </div>
            <button type="button" class="btn btn-primary yk_pack_addrow_landline" data-id="<?php echo $cnt;?>" data-length="0">Add Landline</button>

            <!--  <span class="success  text-left <<?php  if(isset($value['contact_from']) && ($value['contact_from']!='') ){ ?>field_hide <?php }else{ ?>light-color_company<?php } ?>">
            </span> -->

         </div>

      </div>

      </span>

         <span class="primary-inner  work-email_section contact_sort" data-id="<?php echo $Setting_Order['contact_work_email'];?>" <?php echo $Setting_Delete['contact_work_email'];?>>
         <div class="update-primary work_lightmail pack_add_row_wrpr_email">
         <label><?php echo $Setting_Label['contact_work_email'];?></label>
         <div class="remove_one_row ">
         <input type="text" class="text-info clr-check-client" name="work_email[<?php echo $cnt;?>][0]"  value="<?php if(isset($value['work_email']) && ($value['work_email']!='') ){ echo $value['work_email'];}?>">

         <button type="button" class="btn btn-primary yk_pack_addrow_email" data-id="<?php echo $cnt;?>" data-length="0">Add Email</button>

         <!-- <span class="success<?php  if(isset($value['contact_from']) && ($value['contact_from']!='') ){ ?> field_hide <?php } ?>">
         </span> -->
         </div>
         </div>
         </span>

<span class="primary-inner contact_sort date_birth  <?php if(isset($value['date_of_birth']) && ($value['date_of_birth']!='') ){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_date_of_birth'];?>" <?php echo $Setting_Delete['contact_date_of_birth'];?>>
<label><?php echo $Setting_Label['contact_date_of_birth'];?></label>
<div class="picker-appoint"><span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span><input type="text" class="text-info date_picker_dob fields clr-check-select" placeholder="dd-mm-yyyy" name="date_of_birth[<?php echo $cnt;?>]" id="date_of_birth" value="<?php if(isset($value['date_of_birth']) && ($value['date_of_birth']!='') ){ echo $value['date_of_birth'];}?>" >
</div>
</span>

<span class="primary-inner contact_sort  <?php if(isset($value['nature_of_control']) && ($value['nature_of_control']!='')){ ?>light-color_company<?php } ?>" data-id="<?php echo $Setting_Order['contact_nature_of_control'];?>" <?php echo $Setting_Delete['contact_nature_of_control'];?>>
<label><?php echo $Setting_Label['contact_nature_of_control'];?></label>
<input type="text" class="text-info clr-check-client" id="nature_of_control"  name="nature_of_control[<?php echo $cnt; ?>]" id="nature_of_control" value="<?php if(isset($value['nature_of_control']) && ($value['nature_of_control']!='') ){ echo $value['nature_of_control'];}?>">
</span>

    <span class="primary-inner contact_sort" data-id="<?php echo $Setting_Order['contact_marital_status'];?>" <?php echo $Setting_Delete['contact_marital_status'];?>>
    <label><?php echo $Setting_Label['contact_marital_status'];?></label>

        <select name="marital_status[<?php echo $cnt; ?>]" id="marital_status" class="clr-check-select">
            <option value="Single">Single</option>
            <option value="Living_together">Living together</option>
            <option value="Engaged">Engaged</option>
            <option value="Married">Married</option>
            <option value="Civil_partner">Civil partner</option>
            <option value="Separated">Separated</option>
            <option value="Divorced">Divorced</option>
            <option value="Widowed">Widowed</option>
        </select>
    </span>
   

            <span class="primary-inner contact_sort  <?php  if(isset($value['utr_number']) && ($value['utr_number']!='') ){ ?>light-color_company<?php } ?>"  data-id="<?php echo $Setting_Order['contact_utr_number'];?>" <?php echo $Setting_Delete['contact_utr_number'];?>>
            <label><?php echo $Setting_Label['contact_utr_number'];?></label>
            <input type="number" class="text-info clr-check-client" id="utr_number"  name="ccp_personal_utr_number[<?php echo $cnt; ?>]" id="utr_number" value="<?php if(isset($value['utr_number']) && ($value['utr_number']!='') ){ echo $value['utr_number'];}?>">
            </span>
  
            <span class="primary-inner contact_sort <?php if(isset($value['occupation']) && ($value['occupation']!='') ){ ?>light-color_company<?php } ?>"   data-id="<?php echo $Setting_Order['contact_occupation'];?>" <?php echo $Setting_Delete['contact_occupation'];?>>
            <label><?php echo $Setting_Label['contact_occupation'];?></label>
            <input type="text" class="text-info clr-check-client" id="occupation"  name="occupation[<?php echo $cnt; ?>]" value="<?php if(isset($value['occupation']) && ($value['occupation']!='') ){ echo $value['occupation'];}?>">
            </span>
   
      <span class="primary-inner date_birth contact_sort <?php if(isset($value['appointed_on']) && ($value['appointed_on']!='') ){ ?>light-color_company<?php } ?> "  data-id="<?php echo $Setting_Order['contact_appointed_on'];?>" <?php echo $Setting_Delete['contact_appointed_on'];?>>
            <label><?php echo $Setting_Label['contact_appointed_on'];?></label>
            <div class="picker-appoint">
            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
            <input type="text" placeholder="dd-mm-yyyy" class="text-info date_picker_dob clr-check-select" id="appointed_on"  name="appointed_on[<?php echo $cnt; ?>]" value="<?php if(isset($value['appointed_on']) && ($value['appointed_on']!='') ){ echo $value['appointed_on'];}?>">
            </div>
            </span>

            <span class="primary-inner contact_sort"  data-id="<?php echo $Setting_Order['contact_personal_tax_return_required'];?>"
            <?php echo $Setting_Delete['contact_personal_tax_return_required'];?>> 
              <label>
                <?php echo $Setting_Label['contact_personal_tax_return_required'];?>
              </label>
              <div class="picker-appoint">          
                  <select name="personal_tax_return_required[<?php echo $cnt;?>]" class="personal_tax_return_required clr-check-select" data-id="<?php echo $cnt;?>">
                    <option value="">Select</option>
                    <option value="no" <?php if(!$value['personal_tax_return_required']) echo "selected='selected'"; ?>>No</option>
                    <option value="yes" <?php if($value['personal_tax_return_required']) echo "selected='selected'"; ?>>Yes</option>
                  </select>
              </div>
            </span>

        <!--  <input type="hidden" name="make_primary[]" id="make_primary" value="0"> -->
          <?php 

          $j++;
          echo render_custom_fields_one( 'Contact_Person' ,FALSE );

           ?>
      </div>
</div>

<div class="service_tr" style="display: none;">
    <table>
        <tr>
            <td>
                Personal Tax Return
                <b class="ccp_name_<?php echo $cnt;?>"> - <?php echo $value['first_name'].' '.$value['last_name'];?></b>
            </td>
            <td class="switching">
                <input type="checkbox" class="js-small f-right services ccp" name="ccp_ptr_service[<?php echo $cnt;?>]" data-id="ccp_<?php echo $cnt?>" value="1">
            </td>
            <td class="swi1 splwitch">
                <input type="checkbox" class="js-small f-right reminder ccp" name="ccp_ptr_reminder[<?php echo $cnt;?>]"  data-id="ccp_<?php echo $cnt?>_reminder" value="1">
            </td>
            <td class="swi2 textwitch">
                <input type="checkbox" class="js-small f-right text ccp" name="ccp_ptr_text[<?php echo $cnt;?>]" data-id="ccp_<?php echo $cnt?>" value="1">
            </td>
            <td class="swi3 invoicewitch">
                <input type="checkbox" class="js-small f-right invoice ccp" name="ccp_ptr_invoice[<?php echo $cnt;?>]"  data-id="ccp_<?php echo $cnt?>" value="1">
            </td>
        </tr>
    </table>
</div>

<div class="service_content">

<!-- personal-tax tab start -->

<li class="show-block" style="display:none" >
    <a href="#" class="toggle">Personal Tax Return 
        <b class="ccp_name_<?php echo $cnt;?>"> - <?php echo $value['first_name'].' '.$value['last_name'];?></b>
    </a>
<!-- personal-tax-returns-->               
  <div id="ccp_<?php echo $cnt;?>_personal_tax_returns" class="ccp_ptr_cnt masonry-container floating_set inner-views LISTEN_CONTENT">
     <div class="grid-sizer"></div>
     <div class="accordion-panel">
        <div class="box-division03">
           <div class="accordion-heading" role="tab" id="headingOne">
              <h3 class="card-title accordion-title">
                 <a class="accordion-msg">Important Information</a>
              </h3>
           </div>
           <div id="collapse" class="panel-collapse">
              <div class="basic-info-client1 CONTENT_IMPORTANT_INFORMATION" id="personal_box">
                <input type="hidden" class="client_contacts_id"  value="<?php echo $contact_id;?>">
                 <div class="form-group row personal_sort" data-id="<?php echo $Setting_Order['crm_personal_utr_number']; ?>"
                   <?php echo $Setting_Delete['crm_personal_utr_number']; ?>>
                     <label class="col-sm-4 col-form-label">
                        <?php echo $Setting_Label['crm_personal_utr_number']; ?>                        
                     </label>
                    <div class="col-sm-8">
                       <input type="text" name="ccp_personal_utr_number[<?php echo $cnt;?>]" class="form-control fields clr-check-client">
                    </div>
                 </div>
                 <div class="form-group row personal_sort" data-id="<?php echo $Setting_Order['crm_ni_number']; ?>"
                  <?php echo $Setting_Delete['crm_ni_number']; ?>>
                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_ni_number']; ?></label>
                    <div class="col-sm-8">
                       <input type="text" name="ccp_ni_number[<?php echo $cnt;?>]" class="form-control fields clr-check-client" >
                    </div>
                 </div>
                 <div class="form-group row radio_bts personal_sort" data-id="<?php echo $Setting_Order['crm_property_income']; ?>" <?php echo $Setting_Delete['crm_property_income']; ?>>
                    <label class="col-sm-4 col-form-label">
                      <?php echo $Setting_Label['crm_property_income']; ?>
                    </label>
                    <div class="col-sm-8">
                       <input type="checkbox" class="js-small f-right fields clr-check-client" name="ccp_property_income[<?php echo $cnt;?>]" data-id="property_income" value="1">
                    </div>
                 </div>
                 <div class="form-group row radio_bts personal_sort" data-id="<?php echo $Setting_Order['crm_additional_income']; ?>" <?php echo $Setting_Delete['crm_additional_income']; ?>>
                    <label class="col-sm-4 col-form-label">
                      <?php echo $Setting_Label['crm_additional_income']; ?>
                    </label>
                    <div class="col-sm-8">
                       <input type="checkbox" class="js-small f-right fields clr-check-client" name="ccp_additional_income[<?php echo $cnt;?>]" value="1">
                    </div>
                 </div>

                  <div class="form-group row text-box1 personal_sort property_income_notes" data-id="<?php echo $Setting_Order['property_income_notes']; ?>" style="display:none">
                   <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['property_income_notes']; ?> </label>
                   <div class="col-sm-8">
                      <textarea rows="3" placeholder="" name="ccp_property_income_notes[<?php echo $cnt;?>]"  class="fields">                                             
                     </textarea>
                   </div>
                  </div>
              </div>
           </div>
        </div>
     </div>
     <!-- accordion-panel -->
     <div class="accordion-panel">
        <div class="box-division03">
           <div class="accordion-heading" role="tab" id="headingOne">
              <h3 class="card-title accordion-title">
                 <a class="accordion-msg">Personal Tax Return</a>
              </h3>
           </div>
           <div id="collapse" class="panel-collapse">
              <div class="basic-info-client1 CONTENT_PERSONAL_TAX_RETURN" id="personal_box1">
                 <div class="form-group row name_fields date_birth personal_sort" data-id="<?php echo $Setting_Order['crm_personal_tax_return_date']; ?>">
                    <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_personal_tax_return_date']; ?>                                      
                    <span class="Hilight_Required_Feilds">*</span>
                    </label>
                    <div class="col-sm-8">
                       <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>  <input type="text" name="ccp_personal_tax_return_date[<?php echo $cnt;?>]"  placeholder="dd-mm-yyyy"  class="datepicker-13 fields LISTEN_CHANGES clr-check-client" data-cntid="<?php echo $cnt;?>">
                    </div>
                 </div>
                 <div class="form-group row name_fields date_birth personal_sort" data-id="<?php echo $Setting_Order['crm_personal_due_date_return']; ?>" <?php echo $Setting_Delete['crm_personal_due_date_return']; ?>>
                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_personal_due_date_return']; ?></label>
                    <div class="col-sm-8">
                       <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>  <input type="text" name="ccp_personal_due_date_return[<?php echo $cnt;?>]" placeholder="dd-mm-yyyy"  class="fields datepicker-13 clr-check-client">
                    </div>
                 </div>
                 <div class="form-group row name_fields date_birth personal_sort" data-id="<?php echo $Setting_Order['crm_personal_due_date_online']; ?>" <?php echo $Setting_Delete['crm_personal_due_date_online']; ?>>
                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_personal_due_date_online']; ?></label>
                    <div class="col-sm-8">
                       <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>   <input type="text" name="ccp_personal_due_date_online[<?php echo $cnt;?>]" placeholder="dd-mm-yyyy" class="fields datepicker-13 clr-check-client">
                    </div>
                 </div>
              </div>
           </div>
        </div>
     </div>
     <!-- accordion-panel -->
     <div class="accordion-panel" style="display:none;" id="ccp_<?php echo $cnt?>_reminder">
        <div class="box-division03">
           <div class="accordion-heading" role="tab" id="headingOne">
              <h3 class="card-title accordion-title">
                 <a class="accordion-msg">Personal tax Reminders</a>
              </h3>
           </div>
           <div id="collapse" class="panel-collapse">
              <div class="basic-info-client1 CONTENT_PERSONAL_TAX_RETURN_REMINDERS" id="personal_box2">

                 <div class="form-group row name_fields radio_bts personal_sort" data-id="<?php echo $Setting_Order['crm_personal_next_reminder_date']; ?>" <?php echo $Setting_Delete['crm_personal_next_reminder_date']; ?>>
                    <label class="col-sm-4 col-form-label">
                        <a id="personal_custom_reminder_link" name="personal_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/4" target="_blank"><?php echo $Setting_Label['crm_personal_next_reminder_date']; ?></a>
                    </label>
                    <div class="col-sm-8">                   
                       <input type="checkbox" class="js-small f-right fields as_per_firm LISTEN_CHANGES clr-check-client" name="ccp_ptr_as_per_firm_reminder[<?php echo $cnt;?>]" value="1">
                    </div>
                 </div>

                 <div class="form-group row radio_bts personal_custom_reminder_label personal_sort" data-id="<?php echo $Setting_Order['crm_personal_custom_reminder']; ?>" <?php echo $Setting_Delete['crm_personal_custom_reminder']; ?>>

                    <label id="personal_custom_reminder_label" name="ccp_personal_custom_reminder_label[<?php echo $cnt;?>]" class="col-sm-4 col-form-label">
                      <?php echo $Setting_Label['crm_personal_custom_reminder']; ?>                      
                    </label>

                    <div class="col-sm-8" id="personal_custom_reminder">
                       <input type="checkbox" class="js-small f-right fields cus_reminder LISTEN_CHANGES clr-check-client" name="ccp_ptr_cus_reminder[<?php echo $cnt;?>]"  data-id="personal_cus" data-serid="4" value="1">
                    </div>
                 </div>
              </div>
           </div>
        </div>
     </div>
      <input type="hidden" class="UPDATE_LISTENED" name="is_service_data_edited[4_<?php echo $contact_id;?>]" value="0">
      <div class="SATR_custom_fields" style="display: none;">
        <?php echo render_custom_fields_one( '4' ,FALSE ); ?>
      </div>
  </div>              
<!-- Personal Tax Return -->
</li>
<!-- personal-tax tab start -->
</div>

</div>

<script>
   $(document).ready(function(){
		$(".clr-check-client").keyup(function(){
			var clr = $(this).val();
			if(clr.length >= 3){
				$(this).addClass("clr-check-client-outline");
			}else{
				$(this).removeClass("clr-check-client-outline");
			}
		});
      $(".clr-check-client").each(function(i){
      if($(this).val() !== ""){
        $(this).addClass("clr-check-client-outline");
      }  
    })

		$(".clr-check-select").change(function(){
			var clr_select = $(this).val();
         if(clr_select !== ' '){
            $(this).addClass("clr-check-select-client-outline");
         }else{
				$(this).removeClass("clr-check-select-client-outline");
			}
		});

      var check_class = $(".form-group").hasClass("Contact_Person");
      if(check_class){
         $(".Contact_Person > .col-sm-8 > select.form-control").addClass("clr-check-select");
         
         $(".clr-check-select").change(function(){
			var clr_select = $(this).val();
         if(clr_select !== ' '){
            $(this).addClass("clr-check-select-client-outline");
         }else{
				$(this).removeClass("clr-check-select-client-outline");
			}
		});
      }
   });
</script>