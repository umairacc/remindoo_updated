<?php 
   //$this->load->view('includes/header');
      // $role = $this->Common_mdl->getRole($_SESSION['id']);
      // function convertToHoursMins($time, $format = '%02d:%02d') {
      //  if ($time < 1) {
      //      return;
      //  }
      //  $hours = floor($time / 60);
      //  $minutes = ($time % 60);
      //  return sprintf($format, $hours, $minutes);
      // }
      
      ?>
<html lang="en">
   <head>
      <title>CRM </title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="description" content="#">
      <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
      <meta name="author" content="#">
      <!-- Favicon icon -->
      <link rel="icon" href="http://remindoo.org/CRMTool/assets/images/favicon.ico" type="image/x-icon">
      <!-- Google font-->
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
      <!-- Required Fremwork -->
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/bower_components/bootstrap/css/bootstrap.min.css">
      <!-- themify-icons line icon -->
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/icon/themify-icons/themify-icons.css">
      <!-- ico font -->
      <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/icon/icofont/css/icofont.css">
      <!-- flag icon framework css -->
      <!-- Menu-Search css -->
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/pages/menu-search/css/component.css">
      <!-- jpro forms css -->
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/pages/j-pro/css/demo.css">
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/pages/j-pro/css/font-awesome.min.css">
      <!--  <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/pages/j-pro/css/j-pro-modern.css"> -->
      <!-- Style.css -->
      <link type="text/css" href="http://remindoo.org/CRMTool/bower_components/bootstrap-timepicker/css/bootstrap-timepicker.min.css" />
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/style.css">
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/pages/advance-elements/css/bootstrap-datetimepicker.css">
      <!-- Date-range picker css  -->
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/bower_components/bootstrap-daterangepicker/css/daterangepicker.css" />
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/bower_components/datedropper/css/datedropper.min.css" />
      <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
      <link rel="stylesheet" href="https:/resources/demos/style.css">
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/bower_components/switchery/css/switchery.min.css">
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/jquery.mCustomScrollbar.css">
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/bower_components/owl.carousel/css/owl.carousel.css">
      <!--     <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/dataTables.bootstrap4.min.css">
         <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/buttons.dataTables.min.css?ver=2">
         <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/responsive.bootstrap4.min.css"> -->
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/ui_style.css?ver=3">
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/common_style.css?ver=4">
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/common_responsive.css?ver=2">
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool//assets/css/dataTables.css">
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool//assets/css/jquery.dropdown.css">
      <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
      <link rel="stylesheet" type="text/css" href="https://bootswatch.com/superhero/bootstrap.min.css">
      <link href="http://remindoo.org/CRMTool/css/jquery.signaturepad.css" rel="stylesheet">
      <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
      <link rel="stylesheet" href="http://remindoo.org/CRMTool/assets/css/bootstrap-colorpicker.min.css">
      <link rel="stylesheet" href="http://remindoo.org/CRMTool/assets/css/bootstrap-slider.min.css">
      <link rel="stylesheet" href="http://remindoo.org/CRMTool/assets/plugins/medium-editor/medium-editor.min.css">
      <link rel="stylesheet" href="http://remindoo.org/CRMTool/assets/plugins/medium-editor/template.min.css">
      <!-- <link rel="stylesheet" href="http://remindoo.org/CRMTool/assets/css/style1.css"> -->
      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.css">
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool//assets/icon/simple-line-icons/css/simple-line-icons.css">
      <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool//assets/icon/icofont/css/icofont.css">
      <style>
         .common_form_section1  .col-xs-12.col-sm-6.col-md-6 {
         float: left !important;
         }
         .common_form_section1 {
         background: #fff;
         box-shadow: 1px 2px 5px #dbdbdb;
         border-radius: 5px;
         padding: 30px;
         }
         .common_form_section1 label{
         display: block;
         margin-bottom: 7px;
         font-size: 15px;
         }
         .common_form_section1 input{
         width: 100%;
         }
         .col-sm-12{
         float: left;
         }
         .col-sm-8 {
         float: left;
         }
         .pcoded-content.card12.pre-pro {
            float: left;
            width: 100%;
        }
        .pcoded-content.card12.pre-pro table {
            display: table;
        }
        .pcoded-content.card12.pre-pro {
            background: #F6F7FB;
        }
        
      </style>
   </head>
   <div class="LoadingImage"></div>
   <style>
      .LoadingImage {
      display : none;
      position : fixed;
      z-index: 100;
      background-image : url('http://remindoo.org/CRMTool/assets/images/ajax-loader.gif');
      background-color:#666;
      opacity : 0.4;
      background-repeat : no-repeat;
      background-position : center;
      left : 0;
      bottom : 0;
      right : 0;
      top : 0;
      }
   </style>
   <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/responsive.dataTables.css">
   <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/buttons.dataTables.min.css?ver=2">
   <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/progress-circle.css">
   <div class="theme-loader">
      <div class="ball-scale">
         <div class='contain'>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
         </div>
      </div>
   </div>
   <div class="pcoded-content card12 pre-pro">
      <div class="pcoded-inner-content">
         <input type="hidden" name="password"  id="password" value="<?php echo $records['password']; ?>">
         <!-- Main-body start -->
         <div class="main-body">
            <div class="page-wrapper">
               <div class="deadline-crm1 floating_set">
                <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                  <li class="nav-item"><a class="nav-link"><?php echo $records['proposal_name']; ?></a></li>
                </ul>
                  <a href="<?php echo base_url(); ?>proposal/proposal_feedback/<?php echo $records['id']; ?>" class="qus-ans btn btn-sm btn-primary f-right">Question? Discuss Now</a>
               </div>
               <!-- Page body start -->
               <div class="page-body task-d">
                  <div class="row">
                     <div class="card col-xs-12">
                        <center>
                           <?php echo $records['proposal_contents']; ?>
                           <div class="fixed-div">
                              <span class="fixed-tx">
                              <a href="<?php echo base_url(); ?>user/pdf_download/<?php echo $records['id']; ?>"><i class="fa fa-download" aria-hidden="true"></i>Download PDF</a>
                              </span>
                              <span class="accpt-pro">
                              <span class="dec-text"><a href="javascript:;" data-toggle="modal" data-target="#decline_proposal">Decline</a> or </span>
                              <a href="javascript:;" data-toggle="modal" data-target="#accept_proposal" class="ac-proposal btn btn-sm btn-primary f-right">accept proposal</a>
                              </span>
                           </div>
                        </center>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Warning Section Starts -->
   <div class="modal fade" id="accept_proposal" role="dialog">
      <div class="modal-dialog">
         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Accept Proposal</h4>
            </div>
            <div class="modal-body">
               <p class="success_msg" style="color:red;display: none;"> Proposal Accepted </p>
               <input type="hidden" name="id" id="proposal_id" value="<?php echo $records['id']; ?>">
               <p>Do You Accept the Proposal</p>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" id="proposal_accept">Yes</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            </div>
         </div>
      </div>
   </div>
   <div class="modal fade" id="decline_proposal" role="dialog">
      <div class="modal-dialog">
         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Accept Proposal</h4>
            </div>
            <div class="modal-body">
               <p class="success_msg" style="color:red;display: none;"> Proposal Declined </p>
               <input type="hidden" name="id" id="proposal_id" value="<?php echo $records['id']; ?>">
               <p>Do You Decline the Proposal</p>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" id="proposal_decline">Yes</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            </div>
         </div>
      </div>
   </div>
   <div class="modal fade" id="password_popup" role="dialog">
      <div class="modal-dialog">
         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header">
               <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
               <h4 class="modal-title">Proposal Access key</h4>
            </div>
            <div class="modal-body">
               <p class="wrong_msg" style="color:red;display: none;"> Proposal Key Wrong </p>
               <label>Enter Access key:</label>
               <input type="hidden" name="id" id="proposal_id" value="<?php echo $records['id']; ?>">
               <input type="password" name="pdf_password" id="pdf_password" value="">
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" id="access_key">Check</button>
               <!--  <button type="button" class="btn btn-default" data-dismiss="modal">No</button> -->
            </div>
         </div>
      </div>
   </div>
   <!-- Warning Section Ends -->
   <?php //$this->load->view('includes/session_timeout');?>
   <?php $fun = $this->uri->segment(2);?>
   <!-- Required Jquery -->
   <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-ui/js/jquery-ui.min.js"></script>
   <script src="<?php echo base_url();?>assets/js/timezones.full.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>bower_components/popper.js/js/popper.min.js"></script>  
   <script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap/js/bootstrap.min.js"></script>  
   <!-- j-pro js -->
   <!-- <script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.ui.min.js"></script> -->
   <script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.maskedinput.min.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.j-pro.js"></script>
   <!-- jquery slimscroll js -->
   <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
   <!-- modernizr js -->
   <script type="text/javascript" src="<?php echo base_url();?>bower_components/switchery/js/switchery.min.js"></script>
   <!--  
      <script type="text/javascript" src="<?php echo base_url();?>bower_components/modernizr/js/css-scrollbars.js"></script> -->
   <!-- <script src="<?php echo base_url();?>assets/js/mmc-common.js"></script>
      <script src="<?php echo base_url();?>assets/js/mmc-chat.js"></script>
      <script type="text/javascript" src="<?php echo base_url();?>assets/js/chat.js"></script> -->
   <!-- Custom js -->
   <script src="<?php echo base_url();?>assets/js/pcoded.min.js"></script>
   <script src="<?php echo base_url();?>assets/js/demo-12.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>assets/js/masonry.pkgd.min.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script> 
   <script type="text/javascript" src="<?php echo base_url();?>assets/js/common_script.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
   <script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
   <script src="<?php echo base_url();?>assets/js/mock.js"></script>
   <!-- <script src="https://use.fontawesome.com/86c8941095.js"></script> -->
   <?php $url = $this->uri->segment('2');?>
   <? if ($url == 'step_proposal' || $url == 'template_design' || $url=='templates') { ?>
   <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
   <?php } ?>
   <script src="<?php echo base_url();?>assets/js/bootbox.min.js"></script>
   <script src="<?php echo base_url();?>assets/js/debounce.js"></script>
   <script src="<?php echo base_url();?>assets/js/bootstrap-colorpicker.min.js"></script>
   <!--   <script src="<?php //echo base_url();?>assets/js/bootstrap-slider.min.js"></script> -->
   <script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.js"></script>
   <script src="//cdn.jsdelivr.net/medium-editor/latest/js/medium-editor.min.js"></script>
   <script src="<?php echo base_url(); ?>assets/js/creative.tools.js"></script>
   <script src="<?php echo base_url(); ?>assets/js/html2canvas.js"></script>
   <script src="<?php echo base_url(); ?>assets/js/image-edit.js"></script>
   <script src="<?php echo base_url(); ?>assets/js/editor.js"></script>
   <!--   <script src="<?php //echo base_url();?>js/jquery.signaturepad.js"></script>
      <script src="<?php //echo base_url();?>js/json2.min.js"></script> -->
   <script>
      // $(document).ready(function() {
      
      //  $("ul#owl-demo1 li").on('click',function() {
      
       
      
      //    $(this).find('ul.dropdown-menu').slideToggle();
      //  });
      
      // });
      
      $( document ).ready(function() {
      
      
          var date = $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
      
          // Multiple swithces
          var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));
      
          elem.forEach(function(html) {
              var switchery = new Switchery(html, {
                  color: '#1abc9c',
                  jackColor: '#fff',
                  size: 'small'
              });
          });
      
          $('#accordion_close').on('click', function(){
                  $('#accordion').slideToggle(300);
                  $(this).toggleClass('accordion_down');
          });
      
      
          $('.chat-single-box .chat-header .close').on('click', function(){
            $('.chat-single-box').hide();
          });
      
      
      var resrow = $('#responsive-reorder').DataTable({
          rowReorder: {
              selector: 'td:nth-child(2)'
          },
          responsive: true
      });
      });
   </script>
   </body>
   <!-- chat box -->
   <!-- <div class="chat-box cus-tinybox">
      <ul class="text-right boxs">
         <li class="chat-single-box card-shadow bg-white active chatbox_2340" data-id="2340">
            <div class="had-container">
               <div class="chat-header p-10 bg-gray">
                  <div class="user-info d-inline-block f-left">
                     <div class="box-live-status d-inline-block m-r-10 bg-danger"></div>
                     <a href="#">demouser2</a>
                  </div>
                  <div class="box-tools d-inline-block"><a href="#" class="mini"><i class="icofont icofont-minus f-20 m-r-10"></i></a><a class="close" href="#"><i class="icofont icofont-close f-20"></i></a></div>
               </div>
               <div class="chat-body p-10">
                  <div class="message-scrooler">
                     <input type="hidden" id="ajax_value_2340" name="ajax_value">
                     <div class="messages our_message_2340"></div>
                  </div>
               </div>
               <div class="chat-footer b-t-muted">
                  <div class="input-group write-msg"><input type="text" class="form-control input-value" name="text_message" data-id="2340" id="text_message_2340" placeholder="Type a Message"><span class="input-group-btn"><button id="paper-btn" class="btn btn-primary message_send" data-id="2340" type="button"><i class="icofont icofont-paper-plane"></i></button></span></div>
               </div>
            </div>
         </li>
      </ul>
      </div> -->
   <!-- chat box -->
   <!-- modal 1-->
   <div class="modal fade all_layout_modal" id="default-Modal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h4 class="modal-title">Search Companines House</h4>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <div class="modal-body">
               <div class="input-group input-group-button input-group-primary modal-search">
                  <input type="text" class="form-control" placeholder="Search here..." id="searchCompany">
                  <button class="btn btn-primary input-group-addon" id="basic-addon1">
                  <i class="ti-search" aria-hidden="true"></i>
                  </button>
               </div>
               <div class="british_gas1 trading_limited" id="searchresult">
               </div>
               <div class="british_view" style="display:none" id="companyprofile">
                  <!-- <div class="br_company_profile">
                     <h3>british gas trading limited</h3>
                     <div class="company_numbers">
                     <span>Company Number</span>    
                     <p>03078711</p>
                     </div>
                     <div class="company_numbers">
                     <span>Company Status</span>    
                     <p>Active</p>
                     </div>    
                     <div class="company_numbers">
                     <span>Incorporated Date</span>    
                     <p>06/07/1995</p>
                     </div>
                     <div class="company_numbers">
                     <span>Accounts Reference Date</span>    
                     <p>31/12</p>
                     </div>
                     <div class="company_numbers">
                     <span>Charges</span>    
                     <p><strong>1</strong></p>
                     </div>
                     <div class="company_numbers">
                     <span>Current Directors</span>    
                     <p>R.O.Y. International PO Box 13056 ISL-61130 TEL-AVIV ISRAEL</p>
                     </div>
                     <div class="company_numbers">
                     <span>Registered Directors</span>    
                     <p>The Israel Philatelic Service 12 Shderot Yerushalayim 68021 Tel Aviv - Yafo ISRAEL</p>
                     </div>
                     <div class="company_chooses">
                     <a href="#">select company</a>
                     <a href="#">View on Companies House</a>
                     </div>
                     </div> -->
               </div>
               <div class="main_contacts" id="selectcompany" style="display:none">
                  <h3>british gas trading limited</h3>
                  <div class="trading_tables">
                     <div class="trading_rate">
                        <p>
                           <span><strong>BARBARO,Gab</strong></span>
                           <span>Born 1971</span>
                           <span><a href="#">Use as Main Contact</a></span>
                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <!-- modalbody -->
         </div>
      </div>
   </div>
   <!-- modal-close -->
   <!--modal 2-->
   <div class="modal fade all_layout_modals" id="exist_person" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h4 class="modal-title">Select Existing Person</h4>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <div class="modal-body">
               <div class="main_contacts" id="main_contacts" style="display:block">     
               </div>
            </div>
            <!-- modalbody -->
         </div>
      </div>
   </div>
   <!-- modal-close -->
   <!-- modal 2-->
</html>
<script type="text/javascript">
  $(document).ready(function() {

    $('.task-d').find('p').css('text-align', 'left');

  });
</script>
<script>
   $("#searchCompany").keyup(function(){
   
   
    $(".form_heading").html("<h2>Adding Client via Companies House</h2>");
         var currentRequest = null;    
         var term = $(this).val();
         //var term = $(this).val().replace(/ +?/g, '');
         //var term = $(this).val($(this).val().replace(/ +?/g, ''));
         $("#searchresult").show();
         $('#selectcompany').hide();
          $(".LoadingImage").show();
         currentRequest = $.ajax({
            url: '<?php echo base_url();?>client/SearchCompany/',
            type: 'post',
            data: { 'term':term },
            beforeSend : function()    {           
            if(currentRequest != null) {
                currentRequest.abort();
            }
        },
            success: function( data ){
                $("#searchresult").html(data);
                 $(".LoadingImage").hide();
                },
                error: function( errorThrown ){
                    console.log( errorThrown );
                }
            });
       });
   
   function addmorecontact(id)
   {
   $(".LoadingImage").hide();
   $(".modal-backdrop").css("display","none");
      var url = $(location).attr('href').split("/").splice(0, 8).join("/");
   
      var segments = url.split( '/' );
      var fun = segments[5];
   
   if(fun!='' && fun !='addnewclient'  && fun !='addnewclient#')
   {
      $(location).attr('href', '<?php echo base_url();?>client/addnewclient/'+id);
   }  else{
     $(location).attr('href', '<?php echo base_url();?>client/addnewclient/'+id);
     $("#companyss").attr("href","#");
   
   $('.contact_form').show();
   $('.all_layout_modal').modal('hide');
   
       $('.nav-link').removeClass('active');
    //   $('.main_contacttab').addClass('active');
   //$('.contact_form').show();
   
   $("#company").removeClass("show");
   $("#company").removeClass("active");
   $("#required_information").removeClass("active");
   $(".main_contacttab").addClass("active");
   $("#main_Contact").addClass("active");
   $("#main_Contact").addClass("show"); 
   }      
                  
    /* $("#companyss").attr("href","#");
   
   $('.contact_form').show();
   $('.all_layout_modal').modal('hide');
   
       $('.nav-link').removeClass('active');
    //   $('.main_contacttab').addClass('active');
   //$('.contact_form').show();
   
   $("#company").removeClass("show");
   $("#company").removeClass("active");
   $("#required_information").removeClass("active");
   $(".main_contacttab").addClass("active");
   $("#main_Contact").addClass("active");
   $("#main_Contact").addClass("show");
   */
   //$( "." ).tabs( { disabled: [1, 2] } );
    // $(".it3").attr("style", "display:none");
   //$(".it3").tabs({disabled: true });
   }
   
   var xhr = null;
     function getCompanyRec(companyNo)
     {
        $(".LoadingImage").show();
         if( xhr != null ) {
                    xhr.abort();
                    xhr = null;
            }
         
        xhr = $.ajax({
            url: '<?php echo base_url();?>client/CompanyDetails/',
            type: 'post',
            data: { 'companyNo':companyNo },
            success: function( data ){
                //alert(data);
                $("#searchresult").hide();
                $('#companyprofile').show();
                $("#companyprofile").html(data);
                $(".LoadingImage").hide();
                
                },
                error: function( errorThrown ){
                    console.log( errorThrown );
                }
            });}
   
   
        function getCompanyView(companyNo)
     {
   
        //$('.modal-search').hide();
        $(".LoadingImage").show();
       var user_id= $("#user_id").val();
       
         if( xhr != null ) {
                    xhr.abort();
                    xhr = null;
            }
     
         
        xhr = $.ajax({
            url: '<?php echo base_url();?>client/selectcompany/',
            type: 'post',
            dataType: 'JSON',
            data: { 'companyNo':companyNo,'user_id': user_id},
            success: function( data ){
                //alert(data.html);
                $("#searchresult").hide();
                $('#companyprofile').hide();
                $('#selectcompany').show();
                $("#selectcompany").html(data.html);
                $('.main_contacts').html(data.html);
     // append form field value
     $("#company_house").val('1');
   
     //alert($("#company_house").val());
     $("#company_name").val(data.company_name);
     $("#company_name1").val(data.company_name);
     $("#company_number").val(data.company_number);
     $("#companynumber").val(data.company_number);
     $("#company_url").val(data.company_url);
     $("#company_url_anchor").attr("href",data.company_url);
     $("#officers_url").val(data.officers_url);
     $("#officers_url_anchor").attr("href",data.officers_url);
     $("#company_status").val(data.company_status);
     $("#company_type").val(data.company_type);
     //$("#company_sic").append(data.company_sic);
     $("#company_sic").val(data.company_sic);
     $("#sic_codes").val(data.sic_codes);
     
     $("#register_address").val(data.address1+"\n"+data.address2+"\n"+data.locality+"\n"+data.postal);
     $("#allocation_holder").val(data.allocation_holder);
     $("#date_of_creation").val(data.date_of_creation);
     $("#period_end_on").val(data.period_end_on);
     $("#next_made_up_to").val(data.next_made_up_to);
     $("#next_due").val(data.next_due);
     $("#accounts_due_date_hmrc").val(data.accounts_due_date_hmrc);
     $("#accounts_tax_date_hmrc").val(data.accounts_tax_date_hmrc);
     $("#confirm_next_made_up_to").val(data.confirm_next_made_up_to);
     $("#confirm_next_due").val(data.confirm_next_due);
     
     $("#tradingas").val(data.company_name);
     $("#business_details_nature_of_business").val(data.nature_business);
     $("#user_id").val(data.user_id);
   
     $(".edit-button-confim").show();
     $(".LoadingImage").hide();
                
                },
                error: function( errorThrown ){
                    console.log( errorThrown );
                }
            });}
     
     
        function getmaincontact(companyNo,appointments,i,birth)
     {
        $(".LoadingImage").show();
        var user_id= $("#user_id").val();
         if( xhr != null ) {
                    xhr.abort();
                    xhr = null;
            }
   
            var cnt = $("#append_cnt").val();
   
              if(cnt==''){
              cnt = 1;
             }else{
              cnt = parseInt(cnt)+1;
             }
          $("#append_cnt").val(cnt);
   
          var incre=$("#incre").val();
          if(incre==''){
              incre = 1;
             }else{
              incre = parseInt(incre)+1;
             }
          $("#incre").val(incre);
        xhr = $.ajax({
            url: '<?php echo base_url();?>client/maincontact/',
            type: 'post',
            
            data: { 'companyNo':companyNo,'appointments': appointments,'user_id': user_id,'cnt':cnt,'birth':birth,'incre':incre},
            success: function( data ){
   
              $("#usecontact"+i).html('<span class="succ_contact"><a href="#">Added</a></span>');
                //$("#default-Modal,.modal-backdrop.show").hide();
                  // $('#default-Modal').modal('hide');
                   // $('#exist_person').modal('hide');
         $('.contact_form').append(data);
     $(".LoadingImage").hide();
     
     // append form field value
     /*$("#title").val(data.title);
     $("#first_name").val(data.first_name);
     $("#middle_name").val(data.middle_name);
     $("#last_name").val(data.last_name);
     $("#postal_address").val(data.premises+"\n"+data.address_line_1+"\n"+data.address_line_2+"\n"+data.region+"\n"+data.locality+"\n"+data.country+"\n"+data.postal_code);
     $(".LoadingImage").hide();
     */
                
                },
                error: function( errorThrown ){
                    console.log( errorThrown );
                }
            });}
   function backto_company()
   {
     $('#companyprofile').show();
     $('.main_contacts').hide();
   }
   
</script>
<script type="text/javascript">
   $(function(){
     
     var headheight = $('.header-navbar').outerHeight();
     var navbar = $('.remin-admin');
     
   
   
   // if ($(window).width() > 1024) {
   //                $(window).scroll(function() {
   //                    if ($(window).scrollTop() >= 100) {
   //                        navbar.addClass('navbar-scroll');
   //          $('.remin-admin.navbar-scroll').css({"top":headheight+"px"});
           
   //                    } else {
   
   //                        navbar.removeClass('navbar-scroll');
   //                    }
   //                });
   //            }
   
     // if ($(window).width() > 1024) {
     //   $(document).scroll(function() {
     //     var scrollheight1 = $(this).scrollTop();
     //     console.log(scrollheight1);
     //     if (scrollheight1 > 200) {
     //         navbar.addClass('navbar-scroll');
     //         $('.remin-admin.navbar-scroll').css({"top":headheight+"px"});
     //     } else {
     //       navbar.removeClass('navbar-scroll');
     //     }
     //   });
     // }
   
   });
   
</script>
<script type="text/javascript">
   $(document).ready(function(){
   
     var $grid = $('.masonry-container').masonry({
       itemSelector: '.accordion-panel',
       percentPosition: true,
       columnWidth: '.grid-sizer' 
     });
   
     $(document).on( 'click', 'td.splwitch .switchery', function() {
       setTimeout(function(){ $grid.masonry('layout'); }, 600);
     });
   
       $(document).on( 'click', 'ol .nav-item a', function() {
           setTimeout(function(){ $grid.masonry('layout'); }, 600);
       });
   
       $(document).on( 'change', '.main-pane-border1', function() {
         setTimeout(function(){ $grid.masonry('layout'); }, 600);
       });
   
       $(document).on( 'click', '.btn', function() {
         setTimeout(function(){ $grid.masonry('layout'); }, 600);
        });
   
       $(document).on( 'click', '.switching', function() {
         setTimeout(function(){ $grid.masonry('layout'); }, 600);
        });
   
       $(document).on( 'click', '.service-table-client .checkall', function() {
         setTimeout(function(){ $grid.masonry('layout'); }, 600);
        });
   
       $(document).on( 'click', '.service-table-client .switchery', function() {
         setTimeout(function(){ $grid.masonry('layout'); }, 600);
        });
   
       setTimeout(function(){ $grid.masonry('layout'); }, 600);
   
       var test = $('body').height();
       $('test').scrollTop(test);
   
    });
</script>
<!-- <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap.js"></script> -->
<!-- <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>  -->
<script src="<?php echo base_url();?>js/jquery.workout-timer.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="http://remindoo.org/CRMTool/assets/js/dataTables.responsive.min.js"></script>
<script>
   $("#proposal_accept").click(function(){
      //  alert('ok');
     // alert($(this).val());
      var status='accepted';
      var id=$("#proposal_id").val();
    //  alert(id);
      var formData={'id':id,'status':status};
         $.ajax({
            url: '<?php echo base_url(); ?>proposals/status_change',
            type : 'POST',
            data : formData,                    
              beforeSend: function() {
                $(".LoadingImage").show();
              },
              success: function(data) {
                 $(".LoadingImage").hide();
                 $(".success_msg").show();
                 setTimeout(function(){  $(".success_msg").hide();  $("#accept_proposal").modal('toggle'); }, 3000);
                // alert(data);
              }
          });
   });
   
   $("#proposal_decline").click(function(){
      //  alert('ok');
     // alert($(this).val());
      var status='declined';
      var id=$("#proposal_id").val();
    //  alert(id);
      var formData={'id':id,'status':status};
         $.ajax({
            url: '<?php echo base_url(); ?>proposals/status_change',
            type : 'POST',
            data : formData,                    
              beforeSend: function() {
                $(".LoadingImage").show();
              },
              success: function(data) {
                 $(".LoadingImage").hide();
                 $(".success_msg").show();
                 setTimeout(function(){  $(".success_msg").hide();  $("#decline_proposal").modal('toggle'); }, 3000);
                // alert(data);
              }
          });
   });
    
    $(document).ready(function(){
   var password_condition=$("#password").val();
   //alert(password_condition);
   if(password_condition=='on'){
   $(function() {
    $('#password_popup').modal({
      show: true,
      keyboard: false,
      backdrop: 'static'
    });
   });
   }
    });
   
    $("#access_key").click(function(){
   var proposal_id=$("#proposal_id").val();
   var pdf_password=$("#pdf_password").val();
   //alert(proposal_id);
   //alert(pdf_password);
   var formData={'id':proposal_id,'password':pdf_password};
         $.ajax({
            url: '<?php echo base_url(); ?>proposal_pdf/access_key',
            type : 'POST',
            data : formData,                    
              beforeSend: function() {
                $(".LoadingImage").show();
              },
              success: function(data) {
               // alert(data);
                 $(".LoadingImage").hide();
                 if(data=='1'){
                  $("#password_popup").modal('toggle');
                }else{
                 $(".wrong_msg").show();
                 setTimeout(function(){  $(".wrong_msg").hide();   }, 3000);
               }
                // alert(data);
              }
          });
   
    });
    
</script>