<!-- Accounts Edit reminder-->
<?php $cus_temp = $this->db->query('select * from reminder_setting where user_id='.$client[0]['user_id'].' and service_id=3')->result_array();
if(isset($cus_temp)){
foreach ($cus_temp as $key => $value) {

?>
   <div class="modal fade no-idea_spacing" id="edit-reminder<?php echo $value['id'];?>" role="dialog">
    <div class="modal-dialog modal-md">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">edit reminder</h4>
        </div>
        <form action="<?php echo base_url().'client/edit_reminder/'.$value['id'];?>" method="post" accept-charset="utf-8" novalidate="novalidate">
        <div class="modal-body">
         <div class="modal-topsec">
            <div class="send-invoice">
             <!--  <span class="invoice-notify">send</span> -->
			 <div class="form-group">
              <select name="due">
                <option value="due_by" <?php if(isset($value['due']) && $value['due']=='due_by') {?> selected="selected"<?php } ?>>due by</option>
                <option value="overdue_by" <?php if(isset($value['due']) && $value['due']=='overdue_by') {?> selected="selected"<?php } ?>>overdue by</option>
              </select>
			  </div>
			  
			  <div class="form-group">
              <span class="days1">days</span>
              <input type="text" class="due-days" name="days" value="<?php echo $value['days'];?>">
			  </div>
            </div>
            <div class="insert-placeholder form-group">
              <?php
              $email_temp = $this->Common_mdl->GetAllWithWhere('tblemailtemplates','id','3'); 
               $exp_contect=explode(',', $email_temp[0]['placeholder']);
              foreach ($exp_contect as $key=>$val){ ?>
              
              <span target="message" class="add_merge"><?php echo $val;?></span><br>
              <?php } ?>
            
            </div>
         </div><!--modal-topsec-->
         <div class="modal-bottomsec">
           <div class="form-group"><input type="text" class="invoice-msg" name="subject" placeholder="Subject" value="<?php echo $value['subject'];?>"></div>
            <textarea name="message" id="editor1"><?php echo $value['message'];?></textarea>
            <div class="form-group"><input type="hidden" name="service_id" value="<?php echo $value['service_id'];?>"></div>
            <div class="form-group"><input type="hidden" name="userid" id="userid" value="<?php if(isset($client[0]['user_id']) && ($client[0]['user_id']!='') ){ echo $client[0]['user_id'];}?>" class="fields"></div>

         </div>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
          <a href="<?php echo base_url().'client/reminder_set_delete/'.$value['id'];?>" onclick="return confirm('Are you sure want to delete');">delete</a>
          <button type="submit" class="reminder-save">save</button>
        </div>
        </form>
      </div>      
    </div>
  </div> 
<?php } } ?>
  <!--edit reminder-->
<!-- Confirmation Edit reminder-->
<?php $cus_temp = $this->db->query('select * from reminder_setting where user_id='.$client[0]['user_id'].' and service_id=4')->result_array();
if(isset($cus_temp)){
foreach ($cus_temp as $key => $value) {

?>
   <div class="modal fade no-idea_spacing" id="edit-reminder<?php echo $value['id'];?>" role="dialog">
    <div class="modal-dialog modal-md">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">edit reminder</h4>
        </div>
        <form action="<?php echo base_url().'client/edit_reminder/'.$value['id'];?>" method="post" accept-charset="utf-8" novalidate="novalidate">
        <div class="modal-body">
         <div class="modal-topsec">
            <div class="send-invoice">
             <!--  <span class="invoice-notify">send</span> -->
			 <div class="form-group">
              <select name="due">
                <option value="due_by" <?php if(isset($value['due']) && $value['due']=='due_by') {?> selected="selected"<?php } ?>>due by</option>
                <option value="overdue_by" <?php if(isset($value['due']) && $value['due']=='overdue_by') {?> selected="selected"<?php } ?>>overdue by</option>
              </select>
			  </div>
        <div class="form-group">
              <span class="days1">days</span>
              <input type="text" class="due-days" name="days" value="<?php echo $value['days'];?>"></div>
            </div>
            <div class="insert-placeholder form-group">
              <?php
              $email_temp = $this->Common_mdl->GetAllWithWhere('tblemailtemplates','id','4'); 
               $exp_contect=explode(',', $email_temp[0]['placeholder']);
              foreach ($exp_contect as $key=>$val){ ?>
              
              <span target="message" class="add_merge"><?php echo $val;?></span><br>
              <?php } ?>
            
            </div>
         </div><!--modal-topsec-->
         <div class="modal-bottomsec">
          <div class="form-group">
           <input type="text" class="invoice-msg" name="subject" placeholder="Subject" value="<?php echo $value['subject'];?>"></div>
            <div class="form-group"><textarea name="message" id="editor1"><?php echo $value['message'];?></textarea></div>
            <div class="form-group"><input type="hidden" name="service_id" value="<?php echo $value['service_id'];?>"></div>
            <div class="form-group"><input type="hidden" name="userid" id="userid" value="<?php if(isset($client[0]['user_id']) && ($client[0]['user_id']!='') ){ echo $client[0]['user_id'];}?>" class="fields"></div>

         </div>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
          <a href="<?php echo base_url().'client/reminder_set_delete/'.$value['id'];?>" onclick="return confirm('Are you sure want to delete');">delete</a>
          <button type="submit" class="reminder-save">save</button>
        </div>
        </form>
      </div>      
    </div>
  </div> 
<?php } } ?>
  <!--edit reminder-->
  <!-- company tax Edit reminder-->
<?php $cus_temp = $this->db->query('select * from reminder_setting where user_id='.$client[0]['user_id'].' and service_id=5')->result_array();
if(isset($cus_temp)){
foreach ($cus_temp as $key => $value) {

?>
   <div class="modal fade no-idea_spacing" id="edit-reminder<?php echo $value['id'];?>" role="dialog">
    <div class="modal-dialog modal-md">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">edit reminder</h4>
        </div>
        <form action="<?php echo base_url().'client/edit_reminder/'.$value['id'];?>" method="post" accept-charset="utf-8" novalidate="novalidate">
        <div class="modal-body">
         <div class="modal-topsec">
            <div class="send-invoice">
             <!--  <span class="invoice-notify">send</span> -->
              <div class="form-group"><select name="due">
                <option value="due_by" <?php if(isset($value['due']) && $value['due']=='due_by') {?> selected="selected"<?php } ?>>due by</option>
                <option value="overdue_by" <?php if(isset($value['due']) && $value['due']=='overdue_by') {?> selected="selected"<?php } ?>>overdue by</option>
              </select></div>
              <div class="form-group">
              <span class="days1">days</span>
              <input type="text" class="due-days" name="days" value="<?php echo $value['days'];?>"></div>
            </div>
            <div class="insert-placeholder form-group">
              <?php
              $email_temp = $this->Common_mdl->GetAllWithWhere('tblemailtemplates','id','5'); 
               $exp_contect=explode(',', $email_temp[0]['placeholder']);
              foreach ($exp_contect as $key=>$val){ ?>
              
              <span target="message" class="add_merge"><?php echo $val;?></span><br>
              <?php } ?>
            
            </div>
         </div><!--modal-topsec-->
         <div class="modal-bottomsec">
           <input type="text" class="invoice-msg" name="subject" placeholder="Subject" value="<?php echo $value['subject'];?>">
            <textarea name="message" id="editor1"><?php echo $value['message'];?></textarea>
            <input type="hidden" name="service_id" value="<?php echo $value['service_id'];?>">
            <input type="hidden" name="userid" id="userid" value="<?php if(isset($client[0]['user_id']) && ($client[0]['user_id']!='') ){ echo $client[0]['user_id'];}?>" class="fields">

         </div>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
          <a href="<?php echo base_url().'client/reminder_set_delete/'.$value['id'];?>" onclick="return confirm('Are you sure want to delete');">delete</a>
          <button type="submit" class="reminder-save">save</button>
        </div>
        </form>
      </div>      
    </div>
  </div> 
<?php } } ?>
  <!--edit reminder-->
    <!-- personal tax Edit reminder-->
<?php $cus_temp = $this->db->query('select * from reminder_setting where user_id='.$client[0]['user_id'].' and service_id=6')->result_array();
if(isset($cus_temp)){
foreach ($cus_temp as $key => $value) {

?>
   <div class="modal fade no-idea_spacing" id="edit-reminder<?php echo $value['id'];?>" role="dialog">
    <div class="modal-dialog modal-md">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">edit reminder</h4>
        </div>
        <form action="<?php echo base_url().'client/edit_reminder/'.$value['id'];?>" method="post" accept-charset="utf-8" novalidate="novalidate">
        <div class="modal-body">
         <div class="modal-topsec">
            <div class="send-invoice">
             <!--  <span class="invoice-notify">send</span> -->
              <select name="due">
                <option value="due_by" <?php if(isset($value['due']) && $value['due']=='due_by') {?> selected="selected"<?php } ?>>due by</option>
                <option value="overdue_by" <?php if(isset($value['due']) && $value['due']=='overdue_by') {?> selected="selected"<?php } ?>>overdue by</option>
              </select>
              <input type="text" class="due-days" name="days" value="<?php echo $value['days'];?>">
              <span class="days1">days</span>
            </div>
            <div class="insert-placeholder">
              <?php
              $email_temp = $this->Common_mdl->GetAllWithWhere('tblemailtemplates','id','6'); 
               $exp_contect=explode(',', $email_temp[0]['placeholder']);
              foreach ($exp_contect as $key=>$val){ ?>
              
              <span target="message" class="add_merge"><?php echo $val;?></span><br>
              <?php } ?>
            
            </div>
         </div><!--modal-topsec-->
         <div class="modal-bottomsec">
           <div class="form-group"><input type="text" class="invoice-msg" name="subject" placeholder="Subject" value="<?php echo $value['subject'];?>"></div>
            <div class="form-group"><textarea name="message" id="editor1"><?php echo $value['message'];?></textarea></div>
            <div class="form-group"><input type="hidden" name="service_id" value="<?php echo $value['service_id'];?>"></div>
            <div class="form-group"><input type="hidden" name="userid" id="userid" value="<?php if(isset($client[0]['user_id']) && ($client[0]['user_id']!='') ){ echo $client[0]['user_id'];}?>" class="fields"></div>

         </div>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
          <a href="<?php echo base_url().'client/reminder_set_delete/'.$value['id'];?>" onclick="return confirm('Are you sure want to delete');">delete</a>
          <button type="submit" class="reminder-save">save</button>
        </div>
        </form>
      </div>      
    </div>
  </div> 
<?php } } ?>
  <!--edit reminder-->
<!-- payroll Edit reminder-->
<?php $cus_temp = $this->db->query('select * from reminder_setting where user_id='.$client[0]['user_id'].' and service_id=7')->result_array();
if(isset($cus_temp)){
foreach ($cus_temp as $key => $value) {

?>
   <div class="modal fade no-idea_spacing" id="edit-reminder<?php echo $value['id'];?>" role="dialog">
    <div class="modal-dialog modal-md">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">edit reminder</h4>
        </div>
        <form action="<?php echo base_url().'client/edit_reminder/'.$value['id'];?>" method="post" accept-charset="utf-8" novalidate="novalidate">
        <div class="modal-body">
         <div class="modal-topsec">
            <div class="send-invoice">
             <!--  <span class="invoice-notify">send</span> -->
              <div class="form-group"><select name="due">
                <option value="due_by" <?php if(isset($value['due']) && $value['due']=='due_by') {?> selected="selected"<?php } ?>>due by</option>
                <option value="overdue_by" <?php if(isset($value['due']) && $value['due']=='overdue_by') {?> selected="selected"<?php } ?>>overdue by</option>
              </select>
              <span class="days1">days</span>
              <input type="text" class="due-days" name="days" value="<?php echo $value['days'];?>"></div>
            </div>
            <div class="insert-placeholder form-group">
              <?php
              $email_temp = $this->Common_mdl->GetAllWithWhere('tblemailtemplates','id','7'); 
               $exp_contect=explode(',', $email_temp[0]['placeholder']);
              foreach ($exp_contect as $key=>$val){ ?>
              
              <span target="message" class="add_merge"><?php echo $val;?></span><br>
              <?php } ?>
            
            </div>
         </div><!--modal-topsec-->
         <div class="modal-bottomsec">
          <div class="form-group"> <input type="text" class="invoice-msg" name="subject" placeholder="Subject" value="<?php echo $value['subject'];?>">?</div>
            <div class="form-group"><textarea name="message" id="editor1"><?php echo $value['message'];?></textarea></div>
            <div class="form-group"><input type="hidden" name="service_id" value="<?php echo $value['service_id'];?>"></div>
            <div class="form-group"><input type="hidden" name="userid" id="userid" value="<?php if(isset($client[0]['user_id']) && ($client[0]['user_id']!='') ){ echo $client[0]['user_id'];}?>" class="fields"></div>

         </div>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
          <a href="<?php echo base_url().'client/reminder_set_delete/'.$value['id'];?>" onclick="return confirm('Are you sure want to delete');">delete</a>
          <button type="submit" class="reminder-save">save</button>
        </div>
        </form>
      </div>      
    </div>
  </div> 
<?php } } ?>
<!--edit reminder-->
<!-- workplace pension Edit reminder-->
<?php $cus_temp = $this->db->query('select * from reminder_setting where user_id='.$client[0]['user_id'].' and service_id=8')->result_array();
if(isset($cus_temp)){
foreach ($cus_temp as $key => $value) {

?>
   <div class="modal fade no-idea_spacing" id="edit-reminder<?php echo $value['id'];?>" role="dialog">
    <div class="modal-dialog modal-md">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">edit reminder</h4>
        </div>
        <form action="<?php echo base_url().'client/edit_reminder/'.$value['id'];?>" method="post" accept-charset="utf-8" novalidate="novalidate">
        <div class="modal-body">
         <div class="modal-topsec">
            <div class="send-invoice">
             <!--  <span class="invoice-notify">send</span> -->
          <div class="form-group">    <select name="due">
                <option value="due_by" <?php if(isset($value['due']) && $value['due']=='due_by') {?> selected="selected"<?php } ?>>due by</option>
                <option value="overdue_by" <?php if(isset($value['due']) && $value['due']=='overdue_by') {?> selected="selected"<?php } ?>>overdue by</option>
              </select></div>
            <div class="form-group">
              <span class="days1">days</span>
              <input type="text" class="due-days" name="days" value="<?php echo $value['days'];?>"></div>
            </div>
            <div class="insert-placeholder form-group">
              <?php
              $email_temp = $this->Common_mdl->GetAllWithWhere('tblemailtemplates','id','8'); 
               $exp_contect=explode(',', $email_temp[0]['placeholder']);
              foreach ($exp_contect as $key=>$val){ ?>
              
              <span target="message" class="add_merge"><?php echo $val;?></span><br>
              <?php } ?>
            
            </div>
         </div><!--modal-topsec-->
         <div class="modal-bottomsec">
          <div class="form-group">
           <input type="text" class="invoice-msg" name="subject" placeholder="Subject" value="<?php echo $value['subject'];?>"></div>
           <div class="form-group">
            <textarea name="message" id="editor1"><?php echo $value['message'];?></textarea></div>
            <div class="form-group"><input type="hidden" name="service_id" value="<?php echo $value['service_id'];?>"></div>
            <div class="form-group"><input type="hidden" name="userid" id="userid" value="<?php if(isset($client[0]['user_id']) && ($client[0]['user_id']!='') ){ echo $client[0]['user_id'];}?>" class="fields"></div>

         </div>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
          <a href="<?php echo base_url().'client/reminder_set_delete/'.$value['id'];?>" onclick="return confirm('Are you sure want to delete');">delete</a>
          <button type="submit" class="reminder-save">save</button>
        </div>
        </form>
      </div>      
    </div>
  </div> 
<?php } } ?>
<!--edit reminder-->
<!--Investigation Insurance Edit reminder-->
<?php $cus_temp = $this->db->query('select * from reminder_setting where user_id='.$client[0]['user_id'].' and service_id=9')->result_array();
if(isset($cus_temp)){
foreach ($cus_temp as $key => $value) {

?>
   <div class="modal fade no-idea_spacing" id="edit-reminder<?php echo $value['id'];?>" role="dialog">
    <div class="modal-dialog modal-md">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">edit reminder</h4>
        </div>
        <form action="<?php echo base_url().'client/edit_reminder/'.$value['id'];?>" method="post" accept-charset="utf-8" novalidate="novalidate">
        <div class="modal-body">
         <div class="modal-topsec">
            <div class="send-invoice">
             <!--  <span class="invoice-notify">send</span> -->
            <div class="form-group">  <select name="due">
                <option value="due_by" <?php if(isset($value['due']) && $value['due']=='due_by') {?> selected="selected"<?php } ?>>due by</option>
                <option value="overdue_by" <?php if(isset($value['due']) && $value['due']=='overdue_by') {?> selected="selected"<?php } ?>>overdue by</option>
              </select></div>
              <div class="form-group">
              <span class="days1">days</span>
                <input type="text" class="due-days" name="days" value="<?php echo $value['days'];?>">
            </div>
            </div>
            <div class="insert-placeholder form-group">
              <?php
              $email_temp = $this->Common_mdl->GetAllWithWhere('tblemailtemplates','id','9'); 
               $exp_contect=explode(',', $email_temp[0]['placeholder']);
              foreach ($exp_contect as $key=>$val){ ?>
              
              <span target="message" class="add_merge"><?php echo $val;?></span><br>
              <?php } ?>
            
            </div>
         </div><!--modal-topsec-->
         <div class="modal-bottomsec">
       <div class="form-group">    <input type="text" class="invoice-msg" name="subject" placeholder="Subject" value="<?php echo $value['subject'];?>"></div>
       <div class="form-group">
            <textarea name="message" id="editor1"><?php echo $value['message'];?></textarea></div>
            <div class="form-group">
            <input type="hidden" name="service_id" value="<?php echo $value['service_id'];?>"></div>
            <div class="form-group">
            <input type="hidden" name="userid" id="userid" value="<?php if(isset($client[0]['user_id']) && ($client[0]['user_id']!='') ){ echo $client[0]['user_id'];}?>" class="fields"></div>

         </div>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
          <a href="<?php echo base_url().'client/reminder_set_delete/'.$value['id'];?>" onclick="return confirm('Are you sure want to delete');">delete</a>
          <button type="submit" class="reminder-save">save</button>
        </div>
        </form>
      </div>      
    </div>
  </div> 
<?php } } ?>
<!--edit reminder-->
<!--registered Edit reminder-->
<?php $cus_temp = $this->db->query('select * from reminder_setting where user_id='.$client[0]['user_id'].' and service_id=9')->result_array();
if(isset($cus_temp)){
foreach ($cus_temp as $key => $value) {

?>
   <div class="modal fade no-idea_spacing" id="edit-reminder<?php echo $value['id'];?>" role="dialog">
    <div class="modal-dialog modal-md">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">edit reminder</h4>
        </div>
        <form action="<?php echo base_url().'client/edit_reminder/'.$value['id'];?>" method="post" accept-charset="utf-8" novalidate="novalidate">
        <div class="modal-body">
         <div class="modal-topsec">
            <div class="send-invoice">
             <!--  <span class="invoice-notify">send</span> -->
          <div class="form-group">    <select name="due">
                <option value="due_by" <?php if(isset($value['due']) && $value['due']=='due_by') {?> selected="selected"<?php } ?>>due by</option>
                <option value="overdue_by" <?php if(isset($value['due']) && $value['due']=='overdue_by') {?> selected="selected"<?php } ?>>overdue by</option>
              </select></div>
         <div class="form-group">
              <span class="days1">days</span>
              <input type="text" class="due-days" name="days" value="<?php echo $value['days'];?>">
            </div>
            </div>
            <div class="insert-placeholder form-group">
              <?php
              $email_temp = $this->Common_mdl->GetAllWithWhere('tblemailtemplates','id','9'); 
               $exp_contect=explode(',', $email_temp[0]['placeholder']);
              foreach ($exp_contect as $key=>$val){ ?>
              
              <span target="message" class="add_merge"><?php echo $val;?></span><br>
              <?php } ?>
            
            </div>
         </div><!--modal-topsec-->
         <div class="modal-bottomsec">
           <div class="form-group"><input type="text" class="invoice-msg" name="subject" placeholder="Subject" value="<?php echo $value['subject'];?>"></div>
            <div class="form-group"><textarea name="message" id="editor1"><?php echo $value['message'];?></textarea>
            </div>
            <div class="form-group"><input type="hidden" name="service_id" value="<?php echo $value['service_id'];?>"></div>
            <div class="form-group"><input type="hidden" name="userid" id="userid" value="<?php if(isset($client[0]['user_id']) && ($client[0]['user_id']!='') ){ echo $client[0]['user_id'];}?>" class="fields"></div>

         </div>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
          <a href="<?php echo base_url().'client/reminder_set_delete/'.$value['id'];?>" onclick="return confirm('Are you sure want to delete');">delete</a>
          <button type="submit" class="reminder-save">save</button>
        </div>
        </form>
      </div>      
    </div>
  </div> 
<?php } } ?>
<!--edit reminder-->
<!--Tax advice/investigation Edit reminder-->
<?php $cus_temp = $this->db->query('select * from reminder_setting where user_id='.$client[0]['user_id'].' and service_id=9')->result_array();
if(isset($cus_temp)){
foreach ($cus_temp as $key => $value) {

?>
   <div class="modal fade no-idea_spacing" id="edit-reminder<?php echo $value['id'];?>" role="dialog">
    <div class="modal-dialog modal-md">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">edit reminder</h4>
        </div>
        <form action="<?php echo base_url().'client/edit_reminder/'.$value['id'];?>" method="post" accept-charset="utf-8" novalidate="novalidate">
        <div class="modal-body">
         <div class="modal-topsec">
            <div class="send-invoice">
             <!--  <span class="invoice-notify">send</span> -->
           <div class="form-group">   <select name="due">
                <option value="due_by" <?php if(isset($value['due']) && $value['due']=='due_by') {?> selected="selected"<?php } ?>>due by</option>
                <option value="overdue_by" <?php if(isset($value['due']) && $value['due']=='overdue_by') {?> selected="selected"<?php } ?>>overdue by</option>
              </select></div>
            <div class="form-group">
              <span class="days1">days</span>
              <input type="text" class="due-days" name="days" value="<?php echo $value['days'];?>">
            </div>
            </div>
            <div class="insert-placeholder form-group">
              <?php
              $email_temp = $this->Common_mdl->GetAllWithWhere('tblemailtemplates','id','9'); 
               $exp_contect=explode(',', $email_temp[0]['placeholder']);
              foreach ($exp_contect as $key=>$val){ ?>
              
              <span target="message" class="add_merge"><?php echo $val;?></span><br>
              <?php } ?>
            
            </div>
         </div><!--modal-topsec-->
         <div class="modal-bottomsec">
          <div class="form-group">
           <input type="text" class="invoice-msg" name="subject" placeholder="Subject" value="<?php echo $value['subject'];?>"></div>
           <div class="form-group">
            <textarea name="message" id="editor1"><?php echo $value['message'];?></textarea></div>
            <div class="form-group"><input type="hidden" name="service_id" value="<?php echo $value['service_id'];?>"></div>
            <div class="form-group"><input type="hidden" name="userid" id="userid" value="<?php if(isset($client[0]['user_id']) && ($client[0]['user_id']!='') ){ echo $client[0]['user_id'];}?>" class="fields"></div>

         </div>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
          <a href="<?php echo base_url().'client/reminder_set_delete/'.$value['id'];?>" onclick="return confirm('Are you sure want to delete');">delete</a>
          <button type="submit" class="reminder-save">save</button>
        </div>
        </form>
      </div>      
    </div>
  </div> 
<?php } } ?>
<!--edit reminder-->
<!--CIS Edit reminder-->
<?php $cus_temp = $this->db->query('select * from reminder_setting where user_id='.$client[0]['user_id'].' and service_id=10')->result_array();
if(isset($cus_temp)){
foreach ($cus_temp as $key => $value) {

?>
   <div class="modal fade no-idea_spacing" id="edit-reminder<?php echo $value['id'];?>" role="dialog">
    <div class="modal-dialog modal-md">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">edit reminder</h4>
        </div>
        <form action="<?php echo base_url().'client/edit_reminder/'.$value['id'];?>" method="post" accept-charset="utf-8" novalidate="novalidate">
        <div class="modal-body">
         <div class="modal-topsec">
            <div class="send-invoice">
             <!--  <span class="invoice-notify">send</span> -->
              <div class="form-group"><select name="due">
                <option value="due_by" <?php if(isset($value['due']) && $value['due']=='due_by') {?> selected="selected"<?php } ?>>due by</option>
                <option value="overdue_by" <?php if(isset($value['due']) && $value['due']=='overdue_by') {?> selected="selected"<?php } ?>>overdue by</option>
              </select></div>
              <div class="form-group">
              <span class="days1">days</span>
              <input type="text" class="due-days" name="days" value="<?php echo $value['days'];?>">
            </div>
            </div>
            <div class="insert-placeholder form-group">
              <?php
              $email_temp = $this->Common_mdl->GetAllWithWhere('tblemailtemplates','id','10'); 
               $exp_contect=explode(',', $email_temp[0]['placeholder']);
              foreach ($exp_contect as $key=>$val){ ?>
              
              <span target="message" class="add_merge"><?php echo $val;?></span><br>
              <?php } ?>
            
            </div>
         </div><!--modal-topsec-->
         <div class="modal-bottomsec">
          <div class="form-group">   <input type="text" class="invoice-msg" name="subject" placeholder="Subject" value="<?php echo $value['subject'];?>"></div>
            <div class="form-group"><textarea name="message" id="editor1"><?php echo $value['message'];?></textarea></div>
            <div class="form-group"><input type="hidden" name="service_id" value="<?php echo $value['service_id'];?>"></div>
            <div class="form-group"><input type="hidden" name="userid" id="userid" value="<?php if(isset($client[0]['user_id']) && ($client[0]['user_id']!='') ){ echo $client[0]['user_id'];}?>" class="fields"></div>

         </div>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
          <a href="<?php echo base_url().'client/reminder_set_delete/'.$value['id'];?>" onclick="return confirm('Are you sure want to delete');">delete</a>
          <button type="submit" class="reminder-save">save</button>
        </div>
        </form>
      </div>      
    </div>
  </div> 
<?php } } ?>
<!--edit reminder-->
<!--CIS sub Edit reminder-->
<?php $cus_temp = $this->db->query('select * from reminder_setting where user_id='.$client[0]['user_id'].' and service_id=11')->result_array();
if(isset($cus_temp)){
foreach ($cus_temp as $key => $value) {

?>
   <div class="modal fade no-idea_spacing" id="edit-reminder<?php echo $value['id'];?>" role="dialog">
    <div class="modal-dialog modal-md">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">edit reminder</h4>
        </div>
        <form action="<?php echo base_url().'client/edit_reminder/'.$value['id'];?>" method="post" accept-charset="utf-8" novalidate="novalidate">
        <div class="modal-body">
         <div class="modal-topsec">
            <div class="send-invoice">
             <!--  <span class="invoice-notify">send</span> -->
             <div class="form-group">
              <select name="due">
                <option value="due_by" <?php if(isset($value['due']) && $value['due']=='due_by') {?> selected="selected"<?php } ?>>due by</option>
                <option value="overdue_by" <?php if(isset($value['due']) && $value['due']=='overdue_by') {?> selected="selected"<?php } ?>>overdue by</option>
              </select>
            </div>
            <div class="form-group">
              <span class="days1">days</span>
              <input type="text" class="due-days" name="days" value="<?php echo $value['days'];?>"></div>
            </div>
            <div class="insert-placeholder form-group">
              <?php
              $email_temp = $this->Common_mdl->GetAllWithWhere('tblemailtemplates','id','11'); 
               $exp_contect=explode(',', $email_temp[0]['placeholder']);
              foreach ($exp_contect as $key=>$val){ ?>
              
              <span target="message" class="add_merge"><?php echo $val;?></span><br>
              <?php } ?>
            
            </div>
         </div><!--modal-topsec-->
         <div class="modal-bottomsec">
          <div class="form-group"> <input type="text" class="invoice-msg" name="subject" placeholder="Subject" value="<?php echo $value['subject'];?>"></div>
            <div class="form-group"><textarea name="message" id="editor1"><?php echo $value['message'];?></textarea></div>
            <div class="form-group"><input type="hidden" name="service_id" value="<?php echo $value['service_id'];?>"></div>
            <div class="form-group"><input type="hidden" name="userid" id="userid" value="<?php if(isset($client[0]['user_id']) && ($client[0]['user_id']!='') ){ echo $client[0]['user_id'];}?>" class="fields"></div>

         </div>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
          <a href="<?php echo base_url().'client/reminder_set_delete/'.$value['id'];?>" onclick="return confirm('Are you sure want to delete');">delete</a>
          <button type="submit" class="reminder-save">save</button>
        </div>
        </form>
      </div>      
    </div>
  </div> 
<?php } } ?>
<!--edit reminder-->
<!--p11d Edit reminder-->
<?php $cus_temp = $this->db->query('select * from reminder_setting where user_id='.$client[0]['user_id'].' and service_id=12')->result_array();
if(isset($cus_temp)){
foreach ($cus_temp as $key => $value) {

?>
   <div class="modal fade no-idea_spacing" id="edit-reminder<?php echo $value['id'];?>" role="dialog">
    <div class="modal-dialog modal-md">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">edit reminder</h4>
        </div>
        <form action="<?php echo base_url().'client/edit_reminder/'.$value['id'];?>" method="post" accept-charset="utf-8" novalidate="novalidate">
        <div class="modal-body">
         <div class="modal-topsec">
            <div class="send-invoice">
             <!--  <span class="invoice-notify">send</span> -->
          <div class="form-group">    <select name="due">
                <option value="due_by" <?php if(isset($value['due']) && $value['due']=='due_by') {?> selected="selected"<?php } ?>>due by</option>
                <option value="overdue_by" <?php if(isset($value['due']) && $value['due']=='overdue_by') {?> selected="selected"<?php } ?>>overdue by</option>
              </select></div>
              <div class="form-group">
              <span class="days1">days</span>
              <input type="text" class="due-days" name="days" value="<?php echo $value['days'];?>"></div>
            </div>
            <div class="insert-placeholder form-group">
              <?php
              $email_temp = $this->Common_mdl->GetAllWithWhere('tblemailtemplates','id','12'); 
               $exp_contect=explode(',', $email_temp[0]['placeholder']);
              foreach ($exp_contect as $key=>$val){ ?>
              
              <span target="message" class="add_merge"><?php echo $val;?></span><br>
              <?php } ?>
            
            </div>
         </div><!--modal-topsec-->
         <div class="modal-bottomsec">
        <div class="form-group">   <input type="text" class="invoice-msg" name="subject" placeholder="Subject" value="<?php echo $value['subject'];?>">
         <div class="form-group">   <textarea name="message" id="editor1"><?php echo $value['message'];?></textarea></div>
           <div class="form-group"> <input type="hidden" name="service_id" value="<?php echo $value['service_id'];?>"></div>
           <div class="form-group"> <input type="hidden" name="userid" id="userid" value="<?php if(isset($client[0]['user_id']) && ($client[0]['user_id']!='') ){ echo $client[0]['user_id'];}?>" class="fields"></div>

         </div>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
          <a href="<?php echo base_url().'client/reminder_set_delete/'.$value['id'];?>" onclick="return confirm('Are you sure want to delete');">delete</a>
          <button type="submit" class="reminder-save">save</button>
        </div>
        </form>
      </div>      
    </div>
  </div> 
<?php } } ?>
<!--edit reminder-->
<!--p11d Edit reminder-->
<?php $cus_temp = $this->db->query('select * from reminder_setting where user_id='.$client[0]['user_id'].' and service_id=13')->result_array();
if(isset($cus_temp)){
foreach ($cus_temp as $key => $value) {

?>
   <div class="modal fade no-idea_spacing" id="edit-reminder<?php echo $value['id'];?>" role="dialog">
    <div class="modal-dialog modal-md">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">edit reminder</h4>
        </div>
        <form action="<?php echo base_url().'client/edit_reminder/'.$value['id'];?>" method="post" accept-charset="utf-8" novalidate="novalidate">
        <div class="modal-body">
         <div class="modal-topsec">
            <div class="send-invoice">
             <!--  <span class="invoice-notify">send</span> -->
          <div class="form-group">    <select name="due">
                <option value="due_by" <?php if(isset($value['due']) && $value['due']=='due_by') {?> selected="selected"<?php } ?>>due by</option>
                <option value="overdue_by" <?php if(isset($value['due']) && $value['due']=='overdue_by') {?> selected="selected"<?php } ?>>overdue by</option>
              </select></div>
              <div class="form-group">
              <span class="days1">days</span>
              <input type="text" class="due-days" name="days" value="<?php echo $value['days'];?>">
            </div>
            </div>
            <div class="insert-placeholder form-group">
              <?php
              $email_temp = $this->Common_mdl->GetAllWithWhere('tblemailtemplates','id','13'); 
               $exp_contect=explode(',', $email_temp[0]['placeholder']);
              foreach ($exp_contect as $key=>$val){ ?>
              
              <span target="message" class="add_merge"><?php echo $val;?></span><br>
              <?php } ?>
            
            </div>
         </div><!--modal-topsec-->
         <div class="modal-bottomsec">
           <div class="form-group"><input type="text" class="invoice-msg" name="subject" placeholder="Subject" value="<?php echo $value['subject'];?>"></div>
            <div class="form-group"><textarea name="message" id="editor1"><?php echo $value['message'];?></textarea></div>
            <div class="form-group"><input type="hidden" name="service_id" value="<?php echo $value['service_id'];?>"></div>
            <div class="form-group"><input type="hidden" name="userid" id="userid" value="<?php if(isset($client[0]['user_id']) && ($client[0]['user_id']!='') ){ echo $client[0]['user_id'];}?>" class="fields"></div>

         </div>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
          <a href="<?php echo base_url().'client/reminder_set_delete/'.$value['id'];?>" onclick="return confirm('Are you sure want to delete');">delete</a>
          <button type="submit" class="reminder-save">save</button>
        </div>
        </form>
      </div>      
    </div>
  </div> 
<?php } } ?>
<!--edit reminder-->
<!--management Edit reminder-->
<?php $cus_temp = $this->db->query('select * from reminder_setting where user_id='.$client[0]['user_id'].' and service_id=14')->result_array();
if(isset($cus_temp)){
foreach ($cus_temp as $key => $value) {

?>
   <div class="modal fade no-idea_spacing" id="edit-reminder<?php echo $value['id'];?>" role="dialog">
    <div class="modal-dialog modal-md">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">edit reminder</h4>
        </div>
        <form action="<?php echo base_url().'client/edit_reminder/'.$value['id'];?>" method="post" accept-charset="utf-8" novalidate="novalidate">
        <div class="modal-body">
         <div class="modal-topsec">
            <div class="send-invoice">
             <!--  <span class="invoice-notify">send</span> -->
            <div class="form-group">  <select name="due">
                <option value="due_by" <?php if(isset($value['due']) && $value['due']=='due_by') {?> selected="selected"<?php } ?>>due by</option>
                <option value="overdue_by" <?php if(isset($value['due']) && $value['due']=='overdue_by') {?> selected="selected"<?php } ?>>overdue by</option>
              </select> </div>
              <div class="form-group">
              <span class="days1">days</span>
                <input type="text" class="due-days" name="days" value="<?php echo $value['days'];?>">
            </div>
            </div>
            <div class="insert-placeholder form-group">
              <?php
              $email_temp = $this->Common_mdl->GetAllWithWhere('tblemailtemplates','id','14'); 
               $exp_contect=explode(',', $email_temp[0]['placeholder']);
              foreach ($exp_contect as $key=>$val){ ?>
              
              <span target="message" class="add_merge"><?php echo $val;?></span><br>
              <?php } ?>
            
            </div>
         </div><!--modal-topsec-->
         <div class="modal-bottomsec">
        <div class="form-group">   <input type="text" class="invoice-msg" name="subject" placeholder="Subject" value="<?php echo $value['subject'];?>"></div>
            <div class="form-group"><textarea name="message" id="editor1"><?php echo $value['message'];?></textarea></div>
            <div class="form-group"><input type="hidden" name="service_id" value="<?php echo $value['service_id'];?>"></div>
            <div class="form-group"><input type="hidden" name="userid" id="userid" value="<?php if(isset($client[0]['user_id']) && ($client[0]['user_id']!='') ){ echo $client[0]['user_id'];}?>" class="fields"></div>

         </div>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
          <a href="<?php echo base_url().'client/reminder_set_delete/'.$value['id'];?>" onclick="return confirm('Are you sure want to delete');">delete</a>
          <button type="submit" class="reminder-save">save</button>
        </div>
        </form>
      </div>      
    </div>
  </div> 
<?php } } ?>
<!--edit reminder-->
<!--vat Edit reminder-->
<?php $cus_temp = $this->db->query('select * from reminder_setting where user_id='.$client[0]['user_id'].' and service_id=15')->result_array();
if(isset($cus_temp)){
foreach ($cus_temp as $key => $value) {

?>
   <div class="modal fade no-idea_spacing" id="edit-reminder<?php echo $value['id'];?>" role="dialog">
    <div class="modal-dialog modal-md">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">edit reminder</h4>
        </div>
        <form action="<?php echo base_url().'client/edit_reminder/'.$value['id'];?>" method="post" accept-charset="utf-8" novalidate="novalidate">
        <div class="modal-body">
         <div class="modal-topsec">
            <div class="send-invoice">
             <!--  <span class="invoice-notify">send</span> -->
            <div class="form-group">  <select name="due">
                <option value="due_by" <?php if(isset($value['due']) && $value['due']=='due_by') {?> selected="selected"<?php } ?>>due by</option>
                <option value="overdue_by" <?php if(isset($value['due']) && $value['due']=='overdue_by') {?> selected="selected"<?php } ?>>overdue by</option>
              </select></div>
              <div class="form-group">
              <span class="days1">days</span>
              <input type="text" class="due-days" name="days" value="<?php echo $value['days'];?>">
            </div>
            </div>
            <div class="insert-placeholder form-group">
              <?php
              $email_temp = $this->Common_mdl->GetAllWithWhere('tblemailtemplates','id','15'); 
               $exp_contect=explode(',', $email_temp[0]['placeholder']);
              foreach ($exp_contect as $key=>$val){ ?>
              
              <span target="message" class="add_merge"><?php echo $val;?></span><br>
              <?php } ?>
            
            </div>
         </div><!--modal-topsec-->
         <div class="modal-bottomsec">
          <div class="form-group">
           <input type="text" class="invoice-msg" name="subject" placeholder="Subject" value="<?php echo $value['subject'];?>"></div>
           <div class="form-group">

            <textarea name="message" id="editor1"><?php echo $value['message'];?></textarea></div>
            <div class="form-group"><input type="hidden" name="service_id" value="<?php echo $value['service_id'];?>"></div>
            <div class="form-group"><input type="hidden" name="userid" id="userid" value="<?php if(isset($client[0]['user_id']) && ($client[0]['user_id']!='') ){ echo $client[0]['user_id'];}?>" class="fields"></div>

         </div>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
          <a href="<?php echo base_url().'client/reminder_set_delete/'.$value['id'];?>" onclick="return confirm('Are you sure want to delete');">delete</a>
          <button type="submit" class="reminder-save">save</button>
        </div>
        </form>
      </div>      
    </div>
  </div> 
<?php } } ?>
<!--edit reminder-->