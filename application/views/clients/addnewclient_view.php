<?php 
   $this->load->view('includes/header');
   $uri_seg = $this->uri->segment(3);
   /*if(isset($_GET['action'])){
   $action = $_GET['action'];
   }
   else{
      $action = 'mannual';
   }*/
   ?>
<style>
/** 28-08-2018 for hide edit option on company house basic detailes **/
.edit-button-confim
{
    display: none;
}
span.primary-inner.field_hide
{
   display: none !important;
}
li.show-block
{
   display: none;
   float: left;
   width: 100%;
}
.inner-views
{
   display: block;
}
.make_primary_section.active
{
 background: #4dad2f;
}
span.hilight_due_date
{
   color: red;
   font-weight: bold;
}
/** end of 28-08-2018 **/
</style>


<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/tree_select/style.css?ver=2">

<div class="modal-alertsuccess  alert alert-success-reminder succs" style="display: none;">
   <div class="newupdate_alert">
   <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   <div class="pop-realted1">
      <div class="position-alert1">Reminder Added Successfully.</div>
   </div></div>
</div>
<div class="pcoded-content card-removes clientredesign">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card">
                        <!-- admin start-->
                        <?php 
                           /*echo "<pre>";
                           print_r($client);echo "</pre>";*/ ?>
                        <div class="modal-alertsuccess alert alert-success" style="display:none;">
                          <div class="newupdate_alert"> <a href="javascript:;" class="close alert_close"  aria-label="close">x</a>
                           <div class="pop-realted1">
                              <div class="position-alert1 sample_check">
                                 YOU HAVE SUCCESSFULLY ADDED CLIENT VIA MANUAL ADD - DO YOU WANT TO 
                                 <div class="alertclsnew">
                                    <a href="<?php echo base_url(); ?>client/addnewclient"> ADD ANOTHER CLIENT </a>                             
                                    <a href="<?php echo base_url(); ?>user"> GO BACK</a>
                                 </div>
                              </div>
                           </div></div>
                        </div>
                     
                        <div class="modal-alertsuccess alert alert-danger" style="display:none;">
                           <div class="newupdate_alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                           <div class="pop-realted1">
                              <div class="position-alert1">
                                 Please fill the required fields.
                              </div>
                           </div></div>
                        </div>

                        <form id="insert_form" class="validation modal-addnew1" method="post" action="" enctype="multipart/form-data">
                           <!-- 06-07-2018 for addreminders ids-->
                           <input type="hidden" id="add_reminder_ids" name="add_reminder_ids" value=''>
                           <!-- end of 06-07-2018 -->
                           <div class="remin-admin">
                              <div class="space-required">
                                 <div class="deadline-crm1 floating_set">
                                    <ul class="nav nav-tabs1 all_user1 md-tabs pull-left u-dashboard">
                                       <li class="nav-item">
                                          <a class="nav-link" href="<?php echo base_url().'user';?>">
                                          <img class="themicon" src="<?php echo base_url();?>assets/images/tabicon1.png" alt="themeicon" />
                                       All Clients</a>
                                          <div class="slide"></div>
                                       </li>
                                       <li class="nav-item">
                                          <a class="nav-link active" href="#addnewclient">
                                             <img class="themicon" src="<?php echo base_url();?>assets/images/tabicon2.png" alt="themeicon" />Add new client</a>
                                          <div class="slide"></div>
                                       </li>
                                       <!-- <li class="nav-item">
                                          <a class="nav-link" href="<?php echo base_url().'user';?>">Email all</a>
                                          <div class="slide"></div>
                                          </li>
                                          <li class="nav-item">
                                          <a class="nav-link" href="<?php echo base_url().'user';?>">Give feedback</a>
                                          <div class="slide"></div>
                                          </li> -->
                                    </ul>
                                    <div class="Footer common-clienttab pull-right">
                                       <div class="change-client-bts1">
                                        <!-- <?php if($_SESSION['permission']['Add_Client']['create']=='1'){  } ?> -->
                                            <?php
                                                $SAVE_L = "Add Client";
                                                if(!empty($uri_seg))
                                                {
                                                   $SAVE_L = "Update Client";
                                                }
                                             ?>
                                          <input type="submit" class="add_acc_client" value="<?php echo $SAVE_L;?>" id="submit"/>
                                       </div>
                                       <div class="divleft">
                                          <button  class="signed-change2"  type="button" value="Previous Tab" text="Previous Tab">Previous 
                                          </button>
                                       </div>
                                       <div class="divright">
                                          <button  class="signed-change3" type="button" value="Next Tab"  text="Next Tab">Next
                                          </button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              </div>                             
                              <div class="space-required">
                                 <div class="document-center floating_set">
                                    <div class="Companies_House floating_set">
                                       <div class="pull-left form_heading">
                                          <?php if($uri_seg==''){ 
                                             ?>
                                          <h2>Add Client</h2>
                                          <?php } else{ ?>
                                          <h2><?php if(isset($client['crm_company_name']) && ($client['crm_company_name']!='') ){ echo $client['crm_company_name'];}?></h2>
                                          <?php } ?>
                                       </div>
                                    </div>
                                    <div class="addnewclient_pages1 floating_set bg_new1">
                                       <ol class="nav nav-tabs all_user1 md-tabs floating_set" id="tabs">
                                          <!--  <li class="nav-item it3 bbs" value="3"><a class="nav-link active" data-toggle="tab" id="companyss" href="#company">Company</a></li> -->
                                          <?php $uri=$this->uri->segment(3); ?>
                                          
                                          <li class="nav-item it0" value="0">
                                             <a class="nav-link active" data-id="#required_information" href="javascript:void()">Required information</a>
                                          </li>
                                          
                                          <li class="nav-item it18 basic_details_tab" value="18">
                                             <a class="nav-link basic_deatils_id" data-id="#basic_details" href="javascript:void()"> Basic Details</a>
                                          </li>

                                          <li class="nav-item it1 for_contact_tab " value="1">
                                             <a class="nav-link main_contacttab" data-id="#main_Contact" href="javascript:void()">Contact</a>
                                          </li>
                                          <li class="nav-item it2 hide" value="2">
                                             <a class="nav-link" data-id="#service" href="javascript:void()"> Service</a>
                                          </li>
                                          <li class="nav-item it2 hide" value="2">
                                             <a class="nav-link" data-id="#assignto" href="javascript:void()"> Assign to</a>
                                          </li>
                                          <li class="nav-item it2 hide" value="2">
                                             <a class="nav-link" data-id="#amlchecks" href="javascript:void()"> AML Checks</a></li>
                                          <li class="nav-item it2 hide business-info-tab" value="2" style="display: none;" >
                                             <a class="nav-link" data-id="#business-det" href="javascript:void()"> Business Details</a>
                                          </li>
                                          <li class="nav-item it10 other_tabs" value="10">
                                             <a class="nav-link" data-id="#other" href="javascript:void()"> <span id="other_tab_cnt"></span><span id="old_cnt"></span> Other</a>
                                          </li>
                                          <!-- for 30-08-2018 -->
                                          <li class="nav-item it10" value="10">
                                             <a class="nav-link" data-id="#referral" href="javascript:void()">Referral</a>
                                          </li>
                                          <li class="nav-item" id="import_tab_li" style="display: none;">
                                             <a class="nav-link" data-id="#import_tab" href="javascript:void()"> Important Information</a>
                                          </li>
                                        <!--   <li class="nav-item it7 hide li_append_con" value="2" style="display: none;"></li>
                                          <li class="nav-item it6 hide li_append_acc" value="2" style="display: none;"></li>
                                          <li class="nav-item it12 hide li_append_per" value="2" style="display: none;"></li>
                                          <li class="nav-item it11 hide li_append_pay" value="2" style="display: none;"></li>
                                          <li class="nav-item it8 hide li_append_vat" value="2" style="display: none;"></li>
                                          <li class="nav-item it13 hide li_append_man" value="2" style="display: none;"></li>
                                          <li class="nav-item it14 hide li_append_inv" value="2" style="display: none;"></li> -->
                                          <!-- end of 30-08-2018 -->
                                          <input type="hidden" id="cnt_of_other" name="cnt_of_other" >
                                          <input type="hidden" class="service_value" id="service_value" >
                                       </ol>
                                       <!-- tab close -->
                                    </div>
                                 </div>
                              </div>
                       
                           <div class="newupdate_design <?php  if($_SESSION['permission']['Add_Client']['create']!='1'){ ?> permission_deined <?php } ?>">    
                           <div class="management_section floating_set realign newmanage-animation">
                              <div class="card-block accordion-block">
                                 <!-- tab start -->
                                 <div class="tab-content card">
                                    <div id="required_information" class="columns-two tab-pane fade in active<?php /*if(!isset($client)){ echo "active";}*/ ?>">
                                       <div class="space-required">
                                          <div class="main-pane-border1" id="required_information_error">
                                             <div class="alert-ss"></div>
                                          </div>
                                       </div>
                                       <div class="space-required">
                                          <div class="white-space01">
                                             <div class="pane-border1">
                                                <div class="management_form1">
                                                   <!--  <div class="form-titles"><a href="#" class="waves-effect" data-toggle="modal" data-target="#default-Modal"><h2>View on Companies House</h2></a></div> -->
                                                   <!--  <form> -->
                                                   <div class="form-group row name_fields">
                                                      <label class="col-sm-4 col-form-label">Name</label>
                                                      <div class="col-sm-8">
                                                         <input type="text" name="company_name" id="company_name" placeholder="" value="<?php $class='';if(isset($client['crm_company_name']) && ($client['crm_company_name']!='') ){ $class='light-color_company'; echo $client['crm_company_name'];}?>" class="fields <?php echo $class;?>">
                                                      </div>
                                                   </div>
                                                   <div class="form-group row">
                                                      <label class="col-sm-4 col-form-label">Legal Form</label>
                                                      <div class="col-sm-8">
                                                         <select name="legal_form" class="form-control fields sel-legal-form" id="legal_form">
                                                            <!--  <option value="">Select</option> -->
                                                            <option value="Private Limited company" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Private Limited company') {?> selected="selected"<?php } ?> data-id="1">Private Limited company</option>
                                                            <option value="Public Limited company" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Public Limited company') {?> selected="selected"<?php } ?> data-id="1">Public Limited company</option>
                                                            <option value="Limited Liability Partnership" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Limited Liability Partnership') {?> selected="selected"<?php } ?> data-id="1">Limited Liability Partnership</option>
                                                            <option value="Partnership" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Partnership') {?> selected="selected"<?php } ?> data-id="2">Partnership</option>
                                                            <option value="Self Assessment" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Self Assessment') {?> selected="selected"<?php } ?> data-id="2">Self Assessment</option>
                                                            <option value="Trust" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Trust') {?> selected="selected"<?php } ?> data-id="1">Trust</option>
                                                            <option value="Charity" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Charity') {?> selected="selected"<?php } ?> data-id="1">Charity</option>
                                                            <option value="Other" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Other') {?> selected="selected"<?php } ?> data-id="1">Other</option>
                                                         </select>
                                                      </div>
                                                   </div>
                                                   <!-- <div class="form-group row">
                                                      <label class="col-sm-4 col-form-label">Allocation Holder</label>
                                                      <div class="col-sm-8">
                                                         <input type="text" name="allocation_holders" id="allocation_holders" placeholder="Allocation holder" value="<?php if(isset($client['crm_allocation_holder']) && ($client['crm_allocation_holder']!='') ){ echo $client['crm_allocation_holder'];}?>" class="fields">
                                                      </div>
                                                      </div>-->
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!--  <div class="button-group">
                                          <div class="floatleft">
                                             <button type="button" class="btn btn-primary btnNext">Next</button>
                                          </div>
                                          
                                          </div> -->
                                    </div>
                                    <!-- 1tab close-->
                                    <!-- 2tab  -->
                                    <div id="basic_details" class="fullwidth-animation3 tab-pane fade  <?php  ?> ">
                                    <div class="space-required">
                                          <div class="main-pane-border1" id="basic_details_error">
                                             <div class="alert-ss"></div>
                                          </div>
                                       </div>
                                       <div class="space-required">
                                          <div class="white-space01">
                                             <div class="pane-border1 management_accordion">
                                                <div class="management_form1 management_accordion" id="important_details_section">
                                                   <!--<form>-->
                                                   <?php 
                                                    //  $read_edit=(isset($client['status']) && $client['status']==1)? '' :'display:none;'; 
                                                   $read_edit='display:none';
                                                      
                                                      ?>
                                                   <div class="form-group row name_fields sorting  <?php if(isset($client['crm_company_url']) && ($client['crm_company_url']!='') ){?>light-color_company<?php } ?>" id="<?php echo $this->Common_mdl->get_order_details(1); ?>" <?php echo $this->Common_mdl->get_delete_details(1); ?>>
                                                      <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(1); ?></label>
                                                      <div class="col-sm-8 edit-field-popup1">
                                                         <?php if($uri_seg!=''){ ?> 
                                                         <a href="<?php if(isset($client['crm_company_url']) && ($client['crm_company_url']!='') ){ echo $client['crm_company_url'];} else { echo "#"; }?>" id="company_url_anchor" target="_blank"><?php if(isset($client['crm_company_name']) && ($client['crm_company_name']!='') ){ echo $client['crm_company_name'];}?></a> 
                                                         <input type="hidden" name="company_url" id="company_url" value="<?php if(isset($client['crm_company_url']) && ($client['crm_company_url']!='') ){ echo $client['crm_company_url'];}?>" class="fields" placeholder="www.google.com"> 
                                                         <input type="hidden" name="company_name" id="company_name" placeholder="Accotax Limited" value="<?php if(isset($client['crm_company_name']) && ($client['crm_company_name']!='') ){ echo $client['crm_company_name'];}?>" class="fields edit_classname" >
                                                         <!-- <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                                            <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p> -->
                                                     
                                                      <?php }else { ?> 
                                                     

                                                      <input type="text" name="company_name1" id="company_name1" placeholder="Accotax Limited" value="<?php if(isset($client['crm_company_name']) && ($client['crm_company_name']!='') ){ echo $client['crm_company_name'];}?>" class="fields edit_classname" >
                                                      <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                                         <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                                      </div> 
                                                    
                                                      <?php } ?>
                                                       </div>
                                                   </div>
                                                
                                                <div class="form-group row sorting newappend-03  <?php if(isset($client['crm_company_number']) && ($client['crm_company_number']!='') ){?>light-color_company<?php } ?>" id="<?php echo $this->Common_mdl->get_order_details(2); ?>" <?php echo $this->Common_mdl->get_delete_details(2); ?>>
                                                   <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(2); ?></label>
                                                  
                                                   <div class="col-sm-8 edit-field-popup1">
                                                      <input type="number" name="company_number" id="company_number" placeholder="Company number" value="<?php if(isset($client['crm_company_number']) && ($client['crm_company_number']!='') ){ echo $client['crm_company_number'];}?>" class="fields edit_classname"  >
                                                      <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                                         <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group row date_birth name_fields sorting <?php if(isset($client['crm_incorporation_date']) && ($client['crm_incorporation_date']!='') ){?>light-color_company<?php } ?>" id="<?php echo $this->Common_mdl->get_order_details(3); ?>" <?php echo $this->Common_mdl->get_delete_details(3); ?>>
                                                   <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(3); ?></label>
                                                   <div class="col-sm-8 edit-field-popup1">
                                                      <!--  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span> -->
                                                      <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                      <input class="fields edit_classname <?php if(isset($client['crm_incorporation_date']) && ($client['crm_incorporation_date']!='') ){?>  light-color_company <?php }else{?> date_picker_dob <?php } ?>" type="text" name="date_of_creation" id="date_of_creation" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_incorporation_date']) && ($client['crm_incorporation_date']!='') ){ echo $client['crm_incorporation_date'];}?>" >
                                                      <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                                         <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="form-group row name_fields sorting <?php if(isset($client['crm_registered_in']) && ($client['crm_registered_in']!='') ){?>light-color_company<?php } ?>" id="<?php echo $this->Common_mdl->get_order_details(4); ?>" <?php echo $this->Common_mdl->get_delete_details(4); ?>>
                                                   <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(4); ?></label>
                                                   <div class="col-sm-8 edit-field-popup1">
                                                      <input type="text" name="registered_in" id="registered_in" placeholder="" value="<?php if(isset($client['crm_registered_in']) && ($client['crm_registered_in']!='')  ){ echo $client['crm_registered_in'];}?>" class="fields edit_classname" >
                                                      <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                                         <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="form-group row name_fields sorting <?php if(isset($client['crm_address_line_one']) && ($client['crm_address_line_one']!='') ){?>light-color_company<?php } ?>" id="<?php echo $this->Common_mdl->get_order_details(5); ?>" <?php echo $this->Common_mdl->get_delete_details(5); ?>>
                                                   <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(5); ?></label>
                                                   <div class="col-sm-8 edit-field-popup1">
                                                      <input type="text" name="address_line_one" id="address_line_one" placeholder="" value="<?php if(isset($client['crm_address_line_one']) && ($client['crm_address_line_one']!='')  ){ echo $client['crm_address_line_one'];}?>" class="fields edit_classname" >
                                                      <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                                         <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="form-group row name_fields sorting <?php if(isset($client['crm_address_line_two']) && ($client['crm_address_line_two']!='') ){?>light-color_company<?php } ?>"  id="<?php echo $this->Common_mdl->get_order_details(6); ?>" <?php echo $this->Common_mdl->get_delete_details(6); ?>>
                                                   <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(6); ?></label>
                                                   <div class="col-sm-8 edit-field-popup1">
                                                      <input type="text" name="address_line_two" id="address_line_two" placeholder="" value="<?php if(isset($client['crm_address_line_two']) && ($client['crm_address_line_two']!='')  ){ echo $client['crm_address_line_two'];}?>" class="fields edit_classname" >
                                                      <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                                         <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="form-group row name_fields sorting <?php if(isset($client['crm_address_line_three']) && ($client['crm_address_line_three']!='') ){?>light-color_company<?php } ?>" id="<?php echo $this->Common_mdl->get_order_details(7); ?>" <?php echo $this->Common_mdl->get_delete_details(7); ?>>
                                                   <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(7); ?></label>
                                                   <div class="col-sm-8 edit-field-popup1">
                                                      <input type="text" name="address_line_three" id="address_line_three" placeholder="" value="<?php if(isset($client['crm_address_line_three']) && ($client['crm_address_line_three']!='')  ){ echo $client['crm_address_line_three'];}?>" class="fields edit_classname" >
                                                      <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                                         <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="form-group row name_fields sorting <?php if(isset($client['crm_town_city']) && ($client['crm_town_city']!='') ){?>light-color_company<?php } ?>" id="<?php echo $this->Common_mdl->get_order_details(8); ?>" <?php echo $this->Common_mdl->get_delete_details(8); ?>>
                                                   <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(8); ?></label>
                                                   <div class="col-sm-8 edit-field-popup1">
                                                      <input type="text" name="crm_town_city" id="crm_town_city" placeholder="" value="<?php if(isset($client['crm_town_city']) && ($client['crm_town_city']!='')  ){ echo $client['crm_town_city'];}?>" class="fields edit_classname" >
                                                      <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                                         <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="form-group row name_fields sorting <?php if(isset($client['crm_post_code']) && ($client['crm_post_code']!='') ){?>light-color_company<?php } ?>" id="<?php echo $this->Common_mdl->get_order_details(9); ?>" <?php echo $this->Common_mdl->get_delete_details(9); ?>>
                                                   <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(9); ?></label>
                                                   <div class="col-sm-8 edit-field-popup1">
                                                      <input type="text" name="crm_post_code" id="crm_post_code" placeholder="" value="<?php if(isset($client['crm_post_code']) && ($client['crm_post_code']!='')  ){ echo $client['crm_post_code'];}?>" class="fields edit_classname" >
                                                      <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                                         <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="form-group row name_fields sorting  <?php if(isset($client['crm_company_status']) && ($client['crm_company_status']!='') ){?>light-color_company<?php } ?>" id="<?php echo $this->Common_mdl->get_order_details(10); ?>" <?php echo $this->Common_mdl->get_delete_details(10); ?>>
                                                   <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(10); ?></label>
                                                   <div class="col-sm-8 edit-field-popup1">
                                                      <!-- <input type="text" name="company_status" id="company_status" placeholder="active" value="<?php if(isset($client['crm_company_status']) && ($client['crm_company_status']!='')  ){ echo $client['crm_company_status'];}else{ echo '
                                                      active'; }?>" class="fields edit_classname" > -->
                                                      
                                                      <select name="company_status" id="company_status" class="fields edit_classname">
                                                         <option value="1">Active</option>
                                                         <option value="0">Inactive</option>
                                                         <option value="3">Frozen</option>
                                                         <option value="5">Archive</option>
                                                      </select>
                                                      <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                                         <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="form-group row name_fields sorting <?php if(isset($client['crm_company_type']) && ($client['crm_company_type']!='') ){?>light-color_company<?php } ?>" id="<?php echo $this->Common_mdl->get_order_details(11); ?>" <?php echo $this->Common_mdl->get_delete_details(11); ?>>
                                                   <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(11); ?></label>
                                                   <div class="col-sm-8 edit-field-popup1">
                                                      <input type="text" name="company_type" id="company_type" placeholder="Private Limited Company" value="<?php if(isset($client['crm_company_type']) && ($client['crm_company_type']!='') ){ echo $client['crm_company_type'];}?>" class="fields edit_classname" readonly>
                                                      <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                                         <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="form-group row name_fields sorting <?php if(isset($client['crm_company_sic']) && ($client['crm_company_sic']!='') ){?>light-color_company<?php } ?>" id="<?php echo $this->Common_mdl->get_order_details(12); ?>" <?php echo $this->Common_mdl->get_delete_details(12); ?>>
                                                   <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(12); ?></label>
                                                   <div class="col-sm-8 edit-field-popup1">
                                                      <input type="text" name="company_sic" id="company_sic" value="<?php if(isset($client['crm_company_sic']) && ($client['crm_company_sic']!='') ){ echo $client['crm_company_sic'];}?>" class="fields edit_classname" > 
                                                      <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                                         <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                                      </div>
                                                      <input type="hidden" name="sic_codes" id="sic_codes" value="">
                                                   </div>
                                                </div>
                                                <div class="form-group row date_birth sorting <?php if(isset($client['crm_engagement_letter']) && ($client['crm_engagement_letter']!='') ){?>light-color_company<?php } ?>" id="<?php echo $this->Common_mdl->get_order_details(13); ?>" <?php echo $this->Common_mdl->get_delete_details(13); ?>>
                                                   <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(13); ?></label>
                                                   <div class="col-sm-8">
                                                      <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                      <input class="form-control fields dob_picker" placeholder="dd-mm-yyyy" type="text" name="engagement_letter" id="engagement_letter" value="<?php if(isset($client['crm_letter_sign']) && ($client['crm_letter_sign']!='') ){ echo $client['crm_letter_sign'];}?>"/>
                                                   </div>
                                                </div>
                                                <div class="form-group row name_fields sorting <?php if(isset($client['crm_business_website']) && ($client['crm_business_website']!='') ){?>light-color_company<?php } ?>" id="<?php echo $this->Common_mdl->get_order_details(14); ?>" <?php echo $this->Common_mdl->get_delete_details(14); ?>>
                                                   <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(14); ?></label>
                                                   <div class="col-sm-8">
                                                      <input type="text" class="fields" name="business_website" id="business_website" placeholder="Business Website" value="<?php if(isset($client['crm_business_website']) && ($client['crm_business_website']!='') ){ echo $client['crm_business_website'];}?>">
                                                   </div>
                                                </div>
                                                <!-- rsptrspt-->
                                           
                                                <div class="form-group row sorting  <?php if(isset($client['crm_company_url']) && ($client['crm_company_url']!='') ){?>light-color_company<?php } ?>" id="<?php echo $this->Common_mdl->get_order_details(15); ?>" <?php echo $this->Common_mdl->get_delete_details(15); ?>>
                                                   <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(15); ?></label>
                                                   <div class="col-sm-8">
                                                      
                                                      <input type="text" name="company_url" id="company_url" value="<?php if(isset($client['crm_company_url']) && ($client['crm_company_url']!='') ){ echo $client['crm_company_url'];}?>" class="fields" placeholder="www.google.com">
                                                   </div>
                                                </div>
                                                
                                                <div class="form-group row sorting <?php if(isset($client['crm_officers_url']) && ($client['crm_officers_url']!='') ){?>light-color_company<?php } ?>" id="<?php echo $this->Common_mdl->get_order_details(16); ?>" <?php echo $this->Common_mdl->get_delete_details(16); ?>>
                                                   <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(16); ?></label>
                                                   <div class="col-sm-8">
                                                     
                                                      <input type="text" name="officers_url" id="officers_url" value="<?php if(isset($client['crm_officers_url']) && ($client['crm_officers_url']!='') ){ echo $client['crm_officers_url'];}?>" class="fields" placeholder="www.google.com">
                                                     
                                                   </div>
                                                </div>
                                       
                                             
                                                <div class="form-group row name_fields sorting" id="<?php echo $this->Common_mdl->get_order_details(17); ?>" <?php echo $this->Common_mdl->get_delete_details(17); ?>>
                                                   <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(17); ?></label>
                                                   <div class="col-sm-8">
                                                      <input type="text" class="fields" name="accounting_system_inuse" id="accounting_system_inuse" value="<?php if(isset($client['crm_accounting_system_inuse']) && ($client['crm_accounting_system_inuse']!='') ){ echo $client['crm_accounting_system_inuse'];}?>">
                                                   </div>
                                                </div>


                                                 <div class="form-group row name_fields sorting" id="<?php echo $this->Common_mdl->get_order_details(178); ?>" <?php echo $this->Common_mdl->get_delete_details(178); ?>>
                                                   <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(178); ?></label>
                                                   <div class="col-sm-8">                                              

                                                      <input type="text" value="" name="client_hashtag" id="tags" class="tags" />
                                                     
                                                   </div>
                                                </div>
                                                
                                       </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <!-- 2tab close -->
                                    <!-- 3tab start -->
                                    <div id="main_Contact" class="fullwidth-animation tab-pane fade" >
                                    <div class="space-required">
                                          <div class="main-pane-border1 main_Contact" id="main_Contact_error">
                                             <div class="alert-ss"></div>
                                          </div>
                                       </div>
                                       <div class="space-required remove_updatespace">
                                          <div class="main-pane-border1">
                                             <div class="pane-border1">
                                                <div class="management_form1 management_accordion">
                                                   <!-- <form>-->
                                                   <div class="new_bts"> 
                                                         <?php  if($uri_seg!='' &&  !empty($client['crm_company_number'])){ ?>
                                                         <a href="javascript:;" id="" class="btn btn-primary add_existing_person personss"  data-id="opt1" >Add Existing Person</a> 
                                                         <?php } ?> 
                                                         <a href="javascript:;" id="Add_New_Contact" class="">Add New Person</a>
                                                   </div>
                                                   <!-- primary info -->
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                        <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_ids;?>">
                                       <div id="contactss"></div>                                                                                
                                      <div class="contact_form companies-house-form">
                                       <?php 
                                          $i=1;                                          
                                          foreach ($contactRec as $contactRec_key => $contactRec_value) {                                          
                                                $title = $contactRec_value['title'];
                                                $first_name = $contactRec_value['first_name'];
                                                $middle_name = $contactRec_value['middle_name'];
                                                $surname = $contactRec_value['surname']; 
                                                $preferred_name = $contactRec_value['preferred_name']; 
                                                $mobile = $contactRec_value['mobile']; 
                                                $main_email = $contactRec_value['main_email']; 
                                                $nationality = $contactRec_value['nationality']; 
                                                $psc = $contactRec_value['psc']; 
                                                $shareholder = $contactRec_value['shareholder']; 
                                                $ni_number = $contactRec_value['ni_number']; 
                                                $contact_type = $contactRec_value['contact_type']; 
                                                $address_line1 = $contactRec_value['address_line1']; 
                                                $address_line2 = $contactRec_value['address_line2']; 
                                                $town_city = $contactRec_value['town_city']; 
                                                $post_code = $contactRec_value['post_code']; 
                                                $landline = $contactRec_value['landline']; 
                                                $work_email   = $contactRec_value['work_email']; 
                                                //$date_of_birth   = $contactRec_value['date_of_birth']; 
                                                $date_of_birth   = $contactRec_value['date_of_birth']; 
                                                $nature_of_control   = $contactRec_value['nature_of_control']; 
                                                $marital_status   = $contactRec_value['marital_status']; 
                                                $utr_number   = $contactRec_value['utr_number']; 
                                                $id   = $contactRec_value['id']; 
                                                $client_id   = $contactRec_value['client_id']; 
                                                $make_primary   = $contactRec_value['make_primary'];                                           
                                                $contact_names = $this->Common_mdl->numToOrdinalWord($i).' Contact ';
                                                $name=ucfirst($contactRec_value['first_name']).' '.ucfirst($contactRec_value['surname']);
                                            ?>
                                      <div class="space-required new_update1">
                                          <div class="update-data01 make_a_primary  common_div_remove-<?php echo $i; ?>" id="common_div_remove-<?php echo $i; ?>">
                           
                                              <input type="hidden" class="contact_table_id" name="contact_table_id[]" value="<?php echo $id;?>">

                                             <div class="client-info-circle1 floating_set">
                                                <div class=" remove<?php echo $id;?> contactcc ">
                                                   <div class="main-contact append_contact">
                                                      <span class="h4"><!-- <?php echo $name;?> -->Contact Person Details</span>
                                                      <div class="dead-primary1 for_row_count-<?php echo $i; ?>">

                                                        <input type="hidden" name="make_primary_loop[]" id="make_primary_loop" value="<?php echo $i; ?>">
                                                         <?php if($make_primary==1){?>
                                                             <div class="radio radio-inline">
                                                            <label>
                                                             <input type="radio" name="make_primary" id="make_primary<?php echo $i;?>" value="<?php echo $i;?>" checked>
                                                              <i class="helper" style="display: none;"></i>
                                                            </label>
                                                            <a href="javascript:void(0)" class="make_primary_section active" data-id="make_primary<?php echo $i;?>" ><span>Primary Contact</span></a>
                                                         </div>
                                                         <?php }else{
                                                            /** 01-09-2018 **/
                                                            ?>
                                                         <div class="radio radio-inline">
                                                            <label>
                                                           <!--  <input type="radio" name="" id="" value="" onclick="return make_a_primary(<?php echo $id.','.$client_id;?>);"> -->
                                                            <input type="radio" name="make_primary" id="make_primary<?php echo $i;?>" value="<?php echo $i;?>">

                                                            <i class="helper" style="display: none;"></i>
                                                            </label>
                                                            <a href="javascript:void(0)" class="make_primary_section"  data-id="make_primary<?php echo $i;?>" ><span>Make a primary</span></a>
                                                         </div>
                                                           
                                                            <?php
                                                            } ?>
                                                      </div>
                                                      <!-- <a href="#" data-toggle="modal" data-target="#modalcontact<?php echo $id;?>"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a> -->
                                                   </div>
                                                   <div class="primary-info02 common-spent01 addnewclient">
                                                      <div class="primary-info03 floating_set ">
                                                         <div id="collapse" class="panel-collapse">
                                                            <div class="basic-info-client15">
                                                                  <span class="primary-inner">
                                                                     <label>title</label>
                                                                     <!-- <input type="text" class="text-info title"  name="title[]" id="title<?php echo $id;?>" value="<?php echo $title;?>">  -->
                                                                     <select class="text-info title"  name="title" id="title<?php echo $id;?>">
                                                                        <option value="Mr" <?php if(isset($title) && $title=='Mr') {?> selected="selected"<?php } ?>>Mr</option>
                                                                        <option value="Mrs" <?php if(isset($title) && $title=='Mrs') {?> selected="selected"<?php } ?>>Mrs</option>
                                                                        <option value="Miss" <?php if(isset($title) && $title=='Miss') {?> selected="selected"<?php } ?>>Miss</option>
                                                                        <option value="Dr" <?php if(isset($title) && $title=='Dr') {?> selected="selected"<?php } ?>>Dr</option>
                                                                        <option value="Ms" <?php if(isset($title) && $title=='Ms') {?> selected="selected"<?php } ?>>Ms</option>
                                                                        <option value="Prof" <?php if(isset($title) && $title=='Prof') {?> selected="selected"<?php } ?>>Prof</option>
                                                                     </select>
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($first_name) && ($first_name!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>first name</label>
                                                                  <input type="text" class="text-info" name="first_name[<?php echo $i;?>]" id="first_name" value="<?php echo $first_name;?>">
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($middle_name) && ($middle_name!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>middle name</label>
                                                                  <input type="text" class="text-info" name="middle_name[]" id="middle_name<?php echo $id;?>" value="<?php echo $middle_name;?>">
                                                                  </span>
                                                                   <span class="primary-inner <?php  if(isset($last_name) && ($last_name!='') ){ ?>light-color_company<?php } ?>">
                                                                    <label>Last name</label>
                                                                    <input type="text" name="last_name[]" id="last_name" placeholder="" value="<?php if(isset($last_name) && ($last_name!='') ){ echo $last_name;}?>" class="text-info">
                                                                    </span>
                                                                  <span class="primary-inner <?php  if(isset($surname) && ($surname!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>surname</label>
                                                                  <input type="text" class="text-info" name="surname[]" id="surname<?php echo $id;?>" value="<?php echo $surname;?>">
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($preferred_name) && ($preferred_name!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>prefered name</label>
                                                                  <input type="text" class="text-info" name="preferred_name[]" id="preferred_name<?php echo $id;?>" value="<?php echo $preferred_name;?>">
                                                                  </span> 
                                                                  <span class="primary-inner <?php  if(isset($mobile) && ($mobile!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>mobile</label>
                                                                  <input type="text" class="text-info" name="mobile_number[<?php echo $i;?>]" id="mobile<?php echo $id;?>" value="<?php echo $mobile;?>">
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($main_email) && ($main_email!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>main E-Mail address</label>
                                                                  <input type="text" class="text-info" name="main_email[<?php echo $i;?>]" id="email_id<?php echo $id;?>"  value="<?php echo $main_email;?>">
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($nationality) && ($nationality!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>Nationality</label>
                                                                  <input type="text" class="text-info" name="nationality[]" id="nationality<?php echo $id;?>"  value="<?php echo $nationality;?>">
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($psc) && ($psc!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>PSC</label>
                                                                  <input type="text" class="text-info" name="psc[]" id="psc<?php echo $id;?>"  value="<?php echo $psc;?>">
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($shareholder) && ($shareholder!='') ){ ?>light-color_company<?php } ?>">
                                                                     <label>shareholder</label>
                                                                     <select name="shareholder" id="shareholder<?php echo $id;?>">
                                                                        <option value="yes" <?php if(isset($shareholder) && $shareholder=='yes') {?> selected="selected"<?php } ?>>Yes</option>
                                                                        <option value="no" <?php if(isset($shareholder) && $shareholder=='no') {?> selected="selected"<?php } ?>>No</option>
                                                                     </select>
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($ni_number) && ($ni_number!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>national insurance number</label>
                                                                  <input type="text" class="text-info" name="ni_number[]" id="ni_number<?php echo $id;?>"  value="<?php echo $ni_number;?>">
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($contactRec_value['country_of_residence']) && ($contactRec_value['country_of_residence']!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>Country Of Residence</label>
                                                                  <input type="text" name="country_of_residence[]" id="country_of_residence<?php echo $id;?>" class="text-info" value="<?php if(isset($contactRec_value['country_of_residence']) && ($contactRec_value['country_of_residence']!='') ){ echo $contactRec_value['country_of_residence'];}?>">
                                                                  </span>
                                                               
                                                               
                                                                  <span class="primary-inner contact_type <?php  if(isset($contact_type) && ($contact_type!='') ){ ?>light-color_company<?php } ?>">
                                                                     <label>contact type</label>
                                                                     <select name="contact_type" id="contact_type<?php echo $id;?>" class="othercus">
                                                                        <option value="Director" <?php if(isset($contact_type) && $contact_type=='Director') {?> selected="selected"<?php } ?>>Director</option>
                                                                        <option value="Director/Shareholder" <?php if(isset($contact_type) && $contact_type=='Director/Shareholder') {?> selected="selected"<?php } ?>>Director/Shareholder</option>
                                                                        <option value="Shareholder" <?php if(isset($contact_type) && $contact_type=='Shareholder') {?> selected="selected"<?php } ?>>Shareholder</option>
                                                                        <option value="Accountant" <?php if(isset($contact_type) && $contact_type=='Accountant') {?> selected="selected"<?php } ?>>Accountant</option>
                                                                        <option value="Bookkeeper" <?php if(isset($contact_type) && $contact_type=='Bookkeeper') {?> selected="selected"<?php } ?>>Bookkeeper</option>
                                                                        <option value="Other" <?php if(isset($contact_type) && $contact_type=='Other') {?> selected="selected"<?php } ?>>Other(Custom)</option>
                                                                     </select>
                                                                  </span>
                                                                  <span class="primary-inner spnMulti" id="others_customs" style="<?php if($contact_type=="Other"){echo 'display:block';}else {echo "display: none;";}?>">
                                                                  <label>Other(Custom)</label>
                                                                  <input type="text" class="text-info" name="other_custom[]" id="other_custom<?php echo $id;?>"  value="<?php if(isset($contactRec_value['other_custom']) && ($contactRec_value['other_custom']!='') ){ echo $contactRec_value['other_custom'];}?>">
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($address_line1) && ($address_line1=!'') ){ ?>light-color_company<?php } ?>">
                                                                  <label>address line1</label>
                                                                  <input type="text" class="text-info" name="address_line1[]" id="address_line1<?php echo $id;?>"  value="<?php echo $address_line1;?>">
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($address_line2) && ($address_line2!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>address line2</label>
                                                                  <input type="text" class="text-info" name="address_line2[]" id="address_line2<?php echo $id;?>"  value="<?php echo $address_line2;?>">
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($town_city) && ($town_city!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>town/city</label>
                                                                  <input type="text" class="text-info" name="town_city[]" id="town_city<?php echo $id;?>"  value="<?php echo $town_city;?>">
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($post_code) && ($post_code!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>post code</label>
                                                                  <input type="text" class="text-info" name="post_code[]" id="post_code<?php echo $id;?>"  value="<?php echo $post_code;?>">
                                                                  </span>
                                                  <span class="primary-inner landlnecls <?php  if(isset($contactRec_value['landline']) && ($contactRec_value['landline']!='') ){ ?>light-color_company<?php } ?>">
                                                  <div class="cmn-land-append1 update-primary pack_add_row_wrpr_landline">
                                                                     <label>landline</label>
                                                                     <div class="remove_one_row">
                                                                  <?php 
                                                                     $land = json_decode($contactRec_value['landline']);
                                                                     $pre_landline = json_decode($contactRec_value['pre_landline']);
                                                                     //print_r( $land);
                                                                     if(!empty($land)){
                                                                     foreach($land as $key =>$val) {
                                                                        
                                                                        $preselect = ['mobile'=>'','work'=>'','home'=>'','main'=>'','workfax'=>'','homefax'=>''];
                                                                        foreach ($preselect as $preselect_key => $preselect_val)
                                                                        {
                                                                           if( $preselect_key == $pre_landline[$key])
                                                                              {
                                                                                 $preselect[$key] = "selected='selected'";
                                                                              }
                                                                        }
                                                                     ?>
                                                                  
                                                   

                                                                     <select name="pre_landline[<?php echo $i;?>][<?php echo $key;?>]" id="pre_landline<?php echo $id;?>">
                                                                        <option value="mobile" <?php echo $preselect['mobile'];?>>Mobile</option>
                                                                        <option value="work" <?php echo $preselect['mobile'];?>>Work</option>
                                                                        <option value="home" <?php echo $preselect['home'];?>>Home</option>
                                                                        <option value="main" <?php echo $preselect['main'];?>>Main</option>
                                                                        <option value="workfax" <?php echo $preselect['workfax'];?>>Work Fax</option>
                                                                        <option value="homefax" <?php echo $preselect['homefax'];?>>Home Fax</option>
                                                                     </select>
                                                                     <div class="land-spaces">   <input type="text" class="text-info" name="landline<?php echo $i;?>[<?php echo $key;?>]" id="landline<?php echo $id;?>"  value="<?php echo $val;?>"> </div>
                                                                 
                                                
                                                                  <?php } } else {?>
                                                                  
                                                                     <select name="pre_landline[<?php echo $i;?>][0]" id="pre_landline<?php echo $id;?>">
                                                                        <option value="mobile">Mobile</option>
                                                                        <option value="work">Work</option>
                                                                        <option value="home">Home</option>
                                                                        <option value="main">Main</option>
                                                                        <option value="workfax">Work Fax</option>
                                                                        <option value="homefax">Home Fax</option>
                                                                     </select>
                                                                     <div class="land-spaces">    <input type="text" class="text-info" name="landline[<?php echo $i;?>][0]" id="landline<?php echo $id;?>"  value=""> </div>
                                                                  
                                                  
                                                                  <?php } ?>
                                                  
                                                                  <button type="button" class="btn btn-primary yk_pack_addrow_landline" data-id="<?php echo $i;?>">Add Landline</button>
                                                               
                                                               </div>
                                                               </div>
                                                </span>
                                                
                                                <span class="primary-inner displylinblock <?php  if(isset($contactRec_value['work_email']) && ($contactRec_value['work_email']!='') ){ ?>light-color_company <?php } ?>">
                                                                    <div class="update-primary pack_add_row_wrpr_email">
                                                                           <label>Work email</label>
                                                                        <div class="remove_one_row ">
                                                                  <?php 
                                                                     $work=json_decode($contactRec_value['work_email']);
                                                                                     if(!empty($land)){
                                                                     foreach($work as $key =>$val) {
                                                                     ?>
                                                                  
                                                                                    <input type="text" class="text-info" name="work_email[<?php echo$i;?>][<?php echo $key;?>]" id="work_email<?php echo $id;?>"  value="<?php echo $val;?>">

                                                                  <?php } } else {?>
                                                                  <input type="text" class="text-info" name="work_email[<?php echo$i;?>][0]" id="work_email<?php echo $id;?>"  value="">
                                                                  <?php } ?>
                                                                  <button type="button" class="btn btn-primary yk_pack_addrow_email" data-id="<?php echo $i;?>">Add Email</button>
                                                                     </div>
                                                                  </div>
                                                </span>  

                                                                  <span class="primary-inner date_birth <?php  if(isset($date_of_birth) && ($date_of_birth!='') ){ ?>light-color_company<?php } ?>">
                                                                     <label>dateof birth</label>
                                                                     <div class="picker-appoint">
                                                                        <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span> <input type="text" class="text-info date_picker_dob" name="date_of_birth[]"  placeholder="dd-mm-yyyy" value="<?php if(isset($date_of_birth) && ($date_of_birth!='') ){ echo date('d-m-Y',strtotime($date_of_birth));}?>">
                                                                     </div>
                                                                  </span>


                                                                  <span class="primary-inner <?php  if(isset($nature_of_control) && ($nature_of_control!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>nature of control</label>
                                                                  <input type="text" class="text-info" name="nature_of_control[]" id="nature_of_control<?php echo $id;?>"  value="<?php echo $nature_of_control;?>">
                                                                  </span>


                                                                  <span class="primary-inner <?php  if(isset($marital_status) && ($marital_status!='') ){ ?>light-color_company<?php } ?>">
                                                                     <label>marital status</label>
                                                                     <!-- <input type="text" class="text-info" name="marital_status" id="marital_status<?php// echo $id;?>"  value="<?php //echo $marital_status;?>"> -->
                                                                     <select name="marital_status" id="marital_status<?php echo $id;?>">
                                                                        <option value="Single" <?php if(isset($marital_status) && $marital_status=='Single') {?> selected="selected"<?php } ?>>Single</option>
                                                                        <option value="Living_together" <?php if(isset($marital_status) && $marital_status=='Living_together') {?> selected="selected"<?php } ?>>Living together</option>
                                                                        <option value="Engaged" <?php if(isset($marital_status) && $marital_status=='Engaged') {?> selected="selected"<?php } ?>>Engaged</option>
                                                                        <option value="Married" <?php if(isset($marital_status) && $marital_status=='Married') {?> selected="selected"<?php } ?>>Married</option>
                                                                        <option value="Civil_partner" <?php if(isset($marital_status) && $marital_status=='Civil_partner') {?> selected="selected"<?php } ?>>Civil partner</option>
                                                                        <option value="Separated" <?php if(isset($marital_status) && $marital_status=='Separated') {?> selected="selected"<?php } ?>>Separated</option>
                                                                        <option value="Divorced" <?php if(isset($marital_status) && $marital_status=='Divorced') {?> selected="selected"<?php } ?>>Divorced</option>
                                                                        <option value="Widowed" <?php if(isset($marital_status) && $marital_status=='Widowed') {?> selected="selected"<?php } ?>>Widowed</option>
                                                                     </select>
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($utr_number) && ($utr_number=!'') ){ ?>light-color_company<?php } ?>">
                                                                  <label>utr number</label>
                                                                  <input type="text" class="text-info" name="utr_number[]" id="utr_number<?php echo $id;?>"  value="<?php echo $utr_number;?>">
                                                                  </span>


                                                                  <span class="primary-inner <?php  if(isset($contactRec_value['occupation']) && ($contactRec_value['occupation']!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>Occupation</label>
                                                                  <input type="text" class="text-info" id="occupation<?php echo $id;?>"  name="occupation[]" value="<?php if(isset($contactRec_value['occupation']) && ($contactRec_value['occupation']!='') ){ echo $contactRec_value['occupation'];}?>">
                                                                  </span>

                                                                  <span class="primary-inner date_birth <?php  if(isset($contactRec_value['appointed_on']) && ($contactRec_value['appointed_on']!='') ){ ?>light-color_company<?php } ?>">
                                                                     <label>Appointed On</label>
                                                                     <div class="picker-appoint">
                                                                        <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                                        <input type="text" class="text-info" id="appointed_on<?php echo $id;?>"  name="appointed_on[]" value="<?php if(isset($contactRec_value['appointed_on']) && ($contactRec_value['appointed_on']!='') ){ echo $contactRec_value['appointed_on'];}?>">
                                                                     </div>
                                                                  </span>                                                              
                                                            
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div> 
                                    <!--    <input type="hidden" name="make_primary[]" id="make_primary[]" value="<?php echo $make_primary;?>"> -->
                                       <?php $i++;  } ?> 
                                        </div>
                                       <!--  <input type="hidden" name="incre" id="incre" value="<?php //echo $i-1;?>"> -->
                                       <!-- 05-07-2018 rs -->
                                       <input type="hidden" name="incre" id="incre" value="<?php echo $i;?>">
                                       <input type="hidden" name="append_cnt" id="append_cnt" value="<?php echo $i-1; ?>">
                                       <!-- end of 05-07-2018 -->
                                       <!--                        </form>
                                          -->
                                       <div class="contact_formss"></div>
                                       <!-- change into botton 05-07-2018 -->
                                      
                                       <!-- end of 05-07-2018 -->
                                       <!-- </form>-->
                                       <!-- <div class="sav-btn">
                                          <input type="button" value="save" class="contact_add">
                                          </div> -->
                                       <!--  <div class="button-group">
                                          <div class="next-bt-primary floatleft">
                                             <button type="button" class="btn btn-primary btnNext ">Next</button>
                                          </div>
                                          <div class="previous-bt-primary floatright">
                                             <button type="button" class="btn btn-primary btnPrevious">Previous</button>
                                          </div>
                                          </div> -->
                                    </div>
                                    <!-- accordion-panel -->  
                                    <!-- 3tab close -->
                                    <!-- 4tab open -->
                                    <!-- service tab-->
                                    <div id="service" class="fullwidth-animation tab-pane fade">
                                    <div class="space-required">
                                          <div class="main-pane-border1"  id="service_error">
                                                <div class="alert-ss"></div>
                                          </div>
                                       </div>
                                       <div class="space-required">
                                          <div class="basic-available-data">
                                             <div class="basic-available-data1">
                                                <table class="client-detail-table01 service-table-client">
                                                   <thead>
                                                      <tr>
                                                         <th>Services Name</th>
                                                         <!--   <th>Main Tabs</th> -->
                                                         <th><input type="checkbox" class="checkall js-small"  name="checkall" data-id="consu"> <span class="js-services1">Services</span></th>


                                                         <th class="reminds"><input type="checkbox" class="checkall_reminder js-small"  name="checkall" data-id="consu"> <span class="js-services1">Reminders</span></th>
                                                         <th><input type="checkbox" class="checkall_text js-small"  name="checkall" data-id="consu"> <span class="js-services1">Text</span></th>
                                                         <th><input type="checkbox" class="checkall_notification js-small"  name="checkall" data-id="consu"> <span class="js-services1">Invoice Notifications</span></th>
                                                      </tr>
                                                   </thead>
                                       
                                                   <tfoot class="ex_data1">
                                                   <tr>
                                                   <th>
                                                   </th>
                                                   </tr>
                                                   </tfoot>

                                                   <tbody>
                                                      <tr>
                                                         <td>Accounts</td>
                                                         <!-- <td><input type="checkbox" class="js-small f-right tab-all" name="enableAll" data-id="enableAll" ></td> -->
                                                         <td class="switching"><input type="checkbox" class="js-small f-right tab-one services" name="accounts[tab]" data-id="accounts"></td>
                                                         <td class="splwitch"><input type="checkbox" class="js-small f-right tab-second reminder" name="accounts[reminder]" data-id="enableacc"></td>
                                                         <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="accounts[text]" data-id="twot"></td>
                                                         <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="accounts[invoice]" data-id="inas"></td>
                                                      </tr>
                                                      <tr>
                                                         <td>Company Tax Return</td>
                                                         <!--  <td><input type="checkbox" class="js-small f-right tab-all" name="enableAll" data-id="enableAll" ></td> -->
                                                         <td class="switching"><input type="checkbox" class="js-small f-right services" name="companytax[tab]" data-id="companytax"></td>
                                                         <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="companytax[reminder]" data-id="enabletax"></td>
                                                         <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="companytax[text]" data-id="threet"></td>
                                                         <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="companytax[invoice]" data-id="inct"></td>
                                                      </tr>
                                                      <tr>
                                                         <td>Personal Tax Return</td>
                                                         <!--  <td><input type="checkbox" class="js-small f-right tab-all" name="enableAll" data-id="enableAll" ></td> -->
                                                         <td class="switching"><input type="checkbox" class="js-small f-right services" name="personaltax[tab]" data-id="personaltax"></td>
                                                         <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="personaltax[reminder]" data-id="enablepertax"></td>
                                                         <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="personaltax[text]" data-id="fourt"></td>
                                                         <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="personaltax[invoice]" data-id="inpt"></td>
                                                      </tr>
                                                      <tr>
                                                         <td>VAT Returns</td>
                                                         <!--   <td><input type="checkbox" class="js-small f-right tab-all" name="enableAll" data-id="enableAll" ></td> -->
                                                         <td class="switching"> <input type="checkbox" class="js-small f-right services" name="vat[tab]" data-id="vat"></td>
                                                         <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="vat[reminder]" data-id="enablevat"></td>
                                                         <td class="textwitch"> <input type="checkbox" class="js-small f-right text" name="vat[text]" data-id="sevent"></td>
                                                         <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="vat[invoice]" data-id="invt"></td>
                                                      </tr>
                                                      <tr>
                                                         <td>Payroll</td>
                                                         <!--   <td><input type="checkbox" class="js-small f-right tab-all" name="enableAll" data-id="enableAll" ></td> -->
                                                         <td class="switching"> <input type="checkbox" class="js-small f-right services" name="payroll[tab]" data-id="payroll"></td>
                                                         <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="payroll[reminder]" data-id="enablepay"></td>
                                                         <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="payroll[text]" data-id="fivet"></td>
                                                         <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="payroll[invoice]" data-id="inpr"></td>
                                                      </tr>
                                                      <tr id="service_activation_tab">
                                                         <td>Confirmation Statement</td>
                                                         <!--  <td><input type="checkbox" class="js-small f-right tab-all" name="enableAll" data-id="enableAll" ></td> -->
                                                         <td class="switching"><input type="checkbox" class="js-small f-right tab-one services" name="confirm[tab]" data-id="cons" ></td>
                                                         <td class="splwitch"><input type="checkbox" class="js-small f-right tab-second reminder" name="confirm[reminder]" data-id="enableconf"></td>
                                                         <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="confirm[text]" data-id="onet"></td>
                                                         <td class="invoicewitch"> <input type="checkbox" class="js-small f-right invoice" name="confirm[invoice]" data-id="incs"></td>
                                                      </tr>
                                                      <tr>
                                                         <td>WorkPlace Pension - AE</td>
                                                         <!--  <td><input type="checkbox" class="js-small f-right tab-all" name="enableAll" data-id="enableAll" ></td> -->
                                                         <td class="switching"><input type="checkbox" class="js-small f-right services" name="workplace[tab]" data-id="workplace"></td>
                                                         <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="workplace[reminder]" data-id="enablework"></td>
                                                         <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="workplace[text]" data-id="sixt"></td>
                                                         <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="workplace[invoice]" data-id="inpn"></td>
                                                      </tr>
                                                      <tr>
                                                         <td>CIS - Contractor</td>
                                                         <!--  <td><input type="checkbox" class="js-small f-right tab-all" name="enableAll" data-id="enableAll" ></td> -->
                                                         <td class="switching"><input type="checkbox" class="js-small f-right services" name="cis[tab]" data-id="cis"></td>
                                                         <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="cis[reminder]"  data-id="enablecis"></td>
                                                         <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="cis[text]" data-id="eightt"></td>
                                                         <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="cis[invoice]" data-id="incis"></td>
                                                      </tr>
                                                      <tr>
                                                         <td>CIS - Sub Contractor</td>
                                                         <!-- <td><input type="checkbox" class="js-small f-right tab-all" name="enableAll" data-id="enableAll" ></td> -->
                                                         <td class="switching"><input type="checkbox" class="js-small f-right services" name="cissub[tab]" data-id="cissub"></td>
                                                         <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="cissub[reminder]"  data-id="enablecissub"></td>
                                                         <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="cissub[text]" data-id="ninet"></td>
                                                         <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="cissub[invoice]" data-id="incis_sub"></td>
                                                      </tr>
                                                      <tr>
                                                         <td>Bookkeeping</td>
                                                         <!-- <td><input type="checkbox" class="js-small f-right tab-all" name="enableAll" data-id="enableAll" ></td> -->
                                                         <td class="switching"><input type="checkbox" class="js-small f-right services" name="bookkeep[tab]" data-id="bookkeep"></td>
                                                         <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="bookkeep[reminder]" data-id="enablebook"></td>
                                                         </td>
                                                         <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="bookkeep[text]" data-id="elevent"></td>
                                                         <td class="invoicewitch"> <input type="checkbox" class="js-small f-right invoice" name="bookkeep[invoice]" data-id="inbook"></td>
                                                      </tr>
                                                      <tr>
                                                         <td>P11D</td>
                                                         <!--  <td><input type="checkbox" class="js-small f-right tab-all" name="enableAll" data-id="enableAll" ></td> -->
                                                         <td class="switching"><input type="checkbox" class="js-small f-right services" name="p11d[tab]" data-id="p11d" ></td>
                                                         <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="p11d[reminder]" data-id="enableplld"></td>
                                                         <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="p11d[text]" data-id="tent"></td>
                                                         <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="p11d[invoice]" data-id="inp11d"></td>
                                                      </tr>
                                                      <tr>
                                                         <td>Management Accounts</td>
                                                         <!-- <td><input type="checkbox" class="js-small f-right tab-all" name="enableAll" data-id="enableAll" ></td> -->
                                                         <td class="switching"><input type="checkbox" class="js-small f-right services" name="management[tab]" data-id="management"></td>
                                                         <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="management[reminder]" data-id="enablemanagement"></td>
                                                         <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="management[text]" data-id="twelvet"></td>
                                                         <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="management[invoice]" data-id="inma"></td>
                                                      </tr>
                                                      <!-- ********** -->
                                                      <tr>
                                                         <td>Investigation Insurance</td>
                                                         <!--  <td><input type="checkbox" class="js-small f-right tab-all" name="enableAll" data-id="enableAll" ></td> -->
                                                         <td class="switching"> <input type="checkbox" class="js-small f-right services" name="investgate[tab]" data-id="investgate"></td>
                                                         <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="investgate[reminder]" data-id="enableinvest"></td>
                                                         <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="investgate[text]" data-id="thirteent"></td>
                                                         <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="investgate[invoice]" data-id="ininves"></td>
                                                      </tr>
                                                      <tr>
                                                         <td>Registered Address</td>
                                                         <!--   <td><input type="checkbox" class="js-small f-right tab-all" name="enableAll" data-id="enableAll" ></td> -->
                                                         <td class="switching"><input type="checkbox" class="js-small f-right services" name="registered[tab]" data-id="registered"></td>
                                                         <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="registered[reminder]" data-id="enablereg"></td>
                                                         <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="registered[text]" data-id="fourteent"></td>
                                                         <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="registered[invoice]" data-id="inreg"></td>
                                                      </tr>
                                                      <tr>
                                                         <td>Tax Advice/Investigation</td>
                                                         <!--  <td><input type="checkbox" class="js-small f-right tab-all" name="enableAll" data-id="enableAll" ></td> -->
                                                         <td class="switching"><input type="checkbox" class="js-small f-right services" name="taxadvice[tab]" data-id="taxadvice"></td>
                                                         <td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="taxadvice[reminder]" data-id="enabletaxad"></td>
                                                         <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="taxadvice[text]" data-id="fifteent"></td>
                                                         <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="taxadvice[invoice]" data-id="intaxadv"></td>
                                                      </tr>
                                                      <tr style="display: none;">
                                                         <td>Tax Investigation</td>
                                                         <!-- <td><input type="checkbox" class="js-small f-right tab-all" name="enableAll" data-id="enableAll" ></td> -->
                                                         <td class="switching"><input type="checkbox" class="js-small f-right services" name="taxinvest[tab]" data-id="taxinvest"></td>
                                                         <td class="splwitch"><input type="checkbox" class="js-small f-right " name="taxinvest[reminder]" data-id="enabletaxinves"></td>
                                                         <td class="textwitch"><input type="checkbox" class="js-small f-right text" name="taxinvest[text]" data-id="sixteent"></td>
                                                         <td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="taxinvest[invoice]" data-id="intaxinves"></td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <!-- service tab close -->
                                    <!-- 4tab close -->

                                    <!-- 5tab open -->
                                    <!-- assign to tab-->
                                    <div id="assignto" class="tab-pane fade ">
                                    <div class="space-required">
                                          <div class="main-pane-border1" id="assignto_error">
                                             <div class="alert-ss"></div>
                                          </div>
                                    </div>
                                       <input type="text" id="tree_select" name="assignees" placeholder="Select">
                                       <!-- IT's only fro assinees validation -->
                                        <input type="hidden" id="tree_select_values" name="assignees_values" placeholder="Select">
                                    </div>
                                    <!-- assignto -->
                                    <!-- 5tab close -->

                                    <!-- 6tab open -->

                                     <div id="amlchecks" class="tab-pane fade ">
                                    <div class="space-required">
                                          <div class="main-pane-border1" id="amlchecks_error">
                                             <div class="alert-ss"></div>
                                          </div>
                                       </div>

                                       <div class="masonry-container floating_set ">
                                          <div class="grid-sizer"></div>
                                          <div class="accordion-panel">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Anti Money Laundering Checks</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1">
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Client ID Verified</label>
                                                         <div class="col-sm-8">
                                                            <input type="checkbox" class="js-small f-right fields" name="client_id_verified" id="client_id_verified" >
                                                         </div>
                                                      </div>
                                                      <div class="form-group row assign_cus_type client_id_verified_data" style="display: none;">
                                                         <label class="col-sm-4 col-form-label">Type of ID Provided</label>
                                                         <div class="col-sm-8">
                                                         <div class="dropdown-sin-11 lead-form-st">
                                                            <select name="type_of_id[]" id="person" class="form-control fields assign_cus" multiple="multiple" placeholder="Select">
                                                               <option value="" disabled="disabled">--Select--</option>
                                                               <option value="Passport">Passport</option>
                                                               <option value="Driving_License">Driving License</option>
                                                               <option value="Other_Custom">Other Custom</option>
                                                            </select>
                                                         </div>
                                                         </div>
                                                      </div>
                                             <!-- 29-08-2018 for multiple image upload option -->
                                             <div class="form-group row" >
                                                   <label class="col-sm-4 col-form-label">Attachement</label>
                                                   <div class="col-sm-8">
                                                    
                                                      <div class="custom_upload">
                                                      <label for="proof_attach_file" class="other-file"></label>
                                                      <input type="file" name="proof_attach_file[]" id="proof_attach_file" multiple="multiple" >
                                                   </div>

                                                   </div>
                                             </div>
                                             <!-- en dof image upload for type of ids -->
                                                      <div class="form-group row name_fields spanassign for_other_custom_choose" style="display:none">
                                                         <label class="col-sm-4 col-form-label">Other Custom</label>
                                                         <div class="col-sm-8">
                                                            <input type="text" name="assign_other_custom" id="assign_other_custom" placeholder="" value="" class="fields">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Proof of Address</label>
                                                         <div class="col-sm-8">
                                                            <input type="checkbox" class="js-small f-right fields" name="proof_of_address" id="proof_of_address">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Meeting with the Client</label>
                                                         <div class="col-sm-8">
                                                            <input type="checkbox" class="js-small f-right fields" name="meeting_client" id="meeting_client">
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- accordion-panel -->   
                                       </div>
                                    </div>
                                    <!-- 6 tab close -->

                                    <!-- 7tab start -->
                                    <div id="other" class="tab-pane fade">
                                       <div class="space-required ">
                                             <div class="main-pane-border1" id="other_error">
                                                <div class="alert-ss"></div>
                                             </div>
                                       </div>
                                       <div class="space-required" style="display: none;">
                                          <div class="management_form1 management_accordion">
                                             <div class="pane-border1">
                                                <!-- <form>-->
                                                <!--<div class="form-group row">
                                                   <label class="col-sm-4 col-form-label">Referred By</label>
                                                   <div class="col-sm-8">
                                                      <select name="refered_by" id="refered_by" class="fields">
                                                         <option value="">select</option>
                                                         <?php foreach ($referby as $referbykey => $referbyvalue) {
                                                      # code...
                                                       ?>
                                                         <option value="<?php echo $referbyvalue['id'];?>" <?php if(isset($client['crm_refered_by']) && $client['crm_refered_by']==$referbyvalue['user_id']) {?> selected="selected"<?php } ?>><?php echo $referbyvalue['crm_first_name'];?></option>
                                                         <?php } ?>
                                                      </select>
                                                   </div>
                                                   </div>-->
                                                <!--  <div class="form-group row">
                                                   <label class="col-sm-4 col-form-label">Username</label>
                                                   <div class="col-sm-8">
                                                      <input type="text" name="user_name" id="user_name" value="" class="fields">
                                                   </div>
                                                   </div> 
                                                   <div class="form-group row">
                                                   <label class="col-sm-4 col-form-label">Email</label>
                                                   <div class="col-sm-8">
                                                      <input type="text" name="emailid" id="emailid" value="" class="fields">
                                                   </div>
                                                   </div>
                                                   <div class="form-group row">
                                                   <label class="col-sm-4 col-form-label">Phone Number</label>
                                                   <div class="col-sm-8">
                                                      <input type="number" class="con-code" name="cun_code" name="cun_code">
                                                      <input type="number" name="pn_no_rec" id="pn_no_rec" value="" class="fields">
                                                   </div>
                                                   </div>-->
                                                <div class="form-group row">
                                                   <label class="col-sm-4 col-form-label">Profile Image</label>
                                                   <div class="col-sm-8">
                                                      <input type="file" name="profile_image" id="profile_image" class="fields">
                                                   </div>
                                                </div>
                                                <div class="form-group row name_fields radio_button02">
                                                   <label class="col-sm-4 col-form-label">Gender</label>
                                                   <div class="col-sm-8 gender-01">
                                                      <p  class="radio radio-inline">
                                                         <label><input type="radio" class="fields" id="test1" name="gender" value="male" <?php if(isset($client['crm_gender']) && ($client['crm_gender']=='male') ){?> checked="checked"<?php } ?>><i class="helper"></i> <span>Male</span> </label>
                                                      </p>
                                                      <p  class="radio radio-inline"><label><input type="radio" class="fields" id="test2" name="gender" value="female" <?php if(isset($client['crm_gender']) && ($client['crm_gender']=='female') ){?> checked="checked"<?php } ?>><i class="helper"></i> <span>Female</span></label></p>
                                                   </div>
                                                </div>
                                                <!-- <div class="form-group row" style="display:none;">
                                                   <label class="col-sm-4 col-form-label">Packages</label>
                                                   <div class="col-sm-8">
                                                      <input type="text" name="packages" id="pakages" placeholder="Packages" value="<?php if(isset($client['crm_packages']) && ($client['crm_packages']!='') ){ echo $client['crm_packages'];}?>" class="fields">
                                                   </div>
                                                   </div>
                                                   <div class="form-group row name_fields">
                                                   <label class="col-sm-4 col-form-label">Password</label>
                                                   <div class="col-sm-8">
                                                      <input type="password" name="password" id="password" placeholder="password" value="" class="fields">
                                                   </div>
                                                   </div>
                                                   <div class="form-group row name_fields">
                                                   <label class="col-sm-4 col-form-label">Confirm Passowrd</label>
                                                   <div class="col-sm-8">
                                                      <input type="password" name="confirm_password" id="confirm_password" placeholder="confirm password" value="" class="fields"> 
                                                   </div>
                                                   </div> -->
                                             </div>
                                          </div>
                                       </div>
                                       <div class="masonry-container floating_set">
                                          <div class="grid-sizer"></div>
                                          <!-- other sub-->
                                          <div class="accordion-panel">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Other</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1" id="others_section">
                                                      <div class="form-group row radio_bts others_details" id="<?php echo $this->Common_mdl->get_order_details(78); ?>">
                                                         <label class="col-sm-4 col-form-label">Previous Accounts</label>
                                                         <div class="col-sm-8">
                                                            <input type="checkbox" class="js-small f-right fields" name="previous_account" id="previous_account" value="on" data-id="preacc">
                                                         </div>
                                                      </div>
                                                      <!-- <div id="" style="display:none;"> -->
                                                         <div class="form-group row name_fields preacc_content_toggle others_details" id="<?php echo $this->Common_mdl->get_order_details(192); ?>" style="display: none;">
                                                            <label class="col-sm-4 col-form-label">Name of the Firm</label>
                                                            <div class="col-sm-8">
                                                               <input type="text" name="name_of_firm" id="name_of_firm" placeholder="" value="" class="fields">
                                                            </div>
                                                         </div>
                                                         <div class="form-group row text-box1 preacc_content_toggle others_details" id="<?php echo $this->Common_mdl->get_order_details(191); ?>" style="display: none;">
                                                            <label class="col-sm-4 col-form-label">Address</label>
                                                            <div class="col-sm-8">
                                                               <textarea rows="4" name="other_address" id="other_address" class="form-control fields"></textarea>
                                                            </div>
                                                         </div>
                                                    
                                                      <div class="form-group row name_fields others_details preacc_content_toggle" id="<?php echo $this->Common_mdl->get_order_details(79); ?>" style="display: none;">
                                                         <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(79); ?></label>
                                                         <div class="col-sm-8">
                                                            <input type="text" class="con-code" name="cun_code" name="cun_code"  >
                                                            <input type="text" name="pn_no_rec" id="pn_no_rec" class="fields">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row name_fields others_details preacc_content_toggle" id="<?php echo $this->Common_mdl->get_order_details(80); ?>" style="display: none;">
                                                         <label class="col-sm-4 col-form-label">Email Address</label>
                                                         <div class="col-sm-8">
                                                            <input type="email" name="emailid" id="emailid" placeholder="" value="" class="fields">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts others_details preacc_content_toggle" id="<?php echo $this->Common_mdl->get_order_details(81); ?>" style="display: none;">
                                                         <label class="col-sm-4 col-form-label">Chase for Information</label>
                                                         <div class="col-sm-8">
                                                            <input type="checkbox" class="js-small f-right fields" name="chase_for_info" id="chase_for_info" data-id="chase_for_info">
                                                         </div>
                                                      </div>
                                                        <!-- </div> --><!-- hide div for previous account -->
                                                      <!-- preacc close-->
                                                    
                                                      <div class="form-group row for_other_notes others_details text-box1" id="<?php echo $this->Common_mdl->get_order_details(82); ?>" style="display: none;">
                                                         <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(82); ?></label>
                                                         <div class="col-sm-8">
                                                            <textarea rows="4" name="other_notes" id="other_notes" class="form-control fields"></textarea>
                                                         </div>
                                                      </div>
                                                    
                                             
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- accordion-panel -->
                                          <div class="accordion-panel">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Additional Information - Internal Notes</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1">
                                                      <div class="form-group row text-box1" >
                                                         <label class="col-sm-4 col-form-label">Notes</label>
                                                         <div class="col-sm-8">
                                                            <textarea rows="4" name="other_internal_notes" id="other_internal_notes" class="form-control fields"></textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- accordion-panel -->
                                               <!-- accordion-panel client login-->
                                          <div class="accordion-panel">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Client Login
                                                         <input type="checkbox" class="js-small f-right fields" name="show_login" id="show_login" data-id="show_login" value="on">
                                                      </a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse show_login" style="display: none;" >
                                                   <div class="basic-info-client1" id="other_details1">
                                                              <!-- 30-08-2018 move section -->
                                                        <div class="form-group row name_fields others_details" id="<?php echo $this->Common_mdl->get_order_details(88); ?>"  <?php echo $this->Common_mdl->get_delete_details(88); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(88); ?></label>
                                                         <div class="col-sm-8">
                                                            <input type="text" name="user_name" id="user_name" placeholder="" value="" class="fields">
                                                           
                                                         </div>
                                                      </div>
                                                      <div class="form-group row name_fields others_details" id="<?php echo $this->Common_mdl->get_order_details(89); ?>" <?php echo $this->Common_mdl->get_delete_details(89); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(89); ?></label>
                                                         <div class="col-sm-8">
                                                            <input type="password" name="password" id="password" placeholder="Password" value="" class="fields">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row name_fields others_details" id="<?php  $query=$this->Common_mdl->get_order_details(89); echo $query+1; ?>" <?php echo $this->Common_mdl->get_delete_details(89); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(89); ?></label>
                                                         <div class="col-sm-8">
                                                            <input type="password" name="confirm_password" id="confirm_password" placeholder="confirm password" value="" class="fields"> 
                                                         </div>
                                                      </div>
                                                      <!-- end of move section -->
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- accordion-panel -->
                                         <div class="accordion-panel">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headipn_no_recngOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Client Source</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1">
                                                      <div class="form-group row">
                                                         <label class="col-sm-4 col-form-label">Source</label>
                                                         <div class="col-sm-8">
                                                            <select name="source" id="source" class="form-control fields">
                                                               <option value="" selected="selected">--Select--</option>
                                                               <option value="Google">Google</option>
                                                               <option value="FB">FB</option>
                                                               <option value="Website">Website</option>
                                                               <option value="Existing Client">Existing Client</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                      <div class="form-group row name_fields dropdown_source" style="display: none;">
                                                         <label class="col-sm-4 col-form-label">Refered by Existing Client</label>
                                                         <div class="col-sm-8">
                                                            <!-- <div class="textfor_source">
                                                               <input type="text" name="refer_exist_client" id="refer_exist_client" class="fields" placeholder="Search Client" value="" />
                                                               <div id="searchresultclient"></div>
                                                               </div> -->
                                                            <div class="dropdown-sin-3">
                                                               <select name="refer_exist_client" id="refer_exist_client" class="fields">
                                                                  <option value=''>--Select one--</option>
                                                                  <?php foreach ($referby as $referbykey => $referbyvalue) {
                                                                     # code...
                                                                     ?>
                                                                  <option value="<?php echo $referbyvalue['id'];?>"><?php echo $referbyvalue['crm_first_name'];?></option>
                                                                  <?php } ?>
                                                               </select>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="form-group row name_fields">
                                                         <label class="col-sm-4 col-form-label">Relationship to the Client</label>
                                                         <div class="col-sm-8">
                                                            <input type="text" name="relationship_client" id="relationship_client" placeholder="" value="" class="fields">
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- accordion-panel -->
                                          <div class="accordion-panel" style="display: none;">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Notes if any other</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1">
                                                      <div class="form-group row text-box1 ">
                                                         <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(90); ?></label>
                                                         <div class="col-sm-8">
                                                            <textarea rows="4" name="other_any_notes" id="other_any_notes" class="form-control fields"></textarea>
                                                         </div>
                                                      </div>
                                                      <?php
                                                     //    echo render_custom_fields_one( 'other',false);  ?>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!-- accordion-panel -->
                                       <!-- other sub close -->
                                       <div class="floating_set text-right accordion_ups">
                                          <!--  <a href="javascript:void()" id="accordion_close">
                                             <img src="<?php echo base_url();?>assets/images/acc_up.png" alt="img">
                                             <img src="<?php echo base_url();?>assets/images/acc_down.png" alt="img">
                                             </a> -->
                                          <!-- <a class="add_acc_client" href="javascript:void()">Add Client</a> -->
                                          <input type="hidden" name="user_id" id="user_id" value="<?php if(isset($client['user_id']) && ($client['user_id']!='') ){ echo $client['user_id'];}?>" class="fields">
                                          <input type="hidden" name="client_id" id="client_id" value="<?php if(isset($user[0]['client_id']) && ($user[0]['client_id']!='') ){ echo $user[0]['client_id'];}?>" class="fields">
                                          <input type="hidden" id="company_house" name="company_house" value="0" class="fields">
                                          <input type="hidden" name="companynumber" id="companynumber" value="" class="fields">
                                       </div>
                                    </div>
                                    <!-- other tab close -->  
                                    <!-- 7tab close -->
                                    <!-- 8tab -->
                                    <!-- for referral tab section 30-08-2018 -->
                                    <div id="referral" class="tab-pane fade">
                                      <div class="space-required ">
                                          <div class="main-pane-border1" id="referral_error" >
                                             <div class="alert-ss"></div>
                                          </div>
                                       </div>
                                       <div class="masonry-container floating_set">
                                          <div class="grid-sizer"></div>
                                       
                                  <div class="accordion-panel">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Invite Client</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1" id="referals">
                                                      <div class="form-group row radio_bts referal_details" id="<?php echo $this->Common_mdl->get_order_details(83); ?>" <?php echo $this->Common_mdl->get_delete_details(83); ?>>
                                                         <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(83); ?></label>
                                                         <div class="col-sm-8">
                                                            <input type="checkbox" class="js-small f-right fields" name="invite_use" id="invite_use">
                                                         </div>
                                                      </div>
                                                      <div class="nonediv" style="display: none;">
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">crm</label>
                                                         <div class="col-sm-8">
                                                            <input type="checkbox" class="js-small f-right fields" name="other_crm" id="other_crm" >
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Proposal</label>
                                                         <div class="col-sm-8">
                                                            <input type="checkbox" class="js-small f-right fields" name="other_proposal" id="other_proposal">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Tasks</label>
                                                         <div class="col-sm-8">
                                                            <input type="checkbox" class="js-small f-right fields" name="other_task" id="other_task">
                                                         </div>
                                                      </div>
                                                      </div><!-- display none div -->
                                                      <div class="form-group row name_fields referal_details" id="<?php echo $this->Common_mdl->get_order_details(87); ?>">
                                                         <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(87); ?></label>
                                                         <div class="col-sm-8">
                                                            <input type="text" name="send_invit_link" id="send_invit_link" placeholder="" value="" class="fields">
                                                         </div>
                                                      </div>
                                            
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                 
                                       </div>
                              
                           <!-- tree start    -->
                           <!-- display:none for temprary -->
                           <div class="child-trees1" style="display: none;" > <!-- display:none for temprary -->
                           <div class="tree">
   <ul>
      <li>
          
<div class="child_drag">
            <div class="child_imgs">
            <img src="https://remindoo.uk/uploads/unknown.png"></div>
            <div class="child_text">
                <span>henry bennett</span>
                <p>
                    chairman & ceo
                </p>
             </div>
           </div> 
         <ul>
            <li>
               <div class="child_drag">
            <div class="child_imgs">
            <img src="https://remindoo.uk/uploads/unknown.png"></div>
            <div class="child_text">
                <span>henry bennett</span>
                <p>
                    chairman & ceo
                </p>
             </div>
           </div> 
            </li>
            <li>
               <div class="child_drag">
            <div class="child_imgs">
            <img src="https://remindoo.uk/uploads/unknown.png"></div>
            <div class="child_text">
                <span>henry bennett</span>
                <p>
                    chairman & ceo
                </p>
             </div>
           </div> 
            </li>
            <li>
               <div class="child_drag">
            <div class="child_imgs">
            <img src="https://remindoo.uk/uploads/unknown.png"></div>
            <div class="child_text">
                <span>henry bennett</span>
                <p>
                    chairman & ceo
                </p>
             </div>
           </div> 
               <ul>
                  <li>
                     <div class="child_drag">
            <div class="child_imgs">
            <img src="https://remindoo.uk/uploads/unknown.png"></div>
            <div class="child_text">
                <span>henry bennett</span>
                <p>
                    chairman & ceo
                </p>
             </div>
           </div>
                  </li>
               </ul>
            </li>
            <li>
                <div class="child_drag">
            <div class="child_imgs">
            <img src="https://remindoo.uk/uploads/unknown.png"></div>
            <div class="child_text">
                <span>henry bennett</span>
                <p>
                    chairman & ceo
                </p>
             </div>
           </div>
               <ul>
                  <li> 
              <div class="child_drag">
            <div class="child_imgs">
            <img src="https://remindoo.uk/uploads/unknown.png"></div>
            <div class="child_text">
                <span>henry bennett</span>
                <p>
                    chairman & ceo
                </p>
             </div>
           </div> </li>
                  <li>
                     <div class="child_drag">
            <div class="child_imgs">
            <img src="https://remindoo.uk/uploads/unknown.png"></div>
            <div class="child_text">
                <span>henry bennett</span>
                <p>
                    chairman & ceo
                </p>
             </div>
           </div> 
                  </li>
                  <li>
               <div class="child_drag">
            <div class="child_imgs">
            <img src="https://remindoo.uk/uploads/unknown.png"></div>
            <div class="child_text">
                <span>henry bennett</span>
                <p>
                    chairman & ceo
                </p>
             </div>
           </div> </li>
               </ul>
            </li>
         </ul>
      </li>
   </ul>
</div>
</div>
                           
                           <!-- tree close -->
                              
                                       <!-- accordion-panel -->
                                       <!-- other sub close -->
                                   
                                    </div>
                                    <!-- end of refferral tab -->
                                    <!-- 8tab close -->
                                    
                                    <!-- <div class="button-group">
                                       <div class="next-bt-primary floatleft">
                                          <button type="button" class="btn btn-primary btnNext">Next</button>
                                       </div>
                                       <div class="previous-bt-primary floatright">
                                          <button type="button" class="btn btn-primary btnPrevious">Previous</button>
                                          </div>
                                       </div> -->
                                    <?php if($uri_seg==''){ ?>
                                  <!--   </div>  -->
                                    <?php } ?>  <!-- hide div -->
                                    <!-- 13tab close -->
                                    
                                    
                                    <!-- 20-08-2018 aml checks-->
                                   
                                    <!-- end of 20-08-2018 -->

                                    <!-- 9tab open -->
                                    <!-- Filing Allocation -->
                                    <div id="filing-allocation" class="tab-pane fade ">
                                       <div class="masonry-container floating_set ">
                                          <div class="grid-sizer"></div>
                                          <!-- accordion-panel -->
                                          <div class="accordion-panel">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Filing Allocation</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1">
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Company Number</label>
                                                         <div class="col-sm-8">
                                                            <input type="text" class="fields" name="fa_cmyno" id="fa_cmyno" >
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts date_birth">
                                                         <label class="col-sm-4 col-form-label">Incorporation Date</label>
                                                         <div class="col-sm-8">
                                                            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                            <input class="form-control fields dob_picker" placeholder="dd-mm-yyyy" type="text" name="fa_incordate" id="fa_incordate" />
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Turn Over</label>
                                                         <div class="col-sm-8">
                                                            <input type="text" class="fields" name="fa_turnover" id="fa_turnover" >
                                                         </div>
                                                      </div>
                                                      <div class="form-group row assign_cus_type date_birth">
                                                         <label class="col-sm-4 col-form-label">Date Of Trading</label>
                                                         <div class="col-sm-8">
                                                            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                            <input class="form-control fields dob_picker" placeholder="dd-mm-yyyy" type="text" name="fa_dateoftrading" id="fa_dateoftrading" />
                                                         </div>
                                                      </div>
                                                      <div class="form-group row name_fields spanassign" >
                                                         <label class="col-sm-4 col-form-label">SIC Code</label>
                                                         <div class="col-sm-8">
                                                            <input type="text" name="fa_siccode" id="fa_siccode" placeholder="" value="" class="fields">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Nuture Of Business</label>
                                                         <div class="col-sm-8">
                                                            <input type="text" class="fields" name="fa_nutureofbus" id="fa_nutureofbus" >
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Company UTR</label>
                                                         <div class="col-sm-8">
                                                            <input type="text" class="fields" name="fa_cmyutr" id="fa_cmyutr" >
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Companies House Authorisation Code</label>
                                                         <div class="col-sm-8">
                                                            <input type="text" class="fields" name="fa_cmyhouse" id="fa_cmyhouse" >
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts text-box1">
                                                         <label class="col-sm-4 col-form-label">Registered Address</label>
                                                         <div class="col-sm-8">
                                                            <textarea rows="3" placeholder="" name="fa_registadres" id="fa_registadres" class="fields"></textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- accordion-panel -->   
                                       </div>
                                    </div>
                                    <!-- Filing Allocation -->
                                    <!-- 9tab close -->
                                    <!-- 10tab open -->
                                    <!-- Business Details -->
                                    <div id="business-det" class="tab-pane fade ">
                                       <div class="masonry-container floating_set ">
                                          <div class="grid-sizer"></div>
                                          <!-- accordion-panel -->
                                          <div class="accordion-panel">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Business Details</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1">
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Trading As</label>
                                                         <div class="col-sm-8">
                                                            <input type="text" class="fields" name="bus_tradingas" id="bus_tradingas" >
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts date_birth">
                                                         <label class="col-sm-4 col-form-label">Commenced Trading</label>
                                                         <div class="col-sm-8">
                                                            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                            <input class="form-control fields dob_picker" placeholder="dd-mm-yyyy" type="text" name="bus_commencedtrading" id="bus_commencedtrading" />
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts date_birth">
                                                         <label class="col-sm-4 col-form-label">Registered for SA</label>
                                                         <div class="col-sm-8">
                                                            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                            <input class="form-control fields dob_picker" placeholder="dd-mm-yyyy" type="text" name="bus_regist" id="bus_regist" />
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Turn Over</label>
                                                         <div class="col-sm-8">
                                                            <input type="text" class="fields" name="bus_turnover" id="bus_turnover" >
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Nuture Of Business</label>
                                                         <div class="col-sm-8">
                                                            <input type="text" class="fields" name="bus_nutureofbus" id="bus_nutureofbus" >
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- accordion-panel -->   
                                       </div>
                                    </div>
                                    <!-- Business Details -->
                                   <!-- 10th tab close -->
                                    
                                  
                                   <!-- 11tab open -->
                                    <div id="import_tab" class="tab-pane fade">
                                       <div class="space-required">
                                          <div class="main-pane-border1 cmn-errors1" id="services_information_error">
                                             <div class="alert-ss"></div>
                                          </div>
                                       </div>
                           <ul class="accordion-views">
                              
                              <!-- confirm tab start-->

                              <li class="show-block">
                                 <a href="JavaScript:void(0);" class="toggle">  Confirmation Statement</a>
                               
                               <div id="confirmation-statement" class="masonry-container floating_set inner-views" id="conf_stmt">
                                          <div class="accordion-panel">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Important Information</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1">
                                                      <div class="form-group row name_fields"  <?php echo $this->Common_mdl->get_delete_details(18); ?>>
                                                         <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(18); ?></label>
                                                         <div class="col-sm-8">
                                                            <input type="text" name="confirmed" id="confirmation_auth_code" placeholder="" value="" class="fields confirmation_auth_code" ><!-- readonly -->
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- accordion-panel -->
                                          <div class="accordion-panel">
                                             <div class="box-division03" >
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Confirmation Statement</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1" id="box1">
                                                      <div class="form-group row name_fields date_birth sorting_conf" id="<?php echo $this->Common_mdl->get_order_details(23); ?>"  <?php echo $this->Common_mdl->get_delete_details(23); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(23); ?></label><span class="hilight_due_date">*</span>
                                                         <div class="col-sm-8  edit-field-popup1">
                                                            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                            <input type="text" name="confirmation_next_made_up_to" id="confirm_next_made_up_to" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_confirmation_statement_date'])){ echo date('d-m-Y',strtotime($client['crm_confirmation_statement_date'])); }  ?>" class="fields edit_classname datepicker" >
                                                            <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                                               <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="form-group row name_fields date_birth sorting_conf" id="<?php echo $this->Common_mdl->get_order_details(24); ?>" <?php echo $this->Common_mdl->get_delete_details(24); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(24); ?></label>
                                                         <div class="col-sm-8 edit-field-popup1">
                                                            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                            
                                                            <input type="text" name="confirmation_next_due" id="confirm_next_due" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_confirmation_statement_due_date'])){
                                                             echo date('d-m-Y',strtotime($client['crm_confirmation_statement_due_date'])); }  ?>" class="fields edit_classname datepicker" >
                                                            <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                                               <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                                            </div>
                                                         </div>
                                                      </div>
                                                       <div class="form-group row name_fields date_birth sorting_conf" id="<?php echo $this->Common_mdl->get_order_details(195); ?>" <?php echo $this->Common_mdl->get_delete_details(195); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(195); ?></label>
                                                         <div class="col-sm-8 edit-field-popup1">
                                                            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                            <input type="text" name="statement_last_made_up_to_date" id="statement_last_made_up_to_date" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_confirmation_statement_last_made_up_to_date'])){
                                                             echo date('d-m-Y',strtotime($client['crm_confirmation_statement_last_made_up_to_date'])); }  ?>" class="fields edit_classname datepicker" >
                                                            <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                                               <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                                            </div>
                                                         </div>
                                                      </div>

                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- accordion-panel -->
                                          <div class="accordion-panel enable_conf" style="display:none;">
                                             <div class="box-division03" >
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Confirmation Reminders</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1" id="box2">
                                                      <div class="form-group row name_fields radio_bts" >
                                                         <label class="col-sm-4 col-form-label"><a name="add_custom_reminder_link" id="add_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/1" target="_blank">As per firm setting</a></label>
                                                         <div class="col-sm-8">
                                                            <!--  <input type="hidden" name="confirmation_next_reminder" id="confirmation_next_reminder" placeholder="" value="" class="fields dob_picker"> -->
                                                            <input type="checkbox" class="js-small f-right fields as_per_firm" name="confirmation_next_reminder" id="confirmation_next_reminder_date"  >
                                                         </div>
                                                      </div>
                                                      <!-- <div class="form-group row radio_bts sorting_conf" id="<?php echo $this->Common_mdl->get_order_details(26); ?>"  <?php echo $this->Common_mdl->get_delete_details(26); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(26); ?></label>
                                                         <div class="col-sm-8">
                                                            <input type="checkbox" class="js-small f-right fields" name="create_task_reminder" id="create_task_reminder">
                                                         </div>
                                                      </div> -->
                                                      <div class="form-group row fr-float radio_bts custom_remain add_custom_reminder_label sorting_conf" id="<?php echo $this->Common_mdl->get_order_details(27); ?>" <?php $val=$this->Common_mdl->get_delete_details(27); if($val!=''){ echo $value.'!important'; } ?>>
                                                         <label class="col-sm-4 col-form-label" id="add_custom_reminder_label" name="add_custom_reminder_label"> <?php echo $this->Common_mdl->get_name_details(27); ?></label>
                                                         <div class="col-sm-8" id="add_custom_reminder">
                                                            <input type="checkbox" class="js-small f-right fields cus_reminder" name="add_custom_reminder" id="" data-id="confirm_cus" data-serid="4">
                                                            <!-- <textarea rows="3" placeholder="" name="add_custom_reminder" id="add_custom_reminder" class="fields"></textarea> -->
                                                            <!-- <a name="add_custom_reminder" id="add_custom_reminder" href="<?php echo base_url();?>user/Service_reminder_settings/1" target="_blank">Click to add Custom Reminder</a> -->
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- accordion-panel -->
                                 <!--          <div class="accordion-panel incs" style="display: none;">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">confirmation Invoice</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1">
                                                      <div class="form-group row name_fields">
                                                         <label class="col-sm-4 col-form-label">Invoice</label>
                                                         <div class="col-sm-8">
                                                            <select name="invoice_cs" id="invoice_cs">
                                                               <option value="">Select</option>
                                                               <option value="email">E-Mail</option>
                                                               <option value="sms">SMS</option>
                                                               <option value="phone">Phone</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div> -->
                                          <div class="accordion-panel">
                                             <div class="box-division03" >
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">capital</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1" id="box3">
                                                      <div class="form-group row sorting_conf text-box1" id="<?php echo $this->Common_mdl->get_order_details(19); ?>" <?php echo $this->Common_mdl->get_delete_details(19); ?>>
                                                         <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(19); ?></label>
                                                         <div class="col-sm-8 ">
                                                            <textarea rows="3" placeholder="" name="confirmation_officers" id="confirmation_officers" class="fields"></textarea>
                                                         </div>
                                                      </div>
                                                      <div class="form-group row sorting_conf text-box1" id="<?php echo $this->Common_mdl->get_order_details(20); ?>" <?php echo $this->Common_mdl->get_delete_details(20); ?>>
                                                         <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(20); ?></label>
                                                         <div class="col-sm-8">
                                                            <textarea rows="3" placeholder="" name="share_capital" id="share_capital" class="fields"></textarea>
                                                         </div>
                                                      </div>
                                                      <div class="form-group row sorting_conf text-box1" id="<?php echo $this->Common_mdl->get_order_details(21); ?>" <?php echo $this->Common_mdl->get_delete_details(21); ?>>
                                                         <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(21); ?></label>
                                                         <div class="col-sm-8">
                                                            <textarea rows="3" placeholder="" name="shareholders" id="shareholders" class="fields"></textarea>
                                                         </div>
                                                      </div>
                                                      <div class="form-group row sorting_conf text-box1" id="<?php echo $this->Common_mdl->get_order_details(22); ?>" <?php echo $this->Common_mdl->get_delete_details(22); ?>>
                                                         <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(22); ?></label>
                                                         <div class="col-sm-8">
                                                            <textarea rows="3" placeholder="" name="people_with_significant_control" id="people_with_significant_control" class="fields"></textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                         
                                       </div>   
                              </li>  <!-- confirm tab close -->
                              
                              <!-- account tab start -->
                              <li class="show-block">
                              <a href="JavaScript:void(0);" class="toggle">  Accounts</a>
                                 <div id="accounts" class="masonry-container floating_set inner-views">
                                          <div class="accordion-panel">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Important Information</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1" id="accounts_box">
                                                      <div class="form-group row name_fields accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(91); ?>" <?php echo $this->Common_mdl->get_delete_details(91); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(91); ?></label>
                                                         <div class="col-sm-8">
                                                            <input type="text" name="accounts_auth_code" id="accounts_auth_code" placeholder="" value="" class="fields accounts_auth_code">
                                                            <!-- accounts_auth_code  readonly before-->
                                                         </div>
                                                      </div>
                                                      <div class="form-group row name_fields accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(92); ?>" <?php echo $this->Common_mdl->get_delete_details(92); ?>>
                                                         <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(92); ?></label>
                                                         <div class="col-sm-8">
                                                            <input type="number" name="accounts_utr_number" id="accounts_utr_number" placeholder="" value="" class="fields">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(93); ?>" <?php echo $this->Common_mdl->get_delete_details(93); ?>>
                                                         <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(93); ?></label>
                                                         <div class="col-sm-8">
                                                            <input type="checkbox" class="js-small f-right fields" name="company_house_reminder" id="company_house_reminder">
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- accordion-panel -->
                                          <div class="accordion-panel accounts_tab_section" style="display: none">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Accounts</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1" id="accounts_box1">
                                                      <div class="form-group row name_fields date_birth accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(94); ?>" <?php echo $this->Common_mdl->get_delete_details(94); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(94); ?></label> <span class="hilight_due_date">*</span>
                                                         <div class="col-sm-8 edit-field-popup1">
                                                            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span><input type="text" name="accounts_next_made_up_to" id="next_made_up_to" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_hmrc_yearend'])){ echo date('d-m-Y',strtotime($client['crm_hmrc_yearend'])); }  ?>" class="fields edit_classname datepicker" >
                                                            <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                                               <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                                            </div>
                                                         </div>
                                                      </div>


                                                       <div class="form-group row name_fields date_birth accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(194); ?>" <?php echo $this->Common_mdl->get_delete_details(194); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(194); ?></label>
                                                         <div class="col-sm-8 edit-field-popup1">
                                                            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span><input type="text" name="accounts_last_made_up_to" id="accounts_last_made_up_to" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_accounts_last_made_up_to_date'])){ echo date('d-m-Y',strtotime($client['crm_accounts_last_made_up_to_date'])); }  ?>" class="fields edit_classname datepicker" >
                                                            <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                                               <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                                            </div>
                                                         </div>
                                                      </div>

                                                      <div class="form-group row name_fields date_birth accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(95); ?>" <?php echo $this->Common_mdl->get_delete_details(95); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(95); ?></label>
                                                         <div class="col-sm-8 edit-field-popup1">
                                                            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                           
                                                            <input type="text" name="accounts_next_due" id="next_due" placeholder="dd-mm-yyyy" value="<?php if(isset($client['crm_ch_accounts_next_due'])){ echo  date('d-m-Y',strtotime($client['crm_ch_accounts_next_due'])); }  ?>" class="fields edit_classname datepicker" >
                                                            <div class="edit-button-confim" style="<?php echo $read_edit;?>">
                                                               <p data-toggle="modal" data-target="#edit_confirmation1" class="click_code03"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> edit</p>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- accordion-panel -->
                                          <!-- <div class="clearfix"></div> -->
                                          <div class="accordion-panel enable_acc" style="display:none;">
                                             <div class="box-division03" >
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Accounts Reminders</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1" id="accounts_box2">
                                                      <div class="form-group row name_fields radio_bts">
                                                         <label class="col-sm-4 col-form-label"><a id="accounts_custom_reminder_link" name="accounts_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/2" target="_blank">As per firm setting</a></label>
                                                         <div class="col-sm-8">
                                                            <!-- <input type="hidden" name="accounts_next_reminder_date" id="accounts_next_reminder_date" placeholder="" value="" class="fields dob_picker"> -->
                                                            <input type="checkbox" class="js-small f-right fields as_per_firm" name="accounts_next_reminder_date" id="accounts_next_reminder_date"  >
                                                         </div>
                                                      </div>
                                                      <!-- <div class="form-group row radio_bts accounts_sec radio_bts" id="<?php echo $this->Common_mdl->get_order_details(97); ?>" <?php echo $this->Common_mdl->get_delete_details(98); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(98); ?></label>
                                                         <div class="col-sm-8">
                                                            <input type="checkbox" class="js-small f-right fields" name="accounts_create_task_reminder" id="accounts_create_task_reminder">
                                                         </div>
                                                      </div> -->
                                                      <div class="form-group row fr-float radio_bts twot accounts_custom_reminder_label accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(98); ?>"  <?php $v1=$this->Common_mdl->get_delete_details(98); if($v1!=''){ echo $v1.'!important'; } ?> >
                                                         <label id="accounts_custom_reminder_label" name="accounts_custom_reminder_label" class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(98); ?></label>
                                                         <div class="col-sm-8" id="accounts_custom_reminder">
                                                            <input type="checkbox" class="js-small f-right fields cus_reminder" name="accounts_custom_reminder" id="" data-id="accounts_cus" data-serid="3">
                                                            <!-- <a name="accounts_custom_reminder" id="accounts_custom_reminder" href="<?php echo base_url();?>user/Service_reminder_settings/" target="_blank">Click to add Custom Reminder</a> -->
                                                            <!-- <textarea rows="4" name="accounts_custom_reminder" id="accounts_custom_reminder" class="form-control fields"></textarea> -->
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- accordion-panel -->
                                  <!--         <div class="accordion-panel inas" style="display: none">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Accounts Invoice</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1">
                                                      <div class="form-group row name_fields">
                                                         <label class="col-sm-4 col-form-label">Invoice</label>
                                                         <div class="col-sm-8">
                                                            <select name="invoice_as" id="invoice_as">
                                                               <option value="">Select</option>
                                                               <option value="email">E-Mail</option>
                                                               <option value="sms">SMS</option>
                                                               <option value="phone">Phone</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div> -->
                                          <div class="companytax" style="display:none;">
                                             <div class="accordion-panel">
                                                <div class="box-division03">
                                                   <div class="accordion-heading" role="tab" id="headingOne">
                                                      <h3 class="card-title accordion-title">
                                                         <a class="accordion-msg">Tax Return </a>
                                                      </h3>
                                                   </div>
                                                   <div id="collapse" class="panel-collapse">
                                                      <div class="basic-info-client1" id="accounts_box3">
                                                         <div class="form-group row name_fields accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(95); ?>" <?php echo $this->Common_mdl->get_delete_details(95); ?>>
                                                            <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(95); ?></label>
                                                            <span class="hilight_due_date">*</span>
                                                            <div class="col-sm-8">
                                                               <input type="text" name="accounts_due_date_hmrc" id="accounts_due_date_hmrc" placeholder="" value="" class="fields dob_picker">
                                                            </div>
                                                         </div>
                                                         <div class="form-group row name_fields accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(100); ?>" <?php echo $this->Common_mdl->get_delete_details(100); ?>>
                                                            <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(100); ?></label>
                                                            <div class="col-sm-8">
                                                               <input type="text" name="accounts_tax_date_hmrc" id="accounts_tax_date_hmrc" placeholder="" value="" class="fields dob_picker">
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <!-- accordion-panel -->
                                             <div class="accordion-panel enable_tax" style="display:none;">
                                                <div class="box-division03">
                                                   <div class="accordion-heading" role="tab" id="headingOne">
                                                      <h3 class="card-title accordion-title">
                                                         <a class="accordion-msg">Company tax Reminders</a>
                                                      </h3>
                                                   </div>
                                                   <div id="collapse" class="panel-collapse">
                                                      <div class="basic-info-client1" id="accounts_box4">
                                                         <div class="form-group row name_fields radio_bts">
                                                            <label class="col-sm-4 col-form-label"><a id="company_custom_reminder_link" name="company_custom_reminder_link" href="<?php echo base_url(); ?>user/Service_reminder_settings/3" target="_blank">As per firm setting</a></label>
                                                            <div class="col-sm-8">
                                                               <!-- <input type="hidden" name="company_next_reminder_date" id="company_next_reminder_date" placeholder="" value="" class="fields dob_picker"> -->
                                                               <input type="checkbox" class="js-small f-right fields as_per_firm" name="company_next_reminder_date" id="company_next_reminder_date" >
                                                            </div>
                                                         </div>
                                                         <!-- <div class="form-group row radio_bts accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(102); ?>" <?php echo $this->Common_mdl->get_delete_details(102); ?>>
                                                            <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(102); ?></label>
                                                            <div class="col-sm-8">
                                                               <input type="checkbox" class="js-small f-right fields" name="company_create_task_reminder" id="company_create_task_reminder" >
                                                            </div>
                                                         </div> -->
                                                         <div class="form-group fr-float row radio_bts company_custom_reminder_label accounts_sec" id="<?php echo $this->Common_mdl->get_order_details(103); ?>" <?php $bb=$this->Common_mdl->get_delete_details(103);  if($bb!=''){ echo $bb.'!important'; } ?> >
                                                            <label id="company_custom_reminder_label" name="company_custom_reminder_label" class="col-sm-4 col-form-label "> <?php echo $this->Common_mdl->get_name_details(103); ?></label>
                                                            <div class="col-sm-8" id="company_custom_reminder">
                                                               <input type="checkbox" class="js-small f-right fields cus_reminder" name="company_custom_reminder" id="" data-id="company_cus" data-serid="5">
                                                               <!-- <textarea rows="4" name="company_custom_reminder" id="company_custom_reminder" class="form-control fields"></textarea> -->
                                                               <!--  <a name="company_custom_reminder" id="company_custom_reminder" href="<?php echo base_url(); ?>user/Service_reminder_settings/3" target="_blank">Click to add Custom Reminder</a> -->
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <!-- accordion-panel -->
                                          </div>
                                          <!-- companytax-->
                             <!--              <div class="accordion-panel inct" style="display: none">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Company tax return Invoice</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1">
                                                      <div class="form-group row name_fields">
                                                         <label class="col-sm-4 col-form-label">Invoice</label>
                                                         <div class="col-sm-8">
                                                            <select name="invoice_ct" id="invoice_ct">
                                                               <option value="">Select</option>
                                                               <option value="email">E-Mail</option>
                                                               <option value="sms">SMS</option>
                                                               <option value="phone">Phone</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div> -->
                                         
                                       </div>      
                              </li> <!-- account tab close -->
                              
                              <!-- personal-tax tab start -->
                              <li class="show-block">
                              <a href="JavaScript:void(0);" class="toggle">  Personal Tax Return</a>
                                 <div id="personal-tax-returns" class="masonry-container floating_set inner-views">
                                          <div class="grid-sizer"></div>
                                          <div class="accordion-panel">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Important Information</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1" id="personal_box">
                                                      <div class="form-group row personal_sort" id="<?php echo $this->Common_mdl->get_order_details(29); ?>"  <?php echo $this->Common_mdl->get_delete_details(29); ?>>
                                                         <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(29); ?></label>
                                                         <div class="col-sm-8">
                                                            <input type="number" name="personal_utr_number" id="personal_utr_number" class="form-control fields" value="">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row personal_sort" id="<?php echo $this->Common_mdl->get_order_details(30); ?>"  <?php echo $this->Common_mdl->get_delete_details(30); ?>>
                                                         <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(30); ?></label>
                                                         <div class="col-sm-8">
                                                            <input type="number" name="ni_number" id="ni_number" class="form-control fields" value="">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts personal_sort" id="<?php echo $this->Common_mdl->get_order_details(31); ?>"  <?php echo $this->Common_mdl->get_delete_details(31); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(31); ?></label>
                                                         <div class="col-sm-8">
                                                            <input type="checkbox" class="js-small f-right fields" name="property_income" id="property_income" data-id="property_income">
                                                         </div>
                                                      </div>



                                             
                                                  

                                                     

                                                      <div class="form-group row radio_bts personal_sort" id="<?php echo $this->Common_mdl->get_order_details(32); ?>"  <?php echo $this->Common_mdl->get_delete_details(32); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(32); ?></label>
                                                         <div class="col-sm-8">
                                                            <input type="checkbox" class="js-small f-right fields" name="additional_income" id="additional_income">
                                                         </div>
                                                      </div>
                                                       <div class="form-group row text-box1 personal_sort property_income_notes" id="<?php echo $this->Common_mdl->get_order_details(193); ?>"  <?php echo $this->Common_mdl->get_delete_details(193); ?> style="display: none;">
                                                         <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(193); ?> </label>
                                                         <div class="col-sm-8">
                                                            <textarea rows="3" placeholder="" name="property_income_notes" id="property_income_notes" class="fields"></textarea>
                                                         </div>
                                                      </div>
                                                      
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- accordion-panel -->
                                          <div class="accordion-panel">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Personal Tax Return</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1" id="personal_box1">
                                                      <div class="form-group row name_fields date_birth personal_sort" id="<?php echo $this->Common_mdl->get_order_details(33); ?>"  <?php echo $this->Common_mdl->get_delete_details(33); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(33); ?></label>
                                                         <span class="hilight_due_date">*</span>
                                                         <div class="col-sm-8"><span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                            <input type="text" name="personal_tax_return_date" id="personal_tax_return_date" placeholder="dd-mm-yyyy" value="" class="datepicker fields dob_picker">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row name_fields date_birth personal_sort" id="<?php echo $this->Common_mdl->get_order_details(34); ?>"  <?php echo $this->Common_mdl->get_delete_details(34); ?>>
                                                         <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(34); ?></label>
                                                         <div class="col-sm-8">
                                                            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span> <input type="text" name="personal_due_date_return" id="personal_due_date_return" placeholder="dd-mm-yyyy" value="" class="fields dob_picker">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row name_fields date_birth personal_sort" id="<?php echo $this->Common_mdl->get_order_details(35); ?>"  <?php echo $this->Common_mdl->get_delete_details(35); ?>>
                                                         <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(35); ?></label>
                                                         <div class="col-sm-8"><span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                            <input type="text" name="personal_due_date_online" id="personal_due_date_online" placeholder="dd-mm-yyyy" value="" class="fields dob_picker">
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- accordion-panel -->
                                          <div class="accordion-panel enable_pertax" style="display:none">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Personal tax Reminders</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1" id="personal_box2">
                                                      <div class="form-group row name_fields radio_bts ">
                                                         <label class="col-sm-4 col-form-label"><a id="personal_custom_reminder_link" name="personal_custom_reminder_link" href="<?php echo base_url(); ?>user/Service_reminder_settings/4" target="_blank">As per firm setting</a></label>
                                                         <div class="col-sm-8">
                                                            <!--   <input type="hidden" name="personal_next_reminder_date" id="personal_next_reminder_date" placeholder="" value="" class="fields dob_picker"> -->
                                                            <input type="checkbox" class="js-small f-right fields as_per_firm" name="personal_next_reminder_date" id="Personal_next_reminder_date"   >
                                                         </div>
                                                      </div>
                                                      <!-- <div class="form-group row radio_bts personal_sort" id="<?php echo $this->Common_mdl->get_order_details(37); ?>"  <?php echo $this->Common_mdl->get_delete_details(37); ?>>
                                                         <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(37); ?></label>
                                                         <div class="col-sm-8">
                                                            <input type="checkbox" class="js-small f-right fields" name="personal_task_reminder" id="personal_task_reminder">
                                                         </div>
                                                      </div> -->
                                                      <div class="form-group row fr-float radio_bts personal_custom_reminder_label personal_sort" id="<?php echo $this->Common_mdl->get_order_details(38); ?>"  <?php $tt=$this->Common_mdl->get_delete_details(38); if($tt!=''){ echo $tt.'!important'; } ?>>
                                                         <label id="personal_custom_reminder_label" name="personal_custom_reminder_label" class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(38); ?></label>
                                                         <div class="col-sm-8" id="personal_custom_reminder">
                                                            <input type="checkbox" class="js-small f-right fields cus_reminder" name="personal_custom_reminder" id="" data-id="personal_cus" data-serid="6">
                                                            <!-- <textarea rows="3" placeholder="" name="personal_custom_reminder" id="personal_custom_reminder" class="fields"></textarea> -->
                                                            <!-- <a name="personal_custom_reminder" id="personal_custom_reminder" href="<?php echo base_url(); ?>user/Service_reminder_settings/4" target="_blank">Click to add Custom Reminder</a> -->
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- accordion-panel -->
                                  <!--         <div class="accordion-panel inpt" style="display: none">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Personal Tax Invoice</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1">
                                                      <div class="form-group row name_fields">
                                                         <label class="col-sm-4 col-form-label">Invoice</label>
                                                         <div class="col-sm-8">
                                                            <select name="invoice_pt" id="invoice_pt">
                                                               <option value="">Select</option>
                                                               <option value="email">E-Mail</option>
                                                               <option value="sms">SMS</option>
                                                               <option value="phone">Phone</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div> -->
                                       
                                       </div>
                              </li> <!-- personal-tax tab close -->
                              
                           <!-- payroll tab start -->
                           <li class="show-block">
                           <a href="JavaScript:void(0);" class="toggle">  Payroll</a>
                              <div id="payroll" class="masonry-container floating_set inner-views">
                                            <div class="grid-sizer"></div>
                                          <div class="accordion-panel">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Important Information</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1" id="payroll_box">
                                                      <div class="form-group row name_fields payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(105); ?>"  <?php echo $this->Common_mdl->get_delete_details(105); ?>>
                                                         <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(105); ?></label>
                                                         <div class="col-sm-8">
                                                            <input type="number" name="payroll_acco_off_ref_no" id="payroll_acco_off_ref_no" placeholder="" value="" class="fields">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row name_fields payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(106); ?>"  <?php echo $this->Common_mdl->get_delete_details(106); ?>>
                                                         <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(106); ?></label>
                                                         <div class="col-sm-8">
                                                            <input type="number" name="paye_off_ref_no" id="paye_off_ref_no" placeholder="" value="" class="fields">
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>


                                           <!-- accordion-panel -->
                                          <div class="accordion-panel for_payroll_section" style="display: none;">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Payroll</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1" id="payroll_box1">
                                                      <div class="form-group row name_fields date_birth payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(107); ?>"  <?php echo $this->Common_mdl->get_delete_details(107); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(107); ?></label>
                                                         <div class="col-sm-8">
                                                            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span><input type="text" name="payroll_reg_date" id="datepicker1" placeholder="dd-mm-yyyy" value="" class="fields dob_picker">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(108); ?>"  <?php echo $this->Common_mdl->get_delete_details(108); ?>>
                                                         <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(108); ?></label>
                                                         <div class="col-sm-8">
                                                            <input type="number" name="no_of_employees" id="no_of_employees" value="" class="fields">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row  date_birth payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(109); ?>"  <?php echo $this->Common_mdl->get_delete_details(109); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(109); ?></label>
                                                         <div class="col-sm-8">
                                                            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                            <input class="fields dob_picker" type="text" name="first_pay_date" id="datepicker2" placeholder="dd-mm-yyyy" value=""/>
                                                         </div>
                                                      </div>
                                                      <div class="form-group row payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(110); ?>"  <?php echo $this->Common_mdl->get_delete_details(110); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(110); ?></label>
                                                         <div class="col-sm-8">
                                                            <select name="payroll_run" id="payroll_run" class="form-control fields">
                                                               <option value="">--Select--</option>
                                                               <option value="Monthly">Monthly</option>
                                                               <option value="Weekly">Weekly</option>
                                                               <option value="Forthnightly">Forthnightly</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                      <div class="form-group row  date_birth payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(111); ?>"  <?php echo $this->Common_mdl->get_delete_details(111); ?>>
                                                         <label class="col-sm-4 col-form-label">
                                                            <?php echo $this->Common_mdl->get_name_details(111); ?></label>
                                                         <span class="hilight_due_date">*</span>
                                                         <div class="col-sm-8">
                                                            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                            <input class="datepicker fields dob_picker" type="text" name="payroll_run_date" id="datepicker3" placeholder="dd-mm-yyyy" value=""/>
                                                         </div>
                                                      </div>
                                                      <div class="form-group row  date_birth payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(112); ?>"  <?php echo $this->Common_mdl->get_delete_details(112); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(112); ?></label>
                                                         <div class="col-sm-8">
                                                            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                            <input class="fields dob_picker" type="text" name="rti_deadline" id="datepicker4" placeholder="dd-mm-yyyy" value=""/>
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(113); ?>"  <?php echo $this->Common_mdl->get_delete_details(113); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(113); ?></label>
                                                         <div class="col-sm-8">
                                                            <input type="checkbox" class="js-small f-right fields" name="previous_year_require" id="previous_year_require" data-id="preyear">
                                                         </div>
                                                      </div>
                                                      <div id="enable_preyear" style="display:none;">
                                                         <div class="form-group row name_fields " id="">
                                                            <label class="col-sm-4 col-form-label">If Yes</label>
                                                            <div class="col-sm-8">
                                                               <select name="payroll_if_yes" id="payroll_if_yes" class="form-control fields">
                                                                  <option value="" selected="selected">--Select--</option>
                                                                  <option value="01">01</option>
                                                                  <option value="02">02</option>
                                                                  <option value="03">03</option>
                                                                  <option value="04">04</option>
                                                                  <option value="05">05</option>
                                                                  <option value="06">06</option>
                                                                  <option value="07">07</option>
                                                                  <option value="08">08</option>
                                                                  <option value="09">09</option>
                                                                  <option value="10">10</option>
                                                                  <option value="11">11</option>
                                                                  <option value="12">12</option>
                                                                  <option value="13">13</option>
                                                                  <option value="14">14</option>
                                                                  <option value="15">15</option>
                                                               </select>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="form-group row  date_birth payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(114); ?>"  <?php echo $this->Common_mdl->get_delete_details(114); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(114); ?></label>
                                                         <div class="col-sm-8">
                                                            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                            <input class="form-control datepicker fields dob_picker" type="text" name="paye_scheme_ceased" id="datepicker5" placeholder="dd-mm-yyyy" value=""/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>

                                            <!-- accordion-panel -->
                                          <div class="accordion-panel enable_pay" style="display:none;">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Payroll Reminders</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1" id="payroll_box2">
                                                      <div class="form-group row  date_birth radio_bts" >
                                                         <label class="col-sm-4 col-form-label"><a id="payroll_add_custom_reminder_link" name="payroll_add_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/6" target="_blank">As per firm setting</a></label>
                                                         <div class="col-sm-8">
                                                            <!-- <input type="hidden" name="payroll_next_reminder_date" id="payroll_next_reminder_date" placeholder="How many years" value="" class="fields dob_picker"> -->
                                                            <input type="checkbox" class="js-small f-right fields as_per_firm" name="Payroll_next_reminder_date" id="Payroll_next_reminder_date"  >
                                                         </div>
                                                      </div>
                                                      <!-- <div class="form-group row radio_bts payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(116); ?>"  <?php echo $this->Common_mdl->get_delete_details(116); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(116); ?></label>
                                                         <div class="col-sm-8">
                                                            <input type="checkbox" class="js-small f-right fields" name="payroll_create_task_reminder" id="payroll_create_task_reminder">
                                                         </div>
                                                      </div> -->
                                                      <div class="form-group row fr-float radio_bts payroll_add_custom_reminder_label payroll_sec" id="<?php echo $this->Common_mdl->get_order_details(117); ?>"  <?php $yy=$this->Common_mdl->get_delete_details(117); if($yy!=''){ echo $yy.'!important'; } ?>>
                                                         <label id="payroll_add_custom_reminder_label" name="payroll_add_custom_reminder_label" class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(117); ?></label>
                                                         <div class="col-sm-8" id="payroll_add_custom_reminder">
                                                            <input type="checkbox" class="js-small f-right fields cus_reminder" name="payroll_add_custom_reminder" id="" data-id="payroll_cus" data-serid="7">
                                                            <!-- <a name="payroll_add_custom_reminder" id="payroll_add_custom_reminder" href="<?php echo base_url();?>user/Service_reminder_settings/7" target="_blank">Click to add Custom Reminder</a>  -->
                                                            <!-- <textarea rows="4" name="payroll_add_custom_reminder" id="payroll_add_custom_reminder" class="form-control fields"></textarea> -->
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                           <!-- accordion-panel -->
                                   <!--        <div class="accordion-panel inpr" style="display: none">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Payroll Invoice</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1">
                                                      <div class="form-group row name_fields">
                                                         <label class="col-sm-4 col-form-label">Invoice</label>
                                                         <div class="col-sm-8">
                                                            <select name="invoice_pr" id="invoice_pr">
                                                               <option value="">Select</option>
                                                               <option value="email">E-Mail</option>
                                                               <option value="sms">SMS</option>
                                                               <option value="phone">Phone</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div> -->

                                        <!-- contratab close-->
                                             
                                       

                                           <!-- accordion-panel -->
                                           
                                             <!-- accordion-panel -->
                                          
                                          <!-- contratab close-->
                                            
                                <!--           <div class="accordion-panel incis" style="display: none">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">CIS Invoice</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1">
                                                      <div class="form-group row name_fields">
                                                         <label class="col-sm-4 col-form-label">Invoice</label>
                                                         <div class="col-sm-8">
                                                            <select name="invoice_cis" id="invoice_cis">
                                                               <option value="">Select</option>
                                                               <option value="email">E-Mail</option>
                                                               <option value="sms">SMS</option>
                                                               <option value="phone">Phone</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div> -->
                                 

                                            
                                         
                                     

                                <!--           
                                          <div class="accordion-panel incis_sub" style="display: none">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">CIS SUB Invoice</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1">
                                                      <div class="form-group row name_fields">
                                                         <label class="col-sm-4 col-form-label">Invoice</label>
                                                         <div class="col-sm-8">
                                                            <select name="invoice_sub" id="invoice_sub">
                                                               <option value="">Select</option>
                                                               <option value="email">E-Mail</option>
                                                               <option value="sms">SMS</option>
                                                               <option value="phone">Phone</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                        -->
                                    
  
                                         
                              </div>
                           </li> <!-- payroll tab close -->
                           
                           <!-- WorkPlace Pension-->
                           <li class="show-block">
                              <a href="JavaScript:void(0);" class="toggle"> WorkPlace Pension - AE</a>
                                          <div id='worktab' class="masonry-container floating_set inner-views">
                                             <div class="accordion-panel">
                                                <div class="box-division03">
                                                   <div class="accordion-heading" role="tab" id="headingOne">
                                                      <h3 class="card-title accordion-title">
                                                         <a class="accordion-msg">WorkPlace Pension - AE</a>
                                                      </h3>
                                                   </div>
                                                   <div id="collapse" class="panel-collapse">
                                                      <div class="basic-info-client1" id="workplace_sec">
                                                         <div class="form-group row  date_birth workplace" id="<?php echo $this->Common_mdl->get_order_details(118); ?>"  <?php echo $this->Common_mdl->get_delete_details(118); ?>>
                                                            <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(118); ?></label>
                                                            <div class="col-sm-8">
                                                               <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                               <input class="form-control datepicker fields dob_picker" type="text" name="staging_date" id="datepicker6" placeholder="dd-mm-yyyy" value=""/>
                                                            </div>
                                                         </div>
                                                         <div class="form-group row workplace" id="<?php echo $this->Common_mdl->get_order_details(119); ?>"  <?php echo $this->Common_mdl->get_delete_details(119); ?>>
                                                            <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(119); ?></label>
                                                            <div class="col-sm-8">
                                                               <input type="text" name="pension_id" id="pension_id" value="" class="fields">
                                                            </div>
                                                         </div>
                                                         <div class="form-group row  date_birth workplace" id="<?php echo $this->Common_mdl->get_order_details(120); ?>"  <?php echo $this->Common_mdl->get_delete_details(120); ?>>
                                                            <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(120); ?></label>
                                                            <span class="hilight_due_date">*</span>
                                                            <div class="col-sm-8">
                                                               <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                               <input class="form-control datepicker fields dob_picker" type="text" name="pension_subm_due_date" id="datepicker7" placeholder="dd-mm-yyyy" value=""/>
                                                            </div>
                                                         </div>
                                                         <div class="form-group row  date_birth workplace" id="<?php echo $this->Common_mdl->get_order_details(121); ?>"  <?php echo $this->Common_mdl->get_delete_details(121); ?>>
                                                            <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(121); ?></label>
                                                            <div class="col-sm-8">
                                                               <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                               <input class="form-control datepicker fields dob_picker" type="text" name="defer_post_upto" id="datepicker8" placeholder="dd-mm-yyyy" value=""/>
                                                            </div>
                                                         </div>
                                                         <div class="form-group row  date_birth workplace" id="<?php echo $this->Common_mdl->get_order_details(122); ?>"  <?php echo $this->Common_mdl->get_delete_details(122); ?>>
                                                            <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(122); ?></label>
                                                            <div class="col-sm-8">
                                                               <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                               <input class="form-control datepicker fields dob_picker" type="text" name="pension_regulator_date" id="datepicker9" placeholder="dd-mm-yyyy" value=""/>
                                                            </div>
                                                         </div>
                                                         <div class="form-group row  date_birth workplace" id="<?php echo $this->Common_mdl->get_order_details(123); ?>"  <?php echo $this->Common_mdl->get_delete_details(123); ?>>
                                                            <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(123); ?></label>
                                                            <div class="col-sm-8">
                                                               <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                               <input class="form-control datepicker fields dob_picker" type="text" name="paye_re_enrolment_date" id="datepicker10" placeholder="dd-mm-yyyy" value=""/>
                                                            </div>
                                                         </div>
                                                         <div class="form-group row  date_birth " >
                                                            <label class="col-sm-4 col-form-label">Declaration of Compliance Due</label>
                                                            <div class="col-sm-8">
                                                               <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                               <input class="form-control datepicker fields" type="text" name="declaration_of_compliance_due_date" id="datepicker11" placeholder="dd-mm-yyyy" value=""/>
                                                            </div>
                                                         </div>
                                                         <div class="form-group row  date_birth workplace" id="<?php echo $this->Common_mdl->get_order_details(124); ?>"  <?php echo $this->Common_mdl->get_delete_details(124); ?>>
                                                            <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(124); ?></label>
                                                            <div class="col-sm-8">
                                                               <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                               <input class="form-control datepicker fields" type="text" name="declaration_of_compliance_last_filed" id="datepicker12" placeholder="dd-mm-yyyy" value=""/>
                                                            </div>
                                                         </div>
                                                         <div class="form-group row workplace" id="<?php echo $this->Common_mdl->get_order_details(125); ?>"  <?php echo $this->Common_mdl->get_delete_details(125); ?>>
                                                            <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(125); ?></label>
                                                            <div class="col-sm-8">
                                                               <input type="text" name="paye_pension_provider" id="paye_pension_provider" value="" class="fields">
                                                            </div>
                                                         </div>
                                                         <div class="form-group row workplace" id="<?php echo $this->Common_mdl->get_order_details(126); ?>"  <?php echo $this->Common_mdl->get_delete_details(126); ?>>
                                                            <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(126); ?></label>
                                                            <div class="col-sm-8">
                                                               <input type="text" name="paye_pension_provider_userid" id="paye_pension_provider_userid" value="" class="fields">
                                                            </div>
                                                         </div>
                                                         <div class="form-group row workplace" id="<?php echo $this->Common_mdl->get_order_details(126); ?>"  <?php echo $this->Common_mdl->get_delete_details(126); ?>>
                                                            <label class="col-sm-4 col-form-label">Pension Provider Password</label>
                                                            <div class="col-sm-8">
                                                               <input type="password" name="paye_pension_provider_password" id="paye_pension_provider_password" value="" class="fields">
                                                            </div>
                                                         </div>
                                                         <div class="form-group row workplace" id="<?php echo $this->Common_mdl->get_order_details(127); ?>"  <?php echo $this->Common_mdl->get_delete_details(127); ?>>
                                                            <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(127); ?></label>
                                                            <div class="col-sm-8">
                                                               <input type="number" name="employer_contri_percentage" id="employer_contri_percentage" value="" class="fields">
                                                            </div>
                                                         </div>
                                           
                                           <div class="form-group row ">
                                                            <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(128); ?></label>
                                                            <div class="col-sm-8">
                                                               <input type="number" name="employee_contri_percentage" id="employee_contri_percentage" value="" class="fields">
                                                            </div>
                                                         </div>
                                           
                                                         <div class="form-group row text-box1" >
                                                            <label class="col-sm-4 col-form-label">Other Notes</label>
                                                            <div class="col-sm-8">
                                                               <textarea rows="3" placeholder="" name="pension_notes" id="pension_notes" class="fields"></textarea>
                                                            </div>
                                                         </div>
                                           </div>
                                                      </div>
                                                   </div>
                                                </div>
                                     
                                             <!-- accordion-panel -->
                                   <!--           <div class="accordion-panel inpn" style="display: none">
                                                <div class="box-division03">
                                                   <div class="accordion-heading" role="tab" id="headingOne">
                                                      <h3 class="card-title accordion-title">
                                                         <a class="accordion-msg">Pension Invoice</a>
                                                      </h3>
                                                   </div>
                                                   <div id="collapse" class="panel-collapse">
                                                      <div class="basic-info-client1">
                                                         <div class="form-group row name_fields">
                                                            <label class="col-sm-4 col-form-label">Invoice</label>
                                                            <div class="col-sm-8">
                                                               <select name="invoice_pn" id="invoice_pn">
                                                                  <option value="">Select</option>
                                                                  <option value="email">E-Mail</option>
                                                                  <option value="sms">SMS</option>
                                                                  <option value="phone">Phone</option>
                                                               </select>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div> -->
                                             <div class="accordion-panel enable_work" style="display:none;">
                                                <div class="box-division03">
                                                   <div class="accordion-heading" role="tab" id="headingOne">
                                                      <h3 class="card-title accordion-title">
                                                         <a class="accordion-msg">Workplace Pension Reminders</a>
                                                      </h3>
                                                   </div>
                                                   <div id="collapse" class="panel-collapse">
                                                      <div class="basic-info-client1" id="workplace_sec1">
                                                         <div class="form-group row  radio_bts date_birth ">
                                                            <label class="col-sm-4 col-form-label"><a id="pension_create_task_reminder_link" name="pension_create_task_reminder_link" href="<?php echo base_url(); ?>user/Service_reminder_settings/7" target="_blank">As per firm setting</a></label>
                                                            <div class="col-sm-8">
                                                               <!-- <input type="hidden" name="pension_next_reminder_date" id="pension_next_reminder_date" placeholder="How many years" value="" class="fields dob_picker"> -->
                                                               <input type="checkbox" class="js-small f-right fields as_per_firm" name="Pension_next_reminder_date" id="Pension_next_reminder_date" >
                                                            </div>
                                                         </div>
                                                         <!-- <div class="form-group row radio_bts workplace" id="<?php echo $this->Common_mdl->get_order_details(130); ?>"  <?php echo $this->Common_mdl->get_delete_details(130); ?>>
                                                            <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(130); ?></label>
                                                            <div class="col-sm-8">
                                                               <input type="checkbox" class="js-small f-right fields" name="pension_create_task_reminder" id="pension_create_task_reminder">
                                                            </div>
                                                         </div> -->
                                                         <div class="form-group row fr-float radio_bts pension_create_task_reminder_label workplace" id="<?php echo $this->Common_mdl->get_order_details(131); ?>"  <?php $vn=$this->Common_mdl->get_delete_details(131);  if($vn!=''){ echo $vn.'!important'; }?>>
                                                            <label id="pension_create_task_reminder_label" name="pension_create_task_reminder_label" class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(131); ?></label>
                                                            <div class="col-sm-8" id="pension_add_custom_reminder">
                                                               <input type="checkbox" class="js-small f-right fields cus_reminder" name="pension_add_custom_reminder" id="" data-id="pension_cus" data-serid="8">
                                                               <!-- <textarea rows="4" name="pension_add_custom_reminder" id="pension_add_custom_reminder" class="form-control fields"></textarea> -->
                                                               <!-- <a name="pension_add_custom_reminder" id="pension_add_custom_reminder" href="<?php echo base_url(); ?>user/Service_reminder_settings/8" target="_blank">Click to add Custom Reminder</a> -->
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <!-- accordion-panel -->
                                          </div>
                                             <!--worktab close-->
                           </li>
                           <!-- WorkPlace Pension tab close--> 
                           <!-- CIS Contractor/Sub Contractor open -->                          
                           <li class="show-block">
                              <a href="JavaScript:void(0);" class="toggle">CIS Contractor/Sub Contractor</a>
                                     <div id="contratab" class="masonry-container floating_set inner-views">
                                             <div class="accordion-panel">
                                                <div class="box-division03">
                                                   <div class="accordion-heading" role="tab" id="headingOne">
                                                      <h3 class="card-title accordion-title">
                                                         <a class="accordion-msg">  CIS Contractor/Sub Contractor       </a>
                                                      </h3>
                                                   </div>
                                                   <div id="collapse" class="panel-collapse">
                                                      <div class="basic-info-client1">
                                                         <div class="form-group row name_fields radio_bts">
                                                            <label class="col-sm-4 col-form-label">C.I.S Contractor</label>
                                                            <div class="col-sm-8">
                                                               <input type="checkbox" class="js-small f-right fields" name="cis_contractor" id="cis_contractor">
                                                            </div>
                                                         </div>
                                                         <div class="form-group row  date_birth">
                                                            <label class="col-sm-4 col-form-label">Start Date</label>
                                                            <span class="hilight_due_date">*</span>
                                                            <div class="col-sm-8">
                                                               <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                               <input class="form-control datepicker fields dob_picker" type="text" name="cis_contractor_start_date" id="datepicker14" placeholder="dd-mm-yyyy" value=""/>
                                                            </div>
                                                         </div>
                                                         <div class="form-group row text-box1">
                                                            <label class="col-sm-4 col-form-label">C.I.S Scheme</label>
                                                            <div class="col-sm-8">
                                                               <textarea rows="3" placeholder="" name="cis_scheme_notes" id="cis_scheme_notes" class="fields"></textarea>
                                                            </div>
                                                         </div>
                                                         <div class="form-group row name_fields radio_bts">
                                                            <label class="col-sm-4 col-form-label">CIS Subcontractor</label>
                                                            <div class="col-sm-8">
                                                               <input type="checkbox" name="cis_subcontractor" id="cis_subcontractor" class="js-small f-right fields">
                                                            </div>
                                                         </div>
                                                         <div class="form-group row  date_birth">
                                                            <label class="col-sm-4 col-form-label">Start Date</label>
                                                            <span class="hilight_due_date">*</span>
                                                            <div class="col-sm-8">
                                                               <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                               <input class="form-control datepicker fields dob_picker" type="text" name="cis_subcontractor_start_date" id="datepicker15" placeholder="dd-mm-yyyy" value=""/>
                                                            </div>
                                                         </div>
                                                         <div class="form-group row text-box1">
                                                            <label class="col-sm-4 col-form-label">C.I.S Scheme</label>
                                                            <div class="col-sm-8">
                                                               <textarea rows="3" placeholder="" name="cis_subcontractor_scheme_notes" id="cis_subcontractor_scheme_notes" class="fields"></textarea>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <!-- accordion-panel -->

                                             <!--id  is dumb <div class="accordion-panel enable_cis"  style="display:none">
                                                <div class="box-division03">
                                                   <div class="accordion-heading" role="tab" id="headingOne">
                                                      <h3 class="card-title accordion-title">
                                                         <a class="accordion-msg">CIS Reminders</a>
                                                      </h3>
                                                   </div>
                                                   <div id="collapse" class="panel-collapse">
                                                      <div class="basic-info-client1">
                                                         <div class="form-group row  date_birth radio_bts">
                                                            <label class="col-sm-4 col-form-label"><a id="cis_add_custom_reminder_link" name="cis_add_custom_reminder_link" href="<?php echo base_url(); ?>user/Service_reminder_settings/10" target="_blank">As per firm setting</a></label>
                                                            <div class="col-sm-8">
                                                               <input type="hidden" name="cis_next_reminder_date" id="cis_next_reminder_date" placeholder="How many years" value="" class="fields dob_picker">
                                                               <input type="checkbox" class="js-small f-right fields as_per_firm" name="Cis_next_reminder_date" id="Cis_next_reminder_date"  checked="checked" >
                                                            </div>
                                                         </div>
                                                         <div class="form-group row radio_bts" style="">
                                                            <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                                                            <div class="col-sm-8">
                                                               <input type="checkbox" class="js-small f-right fields" name="cis_create_task_reminder" id="cis_create_task_reminder">
                                                            </div>
                                                         </div>
                                                         <div class="form-group row fr-float radio_bts cis_add_custom_reminder_label" style="display:none;">
                                                            <label id="cis_add_custom_reminder_label" name="cis_add_custom_reminder_label" class="col-sm-4 col-form-label">Add Custom Reminder</label>
                                                            <div class="col-sm-8" id="cis_add_custom_reminder">
                                                               <input type="checkbox" class="js-small f-right fields" name="cis_add_custom_reminder" id="" data-id="cis_cus" data-serid="10">
                                                              
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div> -->
                                             <!-- accordion-panel -->
                                               <div class="accordion-panel enable_cis"  style="display:none">
                                                <div class="box-division03">
                                                   <div class="accordion-heading" role="tab" id="headingOne">
                                                      <h3 class="card-title accordion-title">
                                                         <a class="accordion-msg">CIS Reminders</a>
                                                      </h3>
                                                   </div>
                                                   <div id="collapse" class="panel-collapse">
                                                      <div class="basic-info-client1">
                                                         <div class="form-group row  date_birth radio_bts">
                                                            <label class="col-sm-4 col-form-label"><a id="cis_add_custom_reminder_link" name="cis_add_custom_reminder_link" href="<?php echo base_url(); ?>user/Service_reminder_settings/8" target="_blank">As per firm setting</a></label>
                                                            <div class="col-sm-8">
                                                               <input type="hidden" name="cis_next_reminder_date" id="cis_next_reminder_date" placeholder="How many years" value="" class="fields dob_picker">
                                                               <input type="checkbox" class="js-small f-right fields as_per_firm" name="Cis_next_reminder_date" id="Cis_next_reminder_date">
                                                            </div>
                                                         </div>
                                                         <!-- <div class="form-group row radio_bts" style="">
                                                            <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                                                            <div class="col-sm-8">
                                                               <input type="checkbox" class="js-small f-right fields" name="cis_create_task_reminder" id="cis_create_task_reminder">
                                                            </div>
                                                         </div> -->
                                                         <div class="form-group row fr-float radio_bts cis_add_custom_reminder_label">
                                                            <label id="cis_add_custom_reminder_label" name="cis_add_custom_reminder_label" class="col-sm-4 col-form-label">Add Custom Reminder</label>
                                                            <div class="col-sm-8" id="cis_add_custom_reminder">
                                                               <input type="checkbox" class="js-small f-right fields cus_reminder" name="cis_add_custom_reminder" id="" data-id="cis_cus" data-serid="10">
                                                               <!-- <textarea rows="4" name="cis_add_custom_reminder" id="cis_add_custom_reminder" class="form-control fields"></textarea> -->
                                                               <!-- <a name="cis_add_custom_reminder" id="cis_add_custom_reminder" href="<?php echo base_url(); ?>user/Service_reminder_settings/10" target="_blank">Click to add Custom Reminder</a> -->
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          <div class="accordion-panel enable_cissub" style="display:none;">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">CIS SUB Reminders</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1">
                                                      <div class="form-group row  date_birth radio_bts">
                                                         <label class="col-sm-4 col-form-label"><a id="cissub_add_custom_reminder_link" name="cissub_add_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/9" target="_blank">As per firm setting</a></label>
                                                         <div class="col-sm-8">
                                                            <!--<input type="text" name="payroll_next_reminder_date" id="payroll_next_reminder_date" placeholder="How many years" value="" class="fields dob_picker"> -->
                                                            <input type="checkbox" class="js-small f-right fields as_per_firm" name="cissub_next_reminder_date" id="cissub_next_reminder_date">
                                                         </div>
                                                      </div>
                                                      <!-- <div class="form-group row radio_bts">
                                                         <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                                                         <div class="col-sm-8">
                                                            <input type="checkbox" class="js-small f-right fields" name="cissub_create_task_reminder" id="cissub_create_task_reminder">
                                                         </div>
                                                      </div> -->
                                                      <div class="form-group row fr-float radio_bts cissub_add_custom_reminder_label">
                                                         <label id="cissub_add_custom_reminder_label" name="cissub_add_custom_reminder_label" class="col-sm-4 col-form-label">Add Custom Reminder</label>
                                                         <div class="col-sm-8" id="cissub_add_custom_reminder">
                                                            <input type="checkbox" class="js-small f-right fields cus_reminder" name="cissub_add_custom_reminder" id="" data-id="cissub_cus" data-serid="11">
                                                            <!-- <textarea rows="4" name="payroll_add_custom_reminder" id="payroll_add_custom_reminder" class="form-control fields"></textarea> -->
                                                            <!-- <a name="cissub_add_custom_reminder" id="cissub_add_custom_reminder" href="<?php echo base_url();?>user/Service_reminder_settings/11" target="_blank">Click to add Custom Reminder</a> -->
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          </div>
                           </li>
                           <!-- CIS Contractor/Sub Contractor close -->     
                           <!--  -->                     
                           <li class="show-block">
                              <a href="JavaScript:void(0);" class="toggle">P11D</a>
                                                                        <div id="p11dtab" class="masonry-container floating_set inner-views">
                                             <div class="accordion-panel">
                                                <div class="box-division03">
                                                   <div class="accordion-heading" role="tab" id="headingOne">
                                                      <h3 class="card-title accordion-title">
                                                         <a class="accordion-msg">P11D</a>
                                                      </h3>
                                                   </div>
                                                   <div id="collapse" class="panel-collapse">
                                                      <div class="basic-info-client1" id="p11d_sec">
                                                         <div class="form-group row  date_birth p11d" id="<?php echo $this->Common_mdl->get_order_details(132); ?>"  <?php echo $this->Common_mdl->get_delete_details(132); ?>>
                                                            <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(132); ?></label>
                                                            <div class="col-sm-8">
                                                               <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                               <input class="form-control datepicker fields dob_picker" type="text" name="p11d_start_date" id="datepicker17" placeholder="dd-mm-yyyy" value=""/>
                                                            </div>
                                                         </div>
                                                         <div class="form-group row  date_birth p11d" id="<?php echo $this->Common_mdl->get_order_details(133); ?>"  <?php echo $this->Common_mdl->get_delete_details(133); ?>>
                                                            <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(133); ?></label>
                                                            <div class="col-sm-8">
                                                               <input type="number" name="p11d_todo" id="p11d_todo" placeholder="" value="" class="fields ">
                                                            </div>
                                                         </div>
                                                         <div class="form-group row  date_birth p11d" id="<?php echo $this->Common_mdl->get_order_details(134); ?>"  <?php echo $this->Common_mdl->get_delete_details(134); ?>>
                                                            <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(134); ?></label>
                                                            <div class="col-sm-8">
                                                               <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                               <input class="form-control datepicker fields dob_picker" type="text" name="p11d_first_benefit_date" id="datepicker18" placeholder="dd-mm-yyyy" value=""/>
                                                            </div>
                                                         </div>
                                                         <div class="form-group row  date_birth p11d" id="<?php echo $this->Common_mdl->get_order_details(135); ?>"  <?php echo $this->Common_mdl->get_delete_details(135); ?>>
                                                            <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(135); ?></label>
                                                            <span class="hilight_due_date">*</span>
                                                            <div class="col-sm-8">
                                                               <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                               <input type="text" name="p11d_due_date" id="p11d_due_date" placeholder="dd-mm-yyyy" value="" class="datepicker fields dob_picker">
                                                            </div>
                                                         </div>
                                                         <div class="form-group row radio_bts p11d" id="<?php echo $this->Common_mdl->get_order_details(136); ?>"  <?php echo $this->Common_mdl->get_delete_details(136); ?>>
                                                            <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(136); ?></label>
                                                            <div class="col-sm-8">
                                                               <input type="checkbox" class="js-small f-right fields" name="p11d_previous_year_require" id="p11d_previous_year_require">
                                                            </div>
                                                         </div>
                                                         <div class="form-group row name_fields p11d" id="<?php echo $this->Common_mdl->get_order_details(137); ?>"  <?php echo $this->Common_mdl->get_delete_details(137); ?>>
                                                            <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(137); ?></label>
                                                            <div class="col-sm-8">
                                                               <input type="text" name="p11d_payroll_if_yes" id="p11d_payroll_if_yes" placeholder="How many years" value="" class="fields">
                                                            </div>
                                                         </div>
                                                         <div class="form-group row  date_birth p11d" id="<?php echo $this->Common_mdl->get_order_details(138); ?>"  <?php echo $this->Common_mdl->get_delete_details(138); ?>>
                                                            <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(138); ?></label>
                                                            <div class="col-sm-8">
                                                               <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                               <input class="form-control datepicker fields dob_picker" type="text" name="p11d_paye_scheme_ceased" id="datepicker20" placeholder="dd-mm-yyyy" value=""/>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <!-- accordion-panel -->
                                 <!--             <div class="accordion-panel inp11d" style="display: none">
                                                <div class="box-division03">
                                                   <div class="accordion-heading" role="tab" id="headingOne">
                                                      <h3 class="card-title accordion-title">
                                                         <a class="accordion-msg">P11d Invoice</a>
                                                      </h3>
                                                   </div>
                                                   <div id="collapse" class="panel-collapse">
                                                      <div class="basic-info-client1" >
                                                         <div class="form-group row name_fields">
                                                            <label class="col-sm-4 col-form-label">Invoice</label>
                                                            <div class="col-sm-8">
                                                               <select name="invoice_p11d" id="invoice_p11d">
                                                                  <option value="">Select</option>
                                                                  <option value="email">E-Mail</option>
                                                                  <option value="sms">SMS</option>
                                                                  <option value="phone">Phone</option>
                                                               </select>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div> -->
                                             <div class="accordion-panel enable_plld" style="display:none;">
                                                <div class="box-division03">
                                                   <div class="accordion-heading" role="tab" id="headingOne">
                                                      <h3 class="card-title accordion-title">
                                                         <a class="accordion-msg">P11D Reminders</a>
                                                      </h3>
                                                   </div>
                                                   <div id="collapse" class="panel-collapse">
                                                      <div class="basic-info-client1" id="p11d_sec1">
                                                         <div class="form-group row  date_birth radio_bts">
                                                            <label class="col-sm-4 col-form-label"><a id="p11d_add_custom_reminder_link" name="p11d_add_custom_reminder_link" href="<?php echo base_url(); ?>user/Service_reminder_settings/11" target="_blank">As per firm setting</a></label>
                                                            <div class="col-sm-8">
                                                               <input type="hidden" name="p11d_next_reminder_date" id="p11d_next_reminder_date" placeholder="" value="" class="fields dob_picker">
                                                               <input type="checkbox" class="js-small f-right fields as_per_firm" name="P11d_next_reminder_date" id="P11d_next_reminder_date" 
                                                                  >
                                                            </div>
                                                         </div>
                                                        <!--  <div class="form-group row radio_bts " id="">
                                                            <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                                                            <div class="col-sm-8">
                                                               <input type="checkbox" class="js-small f-right fields" name="p11d_create_task_reminder" id="p11d_create_task_reminder">
                                                            </div>
                                                         </div> -->
                                                         <div class="form-group row fr-float radio_bts p11d_add_custom_reminder_label ">
                                                            <label id="p11d_add_custom_reminder_label" name="p11d_add_custom_reminder_label" class="col-sm-4 col-form-label">Add Custom Reminder</label>
                                                            <div class="col-sm-8" id="p11d_add_custom_reminder">
                                                               <input type="checkbox" class="js-small f-right fields cus_reminder" name="p11d_add_custom_reminder" id="" data-id="p11d_cus" data-serid="12">
                                                               <!-- <textarea rows="4" name="p11d_add_custom_reminder" id="p11d_add_custom_reminder" class="form-control fields"></textarea> -->
                                                               <!-- <a name="p11d_add_custom_reminder" id="p11d_add_custom_reminder" href="<?php echo base_url(); ?>/user/Service_reminder_settings/12" target="_blank">Click to add Custom Reminder</a> -->
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             
                                          </div>

                           </li>
                           <!-- P11D close -->
                           <!-- vat returns tab start -->
                           <li class="show-block">
                           <a href="JavaScript:void(0);" class="toggle"> VAT Returns </a>
                              <div id="vat-Returns" class="masonry-container floating_set inner-views">
                                          <div class="grid-sizer"></div>
                                          <div class="accordion-panel">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Important Information</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1">
                                                      <div class="form-group row name_fields"  <?php echo $this->Common_mdl->get_delete_details(40); ?>>
                                                         <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(40); ?></label>
                                                         <div class="col-sm-8">
                                                            <input type="number" name="vat_number_one" id="vat_number_one" placeholder="" value="" class="fields">
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- accordion-panel -->
                                          <div class="accordion-panel">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">VAT</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1" id="vat_box">
                                                      <div class="form-group row  date_birth vat_sort" id="<?php echo $this->Common_mdl->get_order_details(41); ?>"  <?php echo $this->Common_mdl->get_delete_details(41); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(41); ?></label>
                                                         <div class="col-sm-8">
                                                            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                            <input class="form-control datepicker fields dob_picker" type="text" name="vat_registration_date" id="datepicker21" placeholder="dd-mm-yyyy" value=""/>
                                                         </div>
                                                      </div>
                                                      <div class="form-group row vat_sort" id="<?php echo $this->Common_mdl->get_order_details(42); ?>"  <?php echo $this->Common_mdl->get_delete_details(42); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(42); ?></label>
                                                         <div class="col-sm-8">
                                                            <select name="vat_frequency" id="vat_frequency" class="form-control fields">
                                                               <option value="" selected="selected">--Select--</option>
                                                               <option value="Monthly">Monthly</option>
                                                               <option value="Quarterly">Quarterly</option>
                                                               <option value="Yearly">Yearly</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                      <div class="form-group row help_icon date_birth vat_sort" id="<?php echo $this->Common_mdl->get_order_details(141); ?>">
                                                         <label class="col-sm-4 col-form-label">VAT Quarter End Date</label>
                                                         <div class="col-sm-8">
                                                            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                            <input class="form-control datepicker fields dob_picker" name="vat_quater_end_date" id="datepicker22" type="text" placeholder="dd-mm-yyyy" value=""/>
                                                         </div>
                                                      </div>
                                                      <div class="form-group row vat_sort" id="<?php echo $this->Common_mdl->get_order_details(142); ?>"  <?php echo $this->Common_mdl->get_delete_details(142); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(142); ?></label>
                                                         <span class="hilight_due_date">*</span>
                                                         <div class="col-sm-8">
                                                            <select name="vat_quarters" id="vat_quarters" class="form-control fields">
                                                               <option value="">--Select--</option>
                                                               <option value="Jan/Apr/Jul/Oct">Jan/Apr/Jul/Oct</option>
                                                               <option value="Feb/May/Aug/Nov">Feb/May/Aug/Nov</option>
                                                               <option value="Mar/Jun/Sep/Dec">Mar/Jun/Sep/Dec</option>
                                                               <option value="Monthly">Monthly</option>
                                                               <option value="Annual End Jan">Annual End Jan</option>
                                                               <option value="Annual End Feb">Annual End Feb</option>
                                                               <option value="Annual End Mar">Annual End Mar</option>
                                                               <option value="Annual End Apr">Annual End Apr</option>
                                                               <option value="Annual End May">Annual End May</option>
                                                               <option value="Annual End Jun">Annual End Jun</option>
                                                               <option value="Annual End Jul">Annual End Jul</option>
                                                               <option value="Annual End Aug">Annual End Aug</option>
                                                               <option value="Annual End Sep">Annual End Sep</option>
                                                               <option value="Annual End Oct">Annual End Oct</option>
                                                               <option value="Annual End Nov">Annual End Nov</option>
                                                               <option value="Annual End Dec">Annual End Dec</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                      <div class="form-group row help_icon date_birth vat_sort" id="<?php echo $this->Common_mdl->get_order_details(143); ?>"  <?php echo $this->Common_mdl->get_delete_details(143); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(143); ?></label>
                                                         <div class="col-sm-8">
                                                            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                            <input class="form-control datepicker fields dob_picker" name="vat_due_date" id="datepicker24" type="text" placeholder="dd-mm-yyyy" value=""/>
                                                         </div>
                                                      </div>
                                                      <div class="form-group row vat_sort" id="<?php echo $this->Common_mdl->get_order_details(144); ?>"  <?php echo $this->Common_mdl->get_delete_details(144); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(144); ?></label>
                                                         <div class="col-sm-8">
                                                            <select name="vat_scheme" id="vat_scheme" class="form-control fields">
                                                               <option value="">--Select--</option>
                                                               <option value="Standard Rate">Standard Rate</option>
                                                               <option value="Flat Rate Scheme">Flat Rate Scheme</option>
                                                               <option value="Marginal Scheme">Marginal Scheme</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                      <div class="form-group row vat_sort" id="<?php echo $this->Common_mdl->get_order_details(43); ?>"  <?php echo $this->Common_mdl->get_delete_details(43); ?>>
                                                         <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(43); ?></label>
                                                         <div class="col-sm-8">
                                                            <input type="text" name="flat_rate_category" id="flat_rate_category" placeholder="" value="" class="fields">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row vat_sort" id="<?php echo $this->Common_mdl->get_order_details(44); ?>"  <?php echo $this->Common_mdl->get_delete_details(44); ?>>
                                                         <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(44); ?></label>
                                                         <div class="col-sm-8">
                                                            <input type="number" name="flat_rate_percentage" id="flat_rate_percentage" placeholder="" value="" class="fields">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row name_fields vat_sort radio_bts" id="<?php echo $this->Common_mdl->get_order_details(45); ?>"  <?php echo $this->Common_mdl->get_delete_details(45); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(45); ?></label>
                                                         <div class="col-sm-8">
                                                            <input type="checkbox" class="js-small f-right fields" name="direct_debit" id="direct_debit">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row name_fields vat_sort radio_bts" id="<?php echo $this->Common_mdl->get_order_details(46); ?>"  <?php echo $this->Common_mdl->get_delete_details(46); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(46); ?></label>
                                                         <div class="col-sm-8">
                                                            <input type="checkbox" class="js-small f-right fields" name="annual_accounting_scheme" id="annual_accounting_scheme">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row vat_sort" id="<?php echo $this->Common_mdl->get_order_details(47); ?>"  <?php echo $this->Common_mdl->get_delete_details(47); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(47); ?></label>
                                                         <div class="col-sm-8">
                                                            <select name="box5_of_last_quarter_submitted" id="box5_of_last_quarter_submitted" class="form-control fields">
                                                               <option value="">--Select--</option>
                                                               <option value="01">01</option>
                                                               <option value="02">02</option>
                                                               <option value="03">03</option>
                                                               <option value="04">04</option>
                                                               <option value="05">05</option>
                                                               <option value="06">06</option>
                                                               <option value="07">07</option>
                                                               <option value="08">08</option>
                                                               <option value="09">09</option>
                                                               <option value="10">10</option>
                                                               <option value="11">11</option>
                                                               <option value="12">12</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                      <div class="form-group row vat_sort text-box1" id="<?php echo $this->Common_mdl->get_order_details(48); ?>"  <?php echo $this->Common_mdl->get_delete_details(48); ?>>
                                                         <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(48); ?></label>
                                                         <div class="col-sm-8">
                                                            <textarea rows="3" placeholder="" class="fields" name="vat_address" id="vat_address"></textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- accordion-panel -->
                                          <div class="accordion-panel enable_vat" style="display:none;">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">VAT Reminders</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1" id="vat_box1">
                                                      <div class="form-group row  date_birth radio_bts">
                                                         <label class="col-sm-4 col-form-label"><a id="vat_add_custom_reminder_link" name="vat_add_custom_reminder_link" href="<?php echo base_url(); ?>user/Service_reminder_settings/5" target="_blank">As per firm setting</a></label>
                                                         <div class="col-sm-8">
                                                            <input type="hidden" name="vat_next_reminder_date" id="vat_next_reminder_date" placeholder="" value="" class="fields dob_picker">
                                                            <input type="checkbox" class="js-small f-right fields as_per_firm" name="vat_next_reminder_date" id="Vat_next_reminder_date">
                                                         </div>
                                                      </div>
                                                      <!-- <div class="form-group row radio_bts vat_sort" id="<?php echo $this->Common_mdl->get_order_details(145); ?>">
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(145); ?></label>
                                                         <div class="col-sm-8">
                                                            <input type="checkbox" class="js-small f-right fields" name="vat_create_task_reminder" id="vat_create_task_reminder">
                                                         </div>
                                                      </div> -->
                                                      <div class="form-group row fr-float radio_bts vat_add_custom_reminder_label vat_sort" id="<?php echo $this->Common_mdl->get_order_details(146); ?>">
                                                         <label id="vat_add_custom_reminder_label" name="vat_add_custom_reminder_label" class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(146); ?></label>
                                                         <div class="col-sm-8" id="vat_add_custom_reminder">
                                                            <input type="checkbox" class="js-small f-right fields cus_reminder" name="vat_add_custom_reminder" id="" data-id="vat_cus" data-serid="15">
                                                            <!--  <textarea rows="3" placeholder="" name="vat_add_custom_reminder" id="vat_add_custom_reminder" class="fields"></textarea> -->
                                                            <!-- <a name="vat_add_custom_reminder" id="vat_add_custom_reminder" href="<?php echo base_url(); ?>user/Service_reminder_settings/15" target="_blank">Click to add Custom Reminder</a> -->
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                         
                                    <!--       <div class="accordion-panel invt" style="display: none">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Invoice</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1">
                                                      <div class="form-group row name_fields">
                                                         <label class="col-sm-4 col-form-label">Invoice</label>
                                                         <div class="col-sm-8">
                                                            <select name="invoice_vt" id="invoice_vt">
                                                               <option value="">Select</option>
                                                               <option value="email">E-Mail</option>
                                                               <option value="sms">SMS</option>
                                                               <option value="phone">Phone</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div> -->
                                       </div>
                           </li> <!-- vat return tab close -->
                           
                           <!-- management-a/c tab start -->
                           <li class="show-block">
                           <a href="JavaScript:void(0);" class="toggle">  Management Accounts  </a>
                              <div id="management-account" class="masonry-container floating_set inner-views">
                                          <div class="bookkeeptab" style="display:none;">
                                             <div class="accordion-panel">
                                                <div class="box-division03">
                                                   <div class="accordion-heading" role="tab" id="headingOne">
                                                      <h3 class="card-title accordion-title">
                                                         <a class="accordion-msg">Bookkeeping</a>
                                                      </h3>
                                                   </div>
                                                   <div id="collapse" class="panel-collapse">
                                                      <div class="basic-info-client1" id="management_box1">
                                                         <div class="form-group row management" id="<?php echo $this->Common_mdl->get_order_details(49); ?>"  <?php echo $this->Common_mdl->get_delete_details(49); ?>>
                                                            <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(49); ?></label>
                                                            <div class="col-sm-8">
                                                               <select name="bookkeeping" id="bookkeeping" class="form-control fields">
                                                                  <option value="">--Select--</option>
                                                                  <option value="Weekly">Weekly</option>
                                                                  <option value="Monthly">Monthly</option>
                                                                  <option value="Quarterly">Quarterly</option>
                                                                  <option value="Annually">Annually</option>
                                                               </select>
                                                            </div>
                                                         </div>
                                                         <div class="form-group row help_icon date_birth management" id="<?php echo $this->Common_mdl->get_order_details(50); ?>"  <?php echo $this->Common_mdl->get_delete_details(50); ?>>
                                                            <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(50); ?></label>
                                                            <span class="hilight_due_date">*</span>
                                                            <div class="col-sm-8">
                                                               <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                               <input class="form-control datepicker fields dob_picker" name="next_booking_date" id="datepicker26" type="text" placeholder="dd-mm-yyyy" value=""/>
                                                            </div>
                                                         </div>
                                                         <div class="form-group row management" id="<?php echo $this->Common_mdl->get_order_details(51); ?>"  <?php echo $this->Common_mdl->get_delete_details(51); ?>>
                                                            <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(51); ?></label>
                                                            <div class="col-sm-8">
                                                               <select name="method_bookkeeping" id="method_bookkeeping" class="form-control fields">
                                                                  <option value="">--Select--</option>
                                                                  <option value="Excel">Excel</option>
                                                                  <option value="Software">Software</option>
                                                               </select>
                                                            </div>
                                                         </div>
                                                         <div class="form-group row management" id="<?php echo $this->Common_mdl->get_order_details(52); ?>"  <?php echo $this->Common_mdl->get_delete_details(52); ?>>
                                                            <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(52); ?></label>
                                                            <div class="col-sm-8">
                                                               <select name="client_provide_record" id="client_provide_record" class="form-control fields">
                                                                  <option value="">--Select--</option>
                                                                  <option value="Email/DropBox">Email/DropBox</option>
                                                                  <option value="Google/Online Portal">Google/Online Portal</option>
                                                               </select>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <!-- accordion-panel -->
                                             <div class="accordion-panel enable_book" style="display:none">
                                                <div class="box-division03">
                                                   <div class="accordion-heading" role="tab" id="headingOne">
                                                      <h3 class="card-title accordion-title">
                                                         <a class="accordion-msg">Book keeping Reminders</a>
                                                      </h3>
                                                   </div>
                                                   <div id="collapse" class="panel-collapse">
                                                      <div class="basic-info-client1" id="management_box2">
                                                         <div class="form-group row  date_birth radio_bts">
                                                            <label class="col-sm-4 col-form-label"><a id="bookkeep_add_custom_reminder_link" name="bookkeep_add_custom_reminder_link" href="<?php echo base_url(); ?>/user/Service_reminder_settings/10" target="_blank">As per firm setting</a></label>
                                                            <div class="col-sm-8">
                                                               <input type="hidden" name="bookkeep_next_reminder_date" id="bookkeep_next_reminder_date" placeholder="" value="" class="fields dob_picker">
                                                               <input type="checkbox" class="js-small f-right fields as_per_firm" name="bookkeep_next_reminder_date" id="bookkeep_next_reminder_date"  >
                                                            </div>
                                                         </div>
                                                         <!-- <div class="form-group row radio_bts management" id="<?php echo $this->Common_mdl->get_order_details(147); ?>"  <?php echo $this->Common_mdl->get_delete_details(147); ?>>
                                                            <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(147); ?></label>
                                                            <div class="col-sm-8">
                                                               <input type="checkbox" class="js-small f-right fields" name="bookkeep_create_task_reminder" id="bookkeep_create_task_reminder">
                                                            </div>
                                                         </div> -->
                                                         <div class="form-group row fr-float radio_bts bookkeep_add_custom_reminder_label management" id="<?php echo $this->Common_mdl->get_order_details(148); ?>" <?php $hb=$this->Common_mdl->get_delete_details(148); if($hb!=''){ echo $hb.'!important';} ?> >
                                                            <label id="bookkeep_add_custom_reminder_label" name="bookkeep_add_custom_reminder_label" class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(148); ?></label>
                                                            <div class="col-sm-8" id="bookkeep_add_custom_reminder">
                                                               <input type="checkbox" class="js-small f-right fields cus_reminder" name="bookkeep_add_custom_reminder" id="" data-id="bookkeep_cus" data-serid="13">
                                                               <!-- <textarea rows="3" placeholder="" name="bookkeep_add_custom_reminder" id="bookkeep_add_custom_reminder" class="fields"></textarea> -->
                                                               <!-- <a name="bookkeep_add_custom_reminder" id="bookkeep_add_custom_reminder" href="<?php echo base_url(); ?>/user/Service_reminder_settings/13" target="_blank">Click to add Custom Reminder</a> -->
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <!-- accordion-panel -->
                                          </div>
                                          <!-- bookkeep tab close-->
                                          <div class="accordion-panel for_management_account" style="display: none;" >
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Management Accounts</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1" id="management_account">
                                                      <div class="form-group row management" id="<?php echo $this->Common_mdl->get_order_details(53); ?>"  <?php echo $this->Common_mdl->get_delete_details(53); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(53); ?></label>
                                                         <div class="col-sm-8">
                                                            <select name="manage_acc_fre" id="manage_acc_fre" class="form-control fields">
                                                               <option value="">--Select--</option>
                                                               <option value="Weekly">Weekly</option>
                                                               <option value="Monthly">Monthly</option>
                                                               <option value="Quarterly">Quarterly</option>
                                                               <option value="Annually">Annually</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                      <div class="form-group row help_icon date_birth management" id="<?php echo $this->Common_mdl->get_order_details(54); ?>"  <?php echo $this->Common_mdl->get_delete_details(54); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(54); ?></label>
                                                         <span class="hilight_due_date">*</span>
                                                         <div class="col-sm-8">
                                                            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                            <input class="form-control datepicker fields dob_picker" name="next_manage_acc_date" id="datepicker28" type="text" placeholder="dd-mm-yyyy" value=""/>
                                                         </div>
                                                      </div>
                                                      <div class="form-group row management" id="<?php echo $this->Common_mdl->get_order_details(55); ?>" <?php echo $this->Common_mdl->get_delete_details(55); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(55); ?></label>
                                                         <div class="col-sm-8">
                                                            <select name="manage_method_bookkeeping" id="manage_method_bookkeeping" class="form-control fields">
                                                               <option value="">--Select--</option>
                                                               <option value="Excel">Excel</option>
                                                               <option value="Software">Software</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                      <div class="form-group row management" id="<?php echo $this->Common_mdl->get_order_details(56); ?>"  <?php echo $this->Common_mdl->get_delete_details(56); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(56); ?></label>
                                                         <div class="col-sm-8">
                                                            <select name="manage_client_provide_record" id="manage_client_provide_record" class="form-control fields">
                                                               <option value="">--Select--</option>
                                                               <option value="Email/DropBox">Email/DropBox</option>
                                                               <option value="Google/Online Portal">Google/Online Portal</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- accordion-panel -->
                                     <!--      <div class="accordion-panel inbook" style="display: none">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Bookkeep Invoice</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1">
                                                      <div class="form-group row name_fields">
                                                         <label class="col-sm-4 col-form-label">Invoice</label>
                                                         <div class="col-sm-8">
                                                            <select name="invoice_book" id="invoice_book">
                                                               <option value="">Select</option>
                                                               <option value="email">E-Mail</option>
                                                               <option value="sms">SMS</option>
                                                               <option value="phone">Phone</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div> -->
                                          <div class="accordion-panel enable_management" style="display:none">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Management Reminders</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1" id="management_account1">
                                                      <div class="form-group row  date_birth radio_bts">
                                                         <label class="col-sm-4 col-form-label"><a id="manage_add_custom_reminder_link" name="manage_add_custom_reminder_link" href="<?php echo base_url(); ?>/user/Service_reminder_settings/12" target="_blank">As per firm setting</a></label>
                                                         <div class="col-sm-8">
                                                            <input type="hidden" name="manage_next_reminder_date" id="manage_next_reminder_date" placeholder="" value="" class="fields dob_picker">
                                                            <input type="checkbox" class="js-small f-right fields as_per_firm" name="manage_next_reminder_date" id="manage_next_reminder_date" >
                                                         </div>
                                                      </div>
                                                      <!-- <div class="form-group row radio_bts management" id="<?php echo $this->Common_mdl->get_order_details(147); ?>"  <?php echo $this->Common_mdl->get_delete_details(147); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(147); ?></label>
                                                         <div class="col-sm-8">
                                                            <input type="checkbox" class="js-small f-right fields" name="manage_create_task_reminder" id="manage_create_task_reminder">
                                                         </div>
                                                      </div> -->
                                                      <div class="form-group row fr-float radio_bts manage_add_custom_reminder_label management" id="<?php echo $this->Common_mdl->get_order_details(148); ?>" <?php $vf=$this->Common_mdl->get_delete_details(148); if($vf!=''){ echo $vf.'!important'; } ?>>
                                                         <label id="manage_add_custom_reminder_label" name="manage_add_custom_reminder_label" class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(148); ?></label>
                                                         <div class="col-sm-8" id="manage_add_custom_reminder">
                                                            <input type="checkbox" class="js-small f-right fields cus_reminder" name="manage_add_custom_reminder" id="" data-id="manage_cus" data-serid="14">
                                                            <!-- <textarea rows="3" placeholder="" name="manage_add_custom_reminder" id="manage_add_custom_reminder" class="fields"></textarea> -->
                                                            <!-- <a name="manage_add_custom_reminder" id="manage_add_custom_reminder" href="<?php echo base_url(); ?>/user/Service_reminder_settings/14" target="_blank">Click to add Custom Reminder</a> -->
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- accordion-panel -->
                                    <!--       <div class="accordion-panel inma" style="display: none">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Management Invoice</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1">
                                                      <div class="form-group row name_fields">
                                                         <label class="col-sm-4 col-form-label">Invoice</label>
                                                         <div class="col-sm-8">
                                                            <select name="invoice_ma" id="invoice_ma">
                                                               <option value="">Select</option>
                                                               <option value="email">E-Mail</option>
                                                               <option value="sms">SMS</option>
                                                               <option value="phone">Phone</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div> -->
                                         
                                       </div>
                           </li> <!-- management-account tab close -->
                           
                           <!-- investigation tab start -->
                           <li class="show-block">
                              <a href="JavaScript:void(0);" class="toggle"> Investigation Insurance</a>
                              <div id="investigation-insurance" class="masonry-container floating_set inner-views">
                                          <div class="accordion-panel for_investigation" style="display: none;">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Investigation Insurance</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1" id="invest-box">
                                                      <div class="form-group row help_icon date_birth invest" id="<?php echo $this->Common_mdl->get_order_details(58); ?>"  <?php echo $this->Common_mdl->get_delete_details(58); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(58); ?></label>
                                                         <div class="col-sm-8">
                                                            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                            <input class="form-control datepicker fields dob_picker" name="insurance_start_date" id="datepicker30" type="text" placeholder="dd-mm-yyyy" value=""/>
                                                         </div>
                                                      </div>
                                                      <div class="form-group row help_icon date_birth invest" id="<?php echo $this->Common_mdl->get_order_details(59); ?>"  <?php echo $this->Common_mdl->get_delete_details(59); ?>>
                                                         <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(59); ?></label>
                                                         <span class="hilight_due_date">*</span>
                                                         <div class="col-sm-8">
                                                            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                            <input class="form-control datepicker fields dob_picker" name="insurance_renew_date" id="datepicker31" type="text" placeholder="dd-mm-yyyy" value=""/>
                                                         </div>
                                                      </div>
                                                      <div class="form-group row invest" id="<?php echo $this->Common_mdl->get_order_details(60); ?>"  <?php echo $this->Common_mdl->get_delete_details(60); ?>>
                                                         <label class="col-sm-4 col-form-label">  <?php echo $this->Common_mdl->get_name_details(60); ?></label>
                                                         <div class="col-sm-8">
                                                            <select name="insurance_provider" id="insurance_provider" class="form-control fields">
                                                               <option value="">--Select--</option>
                                                               <option value="Excel">Excel</option>
                                                               <option value="Software">Software</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                      <div class="form-group rowv invest" id="<?php echo $this->Common_mdl->get_order_details(140); ?>">
                                                         <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(140); ?></label>
                                                         <div class="col-sm-8 text-box1">
                                                            <textarea rows="3" placeholder="" name="claims_note" id="claims_note" class="fields"></textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- accordion-panel -->
                                          <div class="accordion-panel enable_invest" style="display:none;">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Investigation Insurance Reminders</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1" id="invest_box1">
                                                      <div class="form-group row  date_birth radio_bts">
                                                         <label class="col-sm-4 col-form-label"><a id="insurance_add_custom_reminder_link" name="insurance_add_custom_reminder_link" href="<?php echo base_url(); ?>user/Service_reminder_settings/13" target="_blank">As per firm setting</a></label>
                                                         <div class="col-sm-8">
                                                            <!-- <input type="hidden" name="insurance_next_reminder_date" id="insurance_next_reminder_date" placeholder="" value="" class="fields dob_picker"> -->
                                                            <input type="checkbox" class="js-small f-right fields as_per_firm" name="insurance_next_reminder_date" id="insurance_next_reminder_date">
                                                         </div>
                                                      </div>
                                                      <!-- <div class="form-group row radio_bts" style="">
                                                         <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                                                         <div class="col-sm-8">
                                                            <input type="checkbox" class="js-small f-right fields" name="insurance_create_task_reminder" id="insurance_create_task_reminder">
                                                         </div>
                                                      </div> -->
                                                      <div class="form-group row  fr-float radio_bts insurance_add_custom_reminder_label">
                                                         <label class="col-sm-4 col-form-label" name="insurance_add_custom_reminder_label" id="insurance_add_custom_reminder_label">Add Custom Reminder</label>
                                                         <div class="col-sm-8" id="insurance_add_custom_reminder">
                                                            <input type="checkbox" class="js-small f-right fields cus_reminder" name="insurance_add_custom_reminder" id="" data-id="insurance_cus" data-serid="9">
                                                          
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- accordion-panel -->
                                          <!-- investigation insurance invoice-->
                                         <!--  <div class="accordion-panel ininves" style="display: none">
                                             <div class="box-division03">
                                             <div class="accordion-heading" role="tab" id="headingOne">
                                                <h3 class="card-title accordion-title">
                                                   <a class="accordion-msg">Investigation Insurance Invoice</a>
                                                </h3>
                                             </div>
                                             <div id="collapse" class="panel-collapse">
                                                <div class="basic-info-client1">
                                                   <div class="form-group row name_fields">
                                                      <label class="col-sm-4 col-form-label">Invoice</label>
                                                      <div class="col-sm-8">
                                                         <select name="invoice_insuance" id="invoice_insuance">
                                                            <option value="email">E-Mail</option>
                                                            <option value="sms">SMS</option>
                                                            <option value="phone">Phone</option>
                                                         </select>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          </div> -->
                                          <!-- investigation insurance invoice-->
                                          <!-- <div class="clearfix"></div> -->
                                          <div class="registeredtab">
                                             <div class="accordion-panel" style="display:block;">
                                                <div class="box-division03">
                                                   <div class="accordion-heading" role="tab" id="headingOne">
                                                      <h3 class="card-title accordion-title">
                                                         <a class="accordion-msg">Registered Office</a>
                                                      </h3>
                                                   </div>
                                                   <div id="collapse" class="panel-collapse">
                                                      <div class="basic-info-client1" id="invest_box2">
                                                         <div class="form-group row help_icon date_birth invest radio_bts" id="<?php echo $this->Common_mdl->get_order_details(62); ?>" <?php echo $this->Common_mdl->get_delete_details(62); ?>>
                                                            <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(62); ?></label>
                                                            <div class="col-sm-8">
                                                               <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                               <input class="form-control datepicker fields dob_picker" name="registered_start_date" id="datepicker33" type="text" placeholder="dd-mm-yyyy" value=""/>
                                                            </div>
                                                         </div>
                                                         <div class="form-group row help_icon date_birth invest" id="<?php echo $this->Common_mdl->get_order_details(63); ?>" <?php echo $this->Common_mdl->get_delete_details(63); ?>>
                                                            <label class="col-sm-4 col-form-label"> <?php echo $this->Common_mdl->get_name_details(63); ?></label>
                                                            <span class="hilight_due_date">*</span>
                                                            <div class="col-sm-8">
                                                               <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                               <input class="form-control datepicker fields dob_picker" name="registered_renew_date" id="datepicker34" type="text" placeholder="dd-mm-yyyy" value=""/>
                                                            </div>
                                                         </div>
                                                         <div class="form-group row invest" id="<?php echo $this->Common_mdl->get_order_details(64); ?>" <?php echo $this->Common_mdl->get_delete_details(64); ?>>
                                                            <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(64); ?></label>
                                                            <div class="col-sm-8">
                                                               <input type="text" name="registered_office_inuse" id="registered_office_inuse" placeholder="" value="" class="fields">
                                                            </div>
                                                         </div>
                                                         <div class="form-group row invest text-box1" id="<?php echo $this->Common_mdl->get_order_details(65); ?>" <?php echo $this->Common_mdl->get_delete_details(65); ?>>
                                                            <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(65); ?></label>
                                                            <div class="col-sm-8">
                                                               <textarea rows="3" placeholder="" name="registered_claims_note" id="registered_claims_note" class="fields"></textarea>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <!-- accordion-panel -->
                                             <div class="accordion-panel enable_reg" style="display:none;">
                                                <div class="box-division03">
                                                   <div class="accordion-heading" role="tab" id="headingOne">
                                                      <h3 class="card-title accordion-title">
                                                         <a class="accordion-msg">Registered Reminders</a>
                                                      </h3>
                                                   </div>
                                                   <div id="collapse" class="panel-collapse">
                                                      <div class="basic-info-client1">
                                                         <div class="form-group row  date_birth radio_bts">
                                                            <label class="col-sm-4 col-form-label"><a id="registered_add_custom_reminder_link" name="registered_add_custom_reminder_link" href="<?php echo base_url(); ?>user/Service_reminder_settings/14" target="_blank">As per firm setting</a></label>
                                                            <div class="col-sm-8">
                                                               <!-- <input type="hidden" name="registered_next_reminder_date" id="registered_next_reminder_date" placeholder="" value="" class="fields dob_picker"> -->
                                                               <input type="checkbox" class="js-small f-right fields as_per_firm" name="registered_next_reminder_date" id="registered_next_reminder_date" >
                                                            </div>
                                                         </div>
                                                         <!-- <div class="form-group row radio_bts "" >
                                                            <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                                                            <div class="col-sm-8">
                                                               <input type="checkbox" class="js-small f-right fields" name="registered_create_task_reminder" id="registered_create_task_reminder">
                                                            </div>
                                                         </div> -->
                                                         <div class="form-group row  fr-float radio_bts registered_add_custom_reminder_label">
                                                            <label class="col-sm-4 col-form-label" id="registered_add_custom_reminder_label" name="registered_add_custom_reminder_label">Add Custom Reminder</label>
                                                            <div class="col-sm-8" id="registered_add_custom_reminder">
                                                               <input type="checkbox" class="js-small f-right fields cus_reminder" name="registered_add_custom_reminder" id="" data-id="registered_cus" data-serid="9">
                                                               <!-- <textarea rows="3" placeholder="" name="registered_add_custom_reminder" id="registered_add_custom_reminder" class="fields"></textarea> -->
                                                               <!-- <a name="registered_add_custom_reminder" id="registered_add_custom_reminder" href="<?php echo base_url(); ?>user/Service_reminder_settings/9" target="_blank">Click to add Custom Reminder</a> -->
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <!-- accordion-panel -->
                                             <!-- invoice registration-->
                                         <!--     <div class="accordion-panel inreg" style="display: none">
                                                <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Registered Invoice</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1">
                                                      <div class="form-group row name_fields">
                                                         <label class="col-sm-4 col-form-label">Invoice</label>
                                                         <div class="col-sm-8">
                                                            <select name="invoice_reg" id="invoice_reg">
                                                               <option value="">Select</option>
                                                               <option value="email">E-Mail</option>
                                                               <option value="sms">SMS</option>
                                                               <option value="phone">Phone</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             </div> -->
                                             <!-- invoice registration-->
                                          </div>
                                          <!-- registeredtab close-->
                                          <div class="taxadvicetab" style="display:none;">
                                             <div class="accordion-panel">
                                                <div class="box-division03">
                                                   <div class="accordion-heading" role="tab" id="headingOne">
                                                      <h3 class="card-title accordion-title">
                                                         <a class="accordion-msg">Tax Advice/Investigation</a>
                                                      </h3>
                                                   </div>
                                                   <div id="collapse" class="panel-collapse">
                                                      <div class="basic-info-client1">
                                                         <div class="form-group row help_icon date_birth">
                                                            <label class="col-sm-4 col-form-label">Start Date</label>
                                                            <div class="col-sm-8">
                                                               <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                               <input class="form-control datepicker fields dob_picker" name="investigation_start_date" id="datepicker36" type="text" placeholder="dd-mm-yyyy" value=""/>
                                                            </div>
                                                         </div>
                                                         <div class="form-group row help_icon date_birth">
                                                            <label class="col-sm-4 col-form-label">End Date</label>
                                                            <span class="hilight_due_date">*</span>
                                                            <div class="col-sm-8">
                                                               <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                               <input class="form-control datepicker fields dob_picker" name="investigation_end_date" id="datepicker37" type="text" placeholder="dd-mm-yyyy" value=""/>
                                                            </div>
                                                         </div>
                                                         <div class="form-group row text-box1"  <?php echo $this->Common_mdl->get_delete_details(69); ?>>
                                                            <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(69); ?></label>
                                                            <div class="col-sm-8">
                                                               <textarea rows="3" placeholder="" name="investigation_note" id="investigation_note" class="fields"></textarea>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <!-- accordion-panel -->
                                             <div class="accordion-panel enable_taxad" style="display:none;">
                                                <div class="box-division03">
                                                   <div class="accordion-heading" role="tab" id="headingOne">
                                                      <h3 class="card-title accordion-title">
                                                         <a class="accordion-msg">Tax advice/investigation Reminders</a>
                                                      </h3>
                                                   </div>
                                                   <div id="collapse" class="panel-collapse">
                                                      <div class="basic-info-client1">
                                                         <div class="form-group row  date_birth radio_bts">
                                                            <label class="col-sm-4 col-form-label"><a id="investigation_add_custom_reminder_link" name="investigation_add_custom_reminder_link" href="<?php echo base_url(); ?>user/Service_reminder_settings/15" target="_blank">As per firm setting</a></label>
                                                            <div class="col-sm-8">
                                                               <!-- <input type="hidden" name="investigation_next_reminder_date" id="investigation_next_reminder_date" placeholder="" value="" class="fields dob_picker"> -->
                                                               <input type="checkbox" class="js-small f-right fields as_per_firm" name="investigation_next_reminder_date" id="investigation_next_reminder_date">
                                                            </div>
                                                         </div>
                                                         <!-- <div class="form-group row radio_bts ">
                                                            <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                                                            <div class="col-sm-8">
                                                               <input type="checkbox" class="js-small f-right fields" name="investigation_create_task_reminder" id="investigation_create_task_reminder">
                                                            </div>
                                                         </div> -->
                                                         <div class="form-group row  fr-float radio_bts investigation_add_custom_reminder_label" >
                                                            <label id="investigation_add_custom_reminder_label" name="investigation_add_custom_reminder_label" class="col-sm-4 col-form-label">Add Custom Reminder</label>
                                                            <div class="col-sm-8" id="investigation_add_custom_reminder">
                                                               <input type="checkbox" class="js-small f-right fields cus_reminder" name="investigation_add_custom_reminder" id="" data-id="investigation_cus" data-serid="9">
                                                               <!-- <textarea rows="3" placeholder="" name="investigation_add_custom_reminder" id="investigation_add_custom_reminder" class="fields"></textarea> -->
                                                               <!-- <a name="investigation_add_custom_reminder" id="investigation_add_custom_reminder" href="<?php echo base_url(); ?>user/Service_reminder_settings/9" target="_blank">Click to add Custom Reminder</a> -->
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <!-- accordion-panel -->
                                          </div>
                                          <!-- taxadvicetab-->
                                          <!-- invoice tax advice -->
                                        <!--   <div class="accordion-panel intaxadv" style="display: none">
                                             <div class="box-division03">
                                             <div class="accordion-heading" role="tab" id="headingOne">
                                                <h3 class="card-title accordion-title">
                                                   <a class="accordion-msg">Tax advice Invoice</a>
                                                </h3>
                                             </div>
                                             <div id="collapse" class="panel-collapse">
                                                <div class="basic-info-client1">
                                                   <div class="form-group row name_fields">
                                                      <label class="col-sm-4 col-form-label">Invoice</label>
                                                      <div class="col-sm-8">
                                                         <select name="invoice_taxadv" id="invoice_taxadv">
                                                            <option value="">Select</option>
                                                            <option value="email">E-Mail</option>
                                                            <option value="sms">SMS</option>
                                                            <option value="phone">Phone</option>
                                                         </select>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          </div> -->
                                          <!-- invoice tax advice -->
                                          <!-- invoice tax Investigation -->
                                          <!-- <div class="accordion-panel intaxinves" style="display: none">
                                             <div class="box-division03">
                                             <div class="accordion-heading" role="tab" id="headingOne">
                                                <h3 class="card-title accordion-title">
                                                   <a class="accordion-msg">Tax Investigation Invoice</a>
                                                </h3>
                                             </div>
                                             <div id="collapse" class="panel-collapse">
                                                <div class="basic-info-client1">
                                                   <div class="form-group row name_fields">
                                                      <label class="col-sm-4 col-form-label">Invoice</label>
                                                      <div class="col-sm-8">
                                                         <select name="invoice_taxinves" id="invoice_taxinves">
                                                            <option value="">Select</option>
                                                            <option value="email">E-Mail</option>
                                                            <option value="sms">SMS</option>
                                                            <option value="phone">Phone</option>
                                                         </select>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             </div>
                                          </div> -->
                                          <!-- invoice tax Investigation -->
                                          <!-- <div class="clearfix"></div> -->
                                         
                                          <!-- accordion-panel -->
                                       </div>
                           </li> <!-- investigation tab close -->
                              
                              
                           </ul>
                           </div>  <!-- import_tab close -->
                                    <!-- 11 tab close -->
                                    <!-- investication insurance tab close-->
                                 </div>
                                 <!-- tab-content -->
                              </div>
                       </div>
                           </div>
                           <!-- managementclose -->
                        </form>
                        <!-- admin close-->
                     </div>
                  </div>
               </div>
            </div>
            <!-- Page body end -->
         </div>
      </div>
      <!-- Main-body end -->
      <div id="styleSelector">
      </div>
   </div>
</div>
</div>
</div>
</div>
</div>
<div class="add_custom_fields_section" style="display: none;">
<?php
echo render_custom_fields_one( 'accounts',false,['firm_id'=>$_SESSION['firm_id']] ); 
echo render_custom_fields_one( 'assign_to',false,['firm_id'=>$_SESSION['firm_id']] );
echo render_custom_fields_one( 'confirmation',false,['firm_id'=>$_SESSION['firm_id']] );
echo render_custom_fields_one( 'personal_tax',false,['firm_id'=>$_SESSION['firm_id']] ); 
echo render_custom_fields_one( 'payroll',false,['firm_id'=>$_SESSION['firm_id']] ); 
echo render_custom_fields_one( 'vat',false,['firm_id'=>$_SESSION['firm_id']] );
echo render_custom_fields_one( 'management',false,['firm_id'=>$_SESSION['firm_id']] ); 
echo render_custom_fields_one( 'investigation',false,['firm_id'=>$_SESSION['firm_id']] );
echo render_custom_fields_one( 'other',false,['firm_id'=>$_SESSION['firm_id']] ); 
echo render_custom_fields_one( 'customers',false,['firm_id'=>$_SESSION['firm_id']] ); ?>
</div>
<!-- modal 1-->

<!-- modal-close -->
<!-- modal 2-->
<div class="modal fade" id="add-reminder" role="dialog">
   <div class="modal-dialog modal-md">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close reminder_close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">add reminder</h4>
         </div>
         <div class="alert alert-success-re" style="display:none;"
            ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Remainder Added.</div>
         <div class="alert alert-danger-re" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Not Added.</div>
         <form action="" class="validation edit_field_form" method="post" accept-charset="utf-8" novalidate="novalidate">
            <div class="modal-body">
               <div class="modal-topsec">
                  <div class="send-invoice">
                     <div class="form-group">
                        <span class="invoice-notify">send</span>
                        <select name="due">
                           <option value="due_by">due by</option>
                           <option value="overdue_by">overdue by</option>
                        </select>
                     </div>
                     <div class="form-group">
                        <span class="days1">Email Template</span>
                        <select name="email_template" id="email_template">
                        <option value="">Select</option>
                          <?php foreach($reminder_templates as $key => $value1){ ?>              
                          <option value="<?php echo $value1['id']; ?>"><?php echo ucwords($value1['title']); ?></option>            
                          <?php } ?>
                        </select>

                        <span class="error_msg_email" style="color: red;display: none;">This Field is Required</span>
                     </div>      



                     <div class="form-group">
                        <span class="days1">days</span>
                        <input type="text" class="due-days decimal decimal_days" name="days">
                        <span class="error_msg_days" style="color: red;display: none;">This Field is Required</span>
                     </div>
                  </div>


                  <div class="insert-placeholder">
                 
                     <div class="append_placeholder">
                     </div>
                  </div>
               </div>
               <!--modal-topsec-->
               <div class="modal-bottomsec">
                  <div class="form-group">
                     <input type="hidden" name="service_id" id="service_id" value="">
                  </div>
                  <div class="form-group">
                     <input type="text" class="invoice-msg" name="subject" placeholder="Subject" value="">
                      <span class="error_msg_subject" style="color: red;display: none;">This Field is Required</span>
                  </div>
                  <div class="form-group">
                     <textarea name="message" id="editor2" class="editor2"></textarea>
                     <span class="error_msg_message" style="color: red;display: none;">This Field is Required</span>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
               <!-- <button type="submit" class="reminder-delete">delete</button> -->
               <input type="hidden" id="clicked_checkbox_name">
               <button type="submit" class="reminder-save">save</button>
            </div>
         </form>
      </div>
   </div>
</div>
<div class="modal fade" id="myAlert" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Alert</h4>
         </div>
         <div class="modal-body">
            <p>  
               Do you want to exit? 
            </p>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default no" data-dismiss="modal">No</button> 
            <button type="button" class="btn btn-default exit">Yes</button>        
         </div>
      </div>
   </div>
</div>
<div class="modal fade remind_popup_client reminder_section_one" id="myReminders" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <!--   <button type="button" class="close" data-dismiss="modal">&times;</button> -->
            <h4 class="modal-title">Reminders</h4>
         </div>
         <!--   <form id="insert_form1" class="required-save client-firm-info1 validation" method="post" action="" enctype="multipart/form-data">   -->
         <input type="hidden" name="user_id" id="user_id" value="<?php if(isset($client['user_id']) && ($client['user_id']!='') ){ echo $client['user_id'];}?>" class="fields">
         <input type="hidden" name="client_id" id="client_id" value="<?php if(isset($user[0]['client_id']) && ($user[0]['client_id']!='') ){ echo $user[0]['client_id'];}?>" class="fields">
         <input type="hidden" id="company_house" name="company_house" value="<?php if(isset($user[0]['company_roles']) && ($user[0]['company_roles']!='') ){ echo $user[0]['company_roles'];}?>" class="fields">
         <input type="hidden" name="emailid" id="emailid" placeholder="" value="<?php if(isset($client['crm_email']) && ($client['crm_email']!='') ){ echo $client['crm_email'];}?>" class="fields">
         <input type="hidden" name="user_name" id="username" placeholder="" value="<?php if(isset($user[0]['username']) && ($user[0]['username']!='') ){ echo $user[0]['username'];}?>" class="fields">
         <div class="modal-body">
            <div class="body-popup-content">
            </div>
         </div>
         <!-- NEED TO REMOVE THIS CODE -->     
         <div id = "suganya"></div>
         <div class="modal-footer">
            <!--  <input type="submit" class="signed-change1 btn btn-primary" id="save_exit" value="Save" >  -->
            <button type="button" class="btn btn-default save" data-dismiss="modal">Close</button>        
         </div>
      </div>
      <!--  </form> -->
   </div>
</div>
<div class="modal fade all_layout_modal" id="company_house_contact" tabindex="-1" role="dialog">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h4 class="modal-title"> Companines House Contact</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  

                  <div class="main_contacts" style="display:none">
                   
                  </div>
               </div>
               <!-- modalbody -->
            </div>
         </div>
</div>

<!-- add reminder -->
<!-- ajax loader -->
<!-- ajax loader end-->
<?php //$this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>

<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script>
<!-- <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-ui/js/jquery-ui.min.js"></script> -->
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/masonry/3.3.1/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/popper.js/js/popper.min.js"></script>
<!-- j-pro js -->
<!-- <script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.ui.min.js"></script> -->
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.j-pro.js"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<!-- modernizr js -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/switchery/js/switchery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/modernizr/js/css-scrollbars.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/advance-elements/custom-picker.js"></script>
<script src="<?php echo base_url();?>assets/js/pcoded.min.js"></script>
<script src="<?php echo base_url();?>assets/js/demo-12.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/common_script.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/client_page.js"></script>
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
<script src="<?php echo base_url();?>assets/tree_select/comboTreePlugin.js"></script>
<script src="<?php echo base_url();?>assets/tree_select/icontains.js"></script>

<script type="text/javascript">
         /** 29-08-2018 **/
      
         var tree_select = <?php echo json_encode( GetAssignees_SelectBox() ); ?>;
         $('#tree_select').comboTree({ 
            source : tree_select,
            isMultiple: true
         });

         $('.comboTreeItemTitle').click(function(){  
            var id = $(this).attr('data-id').split('_');
            var selected = [];
               $('.comboTreeItemTitle').each(function(){
                  var id1 = $(this).attr('data-id');
                  var id1 = id1.split('_');
                     if($(this).find('input[type="checkbox"]').is(":checked"))
                     {
                        selected.push(id1[0]);
                     }
                  //console.log(id[1]+"=="+id1[1]);
                  if(id[1]==id1[1])
                  {
                     $(this).toggleClass('disabled');
                  }
               });
               $("#tree_select_values").val(selected.join());
               $(this).removeClass('disabled');
         });

         // $('.comboTreeItemTitle input[type="checkbox"]').wrap( '<label class="custom_checkbox1"></label>');
         // $('.custom_checkbox1 input').after( "<i></i>" )




      $('.assign_cus_type').find('.dropdown-sin-11').on('click',function(){

      $(this).find('.dropdown-main > ul > li').click(function(){
           var custom=$(this).attr('data-value');
           var customclass=$(this).attr('class');
          if(custom=='Other_Custom')
         {   
            //if(customclass=='dropdown-option dropdown-chose'){
            if (customclass.indexOf('dropdown-chose') > -1){
                   $('.for_other_custom_choose').css('display','none');
            }
            else
            {
               $('.for_other_custom_choose').css('display','');
            }          
         }
         else{     
          }
      
        
         });
         $(this).find('.dropdown-clear-all').on('click',function(){
            $('.for_other_custom_choose').css('display','none');
         });
      });
$('.tags').tagsinput({
        allowDuplicates: true
      });


</script>
<?php
   if(isset($_SESSION['client_added'])){
   if($_SESSION['client_added']=='company'){
   $this->session->set_userdata('client_section','company');
    ?>
<script type="text/javascript">
   $(document).ready(function() {
   
   
   $('li.company a').trigger('click');
   });
</script>
<?php } }
   ?>
<script> 
function i_change(){  
    var rec = staff;   
      $('.responsible_user_tbl').show();
      $('.responsible_team_tbl').show();
      $('.responsible_department_tbl').show();
    //  $('.responsible_member_tbl').show();
   

     // $('.responsible_member_tbl').show();
   
   
  
}




//    $(document).ready(function() {
 

// i_change();

       


//    var $grid11 = $('.masonry-container').masonry({
//            itemSelector: '  .accordion-panel',
//            percentPosition: true,
//            columnWidth: '  .grid-sizer' 
//          });
      
         
//          $(document).on('click', '.add_acc_client', function(){
//              $('.accordion-panel').css("transform", "");
//            setTimeout(function(){ 
           
//               $grid11.masonry('layout'); 
            
   
//            }, 600);
//          });
         
//           $(document).on('click', '.switchery', function(){
   
//            setTimeout(function(){ 
//               $grid11.masonry('reloadItems');
//               $grid11.masonry('layout'); 
//             $('.accordion-panel').css("transform", "");
   
//            }, 600);  
//           });
          
//             });
</script> 
<script>
   /** rs **/
   $(document).on('change','.datepicker',function()
   {
     var date_val=$(this).val();
     var custom=$(this).attr('data-filed-custom');
       var fi_data_to = $(this).attr("data-fieldto");
              var fi_data_id  = $(this).attr("data-fieldid");
     if(typeof custom != "undefined"){
   
             $("[name^=custom_fields]").each(function() {
           var data_to = $(this).attr("data-fieldto");
              var data_id  = $(this).attr("data-fieldid");
   if((fi_data_id==data_id) && (fi_data_to==data_to) )
   {
     $(this).attr('value',date_val);
   }
           // Do stuff
         });
     }
     else
     {
       $("input[name="+$(this).attr('name')+"]").each(function() {
                              $(this).attr('value',date_val);
                             });
     }
     //$(this).attr('value','sdasd');
   
   });
   $(document).on('change','.dob_picker',function()
   {
     var date_val=$(this).val();
     var custom=$(this).attr('data-filed-custom');
       var fi_data_to = $(this).attr("data-fieldto");
              var fi_data_id  = $(this).attr("data-fieldid");
     if(typeof custom != "undefined"){
   
             $("[name^=custom_fields]").each(function() {
           var data_to = $(this).attr("data-fieldto");
              var data_id  = $(this).attr("data-fieldid");
   if((fi_data_id==data_id) && (fi_data_to==data_to) )
   {
     $(this).attr('value',date_val);
   }
           // Do stuff
         });
     }
     else
     {
       $("input[name="+$(this).attr('name')+"]").each(function() {
                              $(this).attr('value',date_val);
                             });
     }
     //$(this).attr('value','sdasd');
   
   });
   
   /*** end of rs **/
   
      // $( document ).load(function() {
      //  $(".dob_picker").datepicker();
      // //$(document).on('change','.othercus',function(e){
      // });
      $( document ).ready(function() {
         //minDate:0,
       $(".dob_picker").datepicker({ dateFormat: 'dd-mm-yy', changeMonth: true,
           changeYear: true,  }).val();
       // $(".dob_picker").datepicker({ dateFormat: 'dd-mm-yy' }).val();
      //$(document).on('change','.othercus',function(e){
      });
   /*for copy  auth_code to all place if enter in one place */
      $('#confirmation_auth_code, #accounts_auth_code').keyup(function () {
            $('#confirmation_auth_code').val($(this).val());
            $('#accounts_auth_code').val($(this).val());
        });
   /*for copy  auth_code to all place if enter in one place */

         $( document ).ready(function() { 
              var today = new Date();
             $('#terms_signed').datepicker({
                 dateFormat: 'dd-mm-yy',
                 autoclose:true,
                 endDate: "today",
                // minDate:0,
                 changeMonth: true,
           changeYear: true,
             }).on('changeDate', function (ev) {
                     $(this).datepicker('hide');
                 });
         
         
             $('#terms_signed').keyup(function () {
                 if (this.value.match(/[^0-9]/g)) {
                     this.value = this.value.replace(/[^0-9^-]/g, '');
                 }
             });  
      
             var today = new Date();
             $('.dob_picker').datepicker({
                 dateFormat: 'dd-mm-yy',
                 autoclose:true,
                 endDate: "today",
               //  minDate:0, 
                 changeMonth: true,
           changeYear: true,
             }).on('changeDate', function (ev) {
                     $(this).datepicker('hide');
                 });
         
         
             $('.dob_picker').keyup(function () {
                 if (this.value.match(/[^0-9]/g)) {
                     this.value = this.value.replace(/[^0-9^-]/g, '');
                 }
             });
         
         
             var date = $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy',
               //minDate:0,
                changeMonth: true,
           changeYear: true, }).val();
         
             // Multiple swithces
             var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));
      
             elem.forEach(function(html) {
      
                  /* var switchery = new Switchery(html, {
                     color: '#1abc9c',
                     jackColor: '#fff',
                     size: 'small'
                 }); */
             });
         
             $('#accordion_close').on('click', function(){
                     $('#accordion').slideToggle(300);
                     $(this).toggleClass('accordion_down');
             });
         
        /*$("#legal_form").change(function(){
         var legal_form = $(':selected',this).data('id');
         //alert(legal_form);
         if(legal_form=='2'){
         $(".it2").attr("style", "display:block");
         $(".it2").removeClass("hide");
         } else{
         $(".it2").attr("style", "display:none");
         $(".it2").addClass("hide");
         }
         
         });*/
         
         
      
         // Other custom label
      
      $(document).on('change','.othercus',function(e){
      //$(".othercus").change(function(){
         var custom = $(':selected',this).val();
         var client_id = $('#user_id').val();
          //alert(custom);
          
       if(custom=='Other')
         {
        // $('.contact_type').next('span').show();
        $(this).closest('.contact_type').next('.spnMulti').show();
       } else {
      $('.contact_type').next('span').hide();
       }
      
         
         });  
      
      
      //Assign Other custom label
      $(document).on('change','.assign_cus',function(e){
      //$(".othercus").change(function(){
         var custom = $(':selected',this).val();
         var client_id = $('#user_id').val();
         // alert(custom);
          
       if(custom=='Other_Custom')
         {
        // $('.contact_type').next('span').show();
        $(this).closest('.assign_cus_type').next('.spanassign').show();
          } else {
         $(this).closest('.assign_cus_type').next('.spanassign').hide();
          }
      
         }); 

      
         });

   /** end of 29-08-2018 **/
</script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script>   
 function validateEmail($email)
 {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return emailReg.test( $email );
 }

function phonenumber(inputtxt)
{
   console.log(inputtxt);

   var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;

         if(phoneno.test(inputtxt.value))
        {
            return true;
        }
        else
        {    
            return false;
        }
}


      var check_username = 
      {
         url:'<?php echo base_url();?>firm/check_username/',
         type : 'post',
         data:{
            <?php if(!empty($uri_seg)){?> id:function(){return $('#user_id').val();} <?php } ?>
            
         }   
    };
    var check_company_numper = {
      url:'<?php echo base_url();?>client/check_company_numberExist/',
      type : 'post',
       data:{
            <?php if(!empty($uri_seg)){?> id:function(){return $('#user_id').val();} <?php } ?>
         }
   };
</script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/add_client/client_validation_rules.js"></script>
<script type="text/javascript">

   $(document).ready(function(){
    
   jQuery.validator.addMethod("contact_no",function(inputtxt ,element)
   {
      var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
      //console.log("inside contact_no validation"+this.optional(element) || phoneno.test(inputtxt.trim()) );
      return this.optional(element) || phoneno.test(inputtxt.trim());
       
   }, "Enter Valid Contact Number.");

$("form#insert_form").on('submit',function(){

         $(document).find('input[name^="work_email"] , input[name^="main_email"]').each(function(){
            $(this).rules("add", 
                  {
                      email:true,
                      messages: {
                          email:"Enter Valid Email Address."
                      }
                  });
         });
         $(document).find('input[name^="landline"] ,input[name^="mobile_number"] ').each(function(){
            $(this).rules("add", 
                  {  required:false,
                      contact_no:true,
                                         
                  });
         });   
         $(document).find('input[name^="first_name"] ').each(function(){
            $(this).rules("add", 
                  {
                      required:true,
                      messages: 
                      {
                          required: "First Name is required.",                                                    
                      }
                  });
         }); 
         
            $("#insert_form").find("input[name='emailid']").rules("add", 
                  {  
                     required: {depends:check_previous_tab_open},
                     email:    {depends:check_previous_tab_open},                     
                     messages:{
                                 required:"Email Address For Previous Accounts.",
                                 email:"Please Enter A Valid Email Address In Previous Accounts."
                              },
                  });
            $("#insert_form").find("input[name='cun_code']").rules("add", 
                  {  
                     required: {depends:check_previous_tab_open},
                     digits:   {depends:check_previous_tab_open},
                     minlength: {param:2,depends:check_previous_tab_open},
                     maxlength:{param:2,depends:check_previous_tab_open},
                     messages:{
                                 required:"Enter Valide Country Code",
                              }
                  });
            $("#insert_form").find("input[name='pn_no_rec']").rules("add",
                  {
                     required:{depends:check_previous_tab_open},
                     contact_no:{depends:check_previous_tab_open},
                     messages:{
                                 required:"Contact No Required.",
                              }

                  });

         
    });

   $("#insert_form").validate({   
          ignore: false,     
            errorPlacement: function(error, element) {
               
                  if( ! ($(element)[0].nodeName=="INPUT" &&  $(element).attr('type') == 'hidden') )
                  {
                     error.insertAfter(element);
                  }
               var err=error.prop('outerHTML');

               if($("#required_information").has(element).length){                    
                   $('#required_information_error').append(err);
                   $('#required_information_error').find('.field-error').each(function(){
                     if($(this).html()!=''){
                        $(this).addClass('required-errors');
                     }
                   });                   
               }          
               if($("#basic_details").has(element).length){
                  $('#basic_details_error').append(err);
                    $('#basic_details_error').find('.field-error').each(function(){
                        if($(this).html()!=''){
                              $(this).addClass('required-errors');
                        }
                    });                     
               }
               if($("#main_Contact").has(element).length){
                   $('#main_Contact_error').append(err);
                   $('#main_Contact_error').find('.field-error').each(function(){
                     if($(this).html()!=''){
                        $(this).addClass('required-errors');
                     }
                   }); 
               }
               if($("#service").has(element).length){
                  $('#service_error').append(err);
                  $('#service_error').find('.field-error').each(function(){
                     if($(this).html()!=''){
                        $(this).addClass('required-errors');
                     }
                  });
               }
               if($("#assignto").has(element).length){
                  $('#assignto_error').append(err);
                   $('#assignto_error').find('.field-error').each(function(){
                        if($(this).html()!=''){
                            $(this).addClass('required-errors');
                        }
                   }); 
               }
               if($("#amlchecks").has(element).length){
                  $('#amlchecks_error').append(err);
                  $('#amlchecks_error').find('.field-error').each(function(){
                        if($(this).html()!=''){
                            $(this).addClass('required-errors');
                        }
                   }); 
               }
               if($("#other").has(element).length){
                  $('#other_error').append(err);
                  $('#other_error').find('.field-error').each(function(){
                        if($(this).html()!=''){
                            $(this).addClass('required-errors');
                        }
                   }); 
               }

               if($("#referral").has(element).length){
                  $('#referral_error').append(err);
                $('#referral_error').find('.field-error').each(function(){
                        if($(this).html()!=''){
                            $(this).addClass('required-errors');
                        }
                   }); 
               }               
               if($("#import_tab").has(element).length)
                {
                   $('#services_information_error').append(err);
                   $('#services_information_error').find('.field-error').addClass('required-errors');
                }
               

            },
            rules:Rules,
            messages:Message,
                           errorElement: "span" , 
                           errorClass: "field-error",                             
                           submitHandler: function(form) {                            
                                 //for get custom service reminder templates id to update client id
                                 var customReminder_id=[];
                                 $("input[type='checkbox']:checked.cus_reminder").each(function()
                                    {
                                       customReminder_id.push( $(this).val() );
                                    });
                                 $('#add_reminder_ids').val(customReminder_id.join());

                              var formData = new FormData($("#insert_form")[0]);
                              $(".LoadingImage").show();
                              // console.log(formData);
                              $.ajax({
                                   url: '<?php echo base_url();?>client/insert_client/',
                                   dataType : 'json',
                                   type : 'POST',
                                   data : formData,
                                   contentType : false,
                                   processData : false,
                                   success: function(data) {
                                     if(data.user_id != '')
                                     {
                                       contact_add(data.user_id);
                                     }
                                   },
                                   error: function()
                                   {
                              
                                   }
                              });
                              
   
                           } ,
                            /* invalidHandler: function(e,validator) {
           for (var i=0;i<validator.errorList.length;i++){   
               $(validator.errorList[i].element).parents('.panel-collapse.collapse').collapse('show');
           }
       }*/
        invalidHandler: function(e, validator)
        {
              if(validator.errorList.length)
              {
                   //console.log( jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id'));
                  $('#tabs a[data-id="#' + jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').trigger('click');
               }
              /*if(jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id')=='other')
              {
                  $(".other_tabs").find('.nav-link').trigger('click');
              }*/
   
        }
   
                       });
   
   });
   
   
 function contact_add(id)
 {
      $(".LoadingImage").show();
      var cnt = $("#append_cnt").val();
      var data={};
      var title=[];
      $("select[name=title]").each(function(){
       title.push($(this).val());
      }); 
      //data['title'] = arr('title');
      data['title'] = title;
      
      data['middle_name'] = arr('middle_name');
      data['last_name'] = arr('last_name');
      data['surname'] = arr('surname');
      data['preferred_name'] = arr('preferred_name');
      data['nationality'] = arr('nationality');
      data['psc'] = arr('psc');
      data['ni_number'] = arr('ni_number');
      //data['content_type'] = arr('content_type');
      data['address_line1'] = arr('address_line1');
      data['address_line2'] = arr('address_line2');
      data['town_city'] = arr('town_city');
      data['post_code'] = arr('post_code');
      //data['landline'] = arr('landline');
      data['date_of_birth'] = arr('date_of_birth');
      data['nature_of_control'] = arr('nature_of_control');
      //data['marital_status'] = arr('marital_status');
      data['utr_number'] = arr('utr_number');
      data['occupation'] = arr('occupation');
      data['appointed_on'] = arr('appointed_on');
      data['country_of_residence'] = arr('country_of_residence');
      data['other_custom'] = arr('other_custom');
      var selValue = $('input[name=make_primary]:checked').val(); 

      data['make_primary'] = selValue;
      data['make_primary_loop'] = arr('make_primary_loop');
      data['event'] = 'add';

      //data['work_email'] = arr('work_email');
      data['pre_landline'] = [];
      data['landline'] = [];
      data['work_email'] = [];
      data['client_contact_table_id'] = [];
      data['first_name'] = [];
      data['mobile'] = [];
      data['main_email'] =[]; 
         $('.make_a_primary').each(function(){

            
               var i = $(this).attr('id').split('-')[1];

            data['first_name'].push ( $(this).find('input[name="first_name['+i+']"]').val() ); 
            data['mobile'].push ( $(this).find('input[name="mobile_number['+i+']"]').val() ); 
            data['main_email'].push ( $(this).find('input[name="main_email['+i+']"]').val() ); 

            var temp = '';
            if( $(this).find('.contact_table_id').length )
            {
              temp = $(this).find('.contact_table_id').val();
            }
            data['client_contact_table_id'].push( temp );

            
            var temp = [];
            $('select[name^="pre_landline['+i+']"]').each(function(){
                temp.push( $(this).val() ); 
            });
            data['pre_landline'].push( temp );

            var temp = [];

            $('input[name^="landline['+i+']"]').each(function(){
               temp.push( $(this).val() ); 
               
            });
            data['landline'].push( temp );


            var temp = [];
        

            $('input[name^="work_email['+i+']"]').each(function(){
                temp.push( $(this).val() );  
            });
            data['work_email'].push( temp );

         });
          var usertype=[];
      $("select[name=shareholder]").each(function(){
       usertype.push($(this).val());
      }); 
      
       var marital=[];
      $("select[name=marital_status]").each(function(){
       marital.push($(this).val());
      }); 
      
      var contacttype=[];
      $("select[name=contact_type]").each(function(){
       contacttype.push($(this).val());
      }); 
      
      data['marital_status'] = marital;
      data['contact_type'] = contacttype;
      data['shareholder'] = usertype;  
      data['cnt'] =cnt;
      data['event']='add';
      data['client_id'] = id;
      $.ajax({
              url: '<?php echo base_url();?>client/update_client_contacts/',
              type : 'POST',
              data : data,
              success: function(data) {
               $('.alert-success').show();
              },
            });
}


   $("#insert_form1").validate({
   
          ignore: false,
     
                           rules: {
                           company_name: {required: true},
                       //   client_hashtag :{ required : true},
                          // company_number: {required: true},
                   
                           },
                           errorElement: "span", 
                           errorClass: "field-error",                             
                            messages: {
                             company_name: "Give company name",
                         //    client_hashtag : "Give Hashtag details"
                             //company_number: "Give company number",
                             
                            },
   
                           
                             submitHandler: function(form) {
   
                            var formData = new FormData($("#insert_form1")[0]);
                              // $(".LoadingImage").show();
                               $.ajax({
                                   url: '<?php echo base_url();?>client/insert_client/',
                                   dataType : 'json',
                                   type : 'POST',
                                   data : formData,
                                   contentType : false,
                                   processData : false,
                                   success: function(data) {
   
                                     $("#myReminders .close").click();
   //alert(data);
                                        if(data == '0')
                                        {
                                          // alert('failed');
                                           $('.alert-danger').show();
                                           $('.alert-success').hide();
                                        }
                                       else
                                       {
                                                   
                                      // contact_add(data);
                                       }
                                  
                                   },
                                   error: function() {
                                        //contact_add(data);
                                       // $(".LoadingImage").hide();
                                     //  alert('faileddd');
                                     }
                               });
   
                               return false;
   
                           } ,
   
                                   // location.reload();
                       
                                 
                            /* invalidHandler: function(e,validator) {
           for (var i=0;i<validator.errorList.length;i++){   
               $(validator.errorList[i].element).parents('.panel-collapse.collapse').collapse('show');
           }
       }*/
        invalidHandler: function(e, validator) {
              if(validator.errorList.length)
           $('#tabs a[href="#' + jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
                if(jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id')=='other'){
               //alert('ok');
                    $("li.other_tabs").find('.nav-link').trigger('click');
                 }
           }
   
                       });
   
   function for_add_reminderintotask(id)
   {
   var data={};
   data['user_id']=id;
     $.ajax({
                                   url: '<?php echo base_url();?>client/insert_remindertask/',
                                  
                                   type : 'POST',
                                   data :data ,
                                
                                   success: function(data) {
                                                   $('.LoadingImage').hide(); 
                                      // window.location.href = '<?php echo base_url(); ?>user';
                                           $('.alert-success').show();


                                   },
                                   error: function() {
                                       //contact_add(data);
                                       // $(".LoadingImage").hide();
                                       //alert('faileddd');
                                     }
                               });
   
   }
   
   
     
   function arr(name){
   var values = $("input[name='"+name+"[]']")
                 .map(function(){return $(this).val();}).get();
                 return values;
   }
   
   
   function add_assignto(clientId)
   {
   var data = {};
   var Assignees= [];

   $('.comboTreeItemTitle input[type="checkbox"]:checked').each(function(){
       var id = $(this).closest('.comboTreeItemTitle').attr('data-id');
       var id = id.split('_');
      Assignees.push( id[0] );
   });
   
   data['assignee'] = Assignees;
   
   /*       var team=[];
   $("select[name=team]").each(function(){
    team.push($(this).val());
   }); 
   data['team'] = team;
   
    data['allocation_holder'] = arr('allocation_holder');*/
    data['clientId'] = clientId;
    $.ajax({
      url: '<?php echo base_url();?>client/add_assignto/',
       type: "POST",
       data: data,
       success: function(data)  
       {
            //add_responsibleuser(clientId);
            for_add_reminderintotask(clientId);
       }
   });
   }
   
   
   function add_responsibleuser(clientId)
   {
                                 $(".LoadingImage").show();
   
   var data = {};
   /*data['assign_managed'] = arr('assign_managed');
    data['manager_reviewer'] = arr('manager_reviewer');
   */
        var assign_managed=[];
   $("select[name=assign_managed]").each(function(){
    assign_managed.push($(this).val());
   }); 
   
    var manager_reviewer=[];
   $("select[name=manager_reviewer]").each(function(){
    manager_reviewer.push($(this).val());
   }); 
   
   data['assign_managed'] = assign_managed;
    data['manager_reviewer'] = manager_reviewer;
   
    data['clientId'] = clientId;
    $.ajax({
      url: '<?php echo base_url();?>client/add_responsibleuser/',
    type: "POST",
    data: data,
    success: function(data)  
    {
      add_assigntodepart(clientId);
       //location.reload();  
      // window.location.href="<?php echo base_url().'user'?>"
    }
   });
   }
   
   function add_assigntodepart(clientId)
   {
   var data = {};
   
                               $(".LoadingImage").show();
   
    var depart=[];
   $("select[name=depart]").each(function(){
    depart.push($(this).val());
   }); 
   data['depart'] = depart;
    data['allocation_holder'] = arr('allocation_holder_dept');
    data['clientId'] = clientId;
    $.ajax({
      url: '<?php echo base_url();?>client/add_assigntodepart/',
    type: "POST",
    data: data,
    success: function(data)  
    {
         add_responsiblemember(clientId);
    }
   });
   }
   
   function add_responsiblemember(clientId)
   {
   var data = {};
   /*data['assign_managed'] = arr('assign_managed');
    data['manager_reviewer'] = arr('manager_reviewer');
   */
        /*var assign_managed_member=[];
   $("select[name=assign_managed_member]").each(function(){
    assign_managed_member.push($(this).val());
   });*/                                $(".LoadingImage").show();
   
   
    var manager_reviewer_member=[];
   $("select[name=manager_reviewer_member]").each(function(){
    manager_reviewer_member.push($(this).val());
   }); 
   
   //data['assign_managed_member'] = assign_managed_member;
   data['assign_managed_member'] = arr('assign_managed_member');
    data['manager_reviewer_member'] = manager_reviewer_member;
   
    data['clientId'] = clientId;
    $.ajax({
      url: '<?php echo base_url();?>client/add_responsiblemember/',
    type: "POST",
    data: data,
    success: function(data)  
    {
      
   //alert(save_exit);
   /*if(save_exit=='true')
   {
   window.location.href="<?php echo base_url().'user'?>";
   }else{*/
   
      if($("#user_ids").val()==''){
      $('.alert-success').show();
      <?php 
      if(isset($_SESSION['client_added'])){ ?>
      setTimeout(function(){ window.history.go(-1); }, 2000);
      <?php } ?>
      
   }else{
   
      $('.alert-success-userid').show();
       <?php 
      if(isset($_SESSION['client_section'])){ ?>
       setTimeout(function(){ window.history.go(-3); }, 2000);
       <?php } ?>
   }
      $('.alert-danger').hide();
      $(".LoadingImage").hide();
   var u_d = <?php if($uri_seg!=''){ echo $uri_seg; }else{ echo '0'; }?>;
   if(u_d!='0')
   {
   $.ajax({
      url: '<?php echo base_url();?>client/update_client_statuss/',
    type: "POST",
    data: {status : '1',id:u_d},
    success: function(data)  
    {
    } 
   });
   }
       //location.reload();  
      // window.location.href="<?php echo base_url().'user'?>";
   // }
   }
   });
   }
</script>
<script>
   $(document).ready(function(){
    
   //  var $grid = $('.masonry-container').masonry({
   //    itemSelector: '.accordion-panel',
   //    percentPosition: true,
   //    columnWidth: '.grid-sizer' 
   // });
   
   // $(document).on( 'click', '.basic-info-client1 .switchery', function() {
   //       setTimeout(function(){ $grid.masonry('layout'); }, 600);
   // });  
   
    
   
   $(".registeredtab").hide();
   
   });
   
</script> 
<script type="text/javascript" src="<?php echo base_url()?>assets/js/add_client/checkbox_toggleAction.js"></script>
<script>
   $(document).ready(function(){
       $('.step-tabs').on('click', function(){
           $('html,body').animate({scrollTop: $(this).offset().top}, 800);
       }); 
   });
   
   
   $('.btnNext').click(function(){   
   
             $('.nav-tabs > .bbs').nextAll('li').not(".hide").first().find('a').trigger('click');
   
       }
   );
   
   
     $('.btnPrevious').click(function(){
     $('.nav-tabs > .bbs').prevAll('li').not(".hide").first().find('a').trigger('click');
   });
</script>
<script>
   $(document).ready(function() {
   /*$(".nav-tabs a").click(function(event) {
       event.preventDefault();
       $(this).parent().addClass("bbs");
       $(this).parent().siblings().removeClass("bbs");
   });*/
   $(".nav-tabs li.nav-item a.nav-link").click(function(){
      var id = $(this).attr('data-id');
      $(this).closest(".nav-tabs").find("a.nav-link").removeClass('active');
      $(this).addClass('active');
      $(this).closest(".nav-tabs").find("li.nav-item").removeClass("bbs");
      $(this).parent().addClass("bbs");      
      $(".newupdate_design .tab-content .tab-pane").removeClass("active show");
      $(".newupdate_design .tab-content").find(id).addClass("active show");
   });
   
    $(".th").click(function(event) {
   
        $("#ss").addClass("bbs");
   });
   });
</script>
<script type="text/javascript">
  
  
   $(document).on('click','.yk_pack_addrow_landline',function(e)
   {
 
  $(this).addClass('yyyyyy');
   
   e.preventDefault();
   var id=$(this).data('id');
   var i=1;
   var parent = $(this).parents('.pack_add_row_wrpr_landline');

   var len = parent.find('.yk_pack_addrow_landline').length;
    parent.append('<span class="primary-inner success ykpackrow_landline add-delete-work width_check" id="'+len+'"><div class="data_adds"><select name="pre_landline['+id+']['+len+']" id="pre_landline"><option value="mobile">Mobile</option><option value="work">Work</option><option value="home">Home</option><option value="main">Main</option><option value="workfax">Work Fax</option><option value="homefax">Home Fax</option></select><input type="text" class="text-info" name="landline['+id+']['+len+']" value=""></div><div class="text-right danger-make1"><a href="javascript:;" class="btn btn-danger yk_pack_delrow_landline" id="'+len+'"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true"></i></a></div>   <button type="button" class="btn btn-primary yk_pack_addrow_landline" data-id="'+id+'">Add Landline</button></span>');
   i++;
      // alert($('.yk_pack_addrow_landline').length);
    if(parent.find('.yk_pack_addrow_landline').length > 2){
     // alert('ok');
      $(this).parents('.primary-inner').addClass('check_width');

    }

   
   });
   
   /**************************************************
   landline REMOVE ROW 
   ***************************************************/  
   
     $(document).on('click','.yk_pack_delrow_landline',function(e)
   {
    
      var parent = $(this).parents('.pack_add_row_wrpr_landline');
      $(this).parents('.ykpackrow_landline').remove();

      //  alert($('.yk_pack_addrow_landline').length);
       if( parent.find('.yk_pack_addrow_landline').length == 1)
       {
       //  alert('true');
            parent.find('.yk_pack_addrow_landline').removeClass('yyyyyy');
       }
       else{
           //  alert('false');
                 parent.find('span:last-child .yk_pack_addrow_landline').removeClass('yyyyyy');
          }    //   $(this).parents('.remove_one_row').last('.width_check').addClass('class1');
          
      // alert($('.yk_pack_addrow_landline').length);

     //    $(this).parents('.remove_one_row').last('.ykpackrow_landline').find('.yk_pack_addrow_landline').removeClass('yyyyyy');
   });
  
     $(document).on('click','.yk_pack_addrow_email',function(e)
   {
     //alert('op');
       $(this).addClass('yyyyyy');
      e.preventDefault();
      var id=$(this).data('id');
      var parent = $(this).parents('.pack_add_row_wrpr_email');
      
      var len = parent.find('input[name^="work_email"]').length;
     
      parent.append('<span class="primary-inner success ykpackrow_email add-delete-work"><input type="email" class="text-info" name="work_email['+id+']['+len+']" value=""><div class="text-right danger-make1"><a href="javascript:;" class="btn btn-danger yk_pack_delrow_email"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true"></i></a></div> <button type="button" class="btn btn-primary yk_pack_addrow_email" data-id="'+id+'">Add Email</button></span>');
       // alert($('.yk_pack_addrow_email').length);
     if(parent.find('.yk_pack_addrow_email').length > 2)
     {
         $(this).parents('.primary-inner').addClass('check_width');
      }



   });
   

   
     
   $(document).on('click','.yk_pack_delrow_email',function(e)
   {
       var parent = $(this).closest('.pack_add_row_wrpr_email');
       $(this).parents('.ykpackrow_email').remove();

       if(parent.find('.yk_pack_addrow_email').length == 1)
       {
            parent.find('.yk_pack_addrow_email').removeClass('yyyyyy');
       }
       else
       {
       
         parent.find('span:last-child .yk_pack_addrow_email').removeClass('yyyyyy');
       }    
   });  
   
   
</script>
<script type="text/javascript">
   /*   $(document).ready(function(){
      var url = $(location).attr('href');
      var segments = url.split( '/' );
      //var lm = url.split('/').reverse()[0];
      var cd = '<?php echo $_GET["action"];?>';
      if(cd=='company_house')
      {
         $('#default-Modal').modal('show');
      }
     
      });*/
</script>
<script type="text/javascript">
   $(document).ready(function(){
        var start = new Date();
    start.setFullYear(start.getFullYear() - 70);
    var end = new Date();
    end.setFullYear(end.getFullYear());
     $(".date_picker_dob").datepicker({ dateFormat: 'dd-mm-yy',changeMonth: true,
        changeYear: true,yearRange: start.getFullYear() + ':' + end.getFullYear() }).val();
   $("#select_responsible_type").change(function(){


    // alert('ok');
   var rec = $(this).val();
   if(rec=='staff')
   {
      $('.responsible_user_tbl').show();
      $('.responsible_team_tbl').show();
      $('.responsible_department_tbl').show();
    //  $('.responsible_member_tbl').show();
   
   }else if(rec=='team')
   {
      $('.responsible_team_tbl').show();
      $('.responsible_user_tbl').show();
      $('.responsible_department_tbl').show();
     // $('.responsible_member_tbl').show();
   }else if(rec=='departments')
   {
      $('.responsible_department_tbl').show();
      $('.responsible_user_tbl').show();
      $('.responsible_team_tbl').show();
      //$('.responsible_member_tbl').show();
   }else if(rec=='members')
   {
     // $('.responsible_member_tbl').show();
      $('.responsible_user_tbl').show();
      $('.responsible_team_tbl').show();
      $('.responsible_department_tbl').show();
   
   }
   else{
         $('.responsible_user_tbl').hide();
         $('.responsible_team_tbl').hide();
         $('.responsible_department_tbl').hide();
       //  $('.responsible_member_tbl').hide();
   }
   });
   });
   
   function make_a_primary(id,clientId)
   {
         $(".LoadingImage").show();
   
   //alert(id);
   var data = {};
   data['contact_id'] = id;
   data['clientId'] = clientId;
   
   $.ajax({
      url: '<?php echo base_url();?>client/update_primary_contact/',
       type: "POST",
       data: data,
       success: function(data)  
       {
           $("#contactss").html(data);
               $(".LoadingImage").hide();
               $(".contactcc").hide();
   
            // location.reload();
   
         //alert('success');
       }
     });
   
   }
   
   
    //add contect from company house.
     $(".personss").click(function(){

    var companyNo=$("#company_number").val();
    var user_id = <?php echo $uri_seg; ?>
      
      $.ajax({
          url: '<?php echo base_url();?>client/selectcompany/'+user_id,
          type: 'post',
          dataType: 'JSON',
          data: { 'companyNo':companyNo},
          beforeSend : function(){$('.LoadingImage').show();},
          success: function( data ){
            $('#company_house_contact').find('.main_contacts').html(data.html).show().find('.view_contacts').hide();
            $('#company_house_contact').modal('show');
            $('.LoadingImage').hide();
         },
         error: function( errorThrown ){
            $('#company_house_contact').find('.main_contacts').html('Data Not found.').show();
            $('#company_house_contact').modal('show');
            $('.LoadingImage').hide();
         }

      });
   
      });   
   
   
   
    $("#Add_New_Contact").click(function(){
    //  $('.for_contact_validation').html('');
      //$('#main_Contact_error').html('');
       var cnt = $("#append_cnt").val();
   
               if(cnt==''){
               cnt = 1;
              }else{
               cnt = parseInt(cnt)+1;
              }
           $("#append_cnt").val(cnt).valid(); //for auto remove jquery validation error if set.
   
          $.ajax({
             url: '<?php echo base_url();?>client/new_contact/',
             type: 'post',
             
             data: { 'cnt':cnt,'incre':'1'},

              beforeSend: function() {
                        $(".LoadingImage").show();
                      },
             success: function( data ){
   
                 $('.contact_form').append(data);

                     $(".LoadingImage").hide();
                   var start = new Date();
    start.setFullYear(start.getFullYear() - 70);
    var end = new Date();
    end.setFullYear(end.getFullYear());
                 $(".date_picker_dob").datepicker({ dateFormat: 'dd-mm-yy',changeMonth: true,
        changeYear: true,yearRange: start.getFullYear() + ':' + end.getFullYear() }).val();
                 /** for append remove button 04-07-2018**/
                 if(cnt>1){
                  $('.for_remove-'+cnt).remove();
                 $('.for_row_count-'+cnt).append('<div class="btn btn-danger remove for_remove-'+cnt+'"><a href="javascript:void(0)" class="contact_remove" id="remove-'+cnt+'">Remove</a></div>');
                 }
               var countofdiv=$('.make_a_primary').length;
               //alert(countofdiv);
   //             if(countofdiv>1)
   //             {
   //       $('.for_remove-1').remove();
   // $('.for_row_count-1').append('<div class="remove for_remove-1"><a href="javascript:void(0)" class="contact_remove" id="remove-1">Remove</a></div>');
   //             }
               
                 $('.make_a_primary').each(function(){
                     var countofdiv=$('.make_a_primary').length;
                     if(countofdiv>1){
                     var id=$(this).attr('id').split('-')[1];
                     $('.for_remove-'+id).remove();
                   $('.for_row_count-'+id).append('<div class="btn btn-danger remove for_remove-'+id+'"><a href="javascript:void(0)" class="contact_remove" id="remove-'+id+'">Remove</a></div>');
                  }
   
                  });
                 /** end of remove button **/
                   $(".date_picker").datepicker({ dateFormat: 'dd-mm-yy',
                     //minDate:0,
                      changeMonth: true,
        changeYear: true, }).val();
                  // $('.').datepicker({
                  // format: 'dd-mm-yyyy',
                  // startDate: '-3d'
                  // });
   
                 },
                 error: function( errorThrown ){
                     console.log( errorThrown );
                 }
             });
    });
   
      $(document).on("click", ".fields.edit_classname.datepicker.hasDatepicker", function() {
   
         // alert('hi');
   
      $(".date_picker").datepicker({ dateFormat: 'dd/mm/yy',
         //minDate:0,
          changeMonth: true,
        changeYear: true, }).val();
         
      }); 
   
   
   
   
   
</script>
<style type="text/css">
   .disabled{
   opacity: 0.5;
   pointer-events: none;    
   }
</style>
<script type="text/javascript">
   $(document).on('click','.add_merge',function(e)
   {
   //$(".add_merge").on('click',function() { // bulk checked
       
           var target = $(this).attr('target');
           var name=$(this).html();
           
           $('[name='+target+']').append(name);
          
         });   
   
   
   
   $(document).on('keyup',".allocation_holder_cls",function(){     
   
         var album_text = [];
   
   $("input[name='allocation_holder[]']").each(function() {
       var value = $(this).val();
       if (value) {
           album_text.push(value);
       }
   });
   if (album_text.length === 0) {
     $('.yk_pack_addrow_td').css('display','none');
   }
   
   else {
     $('.yk_pack_addrow_td').css('display','block');
   } 
       
   
   
   });
   
   $(document).on('keyup',".allocation_holder_dept_cls",function(){
   var album_text = [];
   $("input[name='allocation_holder_dept[]']").each(function() {
       var value = $(this).val();
       if (value) {
           album_text.push(value);
       }
   });
   if (album_text.length === 0) {
     $('.yk_pack_addrow_tr').css('display','none');
   }
   
   else {
     $('.yk_pack_addrow_tr').css('display','block');
   }
   });
    
   
</script>
<script type="text/javascript">
   $(document).ready(function(){  
        $('#refer_exist_client').keyup(function(){  
         // alert('text');
             var query = $(this).val();  
             if(query != '')  
             {  
                  $.ajax({  
                       url:"<?php echo base_url();?>client/search_client/",  
                       method:"POST",  
                       data:{query:query},  
                       success:function(data)  
                       {  
                            $('#searchresultclient').fadeIn();  
                            $('#searchresultclient').html(data);  
                       }
                  });  
             }
             if(query=='')
             {
               $('#searchresultclient').fadeOut();
             }  
        });  
        $(document).on('click', '.client_name', function(){  
             $('#refer_exist_client').val($(this).text()); 
             //$('#user_id').val($(this).data('id')); 
             $('#searchresultclient').fadeOut();  
        });  
   });
</script>
<script type="text/javascript">
   $(document).ready(function(){  
        $(document).on('change','#placeholder',function(e){ 
         // alert('text');
             var query = $(this).val();  
             if(query != '')  
             {  
                  $.ajax({  
                       url:"<?php echo base_url();?>client/get_placeholder/",  
                       method:"POST",  
                       data:{query:query},  
                       success:function(data)  
                       {  
                            $('.append_placeholder').fadeIn();  
                            $('.append_placeholder').html(data); 
        /*$(".add_merge").on('click',function() { // bulk checked
        var target = $(this).attr('target');
        var name=$(this).html();
        
        $('[name='+target+']').append(name);
       
      }); */
                       }
                  });  
             }
             if(query=='')
             {
               $('.append_placeholder').fadeOut();
             }  
        }); 
   
   
   });
</script>
<script type="text/javascript">
  
  /* function getConfirmation_RemoveCusReminder(aspf_obj,cus_obj)
   {
      console.log("inside getConfirmation_RemoveCusReminder");
      var action = function()
      {
         $.ajax({ url:"<?php echo base_url();?>client/Remove_CusServiceReminder",
                  data:{'id':cus_obj.val()},
                  type:"POST",
                  dataType:"json",
                  beforeSend:Show_LoadingImg,
                  success:function(data)
                  {
                     if(data.result)
                     {
                        Hide_LoadingImg();
                        cus_obj.trigger('click');;
                     }
                  }
               });
      };
      var C_ation = function()
      {
         aspf_obj.trigger('click');
      };

      Conf_Confirm_Popup({'OkButton':{'Handler':action},'CancelButton':{'Handler':C_ation},'Close':{'Handler':C_ation},'Info':'Do You Want Remove Custom Services Reminder...!'}); 
      $('#Confirmation_popup').modal('show');
   }
  */
   
   
   
      $(document).ready(function(){
          $(document).on('change','.as_per_firm',function(){

   if($(this).prop('checked') == true)
   {
      $(this).closest('.panel-collapse').find('input[type="checkbox"]:checked.cus_reminder').trigger('click');            
      /*if( cus_obj.is(':checked') && cus_obj.val()!="")
      {     
        getConfirmation_RemoveCusReminder($(this),cus_obj);
      }*/
   }
/*
      if($(this).prop('checked') == false)
      {

         if(id=="Pension_next_reminder_date"){ // penion
             $(".pension_create_task_reminder_label").css('display','inline-block');         
         }
         if(id=="Cis_next_reminder_date"){ // 
             $(".cis_add_custom_reminder_label").css('display','inline-block');         
         }
         if(id=="bookkeep_next_reminder_date"){ // 
             $(".bookkeep_add_custom_reminder_label").css('display','inline-block');         
         }
         if(id=="P11d_next_reminder_date"){ // 
             $(".p11d_add_custom_reminder_label").css('display','inline-block');         
         }
         if(id=="investigation_next_reminder_date"){ // 
             $(".investigation_add_custom_reminder_label").css('display','inline-block');         
         }
         if(id=="registered_next_reminder_date"){ // registered 
             $(".registered_add_custom_reminder_label").css('display','inline-block');         
         }
         
         
        
      if(id=="confirmation_next_reminder_date"){
          $(".custom_remain").css('display','block');         
      }if(id=="accounts_next_reminder_date")
      {
         $('.accounts_custom_reminder_label').css('display','inline-block');
      }if(id=="Personal_next_reminder_date")
      {
         $('.personal_custom_reminder_label').css('display','inline-block');
      }if(id=="Payroll_next_reminder_date")
      {
         $('.payroll_add_custom_reminder_label').css('display','inline-block');
      }if(id=="cissub_next_reminder_date")
      {
         $('.cissub_add_custom_reminder_label').css('display','inline-block');
      }if(id=="Vat_next_reminder_date")
      {
         $('.vat_add_custom_reminder_label').css('display','inline-block');
      }if(id=="Vat_next_reminder_date")
      {
         $('.vat_add_custom_reminder_label').css('display','inline-block');
      }if(id=="manage_next_reminder_date")
      {
         $('.manage_add_custom_reminder_label').css('display','inline-block');
      }if(id=="insurance_next_reminder_date")
      {
         $('.insurance_add_custom_reminder_label').css('display','inline-block');
      }
      }
      else{
        
         if(id=="company_next_reminder_date"){ // company tax
             $(".company_custom_reminder_label").css('display','none');         
         }
         if(id=="Pension_next_reminder_date"){ // penion
             $(".pension_create_task_reminder_label").css('display','none');         
         }
         if(id=="Cis_next_reminder_date"){ // 
             $(".cis_add_custom_reminder_label").css('display','none');         
         }
         if(id=="bookkeep_next_reminder_date"){ // 
             $(".bookkeep_add_custom_reminder_label").css('display','none');         
         }
        
         if(id=="investigation_next_reminder_date"){ // 
             $(".investigation_add_custom_reminder_label").css('display','none');         
         }
         if(id=="registered_next_reminder_date"){ // registered 
             $(".registered_add_custom_reminder_label").css('display','none');         
         }
         if(id=="P11d_next_reminder_date"){ // p11d 
             $(".p11d_add_custom_reminder_label").css('display','none');         
         }
         
        
          
      if(id=="confirmation_next_reminder_date"){
     //  $('.add_custom_reminder_label').css('display','none');
         $(".custom_remain").css('display','none');
      }if(id=="accounts_next_reminder_date")
      {
         $('.accounts_custom_reminder_label').css('display','none');
      }if(id=="Personal_next_reminder_date")
      {
         $('.personal_custom_reminder_label').css('display','none');
      }if(id=="Payroll_next_reminder_date")
      {
         $('.payroll_add_custom_reminder_label').css('display','none');
      }if(id=="cissub_next_reminder_date")
      {
         $('.cissub_add_custom_reminder_label').css('display','none');
      }if(id=="Vat_next_reminder_date")
      {
         $('.vat_add_custom_reminder_label').css('display','none');
      }if(id=="Vat_next_reminder_date")
      {
         $('.vat_add_custom_reminder_label').css('display','none');
      }if(id=="manage_next_reminder_date")
      {
         $('.manage_add_custom_reminder_label').css('display','none');
      }if(id=="insurance_next_reminder_date")
      {
         $('.insurance_add_custom_reminder_label').css('display','none');
      }
      }*/
      
   });
   
      $(".reminder_close").click(function(){
         $('input[name="'+$('#clicked_checkbox_name').val()+'"]').trigger('click');
       }); 

      $(".edit_field_form").submit(function () {

            console.log($(".edit_field_form").find("input[name='service_id']").val());
      
            var clikedForm = $(this); // Select Form
            var user_id = $('#user_id').val();
            var days = clikedForm.find("input[name='days']").val().trim();
            var subject = clikedForm.find("input[name='subject']").val().trim();
            var body = clikedForm.find("textarea[name='message']").val().trim();
            if(days=='')
            {
               $('.error_msg_days').show();
            }
            else
            {
               $('.error_msg_days').hide();
            }
            if(subject=='')
            {
               $('.error_msg_subject').show();
            }
            else
            {  

               $('.error_msg_subject').hide();
            }
            if(body=='')
            {

               $('.error_msg_message').show();
            }
            else
            {

               $('.error_msg_message').hide();
            }
            
                 
            /*if (clikedForm.find("[name='name']").val() == '') {
               //$('.error').show();
               $(this).find('.error').show();
              //alert('Enter Valid name');
             
            } else {$('.error').hide();}*/
            if(days!='' && subject!='' && body!='')
            {
                  
               $.ajax({
               type: "POST",
               dataType:'json',
               url: '<?php echo base_url();?>client/AddCustom_ServiceReminder/',
               data: $(this).serialize()+"&user_id="+user_id,
               beforeSend:Show_LoadingImg,
               success: function (data) { 
                  // Inserting html into the result div
                  //console.log(data);
                  Hide_LoadingImg();
                  if(data.result != 0)
                  {
                     $('input[name="'+$("#clicked_checkbox_name").val()+'"]').val(data.result);
                     $('.alert-success-reminder').show();
                     setTimeout(function() { 
                        $('.alert-success-reminder').hide();
                        //$('#add-reminder').css('display','none');
                        $('#add-reminder').modal('hide');
                        $('.modal-backdrop.show').hide();
                     },1000);
                  }
             
               },
               error: function(jqXHR, text, error){
                  // Displaying if there are any errors
                     $('#result').html(error);           
              }
          });
            return false;
            }
            else
            {
                return false;
            }
           
           
      
          });
      });
</script>
<div id="edit_confirmation1" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" style="text-align-last: center">Confirmation</h4>
         </div>
         <div class="modal-body">
            <p>Are you want edit this information</p>
         </div>
         <div class="modal-footer profileEdit">
            <input type="hidden" name="hidden">
            <a href="javascript:void();" id="acompany_name" data-dismiss="modal" class="delcontact">Yes</a>
            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
   $(function() {
               $('.click_code03').on('click', function( e ) {
             var text = $(this).parents('.edit-field-popup1').find('.edit_classname').attr('id');
             $('[name="hidden"]').val(text);
           });
   });
   
   $(document).ready(function () {
           $('.profileEdit a').click(function (elem) {
         $('#'+$('[name="hidden"]').val()).attr("readonly", false); 
         var id=$('[name="hidden"]').val();
         if('date_of_creation'==id || 'confirm_next_made_up_to'== id || 'confirm_next_due'==id || 'next_made_up_to'==id || 'next_due'==id ){
   
         $('#'+id).addClass('dob_picker');
          $(".dob_picker").datepicker({ dateFormat: 'dd-mm-yy',
            //minDate:0,
             changeMonth: true,
        changeYear: true, }).val();
       } 
         $('.modal').modal('hide');
           });
   });
</script>
<script> 
   $(".exit").click(function(){
   //alert('not save')
   location.reload();
   });
   
   
   
   $('.atleta').click(function(e) {
      e.preventDefault();
      var h = $("a",this).attr('href');
   
        $("#myAlert").modal(); 
   
        $(".exit").click(function(){
            window.location.href = h;
      });
      
   });
   
   
   $(".save").click(function(e) {
   
    $('.body-popup-content input, textarea, select').each(function () {
      
      var id = $(this).attr('id');    
   
   //  console.log($(this).prop('type'));
   //   console.log(id);
   // console.log($(this).val());
   
      //input box
      if($(this).prop('type') == 'text')
         $('#'+id).val($(this).val());
   
      if($(this).prop('type') == 'number')
         $('#'+id).val($(this).val());
      
      if($(this).prop('type') == 'textarea')
      {
        // alert($(this).val());
     //    $('#'+id).val($(this).val());   
      }
   
     
   
      if($(this).prop('type') == 'select-one')
         $('#'+id).val($(this).val());
   
   
    if($(this).prop('type') == 'checkbox'){
         var id = $(this).attr('id');
         
         if($(this).attr('data-switchery'))
         {
            if($(this).prop('checked') != $('#'+id).prop('checked'))
            {
               $('#'+id).next('.switchery').trigger('click');
            }
         }
         else
         {
            if($('#'+id).prop('checked','true')){
       //  alert(id);
         $('#'+id).prop('checked','true');
         $('#'+id).parents(".col-sm-8").addClass("switttt");
         //$('#'+id).find();
          //$('div.switttt .switchery').trigger('click'); 
      }
         }
   
   
      
   }
                   
         // checkbox();
      });      
   
   
    
   
      });
   
   /*$(document).on('change','.othercus',function(e){
      });*/
   
   
   
</script>  
<script type="text/javascript">
   $(document).ready(function(){
   /*<?php if($uri_seg!=''){ ?>
           $('.basic_details_tab').show();
      
            $('.basic_details_tab').addClass('bbs');
            $('.basic_deatils_id').addClass('active');
            $('#basic_details').addClass('active');
            $('.for_contact_tab').removeClass('bbs');
            $('.main_contacttab').removeClass('active');
            $('#main_Contact').removeClass('active');
      <?php } ?>*/
   });
    $(document).on('change', '.sel-legal-form', function() {
      var selectVal = $(this).find(':selected').val();
      // company type value
      
      $('#company_type').val(selectVal);
      
      // alert(selectVal);
   
      if((selectVal == "Private Limited company") || (selectVal == "Public Limited company") || (selectVal == "Limited Liability Partnership")) {
   
         $('.filing-alcat-tab').show();
         $('.business-info-tab').hide();
         // $('.business-info-tab').show();
         /** 16-08-2018 shown shown basic details tab **/
         $('.basic_details_tab').show();
       /*  <?php if(isset($client)){ ?>
            $('.basic_details_tab').addClass('bbs');
            $('.basic_deatils_id').addClass('active');
            $('#basic_details').addClass('active');
            $('.for_contact_tab').removeClass('bbs');
            $('.main_contacttab').removeClass('active');
            $('#main_Contact').removeClass('active');
         <?php } ?>*/
         /** end of 16-08-2018 **/
      }
      else if((selectVal == "Partnership") || (selectVal == "Self Assessment")) {
   
         $('.business-info-tab').show();
         $('.filing-alcat-tab').hide();
         /** 16-08-2018 shown hide basic details tab **/
         $('.basic_details_tab').hide();
         /*  <?php if(isset($client)){ ?>
         $('.for_contact_tab').addClass('bbs');
           $('.main_contacttab').addClass('active');
            $('#main_Contact').addClass('active');
            $('.basic_details_tab').removeClass('bbs');
              $('.basic_deatils_id').removeClass('bbs');
             $('#basic_details').removeClass('active');
            <?php } ?>*/
         /** end of 16-08-2018 **/
      }
      else
      {
         $('.business-info-tab').hide();
         $('.filing-alcat-tab').hide();
         /** 16-08-2018 shown hide basic details tab **/
         $('.basic_details_tab').hide();
                /* <?php if(isset($client)){ ?>
     $('.for_contact_tab').addClass('bbs');
           $('.main_contacttab').addClass('active');
            $('#main_Contact').addClass('active');
            $('.basic_details_tab').removeClass('bbs');
              $('.basic_deatils_id').removeClass('bbs');
             $('#basic_details').removeClass('active');
            <?php } ?>*/
         /** end of 16-08-2018 **/
      }
    });  
   $('.sel-legal-form').trigger('change');

   $(document).on('click','.contact_remove',function(){
    var countno=$(this).attr('id').split('-')[1];
    var parent = $('.common_div_remove-'+countno);

       var action  = function ()
       {
         if( parent.find('.contact_table_id').length )
         {
            var id = parent.find('.contact_table_id').val();
            $.ajax(
               {
                  url:"<?php echo base_url();?>client/Client_Contact_Delete/"+id,
                  beforeSend:Show_LoadingImg,
                  success:function(res){ Hide_LoadingImg();}
               });
         }
         parent.remove();           
         var countofdiv = $('.make_a_primary').length;
         if(countofdiv == 1)
         {
            $('.contact_remove').css('display','none');
            $('.btn.btn-danger.remove').css('display','none');
         }
         $('#Confirmation_popup').modal('hide');
      };
    
      Conf_Confirm_Popup({'OkButton':{'Handler':action},'Info':'Do You Want Remove This Contact...!'}); 
      $('#Confirmation_popup').modal('show');
    });
   
   
   
    $(document).ready(function(){
   $('.make_a_primary').each(function(){
      var countofdiv=$('.make_a_primary').length;
      if(countofdiv>1){
      var id=$(this).attr('id').split('-')[1];
      $('.for_remove-'+id).remove();
    $('.for_row_count-'+id).append('<div class="btn btn-danger remove for_remove-'+id+'"><a href="javascript:void(0)" class="contact_remove" id="remove-'+id+'">Remove</a></div>');
   }
   
   });
    });
     $(document).ready(function() {
   
      $('.dropdown-sin-2').dropdown({
     
        input: '<input type="text" maxLength="20" placeholder="Search">'
      });
      $('.dropdown-sin-3').dropdown({
     
        input: '<input type="text" maxLength="20" placeholder="Search">'
      });
      $('.dropdown-sin-11').dropdown({
     
        input: '<input type="text" maxLength="20" placeholder="Search">'
      });
      
   
   });
     $(document).on('change','#source',function(){
            var value=$(this).val();
            if(value=='Existing Client')
            {
               $('.dropdown_source').css('display','');
               $('.textfor_source').css('display','none');
            }
            else
            {
               $('.dropdown_source').css('display','none');
               $('.textfor_source').css('display','');
            }
     });

   
    $('.decimal_days').keyup(function(evt){
      var text = $(this).val();
      var test_value = text.replace(/[^0-9]+/g, "");
      $(this).val(test_value);
    });

   $('.decimal').keyup(function(){
    console.log('#####');
    var val = $(this).val();
    if(isNaN(val)){
         val = val.replace(/[^0-9\.]/g,'');
         if(val.split('.').length>2) 
             val =val.replace(/\.+$/,"");
    }
    $(this).val(val); 
});

$(document).ready(function () {
  //called when key is pressed in textbox
  $(".due-days").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
              return false;
    }
   });
});


/** 11-09-2018 **/
   $(document).on('keyup',"input",function(){

      var name=$(this).attr('name');
      var thisval=$(this).val();
      var type=$(this).attr('type');   
  
      if(name == 'company_name' && $('#company_url_anchor').length )
      {
         $('#company_url_anchor').text(thisval);
      }

      var vallen=$("input[name^='"+name+"']").length;
      //alert(vallen);
      if(vallen >1 && (type=="text" || type=="number"))
      {
         $("[name^='"+name+"']").each(function() {
            $(this).val(thisval);
         });
      }

    });
   $(document).on('keyup',"textarea",function(){
   var name=$(this).attr('name');
   var thisval=$(this).val();
   var vallen=$("[name^="+name+"]").length;
   console.log(thisval);
   console.log(name);
   if(vallen >1){
          $("[name^="+name+"]").each(function() {
           $(this).val(thisval);
         });
       }
  
   });

/** end of 11-09-2018 **/
/** 14-09-2018 for client section **/
$(document).on('click','.make_primary_section',function(){
   $(".LoadingImage").show();
   //$('.for_contact_validation').html('');
   var its_data_id=$(this).attr('data-id');
   $('#'+its_data_id).prop('checked', true).valid();
   $('.make_primary_section').each(function(){
      $(this).html('<span>Make A Primary</span>');
      $(this).removeClass('active');
   });
   $(this).html('<span style="color:red;">Primary Contact</span>');
   $(this).addClass('active');
   $(".LoadingImage").hide();
});



$( document ).ready(function() {
var mylist = $('#basic_details').find('#important_details_section');
var listitems = $('#basic_details').find('#important_details_section').children('.sorting').get();
listitems.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems, function(idx, itm) { 
    mylist.append(itm);
});
var mylist1 = $('#confirmation-statement').find('#box1');
var listitems1 = $('#confirmation-statement').find('#box1').children('.sorting_conf').get();
listitems1.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems1, function(idx, itm) {
    mylist1.append(itm);
})
var mylist2 = $('#confirmation-statement').find('#box2');
var listitems2 = $('#confirmation-statement').find('#box2').children('.sorting_conf').get();
listitems2.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems2, function(idx, itm) { 
    mylist2.append(itm);
})
var mylist3 = $('#confirmation-statement').find('#box3');
var listitems3 = $('#confirmation-statement').find('#box3').children('.sorting_conf').get();
listitems3.sort(function(a, b) { 
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems3, function(idx, itm) { 
    mylist3.append(itm);
})
var mylist4 = $('#personal-tax-returns').find('#personal_box');
var listitems4 = $('#personal-tax-returns').find('#personal_box').children('.personal_sort').get();
listitems4.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems4, function(idx, itm) { 
    mylist4.append(itm);
})
var mylist4 = $('#personal-tax-returns').find('#personal_box1');
var listitems4 = $('#personal-tax-returns').find('#personal_box1').children('.personal_sort').get();
listitems4.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems4, function(idx, itm) { 
    mylist4.append(itm);
})
var mylist4 = $('#personal-tax-returns').find('#personal_box2');
var listitems4 = $('#personal-tax-returns').find('#personal_box2').children('.personal_sort').get();
listitems4.sort(function(a, b) { 
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems4, function(idx, itm) {
    mylist4.append(itm);
})
var mylist5 = $('#vat-Returns').find('#vat_box');
var listitems5 = $('#vat-Returns').find('#vat_box').children('.vat_sort').get();
listitems5.sort(function(a, b) { 
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems5, function(idx, itm) {
  // console.log(idx);
    mylist5.append(itm);
})
var mylist6 = $('#vat-Returns').find('#vat_box1');
var listitems6 = $('#vat-Returns').find('#vat_box1').children('.vat_sort').get();
listitems6.sort(function(a, b) {  
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems6, function(idx, itm) { 
    mylist6.append(itm);
})
var mylist7 = $('#management-account').find('#management_account');
var listitems7 = $('#management-account').find('#management_account').children('.management').get();
listitems7.sort(function(a, b) {  
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems7, function(idx, itm) {
    mylist7.append(itm);
})
var mylist8 = $('#management-account').find('#management_account1');
var listitems8 = $('#management-account').find('#management_account1').children('.management').get();
listitems8.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems8, function(idx, itm) {
    mylist8.append(itm);
})
var mylist9 = $('#management-account').find('#management_box1');
var listitems9 = $('#management-account').find('#management_box1').children('.management').get();
listitems9.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems9, function(idx, itm) {
    mylist9.append(itm);
})
var mylist10 = $('#management-account').find('#management_box2');
var listitems10 = $('#management-account').find('#management_box2').children('.management').get();
listitems10.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems10, function(idx, itm) {
    mylist10.append(itm);
})

var mylist11 = $('#investigation-insurance').find('#invest-box');
var listitems11 = $('#investigation-insurance').find('#invest-box').children('.invest').get();
listitems11.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems11, function(idx, itm) {
    mylist11.append(itm);
})
var mylist13 = $('#investigation-insurance').find('#invest_box2');
var listitems13 = $('#investigation-insurance').find('#invest_box2').children('.invest').get();
listitems13.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems13, function(idx, itm) {
 //  console.log(idx);
    mylist13.append(itm);
})
var mylist12 = $('#other').find('#others_section');
var listitems12 = $('#other').find('#others_section').children('.others_details').get();
listitems12.sort(function(a, b) { 
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems12, function(idx, itm) {
  // console.log(idx);
    mylist12.append(itm);
})
var mylist14 = $('#other').find('#other_details1');
var listitems14 = $('#other').find('#other_details1').children('.others_details').get();
listitems14.sort(function(a, b) { 
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems14, function(idx, itm) {
 //  console.log(idx);
    mylist14.append(itm);
})
var mylist15 = $('#referral').find('#referals');
var listitems15 = $('#referral').find('#referals').children('.referal_details').get();
listitems15.sort(function(a, b) { 
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems15, function(idx, itm) {
  // console.log(idx);
    mylist15.append(itm);
})
/* Accounts */
var mylist16 = $('#accounts').find('#accounts_box');
var listitems16 = $('#accounts').find('#accounts_box').children('.accounts_sec').get();
listitems16.sort(function(a, b) { 
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems16, function(idx, itm) {
 //  console.log(idx);
    mylist16.append(itm);
})
var mylist17 = $('#accounts').find('#accounts_box1');
var listitems17 = $('#accounts').find('#accounts_box1').children('.accounts_sec').get();
listitems17.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems17, function(idx, itm) {
 //  console.log(idx);
    mylist17.append(itm);
})
var mylist18 = $('#accounts').find('#accounts_box2');
var listitems18 = $('#accounts').find('#accounts_box2').children('.accounts_sec').get();
listitems18.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems18, function(idx, itm) {
 //  console.log(idx);
    mylist18.append(itm);
})
var mylist19 = $('#accounts').find('#accounts_box3');
var listitems19 = $('#accounts').find('#accounts_box3').children('.accounts_sec').get();
listitems16.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems19, function(idx, itm) {
  // console.log(idx);
    mylist19.append(itm);
})

var mylist20 = $('#accounts').find('#accounts_box4');
var listitems20 = $('#accounts').find('#accounts_box4').children('.accounts_sec').get();
listitems20.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems20, function(idx, itm) {
  // console.log(idx);
    mylist20.append(itm);
})
var mylist21 = $('#payroll').find('#payroll_box');
var listitems21 = $('#payroll').find('#payroll_box').children('.payroll_sec').get();
listitems21.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems21, function(idx, itm) {
 //  console.log(idx);
    mylist21.append(itm);
})

var mylist22 = $('#payroll').find('#payroll_box1');
var listitems22 = $('#payroll').find('#payroll_box1').children('.payroll_sec').get();
listitems22.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems22, function(idx, itm) {
 //  console.log(idx);
    mylist22.append(itm);
})
var mylist23 = $('#payroll').find('#payroll_box2');
var listitems23 = $('#payroll').find('#payroll_box2').children('.payroll_sec').get();
listitems23.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems23, function(idx, itm) {
 //  console.log(idx);
    mylist23.append(itm);
})
var mylist24 = $('#payroll').find('#workplace_sec');
var listitems24 = $('#payroll').find('#workplace_sec').children('.workplace').get();
listitems24.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems24, function(idx, itm) {
  // console.log(idx);
    mylist24.append(itm);
})
var mylist25 = $('#payroll').find('#workplace_sec1');
var listitems25 = $('#payroll').find('#workplace_sec1').children('.workplace').get();
listitems25.sort(function(a, b) { 
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems25, function(idx, itm) {
//console.log(idx);
    mylist25.append(itm);
})
var mylist26 = $('#payroll').find('#p11d_sec');
var listitems26 = $('#payroll').find('#p11d_sec').children('.p11d').get();
listitems26.sort(function(a, b) {
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems26, function(idx, itm) {
  // console.log(idx);
    mylist26.append(itm);
})


var listitems27 = $('#payroll').find('#p11d_sec1').children('.p11d').get();
listitems27.sort(function(a, b) {  
    var compA = $(a).attr('id').toUpperCase();
    var compB = $(b).attr('id').toUpperCase();
    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
})
$.each(listitems27, function(idx, itm) {
  // console.log(idx);
    mylist27.append(itm);
})
});



$( document ).ready(function() { 
//console.log( $(".add_custom_fields_section").html());

$( "div.add_custom_fields_section div.form-group").each(function( index ) {
  console.log( index + ": " + $( this ).attr('class')+ "ID :" +  $( this ).attr('id') );
  var goal=$( this ).attr('id');
  if ( $( this ).hasClass( "personal_sort" ) ) { 
        append_text_section('personal-tax-returns','personal_sort',index,goal);
   }

    if ( $( this ).hasClass( "sorting" ) ) { 
        append_text_section('basic_details','sorting',index,goal);
   }

   if ( $( this ).hasClass( "accounts_sec" ) ) {          
         append_text_section('accounts','accounts_sec',index,goal); 
   }

   if ( $( this ).hasClass( "workplace" ) ) {  
         append_text_section('payroll','workplace',index,goal);    
   }

   if ( $( this ).hasClass( "p11d" ) ) {   
         append_text_section('payroll','p11d',index,goal);
     
   }

   if ( $( this ).hasClass( "payroll_sec" ) ) {        
          append_text_section('payroll','payroll_sec',index,goal);      
   }

   if ( $( this ).hasClass( "vat_sort" ) ) {       
         append_text_section('vat-Returns','vat_sort',index,goal);     
   }

   if ( $( this ).hasClass( "management" ) ) {        
         append_text_section('management-account','management',index,goal);      
   }

   if ( $( this ).hasClass( "invest" ) ) {        
         append_text_section('investigation-insurance','invest',index,goal);     
   }

   if ( $( this ).hasClass( "others_details" ) ) {        
        append_text_section('other','others_details',index,goal);   
   }

    if ( $( this ).hasClass( "sorting_conf" ) ) {        
        append_text_section('confirmation-statement','sorting_conf',index,goal);   
   }
});

   });



function append_text_section(parent,value,index,goal){

   // console.log($('#'+parent+' .'+value).html());
         var myarray_parents=[];
         var myarray = [];

         $('#'+parent+' .'+value).each(function(){

            var condition = $(this).parents().attr('class');

            if(condition != 'add_custom_fields_section')
            {
               myarray.push($(this).attr('id')); 
               myarray_parents.push($(this).parents().attr('id'));                 
           }      
         });
          var closest = null;  
              values = myarray;

              userNum = goal; //This is coming from user input

               smallestDiff = Math.abs(userNum - values[0]);
               closest = 0; //index of the current closest number
               for (i = 1; i < values.length; i++) {
                  currentDiff = Math.abs(userNum - values[i]);
                  if (currentDiff < smallestDiff) {
                     smallestDiff = currentDiff;
                     closest = i;
                  }
               }      

            var search_value=values[closest];                
               var position=jQuery.inArray( search_value, myarray );
               var id_value=myarray[position];                
               var parent_value=myarray_parents[position]; 
               var next_index=index +1;
               console.log(id_value);
            //   console.log(value);  
             //   console.log(next_index);   
            //    console.log('*************'+goal+'****************');      
               var consume=$("#"+parent_value).find("#"+id_value);  

            //  console.log($('.add_custom_fields_section').html());
            //   console.log($(".add_custom_fields_section div."+value+",#"+goal+""));
             //     console.log( $("div.add_custom_fields_section  div."+value+"#"+goal+"").html());
//
                 //  console.log( $("div.add_custom_fields_section  .form-group:nth-child(1)").html());

              // $(".add_custom_fields_section .form-group:nth-child("+next_index+")").insertAfter(consume); 

               $("div.add_custom_fields_section  .form-group:nth-child(1)").insertAfter(consume);  
}

/** end of 14-09-2018 **/
/*$(document).on('click', '.Edit_contact', function(){

$(this).parents(".space-required").find('.primary-inner').each(function(  ) {
   $(this).removeClass('field_hide');
});
  
});*/




$("#email_template").change(function(){
var id=$(this).val();
   $.ajax({
                   url: '<?php echo base_url();?>User/get_template',
                   type : 'POST',
                   data : {'id':id},                     
                   beforeSend: function()
                   {
                       $(".LoadingImage").show();
                   },
                   success: function(msg)
                   {

                       $(".LoadingImage").hide();                   
                       var json = JSON.parse(msg); 

                       $("#add-reminder .invoice-msg").val(json['subject']);

                       $("#add-reminder #editor2").html(json['body']);
            
                    }

          });
});




/*$(document).ready(function() {   

 $('.alert-ss').parent().addClass('cmn-errors1')

});*/


$(".alert_close").click(function(){
   window.location.href = '<?php echo base_url(); ?>user';
});


</script>

<script type="text/javascript">
  $(document).ready(function(){
     
  $('.js-small').parent('.col-sm-8').addClass('addbts-radio');
  $('.accordion-views .toggle').click(function(e) {
$(this).next('.inner-views').slideToggle(300);
$(this).toggleClass('this-active');
});
  });


  
</script>

<?php  //$this->load->view('includes/footer_permission_script'); ?>
</body>
</html>