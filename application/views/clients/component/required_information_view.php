<?php $rel_id = !empty( $client['user_id'] )? $client['user_id'] : '' ;?>
<div class="basic-info-client1 CONTENT">

   <div class="form-group row name_fields sort" data-id="<?php echo $Setting_Order['crm_company_name']; ?>">
      <label class="col-sm-4 col-form-label">
         <?php echo $Setting_Label['crm_company_name']; ?>
         <span class="Hilight_Required_Feilds">*</span>
       </label>
      <div class="col-sm-8">
         <input type="text" name="company_name" id="company_name" placeholder="" value="<?php if(isset($client['crm_company_name']) && ($client['crm_company_name']!='') ){ echo $client['crm_company_name'];}?>" class="fields">
      </div>
   </div>

   <div class="form-group row name_fields sort" data-id="<?php echo $Setting_Order['crm_legal_form']; ?>" >
      <label class="col-sm-4 col-form-label">
         <?php echo $Setting_Label['crm_legal_form']; ?>
      </label>
      <div class="col-sm-8">
         <select name="legal_form" class="form-control fields sel-legal-form" id="legal_form">
            <option value="Private Limited company" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Private Limited company') {?> selected="selected"<?php } ?> data-id="1">Private Limited company</option>
            <option value="Public Limited company" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Public Limited company') {?> selected="selected"<?php } ?> data-id="1">Public Limited company</option>
            <option value="Limited Liability Partnership" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Limited Liability Partnership') {?> selected="selected"<?php } ?> data-id="1">Limited Liability Partnership</option>
            <option value="Partnership" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Partnership') {?> selected="selected"<?php } ?> data-id="2">Partnership</option>
            <option value="Self Assessment" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Self Assessment') {?> selected="selected"<?php } ?> data-id="2">Self Assessment</option>
            <option value="Trust" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Trust') {?> selected="selected"<?php } ?> data-id="1">Trust</option>
            <option value="Charity" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Charity') {?> selected="selected"<?php } ?> data-id="1">Charity</option>
            <option value="Other" <?php if(isset($client['crm_legal_form']) && $client['crm_legal_form']=='Other') {?> selected="selected"<?php } ?> data-id="1">Other</option>
         </select>
      </div>
   </div>
   <?php echo render_custom_fields_one( 'required_information' , $rel_id ); ?>
</div>

