<ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard client-top-nav">
    <?php if ($_SESSION['permission']['Client_Section']['view']) { ?> 
        <li class="nav-item">
            <a class="nav-link <?php echo isset($active) && $active == 'all_clients' ? 'active' : '' ?>" href="<?php echo base_url(); ?>user"> <i class="fa fa-users"></i> All Clients</a>
            <div class="slide"></div>
        </li>
    <?php }

    if ($_SESSION['permission']['Client_Section']['edit'] && isset($active) && $active == 'edit_client') { ?> 
        <li class="nav-item">
            <a class="nav-link active" href="<?php echo base_url(); ?>client/addnewclient/<?php echo $client_id ?>"> <i class="fa fa-edit"></i> Edit Client</a>
            <div class="slide"></div>
        </li>
    <?php }

    if ($_SESSION['permission']['Client_Section']['create'] != 0) {
    ?>  
        <li class="nav-item">
            <a class="nav-link <?php echo isset($active) && $active == 'add_client' ? 'active' : '' ?>" href="<?php echo base_url(); ?>client/addnewclient"> <i class="fa fa-user-plus"></i> New Client</a>
            <div class="slide"></div>
        </li>
        <li class="nav-item">
            <a class="nav-link <?php echo isset($active) && $active == 'add_from_company_house' ? 'active' : '' ?>" href="#default-Modal" data-toggle="modal"> <i class="fa fa-building"></i> Add From Company House </a>
            <div class="slide"></div>
        </li>
        <li class="nav-item">
            <a class="nav-link <?php echo isset($active) && $active == 'import_client' ? 'active' : '' ?>" href="<?php echo base_url(); ?>user/import_csv"> <i class="fa fa-file"></i> Import Client </a>
            <div class="slide"></div>
        </li>
    <?php
    }

    if ($_SESSION['permission']['Client_Section']['view']) { ?> 
        <!-- <li class="nav-item">
            <a class="nav-link <?php echo isset($active) && $active == 'services_timeline' ? 'active' : '' ?>" href="<?php echo base_url(); ?>timeline"> <i class="fa fa-calendar"></i> Services Timeline </a>
            <div class="slide"></div>
        </li> -->
    <?php } ?>
    <!-- <li class="nav-item">
        <a class="nav-link" data-popup-open="popup-1" href="#"><img class="themicon" src="<?php echo base_url(); ?>assets/images/tabicon3.png" alt="themeicon" />Email all</a>
        <div class="slide"></div>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-popup-open="popup-2" href="#"><img class="themicon" src="<?php echo base_url(); ?>assets/images/tabicon4.png" alt="themeicon" />Give feedback</a>
        <div class="slide"></div>
    </li>  -->
</ul>