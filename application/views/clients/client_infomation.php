<?php
$this->load->view('includes/header');
error_reporting(-1);
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/org_chart/css/orgchart.min.css">
<link href="<?php echo base_url(); ?>assets/css/jquery.filer.css" type="text/css" rel="stylesheet" />
<!--<link href="<?php echo base_url(); ?>assets/css/timepicki.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/fullcalendar12.css"> 
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/fullcalendar.print.css" media='print'> -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/email.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/timeline-style.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!--<link   rel="stylesheet" href="http:/resources/demos/style.css"> -->
<style Ftype="text/css">
	.btn.btn-success {
		padding: 8px 15px;
		background: #22b14c;
		color: white;
	}

	li.show-block {
		display: none;
		float: left;
		width: 100%;
	}

	.inner-views {
		display: block;
	}

	.newupdate_design .form-group .edit-field-popup1 a {
		border: none !important;
	}

	.service_reminder_content .modal-dialog,
	#send_reminder_content_popup .modal-dialog {
		min-width: 700px !important;
		overflow-y: auto;
		overflow-x: hidden;
	}

	#allactivitylog_sectionsss button.applied_filter {
		color: #045f94 !important;
	}

	#tabs221 li:hover,
	div#tabs221 li {
		margin-right: 0px;
	}

	form#timeline_notes button.btn-success {
		padding: 7px 40px;
	}

	#chart-container .node {
		width: auto !important;
	}

	#chart-container .node i.fa.fa-users.symbol {
		display: none;
	}
	.log ul, .log ol {
		margin: 8% 0% 2% 5%;
	}
	.log ul li, .log ol li {
		width: 100% !important;
		float: initial !important;
		margin-top: 2%;
	}
	.log ul li {
		list-style: initial !important;
	}
	.log ol li {
		list-style: decimal !important;
	}
	.log ul li::before, .log ol li::before {
		background: none !important;
	}
	.log p {
		font-weight: normal;
		display: block;
	}
	#trig_acts a {
		font-weight: 500 !important;
	}
	#trig_acts a.active {
		background-color: #e8f0fe !important;
		border: none !important;
		color: #1a73e8 !important;
		border-radius: 4px !important;
	}
	.tox-tinymce-aux {
		z-index: 99999;
	}
	.oval {
	  width: 160px;
	  height: 80px;
	  background: #e74a25;
	  border-radius: 50%;
	  color: white;
	  padding: 10px;
	}
	.red_class
	{
		color: red !important;
	}
	.red_class a
	{
		color: red !important;
	}
</style>
<?php
$res_suc  = 1; //$this->session->flashdata('email_url_redirect');

$other_services = $this->db->query('SELECT * FROM service_lists WHERE id>16')->result_array();
$fill = ['conf_statement', 'accounts', 'company_tax_return', 'personal_tax_return', 'vat', 'payroll', 'workplace', 'cis', 'cissub', 'bookkeep', 'p11d', 'management', 'investgate', 'registered', 'taxinvest', 'taxadvice'];

foreach ($other_services as $key => $value) {
	$fill[] = $value['services_subnames'];
}

$tab_column_name = array_fill_keys($fill, 0);

$tab_on = array_intersect_key($client, $tab_column_name);

function jsonToArray($v)
{
	$return  = ["tab" => 0, "reminder" => 0, "text" => 0, "invoice" => 0];

	if (!empty(json_decode($v, true))) {
		$return  = array_replace($return, json_decode($v, true));
	}
	return $return;
}
$tab_on = array_map('jsonToArray', $tab_on);
?>

<div class="pcoded-content card-removes clientredesign client-infor-design1">
	<section class="client-details-view hidden-user01 floating_set card-removes">
		<div class="modal-alertsuccess alert alert-success" style="display:none;">
			<div class="newupdate_alert">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<div class="pop-realted1">
					<div class="position-alert1">Your client was updated successfully.
					</div>
				</div>
			</div>
		</div>

		<div class="modal-alertsuccess alert alert-danger info-box" style="display:none;">
			<div class="newupdate_alert">
				<a href="#" class="close">&times;</a>
				<div class="pop-realted1">
					<div class="position-alert1 info-text">
						Please fill the required fields.
					</div>
				</div>
			</div>
		</div>

		<div class="main-body main-float1 floating_set">
			<div class="information-tab floating_set">
				<div class="space-required">
					<div class="deadline-crm1 floating_set">
						<!-- <ul class="nav nav-tabs1 all_user1 md-tabs pull-left u-dashboard">
							<li class="nav-item">
								<a class="nav-link" href="<?php echo base_url() . 'user'; ?>">
									<img class="themicon" src="<?php echo base_url(); ?>assets/images/tabicon1.png" alt="themeicon" />
									All Clients</a>
								<div class="slide"></div>
							</li>
							<li class="nav-item">
								<a class="nav-link active" href="#addnewclient">
									<img class="themicon" src="<?php echo base_url(); ?>assets/images/tabicon2.png" alt="themeicon" />View Client</a>
								<div class="slide"></div>
							</li>
						</ul> -->
						<?php $this->load->view('clients/component/navigation.php', [ 'active' => 'all_clients']); ?>

						<div class="Footer common-clienttab pull-right new-client-btns-wrapper">
							<div class="new-client-detail-wrapper">
								<?php $cmy_role = $client['status'];
									//echo $cmy_role;
									if ($cmy_role == 0) {
										$cmy_status = 'Manual';
									} else if ($cmy_role == 1) {
										$cmy_status = 'Company House';
									} else if ($cmy_role == 2) {
										$cmy_status = 'Import';
									} else if ($cmy_role == 3) {
										$cmy_status = 'From Lead';
									} ?>

									<?php
									if (isset($client['crm_company_name']) && $client['crm_company_name'] != '') {
										$client_name = $client['crm_company_name'];
									} else {
										$client_name = $this->Common_mdl->get_crm_name($client['user_id']);
									}
									$land_line = $contactRec[0]['landline'];
									$land_line = str_replace('["',"",$land_line);
									$land_line = str_replace('","'," , ",$land_line);
									$land_line = str_replace('"]',"",$land_line);
									// if(isset($client['crm_companies_house_authorisation_code']) && $client['crm_companies_house_authorisation_code'] !== ''){
									// 	$company_hac = $client['crm_companies_house_authorisation_code'];
									// }
									echo '<h2 class="view-update-client">' . '<span>' . $client_name . '</span>' . ' - ' . '<span class="view-update-client-data">' . '<i class="fa fa-unlock-alt" aria-hidden="true"></i>' . '<span class="tootlip-detail-chc" style="display:none">Company House Code</span>' . $client['crm_companies_house_authorisation_code'] . '</span>' . ' - ' . '<span class="view-update-client-data">' . '<i class="fa fa-id-card" aria-hidden="true"></i>'. '<span class="tootlip-detail-utr" style="display:none">CO - UTR</span>'  . $client['crm_accounts_utr_number'] . '</span>' . ' - ' . '<span class="view-update-client-data">' . '<i class="fa fa-phone" aria-hidden="true"></i>'. '<span class="tootlip-detail-landline" style="display:none">Landline</span>' . $land_line . '</span>' . ' - ' . '<span class="view-update-client-data">' . '<i class="fa fa-envelope-o" aria-hidden="true"></i>'. '<span class="tootlip-detail-email" style="display:none">Email</span>'  . $contactRec[0]['main_email'] . '</span>' .  '</h2>';
								?>
							</div>
							<div class="client-btns-view-wrapper new-client-btns-view-wrapper">
								<?php if ($_SESSION['permission']['Client_Section']['edit'] == '1') { ?>
									<div class="change-client-bts1 client_view_saveedit_button edit-bts-in">
										<a href="<?php echo base_url() . 'Client/addnewclient/' . $client['user_id']; ?>" class="signed-change1">EDIT</a>
									</div>
								<?php } ?>
								<div class="divleft">
									<button class="signed-change2" type="button" value="Previous Tab" text="Previous Tab" style="display: none;">Previous
									</button>
								</div>
								<div class="divright">
									<button class="signed-change3" type="button" value="Next Tab" text="Next Tab">Next
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="space-required">
					<div class="document-center client-infom-1 floating_set p-t-2">
						<div class="Companies_House floating_set p-l-1">
							<!-- <div class="pull-left">
								<?php $cmy_role = $client['status'];
								//echo $cmy_role;
								if ($cmy_role == 0) {
									$cmy_status = 'Manual';
								} else if ($cmy_role == 1) {
									$cmy_status = 'Company House';
								} else if ($cmy_role == 2) {
									$cmy_status = 'Import';
								} else if ($cmy_role == 3) {
									$cmy_status = 'From Lead';
								} ?>

								<?php
								if (isset($client['crm_company_name']) && $client['crm_company_name'] != '') {
									$client_name = $client['crm_company_name'];
								} else {
									$client_name = $this->Common_mdl->get_crm_name($client['user_id']);
								}
								echo '<h2>' . $client_name . ' - ' . $cmy_status . '</h2>';
								?>
							</div> -->
						</div>
						<div class="addnewclient_pages1 floating_set bg_new1 view-client-btns-wrapper">
							<ol class="nav nav-tabs CLIENT_CONTENT_TAB all_user1 md-tabs floating_set" id="tabs">
								<li class="nav-item it0 bbs">
									<a class="nav-link active" data-toggle="tab" href="#details">Basic Details</a>
								</li>
								<li class="nav-item it1" value="1">
									<a class="nav-link" data-toggle="tab" href="#contacts1"> Contact</a>
								</li>
								<li class="nav-item it2 hide">
									<a class="nav-link" data-toggle="tab" href="#service"> Service</a>
								</li>
								<li class="nav-item it2 hide">
									<a class="nav-link " data-toggle="tab" href="#assignto"> Assign to</a>
								</li>
								<li class="nav-item it2 hide">
									<a class="nav-link " data-toggle="tab" href="#amlchecks"> AML Checks</a>
								</li>
								<li class="nav-item it2 hide">
									<a class="nav-link" data-toggle="tab" href="#other"> Other & Referral </a>
								</li>
								<!-- <li class="nav-item it2 hide">
									<a class="nav-link" data-toggle="tab" href="#referral"> Referral</a>
								</li> -->
								<?php
								$import_tab_on = (in_array("on", array_column($tab_on, 'tab'), TRUE) ? "display:block" :   "display:none");
								?>
								<li class="nav-item" id="import_tab_li" style="<?php echo $import_tab_on; ?> ">
									<a class="nav-link" data-toggle="tab" href="#import_tab"> Important Information</a>
								</li>
								<li class="nav-item it2 hide">
									<a class="nav-link" data-toggle="tab" href="#task"> Task</a>
								</li>
								<li class="nav-item it2 hide">
									<a class="nav-link" data-toggle="tab" href="#leads"> Leads</a>
								</li>
								<li class="nav-item it2 hide">
									<a class="nav-link " target="_blank" data-id="<?php echo $client['user_id'] ?>" href="<?php echo base_url() . 'Documents?dir=/' . $client['user_id'] ?>"> Documents</a>
									<!-- get-client-doc -->
								</li>
								<li class="nav-item it2 hide">
									<a class="nav-link" data-toggle="tab" href="#proposals"> Proposals</a>
								</li>
								<li class="nav-item it2 hide">
									<a class="nav-link" data-toggle="tab" href="#invoice"> Invoice</a>
								</li>
								<li class="nav-item it2 hide trigger_to_show_queue" value="2">
									<a class="nav-link " data-toggle="tab" href="#reminder">E-Reminders</a>
								</li>
								<li class="nav-item it2 hide">
									<a class="nav-link" data-toggle="tab" href="#newtimelineservices"> Timeline Services</a>
								</li>
							</ol>
						</div>
						<!-- tab close -->
					</div>
				</div>
				<?php
				$rel_id = (isset($client) ? $client['user_id'] : false);
				?>
				<div class="newupdate_design client-information-view1 <?php if ($_SESSION['permission']['Client_Section']['view'] != '1') { ?> permission_deined <?php } ?>">
					<div class="management_section client-details-tab01 data-mang-06  floating_set realign newmanage-animation">
						<div class="tab-content card">

							<div id="detailss" class="tab-pane fade">
								<div class="search-client-details01 floating_set">
									<div class="pull-left detail-pull-left">
										<span>Details</span>
									</div>
									<div class="pull-right edit-tag1">
										<a href="#"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
									</div>
								</div>
								<div id="accordion" role="tablist" aria-multiselectable="true">
									<div class="accordion-panel">
										<div class="box-division03 view-client-div">
											<div class="accordion-heading" role="tab" id="headingOne">
												<h3 class="card-title accordion-title">
													<a class="accordion-msg" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Basic Info</a>
												</h3>
											</div>
											<div id="collapseOne" class="panel-collapse collapse in show" role="tabpanel" aria-labelledby="headingOne">
												<div class="basic-info-client1">
													<table class="client-detail-table01">
														<tr>
															<th>Client ID</th>
															<td><?php if (isset($user['client_id'])) {
																	echo $user['client_id'];
																} ?></td>
															<th>Account Office Reference</th>
															<td><?php if (isset($client['crm_accounts_office_reference'])) {
																	echo $client['crm_accounts_office_reference'];
																} ?></td>
														</tr>
														<tr>
															<th>Client Type</th>
															<td><?php if (isset($client['crm_company_type'])) {
																	echo $client['crm_company_type'];
																} ?></td>
															<th>PAYE Reference Number</th>
															<td><?php if (isset($client['crm_employers_reference'])) {
																	echo $client['crm_employers_reference'];
																} ?></td>
														</tr>
														<tr>
															<th>Company Name</th>
															<td><?php if (isset($client['crm_company_name'])) {
																	echo $client['crm_company_name'];
																} ?></td>
														</tr>
													</table>
												</div>
											</div>
										</div>
									</div>
									<!-- accordion1 -->
									<div class="accordion-panel">
										<div class="box-division03 view-client-div">
											<div class="accordion-heading" role="tab" id="headingTwo">
												<h3 class="card-title accordion-title">
													<a class="accordion-msg" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseOne">Address</a>
												</h3>
											</div>
											<div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
												<div class="basic-info-client1">
													<table class="client-detail-table01">
														<tr>
															<th>Address Line 1</th>
															<td><?php if (isset($company['registered_office_address']['address_line_1'])) {
																	echo $company['registered_office_address']['address_line_1'];
																}  ?></td>
															<th>Post Code</th>
															<td><?php if (isset($company['registered_office_address']['postal_code'])) {
																	echo $company['registered_office_address']['postal_code'];
																}  ?></td>
														</tr>
														<tr>
															<th>Address Line 2</th>
															<td><?php if (isset($company['registered_office_address']['address_line_2'])) {
																	echo $company['registered_office_address']['address_line_2'];
																}  ?></td>
															<th>country</th>
															<td><?php if (isset($company['registered_office_address']['country'])) {
																	echo $company['registered_office_address']['country'];
																}  ?></td>
														</tr>
														<tr>
															<th>Town/City</th>
															<td><?php if (isset($company['registered_office_address']['locality'])) {
																	echo $company['registered_office_address']['locality'];
																}  ?></td>
															<th>Region</th>
															<td><?php if (isset($company['registered_office_address']['region'])) {
																	echo $company['registered_office_address']['region'];
																}  ?></td>
														</tr>
													</table>
												</div>
											</div>
										</div>
									</div>
									<!-- accordion1 -->
									<div class="accordion-panel">
										<div class="box-division03 view-client-div">
											<div class="accordion-heading" role="tab" id="headingOne">
												<h3 class="card-title accordion-title">
													<a class="accordion-msg" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Contact Info</a>
												</h3>
											</div>
											<div id="collapseOne" class="panel-collapse collapse in show" role="tabpanel" aria-labelledby="headingOne">
												<div class="basic-info-client1">
													<table class="client-detail-table01">
														<tr>
															<th>Phone</th>
															<td><?php if (isset($client['crm_telephone_number'])) {
																	echo $client['crm_telephone_number'];
																} ?></td>
															<th>Website</th>
															<td><?php if (isset($client['crm_website'])) {
																	echo $client['crm_website'];
																} ?></td>
														</tr>
														<tr>
															<th>Email</th>
															<td><?php if (isset($client['crm_email'])) {
																	echo $client['crm_email'];
																} ?></td>
														</tr>
													</table>
												</div>
											</div>
										</div>
									</div>
									<!-- accordion1 -->
									<div class="accordion-panel">
										<div class="box-division03 view-client-div">
											<div class="accordion-heading" role="tab" id="headingOne">
												<h3 class="card-title accordion-title">
													<a class="accordion-msg" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Business Info</a>
												</h3>
											</div>
											<div id="collapseOne" class="panel-collapse collapse in show" role="tabpanel" aria-labelledby="headingOne">
												<div class="basic-info-client1">
													<table class="client-detail-table01">
														<tr>
															<th>Business Start Date</th>
															<td><?php if (isset($company['date_of_creation'])) {
																	echo date('d-m-Y', strtotime($company['date_of_creation']));
																} ?></td>
															<th>VAT Scheme</th>
															<td></td>
														</tr>
														<tr>
															<th>Book Start Date</th>
															<td></td>
															<th>VAT Submit Type</th>
															<td></td>
														</tr>
														<tr>
															<th>Year End Date</th>
															<td><?php if (isset($company['accounts']['accounting_reference_date'])) {
																	echo $company['accounts']['accounting_reference_date']['day'] . '/' . $company['accounts']['accounting_reference_date']['month'];
																} ?></td>
															<th>VAT Reg.No</th>
															<td><?php if (isset($client['vat_number'])) {
																	echo $client['vat_number'];
																} ?></td>
														</tr>
														<tr>
															<th>Company Reg.No</th>
															<td><?php if (isset($company['company_number'])) {
																	echo $company['company_number'];
																} ?></td>
															<th>VAT Reg.Date</th>
															<td><?php if (isset($client['crm_vat_date_of_registration'])) {
																	echo date('d-m-Y', strtotime($client['crm_vat_date_of_registration']));
																} ?></td>
														</tr>
														<tr>
															<th>UTR No</th>
															<td><?php if (isset($client['crm_company_utr'])) {
																	echo $client['crm_company_utr'];
																} ?></td>
															<th>Company Status</th>
															<td><?php if (isset($company['company_status'])) {
																	echo $company['company_status'];
																} ?></td>
														</tr>
													</table>
												</div>
											</div>
										</div>
									</div>
									<!-- accordion1 -->
								</div>
							</div>
							<!-- 1tab -->

							<div id="details" class="accord-proposal1 tab-pane fade in active">
								<div class="masonry-container floating_set">
									<div class="grid-sizer"></div>
									<div class="accordion-panel">
										<div class="box-division03 view-client-div">
											<div class="accordion-heading" role="tab" id="headingOne">
												<h3 class="card-title accordion-title">
													<a class="accordion-msg">Important Information</a>
												</h3>
											</div>
											<div id="collapse" class="panel-collapse">
												<div class="basic-info-client1 CONTENT_IMPORTANT_DETAILS" id="important_details_section">

													<?php if (isset($client['crm_company_name']) && $client['crm_company_name'] != '') {  ?>
														<div class="form-group row name_fields sorting" data-id="<?php echo $Setting_Order['crm_company_name']; ?>" <?php echo $Setting_Delete['crm_company_name']; ?>>
															<?php $company_roles = (isset($client['status']) && $client['status'] == 1) ? 'readonly' : '' ?>
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_company_name']; ?> </label>
															<div class="col-sm-8">
																<span class="small-info"><?php if (isset($client['crm_company_name'])) {
																								echo $client['crm_company_name'];
																							} ?></span>
															</div>
														</div>
													<?php } ?>

													<?php if (isset($client['crm_company_number']) && $client['crm_company_number'] != '') { ?>
														<div class="form-group row name_fields sorting" data-id="<?php echo $Setting_Order['crm_company_number']; ?>" <?php echo $Setting_Delete['crm_company_number']; ?>>
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_company_number']; ?> </label>
															<div class="col-sm-8">
																<span class="small-info"><?php if (isset($client['crm_company_number'])) {
																								echo $client['crm_company_number'];
																							} ?></span>
															</div>
														</div>
													<?php } ?>

													<?php if (isset($client['crm_company_url']) && $client['crm_company_url'] != '') { ?>
														<div class="form-group row name_fields sorting" data-id="<?php echo $Setting_Order['crm_company_url']; ?>" <?php echo $Setting_Delete['crm_company_url']; ?>>
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_company_url']; ?> </label>
															<div class="col-sm-8">
																<span class="small-info company_slcs edit-field-popup1">
																	<a href="<?php echo $client['crm_company_url']; ?>" target="_blank">Company url</a>
																</span>
															</div>
														</div>
													<?php } ?>

													<?php if (isset($client['crm_officers_url']) && $client['crm_officers_url'] != '') { ?>
														<div class="form-group row name_fields sorting" data-id="<?php echo $Setting_Order['crm_officers_url']; ?>" <?php echo $Setting_Delete['crm_officers_url']; ?>>
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_officers_url']; ?> </label>
															<div class="col-sm-8">
																<span class="small-info company_slcs edit-field-popup1">
																	<a href="<?php echo $client['crm_officers_url']; ?>" target="_blank">Officers url</a>
																</span>
															</div>
														</div>
													<?php } ?>

													<?php if (isset($client['crm_incorporation_date']) && $client['crm_incorporation_date'] != '') { ?>
														<div class="form-group row name_fields sorting" data-id="<?php echo $Setting_Order['crm_incorporation_date']; ?>" <?php echo $Setting_Delete['crm_incorporation_date']; ?>>
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_incorporation_date']; ?> </label>
															<div class="col-sm-8">
																<span class="small-info"><?php if (isset($client['crm_incorporation_date'])) {
																								echo Change_Date_Format($client['crm_incorporation_date']);
																							} ?></span>
															</div>
														</div>
													<?php } ?>

													<?php if (isset($client['crm_registered_in']) && $client['crm_registered_in'] != '') { ?>
														<div class="form-group row name_fields sorting" data-id="<?php echo $Setting_Order['crm_registered_in']; ?>" <?php echo $Setting_Delete['crm_registered_in']; ?>>
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_registered_in']; ?> </label>
															<div class="col-sm-8">
																<!-- <input type="text" name="registered_in" id="registered_in" placeholder="" value="<?php if (isset($company['jurisdiction'])) {
																																							echo $company['jurisdiction'];
																																						} ?>" class="fields" <?php echo $company_roles; ?>> -->
																<span class="small-info"><?php if (isset($client['crm_registered_in'])) {
																								echo $client['crm_registered_in'];
																							} ?></span>
															</div>
														</div>

													<?php } ?>

													<?php if (isset($client['crm_address_line_one']) && $client['crm_address_line_one'] != '') { ?>
														<div class="form-group row name_fields sorting" data-id="<?php echo $Setting_Order['crm_address_line_one']; ?>" <?php echo $Setting_Delete['crm_address_line_one']; ?>>
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_address_line_one']; ?> </label>
															<div class="col-sm-8">
																<p class="for-address"><?php if (isset($client['crm_address_line_one'])) {
																							echo $client['crm_address_line_one'];
																						}  ?></p>
															</div>
														</div>
													<?php } ?>

													<?php if (isset($client['crm_address_line_two']) && $client['crm_address_line_two'] != '') { ?>

														<div class="form-group row name_fields sorting" data-id="<?php echo $Setting_Order['crm_address_line_two']; ?>" <?php echo $Setting_Delete['crm_address_line_two']; ?>>
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_address_line_two']; ?> </label>
															<div class="col-sm-8">
																<p class="for-address"><?php if (isset($client['crm_address_line_two'])) {
																							echo $client['crm_address_line_two'];
																						}  ?></p>
															</div>
														</div>

													<?php } ?>

													<?php if (isset($client['crm_address_line_three']) && $client['crm_address_line_three'] != '') { ?>
														<div class="form-group row name_fields sorting" data-id="<?php echo $Setting_Order['crm_address_line_three']; ?>" <?php echo $Setting_Delete['crm_address_line_three']; ?>>
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_address_line_three']; ?> </label>
															<div class="col-sm-8">
																<p class="for-address"><?php if (isset($client['crm_address_line_three'])) {
																							echo $client['crm_address_line_three'];
																						}  ?></p>
															</div>
														</div>
													<?php } ?>

													<?php if (isset($client['crm_town_city']) && $client['crm_town_city'] != '') { ?>
														<div class="form-group row name_fields sorting" data-id="<?php echo $Setting_Order['crm_town_city']; ?>" <?php echo $Setting_Delete['crm_town_city']; ?>>
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_town_city']; ?> </label>
															<div class="col-sm-8">
																<span class="small-info"><?php if (isset($client['crm_town_city'])) {
																								echo $client['crm_town_city'];
																							}  ?></span>
															</div>
														</div>
													<?php } ?>

													<?php if (isset($client['crm_post_code']) && $client['crm_post_code'] != '') { ?>
														<div class="form-group row name_fields sorting" data-id="<?php echo $Setting_Order['crm_post_code']; ?>" <?php echo $Setting_Delete['crm_post_code']; ?>>
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_post_code']; ?> </label>
															<div class="col-sm-8">
																<span class="small-info"><?php if (isset($client['crm_post_code'])) {
																								echo $client['crm_post_code'];
																							}  ?></span>
															</div>
														</div>
													<?php } ?>
													<?php if (isset($client['crm_company_status']) && $client['crm_company_status'] != '') { ?>
														<div class="form-group row name_fields sorting" data-id="<?php echo $Setting_Order['crm_company_status']; ?>" <?php echo $Setting_Delete['crm_company_status']; ?>>
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_company_status']; ?> </label>
															<div class="col-sm-8">
																<span class="small-info"><?php if (isset($client['crm_company_status'])) {
																								if ($client['crm_company_status'] == 1) {
																									echo "Active";
																								} else {
																									echo "Not Active";
																								}
																							} ?></span>
															</div>
														</div>
													<?php } ?>
													<?php if (isset($client['crm_company_type']) && $client['crm_company_type'] != '') { ?>
														<div class="form-group row name_fields sorting" data-id="<?php echo $Setting_Order['crm_company_type']; ?>" <?php echo $Setting_Delete['crm_company_type']; ?>>
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_company_type']; ?> </label>
															<div class="col-sm-8">
																<span class="small-info company_slcs"><?php if (isset($client['crm_company_type'])) {
																											echo $client['crm_company_type'];
																										} ?></span>
															</div>
														</div>
													<?php } ?>

													<?php if (isset($client['crm_company_sic']) && ($client['crm_company_sic'] != '')) { ?>

														<div class="form-group row name_fields sorting" data-id="<?php echo $Setting_Order['crm_company_sic']; ?>" <?php echo $Setting_Delete['crm_company_sic']; ?>>
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_company_sic']; ?> </label>
															<div class="col-sm-8">
																<span class="small-info company_slcs"><?php if (isset($client['crm_company_sic']) && ($client['crm_company_sic'][0] != '')) {
																											$code = $this->db->query('SELECT * FROM sic_code WHERE COL1="' . $client['crm_company_sic'][0] . '"')->row_array();
																											echo $client['crm_company_sic'];
																										} ?>
																</span>
															</div>
														</div>

													<?php } ?>

													<?php if (isset($client['crm_letter_sign']) && $client['crm_letter_sign'] != '') { ?>

														<div class="form-group row name_fields sorting" data-id="<?php echo $Setting_Order['crm_letter_sign']; ?>" <?php echo $Setting_Delete['crm_letter_sign']; ?>>
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_letter_sign']; ?> </label>
															<div class="col-sm-8">
																<span class="small-info"><?php if (isset($client['crm_letter_sign'])) {
																								echo  $client['crm_letter_sign'];
																							} ?></span>
															</div>
														</div>
													<?php } ?>
													<!-- <?php
															$crm_business_website = (isset($client['crm_business_website']) ? $client['crm_business_website'] : false);
															$crm_business = render_custom_fields_edit('business_website', $rel_id, $crm_business_website);
															if ($crm_business != '') {
																$s = $this->Common_mdl->getCustomFieldRec($rel_id, 'business_website');

																if ($s) {
																	foreach ($s as $keys => $values) {
																		if ($keys == '') {
																			$keys = '-';
																		}
																		if ($values == '') {
																			$values = '-';
																		}
															?>
                          <div class="form-group row name_fields sorting">
                            <label class="col-sm-4 col-form-label"><?php echo $keys; ?></label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php echo $values; ?></span>
                              </div>
                          </div>
                    <?php
																	}
																}
															} ?>
                    <?php
					$crm_accounting_system = (isset($client['crm_accounting_system']) ? $client['crm_accounting_system'] : false);
					$crm_accounting = render_custom_fields_edit('accounting_system', $rel_id, $crm_accounting_system);
					if ($crm_accounting != '') {
						$s_accounting_system = $this->Common_mdl->getCustomFieldRec($rel_id, 'accounting_system');

						if ($s_accounting_system) {
							foreach ($s_accounting_system as $key_as => $value_as) {
								if ($key_as == '') {
									$key_as = '-';
								}
								if ($value_as == '') {
									$value_as = '-';
								}
					?>
                          <div class="form-group row name_fields sorting" >
                            <label class="col-sm-4 col-form-label"><?php echo $key_as; ?></label>
                              <div class="col-sm-8">
                                <span class="small-info"><?php echo $value_as; ?></span>
                              </div>
                          </div>
                    <?php
							}
						}
					} ?> -->
													<?php if (isset($client['crm_business_website']) && $client['crm_business_website'] != '') { ?>

														<div class="form-group row name_fields sorting" data-id="<?php echo $Setting_Order['crm_business_website']; ?>" <?php echo $Setting_Delete['crm_business_website']; ?>>
															<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_business_website']; ?></label>
															<div class="col-sm-8">
																<span class="small-info"><?php echo $client['crm_business_website']; ?></span>
															</div>
														</div>

													<?php } ?>

													<?php if (isset($client['crm_accounting_system']) && $client['crm_accounting_system'] != '') { ?>
														<div class="form-group row name_fields sorting" data-id="<?php echo $Setting_Order['crm_accounting_system']; ?>" <?php echo $Setting_Delete['crm_accounting_system']; ?>>
															<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_accounting_system']; ?></label>
															<div class="col-sm-8">
																<span class="small-info"><?php echo $client['crm_accounting_system']; ?></span>
															</div>
														</div>
													<?php } ?>

													<?php if (isset($client['crm_hashtag']) && $client['crm_hashtag'] != '') { ?>
														<div class="form-group row name_fields sorting" data-id="<?php echo $Setting_Order['crm_hashtag']; ?>" <?php echo $Setting_Delete['crm_hashtag']; ?>>
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_hashtag']; ?> </label>
															<div class="col-sm-8">
																<span class="small-info"><?php echo $client['crm_hashtag']; ?></span>
															</div>
														</div>
													<?php } ?>
												</div>
											</div>
										</div>
									</div>


									<div class="accordion-panel">
										<div class="box-division03 view-client-div">
											<div class="accordion-heading" role="tab" id="headingOne">
												<h3 class="card-title accordion-title">
													<a class="accordion-msg">Service Due Details
													</a>
												</h3>
											</div>
											<div id="collapse" class="panel-collapse">
												<div class="basic-info-client1">

													<?php if (isset($client['crm_hmrc_yearend']) && ($client['crm_hmrc_yearend'] != '')) { ?>
														<div class="form-group row name_fields  accounts_sec" data-id="<?php echo $Setting_Order['crm_hmrc_yearend']; ?>" <?php echo $Setting_Delete['crm_hmrc_yearend']; ?>>
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_hmrc_yearend']; ?> </label>
															<div class="col-sm-8">
																<span class="small-info"><?php if (isset($client['crm_hmrc_yearend']) && ($client['crm_hmrc_yearend'] != '')) {
																								echo date("d-m-Y", strtotime($client['crm_hmrc_yearend']));
																							} ?></span>
															</div>
														</div>
													<?php } ?>

													<?php if (isset($client['crm_personal_tax_return_date']) && ($client['crm_personal_tax_return_date'] != '')) { ?>
														<div class="form-group row name_fields personal_sort" data-id="<?php echo $Setting_Order['crm_personal_tax_return_date']; ?>">
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_personal_tax_return_date']; ?> </label>
															<div class="col-sm-8">
																<span class="small-info"><?php if (isset($client['crm_personal_tax_return_date']) && ($client['crm_personal_tax_return_date'] != '')) {
																								echo  Change_Date_Format($client['crm_personal_tax_return_date']);
																							} ?></span>
															</div>
														</div>
													<?php } ?>

													<?php if (isset($client['crm_confirmation_statement_date']) && ($client['crm_confirmation_statement_date'] != '')) {  ?>
														<div class="form-group row name_fields sorting_conf" data-id="<?php echo $Setting_Order['crm_confirmation_statement_date']; ?>" <?php echo $Setting_Delete['crm_confirmation_statement_date']; ?>>
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_confirmation_statement_date']; ?> </label>
															<div class="col-sm-8 edit-field-popup1">
																<?php if (isset($client['crm_confirmation_statement_date']) && ($client['crm_confirmation_statement_date'] != '')) {
																	echo date("d-m-Y", strtotime($client['crm_confirmation_statement_date']));
																} ?>
															</div>
														</div>
													<?php } ?>

													<?php if (isset($client['crm_accounts_due_date_hmrc']) && ($client['crm_accounts_due_date_hmrc'] != '')) { ?>
														<div class="form-group row name_fields personal_sort" data-id="<?php echo $Setting_Order['crm_accounts_due_date_hmrc']; ?>" <?php echo $Setting_Delete['crm_accounts_due_date_hmrc']; ?>>
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_accounts_due_date_hmrc']; ?> </label>
															<div class="col-sm-8">
																<span class="small-info"><?php if (isset($client['crm_accounts_due_date_hmrc']) && ($client['crm_accounts_due_date_hmrc'] != '')) {
																								echo  Change_Date_Format($client['crm_accounts_due_date_hmrc']);
																							} ?></span>
															</div>
														</div>
													<?php } ?>

													<?php if (isset($client['crm_vat_quarters']) && ($client['crm_vat_quarters'] != 0)) { ?>
														<div class="form-group row help_icon date_birth vat_sort" data-id="<?php echo $Setting_Order['crm_vat_quarters']; ?>" <?php echo $Setting_Delete['crm_vat_quarters']; ?>>
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_vat_quarters']; ?> </label>
															<div class="col-sm-8">
																<?php if (isset($client['crm_vat_quarters']) && ($client['crm_vat_quarters'] != '')) {
																	echo $client['crm_vat_quarters'];
																} ?>
															</div>
														</div>
													<?php } ?>

													<?php if (isset($client['crm_payroll_run_date']) && ($client['crm_payroll_run_date'] != '')) { ?>
														<div class="form-group row  date_birth payroll_sec" data-id="<?php echo $Setting_Order['crm_payroll_run_date']; ?>" <?php echo $Setting_Delete['crm_payroll_run_date']; ?>>
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_payroll_run_date']; ?> </label>
															<div class="col-sm-8">
																<?php if (isset($client['crm_payroll_run_date']) && ($client['crm_payroll_run_date'] != '')) {
																	echo    Change_Date_Format($client['crm_payroll_run_date']);
																} ?>
															</div>
														</div>
													<?php } ?>

													<?php if (isset($client['crm_pension_subm_due_date']) && ($client['crm_pension_subm_due_date'] != '')) { ?>
														<div class="form-group row  date_birth workplace" data-id="<?php echo $Setting_Order['crm_pension_subm_due_date']; ?>">
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_pension_subm_due_date']; ?> </label>
															<div class="col-sm-8">
																<?php if (isset($client['crm_pension_subm_due_date']) && ($client['crm_pension_subm_due_date'] != '')) {
																	echo  Change_Date_Format($client['crm_pension_subm_due_date']);
																} ?>
															</div>
														</div>
													<?php } ?>

													<?php if (isset($client['crm_next_p11d_return_due']) && ($client['crm_next_p11d_return_due'] != '')) {  ?>
														<div class="form-group row date_birth p11d" data-id="<?php echo $Setting_Order['crm_next_p11d_return_due']; ?>">
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_next_p11d_return_due']; ?> </label>
															<div class="col-sm-8">
																<?php if (isset($client['crm_next_p11d_return_due']) && ($client['crm_next_p11d_return_due'] != '')) {
																	echo  Change_Date_Format($client['crm_next_p11d_return_due']);
																} ?>
															</div>
														</div>
													<?php } ?>

													<?php if (isset($client['crm_next_manage_acc_date']) && ($client['crm_next_manage_acc_date'] != '')) { ?>
														<div class="form-group row help_icon date_birth management" data-id="<?php echo $Setting_Order['crm_next_manage_acc_date']; ?>">
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_next_manage_acc_date']; ?> </label>
															<div class="col-sm-8">

																<span class="small-info"><?php if (isset($client['crm_next_manage_acc_date']) && ($client['crm_next_manage_acc_date'] != '')) {
																								echo  Change_Date_Format($client['crm_next_manage_acc_date']);
																							} ?></span>
															</div>
														</div>
													<?php } ?>

													<?php if (isset($client['crm_next_booking_date']) && ($client['crm_next_booking_date'] != '')) { ?>
														<div class="form-group row help_icon date_birth management" data-id="<?php echo $Setting_Order['crm_next_booking_date']; ?>">
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_next_booking_date']; ?> </label>
															<div class="col-sm-8">
																<?php if (isset($client['crm_next_booking_date']) && ($client['crm_next_booking_date'] != '')) {
																	echo  Change_Date_Format($client['crm_next_booking_date']);
																} ?>
															</div>
														</div>
													<?php } ?>


													<?php if (isset($client['crm_insurance_renew_date']) && ($client['crm_insurance_renew_date'] != '')) { ?>
														<div class="form-group row help_icon date_birth invest" data-id="<?php echo $Setting_Order['crm_insurance_renew_date']; ?>">
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_insurance_renew_date']; ?> </label>
															<div class="col-sm-8">

																<span class="small-info"><?php if (isset($client['crm_insurance_renew_date']) && ($client['crm_insurance_renew_date'] != '')) {
																								echo  Change_Date_Format($client['crm_insurance_renew_date']);
																							} ?></span>
															</div>
														</div>
													<?php } ?>

													<?php if (isset($client['crm_registered_renew_date']) && ($client['crm_registered_renew_date'] != '')) { ?>
														<div class="form-group row help_icon date_birth invest" data-id="<?php echo $Setting_Order['crm_registered_renew_date']; ?>">
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_registered_renew_date']; ?> </label>
															<div class="col-sm-8">

																<span class="small-info"><?php if (isset($client['crm_registered_renew_date']) && ($client['crm_registered_renew_date'] != '')) {
																								echo  Change_Date_Format($client['crm_registered_renew_date']);
																							} ?></span>
															</div>
														</div>
													<?php } ?>

													<?php if (isset($client['crm_investigation_end_date']) && ($client['crm_investigation_end_date'] != '')) { ?>
														<div class="form-group row help_icon date_birth" data-id="<?php echo $Setting_Order['crm_investigation_end_date']; ?>">
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_investigation_end_date']; ?> </label>
															<div class="col-sm-8">

																<span class="small-info"><?php if (isset($client['crm_investigation_end_date']) && ($client['crm_investigation_end_date'] != '')) {
																								echo  Change_Date_Format($client['crm_investigation_end_date']);
																							} ?></span>
															</div>
														</div>
													<?php } ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div id="contacts1" class="tab-pane fade">

								<div id="contactss"></div>

								<?php
								$i = 1;
								foreach ($contactRec as $contactRec_key => $contactRec_value) {

									$title = $contactRec_value['title'];
									$first_name = $contactRec_value['first_name'];
									$middle_name = $contactRec_value['middle_name'];
									$surname = $contactRec_value['surname'];
									$preferred_name = $contactRec_value['preferred_name'];
									$last_name = $contactRec_value['last_name'];
									$mobile = $contactRec_value['mobile'];
									$main_email = $contactRec_value['main_email'];
									$nationality = $contactRec_value['nationality'];
									$psc = $contactRec_value['psc'];
									$shareholder = $contactRec_value['shareholder'];
									$ni_number = $contactRec_value['ni_number'];
									$contact_type = $contactRec_value['contact_type'];
									$address_line1 = $contactRec_value['address_line1'];
									$address_line2 = $contactRec_value['address_line2'];
									$town_city = $contactRec_value['town_city'];
									$post_code = $contactRec_value['post_code'];
									$landline = $contactRec_value['landline'];
									$work_email   = $contactRec_value['work_email'];
									$date_of_birth   =  (!empty($contactRec_value['date_of_birth']) ? date('d-m-Y', strtotime($contactRec_value['date_of_birth'])) : '');
									$nature_of_control   = $contactRec_value['nature_of_control'];
									$marital_status   = $contactRec_value['marital_status'];
									$utr_number   = $contactRec_value['utr_number'];
									$id   = $contactRec_value['id'];
									$client_id   = $contactRec_value['client_id'];
									$make_primary   = $contactRec_value['make_primary'];

									$contact_names = $this->Common_mdl->numToOrdinalWord($i) . ' Contact ';
									$name = ucfirst($contactRec_value['first_name']) . ' ' . ucfirst($contactRec_value['surname']);
								?>
									<div class="space-required">
										<div class="masonry-container floating_set">
											<div class="grid-sizer"></div>
											<div class="accordion-panel remove<?php echo $id; ?> contactcc ">
												<div class="box-division03 view-client-div">
													<div class="accordion-heading" role="tab" id="headingOne">
														<h3 class="card-title accordion-title">
															<a class="accordion-msg"><?php echo $name; ?></a>
														</h3>
													</div>

													<div class="primary-info03 floating_set">
														<div id="collapse" class="panel-collapse">
															<div class="basic-info-client1 CONTENT_CONTACT_PERSON">
																<!--   <div class="left-primary remove-space-set1"> -->
																<?php if (isset($title) && $title != '') { ?>
																	<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_title']; ?>" <?php echo $Setting_Delete['contact_title']; ?>>
																		<label class="col-sm-4 col-form-label">
																			<?php echo $Setting_Label['contact_title']; ?>
																		</label>
																		<div class="col-sm-8">
																			<span class="small-info"><?php if (isset($title)) {
																											echo $title;
																										} ?></span>
																		</div>
																	</div>
																<?php } ?>

																<?php if (isset($first_name) && $first_name != '') { ?>
																	<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_first_name']; ?>" <?php echo $Setting_Delete['contact_first_name']; ?>>
																		<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_first_name']; ?></label>
																		<div class="col-sm-8">
																			<span class="small-info"><?php echo $first_name; ?></span>
																		</div>
																	</div>
																<?php } ?>

																<?php if (isset($middle_name) && $middle_name != '') { ?>
																	<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_middle_name']; ?>" <?php echo $Setting_Delete['contact_middle_name']; ?>>
																		<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_middle_name']; ?></label>
																		<div class="col-sm-8">
																			<span class="small-info"><?php echo $middle_name; ?></span>
																		</div>
																	</div>
																<?php } ?>
																<?php if (isset($surname) && $surname != '') { ?>
																	<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_surname']; ?>" <?php echo $Setting_Delete['contact_surname']; ?>>
																		<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_surname']; ?></label>
																		<div class="col-sm-8">
																			<span class="small-info"><?php echo $surname; ?></span>
																		</div>
																	</div>
																<?php } ?>
																<?php if (isset($preferred_name) && $preferred_name != '') { ?>
																	<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_preferred_name']; ?>" <?php echo $Setting_Delete['contact_preferred_name']; ?>>
																		<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_preferred_name']; ?></label>
																		<div class="col-sm-8">
																			<span class="small-info"><?php echo $preferred_name; ?></span>
																		</div>
																	</div>
																<?php } ?>
																<?php if (isset($last_name) && $last_name != '') { ?>
																	<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_last_name']; ?>" <?php echo $Setting_Delete['contact_last_name']; ?>>
																		<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_last_name']; ?></label>
																		<div class="col-sm-8">
																			<span class="small-info"><?php echo $last_name; ?></span>
																		</div>
																	</div>
																<?php } ?>

																<?php if (isset($mobile) && $mobile != '') { ?>
																	<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_mobile']; ?>" <?php echo $Setting_Delete['contact_mobile']; ?>>
																		<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_mobile']; ?></label>
																		<div class="col-sm-8">
																			<span class="small-info"><?php echo $mobile; ?></span>
																		</div>
																	</div>
																<?php } ?>

																<?php if (isset($main_email) && $main_email != '') { ?>
																	<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_main_email']; ?>" <?php echo $Setting_Delete['contact_main_email']; ?>>
																		<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_main_email']; ?></label>
																		<div class="col-sm-8">
																			<span class="small-info"><?php echo $main_email; ?></span>
																		</div>
																	</div>
																<?php } ?>
																<?php if (isset($nationality) && $nationality != '') { ?>

																	<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_nationality']; ?>" <?php echo $Setting_Delete['contact_nationality']; ?>>
																		<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_nationality']; ?></label>
																		<div class="col-sm-8">
																			<span class="small-info"><?php echo $nationality; ?></span>
																		</div>
																	</div>
																<?php } ?>
																<?php if (isset($psc) && $psc != '') { ?>
																	<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_psc']; ?>" <?php echo $Setting_Delete['contact_psc']; ?>>
																		<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_psc']; ?></label>
																		<div class="col-sm-8">
																			<span class="small-info"><?php echo $psc; ?></span>
																		</div>
																	</div>
																<?php } ?>

																<?php if (isset($shareholder) && $shareholder != '') { ?>
																	<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_shareholder']; ?>" <?php echo $Setting_Delete['contact_shareholder']; ?>>
																		<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_shareholder']; ?></label>
																		<div class="col-sm-8">
																			<span class="small-info"><?php echo $shareholder; ?></span>
																		</div>
																	</div>
																<?php } ?>
																<?php if (isset($ni_number) && $ni_number != '') { ?>
																	<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_ni_number']; ?>" <?php echo $Setting_Delete['contact_ni_number']; ?>>
																		<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_ni_number']; ?></label>
																		<div class="col-sm-8">
																			<span class="small-info"><?php echo $ni_number; ?></span>
																		</div>
																	</div>
																<?php } ?>

																<?php if (isset($contactRec_value['country_of_residence']) && $contactRec_value['country_of_residence'] != '') { ?>
																	<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_country_of_residence']; ?>" <?php echo $Setting_Delete['contact_country_of_residence']; ?>>
																		<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_country_of_residence']; ?></label>
																		<div class="col-sm-8">
																			<span class="small-info"><?php if (isset($contactRec_value['country_of_residence']) && ($contactRec_value['country_of_residence'] != '')) {
																											echo $contactRec_value['country_of_residence'];
																										} ?></span>
																		</div>
																	</div>

																<?php } ?>
																<!-- </div>
                         <div class="left-primary right-primary"> -->
																<?php if (isset($contact_type) && $contact_type != '') { ?>
																	<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_contact_type']; ?>" <?php echo $Setting_Delete['contact_contact_type']; ?>>
																		<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_contact_type']; ?></label>
																		<div class="col-sm-8">
																			<span class="small-info"><?php echo $contact_type; ?></span>
																		</div>
																	</div>
																<?php } ?>
																<?php if (isset($contactRec_value['other_custom']) && $contactRec_value['other_custom'] != '') { ?>
																	<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_other_custom']; ?>" <?php echo $Setting_Delete['contact_other_custom']; ?>>
																		<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_other_custom']; ?> </label>
																		<div class="col-sm-8">
																			<span class="small-info"><?php if (isset($contactRec_value['other_custom']) && ($contactRec_value['other_custom'] != '')) {
																											echo $contactRec_value['other_custom'];
																										} ?></span>
																		</div>
																	</div>
																<?php } ?>
																<?php if (isset($address_line1) && $address_line1 != '') { ?>

																	<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_address_line1']; ?>" <?php echo $Setting_Delete['contact_address_line1']; ?>>
																		<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_address_line1']; ?></label>
																		<div class="col-sm-8">
																			<span class="small-info"><?php echo $address_line1; ?></span>
																		</div>
																	</div>
																<?php } ?>
																<?php if (isset($address_line2) && $address_line2 != '') { ?>
																	<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_address_line2']; ?>" <?php echo $Setting_Delete['contact_address_line2']; ?>>
																		<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_address_line2']; ?></label>
																		<div class="col-sm-8">
																			<span class="small-info"><?php echo $address_line2; ?></span>
																		</div>
																	</div>
																<?php } ?>
																<?php if (isset($town_city) && $town_city != '') { ?>
																	<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_town_city']; ?>" <?php echo $Setting_Delete['contact_town_city']; ?>>
																		<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_town_city']; ?></label>
																		<div class="col-sm-8">
																			<span class="small-info"><?php echo $town_city; ?></span>
																		</div>
																	</div>
																<?php } ?>
																<?php if (isset($post_code) && $post_code != '') { ?>
																	<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_post_code']; ?>" <?php echo $Setting_Delete['contact_post_code']; ?>>
																		<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_post_code']; ?></label>
																		<div class="col-sm-8">
																			<span class="small-info"><?php echo $post_code; ?></span>
																		</div>
																	</div>

																<?php } ?>

																<?php
																$land = json_decode($contactRec_value['landline']);
																//print_r( $land);
																if (!empty($land)) { ?>

																	<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_landline']; ?>" <?php echo $Setting_Delete['contact_landline']; ?>>
																		<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_landline']; ?></label>
																		<div class="col-sm-8">

																			<?php
																			foreach ($land as $key => $val) {

																				if (isset($val) && $val != '') { ?>
																					<span class="small-info"><?php echo $val; ?></span><br>

																			<?php }
																			} ?>

																		</div>
																	</div>

																<?php } ?>

																<?php
																$work = json_decode($contactRec_value['work_email']);
																if (!empty($land)) { ?>
																	<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_work_email']; ?>" <?php echo $Setting_Delete['contact_work_email']; ?>>
																		<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_work_email']; ?></label>
																		<div class="col-sm-8">
																			<?php
																			foreach ($work as $key => $val) {
																				if (isset($val) && $val != '') { ?>
																					<span class="small-info"><?php echo $val; ?></span><br>
																			<?php }
																			} ?>

																		</div>
																	</div>

																<?php } ?>
																<?php if (isset($date_of_birth) && $date_of_birth != '') { ?>

																	<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_date_of_birth']; ?>" <?php echo $Setting_Delete['contact_date_of_birth']; ?>>
																		<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_date_of_birth']; ?></label>
																		<div class="col-sm-8">
																			<span class="small-info"><?php echo $date_of_birth; ?></span>
																		</div>
																	</div>
																<?php } ?>

																<?php if (isset($nature_of_control) && $nature_of_control != '') { ?>

																	<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_nature_of_control']; ?>" <?php echo $Setting_Delete['contact_nature_of_control']; ?>>
																		<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_nature_of_control']; ?></label>
																		<div class="col-sm-8">
																			<span class="small-info"><?php echo $nature_of_control; ?></span>
																		</div>
																	</div>
																<?php } ?>
																<?php if (isset($marital_status) && $marital_status != '') { ?>
																	<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_marital_status']; ?>" <?php echo $Setting_Delete['contact_marital_status']; ?>>
																		<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_marital_status']; ?></label>
																		<div class="col-sm-8">
																			<span class="small-info"><?php echo $marital_status; ?></span>
																		</div>
																	</div>
																<?php } ?>
																<?php if (isset($utr_number) && $utr_number != '') { ?>
																	<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_utr_number']; ?>" <?php echo $Setting_Delete['contact_utr_number']; ?>>
																		<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_utr_number']; ?></label>
																		<div class="col-sm-8">
																			<span class="small-info"><?php echo $utr_number; ?></span>
																		</div>
																	</div>
																<?php } ?>

																<?php if (isset($contactRec_value['occupation']) && ($contactRec_value['occupation'] != '')) {  ?>
																	<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_occupation']; ?>" <?php echo $Setting_Delete['contact_occupation']; ?>>
																		<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_occupation']; ?></label>
																		<div class="col-sm-8">
																			<span class="small-info"><?php if (isset($contactRec_value['occupation']) && ($contactRec_value['occupation'] != '')) {
																											echo $contactRec_value['occupation'];
																										} ?></span>
																		</div>
																	</div>
																<?php } ?>
																<?php if (isset($contactRec_value['appointed_on']) && ($contactRec_value['appointed_on'] != '')) {  ?>
																	<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_appointed_on']; ?>" <?php echo $Setting_Delete['contact_appointed_on']; ?>>
																		<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_appointed_on']; ?></label>
																		<div class="col-sm-8">
																			<span class="small-info"><?php if (isset($contactRec_value['appointed_on']) && ($contactRec_value['appointed_on'] != '')) {
																											echo  $contactRec_value['appointed_on'];
																										} ?></span>
																		</div>
																	</div>
																<?php } ?>

																<?php if (isset($contactRec_value['personal_tax_return_required']) && ($contactRec_value['personal_tax_return_required'] != '')) {  ?>
																	<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_personal_tax_return_required']; ?>" <?php echo $Setting_Delete['contact_personal_tax_return_required']; ?>>
																		<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_personal_tax_return_required']; ?></label>
																		<div class="col-sm-8">
																			<span class="small-info"><?php if (isset($contactRec_value['personal_tax_return_required']) && ($contactRec_value['personal_tax_return_required'] != '')) {
																											echo ucwords($contactRec_value['personal_tax_return_required']);
																										} ?></span>
																		</div>
																	</div>
																<?php } ?>

																<?php
																$_POST['cnt'] = $i;
																$user_id_contact_id = $client['user_id'] . "--" . $contactRec_value['id'];

																$fields = $this->db->get_where('tblcustomfieldsvalues', "relid='" . $user_id_contact_id . "' AND section_name='Contact_Person' AND value!=''")->result_array();

																foreach ($fields as $fields_value) {
																	echo render_custom_fields_one('Contact_Person', $user_id_contact_id, "id=" . $fields_value['fieldid'], 1);
																}
																?>
																<!-- </div> -->
															</div>
														</div>
													</div>
												</div>
											</div>

											<?php if ($make_primary == '1') { ?>
												<div class="accordion-panel remove<?php echo $id; ?> contactcc ">
													<div class="box-division03 view-client-div">
														<div class="accordion-heading" role="tab" id="headingOne">
															<h3 class="card-title accordion-title">
																<a class="accordion-msg"><?php echo $name; ?>-Primary Contact</a>
															</h3>
														</div>
														<div class="primary-info03 floating_set">
															<div id="collapse" class="panel-collapse">
																<div class="basic-info-client1 CONTENT_CONTACT_PERSON">
																	<?php if (isset($title) && $title != '') { ?>
																		<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_title']; ?>" <?php echo $Setting_Delete['contact_title']; ?>>
																			<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_title']; ?></label>
																			<div class="col-sm-8">
																				<span class="small-info"><?php if (isset($title)) {
																												echo $title;
																											} ?></span>
																			</div>
																		</div>
																	<?php } ?>

																	<?php if (isset($first_name) && $first_name != '') { ?>
																		<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_first_name']; ?>" <?php echo $Setting_Delete['contact_first_name']; ?>>
																			<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_first_name']; ?></label>
																			<div class="col-sm-8">
																				<span class="small-info"><?php echo $first_name; ?></span>
																			</div>
																		</div>
																	<?php } ?>

																	<?php if (isset($mobile) && $mobile != '') { ?>
																		<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_mobile']; ?>" <?php echo $Setting_Delete['contact_mobile']; ?>>
																			<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_mobile']; ?></label>
																			<div class="col-sm-8">
																				<span class="small-info"><?php echo $mobile; ?></span>
																			</div>
																		</div>
																	<?php } ?>

																	<?php if (isset($main_email) && $main_email != '') { ?>
																		<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_main_email']; ?>" <?php echo $Setting_Delete['contact_main_email']; ?>>
																			<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_main_email']; ?></label>
																			<div class="col-sm-8">
																				<span class="small-info"><?php echo $main_email; ?></span>
																			</div>
																		</div>
																	<?php } ?>
																	<?php if (isset($nationality) && $nationality != '') { ?>

																		<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_nationality']; ?>" <?php echo $Setting_Delete['contact_nationality']; ?>>
																			<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_nationality']; ?></label>
																			<div class="col-sm-8">
																				<span class="small-info"><?php echo $nationality; ?></span>
																			</div>
																		</div>
																	<?php } ?>

																	<?php if (isset($shareholder) && $shareholder != '') { ?>
																		<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_psc']; ?>" <?php echo $Setting_Delete['contact_psc']; ?>>
																			<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_psc']; ?></label>
																			<div class="col-sm-8">
																				<span class="small-info"><?php echo $shareholder; ?></span>
																			</div>
																		</div>
																	<?php } ?>
																	<?php if (isset($ni_number) && $ni_number != '') { ?>
																		<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_ni_number']; ?>" <?php echo $Setting_Delete['contact_ni_number']; ?>>
																			<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_ni_number']; ?></label>
																			<div class="col-sm-8">
																				<span class="small-info"><?php echo $ni_number; ?></span>
																			</div>
																		</div>
																	<?php } ?>

																	<!--  </div> -->

																	<?php if (isset($contact_type) && $contact_type != '') { ?>
																		<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_contact_type']; ?>" <?php echo $Setting_Delete['contact_contact_type']; ?>>
																			<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_contact_type']; ?></label>
																			<div class="col-sm-8">
																				<span class="small-info"><?php echo $contact_type; ?></span>
																			</div>
																		</div>
																	<?php } ?>

																	<?php
																	$land = json_decode($contactRec_value['landline']);
																	//print_r( $land);
																	if (!empty($land)) { ?>

																		<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_landline']; ?>" <?php echo $Setting_Delete['contact_landline']; ?>>
																			<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_landline']; ?></label>
																			<div class="col-sm-8">

																				<?php
																				foreach ($land as $key => $val) {
																					if (isset($val) && $val != '') { ?>
																						<span class="small-info"><?php echo $val; ?></span><br>

																				<?php }
																				} ?>
																			</div>
																		</div>
																	<?php } ?>

																	<?php
																	$work = json_decode($contactRec_value['work_email']);
																	if (!empty($land)) {
																	?>
																		<div class="form-group row name_fields contact_sort" data-id="<?php echo $Setting_Order['contact_work_email']; ?>" <?php echo $Setting_Delete['contact_work_email']; ?>>
																			<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['contact_work_email']; ?></label>
																			<div class="col-sm-8">
																				<?php
																				foreach ($work as $key => $val) {
																					if (isset($val) && $val != '') { ?>

																						<span class="small-info"><?php echo $val; ?></span><br>
																				<?php
																					}
																				}
																				?>
																			</div>
																		</div>

																	<?php  }
																	$_POST['cnt'] = $i;
																	$user_id_contact_id = $client['user_id'] . "--" . $contactRec_value['id'];

																	$fields = $this->db->get_where('tblcustomfieldsvalues', "relid='" . $user_id_contact_id . "' AND section_name='Contact_Person' AND value!=''")->result_array();

																	foreach ($fields as $fields_value) {
																		echo render_custom_fields_one('Contact_Person', $user_id_contact_id, "id=" . $fields_value['fieldid'], 1);
																	}
																	?>
																</div>
															</div>
														</div>
													</div>
												</div>
											<?php } ?>
										</div>
									</div>
									<input type="hidden" name="make_primary[]" id="make_primary[]" value="<?php echo $make_primary; ?>">
								<?php $i++;
								} ?>
								<input type="hidden" name="incre" id="incre" value="<?php echo $i - 1; ?>">
								<div class="contact_form"></div>
								<input type="hidden" name="append_cnt" id="append_cnt" value="">
							</div>
							<!-- contact1 -->

							<div id="service" class="tab-pane fade  <?php /*if($res_suc!=''){ echo "active";}*/  ?>">
								<div class="space-required">
									<div class="basic-available-data">
										<div class="basic-available-data1">
											<table class="client-detail-table01 service-table-client">
												<thead>
													<tr>
														<th>Services</th>
														<th>( Services )Enable/Disable</th>
														<th>( Reminders )Enable/Disable</th>
														<th>( Text )Enable/Disable</th>
														<th>( Invoice Notifications )Enable/Disable</th>
														<th>HMRC Authorization</th>
													</tr>
												</thead>
												<tfoot class="ex_data1">
													<tr>
														<th>
														</th>
													</tr>
												</tfoot>
												<tbody>
													<?php

													$columns = array('1' => 'conf_statement', '2' => 'accounts', '3' => 'company_tax_return', '4' => 'personal_tax_return', '5' => 'vat', '6' => 'payroll', '7' => 'workplace', '8' => 'cis', '9' => 'cissub', '10' => 'bookkeep', '11' => 'p11d', '12' => 'management', '13' => 'investgate', '14' => 'registered', '15' => 'taxinvest', '16' => 'taxadvice');

													foreach ($other_services as $key => $value) {
														$columns[$value['id']] = $value['services_subnames'];
													}

													/*  $FirmEnabled_Services = $this->Common_mdl->get_FirmEnabled_Services();

                         $find_ongonig_service = array_diff_key($columns, $FirmEnabled_Services);

                         foreach ($find_ongonig_service as $key => $value)
                         {
                          if( $tab_on[ $value ]['tab']!==0 )
                          {
                            $FirmEnabled_Services[ $key ] = 1; 
                          }
                         }*/
													$client_services = $this->Common_mdl->getServicelist();

													foreach ($client_services as $key => $value) {
														/*(isset(json_decode($client[$columns[$value['id']]])->tab) && $client[$columns[$value['id']]] != '') ? $jsnCst =  json_decode($client[$columns[$value['id']]])->tab : $jsnCst = '';
                            */
														$cst = "";
														$cstre = "";
														$csttext = "";
														$cstinvoice = "";

														$cst = ($tab_on[$columns[$value['id']]]['tab'] !== 0) ? "checked='checked'" : "";

														/*(isset(json_decode($client[$columns[$value['id']]])->reminder) && $client[$columns[$value['id']]] != '') ? $jsnCstre =  json_decode($client[$columns[$value['id']]])->reminder : $jsnCstre = '';
                            ($jsnCstre!='') ? */

														$cstre = ($tab_on[$columns[$value['id']]]['reminder'] !== 0) ? "checked='checked'" :  "";

														/*(isset(json_decode($client[$columns[$value['id']]])->text) && $client[$columns[$value['id']]] != '') ? $jsnCsttext =  json_decode($client[$columns[$value['id']]])->text : $jsnCsttext = '';*/

														$csttext = ($tab_on[$columns[$value['id']]]['text'] !== 0) ?  "checked='checked'" : "";


														/*(isset(json_decode($client[$columns[$value['id']]])->invoice) && $client[$columns[$value['id']]] != '') ? $jsnCstinvoice =  json_decode($client[$columns[$value['id']]])->invoice : $jsnCstinvoice = '';*/

														$cstinvoice = ($tab_on[$columns[$value['id']]]['invoice'] !== 0) ?  "checked='checked'" : "";
														if (!empty($cst)) {
													?>
															<tr>
																<td><?php echo $value['service_name']; ?></td>
																<td class="switching"><input type="checkbox" class="js-small f-right services" name="confirm[tab]" data-id="cons" <?php if ($client['blocked_status'] == 0) {
																																														echo $cst;
																																													} ?> readonly></td>
																<td class="splwitch"><input type="checkbox" class="js-small f-right reminder" name="confirm[reminder]" <?php if ($client['blocked_status'] == 0 && $client['reminder_block'] == 0) {
																																											echo $cstre;
																																										} ?> data-id="enableconf" readonly></td>
																<td class="swi2 textwitch"><input type="checkbox" class="js-small f-right text" name="confirm[text]" <?php if ($client['blocked_status'] == 0) {
																																											echo $csttext;
																																										} ?> data-id="onet" readonly></td>
																<td class="invoicewitch"><input type="checkbox" class="js-small f-right invoice" name="confirm[invoice]" <?php if ($client['blocked_status'] == 0) {
																																												echo $cstinvoice;
																																											} ?> readonly></td>
																																											<td>																												<button type="submit" class="btn btn-success">Send</button>
															</td>
															</tr>
													<?php }
													}
													?>
													<?php
													//for contact person personal tax return  
													$i = 1;

													/*print_r( $contactRec );*/

													foreach ($contactRec as $key => $contactRec_val) {
														/*print_r( $contactRec_val );*/
														$ser    =   ($contactRec_val['has_ptr_service'] == 1 ? "checked='checked'" : '');
														$rem    =   ($contactRec_val['has_ptr_reminder'] == 1 ? "checked='checked'" : '');
														$inv    =   ($contactRec_val['has_ptr_invoice'] == 1 ? "checked='checked'" : '');
														$text   =   ($contactRec_val['has_ptr_text'] == 1 ? "checked='checked'" : '');
														if ($ser != '') {
													?>
															<tr>
																<td>
																	Personal Tax Return
																	<b class="ccp_name_<?php echo $i; ?>"> - <?php echo $contactRec_val['first_name'] . ' ' . $contactRec_val['last_name']; ?></b>
																</td>
																<td class="switching">
																	<input type="checkbox" class="js-small f-right services ccp" name="ccp_ptr_service[<?php echo $i; ?>]" data-id="ccp_<?php echo $i ?>" value="1" <?php echo $ser; ?> readonly>
																</td>
																<td class="swi1 splwitch">
																	<input type="checkbox" class="js-small f-right reminder ccp" name="ccp_ptr_reminder[<?php echo $i; ?>]" data-id="ccp_<?php echo $i ?>_reminder" value="1" <?php echo $rem; ?> readonly>
																</td>
																<td class="swi2 textwitch">
																	<input type="checkbox" class="js-small f-right text ccp" name="ccp_ptr_text[<?php echo $i; ?>]" data-id="ccp_<?php echo $i ?>" value="1" <?php echo $inv; ?> readonly>
																</td>
																<td class="swi3 invoicewitch">
																	<input type="checkbox" class="js-small f-right invoice ccp" name="ccp_ptr_invoice[<?php echo $i; ?>]" data-id="ccp_<?php echo $i ?>" value="1" <?php echo $text; ?> readonly>
																</td>
															</tr>
													<?php
														}
														$i++;
													} ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
							<!-- service -->
							<div id="assignto" class="tab-pane fade">
								<div class="masonry-container floating_set">
									<div class="grid-sizer"></div>
									<div class="accordion-panel responsible_team_tbl">
										<div class="box-division03 view-client-div">
											<div class="accordion-heading" role="tab" id="headingOne">
												<h3 class="card-title accordion-title">
													<a class="accordion-msg">Client Assignees</a>
												</h3>
											</div>
											<div class="basic-info-client1">
												<div class="basic-border1 ">
													<table class="table table-hover table-bordered">
														<thead>
															<tr>
																<th>Assignee Name</th>
															</tr>
														</thead>
														<tbody>
															<?php
															if (!empty($assignees)) {
																foreach ($assignees as $assignee) {
															?>
																	<tr class="success ykpackrowfirst ykpackrow">
																		<td><?php echo ucfirst($assignee['crm_name']); ?></td>
																	</tr>
															<?php
																}
															} ?>

														</tbody>
													</table>

												</div>
											</div>
										</div>
									</div>

									<div class="accordion-panel">
										<div class="box-division03 view-client-div">
											<div class="accordion-heading" role="tab" id="headingOne">
												<h3 class="card-title accordion-title">
													<a class="accordion-msg">Service Assignees </a>
												</h3>
											</div>
											<div id="collapse" class="panel-collapse">
												<div class="basic-info-client1 basic-border1">
													<?php
													if (!empty($service_wise_assigee)) {
													?>
														<table class="table table-hover table-bordered">
															<thead>
																<tr>
																	<th>Service Name</th>
																	<th>Manager</th>
																	<th>Assignees</th>
																</tr>
															</thead>
															<tbody>
																<?php
																foreach ($service_wise_assigee as $serv_name => $assignee_data) {
																?>
																	<tr class="success ykpackrowfirst ykpackrow">
																		<td><?php echo $serv_name; ?></td>
																		<td><?php echo implode(',', $assignee_data['manager']); ?></td>
																		<td><?php echo implode(',', $assignee_data['assignee']); ?></td>
																	</tr>
																<?php
																	$i++;
																}
																?>
															</tbody>
														</table>
													<?php } ?>
												</div>
											</div>
										</div>
									</div>
									<!-- accordion-panel -->
								</div>
							</div>
							<!-- assignto -->
							<!-- amlchecks -->
							<div id="amlchecks" class="tab-pane fade">

								<?php

								if ($client['select_responsible_type'] == 'staff' || $client['select_responsible_type'] == 'team' || $client['select_responsible_type'] == 'departments' || $client['select_responsible_type'] == 'members') {
									$members_css = $department_css = $team_css = $css = "display:block";
								} else {
									$members_css = $department_css = $team_css = $css = "display:none";
								}
								?>

								<div class="masonry-container floating_set">
									<div class="accordion-panel">
										<div class="box-division03 view-client-div">
											<div class="accordion-heading" role="tab" id="headingOne">
												<h3 class="card-title accordion-title">
													<a class="accordion-msg">Anti Money Laundering Checks </a>
												</h3>
											</div>
											<div id="collapse" class="panel-collapse">
												<div class="basic-info-client1 CONTENT_ANTI_MONEY_LAUNDERING_CHECKS">
													<?php if (isset($client['crm_assign_client_id_verified'])) { ?>
														<div class="form-group row radio_bts aml_sort" data-id="<?php echo $Setting_Order['crm_assign_client_id_verified']; ?>" <?php echo $Setting_Delete['crm_assign_client_id_verified']; ?>>
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_assign_client_id_verified']; ?> </label>
															<div class="col-sm-8">
																<input type="checkbox" class="js-small f-right fields" name="client_id_verified" id="client_id_verified" <?php if (isset($client['crm_assign_client_id_verified']) && ($client['crm_assign_client_id_verified'] == 'on')) { ?> checked="checked" <?php } ?> readonly>
															</div>
														</div>
													<?php } ?>
													<?php if (isset($client['crm_assign_type_of_id']) && ($client['crm_assign_type_of_id'] != '')) { ?>
														<div class="form-group row assign_cus_type aml_sort" data-id="<?php echo $Setting_Order['crm_assign_type_of_id']; ?>" <?php echo $Setting_Delete['crm_assign_type_of_id']; ?>>
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_assign_type_of_id']; ?> </label>
															<div class="col-sm-8">

																<span class="small-info"><?php if (isset($client['crm_assign_type_of_id'])) {
																								echo $client['crm_assign_type_of_id'];
																							} ?></span>
															</div>
														</div>
													<?php } ?>
													<!-- 29-08-2018 for multiple image upload option -->
													<?php if (isset($client['proof_attach_file']) && ($client['proof_attach_file'] != '')) { ?>
														<div class="form-group row aml_sort" data-id="<?php echo $Setting_Order['proof_attach_file']; ?>" <?php echo $Setting_Delete['proof_attach_file']; ?>>
															<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['proof_attach_file']; ?></label>
															<div class="col-sm-8">
															</div>

															<div class="attach-files showtm f-right">
																<?php
																if (isset($client['proof_attach_file'])) {
																?>
																	<div class="jFiler-items jFiler-row">
																		<ul class="jFiler-items-list jFiler-items-grid">
																			<?php
																			$ex_attach = array_filter(explode(',', $client['proof_attach_file']));
																			foreach ($ex_attach as $attach_key => $attach_value) {
																				$replace_val = str_replace(base_url(), '', $attach_value);
																				$ext = explode(".", $replace_val);
																				$extension = end($ext);
																				$res = $this->Common_mdl->geturl_image_or_not($extension);
																				// echo $res;
																				if ($res == 'image') {
																			?>
																					<li class="jFiler-item for_img_<?php echo $attach_key; ?>" data-jfiler-index="3" style="">
																						<input type="hidden" name="already_upload_img[]" value="<?php echo $attach_value; ?>">
																						<div class="jFiler-item-container">
																							<div class="jFiler-item-inner">
																								<div class="jFiler-item-thumb">
																									<div class="jFiler-item-status"></div>
																									<div class="jFiler-item-info">

																									</div>
																									<div class="jFiler-item-thumb-image"><img src="<?php echo base_url() . $attach_value; ?>" draggable="false"></div>
																								</div>
																							</div>
																					</li>
																				<?php
																				} // if image
																				else { ?>
																					<li class="jFiler-item jFiler-no-thumbnail for_img_<?php echo $attach_key; ?>" data-jfiler-index="2" style="">
																						<input type="hidden" name="already_upload_img[]" value="<?php echo $attach_value; ?>">
																						<div class="jFiler-item-container">
																							<div class="jFiler-item-inner">
																								<div class="jFiler-item-thumb">
																									<div class="jFiler-item-status"></div>
																									<a href="<?php echo base_url() . $attach_value; ?>" target="_blank">
																										<div class="jFiler-item-info">
																										</div>
																										<div class="jFiler-item-thumb-image">
																											<span class="jFiler-icon-file f-file f-file-ext-odt" style="background-color: rgb(63, 79, 211);">
																												<?php echo $attach_value; ?>
																											</span>
																										</div>
																									</a>

																								</div>
																							</div>
																						</div>
																					</li>
																			<?php } //else end

																			} // foreach
																			?>
																		</ul>
																	</div>
																<?php
																}
																?>
															</div>
														</div>

													<?php } ?>
													<!-- en dof image upload for type of ids -->
													<?php
													$new_arr = array();
													$all_val = (isset($client['crm_assign_type_of_id']) && $client['crm_assign_type_of_id'] != '') ? explode(',', $client['crm_assign_type_of_id']) : $new_arr;
													?>
													<div class="form-group row name_fields spanassign aml_sort" <?php if (in_array('Other_Custom', $all_val) && (isset($client['crm_assign_client_id_verified']) && ($client['crm_assign_client_id_verified'] != ''))) {
																												} else { ?> style="display:none" <?php } ?> data-id="<?php echo $Setting_Order['crm_assign_other_custom']; ?>" <?php echo $Setting_Delete['crm_assign_other_custom']; ?>>
														<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_assign_other_custom']; ?> </label>
														<div class="col-sm-8">
															<span class="small-info"><?php if (isset($client['crm_assign_other_custom']) && ($client['crm_assign_other_custom'] != '')) {
																							echo $client['crm_assign_other_custom'];
																						} ?></span>
														</div>
													</div>
													<?php if (isset($client['crm_assign_proof_of_address']) /*&& ($client['crm_assign_proof_of_address']!='')*/) { ?>
														<div class="form-group row radio_bts aml_sort" data-id="<?php echo $Setting_Order['crm_assign_proof_of_address']; ?>" <?php echo $Setting_Delete['crm_assign_proof_of_address']; ?>>
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_assign_proof_of_address']; ?> </label>
															<div class="col-sm-8">
																<input type="checkbox" class="js-small f-right fields" name="proof_of_address" id="proof_of_address" <?php if (isset($client['crm_assign_proof_of_address']) && ($client['crm_assign_proof_of_address'] == 'on')) { ?> checked="checked" <?php } ?> readonly>
															</div>
														</div>
													<?php } ?>
													<?php if (isset($client['crm_assign_meeting_client']) /*&& ($client['crm_assign_meeting_client']!='')*/) { ?>
														<div class="form-group row radio_bts aml_sort" data-id="<?php echo $Setting_Order['crm_assign_meeting_client']; ?>" <?php echo $Setting_Delete['crm_assign_meeting_client']; ?>>
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_assign_meeting_client']; ?> </label>
															<div class="col-sm-8">
																<input type="checkbox" class="js-small f-right fields" name="meeting_client" id="meeting_client" <?php if (isset($client['crm_assign_meeting_client']) && ($client['crm_assign_meeting_client'] == 'on')) { ?> checked="checked" <?php } ?> readonly>
															</div>
														</div>
													<?php } ?>
												</div>
											</div>
										</div>
									</div>
									</li>
								</div>
							</div>

							<!-- end of amlchecks -->
							<div id="other" class="tab-pane fade">
								<div class="masonry-container floating_set">
									<div class="grid-sizer"></div>
									<div class="accordion-panel">
										<div class="box-division03 view-client-div">
											<div class="accordion-heading" role="tab" id="headingOne">
												<h3 class="card-title accordion-title">
													<a class="accordion-msg"><?php echo $Setting_Label['crm_previous_accountant']; ?>
														<input type="checkbox" class="js-small f-right fields" name="previous_account" id="previous_account" value="on" <?php if (isset($client['crm_previous_accountant']) && ($client['crm_previous_accountant'] == 'on')) { ?> checked="checked" <?php } ?> readonly data-id="preacc">
													</a>
												</h3>
											</div>
											<?php
											(isset($client['crm_previous_accountant']) && $client['crm_previous_accountant'] == 'on') ? $pre =  "block" : $pre = 'none';

											?>
											<div id="collapse" class="panel-collapse">
												<div class="basic-info-client1 CONTENT_PREVIOUS_ACCOUNTS" id="others_box1">
													<div id="enable_preacc" style="display:<?php echo $pre; ?>;"></div>

													<?php if (isset($client['crm_other_name_of_firm']) && ($client['crm_other_name_of_firm'] != '')) { ?>
														<div class="form-group row name_fields others_details_sort" data-id="<?php echo $Setting_Order['crm_other_name_of_firm']; ?>" <?php echo $Setting_Delete['crm_other_name_of_firm']; ?>>
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_other_name_of_firm']; ?> </label>
															<div class="col-sm-8">

																<span class="small-info"><?php if (isset($client['crm_other_name_of_firm']) && ($client['crm_other_name_of_firm'] != '')) {
																								echo $client['crm_other_name_of_firm'];
																							} ?></span>
															</div>
														</div>
													<?php } ?>

													<?php if (isset($client['crm_other_address']) && ($client['crm_other_address'] != '')) { ?>
														<div class="form-group row others_details_sort" data-id="<?php echo $Setting_Order['crm_other_address']; ?>" <?php echo $Setting_Delete['crm_other_address']; ?>>
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_other_address']; ?> </label>
															<div class="col-sm-8">

																<p class="for-address"><?php if (isset($client['crm_other_address']) && ($client['crm_other_address'] != '')) {
																							echo $client['crm_other_address'];
																						} ?></p>
															</div>
														</div>
													<?php } ?>

													<!-- preacc close-->
													<?php if (isset($client['crm_other_contact_no']) && ($client['crm_other_contact_no'] != '')) {  ?>
														<div class="form-group row name_fields others_details_sort" data-id="<?php echo $Setting_Order['crm_other_contact_no']; ?>" <?php echo $Setting_Delete['crm_other_contact_no']; ?>>
															<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_other_contact_no']; ?></label>
															<div class="col-sm-8">
																<span class="small-info">
																	<?php if (isset($client['crm_other_contact_no']) && ($client['crm_other_contact_no'] != '')) {
																		$contact = explode('-', $client['crm_other_contact_no'], 2);
																		$code = $this->Common_mdl->select_record('countries', 'id', $contact[0]);
																		echo $code['name'] . '-' . $contact[1];
																	} ?>
																</span>
															</div>
														</div>
													<?php } ?>

													<?php if (isset($client['crm_other_email']) && ($client['crm_other_email'] != '')) {  ?>
														<div class="form-group row name_fields others_details_sort" data-id="<?php echo $Setting_Order['crm_other_email']; ?>" <?php echo $Setting_Delete['crm_other_email']; ?>>
															<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_other_email']; ?></label>
															<div class="col-sm-8">
																<span class="small-info"><?php if (isset($client['crm_other_email']) && ($client['crm_other_email'] != '')) {
																								echo $client['crm_other_email'];
																							} ?></span>
															</div>
														</div>
													<?php } ?>

													<?php if (isset($client['crm_other_chase_for_info']) && ($client['crm_other_chase_for_info'] != '')) {  ?>
														<div class="form-group row radio_bts  others_details_sort" data-id="<?php echo $Setting_Order['crm_other_chase_for_info']; ?>" <?php echo $Setting_Delete['crm_other_chase_for_info']; ?>>
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_other_chase_for_info']; ?> </label>
															<div class="col-sm-8">
																<input type="checkbox" class="js-small f-right fields" name="chase_for_info" id="chase_for_info" <?php if (isset($client['crm_other_chase_for_info']) && ($client['crm_other_chase_for_info'] == 'on')) { ?> checked="checked" <?php } ?> readonly>
															</div>
														</div>

													<?php } ?>

													<div class="form-group row others_details_sort" data-id="<?php echo $Setting_Order['crm_other_notes']; ?>" <?php if (isset($client['crm_other_chase_for_info']) && ($client['crm_other_chase_for_info'] == 'on')) { ?> <?php } else { ?> style="display: none;" <?php } ?>>
														<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_other_notes']; ?> </label>
														<div class="col-sm-8">
															<p class="for-address"><?php if (isset($client['crm_other_notes']) && ($client['crm_other_notes'] != '')) {
																						echo $client['crm_other_notes'];
																					} ?></p>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="accordion-panel">
										<div class="box-division03 view-client-div">
											<div class="accordion-heading" role="tab" id="headingOne">
												<h3 class="card-title accordion-title">
													<a class="accordion-msg">Additional Information - Internal Notes</a>
												</h3>
											</div>
											<div id="collapse" class="panel-collapse">
												<div class="basic-info-client1 CONTENT_ADDITIONAL_INFORMATION" id="others_box2">
													<?php if (isset($client['crm_other_internal_notes']) && ($client['crm_other_internal_notes'] != '')) { ?>
														<div class="form-group row others_details_sort" data-id="<?php echo $Setting_Order['crm_other_internal_notes']; ?>" <?php echo $Setting_Delete['crm_other_internal_notes']; ?>>
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_other_internal_notes']; ?> </label>
															<div class="col-sm-8">
																<p class="for-address"><?php if (isset($client['crm_other_internal_notes']) && ($client['crm_other_internal_notes'] != '')) {
																							echo $client['crm_other_internal_notes'];
																						} ?></p>
															</div>
														</div>
													<?php } ?>
												</div>
											</div>
										</div>
									</div>
									<!-- accordion-panel -->
									<div class="accordion-panel">
										<div class="box-division03 view-client-div">
											<div class="accordion-heading" role="tab" id="headingOne">
												<h3 class="card-title accordion-title">
													<a class="accordion-msg" style="display: inline-block;"><?php echo $Setting_Label['crm_other_invite_use']; ?>
														<input type="checkbox" class="js-small f-right fields" name="show_login" id="show_login" data-id="show_login" <?php if (!empty($client['crm_other_invite_use'])) {
																																											echo "checked='checked'";
																																										} ?> readonly>
													</a>
												</h3>
											</div>

											<div id="collapse" class="panel-collapse show_login" style="<?php if (empty($client['crm_other_invite_use'])) { ?>display: none <?php } ?>">
												<div class="basic-info-client1 CONTENT_CLIENT_LOGIN" id="others_box3">
													<!-- 30-08-2018 -->
													<?php if (isset($user['username']) && ($user['username'] != '')) { ?>
														<div class="form-group row name_fields">
															<label class="col-sm-4 col-form-label"><?php echo $Setting_Label['usertable_username']; ?></label>
															<div class="col-sm-8">
																<span class="small-info"><?php if (isset($user['username']) && ($user['username'] != '')) {
																								echo $user['username'];
																							} ?></span>
															</div>
														</div>
													<?php } ?>
													<!-- end of 30-08-2018 -->
												</div>
											</div>
										</div>
									</div>
									<!-- accordion-panel -->
									<div class="accordion-panel">
										<div class="box-division03 view-client-div">
											<div class="accordion-heading" role="tab" id="headingOne">
												<h3 class="card-title accordion-title">
													<a class="accordion-msg">Client Source</a>
												</h3>
											</div>
											<div id="collapse" class="panel-collapse">
												<div class="basic-info-client1 CONTENT_CLIENT_SOURCE" id="others_box4">
													<?php if (isset($client['crm_assign_source']) && !empty($client['crm_assign_source'])) { ?>
														<div class="form-group row others_details_sort" data-id="<?php echo $Setting_Order['crm_assign_source']; ?>" <?php echo $Setting_Delete['crm_assign_source']; ?>>
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_assign_source']; ?> </label>
															<div class="col-sm-8">
																<span class="small-info"><?php if (isset($client['crm_assign_source'])) {
																								echo $client['crm_assign_source'];
																							} ?></span>
															</div>
														</div>
													<?php } ?>
													<?php if (isset($client['crm_assign_source']) && $client['crm_assign_source'] == 'Existing Client') { ?>
														<div class="form-group row name_fields others_details_sort" data-id="<?php echo $Setting_Order['crm_refered_by']; ?>" <?php echo $Setting_Delete['crm_refered_by']; ?>>
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_refered_by']; ?> </label>
															<div class="col-sm-8">
																<span class="small-info"> <?php foreach ($referby as $referbykey => $referbyvalue) {
																								if (isset($client['crm_refered_by']) && $client['crm_refered_by'] == $referbyvalue['id']) {
																									echo ucfirst($referbyvalue['crm_company_name'] . "-" . $referbyvalue['crm_first_name']);
																								}
																							} ?></span>
															</div>
														</div>
													<?php } ?>

													<div class="form-group row name_fields others_details_sort" data-id="<?php echo $Setting_Order['crm_refered_by']; ?>" <?php echo $Setting_Delete['crm_refered_by']; ?>>
														<label class="col-sm-4 col-form-label"> Has Refered </label>
														<div class="col-sm-8">
															<span class="small-info"> 
																<ul class="tbl-refered"><?php 
																	$ctr = 1;
																	foreach ($ref_details as $key => $val) { ?>
																		<li>
																			<span>[<?php echo $ctr; ?>] </span>
																			<span><?php echo ucfirst($val); ?></span>
																		</li><?php
																		$ctr++;
																	} ?>
																</ul>
															</span>																
														</div>
													</div>

													<?php if (isset($client['crm_assign_relationship_client']) && !empty($client['crm_assign_relationship_client'])) {  ?>
														<div class="form-group row name_fields others_details_sort" data-id="<?php echo $Setting_Order['crm_assign_relationship_client']; ?>" <?php echo $Setting_Delete['crm_assign_relationship_client']; ?>>
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_assign_relationship_client']; ?> </label>
															<div class="col-sm-8">
																<span class="small-info"><?php if (isset($client['crm_assign_relationship_client']) && ($client['crm_assign_relationship_client'] != '')) {
																								echo $client['crm_assign_relationship_client'];
																							} ?></span>
															</div>
														</div>
														<?php
														$crm_assign_relationship_client = (isset($client['crm_assign_relationship_client']) ? $client['crm_assign_relationship_client'] : false);
														$crm_assign_relationship = render_custom_fields_edit('relationship_client', $rel_id, $crm_assign_relationship_client);
														if ($crm_assign_relationship != '') {

															$s_rel = $this->Common_mdl->getCustomFieldRec($rel_id, 'relationship_client');

															if ($s_rel) {
																foreach ($s_rel as $key_rel => $value_rel) {
																	if ($key_rel == '') {
																		$key_rel = '-';
																	}
																	if ($value_rel == '') {
																		$value_rel = '-';
																	}
														?>
																	<div class="form-group row name_fields">
																		<label class="col-sm-4 col-form-label"><?php echo $key_rel; ?></label>
																		<div class="col-sm-8">
																			<span class="small-info"><?php echo $value_rel; ?></span>
																		</div>
																	</div>
													<?php
																}
															}
														}
													} ?>
												</div>
											</div>
										</div>
									</div>
									<!-- accordion-panel -->

									<div class="accordion-panel" style="display: none;">
										<div class="box-division03 view-client-div">
											<div class="accordion-heading" role="tab" id="headingOne">
												<h3 class="card-title accordion-title">
													<a class="accordion-msg">Notes if any other</a>
												</h3>
											</div>
											<div id="collapse" class="panel-collapse">
												<div class="basic-info-client1" id="others_box5">
													<?php if (isset($client['crm_other_any_notes']) && ($client['crm_other_any_notes'] != '')) {  ?>
														<div class="form-group row others_details_sort" data-id="<?php echo $Setting_Order['crm_other_any_notes']; ?>" <?php echo $Setting_Delete['crm_other_any_notes']; ?>>
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Order['crm_other_any_notes']; ?> </label>
															<div class="col-sm-8">
																<p class="for-address"><?php if (isset($client['crm_other_any_notes']) && ($client['crm_other_any_notes'] != '')) {
																							echo $client['crm_other_any_notes'];
																						} ?></p>
															</div>
														</div>
													<?php } ?>
													<?php
													$s_other = $this->Common_mdl->getCustomFieldRec($rel_id, 'other');

													if ($s_other) {
														foreach ($s_other as $key_other => $value_other) {
															if ($key_other == '') {
																$key_other = '-';
															}
															if ($value_other == '') {
																$value_other = '-';
															}
													?>
															<div class="form-group row name_fields">
																<label class="col-sm-4 col-form-label"><?php echo $key_other; ?></label>
																<div class="col-sm-8">
																	<span class="small-info"><?php echo $value_other; ?></span>
																</div>
															</div>
													<?php
														}
													}
													?>
												</div>
											</div>
										</div>
									</div>
									<div class="accordion-panel">
										<div class="box-division03 view-client-div">
											<div class="accordion-heading" role="tab" id="headingOne">
												<h3 class="card-title accordion-title">
													<a class="accordion-msg">Client Referral</a>
												</h3>
											</div>
											<div id="collapse" class="panel-collapse">
												<div class="basic-info-client1 CONTENT_INVITE_CLIENT" id="referals_box1">
													<div style="display: none;">
														<!-- hided div -->
														<?php if (isset($client['crm_other_crm']) /*&& ($client['crm_other_crm']!='')*/) {  ?>
															<div class="form-group row radio_bts ">
																<label class="col-sm-4 col-form-label">crm</label>
																<div class="col-sm-8">
																	<input type="checkbox" class="js-small f-right fields" name="other_crm" id="other_crm" <?php if (isset($client['crm_other_crm']) && ($client['crm_other_crm'] == 'on')) { ?> checked="checked" <?php } ?> readonly>
																</div>
															</div>
														<?php } ?>
														<?php if (isset($client['crm_other_proposal']) /*&& ($client['crm_other_proposal']!='')*/) {  ?>
															<div class="form-group row radio_bts ">
																<label class="col-sm-4 col-form-label">Proposal</label>
																<div class="col-sm-8">
																	<input type="checkbox" class="js-small f-right fields" name="other_proposal" id="other_proposal" <?php if (isset($client['crm_other_proposal']) && ($client['crm_other_proposal'] == 'on')) { ?> checked="checked" <?php } ?> readonly>
																</div>
															</div>
														<?php } ?>
														<?php if (isset($client['crm_other_task']) /*&& ($client['crm_other_task']!='')*/) {  ?>
															<div class="form-group row radio_bts ">
																<label class="col-sm-4 col-form-label">Tasks</label>
																<div class="col-sm-8">
																	<input type="checkbox" class="js-small f-right fields" name="other_task" id="other_task" <?php if (isset($client['crm_other_task']) && ($client['crm_other_task'] == 'on')) { ?> checked="checked" <?php } ?> readonly>
																</div>
															</div>
														<?php } ?>
													</div><!-- hide div -->

													<?php if (isset($client['crm_other_send_invit_link']) && ($client['crm_other_send_invit_link'] != '')) {  ?>
														<div class="form-group row name_fields referal_details_sort" data-id="<?php echo $Setting_Order['crm_other_send_invit_link']; ?>">
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_other_send_invit_link']; ?> </label>
															<div class="col-sm-8">
																<p class="for-address"><?php if (isset($client['crm_other_send_invit_link']) && ($client['crm_other_send_invit_link'] != '')) {
																							echo $client['crm_other_send_invit_link'];
																						} ?></p>
															</div>
														</div>
													<?php } ?>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- accordion-panel -->
							</div>

							<!-- other -->
							<!-- referral tab section -->
							
							<div id="referral" class="tab-pane fade">
								<div class="masonry-container floating_set">
									<div class="grid-sizer"></div>
									<div class="accordion-panel">
										<div class="box-division03 view-client-div">
											<div class="accordion-heading" role="tab" id="headingOne">
												<h3 class="card-title accordion-title">
													<a class="accordion-msg">Client Referral</a>
												</h3>
											</div>
											<div id="collapse" class="panel-collapse">
												<div class="basic-info-client1 CONTENT_INVITE_CLIENT" id="referals_box1">
													<div style="display: none;">
														<!-- hided div -->
														<?php if (isset($client['crm_other_crm']) /*&& ($client['crm_other_crm']!='')*/) {  ?>
															<div class="form-group row radio_bts ">
																<label class="col-sm-4 col-form-label">crm</label>
																<div class="col-sm-8">
																	<input type="checkbox" class="js-small f-right fields" name="other_crm" id="other_crm" <?php if (isset($client['crm_other_crm']) && ($client['crm_other_crm'] == 'on')) { ?> checked="checked" <?php } ?> readonly>
																</div>
															</div>
														<?php } ?>
														<?php if (isset($client['crm_other_proposal']) /*&& ($client['crm_other_proposal']!='')*/) {  ?>
															<div class="form-group row radio_bts ">
																<label class="col-sm-4 col-form-label">Proposal</label>
																<div class="col-sm-8">
																	<input type="checkbox" class="js-small f-right fields" name="other_proposal" id="other_proposal" <?php if (isset($client['crm_other_proposal']) && ($client['crm_other_proposal'] == 'on')) { ?> checked="checked" <?php } ?> readonly>
																</div>
															</div>
														<?php } ?>
														<?php if (isset($client['crm_other_task']) /*&& ($client['crm_other_task']!='')*/) {  ?>
															<div class="form-group row radio_bts ">
																<label class="col-sm-4 col-form-label">Tasks</label>
																<div class="col-sm-8">
																	<input type="checkbox" class="js-small f-right fields" name="other_task" id="other_task" <?php if (isset($client['crm_other_task']) && ($client['crm_other_task'] == 'on')) { ?> checked="checked" <?php } ?> readonly>
																</div>
															</div>
														<?php } ?>
													</div><!-- hide div -->

													<?php if (isset($client['crm_other_send_invit_link']) && ($client['crm_other_send_invit_link'] != '')) {  ?>
														<div class="form-group row name_fields referal_details_sort" data-id="<?php echo $Setting_Order['crm_other_send_invit_link']; ?>">
															<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_other_send_invit_link']; ?> </label>
															<div class="col-sm-8">
																<p class="for-address"><?php if (isset($client['crm_other_send_invit_link']) && ($client['crm_other_send_invit_link'] != '')) {
																							echo $client['crm_other_send_invit_link'];
																						} ?></p>
															</div>
														</div>
													<?php } ?>
												</div>
											</div>
										</div>
									</div>

									<div class="chartclass">
										<h4>Client Referral</h4>
										<div id="chart-container">
										</div>
									</div>
								</div>
							</div>

							<div id="import_tab" class="tab-pane fade">
								<ul class="accordion-views">
									<?php
									$data['tab_on'] = $tab_on;
									$data['other_services'] = $other_services;
									$this->load->view("clients/client_details_service_view", $data);
									?>
								</ul>
							</div> <!-- import_tab close -->

							<div id="notes" class="tab-pane fade" style="display: none;">
								<div class="masonry-container floating_set">
									<div class="accordion-panel">
										<div class="box-division03 view-client-div">
											<div class="accordion-heading" role="tab" id="headingOne">
												<h3 class="card-title accordion-title">
													<a class="accordion-msg">Additional Information - Internal Notes</a>
												</h3>
											</div>
											<div id="collapse" class="panel-collapse">
												<div class="basic-info-client1">
													<div class="form-group row" data-id="<?php echo $Setting_Order['crm_other_internal_notes']; ?>" <?php echo $Setting_Delete['crm_other_internal_notes']; ?>>
														<label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_other_internal_notes']; ?> </label>
														<div class="col-sm-8">
															<p class="for-address"><?php if (isset($client['crm_other_internal_notes']) && ($client['crm_other_internal_notes'] != '')) {
																						echo $client['crm_other_internal_notes'];
																					} ?></p>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- accordion-panel -->
								</div>
							</div>

							<div id="newtimelineservices" class="tab-pane fade">
								<div class="modal-alertsuccess  alert alert-success-reminder succs" style="display: none;">
									<div class="newupdate_alert">
										<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
										<div class="pop-realted1">
											<div class="position-alert1 timeline_success_messages">
											</div>
										</div>
									</div>
								</div>
								<div class="client_timeline_services">
									<?php
									$data['client_details'] = $client;
									$data['client_id'] = $client['user_id'];
									$this->load->view('Timeline/client_timeline_services', $data);
									?>
								</div>
							</div>

							<!-- documents tab close -->
							<div id="task" class="tab-pane fade">
								<div class="space-required">
									<div class="all_user-section2 floating_set">
										<div class="all-usera1 user-dashboard-section1">
											<div class="text-right">
												<a class="btn btn-success add-client-btn" href="<?php echo base_url('user/new_task?client_id=' . $client['user_id']); ?>"> <i class="fa fa-plus"></i> Add Task</a>
											</div>
											<div class="client_section3 table-responsive p-t-2">
												<table class="table client_table1 text-center display nowrap" id="alltask" cellspacing="0" width="100%">
													<thead>
														<tr class="text-uppercase">
															<!--  <th>
                          <div class="checkbox-fade fade-in-primary">
                            <label>
                            <input type="checkbox"  id="bulkDelete"  />
                            <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                            </label>
                          </div>
                        </th> -->
															<th>Task Created</th>
															<th>Task Name</th>
															<!-- <th>CRM-Username</th> -->
															<th>Start Date</th>
															<th>Due Date</th>
															<th>Status</th>
															<th>Progress Status</th>
															<th>Priority</th>
															<th>Tag</th>
															<th>Assignto</th>
															<!--  <th>Actions</th> -->
														</tr>
													</thead>
													<tbody>
														<?php foreach ($task_list as $key => $value) {
															$client_contact_name = '';
															if($value['client_contacts_id'] != 0)
															{
																foreach ($contactRec as $key => $Rec)
																{
																	if($value['client_contacts_id'] == $Rec['id'])
																	{
																		$client_contact_name = ' ('.$Rec['first_name'].' '.$Rec['last_name'].')';
																	}
																}
															}
															error_reporting(E_ALL & ~E_NOTICE);
															$assignee_det = $this->Common_mdl->getAssignee('TASK', $value['id']);
															$cur_assignee_ids = Get_Module_Assigees('TASK',  $value['id']);
															$assignees = array();
															foreach ($assignee_det as $a_value) {
																if (in_array($a_value['id'], $cur_assignee_ids)) {
																	$assignees[] = $a_value;
																}
															}
															error_reporting(E_ALL);
															if ($value['task_status'] == '1') {
																$stat = 'Not Started';
															} else if ($value['task_status'] == '2') {
																$stat = 'In Progress';
															} else if ($value['task_status'] == '3') {
																$stat = 'Awaiting for a feedback';
															} else if ($value['task_status'] == '4') {
																$stat = 'Archive';
															} else if ($value['task_status'] = '5') {
																$stat = 'Complete';
															}
															$exp_tag = explode(',', $value['tag']);

															$progress_status = '';
															$progress_task_status = $this->Common_mdl->get_firm_progress();
															foreach ($progress_task_status as $p_status_val) {
																if ($p_status_val['id'] == $value['progress_status']) {
																	$progress_status = $p_status_val['status_name'];
																}
															}
														?>
															<tr id="<?php echo $value['id']; ?>">
																<!--   <td>
                          <div class="checkbox-fade fade-in-primary">
                            <label>
                            <input type='checkbox'  class='deleteRow' value="<?php echo $value['id']; ?>"  />
                            <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                            </label>
                          </div>
                        </td> -->
																<td><?php if ($value['created_date'] != '') {
																		echo date('d-m-Y', $value['created_date']);
																	} ?></td>
																<td><a href="<?php echo base_url() . 'user/task_details/' . $value['id']; ?>"><?php echo ucfirst($value['subject']).$client_contact_name; ?></a></td>
																<td><?php echo date('d-m-Y', strtotime($value['start_date'])); ?></td>
																<td><?php echo date('d-m-Y', strtotime($value['end_date'])); ?></td>
																<td><?php echo $stat; ?></td>
																<td><?php echo $progress_status; ?></td>
																<td>
																	<?php echo $value['priority']; ?>
																</td>
																<td>
																	<?php foreach ($exp_tag as $exp_tag_key => $exp_tag_value) {
																		echo $tagName = $this->Common_mdl->getTag($exp_tag_value) . ' ';
																	} ?>
																</td>
																<td class="user_imgs">
																	<?php
																	echo implode(",", array_column($assignees, 'crm_name'));
																	?>
																</td>
															</tr>
														<?php } ?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>


							<div id="reminder" class="tab-pane fade">
								<ul class="nav nav-pills">
									<li><a class="reminder_tap_link active" data-id="#next_queue_reminder">Queue Reminder</a></li>
									<li><a class="reminder_tap_link" data-id="#draft_reminder">Draft Reminder</a></li>
									<li><a class="reminder_tap_link" data-id="#sent_reminders">Sent Reminder</a></li>
								</ul>
								<div class="reminder-tab-content">

									<div id="next_queue_reminder" class="box_reminders01 toggle_reminder_content" style="display: block;">
										<h4> Next Queue Reminders </h4>
										<div class="space-required">
											<div class="all_user-section2 floating_set">
												<div class="all-usera1 user-dashboard-section1">
													<div class="client_section3 table-responsive ">
														<table class="table client_table1 text-center display nowrap" id="queue_reminder_table" cellspacing="0" width="100%">
															<thead>
																<tr class="text-uppercase">
																	<th>S.no</th>
																	<th>Service Name</th>
																	<th>REMINDER SUBJECT</th>
																	<th>REMINDER MESSAGE</th>
																	<th>Send DateTime</th>
																	<th style="display: none;"></th>
																</tr>
															</thead>
															<tbody>
																<?php
																$firm_settings = $this->Common_mdl->select_record("admin_setting", "firm_id", $_SESSION['firm_id']);
																$i = 1;
																//print_r($queue_reminders);
																foreach ($queue_reminders as $key => $value) {
																	$tr_class = 'normal_class';
																	$sub_ext = '';
																	if($value['status'] == 2)
																	{
																		$tr_class = 'red_class';
																		$sub_ext = '(STOPPED)';
																	}
																?>
																	<tr class="<?php echo $tr_class;?>">
																		<td><?php echo $i; ?></td>
																		<td><?php echo $value['service_name']; ?></td>
																		<td><?php echo $value['subject'].' '.$sub_ext; ?></td>
																		<td>
																			<a href="#" class="click_to_view_sr_message queue_reminders" reminder-id="<?php echo $value['id']?>">Click View Message</a>
																		</td>
																		<td><?php echo date('d-m-Y', $value['date']) . ' ' . $firm_settings['client_reminder_time']; ?></td>
																		<td style="display: none;" class="reminder_message">
																			<?php echo $value['body']; ?>
																		</td>
																	</tr>
																<?php $i++;
																} ?>
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div id="draft_reminder" class="box_reminders01 toggle_reminder_content" style="display: none;">
										<h4> Draft Reminders </h4>
										<button type="button" class="btn btn-danger pull-right delete_selected_rmdr" style="display: none;margin-right: 15px;" data-tableid="draft_reminder_table">Delete</button>
										<div class="space-required">
											<div class="all_user-section2 floating_set">
												<div class="all-usera1 user-dashboard-section1">
													<div class="client_section3 table-responsive ">
														<table class="table client_table1 text-center display nowrap" id="draft_reminder_table" cellspacing="0" width="100%">
															<thead>
																<tr class="text-uppercase">
																	<th>
																		<label class="custom_checkbox1">
																			<input type="checkbox" class="select_all_rmdr"><i></i>
																		</label>
																	</th>
																	<th>Service Name</th>
																	<th>REMINDER SUBJECT</th>
																	<th>REMINDER MESSAGE</th>
																	<th>Send DateTime</th>
																	<th>Action</th>
																	<th style="display: none;"></th>
																</tr>
															</thead>
															<tbody>
																<?php
																$firm_settings = $this->Common_mdl->select_record("admin_setting", "firm_id", $_SESSION['firm_id']);
																$i = 1;
																foreach ($draft_reminders as $key => $value) {
																	if(strpos($value['subject'],"VAT")!== false){
																		if(strpos($value['subject'],"First Reminder")!== false){
																			$date_which_will_replace = date('d-m-Y', $value['date']);
																		}
																		$pattern = "/\d{2}\-\d{2}\-\d{4}/";
																		if (preg_match($pattern, $value['subject'], $matches)) {
																			$to_be_replaced_date = $matches[0];
																		}
																		if($date_which_will_replace!=''){
																			$value['subject'] = str_replace($to_be_replaced_date,$date_which_will_replace,$value['subject']);
																			$value['body'] = str_replace($to_be_replaced_date,$date_which_will_replace,$value['body']);
																		}
																	}
																?>
																	<tr class="rmdr_tr_<?php echo $value['id']; ?>">
																		<td>
																			<label class="custom_checkbox1">
																				<input type="checkbox" class="select_rmdr" value="<?php echo $value['id']; ?>"><i></i>
																			</label>
																		</td>
																		<td><?php echo $value['service_name']; ?></td>
																		<td><?php echo $value['subject']; ?></td>
																		<td>
																			<a href="#" class="click_to_view_sr_message" reminder-id="<?php echo $value['id']?>">Click View Message</a>
																		</td>
																		<td>
																			<?php echo date('d-m-Y', $value['date']) . ' ' . $firm_settings['client_reminder_time']; ?>
																		</td>
																		<td>
																			<div style="display: flex;">
																				<a href="javascript:;" style="margin-right: 10px;" class="send_draft_reminder" data-id="<?php echo $value['id']; ?>">
																					<i class="fa fa-paper-plane" aria-hidden="true"></i>
																				</a>
																				<a href="javascript:;" class="delete_rmdr" data-id="<?php echo $value['id']; ?>">
																					<i class="icofont icofont-ui-delete" aria-hidden="true"></i>
																				</a>
																			</div>
																		</td>
																		<td style="display: none;" class="reminder_message">
																			<div class="message_subject">
																				<?php echo $value['subject']; ?>
																			</div>
																			<div class="message_body">
																				<?php echo $value['body']; ?>
																			</div>
																		</td>
																	</tr>
																<?php $i++;
																} ?>
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div id="sent_reminders" class="box_reminders01 toggle_reminder_content" style="display: none;">
										<h4>Sent Reminders</h4>
										<button type="button" class="btn btn-danger pull-right delete_selected_rmdr" style="display: none;margin-right: 15px;" data-tableid="sent_reminders_table">Delete</button>
										<div class="space-required">
											<div class="all_user-section2 floating_set">
												<div class="all-usera1 user-dashboard-section1">
													<div class="client_section3 table-responsive ">
														<table class="table client_table1 text-center display nowrap" id="sent_reminders_table" cellspacing="0" width="100%">
															<thead>
																<tr class="text-uppercase">
																	<th>
																		<label class="custom_checkbox1">
																			<input type="checkbox" class="select_all_rmdr"><i></i>
																		</label>
																	</th>
																	<th>Reminder Subject</th>
																	<th>Reminder Message</th>
																	<th style="display: none;"></th>
																	<th>Reminder Sent on</th>
																	<th>Action</th>
																</tr>
															</thead>
															<tbody>
																<?php
																$i = 1;
																foreach ($reminder_details as $key => $value) {
																?>
																	<tr class="rmdr_tr_<?php echo $value['id']; ?>">
																		<td>
																			<label class="custom_checkbox1">
																				<input type="checkbox" class="select_rmdr" value="<?php echo $value['id']; ?>"><i></i>
																			</label>
																		</td>
																		<td>
																			<?php echo $value['reminder_subject']; ?>
																		</td>
																		<td>
																			<a href="#" class="click_to_view_sr_message" reminder-id="<?php echo $value['id']?>">Click View Message</a>
																		</td>
																		<td style="display: none;" class="reminder_message">
																			<?php echo $value['reminder_message']; ?>
																		</td>
																		<td>
																			<?php echo date('d-m-Y', strtotime($value['created_at'])); ?>
																		</td>
																		<td>
																			<a href="javascript:;" class="delete_rmdr" data-id="<?php echo $value['id']; ?>">
																				<i class="icofont icofont-ui-delete" aria-hidden="true"></i>
																			</a>

																		</td>
																	</tr>
																<?php $i++;
																} ?>
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div id="leads" class="tab-pane fade">
								<div class="space-required">
									<div class="all_user-section2 floating_set">
										<div class="all-usera1 user-dashboard-section1">
											<div class="client_section3 table-responsive ">
												<table class="table client_table1 text-center display nowrap" id="allleads" cellspacing="0" width="100%">
													<thead>
														<tr class="text-uppercase">
															<th>SNO</th>
															<th>Name</th>
															<th>Company</th>
															<th>Email</th>
															<th>Phone</th>
															<th>Tags</th>
															<th>Assigned</th>
															<th>status</th>
															<th>source</th>
															<th>contact Date</th>
															<!--  <th>Action</th> -->
														</tr>
													</thead>
													<tbody>
														<?php $i = 1;

														foreach ($leads as $key => $value) {
															$assignee_det = $this->Common_mdl->getAssignee('LEADS', $value['id']);

															$status_name = $this->Common_mdl->GetAllWithWhere('leads_status', 'id', $value['lead_status']);
															if (!empty($status_name)) {
																$statusname = $status_name[0]['status_name'];
															} else {
																$statusname = '-';
															}

															/* if($value['source']==1)
                              {
                                $value['source'] = 'Google';
                              }elseif($value['source']==2)
                              {
                                $value['source'] = 'Facebook';
                              }else{
                                $value['source'] = '';
                              }*/

															$source_name = $this->Common_mdl->GetAllWithWhere('source', 'id', $value['source']);
															if (!empty($source_name)) {
																$value['source'] = $source_name[0]['source_name'];
																/*if($value['source']!='')
                              {
                                $value['source'] = $value['source'];*/
															} else {
																$value['source'] = '-';
															}

														?>
															<tr>
																<td><?php echo $i; ?></td>
																<td>
																	<!-- <a href="#" data-toggle="modal" data-target="#profile_lead_<?php echo $value['id']; ?>"><?php echo $value['name']; ?></a> -->
																	<a href="<?php echo base_url() . 'leads/leads_detailed_tab/' . $value['id']; ?>"><?php echo $value['name']; ?></a>
																</td>
																<!-- <td><?php echo $value['company']; ?></td> -->
																<td>
																	<?php
																	//echo $value['company'];
																	if (is_numeric($value['company'])) {
																		$client_query = $this->db->query("SELECT * FROM client WHERE autosave_status=0 and id='" . $value['company'] . "' order by id desc ")->result_array();
																		echo $client_query[0]['crm_company_name'];
																	} else {
																		echo $value['company'];
																	}

																	?>
																</td>
																<td><?php echo $value['email_address']; ?></td>
																<td><?php echo $value['phone']; ?></td>
																<td><?php echo $value['tags']; ?></td>
																<td class="user_imgs">
																	<?php
																	echo implode(",", array_column($assignee_det, 'crm_name'));
																	?>
																</td>
																<td class="lead_status_<?php echo $value['id']; ?>"><?php echo $statusname; ?></td>
																<td><?php echo $value['source']; ?></td>
																<td><?php echo date('d-m-Y', strtotime($value['contact_date'])); ?></td>
																<!-- <td>
                                 <p class="action_01">
                                    <a href="<?php echo base_url() . 'leads/delete/' . $value['id'] . '/0'; ?>" onclick="return confirm('Are you sure want to delete');"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>
                                  
                                    <a href="<?php echo base_url() . 'leads/edit_lead_view/' . $value['id'] . ''; ?>" ><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                                   
                                 </p>
                              </td>-->
															</tr>
														<?php $i++;
														} ?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>

							<!-- <div id="documents" class="tab-pane fade">
								<div class="space-required">
									<div class="all_user-section2 floating_set">
										<div class="all-usera1 user-dashboard-section1 client-doc-section">
											
										</div>
									</div>
								</div>
							</div> -->

							<div id="proposals" class="tab-pane fade">
								<div class="space-required">
									<div class="all_user-section2 floating_set">
										<div class="all-usera1 user-dashboard-section1">
											<div class="text-right">
												<a class="btn btn-success add-client-btn" href="<?php echo base_url('proposal_page/step_proposal?client_id=' . $client['user_id']); ?>"> <i class="fa fa-plus"></i> Add Proposal</a>
											</div>
											<div class="client_section3 table-responsive p-t-2">
												<table class="table client_table1 text-center display nowrap" id="allproposal" cellspacing="0" width="100%">
													<thead>
														<tr class="text-uppercase">
															<!-- <th>
																<div class="checkbox-fade fade-in-primary">
																	<label>
																	<input type="checkbox"  id="bulkDelete"  />
																	<span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
																	</label>
																</div>
															</th> -->
															<th>S.no</th>
															<th>Proposal Name</th>
															<th>Created Date</th>
															<th>Status</th>
															<!--  <th>Actions</th> -->
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 1;
														foreach ($proposals as $key => $value) {
															$random_string = generateRandomString('100');
															$link = base_url() . 'proposal_page/step_proposal/' . $random_string . '---' . $value['id'];
														?>

															<tr id="<?php echo $value['id']; ?>">
																<td><?php echo $i; ?>
																<td><a href="<?php echo $link; ?>"><?php echo $value['proposal_name']; ?></a></td>
																<td><?php echo date('d-m-Y', strtotime($value['created_at'])); ?></td>
																<td><?php echo $value['status']; ?></td>
															</tr>
															<div class="modal-alertsuccess alert alert-success" id="delete_proposal<?php echo $value['id']; ?>" style="display:none;">
																<div class="newupdate_alert"> <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
																	<div class="pop-realted1">
																		<div class="position-alert1">
																			Are you sure want to delete <b>
																				<a href="<?php echo base_url() . 'client/delete_proposal/' . $value['id']; ?>"> Yes </a></b> OR <b><a href="#" id="close">No</a></b></div>
																	</div>
																</div>
															</div>
														<?php $i++;
														} ?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div id="invoice" class="tab-pane fade">
								<div class="space-required">
									<div class="all_user-section2 floating_set">
										<div class="all-usera1 user-dashboard-section1">
											<div class="client_section3 table-responsive ">
												<table class="table client_table1 text-center display nowrap printableArea" id="home_table" cellspacing="0" width="100%">
													<thead>
														<tr class="text-uppercase">
															<!--   <th><div class="checkbox-fade fade-in-primary">
                          <label>
                          <input type="checkbox" id="select_all_invoice">
                          <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                          </label>
                     </div></th> -->
															<th>Client Email</th>
															<th>Date</th>
															<th>Due Date</th>
															<th>Invoice Number</th>
															<th>Reference</th>
															<!-- <th>Action</th> -->
														</tr>
													</thead>
													<tbody>

														<?php
														if ($invoice_details) {
															foreach ($invoice_details as $value) { ?>
																<tr class="client_info" id="client_info">
																	<td><a href="<?php echo base_url() . 'invoice/EditInvoice/' . $value['client_id'] ?>?preview=page"><?php echo $this->Common_mdl->get_field_value('user', 'crm_email_id', 'id', $value['client_email']); ?></a></td>
																	<td><?php echo date("M, d, Y", $value['invoice_date']); ?></td>
																	<td><?php if ($value['invoice_duedate'] == 'Expired') {
																			echo $value['invoice_duedate'];
																		} else {
																			echo date("M, d, Y", $value['invoice_duedate']);
																		} ?></td>
																	<td><?php echo $value['invoice_no']; ?></td>
																	<td><?php echo $value['reference']; ?></td>

																	<div class="modal-alertsuccess alert alert-success" id="delete_crm<?php echo $value['client_id']; ?>" style="display:none;">
																		<div class="newupdate_alert">
																			<a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
																			<div class="pop-realted1">
																				<div class="position-alert1">
																					Are you sure want to delete <b><a href="<?php echo base_url() . 'invoice/DeleteInvoice/' . $value['client_id']; ?>"> Yes </a></b> OR <b><a href="#" id="close">No</a></b></div>
																			</div>
																		</div>
																	</div>

																	<div class="modal-alertsuccess alert alert-success" id="delete_all_invoice<?php echo $value['client_id']; ?>" style="display:none;">
																		<div class="newupdate_alert">
																			<a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
																			<div class="pop-realted1">
																				<div class="position-alert1">
																					Are you sure want to delete <b><a href="#" class="delete_yes"> Yes </a></b> OR <b><a href="#" id="close">No</a></b></div>
																			</div>
																		</div>
																	</div>
																</tr>
															<?php  }
														} else { ?>
															<!--   <tr class="for-norecords">
                        <td colspan="5"><h5>No records to display</h5></td>
                      </tr> -->
														<?php } ?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="space-required">
								<div class="comp_cre">
									<span><?php if (isset($client['created_date'])) {
												echo 'Company Created' . ' ' . date('F j, Y, g:i', $client['created_date']);
											} ?></span>
									<span><?php if (isset($user['CreatedTime'])) {
												echo ';Updated' . ' ' . date('F j, Y, g:i', $user['CreatedTime']);
											} ?></span>

									<!--  <span><?php if (isset($user['CreatedTime'])) {
													echo 'Company Created' . ' ' . date('F j, Y, g:i', $user['CreatedTime']);
												} ?></span>
            <span><?php if (isset($client['created_date'])) {
						echo ';Updated' . ' ' . date('F j, Y, g:i', $client['created_date']);
					} ?></span> -->
								</div>
							</div>

							<!-- task tab tab close -->
						</div>
						<!-- tabcontent -->
					</div>
					<div class="floation_set text-right accordion_ups">
						<input type="hidden" name="user_id" id="user_id" value="<?php if (isset($client['user_id']) && ($client['user_id'] != '')) {
																					echo $client['user_id'];
																				} ?>" class="fields">
						<input type="hidden" name="client_id" id="client_id" value="<?php if (isset($user['client_id']) && ($user['client_id'] != '')) {
																						echo $user['client_id'];
																					} ?>" class="fields">
						<input type="hidden" id="company_house" name="company_house" value="<?php if (isset($user['company_roles']) && ($user['company_roles'] != '')) {
																								echo $user['company_roles'];
																							} ?>" class="fields">
						<!-- <input type="submit" class="add_acc_client" value="Save & Exit" id="submit"/> -->
					</div>
				</div>
			</div>
			<!-- information-tab -->
			<!--   </form> -->
	</section>
</div>
</div>
<!-- client-details-view -->

<!-- feedback all -->
<div class="modal fade" id="service_reminder_content" role="dialog" style="display: none;">
	<div class="modal-dialog modal-dialog-content">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header modal-header-content">				
				<h4 class="modal-title">Message Content</h4>
				<div class="btn-wrap">
					<button type="button" class="btn-success btn-sm send_mail_task" id="mark_sent_task" data-email="0" reminder_id="">Mark Sent & Create Task</button>
					<button type="button" class="btn-success btn-sm send_mail_task" id="send_task" data-email="1" reminder_id="">Send & Create Task</button>
				</div>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body modal-body-content">

			</div>
		</div>
	</div>
</div>
<!-- end fedd-->

<div class="modal fade" id="send_reminder_content_popup" role="dialog" style="display: none;">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Message Content</h4>
			</div>
			<div class="modal-body">
				<form id="send_rmeinder_form">
					<div class="form-group">
						<label>Subject</label>
						<input type="text" class="form-control" name="subject">
					</div>
					<div class="form-group">
						<label>Content</label>
						<textarea class="form-control" name="body" id="send_reminder_body"></textarea>
					</div>
					<input type="hidden" name="id">
					<button type="submit" class="btn btn-success pull-right">Send</button>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="add_custom_fields_section" style="display: none;">
	<?php

	/* $mandatory_custom = array('other','confirmation','accounts','personal_tax','investigation','assign_to','payroll','vat','management','customers');*/
	$firm_id = array('firm_id' => $_SESSION['firm_id']);
	$static_sections =
		[
			'required_information',
			'important_details',
			'AML_check',
			'others',
			'referral'
		];
	$sections = array_merge($static_sections, $sections);

	$result = $this->FirmFields_model->MyFirm_CustomFields($_SESSION['firm_id']);

	if (!empty($result['ids'])) {
		foreach ($sections as $sections_name) {
			$field_records = $this->db->query('SELECT id FROM tblcustomfields WHERE active = "1" AND section_name = "' . $sections_name . '" AND id IN (' . $result['ids'] . ')')->result_array();

			foreach ($field_records as $field_record) {
				$value = get_custom_field_value_one($rel_id, $field_record['id'], $sections_name, false);

				if (!empty($value)) {
					echo render_custom_fields_one($sections_name, $rel_id, 'id=' . $field_record['id'], 1);
				}
			}
		}
	}
	?>
</div>

<?php $this->load->view('includes/footer'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/org_chart/js/jquery.orgchart.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dropdown.js"></script>
<script src="<?php echo base_url(); ?>assets/js/pcoded.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/moment.min.js"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/add_client/Custom_Field_handler.js"></script>
<script type="text/javascript">
	function get_selected_rows(table_id) {
		var table = $("#" + table_id).DataTable();
		var ids = [];

		table.column(0).nodes().to$().each(function(index) {
			var checkbox = $(this).find("input[type='checkbox']");
			if (checkbox.is(":checked")) {
				ids.push(checkbox.val());
			}
		});
		console.log(ids);
		return ids;
	}

	function delete_rmdr(table_id, ids) {
		var db = 'client_reminder';

		if (table_id == "draft_reminder_table") {
			db = 'draft_service_reminder';
		}
		var Datatable = $('#' + table_id).DataTable();

		$.ajax({
			url: '<?php echo base_url(); ?>client/delete_service_reminder',
			type: 'POST',
			data: {
				ids: ids,
				table: db
			},
			dataType: 'json',
			beforeSend: function() {
				$('.LoadingImage').show();
			},
			success: function(data) {
				$('.LoadingImage').hide();
				if (data.result) {
					$('.info-box').find('.info-text').text('Reminder Delete Sccessfully..');
					$('.info-box').show();

					$.each(ids, function(i, v) {
						console.log('row_id' + v);
						Datatable
							.rows('.rmdr_tr_' + v)
							.remove()
							.draw();
					});
					setTimeout(function() {
						$('.info-box').hide();
					}, 1500);
				}
			}
		});

	}

	$(document).ready(function() {

		$(".datepicker").datepicker({
			dateFormat: 'd MM, y'
		});

		$('.datepicker_service').datepicker({
                 dateFormat: 'dd-mm-yy',
                 autoclose:true,
                 endDate: "today",
               //  minDate:0, 
                 changeMonth: true,
           changeYear: true,
             }).on('changeDate', function (ev) {
                     $(this).datepicker('hide');
                 });

		var pageLength = "<?php echo get_firm_page_length() ?>";

		$("#alltask").DataTable({
			"iDisplayLength": 10,
			"pageLength": pageLength,
			order: [], //ITS FOR DISABLE SORTING
			columnDefs: [{
				"orderable": false,
				"targets": '_all'
			}]
			//  "scrollX": true,
		});

		$("#sent_reminders_table").DataTable({
			"iDisplayLength": 10,
			"pageLength": pageLength,
			order: [], //ITS FOR DISABLE SORTING
			columnDefs: [{
				"orderable": false,
				"targets": '_all'
			}]
			//  "scrollX": true,
		});
		var draft_reminder_table = $("#draft_reminder_table").DataTable({
			"iDisplayLength": 10,
			"pageLength": pageLength,
			order: [], //ITS FOR DISABLE SORTING
			columnDefs: [{
				"orderable": false,
				"targets": '_all'
			}]
			//  "scrollX": true,
		});
		$("#queue_reminder_table").DataTable({
			"iDisplayLength": 10,
			"pageLength": pageLength,
			order: [], //ITS FOR DISABLE SORTING
			columnDefs: [{
				"orderable": false,
				"targets": '_all'
			}]
			//  "scrollX": true,
		});

		$("#allleads").DataTable({
			"iDisplayLength": 10,
			"pageLength": pageLength,
			order: [], //ITS FOR DISABLE SORTING
			columnDefs: [{
				"orderable": false,
				"targets": '_all'
			}]
			// "scrollX": true,
		});

		$("#allproposal").DataTable({
			"iDisplayLength": 10,
			"pageLength": pageLength,
			order: [], //ITS FOR DISABLE SORTING
			columnDefs: [{
				"orderable": false,
				"targets": '_all'
			}]
			//"scrollX": true,
		});
		$('#home_table').DataTable({
			"iDisplayLength": 10,
			"pageLength": pageLength,
			order: [], //ITS FOR DISABLE SORTING
			columnDefs: [{
				"orderable": false,
				"targets": '_all'
			}]
			//"scrollX": true,
		});


		$(document).on("change", ".select_rmdr", function(event) {
			var table = $(this).closest('table');
			var action_button = $(this).closest('.toggle_reminder_content').find('.delete_selected_rmdr');

			var select_all = table.find('.select_all_rmdr');
			console.log($('#' + table.attr('id')).length + "Table length");
			var dataTable = $('#' + table.attr('id')).DataTable();

			var tr = 0;
			var ck_tr = 0;

			dataTable.column(0).nodes().to$().each(function(index) {
				if ($(this).find("input[type='checkbox']").is(":checked")) {
					ck_tr++;
				}
				tr++;
			});

			select_all.prop("checked", false);
			if (tr == ck_tr) {
				select_all.prop("checked", true);
			}

			action_button.hide();
			if (ck_tr) {
				action_button.show();
			}
		});

		$(document).on("change", ".select_all_rmdr", function(event) {
			var table_id = $(this).closest('table').attr('id');
			var action_button = $(this).closest('.toggle_reminder_content').find('.delete_selected_rmdr');
			var dataTable = $('#' + table_id).DataTable();


			var Ischecked = $(this).is(':checked');

			dataTable.column(0).nodes().to$().each(function(index) {
				$(this).find("input[type='checkbox']").prop("checked", Ischecked);
			});

			action_button.hide();
			if (Ischecked) {
				action_button.show();
			}

		});

		$('.delete_selected_rmdr').click(function() {

			var table_id = $(this).attr('data-tableid');
			var ids = get_selected_rows(table_id);

			delete_rmdr(table_id, ids);

		});
		$('.delete_rmdr').click(function() {
			var res = confirm("Are you sure you want to Delete?");
			if(res == true){			
				var table_id = $(this).closest('table').attr('id');
				var id = $(this).attr('data-id');

				delete_rmdr(table_id, [id]);
			}
		});

		tinymce_config['selector'] = '#send_reminder_body';
		tinymce_config['height'] = '420px';
		tinymce.init(tinymce_config);

		tinymce_config['selector'] = '#notes_msg';
		tinymce_config['height'] = '300px';
		tinymce.init(tinymce_config);

		$('.send_draft_reminder').click(function() {

			var modal = $('#send_reminder_content_popup');
			modal.modal('show');
			var content = $(this).closest('tr').find('.reminder_message');

			var sub = content.find('.message_subject').text();
			var body = content.find('.message_body').html();

			modal.find('input[name="subject"]').val(sub.trim());
			modal.find('input[name="id"]').val($(this).attr('data-id'));
			tinymce.get("send_reminder_body").setContent(body);

		});

		$('#send_rmeinder_form').validate({
			ignore: false,
			rules: {
				subject: {
					required: true
				},
				body: {
					required: true
				}
			},
			submitHandler: function(form) {

				var formData = new FormData($("#send_rmeinder_form")[0]);
				$.ajax({
					url: '<?php echo base_url(); ?>client/send_draft_reminder',
					dataType: 'json',
					type: 'POST',
					data: formData,
					contentType: false,
					processData: false,
					beforeSend: function() {
						$('.LoadingImage').show();
					},
					success: function(data) {
						$('.LoadingImage').hide();
						if (data.result) {
							draft_reminder_table.rows('.rmdr_tr_' + data.id)
								.remove()
								.draw();
							$('.info-box').find('.info-text').text('Mail Send Sccessfully..');
							$('.info-box').show();
							$('#send_reminder_content_popup').modal('hide');
							setTimeout(function() {
								$('.info-box').hide();
							}, 1500);
						}
					}
				});
			}
		})

		$('.reminder_tap_link').click(function() {

			$('.reminder_tap_link').removeClass('active');
			$(this).addClass('active');
			var div_id = $(this).attr('data-id');
			console.log('div id' + div_id);
			$('.toggle_reminder_content').hide();
			$(div_id).show();
		});

		/*Service Reminder message view*/
		$('a.click_to_view_sr_message').click(function(e) {
			var message = $(this).closest('tr').find(".reminder_message").html();
			var reminder_id = $(this).attr('reminder-id');
			if($(this).hasClass('queue_reminders'))
			{
				$('#mark_sent_task').show();
				$('#send_task').show();
				$('#mark_sent_task').attr('reminder_id',reminder_id);
				$('#send_task').attr('reminder_id',reminder_id);
			}
			else
			{
				$('#mark_sent_task').hide();
				$('#send_task').hide();
			}
			$('#service_reminder_content .modal-body').html(message);
			$('#service_reminder_content').modal('show');
		});
		/*Service Reminder message view*/


		// prev click
		$('.signed-change2').click(function() {
			$('ol.CLIENT_CONTENT_TAB.nav-tabs .active').closest('li').prevAll('li:visible').eq(0).find('a').trigger('click');
			//console.log( $('ol.nav-tabs .active').closest('li').prevAll('li:visible').eq(0).find('a').attr('class') );
		});
		// next click
		$('.signed-change3').click(function() {
			$('ol.CLIENT_CONTENT_TAB.nav-tabs .active').closest('li').nextAll("li:visible").eq(0).find('a').trigger('click');
			//console.log( $('ol.nav-tabs .active').closest('li').nextAll("li:visible").eq(0).find('a').attr('class') );
		});

		$("ol.CLIENT_CONTENT_TAB.nav-tabs li.nav-item a.nav-link").click(function() {
			
			var id = $(this).attr('data-id');
			$(this).closest(".nav-tabs").find("a.nav-link").removeClass('active');
			$(this).addClass('active');

			$(this).closest(".nav-tabs").find("li.nav-item").removeClass("active");
			$(this).parent().addClass("active");

			$(".newupdate_design .tab-content .tab-pane").removeClass("active show");
			$(".newupdate_design .tab-content").find(id).addClass("active show");

			var items = $(this).closest(".nav-tabs").find("li.nav-item").filter(":visible");


			items.removeClass("active-previous");
			var item_len = items.length - 1;


			for (var i = 0; i <= item_len; i++) {
				if ($(items[i]).hasClass('active')) {
					break;
				}
				$(items[i]).addClass("active-previous");
			}

			if (i == item_len) {
				$('.signed-change3').hide();
			} else {
				$('.signed-change3').show();
			}

			if (i == 0) {
				$('.signed-change2').hide();
			} else {
				$('.signed-change2').show();
			}
		});

		$('.info-box').find('.close').click(function() {
			$('.info-box').hide();
		});


		//when page load
		var STAR_cnts = $("div#import_tab ul").find(".ccp_ptr_cnt.masonry-container");
		arrange_SATR_custom_fields(STAR_cnts);
		$('#details').find('#important_details_section div.clearfix').remove();

		$('[data-popup-open]').on('click', function(e) {
			var targeted_popup_class = jQuery(this).attr('data-popup-open');
			$('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
			e.preventDefault();
		});
		//----- CLOSE
		$('[data-popup-close]').on('click', function(e) {
			var targeted_popup_class = jQuery(this).attr('data-popup-close');
			$('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
			e.preventDefault();
		});

		var client_user_id = "<?php echo $client['user_id']; ?>";
		var TL_options = {};
		TL_options['search'] = "";
		TL_options['pagination_start'] = 0;
		TL_options['pagination_length'] = 50;
		TL_options['service'] = [];
		TL_options['section_filter'] = [];

		var get_time_line = function() {
			$.ajax({
				url: "<?php echo base_url(); ?>client/get_client_time_line/" + client_user_id + "/" + TL_options['pagination_start'],
				type: "POST",
				data: TL_options,
				dataType: 'json',
				//beforeSend : function(){ $('.LoadingImage').show(); },
				success: function(res) {
					console.log(res);
					$("#search_for_activity").html(res['content']);
					$('div.pageination').html(res['pagination']);
				},
			});
		};

		//load timeline when click timeline tab
		$('a[href="#newtimelineservices"]').click(get_time_line);

		$('a[href="#referral"]').click(function() {

			/*Client Reffreal Tree */
			var referal_tree_source = <?php echo json_encode($tree_source); ?>;

			if (!$('#chart-container').find(".orgchart").length) {
				$('#chart-container').orgchart({
					'data': referal_tree_source
				});
			}
			/*Client Reffreal Tree */
		});

		function updatenotes_count() {
			var clientId = '<?php echo $this->uri->segment(3); ?>';
			$.ajax({
				url: '<?php echo base_url(); ?>client/GetTimelineNotesCount',
				type: 'POST',
				data: {
					'clientId': clientId
				},

				success: function(response) {
					var data = JSON.parse(response);

					var notes_count = (data.notes != '0' ? '(' + data.notes + ')' : '');
					var email_count = (data.email != '0' ? '(' + data.email + ')' : '');
					var calllog_count = (data.calllog != '0' ? '(' + data.calllog + ')' : '');
					var smslog_count = (data.smslog != '0' ? '(' + data.smslog + ')' : '');
					var logactivity_count = (data.logactivity != '0' ? '(' + data.logactivity + ')' : '');
					var meeting_count = (data.meeting != '0' ? '(' + data.meeting + ')' : '');
					var schedule_count = (data.schedule != '0' ? '(' + data.schedule + ')' : '');

					$('button[data-id="notes"] p').html(notes_count);
					$('button[data-id="email"] p').html(email_count);
					$('button[data-id="calllog"] p').html(calllog_count);
					$('button[data-id="smslog"] p').html(smslog_count);
					$('button[data-id="logactivity"] p').html(logactivity_count);
					$('button[data-id="meeting"] p').html(meeting_count);
					$('button[data-id="schedule"] p').html(schedule_count);
				}
			});
		}

		updatenotes_count();

		function ClearTimelineNotesSection() {
			tinymce.get("notes_msg").setContent('');
			$("select#services_type").val('');
			$("input#call_date").val('');
			$("input[name='notes_id']").val('');
			$(".nav-item.notes a").trigger('click');
		}

		$('.show_all_activity').click(function() {

			$(this).addClass('applied_filter');
			$('.section_filter').removeClass('applied_filter');
			$('#service_filter').removeClass('applied_filter');
			$('#allactivitylog_section').val('');

			TL_options['section_filter'] = [];
			TL_options['service'] = [];
			TL_options['search'] = '';
			TL_options['pagination_start'] = 0;
			TL_options['pagination_length'] = 50;

			get_time_line();
		});

		$('#allactivitylog_section').keyup(debounce(function() {

			TL_options['pagination_start'] = 0;
			TL_options['search'] = $(this).val();
			get_time_line();

		}, 500));

		$('input.service_filter').change(function() {

			TL_options['service'] = [];
			TL_options['pagination_start'] = 0;
			$('input.service_filter:checked').each(function() {
				TL_options['service'].push($(this).val());
			});

			if (TL_options['service'].length)
				$(this).closest('div.button-group').find('button').addClass('applied_filter');
			else
				$(this).closest('div.button-group').find('button').removeClass('applied_filter');

			get_time_line();

		});

		$('button.section_filter').click(function() {

			var val = $(this).data('id');
			TL_options['pagination_start'] = 0;

			if ($(this).hasClass('applied_filter')) {
				$(this).removeClass('applied_filter');

				var i = TL_options['section_filter'].indexOf(val);

				TL_options['section_filter'].splice(i, 1);
			} else {
				$(this).addClass('applied_filter');
				TL_options['section_filter'].push(val);
			}
			get_time_line();
		});

		$(document).on("click", ".pin_to_top", function(event) {
			event.preventDefault();
			var timeline_id = $(this).attr('data-id');
			var section = $(this).attr('data-section');
			var activity = $(this).attr('data-activity');
			$.ajax({
				url: '<?php echo base_url(); ?>client/pin_to_top',
				type: 'POST',
				data: {
					timeline_id: timeline_id,
					section: section,
					activity: activity
				},
				dataType: 'json',
				beforeSend: function() {
					$('.LoadingImage').show();
				},
				success: function(data) {
					if(data.result)
					{
						$('.LoadingImage').hide();
						TL_options['pagination_start'] = 0;
						get_time_line();
						var text = 'Activity Unpinned..';
						if(activity == 'pin')
						{
							var text = 'Activity Pinned To The Top..';
						}
						$('.info-box').find('.info-text').text(text);
						$('.info-box').show();

						
						setTimeout(function() {
							$('.info-box').hide();
						}, 3000);
					}
				}
			});
		});


		$(document).on('click', '.button_pagination_nav a', function(e) {
			e.preventDefault();
			var index = $(this).attr('href');
			var index = index.split('/');

			TL_options['pagination_start'] = (typeof(index[1]) != 'undefined' ? index[1] : 0);
			get_time_line();
		});


		$('ol.all_user1.md-tabs a').click(function() {
			
			$(this).closest('ol').find('a').removeClass('active');
			$(this).addClass('active');
			$('input[name="notes_section"]').val($(this).data('value'));
		});

		$('#show_add_service_notes_popup').click(ClearTimelineNotesSection);

		$('#timeline_notes').validate({
			ignore: false,
			rules: {
				feed_msg: {
					required: true
				},
			},
			messages: {
				feed_msg: {
					required: "Please Enter Any Text Here"
				}
			},
			errorPlacement: function(error, element) {

				if ($(element).hasClass('notes_text_box')) {
					$(error).insertAfter($('.notes_text_box').closest('div.editor_note'));
				}
			},
			submitHandler: function(form) {

				var formData = new FormData($("#timeline_notes")[0]);

				$.ajax({
					url: '<?php echo base_url(); ?>client/SaveClientTimelineNotes',
					type: 'POST',
					dataType: 'json',
					data: formData,
					contentType: false,
					processData: false,
					beforeSend: function() {
						$('.LoadingImage').show();
					},
					beforeSend: function() {
						$('.LoadingImage').show();
					},
					success: function(data) {
						$('.LoadingImage').hide();
						if (data.result) {
							TL_options['pagination_start'] = 0;
							get_time_line();
							$('.info-box').find('.info-text').text('Note Save Sccessfully..');
							$('.info-box').show();
							$('#note_popup').modal('hide');
							setTimeout(function() {
								$('.info-box').hide();
							}, 1500);
						}
					}
				});
			}
		});


		$(document).on('click', '.client_timeline_note_delete', function() {

			var note_id = $(this).data('id');

			var action = function() {
				$.ajax({
					url: "<?php echo base_url() ?>client/DeleteClientTimelineNotes",
					type: "POST",
					data: {
						'note_id': note_id
					},
					beforeSend: function() {
						$('.LoadingImage').show();
					},
					success: function(res) {
						$('#Confirmation_popup').modal('hide');
						TL_options['pagination_start'] = 0;
						get_time_line();
						$('.info-box').find('.info-text').text('Note Deleted Sccessfully..');
						$('.info-box').show();
						setTimeout(function() {
							$('.info-box').hide();
						}, 1500);
					},
					complete: function() {
						$('.LoadingImage').hide();
					}
				});
			};

			Conf_Confirm_Popup({
				'OkButton': {
					'Handler': action
				},
				'Info': 'Do you want to Delete...!'
			});
			$('#Confirmation_popup').modal('show');
		});

		$(document).on('click', '.client_timeline_note_edit', function() {

			var note_id = $(this).data('id');

			$.ajax({
				url: "<?php echo base_url() ?>client/getClientTimelineNoteDetails",
				type: "post",
				dataType: 'json',
				data: {
					note_id: note_id
				},
				beforeSend: function() {
					$('.LoadingImage').show();
				},
				success: function(data) {
					$('.LoadingImage').hide();
					ClearTimelineNotesSection();

					tinymce.get("notes_msg").setContent(data.notes);
					$("#trig_acts a").removeClass('active');
					$("#trig_acts ." + data.module + " a").addClass('active');
					$("select#services_type").val(data.services_type);
					$("input#call_date").val(data.date);
					$("input[name='notes_id']").val(data.id);
					$(".nav-item.notes a[data-value='" + data.module + "']").trigger('click');
					$('#note_popup').modal('show');

				}, 
				complete: function() {
					$('.LoadingImage').hide();
				}
			});
		});

		$('.js-small').parent('.col-sm-8').addClass('addbts-radio');

		$('.accordion-views .toggle').click(function(e) {
			$(this).next('.inner-views').slideToggle(300);
			$(this).toggleClass('this-active');
		});

		/*For all tab section show hide option*/
		$(document).on('click', 'ol.nav.nav-tabs.all_user1.md-tabs li.nav-item', function() {
			var attr_id = $(this).find('a').attr('href');
			$('.management_section').children('.tab-content.card').children('.tab-pane').each(function() {
				$(this).removeClass('active');
			});
			$(attr_id).addClass('active');
		});

		/* $('.get-client-doc').on('click', function () {
			$.ajax({
				type: "get",
				url: "<?php echo base_url() . 'Documents?dir=/' . $client['user_id'] ?>",
				data: {
					from_client_tab : true,
				},
				beforeSend: function() {
					$('.LoadingImage').show();
				},	
				success: function (response) {
					$(".client-doc-section").html(response);
				},
				complete: function() {
					$('.LoadingImage').hide();
				}
			});
		}); */

	});

	$(document).on("click", ".reminder_in_queue", function(event) {
		event.preventDefault();
		var client_id = $(this).attr('data-client_id');
		var user_id = $(this).attr('data-user_id');
		var contact_id = $(this).attr('data-contact_id');
		var date = $(this).closest('div').find('.datepicker_service').val();
		if(date == ''){
			$('.info-box').find('.info-text').text('Please Select a Date..');
			$('.info-box').show();
			setTimeout(function() {
				$('.info-box').hide();
			}, 1500);
		}
		else
		{
			$.ajax({
			url: '<?php echo base_url(); ?>client/send_reminders_in_queue',
			type: 'POST',
			data: {
				client_id: client_id,
				user_id: user_id,
				contact_id: contact_id,
				date: date
			},
			dataType: 'json',
			beforeSend: function() {
				$('.LoadingImage').show();
			},
			success: function(data) {
				console.log(data);
				if(data.result)
				{
					$('.LoadingImage').hide();
					$('.info-box').find('.info-text').text('The Action Has Been Completed Successfully..');
					$('.info-box').show();

					setTimeout(function() {
						$('.info-box').hide();
						location.reload();
					}, 1500);
				}
			}
			});
		}
	});

	$(document).on("click", ".send_mail_task", function(event) {
		event.preventDefault();
		var is_email = $(this).attr('data-email');
		var reminder_id = $(this).attr('reminder_id');
		$.ajax({
			url: '<?php echo base_url(); ?>client/force_send_reminders',
			type: 'POST',
			data: {
				is_email: is_email,
				reminder_id: reminder_id
			},
			dataType: 'json',
			beforeSend: function() {
				$('.LoadingImage').show();
			},
			success: function(data) {
				console.log(data);
				if(data.result)
				{
					$('.LoadingImage').hide();
					$('.info-box').find('.info-text').text('Reminder Sent Sccessfully..');
					$('.info-box').show();

					setTimeout(function() {
						$('.info-box').hide();
						location.reload();
					}, 1500);
				}
				else{
					$('.LoadingImage').hide();
					$('.info-box').find('.info-text').text('Reminder Could Not Be Sent Because This Is A Stopped Reminder..');
					$('.info-box').show();
				}
			}
			});
		});

</script>