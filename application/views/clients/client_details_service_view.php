<?php 
  $accService = ($tab_on['accounts']['tab']!==0) ? "display:block" : 'display:none' ;
  $fill = ['conf_statement', 'accounts', 'company_tax_return', 'personal_tax_return', 'vat', 'payroll', 'workplace', 'cis', 'cissub', 'bookkeep', 'p11d', 'management', 'investgate', 'registered', 'taxinvest', 'taxadvice'];
  // $fill = array_keys($tab_on);
  $service_id = array_search('accounts', $fill) + 1;
?>
<li class="show-block" style="<?php echo $accService; ?>">
    <a href="#" class="toggle">  Accounts</a>
    <!--Account -->
  <div id="accounts" class="masonry-container floating_set inner-views">
  <div class="accordion-panel">
    <div class="box-division03">
      <div class="accordion-heading" role="tab" id="headingOne">
        <h3 class="card-title accordion-title">
          <a class="accordion-msg">Important Information</a>
        </h3>
      </div>
      <div id="collapse" class="panel-collapse">
        <div class="basic-info-client1 CONTENT_IMPORTANT_INFORMATION" id="accounts_box">
        <?php if(isset($client['crm_companies_house_authorisation_code']) && $client['crm_companies_house_authorisation_code']!=''){  ?>
          <div class="form-group row name_fields accounts_sec" data-id="<?php echo $Setting_Order['crm_companies_house_authorisation_code']; ?>" <?php echo $Setting_Delete['crm_companies_house_authorisation_code']; ?>>
            <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_companies_house_authorisation_code']; ?> </label>
            <div class="col-sm-8">                       
             <span class="small-info"><?php if(isset($client['crm_companies_house_authorisation_code'])){ echo $client['crm_companies_house_authorisation_code']; } ?></span>
            </div>
          </div>
          <?php } ?>

          <?php if(isset($client['crm_accounts_utr_number']) && ($client['crm_accounts_utr_number']!='') ){  ?>
          <div class="form-group row name_fields accounts_sec" data-id="<?php echo $Setting_Order['crm_accounts_utr_number']; ?>" <?php echo $Setting_Delete['crm_accounts_utr_number']; ?>>
            <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_accounts_utr_number']; ?> </label>
            <div class="col-sm-8">
              
              <span class="small-info"><?php if(isset($client['crm_accounts_utr_number']) && ($client['crm_accounts_utr_number']!='') ){ echo $client['crm_accounts_utr_number'];}?></span>
            </div>
          </div>
          <?php } ?>

          <?php if(isset($client['crm_companies_house_email_remainder']) && ($client['crm_companies_house_email_remainder']=='on') ){?> 

          <div class="form-group row radio_bts accounts_sec" data-id="<?php echo $Setting_Order['crm_companies_house_email_remainder']; ?>" <?php echo $Setting_Delete['crm_companies_house_email_remainder']; ?>>
            <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_companies_house_email_remainder']; ?> </label>
            <div class="col-sm-8">
              <input type="checkbox" class="js-small f-right fields" name="company_house_reminder" id="company_house_reminder" <?php if(isset($client['crm_companies_house_email_remainder']) && ($client['crm_companies_house_email_remainder']=='on') ){?> checked="checked"<?php } ?> readonly>
            </div>
          </div>
          <?php }  ?>
        </div>
      </div>
    </div>
  </div>
  <!-- accordion-panel -->
  <div class="accordion-panel">
    <div class="box-division03">
      <div class="accordion-heading" role="tab" id="headingOne">
        <h3 class="card-title accordion-title">
          <a class="accordion-msg">Accounts</a>
        </h3>
      </div>
      <div id="collapse" class="panel-collapse">
        <div class="basic-info-client1 CONTENT_ACCOUNTS" id="accounts_box1">
          <?php 
            (isset(json_decode($client['accounts'])->reminder) && $client['accounts'] != '') ? $two =  json_decode($client['accounts'])->reminder : $two = '';
            
            ($two !='') ? $two_acc = "" : $two_acc = "display:none;";
            
            ?>
            <?php if(isset($client['crm_hmrc_yearend']) && ($client['crm_hmrc_yearend']!='') ){ ?>
          <div class="form-group row name_fields  accounts_sec" data-id="<?php echo $Setting_Order['crm_hmrc_yearend']; ?>">
            <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_hmrc_yearend']; ?></label>
            <div class="col-sm-8">
             
              <span class="small-info"><?php if(isset($client['crm_hmrc_yearend']) && ($client['crm_hmrc_yearend']!='') ){ echo date("d-m-Y", strtotime($client['crm_hmrc_yearend']));}?></span>
            </div>
          </div>
          <?php } ?>

          <?php if(!empty($client['crm_accounts_last_made_up_to_date'])){ ?>

            <div class="form-group row name_fields date_birth accounts_sec" data-id="<?php echo $Setting_Order['crm_accounts_last_made_up_to_date']; ?>" <?php echo $Setting_Delete['crm_accounts_last_made_up_to_date']; ?>>
              <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_accounts_last_made_up_to_date']; ?></label>
               <div class="col-sm-8 edit-field-popup1">
                <?php  echo Change_Date_Format( $client['crm_accounts_last_made_up_to_date'] ); ?>
               </div>
            </div>
          <?php } ?>  

          <?php if(isset($client['crm_ch_accounts_next_due']) && ($client['crm_ch_accounts_next_due']!='') ){ ?>
          <div class="form-group row name_fields  accounts_sec" data-id="<?php echo $Setting_Order['crm_ch_accounts_next_due']; ?>" <?php echo $Setting_Delete['crm_ch_accounts_next_due']; ?>>
            <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_ch_accounts_next_due']; ?> </label>
            <div class="col-sm-8">
              
              <span class="small-info"><?php if(isset($client['crm_ch_accounts_next_due']) && ($client['crm_ch_accounts_next_due']!='') ){ echo date("d-m-Y", strtotime($client['crm_ch_accounts_next_due']));}?></span>
            </div>

          </div>
          <?php } ?>
      </div>
      <div class="col-sm-10">
              <span class="small-info">
                <label class="col-sm-4 col-form-label">Choose the date from which you want to set the reminders</label>
                  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span><input type="text" name="accounts" placeholder="dd-mm-yyyy" value="" class="fields datepicker_service" readonly>
              </span>
              <span class="small-info" style="margin-top: 10px;">
                <button class="btn-primary btn-sm reminder_in_queue" data-client_id="<?php echo $client['id'];?>" data-user_id="0" data-contact_id="<?php echo $service_id; ?>">Create Reminders</button>
              </span>
          </div>
        </div>
    </div>
  </div>
  <!-- accordion-panel -->
  <div class="accordion-panel enable_acc" style="<?php echo $two_acc?>">
    <div class="box-division03">
      <div class="accordion-heading" role="tab" id="headingOne">
        <h3 class="card-title accordion-title">
          <a class="accordion-msg">Accounts Reminders</a>
        </h3>
      </div>
      <div id="collapse" class="panel-collapse">
        <div class="basic-info-client1 CONTENT_ACCOUNTS_REMINDERS" id="accounts_box2" >
           <?php if(isset($client['crm_accounts_next_reminder_date']) && ($client['crm_accounts_next_reminder_date']!='') ){?>
          <div class="form-group row name_fields accounts_sec" data-id="<?php echo $Setting_Order['crm_accounts_next_reminder_date']; ?>"
            <?php echo $Setting_Delete['crm_accounts_next_reminder_date']; ?>>
            <div class="col-sm-8">                 
              <label class="col-sm-4 col-form-label">
                <a id="accounts_custom_reminder_link" name="accounts_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/2" target="_blank">
                  <?php echo $Setting_Label['crm_accounts_next_reminder_date']; ?>
                </a>
              </label>
              <input type="checkbox" class="js-small f-right fields" name="accounts_next_reminder_date" id="accounts_next_reminder_date"  checked="checked"  readonly>
            </div>
          </div>
        <?php } ?>

          <?php if(isset($client['crm_accounts_custom_reminder']) && ($client['crm_accounts_custom_reminder']!='') ){?>
          <div class="form-group row radio_bts accounts_sec" data-id="<?php echo $Setting_Order['crm_accounts_custom_reminder']; ?>" <?php echo $Setting_Delete['crm_accounts_custom_reminder']; ?>>
            <label id="accounts_custom_reminder_label" name="accounts_custom_reminder_label" class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_accounts_custom_reminder']; ?> </label>
            <div class="col-sm-8">
              <input type="checkbox" class="js-small f-right fields" name="accounts_custom_reminder" id="accounts_custom_reminder"  checked="checked" readonly>
            </div>
          </div>
        <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- accounts -->
</li>

<li class="show-block" style="<?php echo ( $tab_on['bookkeep']['tab']!==0  ? "display: block" :"display:none;"); ?>">
  <a href="#" class="toggle">Bookkeeping</a>
  <div id="bookkeeptab" class="masonry-container floating_set inner-views">
    <div class="grid-sizer"></div>
          <div class="accordion-panel">
            <div class="box-division03">
              <div class="accordion-heading" role="tab" id="headingOne">
                <h3 class="card-title accordion-title">
                  <a class="accordion-msg">Bookkeeping</a>
                </h3>
              </div>
             <div id="collapse" class="panel-collapse">
                  <div class="basic-info-client1 CONTENT_BOOKKEEPING" id="bookkeep_box1">
                     <div class="form-group row bookkeep_sort" data-id="<?php echo $Setting_Order['crm_bookkeeping']; ?>" <?php echo $Setting_Delete['crm_bookkeeping']; ?>>
                        <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_bookkeeping']; ?> </label>
                        <div class="col-sm-8">
                          <?php if(isset($client['crm_bookkeeping'])){ echo $client['crm_bookkeeping']; }?> 
                        </div>
                     </div>
                     <?php if(isset($client['crm_next_booking_date']) && ($client['crm_next_booking_date']!='') ){ ?>
                     <div class="form-group row help_icon date_birth bookkeep_sort" data-id="<?php echo $Setting_Order['crm_next_booking_date']; ?>">
                        <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_next_booking_date']; ?> </label>
                        <div class="col-sm-8">
                          <?php if(isset($client['crm_next_booking_date']) && ($client['crm_next_booking_date']!='') ){ echo  Change_Date_Format($client['crm_next_booking_date']);}?>
                        </div>
                     </div>
                     <?php } ?>
                      <?php if(isset($client['crm_method_bookkeeping']) && $client['crm_method_bookkeeping']!=''){ ?>
                     <div class="form-group row bookkeep_sort" data-id="<?php echo $Setting_Order['crm_method_bookkeeping']; ?>" <?php echo $Setting_Delete['crm_method_bookkeeping']; ?>>
                        <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_method_bookkeeping']; ?> </label>
                        <div class="col-sm-8">
                          <?php if(isset($client['crm_method_bookkeeping'])){ echo $client['crm_method_bookkeeping']; }?>
                        </div>
                     </div>
                     <?php } ?>
                     <?php if(isset($client['crm_client_provide_record']) && $client['crm_client_provide_record']!='') { ?>
                     <div class="form-group row bookkeep_sort" data-id="<?php echo $Setting_Order['crm_client_provide_record']; ?>" <?php echo $Setting_Delete['crm_client_provide_record']; ?>>
                        <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_client_provide_record']; ?> </label>
                        <div class="col-sm-8">
                           <?php if(isset($client['crm_client_provide_record'])) { echo $client['crm_client_provide_record']; }?> 
                        </div>
                     </div>
                     <?php } ?>
                  </div>
            </div>
            </div>
          </div>
          <!-- accordion-panel -->
          <div class="accordion-panel enable_book" >
            <div class="box-division03">
              <div class="accordion-heading" role="tab" id="headingOne">
                <h3 class="card-title accordion-title">
                  <a class="accordion-msg">Bookkeep Reminders</a>
                </h3>
              </div>
              <div id="collapse" class="panel-collapse">
                <div class="basic-info-client1 CONTENT_BOOKKEEPING_REMINDERS" id="bookkeep_box2">

                   <?php if(isset($client['crm_bookkeep_next_reminder_date']) && ($client['crm_bookkeep_next_reminder_date']!='') ){ ?>
                  <div class="form-group row  date_birth bookkeep_sort" data-id="<?php echo $Setting_Order['crm_bookkeep_next_reminder_date']; ?>" <?php echo $Setting_Delete['crm_bookkeep_next_reminder_date']; ?>>
                   <!--  <label class="col-sm-4 col-form-label">Next Reminder Date</label> -->

                    <div class="col-sm-8">
                       <label class="col-sm-4 col-form-label"><a id="bookkeep_add_custom_reminder_link" name="bookkeep_add_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/10" target="_blank"><?php echo $Setting_Label['crm_bookkeep_next_reminder_date']; ?> </a></label>

                       <input type="checkbox" class="js-small f-right fields as_per_firm " name="bookkeep_next_reminder_date" id="bookkeep_next_reminder_date"   checked="checked";  readonly>

                   
                    </div>
                  </div>
                  <?php } ?>
                   <?php if(isset($client['crm_bookkeep_add_custom_reminder']) && ($client['crm_bookkeep_add_custom_reminder']!='') ){?> 
                  <div class="form-group row radio_bts bookkeep_sort" data-id="<?php echo $Setting_Order['crm_bookkeep_add_custom_reminder']; ?>" <?php echo $Setting_Delete['crm_bookkeep_add_custom_reminder']; ?>>
                    <label id="bookkeep_add_custom_reminder_label" name="bookkeep_add_custom_reminder_label" class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_bookkeep_add_custom_reminder']; ?> </label>
                    <div class="col-sm-8">
                      <input type="checkbox" class="js-small f-right fields" name="bookkeep_add_custom_reminder" id="bookkeep_add_custom_reminder"checked="checked" readonly> 
                    </div>

                  </div>
                  <?php } ?>

                </div>
                <div class="col-sm-10">
                    <span class="small-info">
                    <label class="col-sm-4 col-form-label">Choose the date from which you want to set the reminders</label>
                      <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span><input type="text" name="bookkeep" placeholder="dd-mm-yyyy" value="" class="fields datepicker_service" readonly>
                  </span>
                    <span class="small-info" style="margin-top: 10px;">
                <button class="btn-primary btn-sm reminder_in_queue" data-client_id="<?php echo $client['id'];?>" data-user_id="0" data-contact_id="<?php echo (array_search('bookkeep', $fill) + 1); ?>">Create Reminders</button>
              </span>
            </div>
              </div>
            </div>
          </div>
        
          <!-- accordion-panel -->
        </div>
        <!-- bookkeep tab close-->
      </li>


<?php 
  $cnsService = ($tab_on['conf_statement']['tab']!==0) ? 'display:block' : 'display:none';
?>
<li class="show-block" style="<?php echo $cnsService?>">
      <a href="#" class="toggle">  Confirmation Statement</a>
      <div id="confirmation-statement" class="masonry-container floating_set inner-views">
        <div class="grid-sizer"></div>
        <div class="accordion-panel">
          <div class="box-division03">
            <div class="accordion-heading" role="tab" id="headingOne">
              <h3 class="card-title accordion-title">
                <a class="accordion-msg">Important Information</a>
              </h3>
            </div>
            <div id="collapse" class="panel-collapse">
              <div class="basic-info-client1 CONTENT_IMPORTANT_INFORMATION" id="box">

               <?php if(isset($client['crm_companies_house_authorisation_code']) && ($client['crm_companies_house_authorisation_code']!='') ){  ?> 
                <div class="form-group row name_fields" data-id="<?php echo $Setting_Order['crm_companies_house_authorisation_code']; ?>" <?php echo $Setting_Delete['crm_companies_house_authorisation_code']; ?>>
                  <label class="col-sm-4 col-form-label">
                    <?php echo $Setting_Label['crm_companies_house_authorisation_code']; ?>
                  </label>
                  <div class="col-sm-8">                      
                    <span class="small-info">
                      <?php if(isset($client['crm_companies_house_authorisation_code'])){ echo $client['crm_companies_house_authorisation_code']; } ?>
                    </span>
                  </div>
                </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
        <!-- accordion-panel -->
        <div class="accordion-panel">
          <div class="box-division03">
            <div class="accordion-heading" role="tab" id="headingOne">
              <h3 class="card-title accordion-title">
                <a class="accordion-msg">Confirmation Statement</a>
              </h3>
            </div>
         
            <div id="collapse" class="panel-collapse">
               <div class="basic-info-client1 CONTENT_CONFIRMATION_STATEMENT" id="box1">
                  <?php 
                     (isset(json_decode($client['conf_statement'])->reminder) && $client['conf_statement'] != '') ? $one =  json_decode($client['conf_statement'])->reminder : $one = '';
                     ($one!='') ? $one_cs = "" : $one_cs = "display:none;";
                     
                     ?> 
                      <?php if(isset($client['crm_confirmation_statement_date']) && ($client['crm_confirmation_statement_date']!='') ){  ?>                                    
                    <div class="form-group row name_fields sorting_conf" data-id="<?php echo $Setting_Order['crm_confirmation_statement_date']; ?>">
                     <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_confirmation_statement_date']; ?> </label>
                     <div class="col-sm-8 edit-field-popup1">
                       <?php 
                        if(isset($client['crm_confirmation_statement_date']) && ($client['crm_confirmation_statement_date']!='') ){ echo date("d-m-Y", strtotime($client['crm_confirmation_statement_date'])) ;}?>
                     </div>
                    </div>
                  <?php } ?>

                 <?php if(isset($client['crm_confirmation_statement_due_date']) && ($client['crm_confirmation_statement_due_date']!='') ){  ?>  
                  <div class="form-group row name_fields sorting_conf" data-id="<?php echo $Setting_Order['crm_confirmation_statement_due_date']; ?>" <?php echo $Setting_Delete['crm_confirmation_statement_due_date']; ?> >
                     <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_confirmation_statement_due_date']; ?> </label>
                     <div class="col-sm-8 edit-field-popup1">
                        <?php if(isset($client['crm_confirmation_statement_due_date']) && ($client['crm_confirmation_statement_due_date']!='') ){ echo date("d-m-Y", strtotime($client['crm_confirmation_statement_due_date']));}?>
                     </div>
                  </div>
                  <?php } ?>

                  <?php if(isset($client['crm_confirmation_statement_last_made_up_to_date'])){ ?>
                  <div class="form-group row name_fields date_birth sorting_conf" data-id="<?php echo $Setting_Order['crm_confirmation_statement_last_made_up_to_date']; ?>" <?php echo $Setting_Delete['crm_confirmation_statement_last_made_up_to_date']; ?>>
                         <label class="col-sm-4 col-form-label"> 
                          <?php echo $Setting_Label['crm_confirmation_statement_last_made_up_to_date']; ?>
                          </label>
                         <div class="col-sm-8 edit-field-popup1">
                            <?php if(isset($client['crm_confirmation_statement_last_made_up_to_date'])){
                             echo Change_Date_Format( $client['crm_confirmation_statement_last_made_up_to_date']); }  ?>
                         </div>
                  </div>
                <?php } ?>
               </div>
            </div>

          </div>
        </div>
        <!-- accordion-panel -->
        <div class="accordion-panel enable_conf" style="<?php echo $one_cs;?>">
          <div class="box-division03">
            <div class="accordion-heading" role="tab" id="headingOne">
              <h3 class="card-title accordion-title">
                <a class="accordion-msg">Confirmation Reminders</a>
              </h3>
            </div>
            <div id="collapse" class="panel-collapse">
              <div class="basic-info-client1 CONTENT_CONFIRMATION_REMINDERS" id="box2">

                <?php if(isset($client['crm_confirmation_email_remainder']) && ($client['crm_confirmation_email_remainder']!='') ){?>

                <div class="form-group row name_fields sorting_conf" data-id="<?php echo $Setting_Order['crm_confirmation_email_remainder']; ?>" <?php echo $Setting_Delete['crm_confirmation_email_remainder']; ?>>
                  <div class="col-sm-8">
                    <input type="hidden" name="confirmation_next_reminder" id="confirmation_next_reminder" placeholder="" value="<?php if(isset($client['crm_confirmation_email_remainder']) && ($client['crm_confirmation_email_remainder']!='') ){ echo $client['crm_confirmation_email_remainder'];}?>" class="fields datepicker">
                    <a name="add_custom_reminder_link" id="add_custom_reminder_link" href="<?php echo base_url();?>User/reminder_settings/4" target="_blank"><?php echo $Setting_Label['crm_confirmation_email_remainder']; ?></a>
                    <input type="checkbox" class="js-small f-right fields" name="confirmation_next_reminder_date" id="confirmation_next_reminder_date"  checked="checked"  readonly>
                  </div>
                </div>
              <?php } ?>
              

                <?php if(isset($client['crm_confirmation_add_custom_reminder']) && ($client['crm_confirmation_add_custom_reminder']!='') ){?>

                <div class="form-group row radio_bts sorting_conf" data-id="<?php echo $Setting_Order['crm_confirmation_add_custom_reminder']; ?>" <?php echo $Setting_Delete['crm_confirmation_add_custom_reminder'];?> >
                  <label class="col-sm-4 col-form-label" id="add_custom_reminder_label" name="add_custom_reminder_label"> <?php echo $Setting_Label['crm_confirmation_add_custom_reminder'];?> </label>
                  <div class="col-sm-8">                      
                    <input type="checkbox" class="js-small f-right fields" name="create_task_reminder" id="create_task_reminder"  checked="checked" readonly>
                  </div>
                </div>

                <?php } ?> 
              </div>
              <div class="col-sm-10">  
                  <span class="small-info">
                  <label class="col-sm-4 col-form-label">Choose the date from which you want to set the reminders</label>
                    <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span><input type="text" name="conf_statement" placeholder="dd-mm-yyyy" value="" class="fields datepicker_service" readonly>
                </span>
                  <span class="small-info" style="margin-top: 10px;">
                <button class="btn-primary btn-sm reminder_in_queue" data-client_id="<?php echo $client['id'];?>" data-user_id="0" data-contact_id="<?php echo (array_search('conf_statement', $fill) + 1); ?>">Create Reminders</button>
              </span>
            </div>
            </div>
          </div>
        </div>
        <!-- accordion-panel -->
        <div class="accordion-panel">
          <div class="box-division03">
            <div class="accordion-heading" role="tab" id="headingOne">
              <h3 class="card-title accordion-title">
                <a class="accordion-msg">Other Details</a>
              </h3>
            </div>
            <div id="collapse" class="panel-collapse">
              <div class="basic-info-client1 CONTENT_OTHERS" id="box3">


              <?php if(isset($client['crm_confirmation_officers']) && ($client['crm_confirmation_officers']!='') ){  ?>
                <div class="form-group row sorting_conf" data-id="<?php echo $Setting_Order['crm_confirmation_officers']; ?>" <?php echo $Setting_Delete['crm_confirmation_officers']; ?>>
                  <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_confirmation_officers']; ?> </label>
                  <div class="col-sm-8">                       
                    <p class="for-address"><?php if(isset($client['crm_confirmation_officers']) && ($client['crm_confirmation_officers']!='') ){ echo $client['crm_confirmation_officers'];}?></p>
                  </div>
                </div>
                <?php } ?>
                <?php if(isset($client['crm_share_capital']) && ($client['crm_share_capital']!='') ){ ?>

                <div class="form-group row sorting_conf" data-id="<?php echo $Setting_Order['crm_share_capital']; ?>" <?php echo $Setting_Delete['crm_share_capital']; ?>>
                  <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_share_capital']; ?> </label>
                  <div class="col-sm-8">                      
                    <p class="for-address"><?php if(isset($client['crm_share_capital']) && ($client['crm_share_capital']!='') ){ echo $client['crm_share_capital'];}?></p>
                  </div>
                </div>
                <?php } ?>

                <?php if(isset($client['crm_shareholders']) && ($client['crm_shareholders']!='') ){ ?>
                <div class="form-group row sorting_conf" data-id="<?php echo $Setting_Order['crm_shareholders']; ?>" <?php echo $Setting_Delete['crm_shareholders']; ?>>
                  <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_shareholders']; ?> </label>
                  <div class="col-sm-8">                      
                    <p class="for-address"><?php if(isset($client['crm_shareholders']) && ($client['crm_shareholders']!='') ){ echo $client['crm_shareholders'];}?></p>
                  </div>
                </div>
                <?php } ?>

                <?php if(isset($client['crm_people_with_significant_control']) && ($client['crm_people_with_significant_control']!='') ){  ?>
                <div class="form-group row sorting_conf" data-id="<?php echo $Setting_Order['crm_people_with_significant_control']; ?>" <?php echo $Setting_Delete['crm_people_with_significant_control']; ?>>
                  <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_people_with_significant_control']; ?> </label>
                  <div class="col-sm-8">                      
                    <p class="for-address"><?php if(isset($client['crm_people_with_significant_control']) && ($client['crm_people_with_significant_control']!='') ){ echo $client['crm_people_with_significant_control'];}?></p>
                  </div>
                </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
  </div>
        <!-- accordion-panel -->
        <!-- confirmation-statement -->
 </li>

 <li class="show-block" style="<?php echo ( $tab_on['company_tax_return']['tab']!==0  ? "display: block" :"display:none;");?>">
    <a href="#" class="toggle">Company Tax Return</a>
   <!-- end accounts invoice -->
   <div id="companytax" class="masonry-container floating_set inner-views">
   <div class="grid-sizer"></div>
      <div class="accordion-panel">
         <div class="box-division03">
            <div class="accordion-heading" role="tab" id="headingOne">
               <h3 class="card-title accordion-title">
                  <a class="accordion-msg">Company Tax Return </a>
               </h3>
            </div>
            <div id="collapse" class="panel-collapse">
               <div class="basic-info-client1 CONTENT_COMPANY_TAX_RETURN" id="companytax_box">
                   <?php if(isset($client['crm_accounts_due_date_hmrc']) && $client['crm_accounts_due_date_hmrc']!=''){ ?>

                         <!-- <div class="form-group row name_fields company_tax_return_sec" >
                            <label class="col-sm-4 col-form-label">Accounts Due Date - HMRC</label>
                            <div class="col-sm-8">
                             <?php echo date("d-m-Y", strtotime("+1 years", strtotime($client['crm_ch_yearend'])));
                                  ?>
                            </div>
                         </div> -->

                         <div class="form-group row name_fields company_tax_return_sec" data-id="<?php echo $Setting_Order['crm_accounts_due_date_hmrc']; ?>">
                            <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_accounts_due_date_hmrc']; ?></label>
                            <div class="col-sm-8">
                             <?php echo date("d-m-Y", strtotime($client['crm_accounts_due_date_hmrc']));
                                  ?>
                            </div>
                         </div>

                         <?php } ?>

                        <?php if(isset($client['crm_ch_accounts_next_due']) && $client['crm_ch_accounts_next_due']!=''){ ?>
                         <div class="form-group row name_fields company_tax_return_sec" data-id="<?php echo $Setting_Order['crm_ch_accounts_next_due']; ?>" <?php echo $Setting_Delete['crm_ch_accounts_next_due'];?>>
                            <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_ch_accounts_next_due']; ?></label>
                            <div class="col-sm-8">
                              <?php 
                              echo Change_Date_Format( $client['crm_ch_accounts_next_due'] ); 
                              /*echo date("d-m-Y", strtotime("+1 days", strtotime($client['crm_ch_accounts_next_due'])));*/
                              ?>
                            </div>
                         </div>
                         <?php } ?>
               </div>
            </div>
         </div>
      </div>
      <div class="accordion-panel enable_tax" style="<?php echo ( $tab_on['company_tax_return']['reminder']!==0 ? "display: block" :"display:none;");?>">
         <div class="box-division03">
            <div class="accordion-heading" role="tab" id="headingOne">
               <h3 class="card-title accordion-title">
                  <a class="accordion-msg">Company Tax Reminders</a>
               </h3>
            </div>
            <div id="collapse" class="panel-collapse">
               <div class="basic-info-client1 CONTENT_COMPANY_TAX_REMINDER" id="companytax_box1">
                    <?php if(isset($client['crm_company_next_reminder_date']) && ($client['crm_company_next_reminder_date']!='') ){?> 

                      <div class="form-group row name_fields" data-id="<?php echo $Setting_Order['crm_company_next_reminder_date']; ?>" <?php echo $Setting_Delete['crm_company_next_reminder_date']; ?>>
                       <!--  <label class="col-sm-4 col-form-label">Next Reminder Date</label> -->
                        <div class="col-sm-8">
                    
                          
                          <label class="col-sm-4 col-form-label">
                            <a id="company_custom_reminder_link" name="company_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/3" target="_blank">
                            <?php echo $Setting_Label['crm_company_next_reminder_date']; ?>
                            </a>
                          </label>

                          <input type="checkbox" class="js-small f-right fields" name="company_next_reminder_date" id="company_next_reminder_date"  checked="checked"  readonly>
                        </div>
                      </div>
                      <?php } ?>

                      <?php if(isset($client['crm_company_custom_reminder']) && ($client['crm_company_custom_reminder']!='') ){?> 
                      <div class="form-group row radio_bts accounts_sec" data-id="<?php echo $Setting_Order['crm_company_custom_reminder']; ?>" <?php echo $Setting_Delete['crm_company_custom_reminder']; ?>>
                        <label id="company_custom_reminder_label" name="company_custom_reminder_label" class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_company_custom_reminder']; ?> </label>
                        <div class="col-sm-8">
                          <input type="checkbox" class="js-small f-right fields" name="accounts_custom_reminder" id="accounts_custom_reminder" checked="checked" readonly> 
                        </div>
                      </div>
                      <?php } ?>
               </div>
               <div class="col-sm-10">
                <span class="small-info">
                  <label class="col-sm-4 col-form-label">Choose the date from which you want to set the reminders</label>
                    <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span><input type="text" name="conf_statement" placeholder="dd-mm-yyyy" value="" class="fields datepicker_service" readonly>
                </span>
                <span class="small-info" style="margin-top: 10px;">
                  <button class="btn-primary btn-sm reminder_in_queue" data-client_id="<?php echo $client['id'];?>" data-user_id="0" data-contact_id="<?php echo (array_search('company_tax_return', $fill) + 1); ?>">Create Reminders</button>
                </span>
              </div>
            </div>
         </div>
      </div>     
   </div>
  </li>


 <li class="show-block" style="<?php echo ( $tab_on['cis']['tab']!==0   ? "display: block" :"display:none;");?>">
  <a href="#" class="toggle">CIS Contractor</a>
  <div id="contratab" class="masonry-container floating_set inner-views">
   <div class="grid-sizer"></div>
    <div class="accordion-panel">
       <div class="box-division03">
          <div class="accordion-heading" role="tab" id="headingOne">
             <h3 class="card-title accordion-title">
                <a class="accordion-msg"> CIS Contractor</a>
             </h3>
          </div>
          <div id="collapse" class="panel-collapse">
             <div class="basic-info-client1 CONTENT_CIS_CONTRACTOR" id="contratab_box">
             
             <?php if(isset($client['crm_cis_contractor']) && ($client['crm_cis_contractor']!='') ){  ?>
              <div class="form-group row radio_bts name_fields contratab_sort" data-id="<?php echo $Setting_Order['crm_cis_contractor']; ?>"
                <?php echo $Setting_Delete['crm_cis_contractor']; ?> >
                   <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_cis_contractor']; ?></label>
                   <div class="col-sm-8">
                      <input type="checkbox" class="js-small f-right fields" name="cis_contractor" id="cis_contractor" <?php if(isset($client['crm_cis_contractor']) && ($client['crm_cis_contractor']=='on') ){?> checked="checked"<?php } ?> readonly>
                   </div>
                </div> 
             <?php } ?>

             <?php if(isset($client['crm_cis_contractor_start_date']) && ($client['crm_cis_contractor_start_date']!='') ){  ?>
                <div class="form-group row  date_birth contratab_sort" data-id="<?php echo $Setting_Order['crm_cis_contractor_start_date']; ?>"
                 >
                   <label class="col-sm-4 col-form-label">
                   <?php echo $Setting_Label['crm_cis_contractor_start_date']; ?>
                   
                  </label>
                   <div class="col-sm-8">
                      <?php if(isset($client['crm_cis_contractor_start_date']) && ($client['crm_cis_contractor_start_date']!='') ){ echo Change_Date_Format( $client['crm_cis_contractor_start_date'] );}?>
                   </div>
                </div>
             <?php } ?>
             
             <?php if(isset($client['crm_cis_scheme_notes']) && ($client['crm_cis_scheme_notes']!='') ){  ?>
                <div class="form-group row contratab_sort" data-id="<?php echo $Setting_Order['crm_cis_scheme_notes']; ?>" <?php echo $Setting_Delete['crm_cis_scheme_notes']; ?>>
                   <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_cis_scheme_notes']; ?></label>
                   <div class="col-sm-8">
                      <?php if(isset($client['crm_cis_scheme_notes']) && ($client['crm_cis_scheme_notes']!='') ){ echo $client['crm_cis_scheme_notes'];}?>
                   </div>
                </div>
             <?php } ?>

              <?php if(isset($client['crm_cis_frequency']) && $client['crm_cis_frequency']=='') {  ?>

               <div class="form-group row bookkeep_sort" data-id="<?php echo $Setting_Order['crm_cis_frequency']; ?>" <?php echo $Setting_Delete['crm_cis_frequency']; ?>>
                     <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_cis_frequency']; ?></label>
                     <div class="col-sm-8">
                        <?php echo $client['crm_cis_frequency']; ?>
                     </div>
                  </div>
              <?php } ?>    
             </div>
             <div class="col-sm-10">  
                <span class="small-info">
                  <label class="col-sm-4 col-form-label">Choose the date from which you want to set the reminders</label>
                    <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span><input type="text" name="cis" placeholder="dd-mm-yyyy" value="" class="fields datepicker_service" readonly>
                </span>
                  <span class="small-info" style="margin-top: 10px;">
                <button class="btn-primary btn-sm reminder_in_queue" data-client_id="<?php echo $client['id'];?>" data-user_id="0" data-contact_id="<?php echo (array_search('cis', $fill) + 1); ?>">Create Reminders</button>
              </span>
            </div>
          </div>
       </div>
    </div>
   
  <div class="accordion-panel enable_cis"  style="<?php echo ( $tab_on['cis']['reminder']!==0 ? "display: block" :"display:none;");?>">
     <div class="box-division03">
        <div class="accordion-heading" role="tab" id="headingOne">
           <h3 class="card-title accordion-title">
              <a class="accordion-msg">CIS Reminders</a>
           </h3>
        </div>
        <div id="collapse" class="panel-collapse">
           <div class="basic-info-client1 CONTENT_CIS_REMINDERS" id="contratab_box2">
            <?php if(isset($client['crm_cis_next_reminder_date']) && ($client['crm_cis_next_reminder_date']!='') ){ ?>
              <div class="form-group row  date_birth radio_bts contratab_sort" data-id="<?php echo $Setting_Order['crm_cis_next_reminder_date']; ?>"
                <?php echo $Setting_Delete['crm_cis_next_reminder_date']; ?>>
                 <label class="col-sm-4 col-form-label"><a id="cis_add_custom_reminder_link" name="cis_add_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/8" target="_blank"><?php echo $Setting_Label['crm_cis_next_reminder_date']; ?></a></label>
                 <div class="col-sm-8">
                    
                    <input type="checkbox" class="js-small f-right fields as_per_firm" name="cis_next_reminder_date" id="Cis_next_reminder_date"  checked="checked" readonly>
                    
                 </div>
              </div>

            <?php } ?>
            
              <?php if(isset($client['crm_cis_add_custom_reminder']) && ($client['crm_cis_add_custom_reminder']!='') ){ ?>
              <div class="form-group row radio_bts cis_add_custom_reminder_label contratab_sort" data-id="<?php echo $Setting_Order['crm_cis_add_custom_reminder'];?>"
                <?php echo $Setting_Delete['crm_cis_add_custom_reminder'];?>>
                 <label id="cis_add_custom_reminder_label" name="cis_add_custom_reminder_label" class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_cis_add_custom_reminder'];?></label>
                 <div class="col-sm-8" id="cis_add_custom_reminder">
                    <input type="checkbox" class="js-small f-right fields cus_reminder" name="cis_add_custom_reminder" id="" data-id="cis_cus" data-serid="8"   checked="checked" readonly>
                 </div>
              </div>
              <?php } ?>
           </div>
        </div>
     </div>
  </div>
  <!-- accordion-panel -->
  </div>
  
                  
   
  </li>

   <!-- CIS SUB OPEN -->
<li class="show-block" style="<?php echo ($tab_on['cissub']['tab']!==0  ? "display: block" :"display:none;");?>">
  <a href="#" class="toggle">CIS Sub Contractor</a>
  <div id="sub_contratab" class="masonry-container floating_set inner-views">
   <div class="grid-sizer"></div>
    <div class="accordion-panel">
       <div class="box-division03">
          <div class="accordion-heading" role="tab" id="headingOne">
             <h3 class="card-title accordion-title">
                <a class="accordion-msg"> CIS Sub Contractor</a>
             </h3>
          </div>
          <div id="collapse" class="panel-collapse">
             <div class="basic-info-client1 CIS_Sub_contractor" id="contratab_box3">

              <?php if(isset($client['crm_cis_subcontractor']) && ($client['crm_cis_subcontractor']!='') ){  ?>
                <div class="form-group row name_fields radio_bts sub_contratab_sort"  data-id="<?php echo $Setting_Order['crm_cis_subcontractor']; ?>" <?php echo $Setting_Delete['crm_cis_subcontractor']; ?>>
                   <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_cis_subcontractor']; ?></label>
                   <div class="col-sm-8">
                      <input type="checkbox" name="cis_subcontractor" id="cis_subcontractor" class="js-small f-right fields" <?php if(isset($client['crm_cis_subcontractor']) && ($client['crm_cis_subcontractor']=='on') ){?> checked="checked"<?php } ?> readonly >
                   </div>
                </div>
              <?php } ?>

              <?php if(isset($client['crm_cis_subcontractor_start_date']) && ($client['crm_cis_subcontractor_start_date']!='') ){  ?>
                <div class="form-group row  date_birth sub_contratab_sort" data-id="<?php echo $Setting_Order['crm_cis_subcontractor_start_date']; ?>">
                   <label class="col-sm-4 col-form-label" >
                   <?php echo $Setting_Label['crm_cis_subcontractor_start_date']; ?>
                   
                  </label>
                   <div class="col-sm-8">
                      <?php if(isset($client['crm_cis_subcontractor_start_date']) && ($client['crm_cis_subcontractor_start_date']!='') ){ echo Change_Date_Format( $client['crm_cis_subcontractor_start_date'] );}?>
                   </div>
                </div>
               <?php } ?>

               <?php if(isset($client['crm_cis_subcontractor_frequency']) && ($client['crm_cis_subcontractor_frequency']!='') ){  ?>
                <div class="form-group row  date_birth sub_contratab_sort" data-id="<?php echo $Setting_Order['crm_cis_subcontractor_frequency']; ?>">
                   <label class="col-sm-4 col-form-label" >
                   <?php echo $Setting_Label['crm_cis_subcontractor_frequency']; ?>
                   
                  </label>
                   <div class="col-sm-8">
                      <?php if(isset($client['crm_cis_subcontractor_frequency']) && ($client['crm_cis_subcontractor_frequency']!='') ){ echo $client['crm_cis_subcontractor_frequency'];}?>
                   </div>
                </div>
               <?php } ?>


               <?php if(isset($client['crm_cis_subcontractor_scheme_notes']) && ($client['crm_cis_subcontractor_scheme_notes']!='') ){  ?>
                <div class="form-group row sub_contratab_sort" data-id="<?php echo $Setting_Order['crm_cis_subcontractor_scheme_notes']; ?>" <?php echo $Setting_Delete['crm_cis_subcontractor_scheme_notes']; ?>>
                   <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_cis_subcontractor_scheme_notes']; ?></label>
                   <div class="col-sm-8">
                     <?php if(isset($client['crm_cis_subcontractor_scheme_notes']) && ($client['crm_cis_subcontractor_scheme_notes']!='') ){ echo $client['crm_cis_subcontractor_scheme_notes'];}?>
                   </div>
                </div>
                 <?php } ?> 
             </div>
             <div class="col-sm-10">  
              <span class="small-info">
                  <label class="col-sm-4 col-form-label">Choose the date from which you want to set the reminders</label>
                    <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span><input type="text" name="cissub" placeholder="dd-mm-yyyy" value="" class="fields datepicker_service" readonly>
                </span>
                  <span class="small-info" style="margin-top: 10px;">
                <button class="btn-primary btn-sm reminder_in_queue" data-client_id="<?php echo $client['id'];?>" data-user_id="0" data-contact_id="<?php echo (array_search('cissub', $fill) + 1); ?>">Create Reminders</button>
              </span>
            </div>
          </div>
       </div>
    </div>
   <div class="accordion-panel  enable_cissub" style="<?php echo ($tab_on['cissub']['reminder']!==0 ? "display: block" :"display:none;");?>">
      <div class="box-division03">
         <div class="accordion-heading" role="tab" id="headingOne">
            <h3 class="card-title accordion-title">
               <a class="accordion-msg">CIS SUB Reminders</a>
            </h3>
         </div>
         <div id="collapse" class="panel-collapse">
            <div class="basic-info-client1 CONTENT_CIS_SUB_REMINDERS" id="contratab_box1">
             
              <?php if(isset($client['crm_cissub_next_reminder_date']) && ($client['crm_cissub_next_reminder_date']!='')){ ?>
               <div class="form-group row  date_birth radio_bts sub_contratab_sort" data-id="<?php echo $Setting_Order['crm_cissub_next_reminder_date']; ?>"
                <?php echo $Setting_Delete['crm_cissub_next_reminder_date']; ?>>
                  <label class="col-sm-4 col-form-label"><a id="cissub_add_custom_reminder_link" name="cissub_add_custom_reminder_link" target="_blank" href="<?php echo base_url(); ?>user/Service_reminder_settings/9"><?php echo $Setting_Label['crm_cissub_next_reminder_date']; ?></a></label>
                  <div class="col-sm-8">
                     
                     <input type="checkbox" class="js-small f-right fields as_per_firm" name="cissub_next_reminder_date" id="cissub_next_reminder_date"  checked="checked" readonly>
                  </div>
               </div>
             <?php } ?>

              <!--  <div class="form-group row radio_bts ">
                  <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                  <div class="col-sm-8">
                     <input type="checkbox" class="js-small f-right fields" name="cissub_create_task_reminder" id="cissub_create_task_reminder" <?php if(isset($client['crm_cissub_create_task_reminder']) && ($client['crm_cissub_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> value="on" >
                  </div>
               </div> -->
              
                <?php if(isset($client['crm_cissub_add_custom_reminder']) && ($client['crm_cissub_add_custom_reminder']!='')){ ?>
               <div class="form-group row radio_bts cissub_add_custom_reminder_label sub_contratab_sort" data-id="<?php echo $Setting_Order['crm_cissub_add_custom_reminder']; ?>"
                <?php echo $Setting_Delete['crm_cissub_add_custom_reminder']; ?>>
                  <label id="cissub_add_custom_reminder_label" name="cissub_add_custom_reminder_label" class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_cissub_add_custom_reminder']; ?></label>
                  <div class="col-sm-8" id="cissub_add_custom_reminder">
                     <input type="checkbox" class="js-small f-right fields cus_reminder" name="cissub_add_custom_reminder" id="" data-id="cissub_cus" data-serid="9"  checked="checked"  readonly>
                    
                  </div>
               </div>
              <?php } ?>
            </div>
         </div>
      </div>
   </div>
 
  </div>
  
                  
   
  </li>


  <?php

$investigateInsService ="display:none";

if($tab_on['investgate']['tab']!==0 || $tab_on['registered']['tab']!==0 || $tab_on['taxadvice']['tab']!==0 )
{
  $investigateInsService ="display:block";
}

?>

<li class="show-block" style="<?php echo ( $tab_on['investgate']['tab']!==0 ? "display: block" :"display:none;"); ?>">
   <a href="#" class="toggle"> Investigation Insurance</a>
   <div  id="investigation-insurance" class="masonry-container floating_set inner-views">
   <div class="grid-sizer"></div>
   <div class="accordion-panel">
      <div class="box-division03">
         <div class="accordion-heading" role="tab" id="headingOne">
            <h3 class="card-title accordion-title">
               <a class="accordion-msg">Investigation Insurance       </a>
            </h3>
         </div>
         <div id="collapse" class="panel-collapse">
            <div class="basic-info-client1 CONTENT_INVESTIGATION_INSURANCE" id="invest-box">
                <?php if(isset($client['crm_invesitgation_insurance']) && ($client['crm_invesitgation_insurance']!='') ){ ?>
                    <div class="form-group row help_icon date_birth invest_sort" data-id="<?php echo $Setting_Order['crm_invesitgation_insurance']; ?>" <?php echo $Setting_Delete['crm_invesitgation_insurance']; ?>>
                      <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_invesitgation_insurance']; ?> </label>
                      <div class="col-sm-8">
                        
                       <span class="small-info"><?php if(isset($client['crm_invesitgation_insurance']) && ($client['crm_invesitgation_insurance']!='') ){ echo  Change_Date_Format($client['crm_invesitgation_insurance']);}?></span>
                      </div>
                    </div>
                    <?php } ?>
                    <?php if(isset($client['crm_insurance_renew_date']) && ($client['crm_insurance_renew_date']!='') ){ ?>
                    <div class="form-group row help_icon date_birth invest_sort" data-id="<?php echo $Setting_Order['crm_insurance_renew_date']; ?>">
                      <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_insurance_renew_date']; ?> </label>
                      <div class="col-sm-8">
                       
                        <span class="small-info"><?php if(isset($client['crm_insurance_renew_date']) && ($client['crm_insurance_renew_date']!='') ){ echo  Change_Date_Format($client['crm_insurance_renew_date']);}?></span>
                      </div>
                    </div>
                    <?php } ?>
                    <?php if(isset($client['crm_insurance_provider'])  && $client['crm_insurance_provider']!=''){?>
                    <div class="form-group row invest_sort" data-id="<?php echo $Setting_Order['crm_insurance_provider']; ?>" <?php echo $Setting_Delete['crm_insurance_provider']; ?>>
                      <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_insurance_provider']; ?> </label>
                      <div class="col-sm-8">
                       
                        <span class="small-info"><?php if(isset($client['crm_insurance_provider']) ) { echo $client['crm_insurance_provider'];}?></span>
                      </div>
                    </div>
                    <?php } ?>
                    <?php if(isset($client['crm_claims_note']) && ($client['crm_claims_note']!='') ){ ?>
                    <div class="form-group row invest_sort" data-id="<?php echo $Setting_Order['crm_claims_note']; ?>" <?php echo $Setting_Delete['crm_claims_note']; ?>>
                      <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_claims_note']; ?> </label>
                      <div class="col-sm-8">
                       
                        <p class="for-address"><?php if(isset($client['crm_claims_note']) && ($client['crm_claims_note']!='') ){ echo $client['crm_claims_note'];}?></p>
                      </div>
                    </div>
                    <?php } ?>
               </div>
               <div class="col-sm-10">  
                <span class="small-info">
                  <label class="col-sm-4 col-form-label">Choose the date from which you want to set the reminders</label>
                    <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span><input type="text" name="investgate" placeholder="dd-mm-yyyy" value="" class="fields datepicker_service" readonly>
                </span>
                  <span class="small-info" style="margin-top: 10px;">
                <button class="btn-primary btn-sm reminder_in_queue" data-client_id="<?php echo $client['id'];?>" data-user_id="0" data-contact_id="<?php echo (array_search('investgate', $fill) + 1); ?>">Create Reminders</button>
              </span>
            </div>
         </div>
      </div>
   </div>

   <!-- accordion-panel -->
   <div class="accordion-panel enable_invest" style="<?php echo ($tab_on['investgate']['reminder']!==0  ? "display: block" :"display:none;");?>">
      <div class="box-division03">
         <div class="accordion-heading" role="tab" id="headingOne">
            <h3 class="card-title accordion-title">
               <a class="accordion-msg">Investigation Insurance Reminders</a>
            </h3>
         </div>
         <div id="collapse" class="panel-collapse">
            <div class="basic-info-client1 CONTENT_INVESTIGATION_INSURANCE_REMINDER"  id="invest_box1">

              <?php if(isset($client['crm_insurance_next_reminder_date']) && ($client['crm_insurance_next_reminder_date']!='') ){ ?>

                  <div class="form-group row  date_birth radio_bts invest_sort" data-id="<?php echo $Setting_Order['crm_insurance_next_reminder_date']; ?>" <?php echo $Setting_Delete['crm_insurance_next_reminder_date']; ?>>
                  <label class="col-sm-4 col-form-label"><a id="insurance_add_custom_reminder_link" name="insurance_add_custom_reminder_link" href="<?php echo base_url(); ?>user/Service_reminder_settings/13" target="_blank"><?php echo $Setting_Label['crm_insurance_next_reminder_date']; ?></a></label>
                  <div class="col-sm-8">
                     
                     <input type="checkbox" class="js-small f-right fields as_per_firm" name="insurance_next_reminder_date" id="insurance_next_reminder_date"    checked="checked" readonly>
                  </div>
                  </div>
               <?php } ?>

            

               <?php if(isset($client['crm_insurance_add_custom_reminder']) && ($client['crm_insurance_add_custom_reminder']!='') ){ ?>

               <div class="form-group row radio_bts insurance_add_custom_reminder_label invest_sort" data-id="<?php echo $Setting_Order['crm_insurance_add_custom_reminder']; ?>" <?php echo $Setting_Delete['crm_insurance_add_custom_reminder']; ?>>
                  <label class="col-sm-4 col-form-label" name="insurance_add_custom_reminder_label" id="insurance_add_custom_reminder_label"><?php echo $Setting_Label['crm_insurance_add_custom_reminder']; ?></label>
                  <div class="col-sm-8" id="insurance_add_custom_reminder">
                     <input type="checkbox" class="js-small f-right fields cus_reminder" name="insurance_add_custom_reminder" id="" data-id="insurance_cus" data-serid="13"  checked readonly>
                    
                  </div>
               </div>
                <?php } ?>
                   </div>
         </div>
      </div>
   </div>
   </div>
</li> 

<?php

$managementService ="display:none";

if($tab_on['management']['tab']!==0 )
{
$managementService ="display:block";
}

?>
<!-- management-a/c tab start -->
<li class="show-block" style="<?php echo $managementService?>">
<a href="#" class="toggle">  Management Accounts  </a>
<div id="management-account" class="masonry-container floating_set inner-views">
      <div class="accordion-panel">
        <div class="box-division03">
          <div class="accordion-heading" role="tab" id="headingOne">
            <h3 class="card-title accordion-title">
              <a class="accordion-msg">Management Accounts</a>
            </h3>
          </div>
          <div id="collapse" class="panel-collapse">
            <div class="basic-info-client1 CONTENT_MANAGEMENT_ACCOUNTS" id="management_account">


            <?php if(isset($client['crm_manage_acc_fre']) && $client['crm_manage_acc_fre']!='') { ?>
              <div class="form-group row management_sort" data-id="<?php echo $Setting_Order['crm_manage_acc_fre']; ?>" 
          <?php echo $Setting_Delete['crm_manage_acc_fre']; ?>>
                <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_manage_acc_fre']; ?> </label>
                <div class="col-sm-8">
                
                  <span class="small-info"><?php if(isset($client['crm_manage_acc_fre'])) { echo $client['crm_manage_acc_fre'];}?></span>
                </div>
              </div>
              <?php } ?>
              <?php if(isset($client['crm_next_manage_acc_date']) && ($client['crm_next_manage_acc_date']!='') ){ ?>
              <div class="form-group row help_icon date_birth management_sort" data-id="<?php echo $Setting_Order['crm_next_manage_acc_date']; ?>">
                <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_next_manage_acc_date']; ?> </label>
                <div class="col-sm-8">
                 
                  <span class="small-info"><?php if(isset($client['crm_next_manage_acc_date']) && ($client['crm_next_manage_acc_date']!='') ){ echo  Change_Date_Format($client['crm_next_manage_acc_date']);}?></span>
                </div>
              </div>
              <?php } ?>
              <?php if(isset($client['crm_manage_method_bookkeeping']) && $client['crm_manage_method_bookkeeping']!='') { ?>
              <div class="form-group row management_sort" data-id="<?php echo $Setting_Order['crm_manage_method_bookkeeping']; ?>" <?php echo $Setting_Delete['crm_manage_method_bookkeeping']; ?>>
                <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_manage_method_bookkeeping']; ?> </label>
                <div class="col-sm-8">
                  
                  <span class="small-info"><?php if(isset($client['crm_manage_method_bookkeeping'])) { echo $client['crm_manage_method_bookkeeping'];}?></span>
                </div>
              </div>
              <?php } ?>

              <?php if(isset($client['crm_manage_client_provide_record']) && $client['crm_manage_client_provide_record']!='') { ?>
              <div class="form-group row management_sort" data-id="<?php echo $Setting_Order['crm_manage_client_provide_record']; ?>" <?php echo $Setting_Delete['crm_manage_client_provide_record']; ?>>
                <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_manage_client_provide_record']; ?> </label>
                <div class="col-sm-8">
                 
                  <span class="small-info"><?php if(isset($client['crm_manage_client_provide_record']) ) { echo $client['crm_manage_client_provide_record'];}?></span>
                </div>
              </div>
              <?php } ?>
            </div>
            <div class="col-sm-10"> 
                <span class="small-info">
                  <label class="col-sm-4 col-form-label">Choose the date from which you want to set the reminders</label>
                    <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span><input type="text" name="management" placeholder="dd-mm-yyyy" value="" class="fields datepicker_service" readonly>
                </span> 
                  <span class="small-info" style="margin-top: 10px;">
                <button class="btn-primary btn-sm reminder_in_queue" data-client_id="<?php echo $client['id'];?>" data-user_id="0" data-contact_id="<?php echo (array_search('management', $fill) + 1); ?>">Create Reminders</button>
              </span>
            </div>
          </div>
        </div>
      </div>
      <!-- accordion-panel -->
      <div class="accordion-panel enable_management" >
        <div class="box-division03">
          <div class="accordion-heading" role="tab" id="headingOne">
            <h3 class="card-title accordion-title">
              <a class="accordion-msg">Management Reminders</a>
            </h3>
          </div>
          <div id="collapse" class="panel-collapse">
            <div class="basic-info-client1 CONTENT_MANAGEMENT_ACCOUNTS_REMINDERS" id="management_account1">

              <?php if(isset($client['crm_manage_next_reminder_date']) && ($client['crm_manage_next_reminder_date']!='') ){ ?>
              <div class="form-group row date_birth management_sort" data-id="<?php echo $Setting_Order['crm_manage_next_reminder_date']; ?>" <?php echo $Setting_Delete['crm_manage_next_reminder_date']; ?>>
           
                <div class="col-sm-8">

                  <label class="col-sm-4 col-form-label"><a id="manage_add_custom_reminder_link" name="manage_add_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/12" target="_blank"><?php echo $Setting_Label['crm_manage_next_reminder_date']; ?></a></label>

                  <input type="checkbox" class="js-small f-right fields as_per_firm" name="manage_next_reminder_date" id="manage_next_reminder_date" checked="checked" readonly>
                  
                </div>
              </div>
              <?php  } ?>
              <?php if(isset($client['crm_manage_add_custom_reminder']) && ($client['crm_manage_add_custom_reminder']!='') ){?> 

              <div class="form-group row radio_bts management_sort" data-id="<?php echo $Setting_Order['crm_manage_add_custom_reminder']; ?>" <?php echo $Setting_Delete['crm_manage_add_custom_reminder']; ?>>
                <label id="manage_add_custom_reminder_label" name="manage_add_custom_reminder_label" class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_manage_add_custom_reminder']; ?> </label>
                <div class="col-sm-8">
                  <input type="checkbox" class="js-small f-right fields" name="manage_add_custom_reminder" id="manage_add_custom_reminder" checked="checked" readonly>                    
                </div>
              </div>

              <?php } ?>
            </div>
          </div>
        </div>
      </div>
  </div>
     <!-- Management Accounts --> 
</li>

<?php
$payrollService ="display:none";

if($tab_on['payroll']['tab']!==0 || $tab_on['workplace']['tab']!==0 ||  $tab_on['cis']['tab']!==0 || $tab_on['cissub']['tab']!==0 || $tab_on['p11d']['tab']!==0 )
{
  $payrollService ="display:block";
}

?>

 <!-- payroll tab start -->
<!-- payroll tab start -->
<!-- payroll tab start -->
<li class="show-block" style="<?php echo ($tab_on['payroll']['tab']!==0?"display: block":"display: none");?>">
<a href="#" class="toggle">  Payroll</a>
<!-- Pay Roll -->              
<div id="payroll" class="masonry-container floating_set inner-views">
   <div class="grid-sizer"></div>
   <div class="accordion-panel">
      <div class="box-division03">
         <div class="accordion-heading" role="tab" id="headingOne">
            <h3 class="card-title accordion-title">
               <a class="accordion-msg">Important Information</a>
            </h3>
         </div>
         <div id="collapse" class="panel-collapse">
            <div class="basic-info-client1 CONTENT_IMPORTANT_INFORMATION" id="payroll_box">
               <?php if(isset($client['crm_payroll_acco_off_ref_no']) && ($client['crm_payroll_acco_off_ref_no']!='') ){ ?>
                  <div class="form-group row name_fields  payroll_sec" data-id="<?php echo $Setting_Order['crm_payroll_acco_off_ref_no']; ?>" <?php echo $Setting_Delete['crm_payroll_acco_off_ref_no']; ?>>
                    <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_payroll_acco_off_ref_no']; ?> </label>
                    <div class="col-sm-8">                       
                      <span class="small-info clr-check-client"><?php if(isset($client['crm_payroll_acco_off_ref_no']) && ($client['crm_payroll_acco_off_ref_no']!='') ){ echo $client['crm_payroll_acco_off_ref_no'];}?></span>
                    </div>
                  </div>
                  <?php } ?>
               <?php if(isset($client['crm_paye_off_ref_no']) && ($client['crm_paye_off_ref_no']!='') ){ ?>
                  <div class="form-group row name_fields payroll_sec" data-id="<?php echo $Setting_Order['crm_paye_off_ref_no']; ?>" <?php echo $Setting_Delete['crm_paye_off_ref_no']; ?>>
                    <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_paye_off_ref_no']; ?> </label>
                    <div class="col-sm-8">                    
                     <span class="small-info clr-check-client"><?php if(isset($client['crm_paye_off_ref_no']) && ($client['crm_paye_off_ref_no']!='') ){ echo $client['crm_paye_off_ref_no'];}?></span>
                    </div>
                  </div>
                  <?php } ?>
            </div>
         </div>
      </div>
   </div>
   <!-- accordion-panel -->
   <div class="accordion-panel for_payroll_section">
      <div class="box-division03">
         <div class="accordion-heading" role="tab" id="headingOne">
            <h3 class="card-title accordion-title">
               <a class="accordion-msg">Payroll</a>
            </h3>
         </div>
         <div id="collapse" class="panel-collapse">
            <div class="basic-info-client1 CONTENT_PAYROLL" id="payroll_box1">

                  <?php if(isset($client['crm_payroll_reg_date']) && ($client['crm_payroll_reg_date']!='') ){ ?>
                    <div class="form-group row date_birth payroll_sec" data-id="<?php echo $Setting_Order['crm_payroll_reg_date']; ?>" <?php echo $Setting_Delete['crm_payroll_reg_date']; ?>>
                       <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_payroll_reg_date']; ?> </label>
                       <div class="col-sm-8">
                         <?php if(isset($client['crm_payroll_reg_date']) && ($client['crm_payroll_reg_date']!='') ){ echo  Change_Date_Format($client['crm_payroll_reg_date']);}?>
                       </div>
                    </div>
                    <?php } ?>
                    
                    <?php if(isset($client['crm_no_of_employees']) && ($client['crm_no_of_employees']!='') ){ ?>
                    <div class="form-group row payroll_sec" data-id="<?php echo $Setting_Order['crm_no_of_employees']; ?>"
                    <?php echo $Setting_Delete['crm_no_of_employees']; ?>>
                       <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_no_of_employees']; ?> </label>
                       <div class="col-sm-8">
                          <?php if(isset($client['crm_no_of_employees']) && ($client['crm_no_of_employees']!='') ){ echo $client['crm_no_of_employees'];}?>                                 </div>
                    </div>
                    <?php } ?>

                <?php if(isset($client['crm_first_pay_date']) && ($client['crm_first_pay_date']!='') ){ ?>
                    <div class="form-group row  date_birth payroll_sec" data-id="<?php echo $Setting_Order['crm_first_pay_date']; ?>" <?php echo $Setting_Delete['crm_first_pay_date']; ?>>
                       <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_first_pay_date']; ?> </label>
                       <div class="col-sm-8">
                          <?php if(isset($client['crm_first_pay_date']) && ($client['crm_first_pay_date']!='') ){ echo  Change_Date_Format($client['crm_first_pay_date']);}?>
                       </div>
                    </div>
                    <?php } ?>

                  <?php if(isset($client['crm_payroll_run']) && $client['crm_payroll_run']!=''){ ?>
                    <div class="form-group row payroll_sec" data-id="<?php echo $Setting_Order['crm_payroll_run']; ?>"
                <?php echo $Setting_Delete['crm_payroll_run']; ?>>
                       <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_payroll_run']; ?> </label>
                       <div class="col-sm-8">
                          <?php if(isset($client['crm_payroll_run'])){ echo $client['crm_payroll_run']; }?>
                       </div>
                    </div>
                    <?php } ?>

                    <?php if(isset($client['crm_payroll_run_date']) && ($client['crm_payroll_run_date']!='') ){  ?>
                    <div class="form-group row  date_birth payroll_sec" data-id="<?php echo $Setting_Order['crm_payroll_run_date']; ?>">
                       <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_payroll_run_date']; ?> </label>
                       <div class="col-sm-8">
                          <?php if(isset($client['crm_payroll_run_date']) && ($client['crm_payroll_run_date']!='') ){ echo  Change_Date_Format($client['crm_payroll_run_date']);}?>
                       </div>
                    </div>
                    <?php } ?>

                <?php if(isset($client['crm_rti_deadline']) && ($client['crm_rti_deadline']!='') ){ ?>
                    <div class="form-group row  date_birth payroll_sec" data-id="<?php echo $Setting_Order['crm_rti_deadline']; ?>" <?php echo $Setting_Delete['crm_rti_deadline']; ?>>
                       <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_rti_deadline']; ?> </label>
                       <div class="col-sm-8">
                         <?php if(isset($client['crm_rti_deadline']) && ($client['crm_rti_deadline']!='') ){ echo  Change_Date_Format($client['crm_rti_deadline']);}?>
                       </div>
                    </div>
                    <?php } ?>

                  <?php if(isset($client['crm_previous_year_require']) && ($client['crm_previous_year_require']=='on') ){?>
                    <div class="form-group row radio_bts payroll_sec" data-id="<?php echo $Setting_Order['crm_previous_year_require']; ?>" <?php echo $Setting_Delete['crm_previous_year_require']; ?>>
                       <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_previous_year_require']; ?> </label>
                       <div class="col-sm-8">
                          <input type="checkbox" class="js-small f-right fields" name="previous_year_require" id="previous_year_require" <?php if(isset($client['crm_previous_year_require']) && ($client['crm_previous_year_require']=='on') ){?> checked="checked"<?php } ?> data-id="preyear" readonly>
                       </div>
                    </div>
                    <?php } ?>
               <?php 
                  (isset($client['crm_previous_year_require']) && $client['crm_previous_year_require'] == 'on') ? $preyear =  "block" : $preyear = 'none';
                  
                  
                  ?>
                   <div id="enable_preyear" style="display:<?php echo $preyear;?>;">
                   <?php if(isset($client['crm_payroll_if_yes']) && $client['crm_payroll_if_yes']!=''){ ?>
                       <div class="form-group row name_fields" data-id="<?php echo $Setting_Order['crm_payroll_if_yes']; ?>" <?php echo $Setting_Delete['crm_payroll_if_yes']; ?>>
                          <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_payroll_if_yes']; ?> </label>
                          <div class="col-sm-8">
                             <!-- <input type="text" name="payroll_if_yes" id="payroll_if_yes" placeholder="How many years" value="<?php if(isset($client['crm_payroll_if_yes']) && ($client['crm_payroll_if_yes']!='') ){ echo $client['crm_payroll_if_yes'];}?>" class="fields"> -->
                             <?php  if(isset($client['crm_payroll_if_yes'])){ echo $client['crm_payroll_if_yes'];}?>
                          </div>
                       </div>
                       <?php } ?>
                    </div>

                <?php if(isset($client['crm_paye_scheme_ceased']) && ($client['crm_paye_scheme_ceased']!='') ){ ?>
                    <div class="form-group row  date_birth payroll_sec" data-id="<?php echo $Setting_Order['crm_paye_scheme_ceased']; ?>" <?php echo $Setting_Delete['crm_paye_scheme_ceased']; ?>>
                       <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_paye_scheme_ceased']; ?> </label>
                       <div class="col-sm-8">
                         <?php echo Change_Date_Format( $client['crm_paye_scheme_ceased']); ?>
                       </div>
                    </div>
                    <?php } ?>
            </div>
            <div class="col-sm-10">
              <span class="small-info">
                  <label class="col-sm-4 col-form-label">Choose the date from which you want to set the reminders</label>
                    <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span><input type="text" name="payroll" placeholder="dd-mm-yyyy" value="" class="fields datepicker_service" readonly>
                </span>
              <span class="small-info" style="margin-top: 10px;">
                <button class="btn-primary btn-sm reminder_in_queue" data-client_id="<?php echo $client['id'];?>" data-user_id="0" data-contact_id="<?php echo (array_search('payroll', $fill) + 1);  ?>">Create Reminders</button>
              </span>
          </div>
         </div>
      </div>
   </div>
   <!-- accordion-panel -->
   <div class="accordion-panel enable_pay" style="<?php echo ($tab_on['payroll']['reminder']!==0 ? "display: block" :"display:none;");?>">
      <div class="box-division03">
         <div class="accordion-heading" role="tab" id="headingOne">
            <h3 class="card-title accordion-title">
               <a class="accordion-msg">Payroll Reminders</a>
            </h3>
         </div>
         <div id="collapse" class="panel-collapse">
            <div class="basic-info-client1 CONTENT_PAYROLL_REMINDERS"  id="payroll_box2">
              <?php if(isset($client['crm_payroll_next_reminder_date']) && ($client['crm_payroll_next_reminder_date']!='') ){ ?>

               <div class="form-group row  radio_bts date_birth payroll_sec" data-id="<?php echo $Setting_Order['crm_payroll_next_reminder_date']; ?>" <?php echo $Setting_Delete['crm_payroll_next_reminder_date']; ?>>
                  <label class="col-sm-4 col-form-label"><a id="payroll_add_custom_reminder_link" name="payroll_add_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/6" target="_blank"><?php echo $Setting_Label['crm_payroll_next_reminder_date']; ?></a></label>
                  <div class="col-sm-8">
           
                     <input type="checkbox" class="js-small f-right fields as_per_firm" name="payroll_next_reminder_date" id="Payroll_next_reminder_date" <?php if(isset($client['crm_payroll_next_reminder_date']) && ($client['crm_payroll_next_reminder_date']!='') ){ echo "checked"; } ?> readonly>
                  </div>
               </div>
              <?php } ?>

               
               <?php if(isset($client['crm_payroll_add_custom_reminder']) && ($client['crm_payroll_add_custom_reminder']!='') ){ ?>
               <div class="form-group row radio_bts payroll_add_custom_reminder_label payroll_sec" data-id="<?php echo $Setting_Order['crm_payroll_add_custom_reminder']; ?>" <?php echo $Setting_Delete['crm_payroll_add_custom_reminder']; ?> >
                  <label id="payroll_add_custom_reminder_label" name="payroll_add_custom_reminder_label" class="col-sm-4 col-form-label">
                    <?php echo $Setting_Label['crm_payroll_add_custom_reminder']; ?>                        
                    </label>
                  <div class="col-sm-8" id="payroll_add_custom_reminder">
                     <input type="checkbox" class="js-small f-right fields cus_reminder" name="payroll_add_custom_reminder" id="" data-id="payroll_cus" data-serid="6"  checked  readonly>
                  </div>
               </div>
              <?php } ?>
            </div>
         </div>
      </div>
   </div>
   <!-- accordion-panel -->
             
</div>
</li>
<!-- pay roll end -->


<!-- p11d open -->
<li class="show-block" style="<?php echo ($tab_on['p11d']['tab']!==0 ? 'display: block' : 'display:none'); ?>" >
<a href="#" class="toggle"> P11D</a>
   <div id="p11dtab" class="masonry-container floating_set inner-views">
        <div class="grid-sizer"></div>
      <div class="accordion-panel">
         <div class="box-division03">
            <div class="accordion-heading" role="tab" id="headingOne">
               <h3 class="card-title accordion-title">
                  <a class="accordion-msg">P11D</a>
               </h3>
            </div>
            <div id="collapse" class="panel-collapse">
               <div class="basic-info-client1 CONTENT_P11D" id="p11d_sec">
                <?php if(isset($client['crm_p11d_latest_action_date']) && ($client['crm_p11d_latest_action_date']!='') ){ ?>
                  <div class="form-group row  date_birth p11d_sort" data-id="<?php echo $Setting_Order['crm_p11d_latest_action_date']; ?>" 
                    <?php echo $Setting_Delete['crm_p11d_latest_action_date']; ?>>
                     <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_p11d_latest_action_date']; ?></label>
                     <div class="col-sm-8">
                       <?php if(isset($client['crm_p11d_latest_action_date']) && ($client['crm_p11d_latest_action_date']!='') ){ echo Change_Date_Format( $client['crm_p11d_latest_action_date'] );}?>
                     </div>
                  </div>
                <?php } ?>

                 <?php if(isset($client['crm_p11d_latest_action']) && ($client['crm_p11d_latest_action']!='') ){ ?>
                  <div class="form-group row name_fields p11d_sort" data-id="<?php echo $Setting_Order['crm_p11d_latest_action']; ?>"
                    <?php echo $Setting_Delete['crm_p11d_latest_action']; ?>>
                     <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_p11d_latest_action']; ?></label>
                     <div class="col-sm-8">
                        <?php if(isset($client['crm_p11d_latest_action']) && ($client['crm_p11d_latest_action']!='') ){ echo $client['crm_p11d_latest_action'];}?>
                     </div>
                  </div>
                <?php } ?>
                <?php if(isset($client['crm_latest_p11d_submitted']) && ($client['crm_latest_p11d_submitted']!='') ){ ?>
                  <div class="form-group row date_birth p11d_sort" data-id="<?php echo $Setting_Order['crm_latest_p11d_submitted']; ?>"
                    <?php echo $Setting_Delete['crm_latest_p11d_submitted']; ?>>
                     <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_latest_p11d_submitted']; ?></label>
                     <div class="col-sm-8">
                        <?php if(isset($client['crm_latest_p11d_submitted']) && ($client['crm_latest_p11d_submitted']!='') ){ echo Change_Date_Format( $client['crm_latest_p11d_submitted']);}?>
                     </div>
                  </div>
                  <?php } ?>

                  <?php if(isset($client['crm_next_p11d_return_due']) && ($client['crm_next_p11d_return_due']!='') ){ ?>
                  <div class="form-group row date_birth p11d_sort" data-id="<?php echo $Setting_Order['crm_next_p11d_return_due']; ?>">
                     <label class="col-sm-4 col-form-label">
                      <?php echo $Setting_Label['crm_next_p11d_return_due']; ?>                      
                      </label>
                     <div class="col-sm-8">
                      <?php if(isset($client['crm_next_p11d_return_due']) && ($client['crm_next_p11d_return_due']!='') ){ echo Change_Date_Format( $client['crm_next_p11d_return_due']);}?>
                     </div>
                  </div>
                  <?php } ?>
                  
                  <?php if(isset($client['crm_p11d_previous_year_require']) && ($client['crm_p11d_previous_year_require']!='') ){ ?>
                  <div class="form-group row radio_bts p11d_sort" data-id="<?php echo $Setting_Order['crm_p11d_previous_year_require']; ?>"
                    <?php echo $Setting_Delete['crm_p11d_previous_year_require']; ?>>
                     <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_p11d_previous_year_require']; ?></label>
                     <div class="col-sm-8">
                        <input type="checkbox" class="js-small f-right fields" name="p11d_previous_year_require" id="p11d_previous_year_require" <?php if(isset($client['crm_p11d_previous_year_require']) && ($client['crm_p11d_previous_year_require']=='on') ){?> checked="checked"<?php } ?> readonly>
                     </div>
                  </div>
                  <?php } ?>

                  <?php if(isset($client['crm_p11d_payroll_if_yes']) && ($client['crm_p11d_payroll_if_yes']!='') ){ ?>
                  <div class="form-group row name_fields p11d_sort" data-id="<?php echo $Setting_Order['crm_p11d_payroll_if_yes']; ?>"
                    <?php echo $Setting_Delete['crm_p11d_payroll_if_yes']; ?>>
                     <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_p11d_payroll_if_yes']; ?></label>
                     <div class="col-sm-8">
                        <?php if(isset($client['crm_p11d_payroll_if_yes']) && ($client['crm_p11d_payroll_if_yes']!='') ){ echo $client['crm_p11d_payroll_if_yes'];}?>
                     </div>
                  </div>
                  <?php } ?>

                  <?php if(isset($client['crm_p11d_records_received']) && ($client['crm_p11d_records_received']!='') ){ ?>
                  <div class="form-group row  date_birth p11d_sort" data-id="<?php echo $Setting_Order['crm_p11d_records_received']; ?>"
                    <?php echo $Setting_Delete['crm_p11d_records_received']; ?>>
                     <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_p11d_records_received']; ?></label>
                     <div class="col-sm-8">
                        <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                        <?php  echo Change_Date_Format( $client['crm_p11d_records_received']);?>
                     </div>
                  </div>
                  <?php } ?>
               </div>
               <div class="col-sm-10"> 
               <span class="small-info">
                  <label class="col-sm-4 col-form-label">Choose the date from which you want to set the reminders</label>
                    <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span><input type="text" name="p11d" placeholder="dd-mm-yyyy" value="" class="fields datepicker_service" readonly>
                </span> 
                  <span class="small-info" style="margin-top: 10px;">
                <button class="btn-primary btn-sm reminder_in_queue" data-client_id="<?php echo $client['id'];?>" data-user_id="0" data-contact_id="<?php echo (array_search('p11d', $fill) + 1); ?>">Create Reminders</button>
              </span>
            </div>
            </div>
         </div>
      </div>
      <!-- accordion-panel -->
      <div class="accordion-panel enable_plld" style="<?php echo ($tab_on['p11d']['reminder']!==0 ? "display: block" :"display:none;");?>">
         <div class="box-division03">
            <div class="accordion-heading" role="tab" id="headingOne">
               <h3 class="card-title accordion-title">
                  <a class="accordion-msg">P11D Reminders</a>
               </h3>
            </div>
            <div id="collapse" class="panel-collapse">
               <div class="basic-info-client1 CONTENT_P11D_REMINDERS" id="p11d_sec1">

                <?php if(isset($client['crm_p11d_next_reminder_date']) && ($client['crm_p11d_next_reminder_date']!='') ){ ?>

                  <div class="form-group row  date_birth radio_bts p11d_sort" data-id="<?php echo $Setting_Order['crm_p11d_next_reminder_date']; ?>"
                    <?php echo $Setting_Delete['crm_p11d_next_reminder_date']; ?>>
                     <label class="col-sm-4 col-form-label"><a id="p11d_add_custom_reminder_link" name="p11d_add_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/11" target="_blank"><?php echo $Setting_Label['crm_p11d_next_reminder_date']; ?></a></label>
                     <div class="col-sm-8">
                        
                        <input type="checkbox" class="js-small f-right fields as_per_firm" name="p11d_next_reminder_date" id="P11d_next_reminder_date"  checked="checked" readonly>
                        
                     </div>
                  </div>
                <?php } ?>
             
                  
                  <?php if(isset($client['crm_p11d_add_custom_reminder']) && ($client['crm_p11d_add_custom_reminder']!='') ){ ?>
                  <div class="form-group row radio_bts p11d_add_custom_reminder_label p11d_sort" data-id="<?php echo $Setting_Order['crm_p11d_add_custom_reminder']; ?>"
                    <?php echo $Setting_Delete['crm_p11d_add_custom_reminder']; ?>>
                     <label id="p11d_add_custom_reminder_label" name="p11d_add_custom_reminder_label" class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_p11d_add_custom_reminder']; ?></label>
                     <div class="col-sm-8" id="p11d_add_custom_reminder">
                        <input type="checkbox" class="js-small f-right fields cus_reminder" name="p11d_add_custom_reminder" id="" data-id="p11d_cus" data-serid="11" checked="checked" readonly>
                     </div>
                  </div>
                  <?php } ?>
               </div>
            </div>
         </div>
      </div>
      <!-- accordion-panel -->
   </div>
   <!-- p11dtab close -->
  </li>
<!-- p11d close -->



<li class="show-block" style="<?php echo ($tab_on['registered']['tab']!==0  ? "display: block" :"display:none;")?>">
<a href="#" class="toggle">Registered Office</a>
  <div  id="registeredtab" class="masonry-container floating_set inner-views">
     <div class="grid-sizer"></div>

        <div class="accordion-panel">
           <div class="box-division03">
              <div class="accordion-heading" role="tab" id="headingOne">
                 <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Registered Office</a>
                 </h3>
              </div>
              <div id="collapse" class="panel-collapse">
                 <div class="basic-info-client1 CONTENT_REGISTERED_OFFICE" id="registeredtab_box">
                   <?php if(isset($client['crm_registered_start_date']) && ($client['crm_registered_start_date']!='') ){ ?>
                      <div class="form-group row help_icon date_birth registered_sort" data-id="<?php echo $Setting_Order['crm_registered_start_date']; ?>" <?php echo $Setting_Delete['crm_registered_start_date']; ?>>
                        <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_registered_start_date']; ?> </label>
                        <div class="col-sm-8">
                         <span class="small-info"><?php if(isset($client['crm_registered_start_date']) && ($client['crm_registered_start_date']!='') ){ echo  Change_Date_Format($client['crm_registered_start_date']);}?></span>
                        </div>
                      </div>
                      <?php } ?>
                      <?php if(isset($client['crm_registered_renew_date']) && ($client['crm_registered_renew_date']!='') ){ ?>
                      <div class="form-group row help_icon date_birth registered_sort" data-id="<?php echo $Setting_Order['crm_registered_renew_date']; ?>">
                        <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_registered_renew_date']; ?> </label>
                        <div class="col-sm-8">
                        
                         <span class="small-info"><?php if(isset($client['crm_registered_renew_date']) && ($client['crm_registered_renew_date']!='') ){ echo  Change_Date_Format($client['crm_registered_renew_date']);}?></span>
                        </div>
                      </div>
                      <?php } ?>
                      <?php if(isset($client['crm_registered_office_inuse']) && ($client['crm_registered_office_inuse']!='') ){ ?>
                      <div class="form-group row registered_sort" data-id="<?php echo $Setting_Order['crm_registered_office_inuse']; ?>" <?php echo $Setting_Delete['crm_registered_office_inuse']; ?>>
                        <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_registered_office_inuse']; ?> </label>
                        <div class="col-sm-8">
                          
                          <span class="small-info"><?php if(isset($client['crm_registered_office_inuse']) && ($client['crm_registered_office_inuse']!='') ){ echo $client['crm_registered_office_inuse'];}?></span>
                        </div>
                      </div>
                      <?php } ?>
                      <?php if(isset($client['crm_registered_claims_note']) && ($client['crm_registered_claims_note']!='') ){ ?>
                      <div class="form-group row registered_sort" data-id="<?php echo $Setting_Order['crm_registered_claims_note']; ?>" <?php echo $Setting_Delete['crm_registered_claims_note']; ?>>
                        <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_registered_claims_note']; ?> </label>
                        <div class="col-sm-8">
                        
                         <p class="for-address"><?php if(isset($client['crm_registered_claims_note']) && ($client['crm_registered_claims_note']!='') ){ echo $client['crm_registered_claims_note'];}?></p>
                        </div>
                      </div>
                      <?php } ?>
                   </div>
                   <div class="col-sm-10">  
                  <span class="small-info">
                  <label class="col-sm-4 col-form-label">Choose the date from which you want to set the reminders</label>
                    <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span><input type="text" name="registered" placeholder="dd-mm-yyyy" value="" class="fields datepicker_service" readonly>
                </span>
                  <span class="small-info" style="margin-top: 10px;">
                <button class="btn-primary btn-sm reminder_in_queue" data-client_id="<?php echo $client['id'];?>" data-user_id="0" data-contact_id="<?php echo (array_search('registered', $fill) + 1); ?>">Create Reminders</button>
              </span>
            </div>
                </div>
             </div>
          </div>

          <div class="accordion-panel enable_reg" style="<?php echo  ($tab_on['registered']['reminder']!==0 ? "display: block" :"display:none;"); ?>">
           <div class="box-division03">
              <div class="accordion-heading" role="tab" id="headingOne">
                 <h3 class="card-title accordion-title">
                    <a class="accordion-msg">Registered Reminders</a>
                 </h3>
              </div>
              <div id="collapse" class="panel-collapse">
                 <div class="basic-info-client1 CONTENT_REGISTERED_OFFICE_REMINDERS" id="registeredtab_box1">

                  <?php if(isset($client['crm_registered_next_reminder_date']) && ($client['crm_registered_next_reminder_date']!='') ){ 
                    ?>
                    <div class="form-group row  date_birth radio_bts registered_sort" data-id="<?php echo $Setting_Order['crm_registered_next_reminder_date']; ?>" <?php echo $Setting_Delete['crm_registered_next_reminder_date']; ?>>
                       <label class="col-sm-4 col-form-label"><a id="registered_add_custom_reminder_link" name="registered_add_custom_reminder_link" href="<?php echo base_url(); ?>user/Service_reminder_settings/14" target="_blank"><?php echo $Setting_Label['crm_registered_next_reminder_date']; ?></a></label>
                       <div class="col-sm-8">                    
                          <input type="checkbox" class="js-small f-right fields as_per_firm" name="registered_next_reminder_date" id="registered_next_reminder_date" checked="checked" readonly>
                       </div>
                    </div>
                  <?php } ?>
                    <!-- <div class="form-group row radio_bts ">
                       <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
                       <div class="col-sm-8">
                          <input type="checkbox" class="js-small f-right fields" name="registered_create_task_reminder" id="registered_create_task_reminder" <?php if(isset($client['crm_registered_create_task_reminder'])  ){?> checked="checked"<?php } ?> value="on" >
                       </div>
                    </div> -->
                    <?php if(!empty($client['crm_registered_add_custom_reminder']) ){ ?>

                    <div class="form-group row radio_bts registered_add_custom_reminder_label registered_sort" data-id="<?php echo $Setting_Order['crm_registered_add_custom_reminder']; ?>" <?php echo $Setting_Delete['crm_registered_add_custom_reminder']; ?>>
                       <label class="col-sm-4 col-form-label" id="registered_add_custom_reminder_label" name="registered_add_custom_reminder_label"><?php echo $Setting_Label['crm_registered_add_custom_reminder']; ?></label>
                       <div class="col-sm-8" id="registered_add_custom_reminder">
                          <input type="checkbox" class="js-small f-right fields cus_reminder" name="registered_add_custom_reminder" id="" data-id="registered_cus" data-serid="14"   checked="checked" readonly>
                       </div>
                    </div>
                  <?php } ?>

                 </div>
              </div>
           </div>
        </div>
    </div>
</li>        



<li class="show-block" style="<?php echo ($tab_on['taxadvice']['tab']!==0  ? "display: block" :"display:none;");?>">
<a href="#" class="toggle"> Tax Advice/Investigation</a>
<div  id="taxadvicetab" class="masonry-container floating_set inner-views">
   <div class="grid-sizer"></div>
   <div class="accordion-panel">
   <div class="box-division03">
      <div class="accordion-heading" role="tab" id="headingOne">
         <h3 class="card-title accordion-title">
            <a class="accordion-msg">Tax Advice/Investigation</a>
         </h3>
      </div>
      <div id="collapse" class="panel-collapse">
         <div class="basic-info-client1 CONTENT_TAX_ADVICE-INVESTIGATION" id="taxadvice_box">
            <?php if(isset($client['crm_investigation_start_date']) && ($client['crm_investigation_start_date']!='') ){ ?>
                      <div class="form-group row help_icon date_birth taxadvice_sort" data-id="<?php echo $Setting_Order['crm_investigation_start_date']; ?>" <?php echo $Setting_Delete['crm_investigation_start_date']; ?>>
                        <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_investigation_start_date']; ?> </label>
                        <div class="col-sm-8">
                         
                          <span class="small-info"><?php if(isset($client['crm_investigation_start_date']) && ($client['crm_investigation_start_date']!='') ){ echo Change_Date_Format($client['crm_investigation_start_date']);}?></span>
                        </div>
                      </div>
                      <?php } ?>
                      <?php if(isset($client['crm_investigation_end_date']) && ($client['crm_investigation_end_date']!='') ){ ?>
                      <div class="form-group row help_icon date_birth taxadvice_sort" data-id="<?php echo $Setting_Order['crm_investigation_end_date']; ?>">
                        <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_investigation_end_date']; ?> </label>
                        <div class="col-sm-8">
                         
                         <span class="small-info"><?php if(isset($client['crm_investigation_end_date']) && ($client['crm_investigation_end_date']!='') ){ echo Change_Date_Format($client['crm_investigation_end_date']);}?></span>
                        </div>
                      </div>
                      <?php } ?>
                    <?php if(isset($client['crm_investigation_note']) && ($client['crm_investigation_note']!='') ){  ?>
                      <div class="form-group row taxadvice_sort" data-id="<?php echo $Setting_Order['crm_investigation_note']; ?>" <?php echo $Setting_Delete['crm_investigation_note']; ?>>
                        <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_investigation_note']; ?> </label>
                        <div class="col-sm-8">
                         
                          <p class="for-address"><?php if(isset($client['crm_investigation_note']) && ($client['crm_investigation_note']!='') ){ echo $client['crm_investigation_note'];}?></p>
                        </div>
                      </div>
                      <?php } ?>
                    </div>
                    <div class="col-sm-10"> 
                    <span class="small-info">
                  <label class="col-sm-4 col-form-label">Choose the date from which you want to set the reminders</label>
                    <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span><input type="text" name="taxadvice" placeholder="dd-mm-yyyy" value="" class="fields datepicker_service" readonly>
                </span> 
                  <span class="small-info" style="margin-top: 10px;">
                <button class="btn-primary btn-sm reminder_in_queue" data-client_id="<?php echo $client['id'];?>" data-user_id="0" data-contact_id="<?php echo (array_search('taxadvice', $fill) + 1); ?>">Create Reminders</button>
              </span>
            </div>
                  </div>
                </div>
              </div>
              <div class="accordion-panel enable_taxad" style="<?php echo ($tab_on['taxadvice']['reminder']!==0  ? "display: block" :"display:none;");?>">
   <div class="box-division03">
      <div class="accordion-heading" role="tab" id="headingOne">
         <h3 class="card-title accordion-title">
            <a class="accordion-msg">Tax advice/investigation Reminders</a>
         </h3>
      </div>
      <div id="collapse" class="panel-collapse">
         <div class="basic-info-client1 CONTENT_TAX_ADVICE-INVESTIGATION_REMINDERS" id="taxadvice_box1">
            
            <?php if(isset($client['crm_investigation_next_reminder_date']) && ($client['crm_investigation_next_reminder_date']!='') ){ ?>

            <div class="form-group row  date_birth radio_bts taxadvice_sort" data-id="<?php echo $Setting_Order['crm_investigation_next_reminder_date']; ?>" <?php echo $Setting_Delete['crm_investigation_next_reminder_date']; ?>>
               <label class="col-sm-4 col-form-label"><a id="investigation_add_custom_reminder_link" name="investigation_add_custom_reminder_link" href="<?php echo base_url(); ?>user/Service_reminder_settings/15" target="_blank"><?php echo $Setting_Label['crm_investigation_next_reminder_date']; ?></a></label>
               <div class="col-sm-8">
                  <input type="checkbox" class="js-small f-right fields as_per_firm " name="investigation_next_reminder_date" id="investigation_next_reminder_date"  checked="checked" readonly>
               </div>
            </div>
          <?php } ?>
            <!-- <div class="form-group row radio_bts ">
               <label class="col-sm-4 col-form-label">Create Task For Reminders</label>
               <div class="col-sm-8">
                  <input type="checkbox" class="js-small f-right fields" name="investigation_create_task_reminder" id="investigation_create_task_reminder" <?php if(isset($client['crm_investigation_create_task_reminder']) && ($client['crm_investigation_create_task_reminder']=='on') ){?> checked="checked"<?php } ?> value="on" >
               </div>
            </div> -->
            <?php if (!empty($client['crm_investigation_add_custom_reminder']) ){ ?>
            <div class="form-group row radio_bts investigation_add_custom_reminder_label taxadvice_sort" data-id="<?php echo $Setting_Order['crm_investigation_add_custom_reminder']; ?>" <?php echo $Setting_Delete['crm_investigation_add_custom_reminder']; ?>>
               <label class="col-sm-4 col-form-label" id="investigation_add_custom_reminder_label" name="investigation_add_custom_reminder_label"><?php echo $Setting_Label['crm_investigation_add_custom_reminder']; ?></label>
               <div class="col-sm-8" id="investigation_add_custom_reminder">
                  <input type="checkbox" class="js-small f-right fields cus_reminder" name="investigation_add_custom_reminder" id="" data-id="investigation_cus" data-serid="15" checked="checked"  readonly>
               </div>
            </div>
           <?php } ?>
         </div>
      </div>
   </div>
   </div>
   <!-- accordion-panel -->
   </div>
</li>

<!-- vat returns tab start -->

<?php

$vatService ="display:none";

if($tab_on['vat']['tab']!==0)
{
  $vatService ="display:block";
}

?>

<li class="show-block" style="<?php echo $vatService ?>">
<a href="#" class="toggle"> VAT Returns </a>
<div id="vat-Returns" class="masonry-container floating_set inner-views">
      <div class="accordion-panel">
        <div class="box-division03">
          <div class="accordion-heading" role="tab" id="headingOne">
            <h3 class="card-title accordion-title">
              <a class="accordion-msg">Important Information</a>
            </h3>
          </div>
          <div id="collapse" class="panel-collapse">
            <div class="basic-info-client1 CONTENT_IMPORTANT_INFORMATION" id="vat_box0">
            <?php if(isset($client['crm_vat_number_one']) && $client['crm_vat_number_one']!=''){ ?>
              <div class="form-group row name_fields">
                <label class="col-sm-4 col-form-label">VAT Number</label>
                <div class="col-sm-8">
                 <!--  <input type="number" name="vat_number_one" id="vat_number_one" placeholder="" value="<?php if(isset($client['crm_vat_number_one'])){ echo $client['crm_vat_number_one']; } ?>" class="fields"> -->
                 <span class="small-info"><?php if(isset($client['crm_vat_number_one'])){ echo $client['crm_vat_number_one']; } ?></span>
                </div>
              </div>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
      <!-- accordion-panel -->
      <div class="accordion-panel">
        <div class="box-division03">
          <div class="accordion-heading" role="tab" id="headingOne">
            <h3 class="card-title accordion-title">
              <a class="accordion-msg">VAT</a>
            </h3>
          </div>
          <div id="collapse" class="panel-collapse">
                     <div class="basic-info-client1 CONTENT_VAT" id="vat_box">

                      <?php if(isset($client['crm_vat_date_of_registration']) && ($client['crm_vat_date_of_registration']!='') ){ ?>
                        <div class="form-group row  date_birth vat_sort" data-id="<?php echo $Setting_Order['crm_vat_date_of_registration']; ?>" <?php echo $Setting_Delete['crm_vat_date_of_registration']; ?>>
                           <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_vat_date_of_registration']; ?> </label>
                           <div class="col-sm-8">
                              <?php if(isset($client['crm_vat_date_of_registration']) && ($client['crm_vat_date_of_registration']!='') ){ echo  Change_Date_Format($client['crm_vat_date_of_registration']);}?>
                           </div>
                        </div>
                        <?php } ?>
                         <?php if(isset($client['crm_vat_frequency']) && $client['crm_vat_frequency']!=''){ ?>
                        <div class="form-group row vat_sort" data-id="<?php echo $Setting_Order['crm_vat_frequency']; ?>" <?php echo $Setting_Delete['crm_vat_frequency']; ?>>
                           <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_vat_frequency']; ?> </label>
                           <div class="col-sm-8">
                              <?php if(isset($client['crm_vat_frequency'])){ echo $client['crm_vat_frequency']; }?>
                           </div>
                        </div>
                        <?php } ?>
                         <?php if(isset($client['crm_vat_quater_end_date']) && ($client['crm_vat_quater_end_date']!='') ){ ?>
                        <div class="form-group row help_icon date_birth vat_sort" data-id="<?php echo $Setting_Order['crm_vat_quater_end_date']; ?>" <?php echo $Setting_Delete['crm_vat_quater_end_date']; ?>>
                           <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_vat_quater_end_date']; ?> </label>
                           <div class="col-sm-8">
                              <?php if(isset($client['crm_vat_quater_end_date']) && ($client['crm_vat_quater_end_date']!='') ){ echo  Change_Date_Format($client['crm_vat_quater_end_date']);}?>
                           </div>
                        </div>
                        <?php } ?>
                        <?php if(isset($client['crm_vat_quarters']) && $client['crm_vat_quarters']!=''){ ?>
                        <div class="form-group row vat_sort" data-id="<?php echo $Setting_Order['crm_vat_quarters']; ?>">
                           <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_vat_quarters']; ?> </label>
                           <div class="col-sm-8">
                             <?php if(isset($client['crm_vat_quarters'])){ echo $client['crm_vat_quarters']; }?> 
                           </div>
                        </div>
                        <?php } ?>
                         <?php if(isset($client['crm_last_vat_return_filed_upto']) && ($client['crm_last_vat_return_filed_upto']!='') ){ ?>
                        <div class="form-group row help_icon date_birth vat_sort" data-id="<?php echo $Setting_Order['crm_last_vat_return_filed_upto']; ?>" <?php echo $Setting_Delete['crm_last_vat_return_filed_upto']; ?>>
                           <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_last_vat_return_filed_upto']; ?> </label>
                           <div class="col-sm-8">
                              <?php if(isset($client['crm_last_vat_return_filed_upto']) && ($client['crm_last_vat_return_filed_upto']!='') ){ echo  Change_Date_Format($client['crm_last_vat_return_filed_upto']);}?>
                           </div>
                        </div>
                        <?php } ?>
                         <?php if(isset($client['crm_vat_scheme']) && $client['crm_vat_scheme']!='') { ?>
                        <div class="form-group row vat_sort" data-id="<?php echo $Setting_Order['crm_vat_scheme']; ?>"
                          <?php echo $Setting_Delete['crm_vat_scheme']; ?>>
                           <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_vat_scheme']; ?> </label>
                           <div class="col-sm-8">
                             <?php if(isset($client['crm_vat_scheme'])) { echo $client['crm_vat_scheme']; } ?>
                           </div>
                        </div>
                        <?php } ?>
                         <?php if(isset($client['crm_flat_rate_category']) && ($client['crm_flat_rate_category']!='') ){ ?>
                        <div class="form-group row vat_sort" data-id="<?php echo $Setting_Order['crm_flat_rate_category']; ?>" <?php echo $Setting_Delete['crm_flat_rate_category']; ?>>
                           <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_flat_rate_category']; ?> </label>
                           <div class="col-sm-8">
                             <?php if(isset($client['crm_flat_rate_category']) && ($client['crm_flat_rate_category']!='') ){ echo $client['crm_flat_rate_category'];}?>
                           </div>
                        </div>
                        <?php } ?>
                        <?php if(isset($client['crm_flat_rate_percentage']) && ($client['crm_flat_rate_percentage']!='') ){ ?>
                        <div class="form-group row vat_sort" data-id="<?php echo $Setting_Order['crm_flat_rate_percentage']; ?>" <?php echo $Setting_Delete['crm_flat_rate_percentage']; ?>>
                           <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_flat_rate_percentage']; ?> </label>
                           <div class="col-sm-8">
                             <?php if(isset($client['crm_flat_rate_percentage']) && ($client['crm_flat_rate_percentage']!='') ){ echo $client['crm_flat_rate_percentage'];}?>
                           </div>
                        </div>
                        <?php } ?>
                        <?php if(isset($client['crm_direct_debit']) && ($client['crm_direct_debit']=='on') ){?>
                        <div class="form-group row name_fields vat_sort" data-id="<?php echo $Setting_Order['crm_direct_debit']; ?>" <?php echo $Setting_Delete['crm_direct_debit']; ?>>
                           <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_direct_debit']; ?> </label>
                           <div class="col-sm-8">
                              <input type="checkbox" class="js-small f-right fields" name="direct_debit" id="direct_debit" <?php if(isset($client['crm_direct_debit']) && ($client['crm_direct_debit']=='on') ){?> checked="checked"<?php } ?> readonly>
                           </div>
                        </div>
                        <?php } ?>  
                         <?php if(isset($client['crm_annual_accounting_scheme']) && ($client['crm_annual_accounting_scheme']=='on') ){?>
                        <div class="form-group row name_fields vat_sort" data-id="<?php echo $Setting_Order['crm_annual_accounting_scheme']; ?>" <?php echo $Setting_Delete['crm_annual_accounting_scheme']; ?>>
                           <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_annual_accounting_scheme']; ?> </label>
                           <div class="col-sm-8">
                              <input type="checkbox" class="js-small f-right fields" name="annual_accounting_scheme" id="annual_accounting_scheme" <?php if(isset($client['crm_annual_accounting_scheme']) && ($client['crm_annual_accounting_scheme']=='on') ){?> checked="checked"<?php } ?> readonly>
                           </div>
                        </div>
                        <?php } ?>
                        <?php if(isset($client['crm_box5_of_last_quarter_submitted']) && $client['crm_box5_of_last_quarter_submitted']!=''){ ?>
                        <div class="form-group row vat_sort" data-id="<?php echo $Setting_Order['crm_box5_of_last_quarter_submitted']; ?>" <?php echo $Setting_Delete['crm_box5_of_last_quarter_submitted']; ?>>
                           <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_box5_of_last_quarter_submitted']; ?> </label>
                           <div class="col-sm-8">
                             <?php if(isset($client['crm_box5_of_last_quarter_submitted'])){ echo $client['crm_box5_of_last_quarter_submitted']; } ?>
                           </div>
                        </div>
                        <?php } ?>
                         <?php if(isset($client['crm_vat_address']) && ($client['crm_vat_address']!='') ){ ?>
                        <div class="form-group row vat_sort" data-id="<?php echo $Setting_Order['crm_vat_address']; ?>"
                          <?php echo $Setting_Delete['crm_vat_address']; ?>>
                           <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_vat_address']; ?> </label>
                           <div class="col-sm-8">
                           <?php if(isset($client['crm_vat_address']) && ($client['crm_vat_address']!='') ){ echo $client['crm_vat_address'];}?>                       
                         </div>
                        </div>
                        <?php } ?>
                     </div>
                     <div class="col-sm-10">
                        <span class="small-info">
                        <label class="col-sm-4 col-form-label">Choose the date from which you want to set the reminders</label>
                          <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span><input type="text" name="vat" placeholder="dd-mm-yyyy" value="" class="fields datepicker_service" readonly>
                      </span>
                         <span class="small-info" style="margin-top: 10px;">
                          <button class="btn-primary btn-sm reminder_in_queue" data-client_id="<?php echo $client['id'];?>" data-user_id="0" data-contact_id="<?php echo (array_search('vat', $fill) + 1); ?>">Create Reminders</button>
                        </span>
                      </div>
                  </div>
        </div>
      </div>
      <!-- accordion-panel -->
      <div class="accordion-panel enable_vat" >
        <div class="box-division03">
          <div class="accordion-heading" role="tab" id="headingOne">
            <h3 class="card-title accordion-title">
              <a class="accordion-msg">VAT Reminders</a>
            </h3>
          </div>
          <div id="collapse" class="panel-collapse">
            <div class="basic-info-client1 CONTENT_VAT_REMINDERS" id="vat_box1" >
               <?php if(isset($client['crm_vat_next_reminder_date']) && ($client['crm_vat_next_reminder_date']!='') ){?>
              <div class="form-group row  date_birth vat_sort" data-id="<?php echo $Setting_Order['crm_vat_next_reminder_date']; ?>" <?php echo $Setting_Delete['crm_vat_next_reminder_date']; ?>>
               
                <div class="col-sm-8">
                  
                  <label class="col-sm-4 col-form-label"><a id="vat_add_custom_reminder_link" name="vat_add_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/5" target="_blank"><?php echo $Setting_Label['crm_vat_next_reminder_date']; ?></a></label>

                  <input type="checkbox" class="js-small f-right fields as_per_firm" name="vat_next_reminder_date" id="vat_next_reminder_date"  checked="checked" readonly>

                </div>
              </div>
            <?php } ?>
              
              <?php if(isset($client['crm_vat_add_custom_reminder']) && ($client['crm_vat_add_custom_reminder']!='') ){?>
              <div class="form-group row radio_bts vat_sort" data-id="<?php echo $Setting_Order['crm_vat_add_custom_reminder']; ?>" <?php echo $Setting_Delete['crm_vat_add_custom_reminder']; ?> style="">
                <label id="vat_add_custom_reminder_label" name="vat_add_custom_reminder_label" class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_vat_add_custom_reminder']; ?> </label>
                <div class="col-sm-8">
                   <input type="checkbox" class="js-small f-right fields" name="vat_add_custom_reminder" id="vat_add_custom_reminder"  checked="checked" readonly>
                </div>
              </div>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
      
      <!-- accordion-panel -->
    </div>

    <!-- VAT Returns -->

    </li>
    <!-- vat returns tab end -->


<!-- work pension start -->
<li class="show-block" style="<?php echo ($tab_on['workplace']['tab']!==0?'display: block':'display: none;'); ?>">
 <a href="#" class="toggle"> WorkPlace Pension - AE</a>
   <div id="worktab" class="masonry-container floating_set inner-views">
     <div class="grid-sizer"></div>
   <div class="accordion-panel">
      <div class="box-division03">
         <div class="accordion-heading" role="tab" id="headingOne">
            <h3 class="card-title accordion-title">
               <a class="accordion-msg">WorkPlace Pension - AE       </a>
            </h3>
         </div>
         <div id="collapse" class="panel-collapse">
            <div class="basic-info-client1 CONTENT_WORKPLACE_PENSION_AE" id="workplace_sec">
               
               <?php if(isset($client['crm_staging_date']) && ($client['crm_staging_date']!='') ){ ?>

               <div class="form-group row  date_birth workplace_sort" data-id="<?php echo $Setting_Order['crm_staging_date']; ?>" <?php echo $Setting_Delete['crm_staging_date']; ?>>
                  <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_staging_date']; ?></label>
                  <div class="col-sm-8">
                     <?php echo Change_Date_Format( $client['crm_staging_date'] ); ?>
                  </div>
               </div>

            <?php } ?>

               <?php if(isset($client['crm_pension_id']) && ($client['crm_pension_id']!='') ){ ?>
               <div class="form-group row workplace_sort" data-id="<?php echo $Setting_Order['crm_pension_id']; ?>"
                <?php echo $Setting_Delete['crm_pension_id']; ?>>
                  <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_pension_id']; ?></label>
                  <div class="col-sm-8">
                     <?php if(isset($client['crm_pension_id']) && ($client['crm_pension_id']!='') ){ echo $client['crm_pension_id'];}?>
                  </div>
               </div>
                <?php } ?>
                <?php if(isset($client['crm_pension_subm_due_date']) && ($client['crm_pension_subm_due_date']!='') ){ ?>
               <div class="form-group row  date_birth workplace_sort" data-id="<?php echo $Setting_Order['crm_pension_subm_due_date']; ?>">
                  <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_pension_subm_due_date']; ?>
                  </label>
                  <div class="col-sm-8">
                     <?php if(isset($client['crm_pension_subm_due_date']) && ($client['crm_pension_subm_due_date']!='') )
                     { 
                        echo Change_Date_Format( $client['crm_pension_subm_due_date'] );
                     }
                     ?>
                  </div>
               </div>
               <?php } ?>

               <?php if(isset($client['crm_postponement_date']) && ($client['crm_postponement_date']!='') ){ ?>
               <div class="form-group row  date_birth workplace_sort" data-id="<?php echo $Setting_Order['crm_postponement_date']; ?>"
                <?php echo $Setting_Delete['crm_postponement_date']; ?>>
                  <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_postponement_date']; ?></label>
                  <div class="col-sm-8">
                    <?php if(isset($client['crm_postponement_date']) && ($client['crm_postponement_date']!='') ){ echo Change_Date_Format( $client['crm_postponement_date'] );}?>
                  </div>
               </div>
               <?php } ?>
                <?php if(isset($client['crm_the_pensions_regulator_opt_out_date']) && ($client['crm_the_pensions_regulator_opt_out_date']!='') ){ ?>
               <div class="form-group row  date_birth workplace_sort" data-id="<?php echo $Setting_Order['crm_the_pensions_regulator_opt_out_date']; ?>" <?php echo $Setting_Delete['crm_the_pensions_regulator_opt_out_date']; ?>>
                  <label class="col-sm-4 col-form-label">
                    <?php echo $Setting_Label['crm_the_pensions_regulator_opt_out_date']; ?>
                  </label>
                  <div class="col-sm-8">
                     <?php if(isset($client['crm_the_pensions_regulator_opt_out_date']) && ($client['crm_the_pensions_regulator_opt_out_date']!='') ){ echo Change_Date_Format ( $client['crm_the_pensions_regulator_opt_out_date'] );}?>
                  </div>
               </div>
               <?php } ?>
               <?php if(isset($client['crm_re_enrolment_date']) && ($client['crm_re_enrolment_date']!='') ){ ?>
               <div class="form-group row  date_birth workplace_sort" data-id="<?php echo $Setting_Order['crm_re_enrolment_date']; ?>" <?php echo $Setting_Delete['crm_re_enrolment_date']; ?>>
                  <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_re_enrolment_date']; ?></label>
                  <div class="col-sm-8">
                     <?php if(isset($client['crm_re_enrolment_date']) && ($client['crm_re_enrolment_date']!='') ){ echo Change_Date_Format( $client['crm_re_enrolment_date'] );}?>
                  </div>
               </div>
                <?php } ?>
               <?php if(isset($client['crm_declaration_of_compliance_due_date']) && ($client['crm_declaration_of_compliance_due_date']!='') ){ ?>
               <div class="form-group row  date_birth workplace_sort" data-id="<?php echo $Setting_Order['crm_declaration_of_compliance_due_date']; ?>" <?php echo $Setting_Delete['crm_declaration_of_compliance_due_date']; ?>>
                  <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_declaration_of_compliance_due_date']; ?></label>
                  <div class="col-sm-8">
                     <?php if(isset($client['crm_declaration_of_compliance_due_date']) && ($client['crm_declaration_of_compliance_due_date']!='') ){ echo Change_Date_Format( $client['crm_declaration_of_compliance_due_date'] );}?>
                  </div>
               </div>
               <?php } ?>
               <?php if(isset($client['crm_declaration_of_compliance_submission']) && ($client['crm_declaration_of_compliance_submission']!='') ){ ?>
               <div class="form-group row  date_birth workplace_sort" data-id="<?php echo $Setting_Order['crm_declaration_of_compliance_submission']; ?>" <?php echo $Setting_Delete['crm_declaration_of_compliance_submission']; ?>>
                  <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_declaration_of_compliance_submission']; ?></label>
                  <div class="col-sm-8">
                    <?php if(isset($client['crm_declaration_of_compliance_submission']) && ($client['crm_declaration_of_compliance_submission']!='') ){ echo Change_Date_Format( $client['crm_declaration_of_compliance_submission'] );}?>
                  </div>
               </div>
               <?php } ?>

                <?php if(isset($client['crm_paye_pension_provider']) && ($client['crm_paye_pension_provider']!='') ){ ?>
               <div class="form-group row workplace_sort" data-id="<?php echo $Setting_Order['crm_paye_pension_provider']; ?>"
                <?php echo $Setting_Delete['crm_paye_pension_provider']; ?>>
                  <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_paye_pension_provider']; ?></label>
                  <div class="col-sm-8">
                     <?php if(isset($client['crm_paye_pension_provider']) && ($client['crm_paye_pension_provider']!='') ){ echo $client['crm_paye_pension_provider'];}?>
                  </div>
               </div>
                <?php } ?>
              <!--  <div class="form-group row workplace_sort" data-id="<?php echo $Setting_Order['crm_pension_id']; ?>"
                <?php echo $Setting_Delete['crm_pension_id']; ?>>
                  <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_pension_id']; ?></label>
                  <div class="col-sm-8">
                     <input type="text" name="paye_pension_provider_userid" id="paye_pension_provider_userid" value="<?php if(isset($client['crm_pension_id']) && ($client['crm_pension_id']!='') ){ echo $client['crm_pension_id'];}?>" class="fields">
                  </div>
               </div> -->
                <?php if(isset($client['crm_paye_pension_provider_password']) && ($client['crm_paye_pension_provider_password']!='') ){ ?>
               <div class="form-group row workplace_sort" data-id="<?php echo $Setting_Order['crm_paye_pension_provider_password']; ?>" <?php echo $Setting_Delete['crm_paye_pension_provider_password']; ?>>
                  <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_paye_pension_provider_password']; ?></label>
                  <div class="col-sm-8">
                     <?php if(isset($client['crm_paye_pension_provider_password']) && ($client['crm_paye_pension_provider_password']!='') ){ echo $client['crm_paye_pension_provider_password'];}?>
                  </div>
               </div>
                <?php } ?>
                <?php if(isset($client['crm_employer_contri_percentage']) && ($client['crm_employer_contri_percentage']!='') ){ ?>
               <div class="form-group row workplace_sort" data-id="<?php echo $Setting_Order['crm_employer_contri_percentage']; ?>"
                <?php echo $Setting_Delete['crm_employer_contri_percentage']; ?>>
                  <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_employer_contri_percentage']; ?></label>
                  <div class="col-sm-8">
                     <?php if(isset($client['crm_employer_contri_percentage']) && ($client['crm_employer_contri_percentage']!='') ){ echo $client['crm_employer_contri_percentage'];}?>
                  </div>
               </div>
               <?php } ?>
               <?php if(isset($client['crm_employee_contri_percentage']) && ($client['crm_employee_contri_percentage']!='') ){ ?>
               <div class="form-group row workplace_sort" data-id="<?php echo $Setting_Order['crm_employee_contri_percentage']; ?>"
                <?php echo $Setting_Delete['crm_employee_contri_percentage']; ?>>
                  <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_employee_contri_percentage']; ?></label>
                  <div class="col-sm-8">
                     <?php if(isset($client['crm_employee_contri_percentage']) && ($client['crm_employee_contri_percentage']!='') ){ echo $client['crm_employee_contri_percentage'];}?>
                  </div>
               </div>
               <?php } ?>
               <?php if(isset($client['crm_pension_notes']) && ($client['crm_pension_notes']!='') ){ ?>
               <div class="form-group row workplace_sort" data-id="<?php echo $Setting_Order['crm_pension_notes']; ?>"
                <?php echo $Setting_Delete['crm_pension_notes']; ?>>
                  <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_pension_notes']; ?></label>
                  <div class="col-sm-8">
                     <?php if(isset($client['crm_pension_notes']) && ($client['crm_pension_notes']!='') ){ echo $client['crm_pension_notes'];}?>
                  </div>
               </div>
               <?php } ?>
            </div>
            <div class="col-sm-10"> 
                <span class="small-info">
                  <label class="col-sm-4 col-form-label">Choose the date from which you want to set the reminders</label>
                    <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span><input type="text" name="workplace" placeholder="dd-mm-yyyy" value="" class="fields datepicker_service" readonly>
                </span> 
                  <span class="small-info" style="margin-top: 10px;">
                <button class="btn-primary btn-sm reminder_in_queue" data-client_id="<?php echo $client['id'];?>" data-user_id="0" data-contact_id="<?php echo (array_search('workplace', $fill) + 1); ?>">Create Reminders</button>
              </span>
            </div>
         </div>
      </div>
   </div>
   <!-- accordion-panel -->
   <div class="accordion-panel enable_work" style="<?php echo ($tab_on['workplace']['reminder']!==0?'display: block':'display: none;');?>">
      <div class="box-division03">
         <div class="accordion-heading" role="tab" id="headingOne">
            <h3 class="card-title accordion-title">
               <a class="accordion-msg">Pension Reminders</a>
            </h3>
         </div>
         <div id="collapse" class="panel-collapse">
            <div class="basic-info-client1 CONTENT_WORKPLACE_PENSION_AE_REMINDERS" id="workplace_sec1">

              
              <?php if(isset($client['crm_pension_next_reminder_date']) && ($client['crm_pension_next_reminder_date']!='') ){ ?>

               <div class="form-group row radio_bts date_birth workplace_sort" data-id="<?php echo $Setting_Order['crm_pension_next_reminder_date']; ?>"
                <?php echo $Setting_Delete['crm_pension_next_reminder_date']; ?>>
                  <label class="col-sm-4 col-form-label"><a id="pension_create_task_reminder_link" name="pension_create_task_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/7" target="_blank"><?php echo $Setting_Label['crm_pension_next_reminder_date']; ?></a></label>
                  <div class="col-sm-8">
                    <input type="checkbox" class="js-small f-right fields as_per_firm" name="pension_next_reminder_date" id="Pension_next_reminder_date"  checked='checked' readonly>
                  </div>
               </div>
               <?php } ?>
            
             
               <?php if(isset($client['crm_pension_add_custom_reminder']) && ($client['crm_pension_add_custom_reminder']!='') ){ ?>
               <div class="form-group row radio_bts pension_create_task_reminder_label workplace_sort" data-id="<?php echo $Setting_Order['crm_pension_add_custom_reminder']; ?>" <?php echo $Setting_Delete['crm_pension_add_custom_reminder']; ?>>
                  <label id="pension_create_task_reminder_label" name="pension_create_task_reminder_label" class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_pension_add_custom_reminder']; ?></label>
                  <div class="col-sm-8" id="pension_add_custom_reminder">
                     <input type="checkbox" class="js-small f-right fields cus_reminder" name="pension_add_custom_reminder" id="" data-id="pension_cus" data-serid="7" value="<?php echo $work_pension_cus_reminder;?>" checked  readonly>
                  </div>
               </div>
                <?php } ?> 
            </div>
         </div>
      </div>
   </div>
   <!-- accordion-panel -->
</div>
<!--worktab close-->
</li>
<!--work pension close -->

<?php 
  $cnt = 1;
  foreach ($contactRec as $key => $contactRec_val) {
    $ser_reminder                 =   ( $contactRec_val['has_ptr_reminder'] == 1 ?"block":"none" );
    $has_property_income          =   ( $contactRec_val['property_income']==1?"checked='checked'":'' );
    $has_additional_income        =   ( $contactRec_val['additional_income']==1?"checked='checked'":'' );
    $has_additional_income_note   =   ( $contactRec_val['additional_income']==1?"block":'none' );
    $is_as_per_firm               =   ( $contactRec_val['is_ptr_as_per_firm_reminder']==1?"checked='checked'":'' );               
    $is_cus_reminder              =   ( $contactRec_val['is_ptr_cus_reminder']==1?"checked='checked'":'' );              
  ?>
<li class="show-block" style="display:<?php echo ($contactRec_val['has_ptr_service'] == 1?'block':'none');?>" >
<a href="#" class="toggle">Personal Tax Return 
    <b class="ccp_name_<?php echo $cnt;?>"> - <?php echo $contactRec_val['first_name'].' '.$contactRec_val['last_name'];?></b>
      </a>
<!-- personal-tax-returns-->               
  <div id="ccp_<?php echo $cnt;?>_personal_tax_returns" class="ccp_ptr_cnt masonry-container floating_set inner-views LISTEN_CONTENT">
     <div class="grid-sizer"></div>
     <div class="accordion-panel">
        <div class="box-division03">
           <div class="accordion-heading" role="tab" id="headingOne">
              <h3 class="card-title accordion-title">
                 <a class="accordion-msg">Important Information</a>
              </h3>
           </div>
           <div id="collapse" class="panel-collapse">
              <div class="basic-info-client1 CONTENT_IMPORTANT_INFORMATION" id="personal_box">
                
                <?php if( !empty($contactRec_val['utr_number']) ){ ?>
                 <div class="form-group row personal_sort" data-id="<?php echo $Setting_Order['crm_personal_utr_number']; ?>"
                   <?php echo $Setting_Delete['crm_personal_utr_number']; ?>>
                     <label class="col-sm-4 col-form-label">
                        <?php echo $Setting_Label['crm_personal_utr_number']; ?>                        
                     </label>
                    <div class="col-sm-8">
                       <span class="small-info"><?php echo $contactRec_val['utr_number'];?></span>
                    </div>
                 </div>
                <?php } ?>

                <?php if( !empty($contactRec_val['ni_number']) ){ ?>
                 <div class="form-group row personal_sort" data-id="<?php echo $Setting_Order['crm_ni_number']; ?>"
                  <?php echo $Setting_Delete['crm_ni_number']; ?>>
                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_ni_number']; ?></label>
                    <div class="col-sm-8">
                       <span class="small-info"><?php echo $contactRec_val['ni_number']; ?></span>
                    </div>
                 </div>
                 <?php } ?>

                 <?php if( !empty($has_property_income) ){ ?>
                 <div class="form-group row radio_bts personal_sort" data-id="<?php echo $Setting_Order['crm_property_income']; ?>" <?php echo $Setting_Delete['crm_property_income']; ?>>
                    <label class="col-sm-4 col-form-label">
                      <?php echo $Setting_Label['crm_property_income']; ?>
                    </label>
                    <div class="col-sm-8">
                       <input type="checkbox" class="js-small f-right fields" name="ccp_property_income[<?php echo $cnt;?>]" data-id="property_income" value="1" <?php echo $has_property_income;?> readonly>
                    </div>
                 </div>
                 <?php } ?>

                 <?php if( !empty($has_additional_income) ){ ?>
                 <div class="form-group row radio_bts personal_sort" data-id="<?php echo $Setting_Order['crm_additional_income']; ?>" <?php echo $Setting_Delete['crm_additional_income']; ?>>
                    <label class="col-sm-4 col-form-label">
                      <?php echo $Setting_Label['crm_additional_income']; ?>
                    </label>
                    <div class="col-sm-8">
                       <input type="checkbox" class="js-small f-right fields" name="ccp_additional_income[<?php echo $cnt;?>]" value="1"
                        <?php echo $has_additional_income;?> readonly>
                    </div>
                 </div>
                 <?php } ?>

                 <?php if( !empty( $contactRec_val['property_income_notes'] ) )
                 {
                  ?>
                  <div class="form-group row text-box1 personal_sort property_income_notes" data-id="<?php echo $Setting_Order['property_income_notes']; ?>" style="display:<?php echo $has_additional_income_note;?> ">
                   <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['property_income_notes']; ?> </label>
                   <div class="col-sm-8">
                      <span class="small-info"><?php echo                $contactRec_val['property_income_notes'];?>                              
                     </span>
                   </div>
                  </div>
                <?php } ?>
              </div>
           </div>
        </div>
     </div>
     <!-- accordion-panel -->
     <div class="accordion-panel">
        <div class="box-division03">
           <div class="accordion-heading" role="tab" id="headingOne">
              <h3 class="card-title accordion-title">
                 <a class="accordion-msg">Personal Tax Return</a>
              </h3>
           </div>
           <div id="collapse" class="panel-collapse">
              <div class="basic-info-client1 CONTENT_PERSONAL_TAX_RETURN" id="personal_box1">

                <?php if( !empty( $contactRec_val['personal_tax_return_date'] )) {?>
                 <div class="form-group row name_fields date_birth personal_sort" data-id="<?php echo $Setting_Order['crm_personal_tax_return_date']; ?>">
                    <label class="col-sm-4 col-form-label"> <?php echo $Setting_Label['crm_personal_tax_return_date']; ?>                                      
                    <span class="Hilight_Required_Feilds">*</span>
                    </label>
                    <div class="col-sm-8">
                       <span class="small-info"><?php echo Change_Date_Format($contactRec_val['personal_tax_return_date']); ?></span> 
                    </div>
                 </div>
               <?php } ?>
               <?php if( !empty( $contactRec_val['personal_due_date_return'] )) {?>
                 <div class="form-group row name_fields date_birth personal_sort" data-id="<?php echo $Setting_Order['crm_personal_due_date_return']; ?>" <?php echo $Setting_Delete['crm_personal_due_date_return']; ?>>
                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_personal_due_date_return']; ?></label>
                    <div class="col-sm-8">
                       <span class="small-info"> <?php echo Change_Date_Format($contactRec_val['personal_due_date_return']); ?> </span>
                    </div>
                 </div>
                 <?php } ?>

                <?php if( !empty( $contactRec_val['personal_due_date_online'] )) {?>
                 <div class="form-group row name_fields date_birth personal_sort" data-id="<?php echo $Setting_Order['crm_personal_due_date_online']; ?>" <?php echo $Setting_Delete['crm_personal_due_date_online']; ?>>
                    <label class="col-sm-4 col-form-label"><?php echo $Setting_Label['crm_personal_due_date_online']; ?></label>
                    <div class="col-sm-8">
                       <span class="small-info"><?php echo Change_Date_Format($contactRec_val['personal_due_date_online']); ?></span>
                    </div>
                 </div>
               <?php } ?>
              </div>
              <div class="col-sm-10">
              <span class="small-info">
                  <label class="col-sm-4 col-form-label">Choose the date from which you want to set the reminders</label>
                    <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span><input type="text" name="personal_tax_return" placeholder="dd-mm-yyyy" value="" class="fields datepicker_service" readonly>
                </span>  
                  <span class="small-info" style="margin-top: 10px;">
                <button class="btn-primary btn-sm reminder_in_queue" data-client_id="<?php echo $client['id'];?>" data-user_id="<?php echo $contactRec_val['id'];?>" data-contact_id="<?php echo (array_search('personal_tax_return', $fill) + 1); ?>">Create Reminders</button>
              </span>
            </div>
           </div>
        </div>
     </div>
     <!-- accordion-panel -->
     <div class="accordion-panel" style="display:<?php echo $ser_reminder;?>" id="ccp_<?php echo $cnt?>_reminder">
        <div class="box-division03">
           <div class="accordion-heading" role="tab" id="headingOne">
              <h3 class="card-title accordion-title">
                 <a class="accordion-msg">Personal tax Reminders</a>
              </h3>
           </div>
           <div id="collapse" class="panel-collapse">
              <div class="basic-info-client1 CONTENT_PERSONAL_TAX_RETURN_REMINDERS" id="personal_box2">

                <?php if( !empty( $is_as_per_firm )) {?>
                 <div class="form-group row name_fields radio_bts personal_sort" data-id="<?php echo $Setting_Order['crm_personal_next_reminder_date']; ?>" <?php echo $Setting_Delete['crm_personal_next_reminder_date']; ?>>
                    <label class="col-sm-4 col-form-label">
                        <a id="personal_custom_reminder_link" name="personal_custom_reminder_link" href="<?php echo base_url();?>user/Service_reminder_settings/4" target="_blank"><?php echo $Setting_Label['crm_personal_next_reminder_date']; ?></a>
                    </label>
                    <div class="col-sm-8">                   
                       <input type="checkbox" class="js-small f-right fields as_per_firm LISTEN_CHANGES" name="ccp_ptr_as_per_firm_reminder[<?php echo $cnt;?>]" value="1" <?php echo $is_as_per_firm;?> readonly>
                    </div>
                 </div>
               <?php } ?>
               <?php if( !empty( $is_cus_reminder )) {?>
                 <div class="form-group row radio_bts personal_custom_reminder_label personal_sort" data-id="<?php echo $Setting_Order['crm_personal_custom_reminder']; ?>" <?php echo $Setting_Delete['crm_personal_custom_reminder']; ?>>

                    <label id="personal_custom_reminder_label" name="ccp_personal_custom_reminder_label[<?php echo $cnt;?>]" class="col-sm-4 col-form-label">
                      <?php echo $Setting_Label['crm_personal_custom_reminder']; ?>                      
                    </label>

                    <div class="col-sm-8" id="personal_custom_reminder">
                       <input type="checkbox" class="js-small f-right fields cus_reminder LISTEN_CHANGES" name="ccp_ptr_cus_reminder[<?php echo $cnt;?>]"  data-id="personal_cus" data-serid="4" value="1" <?php echo $is_cus_reminder;?> readonly>
                    </div>
                 </div>
               <?php } ?>
              </div>
           </div>
        </div>
     </div>
     
     <div class="SATR_custom_fields" style="display: none;">
        <?php 
          $_POST['cnt']       = $cnt;
          $user_id_conact_id  = $client['user_id']."--".$contactRec_val['id'];
          
          $fields = $this->db->get_where( 'tblcustomfieldsvalues' , "relid='".$user_id_conact_id."' AND section_name=4 AND value!=''" )->result_array();

          foreach ($fields as $fields_value) {
            echo render_custom_fields_one( '4' , $user_id_conact_id , "id=".$fields_value['fieldid'] ,1);
            unset( $_POST['cnt'] );
          }
        ?>
      </div>

  </div>              
<!-- Personal Tax Return -->
</li>
  <?php
  $cnt++;
}
?>

<?php 
foreach($other_services as $key => $value) 
{ 
   if($tab_on[$value['services_subnames']]['tab'] !== 0)
   {       
?>
<li class="show-block">
  <a href="#" class="toggle"><?php echo $value['service_name']; ?></a>
  <div id="<?php echo $value['services_subnames']; ?>" class="masonry-container floating_set inner-views">
    <div class="grid-sizer"></div>  

    <?php 
          if($tab_on[$value['services_subnames']]['reminder']!==0)
          { 
             $reminder = 'display: block;';
          }
          else
          {
             $reminder = 'display: none;';
          }
    ?>
  
    <div class="accordion-panel panel">
      <div class="box-division03">
        <div class="accordion-heading" role="tab" id="headingOne">
        <h3 class="card-title accordion-title">
        <a class="accordion-msg"><?php echo $value['service_name'].' Due Date'; ?></a>
        </h3>
        </div>
        <div id="collapse" class="panel-collapse">
          <div class="basic-info-client1">
          <?php if(isset($client["crm_".$value['services_subnames']."_statement_date"])){ ?>
          <div class="form-group row taxadvice_sort" data-id="<?php echo 'crm_'.$value['services_subnames'].'_statement_date'; ?>" <?php echo 'crm_'.$value['services_subnames'].'_statement_date'; ?>>
            <label class="col-sm-4 col-form-label"> Due By </label>
            <div class="col-sm-8">             
              <span class="small-info"><?php if($client["crm_".$value['services_subnames']."_statement_date"]!=''){ echo Change_Date_Format($client["crm_".$value['services_subnames']."_statement_date"]);}else{ echo "-"; }?></span>
            </div>
          </div>
          <?php } ?>     
        </div>
        <div class="col-sm-10">
            <span class="small-info">
                  <label class="col-sm-4 col-form-label">Choose the date from which you want to set the reminders</label>
                    <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span><input type="text" name="other_services" placeholder="dd-mm-yyyy" value="" class="fields datepicker_service" readonly>
            </span>
            <span class="small-info" style="margin-top: 10px;">
          <button class="btn-primary btn-sm reminder_in_queue" data-client_id="<?php echo $client['id'];?>" data-user_id="0" data-contact_id="<?php echo $value['id']; ?>">Create Reminders</button>
        </span>
      </div>
      </div>
    </div>   

      <div class="accordion-panel panels" style="<?php echo $reminder; ?>">
      <div class="box-division03" style="height: 135px !important;">
       <div class="accordion-heading" role="tab" id="headingOne">
       <h3 class="card-title accordion-title">
       <a class="accordion-msg"><?php echo $value['service_name'].' Reminders'; ?></a>
       </h3>
       </div>
         <div id="collapse" class="panel-collapse">
            <div class="basic-info-client1" id="box2">
                <?php if(!empty($client["crm_".$value['services_subnames']."_email_remainder"])){ ?>
                <div class="form-group row radio_bts taxadvice_sort" data-id="<?php echo 'crm_'.$value['services_subnames'].'_email_remainder'; ?>" <?php echo 'crm_'.$value['services_subnames'].'_email_remainder'; ?>>
                   <label class="col-sm-4 col-form-label" id="<?php echo 'crm_'.$value['services_subnames'].'_email_remainder'; ?>_label" name="<?php echo 'crm_'.$value['services_subnames'].'_email_remainder'; ?>_label">Email Reminder</label>
                   <div class="col-sm-8" id="">
                      <input type="checkbox" class="js-small f-right fields" name="<?php echo 'crm_'.$value['services_subnames'].'_email_remainder'; ?>" id="" data-id="<?php echo 'crm_'.$value['services_subnames'].'_email_remainder'; ?>" data-serid="<?php echo $value['id']; ?>" checked="checked"  readonly>
                   </div>
                </div>
               <?php } ?>
                <?php if(!empty($client["crm_".$value['services_subnames']."_add_custom_reminder"])){ ?>
                <div class="form-group row radio_bts taxadvice_sort" data-id="<?php echo 'crm_'.$value['services_subnames'].'_add_custom_reminder'; ?>" <?php echo 'crm_'.$value['services_subnames'].'_add_custom_reminder'; ?>>
                   <label class="col-sm-4 col-form-label" id="<?php echo 'crm_'.$value['services_subnames'].'_add_custom_reminder'; ?>_label" name="<?php echo 'crm_'.$value['services_subnames'].'_add_custom_reminder'; ?>_label">Custom Reminder</label>
                   <div class="col-sm-8" id="">
                      <input type="checkbox" class="js-small f-right fields" name="<?php echo 'crm_'.$value['services_subnames'].'_add_custom_reminder'; ?>" id="" data-id="<?php echo 'crm_'.$value['services_subnames'].'_add_custom_reminder'; ?>" data-serid="<?php echo $value['id']; ?>" checked="checked"  readonly>
                   </div>
                </div>
               <?php } ?>                
         </div>
      </div>
   </div>
   </div>
  </div>
  </li>
<?php } }?>