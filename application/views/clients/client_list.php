<?php $this->load->view('includes/header');?>

                    <div class="pcoded-content">

                        <div class="pcoded-inner-content">

                            <!-- Main-body start -->

                            <div class="main-body">

                                <div class="page-wrapper">

                                   



                                    <!-- Page body start -->

                                    <div class="page-body">

                                        <div class="row">

                                            <div class="col-sm-12">

                                                <!-- Register your self card start -->

                                                <div class="card">
	<!-- admin start-->
<div class="client_section col-xs-12 floating_set">
	<div class="all-clients floating_set">
	<div class="tab_section_01 floating_set">
	<ul class="client_tab">
		  <li class="active"><a href="<?php echo  base_url()?>user/client_list">All Clients</a></li>
		  <li><a href="<?php echo  base_url()?>client/addnewclient">Add new client</a></li>
		  <li><a href="<?php echo  base_url()?>user/column_setting">Column Setting</a></li>
</ul>	

<ul class="client_tab">
	<li><a href="#">Email All</a></li>
	<li><a href="#">filters</a></li>
</ul>
</div>
	
	<div class="text-left search_section1 floating_set">
		<div class="search_box01">
	
		<div class="pcoded-search">

                                <span class="searchbar-toggle">  </span>

                                <div class="pcoded-search-box ">

                                    Search Company<input type="text" placeholder="Search" id="searchCompany">
<div id="searchresult"></div>
                                    <span class="search-icon"><i class="ti-search" aria-hidden="true"></i></span>

                                </div>

                            </div>


		
		</div>

	
</div>



</div> <!-- all-clients -->

	<div class="all_user-section floating_set">
		
		<ul class="nav nav-tabs all_user1 md-tabs floating_set">
  <li class="nav-item">
<a class="nav-link active" data-toggle="tab" href="#allusers">All Clients</a><div class="slide"></div></li>
 <!--  <li class="nav-item">
	<a class="nav-link" data-toggle="tab" href="#newuser">New User</a><div class="slide"></div></li> -->
</ul>	

	<div class="all_user-section2 floating_set">
	<div class="tab-content">
		<div id="allusers" class="tab-pane fade in active">

			
			<div class="count-value1 floating_set">
				<!-- <ul class="pull-left">
					<li><select><option>1</option><option>2</option><option>3</option></select></li>
					<li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i><span>selected</span></a></li>
					<li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>
				</ul>	
				
				<ul class="pull-right">
					<form>
						<label>Search all columns</label>
						<input type="text" name="text">
					</form>
				</ul> -->	
				<ul class="select-filter" id="select-filter">
			
			</ul>
			</div>	
			
			<div class="client_section3 table-responsive floating_set">
				<table class="table client_table1 text-center" id="example" cellspacing="0">
					<thead>
						
						<tr class="text-uppercase">
<?php if(isset($column_setting[0]['user_id'])&&($column_setting[0]['user_id']==1)){?>
							<th>Client Id</th>
<?php } if(isset($column_setting[0]['company_name'])&&($column_setting[0]['company_name']==1)){?>							
							<th>Company Name</th>
<?php } if(isset($column_setting[0]['company_type'])&&($column_setting[0]['company_type']==1)){?>	
							<th>Company Type</th>
<?php } if(isset($column_setting[0]['email'])&&($column_setting[0]['email']==1)){?>								
							<th>Email</th>
<?php } if(isset($column_setting[0]['mobile_number'])&&($column_setting[0]['mobile_number']==1)){?>								
							<th>Contact Number</th>
<?php } ?>							
							<th>Status</th>
							<th>Actions</th>
						</tr>	
					</thead>

					<tbody>
						<?php foreach ($client_list as $key => $value) { 
                        $user=$this->Common_mdl->select_record('user','id',$value['user_id']);

                         if($user['status']==1){
                                
                                $active='Active';
                            }elseif($user['status']==2 || $user['status']==0){
                               
                                $active='Inactive';
                            } elseif($user['status']==3){
                            	$active='Frozen';
                            }

							?>
						<tr>
							<!-- <td class="user_imgs"><img src="<?php echo base_url();?>assets/images/avatar-4.jpg" alt="img"></td> -->
							<?php if(isset($column_setting[0]['user_id'])&&($column_setting[0]['user_id']==1)){?>
							<td><?php echo $value['user_id'];?></td>

							<?php } if(isset($column_setting[0]['company_name'])&&($column_setting[0]['company_name']==1)){?>
							<td><?php echo $value['crm_company_name'];?></td>

							<?php } if(isset($column_setting[0]['company_type'])&&($column_setting[0]['company_type']==1)){?>
							<td><?php echo $value['crm_company_type'];?></td>

							<?php }if(isset($column_setting[0]['email'])&&($column_setting[0]['email']==1)){?>
							<td><?php echo $value['crm_email'];?></td>

							<?php } if(isset($column_setting[0]['mobile_number'])&&($column_setting[0]['mobile_number']==1)){?>
							<td><?php echo $value['crm_mobile_number'];?></td>

							<?php } ?>
							<td><?php echo $active;?></td>
							<td>
							<p class="action_01">	
								<a href="<?php echo base_url().'/user/delete/'.$user['id'];?>" onclick="return confirm('Are you sure want to delete');"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>
								<a href="<?php echo base_url().'Client/addnewclient/'.$user['id'];?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
								<!-- <a href="#"><i class="fa fa-inbox fa-6" aria-hidden="true"></i></a> -->
							</p>

							</td>
						</tr>
						<?php } ?>
						
						
						
					</tbody>		
							</table>
					</div>	
									
		</div> <!-- home-->
		
		<div id="newuser" class="tab-pane fade">
			
			<div class="count-value1 floating_set">
				<ul class="pull-left">
					<li><select><option>1</option><option>2</option><option>3</option></select></li>
					<li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i><span>selected</span></a></li>
					<li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>
				</ul>	
				
				<ul class="pull-right">
					<form>
						<label>Search all columns</label>
						<input type="text" name="text">
					</form>
				</ul>
			
			</div>	
			
			<div class="client_section3 table-responsive floating_set">
				<table class="table client_table1 text-center">
					<thead>
						<tr class="text-uppercase">
							<th>Photo</th>
							<th>Name</th>
							<th>Username</th>
							<th>Active</th>
							<th>User Type</th>
							<th>Actions</th>
						</tr>	
					</thead>

					<tbody>
						<tr>
							<td class="user_imgs"><img src="<?php echo base_url();?>assets/images/avatar-4.jpg" alt="img"></td>
							<td>Mr client1</td>
							<td>admin</td>
							<td>
								<p class="onoff"><input type="checkbox" value="1" id="checkboxid4" checked><label for="checkboxid4"></label></p>
							</td>
							<td>Admin</td>
							<td>
							<p class="action_01">	
								<a href="#"><i class="fa fa-trash fa-6" aria-hidden="true"></i></a>
								<a href="#"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
								<a href="#"><i class="fa fa-inbox fa-6" aria-hidden="true"></i></a>
							</p>
							</td>
						</tr>
						
						<tr>
							<td class="user_imgs"><img src="<?php echo base_url();?>assets/images/avatar-4.jpg" alt="img"></td>
							<td>Mr client2</td>
							<td>admin1</td>
							<td>
								<p class="onoff"><input type="checkbox" value="1" id="checkboxid5"><label for="checkboxid5"></label></p>
							</td>
							<td>Admin</td>
							<td>
							<p class="action_01">	
								<a href="#"><i class="fa fa-trash fa-6" aria-hidden="true"></i></a>
								<a href="#"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
								<a href="#"><i class="fa fa-inbox fa-6" aria-hidden="true"></i></a>
							</p>
							</td>
						</tr>
						
						<tr>
							<td class="user_imgs"><img src="<?php echo base_url();?>assets/images/avatar-4.jpg" alt="img"></td>
							<td>Mr client3</td>
							<td>admin</td>
							<td>
								<p class="onoff"><input type="checkbox" value="1" id="checkboxid6"><label for="checkboxid6"></label></p>
							</td>
							<td>Admin</td>
							<td>
							<p class="action_01">	
								<a href="#"><i class="fa fa-trash fa-6" aria-hidden="true"></i></a>
								<a href="#"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
								<a href="#"><i class="fa fa-inbox fa-6" aria-hidden="true"></i></a>
							</p>
							</td>
						</tr>
					</tbody>		
							</table>
					</div>	
		
		
		</div> <!-- new_user -->
			
		
	
	</div>
	

</div> 

<!-- admin close -->

                                                </div>

                                                <!-- Register your self card end -->

                                            </div>

                                        </div>

                                    </div>

                                    <!-- Page body end -->

                                </div>

                            </div>

                            <!-- Main-body end -->



                            <div id="styleSelector">



                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>







    <!-- Warning Section Starts -->

    <!-- Older IE warning message -->

    <!--[if lt IE 10]>

<div class="ie-warning">

    <h1>Warning!!</h1>

    <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>

    <div class="iew-container">

        <ul class="iew-download">

            <li>

                <a href="http://www.google.com/chrome/">

                    <img src="assets/images/browser/chrome.png" alt="Chrome">

                    <div>Chrome</div>

                </a>

            </li>

            <li>

                <a href="https://www.mozilla.org/en-US/firefox/new/">

                    <img src="assets/images/browser/firefox.png" alt="Firefox">

                    <div>Firefox</div>

                </a>

            </li>

            <li>

                <a href="http://www.opera.com">

                    <img src="assets/images/browser/opera.png" alt="Opera">

                    <div>Opera</div>

                </a>

            </li>

            <li>

                <a href="https://www.apple.com/safari/">

                    <img src="assets/images/browser/safari.png" alt="Safari">

                    <div>Safari</div>

                </a>

            </li>

            <li>

                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">

                    <img src="assets/images/browser/ie.png" alt="">

                    <div>IE (9 & above)</div>

                </a>

            </li>

        </ul>

    </div>

    <p>Sorry for the inconvenience!</p>

</div>

<![endif]-->

    <!-- Warning Section Ends -->

     <script type="text/javascript" src="<?php echo base_url();?>bower_components/switchery/js/switchery.min.js"></script> 
<?php $this->load->view('includes/footer');?>
 
<!-- <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="http://code.jquery.com/jquery-migrate-3.0.0.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<script type="text/javascript"  src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>

 <!--<script src="//code.jquery.com/jquery-1.12.4.js"></script>-->
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

    <script>



    $(document).ready(function() {
    $('#example').DataTable( {
        initComplete: function () {
            this.api().columns(5).every( function () {
                var column = this;

                 // Get the header name for this column
    /*var headerName = 'Status';
    // generate the id name for the element to append to.
    var appendHere = '#' + headerName + '-select-filter';*/

                var select = $('<select><option value="">All</option></select>')
                    .appendTo( $('#select-filter').empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    } );
} );

   </script>
   