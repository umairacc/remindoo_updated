<?php $this->load->view('includes/header');?>
<style>
  .floatleft {
  float:left;
  }
  .floatright {
  float:right;
  }
  #next{
  background-color: #4680FF;
  text-align: center;
  width: 75px;
  border-radius: 3px;
  margin-left: 5px;
  color: white;
  font-size: bold;
  font-weight: 300;
  }
  .prv{
  background-color: #4680FF;
  text-align: center;
  width: 75px;
  border-radius: 3px;
  margin-right: 5px;
  color: white;
  font-size: bold;
  font-weight: 300;
  }
</style>
<div class="pcoded-content">
  <div class="pcoded-inner-content">
    <!-- Main-body start -->
    <div class="main-body">
      <div class="page-wrapper">
        <!-- Page body start -->
        <div class="page-body">
          <div class="row">
            <div class="col-sm-12">
              <!-- Register your self card start -->
              <div class="card button-view">
                <!-- admin start-->
                <?php 
                  /*echo "<pre>";
                  print_r($client);echo "</pre>";*/ ?>
                <div class="alert alert-success" style="display:none;"
                  ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Your client was successfully.</div>
                <div class="alert alert-danger" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Please fill the required fields.</div>
                <?php 
                  $error = $this->session->userdata('error');
                  if($error){ echo $error; }
                  $success = $this->session->userdata('success');
                  if($success){ echo $success; }
                  ?>

                  <!-- bule buttons-->

                  	<div class="col-xs-12 blue-buttons">

                  		<a href="<?php echo base_url()?>client/add_client"><button>add client manually</button></a>
                  		<a href="<?php echo  base_url()?>client/addnewclient"><button>add client via companies house</button></a>
                  		<a href="<?php echo  base_url()?>user/import"><button>add client via import</button></a>

                  	</div>

                  <!-- bule buttons-->

                <input type="hidden" id="userurl" name="userurl" value="<?php echo base_url() ?>Client/register_user_exists">
                <div class="modal fade all_layout_modal" id="default-Modal" tabindex="-1" role="dialog">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title">Search Companines House</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <div class="input-group input-group-button input-group-primary modal-search">
                          <input type="text" class="form-control" placeholder="Search here..." id="searchCompany">
                          <button class="btn btn-primary input-group-addon" id="basic-addon1">
                          <i class="ti-search" aria-hidden="true"></i>
                          </button>
                        </div>
                        <div class="british_gas1 trading_limited" id="searchresult">
                        </div>
                        <div class="british_view" style="display:none" id="companyprofile">
                          <!-- <div class="br_company_profile">
                            <h3>british gas trading limited</h3>
                            <div class="company_numbers">
                            <span>Company Number</span>    
                            <p>03078711</p>
                            </div>
                            <div class="company_numbers">
                            <span>Company Status</span>    
                            <p>Active</p>
                            </div>    
                            <div class="company_numbers">
                            <span>Incorporated Date</span>    
                            <p>06/07/1995</p>
                            </div>
                            <div class="company_numbers">
                            <span>Accounts Reference Date</span>    
                            <p>31/12</p>
                            </div>
                            <div class="company_numbers">
                            <span>Charges</span>    
                            <p><strong>1</strong></p>
                            </div>
                            <div class="company_numbers">
                            <span>Current Directors</span>    
                            <p>R.O.Y. International PO Box 13056 ISL-61130 TEL-AVIV ISRAEL</p>
                            </div>
                            <div class="company_numbers">
                            <span>Registered Directors</span>    
                            <p>The Israel Philatelic Service 12 Shderot Yerushalayim 68021 Tel Aviv - Yafo ISRAEL</p>
                            </div>
                            <div class="company_chooses">
                            <a href="#">select company</a>
                            <a href="#">View on Companies House</a>
                            </div>
                            </div> -->
                        </div>
                        <div class="main_contacts" id="selectcompany" style="display:none">
                          <h3>british gas trading limited</h3>
                          <div class="trading_tables">
                            <div class="trading_rate">
                              <p>
                                <span><strong>BARBARO,Gab</strong></span>
                                <span>Born 1971</span>
                                <span><a href="#">Use as Main Contact</a></span>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- modalbody -->
                    </div>
                  </div>
                </div>
                <!-- modal-close -->
                <!-- admin close-->
              </div>
            </div>
          </div>
        </div>
        <!-- Page body end -->
      </div>
    </div>
    <!-- Main-body end -->
    <div id="styleSelector">
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
<!-- ajax loader -->
<div class="LoadingImage" ></div>
<style>
  .LoadingImage {
  display : none;
  position : fixed;
  z-index: 100;
  background-image : url('<?php echo site_url()?>assets/images/ajax-loader.gif');
  background-color:#666;
  opacity : 0.4;
  background-repeat : no-repeat;
  background-position : center;
  left : 0;
  bottom : 0;
  right : 0;
  top : 0;
  }
</style>
<!-- ajax loader end-->
<?php //$this->load->view('includes/session_timeout');?>
<!-- Warning Section Starts -->
<!-- Older IE warning message -->
<!--[if lt IE 10]>
<div class="ie-warning">
  <h1>Warning!!</h1>
  <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
  <div class="iew-container">
    <ul class="iew-download">
      <li>
        <a href="http://www.google.com/chrome/">
          <img src="assets/images/browser/chrome.png" alt="Chrome">
          <div>Chrome</div>
        </a>
      </li>
      <li>
        <a href="https://www.mozilla.org/en-US/firefox/new/">
          <img src="assets/images/browser/firefox.png" alt="Firefox">
          <div>Firefox</div>
        </a>
      </li>
      <li>
        <a href="http://www.opera.com">
          <img src="assets/images/browser/opera.png" alt="Opera">
          <div>Opera</div>
        </a>
      </li>
      <li>
        <a href="https://www.apple.com/safari/">
          <img src="assets/images/browser/safari.png" alt="Safari">
          <div>Safari</div>
        </a>
      </li>
      <li>
        <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
          <img src="assets/images/browser/ie.png" alt="">
          <div>IE (9 & above)</div>
        </a>
      </li>
    </ul>
  </div>
  <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->
<!-- Warning Section Ends -->
<!-- Required Jquery -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/popper.js/js/popper.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap/js/bootstrap.min.js"></script>
<!-- j-pro js -->
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.j-pro.js"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<!-- modernizr js -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/switchery/js/switchery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/modernizr/js/css-scrollbars.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Bootstrap date-time-picker js -->
<!--     <script type="text/javascript" src="assets/pages/advance-elements/moment-with-locales.min.js"></script>
  <script type="text/javascript" src="bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
  <script type="text/javascript" src="assets/pages/advance-elements/bootstrap-datetimepicker.min.js"></script>
  <script type="text/javascript" src="bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
  <script type="text/javascript" src="bower_components/datedropper/js/datedropper.min.js"></script> -->
<!-- Custom js -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/advance-elements/custom-picker.js"></script>
<script src="<?php echo base_url();?>assets/js/pcoded.min.js"></script>
<script src="<?php echo base_url();?>assets/js/demo-12.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/common_script.js"></script>
<script>
  $( document ).ready(function() {
  
      var date = $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
  
      // Multiple swithces
      var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));
  
      elem.forEach(function(html) {
          var switchery = new Switchery(html, {
              color: '#1abc9c',
              jackColor: '#fff',
              size: 'small'
          });
      });
  
      $('#accordion_close').on('click', function(){
              $('#accordion').slideToggle(300);
              $(this).toggleClass('accordion_down');
      });
  });
</script>
<script>
  /* $( "#searchCompany" ).autocomplete({
         source: function(request, response) {
             //console.info(request, 'request');
             //console.info(response, 'response');
  
             $.ajax({
                 //q: request.term,
                 url: "<?=site_url('client/SearchCompany')?>",
                 data: { term: $("#searchCompany").val()},
                 dataType: "json",
                 type: "POST",
                 success: function(data) {
                     //alert(JSON.stringify(data.searchrec));
                     //add(data.searchrec);
                     //console.log(data);
                     $(".ui-autocomplete").css('display','block');
                     response(data.searchrec);
                 }
             
             });
         },
         minLength: 2
     });*/
  $("#searchCompany").keyup(function(){
      var currentRequest = null;    
      var term = $(this).val();
      $("#searchresult").show();
      $('#selectcompany').hide();
       $(".LoadingImage").show();
      currentRequest = $.ajax({
         url: '<?php echo base_url();?>client/SearchCompany/',
         type: 'post',
         data: { 'term':term },
         beforeSend : function()    {           
         if(currentRequest != null) {
             currentRequest.abort();
         }
     },
         success: function( data ){
             $("#searchresult").html(data);
              $(".LoadingImage").hide();
             },
             error: function( errorThrown ){
                 console.log( errorThrown );
             }
         });
    });
  var xhr = null;
  function getCompanyRec(companyNo)
  {
     $(".LoadingImage").show();
      if( xhr != null ) {
                 xhr.abort();
                 xhr = null;
         }
      
     xhr = $.ajax({
         url: '<?php echo base_url();?>client/CompanyDetails/',
         type: 'post',
         data: { 'companyNo':companyNo },
         success: function( data ){
             //alert(data);
             $("#searchresult").hide();
             $('#companyprofile').show();
             $("#companyprofile").html(data);
             $(".LoadingImage").hide();
             
             },
             error: function( errorThrown ){
                 console.log( errorThrown );
             }
         });}
  
  
     function getCompanyView(companyNo)
  {
     $(".LoadingImage").show();
      if( xhr != null ) {
                 xhr.abort();
                 xhr = null;
         }
      
     xhr = $.ajax({
         url: '<?php echo base_url();?>client/selectcompany/',
         type: 'post',
         dataType: 'JSON',
         data: { 'companyNo':companyNo },
         success: function( data ){
             //alert(data);
             $("#searchresult").hide();
             $('#companyprofile').hide();
             $('#selectcompany').show();
             $("#selectcompany").html(data.html);
  // append form field value
  $("#company_house").val('1');
  $("#company_name").val(data.company_name);
  $("#company_name1").val(data.company_name);
  $("#company_number").val(data.company_number);
  $("#company_url").val(data.company_url);
  $("#company_url_anchor").attr("href",data.company_url);
  $("#officers_url").val(data.officers_url);
  $("#officers_url_anchor").attr("href",data.officers_url);
  $("#company_status").val(data.company_status);
  $("#company_type").val(data.company_type);
  $("#company_sic").append(data.company_sic);
  $("#sic_codes").val(data.sic_codes);
  
  $("#register_address").val(data.address1+"\n"+data.address2+"\n"+data.locality+"\n"+data.postal);
  $("#allocation_holder").val(data.allocation_holder);
  $("#date_of_creation").val(data.date_of_creation);
  $("#period_end_on").val(data.period_end_on);
  $("#next_made_up_to").val(data.next_made_up_to);
  $("#next_due").val(data.next_due);
  $("#confirm_next_made_up_to").val(data.confirm_next_made_up_to);
  $("#confirm_next_due").val(data.confirm_next_due);
  
  $("#tradingas").val(data.company_name);
  $("#business_details_nature_of_business").val(data.nature_business);
  
  $(".LoadingImage").hide();
             
             },
             error: function( errorThrown ){
                 console.log( errorThrown );
             }
         });}
  
     function getmaincontact(companyNo,appointments)
  {
     $(".LoadingImage").show();
      if( xhr != null ) {
                 xhr.abort();
                 xhr = null;
         }
      
     xhr = $.ajax({
         url: '<?php echo base_url();?>client/maincontact/',
         type: 'post',
         dataType: 'JSON',
         data: { 'companyNo':companyNo,'appointments': appointments },
         success: function( data ){
           
             //$("#default-Modal,.modal-backdrop.show").hide();
                $('#default-Modal').modal('hide');
  
  // append form field value
  
  $("#first_name").val(data.first_name);
  $("#middle_name").val(data.middle_name);
  $("#last_name").val(data.last_name);
  $(".LoadingImage").hide();
  
             
             },
             error: function( errorThrown ){
                 console.log( errorThrown );
             }
         });}
</script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
      jQuery.validator.addMethod("noSpace", function(value, element) { 
    return value.indexOf(" ") < 0 && value != ""; 
  }, "No space please and don't leave it empty");
  $("#insert_form").validate({
  
         ignore: false,
     // errorClass: "error text-warning",
      //validClass: "success text-success",
      /*highlight: function (element, errorClass) {
          //alert('em');
         // $(element).fadeOut(function () {
             // $(element).fadeIn();
          //});
      },*/
                          rules: {
                          company_name: {required: true},
                          company_number: {required: true},
                          company_status: {required: true},
                          name:{
  required: function(element) {
  
      if ($("#name").val().length > 0) {
         return false;
      }
      else {
      return true;
      }
    },
  
  
  accept: "[a-zA-Z]+", noSpace: true,
  /*remote: {
  url: "<?php echo base_url() ?>Client/register_user_exists",
  type: "post",
  data: {
  name: function(element){ return $("#name").val(); }
  }
  }*/
   },
                          email_id:{
          required: function(element) {
  
      if ($("#email_id").val().length > 0) {
         return false;
      }
      else {
      return true;
      }
    }, 
  
    email: true,
  
  /*remote: {
  url: "<?php echo base_url() ?>Client/register_email_exists",
  type: "post",
  data: {
  email: function(){ return $("#email_id").val(); }
  }
  }*/
  },
                          /*profile_image: {required: true, accept: "jpg|jpeg|png|gif"},*/
                           profile_image: {
      required: function(element) {
  
      if ($("#image_name").val().length > 0) {
         return false;
      }
      else {
      return false;
      }
    },
    accept: "jpg|jpeg|png|gif"
  },
                          gender:{required: true},
                          packages:{required: true},
                          password:{required: true,minlength : 5},
                          confirm_password : { minlength : 5,equalTo : "#password"},
                          },
                          errorElement: "span" , 
                          errorClass: "field-error",                             
                           messages: {
                            company_name: "Give company name",
                            company_number: "Give company number",
                            company_status: "Give company status",
                            name: { required: "Please enter your User Name",
   accept: "Please enter Alphabets Character only",
    remote: "username already taken. please try another." },
                            email_id:{
      email:"Please enter a valid email address", 
      required:"Please enter a email address",
      remote: "Email already used. Log in to your existing account."
  
  },
  profile_image: {required: 'Required!', accept: 'Not an image!'},
  gender:"Select gender",
  packages:"Enter a package",
  password: {required: "Please enter your password "},
  
                           },
  
                          
                          submitHandler: function(form) {
                              var formData = new FormData($("#insert_form")[0]);
                              $(".LoadingImage").show();
                              $.ajax({
                                  url: '<?php echo base_url();?>client/insert_client/',
                                  dataType : 'json',
                                  type : 'POST',
                                  data : formData,
                                  contentType : false,
                                  processData : false,
                                  success: function(data) {
                                      if(data == '1'){
                                          $('#insert_form')[0].reset();
                                          $('.alert-success').show();
                                          $('.alert-danger').hide();
                                      }
                                      else if(data == '0'){
                                         // alert('failed');
                                          $('.alert-danger').show();
                                          $('.alert-success').hide();
                                      }
                                  $(".LoadingImage").hide();
                                  },
                                  error: function() {
                                       $(".LoadingImage").hide();
                                      alert('faileddd');}
                              });
  
                              return false;
                          } ,
                           /* invalidHandler: function(e,validator) {
          for (var i=0;i<validator.errorList.length;i++){   
              $(validator.errorList[i].element).parents('.panel-collapse.collapse').collapse('show');
          }
      }*/
       invalidHandler: function(e, validator) {
             if(validator.errorList.length)
          $('#tabs a[href="#' + jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
  
          }
  
                      });
  
  });
</script>
<script type="text/javascript">
  $(document).ready(function(){
   $('.js-small').change(function() {
      
    $(this).closest('div').find('.field').prop('disabled', !$(this).is(':checked'));
  });
  });
  
  var count=0;
  
  $('li').click(function(){
  
     var count=$(this).val();
     $("#sanker").val(count);
   
     if(count==0){
      $("#previous").hide();
      $("#next").show();
     }
     if(count==11){
      $("#next").hide();
      $("#previous").show();
     }
     if(count!=0 && count!=11){
      $("#next").show();
      $("#previous").show();
     }
  
  });
  
  if(count==0){
       $("#previous").hide();
  }
  
  if(count==11){
      $("#next").hide();
  }
  
  
  $('#next').click(function(){
      count= $("#sanker").val();
      count ++;
      var id='.it'+(count-1);
     
      
      if(count!=0){
           $("#previous").show();
      }
      if(count!=''){
          $(id).next('li').find('a').trigger('click');
      }
  
      if(count==11){
          $('.it10').next('li').find('a').trigger('click');
           $("#next").hide();
      }
  
   });
  
  
  $('#previous').click(function(){
      count= $("#sanker").val();
      count--;
      var id='.it'+(count+1);
       if(count!=''){
          $(id).prev('li').find('a').trigger('click');
  
      }
  
      if(count==10){
          $('.it11').prev('li').find('a').trigger('click');
           $("#next").show();
      }
  
      if(count==0){
          $('.it1').prev('li').find('a').trigger('click');
          $("#previous").hide();
         
      }
  
  });
</script>
<script type="text/javascript">
  $(document).ready(function(){
  $(".fields").blur(function(){
  var currentRequest = null; 
       var term = $(this).val();
      
       currentRequest = $.ajax({
          url: '<?php echo base_url();?>client/addmanual_client/',
          type: 'post',
          data: $("form").serialize(),
          beforeSend : function()    {           
          if(currentRequest != null) {
              currentRequest.abort();
          }
      },
          success: function( data ){
              $("#user_id").val(data);
               //$(".LoadingImage").hide();
              },
              error: function( errorThrown ){
                  console.log( errorThrown );
              }
          });
     });
     });
</script>
</body>
</html>