<?php 
   $j =1;
      //foreach ($rec as $key => $value) {
   $contact_names = $this->Common_mdl->numToOrdinalWord($cnt).' Contact ';
   
   $value['title'] = $title;
   $value['first_name'] = $first_name;
   $value['surname'] = $surname;
   $value['preferred_name'] = $preferred_name;
   $value['address_line1'] = $address_line1;
   $value['address_line2'] = $address_line2;
   $value['premises'] = $premises;
   $value['region'] = $region;
   $value['country'] = $country;
   $value['locality'] = $locality;
   $value['post_code'] = $post_code;
   $value['nationality'] = $nationality;
   $value['occupation'] = $occupation;
   $value['appointed_on'] = $appointed_on;
   $value['country_of_residence'] = $country_of_residence;
   $value['birth'] = $birth;
   $cnt;
   //$value['created_date'] = time();
   
       ?>
<!--     <form name="contact_exist" id="contact_exist<?php echo $formCount ?>"> -->
 <div class="space-required  new_update1">
<div class="update-data01 client-info-circle1 floating_set col-xs-12 exist-append make_a_primary common_div_remove-<?php echo $cnt; ?>" id="common_div_remove-<?php echo $cnt; ?> ">
<div class="accordion-panel remove contactcc">
   <div class="accordion-heading" role="tab" id="headingOne">
      	<div class="main-contact append_contact">
         <span class="accordion-msg h4"><?php echo $value['first_name'].' '.$value['surname'];?></span>
         <!-- <a href="#" data-toggle="modal" data-target="#modalcontact"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true"></i></a> -->
           <input type="hidden" name="make_primary_loop[]" id="make_primary_loop" value="<?php echo $cnt; ?>">
         <div class="dead-primary1 for_row_count-<?php echo $cnt; ?>">
                                                         <div class="radio radio-inline">
                                                            <label>
                                                             <input type="radio" name="make_primary" id="make_primary<?php echo $cnt;?>" value="<?php echo $cnt;?>"  >
                                                              <i class="helper" style="display: none;"></i>
                                                            </label>
                                                            <a href="javascript:void(0)" class="make_primary_section" data-id="make_primary<?php echo $cnt;?>" ><span>Make a primary</span></a>
                                                         </div>
                                                           
         <div class="btn btn-danger remove for_remove-1"><a href="javascript:void(0)" class="contact_remove" id="remove-<?php echo $cnt; ?>" data-toggle="modal" data-target="#modalcontact">Remove</a></div>
         </div>
      	</div>
   </div>
   <div id="collapse" class="panel-collapse">
      <div class="basic-info-client1">
         <div class="primary-info common-spent01 ">
             
               <span class="primary-inner">
                  <label>title</label>
                  <!-- <input type="text" class="text-info title" name="title[]" id="title" value="<?php if(isset($value['title']) && ($value['title']!='') ){ echo $value['title'];}?>"> -->
                  <select class="text-info title"  name="title" id="title">
                     <option value="Mr" <?php if(isset($value['title']) && $value['title']=='Mr') {?> selected="selected"<?php } ?>>Mr</option>
                     <option value="Mrs" <?php if(isset($value['title']) && $value['title']=='Mrs') {?> selected="selected"<?php } ?>>Mrs</option>
                     <option value="Miss" <?php if(isset($value['title']) && $value['title']=='Miss') {?> selected="selected"<?php } ?>>Miss</option>
                     <option value="Dr" <?php if(isset($value['title']) && $value['title']=='Dr') {?> selected="selected"<?php } ?>>Dr</option>
                     <option value="Ms" <?php if(isset($value['title']) && $value['title']=='Ms') {?> selected="selected"<?php } ?>>Ms</option>
                     <option value="Prof" <?php if(isset($value['title']) && $value['title']=='Prof') {?> selected="selected"<?php } ?>>Prof</option>
                  </select>
               </span>
               <span class="primary-inner">
               <label>first name</label>
               <input type="text" class="text-info" name="first_name[]" id="first_name" value="<?php if(isset($value['first_name']) && ($value['first_name']!='') ){ echo $value['first_name'];}?>">
               </span>
               <span class="primary-inner">
               <label>middle name</label>
               <input type="text" class="text-info" name="middle_name[]" id="middle_name" value="<?php if(isset($value['middle_name']) && ($value['middle_name']!='') ){ echo $value['middle_name'];}?>">
               </span>
               <span class="primary-inner">
               <label>surname</label>
               <input type="text" class="text-info" name="surname[]" id="surname" value="<?php if(isset($value['surname']) && ($value['surname']!='') ){ echo $value['surname'];}?>">
               </span>
               <span class="primary-inner">
               <label>prefered name</label>
               <input type="text" class="text-info" name="preferred_name[]" id="preferred_name" value="<?php if(isset($value['preferred_name']) && ($value['preferred_name']!='') ){ echo $value['preferred_name'];}?>">
               </span>
               <span class="primary-inner">
               <label>mobile</label>
               <input type="text" class="text-info" name="mobile[]" id="mobile" value="<?php if(isset($value['mobile']) && ($value['mobile']!='') ){ echo $value['mobile'];}?>">
               </span>
               <span class="primary-inner">
               <label>main E-Mail address</label>
               <input type="text" class="text-info" name="main_email[]" id="email_id" value="<?php if(isset($value['work_email']) && ($value['work_email']!='') ){ echo $value['work_email'];}?>">
               </span>
               <span class="primary-inner">
               <label>Nationality</label>
               <input type="text" class="text-info" name="nationality[]" id="nationality" value="<?php if(isset($value['nationality']) && ($value['nationality']!='') ){ echo $value['nationality'];}?>">
               </span>
               <span class="primary-inner">
               <label>PSC</label>
               <input type="text" class="text-info" name="psc[]" id="psc" value="<?php if(isset($value['psc']) && ($value['psc']!='') ){ echo $value['psc'];}?>">
               </span>
               <span class="primary-inner">
                  <label>shareholder</label>
                  <select name="shareholder" id="shareholder">
                     <option value="yes">Yes</option>
                     <option value="no">No</option>
                  </select>
               </span>
               <span class="primary-inner">
               <label>national insurance number</label>
               <input type="text" class="text-info" name="ni_number[]" id="ni_number" value="<?php if(isset($value['ni_number']) && ($value['ni_number']!='') ){ echo $value['ni_number'];}?>">
               </span>
               <span class="primary-inner">
               <label>Country Of Residence</label>
               <input type="text" name="country_of_residence[]" id="country_of_residence" class="text-info" value="<?php if(isset($value['country_of_residence']) && ($value['country_of_residence']!='') ){ echo $value['country_of_residence'];}?>">
               </span>
             
            
               <span class="primary-inner contact_type">
                  <label>contact type</label>
                  <select name="contact_type" id="contact_type" class="othercus">
                     <option value="Director">Director</option>
                     <option value="Director/Shareholder">Director/Shareholder</option>
                     <option value="Shareholder">Shareholder</option>
                     <option value="Accountant">Accountant</option>
                     <option value="Bookkeeper">Bookkeeper</option>
                     <option value="Other">Other(Custom)</option>
                  </select>
               </span>
               <span class="primary-inner" id="others_customs" style="display:none">
               <label>Other(Custom)</label>
               <input type="text" class="text-info" name="other_custom[]" id="other_custom"  value="">
               </span>
               <span class="primary-inner">
               <label>address line1</label>
               <input type="text" class="text-info" name="address_line1[]" id="address_line1" value="<?php if(isset($value['address_line1']) && ($value['address_line1']!='') ){ echo $value['address_line1'];}?>">
               </span>
               <span class="primary-inner">
               <label>address line2</label>
               <input type="text" class="text-info" name="address_line2[]" id="address_line2" value="<?php if(isset($value['address_line2']) && ($value['address_line2']!='') ){ echo $value['address_line2'];}?>">
               </span>
               <span class="primary-inner">
               <label>town/city</label>
               <input type="text" class="text-info" name="town_city[]" id="town_city" value="<?php if(isset($value['town_city']) && ($value['town_city']!='') ){ echo $value['town_city'];}?>">
               </span>
               <span class="primary-inner">
               <label>post code</label>
               <input type="text" class="text-info" name="post_code[]" id="post_code" value="<?php if(isset($value['post_code']) && ($value['post_code']!='') ){ echo $value['post_code'];}?>">
               </span>
			   
               <span class="primary-inner">
			   <div class="update-primary">
                  <label>landline</label>
                  <div class="land-spaces">  <input type="text" class="text-info" name="landline<?php echo $incre;?>[]" id="landline" value="<?php if(isset($value['landline']) && ($value['landline']!='') ){ echo $value['landline'];}?>"> </div></div>
              
               <span class="success pack_add_row_wrpr_landline">
               <button type="button" class="btn btn-primary yk_pack_addrow_landline" data-id="<?php echo $incre;?>">Add Landline</button></span>
			    </span>
				
               <span class="primary-inner">
			   <div class="update-primary">
               <label>work email</label>
               <input type="text" class="text-info" name="work_email<?php echo $incre;?>[]" id="work_email" value="<?php if(isset($value['work_email']) && ($value['work_email']!='') ){ echo $value['work_email'];}?>">
               </div>
               <span class="success pack_add_row_wrpr_email">
               <button type="button" class="btn btn-primary yk_pack_addrow_email" data-id="<?php echo $incre;?>">Add Email</button></span>
			   </span>
			   
               <span class="primary-inner date_birth">
                  <label>date of birth</label>
                  <div class="picker-appoint"><span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span><input type="text" placeholder="dd-mm-yyyy" class="text-info date_picker_dob fields" name="date_of_birth[]" id="date_of_birth" value="<?php if(isset($value['birth']) && ($value['birth']!='') ){ echo date('d-m-Y',strtotime($value['birth']));}?>"></div>
               </span>
               <span class="primary-inner">
               <label>nature of control</label>
               <input type="text" class="text-info" name="nature_of_control[]" id="nature_of_control" value="<?php if(isset($value['nature_of_control']) && ($value['nature_of_control']!='') ){ echo $value['nature_of_control'];}?>">
               </span>
               <span class="primary-inner">
                  <label>marital status</label>
                  <select name="marital_status" id="marital_status">
                     <option value="Single">Single</option>
                     <option value="Living_together">Living together</option>
                     <option value="Engaged">Engaged</option>
                     <option value="Married">Married</option>
                     <option value="Civil_partner">Civil partner</option>
                     <option value="Separated">Separated</option>
                     <option value="Divorced">Divorced</option>
                     <option value="Widowed">Widowed</option>
                  </select>
               </span>
               <span class="primary-inner">
               <label>utr number</label>
               <input type="text" class="text-info" name="utr_number[]" id="utr_number" value="<?php if(isset($value['utr_number']) && ($value['utr_number']!='') ){ echo $value['utr_number'];}?>">
               </span>
               <span class="primary-inner">
               <label>Occupation</label>
               <input type="text" class="text-info" id="occupation"  name="occupation[]" value="<?php if(isset($value['occupation']) && ($value['occupation']!='') ){ echo $value['occupation'];}?>">
               </span>
               <span class="primary-inner date_birth">
                  <label>Appointed On</label>
                  <div class="picker-appoint"><span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                     <input type="text" class="text-info" placeholder="dd-mm-yyyy" id="appointed_on"  name="appointed_on[]" value="<?php if(isset($value['appointed_on']) && ($value['appointed_on']!='') ){ echo $value['appointed_on'];}?>">
                  </div>
               </span>
             
        <!--     <input type="hidden" name="make_primary[]" id="make_primary" value="<?php echo $make_primary;?>"> -->
            <!-- 	<div class="sav-btn">
               <span id="succ" class="succ" style="color:green; display:none;">Contact Updated successfully!!!</span>
               <input type="hidden" name="client_id" id="client_id" value="">
               <input type="button" value="save" class="contactexist" onClick = "formSubmit(<?php echo $formCount ?>)" id="perferred_name<?php  ?>" data-id="">
               </div> -->
         </div>
      </div>
   </div>
   <?php $j++; // } ?>
</div>
</div>
</div>
<!--  </form> -->