<?php

// echo ',,';

$column_helper = [
  'crm_company_name'    =>  'ClientColumn_Client',
  'crm_legal_form'      =>  'ClientColumn_Type',
  'crm_company_type'    =>  'ClientColumn_Type',
  'crm_company_status'  =>  'ClientColumn_Status',
  'contact_landline'    =>  'ClientColumn_landlines',
  'contact_work_email'  =>  'ClientColumn_work_emails',
  'proof_attach_file'   =>  'ClientColumn_addBaseUrl',
  'client_assignees'    =>  'ClientColumn_assignees',
  'CreatedTime'         =>  'ClientColumn_created_date',
  'crm_confirmation_statement_due_date' => 'ClientColumn_check_CS_ch_overdue',
  'crm_ch_accounts_next_due'            => 'ClientColumn_check_Acc_ch_overdue'

  /*'staff_manager'=>'ClientColumn_staff_manager',
        'staff_managed' => 'ClientColumn_staff_managed',
        'team' => 'ClientColumn_team',
        'team_allocation' => 'ClientColumn_team_allocation',
        'department'=>'ClientColumn_department',
        'department_allocation'=>'ClientColumn_department_allocation',
        'member_manager'=> 'ClientColumn_member_manager',
        'member_managed' => 'ClientColumn_member_managed',
        'crm_company_status' => 'ClientColumn_client_AddtedFrom',*/
];

$FirmId = $_SESSION['firmId'];
$status = ['Inactive', 'Active', 'Inactive', 'Frozen', 'Draft', 'Archive'];

?>


<div class="filter-data for-reportdash">
  <div class="filter-head">
    <div class="filter-head-client">
      <h4>FILTERED DATA:</h4>
      <div id="container2" class="panel-body box-container">
        <div class="btn btn-default box-item">
          LTD
          <span class="remove" data-targetth=".crm_legal_form_TH" data-index="0">x</span>
        </div>
      </div>
    </div>
    <div>
      <button class="btn btn-danger f-right filter-clear" id="clear_container">clear</button>
    </div>
  </div>
</div>

<table class="table client_table1 text-center display nowrap printableArea" id="client_table" cellspacing="0" width="100%">
  <thead>
    <tr>
      <th class="select_row_TH Exc-colvis">
        <label class="custom_checkbox1">
          <input type="checkbox" id="select_all_client"><i></i>
        </label>
      </th>
      <?php
      foreach ($column_setting as $key => $colval) {
      ?>
        <th class="<?php echo $colval['field_propriety'] . '_TH'; ?> hasFilter" data-orderNo="<?php echo $colval['list_page_order']; ?>" data-table="<?= $colval['table'] ?>" data-column="<?= $colval['column'] ?>">
          <?php echo $colval['label'] ?>
          <img class="themicond" src="<?php echo base_url(); ?>assets/images/filter.png" alt="themeicon" />
          <div class="sortMask"></div>
        </th>
      <?php
      }
      ?>
      <th class="action_TH">
        Action
      </th>
    </tr>
  </thead>

  <tbody>

  </tbody>

</table>
<input type="hidden" class="rows_selected" id="select_client_count">