<?php

$this->load->view('includes/header'); 

$uri_seg = $this->uri->segment(3);
$MODE = $this->uri->segment(2)

?>
<style>
	div#component {
    background: #fff;
    padding: 15px 0;
    border-radius: 5px;
    box-shadow: 0px 0px 4px #ddd;
}
</style>
<div class="modal-alertsuccess alert alert-success" style="display:none;">
	<div class="newupdate_alert">
		<a href="javascript:;" class="close alert_close"  aria-label="close">x</a>
		 <div class="pop-realted1">
		    <div class="position-alert1 sample_check">
		       YOU HAVE SUCCESSFULLY UPDATED CLIENT 
		       <div class="alertclsnew">
		          <a href="<?php echo base_url('Client/addnewclient'); ?>"> New Client</a>
		          <a href="<?php echo base_url(); ?>user"> Go Back</a>
		       </div>
		    </div>
		 </div>
	</div>
</div>

<section class="client-details-view hidden-user01 floating_set card-removes pcoded-content ">
	<div class="information-tab main-body clientredesign floating_set">
        <div class="space-required">
            <div class="deadline-crm1 floating_set">
        		<!-- Module Link -->
            	<ul class="nav nav-tabs1 all_user1 md-tabs pull-left u-dashboard">
                  <?php
                    if( $_SESSION['permission']['Client_Section']['view']!=0 )
                    {

                      ?>
                       <li class="nav-item">
                          <a class="nav-link" href="<?php echo base_url().'user';?>">
                          <img class="themicon" src="<?php echo base_url();?>assets/images/tabicon1.png" alt="themeicon" />
                       All Clients</a>
                          <div class="slide"></div>
                       </li>
                    <?php 
                    }
                   ?>
                   <li class="nav-item">
                      <a class="nav-link active" href="#addnewclient">
                         <img class="themicon" src="<?php echo base_url();?>assets/images/tabicon2.png" alt="themeicon" /><?php echo (!empty($uri_seg)?'Edit Client':'New Client');?>
                       </a>
                      <div class="slide"></div>
                   </li> 
                </ul>
                <!-- End Module Link -->

                <!-- Action Buttons -->
                <div class="Footer common-clienttab pull-right">
	                <div class="change-client-bts1">
	                    <input type="submit" class="signed-change1 sv_ex" id="save_exit" value ="<?php echo (!empty($uri_seg)?'Update':'Save');?>">
	                </div>
                    <div class="divleft">
                        <button  class="signed-change2"  type="button" value="Previous Tab" text="Previous Tab" style="display: none;">Previous 
                        </button>
                    </div>
                    <div class="divright">
                        <button  class="signed-change3"  type="button" value="Next Tab"  text="Next Tab">Next
                        </button>
                    </div>
                </div>
                <!-- Action Buttons End-->
            </div>

            <div class="space-required">
	            <div class="document-center client-infom-1 floating_set">

	            	<!-- Client name OR heading -->
	               <div class="Companies_House floating_set">
	                  	<div class="pull-left">
		                  	<?php           
		                        $client_name =  "Add Client";

		                      	if(!empty($client['crm_company_name']))
		                      	{ 
		                        	$client_name = $client['crm_company_name'];
		                      	}

		                      	echo '<h2>'.$client_name.'</h2>'; 
		                    ?>
		                </div>
		            </div>
		            <!--End Client name OR heading -->

		            <!-- Compound Links -->
		            <div class="addnewclient_pages1 floating_set bg_new1">
	            	  	<ol class="CLIENT_CONTENT_TAB nav nav-tabs all_user1 md-tabs floating_set" id="tabs">
						    <li class="nav-item">
						        <a class="nav-link active" data-id="required_information" href="javascript:void(0);">Required information</a>
						    </li>                  
						    <li class="nav-item">
						       <a class="nav-link basic_deatils_id" data-id="details" href="javascript:void(0);"> Basic Details</a>
						    </li>
						    <li class="nav-item">
						       <a class="nav-link main_contacttab" data-id="contacts1" href="javascript:void(0);">Contact</a>
						    </li>
						    <li class="nav-item">
						       <a class="nav-link" data-id="service" href="javascript:void(0);"> Service</a>
						    </li>
						    <li class="nav-item">
						       <a class="nav-link" data-id="assignto" href="javascript:void(0);"> Assign to</a>
						    </li>
						    <li class="nav-item">
						       <a class="nav-link" data-id="amlchecks" href="javascript:void(0);"> AML Checks</a>
						    </li>
						    <li class="nav-item"style="display: none;" >
						       <a class="nav-link" data-id="business-det" href="javascript:void(0);"> Business Details</a>
						    </li>
						    <li class="nav-item">
						       <a class="nav-link" data-id="other" href="javascript:void(0);"> <span id="other_tab_cnt"></span><span id="old_cnt"></span> Other</a>
						    </li>
						    <li class="nav-item">
						       <a class="nav-link" data-id="referral" href="javascript:void(0);">Referral</a>
						    </li>
						    <li class="nav-item" id="import_tab_li" style="<?php //echo $import_tab_on; ?>">
						       <a class="nav-link" data-id="import_tab" href="javascript:void(0);"> Important Information</a>
						    </li>
						<?php if( $MODE == 'view') { ?>
						    <li class="nav-item">
								<a class="nav-link" href="javascript:void()" data-id="task"> Task</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="javascript:void()" data-id="leads"> Leads</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="javascript:void()" data-id="proposals"> Proposals</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="javascript:void()" data-id="invoice"> Invoice</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="javascript:void()" data-id="reminder"> Reminder</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="javascript:void()" data-id="newtimelineservices"> Timeline Services</a>
							</li>
						<?php } ?>
					  </ol>
					</div>
					<!--End Compound Links -->

					<!-- Compound Content -->
					<div class="newupdate_design">
						<div class="management_section client-details-tab01 data-mang-06  floating_set realign newmanage-animation">
	            			<div class="tab-content card" id="component">

	            			</div>
	            		</div>
					</div>
					<!--End Compound Content -->


				</div>
			</div>

			<!-- date Info -->
			<div class="space-required">
			  <div class="comp_cre"> 
			    <span>Company Created November 23, 2019, 3:24</span>
			    <span>;Updated December 21, 2019, 1:44</span>
			  </div>
			</div>
			<!--End date Info -->

		</div>
	</div>
</section>



<?php $this->load->view('includes/footer');?>

<script>

	var MODE = "<?= $MODE ?>";
	var U_ID = "<?= $uri_seg ?>";

	function sort_content()
	{

		var parent = $('#component').find('div.CONTENT');
		var child = parent.children('.sort').get();
		child.sort(function(a, b) { 
		    var compA = $(a).attr('data-id');
		    var compB = $(b).attr('data-id');
		    return (parseInt(compA) < parseInt(compB)) ? -1 : (parseInt(compA) > parseInt(compB)) ? 1 : 0;
		})
		$.each(child, function(idx, itm) {
		 //  console.log(idx);
		    parent.append(itm);
		});
	}

	function set_mode_class()
	{
		if( MODE == 'view' )
		{
			$('#component').find('div.form-group').addClass('VIEW_MODE');
		}
	}

	function get_component( name )
	{ 	alert("inside"+name);
		$.ajax({
			url 		: "<?php echo base_url()?>Client_component/"+name+"/"+MODE+"/"+U_ID,
			type		: 'GET',
			cache		:  false,
			beforeSend 	: Show_LoadingImg,
			success 	: function( content ){
				Hide_LoadingImg();
				$('#component').html( content );
				sort_content();
				set_mode_class();

			}
		})
	}

	$(document).ready(function(){

		// prev click
   $('.signed-change2').click(function(){
      $('ol.CLIENT_CONTENT_TAB.nav-tabs .active').closest('li').prevAll('li:visible').eq(0).find('a').trigger('click');
   });
   // next click
   $('.signed-change3').click(function(){
      $('ol.CLIENT_CONTENT_TAB.nav-tabs .active').closest('li').nextAll("li:visible").eq(0).find('a').trigger('click');
   });

    $("ol.CLIENT_CONTENT_TAB.nav-tabs li.nav-item a.nav-link").click(function(){

	    var id = $(this).attr('data-id');
	    $(this).closest(".nav-tabs").find("a.nav-link").removeClass('active');
	    $(this).addClass('active');
	      
	    $(this).closest(".nav-tabs").find("li.nav-item").removeClass("active");
	    $(this).parent().addClass("active");      

	    $(".newupdate_design .tab-content .tab-pane").removeClass("active show");
	    $(".newupdate_design .tab-content").find(id).addClass("active show");
	      
	    var items = $(this).closest(".nav-tabs").find("li.nav-item").filter(":visible");
      

        items.removeClass("active-previous");
	    var item_len = items.length - 1;


	      for( var i=0;i<=item_len;i++)
	      {
	        if( $(items[i]).hasClass('active') )
	        {
	        	break;
	        }
	        $(items[i]).addClass("active-previous");
	      }

	      if(i==item_len)
	      {
	        $('.signed-change3').hide();
	         
	      }
	      else
	      {
	        $('.signed-change3').show();    
	         
	      }

	      if(i==0)
	      {
	        $('.signed-change2').hide();
	      }
	      else
	      {
	        $('.signed-change2').show();    
	      }
   		});

    	var active = $('ol.CLIENT_CONTENT_TAB').find('a.active').attr('data-id');
    	get_component( active );
    	

	});
</script>