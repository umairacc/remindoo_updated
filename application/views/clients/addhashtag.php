<?php 
error_reporting('0');
$this->load->view('includes/header');


?>
<style type="text/css">
 .dropdown-main input {

    position: relative;
    opacity: 1;
  }
.form-radio input {
    opacity: 1;
    position: relative;
    left: 0;
}
</style>
<div class="client-details-view hidden-user01 floating_set card-removes priceli addhashtagcls">
 <div class="right-side-proposal">
 <?php 

 if($records['id']==''){ ?>
<form name="form" action="<?php echo base_url(); ?>client/insert_hashtag" method="post">
<?php }else{ ?>
<form name="form" action="<?php echo base_url(); ?>client/update_hashtag" method="post">
<input type="hidden" name="id" id="id" value="<?php echo $records['id']; ?>">
<?php } ?>

        <div class="deadline-crm1 floating_set clienthasttagcls">
               <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                  <li class="nav-item">
                     <a class="nav-link" href="javascript:;">Client Hashtag</a>
                     <div class="slide"></div>
                  </li>
               </ul>
            </div>

            <div class="document-center client-infom-1 floating_set pricelist">

          <div class="price-tb-setting form-radio">
              <!-- <h2></h2> -->
              <div class="discount-types">
                     <div class="line-items1 currency_update">
                      <h3>Client Hashtag</h3>
                        <input type="text" name="client_hashtag" id="client_hashtag" class="" required="required">                     
                        </div>
                        <div class="pricing-radio">                      
                          <input type="submit" name="submit" class="btn btn-primary" value="update">
                        </div>
                    </div>

                                       
                    </div> 
                      <table class="table client_table1 text-center display nowrap hashtagtable" id="alltask" cellspacing="0" width="100%">
                      <thead>
                      <th>S.no</th>
                      <th>Hashtag Name</th>
                      <th>Action</th>
                      </thead>
                      <tbody>
                      <?php $i=1; foreach($client_hashtag as $hashtag){ ?>
                      <tr>
                      <td><?php echo $i; ?></td>
                      <td><?php echo $hashtag['hashtag']; ?></td>
                      <td><a href="javascript:;" id="<?php echo $hashtag['id']; ?>" class="edit_hashtag"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                      <a href="javascript:;" id="<?php echo $hashtag['id']; ?>" class="delete_hashtag"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true"></i></a></td>
                      </tr>
                      <?php $i++; } ?>
                      </tbody>

                      </table>
                </div>  <!-- 1sect -->             
               
                    </div> 

                    
                </div>
          </div><!-- right-side -->

</form>
</div>
</div>


 <div class="modal fade hashtagmodel" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">

<form name="form" action="<?php echo base_url(); ?>client/update_hashtag" method="post">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Hashtag</h4>
        </div>
        <div class="modal-body">

        <input type="hidden" name="hashtag_id" id="hashtag_id" value="">
       <label>Client Hashtag</label>
       <input type="text" name="client_hashtag" id="edit_clienthastag" value="">
        </div>
        <div class="modal-footer">
         <input type="submit" name="submit" class="btn btn-primary" value="update">
          <button type="button" class="btn btn-default close" data-dismiss="modal">Close</button>
        </div>
        </form>
      </div>
      
    </div>
  </div>

  <div class="modal fade" id="deleteModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">

<form name="form" action="<?php echo base_url(); ?>client/delete_hashtag" method="post">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Hashtag</h4>
        </div>
        <div class="modal-body">

        <input type="hidden" name="hashtag_id" id="delete_hashtag_id" value="">
        Do you Want to delete this hashtag?
            </div>
        <div class="modal-footer">
         <input type="submit" name="submit" class="btn btn-primary" value="update">
          <button type="button" class="btn btn-default close" data-dismiss="modal">Close</button>
        </div>
        </form>
      </div>
      
    </div>
  </div>

<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
<script src="<?php echo base_url();?>assets/js/mock.js"></script>
<script type="text/javascript">

 var tabletask1 =$("#alltask").dataTable({
           "iDisplayLength": 10,
        "scrollX": true,
        "dom": '<"toolbar-table">lfrtip',
        responsive: true
        });

 $(".edit_hashtag").click(function(){
var id=$(this).attr('id');
$.ajax({
    url: '<?php echo base_url(); ?>/client/hashtag_select',
    type : 'POST',
    data : {'id':id},                    
      beforeSend: function() {
        $(".LoadingImage").show();
      },
      success: function(data) {
          var json = JSON.parse(data);
          hashtag=json['hashtag'];
          id=json['id'];
          $("#edit_clienthastag").val(hashtag);
          $("#hashtag_id").val(id);
         console.log(hashtag);
         console.log(id);
        $(".LoadingImage").hide();
        $("#myModal").addClass('in show');
        $("#myModal").show();
      }
    });
 });

  $(".delete_hashtag").click(function(){
    var id=$(this).attr('id');
    $("#delete_hashtag_id").val(id);
    $("#deleteModal").addClass('in show');
    $("#deleteModal").show();
   });

  $(".close").click(function(){
     $("#deleteModal").removeClass('in show');
    $("#deleteModal").hide();
    $("#myModal").removeClass('in show');
        $("#myModal").hide();
  });


  $('.dropdown-sin-4').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
   });

    $('.dropdown-sin-6').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
   });

    $('.dropdown-sin-2').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
   });
     $('.dropdown-sin-3').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
   });

  //  dropdown-sin-2
</script>
