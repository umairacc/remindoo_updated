<?php $this->load->view('includes/header');
$success = $this->session->flashdata('success');
?>
<div class="pcoded-content pcodedteam-content staffcustomcls staff-form-wrapper">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
            <div class="deadline-crm1 staff-customadd">
             <ul class="nav nav-tabs all_user1 md-tabs "><li>Add Staff Custom Form Fields</li></ul>
             <div class="add-delete-cus add-field f-right adddclsset"><a href="javascript:void(0);" class="add_button add_custom_button" title="Add field"><i class="fa fa-plus fa-6 deleteUser" aria-hidden="true"></i> Add </a></div>
            </div>
               
                  <!--start-->
                  <div class="update-dept01 staff-cus">
                 
                  
                     <?php if($success){?>
                     <div class="modal-alertsuccess alert alert-success"
                        ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <div class="pop-realted1">
                           <div class="position-alert1">
                              <?php echo $success; ?></div>
                        </div>
                     </div> <?php } ?>

                  <div class="new-task-teams">

                     <?php
                     $labels = explode(',', $data['labels']);
                     if (empty($data['labels'])) {
                     ?>
                        <form id="add_new_form" class="form-addfiled" method="post" action="<?php echo base_url() ?>staff/add_custom_form" enctype="multipart/form-data">
                        <?php } else { ?>
                           <form id="add_new_form" method="post" class="form-textarea1 form-addfiled" action="<?php echo base_url() ?>staff/update_custom_form" enctype="multipart/form-data">
                              <div class="row">
                                 <?php  }
                              if (!empty($data['labels'])) {
                                 $labels = explode(',', $data['labels']);
                                 $field_type = explode(',', $data['field_type']);
                                 for ($i = 0; $i < count($labels); $i++) { ?>

                                    <div class="col-sm-6 space-equal-data">
                                       <div class="form-group">
                                          <label>Label</label>
                                          <div class="field-added1"><input type="text" class="form-control clr-check-client" name="label[]" placeholder="Enter Form Label" value="<?php echo $labels[$i]; ?>"></div>
                                       </div>
                                    </div>
                                    <div class="col-sm-6 space-equal-data">
                                       <div class="form-group">
                                          <label>Field Type</label>
                                          <div class="field-added1"><select class="form-control clr-check-select" name="field_type[]">
                                                <option value="text" <?php if ($field_type[$i] == 'text') {
                                                                        echo "selected";
                                                                     } ?>>Text</option>
                                                <option value="number" <?php if ($field_type[$i] == "number") {
                                                                           echo "selected";
                                                                        } ?>>Number</option>
                                                <!--  <option value="textarea" <?php if ($field_type[$i] == "textarea") {
                                                                                    echo "selected";
                                                                                 } ?> >Text Area</option>   -->
                                                <option value="file" <?php if ($field_type[$i] == "file") {
                                                                        echo "selected";
                                                                     } ?>>File Upload</option>
                                             </select></div>
                                          <div class="add-delete-cus del-field newremovecls"><a href="javascript:void(0);" id="<?php echo $i; ?>" class="unsetvalue" title="Add field"><i class="icofont icofont-ui-delete deleteUser" aria-hidden="true"></i></a></div>
                                       </div>

                                    </div>
                              <?php   }
                              } ?>
                              <input type="hidden" name="staff_id" value="<?php echo $this->session->userdata['userId']; ?>">
                              <div class="field_wrapper">
                                 <div class="col-sm-6 space-equal-data">
                                    <div class="form-group">
                                       <label>Label</label>
                                       <div class="field-added1"><input type="text" class="form-control clr-check-client" name="label[]" placeholder="Enter Form Label" value=""></div>
                                    </div>
                                 </div>
                                 <div class="col-sm-6 space-equal-data">
                                    <div class="form-group">
                                       <label>Field Type</label>
                                       <div class="field-added1"> <select class="form-control clr-check-select" name="field_type[]">
                                             <option value="text">Text</option>
                                             <option value="number">Number</option>
                                             <!--   <option value="textarea">Text Area</option>    -->
                                             <option value="file">File Upload</option>
                                          </select></div>

                                    </div>
                                 </div>
                              </div>

                              <div class="col-sm-12 form-group create-btn">
                                 <input type="submit" name="add_task" class="btn-primary" value="create" />
                              </div>
                              </div>
                           </form>
                  </div>
               </div>
               <!-- close -->

            </div>
            <!-- Page body end -->
         </div>
      </div>
      <!-- Main-body end -->
      <div id="styleSelector">
      </div>
   </div>
</div>
</div>
</div>
</div>
</div>
<!-- ajax loader -->
<div class="LoadingImage"></div>
<style>
   .LoadingImage {
      display: none;
      position: fixed;
      z-index: 100;
      background-image: url('<?php echo site_url() ?>assets/images/ajax-loader.gif');
      background-color: #666;
      opacity: 0.4;
      background-repeat: no-repeat;
      background-position: center;
      left: 0;
      bottom: 0;
      right: 0;
      top: 0;
   }
</style>
<!-- ajax loader end-->
<?php $this->load->view('includes/session_timeout'); ?>
<?php $this->load->view('includes/footer'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>bower_components/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dropdown.js"></script>
<script src="<?php echo base_url(); ?>assets/js/mock.js"></script>

<script type="text/javascript">
   $('#timepicker1').timepicker();
</script>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>

<script type="text/javascript">
   $(document).ready(function() {

      $("#add_new_team").validate({

         rules: {
            label: "required",
            field_type: "required",
         },
         messages: {
            label: "Please enter form label values",
            field_type: "Please enter form field type",
         },

      });
   });
</script>
<script type="text/javascript">
   $(document).ready(function() {
      var maxField = 10; //Input fields increment limitation
      var addButton = $('.add_button'); //Add button selector
      var wrapper = $('.field_wrapper'); //Input field wrapper
      var fieldHTML = '<div class="field_wrapper_append"><div class="col-sm-6 space-equal-data"><div class="form-group"><label>Label</label><div class="field-added1"> <input type="text" class="form-control clr-check-client" name="label[]" placeholder="Enter Form Label" value=""></div></div> </div><div class="col-sm-6 space-equal-data"><div class="form-group"><label>Field Type</label><div class="field-added1"> <select  class="form-control clr-check-select" name="field_type[]"> <option value="text">Text</option> <option value="number">Number</option> <option value="file">File Upload</option> </select> </div><div class="add-delete-cus del-field newremovecls"><a href="javascript:void(0);" class="remove_button" title="Remove field"><i class="icofont icofont-ui-delete deleteUser" aria-hidden="true"></i> </a></div></div></div></div></div>'; //New input field html 
      var x = 1; //Initial field counter is 1
      $(addButton).click(function() { //Once add button is clicked
         if (x < maxField) { //Check maximum number of input fields
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); // Add field html
         }
      });
      $(wrapper).on('click', '.remove_button', function(e) { //Once remove button is clicked
         e.preventDefault();
         $(this).parents('div.field_wrapper_append').remove(); //Remove field html
         x--; //Decrement field counter
      });

      /** for hide success msg **/
      setTimeout(function() {
         $('.alert-success').hide();
      }, 2000);
      /** end of hide success msg **/
   });


   $(".unsetvalue").click(function() {

      var id = $(this).attr('id');
      var user_id = <?php echo $this->session->userdata['userId']; ?>;
      var data = {
         'id': id,
         'user_id': user_id
      };
      //alert(data);
      $.ajax({
         url: '<?php echo base_url(); ?>staff/unset_value/',
         type: "POST",
         data: data,
         success: function(data) {
            var json = JSON.parse(data);
            status = json['status'];
            if (status == '1') {
               location.reload();
            }
         }
      });
   })
</script>

<script>
	$(document).ready(function(){
      function staffCustomInput(){
         $(".clr-check-client").keyup(function(){
			var clr = $(this).val();
			if(clr.length >= 3){
				$(this).addClass("clr-check-client-outline");
			}else{
				$(this).removeClass("clr-check-client-outline");
			}
		});
    $(".clr-check-client").each(function(i){
      if($(this).val() !== ""){
        $(this).addClass("clr-check-client-outline");
      }  
    })

		$(".clr-check-select").change(function(){
			var clr_select = $(this).val();
         if(clr_select !== ' '){
            $(this).addClass("clr-check-select-client-outline");
         }else{
				$(this).removeClass("clr-check-select-client-outline");
			}
		});
    $(".clr-check-select").each(function(i){
      if($(this).val() !== ""){
        $(this).addClass("clr-check-select-client-outline");
      }  
    })
   }

      staffCustomInput();

      $(".add_custom_button").click(function(){
         staffCustomInput();
      })
         
   });

</script>
</body>

</html>