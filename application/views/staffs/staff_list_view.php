<?php
$this->load->view('includes/header');    
?>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/buttons.dataTables.min.css?ver=2">
  
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/colreorder/1.5.1/css/colReorder.dataTables.min.css">
<style>
   button.btn.btn-info.btn-lg.newonoff {
   padding: 3px 10px;
   height: initial;
   font-size: 15px;
   border-radius: 5px;
   }
.dropdown-content {
    display: none;
    position: absolute;
    background-color: #fff;
    min-width: 86px;
    overflow: auto;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    left: -92px;
    width: 150px;
}
tfoot {
    display: table-header-group;
}
div.allocate_service_select_div
{
  width:300px;
}
</style>
<div class="modal-alertsuccess alert alert-suc-check succ info_popup" style="display:none;">
   <div class="newupdate_alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
   <div class="pop-realted1">
      <div class="position-alert1 info_text">
         
      </div>
   </div></div>
</div>
 <div class="modal fade" id="myStaff_delete" role="dialog">
    <div class="modal-dialog modal-confirm-staff">
          <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">

        <input type="hidden" name="delete_ids" id="delete_ids" value="">
          <p class="message_confirm">Do you want to delete staff?</p>
        </div>
        <div class="modal-footer">
          <button type="button" id="button_class" class="btn btn-default" >Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
      </div>
      
    </div>
  </div>
<div class="pcoded-content card-removes">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body rm-staff-list staff-list-wrapper">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card">
                        <!-- admin start-->
                        <div class="client_section user-dashboard-section1 floating_set">
                       </div> <!-- all-clients -->
                           <div class="addarchieves floating_set fr-stflist">
                              <div class="deadline-crm1 floating_set">
                                 <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard client-top-nav">
                                    <li class="nav-item">
                                    <a href="<?php echo base_url()?>Role_Assign" class="nav-link " data-tag="home">Roles</a>
                                      <div class="slide"></div>
                                    </li>
                                    <li class="nav-item ">
                                       <a class="nav-link active"  data-tag="home" href="<?php echo base_url()?>user/staff_list">Users</a>
                                       <div class="slide"></div>
                                    </li>
                                     <?php if($_SESSION['permission']['User_and_Management']['create']==1)
                                    {
                                    ?>                                 
                                    <li class="nav-item ">
                                         <a class="nav-link"  data-tag="home" href="<?php echo base_url()?>staff">New User</a>
                                         <div class="slide"></div>
                                      </li>
                                    <?php 
                                    }
                                    ?>
                                    <li class="nav-item ">
                                    <a href="<?php echo base_url()?>team/organisation_tree" class="nav-link " data-tag="home">Groups</a>
                                      <div class="slide"></div>
                                    </li>

                                     <!-- <li class="nav-item">
                                       <a class="nav-link" href="<?php echo base_url().'staff';?>"><img class="themicon" src="https://remindoo.uk/assets/images/tabicon2.png" alt="themeicon">Add New</a>
                                       <div class="slide"></div>
                                    </li> -->

                                 </ul>
                                 <?php $status_column = array_column( $staff_list , 'status'); ?>
                                 <ul class="form-group nav nav-tabs all_user1 md-tabs pull-left u-dashboard don1 renewdesign" style="margin-top: 20px;top: 80px">
                                    <li class="nav-item">
                                       <a class="nav-link  color1 tap_click active" href="javascript:void(0);"  data-id="All">All
                                       <span><?php echo count($staff_list); ?></span></a>
                                       <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link color2 tap_click " href="javascript:void(0);"  data-id="1" data-child-id="active_checkbox">Active<span><?php echo count( array_intersect( $status_column , [1] )  ); ?></span></a>
                                       <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link color3 tap_click"   href="javascript:void(0);" data-id="0|2" data-child-id="inactive_checkbox">Non-Active<span><?php echo count( array_intersect( $status_column , [0,2] )  ); ?></span></a>
                                       <div class="slide"></div>
                                    </li>
                                   
                                    <li class="nav-item">
                                       <a class="nav-link color5 tap_click"  data-id="3" href="javascript:void(0);">Archived
                                        <span>
                                        <?php echo count( array_intersect( $status_column , [3] )  ); ?>                                          
                                        </span>
                                       </a>
                                       <div class="slide"></div>
                                    </li>
                                 </ul>


                                 <div class="pull-right csv-sample01 for-add-staff icon_color">
                                    <button type="button" id="delete" class="delete_staff del-tsk12 f-right button_section"   style="display:none;" data-toggle="modal" data-target="#myStaff_delete"><i class="fa fa-trash fa-6" aria-hidden="true"></i>Delete</button>
                                    <button type="button" id="archive" class="archive_staff del-tsk12 f-right button_section" style="display:none;" data-toggle="modal" data-target="#myStaff_delete" ><i class="fa fa-archive" aria-hidden="true"></i>Archive</button>
                                    <button type="button" id="unarchive" class="unarchive_staff del-tsk12 f-right button_section" style="display:none;" data-toggle="modal" data-target="#myStaff_delete" ><i class="fa fa-archive" aria-hidden="true"></i>Unarchive</button>                                 
                                 </div>

                                
                              </div>
                              <div class="floating_set all-user-staff" style="margin-top: 50px">
                                 <div class="tab-content">
                                    <div id="allusers" class="tab-pane fade in active">
                                       <div class="client_section3 table-responsive floating_set ">
                                          <div id="status_succ"></div>
                                          <div class="all-usera1 button_visibility">

                                            <div class="filter-data for-reportdash">
                                              <div class="filter-head">
                                                <div class="filter-head-content">
                                                  <h4>FILTERED DATA</h4>                                                
                                                  <div id="container2" class="box-container">
                                                  </div>
                                                </div>
                                                <div>
                                                  <button class="btn btn-danger f-right" id="clear_container">clear</button>
                                                </div>                                                
                                              </div>                                   
                                            </div>

                                             <table class="table client_table1 text-center display nowrap" id="allstaff" cellspacing="0" width="100%">
                                                <thead>
                                                  <tr class="text-uppercase">
                                                  <th class="SELECT_ROW_TH Exc-colvis">                                               
                                                  <div class="checkbox-fade fade-in-primary">
                                                  <label class="custom_checkbox1">
                                                  <input type="checkbox" id="select_all_staff">
                                               
                                                   <i></i>
                                                  </label>
                                                  </div>
                                                  </th>
                                                  <th class="PROFILE_PIC_TH">Photo</th>
                                                  <th class="NAME_TH hasFilter">Name  <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                  <div class="sortMask"></div>                                                 
                                                  </th>                                                 
                                                  <th class="USRENAME_TH hasFilter">Username
                                                    <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                    <div class="sortMask"></div> 
                                                  </th>
                                                  <th class="STATUS_TH hasFilter">Active
                                                    <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                    <div class="sortMask"></div> 
                                                    <select multiple="true" searchable="true" class="filter_check" id="" style="display: none;">
                                                      <option value="Active">Active</option>
                                                      <option value="InActive">InActive</option>
                                                      <option value="Archive">Archive</option>
                                                    </select>
                                                  </th>                                                 
                                                  <th class="SERVICE_WORKFLOW_TH hasFilter" style="width: 250px !important;">Service/Work Flow
                                                    <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                    <div class="sortMask"></div>
                                                    <select multiple="true" searchable="true" class="filter_check" id="" style="display: none;" data-search-type="multiple_value">
                                                      <?php 
                                                        if( !empty( $services ) )
                                                        {
                                                           foreach ($services as $key => $value) { 
                                                            ?>
                                                            <option value="ser_<?php echo $value['id']; ?>"><?php echo $value['service_name']; ?>
                                                            </option>
                                                          <?php  }    
                                                        }
                                                        if( !empty($work_flow) )
                                                        { 
                                                          ?>
                                                          <?php foreach ($work_flow as $key => $value) { 
                                                          ?>
                                                            <option value="wor_<?php echo $value['id']; ?>"><?php echo $value['service_name']; ?></option>
                                                            <?php  }    
                                                        }  
                                                        ?>
                                                    </select> 
                                                  </th>
                                                  <th class="ROLE_TH hasFilter">Role  <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                                  <div class="sortMask"></div> 
                                                  </th>

                                                  <th class="ACTION_TH Exc-colvis">Action</th>
                                                  <th style="display: none;" class="STATUS_PRIMARY_SEARCH_TH Exc-colvis"></th>
                                                   </tr>

                                                </thead>


                                                <!--  <tfoot>
                                                   <tr class="text-uppercase">
                                                      <th>  
                                                      </th>
                                                      <th></th>
                                                      <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                      <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                      <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                      <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                       <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                      <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                      <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select> </th>
                                                      <th></th>
                                                   </tr>
                                                </tfoot> -->


                                               
                                                <tbody>
                                                   <?php 
                                                   
                                                   $USER_STATUS = ['0'=>'InActive' ,'1'=>'Active','2'=>'InActive','3'=>'Archive'];
                                                   $i = 0;
                                                  foreach ($staff_list as $getallUserkey => $getallUservalue)
                                                  {
                                                    $staff_name = ucfirst($getallUservalue['first_name']). ' '.$getallUservalue['last_name'];
                                                  	$role=$this->db->query("select role from roles_section where id=".$getallUservalue['roles']." ")->row_array();
                                                  	$profile = ( $getallUservalue['profile']!='' ? $getallUservalue['profile'] : 'unknown.png');
                                                      
                                                      $ser_wor_search = '';
                                                      $ser_wor        = ''; 
                                                      $ser_wor .='<div class="allocate_service_select_div lead-form1" id="allocate_service_select_div'.$i.'">
                                                                    <select name="service_work_flow[]" multiple="true" class="staff_ser_wor" data-id="'.$getallUservalue["user_id"].'">';
                                                      if( !empty( $services ) )
                                                      {
                                                        $allocated_service = ( !empty( $getallUservalue['allocated_service'] ) ? explode(',' , $getallUservalue['allocated_service'] ) :[] );
                                                        $ser_wor .='<option disabled="true">Service</option>';
                                                        
                                                        foreach ($services as $key => $value) { 
                                                          $sel = '';
                                                          if( in_array( $value['id'] ,  $allocated_service ) )
                                                            {
                                                              $sel            = "selected='selected'";
                                                              $ser_wor_search = ( !empty( $ser_wor_search ) ? ",ser_".$value['id'] : "ser_".$value['id'] );
                                                            }

                                                            $ser_wor .='<option value="ser_'.$value['id'].'" '.$sel.'>'.$value['service_name'].'</option>';
                                                            }    
                                                      }
                                                      if( !empty($work_flow) )
                                                      { 
                                                        $allocated_work_flow = ( !empty( $getallUservalue['allocated_work_flow'] ) ? explode(',' , $getallUservalue['allocated_work_flow'] ) :[] ); 
                                                        $ser_wor .='<option disabled="true">Work Flow</option>';
                                                         foreach ($work_flow as $key => $value) { 
                                                          $sel = '';
                                                          if( in_array( $value['id'] ,  $allocated_work_flow ) )
                                                            {
                                                              $sel = "selected='selected'";
                                                              $ser_wor_search = (!empty( $ser_wor_search ) ? ",wor_".$value['id'] : "wor_".$value['id'] );
                                                            }
                                                          $ser_wor .='<option value="wor_'.$value['id'].'" '.$sel.'>'.$value['service_name'].'</option>';
                                                         }    
                                                      }  
                                                      $ser_wor .='</select>
                                                                  </div>';
                                                      ?>

                                                   <tr id="<?php echo $getallUservalue["user_id"]; ?>">                                                   
                                                      <td>
                                                         <div class="checkbox-fade fade-in-primary">
                                                            <label class="custom_checkbox1">
                                                            <input type="checkbox" class="staff_checkbox" data-staff-id="<?php echo $getallUservalue['user_id'];?>">
                                                            <!-- <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>   -->
                                                            <i></i>
                                                            </label>
                                                         </div>
                                                      </td>
                                                      <td class="user_imgs"><img src="<?php echo base_url().'/uploads/'.$profile;?>" alt="img">
                                                      </td>

                                                      <td data-search="<?php echo $staff_name;?>">
                                                        <!-- <?php echo base_url();  ?>staff/staff_profile/<?php echo $getallUservalue['user_id']; ?> -->
                                                        <a href="#"><?php echo $staff_name;?></a>
                                                      </td>
                                                       
                                                      <td data-search="<?php echo $getallUservalue['username'];?>">
                                                         <?php echo $getallUservalue['username'];?>
                                                      </td>
                                                      <td data-search="<?php echo $USER_STATUS[$getallUservalue['status']];?>">                                                       
                                                         <select name="status<?php echo $getallUservalue['id'];?>" id="status<?php echo $getallUservalue['id'];?>" class="status" data-id="<?php echo $getallUservalue['user_id'];?>">
                                                            <option value="1" <?php if(isset($getallUservalue['status']) && $getallUservalue['status']=='1') {?> selected="selected"<?php } ?> >Active</option>
                                                            <option value="2" <?php if(isset($getallUservalue['status']) && ($getallUservalue['status']=='0'|| $getallUservalue['status']=='2' )) {?> selected="selected"<?php } ?>>Inactive</option>
                                                           <option value="3" <?php if(isset($getallUservalue['status']) && $getallUservalue['status']=='3') {?> selected="selected"<?php } ?>>Archive</option> 
                                                         </select>
                                                      </td>
                                                      <td data-search="<?php echo $ser_wor_search;?>">
                                                        <?php echo $ser_wor;?>
                                                      </td>
                                                      <td data-search="<?php echo $role['role'];?>"><?php echo $role['role'];?></td>
                                                      <td>
                                                        <p class="action_01">
                                                          <div class="dropdown">
                                                          <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownLeadsMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="dropdwn-action"></span><span class="dropdwn-action"></span><span class="dropdwn-action"></span></button>
                                                            <div class="dropdown-menu" aria-labelledby="dropdownLeadsMenuButton">
                                                              <?php 
                                                                if($_SESSION['permission']['User_and_Management']['delete']==1)
                                                                {
                                                                ?>                                                       
                                                                  <a href="#" onclick="return confirm_staff('<?php echo $getallUservalue['user_id'];?>');" data-toggle="modal" data-target="#myStaff_delete"><i class="icofont icofont-ui-delete tooltip-delete deleteUser" aria-hidden="true" ></i></a>
                                                                <?php
                                                                }
                                                                if($_SESSION['permission']['User_and_Management']['edit']==1)
                                                                { 
                                                              ?>
                                                              <a href="<?php echo base_url().'staff/index/'.$getallUservalue['user_id'];?>"><i class="icofont icofont-edit" aria-hidden="true"></i></a>

                                                              <?php $is_disable = ( $getallUservalue['status'] == 3 ?'disabled':'' );?>

                                                                <a href="javascript:void(0)" class="<?php echo $is_disable;?>" data-toggle="modal" data-target="#myStaff_delete" onclick="return showconfirmation('<?php echo $getallUservalue['user_id'];?>')">
                                                                  <i class="fa fa-file-archive-o archieve_click" aria-hidden="true"></i> 
                                                                </a>
                                                              <?php
                                                              }
                                                              ?>
                                                            <!-- <a href="<?php echo base_url();?>staff/staff_report/<?php echo $getallUservalue['user_id'];?>" style="color: blue;font-size: 18px;"><i class="fa fa-inbox fa-6" aria-hidden="true"></i></a> -->
                                                            </div>
                                                          </div>
                                                        </p>
                                                      </td>
                                                      <td style="display: none;" data-search="<?php echo $getallUservalue['status'];?>"
                                                        class="Status_primary_search_TD"></td>                                         
                                                   </tr>
                                                   <?php  
                                                      $i++;
                                                      } ?>
                                                </tbody>
                                             </table>
                                             <input type="hidden" class="rows_selected" id="select_staff_count" >     
                                          </div>
                                       </div>
                                    </div>
                                    <!-- home-->
                                 </div>
                              </div>
                              <!-- admin close -->
                           </div>
                           <!-- Register your self card end -->
                        </div>
                     </div>
                  </div>
                  <!-- Page body end -->
               </div>
            </div>
            <!-- Main-body end -->
            <div id="styleSelector">
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div> 
<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

<script src="<?php echo base_url()?>assets/js/user_page/materialize.js"></script> 

<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>

<script src="<?php echo base_url()?>assets/js/custom/buttons.colVis.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>
<script src="<?php echo base_url()?>assets/js/custom/datatable_extension.js"></script>

<!-- For Export Buttom -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>

<!-- For Export Buttom -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/jquery.tagsinput.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/email.css">
<script type="text/javascript" language="javascript" src="<?php echo base_url()?>assets/js/jquery.tagsinput.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
<script>
   /*$(document).ready(function() {   
   // payment/non payment status
      $('.suspent').click(function() {
      if($(this).is(':checked'))
         var stat = '1';
      else
         var stat = '0';
      var rec_id = $(this).val();
      var $this = $(this);
      $.ajax({
         url: '<?php echo base_url();?>user/suspentChange/',
         type: 'post',
         data: { 'rec_id':rec_id,'status':stat },
         success: function( data ){
          //alert('ggg');
             $("#suspent_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! User status have been changed successfully...</span></div></div></div>');
               if(stat=='1'){              
                 $this.closest('td').next('td').html('Payment');
               } else {
                 $this.closest('td').next('td').html('Non payment');
               }
             },
             error: function( errorThrown ){
                 console.log( errorThrown );
             }
         });
      });
   });*/  

   $(document).on('click','#close',function(e){
        $('.alert-success1').hide();
        return false;
    });
   $('.allocate_service_select_div').dropdown();




 function confirm_staff(id) 
 {
   $(".message_confirm").html("Do you want to delete?");
   $("#button_class").addClass('delete_yes');
   $("#delete_ids").val(id);  
    return false;
 } 
 function showconfirmation(id)
 {
    $(".message_confirm").html("Do you want to archive?");
    $("#button_class").addClass('archive_yes');
    $("#delete_ids").val(id);  
 }



 $(document).on('click','#close',function(e)
   {
     $('.alert-success').hide();
     return false;
   });




$(document).ready(function(){
  var staff_table; 

  <?php 
    $column_setting = Firm_column_settings('staff_list');
  ?>
  var column_order = <?php echo $column_setting['order'] ?>;
  
  var hidden_coulmns = <?php echo $column_setting['hidden'] ?>;

  var column_ordering = [];

  $.each(column_order,function(i,v){
   
    var index = $('#allstaff thead th.'+v).index();

    if(index!==-1)
    {
       // alert(index);
      column_ordering.push(index);
    }
  });
   /*for settings button collection modifycation*/
 
  $(document).on('click', '.Settings_Button', AddClass_SettingPopup);
  $(document).on('click', '.Button_List', AddClass_SettingPopup );

  $(document).on('click','.dt-button.close',function()
  {
    $('.dt-buttons').find('.dt-button-collection').detach();
    $('.dt-buttons').find('.dt-button-background').detach();
  });
  function AddClass_SettingPopup()
  {
    $('button.Button_List').closest('.dt-button-collection').addClass('Settings_List');
    $('button.buttons-columnVisibility').closest('.dt-button-collection').addClass('columnVisibility_List');
  }
  /*for settings button collection modifycation*/

  function Trigger_To_Reset_Filter()
  {
      $('div.toolbar-table input').val(' ');

      $('#select_all_staff').prop( 'checked',false );

      $('#select_all_staff').trigger( 'change' );

      Redraw_Table( staff_table );
  }
  function toggle_action_button( state , is_archive_tab )
  {
    if (state)
        {
            $(this).find('.staff_checkbox').prop('checked',true);
            $(".delete_staff").show();         
            if(is_archive_tab != 3)
            {
              $(".archive_staff").show();
            }
            else
            {
              $(".unarchive_staff").show();
            }
        } 
        else
        {
          $(this).find('.staff_checkbox').prop('checked',false);
          $(".delete_staff").hide();
          if(is_archive_tab != 3)
          {
            $(".archive_staff").hide();
          }
          else
          {
            $(".unarchive_staff").hide();
          }
        }
  }


  staff_table = $('#allstaff').DataTable({
    "pageLength": "<?php echo get_firm_page_length() ?>",
    "dom": '<"toolbar-table select_visibles" B>lfrtip',
     buttons: [
      {

          extend: 'collection',
          background:false,
          text: '<i class="fa fa-cog" aria-hidden="true"></i>',
          className:'Settings_Button',
          buttons:[
                  { 
                    text: '<i class="fa fa-filter" aria-hidden="true"></i>Reset Filter',
                    className:'Button_List',
                    action: function ( e, dt, node, config )
                    {
                        Trigger_To_Reset_Filter();
                    }
                  },
                  {
                      extend: 'colvis',
                      text: '<i class="fa fa-cogs" aria-hidden="true"></i>Column Visibility',              
                      columns: ':not(.Exc-colvis)',
                      className:'Button_List',
                      prefixButtons:[
                      {text:'X',className:'close'}]
                  },                        
                  /*DataTable_Export_Buttons*/
          ]
      }
    ],
    order: [],//ITS FOR DISABLE SORTING
    columnDefs: 
    [
      {"orderable": false,"targets": ['SELECT_ROW_TH','ACTION_TH','PROFILE_PIC_TH']}
    ],
    colReorder: 
    {
      realtime: false,
      order:column_ordering,
      fixedColumnsLeft : 1,
      fixedColumnsRight: 2
    },
    "iDisplayLength": 10,
    initComplete: AddColumn_Filters
  });
  
    Filter_IconWrap();
    Change_Sorting_Event( staff_table ); 
        
    ColReorder_Backend_Update ( staff_table , 'staff_list' );
    ColVis_Backend_Update ( staff_table , 'staff_list' );  

    $('.filter-data.for-reportdash #clear_container').on('click' , function(){ Redraw_Table( staff_table ); });

    $('.tap_click').click(function(){
      Trigger_To_Reset_Filter();
      $('.tap_click').removeClass('active');
      $(this).addClass('active');
      var status = $(this).attr('data-id').trim();
      if(status == "All")
      {
        return;
      }
      else
      {
        staff_table.column('.STATUS_PRIMARY_SEARCH_TH').search('^'+status+"$",true,false).draw();
      }

    });

    
  /*    for(j=2;j<numCols;j++){  
          $('#staff_'+j).on('change', function(){ 
            var result=$(this).attr('id').split('_');      
             var c=result[1];
              var search = [];              
              $.each($('#staff_'+c+ ' option:selected'), function(){                
                  search.push($(this).val());
              });      
              search = search.join('|'); 
              if(c==2){               
                c=Number(c) + 1;
              } 
              if(c==5){               
                c=Number(c) + 1;
              } 
              staff_table.column(c).search(search, true, false).draw();  
          });
       }*/



    $(document).on('change','#select_all_staff',function(){
       var checked   =  this.checked;
       var IsArchive =  $('.tap_click.active').attr('data-id'); 
       if(checked){
        $(".all-user-staff").addClass("user-staff-list");
        $(".scroll_table").addClass("scroll-table-staff-list");
      }else{
        $(".all-user-staff").removeClass("user-staff-list");
        $(".scroll_table").removeClass("scroll-table-staff-list");
      }

       staff_table.column(0).nodes().to$().each(function(index) {
        $(this).find('.staff_checkbox').prop('checked',checked);
        toggle_action_button( checked , IsArchive );
      });
    });


    /*$(document).on('click','.staff_checkbox', function() {
      var checked   =  this.checked;
      var IsArchive =  $('.tap_click.active').attr('data-id');
    if($(this).is(':checked', true)) {        
        $(".delete_staff").show();
        $(".archive_staff").show();
        $(".unarchive_staff").show();

    }else{
      $("#select_all_staff").prop('checked',false);
      if($("input.staff_checkbox:checked").length == 0){
             $(".delete_staff").hide(); 
             $(".archive_staff").hide();
             $(".unarchive_staff").hide();
      }
    }      
    }); */ 
    $(document).on("click",".staff_checkbox",function(event)
    {      
      
      var IsArchive=$('.tap_click.active').attr("data-id").trim();
      var checked = this.checked;
      var tr=0;
      var ck_tr=0;
      staff_table.column(0).nodes().to$().each(function(index) {
      if( $(this).find(".staff_checkbox").is(":checked") )
        {
          ck_tr++;
        }
        tr++;
      });

      if(checked){
        $(".all-user-staff").addClass("user-staff-list");
        $(".scroll_table").addClass("scroll-table-staff-list");
      }else{
        $(".all-user-staff").removeClass("user-staff-list");
        $(".scroll_table").removeClass("scroll-table-staff-list");
      }

        if(tr==ck_tr)
        {
          $("#select_all_staff").prop("checked",true);
        } 
        else
        {
          $("#select_all_staff").prop("checked",false);
        } 
      if(ck_tr)
      {
        toggle_action_button(1,IsArchive);
      }
      else  
      {
        toggle_action_button(0,IsArchive);
      } 
    });

   //  $(document).ready(function() { 
    
    $(document).on("change",".staff_ser_wor",function(event){
      event.preventDefault;
      var user_id = $(this).data('id');
      var sel_val = $(this).val();
      $.ajax({
           type: "POST",
           url: "<?php echo base_url().'staff/change_staff_assigned_ser_wor';?>",
           cache: false,
           data: {user_id:user_id,service_work_flow:sel_val},
           beforeSend: function() {
             $(".LoadingImage").show();
           },
           success: function(data) {
             $(".LoadingImage").hide();
         }});
    });  

    $(document).on('click','.button_section', function() {
      $("#button_class").removeClass();
      if($(this).attr('id')=='delete')
      {
        $(".message_confirm").html("Do you want to delete?");
        $("#button_class").addClass('delete_yes');
      }
      else if($(this).attr('id')=='archive')
      {
        $(".message_confirm").html("Do you want to archive?");
        $("#button_class").addClass('archive_yes');
      }
      else
      {
        $(".message_confirm").html("Do you want to unarchive?");
        $("#button_class").addClass('unarchive_yes');
      }
       var staff_id=[];
        staff_table.column(0).nodes().to$().each(function(index){
        if($(this).find(".staff_checkbox").is(":checked"))
        {        
          staff_id.push($(this).find(".staff_checkbox").data('staff-id'));
        }
      });    
         $('#myStaff_delete').show();     
         $("#delete_ids").val(staff_id);  
      });    
    
   $(document).on('click','.delete_yes',function() {
          var selected_staff_values = $("#delete_ids").val();   
         $.ajax({
           type: "POST",
           url: "<?php echo base_url().'staff/deleteStaffs';?>",
           cache: false,
           data: 'data_id='+selected_staff_values,
           beforeSend: function() {
             $('#myStaff_delete').modal('hide');
             $(".LoadingImage").show();
           },
           success: function(data) {
             $(".LoadingImage").hide();
             $(".info_popup .info_text").html("Success !!! User have been deleted successfully...");
             $(".info_popup").show();
             setTimeout(function() { 
             location.reload(); }, 1000);     
           }
         });
       });

      $(document).on('click','.unarchive_yes',function(){         
        var id=$("#delete_ids").val();      
        $.ajax({
          type: "POST",
          url: '<?php echo base_url(); ?>User/staffstatusChange',
          data: {'ids':id,'status':"unarchive"},
          beforeSend: function() {
             $('#myStaff_delete').modal('hide');
             $(".LoadingImage").show();
           },
          success: function(data) {
            $(".LoadingImage").hide();
             if(data)
              {               
                $('.info_popup .info_text').html("Success !!! User status have been changed successfully...");
                $('.info_popup').show();
                setTimeout(function() {
                  location.reload();
                }, 2000);        
             }
          }
        });
      });
        $(document).on("click",".archive_yes",function(){ 
    var id=$("#delete_ids").val();
    $.ajax({
      type: "POST",
      url: '<?php echo base_url(); ?>User/staffstatusChange',
       data: {'ids':id,"status":3},
     beforeSend: function() {
             $('#myStaff_delete').modal('hide');
             $(".LoadingImage").show();
           },
      success: function(data) {
         $(".LoadingImage").hide();
         if(data)
              {               
                $('.info_popup .info_text').html("Success !!! User status have been changed successfully...");
                $('.info_popup').show();
                /*setTimeout(function() {
                  location.reload();
                }, 2000);        */
             }      
      }
    });
  });

           $('#allstaff').on('change','.status',function () {
    var rec_id = $(this).data('id');
    var stat = $(this).val();
   $.ajax({
         url: '<?php echo base_url();?>user/staffstatusChange',
         type: 'post',
         data: {'ids':rec_id,'status':stat },
         beforeSend: function() {
             $('#myStaff_delete').modal('hide');
             $(".LoadingImage").show();
           },
         success: function( data ){  
          $(".LoadingImage").hide();
              if(data)
              {               
                $('.info_popup .info_text').html("Success !!! User status have been changed successfully...");
                $('.info_popup').show();
                /*setTimeout(function() {
                  location.reload();
                }, 2000); */       
             }
              },
             error: function( errorThrown ){
                 console.log( errorThrown );
             }
         });
    }); 


/*     $("th").on("click.DT", function (e) {        
        if (!$(e.target).hasClass('sortMask')) {        
            e.stopImmediatePropagation();
        }
    });


$('th .themicond').on('click', function(e) {
  if ($(this).parent().find(".dropdown-content").hasClass("Show_content")) {
      $(this).parent().find('.dropdown-content').removeClass('Show_content');
  }else{
      $('.dropdown-content').removeClass('Show_content');
      $(this).parent().find('.dropdown-content').addClass('Show_content');
  }
  $(this).parent().find('.select-wrapper').toggleClass('special');
      if( $(this).parent().find('.select-dropdown span input[type="checkbox"]').parent().hasClass('custom_checkbox1')){
      }else{
        $(this).parent().find('.select-dropdown span input[type="checkbox"]').wrap( '<label class="custom_checkbox1"></label>');
        $(this).parent().find('.custom_checkbox1 input').after( "<i></i>" );
      }
});*/
 });


</script>

<script>

      $('#allstaff').on( 'processing.dt', function ( e, settings, processing ) {
        var lgt = $("#container2 div").length;
        console.log(lgt,"length");
        if ( lgt >= 1){
          $(".scroll_table").addClass("scroll_table_filter");
        }else{
          $(".scroll_table").removeClass("scroll_table_filter");
        }
      } );

      $( ".remove, .filter-clear" ).click(function(e) {
        var lgt = $("#container2 div").length;
        if ( lgt >= 1){
          $(".scroll_table").addClass("scroll_table_filter");
        }else{
          $(".scroll_table").removeClass("scroll_table_filter");
        }
      });

      $('#allstaff').on( 'processing.dt', function ( e, settings, processing ) {
        if($(".scroll_table.scroll-table-staff-list").hasClass("scroll_table_filter")){
          $(".scroll_table").addClass("scroll_table_filter_data");
        }else if($(".scroll_table.scroll_table_filter").hasClass("scroll-table-staff-list")){
          $(".scroll_table").addClass("scroll_table_filter_data");
        }else{
          $(".scroll_table").removeClass("scroll_table_filter_data");
        }
      });

</script>

<script>
$(document).ready(function(){
  var checkExist_99 = setInterval(function() {
      if ($('#allstaff').length) {
        var pathname = window.location.pathname; // Returns path only (/path/example.html)
        if(pathname == '/user/staff_list'){
          $('.tap_click[data-child-id="active_checkbox"').trigger('click');
        }
        clearInterval(checkExist_99);
      }
    }, 100); // check every 100ms    
});
</script>
