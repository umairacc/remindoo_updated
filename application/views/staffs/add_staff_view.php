<?php
  $this->load->view('includes/header');
  $is_edit_page = $this->uri->segment(3, 0);
  $is_edit_page = ( $is_edit_page != 0 ? 'Edit User':'Add User');
?>
<!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.css"> -->
<link href="<?php echo base_url();?>assets/css/jquery.filer.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/hierarchy-select.min.css">
<!-- <link href="<?php echo base_url();?>assets/css/jquery.filer-dragdropbox-theme.css" type="text/css" rel="stylesheet" /> -->
<style type="text/css">
  .label_error_cls #telephone_number+ label.error
  {
    bottom: 0px !important;
  }
</style>
<?php
  $get_sign_json='';
  if(isset($staff['signature_path']) && $staff['signature_path']!='')
   { 
      $get_sign_jsons=$this->Common_mdl->get_field_value('pdf_signatures','signature_json_data','id',$staff['signature_path']);
      $get_sign_json=$get_sign_jsons;
   }
?>
<div class="pcoded-content permiss_stafflist">
   <div class="pcoded-inner-content card-removes">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card profile-edit-wrapper">
                        <div class="modal-alertsuccess alert alert-success" style="display:none;">
                         <div class="newupdate_alert">  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                           <div class="pop-realted1">
                              <div class="position-alert1">
                                 User Saved Successfully..
                              </div></div>
                           </div>
                        </div>
                        <div class="modal-alertsuccess alert alert-danger" style="display:none;">
                          <div class="newupdate_alert"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                           <div class="pop-realted1">
                              <div class="position-alert1">
                                 Something wrong Please try again.
                              </div></div>
                           </div>
                        </div>
                        <form id="staff_form" method="post" action="" enctype="multipart/form-data" autocomplete="off">
                          <input type="hidden" name="sign_data" id="sign_data" value="<?php if(isset($staf['signature_path'])){ echo $staff['signature_path']; } ?>">
                          <input type="hidden" name="is_profile_page" class="is_profile_page" value="<?php echo $is_profile_page;?>">

                           <div class="deadline-crm1 floating_set">
                              <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard client-top-nav">
                                <?php 
                                if($is_profile_page)
                                {
                                  ?>
                                  <li class="nav-item current_li"><a class="nav-link active" data-toggle="tab" id="required_information" href="#required_information">Profile</a><div class="slide"></div></li>
                                  <li class="nav-item current_li"><a class="nav-link" data-toggle="tab" id="kin_information" href="#">Next Of Kin</a><div class="slide"></div></li>
                                  <?php
                                }
                                else
                                {
                                  ?>

                                <li class="nav-item">
                                  <a href="<?php echo base_url()?>Role_Assign" class="nav-link " data-tag="home">Roles</a>
                                  <div class="slide"></div>
                                </li>
                                <li class="nav-item ">
                                  <a class="nav-link"  data-tag="home" href="<?php echo base_url()?>user/staff_list">Users</a>
                                  <div class="slide"></div>
                                </li>
                                <li class="nav-item ">
                                  <a class="nav-link active"  data-tag="home" href="#"><?php echo $is_edit_page; ?></a>
                                  <div class="slide"></div>
                                </li>
                                <li class="nav-item ">
                                  <a href="<?php echo base_url()?>team/organisation_tree" class="nav-link" data-tag="home">Groups</a>
                                  <div class="slide"></div>
                                </li>

                                  <?php
                                }
                                ?>

                              <?php if(!empty($custom_form['labels'])){  ?> 
                              <li class="nav-item current_li">
                                 <a class="nav-link" data-toggle="tab" href="#main_cutom">Custom Fields
                                 </a><div class="slide"></div>
                              </li>
                              <?php } ?>
                              </ul>
                           
                              <div class="Footer common-clienttab pull-right">
                                 <div class="change-client-bts1">
                                    <?php
                                      $label    = "Save"; 
                                      if( !empty($staff['id']) )
                                        $label  = "Update";  
                                     ?>
                                  <input type="submit" class="add_acc_client f-right edit-staff" value="<?php echo $label;?>" id="submit" />
                                  <input type="hidden" class="staff_id" value="<?php if(isset($staff['id']) && ($staff['id'])!='' ){ echo $staff['id'];} ?>">
                                 </div> 
                                 <div>
                                  <button class="save-kin-info" style="display:none;">save</button>
                                 </div>                            
                              </div>
                           </div>

                           <div class="management_section staff_mrng staff_mrng permission_staff  floating_set">
                              <div class="card-block accordion-block">
                                 <!-- tab start -->
                                 <div class="tab-content new_staff_ticket">
                                    <div id="required_information" class="border-checkbox-section tab-pane fade in active profile-kin-data">
                                       <div class="floating_set text-right accordion_ups isset-accor">
                                          <input type="hidden" id="user_id" name="user_id" value="<?php if(isset($staff['user_id']) && ($staff['user_id'])!='' ){ echo $staff['user_id'];}?>">
                                       </div>
                                       <div class="management_form1">
                                                                           
                                          <div class="col-sm-6 form-group row name_fields">
                                             <label class="col-sm-4 col-form-label">
                                              First Name
                                              <span class="Hilight_Required_Feilds">*</span>
                                             </label>
                                             <div class="col-sm-8">
                                                <input type="text" name="first_name" id="first_name" class="first_name clr-check-client" placeholder="First name" value="<?php if(isset($staff['first_name']) && ($staff['first_name']!='') ){ echo $staff['first_name'];}?>" >
                                               <!--  <div id="fname-error" class="fname-error" style="color: red"></div> -->
                                             </div>
                                          </div>

                                          <div class="col-sm-6 form-group row">
                                            <label class="col-sm-4 col-form-label">
                                            Last Name
                                            <span class="Hilight_Required_Feilds">*</span>
                                            </label>
                                             <div class="col-sm-8">
                                                <input type="text" name="last_name" id="last_name" class="clr-check-client" placeholder="Last name" value="<?php if(isset($staff['last_name']) && ($staff['last_name']!='') ){ echo $staff['last_name'];}?>" >
                                             </div>
                                          </div>

                                          <div class="col-sm-6 form-group row name_fields">
                                            <label class="col-sm-4 col-form-label">
                                            Email
                                            <span class="Hilight_Required_Feilds">*</span>
                                            </label>
                                             <div class="col-sm-8">
                                                <input type="text" name="email_id" id="email_id" class="form-control clr-check-client" value="<?php if(isset($staff['email_id']) && ($staff['email_id']!='') ){ echo $staff['email_id'];}?>">
                                             </div>
                                          </div>

                                          <div class="clearfix"></div>

                                          <div class="col-sm-6 form-group row name_fields">
                                            <label class="col-sm-4 col-form-label">
                                            Phone Number
                                            <span class="Hilight_Required_Feilds">*</span>
                                            </label>
                                       
                                           <?php 
                                           if(isset($staff['telephone_number']) && ($staff['telephone_number']!='') ){ $ctry=explode('-', $staff['telephone_number'] ,2); }
                                           ?>             

                                             <div class="col-sm-8 label_error_cls">
                                                <!-- <input type="text" name="country_code" class="country_code" style="width:80px;" value="<?php if(!empty($ctry[0])) { echo $ctry[0]; }?>" placeholder="Code"> -->
                                                 <div class="country_select_div">
                                                      <select class="country_code clr-check-select" name="country_code" placeholder="Country Code">
                                                        <option value="">Select Country Code</option>
                                                       <?php 
                                                         foreach ( $countries as $value )
                                                         {
                                                           $select = "";
                                                           if(!empty($ctry[0]))
                                                           {
                                                              if( $ctry[0] == $value['id'] )
                                                              {
                                                                $select = "selected='selected'"; 
                                                              }
                                                            }  
                                                           echo "<option value='".$value['id']."' ".$select.">".$value['sortname']."</option>";
                                                         }
                                                       ?>
                                                      </select>
                                                </div>

                                                <input type="tel" name="telephone_number" id="telephone_number" class="form-control clr-check-client" value="<?php if(isset($ctry[1]) && ($ctry[1]!='') ){ echo $ctry[1];}?>" placeholder="Phone number">
                                             </div>
                                          </div>

                                       

                                          <div class="col-sm-6 form-group row name_fields">
                                             <label class="col-sm-4 col-form-label">
                                              User Name
                                              <span class="Hilight_Required_Feilds">*</span>
                                             </label>
                                             <div class="col-sm-8">
                                                <input type="text" name="user_name" id="username" class="username clr-check-client" placeholder="User name" value="<?php if(isset($staff['username']) && ($staff['username']!='') ){ echo $staff['username'];}?>" autocomplete="off">
                                                <div id="fname-error" class="fname-error field-error" style="color: red"></div>
                                             </div>
                                          </div>

                                          <!-- end of 20-08-2018 -->
                                          <div class="col-sm-6 form-group row ">
                                            <label class="col-sm-4 col-form-label">
                                            Password
                                            <span class="Hilight_Required_Feilds">*</span>
                                            </label>
                                             <div class="col-sm-8">
                                                <input type="password" name="password" id="password" class="clr-check-client" placeholder="Password" value="<?php if(isset($staff['confirm_password']) && ($staff['confirm_password']) ){ echo $staff['confirm_password'];}?>" autocomplete="off">
                                             </div>
                                          </div>
                                          <div class="clearfix"></div> 

                                                 
                                             <?php
                                              $if_hide = ( $is_profile_page==1 ?'style="display:none"':'');
                                              $is_ignore = ( $is_profile_page==1 ?'ignore':'');
                                              ?>
                                             <div class="col-sm-6 form-group row name_fields widtincrese" <?php echo $if_hide;?> >
                                              <label class="col-sm-4 col-form-label">Role</label>
                                               <div class="col-sm-12">
                                              
                                                  <div class="dropdown hierarchy-select dropdn_hi" id="roles_select">
                                                    <button type="button" class="btn btn-secondary dropdown-toggle" id="example-one-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                                    <div class="dropdown-menu" aria-labelledby="example-one-button">
                                                       <div class="hs-searchbox">
                                                             <input type="text" class="form-control" autocomplete="off">
                                                       </div>
                                                    
                                                       <div class="hs-menu-inner">
                                                          <?php echo $roles_tag; ?>
                                                       </div>

                                                    </div>
                                                    <input class="d-none <?php echo $is_ignore;?>" name="roles" readonly="readonly"  aria-hidden="true" type="text" value="<?php if(isset($staff['roles'])) { echo $staff['roles'];}?>" />
                                                 </div>                                             
                                               </div>
                                            </div>

                                            <div class="col-sm-6 form-group row name_fields widtincrese" <?php echo $if_hide;?> >
                                              <label class="col-sm-4 col-form-label">Service/Work Flow</label>
                                               <div class="col-sm-12">
                                                
                                                <div class="allocate_service_select_div lead-form1">
                                                  <select name="service_work_flow[]" multiple="true">
                                                    <?php 
                                                      if( !empty( $services ) )
                                                      {
                                                        $allocated_service = ( !empty( $staff['allocated_service'] ) ? explode(',' , $staff['allocated_service'] ) :[] );
                                                        ?>
                                                        <option disabled="true">Service</option>
                                                        <?php foreach ($services as $key => $value) { 
                                                          $sel = '';
                                                          if( in_array( $value['id'] ,  $allocated_service ) ) $sel = "selected='selected'";
                                                          ?>
                                                          <option value="ser_<?php echo $value['id']; ?>" <?php echo $sel; ?> ><?php echo $value['service_name']; ?>
                                                          </option>
                                                        <?php  }    ?>
                                                      <?php
                                                      }
                                                      if( !empty($work_flow) )
                                                      { 
                                                        $allocated_work_flow = ( !empty( $staff['allocated_work_flow'] ) ? explode(',' , $staff['allocated_work_flow'] ) :[] ); 
                                                        ?>
                                                        <option disabled="true">Work Flow</option>
                                                        <?php foreach ($work_flow as $key => $value) { 
                                                          $sel = '';
                                                          if( in_array( $value['id'] ,  $allocated_work_flow ) ) $sel = "selected='selected'";
                                                        ?>
                                                          <option value="wor_<?php echo $value['id']; ?>"  <?php echo $sel; ?>><?php echo $value['service_name']; ?></option>
                                                          <?php  }    ?>
                                                      <?php
                                                      }  
                                                      ?>
                                                  </select>
                                                </div>                                             
                                               </div>
                                            </div>
                                          
                                            <div class="col-sm-6 form-group row name_fields" <?php echo $if_hide;?> >
                                              <label class="col-sm-4 col-form-label">
                                              Hourly Rate
                                              <span class="Hilight_Required_Feilds">*</span>
                                              </label>
                                               <div class="col-sm-8">
                                                  <input type="text" class="<?php echo $is_ignore;?> clr-check-client" name="hourly_rate" id="hourly_rate" placeholder="Hourly rate" value="<?php if(isset($staff['hourly_rate']) && ($staff['hourly_rate']!='') ){ echo $staff['hourly_rate'];}?>">
                                               </div>
                                            </div>
                                            
                                            <div class="clearfix"></div>
                                            <div class="col-sm-6 form-group row name_fields" <?php echo $if_hide;?>> 
                                               <label class="col-sm-4 col-form-label">Status</label>
                                               <div class="col-sm-8">
                                                <select name="status"  class="status <?php echo $is_ignore;?> clr-check-select" data-id="<?php echo $staff['user_id'];?>">
                                                  <option value="1" <?php if(isset($staff['status']) && $staff['status']=='1') {?> selected="selected"<?php } ?> >Active</option>
                                                  <option value="2" <?php if(isset($staff['status']) && ($staff['status']=='0'|| $staff['status']=='2' )) {?> selected="selected"<?php } ?>>Inactive</option>
                                                  <option value="3" <?php if(isset($staff['status']) && $staff['status']=='3') {?> selected="selected"<?php } ?>>Archive</option> 
                                               </select>
                                              </div>
                                            </div>

                                          <div class="col-sm-6 form-group row name_fields">
                                             <label class="col-sm-4 col-form-label">Facebook</label>
                                             <div class="col-sm-8">
                                                <input type="text" name="facebook" id="facebook" class="clr-check-client" placeholder="facebook url" value="<?php if(!empty($staff['facebook']) ){ echo $staff['facebook'];}?>">
                                             </div>
                                          </div>


                                          <div class="col-sm-6 form-group row name_fields">
                                             <label class="col-sm-4 col-form-label">Linkedin</label>
                                             <div class="col-sm-8">
                                                <input type="text" name="linkedin" id="linkedin" class="clr-check-client" placeholder="Linkedin url" value="<?php if(!empty($staff['linkedin'])){ echo $staff['linkedin'];}?>">
                                             </div>
                                          </div>

                                          <div class="clearfix"></div> 
                                          <div class="col-sm-6 form-group row name_fields">
                                             <label class="col-sm-4 col-form-label">Skype</label>
                                             <div class="col-sm-8">
                                                <input type="text" name="skype" id="skype" class="clr-check-client" placeholder="Skype url" value="<?php if(!empty($staff['skype']) ){ echo $staff['skype'];}?>">
                                             </div>
                                          </div>


                                          <div class="col-sm-6 form-group row attach-files file-attached1">
                                            <label class="col-sm-4 col-form-label">Profile Image</label>
                                            <div class="col-sm-8">
                                             <div class="custom_upload">   
                                                <input type="file" name="image_upload" id="image_upload" onchange="readURL(this);">
                                             </div>
                                                <input type="hidden" name="image_name" id="image_name" value="<?php if(isset($staff['profile']) && ($staff['profile']!='') ){ echo $staff['profile'];}else{echo "unknown.png";}?>">
                                             <?php
                                             if(isset($staff['profile']) && $staff['profile']!='') { ?>
                                                <div class="custom_edit_update">
                                                  <img src="<?php echo base_url().'uploads/'.$staff['profile']?>" id="preview_image" width="225" height="225"> 
                                                </div>
                                            <?php } else { ?>
                                                <div class="custom_edit_update">
                                                  <img src="<?php echo base_url().'uploads/unknown.png'?>" id="preview_image" > </div>
                                                </div>
                                            <?php } ?>   
                                           </div>

                                           <?php $if_hide = ($is_profile_page!=1?"style='display:none;'":"");?>

                                           <div class="col-sm-6 form-group row  radio_bts-staff flo_ri" <?php echo $if_hide;?> >
                                             <label class="col-sm-4 col-form-label">Email Signature</label>
                                             <div class="col-sm-8">            
                                                <input type="checkbox" class="js-small f-right fields" id="email_signature" name="email_signature" <?php if( isset($staff['email_signature']) && $staff['email_signature']=='on'){ echo "checked";} ?>>
                                                </div>
                                                <!-- 31-07-2018  for images sign -->
                                                 <div class="for_sign icon-forsign" <?php if( isset($staff['email_signature']) && $staff['email_signature']!='on'){ ?> style="display: none;" <?php } ?>>
                                                    <?php
                                                    if(isset($staff['signature_path']) && $staff['signature_path']!='')
                                                    { 
                                                    $get_sign_data=$this->Common_mdl->get_field_value('pdf_signatures','signature_path','id',$staff['signature_path']);
                                                       ?>
                                                     <div class="attch-trash01">  
                                                       <img src="<?php echo base_url().$get_sign_data; ?>">
                                                       <span class="remove_sign">
                                                             <a class="icon-jfi-trash jFiler-item-trash-action remove_file"></a>
                                                       </span>
                                                      </div>
                                                    <?php } ?> 
                                                 </div>                                                
                                                <!-- end of 31-07-2018 -->
                                            </div>
                                        </div>         
                                          <!-- tab1 -->
                                      </div>
                                    <!-- 1tab close-->                                   
                                  </div>
                                    
                                  <div id="main_cutom" class="tab-pane custom-tab_permission fade">
                                    <div class="padding-custom-picker">

                                    <?php 
                                    if(!empty($custom_form['labels']))
                                    {
                                    $staff_cus_form_values='';

                                    if(isset($staff['id']))
                                    {
                                    $staff_cus_values=$this->db->query('select * from staff_custom_form_values where staff_id='.$staff['user_id'].'')->row_array();
                                    if(count($staff_cus_values)>0)
                                    {
                                    $staff_cus_form_values=json_decode($staff_cus_values['values'],true);
                                    }
                                    }

                                    $labels=explode(',',$custom_form['labels']);
                                    $field_type=explode(',',$custom_form['field_type']);
                                    $field_name=explode(',',$custom_form['field_name']);
                                    $values=explode(',',$custom_form['values']);
                                    for($i=0;$i< count($labels);$i++)
                                    {


                                    if($field_type[$i]=="file"){ ?>
                                    <div class="form-group row ">
                                    <label class="col-sm-4 col-form-label"><?php echo $labels[$i]; ?></label>
                                    <div class="col-sm-8">
                                    <input type="<?php echo $field_type[$i]; ?>" name="<?php echo $field_name[$i]; ?>[]" id="bank_name" ></div>
                                    <input type="hidden" name="<?php echo $field_name[$i]; ?>_hidden" value="<?php if($staff_cus_form_values!=''){
                                    echo $staff_cus_form_values[$field_name[$i]];
                                    } ?>">
                                    <?php if($staff_cus_form_values[$field_name[$i]]!=''){ ?>
                                    <!--   <a href="<?php echo $staff_cus_form_values[$field_name[$i]]; ?>" target="_blank" >View</a> -->
                                    <span class="custom_images">
                                    <img src="<?php echo $staff_cus_form_values[$field_name[$i]]; ?>" onerror=this.src="<?php echo base_url().'uploads/unknown.png'; ?>">
                                    </span>
                                    <?php } ?>
                                    </div>
                                    <?php  }
                                    else
                                    { ?>
                                    <div class="form-group row ">
                                    <label class="col-sm-4 col-form-label"><?php echo $labels[$i]; ?></label>
                                    <div class="col-sm-8">
                                    <input type="<?php echo $field_type[$i]; ?>" name="<?php echo $field_name[$i]; ?>" id="bank_name" value="<?php if($staff_cus_form_values!=''){
                                    if(isset($staff_cus_form_values[$field_name[$i]])){
                                    echo $staff_cus_form_values[$field_name[$i]];
                                    } } ?>">
                                    </div>
                                    </div>
                                    <?php } } ?>
                                    <div class="text-right new-bank-update">

                                    </div>  
                                    <!-- </form> -->
                                    <?php } ?>
                                    </div>

                                  </div>
                              <!-- 3 tab close -->
                              </div> 
                        <!-- tab-content -->
                             </div>
                                </div> <!-- managementclose -->
                                <div class="management_section staff_mrng staff_mrng permission_staff floating_set kin-info-data" style="display: none;">
                                    <!-- Kin Tab Start -->
                                    <!-- <div id="kin_information" class="border-checkbox-section tab-pane fade in active">
                                       <div class="management_form1">
                                        <div class="kin-head-wrap">
                                          Emergency Contact Details:
                                        </div>

                                        <div class="col-sm-6 form-group row name_fields">
                                             <label class="col-sm-4 col-form-label">
                                              Title
                                              <span class="Hilight_Required_Feilds">*</span>
                                             </label>
                                             <div class="col-sm-8">
                                                <select class="text-info title clr-check-select"  name="">
                                                    <option value="Mr" >Mr</option>
                                                    <option value="Mrs">Mrs</option>
                                                    <option value="Miss">Miss</option>
                                                    <option value="Dr">Dr</option>
                                                    <option value="Ms">Ms</option>
                                                    <option value="Prof">Prof</option>
                                                </select>
                                              </div>
                                          </div>
                                                                           
                                          <div class="col-sm-6 form-group row name_fields">
                                             <label class="col-sm-4 col-form-label">
                                              First Name
                                              <span class="Hilight_Required_Feilds">*</span>
                                             </label>
                                             <div class="col-sm-8">
                                                <input type="text" name="first_name" id="first_name" class="first_name clr-check-client" placeholder="First name" value="" >
                                                 <div id="fname-error" class="fname-error" style="color: red"></div> 
                                             </div>
                                          </div>

                                          <div class="col-sm-6 form-group row">
                                            <label class="col-sm-4 col-form-label">
                                            Last Name
                                            <span class="Hilight_Required_Feilds">*</span>
                                            </label>
                                             <div class="col-sm-8">
                                                <input class="clr-check-client" type="text" name="last_name" id="last_name" placeholder="Last name" value="" >
                                             </div>
                                          </div>
                                          <div class="clearfix"></div>

                                          <div class="col-sm-6 form-group row name_fields">
                                            <label class="col-sm-4 col-form-label">
                                            Preferred Name
                                            <span class="Hilight_Required_Feilds">*</span>
                                            </label>
                                             <div class="col-sm-8">
                                                <input type="text" name="preferred_name" id="" class="form-control clr-check-client" value="">
                                             </div>
                                          </div>

                                          <div class="col-sm-6 form-group row name_fields">
                                            <label class="col-sm-4 col-form-label">
                                            Relationship to Employee
                                            <span class="Hilight_Required_Feilds">*</span>
                                            </label>
                                             <div class="col-sm-8">
                                                <input type="text" name="relation_employee" id="" class="form-control clr-check-client" value="">
                                             </div>
                                          </div>

                                          <div class="col-sm-6 form-group row name_fields">
                                            <label class="col-sm-4 col-form-label">
                                            Telephone
                                            <span class="Hilight_Required_Feilds">*</span>
                                            </label>
                                            <div class="col-sm-8">
                                              <select class="clr-check-select tele-kin" name="" id="">
                                                <option value="mobile">Mobile</option>
                                                <option value="work">Work</option>
                                                <option value="home">Home</option>
                                                <option value="main">Main</option>
                                                <option value="workfax">Work Fax</option>
                                                <option value="homefax">Home Fax</option>
                                              </select>
                                              <input type="text" class="text-info tele-kin-input clr-check-client" name=""  value="">
                                            </div>
                                          </div>

                                          <div class="clearfix"></div>

                                          <div class="col-sm-6 form-group row name_fields">
                                            <label class="col-sm-4 col-form-label">
                                            Address
                                            <span class="Hilight_Required_Feilds">*</span>
                                            </label>
                                             <div class="col-sm-8">
                                                <input type="text" name="address" id="" class="form-control clr-check-client" value="">
                                             </div>
                                          </div>

                                          <div class="col-sm-6 form-group row name_fields">
                                            <label class="col-sm-4 col-form-label">
                                            Email
                                            <span class="Hilight_Required_Feilds">*</span>
                                            </label>
                                             <div class="col-sm-8">
                                                <input type="text" name="email_id" id="" class="form-control clr-check-client" value="">
                                             </div>
                                          </div>

                                          <div class="clearfix"></div>

                                          <div class="kin-head-wrap">
                                            Emergency Contact Two:
                                          </div>

                                          <div class="col-sm-6 form-group row name_fields">
                                             <label class="col-sm-4 col-form-label">
                                              Title
                                              <span class="Hilight_Required_Feilds">*</span>
                                             </label>
                                             <div class="col-sm-8">
                                                <select class="text-info title clr-check-select"  name="">
                                                    <option value="Mr" >Mr</option>
                                                    <option value="Mrs">Mrs</option>
                                                    <option value="Miss">Miss</option>
                                                    <option value="Dr">Dr</option>
                                                    <option value="Ms">Ms</option>
                                                    <option value="Prof">Prof</option>
                                                </select>
                                              </div>
                                          </div>
                                                                           
                                          <div class="col-sm-6 form-group row name_fields">
                                             <label class="col-sm-4 col-form-label">
                                              First Name
                                              <span class="Hilight_Required_Feilds">*</span>
                                             </label>
                                             <div class="col-sm-8">
                                                <input type="text" name="first_name" id="first_name" class="first_name clr-check-client" placeholder="First name" value="" >
                                                <div id="fname-error" class="fname-error" style="color: red"></div>
                                             </div>
                                          </div>

                                          <div class="col-sm-6 form-group row">
                                            <label class="col-sm-4 col-form-label">
                                            Last Name
                                            <span class="Hilight_Required_Feilds">*</span>
                                            </label>
                                             <div class="col-sm-8">
                                                <input class="clr-check-client" type="text" name="last_name" id="last_name" placeholder="Last name" value="" >
                                             </div>
                                          </div>
                                          <div class="clearfix"></div>

                                          <div class="col-sm-6 form-group row name_fields">
                                            <label class="col-sm-4 col-form-label">
                                            Preferred Name
                                            <span class="Hilight_Required_Feilds">*</span>
                                            </label>
                                             <div class="col-sm-8">
                                                <input type="text" name="preferred_name" id="" class="form-control clr-check-client" value="">
                                             </div>
                                          </div>

                                          <div class="col-sm-6 form-group row name_fields">
                                            <label class="col-sm-4 col-form-label">
                                            Relationship to Employee
                                            <span class="Hilight_Required_Feilds">*</span>
                                            </label>
                                             <div class="col-sm-8">
                                                <input type="text" name="relation_employee" id="" class="form-control clr-check-client" value="">
                                             </div>
                                          </div>

                                          <div class="col-sm-6 form-group row name_fields">
                                            <label class="col-sm-4 col-form-label">
                                            Telephone
                                            <span class="Hilight_Required_Feilds">*</span>
                                            </label>
                                            <div class="col-sm-8">
                                              <select class="clr-check-select tele-kin" name="" id="">
                                                <option value="mobile">Mobile</option>
                                                <option value="work">Work</option>
                                                <option value="home">Home</option>
                                                <option value="main">Main</option>
                                                <option value="workfax">Work Fax</option>
                                                <option value="homefax">Home Fax</option>
                                              </select>
                                              <input type="text" class="text-info tele-kin-input clr-check-client" name=""  value="">
                                            </div>
                                          </div>

                                          <div class="clearfix"></div>

                                          <div class="col-sm-6 form-group row name_fields">
                                            <label class="col-sm-4 col-form-label">
                                            Address
                                            <span class="Hilight_Required_Feilds">*</span>
                                            </label>
                                             <div class="col-sm-8">
                                                <input type="text" name="address" id="" class="form-control clr-check-client" value="">
                                             </div>
                                          </div>

                                          <div class="col-sm-6 form-group row name_fields">
                                            <label class="col-sm-4 col-form-label">
                                            Email
                                            <span class="Hilight_Required_Feilds">*</span>
                                            </label>
                                             <div class="col-sm-8">
                                                <input type="text" name="email_id" id="" class="form-control clr-check-client" value="">
                                             </div>
                                          </div>

                                          <div class="clearfix"></div>
 
                                        </div>         
                                      </div>  -->
                                  <!-- Kin Tab Ends -->
                                </div>
                    </form>  
                        <!-- admin close-->
                 </div>
              </div>
           </div>
        </div>
            <!-- Page body end -->
     </div>
  </div>
      <!-- Main-body end -->
      <div id="styleSelector">
      </div>
</div>
</div>
</div>
</div>
</div>
</div>

<div class="modal fade" id="EmailSignature" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-e-signature">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>

                </button>
                 <h4 class="modal-title" id="myModalLabel">E-Signature</h4>

            </div>
            <div class="modal-body">
             <?php $this->load->view('staffs/my_sign.php'); ?>
            </div>
            <div class="modal-footer">
                <button id="btnSaveSign" class="btn btn-default">Save Signature</button>
                <button  id="clearSig" class="btn btn-primary save">Clear Signature</button>
                <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                <!-- <button type="button" class="btn btn-primary save">Save changes</button> -->
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/popper.js/js/popper.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.j-pro.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>bower_components/switchery/js/switchery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/modernizr/js/css-scrollbars.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.filer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/advance-elements/custom-picker.js"></script>
<script src="<?php echo base_url();?>assets/js/pcoded.min.js"></script>
<script src="<?php echo base_url();?>assets/js/demo-12.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/common_script.js"></script>
<script src="<?php echo base_url();?>assets/js/hierarchy-select.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>

<script>
   $( document ).ready(function() {

      $('.country_select_div').dropdown({choice:function(){ 
        console.log( $("select[name='country_code']").length +"inside the choice");
        $("select[name='country_code']").valid(); 
      }
    });
    $('.allocate_service_select_div').dropdown();
   
       var date = $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
   
       // Multiple swithces
       var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));
   
       elem.forEach(function(html) {
           var switchery = new Switchery(html, {
               color: '#22b14c',
               jackColor: '#fff',
               size: 'small'
           });
       });
   
       $('#accordion_close').on('click', function(){
               $('#accordion').slideToggle(300);
               $(this).toggleClass('accordion_down');
       });
   });
</script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript">
   $(document).ready(function(){

    /*    $.extend($.validator.messages, {
    //  required: "This field is required.",
      remote: "",
    
  });*/

      

      /*var it_global='';
      $.validator.addMethod(
    "regex",
    function(value, element, regexp) {
        var check = false;
        return this.optional(element) || regexp.test(value);
    },
    "Please check your input."
);*/
      var check_username = 
      {
         url:'<?php echo base_url();?>firm/check_username/',
         type : 'post',
         data:{
            <?php if(!empty($staff)){?> id:function(){return $('#user_id').val();} <?php } ?>
         },
         beforeSend:function(){
            $('#submit_button').addClass('disabled');
         },
         complete:function()
         {
           $('#submit_button').removeClass('disabled');
         }
      };
      
                      $("#staff_form").validate({
                      ignore: '.ignore',
                      rules: {
                      first_name: {required: true},
                      last_name: {required: true},
                      //email_id: {required: true,email:true},
                      user_name:{
                      required: true,
                      remote: check_username
                      },

                      email_id: {
                      required: true,
                      email: true
                      },
                      telephone_number :
                      {                     
                      number: true
                      },    
                      /*country_code :
                      {
                      required:true,                              
                      },  */                                        
                      //  linkedin : {required: true},

                      password:{
                      required: true,
                      minlength:5
                      },
                      /*image_upload: {required: false, accept: "jpg|jpeg|png|gif"},*/
                      //                        image_upload: {
                      //     required: function(element) {

                      //     if ($("#image_name").val().length > 0) {
                      //        return true;
                      //     }
                      //     else {
                      //     return false;
                      //     }
                      //   },
                      //   accept: "jpg|jpeg|png|gif"
                      // },
                      hourly_rate: {required: true,number:true},
                      roles:{required: true},
                      },
                      errorPlacement:function(error , element)
                       {
                          if( $(element).attr('name') == "country_code")
                          {
                            error.insertAfter( $("input[name='telephone_number']") ); 
                          }
                          else
                          {
                            error.insertAfter( $(element) );  
                          }
                       },
                      /*errorElement: "span" , 
                      errorClass: "field-error",  */                           
                      messages: {
                      first_name: "Enter a first name",
                      last_name:  "Enter a last name",
                      user_name:{ 
                      required: "Enter User name",
                      remote: "Username Name already exists.",
                      },
                      roles:"Select a role",

                      //                           email_id: {
                      //     email:"Please enter a valid email address", 
                      //     required:"Please enter a email address",

                      // },
                      email_id: "Please enter a valid email address",
                      telephone_number:{  yourtel:"Enter a valid phone number.",
                      required: "Enter a phone number."},
                      //  linkedin:"Enter a URL",
                      password: {required: "Please enter your password "},
                      // image_upload: {required: 'Required!', accept: 'Not an image!'},

                      hourly_rate: {required: "Please Enter Your rate",number:"Please enter numbers Only"},
                      },



                      submitHandler: function(form)
                      {
                        

                      var formData = new FormData($("#staff_form")[0]);

                      $(".LoadingImage").show();

                        $.ajax({
                          url: '<?php echo base_url();?>staff/add_staff',
                          dataType : 'json',
                          type : 'POST',
                          //enctype: 'multipart/form-data',
                          data : formData,
                          contentType : false,
                          processData : false,
                          //cache: false,
                          success: function(data)
                          {
                            // alert(data); 
                            if(data == 1)
                            {
                              var message       = "User Save Successfully";
                              var redirect_url  = "<?php echo base_url()?>user/staff_list";

                              if( $('input.is_profile_page').val() != 0 )
                              {
                                message       = "Profile Update Successfully";
                                redirect_url  = "<?php echo base_url()?>staff/user_profile";
                                
                              }
                              $('.alert-success').find('.position-alert1').html( message );
                              $('.alert-success').show();
                              setTimeout(function(){
                              $('.alert-danger').hide();
                              window.location.href= redirect_url ;
                              },1000)
                            }
                            else
                            {
                              // alert('failed');
                              $('.alert-danger').show();
                              $('.alert-success').hide();
                            }
                            $(".LoadingImage").hide();
                          },
                          error: function()
                          {
                            $('.alert-danger').show();
                            $('.alert-success').hide();
                            $(".LoadingImage").hide();
                          }
                        });
                      
                        return false;
                      } /*,
                      invalidHandler: function(e, validator) {
                      if(validator.errorList.length)
                      $('#tabs a[href="#' + jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')

                      }*/

                      });
   
   $('#roles_select').hierarchySelect({
            width: 'auto'
        });
      
      <?php if(isset($staff['roles'])) {?>

         var role = <?php echo $staff['roles'];?>;
         $('.hs-menu-inner').find('a[data-value="'+role+'"]').trigger('click');

      <?php } ?> 
   });

</script>
<script src="<?php echo base_url();?>assets/js/mock.js"></script>

<script type="text/javascript">
       function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#preview_image')
                    .attr('src', e.target.result)
                    .width(225)
                    .height(225);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

<!-- <script type="text/javascript">
                                          $('.dept').click(function() {
                                             var a = $(this).val();
                                             alert(a);
                                          });
                                             
                                          </script> --> 





<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.jquery.min.js"></script> -->
<!-- <script type="text/javascript">
   $(".chosen-select").chosen();
$('button').click(function(){
        $(".chosen-select").val('').trigger("chosen:updated");
});
</script> -->

<?php //$this->load->view('staffs/scripts.php');?>

<script src="<?php echo base_url(); ?>assets/signature/js/numeric-1.2.6.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/signature/js/bezier.js"></script>
<script src="<?php echo base_url(); ?>assets/signature/js/jquery.signaturepad.js"></script>   

<script type='text/javascript' src="https://github.com/niklasvh/html2canvas/releases/download/0.4.1/html2canvas.js"></script>
<script src="<?php echo base_url(); ?>assets/signature/js/json2.min.js"></script>

<script>
  $(document).ready(function(){
    $("#kin_information").click(function(){
      $(".profile-kin-data").hide();
      $(".edit-staff").hide();
      $(".save-kin-info").show();
      $(".kin-info-data").show();
    });

    $("#required_information").click(function(){
      $(".kin-info-data").hide();
      $(".save-kin-info").hide();
      $(".edit-staff").show();
      $(".profile-kin-data").show();
    });    
  })
</script>

<script type="text/javascript">

$('#email_signature').on('change', function(e){
   if(e.target.checked){
     $('.for_sign').css('display','');
     $('#EmailSignature').modal();
   }
   else
   {
      $('.for_sign').css('display','none');
   }
});

// var sig = [{lx:20,ly:34,mx:20,my:34},{lx:21,ly:33,mx:20,my:34},{lx:20,ly:34,mx:20,my:34},{lx:21,ly:33,mx:20,my:34},{lx:20,ly:34,mx:20,my:34},{lx:21,ly:33,mx:20,my:34},{lx:20,ly:34,mx:20,my:34},{lx:21,ly:33,mx:20,my:34},{lx:20,ly:34,mx:20,my:34},{lx:21,ly:33,mx:20,my:34},{lx:20,ly:34,mx:20,my:34},{lx:21,ly:33,mx:20,my:34}];

$(document).ready(function () {

  //$('#signArea').signaturePad({displayOnly:true}).regenerate(sig);
 

});

     $(document).ready(function() {

      $(window).scroll(function() {
  var sticky = $('#option-tabs'),
    scroll = $(window).scrollTop();
   
  if (scroll >= 40)
  { 
    sticky.addClass('fixed'); 
 }
  else 
  { 
    sticky.removeClass('fixed');
  }
});


 $(window).scroll(function() {
 var sticky1 = $('#mail-template, .container-fullscreen'),
   scroll = $(window).scrollTop();
  
 if (scroll >= 40) { 
   sticky1.addClass('fixed1'); }
 else { 
  sticky1.removeClass('fixed1');

}
});
  
   //   $('#signArea').signaturePad({drawOnly:true, drawBezierCurves:true, lineTop:90});
     //$('#signArea').signaturePad({displayOnly:true}).regenerate(sig);

   <?php if($get_sign_json!=''){ ?>
  var zz='<?php echo $get_sign_json; ?>';
  $('#signArea').signaturePad({drawOnly:true, drawBezierCurves:true, lineTop:90}).regenerate(zz);
   <?php }else{ ?>
  $('#signArea').signaturePad({drawOnly:true, drawBezierCurves:true, lineTop:90});
   <?php } ?>

      });   

     $("#clearSig").click(function clearSig() {

      $('#signArea').signaturePad().clearCanvas();
        });

   $("#btnSaveSign").click(function(e){
    //  console.log($('.output').val());
      var json_data=$('.output').val();
    // alert('zzz');
   //   var sig = $('#sign-pad').signaturePad();
    //  alert(sig.signaturePad('toJSON'));
          html2canvas([document.getElementById('sign-pad')], {
          onrendered: function (canvas) {

            var canvas_img_data = canvas.toDataURL('image/png');
            var img_data = canvas_img_data.replace(/^data:image\/(png|jpg);base64,/, "");
            //ajax call to save image inside folder
            $.ajax({
              url: '<?php echo base_url(); ?>/Staff/save_sign',
              data: { img_data:img_data,json_data:json_data },
              type: 'post',
              dataType: 'json',
              success: function (response) {    
               console.log('sign data');
              console.log(response);
              //alert(response);
                $("#imgData").html('Thank you! Your signature was saved');
                $("#imgData").show();
                //$('#sign_data').val(response);
              
                $('#sign_data').val(response['insert_data']);

                $('.for_sign icon-forsign').html('<div class="attch-trash01"> <img src="<?php echo base_url(); ?>'+response['file_name']+'"><span class="remove_sign"><a class="icon-jfi-trash jFiler-item-trash-action remove_file"></a></span></div>');

                $('.for_sign').css('display','');
                setTimeout(function(){  $("#imgData").hide();  }, 2000);
                setTimeout(function(){   $("#EmailSignature").modal('hide'); }, 2000);
                 //window.location.reload();
              }
            });
          }
        });
      });  
</script>
<script type="text/javascript">
 $(document).ready(function() {
   /*$('.username').on('input', function() {
      var fname = $(this).val();
      var user_id=$('#user_id').val();
      console.log(user_id);
      $.ajax({
        type: "POST",
        url: "<?php echo base_url().'staff/Firstname_Check';?>",
        data: {"fname": fname,"user_id":user_id},
        success:function(res) {
         
         if(res == 1) {
            $('.fname-error').html("Username Name already exists.");
         } else {
            $('.fname-error').html('');
            
         }
        }

     });


   });*/

  
 });  
 /** 31-07-2018 **/
 $(document).on('click','.remove_sign',function(){
$('.for_sign').css('display','none');
$('#sign_data').val('');
//$('#email_signature').prop('checked','');
var element = $('#email_signature');
changeSwitchery(element, false);
 });

 $('.country_code').on('change input', function() {
  $(this).val($(this).val().replace(/([^+0-9]+)/gi, ''));
});
 $('#telephone_number').on('change input', function() {
  $(this).val($(this).val().replace(/([^0-9]+)/gi, ''));
});
 /** end of 31-07-2018 **/
 function changeSwitchery(element, checked) {
  if ( ( element.is(':checked') && checked == false ) || ( !element.is(':checked') && checked == true ) ) {
    element.parent().find('.switchery').trigger('click');
  }
}

/** for addstaff page prev and next button **/

jQuery( document ).ready(function( $ ) {

// next click
 $('.addstaff-signed-change3').click(function(){
  $('ul.nav-tabs .active').closest('li').nextAll("li:visible").eq(0).find('a').trigger('click').parent().show();
   var current= $('ul.nav-tabs .active').attr('href');

   var last=$('ul.nav-tabs .nav-link').closest('li').nextAll("li:visible").last().find('a').attr('href');

     if(current==last)
     {
       $('.addstaff-signed-change3').hide();
       $('.addstaff-signed-change2').show();
     }
     else
     {
       $('.addstaff-signed-change3').show();
     }

  var current_one= $('ul.nav-tabs .active').attr('href');

  var last_one=$('ul.nav-tabs .nav-link').closest('li.current_li').prevAll("li.current_li:visible").last().find('a').attr('href');
     if(current_one==last_one)
     {
       $('.addstaff-signed-change2').hide();
       $('.addstaff-signed-change3').show();
     }
     else
     {
       $('.addstaff-signed-change2').show();
     }
  //alert(zz);
});


// prev click
  $('.addstaff-signed-change2').click(function(){
    $('ul.nav-tabs .active').closest('li').prevAll('li:visible').eq(0).find('a').trigger('click');
    $('.addstaff-signed-change3').show();

   var current= $('ul.nav-tabs .active').attr('href');

   var last=$('ul.nav-tabs .nav-link').closest('li.current_li').prevAll("li.current_li:visible").last().find('a').attr('href');
        if(current==last)
        {
          $('.addstaff-signed-change2').hide();
          $('.addstaff-signed-change3').show();
        }
        else
        {
          $('.addstaff-signed-change2').show();
        }
   });

   $('ul.nav-tabs li a').click(function(){
      var $this = $(this);
      $('.nav-tabs li').removeClass('active-previous');
      $this.closest('li').prevAll().addClass('active-previous');


       var current= $this.attr('href');

       var last=$('ul.nav-tabs .nav-link').closest('li').nextAll("li:visible").last().find('a').attr('href');
            if(current==last)
              {
                $('.addstaff-signed-change3').hide();
                $('.addstaff-signed-change2').show();
              }
              else
              {
                $('.addstaff-signed-change3').show();
              }

     var current_one=  $this.attr('href');

     var last_one=$('ul.nav-tabs .nav-link').closest('li.current_li').prevAll("li.current_li:visible").last().find('a').attr('href');
              if(current_one==last_one)
              {
                $('.addstaff-signed-change2').hide();
                $('.addstaff-signed-change3').show();
              }
              else
              {
                $('.addstaff-signed-change2').show();
              }
      });

  }); 

// $('.addstaff-signed-change2').click(function(){
//   $('ul.nav-tabs li.nav-item').each(function(){
//                var zz=$(this).find('a').attr('href');
//                $('#main_Contact').removeClass('active');
//                $(this).find('a').removeClass('active');
//                if(zz=='#required_information')
//                {
//                  $(this).find('a').addClass('active');
//                  $('#required_information').addClass('active');
//                  $('.Footer.common-clienttab.pull-right > div.divleft').css('display','none');
//                  $('.Footer.common-clienttab.pull-right > div.divright').css('display','');
//                 //  $(activeTab).show();
//                   //$(this).find('a').trigger('click');
//                }

//             });
//       });
// $('.addstaff-signed-change3').click(function(){
//   $('ul.nav-tabs li.nav-item').each(function(){
//                var zz=$(this).find('a').attr('href');
//                $('#required_information').removeClass('active');
//                $(this).find('a').removeClass('active');
//                if(zz=='#main_Contact')
//                {
//                  $(this).find('a').addClass('active');
//                  $('#main_Contact').addClass('active');
//                  $('.Footer.common-clienttab.pull-right > div.divright').css('display','none');
//                  $('.Footer.common-clienttab.pull-right > div.divleft').css('display','');
//                 //  $(activeTab).show();
//                   //$(this).find('a').trigger('click');
//                }

//             });
//       });
/** end of addstaff **/


</script>

<script>
	$(document).ready(function(){
		$(".clr-check-client").keyup(function(){
			var clr = $(this).val();
			if(clr.length >= 3){
				$(this).addClass("clr-check-client-outline");
			}else{
				$(this).removeClass("clr-check-client-outline");
			}
		});
    $(".clr-check-client").each(function(i){
      if($(this).val() !== ""){
        $(this).addClass("clr-check-client-outline");
      }  
    })

		$(".clr-check-select").change(function(){
			var clr_select = $(this).val();
         if(clr_select !== ' '){
            $(this).addClass("clr-check-select-client-outline");
         }else{
				$(this).removeClass("clr-check-select-client-outline");
			}
		});
    $(".clr-check-select").each(function(i){
      if($(this).val() !== ""){
        $(this).addClass("clr-check-select-client-outline");
      }  
    })
	});

</script>

<script type="text/javascript">
   $(document).ready(function(){
      $('.dept-checkbox-danger .custom_checkbox1 input').after("<i></i>");
   });
</script>


</body>
</html>