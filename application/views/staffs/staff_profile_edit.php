<?php $this->load->view('includes/header');?>
<!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.css"> -->

<link href="<?php echo base_url();?>assets/css/jquery.filer.css" type="text/css" rel="stylesheet" />
<!-- <link href="<?php echo base_url();?>assets/css/jquery.filer-dragdropbox-theme.css" type="text/css" rel="stylesheet" /> -->
<?php
$get_sign_json='';
  if(isset($staff[0]['signature_path']) && $staff[0]['signature_path']!='' && $staff[0]['signature_path']!= null)
   { 
   $get_sign_jsons=$this->Common_mdl->get_field_value('pdf_signatures','signature_json_data','id',$staff[0]['signature_path']);
   $get_sign_json=$get_sign_jsons;
   }
?>
<div class="pcoded-content permiss_stafflist">
   <div class="pcoded-inner-content card-removes">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card profile-edit-wrapper">
                        <!-- admin start-->
                        <?php 
                           // echo "<pre>";
                            //print_r($staff);
                           if(isset($staff[0]['permission'])) {
                           $permission=$staff[0]['permission'];
                           $staa=json_decode($permission);
                           }
                           //print_r($staa);
                           //echo $staa->pdf->global;
                           
                           // echo "</pre>"; ?>
                        <div class="modal-alertsuccess alert alert-success" style="display:none;">
                           <div class="newupdate_alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                           <div class="pop-realted1">
                              <div class="position-alert1">
                                 Your record was successfully.
                              </div>
                           </div></div>
                        </div>

                        <div class="modal-alertsuccess alert alert-danger" style="display:none;">
                           <div class="newupdate_alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                           <div class="pop-realted1">
                              <div class="position-alert1">
                                 Please fill the required fields.
                              </div></div>
                           </div>
                        </div>
                        <form id="staff_form" method="post" action="" enctype="multipart/form-data">
                        <?php
                         $get_author_user=$this->db->query('select * from user where id='.$_SESSION['id'].' ')->row_array();
      //  echo 'select * from user where id='.$user_id.'';
        $firm_admin_id='';
        if(count($get_author_user)>0)
        {
          $firm_admin_id=$get_author_user['firm_admin_id'];
        } ?>
                        <input type="hidden" name="user_firm_id" id="user_firm_id" value="<?php echo $firm_admin_id; ?>">
                        <input type="hidden" name="sign_data" id="sign_data" value="<?php if(isset($staff[0]['signature_path'])){ echo $staff[0]['signature_path']; } ?>">
                           <!--  <div class="addnewclient_pages1 floating_set">
                              <ul class="nav nav-tabs all_user1 md-tabs floating_set" id="tabs">
                              <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#required_information">Profile</a></li>
                              <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#main_Contact">Permissions</a></li> 
                              </ul>   
                              
                              </div> -->
                           <div class="deadline-crm1 floating_set">
                              <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                                
                                 <li class="nav-item current_li"><a class="nav-link active" data-toggle="tab" href="#required_information">Profile</a><div class="slide"></div></li>
                               
                                 <?php if(!empty($custom_form['labels'])){  ?> 
                                 <li class="nav-item current_li"><a class="nav-link" data-toggle="tab" href="#main_cutom">Custom Fields</a><div class="slide"></div></li>
                                 <?php } ?>
                              </ul>
                              <!--<div class="f-right ad-staff">
                           <?php  if(isset($staff[0]['id']) && ($staff[0]['id'])!='' ){ ?>

                                 <input type="submit" class="add_acc_client f-right edit-staff" value="Edit Staff" id="submit" />
                           <?php } else { ?>
                                 <input type="submit" class="add_acc_client f-right add-staff" value="Add Staff" id="submit" />
                           <?php } ?>   
                                 
                                 <input type="hidden" class="staff_id" value="<?php if(isset($staff[0]['id']) && ($staff[0]['id'])!='' ){ echo $staff[0]['id'];} ?>">
                              </div> -->
                              <div class="Footer common-clienttab pull-right">
                                       <div class="change-client-bts1">
                                         <?php  if(isset($staff[0]['id']) && ($staff[0]['id'])!='' ){ ?>
                                             <input type="submit" class="add_acc_client f-right edit-staff" value="Update Profile" id="submit" />
                                       <?php } else { ?>
                                             <input type="submit" class="add_acc_client f-right add-staff" value="Add Staff" id="submit" />
                                       <?php } ?>                                    
                                 <input type="hidden" name="staff_id" id="staff_id" class="staff_id" value="<?php if(isset($staff[0]['id']) && ($staff[0]['id'])!='' ){ echo $staff[0]['id'];} ?>">
                                       </div>
                                       <div class="divleft">
                                          <button class="addstaff-signed-change2" type="button" value="Previous Tab" text="Previous Tab" style="display: none;">Previous 
                                          </button>
                                       </div>
                                       <div class="divright">
                                          <button class="addstaff-signed-change3" type="button" value="Next Tab" text="Next Tab" <?php if(empty($custom_form['labels'])){ ?> style="display:none;" <?php  }?> >Next
                                          </button>
                                       </div>
                                    </div>
                           </div>
                           <div class="management_section staff_mrng permission_staff floating_set">
                              <div class="card-block accordion-block">
                                 <!-- tab start -->
                                 <div class="tab-content">
                                    <div id="required_information" class="border-checkbox-section tab-pane fade in active">
                                       <div class="floating_set text-right accordion_ups isset-accor">
                                          <input type="hidden" name="user_id" value="<?php if(isset($staff[0]['user_id']) && ($staff[0]['user_id'])!='' ){ echo $staff[0]['user_id'];}?>">
                                        
                                       </div>
                                       <div class="management_form1">
                                          <!-- <div class="col-sm-6 form-group row">
                                             <label class="col-sm-4 col-form-label">Enable Two factor authentication</label>
                                             <div class="col-sm-8  checkbox-color checkbox-primary">
                                                <input class="border-checkbox" type="checkbox" name="two_factor" id="two_factor" value="1" <?php if(isset($staff[0]['two_factor']) && $staff[0]['two_factor']==1) {?> checked="checked"<?php } ?>>
                                                <label class="border-checkbox-label"  for="two_factor"></label>
                                             </div>
                                          </div> -->
                                          <!-- <div class="col-sm-6 form-group row">
                                             <label class="col-sm-4 col-form-label">Not staff member</label>
                                             <div class="col-sm-8 checkbox-color checkbox-primary">
                                                <input class="border-checkbox" type="checkbox" name="not_staff" id="not_staff" value="1" <?php if(isset($staff[0]['not_staff']) && $staff[0]['not_staff']==1) {?> checked="checked"<?php } ?>>
                                                <label class="border-checkbox-label"  for="not_staff"></label>
                                             </div>
                                          </div> -->
                                          
                                          <div class="col-sm-6 form-group row name_fields">
                                             <label class="col-sm-4 col-form-label">First Name</label>
                                             <div class="col-sm-8">
                                                <input type="text" name="first_name" id="first_name" class="first_name" placeholder="First name" value="<?php if(isset($staff[0]['first_name']) && ($staff[0]['first_name']!='') ){ echo $staff[0]['first_name'];}?>" >
                                               <!--  <div id="fname-error" class="fname-error" style="color: red"></div> -->
                                             </div>
                                          </div>
                                          <div class="col-sm-6 form-group row">
                                             <label class="col-sm-4 col-form-label">Last Name</label>
                                             <div class="col-sm-8">
                                                <input type="text" name="last_name" id="last_name" placeholder="Last name" value="<?php if(isset($staff[0]['last_name']) && ($staff[0]['last_name']!='') ){ echo $staff[0]['last_name'];}?>" >
                                             </div>
                                          </div>
                                          <div class="col-sm-6 form-group row name_fields">
                                             <label class="col-sm-4 col-form-label">Email</label>
                                             <div class="col-sm-8">
                                                <input type="mail" name="email_id" id="email_id" class="form-control" value="<?php if(isset($staff[0]['email_id']) && ($staff[0]['email_id']!='') ){ echo $staff[0]['email_id'];}?>">
                                             </div>
                                          </div>

                                          <div class="clearfix"></div>

                                          <div class="col-sm-6 form-group row name_fields">
                                             <label class="col-sm-4 col-form-label">Hourly Rate</label>
                                             <div class="col-sm-8">
                                                <input type="text" name="hourly_rate" id="hourly_rate" placeholder="Hourly rate" value="<?php if(isset($staff[0]['hourly_rate']) && ($staff[0]['hourly_rate']!='') ){ echo $staff[0]['hourly_rate'];}?>">
                                             </div>
                                          </div>
                                          <div class="col-sm-6 form-group row name_fields">
                                             <label class="col-sm-4 col-form-label">Phone Number</label>
                                       <!-- <?php $countries=$this->db->query("SELECT * FROM countries")->result_array(); 
                                          if(isset($staff[0]['telephone_number'])) {
                                             $country = explode('-', $staff[0]['telephone_number']); 
                                             } ?> -->
                                       <?php 
                                       if(isset($staff[0]['telephone_number']) && ($staff[0]['telephone_number']!='') ){ $ctry=explode('-', $staff[0]['telephone_number']); }
                                       
                                       ?>             

                                             <div class="col-sm-8">
                                                <input type="text" name="country_code" class="country_code" style="width:80px;" value="<?php if(isset($ctry[0]) && $ctry[0]!='') { echo $ctry[0]; }?>" placeholder="Code">
                                                <!-- <select name="country_code" class="country_code" style="width:80px;">
                                                   <?php foreach ($countries as $key => $ctryval) { ?>
                                                     
                                                      <option value="<?php echo $ctryval['phonecode'];?>" <?php if(isset($country[0]) && $ctryval['phonecode'] == $country[0]) { echo "selected='selected'"; } ?> ><?php echo $ctryval['phonecode'];?></option>
                                                   <?php } ?>
                                                </select> -->

                                                <!-- <input type="tel" name="telephone_number" id="telephone_number" class="form-control" value="<?php if(isset($staff[0]['telephone_number']) && ($staff[0]['telephone_number']!='') ){ echo $country[1];}?>" placeholder="Phone number"> -->
                                                <input type="tel" name="telephone_number" id="telephone_number" class="form-control" value="<?php if(isset($country[1]) && ($country[1]!='') ){ echo $ctry[1];}?>" placeholder="Phone number">
                                             </div>
                                          </div>
                                          <div class="col-sm-6 form-group row name_fields">
                                             <label class="col-sm-4 col-form-label">Facebook</label>
                                             <div class="col-sm-8">
                                                <input type="text" name="facebook" id="facebook" placeholder="facebook url" value="<?php if(isset($staff[0]['facebook']) && ($staff[0]['facebook']!='') ){ echo $staff[0]['facebook'];}?>">
                                             </div>
                                          </div>

                                          <div class="clearfix"></div>

                                          <div class="col-sm-6 form-group row name_fields">
                                             <label class="col-sm-4 col-form-label">Linkedin</label>
                                             <div class="col-sm-8">
                                                <input type="text" name="linkedin" id="linkedin" placeholder="Linkedin url" value="<?php if(isset($staff[0]['linkedin']) && ($staff[0]['linkedin']!='') ){ echo $staff[0]['linkedin'];}?>">
                                             </div>
                                          </div>
                                          <div class="col-sm-6 form-group row name_fields">
                                             <label class="col-sm-4 col-form-label">Skype</label>
                                             <div class="col-sm-8">
                                                <input type="text" name="skype" id="skype" placeholder="Skype url" value="<?php if(isset($staff[0]['skype']) && ($staff[0]['skype']!='') ){ echo $staff[0]['skype'];}?>">
                                             </div>
                                          </div>
                                       <!-- 30-07-2018 remove
                                          <div class="col-sm-6 form-group row turnover">
                                             <label class="col-sm-4 col-form-label">Default Language</label>
                                             <div class="col-sm-8">
                                                <select name="language" class="form-control" id="language">
                                                   <option value="system default" <?php if(isset($staff[0]['language']) && $staff[0]['language']=='system default') {?> selected="selected"<?php } ?>>system default</option>
                                                   <option value="english" <?php if(isset($staff[0]['language']) && $staff[0]['language']=='english') {?> selected="selected"<?php } ?>>English</option>
                                                   <option value="french" <?php if(isset($staff[0]['language']) && $staff[0]['language']=='french') {?> selected="selected"<?php } ?>>French</option>
                                                </select>
                                             </div>
                                          </div> -->
                                         
                                          <!-- <div class="col-sm-6 form-group row turnover">
                                             <label class="col-sm-4 col-form-label">Direction</label>
                                             <div class="col-sm-8">
                                                <select name="direction" class="form-control" id="direction">
                                                   <option value="opt1" <?php if(isset($staff[0]['direction']) && $staff[0]['direction']=='opt1') {?> selected="selected"<?php } ?>>opt1</option>
                                                   <option value="opt2" <?php if(isset($staff[0]['direction']) && $staff[0]['direction']=='opt2') {?> selected="selected"<?php } ?>>opt2</option>
                                                </select>
                                             </div>
                                          </div> -->
                                          <div class="col-sm-6 form-group row " style="display: none;">
                                             <label class="col-sm-4 col-form-label">Member Departments</label>
                                             <div class="col-sm-8 ">
                                                <div class="dropdown-sin-2">
                                                <!--  cus-checkbox1-->
                                             <select id="department" name="department[]" placeholder="Nothing Selected" multiple >
                                                
                                          <?php 
                                       foreach ($new_department as $departments_key => $departments_value) { 

                                               if(isset($staff[0]['department'])) {       
                                          $departments = explode(',', $staff[0]['department']); }  ?>  
                                                <option value=""></option> 
                                                <!-- <option value="<?php echo $departments_value['id'];?>" <?php if($departments == $departments_value['id']) {
                                                  echo "selected='selected'"; } ?>><?php echo $departments_value['new_dept'];?></option> -->
                                                <option value="<?php echo $departments_value['id'];?>" <?php echo in_array($departments_value['id'], $departments) ? 'selected' : ''; ?>><?php echo $departments_value['new_dept'];?></option>    

                                             <?php }  ?>   
                                                </select>
                                                </div>
                                             </div> 
                                                <?php 
                                                   //print_r($staff);die;
                                          //          foreach ($new_department as $departments_key => $departments_value) { 
                                          //      if(isset($staff[0]['department'])) {       
                                          // $departments = explode(',', $staff[0]['department']); } 

                                                      ?>          
                                                <!-- <span class="member-options">
                                                   <input type="radio" name="department" id="check<?php echo $departments_value['id'];?>" <?php if( isset( $staff[0]['department']) && $staff[0]['department']==$departments_value['id']) {?> checked="checked"<?php } ?> value="<?php echo $departments_value['id'];?>">
                                                     <label for="check<?php echo $departments_value['id'];?>"></label>
                                                   <span class="check-text"> <?php echo $departments_value['new_dept'];?></span>
                                                   </span> -->
                                                <!-- <div class="member-options radio radio-inline">

                                                   <label for="check<?php echo $departments_value['id'];?>"> -->
                                                   <!-- <input type="radio" name="department" id="check<?php echo $departments_value['id'];?>" <?php if( isset( $staff[0]['department']) && $staff[0]['department']==$departments_value['id']) {?> checked="checked"<?php } ?> value="<?php echo $departments_value['id'];?>">
                                                   <i class="helper"></i> -->

                                             

                                                  <!--  <input type="checkbox" name="department[]" class="dept" id="check<?php echo $departments_value['id'];?>" <?php if( isset( $departments) && $departments==$departments_value['id']) {?> checked="checked"<?php } ?> value="<?php echo $departments_value['id'];?>">
                                                   <i class="helper"></i> 
                                                   </label>
                                                  
                                                   <span><?php echo $departments_value['new_dept'];?> </span> -->
                                               <!--  </div> -->
                                                
                                                <!--  <span class="member-options">
                                                   <input type="checkbox" name="marketing" id="check1" <?php if(isset($staff[0]['marketing']) && $staff[0]['marketing']=='on') {?> checked="checked"<?php } ?>>
                                                    <label for="check1"></label>
                                                   <span class="check-text"> Marketing</span>
                                                   </span>
                                                   <span class="member-options">
                                                   <input type="checkbox" name="sales" id="check2" <?php if(isset($staff[0]['sales']) && $staff[0]['sales']=='on') {?> checked="checked"<?php } ?>> 
                                                   <label for="check2"></label>
                                                   <span class="check-text">Sales</span>
                                                   </span>
                                                   <span class="member-options">
                                                   <input type="checkbox" name="abuse" id="check3" <?php if(isset($staff[0]['abuse']) && $staff[0]['abuse']=='on') {?> checked="checked"<?php } ?>>
                                                   <label for="check3"></label> <span class="check-text">Abuse</span>
                                                   </span>
                                                   <span class="member-options">
                                                   <input type="checkbox" name="logistics" id="check4" <?php if(isset($staff[0]['logistics']) && $staff[0]['logistics']=='on') {?> checked="checked"<?php } ?>>
                                                   <label for="check4"></label>
                                                    <span class="check-text">Logistics</span>
                                                   </span>
                                                   <span class="member-options">
                                                   <input type="checkbox" name="administrator" id="check5" <?php if(isset($staff[0]['administrator']) && $staff[0]['administrator']=='on') {?> checked="checked"<?php } ?>>
                                                   <label for="check5"></label>
                                                   <span class="check-text"> Administrator</span>
                                                   </span> -->
                                                <!-- <span class="member-options">
                                                   <input class="border-checkbox" type="checkbox" name="welcome_mail" id="check6" <?php if(isset($staff[0]['welcome_mail']) && $staff[0]['welcome_mail']=='on') {?> checked="checked"<?php } ?>>
                                                   <label class="border-checkbox-label" for="check6"> </label>
                                                   <span class="check-text"> Send welcome mail</span>
                                                   </span> -->
                                                <!-- <div class="member-options border-checkbox-group border-checkbox-group-primary">
                                                   <input class="border-checkbox" type="checkbox" name="welcome_mail" id="check6" <?php if(isset($staff[0]['welcome_mail']) && $staff[0]['welcome_mail']=='on') {?> checked="checked"<?php } ?>>
                                                               <label class="border-checkbox-label" for="check6"> Send welcome mail </label>
                                                   </div> -->
                                             </div>
                                          <!-- </div> -->
                                          <!-- start of 20-08-2018 -->

                                          <div class="clearfix"></div>
                                          
                                          <div class="col-sm-6 form-group row name_fields" >
                                             <label class="col-sm-4 col-form-label">User Name</label>
                                             <div class="col-sm-8">
                                                <input type="text" name="username" id="username" class="username" placeholder="First name" value="<?php if(isset($staff[0]['username']) && ($staff[0]['username']!='') ){ echo $staff[0]['username'];}?>" >
                                                <div id="fname-error" class="fname-error field-error" style="color: red"></div>
                                             </div>
                                          </div>
                                          <!-- end of 20-08-2018 -->
                                          <div class="col-sm-6 form-group row" style="display: none;">
                                             <label class="col-sm-4 col-form-label">Password</label>
                                             <div class="col-sm-8">
                                                <input type="password" name="password" id="password" placeholder="Password" value="<?php if(isset($staff[0]['confirm_password']) && ($staff[0]['confirm_password']) ){ echo $staff[0]['confirm_password'];}?>">
                                             </div>
                                          </div>
                                    <div class="col-sm-6 form-group row radio_bts-staff ">
                                             <label class="col-sm-4 col-form-label">Email Signature</label>
                                             <div class="col-sm-8">
                                                <!-- <textarea rows="4" name="email_signature" id="email_signature" class="form-control"><?php if(isset($staff[0]['email_signature']) && ($staff[0]['email_signature']!='') ){ echo $staff[0]['email_signature'];}?></textarea> -->

                                                <input type="checkbox" class="js-small f-right fields" id="email_signature" name="email_signature" <?php if( isset($staff[0]['email_signature']) && $staff[0]['email_signature']=='on'){ echo "checked";} ?>>
                                                <!-- 31-07-2018  for images sign -->
                                             <div class="for_sign" <?php if( isset($staff[0]['email_signature']) && $staff[0]['email_signature']!='on'){ ?> style="display: none;" <?php } ?>>
                                                <?php
                                                if(isset($staff[0]['signature_path']) && $staff[0]['signature_path']!='')
                                                { 
                                                $get_sign_data=$this->Common_mdl->get_field_value('pdf_signatures','signature_path','id',$staff[0]['signature_path']);
                                                   ?>
                                                   
                                             <img src="<?php echo base_url().$get_sign_data; ?>">
                                             <span class="remove_sign" style="color: red;">Remove</span>
                                            
                                                <?php } ?> 
                                             </div>                                                
                                                <!-- end of 31-07-2018 -->
                                             </div>
                                          </div>
                                <!-- <div class="col-sm-6 form-group row attach-files file-attached1">
                                             <label class="col-sm-4 col-form-label">Profile Image</label>
                                             <div class="input-container col-sm-8">
                                                <input type="file" name="image_upload" id="image_upload" >
                                                <input type="hidden" name="image_name" id="image_name" value="<?php if(isset($staff[0]['profile']) && ($staff[0]['profile']!='') ){ echo $staff[0]['profile'];}?>">
                                             </div>
                                          </div> -->
                                 <div class="col-sm-6 form-group row attach-files file-attached1">
                                             <label class="col-sm-4 col-form-label">Profile Image</label>
                                             <div class="col-sm-8">
                                                <div class="custom_upload">
                                                <input type="file" name="image_upload" id="image_upload" onchange="readURL(this);">
                                               </div>
                                                <input type="hidden" name="image_name" id="image_name" value="<?php if(isset($staff[0]['profile']) && ($staff[0]['profile']!='') ){ echo $staff[0]['profile'];}?>">

                                             <?php
                                             if(isset($staff[0]['profile']) && $staff[0]['profile']!='') { ?>
                                              <div class="custom_edit_update">  <img src="<?php echo base_url().'uploads/'.$staff[0]['profile']?>" id="preview_image" width="225" height="225"> </div>
                                             <?php } else { ?>
                                                <div class="custom_edit_update">
                                                <img src="<?php echo base_url().'uploads/profile_image/avatar'?>" id="preview_image" > </div>
                                             <?php } ?>   
                                                
                                             </div>
                                             
                                          </div>         
                                          <!-- tab1 -->
                                       </div>
                                    </div>
                                    <!-- 1tab close-->
                                      <div id="main_Contact" class="tab-pane fade">

                                         

                                       <div class="management_form1 management_accordion">

                                          <div class="form-group row">
                                             <label class="col-sm-3 col-form-label">Role</label>
                                             <div class="col-sm-9">
                                                <select name="roles" id="roles" class="form-control">
                                                   <option value="">--Select option--</option>
                                                   <?php foreach($roles as $role){?>
                                                   <option value="<?php echo $role['id'];?>" <?php if(isset($staff[0]['roles']) && $staff[0]['roles']== $role['id'] ) {?> selected="selected"<?php } ?>><?php echo $role['role'];?></option>
                                                   <?php } ?>
                                                   <!-- <option value="employee" <?php if(isset($staff[0]['roles']) && $staff[0]['roles']=='employee') {?> selected="selected"<?php } ?>>Employee</option>
                                                      <option value="opt2" <?php if(isset($staff[0]['roles']) && $staff[0]['roles']=='opt2') {?> selected="selected"<?php } ?>>Type 2</option>
                                                      <option value="opt3" <?php if(isset($staff[0]['roles']) && $staff[0]['roles']=='opt3') {?> selected="selected"<?php } ?>>Type 3</option> -->
                                                </select>
                                             </div>
                                          </div>
                                          <!-- <div class="form-group row"> -->
                                          <div class="col-xs-12 col-sm-12">
                                             <div class="table-responsive  ">
<!-- 25-04-2018 for permisison -->
   <?php 
              /** hint **/
              // key with empty value is heading ex:"CRM"=>""
              // key with (without - same line) ex:"Dashboard"=>"dashboard"
              // key with (with - its sub value) ex:"Admin Settings"=>"settings-admin_settings"

              $all_pages=array("Dashboard"=>"dashboard","Task Section"=>"","Task"=>"task","Task Create"=>"task-create","CRM"=>"","Leads"=>"crm-leads","Leads Create"=>"crm-leads_create","Webtolead"=>"crm-webtolead","Webtolead Create"=>"crm-webtolead_create","Proposal"=>"","Dashboad"=>"proposal-dashboad","Create Proposal"=>"proposal-create","Catalog"=>"proposal-catalog","Template"=>"proposal-template","Setting"=>"proposal-settings","Client"=>"","Client Section"=>"client-client","Add Client"=>"client-addclient","Add From Company House"=>"client-add_from_company_house","Import Client"=>"client-import_client","Services Timeline"=>"client-services_timeline","Deadline"=>"","Deadline manager"=>"deadline-deadline","Report"=>"","Reports"=>"report-reports","Settings"=>"","Admin Settings"=>"settings-admin_settings","Firm Settings"=>"settings-firm_settings","Column Settings"=>"settings-column_settings","Team and Management"=>"settings-team_and_management","Staff Custom Firmlist"=>"settings-staff_custom_firmlist","Tickets"=>"settings-tickets","Chat"=>"settings-chat","Document"=>"","Documents"=>"documents","Document Create"=>"document-create","Invoice"=>"","Invoices"=>"invoice","Invoices Create"=>"invoice-create");

               $all_pages_section=array("Dashboard"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Task"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Task Create"=>array("view"=>"","add"=>"add","edit"=>"","delete"=>""),"Leads"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Leads Create"=>array("view"=>"","add"=>"add","edit"=>"","delete"=>""),"Webtolead"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete")
                  ,"Webtolead Create"=>array("view"=>"","add"=>"add","edit"=>"","delete"=>""),"Dashboad"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Create Proposal"=>array("view"=>"","add"=>"add","edit"=>"","delete"=>""),"Catalog"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Template"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Setting"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Client Section"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Add Client"=>array("view"=>"","add"=>"add","edit"=>"","delete"=>""),"Add From Company House"=>array("view"=>"","add"=>"add","edit"=>"","delete"=>""),"Import Client"=>array("view"=>"","add"=>"add","edit"=>"","delete"=>""),"Services Timeline"=>array("view"=>"view","add"=>"","edit"=>"","delete"=>""),"Deadline manager"=>array("view"=>"view","add"=>"","edit"=>"","delete"=>""),"Reports"=>array("view"=>"view","add"=>"","edit"=>"","delete"=>""),"Admin Settings"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Firm Settings"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Column Settings"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Team and Management"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Staff Custom Firmlist"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Tickets"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Chat"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Documents"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Document Create"=>array("view"=>"","add"=>"add","edit"=>"","delete"=>""),"Invoices"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Invoices Create"=>array("view"=>"","add"=>"add","edit"=>"","delete"=>""),);
              if(isset($staff[0]['user_id'])){
               $staff_id=$staff[0]['user_id'];
              }
              else
              {
               $staff_id=0;
              }

          $db_access_permission=$this->db->query("select * from staff_access_permission where staff_id=".$staff_id." order by id  asc ")->result_array();
           // echo "<pre>";
           // print_r($db_access_permission);
              ?>
              <div class="dept-checkbox-danger">
                  <table class="permission-table" id="permission-table">
                  <thead>
                        <tr>
                           <th>
                              <div class="checkbox-fade fade-in-primary">
                                 <label>
                                 <input type="checkbox" id="selectall" value="permission" level="parent">
                                 <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                 </label>
                              </div>
                              <label>permissions</label>
                           </th>
                           <th>
                              <div class="checkbox-fade fade-in-primary">
                                 <label>
                                 <input type="checkbox" id="view_all" value="permission_view">
                                 <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                 </label>
                              </div>
                              <label>view</label>
                           </th>
                           <th>
                              <div class="checkbox-fade fade-in-primary">
                                 <label>
                                 <input type="checkbox" id="create_all" value="permission_create">
                                 <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                 </label>
                              </div>
                              <label>create</label>
                           </th>
                           <th>
                              <div class="checkbox-fade fade-in-primary">
                                 <label>
                                 <input type="checkbox" id="edit_all" value="permission_edit">
                                 <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                 </label>
                              </div>
                              <label>edit</label>
                           </th>
                           <th>
                              <div class="checkbox-fade fade-in-primary">
                                 <label>
                                 <input type="checkbox" id="delete_all" value="permission_delete">
                                 <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                 </label>
                              </div>
                              <label>delete</label>
                           </th>
                        </tr>
                        </thead>
                     <tbody>
                     <?php 
                     $i=0;
                     foreach ($all_pages as $page_key => $page_value) {
                      ?>

                      <?php
                        if($page_value=='')
                        { 
                          // echo $page_value;
                           ?>
                        <tr>
                           <td colspan="5" class="tablebeneath-head">
                              <label class="permission_heading"><h6><?php echo $page_key;?></h6></label>
                           </td>
                                                     
                        </tr>
                        <?php }
                        else{
                           $its_view_delete=$all_pages_section[$page_key];

                           // echo $page_key."--";
                           // echo json_encode($its_view_delete['add']);
                           // echo "<br>";
                           
                      // echo $page_value;
                           $its_view=$its_view_delete['view'];
                           $its_add=$its_view_delete['add'];
                           $its_edit=$its_view_delete['edit'];
                           $its_delete=$its_view_delete['delete'];

                            $page_explode=explode('-', $page_value);
                           ?>
                               <tr>
                           <td>
                              <div class="checkbox-fade fade-in-primary">
                                 <label>
                                 <input type="checkbox" id="bulkall" class="dashboard_check checkboxall bulkall" value="dashboard" level="parent">
                                 <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                 </label>
                              </div>
                              <?php 
                         if(count($page_explode)>1)
                           {
                              ?><label><?php echo $page_key; ?></label><?php
                           }else
                           {
                              ?><label><h6><?php echo $page_key; ?></h6></label><?php
                           }
                              ?>
                              <!-- <label><?php echo $page_key; ?></label> -->
                           </td>
                           <td>
                              <div class="checkbox-fade fade-in-primary">
                              <?php if($its_view!=''){ ?>
                                 <label>
                                 <input type="checkbox" id="bulkviews" class="dashboard_check viewall bulkview"  name="bulkview[]" value="<?php echo $page_value."-view"; ?>" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['view']==1){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-view";echo "checked"; } ?> >
                                 
                                 <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                 </label>
                                  
                                  <input type="hidden" id="for_bulkviews" class="viewall for_bulkview"  name="for_bulkview[]" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['view']==1){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-view"; ?> value="<?php echo $val;?>" <?php }else { ?> value="<?php echo $page_value."-view//unchecked"; ?>" <?php } ?> >
                                 <?php }else{
                                    ?>
                                      <input type="hidden" id="for_bulkviews" class="viewall for_bulkviews"  name="for_bulkview[]" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['view']==1){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-view"; ?> value="<?php echo $val;?>" <?php }else { ?> value="<?php echo $page_value."-view//unchecked"; ?>" <?php } ?> >
                                   <?php } ?>
                              </div>
                           </td>
                           <td> <div class="checkbox-fade fade-in-primary">
                            <?php if($its_add!=''){ ?>
                                 <label>
                                 <input type="checkbox" id="bulkcreates" class="dashboard_check viewall bulkcreate" name="bulkcreates[]" value="<?php echo $page_value."-create"; ?>" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['create']==1){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-create";echo "checked"; } ?> >
                               
                                 <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                 </label>
                                 
                                   <input type="hidden" id="for_bulkcreates" class="viewall for_bulkcreate" name="for_bulkcreates[]" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['create']==1){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-create"; ?> value="<?php echo $val;?>" <?php }else { ?> value="<?php echo $page_value."-create//unchecked"; ?>" <?php } ?> >
                                   <?php }else{ ?>
                                     <input type="hidden" id="for_bulkcreates" class="viewall for_bulkcreates" name="for_bulkcreates[]" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['create']==1){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-create"; ?> value="<?php echo $val;?>" <?php }else { ?> value="<?php echo $page_value."-create//unchecked"; ?>" <?php } ?> >
                                   <?php } ?>
                              </div>
                              </td>
                           <td> <div class="checkbox-fade fade-in-primary">
                            <?php if($its_edit!=''){ ?>
                                 <label>
                                 <input type="checkbox" id="bulkedits" class="dashboard_check viewall bulkedit" name="bulkedit[]" value="<?php echo $page_value."-edit"; ?>" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['edit']==1){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-edit";echo "checked"; } ?> >
                                
                                 <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                 </label>
                                
                                   <input type="hidden" id="for_bulkedits" class="viewall for_bulkedit" name="for_bulkedit[]" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['edit']==1){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-edit"; ?> value="<?php echo $val;?>" <?php }else { ?> value="<?php echo $page_value."-edit//unchecked"; ?>" <?php } ?> >
                              <?php }else{
                                       ?>
                                 <input type="hidden" id="for_bulkedits" class="viewall for_bulkedits" name="for_bulkedit[]" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['edit']==1){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-edit"; ?> value="<?php echo $val;?>" <?php }else { ?> value="<?php echo $page_value."-edit//unchecked"; ?>" <?php } ?> >
                                       <?php } ?>
                              </div></td>
                           <td> <div class="checkbox-fade fade-in-primary">
                            <?php if($its_delete!=''){ ?>
                                 <label>
                                 <input type="checkbox" id="bulkdeletes" class="dashboard_check viewall bulkdelete" name="bulkdelete[]" value="<?php echo $page_value."-delete"; ?>" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['delete']){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-delete";echo "checked"; } ?> >
                               
                                 <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                 </label>
                                  
                                    <input type="hidden" id="for_bulkdeletes" class=" viewall for_bulkdelete" name="for_bulkdelete[]" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['delete']){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-delete"; ?> value="<?php echo $val;?>" <?php }else { ?> value="<?php echo $page_value."-delete//unchecked"; ?>" <?php } ?> >
                              <?php }else{ ?>
                                     <input type="hidden" id="for_bulkdeletes" class=" viewall for_bulkdeletes" name="for_bulkdelete[]" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['delete']){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-delete"; ?> value="<?php echo $val;?>" <?php }else { ?> value="<?php echo $page_value."-delete//unchecked"; ?>" <?php } ?> >
                                <?php } ?>     
                              </div></td>
                        </tr>
                           <?php
                        $i++; 
                        }
                      
                     }
                     ?>
                    
                       
                       
                       
                     </tbody>
                  </table>
                </div>
<!-- end of 25-04-2018 access permission --> 
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <!-- 2tab close -->
                                    <!-- 3 tab -->
                                    <div id="main_cutom" class="tab-pane custom-tab_permission fade">
                                       <?php if(!empty($custom_form['labels'])){  ?>
                     <!--    <form action="<?php echo base_url();?>staff/update_custom" name="update_bank" id="update_bank" method="post"> -->
                        <?php 
                           if(!empty($custom_form['labels'])){
$staff_cus_form_values='';
if(isset($staff[0]['id'])){
   $staff_cus_values=$this->db->query('select * from staff_custom_form_values where staff_id='.$staff[0]['user_id'].'')->row_array();
      if(count($staff_cus_values)>0){
         $staff_cus_form_values=json_decode($staff_cus_values['values'],true);
      }
}

                            $labels=explode(',',$custom_form['labels']);
                            $field_type=explode(',',$custom_form['field_type']);
                            $field_name=explode(',',$custom_form['field_name']);
                             $values=explode(',',$custom_form['values']);
                            for($i=0;$i< count($labels);$i++){ 
                           
                                if($field_type[$i]=="file"){ ?>
                        <div class="form-group row ">
                        <label class="col-sm-4 col-form-label"><?php echo $labels[$i]; ?></label>
                        <div class="col-sm-8">
                        <input type="<?php echo $field_type[$i]; ?>" name="<?php echo $field_name[$i]; ?>[]" id="bank_name" ></div>
                        <input type="hidden" name="<?php echo $field_name[$i]; ?>_hidden" value="<?php if($staff_cus_form_values!=''){
                           echo $staff_cus_form_values[$field_name[$i]];
                           } ?>">
                           <?php if($staff_cus_form_values[$field_name[$i]]!=''){ ?>
                           <!--   <a href="<?php echo $staff_cus_form_values[$field_name[$i]]; ?>" target="_blank" >View</a> -->
                           <span class="custom_images">
                           <img src="<?php echo $staff_cus_form_values[$field_name[$i]]; ?>" onerror=this.src="<?php echo base_url().'uploads/unknown.png'; ?>">
                           </span>
                             <?php } ?>
                        </div>
                        <?php  }else{ ?>
                        <div class="form-group row ">
                        <label class="col-sm-4 col-form-label"><?php echo $labels[$i]; ?></label>
                        <div class="col-sm-8">
                        <input type="<?php echo $field_type[$i]; ?>" name="<?php echo $field_name[$i]; ?>" id="bank_name" value="<?php if($staff_cus_form_values!=''){
                           echo $staff_cus_form_values[$field_name[$i]];
                           } ?>">

                           </div>
                        </div>
                        <?php } }} ?>
                        <div class="text-right new-bank-update">
                      <!--   <input type="button" name="cancel" id="cancel" value="cancel">
                        <input type="submit" name="update" id="update" value="update"> -->
                        </div>  
                        <!-- </form> -->
                        <?php } ?>
                        </div>
                        <!-- 3 tab close -->
                        </div>  <!-- tab-content -->
                        </div>
                        </div> <!-- managementclose -->
                        </form>  
                        <!-- admin close-->
                     </div>
                  </div>
               </div>
            </div>
            <!-- Page body end -->
         </div>
      </div>
      <!-- Main-body end -->
      <div id="styleSelector">
      </div>
   </div>
</div>
</div>
</div>
</div>
</div>
<!-- ajax loader -->
<div class="LoadingImage" ></div>
<style>
   .LoadingImage {
   display : none;
   position : fixed;
   z-index: 100;
   background-image : url('<?php echo site_url()?>assets/images/ajax-loader.gif');
   background-color:#666;
   opacity : 0.4;
   background-repeat : no-repeat;
   background-position : center;
   left : 0;
   bottom : 0;
   right : 0;
   top : 0;
   }
   .floating_set.text-right.accordion_ups.isset-accor {
   float: none;
   }
</style>

<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/popper.js/js/popper.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.j-pro.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>bower_components/switchery/js/switchery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/modernizr/js/css-scrollbars.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.filer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/advance-elements/custom-picker.js"></script>
<script src="<?php echo base_url();?>assets/js/pcoded.min.js"></script>
<script src="<?php echo base_url();?>assets/js/demo-12.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/common_script.js"></script>
<script>
   $( document ).ready(function() {
   
       var date = $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
   
       // Multiple swithces
       var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));
   
       elem.forEach(function(html) {
           var switchery = new Switchery(html, {
               color: '#22b14c',
               jackColor: '#fff',
               size: 'small'
           });
       });
   
       $('#accordion_close').on('click', function(){
               $('#accordion').slideToggle(300);
               $(this).toggleClass('accordion_down');
       });
   });
</script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript">
   $(document).ready(function(){
      $.extend($.validator.messages, {
  //  required: "This field is required.",
    remote: "",
  
});
      var it_global='';
      $.validator.addMethod(
    "regex",
    function(value, element, regexp) {
        var check = false;
        return this.optional(element) || regexp.test(value);
    },
    "Please check your input."
);
   $("#staff_form").validate({
     
          ignore: false,
   
          /*onfocusout: function(element) {
           if ( !this.checkable(element)) {
               this.element(element);
           }
       },*/
      // errorClass: "error text-warning",
       //validClass: "success text-success",
       /*highlight: function (element, errorClass) {
           //alert('em');
          // $(element).fadeOut(function () {
              // $(element).fadeIn();
           //});
       },*/
    highlight: function (element, errorClass) {
        //   alert('');
        console.log($(element).attr('id'));
          // $(element).fadeOut(function () {
          //     $(element).fadeIn();
          //  });
          if($(element).attr('id')=='first_name'|| $(element).attr('id')=='last_name'||  $(element).attr('id')=='email_id'|| $(element).attr('id')=='hourly_rate'|| $(element).attr('id')=='telephone_number'|| $(element).attr('id')=='password' )
          {
            // alert(it_global);
            // $it_global='1st';
            $('ul.nav-tabs li.nav-item').each(function(){
               var zz=$(this).find('a').attr('href');
               // $('#main_Contact').removeClass('active');
               // $(this).find('a').removeClass('active');
               if(zz=='#required_information')
               {
                 $(this).find('a').trigger('click');
                 // $(this).find('a').addClass('active');
                 // $('#required_information').addClass('active');
                 // $('.Footer.common-clienttab.pull-right > div.divleft').css('display','none');
                 // $('.Footer.common-clienttab.pull-right > div.divright').css('display','');
                //  $(activeTab).show();
                  //$(this).find('a').trigger('click');
               }

            });
          }
          if($(element).attr('id')=='roles')
          {
            // alert(it_global);
            // it_global='2nd';
             $('ul.nav-tabs li.nav-item').each(function(){
               var zz=$(this).find('a').attr('href');
              // $('#required_information').removeClass('active');
              //  $(this).find('a').removeClass('active');
               if(zz=='#main_Contact')
               {
                  //$(this).find('a').trigger('click');
                // $(this).find('a').addClass('active');
                // $('#main_Contact').addClass('active');
                $(this).find('a').trigger('click');
                 //  $('.Footer.common-clienttab.pull-right > div.divright').css('display','none');
                 // $('.Footer.common-clienttab.pull-right > div.divleft').css('display','');
                  // $(activeTab).show();
               }

            });
          }
       },
                           rules: {
                           first_name: {required: true},
                           last_name: {required: true},
                           //email_id: {required: true,email:true},
                           username:{
                            required: true,
                       
                               },

                           email_id: {
                              required: true,
                              email: true
                           },

                           telephone_number : {required:true,minlength:10,
                                            maxlength:20,
                                            number: true},
                        //  linkedin : {required: true},
                           
                           password:{required: true},
                           /*image_upload: {required: false, accept: "jpg|jpeg|png|gif"},*/
   //                        image_upload: {
   //     required: function(element) {
   
   //     if ($("#image_name").val().length > 0) {
   //        return true;
   //     }
   //     else {
   //     return false;
   //     }
   //   },
   //   accept: "jpg|jpeg|png|gif"
   // },
                           hourly_rate: {required: true,number:true},
                           roles:{required: true},
                           
                           },
                           errorElement: "span" , 
                           errorClass: "field-error",                             
                            messages: {
                             first_name: "Enter a first name",
                             last_name: "Enter a last name",
                               username:{ 
                                       required: "Enter User name",
                                    //    remote: "",
                                       },
                             roles:"Select a role",
   //                           email_id: {
   //     email:"Please enter a valid email address", 
   //     required:"Please enter a email address",
   
   // },
      email_id: "Please enter a valid email address",
                            telephone_number:{  yourtel:"Enter a valid phone number.",
     required: "Enter a phone number."},
                         //  linkedin:"Enter a URL",

                             password: {required: "Please enter your password "},
                             // image_upload: {required: 'Required!', accept: 'Not an image!'},
                             
                             hourly_rate: {required: "Please Enter Your rate",number:"Please enter numbers Only"},
                            },
                            
   
                           
                           submitHandler: function(form) {
                              if($('.fname-error').html()!='')
                              {
                               $('ul.nav-tabs li.nav-item').each(function(){
                                 var zz=$(this).find('a').attr('href');
                            
                                 if(zz=='#required_information')
                                 {
                                   $(this).find('a').trigger('click');
                                
                                 }

                               });
                              }
                              else{
                               var formData = new FormData($("#staff_form")[0]);
                          
                               $(".LoadingImage").show();
   
                               $.ajax({
                                   url: '<?php echo base_url();?>staff/add_staff/',
                                   dataType : 'json',
                                   type : 'POST',
                                   //enctype: 'multipart/form-data',
                                   data : formData,
                                   contentType : false,
                                   processData : false,
                                   //cache: false,
                                   
                                   success: function(data) {
                                        // alert(data); 
                                       if(data == 1){
                                          
                                          // $('#new_user')[0].reset();
                                               location.reload(true);
                                           $('.alert-success').show();
                                           $('.alert-danger').hide();
   
                                       }
                                       else{
                                          // alert('failed');
                                           $('.alert-danger').show();
                                           $('.alert-success').hide();
                                       }
                                   $(".LoadingImage").hide();
                                   },
                                  
                                   error: function() { $('.alert-danger').show();
                                           $('.alert-success').hide();}
                               });
                            }
   
                               return false;
                           } ,
                            invalidHandler: function(e, validator) {
              if(validator.errorList.length)
           $('#tabs a[href="#' + jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
   
           }
                            
                       });
   
   
   });
</script>
<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
<script src="<?php echo base_url();?>assets/js/mock.js"></script>
<script type="text/javascript">
$(document).ready(function() {
   $('.dropdown-sin-2').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });
 });  
    
</script>
<script type="text/javascript">
       function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#preview_image')
                    .attr('src', e.target.result)
                    .width(225)
                    .height(225);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

<!-- <script type="text/javascript">
                                          $('.dept').click(function() {
                                             var a = $(this).val();
                                             alert(a);
                                          });
                                             
                                          </script> --> 




<script type="text/javascript">
  /** for permisison checkboxs event **/
$("#view_all").click(function () {
    if ($(this).is(":checked")){
      $('.bulkview').each(function(){
         $(this).prop('checked', true);
         var value=$(this).attr('value');
         $(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value);
         //$(this).next('.for_bulkview').attr('value',value);
      });
    }
    else
    {
      $('.bulkview').each(function(){
         $(this).prop('checked', false); 
         var value=$(this).attr('value')+"//unchecked";
         $(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value);

       //  $(this).next('.for_bulkview').attr('value',value);
      });
    }
 });
$("#create_all").click(function () {
    if ($(this).is(":checked")){
      $('.bulkcreate').each(function(){
         $(this).prop('checked', true); 
             /** for extra fields **/
     var value=$(this).attr('value');
     $(this).closest('.checkbox-fade').find('.for_bulkcreate').attr('value',value);
     /** for extra fields **/
      });
    }
   else
    {
      $('.bulkcreate').each(function(){
         $(this).prop('checked', false); 
             /** for extra fields **/
     var value=$(this).attr('value')+"//unchecked";
     $(this).closest('.checkbox-fade').find('.for_bulkcreate').attr('value',value);
     /** for extra fields **/
      });
    }

 });
$("#edit_all").click(function () {
    if ($(this).is(":checked")){
      $('.bulkedit').each(function(){
         $(this).prop('checked', true); 
             /** for extra fields **/
     var value=$(this).attr('value');
     $(this).closest('.checkbox-fade').find('.for_bulkedit').attr('value',value);
     /** for extra fields **/
      });
    }
      else
    {
      $('.bulkedit').each(function(){
         $(this).prop('checked', false); 
             /** for extra fields **/
     var value=$(this).attr('value')+"//unchecked";
     $(this).closest('.checkbox-fade').find('.for_bulkedit').attr('value',value);
     /** for extra fields **/
      });
    }
 });
$('#delete_all').click(function () {
    if ($(this).is(":checked")){
      $('.bulkdelete').each(function(){
         $(this).prop('checked', true); 
             /** for extra fields **/
     var value=$(this).attr('value');
     $(this).closest('.checkbox-fade').find('.for_bulkdelete').attr('value',value);
     /** for extra fields **/
      });
    }
      else
    {
      $('.bulkdelete').each(function(){
         $(this).prop('checked', false); 
             /** for extra fields **/
     var value=$(this).attr('value')+"//unchecked";
     $(this).closest('.checkbox-fade').find('.for_bulkdelete').attr('value',value);
     /** for extra fields **/
      });
    }
 });

$('#selectall').click(function () {
    if ($(this).is(":checked")){
      $('#view_all').prop('checked',false).trigger('click');
      $('#create_all').prop('checked',false).trigger('click');
      $('#edit_all').prop('checked',false).trigger('click');
      $('#delete_all').prop('checked',false).trigger('click');
      $('.bulkall').each(function(){
         $(this).prop('checked', true); 
      });
    }
      else
    {     
      $('#view_all').prop('checked',true).trigger('click');
      $('#create_all').prop('checked',true).trigger('click');
      $('#edit_all').prop('checked',true).trigger('click');
      $('#delete_all').prop('checked',true).trigger('click');
      $('.bulkall').each(function(){
         $(this).prop('checked', false); 
      });
    }
 });
$('.bulkall').click(function(){
 if($(this).is(":checked")){
    $(this).closest('tr').find('td').each(function(){
   //alert('sdsd');
   $(this).find('input[type="checkbox"]').prop('checked',true);
  // alert($(this).attr('class'));
      var value=$(this).find('input[type="checkbox"]').attr('value');
   // alert($(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value));
   var itid=$(this).find('input[type="checkbox"]').attr('id');
  //alert(itid);
   if(itid=='bulkviews'){
$(this).find('.for_bulkview').attr('value',value);
}
  if(itid=='bulkcreates'){
$(this).find('.for_bulkcreate').attr('value',value);
}  if(itid=='bulkedits'){
$(this).find('.for_bulkedit').attr('value',value);
}  if(itid=='bulkdeletes'){
$(this).find('.for_bulkdelete').attr('value',value);
}

  });
 }
 else
 {
    $(this).closest('tr').find('td').each(function(){
   //alert('sdsd');
   $(this).find('input[type="checkbox"]').prop('checked',false);
      var value=$(this).find('input[type="checkbox"]').attr('value')+"//unchecked";
   // alert($(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value));
   var itid=$(this).find('input[type="checkbox"]').attr('id');
   if(itid=='bulkviews'){
$(this).find('.for_bulkview').attr('value',value);
}
  if(itid=='bulkcreates'){
$(this).find('.for_bulkcreate').attr('value',value);
}  if(itid=='bulkedits'){
$(this).find('.for_bulkedit').attr('value',value);
}  if(itid=='bulkdeletes'){
$(this).find('.for_bulkdelete').attr('value',value);
}
  });
 }


});
$('.bulkview').click(function(){
 // alert('zz');
  if($(this).is(":checked")){
    var value=$(this).attr('value');
   // alert($(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value));
$(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value);

  }
  else
  {
    var value1=$(this).attr('value')+"//unchecked";
  //  alert($(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value1));
$(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value1);
  }
});

$('.bulkcreate').click(function(){
 // alert('zz');
  if($(this).is(":checked")){
    var value=$(this).attr('value');
   // alert($(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value));
$(this).closest('.checkbox-fade').find('.for_bulkcreate').attr('value',value);
  }
  else
  {
    var value1=$(this).attr('value')+"//unchecked";
  //  alert($(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value1));
$(this).closest('.checkbox-fade').find('.for_bulkcreate').attr('value',value1);
  }
});

$('.bulkedit').click(function(){
 // alert('zz');
  if($(this).is(":checked")){
    var value=$(this).attr('value');
   // alert($(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value));
$(this).closest('.checkbox-fade').find('.for_bulkedit').attr('value',value);
  }
  else
  {
    var value1=$(this).attr('value')+"//unchecked";
  //  alert($(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value1));
$(this).closest('.checkbox-fade').find('.for_bulkedit').attr('value',value1);
  }
});

$('.bulkdelete').click(function(){
 // alert('zz');
  if($(this).is(":checked")){
    var value=$(this).attr('value');
   // alert($(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value));
$(this).closest('.checkbox-fade').find('.for_bulkdelete').attr('value',value);
  }
  else
  {
    var value1=$(this).attr('value')+"//unchecked";
  //  alert($(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value1));
$(this).closest('.checkbox-fade').find('.for_bulkdelete').attr('value',value1);
  }
});

$('#save_btn').click(function(){
     $("input[name='bulkview[]']:not(:checked)").each(function () {
          //  alert("Id: " + $(this).attr("id") + " Value: " + $(this).val());
        });
     //return false;
   });


</script>

<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.jquery.min.js"></script> -->
<!-- <script type="text/javascript">
   $(".chosen-select").chosen();
$('button').click(function(){
        $(".chosen-select").val('').trigger("chosen:updated");
});
</script> -->

<?php //$this->load->view('staffs/scripts.php');?>
<div class="modal fade" id="EmailSignature" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-e-signature">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>

                </button>
                 <h4 class="modal-title" id="myModalLabel">E-Signature</h4>

            </div>
            <div class="modal-body">
             <?php $this->load->view('staffs/my_sign.php'); ?>
            </div>
            <div class="modal-footer">
                <button id="btnSaveSign" class="btn btn-default">Save Signature</button>
                <button  id="clearSig" class="btn btn-primary save">Clear Signature</button>
                <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                <!-- <button type="button" class="btn btn-primary save">Save changes</button> -->
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/signature/js/numeric-1.2.6.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/signature/js/bezier.js"></script>
<script src="<?php echo base_url(); ?>assets/signature/js/jquery.signaturepad.js"></script>   

<script type='text/javascript' src="https://github.com/niklasvh/html2canvas/releases/download/0.4.1/html2canvas.js"></script>
<script src="<?php echo base_url(); ?>assets/signature/js/json2.min.js"></script>
<script type="text/javascript">

$('#email_signature').on('change', function(e){
   if(e.target.checked){
     $('.for_sign').css('display','');
     $('#EmailSignature').modal();
   }
   else
   {
      $('.for_sign').css('display','none');
   }
});

// var sig = [{lx:20,ly:34,mx:20,my:34},{lx:21,ly:33,mx:20,my:34},{lx:20,ly:34,mx:20,my:34},{lx:21,ly:33,mx:20,my:34},{lx:20,ly:34,mx:20,my:34},{lx:21,ly:33,mx:20,my:34},{lx:20,ly:34,mx:20,my:34},{lx:21,ly:33,mx:20,my:34},{lx:20,ly:34,mx:20,my:34},{lx:21,ly:33,mx:20,my:34},{lx:20,ly:34,mx:20,my:34},{lx:21,ly:33,mx:20,my:34}];

$(document).ready(function () {

  //$('#signArea').signaturePad({displayOnly:true}).regenerate(sig);
 

});

     $(document).ready(function() {

      $(window).scroll(function() {
  var sticky = $('#option-tabs'),
    scroll = $(window).scrollTop();
   
  if (scroll >= 40) { 
    sticky.addClass('fixed'); }
  else { 
   sticky.removeClass('fixed');

}
});


 $(window).scroll(function() {
 var sticky1 = $('#mail-template, .container-fullscreen'),
   scroll = $(window).scrollTop();
  
 if (scroll >= 40) { 
   sticky1.addClass('fixed1'); }
 else { 
  sticky1.removeClass('fixed1');

}
});
  
   //   $('#signArea').signaturePad({drawOnly:true, drawBezierCurves:true, lineTop:90});
     //$('#signArea').signaturePad({displayOnly:true}).regenerate(sig);

   <?php if($get_sign_json!=''){ ?>
  var zz='<?php echo $get_sign_json; ?>';
  $('#signArea').signaturePad({drawOnly:true, drawBezierCurves:true, lineTop:90}).regenerate(zz);
   <?php }else{ ?>
  $('#signArea').signaturePad({drawOnly:true, drawBezierCurves:true, lineTop:90});
   <?php } ?>

      });   

     $("#clearSig").click(function clearSig() {

      $('#signArea').signaturePad().clearCanvas ();
        });

   $("#btnSaveSign").click(function(e){
    //  console.log($('.output').val());
      var json_data=$('.output').val();
    // alert('zzz');
   //   var sig = $('#sign-pad').signaturePad();
    //  alert(sig.signaturePad('toJSON'));
          html2canvas([document.getElementById('sign-pad')], {
          onrendered: function (canvas) {

            var canvas_img_data = canvas.toDataURL('image/png');
            var img_data = canvas_img_data.replace(/^data:image\/(png|jpg);base64,/, "");
            //ajax call to save image inside folder
            $.ajax({
              url: '<?php echo base_url(); ?>/Staff/save_sign',
              data: { img_data:img_data,json_data:json_data },
              type: 'post',
              dataType: 'json',
              success: function (response) {    
               console.log('sign data');
              console.log(response);
              //alert(response);
                $("#imgData").html('Thank you! Your signature was saved');
                $("#imgData").show();
                //$('#sign_data').val(response);
              
                $('#sign_data').val(response['insert_data']);
                $('.for_sign').html('<img src="<?php echo base_url(); ?>'+response['file_name']+'"><span class="remove_sign" style="color: red;">Remove</span>');
                $('.for_sign').css('display','');
                setTimeout(function(){  $("#imgData").hide();  }, 2000);
                setTimeout(function(){   $("#EmailSignature").modal('hide'); }, 2000);
                 //window.location.reload();
              }
            });
          }
        });
      });  
</script>
<script type="text/javascript">
 $(document).ready(function() {
   $('.username').on('input', function() {
      var fname = $(this).val();
      var staff_id=$("#staff_id").val();

      $.ajax({
        type: "POST",
        url: "<?php echo base_url().'staff/Firstname_Check';?>",
        data: {"fname": fname,"staff_id":staff_id},
        success:function(res) {
         
         if(res == 1) {
            $('.fname-error').html("First Name already exists.");
         } else {
            $('.fname-error').html('');
            
         }
        }

     });


   });

   $('#email_id').keyup(function(){
     $(this).next("span").remove();
   });

 });  
 /** 31-07-2018 **/
 $(document).on('click','.remove_sign',function(){
$('.for_sign').css('display','none');
$('#sign_data').val('');
//$('#email_signature').prop('checked','');
var element = $('#email_signature');
changeSwitchery(element, false);
 });

 $('.country_code').on('change input', function() {
  $(this).val($(this).val().replace(/([^+0-9]+)/gi, ''));
});
 $('#telephone_number').on('change input', function() {
  $(this).val($(this).val().replace(/([^0-9]+)/gi, ''));
});
 /** end of 31-07-2018 **/
 function changeSwitchery(element, checked) {
  if ( ( element.is(':checked') && checked == false ) || ( !element.is(':checked') && checked == true ) ) {
    element.parent().find('.switchery').trigger('click');
  }
}

/** for addstaff page prev and next button **/

jQuery( document ).ready(function( $ ) {

// next click
 $('.addstaff-signed-change3').click(function(){
  $('ul.nav-tabs .active').closest('li').nextAll("li:visible").eq(0).find('a').trigger('click').parent().show();
   var current= $('ul.nav-tabs .active').attr('href');

   var last=$('ul.nav-tabs .nav-link').closest('li').nextAll("li:visible").last().find('a').attr('href');

     if(current==last)
     {
       $('.addstaff-signed-change3').hide();
       $('.addstaff-signed-change2').show();
     }
     else
     {
       $('.addstaff-signed-change3').show();
     }

  var current_one= $('ul.nav-tabs .active').attr('href');

  var last_one=$('ul.nav-tabs .nav-link').closest('li.current_li').prevAll("li.current_li:visible").last().find('a').attr('href');
     if(current_one==last_one)
     {
       $('.addstaff-signed-change2').hide();
       $('.addstaff-signed-change3').show();
     }
     else
     {
       $('.addstaff-signed-change2').show();
     }
  //alert(zz);
});


// prev click
  $('.addstaff-signed-change2').click(function(){
    $('ul.nav-tabs .active').closest('li').prevAll('li:visible').eq(0).find('a').trigger('click');
    $('.addstaff-signed-change3').show();

   var current= $('ul.nav-tabs .active').attr('href');

   var last=$('ul.nav-tabs .nav-link').closest('li.current_li').prevAll("li.current_li:visible").last().find('a').attr('href');
        if(current==last)
        {
          $('.addstaff-signed-change2').hide();
          $('.addstaff-signed-change3').show();
        }
        else
        {
          $('.addstaff-signed-change2').show();
        }
   });

   $('ul.nav-tabs li a').click(function(){
      var $this = $(this);
      $('.nav-tabs li').removeClass('active-previous');
      $this.closest('li').prevAll().addClass('active-previous');


       var current= $this.attr('href');

       var last=$('ul.nav-tabs .nav-link').closest('li').nextAll("li:visible").last().find('a').attr('href');
            if(current==last)
              {
                $('.addstaff-signed-change3').hide();
                $('.addstaff-signed-change2').show();
              }
              else
              {
                $('.addstaff-signed-change3').show();
              }

     var current_one=  $this.attr('href');

     var last_one=$('ul.nav-tabs .nav-link').closest('li.current_li').prevAll("li.current_li:visible").last().find('a').attr('href');
              if(current_one==last_one)
              {
                $('.addstaff-signed-change2').hide();
                $('.addstaff-signed-change3').show();
              }
              else
              {
                $('.addstaff-signed-change2').show();
              }
      });

  }); 

// $('.addstaff-signed-change2').click(function(){
//   $('ul.nav-tabs li.nav-item').each(function(){
//                var zz=$(this).find('a').attr('href');
//                $('#main_Contact').removeClass('active');
//                $(this).find('a').removeClass('active');
//                if(zz=='#required_information')
//                {
//                  $(this).find('a').addClass('active');
//                  $('#required_information').addClass('active');
//                  $('.Footer.common-clienttab.pull-right > div.divleft').css('display','none');
//                  $('.Footer.common-clienttab.pull-right > div.divright').css('display','');
//                 //  $(activeTab).show();
//                   //$(this).find('a').trigger('click');
//                }

//             });
//       });
// $('.addstaff-signed-change3').click(function(){
//   $('ul.nav-tabs li.nav-item').each(function(){
//                var zz=$(this).find('a').attr('href');
//                $('#required_information').removeClass('active');
//                $(this).find('a').removeClass('active');
//                if(zz=='#main_Contact')
//                {
//                  $(this).find('a').addClass('active');
//                  $('#main_Contact').addClass('active');
//                  $('.Footer.common-clienttab.pull-right > div.divright').css('display','none');
//                  $('.Footer.common-clienttab.pull-right > div.divleft').css('display','');
//                 //  $(activeTab).show();
//                   //$(this).find('a').trigger('click');
//                }

//             });
//       });
/** end of addstaff **/
$(document).on('change','#roles',function(){
   var role=$(this).val();
   <?php //if(!isset($staff[0])){ ?>
   if(role==3)
   {
       //  $("#selectall").trigger('click');
       $('#selectall').prop("checked", false).trigger('click'); // prop false -> trigger -> prop true change

         $('tr > td:first-child').each(function(){
            var text=$(this).text().trim();
                console.log($(this).text().trim());
                if(text=='Admin Settings')
                {
                   $(this).find('input').trigger('click');
                   // alert($(this).find('input').attr('id'));
                }
                if(text=='Firm Settings')
                {
                   $(this).find('input').trigger('click');
                }
         });
   } 
   if(role==5)
   {
      //$("#selectall").trigger('click');

      $('#selectall').prop("checked", false).trigger('click');  // prop false -> trigger -> prop false change
         $('tr > td:first-child').each(function(){
            var text=$(this).text().trim();
                console.log($(this).text().trim());
                if(text=='Admin Settings')
                {
                   $(this).find('input').trigger('click');
                   // alert($(this).find('input').attr('id'));
                }
                if(text=='Firm Settings' || text=='Column Settings' || text=='Team and Management' || text=='Staff Custom Firmlist' || text=='Tickets' || text=='Chat')
                {
                   $(this).find('input').trigger('click');
                }
         });

   }
   if(role==6)
   {
     $('#selectall').prop("checked", true).trigger('click');
   }
      <?php //} ?>
      // $('.checkbox-fade.fade-in-primary').each(function(){
      //  //  alert('zzz');
      //       console.log($(this).children('label:nth-child(1)').text());
      // });
      
});

</script>
<?php $this->load->view('includes/footer_second');?>

</body>
</html>