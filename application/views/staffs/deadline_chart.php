
<div id="columnchart_values" style="width: 100%; height: 350px;"></div>


<script type="text/javascript">
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
     
      var data = google.visualization.arrayToDataTable([
        ["Element", "Density", { role: "style" } ],
        ["Confirmation", <?php echo $confirmation['deadline_count']; ?>, "rgb(252, 97, 128)"],
        ["Accounts", <?php echo $account['deadline_count']; ?>, "silver"],
       ["Personal Tax", <?php echo $personal_due['deadline_count']; ?>, "silver"],
        ["VAT ", <?php echo $vat['deadline_count']; ?>, "gold"],
        ["Payroll", <?php echo $payroll['deadline_count']; ?>, "red"],
         ["WorkPlace Pension - AE", <?php echo $pension['deadline_count']; ?>, "rgb(70, 128, 255)"],
          ["CIS", <?php echo $registeration['deadline_count']; ?>, "brown"],
          ["CIS-Sub", 0, "brown"],
          ["P11D", <?php echo $plld['deadline_count']; ?>, "rgb(70, 128, 255)"],
           ["Management Accounts", <?php echo $management['deadline_count']; ?>, "rgb(147, 190, 82)"],
            ["Bookkeeping", <?php echo $booking['deadline_count']; ?>, "rgb(105, 206, 198)"],
             ["Investigation Insurance", <?php echo $insurance['deadline_count']; ?>, "rgb(255, 182, 77)"],
              ["Registeration", <?php echo $registeration['deadline_count']; ?>, "green"],
               ["Tax Advice", <?php echo $investigation['deadline_count']; ?>, "pink"],
                ["Tax Investigation", <?php echo $investigation['deadline_count']; ?>, "color:#F20FDA"]
      ]);      

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var options = {
        title: "Deadline Chart",
      //  width: 600,
        height: 350,
        bar: {groupWidth: "25%"},
        legend: { position: "none" },
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
      chart.draw(view, options);
  }
  </script>
