<?php $this->load->view('includes/header');
   
   ?>
<link href="<?php echo  base_url()?>assets/css/c3.css" rel="stylesheet" type="text/css">
<link href="https://rawgit.com/KidSysco/jquery-ui-month-picker/v3.0.0/demo/MonthPicker.min.css" rel="stylesheet" type="text/css">
<link href="https://rawgit.com/KidSysco/jquery-ui-month-picker/v3.0.0/test/test.css" rel="stylesheet" type="text/css">


<div class="pcoded-content card desk-dashboard">
  <div class="inner-tab123 ui-tabs ui-corner-all ui-widget ui-widget-content" id="tabs112">
    <div class="staff-profile-cnt floating_set">
      <div class="deadline-crm1 floating_set" >
            <ul class="md-tabs tabs112 floating_set" id="proposal_dashboard">
               <li class="nav-item">
                  <a class="nav-link" href="#Basics">Basics
                  </a>
               </li>
               <li class="nav-item">
                  <a class="nav-link"  href="#Deadline-Manager">Task summary
                  </a>
               </li>
            </ul>
         </div>
      <div class="for-sep12">
        <!-- tab1 -->
        <div id="Basics" class="proposal_send ui-tabs-panel ui-corner-bottom ui-widget-content" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
          <!-- basics -->
          <div class="row">
            <div class="col-md-12 col-xl-6">
              <div class="card">
                <div class="card-block user-detail-card">
                  <div class="row">
                    <div class="col-sm-4">
                      <div class="height-profile">
                        <div class="height-profile1"> <img src="http://remindoo.org/CRMTool/uploads/825898.jpg " alt="" class="img-fluid p-b-10"></div>
                      </div>
                      <!-- <div class="contact-icon">
                        <button class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="View" ><i class="icofont icofont-eye m-0"></i></button>
                        <button class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Print" ><i class="icofont icofont-printer m-0"></i></button>
                        <button class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Download" ><i class="icofont icofont-download-alt m-0"></i></button>
                        <button class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Share" ><i class="icofont icofont-share m-0"></i></button>
                        </div> -->
                    </div>
                    <div class="col-sm-8 user-detail">
                      <div class="row">
                        <div class="col-sm-5">
                          <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-user"></i>Name :</h6>
                        </div>
                        <div class="col-sm-7">
                          <h6 class="m-b-30">ssft</h6>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-5">
                          <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-email"></i>EMP ID</h6>
                        </div>
                        <div class="col-sm-7">
                          <h6 class="m-b-30"><a href="mailto:dummy@example.com">#2515</a></h6>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-5">
                          <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-home"></i>Role ID :</h6>
                        </div>
                        <div class="col-sm-7">
                          <h6 class="m-b-30">Staff</h6>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-5">
                          <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-touch-phone"></i>Phone :</h6>
                        </div>
                        <div class="col-sm-7">
                          <h6 class="m-b-30"> </h6>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-5">
                          <h6 class="f-w-400 m-b-30"><i class="icofont icofont-web"></i>Website :</h6>
                        </div>
                        <div class="col-sm-7">
                          <h6 class="m-b-30"><a href="#!"> </a></h6>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Summery Start -->
            <div class="col-md-12 col-xl-6 summary-card1">
              <div class="card summery-card">
                <div class="card-header">
                  <div class="card-header-left ">
                    <h5>Summary</h5>
                  </div>
                </div>
                <div class="card-block">
                  <div class="row">
                    <div class="col-sm-6 b-r-default p-b-30">
                      <h2 class="f-w-400">0</h2>
                      <p class="text-muted f-w-400">Active users</p>
                      <div class="progress">
                        <div class="progress-bar bg-c-blue" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-6 p-b-30">
                      <h2 class="f-w-400">0</h2>
                      <p class="text-muted f-w-400">Completed task</p>
                      <div class="progress">
                        <div class="progress-bar bg-c-pink" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                        </div>
                      </div>
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-6 b-r-default p-b-30">
                      <h2 class="f-w-400">0</h2>
                      <p class="text-muted f-w-400">Inactive users</p>
                      <div class="progress">
                        <div class="progress-bar bg-c-yellow" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-6 p-b-30">
                      <h2 class="f-w-400">0</h2>
                      <p class="text-muted f-w-400">Inprogress task</p>
                      <div class="progress">
                        <div class="progress-bar bg-c-green" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- summary end -->
          </div>
          <!-- basics -->
        </div>
        <!-- end tab 1-->


        <!--tab 4-->
        <div id="Deadline-Manager" aria-labelledby="ui-id-4" role="tabpanel" class="ui-tabs-panel ui-corner-bottom ui-widget-content forwhite" aria-hidden="true" style="display: none;">
         <!-- tab2 -->  
                <div id="task_summary">
                  <div class="new_bank1">
                    <h2>Task summary</h2>
                    <div class="task-filter1">
                      <div class="time-spent">
                        <h4>Total Task Time Spent</h4>
                        <!-- <div id="defaultCountdown"></div> -->
                        <div class="timer-section01">
                          <span>51 <strong>Hours</strong></span> :
                          <span>43<strong>Minutes</strong></span> :
                          <span>00<strong>Seconds</strong></span> 
                        </div>
                      </div>
                      <!-- 09-08-2018 for individual task timing -->
                      <div class="time-spent">
                        <h4>Individual Task Time Spent</h4>
                        <!-- <div id="defaultCountdown"></div> -->
                        <div class="table-responsive">
                          <table class="table">
                            <thead>
                              <tr>
                                <th>Sno</th>
                                <th>Task Name</th>
                                <th>Timer</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>1</td>
                                <td>rs sample</td>
                                <td>
                                  <div class="timer-section01 for_inner_timer">
                                    <span>00 <strong>Hours</strong></span> :
                                    <span>21<strong>Minutes</strong></span> :
                                    <span>00<strong>Seconds</strong></span> 
                                  </div>
                                </td>
                              </tr>
                              <tr>
                                <td>2</td>
                                <td>rs sample</td>
                                <td>
                                  <div class="timer-section01 for_inner_timer">
                                    <span>51 <strong>Hours</strong></span> :
                                    <span>22<strong>Minutes</strong></span> :
                                    <span>00<strong>Seconds</strong></span> 
                                  </div>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <!-- end of individual task timing -->
                      <div class="time-spent">
                        <h4>Task Reports</h4>
                        <div class="chart-container-01">
                          <div class="open-closed1">
                            <!--
                              <span> <i class="fa fa-square fa-6" aria-hidden="true"></i>Critical</span>
                              <span> <i class="fa fa-square fa-6" aria-hidden="true"></i> High</span>
                              
                              <span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Medium</span>
                              <span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Low</span>
                              
                              <span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Resolved</span>
                              <span> <i class="fa fa-square fa-6" aria-hidden="true"></i> closed</span> -->
                            <span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Low</span>
                            <span> <i class="fa fa-square fa-6" aria-hidden="true"></i> High</span>
                            <span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Medium</span>
                            <span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Super Urgent</span> 
                          </div>
                          <div id="chartContainer" style="width: 100%; height: 420px">
                            <div class="canvasjs-chart-container" style="position: relative; text-align: left; cursor: auto;">
                              <canvas class="canvasjs-chart-canvas" width="500" height="400" style="position: absolute;"></canvas>
                              <canvas class="canvasjs-chart-canvas" width="500" height="400" style="position: absolute; -webkit-tap-highlight-color: transparent;"></canvas>
                              <div class="canvasjs-chart-toolbar" style="position: absolute; right: 1px; top: 1px; border: 1px solid transparent;"></div>
                              <div class="canvasjs-chart-tooltip" style="position: absolute; height: auto; box-shadow: rgba(0, 0, 0, 0.0980392) 1px 1px 2px 2px; z-index: 1000; pointer-events: none; display: none; border-radius: 5px;">
                                <div style=" width: auto;height: auto;min-width: 50px;line-height: auto;margin: 0px 0px 0px 0px;padding: 5px;font-family: Calibri, Arial, Georgia, serif;font-weight: normal;font-style: italic;font-size: 14px;color: #000000;text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.1);text-align: left;border: 2px solid gray;background: rgba(255,255,255,.9);text-indent: 0px;white-space: nowrap;border-radius: 5px;-moz-user-select:none;-khtml-user-select: none;-webkit-user-select: none;-ms-user-select: none;user-select: none;} "> Sample Tooltip</div>
                              </div>
                              <a class="canvasjs-chart-credit" title="JavaScript Charts" style="outline:none;margin:0px;position:absolute;right:2px;top:386px;color:dimgrey;text-decoration:none;font-size:11px;font-family: Calibri, Lucida Grande, Lucida Sans Unicode, Arial, sans-serif" tabindex="-1" target="_blank" href="https://canvasjs.com/">CanvasJS.com</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
          
        </div>
      </div>
    </div>
  </div>
</div>



<!-- title_page01 -->    
<input type="hidden" id="h_f" name="h_f" value="">
<?php $this->load->view('includes/footer');?>
<script src="<?php echo  base_url()?>assets/js/jquery.lineProgressbar.js"></script>
<script src="<?php echo  base_url()?>assets/js/c3-custom-chart.js"></script>
<script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script> 
<script src="https://rawgit.com/digitalBush/jquery.maskedinput/1.4.1/dist/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="https://rawgit.com/KidSysco/jquery-ui-month-picker/v3.0.0/demo/MonthPicker.min.js"></script>
<script type="text/javascript" language="javascript" src="http://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/custom-google-chart.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/todo.js"></script>
<!-- jquery sortable js -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/Sortable.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/sortable-custom.js"></script>
<script type="text/javascript">
   $(document).ready(function(){
   
   $('#all_activity1').DataTable();
      
   });
   
   $(document).ready(function(){
   //  alert('hi');
   $('#month_pic').MonthPicker({ Button: false });
   
    $("#legal_form12").change(function() {
    var val = $(this).val();
    if(val === "from2") {
      // alert('hi');
        $(".from2option").show();
    }
    else {
        $(".from2option").hide();
    }
    });
   
   });
   
   
</script>
<script>
   $(document).ready(function () {
     
     $( "#tabs112" ).tabs({
   
     });
   
     $("#tabs112 ul.md-tabs.tabs112 li").click(function(){
       drawChart1();
        drawChart2();
       drawChart();
     });
   
   
   });
</script>