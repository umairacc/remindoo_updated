<?php
function getStartAndEndDate($week, $year) {
  $dateTime = new DateTime();
  $dateTime->setISODate($year, $week);
  $result['start_date'] = $dateTime->format('d-m-Y');
  $dateTime->modify('+6 days');
  $result['end_date'] = $dateTime->format('d-m-Y');
  return $result;
}
function getDatesFromRange($start, $end, $format = 'Y-m-d') {
    $array = array();
    $interval = new DateInterval('P1D');

    $realEnd = new DateTime($end);
    $realEnd->add($interval);

    $period = new DatePeriod(new DateTime($start), $interval, $realEnd);

    foreach($period as $date) { 
        $array[] = $date->format($format); 
    }

    return $array;
}
function isWeekend($date) {
    return (date('N', strtotime($date)) == 7);
}
?>
<div class="work-month">    
                                <h3>Works Hours Details of <?php echo date('M', mktime(0, 0, 0, $month_ex, 10)).' - '.$year_ex;?></h3>
								<div class="table-responsive">
									<table class="table">
											 <tr>
                                                <?php 
                                                //echo $start_month.'-'.$end_month;
                                                if($month_ex=='12'){
                                                 $x = getStartAndEndDate('53',$year_ex);
                                                 $ex_x = explode('-', $x['start_date']);
                                                 $mx = $ex_x[1];
                                                 if($mx==12)
                                                 {
                                                    $end_month = '53';
                                                 }else{
                                                    $end_month = '52';
                                                 }
                                             }

                                                for ($i=$start_month; $i <=$end_month; $i++) { 
                                                      /*$year = date('Y');
                                                      $m = date('m');*/
                                                      $m = $month_ex;
                                                      $year = $year_ex;
                                                      //echo $m.'-'.$year;
                                                      $dates = getStartAndEndDate($i,$year);

                                                      $alldates = getDatesFromRange($dates['start_date'],$dates['end_date']);
                                                     // echo '<pre>';
                                                      //print_r($alldates);
                                                      
                                                   ?>
                                                  <td class="filed-reject">
                                                     <h4>Week:<?php echo ltrim($i,'0');?></h4>
                                                    <?php  $w = 0;
                                                     foreach ($alldates as $alldates_key => $alldates_value) {

                                                        $weekend = isWeekend($alldates_value);
                                                        //print_r($weekend);
                                                        $exp_d = explode('-', $alldates_value);
                                                        $d = $exp_d[1];

                                                        if( $d == $m )
                                                        {
                                                            if($weekend==''){
                                                                 $w++;
                                                        ?>
                                                    <p><b><?php echo $alldates_value;?></b> <span>8:00 m</span></p><?php } else {?><p><b><?php echo $alldates_value;?></b> <span><a href="#">Holiday</a></span></p> <?php } ?>
                                                    
                                                     <?php   } }
                                                    ?>
                                                     <span class="total-working-hr"><strong>Total Working Hour: </strong> <?php echo $w * 8 ; ?>:0 m</span>
                                               </td>
                                                <?php }?>
                                                 
                                                </tr>

										</table>
								</div>
							</div>					
						