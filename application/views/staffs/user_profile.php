<?php $this->load->view('includes/header');
$status_array=array(1=>"Active",2=>"Inactive",3=>"Frozen",4=>"Draft");
$cmpny_from_array=array(0=>"Manual Client",1=>"Company House",2=>"Import",3=>"From Lead");
?>

<div class="com_class12 profilepagecls">
<div class="pcoded-content card-removes">
<div class="pcoded-inner-content">
<div class="main-body">
<div class="page-wrapper">
<div class="page-body">
  <div class="row">

  <!-- <div class="deadline-crm1 floating_set">
            <ul class="md-tabs tabs112 floating_set ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" id="proposal_dashboard" role="tablist">
               <li class="nav-item ui-tabs-tab ui-corner-top ui-state-default ui-tab" role="tab" tabindex="0" aria-controls="Basics" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true">
                  <a class="nav-link ui-tabs-anchor" href="javascript:;" role="presentation" tabindex="-1" id="ui-id-1">Personal Info
                  </a>
               </li>
            </ul>
    </div> -->

    <div class="col-lg-12 row123 displblockcls">
      <div class="cover-profile">
        <div class="profile-bg-img">
          <div class="card-block user-info">
            <div class="col-md-12">
              <div class="media-left">
                <a href="#" class="profile-image">
                <!-- <img class="user-img img-radius" src="http://html.codedthemes.com/guru-able/files/assets/images/user-profile/user-img.jpg" alt="user-img"> -->
                <?php  $getUserProfilepic = $this->Common_mdl->getUserProfilepic($_SESSION['id']); ?><img src="<?php echo $getUserProfilepic;?> " alt="" width="108" height="108">
                </a>
              </div>
              <div class="media-body row">
                <div class="col-lg-12">
                  <div class="user-title">
                    <h2><?php echo $user[0]['crm_name']; ?></h2>
                    <span class="text-white">Admin</span>
                  </div>
                </div>
                <div>
                  <div class="pull-right cover-btn">
                   <!--  <a href="javascript:;" class="btn btn-primary m-r-10 m-b-5"><i class="icofont icofont-edit"></i> Edit</a> -->
                    <a href="javascript:;" data-toggle="modal" data-target="#profile_change_password" class="btn btn-primary m-r-10 m-b-5"><i class="icofont icofont-edit"></i>Change Password</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-12 padnocls displblockcls">
      <div class="general-info">
        <div class="rowx">
          <div class="col-lg-12 col-xl-6 card">
            <div class="table-responsive">
              <div class="col-sm-8 user-detail">
                <div class="row">
                   <div class="col-sm-5">
                      <h6 class="f-w-400 m-b-30">Name :</h6>
                   </div>
                   <div class="col-sm-7">
                      <h6 class="m-b-30"><?php echo $user[0]['crm_name']; ?></h6>
                   </div>
                </div>
                <div class="row">
                   <div class="col-sm-5">
                      <h6 class="f-w-400 m-b-30">Email</h6>
                   </div>
                   <div class="col-sm-7">
                      <h6 class="m-b-30"><a href="mailto:dummy@example.com"><?php echo $user[0]['crm_email_id']; ?></a></h6>
                   </div>
                </div>
                <div class="row">
                   <div class="col-sm-5">
                      <h6 class="f-w-400 m-b-30">User Name</h6>
                   </div>
                   <div class="col-sm-7">
                      <h6 class="m-b-30"><?php echo $user[0]['username']; ?></h6>
                   </div>
                </div>
                <div class="row">
                   <div class="col-sm-5">
                      <h6 class="f-w-400 m-b-30">Phone No</h6>
                   </div>
                   <div class="col-sm-7">
                      <h6 class="m-b-30"> <?php echo $user[0]['crm_phone_number']; ?></h6>
                   </div>
                </div>
             </div>
            </div>
          </div>
        </div>
      </div>
    </div>



  </div>
  <div class="modal-alertsuccess  alert password_alert_success succs" style="display: none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><div class="pop-realted1"><div class="position-alert1">Password Updated Successfully.</div></div></div>
    <!-- change password popup -->
  <div class="modal fade show" id="profile_change_password" style="display: none;">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h4 class="modal-title">Change Password</h4>
         </div>
         <div class="col-xs-12 inside-popup">
            <div class="pcoded-content pcodedteam-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <!--start-->
                  <div class="update-dept01">
          
                    <form id="change_password_form" method="post" action="" enctype="multipart/form-data">
                      <input type="hidden" name="extra_val" id="extra_val" value="admin">
                      <input type="hidden" name="user_id" id="user_id" value="<?php echo $user[0]['id'] ?>" >
                    
                      <div class="row">
                                  <div class="form-group col-sm-12">
                                     <label>Current Password</label>
                                     <input type="password" class="form-control" name="current_password" id="current_password" placeholder="Enter Current Password" value="" autocomplete="off">
                                     <input type="hidden" name="old_password" id="old_password" value="<?php echo $user[0]['confirm_password']; ?>">
                                     <span class="error_msg" style="color:red;"></span>
                                     
                                  </div>
                                    <div class="form-group col-sm-12">
                                     <label>New Password</label>
                                     <input type="password" class="form-control" name="new_password" id="new_password" placeholder="Enter New Password" value="" autocomplete="off">

                                     <span class="" style="color:red;"></span>
                                  </div>
                                    <div class="form-group col-sm-12">
                                     <label>Re-type Password</label>
                                     <input type="password" class="form-control" name="retype_password" id="retype_password" placeholder="Re-type New Password" value="" autocomplete="off">
                                     <span class="" style="color:red;"></span>
                                  </div>
                                  <div class="form-group create-btn col-sm-12 popupbuttoncls">
                                     <input type="submit" name="add_task" id="add_task" class="btn-primary" value="Update"/>
                                     <input type="button" class="btn-primary" data-dismiss="modal" value="Close"/>
                                  </div>
                      </div>
                     </form>
                     </div>
                  </div>
                  <!-- close -->
               </div>
            </div>
            <!-- Page body end -->
         </div>
      </div>
      <!-- Main-body end -->
      <div id="styleSelector">
      </div>
   </div>
         </div>
      </div>
   </div>
</div>
  <!-- end of change password -->
  <div class="row">
    <div class="col-lg-12 nopadding">
      <div class="tab-content">
        <div class="tab-pane active" id="personal" role="tabpanel" aria-expanded="true">
          <div class="card">
            <div class="card-block">
              <div class="view-info">
                <div class="rowx">
                  
                </div>
              </div>
              <div class="edit-info" style="display: none;">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="general-info">
                      <div class="row">
                        <div class="col-lg-6">
                          <table class="table">
                            <tbody>
                              <tr>
                                <td>
                                  <div class="input-group">
                                    <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                                    <input type="text" class="form-control" placeholder="Full Name">
                                  </div>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <div class="form-radio">
                                    <div class="group-add-on">
                                      <div class="radio radiofill radio-inline">
                                        <label>
                                        <input type="radio" name="radio" checked=""><i class="helper"></i> Male
                                        </label>
                                      </div>
                                      <div class="radio radiofill radio-inline">
                                        <label>
                                        <input type="radio" name="radio"><i class="helper"></i> Female
                                        </label>
                                      </div>
                                    </div>
                                  </div>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <input id="dropper-default" class="form-control" type="text" placeholder="Select Your Birth Date">
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <select id="hello-single" class="form-control">
                                    <option value="">---- Marital Status ----</option>
                                    <option value="married">Married</option>
                                    <option value="unmarried">Unmarried</option>
                                  </select>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <div class="input-group">
                                    <span class="input-group-addon"><i class="icofont icofont-location-pin"></i></span>
                                    <input type="text" class="form-control" placeholder="Address">
                                  </div>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        <div class="col-lg-6">
                          <table class="table">
                            <tbody>
                              <tr>
                                <td>
                                  <div class="input-group">
                                    <span class="input-group-addon"><i class="icofont icofont-mobile-phone"></i></span>
                                    <input type="text" class="form-control" placeholder="Mobile Number">
                                  </div>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <div class="input-group">
                                    <span class="input-group-addon"><i class="icofont icofont-social-twitter"></i></span>
                                    <input type="text" class="form-control" placeholder="Twitter Id">
                                  </div>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <div class="input-group">
                                    <span class="input-group-addon"><i class="icofont icofont-social-skype"></i></span>
                                    <input type="email" class="form-control" placeholder="Skype Id">
                                  </div>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <div class="input-group">
                                    <span class="input-group-addon"><i class="icofont icofont-earth"></i></span>
                                    <input type="text" class="form-control" placeholder="website">
                                  </div>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div class="text-center">
                        <a href="#!" class="btn btn-primary waves-effect waves-light m-r-20">Save</a>
                        <a href="#!" id="edit-cancel" class="btn btn-default waves-effect">Cancel</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
<?php $this->load->view('includes/footer');?>
<script src="<?php echo  base_url()?>assets/js/jquery.lineProgressbar.js"></script>
<script src="<?php echo  base_url()?>assets/js/c3-custom-chart.js"></script>
<script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script> 
<script src="https://rawgit.com/digitalBush/jquery.maskedinput/1.4.1/dist/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="https://rawgit.com/KidSysco/jquery-ui-month-picker/v3.0.0/demo/MonthPicker.min.js"></script>
<script type="text/javascript" language="javascript" src="http://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/custom-google-chart.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/todo.js"></script>
<!-- jquery sortable js -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/Sortable.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/sortable-custom.js"></script>

<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
  $.extend($.validator.messages, {
  //  required: "This field is required.",
    remote: "",
    equalTo: "Please Enter same password as above",
});
});
  $( "#change_password_form" ).validate({
     rules: {
    current_password: {
      required: true,
     // equalTo: "#old_password",
    },
    new_password: {
      required: true,
    },
    retype_password: {
      required: true,
      equalTo: "#new_password",
    },
  },
  message:{
      current_password: {
            required: "This Field is required",
           // equalTo: "Please enter Current Password",
                 },
      new_password: {
          required: "This Field is required",
                },
      retype_password: {
              required: "This Field is required",
              equalTo: "Please enter the same password as above aaa"
                },
  },
   submitHandler: function(form) {
    var current=$('#current_password').val();
    var old_password=$('#old_password').val();
    if(current!=old_password)
    {
      $('.error_msg').html('Your Current password is wrong ');
    }else{
        var formData = new FormData($("#change_password_form")[0]);
        $(".LoadingImage").show();

         $.ajax({
            url:  '<?php echo base_url();?>staff/user_change_password/',
            type: 'POST',
            data: $("#change_password_form").serialize(),
            success: function(response) {
              //  alert(response);
              $('.password_alert_success').show();
              $(".LoadingImage").hide();
              location.reload(true);
                return false;
            }            
        });
          
    }
    return false;
                        
                         },
    
});
 // });
$('#current_password').on('input',function(){
  $('.error_msg').html('');
});
 
</script>
