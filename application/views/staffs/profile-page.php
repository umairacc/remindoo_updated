<?php 

$this->load->view('includes/header');
//$role_array=array(1=>"admin",2=>"Sub Admin",3=>"Director",4=>"Client",5=>"Manager",6=>"Staff");
?>
<div class="com_class12 profilepagecls">
<div class="pcoded-content card-removes profile-page-wrapper">
<div class="pcoded-inner-content">
<div class="main-body">
<div class="page-wrapper">
<div class="page-body">
  <div class="row">
  <!-- <div class="deadline-crm1 floating_set">
      <ul class="md-tabs tabs112 floating_set ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" id="proposal_dashboard" role="tablist">
         <li class="nav-item ui-tabs-tab ui-corner-top ui-state-default ui-tab" role="tab" tabindex="0" aria-controls="Basics" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true">
            <a class="nav-link ui-tabs-anchor" href="javascript:;" role="presentation" tabindex="-1" id="ui-id-1">Personal Info
            </a>
         </li>
      </ul>
    </div> -->
    <div class="col-lg-12 row123 displblockcls">
      <div class="cover-profile">
        <div class="profile-bg-img">
          <div class="card-block user-info">
            <div class="col-md-12">
              <div class="media-left media-leftside">
                <a href="#" class="profile-image">
                <!--  <img class="user-img img-radius" src="http://html.codedthemes.com/guru-able/files/assets/images/user-profile/user-img.jpg" alt="user-img">  -->
                <?php

              
                   if(isset($staff['profile']) && $staff['profile']!='') { ?>
                      <img class="user-img img-radius" src="<?php echo base_url().'uploads/'.$staff['profile']?>" id="preview_image" width="108" height="108" >
                   <?php } else { ?>
                      <img class="user-img img-radius sampleimgclsnew" src="<?php echo base_url().'uploads/unknown.png'?>"  id="preview_image"  >
                   <?php }  ?>   
                </a>
              </div>
              <div class="media-body row">
                <div class="col-lg-12">
                  <div class="user-title">
                    <h2><?php echo $staff['first_name']." ".$staff['last_name'] ?></h2>
                    <span class="text-white"><?php echo $role['role']; ?></span>
                  </div>
                </div>
                <div>
                  <div class="pull-right cover-btn">
                    <a href="<?php echo base_url().'staff/staff_profile_edit/'.$staff['user_id'];?>" class="btn btn-primary m-r-10 m-b-5"><i class="icofont icofont-edit"></i> Edit</a>

                     <!-- <a href="javascript:;" data-toggle="modal" data-target="#profile_change_password" class="btn btn-primary m-r-10 m-b-5">Change Password</a> -->
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-12 padnocls displblockcls">
      <div class="general-info">
        <div class="rowx">
          <div class="col-lg-12 col-xl-6 card">
            <div class="table-responsive">
              <div class="col-sm-8 user-detail">
                <div class="row">
                   <div class="col-sm-5">
                      <h6 class="f-w-400 m-b-30">Full Name :</h6>
                   </div>
                   <div class="col-sm-7">
                      <h6 class="m-b-30"><?php echo $staff['first_name']." ".$staff['last_name'] ?></h6>
                   </div>
                </div>
                <div class="row">
                   <div class="col-sm-5">
                      <h6 class="f-w-400 m-b-30">Email</h6>
                   </div>
                   <div class="col-sm-7">
                      <h6 class="m-b-30"><a href="<?php echo $staff['email_id']; ?>"><?php echo $staff['email_id']; ?></a></h6>
                   </div>
                </div>
                <div class="row">
                   <div class="col-sm-5">
                      <h6 class="f-w-400 m-b-30">Mobile Number</h6>
                   </div>
                   <div class="col-sm-7">
                      <h6 class="m-b-30"><?php echo $staff['telephone_number']; ?></h6>
                   </div>
                </div>
                <div class="row">
                   <div class="col-sm-5">
                      <h6 class="f-w-400 m-b-30">User Name</h6>
                   </div>
                   <div class="col-sm-7">
                      <h6 class="m-b-30"><?php echo $staff['username']; ?></h6>
                   </div>
                </div>
             </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-12 padnocls displblockcls">
      <div class="general-info">
        <div class="rowx">
          <div class="col-lg-12 col-xl-6 card">
            <div class="table-responsive">
              <div class="col-sm-8 user-detail">
                <div class="row">
                   <div class="col-sm-5">
                      <h6 class="f-w-400 m-b-30">Facebook</h6>
                   </div>
                   <div class="col-sm-7">
                      <h6 class="m-b-30"><a href="<?php echo (!empty($staff['facebook'])?$staff['facebook'] : '-'); ?>" target="_blank"><?php echo (!empty( $staff['facebook'])?$staff['facebook']:'-'); ?></a></h6>
                   </div>
                </div>
                <div class="row">
                   <div class="col-sm-5">
                      <h6 class="f-w-400 m-b-30">Skype</h6>
                   </div>
                   <div class="col-sm-7">
                      <h6 class="m-b-30"><?php echo (!empty($staff['skype'])?$staff['skype']:'-'); ?></h6>
                   </div>
                </div>
                <div class="row">
                   <div class="col-sm-5">
                      <h6 class="f-w-400 m-b-30">LinkedIn</h6>
                   </div>
                   <div class="col-sm-7">
                      <h6 class="m-b-30"><a href="<?php echo (!empty($staff['linkedin'])?$staff['linkedin'] : '-'); ?>" target="_blank"><?php echo (!empty($staff['linkedin'])?$staff['linkedin'] : '-'); ?></a></h6>
                   </div>
                </div>
                <div class="row">
                   <div class="col-sm-5">
                      <h6 class="f-w-400 m-b-30">Hourly Rate</h6>
                   </div>
                   <div class="col-sm-7">
                      <h6 class="m-b-30"><?php echo $staff['hourly_rate']; ?></h6>
                   </div>
                </div>
             </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
  <!-- change password popup -->
  <div class="modal fade show" id="profile_change_password" style="display: none;">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h4 class="modal-title">Change Password</h4>
         </div>
         <div class="col-xs-12 inside-popup">
            <div class="pcoded-content pcodedteam-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <!--start-->
                  <div class="update-dept01">
          
                    <form id="change_password_form" method="post" action="" enctype="multipart/form-data">
                      <input type="hidden" name="user_id" id="user_id" value="<?php echo $staff['user_id'] ?>" >
                      <input type="hidden" name="staff_id" id="staff_id" value="<?php echo $staff['id'] ?>" >
                      <div class="row">
                                  <div class="form-group col-sm-12">
                                     <label>Current Password</label>
                                     <input type="password" class="form-control" name="current_password" id="current_password" placeholder="Enter Current Password" value="" autocomplete="off">
                                     <input type="hidden" name="old_password" id="old_password" value="<?php echo $staff['confirm_password']; ?>">
                                     <span class="" style="color:red;"></span>
                                     
                                  </div>
                                    <div class="form-group col-sm-12">
                                     <label>New Password</label>
                                     <input type="password" class="form-control" name="new_password" id="new_password" placeholder="Enter New Password" value="" autocomplete="off">

                                     <span class="error_msg" style="color:red;"></span>
                                   
                                  </div>
                                    <div class="form-group col-sm-12">
                                     <label>Re-type Password</label>
                                     <input type="password" class="form-control" name="retype_password" id="retype_password" placeholder="Re-type New Password" value="" autocomplete="off">

                                     <span class="" style="color:red;"></span>
                                  
                                  </div>
                                  
                                                   
                              
                                  <div class="form-group create-btn col-sm-12 popupbuttoncls">
                                     <input type="submit" name="add_task" id="add_task" class="btn-primary" value="Update"/>
                                     <input type="button" class="btn-primary" data-dismiss="modal" value="Close"/>
                                  </div>
                      </div>
                     </form>
                     </div>
                  </div>
                  <!-- close -->
               </div>
            </div>
            <!-- Page body end -->
         </div>
      </div>
      <!-- Main-body end -->
      <div id="styleSelector">
      </div>
   </div>
         </div>
      </div>
   </div>
</div>
  <!-- end of change password -->
  <div class="row">
    <div class="col-lg-12 nopadding">
      <div class="tab-content">
        <div class="tab-pane active" id="personal" role="tabpanel" aria-expanded="true">
          <div class="card">
            <div class="card-block">
              <div class="view-info">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="general-info">
                      <div class="row">

                        

                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
            </div>
          </div>

        </div>

      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
<?php $this->load->view('includes/footer');?>
<script src="<?php echo  base_url()?>assets/js/jquery.lineProgressbar.js"></script>
<script src="<?php echo  base_url()?>assets/js/c3-custom-chart.js"></script>
<script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script> 
<script src="https://rawgit.com/digitalBush/jquery.maskedinput/1.4.1/dist/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="https://rawgit.com/KidSysco/jquery-ui-month-picker/v3.0.0/demo/MonthPicker.min.js"></script>
<script type="text/javascript" language="javascript" src="http://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/custom-google-chart.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/todo.js"></script>
<!-- jquery sortable js -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/Sortable.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/sortable-custom.js"></script>

<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
  $.extend($.validator.messages, {
  //  required: "This field is required.",
    remote: "",
    equalTo: "Please Enter same password as above",
});
});
  $( "#change_password_form" ).validate({
     rules: {
    current_password: {
      required: true,
     // equalTo: "#old_password",
    },
    new_password: {
      required: true,
    },
    retype_password: {
      required: true,
      equalTo: "#new_password",
    },
  },
  message:{
      current_password: {
            required: "This Field is required",
           // equalTo: "Please enter Current Password",
                 },
      new_password: {
          required: "This Field is required",
                },
      retype_password: {
              required: "This Field is required",
              equalTo: "Please enter the same password as above aaa"
                },
  },
   submitHandler: function(form) {
    var current=$('#current_password').val();
    var old_password=$('#old_password').val();
    // console.log(current);
    //  console.log(old_password);
    if(current==old_password)
    {
      $('.error_msg').html('Your New password is equal to Old Password ');
    }
    else
    {
      var formData = new FormData($("#change_password_form")[0]);
      $(".LoadingImage").show();

       $.ajax({
          url:  '<?php echo base_url();?>staff/user_change_password/',
          type: 'POST',
          data: $("#change_password_form").serialize(),
          success: function(response) {
            //  alert(response);
            $(".LoadingImage").hide();
            location.reload(true);
              return false;
          }            
      }); 
    }
    return false;
                        
                         },
    
});
 // });
$('#current_password').on('input',function(){
  $('.error_msg').html('');
});
 
</script>
