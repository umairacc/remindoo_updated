<?php $this->load->view('includes/header');?>

<link href="<?php echo  base_url()?>assets/css/c3.css" rel="stylesheet" type="text/css">

<div class="pcoded-inner-content">

<div class="title_page01 add_profile_01 floating">

<div class="profile_setting1 floating">
            <div class="col-sm-4">
                <div class="profile_task1">
                        <span>1</span>
                        <strong>Open Projects</strong>
                        <a href="#">More info &gt; </a>
                </div>

                 <div class="profile_task1">
                        <span>2</span>
                        <strong>Open Tasks</strong>
                        <a href="#">More info &gt; </a>
                </div>

                 <div class="profile_task1">
                        <span>3</span>
                        <strong>Complete Projects</strong>
                        <a href="#">More info &gt; </a>
                </div>

                 <div class="profile_task1">
                        <span>4</span>
                        <strong>Complete Tasks</strong>
                        <a href="#">More info &gt; </a>
                </div>
            </div> <!-- col-sm-8 -->

            <div class="col-sm-4">
                <div class="profile_display1">
                        <img src="http://remindoo.org/CRMTool/assets/images/avatar-4.jpg">
                        <h3>Admin</h3>
                        <span>EMP ID: 1</span>
                        <strong>PHP -&gt; Admin</strong>
               </div>  
               
               </div> <!-- col-sm-4 -->   
    	 <div class="col-sm-4">
                <div class="profile_task1">
                        <span>1</span>
                        <strong>Open Projects</strong>
                        <a href="#">More info &gt; </a>
                </div>

                 <div class="profile_task1">
                        <span>2</span>
                        <strong>Open Tasks</strong>
                        <a href="#">More info &gt; </a>
                </div>

                 <div class="profile_task1">
                        <span>3</span>
                        <strong>Complete Projects</strong>
                        <a href="#">More info &gt; </a>
                </div>

                 <div class="profile_task1">
                        <span>4</span>
                        <strong>Complete Tasks</strong>
                        <a href="#">More info &gt; </a>
                </div>
            </div> <!-- col-sm-4 -->
    </div>

    <div class="overall_hours1 top-margin01 floating">   
        <div class="project_hour01">
            <span>4:15:29</span>
            <h4>Projects Hours</h4>
        </div>
        <div class="project_hour01">
            <span>8:23:29</span>
            <h4>Tasks Hours</h4>
        </div>
        <div class="project_hour01">
            <span>0:0m</span>
            <h4>This month working Hours</h4>
        </div>
        <div class="project_hour01">
            <span>22:20m</span>
            <h4>Working Hours</h4>
        </div>    
    </div>

</div> <!-- title_page01 -->	

    <div class="additional-tab floating_set">
    <div class="deadline-crm1 floating_set">
					<ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                      <li class="nav-item all-client-icon1">
						<span><i class="fa fa-user fa-6" aria-hidden="true"></i></span>
					 </li>	

					  <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#bank_details">Bank Details</a>
                        <div class="slide"></div>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#time_card">Time Card - Login</a>
                        <div class="slide"></div>
                      </li>
                        <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#task_summary">Task summary</a>
                        <div class="slide"></div>
                      </li>
                        <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#all_activity">All activity log</a>
                        <div class="slide"></div>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#logout">Logout</a>
                        <div class="slide"></div>
                      </li>
                    </ul>
        </div>  

        <div class="all_user-section2 floating_set">
                      <div class="tab-content">
                        <div id="bank_details" class="tab-pane fade">
                        	<div class="new_bank1">	
                        		<h2>New Bank</h2>
                        		<div class="bank-form">	
                        			<form>
                        				<div class="form-group">
                        					<label>Bank Name</label>
                        					<input type="name" name="bank-name">
                        				</div>
                        				<div class="form-group">
                        					<label>Branch Name</label>
                        					<input type="name" name="Branch-name">
                        				</div>
                        				<div class="form-group">
                        					<label>Account Name</label>
                        					<input type="name" name="account-name">
                        				</div>
                        				<div class="form-group">
                        					<label>A/C number</label>
                        					<input type="name" name="account">
                        				</div>
                        				<div class="text-right new-bank-update">
                        					<button type="button" name="cancel">Cancel</button>
                        					<button type="button" name="cancel">Update</button>
                        				</div>	

                        			</form>
                        		</div>	
                        	</div>	
                        </div>	<!-- tab1 -->

                        <div id="time_card" class="tab-pane fade">
                        	<div class="new_bank1">	
                        		<h2>Timecard Details</h2>
                        			<div class="working-hrs bank-form">
                        				<div class="form-group date_birth">
                        					<form>
                                             <label class="col-form-label">Month *</label>
                                             <div class="working-picker">
                                                <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                <input class="form-control datepicker fields" type="text" name="engagement_letter" id="engagement_letter" value="">
                                             </div>
                                             <div class="go-bts">
                                             		<button type="button" name="cancel">Go</button>
                                             </div>		
                                         </form>
                                          </div>
                                    </div>

                                <div class="work-month">    
                                <h3>Works Hours Details of October-2017</h3>
								<div class="table-responsive">
									<table class="table">
											<tr>
												<td>
												<h4>Week:39</h4>
												<p>01-10-2017</p>
												<span><a href="#">Holiday</a></span>
												<span class="total-working-hr"><strong>Total Working Hour: </strong> 0:0 m</span>
												</td>

												<td class="filed-reject">
													<h4>Week:40</h4>
													<p><b>02-10-17</b> <span>0:0 m</span></p>
													<p><b>02-10-17</b> <span><a href="#">Holiday</a></span></p>
													<p><b>02-10-17</b> <span>0:0 m</span></p>
													<p><b>02-10-17</b> <span>0:0 m</span></p>
													<p><b>02-10-17 </b><span><a href="#">Holiday</a></span></p>
													<p><b>02-10-17</b> <span>0:0 m</span></p>
													<p class="removed-02"><b>02-10-17</b> <span><a href="#">Holiday</a></span></p>
													<span class="total-working-hr"><strong>Total Working Hour: </strong> 0:0 m</span>
												</td>

												<td class="filed-reject">
													<h4>Week:40</h4>
													<p><b>02-10-17</b> <span>0:0 m</span></p>
													<p><b>02-10-17</b> <span><a href="#">Holiday</a></span></p>
													<p><b>02-10-17</b> <span>0:0 m</span></p>
													<p><b>02-10-17</b> <span>0:0 m</span></p>
													<p><b>02-10-17 </b><span><a href="#">Holiday</a></span></p>
													<p><b>02-10-17</b> <span>0:0 m</span></p>
													<p class="removed-02"><b>02-10-17</b> <span><a href="#">Holiday</a></span></p>
													<span class="total-working-hr"><strong>Total Working Hour: </strong> 0:0 m</span>
												</td>	



											</tr>
										</table>
								</div>
							</div>					
						</div>	
					</div> <!-- tab2 -->	

					<div id="task_summary" class="tab-pane fade ">
						<div class="new_bank1">	
                        		<h2>Task summary- With dates filters</h2>	
                        			<div class="task-filter1">
                        				<div class="time-spent">
                        					<h4>Total Task Time Spent</h4>
                        						<!-- <div id="defaultCountdown"></div> -->
                        							<div class="timer-section01">
                        								<span>8 <strong>Hours</strong></span> :
                        								<span>23 <strong>Minutes</strong></span> :
                        								<span>44<strong>Seconds</strong></span> 
                        							</div>	
                        				</div>

                        				<div class="time-spent">
                        					<h4>Task Reports</h4>
                        				
											
											<div class="chart-container-01">
												<div class="open-closed1">
											<span> <i class="fa fa-square fa-6" aria-hidden="true"></i>Critical</span>
											<span> <i class="fa fa-square fa-6" aria-hidden="true"></i> High</span>

											<span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Medium</span>
											<span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Low</span>

											<span> <i class="fa fa-square fa-6" aria-hidden="true"></i> Resolved</span>
											<span> <i class="fa fa-square fa-6" aria-hidden="true"></i> closed</span>

											</div>

                        					<div id="chartContainer" style="width: 100%; height: 300px"></div> 
                        				</div>
                        			</div>
                        					
                        			</div>			
                        		</div>
                        </div>		<!-- tab3 -->	

                        <div id="all_activity" class="tab-pane fade in active">
                        	<div class="new_bank1">	
                        		<h2>All activity log</h2>	
                        	<div class="all_user-section2">
                        		<table class="table client_table1 text-center display nowrap" id="all_activity" cellspacing="0" width="100%">
                        			<thead>
                        				<tr>
                        						<th>Activity Date</th>
                        						<th>User</th>
                        						<th>Module</th>
                        						<th>Activity</th>
                        				</tr>
                        			</thead>

                        			<tbody>
                        				<tr>
                        					<td>19-10-2017 12:20:40</td>
                        					<td>admin</td>
                        					<td>User</td>
                        					<td>Staff Deactive</td>
                        				</tr>
                        				
                        				<tr>
                        					<td>19-10-2017 12:20:40</td>
                        					<td>admin</td>
                        					<td>User</td>
                        					<td>Staff Deactive</td>
                        				</tr>

                        				<tr>
                        					<td>19-10-2017 12:20:40</td>
                        					<td>admin</td>
                        					<td>User</td>
                        					<td>Staff Deactive</td>
                        				</tr>	
                        			</tbody>
                        		</table>
                        	</div>
                        </div>	<!-- tab4 -->					




                                          


                    
                    </div>  <!-- tab-content --> 
                     
    </div>    


</div> <!-- additional-tab -->

</div>

<?php $this->load->view('includes/footer');?>

<script src="<?php echo  base_url()?>assets/js/jquery.lineProgressbar.js"></script>
<script src="<?php echo  base_url()?>assets/js/c3-custom-chart.js"></script>
<script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script> 

<!-- <script src="<?php echo base_url();?>assets/js/jquery.plugin.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.countdown.js"></script>
<script>
$(function () {
	var austDay = new Date();
	austDay = new Date(austDay.getFullYear() + 1, 1 - 1, 26);
	$('#defaultCountdown').countdown({until: austDay});
	$('#year').text(austDay.getFullYear());
});
</script> -->

<script type="text/javascript"> 
window.onload = function() { 
	$("#chartContainer").CanvasJSChart({ 
		title: { 
			fontSize: 24
		}, 
		axisY: { 
			title: "Products in %" 
		}, 
		data: [ 
		{ 
			type: "pie", 
			toolTipContent: "{label} <br/> {y} %", 
			indexLabel: "{y} %", 
			dataPoints: [ 
				{ label: "Critical",  y: 30.3, legendText: "Critical"}, 
				{ label: "High",    y: 19.1, legendText: "High"  }, 
				{ label: "Medium",   y: 4.0,  legendText: "Medium" }, 
				{ label: "Low",       y: 3.8,  legendText: "Low"}, 
				{ label: "Resolved",   y: 3.2,  legendText: "Resolved" }, 
				{ label: "Closed",   y: 39.6, legendText: "Closed" } 
			] 
		} 
		] 
	}); 
	
	$("#chartContainer1").CanvasJSChart({ 
		title: { 
			fontSize: 24
		}, 
		axisY: { 
			title: "Products in %" 
		}, 
		data: [ 
		{ 
			type: "pie", 
			toolTipContent: "{label} <br/> {y} %", 
			indexLabel: "{y} %", 
			dataPoints: [ 
				{ label: "Open",  y: 30.3, legendText: "Open"}, 
				{ label: "Closed",    y: 19.1, legendText: "Closed"  }, 
				
			] 
		} 
		] 
	}); 

	$(document).ready(function(){
    $('#all_activity').DataTable();
});
} 
</script> 