<!DOCTYPE html>

<html lang="en">



<head>

    <title>CRM </title>

    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 10]>

      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

      <![endif]-->

    <!-- Meta -->

   <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">

    <!-- Favicon icon -->

    <link rel="icon" href="<?php echo base_url();?>assets/images/favicon.ico" type="image/x-icon">

    <!-- Google font-->

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">

    <!-- Required Fremwork -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>bower_components/bootstrap/css/bootstrap.min.css">

    <!-- themify-icons line icon -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icon/themify-icons/themify-icons.css">

    <!-- ico font -->
    
    
      <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icon/icofont/css/icofont.css">



    <!-- flag icon framework css -->

    

    <!-- Menu-Search css -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/pages/menu-search/css/component.css">

    <!-- jpro forms css -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/pages/j-pro/css/demo.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/pages/j-pro/css/font-awesome.min.css">
<!-- 
    <link rel="stylesheet" type="text/css" href="assets/pages/j-pro/css/j-pro-modern.css"> -->

    <!-- Style.css -->

    

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/pages/advance-elements/css/bootstrap-datetimepicker.css">
    <!-- Date-range picker css  -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>bower_components/bootstrap-daterangepicker/css/daterangepicker.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>bower_components/datedropper/css/datedropper.min.css" />

    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https:/resources/demos/style.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>bower_components/switchery/css/switchery.min.css">



    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery.mCustomScrollbar.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>bower_components/owl.carousel/css/owl.carousel.css">

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery.mCustomScrollbar.css">
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/common_style.css?ver=4">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/common_responsive.css?ver=2">

    <style>
    .common_form_section1  .col-xs-12.col-sm-6.col-md-6 {
    float: left !important;
}
.common_form_section1 {
    background: #fff;
    box-shadow: 1px 2px 5px #dbdbdb;
    border-radius: 5px;
    padding: 30px;
}
.common_form_section1 label{
	display: block;
	margin-bottom: 7px;
	font-size: 15px;
}
.common_form_section1 input{
	width: 100%;
}
.col-sm-12{
	float: left;
}

.col-sm-8 {
    float: left;
}
</style>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/custom/opt-profile.css?<?php echo time(); ?>">
</head>



<body>

    <!-- Pre-loader start -->

    <div class="theme-loader">

        <div class="ball-scale">

            <div class='contain'>

                <div class="ring">

                    <div class="frame"></div>

                </div>

                <div class="ring">

                    <div class="frame"></div>

                </div>

                <div class="ring">

                    <div class="frame"></div>

                </div>

                <div class="ring">

                    <div class="frame"></div>

                </div>

                <div class="ring">

                    <div class="frame"></div>

                </div>

                <div class="ring">

                    <div class="frame"></div>

                </div>

                <div class="ring">

                    <div class="frame"></div>

                </div>

                <div class="ring">

                    <div class="frame"></div>

                </div>

                <div class="ring">

                    <div class="frame"></div>

                </div>

                <div class="ring">

                    <div class="frame"></div>

                </div>

            </div>

        </div>

    </div>

    <!-- Pre-loader end -->



    <div id="pcoded" class="pcoded">

        <div class="pcoded-overlay-box"></div>

        <div class="pcoded-container navbar-wrapper">

            <nav class="navbar header-navbar pcoded-header">

                <div class="navbar-wrapper">



                    <div class="navbar-logo">

                        <a class="mobile-menu" id="mobile-collapse" href="#!">

                            <i class="ti-menu"></i>

                        </a>

                        <a class="mobile-search morphsearch-search" href="#">

                            <i class="ti-search"></i>

                        </a>

                        <a href="index.html">

                            <img class="img-fluid" src="<?php echo base_url();?>assets/images/logo.png" alt="Theme-Logo" />

                        </a>

                        <a class="mobile-options">

                            <i class="ti-more"></i>

                        </a>

                    </div>



                    <div class="navbar-container container-fluid">

                      <div class="nav-left ">

                        <ul class="owl-carousel carousel-nav owl-theme">

                                       <li><a href="#">Dashboard</a></li>

                                       <li><a href="#">Analytics</a></li>

                                       <li><a href="#">comments</a></li>

                                       <li><a href="#">Pages</a></li>

                                       <li><a href="#">Help</a></li>

                                       

                                       <li><a href="#">User</a></li>

                                       <li><a href="#">setting</a></li>

                                       <li><a href="#">Logout</a></li>

                                       <li><a href="#">Post</a></li>

                                       

                        </ul>

                        </div>

                        <ul class="nav-right">

                            <li class="header-notification lng-dropdown">

                                <a href="#" id="dropdown-active-item">

                                    <i class="flag-icon flag-icon-gb m-r-5"></i> English

                                </a>

                                <ul class="show-notification">

                                    <li>

                                        <a href="#" data-lng="en">

                                            <i class="flag-icon flag-icon-gb m-r-5"></i> English

                                        </a>

                                    </li>

                                    <li>

                                        <a href="#" data-lng="es">

                                            <i class="flag-icon flag-icon-es m-r-5"></i> Spanish

                                        </a>

                                    </li>

                                    <li>

                                        <a href="#" data-lng="fr">

                                            <i class="flag-icon flag-icon-fr m-r-5"></i> French

                                        </a>

                                    </li>

                                </ul>

                            </li>

                            <li class="header-notification">

                                <a href="#!">

                                    <i class="ti-bell"></i>

                                    <span class="badge bg-c-pink"></span>

                                </a>

                                <ul class="show-notification">

                                    <li>

                                        <h6>Notifications</h6>

                                        <label class="label label-danger">New</label>

                                    </li>

                                    <li>

                                        <div class="media">

                                            <img class="d-flex align-self-center" src="<?php echo base_url();?>assets/images/user.png" alt="Generic placeholder image">

                                            <div class="media-body">

                                                <h5 class="notification-user">Admin</h5>

                                                <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>

                                                <span class="notification-time">30 minutes ago</span>

                                            </div>

                                        </div>

                                    </li>

                                    <li>

                                        <div class="media">

                                            <img class="d-flex align-self-center" src="assets/images/user.png" alt="Generic placeholder image">

                                            <div class="media-body">

                                                <h5 class="notification-user">Joseph William</h5>

                                                <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>

                                                <span class="notification-time">30 minutes ago</span>

                                            </div>

                                        </div>

                                    </li>

                                    <li>

                                        <div class="media">

                                            <img class="d-flex align-self-center" src="assets/images/user.png" alt="Generic placeholder image">

                                            <div class="media-body">

                                                <h5 class="notification-user">Sara Soudein</h5>

                                                <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>

                                                <span class="notification-time">30 minutes ago</span>

                                            </div>

                                        </div>

                                    </li>

                                </ul>

                            </li>

                            <li class="header-notification">

                                <a href="#!" class="displayChatbox">

                                    <i class="ti-comments"></i>

                                    <span class="badge bg-c-green"></span>

                                </a>

                            </li>

                            <li class="user-profile header-notification">

                                <a href="#!">

                                    <img src="<?php echo base_url();?>assets/images/avatar-4.jpg" class="img-radius" alt="User-Profile-Image">

                                    <span>Admin</span>

                                    <i class="ti-angle-down"></i>

                                </a>

                                <ul class="tgl-opt">

                                    <li>

                                        <a href="#!">

                                            <i class="ti-settings"></i> Settings

                                        </a>

                                    </li>

                                    <li>

                                        <a href="user-profile.html">

                                            <i class="ti-user"></i> Profile

                                        </a>

                                    </li>

                                    <li>

                                        <a href="email-inbox.html">

                                            <i class="ti-email"></i> My Messages

                                        </a>

                                    </li>

                                    <li>

                                        <a href="auth-lock-screen.html">

                                            <i class="ti-lock"></i> Lock Screen

                                        </a>

                                    </li>

                                    <li>

                                        <a href="auth-normal-sign-in.html">

                                        <i class="ti-layout-sidebar-left"></i> Logout

                                    </a>

                                    </li>

                                </ul>

                            </li>

                        </ul>

                        <!-- search -->

                        <div id="morphsearch" class="morphsearch">

                            <form class="morphsearch-form">

                                <input class="morphsearch-input" type="search" placeholder="Search..." />

                                <button class="morphsearch-submit" type="submit">Search</button>

                            </form>

                            <div class="morphsearch-content">

                                <div class="dummy-column">

                                    <h2>People</h2>

                                    <a class="dummy-media-object" href="#!">

                                        <img src="<?php echo base_url();?>assets/images/avatar-1.jpg" alt="Sara Soueidan" />

                                        <h3>Sara Soueidan</h3>

                                    </a>

                                    <a class="dummy-media-object" href="#!">

                                        <img src="<?php echo base_url();?>assets/images/avatar-2.jpg" alt="Shaun Dona" />

                                        <h3>Shaun Dona</h3>

                                    </a>

                                </div>

                                <div class="dummy-column">

                                    <h2>Popular</h2>

                                    <a class="dummy-media-object" href="#!">

                                        <img src="<?php echo base_url();?>assets/images/avatar-3.jpg" alt="PagePreloadingEffect" />

                                        <h3>Page Preloading Effect</h3>

                                    </a>

                                    <a class="dummy-media-object" href="#!">

                                        <img src="<?php echo base_url();?>assets/images/avatar-4.jpg" alt="DraggableDualViewSlideshow" />

                                        <h3>Draggable Dual-View Slideshow</h3>

                                    </a>

                                </div>

                                <div class="dummy-column">

                                    <h2>Recent</h2>

                                    <a class="dummy-media-object" href="#!">

                                        <img src="<?php echo base_url();?>assets/images/avatar-5.jpg" alt="TooltipStylesInspiration" />

                                        <h3>Tooltip Styles Inspiration</h3>

                                    </a>

                                    <a class="dummy-media-object" href="#!">

                                        <img src="<?php echo base_url();?>assets/images/avatar-6.jpg" alt="NotificationStyles" />

                                        <h3>Notification Styles Inspiration</h3>

                                    </a>

                                </div>

                            </div>

                            <!-- /morphsearch-content -->

                            <span class="morphsearch-close"><i class="icofont icofont-search-alt-1"></i></span>

                        </div>

                        <!-- search end -->

                    </div>

                </div>

            </nav>



            

            <div class="pcoded-main-container">

                <div class="pcoded-wrapper">

                    <nav class="pcoded-navbar">

                        <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>

                        <div class="pcoded-inner-navbar main-menu">

                            <div class="">

                                <div class="main-menu-header">

                                    <img class="img-40 img-radius" src="<?php echo base_url();?>assets/images/avatar-4.jpg" alt="User-Profile-Image">

                                    <div class="user-details">

                                        <span>Admin</span>

                                        <span id="more-details">UX Designer<i class="ti-angle-down"></i></span>

                                    </div>

                                </div>



                                <div class="main-menu-content">

                                    <ul>

                                        <li class="more-details">

                                            <a href="user-profile.html"><i class="ti-user"></i>View Profile</a>

                                            <a href="#!"><i class="ti-settings"></i>Settings</a>

                                            <a href="auth-normal-sign-in.html"><i class="ti-layout-sidebar-left"></i>Logout</a>

                                        </li>

                                    </ul>

                                </div>

                            </div>

                            <div class="pcoded-search">

                                <span class="searchbar-toggle">  </span>

                                <div class="pcoded-search-box ">

                                    <input type="text" placeholder="Search">

                                    <span class="search-icon"><i class="ti-search" aria-hidden="true"></i></span>

                                </div>

                            </div>

                            

                            <div class="pcoded-navigatio-lavel" data-i18n="nav.category.navigation"></div>

                            <ul class="pcoded-item pcoded-left-item">

                                <li class="pcoded-hasmenu1">

                                    <a href="javascript:void(0)">

                                        <span class="pcoded-micon"><i class="ti-home"></i><b>D</b></span>

                                        <span class="pcoded-mtext">Dashboard</span>

                                    </a>

                                </li>

                            </ul>

                            

                             <ul class="pcoded-item pcoded-left-item performance_list">

                                <li class="pcoded-hasmenu2">

                                    <a href="javascript:void(0)">

                                        <span class="pcoded-micon"><i class="fa fa-bar-chart fa-6" aria-hidden="true"></i><b>P</b></span>

                                        <span class="pcoded-mtext">Analytics</span>

                                    </a>

                                </li>

                            </ul>

                            

                            <ul class="pcoded-item pcoded-left-item performance_list">

                                <li class="pcoded-hasmenu3">

                                    <a href="javascript:void(0)">

                                        <span class="pcoded-micon"><i class="fa fa-comment fa-6" aria-hidden="true"></i><b>c</b></span>

                                        <span class="pcoded-mtext">Comments</span>

                                    </a>

                                </li>

                            </ul>

                            

                            <ul class="pcoded-item pcoded-left-item">

                                <li class="pcoded-hasmenu pcoded-hasmenu4">

                                    <a href="javascript:void(0)">

                                        <span class="pcoded-micon"><i class="fa fa-plus fa-6" aria-hidden="true"></i><b>P</b></span>

                                        <span class="pcoded-mtext">Pages</span>

                                    </a>

                                    <ul class="pcoded-submenu">

                                        <li class="">

                                            <a href="javascript:void(0)">

                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>

                                                <span class="pcoded-mtext" data-i18n="nav.dash.default">Add page</span>

                                                <span class="pcoded-mcaret"></span>

                                            </a>

                                        </li>

                                        <li class="">

                                            <a href="javascript:void(0)">

                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>

                                                <span class="pcoded-mtext" data-i18n="nav.dash.ecommerce">Delete page</span>

                                                <span class="pcoded-mcaret"></span>

                                            </a>

                                        </li>

                                    </ul>

                                </li>

                            </ul>

                            

                            <ul class="pcoded-item pcoded-left-item performance_list">

                                <li class="pcoded-hasmenu5">

                                    <a href="javascript:void(0)">

                                        <span class="pcoded-micon"><i class="fa fa-user fa-6" aria-hidden="true"></i><b>P</b></span>

                                        <span class="pcoded-mtext">Users</span>

                                    </a>

                                </li>

                            </ul>

                            

                            

                            <ul class="pcoded-item pcoded-left-item">

                                <li class="pcoded-hasmenu pcoded-hasmenu6">

                                    <a href="javascript:void(0)">

                                        <span class="pcoded-micon"><i class="fa fa-question fa-6" aria-hidden="true"></i><b>H</b></span>

                                        <span class="pcoded-mtext">Help</span>

                                        <span class="pcoded-mcaret"></span>

                                    </a>

                                    <ul class="pcoded-submenu">

                                        <li class="">

                                            <a href="javascript:void(0)">

                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>

                                            <span class="pcoded-mtext" data-i18n="nav.dash.default">Contact Support</span>

                                                <span class="pcoded-mcaret"></span>

                                            </a>

                                        </li>

                                        <li class="">

                                            <a href="javascript:void(0)">

                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>

                                                <span class="pcoded-mtext" data-i18n="nav.dash.ecommerce">Manuals</span>

                                                <span class="pcoded-mcaret"></span>

                                            </a>

                                        </li>

                                    </ul>

                                </li>

                                </ul>

                            

                            

                            

                            

                          

                          

                        </div>

                    </nav>

                    

                    <div class="pcoded-content">

                        <div class="pcoded-inner-content">

                            <!-- Main-body start -->

                            <div class="main-body">

                                <div class="page-wrapper">

                                   



                                    <!-- Page body start -->

                                    <div class="page-body">

                                        <div class="row">
<!--start-->

    <div class="title_page01 floating">


            <div class="service_view01">
                <h2> Income Details </h2>
                    <form>

        <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-6 col-form-label">Accounts</label>
            <div class="col-sm-6">
                <input type="checkbox" class="js-small f-right">
             </div>
         </div>

         <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-6 col-form-label">Bookkepping</label>
            <div class="col-sm-6">
                <input type="checkbox" class="js-small f-right">
             </div>
         </div>

         <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-6 col-form-label">CT600 Return</label>
            <div class="col-sm-6">
                <input type="checkbox" class="js-small f-right">
             </div>
         </div>

         <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-6 col-form-label">Payroll</label>
            <div class="col-sm-6">
                <input type="checkbox" class="js-small f-right">
             </div>
         </div>

         <div class="form-group row help_icon radio_bts ">
            <label class="col-sm-6 col-form-label">Auto-Enrollement</label>
            <div class="col-sm-6">
                <input type="checkbox" class="js-small f-right">
             </div>
         </div>

         <div class="form-group row title_submitbt">

            <input type="submit" value="submit" name="submit">


        </div>
        

                    </form>
                </div>
            </div>        



	<!-- close -->


</div>

</div>

                                    <!-- Page body end -->

                                </div>

                            </div>

                            <!-- Main-body end -->



                            <div id="styleSelector">



                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>


<!-- ajax loader -->

<div class="LoadingImage" ></div>

<style>
.LoadingImage {
    display : none;
   position : fixed;
   z-index: 100;
   background-image : url('<?php echo site_url()?>assets/images/ajax-loader.gif');
   background-color:#666;
   opacity : 0.4;
   background-repeat : no-repeat;
   background-position : center;
   left : 0;
   bottom : 0;
   right : 0;
   top : 0;
 
}

</style>
<!-- ajax loader end-->



    <!-- Warning Section Starts -->

    <!-- Older IE warning message -->

    <!--[if lt IE 10]>

<div class="ie-warning">

    <h1>Warning!!</h1>

    <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>

    <div class="iew-container">

        <ul class="iew-download">

            <li>

                <a href="http://www.google.com/chrome/">

                    <img src="assets/images/browser/chrome.png" alt="Chrome">

                    <div>Chrome</div>

                </a>

            </li>

            <li>

                <a href="https://www.mozilla.org/en-US/firefox/new/">

                    <img src="assets/images/browser/firefox.png" alt="Firefox">

                    <div>Firefox</div>

                </a>

            </li>

            <li>

                <a href="http://www.opera.com">

                    <img src="assets/images/browser/opera.png" alt="Opera">

                    <div>Opera</div>

                </a>

            </li>

            <li>

                <a href="https://www.apple.com/safari/">

                    <img src="assets/images/browser/safari.png" alt="Safari">

                    <div>Safari</div>

                </a>

            </li>

            <li>

                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">

                    <img src="assets/images/browser/ie.png" alt="">

                    <div>IE (9 & above)</div>

                </a>

            </li>

        </ul>

    </div>

    <p>Sorry for the inconvenience!</p>

</div>

<![endif]-->

    <!-- Warning Section Ends -->

    <!-- Required Jquery -->

    <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-ui/js/jquery-ui.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>bower_components/popper.js/js/popper.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap/js/bootstrap.min.js"></script>

    <!-- j-pro js -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.ui.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.maskedinput.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.j-pro.js"></script>

    <!-- jquery slimscroll js -->

    <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>

    <!-- modernizr js -->

    <script type="text/javascript" src="<?php echo base_url();?>bower_components/switchery/js/switchery.min.js"></script>

    

    <script type="text/javascript" src="<?php echo base_url();?>bower_components/modernizr/js/css-scrollbars.js"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
   
    

    <script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/pages/advance-elements/custom-picker.js"></script>


    <script src="<?php echo base_url();?>assets/js/pcoded.min.js"></script>

    <script src="<?php echo base_url();?>assets/js/demo-12.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/common_script.js"></script>

    

  <script>

    $( document ).ready(function() {
    
        var date = $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();

        // Multiple swithces
        var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));

        elem.forEach(function(html) {
            var switchery = new Switchery(html, {
                color: '#1abc9c',
                jackColor: '#fff',
                size: 'small'
            });
        });

        $('#accordion_close').on('click', function(){
                $('#accordion').slideToggle(300);
                $(this).toggleClass('accordion_down');
        });
});
    </script>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
$("#new_user").validate({
  
       //ignore: false,

       /*onfocusout: function(element) {
        if ( !this.checkable(element)) {
            this.element(element);
        }
    },*/
   // errorClass: "error text-warning",
    //validClass: "success text-success",
    /*highlight: function (element, errorClass) {
        //alert('em');
       // $(element).fadeOut(function () {
           // $(element).fadeIn();
        //});
    },*/
                        rules: {
                        first_name: {required: true},
                        last_name: {required: true},
                        email_id:{required: true,email:true},
                        phone_number : {required:true,minlength:10,
  maxlength:10,
  number: true},
                        password:{required: true},
                        confirm_password : { equalTo : "#password"},
                         dob:{required: true,date: true},
						 gender:{required: true},
						 street1:{required: true},
						 country:{required: true},
						 state:{required: true},
						 city:{required: true},
						 packages:{required: true},
						 postal_code:{required: true},
						 firm_status:{required: true},
						 firm_name:{required: true},
                        },
                        errorElement: "span" , 
                        errorClass: "field-error",                             
                         messages: {
                          first_name: "Enter a first name",
                          last_name: "Enter a last name",
              
                          email_id: {
    email:"Please enter a valid email address", 
    required:"Please enter a email address",

},
                         phone_number:{  yourtel:"Enter a valid phone number.",
  required: "Enter a phone number."},
                          password: {required: "Please enter your password ",},
                          dob:{required: "Enter a destruction date",date: "Can contain digits only" },
                          gender:"Select gender",
                          street1:"Enter a  street1",
                          country:"Select a country",
                          state:"Select a state",
                          city:"Select a city",
                          packages:"Enter a package",
                          postal_code:"Enter a postal code",
                          firm_status:"Enter a firm status",
                          firm_name:"Enter a firm name",
                         },

                        
                        submitHandler: function(form) {
                            $(".LoadingImage").show();

                            $.ajax({
                                url: '<?php echo base_url();?>client/new_user/',
                                type: 'post',
                                data: $(form).serialize(),
                                timeout: 3000,
                                success: function(data) {
                                    if(data == 'success'){
                                       // $('#new_user')[0].reset();
                                        $('.alert-success').show();
                                        $('.alert-danger').hide();
                                    }
                                    else if(data == 'error'){
                                       // alert('failed');
                                        $('.alert-danger').show();
                                        $('.alert-success').hide();
                                    }
                                $(".LoadingImage").hide();
                                },
                                error: function() { $('.alert-danger').show();
                                        $('.alert-success').hide();}
                            });

                            return false;
                        } ,
                         
                    });

});
</script>
<script type="text/javascript">
 $(document).ready(function(){
   $("#country").change(function(){
   var country_id = $(this).val();
   //alert(country_id);

     $.ajax({

       url:"<?php echo base_url().'Client/state';?>",
       data:{"country_id":country_id},
       type:"POST",
       success:function(data){
         //alert('hi');
         $("#state").append(data);
         
       }

     });
   });
    $("#state").change(function(){
   var state_id = $(this).val();
   //alert(country_id);

     $.ajax({

       url:"<?php echo base_url().'Client/city';?>",
       data:{"state_id":state_id},
       type:"POST",
       success:function(data){
         //alert('hi');
         $("#city").append(data);
         
       }

     });
   });
 });
 </script>

</body>



</html>