<?php 

if($_SESSION['firm_id'] == 0)
{
   $this->load->view('super_admin/superAdmin_header');
}
else
{
   $this->load->view('includes/header');
}

?>

<div class="modal-alertsuccess alert succ popup_info_msg" style="display:none;">
         <div class="newupdate_alert"><a href="#" class="close" id="close_info_msg">×</a>
         <div class="pop-realted1">
         <div class="position-alert1">
               Please! Select Record...
         </div></div>
         </div>
   </div>

<style type="text/css">

   .dropdown-content 
   {
     display: none;
     position: absolute;
     background-color: #fff;
     min-width: 86px;
     overflow: auto;
     box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
     z-index: 1;
     left: -92px;
     width: 150px;
  }

  .modal-alertsuccess a.close 
  {  
    margin-top: 5px;
    margin-right: 15px;
  } 
  .pop-realted1 
  {
     padding: 10px;
     background-color: #fff;
  }
 

</style>

<div class="pcoded-content pcodedteam-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
             <?php echo $this->session->flashdata('alert-currency'); ?>    
                  <!--start-->
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card propose-template">
                        <!-- admin start-->                         
                           
                           
                           <div class="modal-alertsuccess alert alert-danger" <?php if(empty($_SESSION['fail_msg'])){ echo 'style="display:none;"'; } ?>>
                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <div class="pop-realted1">
                                 <div class="position-alert1">
                                    Process Failed.
                                 </div>
                              </div>
                           </div>


                            <div class="modal-alert_insert alert alert-sucess" style="display: none">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <div class="pop-realted2">
                                 <div class="position-alert1">
                                    Created Successfully.
                                 </div>
                              </div>
                           </div>

                           <!-- List -->

                      <div class="deadline-crm1 floating_set col-md-2 pull-right">
                      <button type="button" data-toggle="modal" data-target="#add_status"  class="btn btn-success add_button" ><i class="fa fa-plus fa-6" aria-hidden="true"></i> Add Progress </button>
                      </div>     
                    </div>

                    <div class="f-right">
                       <button type="button" data-toggle="modal" data-target="#deleteconfirmation" id="delete_task" class="btn btn-danger" style="display: none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i> Delete </button>
                     </div>

                    
                    <div class="all_user-section floating_set pros-editemp">                               
                     <div class="all_user-section6 floating_set">
            <div class="tab-content">
            <div id="allusers" class="tab-pane fade in active">
            <div id="task"></div>
            <div class="client_section3 table-responsive">
            <div class="status_succ"></div>
            <div class="all-usera1 data-padding1 ">                                 
                        <div class="">
            <table class="table client_table1 text-center display nowrap printableArea" id="display_service1" cellspacing="0" width="100%">
              <thead>
                <tr class="text-uppercase">
                
                <th class="">                                  
                    <label class="custom_checkbox1" style="margin-left: 5px;">
                    <input type="checkbox" id="select_all_templates">
                    <i></i>
                    </label>                                        
              </th>
             
                <th>S.no
                <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                </th>
             
                <th>Status
                <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                </th>
             
                
                <th>Action</th>
               
                </tr>
              </thead>
               
              <tbody>
                <?php
                $i=1;
                 foreach ($task_status as $key => $task_value){ ?>
                <tr>
               
                <td>
                
                <label class="custom_checkbox1">
                <input type="checkbox" class="template_checkbox" data-id="<?php echo $task_value['id'];?>"><i></i>   
                </label>
                
                </td>
             
                <td><?php echo $i; ?></td>
            
                <td><?php echo $task_value['status_name'];?></td>
                
              <td>
                 <a href="javascript:;"  data-toggle="modal" data-target="#add_status"   data-id='<?php echo $task_value['id'];?>' data-status_name="<?php echo $task_value['status_name'];?>" class="edit_btn"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>

            
                 <a href="javascript:;"  data-toggle="modal" data-target="#add_status"  data-status_name="<?php echo $task_value['status_name'];?>"  data-id='<?php echo $task_value['id'];?>'  class="del_btn"><i class="fa fa-trash fa-6" aria-hidden="true"></i></a>

              </td> 

                </tr>                
                <?php $i++; }  ?>   
              </tbody>
                           </table>
                        <input type="hidden" class="rows_selected" id="select_template_count" >     
                        </div>
                     </div>
                     <!-- List -->           
                  </div>
               </div>
            </div>
         </div>
      </div>      

       <div class="modal fade" id="add_status" role="dialog">
         <div class="modal-dialog">    
           <!-- Modal content-->
           <div class="modal-content">
             <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
                 <h4><div class="cont"></div></h4>     
             </div>
             <div class="modal-body">
             
             <input  type="text" name="status_name" id="status_name" value="">
             <input  type="hidden" name="status_id" id="status_id" value="">
              <input  type="hidden" name="del_status_id" id="del_status_id" value="">
              <div id="error_html" style="color:red"></div>
             </div>
             <div class="modal-footer">
              <button type="button" class="btn btn-default" id="btn_submit">Ok</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
             </div>
           </div>
         </div>
      </div>      

                  
                        <!-- Page body end -->
                     </div>
                  </div>
                  <!-- Main-body end -->
                  <div id="styleSelector">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


<div class="modal fade" id="deleteconfirmation" role="dialog">
    <div class="modal-dialog">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
        <input  type="hidden" name="delete_process_id" id="delete_process_id" value="">
          <p> Are you sure want to delete ?</p>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default" id="confirm_delete" data-dismiss="modal" onclick="delete_action()">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
</div>



<?php 

$this->load->view('includes/session_timeout');

if($_SESSION['firm_id'] == 0)
{
   $this->load->view('super_admin/superAdmin_footer');
}
else
{
   $this->load->view('includes/footer');
}

?>
 
<script type="text/javascript"  src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

<script type="text/javascript">

    var table10;

    $(document).ready(function()
    {           
      var check=0;
      var check1=0;
      var numCols = $('#display_service1 thead th').length;
      table10 = $('#display_service1').DataTable(
      {
          "dom": '<"top"fl>rt<"bottom"ip><"clear">',
          initComplete: function () { 
                 var q=1;
                    $('#display_service1 thead th').find('.filter_check').each( function(){
                    // alert('ok');
                      $(this).attr('id',"all_"+q);
                      q++;
                    });
                 for(i=1;i<numCols;i++){                 
                       var select = $("#all_"+i); 
                     
                     this.api().columns([i]).every( function () {
                       var column = this;
                       column.data().unique().sort().each( function ( d, j ) {    
                     //    console.log(d);         
                         select.append( '<option value="'+d+'">'+d+'</option>' )
                       });
                     }); 
                  
          
                    $("#all_"+i).formSelect();  
                 }
              }
       });

        for(j=1;j<numCols;j++)
        {  
           $('#all_'+j).on('change', function(){ 
             var result=$(this).attr('id').split('_');      
              var c=result[1];
               var search = [];              
               $.each($('#all_'+c+ ' option:selected'), function(){                
               search.push($(this).val());
               });      
               search = search.join('|');    
              

               table10.column(c).search(search, true, false).draw();  
           });
        }     
    });
 

  $(document).on('click','.DT', function (e) 
  {
      if(!$(e.target).hasClass('sortMask')) 
      {      
          e.stopImmediatePropagation();
      }
  });

   $(document).on('click', 'th .themicond', function()
   { 
        if ($(this).parent().find(".dropdown-content").hasClass("Show_content")) {
           $(this).parent().find('.dropdown-content').removeClass('Show_content');
        }else{
           $('.dropdown-content').removeClass('Show_content');
           $(this).parent().find('.dropdown-content').addClass('Show_content');
        }
            $(this).parent().find('.select-wrapper').toggleClass('special');
        if( $(this).parent().find('.select-dropdown span input[type="checkbox"]').parent().hasClass('custom_checkbox1')){
        }else{
            $(this).parent().find('.select-dropdown span input[type="checkbox"]').wrap( '<label class="custom_checkbox1"></label>');
            $(this).parent().find('.custom_checkbox1 input').after( "<i></i>" );
        }       
   });   
   
   $(document).ready(function()
   {
    // window.alltemplate = [];
   });

   function getSelectedRow(id)
   {       
      var  alltemplate = [];       
      alltemplate.push(id);   
      return $.unique(alltemplate);
   }

   // function return_indice(id)
   // { 
   //    for(var i=0;i<alltemplate.length;i++)
   //    {
   //       if(id == alltemplate[i])
   //       {
   //          return i;
   //       }          
   //    }
   // }

   // function dounset(id)
   // {
   //    alltemplate = $.unique(alltemplate);
   //    var index = return_indice(id);
       
   //    if(index > -1) 
   //    {
   //       alltemplate.splice(index, 1);
   //    }

   //    return alltemplate;
   // }

  $('.template_checkbox').click(function(e)
  {  
         $('#delete_process_id').val('');
       var id = $(this).data('id');
       var all_temp=[];       

       if($(this).is(":checked"))
       {         
          all_temp = getSelectedRow(id);
             $('#delete_process_id').val(all_temp);
              $("#delete_task").show();  
       }
       else
       {
          $('#select_all_templates').prop('checked',false);
          //all_temp = dounset(id);  
           $("#delete_task").hide();  
       }

      //showdelete(all_temp); 
  }); 

  $('#btn_submit').click(function() 
  { 
    $('#error_html').html('');

       
       var status_name = $("#status_name").val();
       var status_id = $("#status_id").val();
       var del_status_id = $("#del_status_id").val();
       var status_url='<?php echo base_url(); ?>super_admin/insert_task_status';
       if(status_id!='')
       {
        status_url='<?php echo base_url(); ?>super_admin/updated_task_status';
       }
       if(status_name!=''){
        $('#add_status').hide();
       $.ajax(
       {
          url: status_url,
          type: 'POST',
          data: {'status_name':status_name,'status_id':status_id,'del_status_id':del_status_id},

          beforeSend: function() 
          {          
             $(".LoadingImage").show();
          },
          success:function(status)
          { 

             $(".LoadingImage").hide();

             $('.popup_info_msg').show();

             if(del_status_id==1)
            {
               $(".popup_info_msg .position-alert1").html('').html("Delete the Progress Successfully");
            }
             else if(status_id!='')
             {
               $(".popup_info_msg .position-alert1").html('').html("Update the Progress Successfully");
             }
             else
             {
              $(".popup_info_msg .position-alert1").html('').html("Insert the Progress Successfully");
             
             }

               if(status == 1)
             {           
                location.reload();
             } 
                 $('#modal-alert_insert').show();
          }

       });
     }
     else
     {
      $('#error_html').html('Enter Progress');
     }
  });

  $('.add_button').click(function(e) 
  { $('#error_html').html('');
      $('#status_name').val('');
      $('#status_id').val('');
      $('#del_status_id').val('');
      $('#add_status').find('.cont').html('').append('Enter the Progress'); 
      $('#add_status').find('#status_name').attr('type','text'); 
  });


$('.edit_btn').click(function(e) 
  { 
      $('#error_html').html('');
     $('#status_name').val('');
     $('#status_id').val('');
     $('#del_status_id').val('');

       $('#status_name').val($(this).data('status_name'));
       $('#status_id').val($(this).data('id'));
       $('#add_status').find('.cont').html('').append('Enter the Progress'); 
       $('#add_status').find('#status_name').attr('type','text'); 
  });


  $('.del_btn').click(function(e) 
  { 
    $('#error_html').html('');
     $('#status_name').val('');
     $('#status_id').val('');
     $('#del_status_id').val('');
      $('#status_name').val($(this).data('status_name'));
       $('#status_id').val($(this).data('id'));
       $('#del_status_id').val(1);
       $('#add_status').find('#status_name').attr('type','hidden'); 

       $('#add_status').find('.cont').html('').append(' Are you sure want to delete?'); 
  });

  $(document).on('change','#select_all_templates', function() 
  { 
      var all_temp=[];
         $('#delete_process_id').val('');

      if($(this).is(':checked', true)) 
      {  

          table10.column(0).nodes().to$().each(function(index) 
          {                      
              $(this).find(".template_checkbox").prop('checked',true);
              var id = $(this).find(".template_checkbox").data('id');  
              all_temp = getSelectedRow(id);                            
          }); 

          $("#delete_task").show();  

             $('#delete_process_id').val(all_temp);
      }
      else
      {
          table10.column(0).nodes().to$().each(function(index) 
          {                      
              $(this).find(".template_checkbox").prop('checked',false);
          });
       //   all_temp = [];
          //alltemplate = [];
           $("#delete_task").hide();  

      } 
    //  console.log(all_temp);
   
      //showdelete(all_temp);        
  });   



  /*function getframe(heading,$url)
  {
     $('#template_iframe').find('.modal-title').html('');
     $('#template_iframe').find('.modal-title').html(heading);

     $.ajax(
     {
        url:$url,
        async:'true',
        type:'POST',     

        success:function(response)
        {
           $('#template_iframe').find('.modal-body').html('');
           $('#template_iframe').find('.modal-body').html(response);
           $('#temp_butt').trigger('click');
        }

     });
  }   */


  function delete_action()
{
 
  $.ajax({
        type: "POST",
        url: "<?php echo base_url().'super_admin/alltasks_delete';?>",        
        data: {'id':$("#delete_process_id").val()},
        beforeSend: function() {

          $(".LoadingImage").show();
        },
        success: function(data) {

          $(".LoadingImage").hide();

          $(".popup_info_msg").show();
          $('.popup_info_msg .position-alert1').html('Success !!! Status Successfully Deleted..');
         setTimeout(function() {$(".popup_info_msg").hide();location.reload(); },1500);
           
          /*var task_ids = data.split(",");
          for (var i=0; i < task_ids.length; i++ ) { 
          $("#"+task_ids[i]).remove(); } 
          $(".alert-success-delete").show();
          setTimeout(function() { 
          }, 500);  */   
        }
      });
}

</script>