<!DOCTYPE html>
<?php $uri = $this->uri->segment('2');
$isBasicTheme = ( isset( $theme ) && $theme == 'basic' ) ? true : false;
$Company_types = array();
?>
<html lang="en">
   <head>
      <title>CRM </title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="description" content="#">
      <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
      <meta name="author" content="#">
      <link rel="icon" href="<?php echo base_url();?>assets/images/favicon.ico" type="image/x-icon"> 
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>bower_components/bootstrap/css/bootstrap.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icon/themify-icons/themify-icons.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/common_style.css?ver=4">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/common_responsive.css?ver=2">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/ui_style.css?ver=3">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/pages/menu-search/css/component.css">      
      <?php if( !$isBasicTheme ) { ?>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/pages/j-pro/css/demo.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/pages/j-pro/css/font-awesome.min.css">          
        <link type="text/css" href="<?php echo base_url();?>bower_components/bootstrap-timepicker/css/bootstrap-timepicker.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap-datetimepicker.css">         
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">    
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery.mCustomScrollbar.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>bower_components/owl.carousel/css/owl.carousel.css">    
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>/assets/css/dataTables.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/jquery.dropdown.css">
        <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">    
        <link href="<?php echo base_url(); ?>css/jquery.signaturepad.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>bower_components/bootstrap-daterangepicker/css/daterangepicker.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>bower_components/datedropper/css/datedropper.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>bower_components/switchery/css/switchery.min.css">

        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-colorpicker.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-slider.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/medium-editor/medium-editor.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/medium-editor/template.min.css">   
      <?php } ?>
        <link rel="stylesheet" type="text/css" href="<?php echo  base_url()?>/assets/icon/simple-line-icons/css/simple-line-icons.css">
        <link rel="stylesheet" type="text/css" href="<?php echo  base_url()?>/assets/icon/icofont/css/icofont.css">   
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/mobile_responsive.css?ver=2"> 
<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css">   -->
 <!-- tinymce editor -->
        <link rel="stylesheet" type="text/css" href="https://www.tiny.cloud/css/codepen.min.css">
        <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/custom/opt-profile.css?<?php echo time(); ?>">
        <!-- tinymce editor -->  
      <style>
         .common_form_section1  .col-xs-12.col-sm-6.col-md-6 {
         float: left !important;
         }
         .common_form_section1 {
         background: #fff;
         box-shadow: 1px 2px 5px #dbdbdb;
         border-radius: 5px;
         padding: 30px;
         }
         .common_form_section1 label{
         display: block;
         margin-bottom: 7px;
         font-size: 15px;
         }
         .common_form_section1 input{
         width: 100%;
         }
         .col-sm-12{
         float: left;
         }
         .col-sm-8 {
         float: left;
         }
      .LoadingImage 
      {
        display : none;
        position : fixed;
        z-index: 100;
        /*background-image : url('<?php echo site_url()?>assets/images/ajax-loader.gif');*/
        background-color:rgba(65, 56, 57, 0.4);
        opacity : 1;
        background-repeat : no-repeat;
        background-position : center;
        left : 0;
        bottom : 0;
        right : 0;
        top : 0;
      }
      </style>
      <script type="text/javascript"> 
      var base_url = "<?php echo base_url()?>";
      </script>
   </head>
    <body>
   <div class="LoadingImage">
   <div class="preloader3 loader-block">
      <div class="circ1 loader-primary loader-md"></div>
      <div class="circ2 loader-primary loader-md"></div>
      <div class="circ3 loader-primary loader-md"></div>
      <div class="circ4 loader-primary loader-md"></div>
   </div>
   </div>
   <div id="action_result" class="modal-alertsuccess alert succ dashboard_success_message" style="display:none;">
      <div class="newupdate_alert">
          <a href="#" class="close" id="close_action_result">×</a>
            <div class="pop-realted1">
                <div class="position-alert1 action_result_message_content">
                    Success !!! Staff Assign have been changed successfully...
                </div>
            </div>
      </div>
</div>


  
     
      <!-- Pre-loader start -->
      <div class="theme-loader">
         <div class="ball-scale">
            <div class='contain'>
               <div class="ring">
                  <div class="frame"></div>
               </div>
               <div class="ring">
                  <div class="frame"></div>
               </div>
               <div class="ring">
                  <div class="frame"></div>
               </div>
               <div class="ring">
                  <div class="frame"></div>
               </div>
               <div class="ring">
                  <div class="frame"></div>
               </div>
               <div class="ring">
                  <div class="frame"></div>
               </div>
               <div class="ring">
                  <div class="frame"></div>
               </div>
               <div class="ring">
                  <div class="frame"></div>
               </div>
               <div class="ring">
                  <div class="frame"></div>
               </div>
               <div class="ring">
                  <div class="frame"></div>
               </div>
            </div>
         </div>
      </div>

      <?php //} ?>
      <!-- Pre-loader end -->
      <div id="pcoded" class="pcoded" >
      <div class="pcoded-overlay-box"></div>
      <div class="pcoded-container navbar-wrapper">
      <nav class="navbar header-navbar pcoded-header">
         <div class="navbar-wrapper">
            <div class="navbar-logo">
				<a class="mobile-menu" id="mobile-collapse" href="javascript:void(0);">
					<i class="ti-menu"></i>
				</a>
               <a href="<?php echo base_url();?>">
               <img class="img-fluid" src="<?php echo base_url();?>assets/images/color_logo.png" alt="Theme-Logo" />
               </a>
               <a class="mobile-options">
               <i class="ti-more"></i>
               </a>
            </div>
            <div class="navbar-container container-fluid">
              <div class="nav-left">
              <ul  class="data1" id="owl-demo1">

                                   
                 <li class="item atleta comclass" ><a href="<?php echo base_url()?>Admin_dashboard" class=" mydisk-img1">
                   <img class="menicon" src="<?php echo base_url();?>assets/images/m1.jpg" alt="menuclsicon" />My Desk</a>
                </li>                   
       
                <li class="dropdown comclass">
                    <a href="#" class="com12 mydisk-img5 ">  
                    <img class="menicon" src="<?php echo base_url();?>assets/images/m5.jpg" alt="menuclsicon">Firm
                    <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu1 ">
                    <li class="atleta">
                    <a href="<?php echo base_url();?>firm/add_firm" class="">Add Firm</a>
                    </li>
                    <li class="atleta">
                    <a href="<?php echo base_url();?>firm" class="">Firm List</a>
                    </li> 
                    <li class="atleta"><a href="#" class="waves-effect" data-toggle="modal"
                    data-target="#default-Modal"  data-backdrop="static" data-keyboard="false">Add from Companies House</a></li>
                    
                    </ul>
                </li>
                <li class="dropdown comclass">
                    <a href="#" class="com12 mydisk-img5 ">  
                    <img class="menicon" src="<?php echo base_url();?>assets/images/m8.jpg" alt="menuclsicon">Settings
                    <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu1 ">
                    <li class="atleta">
                    <a href="<?php echo base_url(); ?>user/Service_reminder_settings" class="">Reminder Settings</a>
                    </li>   
                    <li class="atleta">
                    <a href="<?php echo base_url(); ?>Email_template" class="">Email Template</a>
                    </li> 
                    <li class="atleta">
                    <a href="<?php echo base_url(); ?>proposal_page/templates" class="">Proposal Template</a>
                    </li> 
                    <li class="atleta">
                    <a href="<?php echo base_url(); ?>Firm/stripe" class="">Stripe Settings</a>
                    </li>
                     <li class="atleta">
                    <a href="<?php echo base_url(); ?>Tickets/index" class="">Tickets</a>
                    </li>
                    <li class="atleta">
                    <a href="<?php echo base_url(); ?>user/firm_field_check" class="">Firm Fields Settings</a>
                    </li>
                    <li class="atleta">
                    <a href="<?php echo base_url(); ?>service" class="">Service Settings</a>
                    </li>
                    <li class="atleta">
                    <a href="<?php echo base_url(); ?>menu_management" class="">Default Menu Management Settings</a>
                    </li> 

                    <li class="atleta">
                    <a href="<?php echo base_url(); ?>super_admin/task_progress_view" class="">Task Status Management</a>
                    </li>  

                    <li class="atleta">
                    <a href="<?php echo base_url(); ?>super_admin/home_content" class="">Page Content Setting</a>
                    </li>  

                     <li class="atleta">
                    <a href="<?php echo base_url(); ?>super_admin/mail_setting" class="">Mail Setting</a>
                    </li> 

                    </ul>
                </li>

                 <li class="item atleta comclass" ><a href="<?php echo base_url()?>firm/plans" class=" mydisk-img1">
                   <img class="menicon" src="<?php echo base_url();?>uploads/menu_icons/1551713659.jpg" alt="menuclsicon" />Plan</a>
                </li> 

              </ul>
              </div>
              <div class="nav-right">
                <ul>
                  <li class="user-profile header-notification">
                      <a href="#!">
                      <img src="<?php echo base_url().$_SESSION['profile_picture_path']; ?>" alt="img">
                      <span><?php echo $_SESSION['display_name']?> </span>
                      <i class="ti-angle-down"></i>
                      </a>
                     <ul class="tgl-opt">
                        <li class="atleta">
                           <a class="show_profile_content" href="javascript:void(0);">                      
                           <i class="ti-layout-sidebar-left"></i>My Profile
                           </a> 
                        </li>
                        <li class="atleta">
                           <a href="<?php echo base_url()?>super_admin/logout">
                           <i class="ti-layout-sidebar-left"></i> Logout
                           </a> 
                        </li>
                     </ul>
                  </li>
                 </ul> 
              </div>

            </div>
         </div>
      </nav>

      <div class="pcoded-main-container">
      <div class="pcoded-wrapper">      

