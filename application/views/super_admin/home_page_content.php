<?php

if ($_SESSION['firm_id'] == '0') {
    $this->load->view('super_admin/superAdmin_header');
} else {
    $this->load->view('includes/header');
}

?>

<style type="text/css">

/* .Change_Page_Class .error 
{
    text-transform:capitalize;
}*/

.Change_Page_Class .error  {
    display: block;
}

.Change_Page_Class .error:first-letter {
    text-transform: uppercase;
}


   .firm_contact_country_code_select_div
   {
      width: 114px;
      display: inline-block;
   }
  input#company_contact_no
   {
      width: calc(100% - 123px);
      margin-left: 5px;
   }
   .firm_contact_group span.field-error
   {
      display: inline-block !important;
   }
   @media(min-width:1600px){
    .service_view01 .row.form-group.new-append1{
      padding:0;
      margin-left:0;
      margin-right:0;
      width:100%;
    }
    #admin_setting .form-group.row.name_fields{
      width: 23%;
    }
  }

  /*for color picker*/
  .sp-dd
  {
    background: url('<?php
echo base_url();
?>assets/images/new_update1.png');
    background-repeat: no-repeat;
    width: 10px;
    margin: 10px 0px 0px 5px;
  } 
  /*for color picker*/
    
    /*for temporary*/
    /*.cke_reset_all,.cke_reset_all *, .cke_reset_all a,.cke_reset_all textarea {
    display: inherit;
    vertical-align: top;
    }*/
/*    .show_hide_password
    {
          position: absolute;
          padding: 8px;
          min-width: 40px;
          right: 172px;
    }*/
</style>


<?php

$type = $this->uri->segment(2);

?>


  <div class="modal-alertsuccess alert succ popup_info_msg" style="display:none;">
                             <div class="newupdate_alert">
                               <a href="#" class="close" data-dismiss="modal" aria-label="close">&times;</a>
                                <div class="pop-realted1">
                                <div class="position-alert1">
                                     Update Data Sucessfully
                                </div>
                                </div>
                             </div>
                           </div>


<div class="pcoded-content adminsettingcls">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <!--start-->
               <div class="title_page01 floating">
                  <div class="modal-alertsuccess alert alert-success info_box" style="display:none;">
                     <a href="#" class="close" aria-label="close">&times;</a>
                     <div class="pop-realted1">
                        <div class="position-alert1 info_text">
                        </div>
                     </div>
                  </div>
                    


                  <ul class="nav nav-tabs md-tabs pageconcls" id="myTabMD" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link active" id="home-tab-md" data-toggle="tab" href="#home-md" role="tab" aria-controls="home-md"
                        aria-selected="true">Home Page</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="feature-tab-md" data-toggle="tab" href="#feature-md" role="tab" aria-controls="feature-md"
                        aria-selected="false">Features Page</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="price-tab-md" data-toggle="tab" href="#price-md" role="tab" aria-controls="price-md"
                        aria-selected="false">Pricing Page</a>
                    </li>
                  </ul>

                  <div class="service_view01 upload-file05 pageconclsinner">
                      <div class="tab-content">
                     <div id="adminprofile" class="tab-pane active">
                     <div class="admin-boxshadow floating adminsettingsection">
                        <h2 class="page_heading"> Home Page Content </h2>
                        <form id="page_setting" class="validation dragfilesclstop" method="post" action="">

                          <div class="row">

                           <div class="Change_Page_Class" >


                                <div class="tab-content card pt-5" id="myTabContentMD">
                                  <div class="tab-pane fade show active" id="home-md" role="tabpanel" aria-labelledby="home-tab-md">
                                   <div class="col-sm-12">
                              
                              <div class="newemailset">
                                  <div class="">          
                                  <label>Home page header</label>
                                  </div>
                                  <input type="text" name="home_page_title" class="form-control home_page_title required" data-validation="required,number" value="<?= page_content('home_page_title') ?>">
                                  <span style="color: red;" id="home_page_title_er" class="error"></span>
                           

                                  <div class="">          
                                  <label>Home Page header description</label>
                                  </div>
                                  <input id="home_page_title_description" name="home_page_title_description" class="txtDropTarget  home_page_title_description home_content form-control" value="<?= htmlentities(page_content('home_page_title_description')) ?>">
                                  <span style="color: red;" id="home_page_title_description_er" class="error"></span>
                                </div>

                            </div>


            

                            



                            <div class="col-sm-12">
                      
                              <div class="newemailset">
                                  <div class="">          
                                  <label>Task title</label>
                                  </div>
                                  <input type="text" name="task_title" class="form-control required" data-validation="required,number" value="<?= page_content('task_title') ?>">
                                  <span style="color: red;" id="task_title_er" class="error" ></span>
                               
                                  <div class="">          
                                  <label>Task description</label>
                                  </div>
                                  <input id="task_description" name="task_description" class="txtDropTarget  task_description home_content form-control" value="<?= html_entity_decode(page_content('task_description')); ?>">
                                  <span style="color: red;" id="task_description_er" class="error"></span>
                                </div>

                            </div>

                            <div class="col-sm-12 Videotitlecls">
                      
                              <div class="newemailset">
                                  <div class="">          
                                  <label>Section title</label>
                                  </div>
                                  <input type="text" name="section_title" class="form-control required" data-validation="required,number" value="<?= page_content('section_title') ?>">
                                  <span style="color: red;" id="section_title_er" class="error" ></span>
                                </div>
     
                            </div>


                                <div class="col-sm-12">
                      
                              <div class="newemailset">
                                  <div class="">          
                                  <label>Deadline title</label>
                                  </div>
                                  <input type="text" name="deadline_title" class="form-control required" data-validation="required,number" value="<?= page_content('deadline_title') ?>">
                                  <span style="color: red;" id="deadline_title_er" class="error"></span>
                                
                                  <div class="">          
                                  <label>Deadline description</label>
                                  </div>
                                  <input id="deadline_description" name="deadline_description" class="txtDropTarget  deadline_description home_content form-control" value="<?= html_entity_decode(page_content('deadline_description')); ?>">
                                  <span style="color: red;" id="deadline_description_er" class="error"></span>
                                </div>

                            </div>



                      <div class="col-sm-12">
                      
                              <div class="newemailset">
                                  <div class="">          
                                  <label>Timesheet title</label>
                                  </div>
                                  <input type="text" name="timesheet_title" class="form-control required" data-validation="required,number" value="<?= page_content('timesheet_title') ?>">
                                  <span style="color: red;" id="timesheet_title_er" class="error"></span>
                               
                                  <div class="">          
                                  <label>Timesheet description</label>
                                  </div>
                                  <input id="timesheet_description" name="timesheet_description" class="txtDropTarget  timesheet_description home_content form-control" value="<?= html_entity_decode(page_content('timesheet_description')); ?>">
                                  <span style="color: red;" id="timesheet_description_er" class="error"></span>
                                </div>

                            </div>





                              <div class="col-sm-12">
                      
                              <div class="newemailset">
                                  <div class="">          
                                  <label>Proposal title</label>
                                  </div>
                                  <input type="text" name="proposal_title" class="form-control required" data-validation="required,number" value="<?= page_content('proposal_title') ?>">
                                  <span style="color: red;" id="proposal_title_er" class="error"></span>
                               
                                  <div class="">          
                                  <label>Proposal description</label>
                                  </div>
                                  <input id="proposal_description" name="proposal_description" class="txtDropTarget  proposal_description home_content form-control" value="<?= html_entity_decode(page_content('proposal_description')); ?>">
                                  <span style="color: red;" id="proposal_description_er" class="error"></span>
                                </div>

                            </div>




                              <div class="col-sm-12">
                      
                              <div class="newemailset">
                                  <div class="">          
                                  <label>Client title</label>
                                  </div>
                                  <input type="text" name="client_title" class="form-control required" data-validation="required,number" value="<?= page_content('client_title') ?>">
                                  <span style="color: red;" id="client_title_er" class="error"></span>
                                
                                  <div class="">          
                                  <label>Client description</label>
                                  </div>
                                  <input id="client_description" name="client_description" class="txtDropTarget  client_description home_content form-control" value="<?= html_entity_decode(page_content('client_description')); ?>">
                                  <span style="color: red;" id="client_description_er" class="error"></span>
                                </div>

                            </div>






                              <div class="col-sm-12 Videotitlecls">
                      
                              <div class="newemailset">
                                  <div class="">          
                                  <label>Video title</label>
                                  </div>
                                  <input type="text" name="video_title" class="form-control required" data-validation="required,number" value="<?= page_content('video_title') ?>">
                                  <span style="color: red;" id="video_title_er" class="error"></span>
                                </div>
     
                            </div>

      





                    <div class="col-sm-12">
                      
                              <div class="newemailset">
                                  <div class="">          
                                  <label>Create title</label>
                                  </div>
                                  <input type="text" name="create_title" class="form-control required" data-validation="required,number" value="<?= page_content('create_title') ?>">
                                  <span style="color: red;" id="create_title_er" class="error"></span>
                               
                                  <div class="">          
                                  <label>Create description</label>
                                  </div>
                                  <input id="create_description" name="create_description" class="txtDropTarget  create_description home_content form-control" value="<?= html_entity_decode(page_content('create_description')); ?>">
                                  <span style="color: red;" id="create_description_er" class="error"></span>
                                </div>

                            </div>





                <div class="col-sm-12">
                      
                              <div class="newemailset">
                                  <div class="">          
                                  <label>Run title</label>
                                  </div>
                                  <input type="text" name="run_title" class="form-control required" data-validation="required,number" value="<?= page_content('run_title') ?>">
                                  <span style="color: red;" id="run_title_er" class="error"></span>
                               
                                  <div class="">          
                                  <label>Run description</label>
                                  </div>
                                  <input id="run_description" name="run_description" class="txtDropTarget  run_description home_content form-control" value="<?= html_entity_decode(page_content('run_description')); ?>">
                                  <span style="color: red;" id="run_description_er" class="error"></span>
                                </div>

                            </div>




                              <div class="col-sm-12">
                      
                              <div class="newemailset">
                                  <div class="">          
                                  <label>Integrate title</label>
                                  </div>
                                  <input type="text" name="integrate_title" class="form-control required" data-validation="required,number" value="<?= page_content('integrate_title') ?>">
                                  <span style="color: red;" id="integrate_title_er" class="error"></span>
                               
                                  <div class="">          
                                  <label>Integrate description</label>
                                  </div>
                                  <input id="integrate_description" name="integrate_description" class="txtDropTarget  integrate_description home_content form-control" value="<?= html_entity_decode(page_content('integrate_description')); ?>">
                                  <span style="color: red;" id="integrate_description_er" class="error"></span>
                                </div>

                            </div>


                        <div class="col-sm-12">
                      
                              <div class="newemailset">
                                  <div class="">          
                                  <label>Communicate title</label>
                                  </div>
                                  <input type="text" name="communicate_title" class="form-control required" data-validation="required,number" value="<?= page_content('communicate_title') ?>">
                                  <span style="color: red;" id="communicate_title_er" class="error"></span>
                                
                                  <div class="">          
                                  <label>Communicate description</label>
                                  </div>
                                  <input id="communicate_description" name="communicate_description" class="txtDropTarget  communicate_description home_content form-control" value="<?= html_entity_decode(page_content('communicate_description')); ?>">
                                  <span style="color: red;" id="communicate_description_er" class="error"></span>
                                </div>

                            </div>

                            

                            <div class="col-sm-12 Videotitlecls">
                      
                              <div class="newemailset">
                                  <div class="">          
                                  <label>Subscription Title</label>
                                  </div>
                                  <input type="text" name="subscription_title" class="form-control required" data-validation="required,number" value="<?= page_content('subscription_title') ?>">
                                  <span style="color: red;" id="subscription_title_er" class="error"></span>
                                </div>
     
                            </div>
                               
                                  </div>
                                  <div class="tab-pane fade" id="feature-md" role="tabpanel" aria-labelledby="feature-tab-md">
                                  <div class="col-sm-12">
                              
                              <div class="newemailset">
                                  <div class="">          
                                  <label>Feature page header</label>
                                  </div>
                                  <input type="text" name="feature_page_title" class="form-control home_page_title required" data-validation="required,number" value="<?= page_content('feature_page_title') ?>">
                                  <span style="color: red;" id="feature_page_title_er" class="error"></span>
                           

                                  <div class="">          
                                  <label>No Setup description</label>
                                  </div>
                                  <input id="no_setup_description" name="no_setup_description" class="txtDropTarget  no_setup_description  form-control" value="<?= htmlentities(page_content('no_setup_description')) ?>">
                                  <span style="color: red;" id="no_setup_description_er" class="error"></span>
                                </div>

                            </div>



                            <div class="col-sm-12 minauto">
                      
                              <div class="newemailset">
                        
                                  <div class="">          
                                  <label>Safety description</label>
                                  </div>
                                  <input id="safety_description" name="safety_description" class="txtDropTarget  safety_description  form-control" value="<?= html_entity_decode(page_content('safety_description')); ?>">
                                  <span style="color: red;" id="safety_description_er" class="error"></span>
                                </div>

                            </div>



                                <div class="col-sm-12">
                      
                              <div class="newemailset">
                                                                  
                                  <div class="">          
                                  <label>Optimized Data  description</label>
                                  </div>
                                  <input id="optimized_data_description" name="optimized_data_description" class=" txtDropTarget   optimized_data_description form-control" value="<?= html_entity_decode(page_content('optimized_data_description')); ?>">
                                  <span style="color: red;" id="optimized_data_description_er" class="error"></span>
                                </div>

                            </div>



                      <div class="col-sm-12">
                      
                              <div class="newemailset">
                             
                               
                                  <div class="">          
                                  <label>Quick Access description</label>
                                  </div>
                                  <input id="quick_access_description" name="quick_access_description" class="txtDropTarget  quick_access_description  form-control" value="<?= html_entity_decode(page_content('quick_access_description')); ?>">
                                  <span style="color: red;" id="quick_access_description_er" class="error"></span>
                                </div>

                            </div>





                              <div class="col-sm-12">
                      
                              <div class="newemailset">
                               
                               
                                  <div class="">          
                                  <label>24h Support</label>
                                  </div>
                                  <input id="support_description" name="support_description" class="txtDropTarget  support_description  form-control" value="<?= html_entity_decode(page_content('support_description')); ?>">
                                  <span style="color: red;" id="support_description_er" class="error"></span>
                                </div>

                            </div>




                              <div class="col-sm-12">
                      
                              <div class="newemailset">
                                  <div class="">          
                                  <label>Dashboard Image Above title</label>
                                  </div>
                                  <input type="text" name="dashboard_image_title" class="form-control required" data-validation="required,number" value="<?= page_content('dashboard_image_title') ?>">
                                  <span style="color: red;" id="dashboard_image_title_er" class="error"></span>
                                
                                  
                                </div>
                               </div>

                                <div class="col-sm-12">
                                 <div class="newemailset">
                                  <div class="">          
                                  <label>Logo List  Above Content</label>
                                  </div>
                                  <input type="text" name="logo_list_title" class="form-control required" data-validation="required,number" value="<?= page_content('logo_list_title') ?>">
                                  <span style="color: red;" id="logo_list_er" class="error"></span>
                                
                                  
                                </div>

                            </div>

                                   
                                  </div>
                                  <div class="tab-pane fade" id="price-md" role="tabpanel" aria-labelledby="price-tab-md">

                                  <div class="col-sm-12">
                              
                              <div class="newemailset">
                                  <div class="">          
                                  <label>Price page Title</label>
                                  </div>
                                  <input type="text" name="price_page_title" class="form-control price_page_title required" data-validation="required,number" value="<?= page_content('price_page_title') ?>">
                                  <span style="color: red;" id="price_page_title_er" class="error"></span>
                           

                                  <div class="">          
                                  <label>Price page description</label>
                                  </div>
                                  <input id="price_page_description" name="price_page_description" class="txtDropTarget  price_page_description no_setup_ form-control" value="<?= htmlentities(page_content('price_page_description')) ?>">
                                  <span style="color: red;" id="price_page_description_er" class="error"></span>
                                </div>

                            </div>



                            <div class="col-sm-12 minauto">
                      
                              <div class="newemailset">
                        
                                  <div class="">          
                                  <label>Below Subscripttion description</label>
                                  </div>
                                  <input id="below_description" name="safety_description" class="txtDropTarget  below_description below_description form-control" value="<?= html_entity_decode(page_content('below_description')); ?>">
                                  <span style="color: red;" id="below_description_er" class="error"></span>
                                </div>

                            </div>



                                <div class="col-sm-12">
                      
                               <div class="newemailset">
                                  <div class="">    
                                  <label>Title After Cloud Image</label>
                                  </div>
                                  <input type="text" name="price_cloud_title" class="form-control price_page_title required" data-validation="required,number" value="<?= page_content('price_cloud_title') ?>">
                                  <span style="color: red;" id="price_cloud_title_er" class="error"></span>


                                                                  
                                  <div class="">          
                                  <label>Description After Cloud Image</label>
                                  </div>
                                  <input id="price_cloud_description" name="price_cloud_description" class=" txtDropTarget  price_cloud_description price_cloud_description form-control" value="<?= html_entity_decode(page_content('price_cloud_description')); ?>">
                                  <span style="color: red;" id="price_cloud_description_er" class="error"></span>
                                </div>
                                </div>


                                <div class="col-sm-12">
                                 <div class="newemailset">
                                  <div class="">          
                                  <label>Logo List  Above Content</label>
                                  </div>
                                  <input type="text" name="logo_price_title" class="form-control required" data-validation="required,number" value="<?= page_content('logo_price_title') ?>">
                                  <span style="color: red;" id="logo_price_title_er" class="error"></span>
                                
                                  
                                </div>

                            </div>


                                   
                                  </div>
                                </div>


                        

                   
                             


                          </div>
                <div class="form-group row title_submitbt submitborder">
                    <input type="submit" value="submit" name="submit" id="submit_button">
                </div>
                </div>
            </form>
            
<!--end -->
</div>


                  </div>
               </div>
               <!-- close -->
            </div>
            <!-- Page body end -->
         </div>
      </div>
      <!-- Main-body end -->
      <div id="styleSelector">
      </div>
   </div>
</div>
</div>



<?php

$this->load->view('includes/session_timeout');

if ($_SESSION['firm_id'] == '0') {
    $this->load->view('super_admin/superAdmin_footer');
} else {
    $this->load->view('includes/footer');
}

?>

    
    <script type="text/javascript">
          /*For editor config*/


  var tinymce_config = {    
    plugins: ' preview  paste importcss searchreplace autolink     visualblocks visualchars fullscreen image link table  hr pagebreak nonbreaking anchor toc  advlist lists imagetools textpattern noneditable ',
    menubar: 'file edit view insert format tools table',
    toolbar: 'fontselect fontsizeselect | forecolor backcolor | bold italic | alignleft aligncenter alignright alignjustify | insertfile image  link | numlist bullist  | hr | fullscreen  preview ',
    file_picker_types: "image",
    height: 300,
    relative_urls : false,
    remove_script_host : false,
    convert_urls : true,
  };  

function tiny_editor(){
 // console.log(tinymce_config);
    tinymce_config['selector']          = '.txtDropTarget'; tinymce.init( tinymce_config ); 
}



      $(document).ready(function(){   

        tiny_editor();
  /*For editor config*/

  // tinymce_config['selector']          = '.home_page_header_description'; tinymce.init( tinymce_config ); 
  // tinymce_config['selector']          = '.task_description'; tinymce.init( tinymce_config ); 
  // tinymce_config['selector']          = '.deadline_description'; tinymce.init( tinymce_config ); 
  // tinymce_config['selector']          = '.timesheet_description'; tinymce.init( tinymce_config ); 
  // tinymce_config['selector']          = '.proposal_description'; tinymce.init( tinymce_config ); 
  // tinymce_config['selector']          = '.client_description'; tinymce.init( tinymce_config ); 
  // tinymce_config['selector']          = '.create_description'; tinymce.init( tinymce_config ); 
  // tinymce_config['selector']          = '.run_description'; tinymce.init( tinymce_config ); 
  // tinymce_config['selector']          = '.integrate_description'; tinymce.init( tinymce_config ); 
  // tinymce_config['selector']          = '.communicate_description'; tinymce.init( tinymce_config ); 



  // tinymce_config['selector']          = '.communicate_description'; tinymce.init( tinymce_config ); 

  /*For editor config*/



$('.nav-item a').click(function(event) {

            event.preventDefault();

          var active_id=$(this).attr('id');
         // console.log(active_class);

          if(active_id=='home-tab-md')
           {
            $('.page_heading').html(' Home Page Content ');
           }
           else if(active_id=='feature-tab-md')
           {
            $('.page_heading').html('Features Page Content ');

           }
           else{
            $('.page_heading').html('Pricing Page Content');
          }

  
      });
});
</script>

  <!-- tinymce editor -->
      <script src="<?php
echo base_url();
?>assets/js/tinymce.min.js"></script>
    <!-- tinymce editor -->
<script type="text/javascript">


 $(document).ready(function(){   
// $(document).on('click', '#submit_button', function(event){
  $('#submit_button').click(function(){
  /* Act on the event */
    event.preventDefault();
    var error = false;
    var arr = [];
    var txtarr = [];
    var active_class='';
     
     $('.nav-item a').each(function(){

      if($(this).hasClass('active'))
      active_class=$(this).attr('href');

     });

     var formData = [];



      //var active_class=$('.nav-item .nav-link').hasClass('active');

      // console.log(active_class);
     // return;

    $(active_class+' .txtDropTarget').each(function() {

      var id=$(this).attr('id');

      

      var ids=id.replace(/_/g, " ");

        //console.log(ids);



       var description_value = tinyMCE.get(id).getContent();
      // console.log(description_value);

       if(description_value == '') {
       // console.log('check');
       $(this).parent('.newemailset').find('.error').html(ids +' is required');
        // error = false;
        txtarr.push($(this));
      } else {
       $(this).parent('.newemailset').find('.error').html('');
        txtarr.pop($(this));
        // error = true;

        formData.push({ name: id, value: description_value });
      }

    });



// console.log(arr.length+'-'+txtarr.length);




    $(active_class+' .required').each(function() {

       var id=$(this).attr('name').replace(/_/g, " ");
      if($(this).val() == '') {
        $(this).next('.error').html(id+' is required');
        // error = false;
        arr.push($(this));
      } else {
        $(this).next('.error').html('');
        arr.pop($(this));
        // error = true;
        formData.push({ name: id, value: $(this).val() });
      }
    });

 console.log(arr.length+'-'+txtarr.length);

        if(arr.length +parseInt(txtarr.length)  > 0){
      
        return;
      
      }



       
  formData.push({ name: this.name, value: this.value });


 // console.log(formData);

 //  return;


        $.ajax({
          url: '<?= base_url("super_admin/home_content") ?>',
          type: 'POST',
          data: formData,
         beforeSend: function() {
                
                          $(".LoadingImage").show();
                        },
          success:function(ret){
            $(".LoadingImage").hide();
            $('.popup_info_msg').modal('show');
            console.log(ret);
          }
        });


});
  


});

    </script>

