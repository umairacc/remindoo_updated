
</div>
</div>
</div>
</div>

  <div class="modal fade" id="superAdmin_changePassword_popup" role="dialog">
    <div class="modal-dialog modal-dialog-super-admin">
      <form id="superAdmin_changePassword_form" method="post" name="feed_form"> 
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit Profiles</h4>
          </div>
          <div class="modal-body"> 
            <div class="info-box alert alert-danger alert-dismissible fade show " style="display: none;">
              <strong>Danger!</strong>
            </div>       
            <div class="form-group"> 
              <label><b>Display Name</b></label>
              <input type="text" name="display_name"> 
            </div>
            <div class="form-group"> 
              <label><b>User Name</b></label>
              <input type="text" name="user_name" id="user_name"> 
            </div>
            <div class="form-group"> 
              <label><b>Password</b></label>
              <input type="password" name="password" id="new_password"> 
            </div>
            <div class="form-group"> 
              <label><b>Confirm Password</b></label>
              <input type="password" name="confirm_password"> 
            </div>
            <?php $currency = $this->Common_mdl->getallrecords('crm_currency'); ?>
             <div class="form-group">                     
              <label><b>Currency</b></label>             
              <div class="select">
                <select name="currency" class="form-control" required="required">
                <option value="">Select Currency</option>
                <?php foreach($currency as $key => $value) {  ?>
                <option value="<?php echo $value['country']; ?>"><?php echo $value['country'].'-'.$value['currency']; ?></option> 
                <?php  } ?>
               </select>                                    
             </div>       
            </div>
            <div class="form-group row attach-files file-attached1">
               <label class="col-sm-4 col-form-label">Profile Picture</label>
               <div class="col-sm-8">
                 <div class="custom_upload">   
                    <input type="file" name="profile_picture" id="profile_picture" onchange="readURL(this);">
                 </div>
                  <div class="custom_edit_update">
                      <img src="<?php echo base_url().'uploads/unknown.png'?>" id="preview_image" >
                  </div>                  
               </div>
            </div>
         </div>
          <div class="modal-footer">
            <div class="feedback-submit mail_bts1">
              <div class="feed-submit text-left">
                 <input id="feed" name="submit" type="submit" value="Save">
              </div>
           </div>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <div id="Confirmation_popup" class="modal fade" role="dialog" style="display:none;">
      <div class="modal-dialog modal-delete-sub-task">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal">&times;</button>
             <h4 class="modal-title" style="text-align-last: center">Confirmation</h4>
          </div>
          <div class="modal-body body-firm-sa">
             <label class="information"></label>
          </div>
          <div class="modal-footer footer-firm-sa">             
              <button class="Confirmation_OkButton">Save</button>
              <button class="Confirmation_CancelButton" data-dismiss="modal">Close</button>              
          </div>
        </div>
    </div>
  </div>
  <!-- modal 1-->
  <div class="modal fade all_layout_modal" id="default-Modal" tabindex="-1" role="dialog">
     <div class="modal-dialog" role="document">
        <div class="modal-content">
           <div class="modal-header">
              <h4 class="modal-title">Search Companines House</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              </button>
           </div>
           <div class="modal-body">
              <div class="input-group input-group-button input-group-primary modal-search">

                 <input type="text" class="form-control" placeholder="Search here..." id="searchCompany">
                 <button class="btn btn-primary input-group-addon" id="basic-addon1">
                 <i class="ti-search" aria-hidden="true"></i>
                 </button>
              </div>
              <div class="british_gas1 trading_limited" id="searchresult">
              </div>
              <div class="british_view" id="companyprofile" style="display:none" >                     
              </div>
              <div class="main_contacts" id="selectcompany" style="display:none">
               
              </div>
           </div>
           <!-- modalbody -->
        </div>
     </div>
  </div>
<input type="hidden" name="user_id" id="user_id" value="<?php if(isset($client[0]['user_id']) && ($client[0]['user_id']!='') ){ echo $client[0]['user_id'];}?>" class="fields">

<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script> 
  <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-ui/js/jquery-ui.min.js"></script>  
  <script type="text/javascript" src="<?php echo base_url();?>bower_components/popper.js/js/popper.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/switchery/js/switchery.min.js"></script>
  <?php
  if( isset($INCLUDE) &&  in_array( 'DT_JS' , $INCLUDE ) )
  {
    ?>
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url()?>assets/js/user_page/materialize.js"></script> 
  <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
  <script src="<?php echo base_url()?>assets/js/custom/buttons.colVis.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>
  <script src="<?php echo base_url()?>assets/js/custom/datatable_extension.js"></script>

  <!-- For Export Buttom -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
  <!-- For Export Buttom -->

  <?php
    }   
   ?> 
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
  <?php $url = $this->uri->segment('2');  if($url=='templates') { ?>
  <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
  <?php } ?>
  <script src="<?php echo base_url()?>assets/js/validation/jquery.validate.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/validation/additional-methods.js"></script> 
  <script src="<?php echo base_url();?>assets/js/bootbox.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/debounce.js"></script>
  <script src="<?php echo base_url();?>assets/js/bootstrap-colorpicker.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/bootstrap-slider.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.js"></script>
  <script src="https://cdn.jsdelivr.net/medium-editor/latest/js/medium-editor.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/creative.tools.js"></script> 
  <script src="<?php echo base_url(); ?>/assets/js/editor.js"></script> 
  <!-- <script src="https://cdn.ckeditor.com/4.11.1/standard-all/ckeditor.js"></script> -->

  <!-- tinymce editor -->
    <script src="<?php echo base_url();?>assets/js/tinymce.min.js"></script>
  <!-- tinymce editor -->


  <script type="text/javascript">
  var base_url = "<?php echo base_url();?>";
  var Conf_Confirm_Popup =   function(conf){

    if( 'OkButton' in conf ) 
    {
      var text = ('Text' in conf.OkButton   ? conf.OkButton.Text : 'Yes' );
      var fun = ('Handler' in conf.OkButton ? conf.OkButton.Handler : function(){} );

      $('#Confirmation_popup .Confirmation_OkButton').html(text).unbind().on('click', fun );

    }

     if( 'CancelButton' in  conf ) 
    {
      var text = ('Text' in conf.CancelButton ? conf.CancelButton.Text : 'No' );
      var fun = ('Handler' in  conf.CancelButton ? conf.CancelButton.Handler : function(){} );
    
      $('#Confirmation_popup .Confirmation_CancelButton').html(text).unbind().on('click', fun);
    }

     if( 'Info' in conf )
    {
      $('#Confirmation_popup .information').html( conf.Info );
    }

  };
  var Show_LoadingImg = function()
  {
    $('.LoadingImage').show();
  };
  var Hide_LoadingImg = function()
  {
    $('.LoadingImage').hide();
  };

  $('.comclass li a.waves-effect').click(function(){
    $('#searchCompany').prop('readonly',false).val('');
    $('#searchresult').text('').show();
    $('#companyprofile').text('');
    $('#selectcompany').hide();
    $('.all_layout_modal .close').show();

  });

  //start js by ram for company house api//


function debounce(func, wait, immediate) {
  var timeout;
  return function() {
    var context = this, args = arguments;
    var later = function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};
//send arg as fun to debounce it desited to when it will call 

$('#searchCompany').keyup(debounce(function(){
  //$(".form_heading").html("<h2>Adding Client via Companies House</h2>");
        var $this=$(this);
        var term =$this.val();
        type_check(term);
    },500));

function type_check(term)
{ 
     $.ajax({
          url: '<?php echo base_url();?>firm/SearchCompany/',
          type: 'post',
          async:'true',
          data: { 'term':term },
          beforeSend : function()    {           
              $(".LoadingImage").show();
           },
          success: function( data ){
              $("#searchresult").html(data);
               $(".LoadingImage").hide();
              },
              error: function( errorThrown ){
                  console.log( errorThrown );
              }
          });
}






var xhr = null;

   function getCompanyRec(companyNo)
   {
      $(".LoadingImage").show();
       if( xhr != null ) {
                  xhr.abort();
                  xhr = null;
          }
       
      xhr = $.ajax({
          url: '<?php echo base_url();?>firm/CompanyDetails/',
          type: 'post',
          async:'true',
          data: { 'companyNo':companyNo },
          success: function( data ){
              //alert(data);
              $("#searchresult").hide();
              $('#searchCompany').prop('readonly',true);
              $("#companyprofile").html(data).show();
              $(".LoadingImage").hide();
              
              },
              error: function( errorThrown ){
                  console.log( errorThrown );
              }
          });
    }


   function getCompanyView(companyNo)
   {
//alert(companyNo);
      //$('.modal-search').hide();

       if( xhr != null )
       {
                  xhr.abort();
                  xhr = null;
       }

      xhr = $.ajax({
          url: '<?php echo base_url();?>firm/selectcompany/',
          type: 'post',
          async:'true',
          dataType: 'JSON',
          data: {'companyNo':companyNo},
          beforeSend: function()
          {
            $(".LoadingImage").show();
          },
          success: function( data )
          {
              //alert(data.html);
              $("#searchresult").hide();
              $("#selectcompany").html(data.html).show();
              $('#companyprofile').hide();
              $('.all_layout_modal .close').hide();
              //  $(".edit-button-confim").show(); //for hide edit option button 
              $(".LoadingImage").hide();
              },
              error: function( errorThrown ){
              $(".LoadingImage").hide();
              $("#selectcompany").html("Data Not found");
                  console.log( errorThrown );
              }
          });
    }
   
   
   function getmaincontact(companyNo,appointments,i,user_id,obj)
   {
      $(".LoadingImage").show();

        var data = { 'companyNo':companyNo,'appointments': appointments,'count':i,'user_id':user_id};
        var exist_contact = 0;
        var modal = $(obj).closest('.all_layout_modal');

        if( modal.attr('id') == "company_house_contact" ) //check it is edit page popup
        {
          exist_contact = 1;
          var cnt = $("#append_cnt").val();

            if(cnt=='')
            {
              cnt = 1;
            }
           else
           {
            cnt = parseInt(cnt)+1;
           }
           data['cnt'] = cnt;
        $("#append_cnt").val(cnt);

        }
 
      xhr = $.ajax({
          url: '<?php echo base_url();?>firm/maincontact/',
          type: 'post',
          async:'true',
          data: data,
          success: function( data ){

            modal.find('#selectcompany #usecontact'+i).html('<span class="succ_contact"><a href="#">Added</a></span>');

            if(exist_contact)
            {

              $('.contact_form').append(data);
                    if(i=='First')
                    {
                           $(".contact_form .common_div_remove-"+cnt).find('.make_primary_section').trigger('click');
                    }
                  /** 05-07-2018 rs **/
                  $('.make_a_primary').each(function(){
                     var countofdiv=$('.make_a_primary').length;
                     if(countofdiv>1){
                     var id=$(this).attr('id').split('-')[1];

                      $('.for_remove-'+id).remove();
                       $('.for_row_count-'+id).append('<div class="btn btn-danger remove for_remove-'+id+'"><a href="javascript:void(0)" class="contact_remove" id="remove-'+id+'">Remove</a></div>');
                  }
                  });
                  var start = new Date();
              start.setFullYear(start.getFullYear() - 70);
              var end = new Date();
              end.setFullYear(end.getFullYear());
                $(".date_picker_dob").datepicker({ dateFormat: 'dd-mm-yy', changeMonth: true,
                  changeYear: true,yearRange: start.getFullYear() + ':' + end.getFullYear() }).val();
                modal.modal('hide');
              }
               if( typeof trigger_contact_person_validation == "function")
              {
               trigger_contact_person_validation(); 
              }
            $(".LoadingImage").hide();

                      },
              error: function( errorThrown ){
                  console.log( errorThrown );
              }
          });
    }
function addmorecontact(id)
{
  window.location.href = "<?php echo base_url()?>firm/add_firm/"+id;
}
function readURL(input)
{
 $(input).valid();
  if (input.files && input.files[0])
  {
    var type = ['png','jpg','jpeg','gif'];              
    var ext = input.files[0].name.split('.');
    ext = ext[ext.length -1];

    if(type.indexOf(ext.trim()) != -1 )
    {

      var reader = new FileReader();

      reader.onload = function (e) {
          $('#preview_image')
              .attr('src', e.target.result)
              .width(225)
              .height(225);
      };

      reader.readAsDataURL(input.files[0]);

    } 
  }
}

  //End comapny house api //

var switcheryElm = [];

    $(document).ready(function (){

      $(".theme-loader").remove();
      
       var image = $('#preview_image');
      var downloadingImage = new Image();
      downloadingImage.onload = function(){
          image.attr('src',this.src);   
      };

    $('a.show_profile_content').click(function(){  
          $.ajax({
             url: '<?php echo base_url()?>super_admin/GetProfile_Data',
             dataType : 'json',           
             type : 'POST',
             beforeSend:Show_LoadingImg,
             success: function(data)
             { 
              var modal = $("#superAdmin_changePassword_popup");
              modal.find('input[name="display_name"]').val( data.display_name );
              modal.find('input[name="user_name"]').val( data.user_name );
              modal.find('input[name="password"] ,input[name="confirm_password"] ').val( data.decrypted_password );
              modal.find('select[name="currency"]').val(data.currency);
              if( data.profile_picture_path != '')
              {
                downloadingImage.src = base_url+data.profile_picture_path;
              }
              modal.modal('show');
              Hide_LoadingImg();
             }
          });  
    }); 

       $("#superAdmin_changePassword_form").validate({
     
          ignore: false,
       
                           rules: {                           
                             display_name:
                             {
                               required:true
                             },
                             user_name:
                             {
                               required:true,
                               minlength:5
                             },
                             password:
                             {
                               required:true,
                             }, 
                             confirm_password:
                             {
                                equalTo:"#new_password"
                             },
                             profile_picture:
                             {
                                extension:'png|jpg|jpeg|gif'
                             }                           
                           },
                           errorElement: "span" , 
                           errorClass: "field-error",   
                           messages : 
                           {
                            profile_picture:
                            {
                              extension:"Select valid image."
                            }
                           },                          
                           submitHandler: function(form) { 
                              var formData = new FormData($("#superAdmin_changePassword_form")[0]);                               
                              var info_box = $("#superAdmin_changePassword_popup .info-box");
   
                               $.ajax({
                                   url: '<?php echo base_url()?>super_admin/Edit_Profile',
                                   dataType : 'json',
                                   type : 'POST',
                                   data : formData,
                                   contentType : false,
                                   processData : false,
                                   beforeSend:Show_LoadingImg,
                                   success: function(data)
                                   {                                      
                                   		 Hide_LoadingImg();

                                       if(data.result == 1)
                                       {
                                          info_box.removeClass('alert-danger').addClass('alert-success').show();
                                          
                                          info_box.find("strong").text('Updated SuccessFully..!');
                                          
                                          setTimeout(function(){ 
                                            info_box.hide();                                           
                                            $("#superAdmin_changePassword_popup").modal('hide');
                                            location.reload();
                                          }, 2000);
                                          
                                       }
                                       else
                                       {
                                          info_box.removeClass('alert-success').addClass('alert-danger').show();
                                          
                                          info_box.find("strong").text( data.message );
                                          
                                          setTimeout(function(){ info_box.hide(); }, 2000);

                                       }
                                   }
                               });
   
                               return false;
                           }
                            
                       });
   
   
  
        
    
      

      

        var date = $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy',minDate:0 }).val();
        console.log("test2");
        console.log( document.querySelectorAll('.js-small ,.checkall_services ,.services').length +"switchery");

        // Multiple swithces        
        var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small ,.checkall_services ,.services'));

        var index = 1;
        elem.forEach(function(html) {
            var switchery = new Switchery(html, {
                color: '#22b14c',
                jackColor: '#fff',
                size: 'small'
            });
            //var index =((!html.getAttribute('id')) ? '1' : html.getAttribute('id') );
            ///console.log(html.getAttribute('id') );
            html.setAttribute('data-switcheryId',index);
            switcheryElm[index] = switchery;
            index++;
        });

        $('#accordion_close').on('click', function(){
                $('#accordion').slideToggle(300);
                $(this).toggleClass('accordion_down');
        });


        $('.chat-single-box .chat-header .close').on('click', function(){
          $('.chat-single-box').hide();
        });

    
   /* var resrow = $('#responsive-reorder').DataTable({
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        responsive: true
    });*/
});
    $(document).mouseup(function(e) {
    var container = $(".multiple-select-dropdown");
    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) 
    {
        //console.log('ok');
        $('.themicond').removeClass('shown');
        $(".multiple-select-dropdown").removeClass('Show_content');
    }
    });
    $(document).on('click','.dt-button-background',function(e){
         $('.dt-buttons').find('.dt-button-collection').detach();
          $('.dt-buttons').find('.dt-button-background').detach();
    });
       /*For editor config*/
  var tinymce_config = {    
    plugins: ' preview  paste importcss searchreplace autolink     visualblocks visualchars fullscreen image link table  hr pagebreak nonbreaking anchor toc  advlist lists imagetools textpattern noneditable ',
    menubar: 'file edit view insert format tools table',
    toolbar: 'fontselect fontsizeselect | forecolor backcolor | bold italic | alignleft aligncenter alignright alignjustify | insertfile image  link | numlist bullist  | hr | fullscreen  preview ',
    file_picker_types: "image",
    height: 300,
    relative_urls : false,
    remove_script_host : false,
    convert_urls : true,
  };  
  /*For editor config*/
  </script>

    <?php

 $uri_seg_menu = $this->uri->segment('1'); 
if($this->uri->segment(2)!='proposal' || $this->uri->segment(2)!='proposal_feedback'){

if( strtolower($uri_seg_menu)=='tickets'){ 
  ?>  
<?php

   
               if($_SESSION['user_type']=='SA')
              {
                      $result=array();
            $login_user_id=$_SESSION['userId'];

             $data['getalladmin']=$this->db->query("SELECT * FROM user where crm_name!='' and user_type='FA'  order by id DESC")->result_array();

             
                $result=$this->db->query("SELECT `firm_id` FROM tickets")->result_array();
                 $result =array_values(array_unique(array_column( $result,'firm_id')));

                   if(!empty($result)){

                    $result=$this->db->query("SELECT `user_id` FROM firm where firm_id in (".implode(',',$result).")  order by firm_id DESC")->result_array();

                     $result =array_values(array_unique(array_column( $result,'user_id')));
                   }

                  $for_assigned=array();
        
                    $data['getallstaff']=$data['getallUser']=array();
             
                  $newarray=array_values(array_unique($result));
              
                  //  print_r($newarray);
                   // exit;
          
            if(!empty($newarray)){
                $data['getallUser']=$this->db->query("SELECT * FROM user where id in (".implode(',',$newarray).") and user_type!='FC' order by id DESC")->result_array();
            }
            
        }

                // print_r($data['getallUser']);
                // exit;

        
        $data['column_setting']=$this->Common_mdl->getallrecords('column_setting_new');
        $data['client'] =$this->db->query('select * from column_setting_new where id=1')->row_array();
        $data['feedback'] =$this->db->query("select id,username,crm_email_id from user where username!='' and username!='0' and  crm_email_id!='' and crm_email_id!='0' "  )->result();



         $this->load->view('User_chat/chat_page_new',$data); 
      
       ?> 
      
     

<?php } 

} ?>

