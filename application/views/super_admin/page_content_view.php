<?php

 if($page_id ==0) { ?>
						 <div class="col-sm-12">
                              
                              <div class="newemailset">
                                  <div class="">          
                                  <label>Home page header</label>
                                  </div>
                                  <input type="text" name="home_page_title" class="form-control home_page_title required" data-validation="required,number" value="<?=page_content('home_page_title')?>">
                                  <span style="color: red;" id="home_page_title_er" class="error"></span>
                           

                                  <div class="">          
                                  <label>Home Page header description</label>
                                  </div>
                                  <input id="home_page_title_description" name="home_page_title_description" class="txtDropTarget  home_page_title_description home_content form-control required" value="<?=htmlentities(page_content('home_page_title_description'))?>">
                                  <span style="color: red;" id="home_page_title_description_er" class="error"></span>
                                </div>

                            </div>


            

                            <div class="col-sm-12">
                      
                              <div class="newemailset">
                                  <div class="">          
                                  <label>Section title</label>
                                  </div>
                                  <input type="text" name="section_title" class="form-control required" data-validation="required,number" value="<?=page_content('section_title')?>">
                                  <span style="color: red;" id="section_title_er" class="error" ></span>
                                </div>
     
                            </div>



                            <div class="col-sm-12">
                      
                              <div class="newemailset">
                                  <div class="">          
                                  <label>Task title</label>
                                  </div>
                                  <input type="text" name="task_title" class="form-control required" data-validation="required,number" value="<?=page_content('task_title')?>">
                                  <span style="color: red;" id="task_title_er" class="error" ></span>
                               
                                  <div class="">          
                                  <label>Task description</label>
                                  </div>
                                  <input id="task_description" name="task_description" class="txtDropTarget  task_description home_content form-control" value="<?= html_entity_decode(page_content('task_description')); ?>">
                                  <span style="color: red;" id="task_description_er" class="error"></span>
                                </div>

                            </div>



                                <div class="col-sm-12">
                      
                              <div class="newemailset">
                                  <div class="">          
                                  <label>Deadline title</label>
                                  </div>
                                  <input type="text" name="deadline_title" class="form-control required" data-validation="required,number" value="<?=page_content('deadline_title')?>">
                                  <span style="color: red;" id="deadline_title_er" class="error"></span>
                                
                                  <div class="">          
                                  <label>Deadline description</label>
                                  </div>
                                  <input id="deadline_description" name="deadline_description" class="txtDropTarget  deadline_description home_content form-control" value="<?= html_entity_decode(page_content('deadline_description')); ?>">
                                  <span style="color: red;" id="deadline_description_er" class="error"></span>
                                </div>

                            </div>



            				  <div class="col-sm-12">
                      
                              <div class="newemailset">
                                  <div class="">          
                                  <label>Timesheet title</label>
                                  </div>
                                  <input type="text" name="timesheet_title" class="form-control required" data-validation="required,number" value="<?=page_content('timesheet_title')?>">
                                  <span style="color: red;" id="timesheet_title_er" class="error"></span>
                               
                                  <div class="">          
                                  <label>Timesheet description</label>
                                  </div>
                                  <input id="timesheet_description" name="timesheet_description" class="txtDropTarget  timesheet_description home_content form-control" value="<?= html_entity_decode(page_content('timesheet_description')); ?>">
                                  <span style="color: red;" id="timesheet_description_er" class="error"></span>
                                </div>

                            </div>





                              <div class="col-sm-12">
                      
                              <div class="newemailset">
                                  <div class="">          
                                  <label>Proposal title</label>
                                  </div>
                                  <input type="text" name="proposal_title" class="form-control required" data-validation="required,number" value="<?=page_content('proposal_title')?>">
                                  <span style="color: red;" id="proposal_title_er" class="error"></span>
                               
                                  <div class="">          
                                  <label>Proposal description</label>
                                  </div>
                                  <input id="proposal_description" name="proposal_description" class="txtDropTarget  proposal_description home_content form-control" value="<?= html_entity_decode(page_content('proposal_description')); ?>">
                                  <span style="color: red;" id="proposal_description_er" class="error"></span>
                                </div>

                            </div>




                              <div class="col-sm-12">
                      
                              <div class="newemailset">
                                  <div class="">          
                                  <label>Client title</label>
                                  </div>
                                  <input type="text" name="client_title" class="form-control required" data-validation="required,number" value="<?=page_content('client_title')?>">
                                  <span style="color: red;" id="client_title_er" class="error"></span>
                                
                                  <div class="">          
                                  <label>Client description</label>
                                  </div>
                                  <input id="client_description" name="client_description" class="txtDropTarget  client_description home_content form-control" value="<?= html_entity_decode(page_content('client_description')); ?>">
                                  <span style="color: red;" id="client_description_er" class="error"></span>
                                </div>

                            </div>






                              <div class="col-sm-12">
                      
                              <div class="newemailset">
                                  <div class="">          
                                  <label>Video title</label>
                                  </div>
                                  <input type="text" name="video_title" class="form-control required" data-validation="required,number" value="<?=page_content('video_title')?>">
                                  <span style="color: red;" id="video_title_er" class="error"></span>
                                </div>
     
                            </div>

      





          					<div class="col-sm-12">
                      
                              <div class="newemailset">
                                  <div class="">          
                                  <label>Create title</label>
                                  </div>
                                  <input type="text" name="create_title" class="form-control required" data-validation="required,number" value="<?=page_content('create_title')?>">
                                  <span style="color: red;" id="create_title_er" class="error"></span>
                               
                                  <div class="">          
                                  <label>Create description</label>
                                  </div>
                                  <input id="create_description" name="create_description" class="txtDropTarget  create_description home_content form-control" value="<?= html_entity_decode(page_content('create_description')); ?>">
                                  <span style="color: red;" id="create_description_er" class="error"></span>
                                </div>

                            </div>





     						<div class="col-sm-12">
                      
                              <div class="newemailset">
                                  <div class="">          
                                  <label>Run title</label>
                                  </div>
                                  <input type="text" name="run_title" class="form-control required" data-validation="required,number" value="<?=page_content('run_title')?>">
                                  <span style="color: red;" id="run_title_er" class="error"></span>
                               
                                  <div class="">          
                                  <label>Run description</label>
                                  </div>
                                  <input id="run_description" name="run_description" class="txtDropTarget  run_description home_content form-control" value="<?= html_entity_decode(page_content('run_description')); ?>">
                                  <span style="color: red;" id="run_description_er" class="error"></span>
                                </div>

                            </div>




                              <div class="col-sm-12">
                      
                              <div class="newemailset">
                                  <div class="">          
                                  <label>Integrate title</label>
                                  </div>
                                  <input type="text" name="integrate_title" class="form-control required" data-validation="required,number" value="<?=page_content('integrate_title')?>">
                                  <span style="color: red;" id="integrate_title_er" class="error"></span>
                               
                                  <div class="">          
                                  <label>Integrate description</label>
                                  </div>
                                  <input id="integrate_description" name="integrate_description" class="txtDropTarget  integrate_description home_content form-control" value="<?= html_entity_decode(page_content('integrate_description')); ?>">
                                  <span style="color: red;" id="integrate_description_er" class="error"></span>
                                </div>

                            </div>


                    		<div class="col-sm-12">
                      
                              <div class="newemailset">
                                  <div class="">          
                                  <label>Communicate title</label>
                                  </div>
                                  <input type="text" name="communicate_title" class="form-control required" data-validation="required,number" value="<?=page_content('communicate_title')?>">
                                  <span style="color: red;" id="communicate_title_er" class="error"></span>
                                
                                  <div class="">          
                                  <label>Communicate description</label>
                                  </div>
                                  <input id="communicate_description" name="communicate_description" class="txtDropTarget  communicate_description home_content form-control" value="<?= html_entity_decode(page_content('communicate_description')); ?>">
                                  <span style="color: red;" id="communicate_description_er" class="error"></span>
                                </div>

                            </div>

                            

                            <div class="col-sm-12">
                      
                              <div class="newemailset">
                                  <div class="">          
                                  <label>Subscription Title</label>
                                  </div>
                                  <input type="text" name="subscription_title" class="form-control required" data-validation="required,number" value="<?=page_content('subscription_title')?>">
                                  <span style="color: red;" id="subscription_title_er" class="error"></span>
                                </div>
     
                            </div>
                          
<?php } else if($page_id ==1) { ?>
						 <div class="col-sm-12">
                              
                              <div class="newemailset">
                                  <div class="">          
                                  <label>Feature page header</label>
                                  </div>
                                  <input type="text" name="feature_page_title" class="form-control home_page_title required" data-validation="required,number" value="<?=page_content('feature_page_title')?>">
                                  <span style="color: red;" id="feature_page_title_er" class="error"></span>
                           

                                  <div class="">          
                                  <label>No Setup description</label>
                                  </div>
                                  <input id="no_setup_description" name="no_setup_description" class="txtDropTarget  no_setup_description  form-control required" value="<?=htmlentities(page_content('no_setup_description'))?>">
                                  <span style="color: red;" id="no_setup_description_er" class="error"></span>
                                </div>

                            </div>



                            <div class="col-sm-12">
                      
                              <div class="newemailset">
                        
                                  <div class="">          
                                  <label>Safety description</label>
                                  </div>
                                  <input id="safety_description" name="safety_description" class="txtDropTarget  safety_description  form-control" value="<?= html_entity_decode(page_content('safety_description')); ?>">
                                  <span style="color: red;" id="safety_description_er" class="error"></span>
                                </div>

                            </div>



                                <div class="col-sm-12">
                      
                              <div class="newemailset">
                                                                  
                                  <div class="">          
                                  <label>Optimized Data  description</label>
                                  </div>
                                  <input id="optimized_data_description" name="optimized_data_description" class=" txtDropTarget   optimized_data_description form-control" value="<?= html_entity_decode(page_content('optimized_data_description')); ?>">
                                  <span style="color: red;" id="optimized_data_description_er" class="error"></span>
                                </div>

                            </div>



            				  <div class="col-sm-12">
                      
                              <div class="newemailset">
                             
                               
                                  <div class="">          
                                  <label>Quick Access description</label>
                                  </div>
                                  <input id="quick_access_description" name="quick_access_description" class="txtDropTarget  quick_access_description  form-control" value="<?= html_entity_decode(page_content('quick_access_description')); ?>">
                                  <span style="color: red;" id="quick_access_description_er" class="error"></span>
                                </div>

                            </div>





                              <div class="col-sm-12">
                      
                              <div class="newemailset">
                               
                               
                                  <div class="">          
                                  <label>24h Support</label>
                                  </div>
                                  <input id="support_description" name="support_description" class="txtDropTarget  support_description  form-control" value="<?= html_entity_decode(page_content('support_description')); ?>">
                                  <span style="color: red;" id="support_description_er" class="error"></span>
                                </div>

                            </div>




                              <div class="col-sm-12">
                      
                              <div class="newemailset">
                                  <div class="">          
                                  <label>Dashboard Image Above title</label>
                                  </div>
                                  <input type="text" name="dashboard_image_title" class="form-control required" data-validation="required,number" value="<?=page_content('dashboard_image_title')?>">
                                  <span style="color: red;" id="dashboard_image_title_er" class="error"></span>
                                
                                  
                                </div>
                               </div>

                                <div class="col-sm-12">
                                 <div class="newemailset">
                                  <div class="">          
                                  <label>Logo List  Above Content</label>
                                  </div>
                                  <input type="text" name="logo_list_title" class="form-control required" data-validation="required,number" value="<?=page_content('logo_list_title')?>">
                                  <span style="color: red;" id="logo_list_er" class="error"></span>
                                
                                  
                                </div>

                            </div>


                          
<?php } else if($page_id ==2) { ?>
						 <div class="col-sm-12">
                              
                              <div class="newemailset">
                                  <div class="">          
                                  <label>Price page Title</label>
                                  </div>
                                  <input type="text" name="price_page_title" class="form-control price_page_title required" data-validation="required,number" value="<?=page_content('price_page_title')?>">
                                  <span style="color: red;" id="price_page_title_er" class="error"></span>
                           

                                  <div class="">          
                                  <label>Price page description</label>
                                  </div>
                                  <input id="price_page_description" name="price_page_description" class="txtDropTarget  price_page_description no_setup_ form-control required" value="<?=htmlentities(page_content('price_page_description'))?>">
                                  <span style="color: red;" id="price_page_description_er" class="error"></span>
                                </div>

                            </div>



                            <div class="col-sm-12">
                      
                              <div class="newemailset">
                        
                                  <div class="">          
                                  <label>Below Subscripttion description</label>
                                  </div>
                                  <input id="below_description" name="safety_description" class="txtDropTarget  below_description below_description form-control" value="<?= html_entity_decode(page_content('below_description')); ?>">
                                  <span style="color: red;" id="below_description_er" class="error"></span>
                                </div>

                            </div>



                                <div class="col-sm-12">
                      
                             	 <div class="newemailset">
                              		<div class="">    
                                  <label>Title After Cloud Image</label>
                                  </div>
                                  <input type="text" name="price_cloud_title" class="form-control price_page_title required" data-validation="required,number" value="<?=page_content('price_cloud_title')?>">
                                  <span style="color: red;" id="price_cloud_title_er" class="error"></span>


                                                                  
                                  <div class="">          
                                  <label>Description After Cloud Image</label>
                                  </div>
                                  <input id="price_cloud_description" name="price_cloud_description" class=" txtDropTarget  price_cloud_description price_cloud_description form-control" value="<?= html_entity_decode(page_content('price_cloud_description')); ?>">
                                  <span style="color: red;" id="price_cloud_description_er" class="error"></span>
                                </div>
                                </div>


                                <div class="col-sm-12">
                                 <div class="newemailset">
                                  <div class="">          
                                  <label>Logo List  Above Content</label>
                                  </div>
                                  <input type="text" name="logo_price_title" class="form-control required" data-validation="required,number" value="<?=page_content('logo_price_title')?>">
                                  <span style="color: red;" id="logo_price_title_er" class="error"></span>
                                
                                  
                                </div>

                            </div>


                          
<?php } ?>


<!-- <script type="text/javascript">
    	  /*For editor config*/


  var tinymce_config = {    
    plugins: ' preview  paste importcss searchreplace autolink     visualblocks visualchars fullscreen image link table  hr pagebreak nonbreaking anchor toc  advlist lists imagetools textpattern noneditable ',
    menubar: 'file edit view insert format tools table',
    toolbar: 'fontselect fontsizeselect | forecolor backcolor | bold italic | alignleft aligncenter alignright alignjustify | insertfile image  link | numlist bullist  | hr | fullscreen  preview ',
    file_picker_types: "image",
    height: 300,
    relative_urls : false,
    remove_script_host : false,
    convert_urls : true,
  };  

$(document).ready(function(){   
    tinymce_config['selector']          = '.txtDropTarget'; tinymce.init( tinymce_config ); 
});
</script>

      <script src="<?php echo base_url();?>assets/js/tinymce.min.js"></script> -->
