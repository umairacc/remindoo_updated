 <div id="chart_Donut11" style="width: 100%; height: 300px;"></div>


<?php

                  if (array_key_exists(0, $count_details)) {
                        $inactive_firm=$count_details[0];
                    }else
                    {
                       $inactive_firm=0;
                    }
                    if (array_key_exists(3, $count_details)) {
                        $archive_firm=$count_details[3];
                    }else
                    {
                     $archive_firm= 0;
                    }
                    if (array_key_exists(2, $count_details)) {
                        $frozen_firm=$count_details[2];
                    }else
                    {
                      $frozen_firm=0;
                    }
                    if (array_key_exists(1, $count_details)) {
                       $active_firm=$count_details[1];
                    }else
                    {
                       $active_firm=0;
                    } 

                     ?>


<script type="text/javascript">

             google.charts.load("current", {packages:["corechart"]});
         google.charts.setOnLoadCallback(drawChart1);
         function drawChart1() {
         var data = google.visualization.arrayToDataTable([
          ['Status','link','Count'],
          ['Active Client','<?php echo base_url(); ?>Admin_dashboard/Active_firm',<?php echo $active_firm; ?>],
          ['Inactive Client', '<?php echo base_url(); ?>Admin_dashboard/Inactive_firm',<?php echo $inactive_firm; ?>],
          ['Frozen Client', '<?php echo base_url(); ?>Admin_dashboard/Frozen_firm', <?php echo $frozen_firm; ?>],
          ['Archive Client', '<?php echo base_url(); ?>Admin_dashboard/Archive_firm', <?php echo $archive_firm; ?>],
         ]);
         var view = new google.visualization.DataView(data);
         view.setColumns([0, 2]);
         var options = {
         title: 'Firm Lists',
         pieHole: 0.4,
         };
         var chart = new google.visualization.PieChart(document.getElementById('chart_Donut11'));
         chart.draw(view, options);
         var selectHandler = function(e) {
         //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
         window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
         }
         // Add our selection handler.
         google.visualization.events.addListener(chart, 'select', selectHandler);
         }
</script>