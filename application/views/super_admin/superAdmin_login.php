<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Remindoo</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/superAdmin_style.css');?>">
</head>

<body>
 
	<section class="admin_login">
		<div class="login_row"  style="background-color: #fff; background-image: initial;">
			<div class="superadmin">
				<?php $result = $this->session->userdata('Login_result'); 
				if( !empty( $result ) )
				{
				?>

				<div class="alert alert-danger alert-dismissible">
  					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  					<strong>Failed!</strong> <?php echo $result;?>
				</div>
				<?php
					}
				?>
<!-- <div class="super_adminlogo"><img src="<?php echo base_url('assets/images/logo.png');?>" alt="logo"></div> -->
<div class="login_logo text-center">
	<svg class="menu__logo-img" xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48" style="
    display: inline-block;
    vertical-align: middle;
    margin-right: 16px;
    fill: #a485f2;
    fill-rule: evenodd;
	">
		<path data-name="Sigma symbol" class="svg-element" d="M237.418,8583.56a12.688,12.688,0,0,0,.419-3.37c-0.036-5.24-2.691-9.68-7.024-13.2h-3.878a20.819,20.819,0,0,1,4.478,13.01c0,4.56-2.456,10.2-6.413,11.4a16.779,16.779,0,0,1-2.236.51c-10.005,1.55-14.109-17.54-9.489-23.31,2.569-3.21,6.206-4.08,11.525-4.08h17.935A24.22,24.22,0,0,1,237.418,8583.56Zm-12.145-24.45c-8.571.02-12.338,0.98-16.061,4.84-6.267,6.49-6.462,20.69,4.754,27.72a24.092,24.092,0,1,1,27.3-32.57h-16v0.01Z" transform="translate(-195 -8544)"></path>
		</svg><p class="menu__logo-title" style="
			position: relative;
			display: inline-block;
			vertical-align: middle;
			font-weight: 600;
			color: #234c87;
			display: inline-block;
			font-size: 24px;
			padding-left: 0;
			margin: 0;
			vertical-align: middle;
			margin-top: 0;">
		REMINDOO</p></div>
				<div class="login_sections">
					<form action="<?php echo base_url()?>super_admin/check_credentials" role="form" id="LoginForm" method="post" novalidate>
					<div class="form-group">
						<div class="input-group">
							<input type="text" class="form-control" placeholder="User Name" name="user_name" />
							<span class="input-group-addon">
							<span class="glyphicon glyphicon-user"></span>
							</span>
						</div>
					</div>
					<div class="form-group">
						<div class="input-group">
							<input type="password" class="form-control" placeholder="Password" name='password'/>
							<span class="input-group-addon">
							<span class="glyphicon glyphicon-lock"></span>
							</span>
						</div>
					</div>

					<div class="form-group text-center">
						<button type="submit" class="btn btn-success btn-lg">Login</button>
						<!-- <div class="forget_admin"><a href="#" class="btn btn-link">forget Password</a></div> -->
					</div>
				</form>
			</div>
		</div>
		</div>

	</section>
<script
  src="https://code.jquery.com/jquery-2.2.4.js"
  integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
  crossorigin="anonymous"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script>
  $(document).ready(function(){ 
      $("#LoginForm").validate({
            // Rules for form validation
            rules : {
                "user_name" : {
                    required : true
                },                        
                "password" : {
                    required : true
                },
                  
            },
          
            messages : {
                "user_name" : {
                    required : 'Username Required'                           
                },    
                "password" : {
                    required : 'Password Required'                            
                },                        
                        
            },
			errorPlacement: function (error, element) {
			if(element.attr('name') == 'user_name' || element.attr('name') == 'password')
			element.parent().parent().append(error); 
			else
			error.insertAfter(element);
			} 
			 


        });

  });
</script>        

</body>
</html>