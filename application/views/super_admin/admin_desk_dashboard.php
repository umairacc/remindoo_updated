<?php //echo $_SESSION['id']; ?>

<?php $this->load->view('super_admin/superAdmin_header');
   $succ = $this->session->flashdata('success');
   $error = $this->session->flashdata('error');
   $userid = $this->session->userdata('id'); 
   ?>
   <link href="https://remindoo.uk//assets/css/timeline-style.css" type="text/css" rel="stylesheet" />
<style type="text/css">
   .to-do-list.sortable-moves {
   padding-left: 0;
   }
   .pcoded-content
   {
   float: left !important;
   width:100%;
   }
   .hide_classes{
      display: none  !important;
   }
   .hide_check{
      display: none !important;
   }

   .active
   {
     display: block !important;
   }

   .card
   {
     width: 500px;
     margin: auto;
   }   

   .breadcrumb-header span 
   {
      color: green;
   } 

</style>
<?php   
 // $service_list = $this->Common_mdl->getServicelist();   
 $current_date = date('Y-m-d'); 
 $columns_check = array('1'=> 'crm_confirmation_statement_due_date','2' =>'crm_ch_accounts_next_due','3'=>'crm_accounts_tax_date_hmrc','4'=>'crm_personal_due_date_return','5' => 'crm_vat_due_date','6' => 'crm_rti_deadline','7' => 'crm_pension_subm_due_date', '8' => '', '9' => '','10'=>'crm_next_booking_date','11' => 'crm_next_p11d_return_due','12' => 'crm_next_manage_acc_date','13' => 'crm_insurance_renew_date','14'=>'crm_registered_renew_date','15'=>'crm_investigation_end_date','16' => 'crm_investigation_end_date');   
 $select = array('1'=> 'confstatements','2' =>'accountss','3'=>'companytaxs','4'=>'personaltaxs','5' => 'vats','6' => 'payrolls','7' => 'workplaces', '8' => 'ciss', '9' => 'cissubs','10'=>'bookkeeps','11' => 'p11ds','12' => 'managements','13' => 'investgate','14'=>'registereds','15'=>'taxinvests','16' => 'taxadvices');
 $columns = array('1'=> 'conf_statement','2' =>'accounts','3'=>'company_tax_return','4'=>'personal_tax_return','5' => 'vat','6' => 'payroll','7' => 'workplace', '8' => 'cis', '9' => 'cissub','10'=>'bookkeep','11' => 'p11d','12' => 'management','13' => 'investgate','14'=>'registered','15'=>'taxinvest','16' => 'taxadvice');
?>   
<div class="modal fade" id="todoinfo" role="dialog">
   <div class="modal-dialog">      
      <div class="modal-content">         
         <div class="modal-body">
           <button type="button" class="close" data-dismiss="modal">&times;</button>  
           <div class="position-alert1"></div>
         </div>         
      </div>
   </div>
</div>
<button type="button" class="todoinfo" data-toggle="modal" data-target="#todoinfo" style="display: none;"></button>
<!-- management block -->
<div class="pcoded-content pcode-mydisk">
<div class="pcoded-inner-content">
<!-- Main-body start -->
<div class="main-body myDesk">
   <div class="page-wrapper">
      <div class="card desk-dashboard">
         <div class="inner-tab123" >
            <div class="deadline-crm1 floating_set">
              <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard" id="proposal_dashboard">
            <!--       <li class="nav-item ">
                      <a href="javascript:;" class="nav-link active"  data-tag="Basics">
                      Basics
                     </a>
                  </li>
                  <li class="nav-item ">
                      <a href="javascript:;" class="nav-link tasks"  data-tag="Tasks">
                      Tasks
                     </a>
                  </li> -->
                  <li class="nav-item ">
                      <a href="javascript:;" class="nav-link active"  data-tag="Firms">
                      Firm
                     </a>
                  </li>
                  <li class="nav-item ">
                   
                      <a href="javascript:;" class="nav-link"  data-tag="Services">
                      Services
                     </a>
                  </li>
              
               </ul>
            </div>
          <div class="tab-content">





         
           
             

               <!-- tab 3-->
  <div id="Firms" class="tab-pane active">

        
     <div class="card desk-dashboard mydisk-realign1 floating_set">          
        <div class="row impo">
            <div class="number-mydisk floating_set"> 

            <div class="count-mydisk1">
              <div class="target-disk">
              <a href="<?php echo base_url(); ?>Admin_dashboard/All_firm" >
                  <span>Number of Firm(s)</span>
                <div class="count-pos-01">
                  <strong><?php echo count($firm_details); ?></strong>
                  </div>
                </a>
              </div>              
            </div>
            
            <div class="count-mydisk1">
              <div class="target-disk">
              <a href="<?php echo base_url(); ?>Admin_dashboard/Active_firm" >
                <span>Number of Active Firm(s)</span>
                <div class="count-pos-01">
                  <strong><?php 
                    if (array_key_exists(1, $count_details)) {
                       $active_firm=$count_details[1];
                    }else
                    {
                       $active_firm=0;
                    }
                    echo $active_firm;
                  ?></strong>
                  </div>
                </a>
              </div>              
            </div>     

            <div class="count-mydisk1">
              <div class="target-disk">
              <a href="<?php echo base_url(); ?>Admin_dashboard/Frozen_firm" >
                <span>Frozen Firm(s)</span>
                <div class="count-pos-01">
                  <strong><?php if (array_key_exists(2, $count_details)) {
                        $frozen_firm=$count_details[2];
                    }else
                    {
                      $frozen_firm=0;
                    }
                    echo $frozen_firm ?></strong>
                  </div>
                </a>
              </div>              
            </div>

              <div class="count-mydisk1">
              <div class="target-disk">
              <a href="<?php echo base_url(); ?>Admin_dashboard/Archive_firm" >
                <span>Archive Firm(s)</span>
                <div class="count-pos-01">
                  <strong><?php if (array_key_exists(3, $count_details)) {
                        $archive_firm=$count_details[3];
                    }else
                    {
                     $archive_firm= 0;
                    } 
                      echo $archive_firm;
                    ?></strong>
                  </div>
                </a>
              </div>              
            </div>

            <div class="count-mydisk1">
              <div class="target-disk">
              <a href="<?php echo base_url(); ?>Admin_dashboard/Inactive_firm" >
                <span>Non Active Firm(s)</span>
                <div class="count-pos-01">
                  <strong><?php if (array_key_exists(0, $count_details)) {
                        $inactive_firm=$count_details[0];
                    }else
                    {
                       $inactive_firm=0;
                    }
                    echo  $inactive_firm;
                     ?></strong>
                  </div>
                </a>
              </div>              
            </div>
          </div>
                    
                      
            <div class="card desk-dashboard fr-bg12 widmar col-xs-12 col-md-6 mydisk-realign1">
               <div class="card">
                  <div class="card-header">
                     <h5>Firms</h5>
                  </div>
                  <span class="time-frame"><label>Time Frame:</label>
                  <select id="filter_firm">
                     <option value="year">This Year</option>
                     <option value="month">This Month</option>
                  </select>
                  <div class="card-block fileters_client">
                     <div id="chart_Donut11" style="width: 100%; height: 309px;"></div>
                  </div>
               </div>
            </div>

          </div>
        </div>
     </div>


       <!-- tab2-->
      <div id="Services" class="tab-pane fade">
         <div class="card desk-dashboard mydisk-realign1 floating_set">          
           <div class="row impo">
            <div class="number-mydisk floating_set"> 

       
            <?php foreach ($service_filter as $key => $value) { ?>
             
            <div class="count-mydisk1">
              <div class="target-disk">
              <a href="<?php echo base_url(); ?>Admin_dashboard/Active_services/<?php echo str_replace(" ","_",$value['service_name']) ?>" >
                <span><?php echo $value['service_name'] ?> accpeted by no's Firm(s)</span>
                <div class="count-pos-01">
                  <strong><?php 
                    if (array_key_exists($value['id'], $res)) {
                       $active_service=$res[$value['id']];
                    }else
                    {
                       $active_service=0;
                    }
                    echo $active_service;
                  ?></strong>
                  </div>
                </a>
              </div>              
            </div>  
            <?php } ?>

          </div>                                    
         </div>
        </div>
        </div>
      <!-- tab 2-->
     
         </div>
      </div>
      <!-- chart sec -->
   </div>
</div>




<?php $this->load->view('super_admin/superAdmin_footer');?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/todo.js"></script>
<!-- jquery sortable js -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/Sortable.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/sortable-custom.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/custom-google-chart.js"></script>

<script src="<?php echo base_url();?>assets/js/RGraph.common.core.js"></script>
<script src="<?php echo base_url();?>assets/js/RGraph.bar.js"></script>
<script src="<?php echo base_url();?>assets/js/RGraph.common.dynamic.js"></script>
<script type="text/javascript">


$( document ).ready(function() {

  $('#filter_client').val('month');
  $('#proposal').val('month');
  $('#filter_deadline').val('month');

  $(document).on('click','.tasks',function(){$('div .tab-content').find('div #home1').addClass('active');});
  $(document).on('click','.deads',function(){ setTimeout(function(){ $('#filter_deadline').val('month').trigger('change');},1000) });
  $(".checking").first().find('a').trigger('click');


   // $(".hide_check").each(function(){
   //    $(this).next('li').trigger('click');
   //     console.log($(this).next('li').attr('class'));
   // });

  

   // console.log( "ready!" );
   
   // var sec_id=$(".hide_classes").next().attr('id');
   
   // console.log(sec_id);
   
   // console.log($('#proposal_dashboard a[href="#'+sec_id+'"]').attr('class'));

   // $('#proposal_dashboard a[href="#'+sec_id+'"]').trigger('click');

});


 


   $(document).ready(function() {
   
         //   var date = $('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
   /*$( ".datepicker" ).datepicker({
          dateFormat: 'yy-mm-dd'
      });*/
   
   $('.tags').tagsinput({
       allowDuplicates: true
      });
      
      $('.tags').on('itemAdded', function(item, tag) {
          $('.items').html('');
   var tags = $('.tags').tagsinput('items');
        
      });
   });
   
   $("#contact_today").click(function(){
     if($("#contact_today").is(':checked')){
       $(".selectDate").css('display','none');
     }else{
       $(".selectDate").css('display','block');
     }
   });
   
   $(".contact_update_today").click(function(){
     var id = $(this).attr("data-id");
     if($(this).is(':checked')){
   
       $("#selectDate_update_"+id).css('display','none');
     }else{
       $("#selectDate_update_"+id).css('display','block');
     }
   });
   
    $("#all_leads").dataTable({
          "iDisplayLength": 10,
       });
   
</script>
<script type="text/javascript">
   $(document).ready(function(){
   
      $( "#leads_form" ).validate({
        rules: {
          lead_status: "required",  
          source: "required",  
          name: "required",  
        },
        messages: {
          lead_status: "Please enter your Status",
          source: "Please enter your Source",
          name: "Please enter your Name"
        },
        
      });
   
   
      $( ".edit_leads_form" ).validate({
        rules: {
          lead_status: "required",  
          source: "required",  
          name: "required",  
        },
        messages: {
          lead_status: "Please enter your Status",
          source: "Please enter your Source",
          name: "Please enter your Name"
        },
        
      });
   });
   
   
   $(".sortby_filter").click(function(){
         $(".LoadingImage").show();
   
      var sortby_val = $(this).attr("id");
      var data={};
      data['sortby_val'] = sortby_val;
      $.ajax({
      url: '<?php echo base_url();?>leads/sort_by/',
      type : 'POST',
      data : data,
      success: function(data) {
        $(".LoadingImage").hide();
   
      $(".sortrec").html(data);   
      $("#all_leads").dataTable({
      "iDisplayLength": 10,
      });
      },
      });
   
   });
   $("#import_leads").validate({
     rules: {
        file: {required: true, accept: "csv"},
   
          lead_status: "required",  
          source: "required",  
          name: "required",  
        },
        messages: {
          file: {required: 'Required!', accept: 'Please upload a file with .csv extension'},
   
          lead_status: "Please enter your Status",
          source: "Please enter your Source",
          name: "Please enter your Name"
        },
   
   });
                            
   
</script>

<script>

  $(document).on('change','#filter_firm',function()
   {
    //alert('chk');
       var id = $(this).val();
   
         $.ajax(
         {
            type: "POST",
            data:{id:id,},
            url: "<?php echo base_url(); ?>Admin_dashboard/firm_filter",             
            beforeSend: function() {
              $(".LoadingImage").show();
            },
            success: function(data)
            {   
              $(".fileters_client").html('');
              $(".fileters_client").append(data);   
              $(".LoadingImage").hide();             
            }
          });       
       
   });



         google.charts.load("current", {packages:["corechart"]});
         google.charts.setOnLoadCallback(drawChart1);
         function drawChart1() {
        var data = google.visualization.arrayToDataTable([
          ['Status','link','Count'],
          ['Active Client','<?php echo base_url(); ?>Admin_dashboard/Active_firm',<?php echo $active_firm; ?>],
          ['Inactive Client', '<?php echo base_url(); ?>Admin_dashboard/Inactive_firm',<?php echo $inactive_firm; ?>],
          ['Frozen Client', '<?php echo base_url(); ?>Admin_dashboard/Frozen_firm', <?php echo $frozen_firm; ?>],
          ['Archive Client', '<?php echo base_url(); ?>Admin_dashboard/Archive_firm', <?php echo $archive_firm; ?>],
         ]);
         var view = new google.visualization.DataView(data);
         view.setColumns([0, 2]);
         var options = {
         title: 'Firm Lists',
         pieHole: 0.4,
         };
         var chart = new google.visualization.PieChart(document.getElementById('chart_Donut11'));
         chart.draw(view, options);
         var selectHandler = function(e) {
         //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
         window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
         }
         // Add our selection handler.
         google.visualization.events.addListener(chart, 'select', selectHandler);
         }
</script>
<p id="myValueHolder"></p>



  <script type="text/javascript">

 $(document).ready(function(){


        $('.pull-left .nav-item a').click(function(){
            $('.pull-left .nav-item a').removeClass('active');
            $(this).addClass('active');
            var tagid = $(this).data('tag');
            $('.tab-pane').removeClass('active').addClass('hide');
            $('#'+tagid).addClass('active').removeClass('hide');
        });
    });


 </script>