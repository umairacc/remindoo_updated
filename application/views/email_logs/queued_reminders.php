<?php $this->load->view('includes/new_header'); ?>
<!-- Include custom css for email logs page -->
<link href="<?php echo base_url();?>assets/css/custom/email-logs.css" rel="stylesheet" type="text/css" />
<style type="text/css">
    .send_mail_task
    {
        margin-bottom: 2%;
    }
</style>
<div class="pcoded-content card-removes" id="task_list_view">
    <div class="pcoded-inner-content">
        <!-- Main-body start -->
        <div class="main-body">
            <div class="page-wrapper email-page-wrapper">
                <!-- Page body start -->
                <div class="page-body rem-tasks">
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- Register your self card start -->
                            <div id="EmailLogsFilter" class="email-logs-filter">
                                <form class="jtable-filter-form">
                                    <select id="status" class="form-control" name="status">
                                        <option value="">Select Status..</option>
                                        <option value="1">Sent</option>
                                        <option value="0">Queue</option>
                                    </select>
                                    <input type="text" name="from_date" id="from_date" class="form-control datepicker" placeholder="From Date">
                                    <input type="text" name="to_date" id="to_date" class="form-control datepicker" placeholder="To Date">
                                    
                                    <button class="btn-primary btn-sm email-log-btn" type="submit" id="search_botton">Apply</button>
                                    <button class="btn-primary btn-sm email-log-btn" type="submit" id="btn_apply_jtable_filter_clear">Clear</button>
                                   <!--  <button class="btn-primary btn-sm email-log-btn" type="submit" id="btn_send_mail"><i class="fa fa-paper-plane"></i> Send</button> -->
                                </form>
                            </div>
                            <div id="QueuedRemindersContainer" class="email-logs-container"></div>              
                        </div>
                    </div>
                    <!-- Page body end -->
                </div>
            </div>
            <!-- Main-body end -->      
        </div>
    </div>
</div>
<!-- Email Body Modal -->
<div id="emailBody" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Email Body</h4>
            </div>
            <div class="modal-body">
                <!-- <p>Some text in the modal.</p> -->
                <div id="email_body_content"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('includes/footer');?>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap/js/bootstrap.min.js"></script>
<!-- Include one of jTable styles. -->
<link href="<?php echo base_url();?>assets/plugins/jtable.2.4.0/themes/metro/blue/jtable.min.css" rel="stylesheet" type="text/css" />
<!-- Include jTable script file. -->
<script src="<?php echo base_url();?>assets/plugins/jtable.2.4.0/jquery.jtable.min.js" type="text/javascript"></script>
<!-- Include custom js for email logs page -->
<script src="<?php echo base_url();?>assets/js/custom/email_logs.js" type="text/javascript"></script>

<script>
$(document).ready(function () {
    $('#QueuedRemindersContainer').jtable({
        // title               : 'Queued Reminders',
        paging              : true, //Enable paging
        pageSize            : 10, //Set page size (default: 10)
        pageSizes           : [10, 25, 50, 100, 250, 500,1000,2000,5000,10000],
        sorting             : true, //Enable sorting
        multiSorting        : true,
        selecting           : true, //Enable selecting
        multiselect         : false, //Allow multiple selecting
        // selectingCheckboxes : true, //Show checkboxes on first column
        //selectOnRowClick  : false, //Enable this to only select using checkboxes
        actions: {
            listAction    : '<?php echo base_url();?>EmailLogs/get_queued_reminders'
        },
        fields: {
            checkbox: {
                title : '<input type="checkbox" name="email-check" class="order-check">',
                sorting : false,
                width : '2%'
            },
            crm_company_name: {
                title : 'Company Name'
            },
            service_name: {
                title : 'Srevice Name'
            },
            subject: {
                title : 'Subject',
                edit  : false,
                sorting : false
            },
            date: {
                title : 'Date',
                edit  : false
            },
            status: {
                title : 'Status' 
            },
            frequency: {
                title  : 'Frequency'
            },
            resend_email: {
                title   : 'Send',
                edit    : false,
                sorting : false
            }
        }
    });
    $('#QueuedRemindersContainer').jtable('load');

    $('#search_botton').click(function(e) {
        e.preventDefault();
        var data = $('.jtable-filter-form').serializeFormJSON();
        $('#QueuedRemindersContainer').jtable('load',data);
    });

    $('#btn_apply_jtable_filter_clear').click(function(e) 
    {
        e.preventDefault();
        $('.jtable-filter-form').find("input[type=text], textarea, select, input[type=date]").val("");
        $('#QueuedRemindersContainer').jtable('load');
    });

    $('.order-check').change(function(e) 
    {
        e.preventDefault();
        if ($(this).is(":checked")) { 
            $('.email-check').each(function(index, value) {
              $(this).prop('checked', true);
            });
        } else { 
            $('.email-check').each(function(index, value) {
              $(this).prop('checked', false);
            });
        }
    });

    $.fn.serializeFormJSON = function() {

       var o = {};
       var a = this.serializeArray();
       $.each(a, function() {
         if (o[this.name]) {
           if (!o[this.name].push) {
             o[this.name] = [o[this.name]];
           }
           o[this.name].push(this.value || '');
         } else {
           o[this.name] = this.value || '';
         }
       });
       return o;
     };
});

    $(document).on("click", ".send_mail_task", function(event) {
        event.preventDefault();
        var is_email = $(this).attr('data-email');
        var reminder_id = $(this).attr('reminder_id');
        $.ajax({
            url: '<?php echo base_url(); ?>client/force_send_reminders',
            type: 'POST',
            data: {
                is_email: is_email,
                reminder_id: reminder_id
            },
            dataType: 'json',
            beforeSend: function() {
                $('.LoadingImage').show();
            },
            success: function(data) {
                console.log(data);
                if(data.result)
                {
                    $('.LoadingImage').hide();
                    $('.info-box').find('.info-text').text('Reminder Sent Sccessfully..');
                    $('.info-box').show();

                    
                    setTimeout(function() {
                        $('.info-box').hide();
                        location.reload();
                    }, 1500);
                }
            }
            });
        });
</script>
