<?php $this->load->view('includes/header');?>



<section class="client-details-view various-section03 floating_set">
	<div class="client-inform-data1 floating_set">
		<h2><i class="fa fa-plus fa-6" aria-hidden="true"></i> Add New Project</h2>
		
		<div class="proposal-data1 floating_set">

		<form action="<?php echo base_url()?>projects/add_new" id="add_new_proj" id="add_new_proj" method="post">	
		<div class="floating_set data-float01">	
			<div class="col-sm-6 proposal-form-left">
				<div class="customer-edit01">
				<div class="form-group">
					<label>* Project Name</label>
					<input type="text" name="project_name" id="project_name">
				</div>
				
				<div class="form-group">
					<label>* Customer</label>
					<select class="selectpicker" data-live-search="true" name="client" id="client">
					<?php foreach ($client as $key => $value) { ?>
					<option value="<?php echo $value['id'];?>"><?php echo $value['crm_name']?></option>
					<?php } ?>
					</select>
				</div>	

				<div class="form-group progress-range-project">
					<!-- <div class="calculate-range">
							<input type="checkbox" name="checkbox">
							<label>Calculate progress through tasks</label>
					</div> -->

					<div class="calculate-range checkbox-fade fade-in-primary">
					<label>
					<input type="checkbox" id="checkbox" name="calculate_progress_through_task" value="1">
					<span class="cr">
					<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
					</span>
					<span>Calculate progress through tasks</span>
					</label>
					</div>
					
					<div class="slidecontainer">
					<p>progress: <span id="demo"></span>%</p>
					<input type="range" min="1" max="100" value="0" class="slider" id="myRange" name="progress" >
					</div>	
				</div>		


				
				<div class="full-calendar-time1">

				<div class="form-group">
					<label>* Billing Type</label>
					<select class="selectpicker1" name="billing_type" id="billing_type">
					<option value="fixed_rate">Fixed Rate </option>
					<option value="project_hours">Project Hour</option>
					<option value="task_hours">Task Hour(based on task hourly rate)</option>
					</select>
				</div>	

				<div class="form-group">
					<label>Status</label>
					<select class="selectpicker1" name="project_status" id="project_status">
					<option value="notstarted">Not Started</option>
					<option value="inprogress">In Progress</option>
					<option value="on_hold">On Hold</option>
					<option value="cancelled">Cancelled</option>
					<option value="finished">Finished</option>
					</select>
				</div>	
			</div>

				<div class="form-group">
					<label>Rate Per Hour</label>
					<input type="number" name="rate_per_hour" id="rate_per_hour">
				</div>	

				<div class="full-calendar-time1">

					<div class="form-group">
					<label>Estimated Hours</label>
					<input type="number" name="estimation_hour" id="estimation_hour">
				</div>	

				<div class="form-group">
					<label>Members</label>
					<select class="selectpicker" data-live-search="true" name="members" id="members">
					<?php foreach ($staff as $staff_key => $staff_value) { ?>
					<option value="<?php echo $staff_value['id'];?>"><?php echo $staff_value['crm_name']?></option>
					<?php } ?>
					</select>
				</div>	

				<div class="form-group  date_birth">
				<label>* Start Date</label>
				<div class="form-birth05">
				<span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
				<input class="form-control datepicker fields" type="text" name="start_date" id="date_of_creation1" placeholder="Incorporation date" value=""/>
				</div>
				</div> 
					<div class="form-group  date_birth">
				<label>Deadline</label>
				<div class="form-birth05">
				<span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
				<input class="form-control datepicker fields" type="text" name="deadline" id="date_of_creation2" placeholder="Incorporation date" value=""/>
				</div>
				</div> 

			


				</div>	



				<div class="tags-allow1">
					<h4><i class="fa fa-tags fa-6" aria-hidden="true"></i> Tags</h4>
					<input type="text" value="" name="tags" id="tags" class="tags" />
				</div>	

				<div class="form-group">
					<label>Description</label>
					<textarea id="editor4" name="desc" name="desc"></textarea>
				</div>	

					<div class="form-group project-send1 checkbox-fade fade-in-primary">
					<label>
					<input type="checkbox" id="checkbox" name="send_proj_create_mail" value="1">
					<span class="cr">
					<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
					</span>
					<span>Send project created email</span>
					</label>
					</div>
				</div>
			</div>	<!-- col-sm-6 -->	 

			<div class="col-sm-6 proposal-form-right">
					
					<div class="project-setting02">
						<h2>Project settings</h2>

					<div class="form-group project-send1 checkbox-fade fade-in-primary">
					<label>
					<input type="checkbox" id="checkbox" name="allow_view_task" value="1" checked="checked">
					<span class="cr">
					<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
					</span>
					<span>Allow customer to view tasks</span>
					</label>
					</div>

					<div class="form-group project-send1 checkbox-fade fade-in-primary">
					<label>
					<input type="checkbox" id="checkbox" name="allow_create_task" value="1" checked="checked">
					<span class="cr">
					<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
					</span>
					<span>Allow customer to create tasks</span>
					</label>
					</div>

					<div class="form-group project-send1 checkbox-fade fade-in-primary">
					<label>
					<input type="checkbox" id="checkbox" name="allow_customer_edit_task" value="1" checked="checked">
					<span class="cr">
					<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
					</span>
					<span>Allow customer to edit tasks (only tasks created from contact)</span>
					</label>
					</div>

					<div class="form-group project-send1 checkbox-fade fade-in-primary">
					<label>
					<input type="checkbox" id="checkbox" name="allow_customer_comment_task" value="1" checked="checked">
					<span class="cr">
					<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
					</span>
					<span>Allow customer to comment on project tasks</span>
					</label>
					</div>

					<div class="form-group project-send1 checkbox-fade fade-in-primary">
					<label>
					<input type="checkbox" id="checkbox" name="allow_view_task_comment" value="1" checked="checked">
					<span class="cr">
					<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
					</span>
					<span>Allow customer to view task comments</span>
					</label>
					</div>

					<div class="form-group project-send1 checkbox-fade fade-in-primary">
					<label>
					<input type="checkbox" id="checkbox" name="allow_customer_view_task_attachment" value="1" checked="checked">
					<span class="cr">
					<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
					</span>
					<span>Allow customer to view task attachments</span>
					</label>
					</div>

					<div class="form-group project-send1 checkbox-fade fade-in-primary">
					<label>
					<input type="checkbox" id="checkbox" name="allow_customer_view_task_checklist" value="1" checked="checked">
					<span class="cr">
					<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
					</span>
					<span>Allow customer to view task checklist items</span>
					</label>
					</div>

					<div class="form-group project-send1 checkbox-fade fade-in-primary">
					<label>
					<input type="checkbox" id="checkbox" name="allow_customer_upload_attachments" value="1" checked="checked">
					<span class="cr">
					<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
					</span>
					<span>Allow customer to upload attachments on tasks</span>
					</label>
					</div>

					<div class="form-group project-send1 checkbox-fade fade-in-primary">
					<label>
					<input type="checkbox" id="checkbox" name="allow_customer_to_view_total_logged_time" value="1" checked="checked">
					<span class="cr">
					<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
					</span>
					<span>Allow customer to view task total logged time</span>
					</label>
					</div>

					<div class="form-group project-send1 checkbox-fade fade-in-primary">
					<label>
					<input type="checkbox" id="checkbox" name="allow_customer_view_finance" value="1" checked="checked">
					<span class="cr">
					<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
					</span>
					<span>Allow customer to view finance overview</span>
					</label>
					</div>

					<div class="form-group project-send1 checkbox-fade fade-in-primary">
					<label>
					<input type="checkbox" id="checkbox" name="all_customer_upload_file" value="1" checked="checked">
					<span class="cr">
					<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
					</span>
					<span>Allow customer to upload files</span>
					</label>
					</div>

					<div class="form-group project-send1 checkbox-fade fade-in-primary">
					<label>
					<input type="checkbox" id="checkbox" name="allow_customer_open_discussion" value="1" checked="checked">
					<span class="cr">
					<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
					</span>
					<span>Allow customer to open discussions</span>
					</label>
					</div>

					<div class="form-group project-send1 checkbox-fade fade-in-primary">
					<label>
					<input type="checkbox" id="checkbox" name="allow_customer_view_milestone" value="1" checked="checked">
					<span class="cr">
					<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
					</span>
					<span>Allow customer to view milestones</span>
					</label>
					</div>

					<div class="form-group project-send1 checkbox-fade fade-in-primary">
					<label>
					<input type="checkbox" id="checkbox" name="allow_customer_view_gantt" value="1"  checked="checked">
					<span class="cr">
					<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
					</span>
					<span>Allow customer to view Gantt</span>
					</label>
					</div>

					<div class="form-group project-send1 checkbox-fade fade-in-primary">
					<label>
					<input type="checkbox" id="checkbox" name="allow_customer_view_timesheet" value="1" checked="checked">
					<span class="cr">
					<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
					</span>
					<span>Allow customer to view timesheets</span>
					</label>
					</div>

					<div class="form-group project-send1 checkbox-fade fade-in-primary">
					<label>
					<input type="checkbox" id="checkbox" name="allow_customer_view_activitylog" value="1" checked="checked">
					<span class="cr">
					<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
					</span>
					<span>Allow customer to view activity log</span>
					</label>
					</div>

					<div class="form-group project-send1 checkbox-fade fade-in-primary">
					<label>
					<input type="checkbox" id="checkbox" name="allow_customer_view_team_member" value="1" checked="checked">
					<span class="cr">
					<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
					</span>
					<span>Allow customer to view team members</span>
					</label>
					</div>


					<div class="form-group project-send1 checkbox-fade fade-in-primary">
					<label>
					<input type="checkbox" id="checkbox" name="hide_project_task" value="1">
					<span class="cr">
					<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
					</span>
					<span>Hide project tasks on main tasks table (admin area)</span>
					</label>
					</div>


			</div>		



		</div>
		</div>	


		<div class="form-submit-bt01 text-right">
				<!-- <button type="button" name="button">Save & Send</button> -->
				<button type="submit" name="button">Save</button>
		</div>	






				</form>
				</div>
					
					
			
	
</div>
</section>			


<?php $this->load->view('includes/footer');?>

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
 <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>


<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript" src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
	$('.tags').tagsinput({
    	allowDuplicates: true
    });
    
    $('.tags').on('itemAdded', function(item, tag) {
        $('.items').html('');
		var tags = $('.tags').tagsinput('items');
      
    });


    $( function() {
    $('#date_of_creation1,#date_of_creation2').datepicker();
  } );

		var slider = document.getElementById("myRange");
		var output = document.getElementById("demo");
		output.innerHTML = slider.value;

		slider.oninput = function() {
		output.innerHTML = this.value;
		}

		CKEDITOR.replace('editor4');


			$('#checkbox').click(function() {
    
(($(this).is(':checked')) ? $('.slidecontainer').show():$('.slidecontainer').hide());

});

$(".slidecontainer").hide();
});

});



</script>