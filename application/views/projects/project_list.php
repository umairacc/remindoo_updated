<?php $this->load->view('includes/header');
        $role = $this->Common_mdl->getRole($_SESSION['id']);

?>



<style>
/*span.newonoff {
    background: #4680ff;
    padding: 6px 15px;
    display: inline-block;
    color: #fff;
    border-radius: 5px;
}*/
button.btn.btn-info.btn-lg.newonoff {
    padding: 3px 10px;
    height: initial;
    font-size: 15px;
    border-radius: 5px;
}
img.user_imgs {
    width: 38px;
    height: 38px;
    border-radius: 50%;
    display: inline-block;
}
</style>
                    <div class="pcoded-content">

                        <div class="pcoded-inner-content">

                            <!-- Main-body start -->

                            <div class="main-body">

                                <div class="page-wrapper">

                                   



                                    <!-- Page body start -->

                                    <div class="page-body">

                                        <div class="row">

                                            <div class="col-sm-12">

                                                <!-- Register your self card start -->

                                                <div class="card">
	<!-- admin start-->
<div class="client_section col-xs-12 floating_set">
	<!-- <div class="all-clients floating_set">
	  <div class="tab_section_01 floating_set">
	<ul class="client_tab">
		  <li class="active"><a href="<?php echo  base_url()?>user">All Clients</a></li>
		  <li><a href="<?php echo  base_url()?>client/addnewclient">Add new client</a></li>
</ul>	

<ul class="client_tab">
	<li><a href="#">Email All</a></li>
	<li><a href="#">filters</a></li>
</ul> 
</div> 
	
	<div class="text-left search_section1 floating_set">
		<div class="search_box01">
	
		<div class="pcoded-search">

                                <span class="searchbar-toggle">  </span>

                                <div class="pcoded-search-box ">

                                    <input type="text" placeholder="Search">

                                    <span class="search-icon"><i class="ti-search" aria-hidden="true"></i></span>

                                </div>

                            </div>
		
		</div>
</div>



</div> <!-- all-clients -->

	<div class="all_user-section floating_set">
	<!-- 	
<ul class="nav nav-tabs all_user1 md-tabs floating_set">
<li class="nav-item">
<a class="nav-link active" data-toggle="tab" href="#alltasks">All Tasks</a><div class="slide"></div></li>
<li class="nav-item">
<a class="nav-link" data-toggle="tab" href="#inprogress">In Progress</a><div class="slide"></div></li>
<li class="nav-item">
<a class="nav-link" data-toggle="tab" href="#starting">Started</a><div class="slide"></div></li> 
<li class="nav-item">
<a class="nav-link" data-toggle="tab" href="#notstarting">Not Started</a><div class="slide"></div></li> 
</ul>	 -->

	<div class="all_user-section2 floating_set">
	<div class="tab-content">
		<div id="alltasks" class="tab-pane fade in active">

			
			<div class="count-value1 floating_set">
				<!--<ul class="pull-left">
					 <li><select><option>1</option><option>2</option><option>3</option></select></li> -->
					<!-- <li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li> -->
					<!-- <li><a href="#"><i class="fa fa-print fa-6" aria-hidden																																																											="true"></i><span>selected</span></a></li>
					<li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-print fa-6" aria-hidden="true"></i></a></li>
				</ul>	 -->
				
				<!-- <ul class="pull-right">
					<form>
						<label>Search all columns</label>
						<input type="text" name="text">
					</form>
				</ul> -->	
				<!-- <ul class="select-filter" id="select-filter">
			
			</ul> -->
		<!-- 	<select id="dropdown1" name="dropdown1">
    <option value="">--All--</option>
  
                              <option value="today">Today Task</option>
                              <option value="due">Due date passed</option>
                              <option value="upcoming">Upcoming task</option>           
</select> -->

	<select id="dropdown2" >
    <option value="">--All--</option>
  
                              <option value="notstarted">Not started</option>
                              <option value="inprogress">In Progress</option>
                              <option value="awaiting">Awaiting Feedback</option>
                              <option value="testing">Testing</option>
                              <option value="complete">Complete</option>
                              <option value="today_task">Today's Task</option>
                              <option value="due_date_passed">Due Date Passed</option>
                              <option value="upcoming_task">Upcoming Task</option>
                              <option value="assinged_to_me">Task Assigned to me</option>
                              <option value="not_assigned">Not assigned</option>
                              <!-- <option value="assinged_im_followin">Tasks I'm Following</option>
                              <option value="recuring">Recuring</option>
                              <option value="billable">Billabe</option>
                              <option value="billed">Billed</option>
                              <option value="not_billed">Not Billed</option> -->
                          
</select>
<button id="deleteTriger" class="deleteTri" style="display:none;">Delete</button>

			</div>

			<a href="<?php echo base_url()?>projects/add_new_project" class="btn btn-info pull-left display-block">
                New Project              </a>	
			
			<div class="client_section3 table-responsive floating_set">

		
<?php 
		$notstarted = $this->Common_mdl->GetAllWithWhere('projects','status','notstarted');
        $inprogress = $this->Common_mdl->GetAllWithWhere('projects','status','inprogress');
        $on_hold = $this->Common_mdl->GetAllWithWhere('projects','status','on_hold');
        $cancelled = $this->Common_mdl->GetAllWithWhere('projects','status','cancelled');
        $finished = $this->Common_mdl->GetAllWithWhere('projects','status','finished');
        
        $role = $this->Common_mdl->getRole($_SESSION['id']);
        //print_r($task_list);

        $assignNotstarted = 0;
        $assignInprogress = 0;
        $assign_on_hold = 0;
        $assigncancelled = 0;
        $assignfinished = 0;

		foreach ($projects as $key => $value) {
			$staffs = $value['members'];

			$explode_worker=explode(',',$staffs);

			if($value['status']=='notstarted') {
				if(in_array( $_SESSION['id'] ,$explode_worker )){
					$assignNotstarted++;
				}
			}
			if($value['status']=='inprogress') {
				if(in_array( $_SESSION['id'] ,$explode_worker )){
				$assignInprogress++;
				}
			}
			if($value['status']=='on_hold') {
				if(in_array( $_SESSION['id'] ,$explode_worker )){
				$assign_on_hold++;
				}
			}
			if($value['status']=='cancelled') {
				if(in_array( $_SESSION['id'] ,$explode_worker )){
				$assigncancelled++;
				}
			}
			if($value['status']=='finished') {
				if(in_array( $_SESSION['id'] ,$explode_worker )){
				$assignfinished++;
				}
			}

		}
       
?>
<div class="lead-summary1 floating_set task-summary-07">
		<h2>Project Summary</h2>
		<div class="lead-data1 floating_set">
			<div class="junk-lead">
					<div class="lead-point1">
					<span><?php echo count($notstarted);?></span>
					<strong class="tsk-color1">Not stated</strong>
					<?php if($role=='Staff'){?>
					<p>Task assigned to me: <?php echo $assignNotstarted;?></p>
					<?php } ?>
					</div>
					
				</div>	
				
				<div class="junk-lead">
					<div class="lead-point1">
					<span><?php echo count($inprogress);?></span>
					<strong  class="tsk-color2">In Progress</strong>
					<?php if($role=='Staff'){?>
					<p>Task assigned to me: <?php echo $assignInprogress;?></p>
					<?php } ?>
					</div>
				</div>	
				
				<div class="junk-lead">
					<div class="lead-point1">
					<span><?php echo count($on_hold);?></span>
					<strong  class="tsk-color3">On hold</strong>
					<?php if($role=='Staff'){?>
					<p>Task assigned to me: <?php echo $assign_on_hold;?></p>
					<?php } ?>
					</div>
				</div>	
				
				<div class="junk-lead color-junk1">
					<div class="lead-point1">
					<span><?php echo count($cancelled);?></span>
					<strong  class="tsk-color4">Cancelled</strong>
					<?php if($role=='Staff'){?>
					<p>Task assigned to me: <?php echo $assigncancelled;?></p>
					<?php } ?>
					</div>
				</div>

				<div class="junk-lead color-junk2">
					<div class="lead-point1">
					<span><?php echo count($finished);?></span>
					<strong  class="tsk-color5">Finished</strong>
					<?php if($role=='Staff'){?>
					<p>Task assigned to me: <?php echo $assignfinished;?></p>
					<?php } ?>
					</div>
				</div>
		
				
			</div>
		</div>

				<div id="status_succ"></div>
				
			<div class="all-usera1">				
				<table class="table client_table1 text-center display nowrap" id="alltask" cellspacing="0" width="100%">
					<thead>
						<tr class="text-uppercase">
							<th>#</th>
							<th>Project Name</th>
							<th>Customer</th>
							<th>Tag</th>
							<th>Start Date</th>
							<th>Deadline</th>
							<th>Assignto</th>
							<th>Billing Type</th>
							<th>Status</th>
							<th>Options</th>
						</tr>	
					</thead>

					<tbody>
                        <?php foreach ($projects as $key => $value) {
                       $staff=$this->db->query("SELECT * FROM user WHERE id in (".$value['members'].")")->result_array();
                     
                       $customer_name = $this->Common_mdl->getUsername($value['customer_id']);
                        if($value['status']=='notstarted')
                      {
                      	$stat = 'Not Started';
                      }else if($value['status']=='inprogress')
                      {
                      	$stat = 'In Progress';
                      }else if($value['status']=='on_hold')
                      {
                      	$stat = 'On hold';
                      }else if($value['status']=='cancelled')
                      {
                      	$stat = 'Cancelled';
                      }else if($value['status']='finished')
                      {
                      	$stat = 'Finished';
                      }

                      if($value['billing_type']='fixed_rate')
                      {
                      	$billing_type = 'Fixed Rate';
                      }else if($value['billing_type']='project_hours')
                      {
                      	$billing_type = 'Fixed Project Hours';
                      }else if($value['billing_type']='task_hours')
                      {
                      	$billing_type = 'Task Hours Rate';
                      }
                      $exp_tag = explode(',', $value['tag']);
                      $members = explode(',', $value['members']);
                    
                      ?>
						<tr id="<?php echo $value['id']; ?>">
							
							<td><?php echo $value['id'];?></td>
							<td><?php echo $value['project_name'];?></td>
							<td><?php echo $customer_name['crm_name'];?></td>
							
							
							<td>
								<?php  foreach ($exp_tag as $exp_tag_key => $exp_tag_value) {
                      	    echo $exp_tag_value.' ';
                      			}?>
							</td>
							<td><?php echo $value['start_date'];?></td>
							<td><?php echo $value['deadline'];?></td>
							
							<td class="user_imgs">
							<?php
                             foreach($members as $key => $val_id){   
                             $getUserProfilepic = $this->Common_mdl->getUserProfilepic($val_id); 
							?>
							<img src="<?php echo $getUserProfilepic;?>" alt="img">
							<?php } ?>
							</td>
							<td><?php echo $billing_type;?></td>
                            <td><?php echo $stat;?></td>
							<td>
							<p class="action_01">
							<?php if($role!='Staff'){?>	
								<a href="<?php echo base_url().'projects/delete/'.$value['id'];?>" onclick="return confirm('Are you sure want to delete');"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>
								<?php } ?>
								<a href="<?php echo base_url().'projects/update/'.$value['id'];?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
								<!-- <a href="#"><i class="fa fa-inbox fa-6" aria-hidden="true"></i></a> -->
							</p>
							</td>
						</tr>
					<?php } ?>
					</tbody>		
							</table>
</div>	
					</div>	
									
		</div> <!-- home-->
		
	

		

		

			
		
	
	</div>
	

</div> 

<!-- admin close -->

                                                </div>

                                                <!-- Register your self card end -->

                                            </div>

                                        </div>

                                    </div>

                                    <!-- Page body end -->

                                </div>

                            </div>

                            <!-- Main-body end -->



                            <div id="styleSelector">



                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>







    <!-- Warning Section Starts -->

    <!-- Older IE warning message -->

    <!--[if lt IE 10]>

<div class="ie-warning">

    <h1>Warning!!</h1>

    <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>

    <div class="iew-container">

        <ul class="iew-download">

            <li>

                <a href="http://www.google.com/chrome/">

                    <img src="assets/images/browser/chrome.png" alt="Chrome">

                    <div>Chrome</div>

                </a>

            </li>

            <li>

                <a href="https://www.mozilla.org/en-US/firefox/new/">

                    <img src="assets/images/browser/firefox.png" alt="Firefox">

                    <div>Firefox</div>

                </a>

            </li>

            <li>

                <a href="http://www.opera.com">

                    <img src="assets/images/browser/opera.png" alt="Opera">

                    <div>Opera</div>

                </a>

            </li>

            <li>

                <a href="https://www.apple.com/safari/">

                    <img src="assets/images/browser/safari.png" alt="Safari">

                    <div>Safari</div>

                </a>

            </li>

            <li>

                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">

                    <img src="assets/images/browser/ie.png" alt="">

                    <div>IE (9 & above)</div>

                </a>

            </li>

        </ul>

    </div>

    <p>Sorry for the inconvenience!</p>

</div>

<![endif]-->

    <!-- Warning Section Ends -->
    <?php
  $data = $this->db->query("SELECT * FROM update_client")->result_array();
  foreach($data as $row) { ?>
   <!-- Modal -->
   
<div id="myModal<?php echo $row['user_id'];?>" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">CRM-Updated fields</h4>
      </div>
      <div class="modal-body">
      	

      	<?php if(isset($row['company_name'])&&($row['company_name']!='0')){?>
        <p>Company Name: <span><?php echo $row['company_name'];?></span></p>
        <?php } ?> 

        <?php if(isset($row['company_url'])&&($row['company_url']!='0')){?>
        <p>Company URL: <span><?php echo $row['company_url'];?></span></p>
        <?php } ?>
        <?php if(isset($row['officers_url'])&&($row['officers_url']!='0')){?>
        <p>Officers URL: <span><?php echo $row['officers_url'];?></span></p>
        <?php } ?>
        <?php if(isset($row['incorporation_date'])&&($row['incorporation_date']!='0')){?>
        <p>Incorporation Date: <span><?php echo $row['incorporation_date'];?></span></p>
        <?php } ?>
        <?php if(isset($row['register_address'])&&($row['register_address']!='0')){?>
        <p>Registered Address: <span><?php echo $row['register_address'];?></span></p>
        <?php }  ?>
        <?php if(isset($row['company_status'])&&($row['company_status']!='0')){?>
        <p>Company Status: <span><?php echo $row['company_status'];?></span></p>
        <?php }  ?>
        <?php if(isset($row['company_type'])&&($row['company_type']!='0')){?>
        <p>Company Type: <span><?php echo $row['company_type'];?></span></p>
        <?php } ?>
        <?php if(isset($row['accounts_periodend'])&&($row['accounts_periodend']!='0')){?>
        <p>Accounts Period End: <span><?php echo $row['accounts_periodend'];?></span></p>
        <?php } ?>
        <?php if(isset($row['hmrc_yearend'])&&($row['hmrc_yearend']!='0')){?>
        <p>HMRC Year End: <span><?php echo $row['hmrc_yearend'];?></span></p>
        <?php } ?>
        <?php if(isset($row['ch_accounts_next_due'])&&($row['ch_accounts_next_due']!='0')){?>
        <p>CH Accounts Next Due: <span><?php echo $row['ch_accounts_next_due'];?></span></p>
        <?php } ?>
        <?php if(isset($row['confirmation_statement_date'])&&($row['confirmation_statement_date']!='0')){?>
        <p>Confirmation Statement Date: <span><?php echo $row['confirmation_statement_date'];?></span></p>
        <?php } ?>
        <?php if(isset($row['confirmation_statement_due_date'])&&($row['confirmation_statement_due_date']!='0')){?>
        <p>Confirmation Statement Due: <span><?php echo $row['confirmation_statement_due_date'];?></span></p>
        <?php } ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div><!-- /.modal -->
<?php } ?>

<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>


<!-- <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap.js"></script> -->
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js">
	</script>
 

  <script>
  $(document).ready(function() {
	
	
    $("#alltask").dataTable({
       "iDisplayLength": 10,
	   "scrollX": true,
	  /* "processing":true,
	  "serverSide": true,
	    "ajax": {
            "url": "<?php echo site_url('user/ajax_list')?>",
            "type": "POST",
            "data": function ( data ) {
                data.today = $('#dropdown1').val();

            }
       },*/
 
	 /*  initComplete: function () {
            this.api().columns(5).every( function () {
                var column = this;

                 // Get the header name for this column
    /*var headerName = 'Status';
    // generate the id name for the element to append to.
    var appendHere = '#' + headerName + '-select-filter';*/

             /*   var select = $('<select><option value="">All</option></select>')
                    .appendTo( $('#select-filter').empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }*/
    });
 $("#inprogresses").dataTable({
       "iDisplayLength": 10,
    });
 $("#notstarted").dataTable({
       "iDisplayLength": 10,
    });
 $("#started").dataTable({
       "iDisplayLength": 10,
    });
 


 //$(".status").change(function(e){
 	$('#alluser').on('change','.status',function () {
//e.preventDefault();
 	

   var rec_id = $(this).data('id');
   var stat = $(this).val();
  
  $.ajax({
        url: '<?php echo base_url();?>user/statusChange/',
        type: 'post',
        data: { 'rec_id':rec_id,'status':stat },
        timeout: 3000,
        success: function( data ){
        	//alert('ggg');
            $("#status_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! User status have been changed successfully...</span></div></div></div>').hide().fadeIn(1500, function() { $('#status_succ'); });
            setTimeout(resetAll,3000);
            if(stat=='3'){
            	
            	 //$this.closest('td').next('td').html('Active');
            	 $('#frozen'+rec_id).html('Frozen');

            } else {
            	 $this.closest('td').next('td').html('Inactive');
            }
            },
            error: function( errorThrown ){
                console.log( errorThrown );
            }
        });
   });

// payment/non payment status
$('.suspent').click(function() {
    if($(this).is(':checked'))
        var stat = '1';
    else
        var stat = '0';
    var rec_id = $(this).val();
    var $this = $(this);
     $.ajax({
        url: '<?php echo base_url();?>user/suspentChange/',
        type: 'post',
        data: { 'rec_id':rec_id,'status':stat },
        success: function( data ){
        	//alert('ggg');
            $("#suspent_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! User status have been changed successfully...</span></div></div></div>');
            if(stat=='1'){
            	
            	 $this.closest('td').next('td').html('Payment');

            } else {
            	 $this.closest('td').next('td').html('Non payment');
            }
            },
            error: function( errorThrown ){
                console.log( errorThrown );
            }
        });
});
});

  </script>
<script>
  $(document).ready(function() {
   var table =  $('#alltask').DataTable();
    
     $('#dropdown2').on('change', function () {
                   // table.columns(5).search( this.value ).draw();
                   var filterstatus = $(this).val();
                   $.ajax({
  					type: "POST",
  					url: "<?php echo base_url();?>tasksummary/taskFilter",
  					data: {filterstatus:filterstatus},
  					success: function(response) {

  						$(".all-usera1").html(response);
   
  					},
  					//async:false,
  				});
                });

     /*$('#dropdown1').on('change', function () {
                   // table.columns(2).search( this.value ).draw();
                   
                } );*/

               
});

  </script>
  <script>
   $(document).ready(function() {
  $("#bulkDelete").on('click',function() { // bulk checked
  			var status = this.checked;

  			$(".deleteRow").each( function() {
  				$(this).prop("checked",status);

  			});
        if(status==true)
        {
          $('#deleteTriger').show();
        } else {
          $('#deleteTriger').hide();
        }
  		});

  $(".deleteRow").on('click',function() { // bulk checked
  
        var status = this.checked;

        if(status==true)
        {
          $('#deleteTriger').show();
        } else {
          $('#deleteTriger').hide();
        }
      });
  		
  		$('#deleteTriger').on("click", function(event){ // triggering delete one by one
       
  			if( $('.deleteRow:checked').length > 0 ){  // at-least one checkbox checked
  				var ids = [];
  				$('.deleteRow').each(function(){
  					if($(this).is(':checked')) { 
  						ids.push($(this).val());
  					}
  				});
  				var ids_string = ids.toString();  // array to string conversion 
  				$.ajax({
  					type: "POST",
  					url: "<?php echo base_url();?>user/task_delete/",
  					data: {data_ids:ids_string},
  					success: function(response) {
  						//dataTable.draw(); // redrawing datatable
  						var emp_ids = response.split(",");
   for (var i=0; i < emp_ids.length; i++ ) {
  
   	$("#"+emp_ids[i]).remove(); 
   }
   
  					},
  					//async:false,
  				});
  			}
  		});

});
</script>
