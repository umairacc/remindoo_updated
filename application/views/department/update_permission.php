<?php $this->load->view('includes/header');
$id = $this->uri->segment(3);
//echo $id;die;
$exp_permission = $permissions['permissions'];
  $exp_permissions=explode(',',$exp_permission);
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/responsive.dataTables.css">
<style>
   /*span.newonoff {
   background: #4680ff;
   padding: 6px 15px;
   display: inline-block;
   color: #fff;
   border-radius: 5px;
   }*/
   button.btn.btn-info.btn-lg.newonoff {
   padding: 3px 10px;
   height: initial;
   font-size: 15px;
   border-radius: 5px;
   }
   li.ui-state-default.ui-sortable-handle.ui-sortable-helper
   {
   /*top: 0px !important;*/
   }
   body
   {
   overflow-x:hidden; 
   }
   /*.card.firm-field {
   padding: 30px;
   }*/
   .invoice-details {
   margin-top: 30px;
   }
</style>
<?php 
$res=$this->db->query("select * from department_assign_team where depart_id=".$permissions['id']." ")->row_array();
if(count($res)>0)
{
   $update=$res['id'];
   $button="assignupdate";
}
else
{
   $update='';
   $button="assign";
}

?>
<!-- management block -->
<div class="pcoded-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body main-display">
         <div class="page-wrapper notabs-01">
            <div class="firm-field">
               <form name="department_permissions" id="department_permissions" method="post">

               <div class="deadline-crm1 floating_set">
                    <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                  <h4>Change department Permissions</h4>
                  </ul>

                  <div class="save-addmore">
                     <input type="button" id="save_btn" data-id="create"  value="save">
                     <?php if($button=='assign'){ ?>
                     <input type="button" id="save_btn_new" data-id="assign"  value="Assign Team">
                     <?php }
                     else{ ?>
                     <input type="button" id="save_btn_new" data-id="update"  value="Assign Team" class="assign_ts">
                    <?php } ?>
                  </div>
               </div>

               <div class="department-table">
                  <div class="">
                     <!--  <div class="form-titles"><a href="#" class="waves-effect" data-toggle="modal" data-target="#default-Modal"><h2>View on Companies House</h2></a></div> -->
                     <!--  <form> -->
					 <div class="row">
                    <!--  <div class="form-group row name_fields col-sm-6">
                        <label class="col-sm-4 col-form-label">select department</label>
                        <?php //echo '<pre>';
                        //print_r($department);die;
                        ?>
                        <div class="col-sm-8">
                           <select name="select_department" class="form-control fields" id="select_department">
                           
                              <?php foreach ($department as $departmentkey => $departmentvalue) {
                              
                               ?>
                              <option value="<?php echo $departmentvalue['id'];?>" data-id="<?php echo $departmentvalue['id'];?>" <?php if($permissions['dept_id']==$departmentvalue['id']){ echo "selected='selected'"; }?>><?php echo $departmentvalue['department'];?></option> 
                              <?php }?>
                              <option value="new_dept" data-id="0"  <?php if($permissions['dept_id']=='0'){ echo 'selected'; }?>>New Department</option>
                           </select>
                        </div>
                     </div> -->
                     <?php //if($permissions['dept_id']=='0'){?>
                     <div class="form-group new_Department col-sm-6">
                        <label class="col-sm-4 col-form-label">Department</label>
                        <div class="col-sm-8">
                           <input type="text" name="new_department" id="new_department" value="<?php echo $permissions['new_dept'];?>" class="fields"> 
                           <label class="error_msg error"></label>
                           <label class="error_msg_validate error"></label>
                        </div>
                     </div>
                     <?php //} ?>
                     <!-- <div class="form-group row col-sm-6">
                        <label class="col-sm-4 col-form-label">designation</label>
                        <div class="col-sm-8">
                           <input type="text" name="designation" id="designation" value="<?php echo $permissions['designation'];?>" class="fields">
                        </div>
                     </div> -->
					 </div>
                  </div>
                  <!-- <div class="table-responsive"> -->
				  <!-- ccess permision -->
     <?php 
              /** hint **/
              // key with empty value is heading ex:"CRM"=>""
              // key with (without - same line) ex:"Dashboard"=>"dashboard"
              // key with (with - its sub value) ex:"Admin Settings"=>"settings-admin_settings"

    $all_pages=array("Dashboard"=>"dashboard","Task Section"=>"","Task"=>"task","Task Create"=>"task-create","CRM"=>"","Leads"=>"crm-leads","Leads Create"=>"crm-leads_create","Webtolead"=>"crm-webtolead","Webtolead Create"=>"crm-webtolead_create","Proposal"=>"","Dashboad"=>"proposal-dashboad","Create Proposal"=>"proposal-create","Catalog"=>"proposal-catalog","Template"=>"proposal-template","Setting"=>"proposal-settings","Client"=>"","Client Section"=>"client-client","Add Client"=>"client-addclient","Add From Company House"=>"client-add_from_company_house","Import Client"=>"client-import_client","Services Timeline"=>"client-services_timeline","Deadline"=>"","Deadline manager"=>"deadline-deadline","Report"=>"","Reports"=>"report-reports","Settings"=>"","Admin Settings"=>"settings-admin_settings","Firm Settings"=>"settings-firm_settings","Column Settings"=>"settings-column_settings","Team and Management"=>"settings-team_and_management","Staff Custom Firmlist"=>"settings-staff_custom_firmlist","Tickets"=>"settings-tickets","Chat"=>"settings-chat","Document"=>"","Documents"=>"documents","Document Create"=>"document-create","Invoice"=>"","Invoices"=>"invoice","Invoices Create"=>"invoice-create");

               $all_pages_section=array("Dashboard"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Task"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Task Create"=>array("view"=>"","add"=>"add","edit"=>"","delete"=>""),"Leads"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Leads Create"=>array("view"=>"","add"=>"add","edit"=>"","delete"=>""),"Webtolead"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Webtolead Create"=>array("view"=>"","add"=>"add","edit"=>"","delete"=>""),"Dashboad"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Create Proposal"=>array("view"=>"","add"=>"add","edit"=>"","delete"=>""),"Catalog"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Template"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Setting"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Client Section"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Add Client"=>array("view"=>"","add"=>"add","edit"=>"","delete"=>""),"Add From Company House"=>array("view"=>"","add"=>"add","edit"=>"","delete"=>""),"Import Client"=>array("view"=>"","add"=>"add","edit"=>"","delete"=>""),"Services Timeline"=>array("view"=>"view","add"=>"","edit"=>"","delete"=>""),"Deadline manager"=>array("view"=>"view","add"=>"","edit"=>"","delete"=>""),"Reports"=>array("view"=>"view","add"=>"","edit"=>"","delete"=>""),"Admin Settings"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Firm Settings"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Column Settings"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Team and Management"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Staff Custom Firmlist"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Tickets"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Chat"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Documents"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Document Create"=>array("view"=>"","add"=>"add","edit"=>"","delete"=>""),"Invoices"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Invoices Create"=>array("view"=>"","add"=>"add","edit"=>"","delete"=>""));

          $db_access_permission=$this->db->query("select * from department_access_permission where department_id=".$permissions['id']." order by id asc ")->result_array();
           // echo "<pre>";
           // print_r($db_access_permission);
              ?>
              <div class="dept-checkbox-danger permiss_start_table">
                  <table class="permission-table" id="permission-table">
                  <thead>
                        <tr>
                           <th>
                              <div class="checkbox-fade fade-in-primary">
                                 <label class="custom_checkbox1">
                                 <input type="checkbox" id="selectall" value="permission" level="parent">
                                <!--  <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>   -->
                                <i></i>
                                 </label>
                              </div>
                              <label>permissions</label>
                           </th>
                           <th>
                              <div class="checkbox-fade fade-in-primary">
                                 <label class="custom_checkbox1">
                                 <input type="checkbox" id="view_all" value="permission_view">
                                 <!-- <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>   -->
                                 <i></i>
                                 </label>
                              </div>
                              <label>view</label>
                           </th>
                           <th>
                              <div class="checkbox-fade fade-in-primary">
                                 <label class="custom_checkbox1">
                                 <input type="checkbox" id="create_all" value="permission_create">
                                 <!-- <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>   -->
                                 <i></i>
                                 </label>
                              </div>
                              <label>create</label>
                           </th>
                           <th>
                              <div class="checkbox-fade fade-in-primary">
                                 <label class="custom_checkbox1">
                                 <input type="checkbox" id="edit_all" value="permission_edit">
                                <!--  <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>   -->
                                 <i></i></label>
                              </div>
                              <label>edit</label>
                           </th>
                           <th>
                              <div class="checkbox-fade fade-in-primary">
                                 <label class="custom_checkbox1">
                                 <input type="checkbox" id="delete_all" value="permission_delete">
                                 <!-- <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>   -->
                                 <i></i></label>
                              </div>
                              <label>delete</label>
                           </th>
                        </tr>
                        </thead>
                     <tbody>
                     <?php 
                     $i=0;
                     foreach ($all_pages as $page_key => $page_value) {
                      ?>
<div class="rspt">

</div>
                      <?php
                        if($page_value=='')
                        { 
                          // echo $page_value;
                           ?>
                        <tr>
                           <td colspan="5">
                              <label class="permission_heading"><h6><?php echo $page_key;?></h6></label>
                           </td>
                                                     
                        </tr>
                        <?php }
                        else{
                           $page_explode=explode('-', $page_value);
                      // echo $page_value;
                           $its_view_delete=$all_pages_section[$page_key];
                           $its_view=$its_view_delete['view'];
                           $its_add=$its_view_delete['add'];
                           $its_edit=$its_view_delete['edit'];
                           $its_delete=$its_view_delete['delete'];
                           ?>
                               <tr>
                           <td>
                              <div class="checkbox-fade fade-in-primary">
                                 <label class="custom_checkbox1">
                                 <input type="checkbox" id="bulkall" class="dashboard_check checkboxall bulkall" value="dashboard" level="parent">
                                 <!-- <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>   -->
                                 <i></i></label>
                              </div>
                              <?php 
                         if(count($page_explode)>1)
                           {
                              ?><label><?php echo $page_key; ?></label><?php
                           }else
                           {
                              ?><label><h6><?php echo $page_key; ?></h6></label><?php
                           }
                              ?>
                              <!-- <label><?php echo $page_key; ?></label> -->
                           </td>
                           <td>
                              <div class="checkbox-fade fade-in-primary">
                           <?php if($its_view!=''){ ?>
                                 <label class="custom_checkbox1">
                                 <input type="checkbox" id="bulkviews" class="dashboard_check viewall bulkview"  name="bulkview[]" value="<?php echo $page_value."-view"; ?>" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['view']==1){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-view";echo "checked"; } ?> >
                                 
                              <!--    <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>   -->
                                 <i></i></label>
                                  <input type="hidden" id="for_bulkviews" class="viewall for_bulkview"  name="for_bulkview[]" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['view']==1){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-view"; ?> value="<?php echo $val;?>" <?php }else { ?> value="<?php echo $page_value."-view//unchecked"; ?>" <?php } ?> >
                            <?php }else{ ?>
                                  <input type="hidden" id="for_bulkviews" class="viewall for_bulkviews"  name="for_bulkview[]" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['view']==1){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-view"; ?> value="<?php echo $val;?>" <?php }else { ?> value="<?php echo $page_value."-view//unchecked"; ?>" <?php } ?> >
                            <?php } ?>
                              </div>
                           </td>
                           <td> <div class="checkbox-fade fade-in-primary">
                           <?php if($its_add!=''){ ?>
                                 <label class="custom_checkbox1">
                                 <input type="checkbox" id="bulkcreates" class="dashboard_check viewall bulkcreate" name="bulkcreates[]" value="<?php echo $page_value."-create"; ?>" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['create']==1){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-create";echo "checked"; } ?> >
                               
                                 <!-- <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>   -->
                                 <i></i>
                                 </label>
                                   <input type="hidden" id="for_bulkcreates" class="viewall for_bulkcreate" name="for_bulkcreates[]" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['create']==1){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-create"; ?> value="<?php echo $val;?>" <?php }else { ?> value="<?php echo $page_value."-create//unchecked"; ?>" <?php } ?> >
                            <?php }else{ ?>
                                  <input type="hidden" id="for_bulkcreates" class="viewall for_bulkcreates" name="for_bulkcreates[]" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['create']==1){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-create"; ?> value="<?php echo $val;?>" <?php }else { ?> value="<?php echo $page_value."-create//unchecked"; ?>" <?php } ?> >
                            <?php  } ?>
                              </div></td>
                           <td> <div class="checkbox-fade fade-in-primary">
                            <?php if($its_edit!=''){ ?>
                                 <label class="custom_checkbox1">
                                 <input type="checkbox" id="bulkedits" class="dashboard_check viewall bulkedit" name="bulkedit[]" value="<?php echo $page_value."-edit"; ?>" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['edit']==1){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-edit";echo "checked"; } ?> >
                                
                                 <!-- <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>   -->
                                 <i></i>
                                 </label>
                                   <input type="hidden" id="for_bulkedits" class="viewall for_bulkedit" name="for_bulkedit[]" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['edit']==1){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-edit"; ?> value="<?php echo $val;?>" <?php }else { ?> value="<?php echo $page_value."-edit//unchecked"; ?>" <?php } ?> >
                            <?php }else{ ?>
                                <input type="hidden" id="for_bulkedits" class="viewall for_bulkedits" name="for_bulkedit[]" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['edit']==1){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-edit"; ?> value="<?php echo $val;?>" <?php }else { ?> value="<?php echo $page_value."-edit//unchecked"; ?>" <?php } ?> >
                            <?php  } ?>
                              </div></td>
                           <td> <div class="checkbox-fade fade-in-primary">
                           <?php if($its_delete!=''){ ?>
                                 <label class="custom_checkbox1">
                                 <input type="checkbox" id="bulkdeletes" class="dashboard_check viewall bulkdelete" name="bulkdelete[]" value="<?php echo $page_value."-delete"; ?>" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['delete']){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-delete";echo "checked"; } ?> >
                               
                                <!--  <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>   -->
                                <i></i>
                                 </label>
                                    <input type="hidden" id="for_bulkdeletes" class=" viewall for_bulkdelete" name="for_bulkdelete[]" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['delete']){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-delete"; ?> value="<?php echo $val;?>" <?php }else { ?> value="<?php echo $page_value."-delete//unchecked"; ?>" <?php } ?> >
                            <?php }else{ ?>
                                    <input type="hidden" id="for_bulkdeletes" class=" viewall for_bulkdeletes" name="for_bulkdelete[]" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['delete']){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-delete"; ?> value="<?php echo $val;?>" <?php }else { ?> value="<?php echo $page_value."-delete//unchecked"; ?>" <?php } ?> >
                             <?php } ?>
                              </div></td>
                        </tr>
                           <?php
                        $i++; 
                        }
                      
                     }
                     ?>
                    
                       
                       
                       
                     </tbody>
                  </table>
                </div>
              <!--end of access permision -->
				 
               </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- management block -->
<?php //$this->load->view('users/column_setting_view');?>
<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<!-- <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap.js"></script> -->
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> 
<script>
   $(document).ready(function() {
   
     $(".has-subtd").on('click',function() {
   
       $('.hide-td').removeClass('foo');
   
       $(this).next('.hide-td').addClass('foo')
       
       // $('.edit-field:not(.foo)').removeClass('show');
   
       $(this).next('.hide-td').slideToggle(300);
   
       //$(this).next('.edit-field').toggleClass('show');
			
       $(".has-subtd").toggleClass("hide-plus10");
   
     });

     $(".has-subtd.sub-id").on('click',function() {
   
       $('.hide-td').removeClass('foo');
   
       $(this).find('.hide-td').addClass('foo');
       
       // $('.edit-field:not(.foo)').removeClass('show');
  
       $(this).find('.hide-td').slideToggle();
   
       //$(this).next('.edit-field').toggleClass('show');

   
     });
   
     // $('.sep-sec').draggable();
   
     $( ".sortable" ).sortable(
     {
         tolerance: 'pointer',
        cursor: 'move',
        forcePlaceholderSize: true,
        dropOnEmpty: true,
        connectWith: '.sortable',
        create:function(){
          $(this).height($(this).height());
         }
         }).disableSelection();
   
     // $('.sortable').draggable();
    // $( ".sortable" ).disableSelection();
   
   
     $("#alluser").DataTable({
       "iDisplayLength": 20,
     "scrollX": true,
    "dom": '<"top"fl>rt<"bottom"ip><"clear">',
        initComplete: function () {
             this.api().columns('.select-filter').every( function () {
                 var column = this;
                 var select = $('<select><option value=""></option></select>')
                     .appendTo( $(column.footer()).empty() )
                     .on( 'change', function () {
                         var val = $.fn.dataTable.util.escapeRegex(
                             $(this).val()
                         );
   
                         column
                             .search( val ? '^'+val+'$' : '', true, false )
                             .draw();
                     } );
   
                 column.data().unique().sort().each( function ( d, j ) {
                     select.append( '<option value="'+d+'">'+d+'</option>' )
                 } );
             } );
         }
   
     //"processing": true
        
     });
   $("#newactives").dataTable({
        "iDisplayLength": 20,
        // "scrollX": true,
     "dom": '<"top"fl>rt<"bottom"ip><"clear">'
     });
   $("#newinactives").dataTable({
        "iDisplayLength": 20,
         //"scrollX": true,
     "dom": '<"top"fl>rt<"bottom"ip><"clear">'
     });
   $("#frozens").dataTable({
        "iDisplayLength": 20,
         //"scrollX": true,
     "dom": '<"top"fl>rt<"bottom"ip><"clear">'
     });
   
   
   
   
   $("#bulkDelete").on('click',function() { // bulk checked
        var status = this.checked;
        $(".deleteRow").each( function() {
          $(this).prop("checked",status);
        });
      });
      
      $('#deleteTriger').on("click", function(event){ // triggering delete one by one
        if( $('.deleteRow:checked').length > 0 ){  // at-least one checkbox checked
          var ids = [];
          $('.deleteRow').each(function(){
            if($(this).is(':checked')) { 
              ids.push($(this).val());
            }
          });
          var ids_string = ids.toString();  // array to string conversion 
          $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>user/multiple_delete/",
            data: {data_ids:ids_string},
            success: function(response) {
              //dataTable.draw(); // redrawing datatable
              var emp_ids = response.split(",");
    for (var i=0; i < emp_ids.length; i++ ) {
   
      $("#"+emp_ids[i]).remove(); 
    }
            },
            //async:false,
          });
        }
      });
   
   // active/inactive status
   /*$('.status').click(function() {
     if($(this).is(':checked'))
         var stat = '1';
     else
         var stat = '2';
     var rec_id = $(this).val();
     var $this = $(this);
      $.ajax({
         url: '<?php echo base_url();?>user/statusChange/',
         type: 'post',
         data: { 'rec_id':rec_id,'status':stat },
         success: function( data ){
          //alert('ggg');
             $("#status_succ").html('<div class="card borderless-card"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! User status have been changed successfully...</span></div></div></div>');
             if(stat=='1'){
              
               $this.closest('td').next('td').html('Active');
   
             } else {
               $this.closest('td').next('td').html('Inactive');
             }
             },
             error: function( errorThrown ){
                 console.log( errorThrown );
             }
         });
   });*/
   
   
   //$(".status").change(function(e){
   $('#alluser').on('change','.status',function () {
   //e.preventDefault();
   
   
    var rec_id = $(this).data('id');
    var stat = $(this).val();
   
   $.ajax({
         url: '<?php echo base_url();?>user/statusChange/',
         type: 'post',
         data: { 'rec_id':rec_id,'status':stat },
         timeout: 3000,
         success: function( data ){
          //alert('ggg');
             $("#status_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! User status have been changed successfully...</span></div></div></div>').hide().fadeIn(1500, function() { $('#status_succ'); });
             setTimeout(resetAll,3000);
             if(stat=='3'){
              
               //$this.closest('td').next('td').html('Active');
               $('#frozen'+rec_id).html('Frozen');
   
             } else {
               $this.closest('td').next('td').html('Inactive');
             }
             },
             error: function( errorThrown ){
                 console.log( errorThrown );
             }
         });
    });
   
   // payment/non payment status
   $('.suspent').click(function() {
     if($(this).is(':checked'))
         var stat = '1';
     else
         var stat = '0';
     var rec_id = $(this).val();
     var $this = $(this);
      $.ajax({
         url: '<?php echo base_url();?>user/suspentChange/',
         type: 'post',
         data: { 'rec_id':rec_id,'status':stat },
         success: function( data ){
          //alert('ggg');
             $("#suspent_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! User status have been changed successfully...</span></div></div></div>');
             if(stat=='1'){
              
               $this.closest('td').next('td').html('Payment');
   
             } else {
               $this.closest('td').next('td').html('Non payment');
             }
             },
             error: function( errorThrown ){
                 console.log( errorThrown );
             }
         });
   });
   });
   
</script>
<script>
   /* var notes = {};
   
   <?php
      $data = $this->db->query("SELECT * FROM update_client")->result_array();
      foreach($data as $row) {
      
        echo 'notes["'.$row['user_id'].'"] = "<p>company status:'.$row['company_status'].'</p><br><p>account period end:'.$row['accounts_periodend'].'</p>";';
      
       //echo 'notes["'.$row['user_id'].'"] = "'.(isset($row['company_status'])&&($row['company_status']!=0)) ? .'<p>company status:'.$row['company_status'].'</p><br><p>account period end:'.$row['accounts_periodend'].'</p>";';
      
        //echo 'notes["'.$row['user_id'].'"]'= (isset($row['company_status'])&&($row['company_status']!=0)) ? $row['company_status']:'';
      }
      ?>
   
   $(document).on("click", ".noteslink", function() {
     var id = $(this).data("rowid");
     $("#myModalnote .modal-body").html(notes[id]);
     $("#myModalnote").modal("show");
   });
   */
   
   
   
   
</script>
<script type="text/javascript">
   $(document).ready(function(){
     $('#column_setting').on('submit', function (e) {
   
             e.preventDefault();
    $(".LoadingImage").show();
             $.ajax({
               type: 'post',
               url: '<?php echo base_url()."User/column_update";?>',
               data: $('form').serialize(),
               success: function (data) {
                   if(data == '1'){
                   // $('#new_user')[0].reset();
                   $('.alert-success').show();
                   $('.alert-danger').hide();
                   location.reload();
                  // $('.all-usera1').load('<?php echo base_url()?>user .all-usera1');
                   
                 
                   }
                   else if(data == '0'){
                   // alert('failed');
                   $('.alert-danger').show();
                   $('.alert-success').hide();
                   }
                   $(".LoadingImage").hide();
               }
             });
   
           });
   
   
   $('#select_department').change(function(){
      var val = $(this).val();
      if(val=='new_dept')
      {
         $('.new_Department').show();
      }else{
         $('.new_Department').hide();
      }
   });
$('#new_department').keyup(function(){
$('.error_msg').html('');
$('.error_msg_validate').html('');
});
   //$('#save_btn').click(function(e){
      $('#save_btn, #save_btn_new').click(function(e){ 

         var it_value=$(this).attr('data-id');
         e.preventDefault();
         var new_department = $('#new_department').val();
       //  var designation = $('#designation').val();
         var select_department = $('#select_department :selected').val();
 if(new_department=='')
         {
            $('.error_msg').html('<span style="color:red;">This Field is required</span>');
            return false;
         }
         else
         {
            $('.error_msg').html('');
         yourArray = [];

         $("input:checkbox[type=checkbox]:checked").each(function(){
         yourArray.push($(this).val());
         });

         // var data = {};
         // data['new_department'] = new_department;
         // data['it_value'] = it_value;//reference
         // data['select_department'] = select_department;
         // data['permissions'] = yourArray;
         // data['id'] = '<?php echo $id; ?>';

  //var form = $('#department_permissions');
 
var data = {new_department: new_department, it_value: it_value,select_department:select_department,id:'<?php echo $id; ?>',permissions:yourArray};
var postData = $('#department_permissions').serializeArray();
for (var key in data) {
    if (data.hasOwnProperty(key)) {
        postData.push({name:key, value:data[key]});
    }
}
//console.log(postData);

         $(".LoadingImage").show();
         $.ajax({
            type: 'post',
            url: '<?php echo base_url()."department/update_department_permissions";?>',
           // data: data,
          //  data: form.serialize(),
          data:postData,
            success: function (data) {
               $(".LoadingImage").hide();
           // return false;
               //window.location.href = "<?php echo base_url();?>department/dept_permission";
                 if(data=='false'){
                   $('.error_msg_validate').html('<span style="color:red;">Department Name Already Exist</span>');
               }
               else{
                  $('.error_msg_validate').html('');
                  if(it_value=='create'){
                  window.location.href = "<?php echo base_url();?>department/dept_permission";  
                  }
                  else if(it_value=='assign')
                  {
                  window.location.href = "<?php echo base_url();?>department/assign_dept";
                  }
                  else if(it_value=='update')
                  {
                    window.location.href = "<?php echo base_url();?>department/update_assigneddept/<?php echo $update; ?>";
                  }
                  else
                  {
                    window.location.href = "<?php echo base_url();?>department/dept_permission";
                  }
               }
            }
         });
      }
   });


});
/** for permisison checkboxs event **/
$("#view_all").click(function () {
    if ($(this).is(":checked")){
      $('.bulkview').each(function(){
         $(this).prop('checked', true);
         var value=$(this).attr('value');
         $(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value);
         //$(this).next('.for_bulkview').attr('value',value);
      });
    }
    else
    {
      $('.bulkview').each(function(){
         $(this).prop('checked', false); 
         var value=$(this).attr('value')+"//unchecked";
         $(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value);

       //  $(this).next('.for_bulkview').attr('value',value);
      });
    }
 });
$("#create_all").click(function () {
    if ($(this).is(":checked")){
      $('.bulkcreate').each(function(){
         $(this).prop('checked', true); 
             /** for extra fields **/
     var value=$(this).attr('value');
     $(this).closest('.checkbox-fade').find('.for_bulkcreate').attr('value',value);
     /** for extra fields **/
      });
    }
   else
    {
      $('.bulkcreate').each(function(){
         $(this).prop('checked', false); 
             /** for extra fields **/
     var value=$(this).attr('value')+"//unchecked";
     $(this).closest('.checkbox-fade').find('.for_bulkcreate').attr('value',value);
     /** for extra fields **/
      });
    }

 });
$("#edit_all").click(function () {
    if ($(this).is(":checked")){
      $('.bulkedit').each(function(){
         $(this).prop('checked', true); 
             /** for extra fields **/
     var value=$(this).attr('value');
     $(this).closest('.checkbox-fade').find('.for_bulkedit').attr('value',value);
     /** for extra fields **/
      });
    }
      else
    {
      $('.bulkedit').each(function(){
         $(this).prop('checked', false); 
             /** for extra fields **/
     var value=$(this).attr('value')+"//unchecked";
     $(this).closest('.checkbox-fade').find('.for_bulkedit').attr('value',value);
     /** for extra fields **/
      });
    }
 });
$('#delete_all').click(function () {
    if ($(this).is(":checked")){
      $('.bulkdelete').each(function(){
         $(this).prop('checked', true); 
             /** for extra fields **/
     var value=$(this).attr('value');
     $(this).closest('.checkbox-fade').find('.for_bulkdelete').attr('value',value);
     /** for extra fields **/
      });
    }
      else
    {
      $('.bulkdelete').each(function(){
         $(this).prop('checked', false); 
             /** for extra fields **/
     var value=$(this).attr('value')+"//unchecked";
     $(this).closest('.checkbox-fade').find('.for_bulkdelete').attr('value',value);
     /** for extra fields **/
      });
    }
 });

$('#selectall').click(function () {
    if ($(this).is(":checked")){
      $('#view_all').prop('checked',false).trigger('click');
      $('#create_all').prop('checked',false).trigger('click');
      $('#edit_all').prop('checked',false).trigger('click');
      $('#delete_all').prop('checked',false).trigger('click');
      $('.bulkall').each(function(){
         $(this).prop('checked', true); 
      });
    }
      else
    {     
      $('#view_all').prop('checked',true).trigger('click');
      $('#create_all').prop('checked',true).trigger('click');
      $('#edit_all').prop('checked',true).trigger('click');
      $('#delete_all').prop('checked',true).trigger('click');
      $('.bulkall').each(function(){
         $(this).prop('checked', false); 
      });
    }
 });
$('.bulkall').click(function(){
 if($(this).is(":checked")){
    $(this).closest('tr').find('td').each(function(){
   //alert('sdsd');
   $(this).find('input[type="checkbox"]').prop('checked',true);
  // alert($(this).attr('class'));
      var value=$(this).find('input[type="checkbox"]').attr('value');
   // alert($(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value));
   var itid=$(this).find('input[type="checkbox"]').attr('id');
  //alert(itid);
   if(itid=='bulkviews'){
$(this).find('.for_bulkview').attr('value',value);
}
  if(itid=='bulkcreates'){
$(this).find('.for_bulkcreate').attr('value',value);
}  if(itid=='bulkedits'){
$(this).find('.for_bulkedit').attr('value',value);
}  if(itid=='bulkdeletes'){
$(this).find('.for_bulkdelete').attr('value',value);
}

  });
 }
 else
 {
    $(this).closest('tr').find('td').each(function(){
   //alert('sdsd');
   $(this).find('input[type="checkbox"]').prop('checked',false);
      var value=$(this).find('input[type="checkbox"]').attr('value')+"//unchecked";
   // alert($(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value));
   var itid=$(this).find('input[type="checkbox"]').attr('id');
   if(itid=='bulkviews'){
$(this).find('.for_bulkview').attr('value',value);
}
  if(itid=='bulkcreates'){
$(this).find('.for_bulkcreate').attr('value',value);
}  if(itid=='bulkedits'){
$(this).find('.for_bulkedit').attr('value',value);
}  if(itid=='bulkdeletes'){
$(this).find('.for_bulkdelete').attr('value',value);
}
  });
 }


});
$('.bulkview').click(function(){
 // alert('zz');
  if($(this).is(":checked")){
    var value=$(this).attr('value');
   // alert($(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value));
$(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value);

  }
  else
  {
    var value1=$(this).attr('value')+"//unchecked";
  //  alert($(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value1));
$(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value1);
  }
});

$('.bulkcreate').click(function(){
 // alert('zz');
  if($(this).is(":checked")){
    var value=$(this).attr('value');
   // alert($(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value));
$(this).closest('.checkbox-fade').find('.for_bulkcreate').attr('value',value);
  }
  else
  {
    var value1=$(this).attr('value')+"//unchecked";
  //  alert($(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value1));
$(this).closest('.checkbox-fade').find('.for_bulkcreate').attr('value',value1);
  }
});

$('.bulkedit').click(function(){
 // alert('zz');
  if($(this).is(":checked")){
    var value=$(this).attr('value');
   // alert($(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value));
$(this).closest('.checkbox-fade').find('.for_bulkedit').attr('value',value);
  }
  else
  {
    var value1=$(this).attr('value')+"//unchecked";
  //  alert($(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value1));
$(this).closest('.checkbox-fade').find('.for_bulkedit').attr('value',value1);
  }
});

$('.bulkdelete').click(function(){
 // alert('zz');
  if($(this).is(":checked")){
    var value=$(this).attr('value');
   // alert($(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value));
$(this).closest('.checkbox-fade').find('.for_bulkdelete').attr('value',value);
  }
  else
  {
    var value1=$(this).attr('value')+"//unchecked";
  //  alert($(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value1));
$(this).closest('.checkbox-fade').find('.for_bulkdelete').attr('value',value1);
  }
});

$('#save_btn').click(function(){
     $("input[name='bulkview[]']:not(:checked)").each(function () {
          //  alert("Id: " + $(this).attr("id") + " Value: " + $(this).val());
        });
     //return false;
   });



   

   
</script>
