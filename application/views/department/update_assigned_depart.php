<?php $this->load->view('includes/header');
   $succ = $this->session->flashdata('success');
   ?>
<div class="pcoded-content pcodedteam-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <!--start-->
                  <div class="update-dept01">
                     <?php if($succ){?>
                     <div class="modal-alertsuccess alert alert-success"
                        >
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <div class="pop-realted1">
                           <div class="position-alert1">
                              <?php echo $succ; ?>
                           </div>
                        </div>
                     </div>
                     <?php } ?>
                      <div class="deadline-crm1 floating_set">
                                 <ul class="nav nav-tabs1 all_user1 md-tabs pull-left u-dashboard">
                                    <li class="nav-item">
                                       <a class="nav-link" href="javascript:;">Assign Team</a>
                                       <div class="slide"></div>
                                    </li>
                                 </ul>
                      </div>
                     <div class=" new-task-teams">
                        
                        <form id="assign_new_department" method="post" action="<?php echo base_url()?>department/update_assigned_depart/<?php echo $departments['id'];?>" enctype="multipart/form-data">
                           <div class="row">
                              <div class="form-group col-sm-6">
                                 <label>Select Department</label>
                                 <select class="form-control" name="department">
                                    <option value=''>-- select--</option>
                                    <?php foreach($department as $departmentss) {
                                       ?>
                                    <option value="<?php echo $departmentss['id']?>" <?php if($departmentss['id']==$departments['depart_id']){ echo 'selected'; }?>><?php echo $departmentss['new_dept'];?></option>
                                    <?php } ?>
                                 </select>
                              </div>
                              <div class="form-group col-sm-6">
                                 <label>Select Team</label>
                                 <?php  $team_id = $departments['team_id'];
                                    $team_exp = explode(',',$team_id); 
                                    // print_r($departments);die;?>
                                 <div class="dropdown-sin-2">
                                    <select name="team[]" id="team" multiple placeholder="Select">
                                       <?php foreach ($team as $key => $value) { ?>
                                       <option value="<?php echo $value['id'];?>"   <?php if(isset($value['id']) && in_array( $value['id'] ,$team_exp )) {?> selected="selected"<?php } ?> ><?php echo $value['team'];?></option>
                                       <?php } ?>
                                    </select>
                                 </div>
                              </div>
                              <!--  <div class="form-group">
                                 <label>Select Team</label>
                                 <select class="form-control" name="team">
                                    <option value=''>-- select--</option>
                                    <?php foreach ($team as $key => $value) { ?>
                                    <option value="<?php echo $value['id'];?>"><?php echo $value['team'];?></option>
                                    <?php } ?>
                                 </select>
                                 </div> -->
                              <!-- <div class="form-group">
                                 <label>Select Staff</label>
                                 <select class="form-control" name="staff[]">
                                    <option value=''>-- select--</option>
                                    <?php foreach ($staff as $staff_key => $staff_value) { ?>
                                    <option value="<?php echo $staff_value['id'];?>"><?php echo $staff_value['crm_name'];?></option>
                                    <?php } ?>
                                 </select>
                                 </div> -->
                              <div class="form-group create-btn col-sm-12">
                                 <input type="submit" name="add_team" class="btn-primary" value="update"/>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
                  <!-- close -->
               </div>
            </div>
            <!-- Page body end -->
         </div>
      </div>
      <!-- Main-body end -->
      <div id="styleSelector">
      </div>
   </div>
</div>
</div>
</div>
</div>
</div>
<!-- ajax loader -->
<div class="LoadingImage" ></div>
<style>
   .LoadingImage {
   display : none;
   position : fixed;
   z-index: 100;
   background-image : url('<?php echo site_url()?>assets/images/ajax-loader.gif');
   background-color:#666;
   opacity : 0.4;
   background-repeat : no-repeat;
   background-position : center;
   left : 0;
   bottom : 0;
   right : 0;
   top : 0;
   }
</style>
<!-- ajax loader end-->
<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<!-- Warning Section Starts -->
<!-- Older IE warning message -->
<!--[if lt IE 10]>
<div class="ie-warning">
   <h1>Warning!!</h1>
   <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
   <div class="iew-container">
      <ul class="iew-download">
         <li>
            <a href="http://www.google.com/chrome/">
               <img src="assets/images/browser/chrome.png" alt="Chrome">
               <div>Chrome</div>
            </a>
         </li>
         <li>
            <a href="https://www.mozilla.org/en-US/firefox/new/">
               <img src="assets/images/browser/firefox.png" alt="Firefox">
               <div>Firefox</div>
            </a>
         </li>
         <li>
            <a href="http://www.opera.com">
               <img src="assets/images/browser/opera.png" alt="Opera">
               <div>Opera</div>
            </a>
         </li>
         <li>
            <a href="https://www.apple.com/safari/">
               <img src="assets/images/browser/safari.png" alt="Safari">
               <div>Safari</div>
            </a>
         </li>
         <li>
            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
               <img src="assets/images/browser/ie.png" alt="">
               <div>IE (9 & above)</div>
            </a>
         </li>
      </ul>
   </div>
   <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->
<!-- Warning Section Ends -->
<!-- Required Jquery -->
<script type="text/javascript">
   $('#timepicker1').timepicker();
</script> 
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript">
   $(document).ready(function(){
   
      $( "#assign_new_department" ).validate({
   
        rules: {
          department: "required",  
          "team[]": "required",  
        },
        messages: {
          department: "Select Department",
          "team[]": "Select Team",
        },
        
      });
   
       $('.dropdown-sin-2').dropdown({
      limitCount: 5,
      input: '<input type="text" maxLength="20" placeholder="Search">'
       });
   });
     /** for succesmsg hide **/
   $(document).ready(function(){
     setTimeout(function(){ 
      $('.alert-success').hide(); }, 2000);
   });
   /** endof successmsg hide **/
</script>
</body>
</html>