<?php $this->load->view('includes/header');
   $succ = $this->session->flashdata('success');
   ?>
<div class="pcoded-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <!--start-->
                  <div class="col-sm-12 common_form_section12">
                     <?php if($succ){?>
                     <!--   <div class="alert alert-success"
                        ><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo $succ; ?></div> -->
                     <div class="modal-alertsuccess alert alert-success"
                        >
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <div class="pop-realted1">
                           <div class="position-alert1">
                              <?php echo $succ; ?>
                           </div>
                        </div>
                     </div>
                     <?php } ?>
                     <div class="deadline-crm1 floating_set">
                                 <ul class="nav nav-tabs1 all_user1 md-tabs pull-left u-dashboard">
                                    <li class="nav-item">
                                       <a class="nav-link" href="javascript:;">Assign staff To Team</a>
                                       <div class="slide"></div>
                                    </li>
                                 </ul>
                      </div>
                     <div class="col-xs-12 new-task  new-task-teams">
                        
                        <form id="assign_new_team" method="post" action="<?php echo base_url()?>team/update_assigned_team/<?php echo $teams['id'];?>" enctype="multipart/form-data">
                           <div class="row">
                              <div class="form-group col-sm-6">
                                 <label>Select Team</label>
                                 <div class="dropdown-sin-3">
                                 <select class="form-control all_team" name="team" id="for_team">
                                    <option value=''>-- select--</option>
                                    <?php foreach ($team as $key => $value) { ?>
                                    <option value="<?php echo $value['id'];?>" <?php if($value['id']==$teams['team_id']){ echo 'selected'; }?> ><?php echo $value['team'];?></option>
                                    <?php } ?>
                                 </select>
                                 </div>
                              </div>
                              <!-- <div class="form-group">
                                 <label>Select Staff</label>
                                 <select class="form-control" name="staff[]">
                                    <option value=''>-- select--</option>
                                    <?php foreach ($staff as $staff_key => $staff_value) { ?>
                                    <option value="<?php echo $staff_value['id'];?>"><?php echo $staff_value['crm_name'];?></option>
                                    <?php } ?>
                                 </select>
                                 </div> -->
                              <div class="form-group col-sm-6 for_staff_sec" >
                                 <label>Select Members</label><?php  $staffid = $teams['staff_id'];
                                    $staff_exp = explode(',',$staffid); 

                                   //print_r($staff_exp);die;?>
                                    <?php $new_array=array("1"=>"Admin","2"=>"Sub Admin","3"=>"Director","4"=>"Client","5"=>"Manager","6"=>"Staff"); ?>
                                 <div class="dropdown-sin-2">
                                    <select name="staff[]" id="staff" multiple placeholder="Select">
                                       <?php 
                                          foreach($staff as $staffs) {
                                             ?>
                                       <option value="<?php echo $staffs['id'];?>"  <?php if(isset($staffs['id']) && in_array( $staffs['id'] ,$staff_exp )) {?> selected="selected"<?php } ?>><?php echo $staffs['username'];?></option>
                                       <?php } ?>
                                    </select>
                                 </div>
                              </div>
                              <div class="form-group create-btn col-sm-12">
                                 <input type="submit" name="add_task" class="btn-primary" value="update"/>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
                  <!-- close -->
               </div>
            </div>
            <!-- Page body end -->
         </div>
      </div>
      <!-- Main-body end -->
      <div id="styleSelector">
      </div>
   </div>
</div>
</div>
</div>
</div>
</div>
<!-- ajax loader -->
<div class="LoadingImage" ></div>
<style>
   .LoadingImage {
   display : none;
   position : fixed;
   z-index: 100;
   background-image : url('<?php echo site_url()?>assets/images/ajax-loader.gif');
   background-color:#666;
   opacity : 0.4;
   background-repeat : no-repeat;
   background-position : center;
   left : 0;
   bottom : 0;
   right : 0;
   top : 0;
   }
   label.error {
   color: red;
   }
</style>
<!-- ajax loader end-->
<?php $this->load->view('includes/session_timeout');?>

<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/popper.js/js/popper.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap/js/bootstrap.min.js"></script>
<!-- j-pro js -->
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.j-pro.js"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<!-- modernizr js -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/switchery/js/switchery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/modernizr/js/css-scrollbars.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/advance-elements/custom-picker.js"></script>
<script src="<?php echo base_url();?>assets/js/pcoded.min.js"></script>
<script src="<?php echo base_url();?>assets/js/demo-12.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/common_script.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
<script src="<?php echo base_url();?>assets/js/mock.js"></script>
<script type="text/javascript">
   $('#timepicker1').timepicker();
</script> 
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript">
   //$(document).ready(function(){
   $.validator.setDefaults({
         ignore: []
     });
      $( "#assign_new_team" ).validate({
            errorPlacement: function(error, element) {
               if (element.attr("name") == "staff[]" )
               {
                     error.insertAfter(".dropdown-sin-2");
                   error.insertAfter(".dropdown-sin-4");
                 }
               else if(element.attr("name") == "team" )
                     error.insertAfter(".dropdown-sin-3");                   
                 else
                     error.insertAfter(element);
             },
        rules: {
          team: "required",  
          "staff[]": "required",  
        },
        messages: {
          team: "Select Team",
          "staff[]": "Select Staff",
        },
        
      });
   
       $('.dropdown-sin-2').dropdown({
      //limitCount: 5,
      input: '<input type="text" maxLength="20" placeholder="Search">'
       });
            $('.dropdown-sin-3').dropdown({
      //limitCount: 5,
      input: '<input type="text" maxLength="20" placeholder="Search">'
       });

    $('.dropdown-sin-3').find('.dropdown-main > ul > li').click(function(){
 
      var it_val=$(this).attr('data-value');
      console.log(it_val);
        if(it_val!='')
        {
            $.ajax({  
                       url:"<?php echo base_url();?>team/get_teamwise_users/",  
                       method:"POST",  
                       data:{team:it_val},  
                       success:function(data)  
                       {  
                      //  console.log(data);
                        $('.for_staff_sec').html('');
                        $('.for_staff_sec').html(data);
                       //  $('.dropdown-sin-2').dropdown({
  
                       // input: '<input type="text" maxLength="20" placeholder="Search">'
                       //  });
                      
                       }
                  });
        }
    });

   //});
   /** for succesmsg hide **/
   $(document).ready(function(){
     setTimeout(function(){ 
      $('.alert-success').hide(); }, 2000);
   });
   /** endof successmsg hide **/
     $(document).ready(function(){
 //onload_function();
      var members_array = [];
      <?php foreach($staff as $staffs) {
      ?>
members_array.push({'<?php echo $staffs['id'] ?>' :'<?php echo $new_array[$staffs['role']]; ?>' });
      <?php } ?>
    //  console.log(members_array);
     
   // alert($('.dropdown-sin-2').find('.dropdown-main').attr('class'));
   var i=0;
    $('.dropdown-sin-2').find('.dropdown-main > ul > li').each(function(){
   //  alert($(this).attr('data-value'));
   var it_val=$(this).attr('data-value');
 //console.log(members_array[i][it_val]);
    
       $(this).append('&nbsp;<span class="for_staff_permission">Role: '+members_array[i][it_val]+'</span>');
       $(this).find('.for_staff_permission').click(function(){
            //alert('sss');
       $(this).parent('li').trigger('click');
       });
     
    i++;
    });
     setTimeout(function(){ 
      $('.alert-success').hide(); }, 2000);
  });


</script>
</body>
</html>