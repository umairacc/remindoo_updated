<?php $this->load->view('includes/header');?>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/org_chart/css/orgchart.min.css">
	<style type="text/css">
		#chart-container {
  font-family: Arial;
/*  height: 420px;*/
  border: 2px dashed #aaa;
  border-radius: 5px;
  overflow: auto;
  text-align: center;
  background: #fff;
}
</style>
  <!-- show info  -->
  <div class="modal-alertsuccess alert info_popup" style="display:none;">
    <div class="newupdate_alert">
     <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
     <div class="pop-realted1">
        <div class="position-alert1 info-text">
         </div>
        </div>
     </div>
  </div>
  <!-- end show info  -->



 <div class="pcoded-content card-removes">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
                <div class="row">
                  <div class="col-sm-12">

                     <!-- card start -->
                     <div class="card">
                      <div class="add_newd">
                        <div class="deadline-crm1 floating_set">
                          <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard client-top-nav">
                            <li class="nav-item">
                              <a href="<?php echo base_url()?>Role_Assign" class="nav-link " data-tag="home">Roles</a>
                              <div class="slide"></div>
                            </li>
                            <li class="nav-item ">
                             <a class="nav-link"  data-tag="home" href="<?php echo base_url()?>user/staff_list">Users</a>
                             <div class="slide"></div>
                          </li>
                            <li class="nav-item ">
                            <a href="<?php echo base_url()?>team/organisation_tree" class="nav-link active" data-tag="home">Groups</a>
                              <div class="slide"></div>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div id="chart-container">
                      </div>
                    </div>
                    <!-- card End -->

                  </div>
               </div>
            </div>          
         </div>
      </div>
   </div>
</div>


<div class="modal fade" id="AssignMembers_popup" role="dialog" style="display: none;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Assign Member</h4>
        </div>
        <div class="modal-body">
          <form id="AssignMembers_From" action="" name="AssignMembers_From" method="post"> 
              
              <div class="form-group">
                <label>Member</label>
                <div class="assigne_div">
                  <select name="assigne[]" multiple="true">
                
                  </select>
                </div>
              </div>
              <!-- <div class="form-group edit_formcls">
                <label>Title</label>
                <input type="text" name="title" class="title">
              </div> -->
              <button type="submit" class="btn btn-default">Submit</button>
              <input type="hidden" name="parent_id" class="parent_id">
          </form>
        </div>       
      </div>
    </div>
</div>


<div class="modal fade" id="EditAssignedMembers_popup" role="dialog" style="display: none;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Member</h4>
        </div>
        <div class="modal-body">
          <form id="editAssignedMembers_From" action="" name="AssignMembers_From" method="post"> 
              
              <div class="form-group">
                <label>Member</label>
                <div class="assigne_div">
                  <select name="assigne">
                  </select>
                </div>
              </div>
              <div class="form-group edit_formcls">
                <label>Title</label>
                <input type="text" name="title" class="title">
              </div>
              <button type="submit" class="btn btn-default">Submit</button>
              <input type="hidden" name="node_id" class="node_id">
          </form>
        </div>       
      </div>
    </div>
</div>

<?php $this->load->view('includes/footer');?>
<!-- <script src="https://code.jquery.com/jquery-2.2.4.js"></script>-->
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/orgchart/2.1.3/js/jquery.orgchart.min.js"></script>-->
<script type="text/javascript" src="<?php echo base_url();?>assets/org_chart/js/jquery.orgchart.js"></script> 

<script>
var ds = <?php echo $Chart_DataSource_json;?>;
var org_chart;
var Editassigne_div_Instance = '';
var assigne_div_Instance = '';
function Save_Mempers()
{
  console.log('submitHandler');

  var DATA = new FormData( $('#AssignMembers_From')[0] );
  $.ajax({
            url: '<?php echo base_url()."Team/org_chart_Add_newAssignee";?>',
            type: 'post',
            data:DATA,  
            contentType : false,
            processData : false,
            dataType:'json',
            beforeSend:function(){ console.log('beforeSend');$(".LoadingImage").show(); },
            success: function (data) {
               if(data.result == 1)
               {
                 org_chart.init({ 'data':data.DataSource_json});
                 trigger_orgChartEvent();
               }
               $(".LoadingImage").hide();
               $("#AssignMembers_popup").modal('hide');
               //console.log(data.DataSource_json);
               //setTimeout(function(){location.reload();},1500);
            }
          });
}
function Edit_Mempers()
{
  var DATA = new FormData( $('#editAssignedMembers_From')[0] );
  $.ajax({
            url: '<?php echo base_url()."Team/org_chart_edit_Assignee";?>',
            type: 'post',
            data:DATA,  
            contentType : false,
            processData : false,
            dataType:'json',
            beforeSend:function(){$(".LoadingImage").show(); },
            success: function (data) {
               if(data.result == 1)
               {
                 org_chart.init({ 'data':data.DataSource_json});
                 trigger_orgChartEvent();
               }
                $(".LoadingImage").hide();
                $("#EditAssignedMembers_popup").modal('hide');
               //console.log(data.DataSource_json);
               //setTimeout(function(){location.reload();},1500);
            }
          });

}

  function trigger_orgChartEvent()
  {
    org_chart.$chart.on('nodedropped.orgchart', function(event) 
    {
      var dragN = event.draggedNode.attr('id');
      var dropN = event.dropZone.attr('id');
      //console.log('dragN'+dragN+'dropN'+dropN);
       $.ajax({
                url: '<?php echo base_url()."Team/orgChart_ChangeParent";?>',
                type: 'post',
                dataType:'json',
                data:{'dragN':dragN,'dropN':dropN},
                beforeSend:function(){ $(".LoadingImage").show(); },
                success: function (data)
                {
                  console.log(data.DataSource_json);
                  org_chart.init({'data':data.DataSource_json});
                  trigger_orgChartEvent();
                  $(".LoadingImage").hide();
                }
              });
    });
  }

 $(function($) {

    

    var value_split = function () {
              var text = this.$select.find('option:selected:not([value=""])').text();
              var text = text.split('-').pop();
              this.$select.closest("form").find(".title").val( text );
            };
    var Custom_Notes =  function(node, data) {

      <?php if( $PERMISSION['create'] == 1 ) { ?>
          $(node).find('.content').append('<i class="fa fa-plus-circle AssignMembers" style="cursor:pointer;"></i>');
      <?php } ?>

    if( !$(node).hasClass('ROOT_NODE') )
    {
      //<i class="fa fa-edit Edit_Group" data-id="'+data.id+'" style="cursor:pointer;"></i>
      var EditButton = '';
      var DeleteButton = '';
      <?php 
        if( $PERMISSION['edit'] == 1 )
        { 
      ?>
        EditButton = '<li><a href="#" class="Edit_Node" data-id="'+data.id+'" >edit</a></li>';
      <?php
        }
        if( $PERMISSION['delete'] == 1 )
        {
          ?>
          DeleteButton = '<li><a href="#" class="deleteNode" data-id="'+data.id+'">delete</a></li><li><a href="#" class="deleteNodeWChild" data-id="'+data.id+'">delete With Child</a></li>';
          <?php
        }
      ?>
      $(node).find('.title').append('<div class="dropdown"><i class="fa fa-cogs  dropdown-toggle"  data-toggle="dropdown"></i><ul class="dropdown-menu">'+EditButton+DeleteButton+'</ul></div>');
    }

    };  
  $(document).on('click',".deleteNode , .deleteNodeWChild",function(){
    var id = $(this).attr('data-id');
    
    var url ="Team/deleteNode";
    if( $(this).hasClass('deleteNodeWChild') )
    {
      url = "Team/deleteNodeWChild"
    }

    var action = function(){
      $.ajax({
        url:"<?php echo base_url();?>"+url,
        data:{'id':id},
        type:'post',
        dataType:'json',
        beforeSend:function(){Show_LoadingImg();$('#Confirmation_popup').modal('hide');},
        success:function(data)
        {
          Hide_LoadingImg();
          org_chart.init({data:data.DataSource_json});
          trigger_orgChartEvent();
        }
    });
    

    };

     Conf_Confirm_Popup({'OkButton':{'Handler':action},'Info':'Do you want to delete this ?'});  
     $('#Confirmation_popup').modal('show');
  });

    

  $(document).on('click',".AssignMembers",function(){ 

    var SelNode  = $(this).closest('div.node');
    var DRPN = SelNode.attr('class').match(/0:\S+/g);

         $.ajax({
              url: '<?php echo base_url()."Team/orgChart_Get_assignee";?>',
              type: 'post',
              data:{'hry':DRPN[0]},
              dataType:'json',
              beforeSend:function(){ $(".LoadingImage").show(); },
              success: function (data) {
                //remove defual select option.
                data.shift();
                if(assigne_div_Instance == '' )
                {
                 assigne_div_Instance = $('#AssignMembers_popup .assigne_div').dropdown({data:data }).data('dropdown');
                }
                else
                {
                  assigne_div_Instance.update( data , true );
                }
                //$('#AssignMembers_popup .title').val('');
                assigne_div_Instance.choose("");

                $('#AssignMembers_popup .parent_id').val( SelNode.attr('id') );
                
                $('#AssignMembers_popup').modal('show');
                $(".LoadingImage").hide();
              }
            });

  });

  $(document).on('click',".Edit_Node",function(){

    var id = $(this).attr('data-id');
    var DRPN = $(this).closest('div.node').attr('class').match(/0:\S+/g);
        DRPN = DRPN[0].split(':');
        DRPN.pop();//remove the editing person
    $.ajax({
      url:"<?php echo base_url();?>Team/Get_Edit_orgChart",
      type:'post',
      dataType:'json',
      data:{'id':id,'hry':DRPN.join(':')},
      beforeSend:function(){ $(".LoadingImage").show(); },
      success: function (data) {
          //console.log(data);
        if(Editassigne_div_Instance =='')
        {
           Editassigne_div_Instance = $("#EditAssignedMembers_popup .assigne_div").dropdown({data:data.assignee,
            choice: value_split
}).data('dropdown');
           
           Editassigne_div_Instance.choose(data['details']['user_id']);
        }
        else
        {
          Editassigne_div_Instance.update(data.assignee,true);
          Editassigne_div_Instance.choose(data['details']['user_id']);

        }
        
        $("#EditAssignedMembers_popup .title").val(data['details']['title']);
        $("#EditAssignedMembers_popup .node_id").val(id);
        $("#EditAssignedMembers_popup").modal('show');
        $('.LoadingImage').hide();
      }
    });
    
    });



  var Drop_privileges = function(draggedNode, dragZone, dropZone) {
        
          var DN = $(draggedNode).attr('class').match(/0:\S+/g);
          DN = DN[0].split(":");
          DN = DN.pop();
          var DRPN = $(dropZone).attr('class').match(/0:\S+/g);
          DRPN = DRPN[0].split(":");
          //console.log(DRPN);
          
          //console.log( 'DN'+DN +'----DRPN'+DRPN);
          if( DRPN.indexOf(DN)!= -1 )
          {

            return false;
          }
          else
          {

            return true;
          }
        };


	org_chart = $('#chart-container').orgchart({
    'collapsed': false,
        'data' : ds,      
        'nodeContent': 'title',
        'draggable': true,
	      'dropCriteria': Drop_privileges,
	      'createNode': Custom_Notes
	    });

	$('#AssignMembers_From').validate({
	    ignore:false,
	    rules:{
	      assigne:{required:true},
	      /*title:{required:true}*/
	    },    
	    submitHandler:Save_Mempers
	});
  $('#editAssignedMembers_From').validate({ 
      ignore:false,
      rules:{
        assigne:{required:true},
        title:{required:true}
      },    
      submitHandler:Edit_Mempers
  });

  trigger_orgChartEvent();


});
</script>