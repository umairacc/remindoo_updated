<?php $this->load->view('includes/header');?>
<style>
   /*span.newonoff {
   background: #4680ff;
   padding: 6px 15px;
   display: inline-block;
   color: #fff;
   border-radius: 5px;
   }*/
   button.btn.btn-info.btn-lg.newonoff {
   padding: 3px 10px;
   height: initial;
   font-size: 15px;
   border-radius: 5px;
   }
   li.ui-state-default.ui-sortable-handle.ui-sortable-helper
   {
   /*top: 0px !important;*/
   }
   body
   {
   overflow-x:hidden; 
   }
   /*.card.firm-field {
   padding: 30px;
   }*/
   .invoice-details {
   margin-top: 30px;
   }
   .rspt{
    display: none;
   }
</style>
<!-- management block -->
<div class="pcoded-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <div class="card firm-field">
               <form action="<?php echo base_url()?>team/insert_team_access_permission/<?php echo $team['id'];?>" name="department_permissions" id="department_permissions" method="post">
               <div class="heading-save_btn depart">
                  <!-- <span class="pro-head">
                  <a href="<?php echo base_url().'department/dept_permission';?>"> All Departments</a></span>
                  <span class="pro-head">new department</span> -->
                  <ul class="nav nav-tabs1 all_user1 md-tabs tabs112 floating_set" id="depart-per">
                     <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0)">Team Permission
                        </a>
                     </li>
                   
                  </ul>
                  <div class="save-addmore">
                     <input type="submit" id="save_btn" data-id="create" value="save">
                  </div>
               </div>
               <div class="department-table">
                  <div class="management_form1">
                     <div class="basic-width-permis">
                    
                     <div class="form-group row new_Department">
                        <label class="col-sm-4 col-form-label">Team</label>
                        <div class="col-sm-8">
                        <input type="hidden" name="team_id" value="<?php echo $team['id'];?>">
                           <input type="text" name="team" id="team" value="<?php echo $team['team']?>" readonly class="fields"> 
                           <label class="error_msg error"></label>
                        </div>
                     </div>
                  
                  </div>
               </div>
                  <!-- <div class="table-responsive"> -->
              <?php 
              /** hint **/
              // key with empty value is heading ex:"CRM"=>""
              // key with (without - same line) ex:"Dashboard"=>"dashboard"
              // key with (with - its sub value) ex:"Admin Settings"=>"settings-admin_settings"

          $all_pages=array("Dashboard"=>"dashboard","Task Section"=>"","Task"=>"task","Task Create"=>"task-create","CRM"=>"","Leads"=>"crm-leads","Leads Create"=>"crm-leads_create","Webtolead"=>"crm-webtolead","Webtolead Create"=>"crm-webtolead_create","Proposal"=>"","Dashboad"=>"proposal-dashboad","Create Proposal"=>"proposal-create","Catalog"=>"proposal-catalog","Template"=>"proposal-template","Setting"=>"proposal-settings","Client"=>"","Client Section"=>"client-client","Add Client"=>"client-addclient","Add From Company House"=>"client-add_from_company_house","Import Client"=>"client-import_client","Services Timeline"=>"client-services_timeline","Deadline"=>"","Deadline manager"=>"deadline-deadline","Report"=>"","Reports"=>"report-reports","Settings"=>"","Admin Settings"=>"settings-admin_settings","Firm Settings"=>"settings-firm_settings","Column Settings"=>"settings-column_settings","Team and Management"=>"settings-team_and_management","Staff Custom Firmlist"=>"settings-staff_custom_firmlist","Tickets"=>"settings-tickets","Chat"=>"settings-chat","Document"=>"","Documents"=>"documents","Document Create"=>"document-create","Invoice"=>"","Invoices"=>"invoice","Invoices Create"=>"invoice-create");

            $all_pages_section=array("Dashboard"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Task"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Task Create"=>array("view"=>"","add"=>"add","edit"=>"","delete"=>""),"Leads"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Leads Create"=>array("view"=>"","add"=>"add","edit"=>"","delete"=>""),"Webtolead"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Webtolead Create"=>array("view"=>"","add"=>"add","edit"=>"","delete"=>""),"Dashboad"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Create Proposal"=>array("view"=>"","add"=>"add","edit"=>"","delete"=>""),"Catalog"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Template"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Setting"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Client Section"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Add Client"=>array("view"=>"","add"=>"add","edit"=>"","delete"=>""),"Add From Company House"=>array("view"=>"","add"=>"add","edit"=>"","delete"=>""),"Import Client"=>array("view"=>"","add"=>"add","edit"=>"","delete"=>""),"Services Timeline"=>array("view"=>"view","add"=>"","edit"=>"","delete"=>""),"Deadline manager"=>array("view"=>"view","add"=>"","edit"=>"","delete"=>""),"Reports"=>array("view"=>"view","add"=>"","edit"=>"","delete"=>""),"Admin Settings"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Firm Settings"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Column Settings"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Team and Management"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Staff Custom Firmlist"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Tickets"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Chat"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Documents"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Document Create"=>array("view"=>"","add"=>"add","edit"=>"","delete"=>""),"Invoices"=>array("view"=>"view","add"=>"add","edit"=>"edit","delete"=>"delete"),"Invoices Create"=>array("view"=>"","add"=>"add","edit"=>"","delete"=>""));

          $db_access_permission=$this->db->query("select * from team_access_permission where team_id=".$team['id']." order by id  asc ")->result_array();
           // echo "<pre>";
           // print_r($db_access_permission);
              ?>
              <div class="dept-checkbox-danger">
                  <table class="permission-table" id="permission-table">
                  <thead>
                        <tr>
                           <th>
                              <div class="checkbox-fade fade-in-primary">
                                 <label>
                                 <input type="checkbox" id="selectall" value="permission" level="parent">
                                 <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                 </label>
                              </div>
                              <label>permissions</label>
                           </th>
                           <th>
                              <div class="checkbox-fade fade-in-primary">
                                 <label>
                                 <input type="checkbox" id="view_all" value="permission_view">
                                 <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                 </label>
                              </div>
                              <label>view</label>
                           </th>
                           <th>
                              <div class="checkbox-fade fade-in-primary">
                                 <label>
                                 <input type="checkbox" id="create_all" value="permission_create">
                                 <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                 </label>
                              </div>
                              <label>create</label>
                           </th>
                           <th>
                              <div class="checkbox-fade fade-in-primary">
                                 <label>
                                 <input type="checkbox" id="edit_all" value="permission_edit">
                                 <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                 </label>
                              </div>
                              <label>edit</label>
                           </th>
                           <th>
                              <div class="checkbox-fade fade-in-primary">
                                 <label>
                                 <input type="checkbox" id="delete_all" value="permission_delete">
                                 <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                 </label>
                              </div>
                              <label>delete</label>
                           </th>
                        </tr>
                        </thead>
                     <tbody>
                     <?php 
                     $i=0;
                     foreach ($all_pages as $page_key => $page_value) {
                      ?>

                      <?php
                        if($page_value=='')
                        { 
                          // echo $page_value;
                           ?>
                        <tr>
                           <td colspan="5">
                              <label class="permission_heading"><h6><?php echo $page_key;?></h6></label>
                           </td>
                                                     
                        </tr>
                        <?php }
                        else{
                           $its_view_delete=$all_pages_section[$page_key];

                           // echo $page_key."--";
                           // echo json_encode($its_view_delete['add']);
                           // echo "<br>";
                           
                      // echo $page_value;
                           $its_view=$its_view_delete['view'];
                           $its_add=$its_view_delete['add'];
                           $its_edit=$its_view_delete['edit'];
                           $its_delete=$its_view_delete['delete'];

                            $page_explode=explode('-', $page_value);
                           ?>
                               <tr>
                           <td>
                              <div class="checkbox-fade fade-in-primary">
                                 <label>
                                 <input type="checkbox" id="bulkall" class="dashboard_check checkboxall bulkall" value="dashboard" level="parent">
                                 <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                 </label>
                              </div>
                              <?php 
                         if(count($page_explode)>1)
                           {
                              ?><label><?php echo $page_key; ?></label><?php
                           }else
                           {
                              ?><label><h6><?php echo $page_key; ?></h6></label><?php
                           }
                              ?>
                              <!-- <label><?php echo $page_key; ?></label> -->
                           </td>
                           <td>
                              <div class="checkbox-fade fade-in-primary">
                              <?php if($its_view!=''){ ?>
                                 <label>
                                 <input type="checkbox" id="bulkviews" class="dashboard_check viewall bulkview"  name="bulkview[]" value="<?php echo $page_value."-view"; ?>" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['view']==1){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-view";echo "checked"; } ?> >
                                 
                                 <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                 </label>
                                  
                                  <input type="hidden" id="for_bulkviews" class="viewall for_bulkview"  name="for_bulkview[]" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['view']==1){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-view"; ?> value="<?php echo $val;?>" <?php }else { ?> value="<?php echo $page_value."-view//unchecked"; ?>" <?php } ?> >
                                 <?php }else{
                                    ?>
                                      <input type="hidden" id="for_bulkviews" class="viewall for_bulkviews"  name="for_bulkview[]" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['view']==1){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-view"; ?> value="<?php echo $val;?>" <?php }else { ?> value="<?php echo $page_value."-view//unchecked"; ?>" <?php } ?> >
                                   <?php } ?>
                              </div>
                           </td>
                           <td> <div class="checkbox-fade fade-in-primary">
                            <?php if($its_add!=''){ ?>
                                 <label>
                                 <input type="checkbox" id="bulkcreates" class="dashboard_check viewall bulkcreate" name="bulkcreates[]" value="<?php echo $page_value."-create"; ?>" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['create']==1){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-create";echo "checked"; } ?> >
                               
                                 <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                 </label>
                                 
                                   <input type="hidden" id="for_bulkcreates" class="viewall for_bulkcreate" name="for_bulkcreates[]" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['create']==1){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-create"; ?> value="<?php echo $val;?>" <?php }else { ?> value="<?php echo $page_value."-create//unchecked"; ?>" <?php } ?> >
                                   <?php }else{ ?>
                                     <input type="hidden" id="for_bulkcreates" class="viewall for_bulkcreates" name="for_bulkcreates[]" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['create']==1){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-create"; ?> value="<?php echo $val;?>" <?php }else { ?> value="<?php echo $page_value."-create//unchecked"; ?>" <?php } ?> >
                                   <?php } ?>
                              </div>
                              </td>
                           <td> <div class="checkbox-fade fade-in-primary">
                            <?php if($its_edit!=''){ ?>
                                 <label>
                                 <input type="checkbox" id="bulkedits" class="dashboard_check viewall bulkedit" name="bulkedit[]" value="<?php echo $page_value."-edit"; ?>" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['edit']==1){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-edit";echo "checked"; } ?> >
                                
                                 <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                 </label>
                                
                                   <input type="hidden" id="for_bulkedits" class="viewall for_bulkedit" name="for_bulkedit[]" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['edit']==1){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-edit"; ?> value="<?php echo $val;?>" <?php }else { ?> value="<?php echo $page_value."-edit//unchecked"; ?>" <?php } ?> >
                              <?php }else{
                                       ?>
                                 <input type="hidden" id="for_bulkedits" class="viewall for_bulkedits" name="for_bulkedit[]" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['edit']==1){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-edit"; ?> value="<?php echo $val;?>" <?php }else { ?> value="<?php echo $page_value."-edit//unchecked"; ?>" <?php } ?> >
                                       <?php } ?>
                              </div></td>
                           <td> <div class="checkbox-fade fade-in-primary">
                            <?php if($its_delete!=''){ ?>
                                 <label>
                                 <input type="checkbox" id="bulkdeletes" class="dashboard_check viewall bulkdelete" name="bulkdelete[]" value="<?php echo $page_value."-delete"; ?>" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['delete']){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-delete";echo "checked"; } ?> >
                               
                                 <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                 </label>
                                  
                                    <input type="hidden" id="for_bulkdeletes" class=" viewall for_bulkdelete" name="for_bulkdelete[]" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['delete']){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-delete"; ?> value="<?php echo $val;?>" <?php }else { ?> value="<?php echo $page_value."-delete//unchecked"; ?>" <?php } ?> >
                              <?php }else{ ?>
                                     <input type="hidden" id="for_bulkdeletes" class=" viewall for_bulkdeletes" name="for_bulkdelete[]" <?php if(isset($db_access_permission[$i]['main']) && $db_access_permission[$i]['delete']){ $val=$db_access_permission[$i]['main']."-".$db_access_permission[$i]['sub']."-delete"; ?> value="<?php echo $val;?>" <?php }else { ?> value="<?php echo $page_value."-delete//unchecked"; ?>" <?php } ?> >
                                <?php } ?>     
                              </div></td>
                        </tr>
                           <?php
                        $i++; 
                        }
                      
                     }
                     ?>
                    
                    
                       
                       
                       
                     </tbody>
                  </table>
                </div>
                  <!-- </div> -->
               </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- management block -->
<input type="hidden" name="user_id" id="user_id" value="">
<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<!-- <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap.js"></script> -->
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> 
<script>

$("#view_all").click(function () {
    if ($(this).is(":checked")){
      $('.bulkview').each(function(){
         $(this).prop('checked', true);
         var value=$(this).attr('value');
         $(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value);
         //$(this).next('.for_bulkview').attr('value',value);
      });
    }
    else
    {
      $('.bulkview').each(function(){
         $(this).prop('checked', false); 
         var value=$(this).attr('value')+"//unchecked";
         $(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value);

       //  $(this).next('.for_bulkview').attr('value',value);
      });
    }
 });
$("#create_all").click(function () {
    if ($(this).is(":checked")){
      $('.bulkcreate').each(function(){
         $(this).prop('checked', true); 
             /** for extra fields **/
     var value=$(this).attr('value');
     $(this).closest('.checkbox-fade').find('.for_bulkcreate').attr('value',value);
     /** for extra fields **/
      });
    }
   else
    {
      $('.bulkcreate').each(function(){
         $(this).prop('checked', false); 
             /** for extra fields **/
     var value=$(this).attr('value')+"//unchecked";
     $(this).closest('.checkbox-fade').find('.for_bulkcreate').attr('value',value);
     /** for extra fields **/
      });
    }

 });
$("#edit_all").click(function () {
    if ($(this).is(":checked")){
      $('.bulkedit').each(function(){
         $(this).prop('checked', true); 
             /** for extra fields **/
     var value=$(this).attr('value');
     $(this).closest('.checkbox-fade').find('.for_bulkedit').attr('value',value);
     /** for extra fields **/
      });
    }
      else
    {
      $('.bulkedit').each(function(){
         $(this).prop('checked', false); 
             /** for extra fields **/
     var value=$(this).attr('value')+"//unchecked";
     $(this).closest('.checkbox-fade').find('.for_bulkedit').attr('value',value);
     /** for extra fields **/
      });
    }
 });
$('#delete_all').click(function () {
    if ($(this).is(":checked")){
      $('.bulkdelete').each(function(){
         $(this).prop('checked', true); 
             /** for extra fields **/
     var value=$(this).attr('value');
     $(this).closest('.checkbox-fade').find('.for_bulkdelete').attr('value',value);
     /** for extra fields **/
      });
    }
      else
    {
      $('.bulkdelete').each(function(){
         $(this).prop('checked', false); 
             /** for extra fields **/
     var value=$(this).attr('value')+"//unchecked";
     $(this).closest('.checkbox-fade').find('.for_bulkdelete').attr('value',value);
     /** for extra fields **/
      });
    }
 });

$('#selectall').click(function () {
    if ($(this).is(":checked")){
      $('#view_all').prop('checked',false).trigger('click');
      $('#create_all').prop('checked',false).trigger('click');
      $('#edit_all').prop('checked',false).trigger('click');
      $('#delete_all').prop('checked',false).trigger('click');
      $('.bulkall').each(function(){
         $(this).prop('checked', true); 
      });
    }
      else
    {     
      $('#view_all').prop('checked',true).trigger('click');
      $('#create_all').prop('checked',true).trigger('click');
      $('#edit_all').prop('checked',true).trigger('click');
      $('#delete_all').prop('checked',true).trigger('click');
      $('.bulkall').each(function(){
         $(this).prop('checked', false); 
      });
    }
 });
$('.bulkall').click(function(){
 if($(this).is(":checked")){
    $(this).closest('tr').find('td').each(function(){
   //alert('sdsd');
   $(this).find('input[type="checkbox"]').prop('checked',true);
  // alert($(this).attr('class'));
      var value=$(this).find('input[type="checkbox"]').attr('value');
   // alert($(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value));
   var itid=$(this).find('input[type="checkbox"]').attr('id');
  //alert(itid);
   if(itid=='bulkviews'){
$(this).find('.for_bulkview').attr('value',value);
}
  if(itid=='bulkcreates'){
$(this).find('.for_bulkcreate').attr('value',value);
}  if(itid=='bulkedits'){
$(this).find('.for_bulkedit').attr('value',value);
}  if(itid=='bulkdeletes'){
$(this).find('.for_bulkdelete').attr('value',value);
}

  });
 }
 else
 {
    $(this).closest('tr').find('td').each(function(){
   //alert('sdsd');
   $(this).find('input[type="checkbox"]').prop('checked',false);
      var value=$(this).find('input[type="checkbox"]').attr('value')+"//unchecked";
   // alert($(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value));
   var itid=$(this).find('input[type="checkbox"]').attr('id');
   if(itid=='bulkviews'){
$(this).find('.for_bulkview').attr('value',value);
}
  if(itid=='bulkcreates'){
$(this).find('.for_bulkcreate').attr('value',value);
}  if(itid=='bulkedits'){
$(this).find('.for_bulkedit').attr('value',value);
}  if(itid=='bulkdeletes'){
$(this).find('.for_bulkdelete').attr('value',value);
}
  });
 }


});
$('.bulkview').click(function(){
 // alert('zz');
  if($(this).is(":checked")){
    var value=$(this).attr('value');
   // alert($(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value));
$(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value);

  }
  else
  {
    var value1=$(this).attr('value')+"//unchecked";
  //  alert($(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value1));
$(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value1);
  }
});

$('.bulkcreate').click(function(){
 // alert('zz');
  if($(this).is(":checked")){
    var value=$(this).attr('value');
   // alert($(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value));
$(this).closest('.checkbox-fade').find('.for_bulkcreate').attr('value',value);
  }
  else
  {
    var value1=$(this).attr('value')+"//unchecked";
  //  alert($(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value1));
$(this).closest('.checkbox-fade').find('.for_bulkcreate').attr('value',value1);
  }
});

$('.bulkedit').click(function(){
 // alert('zz');
  if($(this).is(":checked")){
    var value=$(this).attr('value');
   // alert($(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value));
$(this).closest('.checkbox-fade').find('.for_bulkedit').attr('value',value);
  }
  else
  {
    var value1=$(this).attr('value')+"//unchecked";
  //  alert($(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value1));
$(this).closest('.checkbox-fade').find('.for_bulkedit').attr('value',value1);
  }
});

$('.bulkdelete').click(function(){
 // alert('zz');
  if($(this).is(":checked")){
    var value=$(this).attr('value');
   // alert($(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value));
$(this).closest('.checkbox-fade').find('.for_bulkdelete').attr('value',value);
  }
  else
  {
    var value1=$(this).attr('value')+"//unchecked";
  //  alert($(this).closest('.checkbox-fade').find('.for_bulkview').attr('value',value1));
$(this).closest('.checkbox-fade').find('.for_bulkdelete').attr('value',value1);
  }
});

$('#save_btn').click(function(){
     $("input[name='bulkview[]']:not(:checked)").each(function () {
          //  alert("Id: " + $(this).attr("id") + " Value: " + $(this).val());
        });
     //return false;
   });

  
</script>