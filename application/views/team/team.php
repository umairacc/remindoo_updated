<?php $this->load->view('includes/header');
   $role = $this->Common_mdl->getRole($_SESSION['id']);
   $succ = $this->session->flashdata('success');
   
   ?>
<style>

   button.btn.btn-info.btn-lg.newonoff {
   padding: 3px 10px;
   height: initial;
   font-size: 15px;
   border-radius: 5px;
   }
   img.user_imgs {
   width: 38px;
   height: 38px;
   border-radius: 50%;
   display: inline-block;
   }

  tfoot {
     display: table-header-group;
  }
 .dropdown-content {
   display: none;
   position: absolute;
   background-color: #fff;
   min-width: 86px;
   overflow: auto;
   box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
   z-index: 1;
   left: -92px;
   width: 150px;
 }
   .text12345 + .modal-backdrop ~ .modal-backdrop {display:none}
</style>

<div class="modal fade" id="myStaff_delete" role="dialog">
    <div class="modal-dialog">
          <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">

        <input type="hidden" name="delete_ids" id="delete_ids" value="">
          <p class="message_confirm">Do you want to Delete Staff?</p>
        </div>
        <div class="modal-footer">
          <button type="button" id="button_class" class="btn btn-default" >Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
      </div>
      
    </div>
  </div>

<div class="pcoded-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card dept-card-perm">
                        <!-- admin start-->
                        <div class="client_section col-xs-12 floating_set">
                              
                              </div> <!-- all-clients -->
                           <div class="all_user-section floating_set">
                              <div class="import-container-1">
                                
                                 <div class="deadline-crm1 floating_set">
                                       <ul class="nav nav-tabs1 all_user1 md-tabs tabs112 floating_set" id="depart-per">
                                             <h4>All Teams
                                             </h4>
                                       </ul>
                                       <div class="pull-right csv-sample01 for-add-staff">
                                          <button type="button" id="delete" data-toggle="modal" data-target="#myStaff_delete" class="deleteTri del-tsk12 f-right button_section" style="display:none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i>Delete</button>

                                             <button type="button" id="archive" data-toggle="modal" data-target="#myStaff_delete" class="archiveTri del-tsk12 f-right button_section" style="display:none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i>Archive</button>

                                             <button type="button" id="unarchive" data-toggle="modal" data-target="#myStaff_delete" class="archiveTri del-tsk12 f-right button_section" style="display:none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i>Unarchive</button>

                                          <a href="javascript:;" data-toggle="modal" data-target="#add_currency12"><i class="fa fa-pencil fa-6" aria-hidden="true"></i> Add New</a>
                                          <a href="<?php echo base_url()?>team/assigned_team" class="assign_ts"><i class="fa fa-pencil fa-6" aria-hidden="true"></i> Assign Members</a>
                                       </div>
                                 </div>

                                 <?php if($succ){?>
                                 <div class="modal-alertsuccess alert alert-success"
                                    ><div class="newupdate_alert">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <div class="pop-realted1">
                                       <div class="position-alert1">
                                          <?php echo $succ; ?>
                                       </div>
                                    </div></div>
                                 </div>
                                 <?php } ?>
                                 <div class="  floating_set">
                                    <div class="tab-content floating_set">
                                       <div id="alltasks" class="tab-pane fade in active">
                                          <div class="count-value1 floating_set">
                                          </div>
                                          <div class="client_section3 table-responsive floating_set">
                                             <?php //$this->load->view('users/task_summary');?>
                                             <div id="status_succ"></div>
                                             <div class="all-usera1 user-dashboard-section1">
                                                <table class="table client_table1 text-center display nowrap" id="alltask" cellspacing="0" width="100%">
                                                   <thead>
                                                      <tr class="text-uppercase">
                                                         <th>
                                                            <label class="custom_checkbox1">
                                                               <input type="checkbox"  id="bulkDelete"  />
                                                               <!-- <label for="bulkDelete"></label> -->
                                                            </div>
                                                         </th>
                                                         <th>SNO <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" style="display: none"/>  
                  <div class="sortMask"></div> 
                  <select multiple="true" class="filter_check" id="" style="display: none;"> </select> </th>
                                                         <th>Team <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon"/>  
                  <div class="sortMask"></div> 
                  <select multiple="true" class="filter_check" id="" style="display: none;"> </select> </th>
                                                         <th>Actions</th>
                                                      </tr>
                                                   </thead>

                                                     <tfoot>
                                                      <tr class="text-uppercase">
                                                         <th>
                                                         </th>
                                                         <th></th>
                                                         <th>   <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                                                         <th></th>
                                                      </tr>
                                                   </thead>

                                                
                                                   <tbody>
                                                      <?php $i =1 ; foreach ($team as $key => $value) {
                                                         ?>
                                                      <tr>
                                                         <td>
                                                            
                                                            <label class="custom_checkbox1">
                                                               <input type='checkbox' id="checkbox<?php echo $value['id'];?>"  class='deleteRow' value="<?php echo $value['id'];?>"  />
                                                               <!-- <label for="checkbox<?php echo $value['id'];?>"> -->

                                                               </label>
                                                         </td>
                                                         <td><?php echo $i;?></td>
                                                         <td><?php echo $value['team'];?></td>
                                                         <td>
                                                            <p class="action_01">
                                                             
                                                               <a href="#" data-toggle="modal" data-target="#myStaff_delete" onclick="return confirm_delete('<?php echo $value['id'];?>');"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>

                                                        
                                                              <a href="javascript:;" data-toggle="modal" data-target="#update_team_<?php echo $value['id']; ?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                                                              <a href="<?php echo base_url();?>team/after_team_permission/<?php echo $value['id'] ?>" style="color:#000;">Permission</a>

                                                              <a href="javascript:void(0)" data-toggle="modal" data-target="#myStaff_delete" onclick="showconfirmation(this)" data-id="<?php echo $value['id'];?>">
                                                              <i class="fa fa-archive archieve_click" aria-hidden="true"></i>

                                                              </a>
                                                            </p>
                                                         </td>
                                                       <!--   <div class="modal-alertsuccess alert alert-success" id="delete_team<?php echo $value['id'];?>" style="display:none;">
                                                            <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                                                            <div class="pop-realted1">
                                                               <div class="position-alert1">
                                                                  Are you sure want to delete <b><a href="#" class="delete_yes"> Yes </a></b> OR <b><a href="#" id="close">No</a></b>
                                                               </div>
                                                            </div>
                                                         </div> -->
                                                      </tr>
                                                     <!--  <div class="modal-alertsuccess alert alert-success" id="delete_user<?php echo $value['id'];?>" style="display:none;">
                                                         <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                                                         <div class="pop-realted1">
                                                            <div class="position-alert1">
                                                               Are you sure want to delete <b><a href="<?php echo base_url().'team/delete/'.$value['id'];?>"> Yes </a></b> OR <b><a href="#" id="close">No</a></b>
                                                            </div>
                                                         </div>
                                                      </div> -->
                                                      <?php $i++; } ?>
                                                   </tbody>
                                                </table>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <!-- home-->
                                 </div>
                                 <!-- admin close -->
                              </div>
                              <!-- Register your self card end -->
                           </div>
                        </div>
                     </div>
                     <!-- Page body end -->
                  </div>
               </div>
               <!-- Main-body end -->
               <div id="styleSelector">
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<!-- update team popup 21-06-2018 -->
 <?php foreach ($team as $key => $value) {
  $id=$value['id'];
  ?>
<div class="modal fade update-tm current-assignmembers" id="update_team_<?php echo $value['id'];?>" style="display: none;">
   <div class="modal-dialog">
      <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">×</button>
               <h4 class="modal-title">Update Team</h4>
            </div>
            <div class="col-xs-12 inside-popup">
               <?php
                $data['team']=$this->Common_mdl->select_record('team','id',$id);
                $data['id'] = $id;
                $this->load->view('team/update_team',$data); ?>
            </div>
         </div>
   </div>
</div>
<?php } ?>
<!--end of team popup -->

<div class="modal fade current-assignmembers" id="add_currency12" style="display: none;">
   <div class="modal-dialog">
      <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">×</button>
               <h4 class="modal-title">Add New Team</h4>
            </div>
            <div class="col-xs-12 inside-popup">
               <?php $this->load->view('team/add_team'); ?>
            </div>
         </div>
   </div>
</div>

<div class="modal fade" id="archiveconfirmation" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
        <inpu type="hidden" name="archive_team_id" id="archive_team_id" value="">
          <p>Do you want to archive team?</p>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default" id="confirm_archive">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
      </div>
      
    </div>
  </div>

  <div class="modal fade" id="Bulkarchiveconfirmation" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
        <inpu type="hidden" name="archive_team_id" id="Bulkarchive_team_id" value="">
          <p>Do you want to archive team?</p>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default" id="Bulkconfirm_archive">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
      </div>
      
    </div>
  </div>


<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<!-- <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap.js"></script> -->
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
<script src="<?php echo base_url();?>assets/js/mock.js"></script>
 
<script type="text/javascript">
   $('#timepicker1').timepicker();
</script> 

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>

<script type="text/javascript">


$( document ).ready(function() {
    var task_list;
});



  function showconfirmation(value)
   {
        var id=$(value).data('id'); 
        $(".message_confirm").html("Do You Want To Archive?");
        $("#button_class").addClass('archive_yes');
        $("#delete_ids").val(id);
  }




// $("#confirm_archive").click(function(){
// var id=$("#archive_team_id").val();
//      $.ajax({
//       url: 'http://remindoo.uk/Team/archive_team/'+id,
//       type : 'get',
//         beforeSend: function() {
//           $(".LoadingImage").show();
//         },
//         success: function(data) {
//           //alert(data);                        
//            $(".LoadingImage").hide();
//            location.reload();
          
//         }

//      });
// });





   // $(document).ready(function(){

   //    $( "#add_new_team" ).validate({

   //      rules: {
   //        new_team: "required",  
   //      },
   //      messages: {
   //        new_team: "Please enter your Team"
   //      },
        
   //    });
   // });
</script>
<script>
   $(document).ready(function() {   
   $('#alluser').on('change','.status',function () {
     var rec_id = $(this).data('id');
     var stat = $(this).val();
   
   $.ajax({
         url: '<?php echo base_url();?>user/statusChange/',
         type: 'post',
         data: { 'rec_id':rec_id,'status':stat },
         timeout: 3000,
         success: function( data ){
          //alert('ggg');
             $("#status_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! User status have been changed successfully...</span></div></div></div>').hide().fadeIn(1500, function() { $('#status_succ'); });
             setTimeout(resetAll,3000);
             if(stat=='3'){
              
               //$this.closest('td').next('td').html('Active');
               $('#frozen'+rec_id).html('Frozen');
   
             } else {
               $this.closest('td').next('td').html('Inactive');
             }
             },
             error: function( errorThrown ){
                 console.log( errorThrown );
             }
         });
    });
   
   // payment/non payment status
   $('.suspent').click(function() {
     if($(this).is(':checked'))
         var stat = '1';
     else
         var stat = '0';
     var rec_id = $(this).val();
     var $this = $(this);
      $.ajax({
         url: '<?php echo base_url();?>user/suspentChange/',
         type: 'post',
         data: { 'rec_id':rec_id,'status':stat },
         success: function( data ){
          //alert('ggg');
             $("#suspent_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! User status have been changed successfully...</span></div></div></div>');
             if(stat=='1'){
              
               $this.closest('td').next('td').html('Payment');
   
             } else {
               $this.closest('td').next('td').html('Non payment');
             }
             },
             error: function( errorThrown ){
                 console.log( errorThrown );
             }
         });
   });
   });
   
</script>
<script>
   $(document).ready(function() {
 
      $('#dropdown2').on('change', function () {
                    // table.columns(5).search( this.value ).draw();
                    var filterstatus = $(this).val();
                    $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>tasksummary/taskFilter",
            data: {filterstatus:filterstatus},
            success: function(response) {
   
              $(".all-usera1").html(response);
    
            },
          
          });
                 });  
    
                
   });
   
</script>
<script>
   $(document).ready(function() {



      $(document).on('change','#bulkDelete',function(){
       var checked = this.checked;
       task_list.column(0).nodes().to$().each(function(index) {
        if (checked)
        {
            $(this).find('.deleteRow').prop('checked',true);
            $(".button_section").show(); 
        } 
        else
        {
            $(this).find('.deleteRow').prop('checked',false);
            $(".button_section").hide();
            
        }
      });
    });


       $(document).on('click','.button_section', function() {
          $("#button_class").removeClass();
          if($(this).attr('id')=='delete'){
               $(".message_confirm").html("Do You Want To Delete?");
               $("#button_class").addClass('delete_yes');
          }else if($(this).attr('id')=='archive'){
               $(".message_confirm").html("Do You Want To Archive?");
                $("#button_class").addClass('archive_yes');
          }else{
                $(".message_confirm").html("Do You Want To unarchive?");
                $("#button_class").addClass('unarchive_yes');
          }
           var staff_id=[];
            task_list.column(0).nodes().to$().each(function(index) {
            if($(this).find(".deleteRow").is(":checked"))
            {        
              staff_id.push($(this).find(".deleteRow").val());
            }
          });    
             $('#myStaff_delete').show();     
             $("#delete_ids").val(staff_id);  
      });  

          $(document).on('click','.deleteRow', function() {
        if($(this).is(':checked', true)) {        
             $(".button_section").show();
        }else{
          $("#bulkDelete").prop('checked',false);
          if($("input.deleteRow:checked").length == 0){
                 $(".button_section").hide();
          }
        }      
      }); 
   // $("#bulkDelete").on('click',function() { // bulk checked
   //      var status = this.checked;
   
   //      $(".deleteRow").each( function() {
   //        $(this).prop("checked",status);
   
   //      });
   //      if(status==true)
   //      {
   //        $('#deleteTriger').show();
   //        $('#archiveTriger').show();
          
   //      } else {
   //        $('#deleteTriger').hide();
   //        $('#archiveTriger').hide();
   //      }
   //    });
   
   // $(".deleteRow").on('click',function() { // bulk checked
   
   //      var status = this.checked;
   
   //      if(status==true)
   //      {
   //        $('#deleteTriger').show();
   //         $('#archiveTriger').show();
   //      } else {
   //        $('#deleteTriger').hide();
   //         $('#archiveTriger').hide();
   //      }
   //    });
      
      // $('#deleteTriger').on("click", function(event){ // triggering delete one by one
       
      //   if( $('.deleteRow:checked').length > 0 ){  // at-least one checkbox checked
      //     var ids = [];
      //     $('.deleteRow').each(function(){
      //       if($(this).is(':checked')) { 
      //         ids.push($(this).val());
      //       }
      //     });
   
      //   $('#delete_team'+ids).show();
        
          
      //   }
      // });



      $(document).on('click','.delete_yes',function() {  
          var ids_string = ids.toString();  // array to string conversion 
          $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>team/teamDelete/",
            data: {data_ids:ids_string},
            success: function(response) {
              //dataTable.draw(); // redrawing datatable
              var emp_ids = response.split(",");
             for (var i=0; i < emp_ids.length; i++ ) {
            
              $("#"+emp_ids[i]).remove(); 
             }
             $(".alert-success-delete").show();
              setTimeout(function() { 
              location.reload(); }, 500);
                },
            //async:false,
          });
   
        });  
   



      // $('#archiveTriger').on("click", function(event){ // triggering delete one by one
       
      //   if( $('.deleteRow:checked').length > 0 ){  // at-least one checkbox checked
      //     var ids = [];
      //     $('.deleteRow').each(function(){
      //       if($(this).is(':checked')) { 
      //         ids.push($(this).val());
      //       }
      //     });

      //     alert(ids);
      //     var id=ids.toString();
      //     $("#Bulkarchive_team_id").val(id);

      //     alert($("#Bulkarchive_team_id").val());

      //   //  $("#Bulkconfirm_archive").addClass('BulkArchive');
      //   //  $("#Bulkarchiveconfirmation").modal('show');


   
      //  // $('#delete_team'+ids).show();
     
   
          
      //   }
      // });



         $(document).on('click','.archive_yes',function() {  
          var ids_string = $("#delete_ids").val();  // array to string conversion 
          $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>team/bulkArchive/",
            data: {data_ids:ids_string},
            success: function(response) {              
             if(response==1)
             {
              location.reload();
              }
             },
            //async:false,
          });
   
        });  


          $(document).on('click','.unarchive_yes',function() {  
          var ids_string = $("#delete_ids").val();  // array to string conversion 
          $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>team/bulkUnArchive/",
            data: {data_ids:ids_string},
            success: function(response) {              
             if(response==1)
             {
              location.reload();
              }
             },
            //async:false,
          });
   
        }); 
   
   });
</script>
<script type="text/javascript">
   function confirm_delete(id)
   {

      $(".message_confirm").html("Do You Want To Delete?");
     $("#button_class").addClass('delete_yes');
      $("#delete_ids").val(id);   // $('#delete_user'+id).show();
   // return false;
   }
   
   $(document).on('click','#close',function(e)
    {
    $('.alert-success').hide();
    return false;
    });
      /** for succesmsg hide **/
   $(document).ready(function(){
     setTimeout(function(){ 
      $('.alert-success').hide(); }, 2000);
   });

    $(document).ready(function(){
      var check=0;
      var check1=0;
      var numCols = $('#alltask thead th').length;   
          task_list = $('#alltask').DataTable({
         "dom": '<"toolbar-table">lfrtip',
       initComplete: function () { 
                  var q=1;
                     $('thead th').find('.filter_check').each( function(){
                       $(this).attr('id',q);
                       q++;
                     });
                  for(i=1;i<numCols;i++){      
                      if(i==1){
                          check=1;          
                          i=Number(i) + 1;
                          var select = $("#1"); 
                      }else if(i==7){
                          check=1;            
                          i=Number(i) + 1;
                          var select = $("#7"); 
                      }else{
                          var select = $("#"+i); 
                      }          
                      this.api().columns([i]).every( function () {
                        var column = this;
                        column.data().unique().sort().each( function ( d, j ) {    
                          console.log(d);         
                          select.append( '<option value="'+d+'">'+d+'</option>' )
                        });
                      });                   
                    if(check=='1'){                 
                       i=Number(i) - 1;
                       check=0;
                    } 
                     $("#"+i).formSelect();  
                  }
        }
    });
      for(j=1;j<numCols;j++){  
          $('#'+j).on('change', function(){  
            var c=$(this).attr('id');         
              var search = [];              
              $.each($('#'+c+ ' option:selected'), function(){                
                  search.push($(this).val());
              });      
              search = search.join('|');             
              if(c==8){               
                c=Number(c) + 1;
              }             
              task_list.column(c).search(search, true, false).draw();  
          });
       }    
});


$("th").on("click.DT", function (e) {        
  if (!$(e.target).hasClass('sortMask')) {        
      e.stopImmediatePropagation();
  }
});

$('th .themicond').on('click', function(e) {
  if ($(this).parent().find(".dropdown-content").hasClass("Show_content")) {
      $(this).parent().find('.dropdown-content').removeClass('Show_content');
  }else{
      $('.dropdown-content').removeClass('Show_content');
      $(this).parent().find('.dropdown-content').addClass('Show_content');
  }
  $(this).parent().find('.select-wrapper').toggleClass('special');
      if( $(this).parent().find('.select-dropdown span input[type="checkbox"]').parent().hasClass('custom_checkbox1')){
      }else{
        $(this).parent().find('.select-dropdown span input[type="checkbox"]').wrap( '<label class="custom_checkbox1"></label>');
        $(this).parent().find('.custom_checkbox1 input').after( "<i></i>" );
      }
});

</script>

<div class="text12345"></div>