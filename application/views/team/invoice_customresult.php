	
	<div class="sample_section">
	<button type="button" class="print_button btn btn-primary" id="print_report">Print</button>
	<div class="export_option">
	<select id="exported_result">
	<option value="">Select</option>
	<option value="pdf">Pdf</option>
	<option value="excel">Excel</option>
	<option value="csv">CSV</option>
	</select>
	</div>
	</div>
	<table id="example" class="display nowrap" style="width:100%">
	<thead>
	<tr>
	<th>S.no</th>    
	<th>Invoice No</th>
	<th>Client Company Name</th> 
	<th>Client Email</th>     
	<th>Invoice Date</th>     
	<th>Invoice Amount</th>    
	</tr>
	</thead>
	<tbody>
	<?php
	$i=1;
	foreach($records as $record){ 
		$record=$this->Report_model->email_find($record['client_email']);
	?>
	<tr>
		<td><?php echo $i; ?></td>	
		<td><?php echo $record['invoice_no']; ?></td>	
		<td><?php echo $record['crm_company_name']; ?></td>	
		<td><?php echo $record['crm_email']; ?></td>	
		<td><?php echo date('Y-m-d',$record['invoice_date']); ?></td>	
		<td><?php echo $record['grand_total']; ?></td>	

	</tr>
	<?php } ?>
	</tbody>
	</table>