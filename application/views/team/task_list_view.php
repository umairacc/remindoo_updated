<?php $this->load->view('includes/header'); ?>
 <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<style type="text/css">
  .add_class{
    display: none;
  }
  .dropdown-content {
    display: none;
    position: absolute;
    background-color: #fff;
    min-width: 86px;
    overflow: auto;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    left: -92px;
    width: 150px;
}

  tfoot {
    display: table-header-group;
}



</style>
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">

<div class="modal fade" id="my_Modal" role="dialog">
    <div class="modal-dialog">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
        <input type="hidden" name="archive_task_id" id="archive_task_id" value="">
        <input type="hidden" name="status_value" id="status_value" value="">
          <p>Do You Want Archive this Task?</p>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal" onclick="archive_action()">yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>


<div class="modal fade" id="deleteconfirmation" role="dialog">
    <div class="modal-dialog">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
        <input  type="hidden" name="delete_task_id" id="delete_task_id" value="">
          <p> Are you sure want to delete ?</p>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default" id="confirm_delete" data-dismiss="modal" onclick="delete_action()">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
</div>
<?php
   $role = $this->Common_mdl->getRole($_SESSION['id']);
   function convertToHoursMins($time, $format = '%02d:%02d') {
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
   }

   function calculate_test($a,$b){
$difference = $b-$a;

$second = 1;
$minute = 60*$second;
$hour   = 60*$minute;

$ans["hour"]   = floor(($difference)/$hour);
$ans["minute"] = floor((($difference)%$hour)/$minute);
$ans["second"] = floor(((($difference)%$hour)%$minute)/$second);
//echo  $ans["hour"] . " hours, "  . $ans["minute"] . " minutes, " . $ans["second"] . " seconds";

$test=$ans["hour"].":".$ans["minute"].":".$ans["second"];
return $test;
}
function time_to_sec($time) {
list($h, $m, $s) = explode (":", $time);
$seconds = 0;
$seconds += (intval($h) * 3600);
$seconds += (intval($m) * 60);
$seconds += (intval($s));
return $seconds;
}
function sec_to_time($sec) {
return sprintf('%02d:%02d:%02d', floor($sec / 3600), floor($sec / 60 % 60), floor($sec % 60));
}    

/*find Column settings*/
$column_name = ['timer','subject','start_date','due_date','status','priority','tag','assignto','services','company_name','Due'];
$columSettings=[];
foreach ($column_name as $cn) 
{
  $columSettings[$cn]=$this->Common_mdl->get_column('task_column_settings','user_id','1',$cn);  
}
/*find Column settings*/
?>

<?php 
if($_SESSION['role']==6 || $_SESSION['role']==5)
{
  ?>
  <style type="text/css">
    /** 04-07-2018 rs **/
.for_user_permission_assign{
  display: none;
}
.for_user_permission_delete
{
  display: none;
}
/** end of 04-07-2018 **/
  </style>
  <?php
}
?>
<style>
/** 28-06-2018 **/
.time{
      padding: 8px;
    font-weight: bold;
}
.new_play_stop{
      width: 20px;
    height: 20px;
    background: #000;
    color: #fff;
    line-height: initial;
    text-align: center;
    font-size: 15px;
    vertical-align: top;
    border-radius: 50%;
    margin: 7px 0 0 0;
}
/** for timer style 28-06-2018 **/
   /*span.newonoff {
   background: #4680ff;
   padding: 6px 15px;
   display: inline-block;
   color: #fff;
   border-radius: 5px;
   }*/
   /** 04-06-2018 **/
   select.close-test-04{
    display: none;
   }
   /** 04-06-2018 **/
   span.demo {
   padding: 0 10px;
   }
   button.btn.btn-info.btn-lg.newonoff {
   padding: 3px 10px;
   height: initial;
   font-size: 15px;
   border-radius: 5px;
   }
   img.user_imgs {
   width: 38px;
   height: 38px;
   border-radius: 50%;
   display: inline-block;
   }


a.adduser1 {
    background: #38b87c;
    color: #fff;
    padding: 3px 7px 3px;
    border-radius: 6px;
    vertical-align: middle;
    display: inline-block;
}
   #adduser label {
   text-transform: capitalize;
   font-size: 14px;
   }
  span.hours-left {
    background: #f76480;
    color: #fff;
    padding: 3px 10px;
    border-radius: 3px;
    margin-right: 15px;
}
   .dropdown12 button.btn.btn-primary {
   background: #ccc;
   color: #555;
   font-size: 13px;
   padding: 4px 10px 4px;
   margin-top: -2px;
   border-color:transparent;
   }
   select + .dropdown:hover .dropdown-menu
   {
   display: none;
   }
   select + .dropdown12 .dropdown-menu
   {
   padding: 0 !important;
   }
   select + .dropdown12 .dropdown-menu a {
   color: #000 !important;
   padding: 7px 10px !important;
   border: none !important;
   font-size: 14px;
   }
   select + .dropdown12 .dropdown-menu li {
   border: none !important;
   padding: 0px !important;
   }
   select + .dropdown12 .dropdown-menu a:hover {
   background:#4c7ffe ;
   color: #fff !important;
   }
   span.created-date {
   color: gray;
   padding-left: 10px;
   font-size: 13px;
   font-weight: 600;
   }
   span.created-date i.fa.fa-clock-o {
   padding: 0 5px;
   }
   .dropdown12 span.caret {
   right: -2px;
   z-index: 99999;
   border-top: 4px solid #555;
   border-left: 4px solid transparent;
   border-right: 4px solid transparent;
   border-bottom: 4px solid transparent;
   top: 12px;
   position: relative;
   }
   span.timer {
   padding: 0 10px;
   }
   body {
   font-family:"Arial", Helvetica, sans-serif;
   text-align: center;
   }
   #controls{
   font-size: 12px;
   }
   #time {
   font-size: 150%;
   }
  
</style>
<!-- dynamic values passed in css -->

<style type="text/css">
  <?php 
   ?>
</style>
<?php 
  /** hint **/
  /* $custom_permission -> for permission overall
     $for_user_edit_permission -> for edit permission 
  */
     $for_main_task_timer=array();
     $for_user_edit_per=array();     
     if(count($for_user_edit_permission)>0){
     foreach ($for_user_edit_permission as $edit_per_key => $edit_per_value) {
      array_push($for_user_edit_per, $edit_per_value['id']);
     }
     }
?>
<!-- end of 29-06-2018 -->
<div class="modal-alertsuccess alert succ popup_info_msg" style="display:none;">
         <div class="newupdate_alert"><a href="#" class="close" id="close_info_msg">×</a>
         <div class="pop-realted1">
         <div class="position-alert1">
               Please! Select Record...
         </div></div>
         </div>
</div>
 
<div id="action_result" class="modal-alertsuccess alert succ dashboard_success_message" style="display:none;">
       <div class="newupdate_alert">  <a href="#" class="close" id="close_action_result">×</a>
         <div class="pop-realted1">
         <div class="position-alert1">
              Success !!! Staff Assign have been changed successfully...
         </div></div>
         </div>
</div>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/buttons.dataTables.min.css?ver=2">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/progress-circle.css">
<?php 
$team = $this->db->query("select * from team where create_by=".$_SESSION['id']." ")->result_array();

$department=$this->db->query("select * from department_permission where create_by=".$_SESSION['id']." ")->result_array();
?>
<div class="pcoded-content card-removes">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card">
                        <!-- admin start-->
                        <div class="client_section col-xs-12 floating_set newtaslcls">
                        <input type="hidden" name="assigneed_value" id="assigneed_value" value="">
                         
                              </div> <!-- all-clients -->
                           <div class="all_user-section floating_set clientredesign">
                              <div class="deadline-crm1 floating_set tsk-crte">
                                <ul class="nav nav-tabs1 all_user1 md-tabs pull-left u-dashboard">
                                    <li class="nav-item">
                                       <a class="nav-link active" href="<?php echo base_url();?>user/task_list">
                                        <img class="themicon" src="<?php echo base_url();?>assets/images/tabicon1.png" alt="themeicon" /> All Tasks</a>
                                       <div class="slide"></div>
                                    </li>
                                
                                    <li class="nav-item">
                                       <a class="nav-link "  href="<?php echo base_url(); ?>user/new_task">
                                       <img class="themicon" src="<?php echo base_url();?>assets/images/tabicon2.png" alt="themeicon" />Create New task</a>
                                       <div class="slide"></div>
                                    </li>
                               
                                    <li class="nav-item">
                                    
                                      <a class="nav-link" data-toggle="modal" data-target="#import-task">
                                      <img class="themicon" src="<?php echo base_url();?>assets/images/tabicon3.png" alt="themeicon" />Import tasks</a> 
                                       <div class="slide"></div>
                                    </li>
                              
                                    <li class="nav-item">
                                      <!--  <a class="nav-link "  href="<?php echo base_url();?>tasksummary/import_task">Import tasks</a> -->
                                      <a class="nav-link" data-toggle="modal" data-target="#service_task_popup"> <img class="themicon" src="<?php echo base_url();?>assets/images/tabicon3.png" alt="themeicon" />Task From Services</a> 
                                      <div class="slide"></div>
                                    </li>
                                                                   
                                    <li class="nav-item">
                                       <a class="nav-link "  href="<?php echo base_url(); ?>user/task_list_kanban">
                                        <img class="themicon" src="<?php echo base_url();?>assets/images/tabicon4.png" alt="themeicon" />Switch To Kanban</a>
                                       <div class="slide"></div>
                                    </li>
                                 </ul>
                                <div class="taskpagedashboard">
                                  <?php 
                                  $team_num=array();
                                           $for_cus_team=$this->db->query("select * from team_assign_staff where FIND_IN_SET('".$_SESSION['id']."',staff_id)")->result_array();
                                  foreach ($for_cus_team as $cu_team_key => $cu_team_value) {
                                      array_push($team_num,$cu_team_value['team_id']);
                                  }
                                  if(!empty($team_num) && count($team_num)>0){
                                  $res_team_num=implode('|', $team_num);
                                  }
                                  else
                                  {
                                      $res_team_num=0;
                                  }

                                        $department_num=array();
                                        $for_cus_department=$this->db->query("SELECT * FROM `department_assign_team` where CONCAT(',', `team_id`, ',') REGEXP ',($res_team_num),'")->result_array();
                                        foreach ($for_cus_department as $cu_dept_key => $cu_dept_value) {
                                              array_push($department_num,$cu_dept_value['depart_id']);
                                          }
                                      if(!empty($department_num) && count($department_num)>0){
                                        $res_dept_num=implode('|', $department_num);
                                      }
                                      else
                                      {
                                          $res_dept_num=0;
                                      }
                                  /** for permission 16-07-2018 **/
                                  $it_session_id='';
                                  $get_this_qry=$this->db->query("select * from user where id=".$_SESSION['id'])->row_array();
                                  $created_id=$get_this_qry['firm_admin_id'];
                                  $its_firm_admin_id=$get_this_qry['firm_admin_id'];
                                  if($_SESSION['role']==6) // staff
                                  {
                                  $it_session_id=$_SESSION['id'];
                                  }
                                  else if($_SESSION['role']==5) //manager
                                  {
                                  $it_session_id=$created_id;
                                  }
                                  else if($_SESSION['role']==4)//client
                                  {
                                  $it_session_id=$created_id;
                                  }
                                  else if($_SESSION['role']==3) //director
                                  {
                                  $it_session_id=$created_id;
                                  }
                                  else if($_SESSION['role']==2) // sub admin
                                  {
                                    $it_session_id=$created_id;
                                  }
                                  else
                                  {
                                    $it_session_id=$_SESSION['id'];
                                  }                                
                                  /** end of permission 16-07-2018 **/
                                  $notstarted='';
                                  $inprogress='';
                                  $awaiting='';
                                  $testing='';
                                  $complete='';
                                  ///for only task  //
                                  $status_array=array("notstarted","inprogress","awaiting","complete","archive");

                                      foreach ($status_array as $st_key => $st_value) {
                                     if($_SESSION['role']==6)
                                  {
                                    /*${$st_value}=$this->db->query("SELECT * FROM add_new_task tl where tl.create_by=".$it_session_id." and tl.task_status='".$st_value."'
                                  UNION 
                                  SELECT * FROM add_new_task tls where tls.create_by!=".$it_session_id." and (FIND_IN_SET('".$it_session_id."',worker) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `department`, ',') REGEXP ',($res_team_num),') or FIND_IN_SET('".$it_session_id."',manager) ) and tls.task_status='".$st_value."' and tls.related_to='tasks' ")->result_array(); for sub task only calculation*/


                                  ${$st_value}=$this->db->query("SELECT * FROM add_new_task tl where tl.create_by=".$it_session_id." and tl.task_status='".$st_value."'
                                  UNION 
                                  SELECT * FROM add_new_task tls where tls.create_by!=".$it_session_id." and (FIND_IN_SET('".$it_session_id."',worker) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `department`, ',') REGEXP ',($res_team_num),') or FIND_IN_SET('".$it_session_id."',manager) ) and tls.task_status='".$st_value."' and tls.sub_task_id=''")->result_array(); 

                                  $rq_review_send=$this->db->query("select * from Manager_notification as mn INNER join add_new_task ant on ant.id=mn.task_id and ant.task_status!='complete' where mn.user_id=$_SESSION[id] and mn.review_status=0 ")->result_array();
                                  $rq_review_accept=$this->db->query("select * from Manager_notification where user_id=$_SESSION[id] and review_status=1")->result_array();
                                  $rq_review_decline=$this->db->query("select * from Manager_notification as mn INNER join add_new_task ant on ant.id=mn.task_id and ant.task_status!='complete' where mn.user_id=$_SESSION[id] and mn.review_status=2")->result_array ();

                                  }
                                  else if($_SESSION['role']==4)
                                  {
                                      $result=array();
                                     array_push($result, $it_session_id);
                                     $reassign_firm_id=$its_firm_admin_id;
                                   $tot_val=1;
                                      for($z=0;$z<$tot_val;$z++)
                                      {
                                        if($reassign_firm_id==0)
                                        {
                                          $tot_val=0;
                                        }
                                        else
                                        {
                                            $get_this_qry=$this->db->query("select * from user where id=".$reassign_firm_id)->row_array();
                                            if(!empty($get_this_qry)){
                                               array_push($result, $get_this_qry['id']);
                                            $new_created_id=$get_this_qry['firm_admin_id'];
                                            $reassign_firm_id=$get_this_qry['firm_admin_id'];
                                            }
                                            else
                                            {
                                              $tot_val=0;
                                            }
                                           
                                        }

                                      }
                                       $res=(!empty($result))?implode(',',array_filter(array_unique($result))):'';
                                    $sql=$this->db->query("select * from client where user_id=".$_SESSION['id']." ")->row_array();
                                    $its_id=$sql['id'];
                                    ${$st_value}=$this->db->query("SELECT * FROM add_new_task tl where tl.company_name=".$its_id." and tl.task_status='".$st_value."' and tl.related_to='tasks'")->result_array();

                                  }
                                  else if($_SESSION['role']==5)
                                  {
                                     ${$st_value}=$this->db->query("SELECT * FROM add_new_task tl where tl.create_by=".$it_session_id." and tl.task_status='".$st_value."'
                                  UNION
                                  SELECT * FROM add_new_task tls where tls.create_by!=".$it_session_id." and (FIND_IN_SET('".$it_session_id."',worker) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `department`, ',') REGEXP ',($res_team_num),') or FIND_IN_SET('".$it_session_id."',manager) ) and tls.task_status='".$st_value."' and tls.related_to='tasks' ")->result_array();
                                  }
                                  else if($_SESSION['role']==3 || $_SESSION['role']==2)
                                  {
                                    $result=array();
                                    $reassign_firm_id=$its_firm_admin_id;

                                      array_push($result, $it_session_id);

                                      $tot_val=1;
                                      for($z=0;$z<$tot_val;$z++)
                                      {
                                        if($reassign_firm_id==0)
                                        {
                                          $tot_val=0;
                                        }
                                        else
                                        {
                                            $get_this_qry=$this->db->query("select * from user where id=".$reassign_firm_id)->row_array();
                                            if(!empty($get_this_qry)){
                                               array_push($result, $get_this_qry['id']);
                                            $new_created_id=$get_this_qry['firm_admin_id'];
                                            $reassign_firm_id=$get_this_qry['firm_admin_id'];
                                            }
                                            else
                                            {
                                              $tot_val=0;
                                            }
                                           
                                        }

                                      }

                                    $res=(!empty($result))?implode(',',array_filter(array_unique($result))):'';
                                   
                                  ${$st_value}=$this->db->query("SELECT * FROM add_new_task tl where tl.create_by in ($res) and tl.task_status='".$st_value."' and tl.related_to='tasks' ")->result_array();
                                  }
                                  else
                                  {
                                     $result=array();
                                     array_push($result, $it_session_id);
                                     $reassign_firm_id=$its_firm_admin_id;
                                   $tot_val=1;
                                      for($z=0;$z<$tot_val;$z++)
                                      {
                                        if($reassign_firm_id==0)
                                        {
                                          $tot_val=0;
                                        }
                                        else
                                        {
                                            $get_this_qry=$this->db->query("select * from user where id=".$reassign_firm_id)->row_array();
                                            if(!empty($get_this_qry)){
                                               array_push($result, $get_this_qry['id']);
                                            $new_created_id=$get_this_qry['firm_admin_id'];
                                            $reassign_firm_id=$get_this_qry['firm_admin_id'];
                                            }
                                            else
                                            {
                                              $tot_val=0;
                                            }
                                           
                                        }

                                      }
                                       $res=(!empty($result))?implode(',',array_filter(array_unique($result))):'';
                                   
                                  ${$st_value}=$this->db->query("SELECT * FROM add_new_task tl where tl.create_by in ($res) and tl.task_status='".$st_value."' and tl.related_to='tasks' ")->result_array();
                                     
                                  }

                                  }

                                  $our_notstarted=$this->db->query("SELECT * FROM add_new_task tls where tls.create_by!=".$_SESSION['id']." and (FIND_IN_SET('".$_SESSION['id']."',worker) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `department`, ',') REGEXP ',($res_team_num),') or FIND_IN_SET('".$_SESSION['id']."',manager) ) and  tls.task_status='notstarted'")->result_array();

                                  $our_inprogress=$this->db->query("SELECT * FROM add_new_task tls where tls.create_by!=".$_SESSION['id']." and (FIND_IN_SET('".$_SESSION['id']."',worker) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `department`, ',') REGEXP ',($res_team_num),') or FIND_IN_SET('".$_SESSION['id']."',manager) ) and  tls.task_status='inprogress'  ")->result_array();

                                  $our_awaiting=$this->db->query("SELECT * FROM add_new_task tls where tls.create_by!=".$_SESSION['id']." and (FIND_IN_SET('".$_SESSION['id']."',worker) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `department`, ',') REGEXP ',($res_team_num),') or FIND_IN_SET('".$_SESSION['id']."',manager) ) and  tls.task_status='awaiting'  ")->result_array();

                                  $our_testing=$this->db->query("SELECT * FROM add_new_task tls where tls.create_by!=".$_SESSION['id']." and (FIND_IN_SET('".$_SESSION['id']."',worker) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `department`, ',') REGEXP ',($res_team_num),') or FIND_IN_SET('".$_SESSION['id']."',manager) ) and  tls.task_status='testing'  ")->result_array();

                                  $our_complete=$this->db->query("SELECT * FROM add_new_task tls where tls.create_by!=".$_SESSION['id']." and (FIND_IN_SET('".$_SESSION['id']."',worker) or (CONCAT(',', `team`, ',') REGEXP ',($res_team_num),') or (CONCAT(',', `department`, ',') REGEXP ',($res_team_num),') or FIND_IN_SET('".$_SESSION['id']."',manager) ) and  tls.task_status='complete'  ")->result_array();                                          
                                          $role = $this->Common_mdl->getRole($_SESSION['id']);   
                                          $assignNotstarted = 0;
                                          $assignInprogress = 0;
                                          $assignAwaiting   = 0;
                                          $assignTesting    = 0;
                                          $assignComplete   = 0;
                                          $assignArchive    = 0;
                                          $assignNotstarted = count($our_notstarted);
                                          $assignInprogress = count($our_inprogress);
                                          $assignAwaiting   = count($our_awaiting);
                                          $assignTesting    = count($our_testing);
                                          $assignComplete   = count($our_complete);                                         
                                  ?>
                                      <div class="top-leads fr-task cfssc">
                                      <div class="lead-data1 pull-right tsk-animation1">
                                        <div class="junk-lead tsk-color1 color-junk2 col-xs-12 col-sm-6 col-md-2 task_status_val nots_task" data-id="notstarted">
                                            <div class="lead-point1">
                                            <strong><?php echo count($notstarted);?></strong>
                                            <span class="task_status_val_1 status_filter" data-id="notstarted">Not started</span>
                                            <?php if($role=='Staff'){?>
                                            <p>Task assigned to me: <?php echo $assignNotstarted;?></p>
                                            <?php } ?>
                                            </div>                                           
                                          </div>  
                                          
                                          <div class="junk-lead tsk-color2 color-junk2 col-xs-12 col-sm-6 col-md-2 task_status_val inpro_task" data-id="inprogress">
                                            <div class="lead-point1">
                                            <strong><?php echo count($inprogress);?></strong>
                                            <span  class="task_status_val_1 status_filter" data-id="inprogress">In Progress</span>
                                            <?php if($role=='Staff'){?>
                                            <p>Task assigned to me: <?php echo $assignInprogress;?></p>
                                            <?php } ?>
                                            </div>
                                          </div>  
                                          
                                          
                                          
                                          <div class="junk-lead tsk-color4 color-junk1 color-junk2 col-xs-12 col-sm-6 col-md-2 task_status_val await_task" data-id="awaiting">
                                            <div class="lead-point1">
                                            <strong><?php echo count($awaiting);?></strong>
                                            <span  class="task_status_val_1 status_filter" data-id="awaiting">Awaiting</span>
                                            <?php if($role=='Staff'){?>
                                            <p>Task assigned to me: <?php echo $assignAwaiting;?></p>
                                            <?php } ?>
                                            </div>
                                          </div>

                                          <div class="junk-lead tsk-color5 color-junk2 col-xs-12 col-sm-6 col-md-2 task_status_val comple_task" data-id="complete">
                                            <div class="lead-point1">
                                            <strong><?php echo count($complete);?></strong>
                                            <span  class="task_status_val_1 status_filter" data-id="complete">Complete</span>
                                            <?php if($role=='Staff'){?>
                                            <p>Task assigned to me: <?php echo $assignComplete;?></p>
                                            <?php } ?>
                                            </div>
                                          </div>

                                            <div class="junk-lead tsk-color6 color-junk2 col-xs-12 col-sm-6 col-md-2 task_status_val archived_task" data-id="archive">
                                            <div class="lead-point1">
                                            <strong><?php echo count($archive);?></strong>
                                            <span  class="task_status_val_1 status_filter" data-id="archive">Archive</span>
                                            <?php if($role=='Staff'){?>
                                            <p>Task assigned to me: <?php echo $assignArchive;?></p>
                                            <?php } ?>
                                            </div>
                                          </div> 
                                        <?php 
                                  if($_SESSION['role']==6)
                                  {
                                  $st_id=implode(',',array_column($rq_review_send,'task_id'));
                                  $at_id=implode(',', array_column($rq_review_accept,'task_id'));
                                  $dt_id=implode(',', array_column($rq_review_decline,'task_id'));

                                  ?>
                                          <div class="junk-lead tsk-color5 color-junk2 col-xs-12 col-sm-6 col-md-2 task_status_val sub_status" data-id="<?=$st_id?>">
                                            <div class="lead-point1">
                                            <strong><?php echo count($rq_review_send);?></strong>
                                            <span  class="task_status_val_1 status_filter" data-id="<?=$st_id?>">Request</span>
                                            <?php if($role=='Staff'){?>
                                            <p>Request Manager To Review Task: <?php echo count($rq_review_send);?></p>
                                            <?php } ?>
                                            </div>
                                          </div>

                                          <div class="junk-lead tsk-color5 color-junk2 col-xs-12 col-sm-6 col-md-2 task_status_val sub_status" data-id="<?=$at_id?>">
                                            <div class="lead-point1">
                                            <strong><?php echo count($rq_review_accept);?></strong>
                                            <span  class="task_status_val_1 status_filter" data-id="<?=$at_id?>">Accpted</span>
                                            <?php if($role=='Staff'){?>
                                            <p>Manager Accpted Task: <?php echo count($rq_review_accept);?></p>
                                            <?php } ?>
                                            </div>
                                          </div>

                                          <div class="junk-lead tsk-color5 color-junk2 col-xs-12 col-sm-6 col-md-2 task_status_val sub_status" data-id="<?=$dt_id?>">
                                            <div class="lead-point1">
                                            <strong><?php echo count($rq_review_decline);?></strong>
                                            <span  class="task_status_val_1 status_filter" data-id="<?=$dt_id?>">Decline</span>
                                            <?php if($role=='Staff'){?>
                                            <p>Manager Decline Task: <?php echo count($rq_review_decline);?></p>
                                            <?php } ?>
                                            </div>
                                          </div>
                                  <?php  } ?> 
                                        </div>
                                      </div>
                                </div>

                                 <div class="f-right">
                                 
                                   <!-- <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal">Open Modal</button> -->
                                  <div class="assign_delete">
                                    <button type="button"  data-toggle="modal" data-target="#task_assign" id="assign_member" class="btn   for_user_permission_assign" style="display: none;">Assign </button>

                                    <button type="button" data-toggle="modal" data-target="#deleteconfirmation" id="delete_task" class="del-tsk12 for_user_permission_delete" style="display: none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i> Delete </button>

                                    <button type="button" id="archive_task" class="archive_task_button del-tsk12 " data-status="archive" data-toggle="modal" data-target="#my_Modal" style="display: none;"><i class="fa fa-archive fa-6" aria-hidden="true"></i>Archive </button>

                                    <button type="button" id="unarchive_task" class="archive_task_button del-tsk12 " data-status="unarchive" style="display: none;"><i class="fa fa-archive fa-6" aria-hidden="true"></i>Unarchive </button>
                                   </div>
                                  </div>
                              </div>
                              <div class="all_user-section2 floating_set <?php if($_SESSION['permission']['Task']['view']!='1'){ ?> permission_deined<?php } ?>" >
                                 <div class="tab-content">
                                    <div id="alltasks" class="data_active_task tab-pane fade in active">
                                    <div class="all_task_counts">
                                    </div>
                                    <input type="hidden" name="for_status_us" id="for_status_us">                                     
                                       <div class="client_section3  floating_set">
                                          <div id="status_succ"></div>
                                          <div class="all-usera1  user-dashboard-section1 for_task_status">
                                            <?php
                                            foreach ($task_list as $tre_key => $tre_value) {
                                            $time=json_decode($tre_value['counttimer']);
                                            $time_start_date =$tre_value['time_start_date'];
                                            $time_end_date=$tre_value['time_end_date'];
                                            $hours=0;
                                            $mins=0;
                                            $sec=0;
                                            $pause='';
                                            if($_SESSION['role']==6)
                                            {
                                                $individual_timer=$this->db->query("select * from individual_task_timer where user_id=".$_SESSION['id']." and task_id=".$tre_value['id']." ")->row_array();
                                            if(count($individual_timer)>0)
                                            {
                                              if($individual_timer['time_start_pause']!=''){
                                                $res=explode(',',$individual_timer['time_start_pause']);
                                                //$res=array(1529919001,1530077528,1530080810,1530080817,1530080930,1530080933);
                                                $res1=array_chunk($res,2);
                                                $result_value=array();
                                                $pause='on';
                                                foreach($res1 as $rre_key => $rre_value)
                                                {
                                                   $abc=$rre_value;
                                                   if(count($abc)>1){
                                                   if($abc[1]!='')
                                                   {
                                                      $ret_val=calculate_test($abc[0],$abc[1]);
                                                      array_push($result_value, $ret_val) ;
                                                   }
                                                   else
                                                   {
                                                    $pause='';
                                                      $ret_val=calculate_test($abc[0],time());
                                                       array_push($result_value, $ret_val) ;
                                                   }
                                                  }
                                                  else
                                                  {
                                                    $pause='';
                                                      $ret_val=calculate_test($abc[0],time());
                                                       array_push($result_value, $ret_val) ;
                                                  }
                                                }

                                                $time_tot=0;
                                                 foreach ($result_value as $re_key => $re_value) {
                                                    $time_tot+=time_to_sec($re_value) ;
                                                 }
                                                 $hr_min_sec=sec_to_time($time_tot);
                                                 $hr_explode=explode(':',$hr_min_sec);
                                                 $hours=(int)$hr_explode[0];
                                                 $min=(int)$hr_explode[1];
                                                 $sec=(int)$hr_explode[2]; 
                                                }
                                                else
                                                {
                                                  $hours=0;
                                                  $min=0;
                                                  $sec=0;
                                                  $pause='on';
                                                }
                                            }
                                            else
                                            {
                                                $hours=0;
                                                $min=0;
                                                $sec=0;
                                                $pause='on';
                                            }
                                            }
                                            else
                                            {
                                             // echo "select * from individual_task_timer where task_id=".$tre_value['id']." ";
                                            /** non staff members shown timer **/
                                            $individual_timer=$this->db->query("select * from individual_task_timer where task_id=".$tre_value['id']." ")->result_array();
                                            $pause_val='on';
                                            $pause='on';
                                            $for_total_time=0;
                                            if(count($individual_timer)>0){
                                            foreach ($individual_timer as $intime_key => $intime_value) {
                                            $its_time=$intime_value['time_start_pause'];
                                            $res=explode(',', $its_time);
                                              $res1=array_chunk($res,2);
                                            $result_value=array();
                                            //  $pause='on';
                                            foreach($res1 as $rre_key => $rre_value)
                                            {
                                               $abc=$rre_value;
                                               if(count($abc)>1){
                                               if($abc[1]!='')
                                               {
                                                  $ret_val=calculate_test($abc[0],$abc[1]);
                                                  array_push($result_value, $ret_val) ;
                                               }
                                               else
                                               {
                                                $pause='';
                                                $pause_val='';
                                                  $ret_val=calculate_test($abc[0],time());
                                                   array_push($result_value, $ret_val) ;
                                               }
                                              }
                                              else
                                              {
                                                $pause='';
                                                $pause_val='';
                                                  $ret_val=calculate_test($abc[0],time());
                                                   array_push($result_value, $ret_val) ;
                                              }
                                            }
                                            // $time_tot=0;
                                             foreach ($result_value as $re_key => $re_value) {
                                                //$time_tot+=time_to_sec($re_value) ;
                                                $for_total_time+=time_to_sec($re_value);
                                             }

                                            }
                                            //echo $for_total_time."val";
                                            $for_main_task_timer[$tre_value['id']]=$for_total_time;

                                            $hr_min_sec=sec_to_time($for_total_time);
                                             $hr_explode=explode(':',$hr_min_sec);
                                              $hours=(int)$hr_explode[0];
                                              $min=(int)$hr_explode[1];
                                             $sec=(int)$hr_explode[2];

                                            }
                                            else
                                            {
                                              $hours=0;
                                              $min=0;
                                              $sec=0;
                                              $pause='on';
                                            }
                                        }
                                        //print_r($for_main_task_timer);echo "dfsdfssssssssssssssssssssssss";
                                    ?>
                                  <input type="hidden" name="trhours_<?php echo $tre_value['id'];?>" id="trhours_<?php echo $tre_value['id'];?>" value='<?php echo $hours; ?>' >
                                  <input type="hidden" name="trmin_<?php echo $tre_value['id'];?>" id="trmin_<?php echo $tre_value['id'];?>" value='<?php echo $min;?>' >
                                  <input type="hidden" name="trsec_<?php echo $tre_value['id'];?>" id="trsec_<?php echo $tre_value['id'];?>" value='<?php echo $sec; ?>' >
                                  <input type="hidden" name="trmili_<?php echo $tre_value['id'];?>" id="trmili_<?php echo $tre_value['id'];?>" value='0' >
                                  <input type="hidden" name="trpause_<?php echo $tre_value['id'];?>" id="trpause_<?php echo $tre_value['id'];?>" value="<?php echo $pause; ?>" >
                                  <?php                             }                                  ?>


                                  <table class="table client_table1 all_task_table text-center display nowrap tasklistseccls" id="alltask" cellspacing="0" width="100%" data-status="notstarted">
                                      <thead>
                                      <tr class="text-uppercase">
                                        
                                      <th <?php if($_SESSION['permission']['Task_Sub_Task']['view']!='1'){ echo "style='display:none'";} ?> >

                                      </th>
                                      
                                      <th> <div class="checkbox-fade fade-in-primary">
									  <label class="custom_checkbox1"><input type="checkbox" id="select_alltask">
                                       <i></i> </label>
                                      </div> 
                                      </th>
                                      <th <?php echo $columSettings['timer']?> >Timer
                                      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                      <div class="sortMask"></div> 
                                      <select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
                                      </th>                                     
                                      <th <?php echo $columSettings['subject']; ?> >Subject
                                      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                      <div class="sortMask"></div> 
                                      <select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
                                      </th>
                                      <th class="add_class" style="display: none;">Subject
                                      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                      <div class="sortMask"></div> 
                                      <select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
                                      </th>                                    
                                      <th <?php echo $columSettings['start_date']; ?>>Start Date 
                                      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                      <div class="sortMask"></div> 
                                      <select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
                                      </th>
                                      <th <?php echo $columSettings['due_date']; ?>>Due Date
                                      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                      <div class="sortMask"></div> 
                                      <select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
                                      </th>
                                      <th <?php echo $columSettings['status']; ?>>Status
                                      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                      <div class="sortMask"></div> 
                                      <select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
                                      </th>
                                      <th <?php echo $columSettings['priority']; ?>>Priority
                                      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                      <div class="sortMask"></div> 
                                      <select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
                                      </th>
                                      <th <?php echo $columSettings['tag']; ?>>Tag 
                                      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
                                      <div class="sortMask"></div> 
                                      <select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
                                      </th>
                                      <th <?php echo $columSettings['assignto']; ?>>Assignto 
                                      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                      <div class="sortMask"></div> 
                                      <select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
                                      </th>
                                      <th style="display: none;">Assignto 
                                      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                      <div class="sortMask"></div> 
                                      <select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
                                      </th>    
                                       <th <?php echo $columSettings['services']; ?>>Related Services 
                                      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                      <div class="sortMask"></div> 
                                      <select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
                                      </th>
                                      <th <?php echo $columSettings['company_name']; ?>>Company Name 
                                      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
                                      <div class="sortMask"></div> 
                                      
                                      <select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
                                      </th>                                     
                                      <th <?php echo $columSettings['Due']; ?> >Due                                      
                                      </th>
                                      <th>Actions  
                                      </th>
                                      </tr>
                                      </thead>                

                                      <tfoot>
                                      <tr class="text-uppercase">
                                      <th <?php if($_SESSION['permission']['Task_Sub_Task']['view']!='1'){ echo "style='display:none'";} ?>>

                                      </th>
                                      <th>  
                                      </th>
                                      <th>
                                      <select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
                                      </th>                                     
                                      <th>
                                      <select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
                                      </th>
                                      <th class="add_class" style="display: none;">
                                      <select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
                                      </th>                                    
                                      <th> 
                                      <select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
                                      </th>
                                      <th>
                                      <select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
                                      </th>
                                      <th>
                                      <select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
                                      </th>
                                      <th>
                                      <select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
                                      </th>
                                      <th> 
                                      <select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
                                      </th>
                                      <th> 
                                      <select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
                                      </th>
                                      <th style="display: none;"> 
                                      <select multiple="true" class="filter_check" id="" style="display: none;"> </select> 
                                      </th>
                                       <th>  
                                      </th>
                                      <th>                                      
                                      </th>
                                      </tr>
                                      </tfoot>  


                                                                                        
                                      <tbody>
                                      <?php 
                                      foreach ($task_list as $key => $value) 
                                      {
                                       if($value['related_to']=='magic task' && $_SESSION['role']!=5 &&  $_SESSION['role']!=6)
                                        {
                                          continue; //don't show subtask to admin,etc..
                                        }
                                        if(!empty($value['sub_task_id']) && ($_SESSION['role']==6 || $_SESSION['role']==5))
                                        {
                                          continue; //for don't show main task to manager and staff
                                        }

                                          error_reporting(0);                                   
                                          if($value['start_date']!='' && $value['end_date']!=''){    
                                            $start  = date_create(implode('-', array_reverse(explode('-', $value['start_date']))));
                                            $end  = date_create(implode('-', array_reverse(explode('-', $value['end_date']))));
                                            $diff    = date_diff ( $start, $end );
                                            $y =  $diff->y;
                                            $m =  $diff->m;
                                            $d =  $diff->d;
                                            $h =  $diff->h;
                                            $min =  $diff->i;
                                            $sec =  $diff->s;
                                          }else{
                                            $y =  0;
                                            $m =  0;
                                            $d =  0;
                                            $h =  0;
                                            $min = 0;
                                            $sec =  0;
                                          }

                                      $date_now = date("Y-m-d"); // this format is string comparable                                   
                                      $d_rec = date('Y-m-d',strtotime($value['end_date']));
                                      if ($date_now > $d_rec) {                                     
                                          $d_val = "EXPIRED";
                                      }else{
                                          $d_val = $y .' years '. $m .' months '. $d .' days '. $h .' Hours '. $min .' min '. $sec .' sec ';
                                      }
                                      if($value['worker']=='')
                                      {
                                      $value['worker'] = 0;
                                      }
                                      $staff=$this->db->query("SELECT * FROM staff_form WHERE user_id in (".$value['worker'].")")->result_array();
                                      if($value['task_status']=='notstarted')
                                      {
                                        $percent = 0;
                                        $stat = 'Not Started';
                                      } if($value['task_status']=='inprogress')
                                      {
                                        $percent = 25;
                                        $stat = 'In Progress';
                                      } if($value['task_status']=='awaiting')
                                      {
                                        $percent = 50;
                                        $stat = 'Awaiting for a feedback';
                                      } if($value['task_status']=='testing')
                                      {
                                        $percent = 75;
                                        $stat = 'Testing';
                                      } if($value['task_status']=='complete')
                                      {
                                        $percent = 100;
                                        $stat = 'Complete';
                                      }
                                      $exp_tag = explode(',', $value['tag']);
                                      $explode_worker=explode(',',$value['worker']);
                                      /** new 12-06-2018 **/
                                      $explode_team=explode(',',$value['team']);
                                      $explode_department=explode(',',$value['department']);
                                      /** end of 12-06-2018 **/

                                      ?>
                                 
        <tr  id="<?php echo $value['id']; ?>" class="count_section" data-id="<?php echo $value['id']; ?>">
<!--             <td class="details-control" id="<?php echo $value['id']; ?>"></td>
 -->            

            <td class="<?php echo (!empty($value['sub_task_id'])?'details-control':' ');?>" data-id="<?php echo $value['id']; ?>" <?php if($_SESSION['permission']['Task_Sub_Task']['view']!='1'){ echo "style='display:none'";} ?>></td>                                     
            <td> <div class="checkbox-fade fade-in-primary">
            <label class="custom_checkbox1">
            <input type="checkbox" class="alltask_checkbox" data-alltask-id="<?php echo $value['id'];?>">
           <i></i>
            </label>
            </div> </td>
            <td <?php echo $columSettings['timer']; ?>><?php echo date('d-m-Y H:i:s', $value['created_date']);?></td>
            <td <?php echo $columSettings['subject']; ?>>
            <?php if($_SESSION['permission']['Task']['edit']=='1'){ ?>
            <a href="<?php echo base_url().'user/task_details/'.$value['id'];?>" target="_blank"><?php }else{ ?><a href="javascript:;"><?php } ?><?php echo ucfirst($value['subject']);?></a></td>
            <td class="add_class"> <?php echo ucfirst($value['subject']);?></td>
            <td <?php echo $columSettings['start_date']; ?>><?php echo date('d-m-Y',strtotime($value['start_date']));?></td>
            <td <?php echo $columSettings['due_date']; ?>><?php echo date('d-m-Y',strtotime($value['end_date']));?></td>
            <td id="status_<?php echo $value['id'];?>" <?php echo $columSettings['status']; ?> > 
              <select name="task_status" id="task_status" class="task_status per_taskstatus_<?php echo $value['id']; ?>" data-id="<?php echo $value['id'];?>" <?php  if($_SESSION['permission']['Task']['edit']!='1'){ ?> disabled="disabled" <?php } ?>>
              <option value="">Select Status</option>              
              <option value="notstarted" <?php if($value['task_status']=='notstarted'){ echo 'selected="selected"'; }?>>Not started</option>
              <option value="inprogress" <?php if($value['task_status']=='inprogress'){ echo 'selected="selected"'; }?>>In Progress</option>
              <option value="awaiting" <?php if($value['task_status']=='awaiting'){ echo 'selected="selected"'; }?>>Awaiting Feedback</option>
              <option value="archive" <?php if($value['task_status']=='archive'){ echo 'selected="selected"'; }?>>Archive</option>
              <option value="complete" <?php if($value['task_status']=='complete'){ echo 'selected="selected"'; }?>>Complete</option>
            </select>
          </td>
          <td <?php echo $columSettings['priority']; ?> > 
          <!-- priority -->
          <select name="task_priority" id="task_priority" class="task_priority per_taskpriority_<?php echo $value['id'];?>" data-id="<?php echo $value['id'];?>" <?php  if($_SESSION['permission']['Task']['edit']!='1'){ ?> disabled="disabled" <?php } ?>>
          <option value="">By Priority</option>
          <option value="low" <?php if(isset($value['priority']) && $value['priority']=='low') {?> selected="selected"<?php } ?>>Low</option>
          <option value="medium" <?php if(isset($value['priority']) && $value['priority']=='medium') {?> selected="selected"<?php } ?>>Medium</option>
          <option value="high" <?php if(isset($value['priority']) && $value['priority']=='high') {?> selected="selected"<?php } ?>>High</option>
          <option value="super_urgent" <?php if(isset($value['priority']) && $value['priority']=='super_urgent') {?> selected="selected"<?php } ?>>Super Urgent</option>
          </select>
          <!-- priority -->
          </td>
            <td <?php echo $columSettings['tag']; ?>> 
                <?php  foreach ($exp_tag as $exp_tag_key => $exp_tag_value) {
                echo $tagName = $this->Common_mdl->getTag($exp_tag_value).' ';
                }?></td>
            <td class="user_imgs" id="task_<?php echo $value['id'];?>" <?php echo $columSettings['assignto']; ?> >
            <span class="task_<?php echo $value['id'];?>">
            <?php
            foreach($explode_worker as $key => $val){     
            $getUserProfilepic = $this->Common_mdl->getUserProfilepic($val);
            ?>
            <img src="<?php echo $getUserProfilepic;?>" alt="img">
            <?php } ?>

            <?php  if($_SESSION['permission']['Task']['edit']=='1'){ ?>
            <a href="javascript:;" data-toggle="modal" data-target="#adduser_<?php echo $value['id'];?>" class="adduser1 per_assigne_<?php echo $value['id']; ?>"><i class="fa fa-plus"></i></a>
            <?php } ?>
            </span></td>
            <td class="user_imgs" style="display: none;">                                     
              <?php
              $username=array();
              foreach($explode_worker as $key => $val){     
              $getUserProfilename = $this->Common_mdl->getUserProfileName($val);
              array_push($username,$getUserProfilename);
              } 
              echo implode(',', $username);
              ?>       
           </td>
           <td <?php echo $columSettings['services']; ?> >
         
           <?php  if(isset($value['related_to_services']) && $value['related_to_services']!=''){
              echo  $value['related_to_services'];
            }else{

              echo '-';
              }?>
           </td>
           <td <?php echo $columSettings['company_name']; ?> >
         
           <?php  echo $this->Common_mdl->get_price('client','id',$value['company_name'],'crm_company_name'); ?>
           </td>

          
                                       <td <?php echo $columSettings['Due']; ?> > <span class="hours-left"><?php
                                      //echo $value['timer_status'];
                                      if($value['timer_status']!=''){ echo convertToHoursMins($value['timer_status']); }else{ echo '0'; }?> hours<?php //echo $value['task_status'];?></span>
                                      <!--  <select>  <option>Normal</option>
                                      <option>Normal</option> </select> -->
                                    
                                    <?php if($_SESSION['role']==6 && $value['task_status']!="complete"){?> 
                                     <button data-id="<?=$value[id]?>" type="button" class="btn btn-primary sendtoreview">Request To Review</button>
                                     <?php } ?> 

                                      <select name="test_status" id="test_status" class="close-test-04 test_status" data-id="<?php echo $value['id'];?>">
                                      <option value="open" <?php if($value['test_status']=='open'){ echo 'selected="selected"'; }?>>Open</option>
                                      <option value="onhold" <?php if($value['test_status']=='onhold'){ echo 'selected="selected"'; }?>>On Hold</option>
                                      <option value="resolved" <?php if($value['test_status']=='resolved'){ echo 'selected="selected"'; }?>>Resolved</option>
                                      <option value="closed" <?php if($value['test_status']=='closed'){ echo 'selected="selected"'; }?>>Closed</option>
                                      <option value="duplicate" <?php if($value['test_status']=='duplicate'){ echo 'selected="selected"'; }?>>Duplicate</option>
                                      <option value="invalid" <?php if($value['test_status']=='invalid'){ echo 'selected="selected"'; }?>>Invalid</option>
                                      <option value="wontfix" <?php if($value['test_status']=='wontfix'){ echo 'selected="selected"'; }?>>Wontfix</option>
                                      </select>
               
                 <span class="created-date">
                 <i class="fa fa-clock-o"></i>Created <?php echo $value['start_date'];?></span>
                 <span class="timer"><?php echo $d_val;?> </span>
                 <span class="demo" id="demo_<?php echo $value['id'];?>"></span>
                                                        
                                                         
                    <!-- for new timer -->
                  <?php
                  $time=json_decode($value['counttimer']);
                  $time_start_date =$value['time_start_date'];
                  $time_end_date=$value['time_end_date'];
                  $hours=0;
                  $mins=0;
                  $sec=0;
                  $pause='';

                  /** for task timer **/
                  if($_SESSION['role']==6)
                  {
                  $individual_timer=$this->db->query("select * from individual_task_timer where user_id=".$_SESSION['id']." and task_id=".$value['id']." ")->row_array();
                  if(count($individual_timer)>0)
                  {
                       if($individual_timer['time_start_pause']!=''){
                    $res=explode(',',$individual_timer['time_start_pause']);
                    //$res=array(1529919001,1530077528,1530080810,1530080817,1530080930,1530080933);
                    $res1=array_chunk($res,2);
                    $result_value=array();
                    $pause='on';
                    foreach($res1 as $rre_key => $rre_value)
                    {
                       $abc=$rre_value;
                       if(count($abc)>1){
                       if($abc[1]!='')
                       {
                          $ret_val=calculate_test($abc[0],$abc[1]);
                          array_push($result_value, $ret_val) ;
                       }
                       else
                       {
                        $pause='';
                          $ret_val=calculate_test($abc[0],time());
                           array_push($result_value, $ret_val) ;
                       }
                      }
                      else
                      {
                        $pause='';
                          $ret_val=calculate_test($abc[0],time());
                           array_push($result_value, $ret_val) ;
                      }


                    }


                    $time_tot=0;
                     foreach ($result_value as $re_key => $re_value) {
                        $time_tot+=time_to_sec($re_value) ;
                     }
                     $hr_min_sec=sec_to_time($time_tot);
                     $hr_explode=explode(':',$hr_min_sec);
                     $hours=(int)$hr_explode[0];
                     $min=(int)$hr_explode[1];
                     $sec=(int)$hr_explode[2];                     
                    }
                    else
                    {
                      $hours=0;
                      $min=0;
                      $sec=0;
                    }
                  }
                  //echo $hours."--".$min."--".$sec;
                  }  
                  /** end of task timer **/
                  else {                    
                  $individual_timer=$this->db->query("select * from individual_task_timer where task_id=".$tre_value['id']." ")->result_array();
                  $pause_val='on';
                  $pause='on';
                  $for_total_time=0;
                  if(count($individual_timer)>0)
                  {

                  foreach ($individual_timer as $intime_key => $intime_value) 
                  {
                  $its_time=$intime_value['time_start_pause'];
                  $res=explode(',', $its_time);
                  $res1=array_chunk($res,2);
                  $result_value=array();
                  //  $pause='on';
                  foreach($res1 as $rre_key => $rre_value)
                  {
                     $abc=$rre_value;
                     if(count($abc)>1){
                     if($abc[1]!='')
                     {
                        $ret_val=calculate_test($abc[0],$abc[1]);
                        array_push($result_value, $ret_val) ;
                     }
                     else
                     {
                      $pause='';
                      $pause_val='';
                        $ret_val=calculate_test($abc[0],time());
                         array_push($result_value, $ret_val) ;
                     }
                    }
                    else
                    {
                      $pause='';
                      $pause_val='';
                        $ret_val=calculate_test($abc[0],time());
                         array_push($result_value, $ret_val) ;
                    }
                  }
                  // $time_tot=0;
                   foreach ($result_value as $re_key => $re_value) {
                      //$time_tot+=time_to_sec($re_value) ;
                      $for_total_time+=time_to_sec($re_value) ;
                   }
                  }
                  //echo $for_total_time."val";
                    $hr_min_sec=sec_to_time($for_total_time);
                    $hr_explode=explode(':',$hr_min_sec);
                    $hours=(int)$hr_explode[0];
                    $min=(int)$hr_explode[1];
                    $sec=(int)$hr_explode[2];

                  }
                  else
                  {
                    $hours=0;
                    $min=0;
                    $sec=0;
                    $pause='on';
                  }


                  }
                  $sub_task_id=array_filter(explode(",",$value['sub_task_id']));
                  if(count($sub_task_id)>0)
                  {
                    $main_task_timer=0;
                    foreach ($sub_task_id as $sk => $sv)
                    {
                      if(array_key_exists($sv,$for_main_task_timer))
                      {
                        $main_task_timer+=$for_main_task_timer[$sv];
                      }
                    }

                    $hr_min_sec=sec_to_time($main_task_timer);
                    $hr_explode=explode(':',$hr_min_sec);
                    $hours=(int)$hr_explode[0];
                    $min=(int)$hr_explode[1];
                    $sec=(int)$hr_explode[2];

                  }

                  ?>

                <div class="stopwatch" data-autostart="false" data-date="<?php echo '2018/06/23 15:37:25'; ?>" data-id="<?php echo $value['id']; ?>"  data-hour="<?php echo $hours;?>" data-min="<?php echo $min; ?>" data-sec="<?php echo $sec;?>" data-mili="0" data-start="<?php if($time_start_date!=''){ echo date('Y/m/d H:i:s',strtotime($time_start_date)); } else { echo ""; } ?>" data-end="<?php if($time_end_date!=''){ echo date('Y/m/d H:i:s',strtotime($time_end_date)); } else { echo ""; } ?>" data-current="Start" data-pauseon="<?php echo $pause;?>" >
                <div class="time timer_<?php echo $value['id'];?>">
                <span class="hours">00</span>: 
                <span class="minutes">00</span>: 
                <span class="seconds">00</span>:: 
                <span class="milliseconds">000</span>
                </div>
                <div class="controls per_timerplay_<?php echo $value['id']; ?>" id="<?php echo $value['id'];?>">
                <!-- Some configurability -->

                <?php //09-08-2018 
                if($_SESSION['permission']['Task_Timer']['edit']==1 && $value['task_status']!="complete"){
                ?>
                <button class="toggle for_timer_start_pause new_play_stop per_timerplay_<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" data-pausetext="||" data-resumetext=">"> > </button>
                <?php } ?>
                <!--  <button class="reset">Reset</button> -->
                </div>
                </div>
                <!-- end of timer -->

                <div class="progress-circle progress-<?php echo $percent;?>">
                  <span><?php echo $percent;?></span></div>
                </td>

                  <td> 
                                      <p class="action_01 per_action_<?php echo $value['id']; ?>">  
                                      <?php //if($role!='Staff'){?>   

                                      <!-- <a href="#" onclick="return alltask_delete('<?php echo $value['id'];?>');"><i class="fa fa-trash fa-6 deleteAllTask" aria-hidden="true" ></i></a> -->
                                       <?php if($_SESSION['permission']['Task']['edit']=='1'){ ?>
                                      <a href="javascript:;" data-toggle="modal" data-target="#deleteconfirmation" onclick="delete_task(this)" data-id="<?php echo $value['id'];?>"><i class="fa fa-trash fa-6 deleteAllTask" aria-hidden="true" ></i></a>
                                      <?php } ?>
                                      
                                      <?php //} ?>
                                       <?php if($_SESSION['permission']['Task']['edit']=='1'){ ?>
                                      <a href="<?php echo base_url().'user/update_task/'.$value['id'];?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                                      <?php } ?>

                                      <?php 
                                      if($value['task_status']=='archive'){ ?> 
                                        <a href="javascript:;" id="<?php echo $value['id'];?>" class="archieve_click" onclick="archieve_click(this)" data-status="unarchive" data-toggle="modal" data-target="#my_Modal">
                                      <i class="fa fa-archive archieve_click"  aria-hidden="true" title="unarchive"></i></a>
                                      <?php }else{ ?>
                                        <a href="javascript:;" id="<?php echo $value['id'];?>" class="archieve_click" onclick="archieve_click(this)" data-status="archive" data-toggle="modal" data-target="#my_Modal">
                                      <i class="fa fa-archive archieve_click"  aria-hidden="true" title="archive"></i></a>
                                      <?php } ?>
                                      </p>
                                      </td>
                                   </tr>
                       
                                     <div id="adduser_<?php echo $value['id'];?>" class="adduserpopup03 modal fade" role="dialog">
                                        <div class="modal-dialog">
                                           <!-- Modal content-->
                                           <div class="modal-content">
                                              <div class="modal-header">
                                                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                 <h4 class="modal-title" style="text-align-last: center">Assign to</h4>
                                              </div>
                                              <div class="modal-body">
                                                 <!--  <label>assign to </label> -->
                                                 <div class="dropdown-sin-2 lead-form-st">
                                                    <select multiple placeholder="select" name="workers[]" id="workers<?php echo $value['id'];?>" class="workers">
                                                     <!--   <?php foreach($staffs as $s_key => $s_val){ ?>
                                                        <option value="<?php echo $s_val['id'];?>"  <?php if(in_array( $s_val['id'] ,$explode_worker )) {?> selected="selected"<?php } ?> ><?php echo $s_val['crm_name'];?></option>
                                                       <?php } ?> -->
                                             <?php if(count($staff_form)){ ?>
                                                    <option disabled>Staff</option>
                                                       <?php foreach($staff_form as $s_key => $s_val){ ?>
                                                        <option value="<?php echo $s_val['id'];?>"  <?php if(in_array( $s_val['id'] ,$explode_worker )) {?> selected="selected"<?php } ?> ><?php echo $s_val['crm_name'];?></option>
                                                       <?php } 
                                                       } ?>
                                                <?php if(count($team)){ ?>
                                                    <option disabled>Team</option>
                                                       <?php foreach($team as $s_key => $s_val){ ?>
                                                        <option value="tm_<?php echo $s_val['id'];?>"  <?php if(in_array( $s_val['id'] ,$explode_team )) {?> selected="selected"<?php } ?> ><?php echo $s_val['team'];?></option>
                                                       <?php } 
                                                       } ?>
                                                    <?php if(count($department)){ ?>
                                                    <option disabled>Department</option>
                                                       <?php foreach($department as $s_key => $s_val){ ?>
                                                        <option value="de_<?php echo $s_val['id'];?>"  <?php if(in_array( $s_val['id'] ,$explode_department )) { ?> selected="selected"<?php } ?> ><?php echo $s_val['new_dept'];?></option>
                                                       <?php } 
                                                       } ?>
                                                    </select>
                                                 </div>
                                              </div>
                                              <div class="modal-footer profileEdit">
                                                 <input type="hidden" name="hidden">
                                                 <a href="javascript:void();" id="acompany_name" data-dismiss="modal" data-id="<?php echo $value['id'];?>" class="save_assign_staff">save</a>
                                              </div>
                                           </div>
                                        </div>
                                     </div>
                                      <?php   } ?>

                                               <div id="task_assign" class="modal fade" role="dialog">
                                                      <div class="modal-dialog">
                                                         <!-- Modal content-->
                                                         <div class="modal-content">
                                                            <div class="modal-header">
                                                               <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                               <h4 class="modal-title" style="text-align-last: center">Assign to</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                               <!--  <label>assign to </label> -->
                                                               <div class="dropdown-sin-2 lead-form-st">

                                                               <input type="hidden" name="task_id" id="task_id" value=""> 

                                        <select multiple placeholder="select" name="workers[]" id="workers<?php echo $value['id'];?>" class="workers_assign">
                                                <?php foreach($staffs as $s_key => $s_val){ ?>
                                                 <!-- <option value="<?php echo $s_val['id'];?>" ><?php echo $s_val['crm_name'];?></option> -->
                                                <?php } ?>
                                                          <?php if(count($staff_form)){ ?>
                                                                  <option disabled>Staff</option>
                                                                     <?php foreach($staff_form as $s_key => $s_val){ ?>
                                                                      <option value="<?php echo $s_val['id'];?>" ><?php echo $s_val['crm_name'];?></option>
                                                                     <?php } 
                                                                     } ?>
                                                              <?php if(count($team)){ ?>
                                                                  <option disabled>Team</option>
                                                                     <?php foreach($team as $s_key => $s_val){ ?>
                                                                      <option value="tm_<?php echo $s_val['id'];?>"  ><?php echo $s_val['team'];?></option>
                                                                     <?php } 
                                                                     } ?>
                                                                  <?php if(count($department)){ ?>
                                                                  <option disabled>Department</option>
                                                                     <?php foreach($department as $s_key => $s_val){ ?>
                                                                      <option value="de_<?php echo $s_val['id'];?>" ><?php echo $s_val['new_dept'];?></option>
                                                                     <?php } 
                                                                     } ?>
                                             </select>
                                                               </div>
                                                            </div>
                                                            <div class="modal-footer profileEdit">
                                                               <input type="hidden" name="hidden">
                                                               <a href="javascript:void();" id="acompany_name" data-dismiss="modal" data-id="<?php echo $value['id'];?>" class="assigned_staff">save</a>
                                                                <a href="javascript:;" data-dismiss="modal">Close</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <!-- <tr>
                                                      <td></td>
                                                      </tr> -->
                                                </tbody>
                                             </table>
                         <input type="hidden" class="rows_selected" id="select_alltask_count" >                     
                                          </div>
                                       </div>
                                    </div>                                
                                 <!-- home-->                             
                                 <!-- new_user --> 
                              </div>
                           </div>
                           <!-- admin close -->
                        </div>
                        <!-- Register your self card end -->
                     </div>
                  </div>
               </div>
               <!-- Page body end -->
            </div>
         </div>
         <!-- Main-body end -->
         <div id="styleSelector">
         </div>
      </div>
   </div>
</div>
</div>
</div>
</div>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

<div class="modal fade" id="import-task" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Import Task</h4>
</div>
<div class="modal-body">
<?php $this->load->view('tasks/import_task'); ?>
</div>
<!-- <div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div> -->
</div>
</div>
</div>
<!-- Warning Section Starts -->
<!-- Older IE warning message -->
<!--[if lt IE 10]>
<div class="ie-warning">
   <h1>Warning!!</h1>
   <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
   <div class="iew-container">
      <ul class="iew-download">
         <li>
            <a href="http://www.google.com/chrome/">
               <img src="assets/images/browser/chrome.png" alt="Chrome">
               <div>Chrome</div>
            </a>
         </li>
         <li>
            <a href="https://www.mozilla.org/en-US/firefox/new/">
               <img src="assets/images/browser/firefox.png" alt="Firefox">
               <div>Firefox</div>
            </a>
         </li>
         <li>
            <a href="http://www.opera.com">
               <img src="assets/images/browser/opera.png" alt="Opera">
               <div>Opera</div>
            </a>
         </li>
         <li>
            <a href="https://www.apple.com/safari/">
               <img src="assets/images/browser/safari.png" alt="Safari">
               <div>Safari</div>
            </a>
         </li>
         <li>
            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
               <img src="assets/images/browser/ie.png" alt="">
               <div>IE (9 & above)</div>
            </a>
         </li>
      </ul>
   </div>
   <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->
<!-- Warning Section Ends -->
<?php
   $data = $this->db->query("SELECT * FROM update_client")->result_array();
   foreach($data as $row) { ?>
<!-- Modal -->
<div id="myModal<?php echo $row['user_id'];?>" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">CRM-Updated fields</h4>
         </div>
         <div class="modal-body">
            <?php if(isset($row['company_name'])&&($row['company_name']!='0')){?>
            <p>Company Name: <span><?php echo $row['company_name'];?></span></p>
            <?php } ?> 
            <?php if(isset($row['company_url'])&&($row['company_url']!='0')){?>
            <p>Company URL: <span><?php echo $row['company_url'];?></span></p>
            <?php } ?>
            <?php if(isset($row['officers_url'])&&($row['officers_url']!='0')){?>
            <p>Officers URL: <span><?php echo $row['officers_url'];?></span></p>
            <?php } ?>
            <?php if(isset($row['incorporation_date'])&&($row['incorporation_date']!='0')){?>
            <p>Incorporation Date: <span><?php echo $row['incorporation_date'];?></span></p>
            <?php } ?>
            <?php if(isset($row['register_address'])&&($row['register_address']!='0')){?>
            <p>Registered Address: <span><?php echo $row['register_address'];?></span></p>
            <?php }  ?>
            <?php if(isset($row['company_status'])&&($row['company_status']!='0')){?>
            <p>Company Status: <span><?php echo $row['company_status'];?></span></p>
            <?php }  ?>
            <?php if(isset($row['company_type'])&&($row['company_type']!='0')){?>
            <p>Company Type: <span><?php echo $row['company_type'];?></span></p>
            <?php } ?>
            <?php if(isset($row['accounts_periodend'])&&($row['accounts_periodend']!='0')){?>
            <p>Accounts Period End: <span><?php echo $row['accounts_periodend'];?></span></p>
            <?php } ?>
            <?php if(isset($row['hmrc_yearend'])&&($row['hmrc_yearend']!='0')){?>
            <p>HMRC Year End: <span><?php echo $row['hmrc_yearend'];?></span></p>
            <?php } ?>
            <?php if(isset($row['ch_accounts_next_due'])&&($row['ch_accounts_next_due']!='0')){?>
            <p>CH Accounts Next Due: <span><?php echo $row['ch_accounts_next_due'];?></span></p>
            <?php } ?>
            <?php if(isset($row['confirmation_statement_date'])&&($row['confirmation_statement_date']!='0')){?>
            <p>Confirmation Statement Date: <span><?php echo $row['confirmation_statement_date'];?></span></p>
            <?php } ?>
            <?php if(isset($row['confirmation_statement_due_date'])&&($row['confirmation_statement_due_date']!='0')){?>
            <p>Confirmation Statement Due: <span><?php echo $row['confirmation_statement_due_date'];?></span></p>
            <?php } ?>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!-- /.modal -->



<?php } ?>


<!-- task service popup -->

<div class="modal fade" id="service_task_popup" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Services Task</h4>
         </div>
         <div class="modal-body">
            <?php $this->load->view('tasks/task_from_sevices'); ?>
         </div>
         <!-- <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div> -->
      </div>
   </div>
</div>

<!-- task service popup -->



<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<script>
  
  //for table dinamic update trigger, don't displace this 
function table_sorting_trigger(){

   $('thead th').on("click.DT", function (e) {
      //stop Propagation if clciked outsidemask
      //becasue we want to sort locally here
        if (!$(e.target).hasClass('sortMask')) {

          e.stopImmediatePropagation();
        }
      });


 $('th .themicond').on('click', function(e) {
    if ($(this).parent().find(".dropdown-content").hasClass("Show_content")) {
      $(this).parent().find('.dropdown-content').removeClass('Show_content');
    }else{
       $('.dropdown-content').removeClass('Show_content');
       $(this).parent().find('.dropdown-content').addClass('Show_content');
    }
      $(this).parent().find('.select-wrapper').toggleClass('special');
     if( $(this).parent().find('.select-dropdown span input[type="checkbox"]').parent().hasClass('custom_checkbox1')){
       // alert('yes');
    }else{
    $(this).parent().find('.select-dropdown span input[type="checkbox"]').wrap( '<label class="custom_checkbox1"></label>');
    $(this).parent().find('.custom_checkbox1 input').after( "<i></i>" );

    }
    });

   }
   //for table dinamic update trigger 
</script>
<!-- <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap.js"></script> -->
<!-- <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>  -->
<!-- <script src="<?php echo base_url();?>js/jquery.workout-timer.js"></script> -->
<!-- <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap.js"></script> -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/jquery.tagsinput.css">
 --><link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/email.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.css" />
<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/1.0.7/css/responsive.dataTables.min.css"> -->
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> 
<!--  <script type="text/javascript" language="javascript" src="http://remindoo.org/CRMTool/assets/js/dataTables.responsive.min.js"></script>
-->
<script src="<?php echo base_url()?>assets/js/user_page/materialize.js"></script> 
<!-- <script type="text/javascript" language="javascript" src="<?php echo base_url()?>assets/js/jquery.tagsinput.js"></script>
 -->
 <script type="text/javascript" src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script> 

<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>

<?php $this->load->view('tasks/task_from_services_js');?>

<!-- <script  src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>  -->
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript">


$(document).ready(function(){




$('.tags').tagsinput({
        allowDuplicates: true
      });

   


      $('.dropdown-sin-2').dropdown({
         limitCount: 5,
      input: '<input type="text" maxLength="20" placeholder="Search">'
    });

      
    $('.dropdown-sin-7').dropdown({
      
      input: '<input type="text" maxLength="20" placeholder="Search">'
    });
    
    /*import tap company selectbox*/
    $('.dropdown-sin-31').dropdown({      
      input: '<input type="text" maxLength="20" placeholder="Search">'
    });
    /*import tap company selectbox*/
    /*task fromservice tap */
$('.dropdown-sin-5').dropdown({      
      input: '<input type="text" maxLength="20" placeholder="Search">'
    });
/*task fromservice tap */

 $('.dropdown-sin-25').dropdown({
     
     input: '<input type="text" maxLength="20" placeholder="Search">'
      });
       $('.dropdown-sin-27').dropdown({
     
     input: '<input type="text" maxLength="20" placeholder="Search">'
      });
        $('.dropdown-sin-29').dropdown({
     
     input: '<input type="text" maxLength="20" placeholder="Search">'
      });
        
});









  $(document).ready(function() {

  $("#confirm-submit.confirm12").on("shown.bs.modal", function () {

   // alert('hi'); 

    $('div#pcoded + div + div + .modal-backdrop.fade.show').css('z-index', '9999');
    
});

    $("#confirm-submit.confirm12").on("hidden.bs.modal", function () {

   // alert('hi'); 

    $('div#pcoded + div + div + .modal-backdrop.fade.show').css('z-index', '1040');
    
})
});
</script>

<script  type="text/javascript">
   var tabletask1;

$('.archive_task_button').on('click', function() {
  var alltask = [];
  tabletask1.column(1).nodes().to$().each(function(index) {
    if($(this).find('.alltask_checkbox').is(':checked'))
    {
      alltask.push($(this).find('.alltask_checkbox').attr('data-alltask-id'));
    }
  });
  if(alltask.length <=0) {
    $('.popup_info_msg').show();
    $(".popup_info_msg .position-alert1").html("Please Selecte Any Records..");    
  } else {
 // console.log(alltask);
  $("#archive_task_id").val(JSON.stringify(alltask));
  $("#status_value").val($(this).data('status'));
  //unarchive does not had conform box
  if($(this).data('status')=="unarchive")archive_action();
  }
});

function archive_action() 
{ //model yes button click
var id=$("#archive_task_id").val();
var status=$("#status_value").val();
 $.ajax({
                    url: '<?php echo base_url(); ?>user/archive_update',
                    type : 'POST',
                    data : { 'id':id,'status':status},                    
                      beforeSend: function() {
                        $(".LoadingImage").show();
                      },
                      success: function(data) {
                        $(".LoadingImage").hide();
                        $(".popup_info_msg").show();
                        $(".popup_info_msg .position-alert1").html('Success !!! Task Archive successfully...');
                   //    console.log(data);
                       setTimeout(function () {$(".popup_info_msg").hide(); location.reload();}, 1500);                                        
                      }

                    });

}

function archieve_click(val){ //single button click
//  alert('ok');
//  alert($(val).attr('id'));
  $("#archive_task_id").val(JSON.stringify([$(val).attr('id')]));
  $("#status_value").val($(val).data('status'));
  //alert($("#archive_task_id").val());
  //alert($("#my_Modal").attr('class'));
}
  
   
         function humanise (diff) {
   
   // The string we're working with to create the representation 
   var str = '';
   // Map lengths of `diff` to different time periods 
   var values = [[' year', 365], [' month', 30], [' day', 1]];
   // Iterate over the values... 
   for (var i=0;i<values.length;i++) {
   var amount = Math.floor(diff / values[i][1]);
   // ... and find the largest time value that fits into the diff 
   if (amount >= 1) { 
   // If we match, add to the string ('s' is for pluralization) 
   str += amount + values[i][0] + (amount > 1 ? 's' : '') + ' '; 
   // and subtract from the diff 
   diff -= amount * values[i][1];
   }
   else
   {
   str += amount + values[i][0]+' ';
   }
   
   } return str; } 
     
</script>
<?php

   ?><script></script>
<script>
   
$(document).on('click','.sendtoreview',function(){
  var id=$(this).data("id");
  
  var timer=0;
  timer+=parseInt($(".timer_"+id+" .hours").text());
  timer+=parseInt($(".timer_"+id+" .minutes").text());
  timer+=parseInt($(".timer_"+id+" .seconds").text());
//alert(timer);
if(timer<=0){$(".popup_info_msg").show();$(".popup_info_msg .position-alert1").html("You Don't Have Worked Time..");return;}
  $.ajax({
            url: '<?php echo base_url();?>user/task_statusChange/',
            type: 'post',
            data: { 'rec_id':id,'status':'complete'},
            timeout: 3000,
            success: function( data ){
               $(".popup_info_msg").show();
               $('.popup_info_msg .position-alert1').html('Success !!! Request Send successfully...');
               setTimeout(function () {
                $(".popup_info_msg").hide();
                }, 1500);
             
                },
                error: function( errorThrown ){
                    console.log( errorThrown );
                }
            });
});
   
      $(document).ready(function() {
   
       humanise();  
       
      var check=0;
      var check1=0;
      var numCols = $('#alltask thead th').length;  

         tabletask1 = $('#alltask').DataTable({       

        order: [],
         
        columnDefs: [ { orderable: false, targets: [0,1,12]}],

         "dom": '<"toolbar-table">lfrtip',

       // responsive: true,
          "iDisplayLength": 10,
       //       scrollX:        true,
       //  scrollCollapse: true,
       // // paging:         false,
       //  fixedColumns:   {
       //      leftColumns: 1,
       //      rightColumns: 1
       //  },
       initComplete: function () { 
                     var q=2;
                     $('#alltask thead th').find('.filter_check').each(function(){
                       $(this).attr('id',"filter_check_"+q);
                       q++;
                     });
                  $('#alltask tfoot th').find(".multiple-select-dropdown").css('display','none');
                  for(i=2;i<numCols;i++){ 
                  //console.log('*****************'+i+'***************');
                     if(i==3){
                          check=1;          
                          i=Number(i) + 1;
                          var select = $("#filter_check_3"); 
                      }else if(i==10){
                          check=1;          
                          i=Number(i) + 1;
                          var select = $("#filter_check_10"); 
                      }else{
                          var select = $("#filter_check_"+i); 

                      } 

                      if(!select.length)continue;
                     

                      //console.log(select);
                  //    console.log('*****************'+i+'***************');
                      // var select = $("#frozen_"+i); 
                         //    alert(i);
                          
                      this.api().columns([i]).every( function () {
                        var column = this;
                        column.data().unique().sort().each( function ( d, j ) {    
                        //  console.log(d);         
                          select.append( '<option  value="'+d+'">'+d+'</option>' );
                        });
                      });     
                        if(check=='1'){                 
                         i=Number(i) - 1;
                         check=0;
                      }              
                    // console.log('*****************'+i+'***************');
                     $("#filter_check_"+i).formSelect();  
                  }
        }
    });
      for(j=2;j<numCols;j++){  
          if(!$('#filter_check_'+j).length)continue;
          $('#filter_check_'+j).on('change', function(){   
            var c=$(this).attr('id').trim();  
          //  alert(c);
              var search = [];              
              $.each($('#'+c+ ' option:selected'), function(){                
                  search.push($(this).val());
              });  
              var i=c.replace("filter_check_","");    
              search = search.join('|');             
              if(i=="3"){               
                i=Number(i) + 1;
              }
              if(i=="10"){               
              i=Number(i) + 1;
              }   
              tabletask1.column(i).search(search, true, false).draw();  
          });
       }



//        function format ( d ) {


//alert("step1");

//$(function () {
    //  var table = $('#alltask').DataTable({});
      // Add event listener for opening and closing details
      $(document).on('click', 'td.details-control, td.sorting_1', function () {
          var tr = $(this).closest('tr');
          var row = tabletask1.row(tr);
          //alert("Outer width of td: " + $(this).closest('td').outerWidth());

          $(tr).each(function (index, value) { 
            console.log("Outer width of td: " + $(tr).find('td').width()); 
          });

          if (row.child.isShown()) {
              // This row is already open - close it
              row.child.hide();
              tr.removeClass('shown');
          } else {
            var id=$(this).data('id');
           // alert(id);
              $.ajax({
                    url: '<?php echo base_url(); ?>Sub_Task/sub_task_get',
                    type : 'POST',
                    data : { 'id':id },                    
                      beforeSend: function() {
                        $(".LoadingImage").show();
                      },
                      success: function(data) {
                                  var json = JSON.parse(data); 
                                  var myJSON = JSON.stringify(json);
                                  var finalData = myJSON.replace(/\\/g, "");  
                                  var result = finalData.replace(/\,{"table":"/g, ' ');
                                  var result1= result.replace(/\"}/g, ' ');
                                  var result2= result1.replace(/\[{"table":"/g, ' ');
                                  var result3= result2.replace(/\]/g, ' ');

                                  //var result= finalData.replaceAll(',{"table":"', "");

                                   //var result1= result.replaceAll('"},{"table":"', "");
                                  // console.log(result1);
                                   $(".LoadingImage").hide();

                                  row.child(  result3  ).show();
                                  tr.addClass('shown');  
                                  console.log(row.child);                    
                                   working_hours_timer();

                                 $('.alltask').dataTable().fnDestroy();
                      }

                    });


            // Open this row
           
         }

      });
//  });
   
      
      
        $("div.toolbar-table").html('<div class="filter-task1"><h2>Filter:</h2><ul><li><img src="<?php echo  base_url()?>assets/images/by-date2.png" alt="data"><select id="statuswise_filter" class="statuswise_filter"><option value="">By Status</option> <option value="notstarted">Not started</option><option value="inprogress">In Progress</option><option value="awaiting">Awaiting Feedback</option><option value="archive">Archive</option><option value="complete">Complete</option></select></li><li><img src="<?php echo  base_url()?>assets/images/by-date3.png" alt="data"><select id="prioritywise_filter" class="prioritywise_filter"><option value="">By Priority</option><option value="low">Low</option><option value="medium">Medium</option><option value="high">High</option><option value="super_urgent">Super Urgent</option></select></li><li><select id="export_report" class="export_report"><option value="">Export</option><option value="excel">Excel</option><option value="pdf">PDF</option><option value="html">HTML</option></select></li>   <li style="    width: 148px;"><select  placeholder="select" name="assignee" id="assignee" class="workers"><option value="">select</option><?php if(count($staff_form)){ ?><option disabled>Staff</option><?php foreach($staff_form as $s_key => $s_val){ ?><option value="<?php echo $s_val['id'];?>" ><?php echo $s_val['crm_name'];?></option><?php } 
} ?><?php if(count($team)){ ?><option disabled>Team</option><?php foreach($team as $s_key => $s_val){ ?><option value="tm_<?php echo $s_val['id'];?>"  ><?php echo $s_val['team'];?></option><?php } } ?><?php if(count($department)){ ?><option disabled>Department</option><?php foreach($department as $s_key => $s_val){ ?><option value="de_<?php echo $s_val['id'];?>" ><?php echo $s_val['new_dept'];?></option><?php } 
} ?></select></li> <li> <div class="billblecheck"><label class="custom_checkbox1"><input type="checkbox" name="billable" value="Billable" checked="" id="p-check1"><i></i></label><span> Billable</span></div></li> </ul></div> ');
      
      
      // $("#inprogresses").dataTable({
      //      "iDisplayLength": 10,
      //   });

      // $("#notstarted").dataTable({
      //      "iDisplayLength": 10,
      //   });

      // $("#started").dataTable({
      //      "iDisplayLength": 10,
      //   });

  /** table inside filter options **/    
       
/** for priority **/
   

 /** end of table inside filter **/

   
       
   
      //$(".status").change(function(e){
     
      
      // payment/non payment status
      
      });
      
</script>
<script>
   $(document).ready(function() {
    //  var check=0;
    //   var check1=0;
    //   var numCols = $('#alltask thead th').length;  

    //     var table = $('#alltask').DataTable({
    //      "dom": '<"toolbar-table">lfrtip',
    //     responsive: true,
    //       "iDisplayLength": 10,
    //    initComplete: function () { 
    //                  var q=2;
    //                  $('#alltask tfoot th').find('.filter_check').each( function(){
    //                    $(this).attr('id',q);
    //                    q++;
    //                  });
    //               $('#alltask tfoot th').find(".multiple-select-dropdown").css('display','none');
    //               for(i=2;i<numCols;i++){ 
    //               //console.log('*****************'+i+'***************');
    //                  if(i==3){
    //                       check=1;          
    //                       i=Number(i) + 1;
    //                       var select = $("#3"); 
    //                   }else{
    //                       var select = $("#"+i); 
    //                   } 
    //                   console.log(select);
    //                   console.log('*****************'+i+'***************');
    //                   // var select = $("#frozen_"+i); 
                              
    //                   this.api().columns([i]).every( function () {
    //                     var column = this;
    //                     column.data().unique().sort().each( function ( d, j ) {    
    //                     //  console.log(d);         
    //                       select.append( '<option value="'+d+'">'+d+'</option>' )
    //                     });
    //                   });     
    //                     if(check=='1'){                 
    //                      i=Number(i) - 1;
    //                      check=0;
    //                   }              
    //                 // console.log('*****************'+i+'***************');
    //                  $("#"+i).formSelect();  
    //               }
    //     }
    // });
    //   for(j=2;j<numCols;j++){  
    //       $('#'+j).on('change', function(){  
    //         var c=$(this).attr('id');  
    //       //  alert(c);
    //           var search = [];              
    //           $.each($('#'+c+ ' option:selected'), function(){                
    //               search.push($(this).val());
    //           });      
    //           search = search.join('|');             
    //           // if(c==8){               
    //           //   c=Number(c) + 1;
    //           // }             
    //           table.column(c).search(search, true, false).draw();  
    //       });
    //    }
      
      
       
      $('#dropdown2').on('change', function () {
                    // table.columns(5).search( this.value ).draw();
                    var filterstatus = $(this).val();
                    $.ajax({
                  type: "POST",
                  url: "<?php echo base_url();?>tasksummary/taskFilter",
                  data: {filterstatus:filterstatus},
                  success: function(response) {
   
                     $(".all-usera1").html(response);
    
                  },
                  //async:false,
               });
                 });
   
      /*$('#dropdown1').on('change', function () {
                    // table.columns(2).search( this.value ).draw();
                    
                 } );*/
   
                
   });
   
</script>
<script>


  $(document).on('change','.test_status',function () {
            //$(".task_status").change(function(){
       var rec_id = $(this).data('id');
       var stat = $(this).val();
          $.ajax({
            url: '<?php echo base_url();?>user/test_statuschange/',
            type: 'post',
            data: { 'rec_id':rec_id,'status':stat },
            timeout: 3000,

            success: function( data ){
               //alert('ggg');
                $("#status_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! Status have been changed successfully...</span></div></div></div>').hide().fadeIn(1500, function() { $('#status_succ'); });

             //   setTimeout(resetAll,3000);
               /* if(stat=='3'){
                  
                   //$this.closest('td').next('td').html('Active');
                   $('#frozen'+rec_id).html('Frozen');
      
                } else {
                   $this.closest('td').next('td').html('Inactive');
                }*/
                },
                error: function( errorThrown ){
                    console.log( errorThrown );
                }
            });
   });



   $(document).ready(function() {




   $("#bulkDelete").on('click',function() { // bulk checked
         var status = this.checked;
   
         $(".deleteRow").each( function() {
            $(this).prop("checked",status);
   
         });
        if(status==true)
        {
          $('#deleteTriger').show();
        } else {
          $('#deleteTriger').hide();
        }
      });
   
   $(".deleteRow").on('click',function() { // bulk checked
   
        var status = this.checked;
   
        if(status==true)
        {
          $('#deleteTriger').show();
        } else {
          $('#deleteTriger').hide();
        }
      });
      
      $('#deleteTriger').on("click", function(event){ // triggering delete one by one
       
         if( $('.deleteRow:checked').length > 0 ){  // at-least one checkbox checked
            var ids = [];
            $('.deleteRow').each(function(){
               if($(this).is(':checked')) { 
                  ids.push($(this).val());
               }
            });
            var ids_string = ids.toString();  // array to string conversion 
            $.ajax({
               type: "POST",
               url: "<?php echo base_url();?>user/task_delete/",
               data: {data_ids:ids_string},
               success: function(response) {
                  //dataTable.draw(); // redrawing datatable
                  var emp_ids = response.split(",");
   for (var i=0; i < emp_ids.length; i++ ) {
   
      $("#"+emp_ids[i]).remove(); 
   }
   
               },
               //async:false,
            });
         }
      });
   
      /*$("#datewise_filter").change(function(){
         alert($(this).val());
      });*/
     /* $("#prioritywise_filter").change(function(){
         alert($(this).val());
      });
      $("#statuswise_filter").change(function(){
         alert($(this).val());
      });
      $("#export_report").change(function(){
         alert($(this).val());
      });*/
   $(  document ).on('change', ".statuswise_filter",function(){
   //$("#statuswise_filter").change(function(){
      //alert($(this).val());
  //    console.log('on that page');
      var data = {};
      data['status'] = $(this).val();
      data['priority'] = $('.prioritywise_filter').val();
      get_result(data);
       return false; 
    });
   $(  document ).on('change', ".prioritywise_filter",function(){
   //$("#prioritywise_filter").change(function(){
      var data = {};
      data['priority'] = $(this).val();
      data['status'] = $('.statuswise_filter').val();
      get_result(data);

      return false;
    }); 



   
   $(  document ).on('change', ".export_report",function(){
   //$("#prioritywise_filter").change(function(){
      var data = {};
      data['priority'] = $('.prioritywise_filter').val();
      data['status'] = $('.statuswise_filter').val();
      data['d_type'] = $(this).val();
      file_download(data);
      return false;
    });

   $(document).on('click',"#billable",function(){
    var data = {};
       if($(this).is(':checked')) { 
      //  alert($(this).val());
      data['billable'] = 'Billable';
      $(this).prop('checked',true);

       $(this).attr('checked','checked');
    }else{
      data['billable'] = '';
    }
      data['priority'] = $('.prioritywise_filter').val();
      data['status'] = $('.statuswise_filter').val();
      get_result(data);
      return false;
   });



   $(document).on('change',"#assignee",function(){
    var data = {};
     
       //alert($(this).val());
      data['worker']=$(this).val();
      $("#assigneed_value").val($(this).val());

      data['billable'] = 'Billable';     
      data['priority'] = $('.prioritywise_filter').val();
      data['status'] = $('.statuswise_filter').val();
      get_result(data);
      return false;
   });



   
   function file_download(data)
   {
   var type = data.d_type;
   var status = data.status;
   var priority = data.priority;
   if(type=='excel')
   {
   window.location.href="<?php echo base_url().'user/task_excel?status="+status+"&priority="+priority+"'?>";
   }else if(type=='pdf')
   {
   window.location.href="<?php echo base_url().'user/task_pdf?status="+status+"&priority="+priority+"'?>";
   }else if(type=='html')
   {
   window.open(
   "<?php echo base_url().'user/task_html?status="+status+"&priority="+priority+"'?>",
   '_blank' // <- This is what makes it open in a new window.
   );
   // window.location.href="<?php echo base_url().'user/task_html?status="+status+"&priority="+priority+"'?>";
   }
   }
   
    function get_result(data){
      $(".LoadingImage").show();
      var pri = data.priority;
      var sta = data.status;

      var billable=data.billable;



      if(billable!=''){
        var char='checked=checked';
      }

        if($('#for_status_us').val()!='')
        {
         sta = $('#for_status_us').val();
         data.status=sta;
        }
        else
        {
         sta = data.status;
        }
      //alert(sta);
      var ns = '';
      var ip = '';
      var af = '';
      var ts = '';
      var cm = '';
      var arc='';
      if(sta == 'notstarted')
      {
         ns = 'selected="selected"';
      }if(sta == 'inprogress')
      {
         ip = 'selected="selected"';
      }if(sta == 'awaiting')
      {
         af = 'selected="selected"';
      }if(sta == 'testing')
      {
         ts = 'selected="selected"';
      }if(sta == 'complete')
      {
         cm = 'selected="selected"';
      }if(sta == 'archive')
      {
         arc = 'selected="selected"';
      }
   
      var lw = '';
      var md = '';
      var hi = '';
      var su = '';
   
      if(pri == 'low')
      {
         lw = 'selected="selected"';
      }if(pri == 'medium')
      {
         md = 'selected="selected"';
      }if(pri == 'high')
      {
         hi = 'selected="selected"';
      }if(pri == 'super_urgent')
      {
         su = 'selected="selected"';
      }
   
      $.ajax({
      url: '<?php echo base_url();?>user/filter_task/',
      type: "POST",
      data: data,
      success: function(data)  
      { 
        $(".LoadingImage").hide();
        $(".all-usera1").html(data);  
      table_sorting_trigger();
          var check=0;
          var check1=0;
          var numCols = $('#alltask thead th').length;  
          tabletask1 = $('#alltask').DataTable({
            // order: [],
            // columnDefs: [ { orderable: false, targets: [0,1,12]}],
                "dom": '<"toolbar-table">lfrtip',
        //responsive: true,
       "iDisplayLength": 10,
       initComplete: function () { 
                     var q=2;
                     $('#alltask thead th').find('.filter_check').each( function(){
                       $(this).attr('id',"filter_check_"+q);
                       q++;
                     });
                  $('#alltask tfoot th').find(".multiple-select-dropdown").css('display','none');
                  for(i=2;i<numCols;i++){ 
                  //console.log('*****************'+i+'***************');
                     if(i==3){
                          check=1;          
                          i=Number(i) + 1;
                          var select = $("#filter_check_3"); 
                          }else   if(i==10){
                          check=1;          
                          i=Number(i) + 1;
                          var select = $("#filter_check_10"); 
                      }else{
                          var select = $("#filter_check_"+i); 
                      } 
                 //     console.log(select);
                 //     console.log('*****************'+i+'***************');
                      // var select = $("#frozen_"+i); 
                               if(!select.length)continue;
                      this.api().columns([i]).every( function () {
                        var column = this;
                        column.data().unique().sort().each( function ( d, j ) {    
                        //  console.log(d);         
                          select.append( '<option value="'+d+'">'+d+'</option>' )
                        });
                      });     
                        if(check=='1'){                 
                         i=Number(i) - 1;
                         check=0;
                      }              
                    // console.log('*****************'+i+'***************');
                     $("#filter_check_"+i).formSelect();  
                  }
        }
    });      
      
        for(j=2;j<numCols;j++){  
          if(!$('#filter_check_'+j).length)continue;
          $('#filter_check_'+j).on('change', function(){   
            var c=$(this).attr('id').trim();  
          //  alert(c);
              var search = [];              
              $.each($('#'+c+ ' option:selected'), function(){                
                  search.push($(this).val());
              });  
              var i=c.replace("filter_check_","");    
              search = search.join('|');             
            
              tabletask1.column(i).search(search, true, false).draw();  
          });
       }

      var ass_ign=$("#assigneed_value").val();

        if($('#for_status_us').val()!='')
        {
          //alert('if');
               $("div.toolbar-table").html('<div class="filter-task1"><h2>Filter:</h2><ul><li style="display:none;"><img src="<?php echo  base_url()?>assets/images/by-date2.png" alt="data"><select id="statuswise_filter" class="statuswise_filter"><option value="">By Status</option> <option value="notstarted" ' +ns+ ' >Not started</option><option value="inprogress" '+ip+' >In Progress</option><option value="awaiting"'+af+'>Awaiting Feedback</option><option value="archive" '+arc+'>Archive</option><option value="complete" '+cm+'>Complete</option></select></li><li><img src="<?php echo  base_url()?>assets/images/by-date3.png" alt="data"><select id="prioritywise_filter" class="prioritywise_filter"><option value="">By Priority</option><option value="low" '+lw+'>Low</option><option value="medium" '+md+'>Medium</option><option value="high" '+hi+'>High</option><option value="super_urgent" '+su+'>Super Urgent</option></select></li><li><select id="export_report" class="export_report"><option value="">Export</option><option value="excel">Excel</option><option value="pdf">PDF</option><option value="html">HTML</option></select></li> <li style="    width: 148px;"><select  placeholder="select" name="assignee" id="assignee" class="workers"><option value="">select</option><?php if(count($staff_form)){ ?><option disabled>Staff</option><?php foreach($staff_form as $s_key => $s_val){ ?><option value="<?php echo $s_val['id'];?>" <?php if(isset($_SESSION['assignee']) && $_SESSION['assignee']== $s_val['id']){ ?> selected="selected" <?php } ?>><?php echo $s_val['crm_name'];?></option><?php } 
} ?><?php if(count($team)){ ?><option disabled>Team</option><?php foreach($team as $s_key => $s_val){ ?><option value="tm_<?php echo $s_val['id'];?>"   <?php if(isset($_SESSION['assignee']) && $_SESSION['assignee']== $s_val['id']){ ?> selected="selected" <?php } ?>><?php echo $s_val['team'];?></option><?php } } ?><?php if(count($department)){ ?><option disabled>Department</option><?php foreach($department as $s_key => $s_val){ ?><option value="de_<?php echo $s_val['id'];?>"  <?php if(isset($_SESSION['assignee']) && $_SESSION['assignee']== $s_val['id']){ ?> selected="selected" <?php } ?> ><?php echo $s_val['new_dept'];?></option><?php } 
} ?></select></li> <li class="bill-checkin"><label class="custom_checkbox1"><input type="checkbox" id="billable" '+char+' > <i></i></div>  <span>Billable</span></li></ul><div>');
        }
        else{

        //  alert('else');
                  $("#alltask_wrapper").find(".toolbar-table").html('<div class="filter-task1"><h2>Filter:</h2><ul><li><img src="<?php echo  base_url()?>assets/images/by-date2.png" alt="data"><select id="statuswise_filter" class="statuswise_filter"><option value="">By Status</option> <option value="notstarted" ' +ns+ ' >Not started</option><option value="inprogress" '+ip+' >In Progress</option><option value="awaiting"'+af+'>Awaiting Feedback</option><option value="archive" '+arc+'>Archive</option><option value="complete" '+cm+'>Complete</option></select></li><li><img src="<?php echo  base_url()?>assets/images/by-date3.png" alt="data"><select id="prioritywise_filter" class="prioritywise_filter"><option value="">By Priority</option><option value="low" '+lw+'>Low</option><option value="medium" '+md+'>Medium</option><option value="high" '+hi+'>High</option><option value="super_urgent" '+su+'>Super Urgent</option></select></li><li><select id="export_report" class="export_report"><option value="">Export</option><option value="excel">Excel</option><option value="pdf">PDF</option><option value="html">HTML</option></select></li> <li style="    width: 148px;"><select  placeholder="select" name="assignee" id="assignee" class="workers"><option value="">select</option><?php if(count($staff_form)){ ?><option disabled>Staff</option><?php foreach($staff_form as $s_key => $s_val){ ?><option value="<?php echo $s_val['id'];?>"  <?php if(isset($_SESSION['assignee']) && $_SESSION['assignee']== $s_val['id']){ ?> selected="selected" <?php } ?>><?php echo $s_val['crm_name'];?></option><?php } 
} ?><?php if(count($team)){ ?><option disabled>Team</option><?php foreach($team as $s_key => $s_val){ ?><option value="tm_<?php echo $s_val['id'];?>"  <?php if(isset($_SESSION['assignee']) && $_SESSION['assignee']== $s_val['id']){ ?> selected="selected" <?php } ?> ><?php echo $s_val['team'];?></option><?php } } ?><?php if(count($department)){ ?><option disabled>Department</option><?php foreach($department as $s_key => $s_val){ ?><option value="de_<?php echo $s_val['id'];?>"  <?php if(isset($_SESSION['assignee']) && $_SESSION['assignee']== $s_val['id']){ ?> selected="selected" <?php } ?> ><?php echo $s_val['new_dept'];?></option><?php } 
} ?></select></li>  <li class="bill-checkin"><label class="custom_checkbox1"><input type="checkbox" id="billable" '+char+' ><i></i></div><span>Billable</span></li></ul></div> ');
        }


        $("#assignee").val(ass_ign);
        /** for access permission **/
          access_permission_function();
        /** end of access permission **/
          $('.dropdown-sin-7').dropdown({
      
      input: '<input type="text" maxLength="20" placeholder="Search">'
    });

      }
      });
    }
   
   
   });
   
   //$(".save_assign_staff").click(function(){
      $(document).on("click",".save_assign_staff",function(){
      var id = $(this).attr("data-id");
      var data = {};
       var countries =$("#workers"+id ).val();
      /* alert($("#workers"+id ).val());
        $.each($(".workers option:selected"), function(){            
            countries.push($(this).val());
        });*/
      data['task_id'] = id;
      data['worker'] = countries;
      $(".LoadingImage").show();
        $.ajax({
               type: "POST",
               url: "<?php echo base_url();?>user/update_assignees/",
               data: data,
               success: function(response) {
                  // alert(response); die();
                  $(".LoadingImage").hide();
               //$('#task_'+id).html(response);
               $('.task_'+id).html(response);
                  $('.popup_info_msg').show();
                  $(".popup_info_msg .position-alert1").html('Success !!! Staff Assign have been changed successfully...');
                 setTimeout(function(){ $('.popup_info_msg').hide(); }, 1500);
               },
            });
   });

  /* $(".save_assign_staff1").click(function(){
      var id = $(this).attr("data-id");
      var data = {};
       var countries =$("#workers1"+id ).val();
      
      data['task1_id'] = id;
      data['worker'] = countries;
        $.ajax({
               type: "POST",
               url: "<?php echo base_url();?>user/update_assignees1/",
               data: data,
               success: function(response) {
                // alert(response); die();
               $('#task1_'+id).html(response);
               },
            });
   });

   $(".save_assign_staff2").click(function(){
      var id = $(this).attr("data-id");
      var data = {};
       var countries =$("#workers2"+id ).val();
      
      data['task2_id'] = id;
      data['worker'] = countries;
        $.ajax({
               type: "POST",
               url: "<?php echo base_url();?>user/update_assignees2/",
               data: data,
               success: function(response) {
               $('#task2_'+id).html(response);
               },
            });
   });

   $(".save_assign_staff3").click(function(){
      var id = $(this).attr("data-id");
      var data = {};
       var countries =$("#workers3"+id ).val();
      
      data['task3_id'] = id;
      data['worker'] = countries;
        $.ajax({
               type: "POST",
               url: "<?php echo base_url();?>user/update_assignees3/",
               data: data,
               success: function(response) {
                // alert(response); die();
               $('#task3_'+id).html(response);
               },
            });
   });

   */
   
   
   $(function() {
    
    var hours = minutes = seconds = milliseconds = 0;
    var prev_hours = prev_minutes = prev_seconds = prev_milliseconds = undefined;
    var timeUpdate;
   
    // Start/Pause/Resume button onClick
    $("#start_pause_resume").button().click(function(){ 
        // Start button
        if($(this).text() == "Start"){  // check button label
            $(this).html("<span class='ui-button-text'>Pause</span>");
            updateTime(0,0,0,0);
        }
    // Pause button
        else if($(this).text() == "Pause"){
            clearInterval(timeUpdate);
            $(this).html("<span class='ui-button-text'>Resume</span>");
        }
    // Resume button    
        else if($(this).text() == "Resume"){
            prev_hours = parseInt($("#hours").html());
            prev_minutes = parseInt($("#minutes").html());
            prev_seconds = parseInt($("#seconds").html());
            prev_milliseconds = parseInt($("#milliseconds").html());
            
            updateTime(prev_hours, prev_minutes, prev_seconds, prev_milliseconds);
            
            $(this).html("<span class='ui-button-text'>Pause</span>");
        }
    });
    
    // Reset button onClick
    $("#reset").button().click(function(){
        if(timeUpdate) clearInterval(timeUpdate);
        setStopwatch(0,0,0,0);
        $("#start_pause_resume").html("<span class='ui-button-text'>Start</span>");      
    });
    
    // Update time in stopwatch periodically - every 25ms
    function updateTime(prev_hours, prev_minutes, prev_seconds, prev_milliseconds){
        var startTime = new Date();    // fetch current time
        
        timeUpdate = setInterval(function () {
            var timeElapsed = new Date().getTime() - startTime.getTime();    // calculate the time elapsed in milliseconds
            
            // calculate hours                
            hours = parseInt(timeElapsed / 1000 / 60 / 60) + prev_hours;
            
            // calculate minutes
            minutes = parseInt(timeElapsed / 1000 / 60) + prev_minutes;
            if (minutes > 60) minutes %= 60;
            
            // calculate seconds
            seconds = parseInt(timeElapsed / 1000) + prev_seconds;
            if (seconds > 60) seconds %= 60;
            
            // calculate milliseconds 
            milliseconds = timeElapsed + prev_milliseconds;
            if (milliseconds > 1000) milliseconds %= 1000;
            
            // set the stopwatch
            setStopwatch(hours, minutes, seconds, milliseconds);
            
        }, 25); // update time in stopwatch after every 25ms
        
    }
    
    // Set the time in stopwatch
    function setStopwatch(hours, minutes, seconds, milliseconds){
        $("#hours").html(prependZero(hours, 2));
        $("#minutes").html(prependZero(minutes, 2));
        $("#seconds").html(prependZero(seconds, 2));
        $("#milliseconds").html(prependZero(milliseconds, 3));
    }
    
    // Prepend zeros to the digits in stopwatch
    function prependZero(time, length) {
        time = new String(time);    // stringify time
        return new Array(Math.max(length - time.length + 1, 0)).join("0") + time;
    }
   });
   
   
   function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
   }
   
   // window.onclick = function(event) {
   // if (!event.target.matches('.dropbtn')) {
   
   //  var dropdowns = document.getElementsByClassName("dropdown-content");
   //  var i;
   //  for (i = 0; i < dropdowns.length; i++) {
   //    var openDropdown = dropdowns[i];
   //    if (openDropdown.classList.contains('show')) {
   //      openDropdown.classList.remove('show');
   //    }
   //  }
   // }
   // }
   
   // Set the date we're counting down to
   var countDownDate = new Date("Sep 4, 2017 15:37:25").getTime();
   
   // Update the count down every 1 second
   var x = setInterval(function() {
   
    // Get todays date and time
    var now = new Date().getTime();
    
    // Find the distance between now an the count down date
    var distance = countDownDate - now;
    //alert(distance);
    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
    // Output the result in an element with id="demo"
    //document.getElementById("demo").innerHTML = days + "d " + hours + "h "
   var result = humanise(days) + hours + "h "
    + minutes + "m " + seconds + "s ";
    // console.log(result);
    $(".demos").html(result);
   
   
    
    // If the count down is over, write some text 
    if (distance < 0) {
     // alert('fff');
      //  clearInterval(x);
        $(".demos").html('EXPIRED');
        //document.getElementById("demo").innerHTML = "EXPIRED";
    }
   }, 1000);
</script>
<!--  <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"/> -->
<link rel="stylesheet" href="<?php echo base_url();?>css/jquery.workout-timer.css" />
<!-- Example-page styles -->
<style>
   body {
   margin-bottom: 40px;
   background-color:#ECF0F1;
   }
   .event-log {
   width: 100%;
   margin: 20px 0;
   }
   .code-example {
   margin: 20px 0;
   }
</style>

<script>
  var vars = {};
  function working_hours_timer()
  {test();
     $('.stopwatch').each(function () {
//        $(document).on('each','.stopwatch',function(){

        // Cache very important elements, especially the ones used always
        var element = $(this);
        var running = element.data('autostart');
        var for_date = element.data('date');
       
        var task_id=element.data('id');

        var data_pause=element.data('pauseon');
          var data_pause=vars['data_pause_'+task_id];
       
        var hours=element.data('hour');
        var minutes=element.data('min');
        var seconds=element.data('sec');
        var milliseconds=element.data('mili');

        if(hours=='0')
        {
          var hours=vars['hours_'+task_id];
        }
        if(minutes=='0')
        {
          var minutes=vars['minutes_'+task_id];
        }
        if(seconds=='0')
        {
          var seconds=vars['seconds_'+task_id];

        }
        if(milliseconds=='0')
        { //alert("assig"+vars['milliseconds_'+task_id]);

          var milliseconds=vars['milliseconds_'+task_id];
        }
       // alert(milliseconds);
        var start=element.data('start');
        var end=element.data('end');

        var hoursElement = element.find('.hours');
        var minutesElement = element.find('.minutes');
        var secondsElement = element.find('.seconds');
        var millisecondsElement = element.find('.milliseconds');
        var toggleElement = element.find('.toggle');
        var resetElement = element.find('.reset');
        var pauseText = toggleElement.data('pausetext');
        var resumeText = toggleElement.data('resumetext');
        var startText = toggleElement.text();

        // And it's better to keep the state of time in variables 
        // than parsing them from the html.
        //var hours, minutes, seconds, milliseconds, timer;
         var timer;

        function prependZero(time, length) {

          //  alert('zzz');
            // Quick way to turn number to string is to prepend it with a string
            // Also, a quick way to turn floats to integers is to complement with 0
            time = '' + (time | 0);     

            // And strings have length too. Prepend 0 until right.
            while (time.length < length) time = '0' + time;
            return time;
            
        }

        function setStopwatch(hours, minutes, seconds, milliseconds) {
            // Using text(). html() will construct HTML when it finds one, overhead.
            hoursElement.text(prependZero(hours, 2));
            minutesElement.text(prependZero(minutes, 2));
            secondsElement.text(prependZero(seconds, 2));            
            millisecondsElement.text(prependZero(milliseconds, 3));
        }

        // Update time in stopwatch periodically - every 25ms
        function runTimer() {
            // Using ES5 Date.now() to get current timestamp            
            var startTime = Date.now();
         
        //  var startTime=1530000373832;

        //  //var startTime=1529926802;
        //  var startTime=new Date("2016/06/25 00:00:00");  
        // var startTime = new Date("June 23, 2018 15:37:25").getTime();
        if(hours!=0 && minutes!=0 && seconds!=0 && milliseconds!=0)
        {
             var startTime = Date.now();
        }
        else
        {
           // hours=$('#trhours_'+task_id).val();
           //  $minutes=$('#trmin_'+task_id).val();
           //  seconds=$('#trsec_'+task_id).val();
           //  milliseconds=$('#trmili_'+task_id).val();
            //var startTime = new Date("June 23, 2018 15:37:25").getTime();
//            var startTime = new Date("2018/06/23 15:37:25").getTime();
           //var startTime = new Date(for_date).getTime();
           var startTime = Date.now();
        }
        




         // alert("before float"+milliseconds);

            var prevHours = parseFloat(hours);
            var prevMinutes = parseFloat(minutes);
            var prevSeconds = parseFloat(seconds);
            var prevMilliseconds = parseFloat(milliseconds);

            //alert("insidetimer"+prevMilliseconds);

            timer = setInterval(function () {
               // var timeElapsed = Date.now() - startTime;
                var timeElapsed = Date.now() - startTime;
              // alert(hours+"h"+minutes+"m"+seconds+"s");
                hours = (timeElapsed / 3600000) + prevHours;
                minutes = ((timeElapsed / 60000) + prevMinutes) % 60;
                seconds = ((timeElapsed / 1000) + prevSeconds) % 60;
                milliseconds = (timeElapsed + prevMilliseconds) % 1000;
            element.attr('data-hour',hours);
            element.attr('data-min',minutes);
            element.attr('data-sec',seconds);
            element.attr('data-mili',milliseconds);
            element.attr('data-pauseon','');

            vars['hours_'+task_id]=hours;
            vars['minutes_'+task_id]=minutes;
            vars['seconds_'+task_id]=seconds;
            vars['milliseconds_'+task_id]=milliseconds;
            vars['data_pause_'+task_id]='';

           // $('#trpause_'+task_id).val('');

            // $('#trhours_'+task_id).val(hours);
            // $('#trmin_'+task_id).val(minutes);
            // $('#trsec_'+task_id).val(seconds);
            // $('#trmili_'+task_id).val(milliseconds);
          //  alert(hours+"h"+minutes+"m"+seconds+"s");
                setStopwatch(hours, minutes, seconds, milliseconds);
          //   $.ajax({
          //   url: '<?php echo base_url();?>user/task_countdown_update/',
          //   type: 'post',
          //   data: { 'task_id':task_id,'hours':hours,'minutes':minutes,'seconds':seconds,'milliseconds':milliseconds },
          //   timeout: 3000,
          //   success: function( data ){
          //   //  alert('updated');
          //   }
          // });

            }, 25);
        }

        // Split out timer functions into functions.
        // Easier to read and write down responsibilities
        function run() {
            $(this).attr('pause','');
            running = true;
            runTimer();
            if(start=='')
            {
                // $.ajax({
                //   url: '<?php echo base_url();?>user/task_countdown_update_start/',
                //   type: 'post',
                //   data: { 'task_id':task_id,'start':start },
                //   timeout: 3000,
                //   success: function( data ){
                //   //  alert('updated');
                //    toggleElement.attr('data-start','changed');
                //   }
                // });
            }
            toggleElement.text(pauseText);
            toggleElement.attr('data-current',pauseText);
        }

        function pause() {
            running = false;
            clearTimeout(timer);
            toggleElement.text(resumeText);
            toggleElement.attr('data-current',resumeText);
              element.attr('data-pauseon','on');
               //  $('#trpause_'+task_id).val('on');
                      vars['data_pause_'+task_id]='on';
          //  alert(parseInt(hours)+"--"+parseInt(minutes)+"--"+parseInt(seconds)+"--"+parseInt(milliseconds));
            $.ajax({
            url: '<?php echo base_url();?>user/task_countdown_update/',
            type: 'post',
            data: { 'task_id':task_id,'hours':hours,'minutes':minutes,'seconds':seconds,'milliseconds':milliseconds,'pause':'on' },
            timeout: 3000,
            success: function( data ){
            //  alert('updated');
            }
          });
            
        }

        function reset() {
            running = false;
            pause();
            hours = minutes = seconds = milliseconds = 0;
            setStopwatch(hours, minutes, seconds, milliseconds);
            toggleElement.text(startText);
            toggleElement.attr('data-current',startText);
        }
      //  And button handlers merely call out the responsibilities
        toggleElement.on('click', function () {
            if(running) {
            //  alert("true");
              pause();
            } 
            else{ 
             // alert("false");
              run();
              }
        });

        resetElement.on('click', function () {
            reset();
        });

        demo();
        function demo(){
              running = false;
            
            //  hours =74; minutes=37;seconds =53; milliseconds = 764;
           
            element.attr('data-hour',hours);
            element.attr('data-min',minutes);
            element.attr('data-sec',seconds);
            element.attr('data-mili',milliseconds);

            vars['hours_'+task_id]=hours;
            vars['minutes_'+task_id]=minutes;
            vars['seconds_'+task_id]=seconds;
            vars['milliseconds_'+task_id]=milliseconds;
       

            //  $('#trhours_'+task_id).val(hours);
            // $('#trmin_'+task_id).val(minutes);
            // $('#trsec_'+task_id).val(seconds);
            // $('#trmili_'+task_id).val(milliseconds);

  // hours=$('#trhours_'+task_id).val();
  //           minutes=$('#trmin_'+task_id).val();
  //           seconds=$('#trsec_'+task_id).val();
  //           milliseconds=$('#trmili_'+task_id).val();
             if(hours!=0 || minutes!=0 || seconds!=0)
             {
              if(data_pause=='')
              { //alert("inside");
                run();
              }
             }
              //alert("after"+milliseconds);
              setStopwatch(hours, minutes, seconds, milliseconds);
              
        }


    });
  }
       // Init timers
       $(document).ready(function(){

 
        
        <?php 
        foreach ($task_list as $tk_key => $tk_value) {
         ?>       
          vars['hours_'+<?php echo $tk_value['id'];?> ]=$('#trhours_<?php echo $tk_value['id']?>').val();
          vars['minutes_'+<?php echo $tk_value['id'];?>]=$('#trmin_<?php echo $tk_value['id']?>').val();
           vars['seconds_'+<?php echo $tk_value['id'];?>]=$('#trsec_<?php echo $tk_value['id']?>').val();
            vars['milliseconds_'+<?php echo $tk_value['id'];?>]=0;
             vars['data_pause_'+<?php echo $tk_value['id'];?>]=$('#trpause_<?php echo $tk_value['id']?>').val();
         <?php
        }
        ?> 

        working_hours_timer();
   $(document).on('click','.paginate_button',function(){
           working_hours_timer();
   });
  
 });
   /***************************************************/

    function test(){
  <?php foreach ($custom_permission as $style_key => $style_value) 
  {
 
  if(!in_array($style_value['id'],$for_user_edit_per))
    { ?>

     $('td a.per_assigne_<?php echo $style_value['id']; ?>').css('display','none');
    $(".per_chkbox_<?php echo $style_value['id']; ?>").html('<div class="checkbox-fade fade-in-primary"><label>--</label></div>');
    $(".per_chkbox_<?php echo $style_value['id']; ?>").html('<div class="checkbox-fade fade-in-primary"><label>--</label></div>');
    //$('.per_timerplay_<?php echo $style_value['id']; ?>').css('display','none');
    $('td a.per_assigne_<?php echo $style_value['id']; ?>').css('display','none');
    $('.per_action_<?php echo $style_value['id']; ?>').css('display','none');
    $('.per_taskstatus_<?php echo $style_value['id']; ?>,.per_taskpriority_<?php echo $style_value['id']; ?>').css('display','none');

 <?php } 
      } ?>
    }
test();
   /*****************************************************/
       $(document).on('click', '.workout-timer__play-pause', function(){
         //$('.workout-timer').workoutTimer();
         var id = $(this).attr("data-id");
         var txt = $("#counter_"+id).html();
         var data = {};
         data['id'] = id;
         data['time'] = txt;
             $.ajax({
                  type: "POST",
                  url: "<?php echo base_url();?>user/update_timer/",
                  data: data,
                  success: function(response) {
                 // $('#task_'+id).html(response);
                  },
               });
        /* alert(id);
         alert(txt);*/
       });
   
   
   
      /* var $eventLog = $('#event-log');
       var logEvent = function(eventType, eventTarget, timer) {
         var newLog = '"' + eventType + '" event triggered on #' + eventTarget.attr('id') + '\r\n';
         $eventLog.val( newLog.concat( $eventLog.val() ) );
       };
   
       $('.workout-timer-events').workoutTimer({
         onStart: logEvent,
         onRestart: logEvent,
         onPause: logEvent,
         onRoundComplete: logEvent,
         onComplete: logEvent
       });*/
     
</script>
<script>
   // Syntax highlighting
   $('pre code').each(function(i, block) {
     hljs.highlightBlock(block);
   });
   
   
</script><script type="text/javascript">
   var _gaq = _gaq || [];
   _gaq.push(['_setAccount', 'UA-36251023-1']);
   _gaq.push(['_setDomainName', 'jqueryscript.net']);
   _gaq.push(['_trackPageview']);
   
   (function() {
     var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
     ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
     var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
   })();
   
</script>
<script type="text/javascript">

  

$('#alltask thead').on('click', '#overall_tasks', function(e){
    if ($(this).is(':checked')) {
     $(".assign_delete").show(); 
     }else{
      $(".assign_delete").hide(); 
     }  
     $('#alltask .overall_tasks').not(this).prop('checked', this.checked);

 });
</script>
      <script type="text/javascript">

function delete_task(del)
{
  $("#delete_task_id").val(JSON.stringify([$(del).data('id')]));
}
function delete_action()
{
 
  $.ajax({
        type: "POST",
        url: "<?php echo base_url().'user/alltasks_delete';?>",        
        data: {'id':$("#delete_task_id").val()},
        beforeSend: function() {

          $(".LoadingImage").show();
        },
        success: function(data) {

          $(".LoadingImage").hide();

          $(".popup_info_msg").show();
          $('.popup_info_msg .position-alert1').html('Success !!! Task Successfully Deleted..');
         setTimeout(function() {$(".popup_info_msg").hide();location.reload(); },1500);
           
          /*var task_ids = data.split(",");
          for (var i=0; i < task_ids.length; i++ ) { 
          $("#"+task_ids[i]).remove(); } 
          $(".alert-success-delete").show();
          setTimeout(function() { 
          }, 500);  */   
        }
      });
}

     

        $(document).on('click', '.task_status_val', function(){
           $('.task_status_val').removeClass('intro');

            $(this).addClass("intro");
            $(".LoadingImage").show();

             // console.log('OO**************00');
            //alert($(this).attr('data-id'));
              var data = {};
             data['task_status'] = $(this).attr('data-id');
             if($(this).hasClass("sub_status")==true) //for manager recevie,denied,accepted
             {
              data['sub_status']=1;
              data['task_ids']=$(this).attr('data-id');
             }
                    $('#for_status_us').val($(this).attr('data-id'));
                    $.ajax({
                       type: "POST",
                       url: "<?php echo base_url();?>user/get_task_status_data/",
                       data: data,
                       success: function(response) {
                       $(".LoadingImage").hide();
                       $(".for_task_status").html(response);   
                       // var tabletask1 =$("#alltask").dataTable({
                       //    "iDisplayLength": 10,
                       // // "scrollX": true,
                       // "dom": '<"toolbar-table">lfrtip',
                       // responsive: true
                       // });
                       table_sorting_trigger();
                        var check=0;
                        var check1=0;
                        var numCols = $('#alltask thead th').length;  
                         tabletask1 = $('#alltask').DataTable({
                         
                          order: [],
                          columnDefs: [ { orderable: false, targets: [0,1,12]}],
                            "dom": '<"toolbar-table">lfrtip',
                            responsive: true,
       //                         scrollX:        true,
       //  scrollCollapse: true,
       // // paging:         false,
       //  fixedColumns:   {
       //    //  leftColumns: 1,
       //      rightColumns: 1
       //  },
                            "iDisplayLength": 10,
                            initComplete: function () { 
                     var q=2;
                     $('#alltask thead th').find('.filter_check').each( function(){
                       $(this).attr('id',"filter_check_"+q);
                       q++;
                     });
               //   $('#alltask tfoot th').find(".multiple-select-dropdown").css('display','none');
                  for(i=2;i<numCols;i++){ 
                  //console.log('*****************'+i+'***************');
                     if(i==3){
                          check=1;          
                          i=Number(i) + 1;
                          var select = $("#filter_check_3"); 
                      }else   if(i==10){
                          check=1;          
                          i=Number(i) + 1;
                          var select = $("#filter_check_10"); 
                      }else{
                          var select = $("#filter_check_"+i); 
                      } 
              //        console.log(select);
              //        console.log('*****************'+i+'***************');
                      // var select = $("#frozen_"+i); 
                       if(!select.length)continue;  
                      this.api().columns([i]).every( function () {
                        var column = this;
                        column.data().unique().sort().each( function ( d, j ) {    
                        //  console.log(d);         
                          select.append( '<option value="'+d+'">'+d+'</option>' )
                        });
                      });     
                        if(check=='1'){                 
                         i=Number(i) - 1;
                         check=0;
                      }              
                    // console.log('*****************'+i+'***************');
                     $("#filter_check_"+i).formSelect();  
                  }
        }
    });
       for(j=2;j<numCols;j++){  
          if(!$('#filter_check_'+j).length)continue;
          $('#filter_check_'+j).on('change', function(){   
            var c=$(this).attr('id').trim();  
          //  alert(c);
              var search = [];              
              $.each($('#'+c+ ' option:selected'), function(){                
                  search.push($(this).val());
              });  
              var i=c.replace("filter_check_","");    
              search = search.join('|');             
              tabletask1.column(i).search(search, true, false).draw();  
          });
       }

                       $("div.toolbar-table").html('<div class="filter-task1"><h2>Filter:</h2><ul><li style="display:none;"><img src="<?php echo  base_url()?>assets/images/by-date2.png" alt="data"><input type="hidden" id="statuswise_filter" name="statuswise_filter" class="statuswise_filter" value="'+$(this).attr('data-id')+'"></li><li><img src="<?php echo  base_url()?>assets/images/by-date3.png" alt="data"><select id="prioritywise_filter" class="prioritywise_filter"><option value="">By Priority</option><option value="low">Low</option><option value="medium">Medium</option><option value="high">High</option><option value="super_urgent">Super Urgent</option></select></li><li><select id="export_report" class="export_report"><option value="">Export</option><option value="excel">Excel</option><option value="pdf">PDF</option><option value="html">HTML</option></select></li><li style="    width: 148px;"><select  placeholder="select" name="assignee" id="assignee" class="workers"><option value="">select</option><?php if(count($staff_form)){ ?><option disabled>Staff</option><?php foreach($staff_form as $s_key => $s_val){ ?><option value="<?php echo $s_val['id'];?>" ><?php echo $s_val['crm_name'];?></option><?php } 
} ?><?php if(count($team)){ ?><option disabled>Team</option><?php foreach($team as $s_key => $s_val){ ?><option value="tm_<?php echo $s_val['id'];?>"  ><?php echo $s_val['team'];?></option><?php } } ?><?php if(count($department)){ ?><option disabled>Department</option><?php foreach($department as $s_key => $s_val){ ?><option value="de_<?php echo $s_val['id'];?>" ><?php echo $s_val['new_dept'];?></option><?php } 
} ?></select></li> <li> <div class="billblecheck"><label class="custom_checkbox1"><input type="checkbox" name="billable" value="Billable" checked="" id="billable"><i></i></label><span> Billable</span></div></li> </ul></div> ');
                         // location.reload();
                       //  alert('zz');
                         $('.dropdown-sin-7').dropdown({   
      input: '<input type="text" maxLength="20" placeholder="Search">'
    });



                       /** access permission 30-07-2018 **/
                       access_permission_function();
                       /** end **/

                       $(document).ready(function() {
  //   $("#select_alltask").change(function(){

  //     alert('ok');
  //   if(this.checked){
  //     $(".alltask_checkbox").each(function(){
  //       this.checked=true;
  //       $(".assign_delete").show();
  //     })              
  //   }else{
  //     $(".alltask_checkbox").each(function(){
  //       this.checked=false;
  //       $(".assign_delete").hide();
  //     })              
  //   }
  //   $("#select_alltask_count").val($("input.alltask_checkbox:checked").length+" Selected");
  // });

/*  $(".alltask_checkbox").click(function () {
    if ($(this).is(":checked")){
      var isAllChecked = 0;
      $(".alltask_checkbox").each(function(){
        if(!this.checked)
           isAllChecked = 1;
         $(".assign_delete").show();
      })              
      if(isAllChecked == 0){ $("#select_alltask").prop("checked", true); }     
    }else {
      $("#select_alltask").prop("checked", false);
    }
  });    */

 

        var tmp = []; 
       $('#alltask tbody').on('click', '.overall_tasks', function(e){
             $(".assign_delete").show();    
           $(".overall_tasks").each(function() {
               if ($(this).is(':checked')) {    

               var result = $(this).attr('id').split('_');
               var checked = result[2];
               tmp.push(checked);
               }
            });         
       }); 
        
      });

               
              working_hours_timer();},
           });
         });


      $(document).ready(function () {
    //var table = $('#alltask').DataTable();           
         var tmp = []; 
       $('#alltask tbody').on('click', '.overall_tasks', function(e){
             $(".assign_delete").show();    
           $(".overall_tasks").each(function() {
               if ($(this).is(':checked')) {    

               var result = $(this).attr('id').split('_');
               var checked = result[2];
               tmp.push(checked);
               }
            });         
     // alert(tmp);    

          });     
             
        $(document).on('click' , "#assign_member" , function(){
      //   alert('assign');
            var alltask = [];
  tabletask1.column(1).nodes().to$().each(function(index) {
      if($(this).find(".alltask_checkbox").is(":checked"))
         alltask.push($(this).find(".alltask_checkbox").data('alltask-id'));
      
      });

 // alert(alltask);

  if(alltask.length <=0) {
     $('.popup_info_msg').show();
     $(".popup_info_msg .position-alert1").html("Please Selecte Any Records..");
  } else {
    //$('#delete_user'+alltask).show();
   // $('.delete_yes').click(function() {
       var selected_alltask_values = alltask.join(",");

    //  alert(selected_alltask_values);
      //  $("#task_assign").modal('show');

      // $('#task_assign').modal({show: true, backdrop: 'static', keyboard: false});
       $(".assigned_staff").click(function(){
       var assign_to= $(".workers_assign").val();

       var formData={'task_id':selected_alltask_values,'staff_id':assign_to};
      // alert(assign_to);
       $.ajax({
        type: "POST",
        url: "<?php echo base_url().'user/alltasks_assign';?>",
        cache: false,
        data: formData,
        beforeSend: function() {
          $(".LoadingImage").show();
        },
        success: function(data) {
          $(".LoadingImage").hide();
          var json = JSON.parse(data); 
          status=json['status'];
          $(".popup_info_msg").show();
          $(".popup_info_msg .position-alert1").html(" Staff Assigned Successfully...");
          if(status=='1'){
           setTimeout(function(){ $(".popup_info_msg").hide(); location.reload(); }, 3000); 
          }           
        }
      });

       });

      // $.ajax({
      //   type: "POST",
      //   url: "<?php //echo base_url().'user/alltasks_delete';?>",
      //   cache: false,
      //   data: 'data_ids='+selected_alltask_values,
      //   beforeSend: function() {
      //     $(".LoadingImage").show();
      //   },
      //   success: function(data) {
      //     $(".LoadingImage").hide();
      //     var task_ids = data.split(",");
      //     for (var i=0; i < task_ids.length; i++ ) { 
      //     $("#"+task_ids[i]).remove(); } 
      //     $(".alert-success-delete").show();
      //     setTimeout(function() { 
      //     location.reload(); }, 500);     
      //   }
      // });
   // });
  }
         });

         //  $("#delete_task").click(function(){
         //    $('#delete_user'+tmp).show();
         //       $('.delete_yes').click(function() {
         //          var formData={'id':tmp};
         //       $.ajax({
            
         //         url: '<?php echo base_url();?>user/tasks_delete',
         //         type : 'POST',        
         //         data : formData,
         //         beforeSend: function() {
         //           $(".LoadingImage").show();
         //         },
         //         success: function(data) {
         //         // alert(data);
         //            $(".LoadingImage").hide();
         //           location.reload();
         //         }
         //    });
         //         }); 
            
         // }); 
       
        
      });
$(".overall_tasks").each(function() {
               if ($(this).is(':checked')) {    

               var result = $(this).attr('id').split('_');
               var checked = result[2];
               tmp.push(checked);
               }
            }); 
</script>
<script type="text/javascript"> 
function alltask_delete(id)
{
   $('#delete_alltask'+id).show();
   return false;
}  

function inprogresses_delete(id)
{
   $('#delete_inprogresses'+id).show();
   return false;
} 
function started_delete(id)
{
   $('#delete_started'+id).show();
   return false;
} 
function notstarted_delete(id)
{
   $('#delete_notstarted'+id).show();
   return false;
} 

$(document).on('click','#close',function(e)
{
   $('.alert-success').hide();
   return false;
});

</script>
<script type="text/javascript">
 $(document).ready(function() {
   

 $(document).on('click','#delete_task', function() {

  var alltask = [];

  tabletask1.column(1).nodes().to$().each(function(index) {
      if($(this).find(".alltask_checkbox").is(":checked"))
      {
        alltask.push($(this).find(".alltask_checkbox").data('alltask-id'));          
      }
      });
  
    //alert(JSON.stringify(alltask));
    //$("#delete_task_id").attr("value",JSON.stringify(alltask));
      $("#delete_task_id").val(JSON.stringify(alltask));


    });

 });  






  $( document ).ready(function() {
  
    $.ajax( {
  
       type:"post",
       dataType : "json",
       url:"<?php echo base_url();?>user/get_projects",
       processData: false,
       contentType: false,
        data:{ 
         'get':'get'
       }
       ,
       success:function(response)
       {
  
  
       var ret='<div id="frame_projects"><label>Projects</label><select name="r_project" class="form-control" id="r_projects" placeholder="select">';
  
           $.each(response, function (index, value){
           ret+='<option value='+value.id+'>'+value.project_name+'</option>';
           });
  
         ret+='</select><div>';
  
        $("#base_projects").before(ret);
       }
   });
  
  
      var date = $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
  
      // Multiple swithces
      var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));
  
      elem.forEach(function(html) {
          var switchery = new Switchery(html, {
              color: '#1abc9c',
              jackColor: '#fff',
              size: 'small'
          });
      });
  
      $('#accordion_close').on('click', function(){
              $('#accordion').slideToggle(300);
              $(this).toggleClass('accordion_down');
      });
  
     
      
  });
</script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
  $("#importFrm").validate({
    
         ignore: false,
  
                          rules: {
                         
                          file: {required: true, accept: "csv"},
                          priority : { required : true },
                          "tag[]" : { required : true },
                          r_project : { required : true },
                          "worker[]" : { required : true },
                          "manager[]" : { required : true },
                          
                          },
                          errorElement: "span" , 
                          errorClass: "field-error",                             
                           messages: {
                            
                            file: {required: 'Required!', accept: 'Please upload a file with .csv extension'},
                            priority: {required: 'Required'},
                            "tag[]": {required: 'Required'},
                            r_project: {required: 'Required'},
                            "worker[]": {required: 'Required'},
                            "manager[]": {required: 'Required'},
                           
                           },
                           
  
                          
                          submitHandler: function(form) {
                              var formData = new FormData($("#importFrm")[0]);
                              
  
                              $(".LoadingImage").show();
  
                              $.ajax({
                                  url: '<?php echo base_url();?>tasksummary/import_tasks',
                                  dataType : 'json',
                                  type : 'POST',
                                  data : formData,
                                  contentType : false,
                                  processData : false,
                                  success: function(data) {
                                      
                                       //  $('#messages').html('<p class="common-tag"><b>'+data.count+'</b> <p class="success-msg">Task has successfully imported.Please click on </p> <span class="view-btn"><a class="view-client" href="<?php echo base_url()."user/task_list/'+data.userid+'"?>">View Task</a></span></p>');
                                       // $('#confirm-submit').modal('toggle');
                                       // $(".LoadingImage").hide();
                                       if(data!=0){
                                        $(".popup_info_msg").show();
               $('.popup_info_msg .position-alert1').html('Success !!! New Task Imported successfully...');
               setTimeout(function () {
                $(".popup_info_msg").hide();
               //  location.reload();
                 window.location.replace("<?php echo base_url(); ?>user/task_details/"+data);
                }, 1500);
             }
              
  
                                       //location.reload(true);
                                   //window.location = "<?php echo base_url();?>tasksummary/import_task?";
  
                                  },
                                  error: function() { $('.alert-danger').show();
                                          $('.popup_info_msg').hide();}
                              });
  
                              return false;
                          } ,
                           invalidHandler: function(e, validator) {
             if(validator.errorList.length)
          $('#tabs a[href="#' + jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
  
          }
                           
                      });
  
  
  });
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#country").change(function(){
    var country_id = $(this).val();
    //alert(country_id);
  
      $.ajax({
  
        url:"<?php echo base_url().'Client/state';?>",
        data:{"country_id":country_id},
        type:"POST",
        success:function(data){
          //alert('hi');
          $("#state").append(data);
          
        }
  
      });
    });
     $("#state").change(function(){
    var state_id = $(this).val();
    //alert(country_id);
  
      $.ajax({
  
        url:"<?php echo base_url().'Client/city';?>",
        data:{"state_id":state_id},
        type:"POST",
        success:function(data){
          //alert('hi');
          $("#city").append(data);
          
        }
  
      });
    });
  
  
  
  $("#related_to").change(function () {
       var val = this.value;
  
       if(val=='projects'){
  
            $.ajax( {
  
       type:"post",
       dataType : "json",
       url:"<?php echo base_url();?>user/get_projects",
       processData: false,
       contentType: false,
        data:{ 
         'get':'get'
       }
       ,
       success:function(response)
       {
  
  
       var ret='<div id="frame_projects"><label>Projects</label><select name="r_project" class="form-control" id="r_projects" placeholder="select">';
  
           $.each(response, function (index, value){
           ret+='<option value='+value.id+'>'+value.project_name+'</option>';
           });
  
         ret+='</select><div>';
  
        $("#base_projects").before(ret);
       }
   });
  
  
       }else{
       
       $('#frame_projects').remove();
       }
       
   });
  
  
  
  });

$(document).ready(function(){      




 
        <?php
        if(isset($_SESSION['firm_seen'])){
        if($_SESSION['firm_seen']=='complete'){ ?>
          $("div.comple_task").trigger('click');
       <?php } ?>

        <?php
        if($_SESSION['firm_seen']=='notstarted'){ ?>
          $("div.nots_task").trigger('click');
       <?php } ?>


        <?php
        if($_SESSION['firm_seen']=='inprogress'){ ?>
          $("div.inpro_task").trigger('click');
       <?php } ?>

         <?php
        if($_SESSION['firm_seen']=='low'){ ?>
          $("#prioritywise_filter").val('low').trigger('change');
       <?php } ?>

         <?php
        if($_SESSION['firm_seen']=='medium'){ ?>
          $("#prioritywise_filter").val('medium').trigger('change');
       <?php } ?>

         <?php
        if($_SESSION['firm_seen']=='high'){ ?>
          $("#prioritywise_filter").val('high').trigger('change');
       <?php } ?>

        <?php
        if($_SESSION['firm_seen']=='super_urgent'){ ?>
          $("#prioritywise_filter").val('super_urgent').trigger('change');
       <?php } ?>


       <?php } ?>
   });
  </script>
<!-- for new timer -->
<script type="text/javascript">
$(document).on('click','.for_timer_start_pause',function(){
var task_id=$(this).attr('id');
//var task_id=$(this).parent().attr("id");
$.ajax({
url: '<?php echo base_url();?>user/task_timer_start_pause/',
type: 'post',
data: { 'task_id':task_id },
timeout: 3000,
success: function( data ){
//  alert('updated');
}
});
$.ajax({
url: '<?php echo base_url();?>user/task_timer_start_pause_individual/',
type: 'post',
data: { 'task_id':task_id },
timeout: 3000,
success: function( data ){
//  alert('updated');
}
});
});

</script>
<!-- end of new timer -->
<!-- for customize permission style -->
<script type="text/javascript">
$(document).ready(function(){
 <?php foreach ($custom_permission as $style_key => $style_value) { 
  ?>
//$('td a.adduser1.per_assigne_<?php echo $style_value['id']; ?>').css('display','none');  
//$(".per_chkbox_<?php echo $style_value['id']; ?>").html('<div class="checkbox-fade fade-in-primary"><label>--</label></div>');
  <?php
if(!in_array($style_value['id'],$for_user_edit_per)){
  ?>
$(".per_chkbox_<?php echo $style_value['id']; ?>").html('<div class="checkbox-fade fade-in-primary"><label>--</label></div>');
//$('button.per_timerplay_<?php echo $style_value['id']; ?>').css('display','none');
//$('td a.per_assigne_<?php echo $style_value['id']; ?>').css('display','none');
$('td a.adduser1.per_assigne_<?php echo $style_value['id']; ?>').css('display','none');  
$('.per_action_<?php echo $style_value['id']; ?>').css('display','none');
$('.per_taskstatus_<?php echo $style_value['id']; ?>,.per_taskpriority_<?php echo $style_value['id']; ?>').css('display','none');
 <?php }
  } ?>
});

 $(document).on('click', 'th .themicond', function(){
if ($(this).parent().find(".dropdown-content").hasClass("Show_content")) {
  $(this).parent().find('.dropdown-content').removeClass('Show_content');
}else{
   $('.dropdown-content').removeClass('Show_content');
   $(this).parent().find('.dropdown-content').addClass('Show_content');
}
  $(this).parent().find('.select-wrapper').toggleClass('special');
if( $(this).parent().find('.select-dropdown span input[type="checkbox"]').parent().hasClass('custom_checkbox1')){
   // alert('yes');
 
   }else{
$(this).parent().find('.select-dropdown span input[type="checkbox"]').wrap( '<label class="custom_checkbox1"></label>');
$(this).parent().find('.custom_checkbox1 input').after( "<i></i>" );
}
});

// $('tr .show_row').click(function(){

// $(this).closest('tr').next('.restcls').toggle();
// })




      $("th").on("click.DT", function (e) {
     //  alert("yes1");
      
        if (!$(e.target).hasClass('sortMask')) {
          //alert("inside");
            e.stopImmediatePropagation();
        }
    });





//  function details_check(details) {


// var fff= $(details).attr('class');
// $(".details-control").trigger('click');

//      // });
//   }



  $(document).on('change','.task_status',function(){
       var rec_id = $(this).data('id');

     //  alert(rec_id);
       var stat = $(this).val();
       if(stat=='')return;
          $.ajax({
            url: '<?php echo base_url();?>user/task_statusChange/',
            type: 'post',
            data: { 'rec_id':rec_id,'status':stat },
            timeout: 3000,
            beforeSend:function(){$(".LoadingImage").show();},
            success: function( data ){
               $(".LoadingImage").hide();

               $(".popup_info_msg").show();
               $('.popup_info_msg .position-alert1').html('Success !!! Task status have been changed successfully...');
                $('.taskpagedashboard').load("<?php base_url();?>task_summary_data");
               setTimeout(function () {
                $(".popup_info_msg").hide();
                }, 1500);
             
                },
                error: function( errorThrown ){
                    console.log( errorThrown );
                }
            });          
        
   });




 $(document).on('change','.task_priority',function () {                                
      
       var rec_id = $(this).data('id');       
       var priority = $(this).val();
       if(priority=='')return;
          $.ajax({
            url: '<?php echo base_url();?>user/task_priorityChange/',
            type: 'post',
            data: { 'rec_id':rec_id,'priority':priority },
            timeout: 3000,
            beforeSend:function(){$(".LoadingImage").show();},
            success: function( data ){
              
              $(".LoadingImage").hide();
              $(".popup_info_msg").show();
               $('.popup_info_msg .position-alert1').html('Success !!! Task Priority have been changed successfully...');
               setTimeout(function () {
                $(".popup_info_msg").hide(); 

                }, 1500);
             
                },
                error: function( errorThrown ){
                    console.log( errorThrown );
                }
            });           
        
   });



$('.suspent').click(function() {
        if($(this).is(':checked'))
            var stat = '1';
        else
            var stat = '0';
        var rec_id = $(this).val();
        var $this = $(this);
         $.ajax({
            url: '<?php echo base_url();?>user/suspentChange/',
            type: 'post',
            data: { 'rec_id':rec_id,'status':stat },
            success: function( data ){
               //alert('ggg');
                $("#suspent_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! User status have been changed successfully...</span></div></div></div>');
                if(stat=='1'){
                  
                   $this.closest('td').next('td').html('Payment');
      
                } else {
                   $this.closest('td').next('td').html('Non payment');
                }
                },
                error: function( errorThrown ){
                    console.log( errorThrown );
                }
            });
      });



 $('#alluser').on('change','.status',function () {
      //e.preventDefault();  
       var rec_id = $(this).data('id');
       var stat = $(this).val();      
      $.ajax({
            url: '<?php echo base_url();?>user/statusChange/',
            type: 'post',
            data: { 'rec_id':rec_id,'status':stat },
            timeout: 3000,
            success: function( data ){
               //alert('ggg');
                $("#status_succ").html('<div class="card borderless-card" id="timeout"><div class="card-block success-breadcrumb"><div class="breadcrumb-header"><span>Success !!! User status have been changed successfully...</span></div></div></div>').hide().fadeIn(1500, function() { $('#status_succ'); });
               // setTimeout(resetAll,3000);
                if(stat=='3'){
                  
                   //$this.closest('td').next('td').html('Active');
                   $('#frozen'+rec_id).html('Frozen');
      
                } else {
                   $this.closest('td').next('td').html('Inactive');
                }
                },
                error: function( errorThrown ){
                    console.log( errorThrown );
                }
            });
       });


$('th .themicond').on('click', function(e) {
 // alert("ok");
  if ($(this).parent().find(".dropdown-content").hasClass("Show_content")) {
  $(this).parent().find('.dropdown-content').removeClass('Show_content');
  }else{
   $('.dropdown-content').removeClass('Show_content');
   $(this).parent().find('.dropdown-content').addClass('Show_content');
  }
  $(this).parent().find('.select-wrapper').toggleClass('special');
      if( $(this).parent().find('.select-dropdown span input[type="checkbox"]').parent().hasClass('custom_checkbox1')){
       // alert('yes');

      }else{
        $(this).parent().find('.select-dropdown span input[type="checkbox"]').wrap( '<label class="custom_checkbox1"></label>');
        $(this).parent().find('.custom_checkbox1 input').after( "<i></i>" );

      }
});



 



$(document).on('click','.DT', function (e) {
        if (!$(e.target).hasClass('sortMask')) {
          //alert("inside");
            e.stopImmediatePropagation();
        }
    });



$(document).on("change", "#select_alltask", function(event) {

   var table=$(this).attr("id").replace("select_","");
   var checked = this.checked;
     $(".LoadingImage").show();
    tabletask1.column(1).nodes().to$().each(function(index) {
        if (checked)
         {         
            $(this).find('.'+table+'_checkbox').prop('checked',true);
            if($("#alltask").attr("data-status")=="archive")$("#unarchive_task").show();
            else $('#archive_task').show(); 
            $("#assign_member").show();
            $("#delete_task").show();           
        } 
        else
         {

            $(this).find('.'+table+'_checkbox').prop('checked',false);
            if($("#alltask").attr("data-status")=="archive")$('#unarchive_task').hide();
            else $("#archive_task").hide();
            $("#assign_member").hide();
            $("#delete_task").hide();        
        }
    });
    $(".LoadingImage").hide();
    tabletask1.draw();
    //$("#select_"+table+"_count").val($("input."+table+"_checkbox:checked").length + " Selected");
  });

$(document).on("change",".alltask_checkbox",function(event)
    { 
      var tr=0;
      var ck_tr=0;
      tabletask1.column(1).nodes().to$().each(function(index) {
        
      if($(this).find(".alltask_checkbox").is(":checked"))ck_tr++;
      tr++;
      });
       //  alert("re"+$("#alltask").attr("data-status")+ck_tr);
        if(tr==ck_tr)
          {
            $("#select_alltask").prop("checked",true);
           } 
        else
        {
         $("#select_alltask").prop("checked",false);
        } 
      if(ck_tr)
      {
           // alert(ck_tr);
            if($("#alltask").attr("data-status")=="archive")$("#unarchive_task").show();
            else $('#archive_task').show(); 
            $("#assign_member").show();
            $("#delete_task").show();  
      }
      else  
      {
            if($("#alltask").attr("data-status")=="archive")$('#unarchive_task').hide();
            else $("#archive_task").hide();
            $("#assign_member").hide();
            $("#delete_task").hide();  
      } 
      tabletask1.draw();
          $("#select_alltask_count").val($("input.alltask_checkbox:checked").length+" Selected");
       
    });
  
$("#close_action_result").click(function(){$(".popup_info_msg").hide();});//for close mssage box
$("#close_info_msg").click(function(){$(".popup_info_msg").hide(); });

/*import tap  select company assignee */

$("select[name='company_name']").change(function(){


       var id = $(this).val();

       var manager_select = $(this).closest('form').find("select[name='manager[]']");
       var workers_select = $(this).closest('form').find("select[name='worker[]']");

      $('#user_id').val(id);
      
      
      if(id!='')
      {

        //for un select
  /*    manager_select.parent().find(".dropdown-main ul li.dropdown-chose").trigger("click");
      manager_select.prop("selected",false);

      workers_select.parent().find(".dropdown-main ul li.dropdown-chose").trigger("click");
      workers_select.prop("selected",false);
  */   
      //for un select




       // alert(id);

        $.ajax({
          url : "<?php echo base_url()?>user/get_company_assign",
          type : "POST",
          data : {"id":id},
          dataType : "json",        
          beforeSend : function(){
            $(".LoadingImage").show();
          },
          success: function(data){
            $(".LoadingImage").hide();  

            var manager = data.manager;

            for(i=0;i < manager.length;i++)
            {
              manager_select.find("option[value="+manager[i]+"]").attr("selected",true);
              //$(".dropdown-sin-3 .dropdown-main ul li[data-value="+manager[i]+"]").addClass("dropdown-chose");
              manager_select.parent().find(".dropdown-main ul li[data-value="+manager[i]+"]").trigger("click");
              //alert("manager-sin"+$(".dropdown-sin-3 .dropdown-main ul li[data-value="+manager[i]+"]").attr("class"));
            }
            
            var assign = data.assignee;

            for(i=0;i < assign.length;i++)
            {
              workers_select.find("option[value="+assign[i]+"]").attr("selected",true);
              //$(".dropdown-sin-2 .dropdown-main ul li[data-value="+assign[i]+"]").addClass("dropdown-chose");
              workers_select.parent().find(".dropdown-main ul li[data-value="+assign[i]+"]").trigger("click");
              //alert("assign-sin"+$(".dropdown-sin-2 .dropdown-main ul li[data-value="+assign[i]+"]").attr("class"));
            }

  
          }
      });
      
      }   
});

 </script>
    
<!-- <script type="text/javascript">
    $(document).ready(function(){
  
 var fruits = [];

$('.data_active_task th').each(function(){
fruits.push($(this).outerWidth());
  console.log(fruits);
}); 



});
</script> -->
 