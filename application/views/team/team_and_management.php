<?php $this->load->view('includes/header');?>
<style>
   /*span.newonoff {
   background: #4680ff;
   padding: 6px 15px;
   display: inline-block;
   color: #fff;
   border-radius: 5px;
   }*/
   button.btn.btn-info.btn-lg.newonoff {
   padding: 3px 10px;
   height: initial;
   font-size: 15px;
   border-radius: 5px;
   }
</style>
<!-- management block -->
<div class="pcoded-content team_management-data card-removes">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <div class="card deadline-block1 import-container-1 team-spl">
			   
			   <div class="deadline-crm1 floating_set single-txt12 ddline">
               <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                  <li class="nav-item">
                     team & management
                  </li>
               </ul>
            </div>
			
				<div class="team-design01 floating_set">
					<div class="number-mydisk floating_set">
						<div class="count-mydisk1">
							<div class="target-disk">
							<a href="<?php echo base_url().'user/staff_list';?>">
								<span>view staff</span>
								<div class="count-pos-01">
									<strong><i class="fa fa-user-plus"></i></strong>
									</div>
								</a>
							</div>							
						</div>
						
						<div class="count-mydisk1">
							<div class="target-disk">
							<a href="<?php echo base_url().'department/dept_permission';?>">
								<span>view department</span>
								<div class="count-pos-01">
									<strong><i class="fa fa-plus-circle"></i></strong>
									</div>
								</a>
							</div>							
						</div>
						
						<div class="count-mydisk1">
							<div class="target-disk">
							<a href="<?php echo base_url().'team';?>">
								<span>view team</span>
								<div class="count-pos-01">
									<strong><i class="fa fa-plus-circle"></i></strong>
									</div>
								</a>
							</div>							
						</div>
						
						<!-- staff list -->
						
						<div class="count-mydisk1">
							<div class="target-disk">
							<a href="<?php echo base_url().'staff';?>">
								<span>add staff</span>
								<div class="count-pos-01">
									<strong><i class="fa fa-user-plus"></i></strong>
									</div>
								</a>
							</div>							
						</div>
						
						<div class="count-mydisk1">
							<div class="target-disk">
							<a href="<?php echo base_url().'department/department_permission';?>">
								<span>add department</span>
								<div class="count-pos-01">
									<strong><i class="fa fa-plus-circle"></i></strong>
									</div>
								</a>
							</div>							
						</div>
						
						<div class="count-mydisk1">
							<div class="target-disk">
							<a href="javascript:;" data-toggle="modal" data-target="#add_currency12">
								<span>add team</span>
								<div class="count-pos-01">
									<strong><i class="fa fa-plus-circle"></i></strong>
									</div>
								</a>
							</div>							
						</div>
						
						<!-- assign -->
						
						<div class="count-mydisk1">
							<div class="target-disk">
							<a href="<?php echo base_url().'Role_Assign/reassign';?>">
								<span>Assign</span>
								<div class="count-pos-01">
									<strong><i class="fa fa-plus-circle"></i></strong>
									</div>
								</a>
							</div>							
						</div>
						
						<div class="count-mydisk1">
							<div class="target-disk">
							<a href="<?php echo base_url().'Role_Assign';?>">
								<span>Add Role</span>
								<div class="count-pos-01">
									<strong><i class="fa fa-plus-circle"></i></strong>
									</div>
								</a>
							</div>							
						</div>
					</div>
				
                       <!-- <div class="teamright-sidebar">
                           <div class="add-staff">
                              <div class="staff-team01 view-dept-section">
                                 <ul class="nav nav-tabs">
                                    <li class="active">
									   <a href="<?php echo base_url().'user/staff_list';?>"><i class="fa fa-user-plus"></i>view staff</a>
                                    </li>
                                    <li>
                                       <a href="<?php echo base_url().'department/dept_permission';?>"><i class="fa fa-plus-circle"></i>view department</a>
                                    </li>
                                    <li>
                                       <a href="<?php echo base_url().'team';?>"><i class="fa fa-plus-circle"></i>view team</a>
                                    </li>
                                 </ul>
                              </div>
                              <div class="staff-team01 add-dept-section">
                                 <ul class="nav nav-tabs">
                                    <li class="active">
                                      <a href="<?php echo base_url().'staff';?>"><i class="fa fa-user-plus"></i>add staff</a>
                                    </li>
                                    <li>
                                       <a href="<?php echo base_url().'department/department_permission';?>"><i class="fa fa-plus-circle"></i>add department</a>
                                    </li>
                                    <li>
                                       <a href="javascript:;" data-toggle="modal" data-target="#add_currency12"><i class="fa fa-plus-circle"></i>add team</a>
                                    </li>
                                 </ul>
                              </div>
                 
                    <div class="staff-team01 assign-dept-section">
                                 <ul class="nav nav-tabs">
                                    <li class="active">
                                       <a href="<?php echo base_url().'Role_Assign/reassign';?>"><i class="fa fa-plus-circle"></i>Assign</a>
                                    </li>
                                    <li>
                                       <a href="<?php echo base_url().'Role_Assign';?>"><i class="fa fa-plus-circle"></i>Add Role </a>
                                    </li>
                                   
                                 </ul>
                      </div>
                 
                              <div class="department-section1">
                                 <div class="tab-content">
                                    <div id="home" class="tab-pane fade">
                                       <h6>staff</h6>
                                    </div>
                                    <div id="menu1" class="tab-pane fade in active">
                                       <h6>departments</h6>
                                       <div class="admins-details">
                                          <?php if(isset($new_department)){
                                             foreach ($new_department as $key => $value) {
                                              
                                              ?>
                                          <div class="dep-sec">
                                             <span class="depart-roles">
                                             <?php echo $value['new_dept']; ?>
                                             </span>
                                             <div class="edit-delete">
                                                <a href="<?php echo base_url().'department/update_permission/'.$value['id'];?>"><span class="edit-op"><i class="fa fa-pencil"></i></span></a>
                                                <a href="#" data-toggle="modal" data-target="#modaldeptdelete<?php echo $value['id']?>"><span class="delete-op"><i class="fa fa-trash"></i></span></a>
                                             </div>
                                          </div>
                                          <?php } } ?>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div> -->
                     </div>
			
               <!-- <span class="h5">team & management</span> -->
               <div class="user-inverstment team_management">
                  <div class="team-wid-rightsidebar">
                     <div class="col-xs-12 col-sm-12">
                     <div class="search-ur" style="float: right;">
                          <label>Search</label><input class="form-control form-control-lg" type="text" id="search-criteria" >
                            </div>
                        <div class="team-members">                         
                           <?php                            
                               
                                foreach ($staff_list as $getallUserkey => $getallUservalue) { 
                                  /** 30-05-2018 rs for checking  **/
                                  /** 26-07-2018 for accesspermission  **/
                                  if($_SESSION['role']==1)
                                  {
                              //$check_user=$this->Common_mdl->GetAllWithWhere('user','id',$getallUservalue['user_id']);
                              $check_user=$this->db->query("select * from user where id=".$getallUservalue['user_id']." and firm_admin_id!='' ")->result_array();
                                  }
                                  else
                                  {
                                  if($_SESSION['role']==5){
                                    $manager_assign=$this->Common_mdl->GetAllWithWhere('assign_manager','manager',$_SESSION['id']);
                                    if(count($manager_assign)>0){
                                    $it_staffs=explode(',', $manager_assign[0]['staff']);
                                     }
                                     else
                                     {
                                      $it_staffs=array();
                                     }
                               
                                     if(in_array($getallUservalue['user_id'], $it_staffs))
                                     {
                                       $check_user=$this->db->query("select * from user where id=".$getallUservalue['user_id']." ")->result_array();
                                      }
                                     else
                                     {
                                      $check_user=$this->db->query("select * from user where id=".$getallUservalue['user_id']." and firm_admin_id=".$_SESSION['id']." ")->result_array();
                                     
                                     }                                      
                                      }
                                      else
                                      {
                                        $check_user=$this->db->query("select * from user where id=".$getallUservalue['user_id']." and firm_admin_id=".$_SESSION['id']." ")->result_array();
                                      }
                                  }
                                  
                                    if(count($check_user)>0){
                                  /** end */
                                // $getallUservalue= $this->Common_mdl->select_record('staff_form','user_id',$value);
                              
                                 $getUserProfilepic = $this->Common_mdl->getUserProfilepic($getallUservalue['user_id']); 
                              
                                 if($getallUservalue['roles']=='1'){
                                                          //$role = 'Staff';
                                                           $role = 'Admin';
                                                        }elseif($getallUservalue['roles']=='2'){
                                                            $role = 'Sub Admin';
                                                        }elseif($getallUservalue['roles']=='3' ){
                                                            $role = 'Director';
                                                        }elseif($getallUservalue['roles']=='4' ){
                                                            $role = 'Client';
                                                        }elseif($getallUservalue['roles']=='5' ){
                                                            $role = 'Manager';
                                                        }elseif($getallUservalue['roles']=='6' ){
                                                            $role = 'Staff';
                                                        }else{
                                                           $role = $getallUservalue['roles'];
                                                        }
                                  ?>
                           <div class="sep-div">
                              <div class="step-bg01">
                                 <div class="top-img">
                                 </div>
                                 <div class="bottom-img">
                                    <span class="user-img"><img src="<?php echo $getUserProfilepic;?>" alt="img"></span>
                                    <div class="user-details">
                                      <a href="<?php echo base_url();  ?>staff/staff_profile/<?php echo $getallUservalue['user_id']; ?>"><span class="uname1 search_word"><?php echo ucfirst($getallUservalue['first_name']). ' '.$getallUservalue['last_name'];?></span></a>
                                       <p class=""> 
                                        <?php $firm_adid=$this->Common_mdl->get_field_value('user','firm_admin_id','id',$getallUservalue['user_id']); ?>
                                       <i class="fa fa-user fa-6" aria-hidden="true"></i>  <span class="search_word"><?php echo $role;?></span>
                                       <span class="created_by search_word"><?php echo "( Created BY:".$this->Common_mdl->get_field_value('user','crm_name','id',$firm_adid)." )" ?></span></p>
                                    </div>
                                 </div>
                                 <div class="edit-delete">
                                    <a href="<?php echo base_url().'staff/index/'.$getallUservalue['user_id'];?>"><span class="edit-op"><i class="fa fa-pencil"></i></span></a>
                                    <a href="#" data-toggle="modal" data-target="#modaldelete<?php echo $getallUservalue['user_id']?>"><span class="delete-op"><i class="fa fa-trash"></i></span></a> 
                                 </div>
                              </div>
                           </div>
                           <?php }// for check crnt user
                              } ?>
                           <!-- <div class="sep-div">
                              <div class="top-img">
                              </div>
                              <div class="bottom-img">
                                <span class="user-img"><img src="http://remindoo.org/CRMTool/assets/images/anon_user.png" alt="img"></span>
                                <div class="user-details">
                                  <span class="uname1">lance bogrol</span>
                                  <p><i class=" fa fa-address-book"></i> administrator</p>
                                </div>
                              </div>
                              <div class="edit-delete">
                                <span class="edit-op"><i class="fa fa-pencil"></i></span>
                                <span class="delete-op"><i class="fa fa-trash"></i></span>
                              </div>
                              </div>
                              <div class="sep-div">
                              <div class="top-img">
                              </div>
                              <div class="bottom-img">
                                <span class="user-img"><img src="http://remindoo.org/CRMTool/assets/images/anon_user.png" alt="img"></span>
                                <div class="user-details">
                                  <span class="uname1">lance bogrol</span>
                                  <p><i class=" fa fa-address-book"></i> administrator</p>
                                </div>
                              </div>
                              <div class="edit-delete">
                                <span class="edit-op"><i class="fa fa-pencil"></i></span>
                                <span class="delete-op"><i class="fa fa-trash"></i></span>
                              </div>
                              </div>
                              <div class="sep-div">
                              <div class="top-img">
                              </div>
                              <div class="bottom-img">
                                <span class="user-img"><img src="http://remindoo.org/CRMTool/assets/images/anon_user.png" alt="img"></span>
                                <div class="user-details">
                                  <span class="uname1">lance bogrol</span>
                                  <p><i class=" fa fa-address-book"></i> administrator</p>
                                </div>
                              </div>
                              <div class="edit-delete">
                                <span class="edit-op"><i class="fa fa-pencil"></i></span>
                                <span class="delete-op"><i class="fa fa-trash"></i></span>
                              </div>
                              </div>-->

<!-- for team and management staff login -->
<?php  if($_SESSION['role']==6){ ?>
<?php 
   $team_num=array();
         $for_cus_team=$this->db->query("select * from team_assign_staff where FIND_IN_SET('".$_SESSION['id']."',staff_id)")->result_array();
    foreach ($for_cus_team as $cu_team_key => $cu_team_value) {
      array_push($team_num,$cu_team_value['team_id']);
    }
    if(!empty($team_num) && count($team_num)>0){
      $res_team_num=implode('|', $team_num);
      $res_team_num1=implode(',', $team_num);
    }
    else
    {
      $res_team_num='0';
      $res_team_num1='0';
    }

    $department_num=array();
    $for_cus_department=$this->db->query("SELECT * FROM `department_assign_team` where CONCAT(',', `team_id`, ',') REGEXP ',($res_team_num),'")->result_array();
    foreach ($for_cus_department as $cu_dept_key => $cu_dept_value) {
          array_push($department_num,$cu_dept_value['depart_id']);
      }
    if(!empty($department_num) && count($department_num)>0){
      $res_dept_num=implode('|', $department_num);
      $res_dept_num1=implode(',', $department_num);
    }
    else
    {
        $res_dept_num='0';
        $res_dept_num1='0';
    }
/** maanger detailes **/
   $manger_num=array();
         $for_cus_manager=$this->db->query("select * from assign_manager where FIND_IN_SET('".$_SESSION['id']."',staff)")->result_array();
    foreach ($for_cus_manager as $cu_manager_key => $cu_manager_value) {
      array_push($manger_num,$cu_manager_value['manager']);
    }
    if(!empty($manger_num) && count($manger_num)>0){
      $res_manager_num=implode('|', $manger_num);
      $res_manager_num1=implode(',', $manger_num);
    }
    else
    {
      $res_manager_num='0';
      $res_manager_num1='0';
    }
/** end of manager **/
/** director **/
  $director_num=array();
    $for_cus_director=$this->db->query("SELECT * FROM `assign_director` where CONCAT(',', `manager`, ',') REGEXP ',($res_manager_num),'")->result_array();
    foreach ($for_cus_director as $cu_dir_key => $cu_dir_value) {
          array_push($director_num,$cu_dir_value['director']);
      }
    if(!empty($director_num) && count($director_num)>0){
      $res_dir_num=implode('|', $director_num);
      $res_dir_num1=implode(',', $director_num);
    }
    else
    {
        $res_dir_num='0';
        $res_dir_num1='0';
    }
/** end of director **/

/* responsible team **/
if($res_team_num1!='0'){
  ?>
<!--   <div class="page-header card">
                  <div class="align-items-end">
                     <div class="page-header-title">
                        <div class="d-inline">
                           <h4>Team</h4>
                        </div>
                     </div>
                  </div>
               </div> -->
  <?php
  foreach ($team_num as $team_key => $team_value) {
    ?>
      <div class="sep-div">
         <div class="step-bg01">
              <div class="top-img">
              </div>
              <div class="bottom-img">
                <span class="user-img"><img src="<?php echo base_url().'uploads/unknown.png'; ?>" alt="img"></span>
                <div class="user-details">
                  <span class="uname1"><?php echo $this->Common_mdl->get_field_value('team','team','id',$team_value); ?></span>
                   <?php $firm_adid=$this->Common_mdl->get_field_value('team','create_by','id',$team_value); ?>
                  <p><i class=" fa fa-address-book"></i> Team 
                  <span class="created_by"><?php echo "( Created BY:".$this->Common_mdl->get_field_value('user','crm_name','id',$firm_adid)." )" ?></span></p>
                </div>
              </div>
      <!--         <div class="edit-delete">
                <span class="edit-op"><i class="fa fa-pencil"></i></span>
                <span class="delete-op"><i class="fa fa-trash"></i></span>
              </div> -->
              </div>
      </div>
    <?php
    
  }
}
/* responsible team **/
if($res_dept_num1!='0'){
  ?>
<!--   <div class="page-header card">
                  <div class="align-items-end">
                     <div class="page-header-title">
                        <div class="d-inline">
                           <h4>Department</h4>
                        </div>
                     </div>
                  </div>
               </div> -->
  <?php
  foreach ($department_num as $dept_key => $dept_value) {
    ?>
      <div class="sep-div">
         <div class="step-bg01">
                              <div class="top-img">
                              </div>
                              <div class="bottom-img">
                                <span class="user-img"><img src="<?php echo base_url().'uploads/unknown.png'; ?>" alt="img"></span>
                                <div class="user-details">
                                  <span class="uname1"><?php echo $this->Common_mdl->get_field_value('department_permission','new_dept','id',$dept_value); ?></span>
                                   <?php $firm_adid=$this->Common_mdl->get_field_value('department_permission','create_by','id',$team_value); ?>
                                  <p><i class=" fa fa-address-book"></i> Department 
                                  <span class="created_by"><?php echo "( Created BY:".$this->Common_mdl->get_field_value('user','crm_name','id',$firm_adid)." )" ?></span>
                                  </p>
                                </div>
                              </div>
                          <!--     <div class="edit-delete">
                                <span class="edit-op"><i class="fa fa-pencil"></i></span>
                                <span class="delete-op"><i class="fa fa-trash"></i></span>
                              </div> -->
                              </div>
                          </div>
    <?php
    
  }
}

/** for manager section 10-08-2018 **/
if($res_manager_num1!='0'){
  ?>
  <?php
  foreach ($manger_num as $manager_key => $manager_value) {
      $getUserProfilepic = $this->Common_mdl->getUserProfilepic($manager_value);
    ?>
      <div class="sep-div">
         <div class="step-bg01">
                              <div class="top-img">
                              </div>
                              <div class="bottom-img">
                                <span class="user-img"><img src="<?php echo $getUserProfilepic;?>" alt="img" onerror=this.src="<?php echo base_url().'uploads/unknown.png'; ?>" ></span>
                                <div class="user-details">
                                  <span class="uname1"><?php echo $this->Common_mdl->get_field_value('user','crm_name','id',$manager_value); ?></span>
                                <?php $firm_adid=$this->Common_mdl->get_field_value('user','firm_admin_id','id',$manager_value); ?>
                                  <p><i class=" fa fa-address-book"></i> Manager  
                                  <span class="created_by"><?php echo "( Created BY:".$this->Common_mdl->get_field_value('user','crm_name','id',$firm_adid)." )" ?></span>
                                  </p>
                                </div>
                              </div>
                          <!--     <div class="edit-delete">
                                <span class="edit-op"><i class="fa fa-pencil"></i></span>
                                <span class="delete-op"><i class="fa fa-trash"></i></span>
                              </div> -->
                              </div>
                          </div>
    <?php
    
  }
}
/** end of manager **/
/** for manager section 10-08-2018 **/
if($res_dir_num1!='0'){
  ?>
  <?php
  foreach ($director_num as $director_key => $director_value) {
    $getUserProfilepic = $this->Common_mdl->getUserProfilepic($director_value);
    ?>
      <div class="sep-div">
         <div class="step-bg01">
                              <div class="top-img">
                              </div>
                              <div class="bottom-img">
                                <span class="user-img"><img src="<?php echo $getUserProfilepic;?>" alt="img" onerror=this.src="<?php echo base_url().'uploads/unknown.png'; ?>" ></span>
                                <div class="user-details">
                                  <span class="uname1"><?php echo $this->Common_mdl->get_field_value('user','crm_name','id',$director_value); ?></span>
                                  <?php $firm_adid=$this->Common_mdl->get_field_value('user','firm_admin_id','id',$manager_value); ?>
                                  <p><i class=" fa fa-address-book"></i> Director 
                                  <span class="created_by"><?php echo "( Created BY:".$this->Common_mdl->get_field_value('user','crm_name','id',$firm_adid)." )" ?></span>
                                  </p>
                                </div>
                              </div>
                          <!--     <div class="edit-delete">
                                <span class="edit-op"><i class="fa fa-pencil"></i></span>
                                <span class="delete-op"><i class="fa fa-trash"></i></span>
                              </div> -->
                              </div>
                          </div>
    <?php
    
  }
}
/** end of manager **/
?>
     
  <?php } ?>
<!-- end of team and management -->
                        </div>
                     </div>
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Main-body start -->
</div>
<!-- management block -->
<!-- delete team staff moadl -->
<?php foreach ($staff_list as $getallUserkey => $getallUservalue) { 
   //$getallUservalue= $this->Common_mdl->select_record('staff_form','user_id',$value); 
   ?>
<div id="modaldelete<?php echo $getallUservalue['user_id'];?>" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" style="text-align-last: center">Delete Staff from team</h4>
         </div>
         <div class="modal-body">
            <p>Are you sure want to delete?</p>
         </div>
         <form id="assign_new_team" method="post" action="<?php echo base_url().'staff/staff_delete/'.$getallUservalue['user_id'];?>" enctype="multipart/form-data">
            <input type="hidden" name="staff_id" value="<?php echo $getallUservalue['user_id'];?>">
            <input type="hidden" name="team_id" value="<?php echo $teams['id'];?>">
            <div class="modal-footer">
               <!--  <a href="<?php echo base_url().'team/team_staff_delete/'.$teams['id'];?>"><button type="submit" class="btn btn-default">Delete</button></a> -->
               <input type="submit" name="remove_team_staff" class="btn btn-default" value="delete"/>
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
         </form>
      </div>
   </div>
</div>
<?php }?>
<!-- close modal -->
<!-- delete department moadl -->
<?php if(isset($new_department)){
   foreach ($new_department as $key => $value) {
    
    ?>
<div id="modaldeptdelete<?php echo $value['id'];?>" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" style="text-align-last: center">Delete Department</h4>
         </div>
         <div class="modal-body">
            <p>Are you sure want to delete?</p>
         </div>
         <form id="assign_new_team" method="post" action="<?php echo base_url().'team/delete_department/'.$value['id'];?>" enctype="multipart/form-data">
            <input type="hidden" name="dept_id" value="<?php echo $value['id'];?>">
            <div class="modal-footer">
               <!--  <a href="<?php echo base_url().'team/team_staff_delete/'.$teams['id'];?>"><button type="submit" class="btn btn-default">Delete</button></a> -->
               <input type="submit" name="remove_dept" class="btn btn-default" value="delete"/>
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
         </form>
      </div>
   </div>
</div>
<?php } } ?>
<!-- close modal -->
<div class="modal fade show current-assignmembers" id="add_currency12" style="display: none;">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h4 class="modal-title">Add New Team</h4>
         </div>
         <div class="col-xs-12 inside-popup">
            <?php $this->load->view('team/add_team'); ?>
         </div>
      </div>
   </div>
</div>
<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<!-- <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap.js"></script> -->
<link rel='stylesheet' href='<?php echo  base_url()?>assets/css/fullcalendar.css' />
<script src='<?php echo  base_url()?>assets/js/moment.min.js'></script>
<script src='<?php echo  base_url()?>assets/js/fullcalendar.min.js'></script>
<script>
   $(function() {
   
   var todayDate = moment().startOf('day');
   var YM = todayDate.format('YYYY-MM');
   var YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
   var TODAY = todayDate.format('YYYY-MM-DD');
   var TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');
   
   $('#calendar').fullCalendar({
     header: {
       left: 'prev,next today',
       center: 'title',
       right: 'month,agendaWeek,agendaDay,listWeek'
     },
     editable: false,
     default:false,
     eventLimit: true, // allow "more" link when too many events
     navLinks: true,
     events: [
       
       {
         title: 'Meeting',
         start: YM + '-06',
         end: YM + '-10'
       },
       {
         
         title: 'Meeting',
         start: YM + '-07T16:00:00'
       },
         
       {
         title: 'Meeting',
         start: YESTERDAY,
         end: TOMORROW
       },
       {
         title: 'Meeting',
         start: YM + '-28',
         end: YM + '-28'
       },
       {
         title: 'Meeting',
         start: YM + 'T10:30:00',
         end: YM + 'T12:30:00'
       }
     ]
   });
   });
   
   
</script>
<script type="text/javascript">
   $(document).ready(function(){
     $('#column_setting').on('submit', function (e) {
   
             e.preventDefault();
    $(".LoadingImage").show();
             $.ajax({
               type: 'post',
               url: '<?php echo base_url()."User/column_update";?>',
               data: $('form').serialize(),
               success: function (data) {
                   if(data == '1'){
                   // $('#new_user')[0].reset();
                   $('.alert-success').show();
                   $('.alert-danger').hide();
                   location.reload();
                  // $('.all-usera1').load('<?php echo base_url()?>user .all-usera1');
                   
                 
                   }
                   else if(data == '0'){
                   // alert('failed');
                   $('.alert-danger').show();
                   $('.alert-success').hide();
                   }
                   $(".LoadingImage").hide();
               }
             });
   
           });
   });



     $('#search-criteria').keyup(function(){
      //$('.box_section').hide();
      var txt = $('#search-criteria').val();
       $(".sep-div").hide();
      $('.search_word').each(function(){ 
         if($(this).text().toUpperCase().indexOf(txt.toUpperCase()) != -1){
             $(this).show();  
            $(this).parents('.sep-div').show();
  
         }
      });
  });
   
</script>