<?php $this->load->view('includes/header'); ?>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.jOrgChart.css"/>

<div class="white-applys">
	<div class="white-applys1">

<ul id="org" style="display:none">
    <li>

         <div class="child_drag">
            <div class="child_imgs">
            <img src="https://remindoo.uk/uploads/unknown.png"></div>
            <div class="child_text">
                <span>henry bennett</span>
                <p>
                    chairman & ceo
                </p>
             </div>
           </div>     
       
       <ul>
         <li> <div class="child_drag">
            <div class="child_imgs">
            <img src="https://remindoo.uk/uploads/unknown.png"></div>
            <div class="child_text">
                <span>admin</span>
                <p>
                    chairman 
                </p>
             </div>
           </div> 
         </li>
         <li>
             <div class="child_drag">
            <div class="child_imgs">
            <img src="https://remindoo.uk/uploads/unknown.png"></div>
            <div class="child_text">
                <span>suganya</span>
                <p>
                    chairman & ceo
                </p>
             </div>
           </div> 

           <ul>
             <li> <div class="child_drag">
            <div class="child_imgs">
            <img src="https://remindoo.uk/uploads/unknown.png"></div>
            <div class="child_text">
                <span>Ajith</span>
                <p>
                    ceo
                </p>
             </div>
           </div>  </li>
              
           </ul>
         </li>
         <li class="fruit">
          <div class="child_drag">
            <div class="child_imgs">
            <img src="https://remindoo.uk/uploads/unknown.png"></div>
            <div class="child_text">
                <span>remindoo</span>
                <p>
                    chairman
                </p>
             </div>
           </div> 
           <ul>
             <li>
                <div class="child_drag">
            <div class="child_imgs">
            <img src="https://remindoo.uk/uploads/unknown.png"></div>
            <div class="child_text">
                <span>palani</span>
                <p>
                    chairman & ceo
                </p>
             </div>
           </div> 
               
             </li>
             <li> 
                <div class="child_drag">
            <div class="child_imgs">
            <img src="https://remindoo.uk/uploads/unknown.png"></div>
            <div class="child_text">
                <span>henry bennett</span>
                <p>
                    chairman & ceo
                </p>
             </div>
           </div> 
                
             </li>
           </ul>
         </li>
        
       </ul>
     </li>
   </ul>    

   <div id="chart" class="orgChart"></div>
      
   </div>
   </div> 

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-drag.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.jOrgChart.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/popper.js/js/popper.min.js"></script>  
<script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap/js/bootstrap.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.j-pro.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/switchery/js/switchery.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/js/datedropper.min.js"></script>
<script src="<?php echo base_url();?>assets/js/pcoded.min.js"></script> 
<script src="<?php echo base_url();?>assets/js/demo-12.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script> 
<script src="<?php echo base_url();?>assets/js/debounce.js"></script>
<script src="<?php echo base_url();?>assets/js/custom/test.js"></script>

<script>
    
</script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#org").jOrgChart({
            chartElement : '#chart',
            dragAndDrop  : true,

        });
      });
 
 
            
            /* Custom jQuery for the example */
            $("#show-list").click(function(e){
                e.preventDefault();
                
                $('#list-html').toggle('fast', function(){
                    if($(this).is(':visible')){
                        $('#show-list').text('Hide underlying list.');
                        $(".topbar").fadeTo('fast',0.9);
                    }else{
                        $('#show-list').text('Show underlying list.');
                        $(".topbar").fadeTo('fast',1);                  
                    }
                });
            });
            
            $('#list-html').text($('#org').html());
            
            $("#org").bind("DOMSubtreeModified", function() {
                $('#list-html').text('');
                
                $('#list-html').text($('#org').html());
                
                prettyPrint();                
            });
      
    </script>
