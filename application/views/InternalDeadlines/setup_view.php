
<?php $this->load->view('includes/header'); ?>
<link href="<?php echo base_url();?>assets/css/custom/internal-deadlines.css?<?=time()?>" rel="stylesheet" type="text/css" />
<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" type="text/css" /> -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    <div class="pcoded-content card-removes push-left" id="internal_deadlines_view" <?php echo ($_SESSION['user_type'] == 'FA' || $_SESSION['user_type'] == 'SA') ? '' : 'style="pointer-events: none;"'; ?>>
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper internal-deadlines-wrapper">
                    <div class="page-body">
                        <div class="row">                        
                            <div class="col-sm-12">
                                <div class="deadline-crm1 floating_set">  
                                    <ul class="nav nav-tabs all_user1 md-tabs floating_set u-dashboard client-top-nav">
                                        <li class="nav-item ">
                                            <a class="nav-link active" href="javascript:void(0)">Internal Deadlines</a>
                                            <div class="slide"></div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="toast-wrapper">
                                    <h2 style="margin: 20px 10px">Internal Deadlines</h2>
                                    <div id='idl_toast' class="toast">
                                        <div class="toast-body">
                                            Data updated..
                                        </div>
                                    </div>    
                                    <button type="button" style="display:none;" class="btn btn-primary" id="myBtn">Show Toast</button>
                                </div>
                            
                                <div id="internal_deadlines_setup">
                                    <table>
                                        <tr>
                                            <th>Serial No</th>
                                            <th>Services/Workflow</th>
                                            <th>Task Status</th>
                                            <th>Internal Deadline</th>
                                            <th>External Deadline</th>
                                        </tr><?php
                                        $i = 1;
                                        foreach($data['settings'] as $key=>$val){ ?>
                                            <tr>
                                                <td><?=$i?></td>
                                                <td><?=$val['service_name']?></td>
                                                <td>
                                                    <select id='ds_<?=$val['id']?>' name='ds' class='ds clr-check-select' data-id=<?=$val['id']?>><?php
                                                        foreach($data['status'] as $key1=>$val1){ 
                                                            $selected = ($val1['id'] == $val['task_status']) ? 'selected' : ''; ?>                                                    
                                                            <option value="<?=$val1['id']?>" <?=$selected?>><?=$val1['status_name']?></option><?php
                                                        } ?>                                                
                                                    </select>
                                                </td>
                                                <td><input type='number' id='idl_<?=$val['id']?>' name='idl' class='idl clr-check-client' <?php echo ($val['internal_deadline'] > 0) ? 'style="pointer-events: none;"' : ''; ?> data-id=<?=$val['id']?> value="<?=$val['internal_deadline']?>"></td>
                                                <td><input type='number' id='edl_<?=$val['id']?>' name='edl' class='edl clr-check-client' <?php echo ($val['external_deadline'] > 0) ? 'style="pointer-events: none;"' : ''; ?> data-id=<?=$val['id']?> value="<?=$val['external_deadline']?>"></td>                                            
                                            </tr><?php
                                            $i++;
                                        } ?>                                    
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  

<?php $this->load->view('includes/footer'); ?>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/custom/internal_deadlines.js?<?=time()?>" type="text/javascript"></script>

<script>
	$(document).ready(function(){
		$(".clr-check-client").keyup(function(){
			var clr = $(this).val();
			if(clr.length >= 3){
				$(this).addClass("clr-check-select-client-outline");
			}else{
				$(this).removeClass("clr-check-select-client-outline");
			}
		});
    $(".clr-check-client").each(function(i){
      if($(this).val() !== ""){
        $(this).addClass("clr-check-select-client-outline");
      }  
    })

		$(".clr-check-select").change(function(){
			var clr_select = $(this).val();
         if(clr_select !== ' '){
            $(this).addClass("clr-check-select-client-outline");
         }else{
				$(this).removeClass("clr-check-select-client-outline");
			}
		});
    $(".clr-check-select").each(function(i){
      if($(this).val() !== ""){
        $(this).addClass("clr-check-select-client-outline");
      }  
    })
	});

</script>