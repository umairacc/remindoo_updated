<?php $this->load->view('includes/login_header');

?>


<style type="text/css">
  .j-pro .j-footer button{
    border-radius: 4px;
    font-size: 14px;
    line-height: 16px;
    padding: 10px 23px;
    text-align: center;
    display: inline-block;
    text-decoration: none;
    margin-right: 0;
    transition: all .3s;
    height: auto;
    cursor: pointer;
    font-weight: 600;
  }
</style>
   <body style="background-color: #edf6ff;
    background-image: initial;">

   <div class="login_user">

      <div class="j-wrapper j-wrapper-400">

      <!-- <div class="login_logo text-center"><img class="img-fluid" src="<?php echo base_url();?>assets/images/logo.png" alt="Theme-Logo"></div> -->

      <div class="login_logo text-center"><svg class="menu__logo-img" xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48" style="
    display: inline-block;
    vertical-align: middle;
    margin-right: 16px;
    fill: #a485f2;
    fill-rule: evenodd;
">
            <path data-name="Sigma symbol" class="svg-element" d="M237.418,8583.56a12.688,12.688,0,0,0,.419-3.37c-0.036-5.24-2.691-9.68-7.024-13.2h-3.878a20.819,20.819,0,0,1,4.478,13.01c0,4.56-2.456,10.2-6.413,11.4a16.779,16.779,0,0,1-2.236.51c-10.005,1.55-14.109-17.54-9.489-23.31,2.569-3.21,6.206-4.08,11.525-4.08h17.935A24.22,24.22,0,0,1,237.418,8583.56Zm-12.145-24.45c-8.571.02-12.338,0.98-16.061,4.84-6.267,6.49-6.462,20.69,4.754,27.72a24.092,24.092,0,1,1,27.3-32.57h-16v0.01Z" transform="translate(-195 -8544)"></path>
          </svg><p class="menu__logo-title" style="
    position: relative;
    display: inline-block;
    vertical-align: middle;
    font-weight: 600;
    color: #234c87;
    display: inline-block;
    font-size: 24px;
    padding-left: 0;
    margin: 0;
    vertical-align: middle;
    margin-top: 0;">
  REMINDOO</p></div>


        <?php if($uri_seg=='login'){?>

         <form action="<?php echo base_url();?>login/login_chk" method="post" class="j-pro j-pro-new" id="login_form" novalidate>

            <!-- end /.header -->

            <center><?php 

            $error = $this->session->userdata('error');

            if($error){ echo $error; }

            $success = $this->session->userdata('success');

            if($success){ echo $success; }

            ?></center>

            <div class="j-content">

               <!-- start login -->

               <div class="j-unit">

                  <div class="j-input">

                  <input type="text" id="username" name="username" placeholder="username">
                     <label class="j-icon-right" for="login">

                     <i class="icofont icofont-ui-user"></i>

                     </label>

                     

                  </div>

               </div>

               <!-- end login -->

               <!-- start password -->

               <div class="j-unit">

                  <div class="j-input">

                  <input type="password" id="password" name="password" placeholder="password">

                     <label class="j-icon-right" for="password">

                     <i class="icofont icofont-lock"></i>

                     </label>

                    

                    

                  </div>

               <span class="j-hint">

                     <a href="<?php echo base_url();?>login/forgotPwd" class="j-link">Forgot password?</a>

                     </span>

               </div>

               <div class="j-response"></div>

               <!-- end response from server -->

            </div>

            <!-- end /.content -->

            <div class="j-footer">

               <button type="submit" class="btn btn-primary">Sign in</button>

            </div>

            <!-- end /.footer -->

         </form>

<?php } elseif($uri_seg=='forgotPwd'){?>

         <form action="<?php echo base_url();?>login/Forgot_pwd" method="post" class="j-pro" id="forgotpwd_form" novalidate>

            <!-- end /.header -->

            <center><?php 

            $success = $this->session->userdata('success');

            $warning = $this->session->userdata('warning');

            if($success){ echo $success; } elseif($warning){ echo $warning; }?></center>

            <div class="j-content">

               <!-- start login -->

               <div class="j-unit">

                  <div class="j-input">

                     <label class="j-icon-right" for="login">

                     <i class="icofont icofont-ui-user"></i>

                     </label>

                     <input type="text" id="email" name="email" placeholder="Email Address">

                  </div>

               </div>

               <!-- end login -->

              

               <div class="j-response"></div>

               <!-- end response from server -->

            </div>

            <!-- end /.content -->

            <div class="j-footer">

               <button type="submit" class="btn btn-primary">Send</button>

            </div>

            <!-- end /.footer -->

         </form>

         <?php }elseif($uri_seg=='resetPwd'){ ?>

             <form action="<?php echo base_url();?>login/changePwd/<?php echo $encrypt_id;?>" method="post" class="j-pro" id="resetPwd" novalidate>

            <!-- end /.header -->

           

            <div class="j-content">

               <!-- start login -->

               <div class="j-unit">

                  <div class="j-input">

                     <label class="j-icon-right" for="login">

                     <i class="icofont icofont-lock"></i>

                     </label>

                     <input type="password" id="newpwd" name="newpwd" placeholder="New Password">

                  </div>

               </div>

               <!-- end login -->

               <!-- start password -->

               <div class="j-unit">

                  <div class="j-input">

                     <label class="j-icon-right" for="password">

                     <i class="icofont icofont-lock"></i>

                     </label>

                     <input type="password" id="confirmpwd" name="confirmpwd" placeholder="Confirm Password">

                  </div>

               </div>

               <div class="j-response"></div>

               <!-- end response from server -->

            </div>

            <!-- end /.content -->

            <div class="j-footer">

               <button type="submit" class="btn btn-primary">Sign in</button>

            </div>

            <!-- end /.footer -->

         </form>

         <?php } ?>

      </div>

     </div>

     <?php $this->load->view('includes/login_footer');?>

<!-- <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script> -->

<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>

<script>

  $(document).ready(function(){ 

      $("#login_form").validate({

            // Rules for form validation

            rules : {

                "username" : {

                    required : true

                },                        

                "password" : {

                    required : true

                },

                  

            },

          

            messages : {

                "username" : {

                    required : 'Username Required'                           

                },    

                "password" : {

                    required : 'Password Required'                            

                },                        

                        

            },



        });



      $("#forgotpwd_form").validate({

      rules : {

                "email" : {

                    required : true,

                    email : true

                }, 

            },

            messages : {

                "email" : {

                    required : 'Email Id Required'                           

                },      

            },

      });

      $("#resetPwd").validate({

        rules : {

                "newpwd" : {

                    required : true,

                }, 

                "confirmpwd" : {

                    required : true,

                    equalTo: "#newpwd"

                }, 

            },

            messages : {

                "newpwd" : {

                    required : 'New Password Required'                           

                }, 

                "confirmpwd" : {

                    required : 'Confirm Password Required'                           

                },      

            },

      });

  });

  </script>

