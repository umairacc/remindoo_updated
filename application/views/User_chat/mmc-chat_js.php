<script type="text/javascript">
  'use strict';
function boxMinimizedCount() {

    var _count = $('#main-chat .chat-single-box.minimized .chat-dropdown li').length;

    $('#main-chat .chat-single-box.minimized .count span').html($('#main-chat .chat-single-box.minimized .chat-dropdown li').length);

    if (_count == 0) {
        $('#main-chat .chat-single-box.minimized').remove();
    }
}


function boxMinimizedUserAdd() {

    var _boxHidden = $('#main-chat .chat-single-box:not(".minimized"):not(".hidden")').eq(0);
    _boxHidden.addClass('hidden');
    var dataId = _boxHidden.data('id');

    var hasItem = false;
    $('#main-chat .chat-single-box.minimized .chat-dropdown li').each(function () {
        if ($(this).data('id') == dataId) {
            hasItem = true;
        }
    });

    if (!hasItem) {

        var dataUserName = _boxHidden.find('.user-info a').text();
        $('#main-chat .chat-single-box.minimized .chat-dropdown').append(box_minimized_dropdownLi.format(dataId, dataUserName));
      //   $('.our_message_{0}').parents(".chat-body").animate({ scrollTop: $('.our_message_'+chat_from).height() }, 0);
    }
}

var box_minimized_dropdownLi = '<li data-id="{0}"><div class="username">{1}</div> <div class="remove">X</div></li>'
function boxMinimized() {

    var _boxDefaultWidth = parseInt($('#main-chat .chat-single-box:not(".minimized")').css('width'));
    var _boxCommonWidth = parseInt($('.chat-box').css('width').replace('px', ''), 10)  + parseInt($('#sidebar').css('width').replace('px', ''), 10);
    var _windowWidth = $(window).width();
    var _hasMinimized = false;

    $('#main-chat .boxs .chat-single-box').each(function (index) {

        if ($(this).hasClass('minimized')) {

                _hasMinimized = true;

        }
    });

    if ((_windowWidth) > (_boxCommonWidth)) {

        if (!_hasMinimized) {
           if((_windowWidth)< 768 ){

                    $(".chat-box").css('margin-right','70px');
                    return;
           }
           else{
                 return;
           }

        }

        var dataId = $('#main-chat .boxs .minimized .chat-dropdown li').last().data('id');
        $('#main-chat .boxs .minimized .chat-dropdown li').last().remove();
        $('#main-chat .boxs .chat-single-box').each(function (index) {

            if ($(this).data('id') == dataId) {
                $(this).removeClass('hidden');
                return false;
            }
        });
    } else {
        if (!_hasMinimized) {

            $('#main-chat .boxs').prepend('<li class="chat-single-box minimized"><div class="count"><span>0</span></div><ul class="chat-dropdown"></ul></li>');
        }

        boxMinimizedUserAdd();

    }

    boxMinimizedCount();
}



$(window).on('resize',function () {

    boxMinimized();
    sidebarClosed();
});
$(function () {

    var waveEffect = $('.user-box').attr('wave-effect');
    var waveColor = $('.user-box').attr('wave-color');
    if (waveEffect == 'true') {

        $('#sidebar .user-box .userlist-box').each(function (index) {
            $(this).addClass('waves-effect ' + waveColor);
        });
    }

    initialTooltip();
    messageScroll();
    generatePlaceholder();

    boxMinimized();
});



$(document).on('click', '#main-chat .chat-single-box', function () {

    if ($(this).hasClass('new-message')) {

        $(this).removeClass('new-message');
    }
    ActiveChatBox(this);
});

$(document).on('click', '#main-chat .chat-header .user-info', function () {

    removeBoxCollapseClass($(this).parents('.chat-single-box'));

    messageScroll();
});

$(document).on('click', '#main-chat .chat-single-box .mini', function () {

    parent = $(this).parents('.chat-single-box');

    if ($(parent.children()[0].children[0]).hasClass('custom-collapsed')) {

        $(parent.children()[0].children[0]).removeClass('custom-collapsed');
        $(parent.children()[0].children[1]).css('display','block');
         $(parent.children()[0].children[2]).css('display','block');
       parent.addClass('bg-white');
       parent.addClass('card-shadow');
        messageScroll();
    } else {
       parent.removeClass('bg-white');
       parent.removeClass('card-shadow');
       $(parent.children()[0].children[0]).addClass('custom-collapsed');
        $(parent.children()[0].children[1]).css('display','none');
         $(parent.children()[0].children[2]).css('display','none');
    }

      // console.log(JSON.stringify(parent.children()[0].children[0])+"---"+JSON.stringify(parent.children()[0].children[1])+"--"+JSON.stringify(parent.children()[0].children[0]));
      //$(parent.children()[0].children[1]).animate({ scrollTop: $(parent.children()[0].children[1]).height() }, 0);
});

$(document).on('click', '#main-chat .chat-single-box .close', function () {

    parent = $(this).parents('.chat-single-box');
    if (parent.hasClass('active')) {

        parent.remove();
        setTimeout(function () { $('#main-chat .boxs .chat-single-box:last-child').addClass('active'); }, 1);
    }
    parent.remove();
    parent.find('.close_tooltip').tooltip('dispose');

    boxMinimized();
});

/*Click on username*/
$(document).on('click', '#main-chat #sidebar .user-box .userlist-box', function () {

    var dataId = $(this).attr('data-id');
    var dataStatus = $(this).data('status');
    var dataUserName = $(this).attr('data-username');

    var _return = false;

/** for group **/
var group=$(this).attr('data-group');
var members=$(this).attr('data-member');
var creater=$(this).attr('data-creater');
var session=$(this).attr('data-session');

if(typeof group === "undefined"){
    group='single';
}

/** end of group **/

    $('#main-chat .chat-box .boxs .chat-single-box').each(function (index) {

        if ($(this).attr('data-id') == dataId) {

            removeBoxCollapseClass(this);
            ActiveChatBox(this);
            _return = true;
        }
    });


    if (_return) {

        return;
    }
    if(group=='single'){ 
    if(dataStatus == "online"){

    var newBox = '<li class="chat-single-box card-shadow bg-white active chatbox_{0}" data-id="{0}"><div class="had-container"><div class="chat-header p-10 bg-gray"><div class="user-info d-inline-block f-left"><div class="box-live-status bg-success  d-inline-block m-r-10"></div><a href="javascript:void(0)" class="mini" >{1}</a></div><div class="box-tools d-inline-block"><a href="#" class="mini"><i class="icofont icofont-minus f-20 m-r-10"></i></a><a class="close" href="#"><i class="icofont icofont-close f-20"></i></a></div></div><div class="chat-body p-10"><input type="hidden" id="ajax_value_{0}" name="ajax_value"><div class="messages our_message_{0}"><div class="our_info_right" data-id="0" data-dbid="{0}"></div><div class="message-scrooler"></div></div></div><div class="chat-footer b-t-muted"><div class="input-group write-msg"><input type="text"  class="form-control input-value" name="text_message" data-id="{0}" id="text_message_{0}" placeholder="Type a Message"><span class="input-group-btn"><button  id="paper-btn" class="btn btn-primary message_send" data-id="{0}"  type="button"><i class="icofont icofont-paper-plane"></i></button></span></div></div></div></li>';
    }
    else{

        var newBox = '<li class="chat-single-box card-shadow bg-white active chatbox_{0}" data-id="{0}"><div class="had-container"><div class="chat-header p-10 bg-gray"><div class="user-info d-inline-block f-left"><div class="box-live-status bg-danger  d-inline-block m-r-10"></div><a href="javascript:void(0)" class="mini" >{1}</a></div><div class="box-tools d-inline-block"><a href="#" class="mini"><i class="icofont icofont-minus f-20 m-r-10"></i></a><a class="close" href="#"><i class="icofont icofont-close f-20"></i></a></div></div><div class="chat-body p-10"><input type="hidden" id="ajax_value_{0}" name="ajax_value"><div class="message-scrooler"><div class="messages our_message_{0}"><div class="our_info_right" data-id="0" data-dbid="{0}"></div></div></div></div><div class="chat-footer b-t-muted"><div class="input-group write-msg"><input type="text" class="form-control input-value" name="text_message" data-id="{0}" id="text_message_{0}" placeholder="Type a Message"><span class="input-group-btn"><button  id="paper-btn" class="btn btn-primary message_send" data-id="{0}" type="button"><i class="icofont icofont-paper-plane"></i></button></span></div></div></div></li>';
    }
      }
    else{ 
if(creater==session){
    var newBox = '<li class="chat-single-box card-shadow bg-white active chatbox_{0}" data-id="{0}"><div class="had-container"><div class="chat-header p-10 bg-gray"><div class="user-info d-inline-block f-left"><a href="javascript:void(0)"  class="mini members_chat_{0}" data-toggle="tooltip" title="'+members+'" >{1}</a></div><div class="box-tools d-inline-block"><a href="#" class="group-info" data-toggle="modal" data-target="#popup_group_edit_{0}" ><i class="icofont icofont-plus f-20 m-r-10"></i></a><a href="#" class="mini"><i class="icofont icofont-minus f-20 m-r-10"></i></a><a class="close" href="#"><i class="icofont icofont-close f-20"></i></a></div></div><div class="chat-body p-10"><input type="hidden" id="ajax_value_{0}" name="ajax_value"><div class="messages our_message_{0}"><div class="group_our_info_right_{0}" data-id="0" data-dbid="{0}"></div><div class="message-scrooler"></div></div></div><div class="chat-footer b-t-muted"><div class="input-group write-msg"><input type="text" data-group="group"  class="form-control input-value" name="text_message" data-id="{0}" id="text_message_{0}" placeholder="Type a Message"><span class="input-group-btn"><button  id="paper-btn" data-group="group" class="btn btn-primary message_send" data-id="{0}"  type="button"><i class="icofont icofont-paper-plane"></i></button></span></div></div></div></li>';
  }
  else
  {
        var newBox = '<li class="chat-single-box card-shadow bg-white active chatbox_{0}" data-id="{0}"><div class="had-container"><div class="chat-header p-10 bg-gray"><div class="user-info d-inline-block f-left"><a href="javascript:void(0)" class="mini members_chat_{0}" data-toggle="tooltip" title="'+members+'" >{1}</a></div><div class="box-tools d-inline-block"><a href="#" class="mini"><i class="icofont icofont-minus f-20 m-r-10"></i></a><a class="close" href="#"><i class="icofont icofont-close f-20"></i></a></div></div><div class="chat-body p-10"><input type="hidden" id="ajax_value_{0}" name="ajax_value"><div class="messages our_message_{0}"><div class="group_our_info_right_{0}" data-id="0" data-dbid="{0}"></div><div class="message-scrooler"></div></div></div><div class="chat-footer b-t-muted"><div class="input-group write-msg"><input type="text" data-group="group"  class="form-control input-value" name="text_message" data-id="{0}" id="text_message_{0}" placeholder="Type a Message"><span class="input-group-btn"><button  id="paper-btn" data-group="group" class="btn btn-primary message_send" data-id="{0}"  type="button"><i class="icofont icofont-paper-plane"></i></button></span></div></div></div></li>';
  }
 

    }  

    $('#main-chat .chat-single-box').removeClass('active');
    $('#main-chat .chat-box .boxs').append(newBox.format(dataId, dataUserName, dataStatus));
   //  $('.our_message_{0}').parents(".chat-body").animate({ scrollTop: $('.our_message_'+chat_from).height() }, 0);
if(group=='single'){
       var data = {};
       data['chat_id'] = dataId;
         data['condition']='no';
             $.ajax({
           url: '<?php echo base_url();?>User_chat/get_chat/',
           type : 'GET',
           data : data,
           success: function(data) {
           if(data!=''){
            $('.our_message_'+dataId).html(data);
            }
             $('.our_message_'+dataId).parents(".chat-body").animate({ scrollTop: $('.our_message_'+dataId).height() }, 0);

           },
       });
}
else
{
       var data = {};
       data['chat_id'] = dataId;
         data['condition']='no';
             $.ajax({
           url: '<?php echo base_url();?>User_chat/get_group_chat/',
           type : 'GET',
           data : data,
           success: function(data) {
           if(data!='null'){
            $('.our_message_'+dataId).html(data);
            }
             $('.our_message_'+dataId).parents(".chat-body").animate({ scrollTop: $('.our_message_'+dataId).height() }, 0);

           },
       });
}  



    generatePlaceholder();
    messageScroll();
    boxMinimized();
    initialTooltip();




});

$(document).on('focus', '#main-chat .textarea', function () {

    if ($(this).html() == '<span class="placeholder">{0}</span>'.format($(this).data('placeholder'))) {

       $(this).html('');
    }
});

$(document).on('blur', ' #main-chat .textarea', function () {

    if ($(this).html() == '') {

        $(this).html('<span class="placeholder">{0}</span>'.format($(this).data('placeholder')));
    }
});

$(document).on('click', '#main-chat .sidebar-collapse', function () {

    if ($('#main-chat').hasClass('sidebar-closed')) {

        $('#main-chat').removeClass('sidebar-closed');

        $('#main-chat .search input').attr('placeholder', '');
        $('#main-chat .search').css('display', 'block');


        deinitialTooltipSiderbarUserList();



    } else {

        $('#main-chat').addClass('sidebar-closed');

        $('#main-chat .search input').attr('placeholder', $('.search input').data('placeholder'));
        $('#main-chat .search').css('display', 'none');
        $('#main-chat .search').removeAttr('style');
        $('#main-chat .searchbar-closed').removeAttr('style');


        initialTooltipSiderbarUserList();
    }
});

$(document).on('click', '#main-chat .searchbar-closed', function () {

    $('#main-chat .sidebar-collapse').click();
    setTimeout(function () { $('#main-chat .searchbar input').focus(); }, 50);
    return false;
});

$(document).on('click', '#main-chat .chat-single-box .maximize', function () {

   /* window.open('inbox.html', 'window name', "width=300,height=400,scrollbars=yes");
    $(this).parents('.chat-single-box').remove();
    $('.maximize').tooltip('dispose');*/
     parent = $(this).parents('.chat-single-box');
      $(parent.children()[0].children[0]).removeClass('custom-collapsed');
        $(parent.children()[0].children[1]).css('display','block');
         $(parent.children()[0].children[2]).css('display','block');
       parent.addClass('bg-white');
       parent.addClass('card-shadow');
        messageScroll();
    return false;
});



$(document).on('click', '#main-chat .boxs .minimized .count', function (e) {

    e.stopPropagation();
    hideStickerBox();
    var _parent = $(this).parents('.minimized');

    if (_parent.hasClass('show')) {

        hideMinimizedBox();
    } else {

        _parent.addClass('show');
        var _bottom = parseInt(_parent.css('height').replace('px', ''),0) + 10;
        _parent.find('.chat-dropdown').css({
            'display': 'block',
            'bottom': _bottom
        });
    }
});

$(document).on('click', '#main-chat .boxs .minimized .chat-dropdown .username', function (e) {

    e.stopPropagation();
    var selectedDataId = $(this).parent().data('id');

    $(this).parent().remove();

    boxMinimizedUserAdd();

    $('#main-chat .boxs .chat-single-box').each(function (index) {

        if ($(this).data('id') == selectedDataId) {

            $(this).removeClass('hidden').removeClass('custom-collapsed');
            ActiveChatBox($(this));
        }
    });
});

$(document).on('click', '#main-chat .boxs .minimized .chat-dropdown .remove', function (e) {

    e.stopPropagation();
    var _parent = $(this).parents('.chat-dropdown li');
    dataId = _parent.data('id');

    $('#main-chat .chat-single-box').each(function () {

        if ($(this).data('id') == dataId) {
            $(this).remove();
        }
    });
    _parent.remove();

    boxMinimizedCount();
});
</script>
<script type="text/javascript">
$(window).on('load', function () {
    var currentXhr1 = null;var currentXhr2 = null;var currentXhr3 = null;var currentXhr4 = null;
    var currentXhr5 = null;var currentXhr6 = null;var currentXhr7 = null;var currentXhr8 = null;
    var currentXhr9 = null;var currentXhr10 = null;
    var current_arr1=[];var current_arr2=[];var current_arr3=[];var current_arr4=[];var current_arr5=[];
     
        window.setInterval(function(){
              
                      var listed_chatlist = [];
                      <?php
                  $common_id_val=array();
                   if(isset($getallUser) && !empty($getallUser)){
                   foreach ($getallUser as $getallUser_key => $getallUser_value) {
                    // echo $getallUser_value['id'];
                      array_push($common_id_val, $getallUser_value['id']);
                   }
                  }
                   if(isset($getalladmin) && !empty($getalladmin)){
                   foreach ($getalladmin as $getallUser_key => $getallUser_value) {
                      array_push($common_id_val, $getallUser_value['id']);
                   }
                  }
                   if(isset($getallstaff) && !empty($getallstaff)){
                   foreach ($getallstaff as $getallUser_key => $getallUser_value) {
                      array_push($common_id_val, $getallUser_value['id']);
                   }
                  }
                  foreach ($common_id_val as $getallUser_key => $getallUser_value) {
                     ?>
                    listed_chatlist.push('<?php echo $getallUser_value;?>');

                      <?php } ?>

                               var data = {};
                       data['id']=JSON.stringify(listed_chatlist);
                    $.ajax({
                         url: '<?php echo base_url();?>User_chat/get_all_result/',
                         type : 'POST',
                         dataType:'json',
                         data:data,
                         success: function(data) {
                         var chat_count=data.chat_count;
                         var group_count=data.group_count;
                         var online_status=data.online_status;
                         var div_first;
                         var div_firsts;
                         // var group_chat=data.group_chat;

                           $.each(group_count,function(i,e){
                              if(e.count_status > 0){
                            $('.created_group_list .new_message_count_'+e.group_id).html(e.count_status);

                            if(i==0){

                              div_first=$('.created_group_list').find('div:first').attr('class');
                            }
                          //  console.log(div_first);
                             $('.box_'+e.group_id).insertAfter('.created_group_list .'+div_first);

                             div_first='box_'+e.group_id;

                             // div_first=div_first.split(" ");

                             // $('.'+div_first[2]).insertAfter('.box_'+e.group_id);


                              $('.our_message_'+e.group_id).html('').html(e.message);
         
                       $('.our_message_'+e.group_id).parents(".chat-body").animate({ scrollTop: $('.our_message_'+e.group_id).height() }, 0);
                       
                            }
                           });



                           $.each(online_status,function(i,e){

                            $('.box_'+e.id).find('.media-left').children('.live-status').removeClass('bg-danger');
                            $('.box_'+e.id).find('.media-left').children('.live-status').removeClass('bg-success');
                            
                            $('.chatbox_'+e.id).find('.user-info').children('.box-live-status').removeClass('bg-danger');
                            $('.chatbox_'+e.id).find('.user-info').children('.box-live-status').removeClass('bg-success');

                            $('.box_'+e.id).find('.media-left').children('.live-status').addClass(e.status);
  
                            $('.chatbox_'+e.id).find('.user-info').children('.box-live-status').addClass(e.status);

                            });

                           
                            $.each(chat_count,function(i,e){
                              if(e.count_status > 0){
                              $('.user-groups .new_message_count_'+e.chat_from).html(e.count_status);

                              var user_type=( e.user_type=='FA' || e.user_type=='FU' ) ? '.staff_section':'.client_section' ;

                           //   console.log(i);
                              if(i==0){
                              div_firsts=$(user_type).find('div:first').attr('class');

                              // div_firsts=div_firsts.split(" ");
                              // div_firsts=div_firsts[2];
                           //   console.log('c');
                              }

                            //  $('.'+div_first[2]).insertAfter('.box_'+e.chat_from);
                         //   console.log(div_firsts+'--A');
                            $('.box_'+e.chat_from).insertAfter(user_type+' .'+div_firsts);
                          //  console.log('box_'+e.chat_from+'--B');
                            div_firsts='box_'+e.chat_from;


                              $('.our_message_'+e.chat_from).html('').html(e.message);

                              $('.our_message_'+e.chat_from).parents(".chat-body").animate({ scrollTop: $('.our_message_'+e.chat_from).height() }, 0);


                          }

                        });
                           
                     }
                   });
                           
},5000);
});
   $(document).on('click','.input-value',function(){
       // alert($(this).attr('data-id'));
         var data = {};
         var it_id=$(this).attr('data-id');
         var data_group=$(this).attr('data-group');
         if(typeof data_group=="undefined")
         {
            data_group='single';
         }
         if(data_group=='single'){
               data['chat_id'] = $(this).attr('data-id');
             
                     $.ajax({
                   url: '<?php echo base_url();?>User_chat/chat_status_change/',
                   type : 'GET',
                   data : data,
                    beforeSend : function(){          
                   
                          },
                   success: function(data) {
                    $('.new_message_count_'+it_id).html('');
                   
                   },
                 });
              }
         if(data_group=='group'){
             data['chat_id'] = $(this).attr('data-id');
           
                  $.ajax({
                 url: '<?php echo base_url();?>User_chat/chat_status_change_forgroup/',
                 type : 'GET',
                 data : data,
                beforeSend : function(){          
                            
                          },
                 success: function(data) {
                  $('.new_message_count_'+it_id).html('');
                 
                 },
               });
            }
      });
      

   $(document).on('keypress','.input-value',function(e){
     if ( e.keyCode == 13 ) {  // detect the enter key
      // alert('zzz');
       //alert($(this).val());

        if($(this).val()!='')
          {
                
             //    $(this).closest('.input-group-btn').find("#paper-btn").trigger('click');
         $(this).parents('.write-msg').children('.input-group-btn').find('#paper-btn').trigger('click');
           // console.log($(this).parents('.write-msg').children('.input-group-btn').find('#paper-btn').trigger('click'));
           
          }
        }
   });

</script>