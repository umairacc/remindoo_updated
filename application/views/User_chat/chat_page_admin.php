
<!-- management block -->
<div class="pcoded-content">
   <div class="pcoded-inner-content chat-page">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
           <!--  <div class="deadline-crm1 floating_set single-txt12">
               <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                  <li class="nav-item">
                     Deadline Manager
                     <div class="slide"></div>
                  </li>
               </ul>              
            </div> -->
            <div class="deadline-block">
                                      <div id="main-chat" class="container-fluid">
                    
                      <div class="page-body">
                        <div class="row">
                          <div class="chat-box">
                            <ul class="text-right boxs">
                
                            </ul>
                            <div id="styleSelector12">
                            <div class="selector-toggle">
                            <a href="javascript:void(0)" class="idea1"><i class="ti-comments"></i></a>
                            </div>
                            <div id="sidebar" class="users p-chat-user active14 maxmin">
                             <!-- <div class="chat-header p-10 bg-gray">
                               <div class="user-info d-inline-block">
                                  <div class="box-live-status d-inline-block m-r-10 bg-danger"></div>
                                  <a href="javascript:void(0)" class="mini">Chat</a>
                               </div>
                               <div class="box-tools d-inline-block"><a href="#" class="mini"><i class="icofont icofont-minus f-20 m-r-10"></i></a><a class="close" href="#"><i class="icofont icofont-close f-20"></i></a></div>
                            </div> -->
                              <div class="had-container">
                                <div class="card card_main p-fixed users-main ">
                                  <div class="user-box">
                                    <div class="card-block">
                                      <div class="right-icon-control">
                                        <input type="text" class="form-control  search-text" placeholder="Search Friend">
                                        <div class="form-icon">
                                          <i class="icofont icofont-search"></i>
                                        </div>
                                      </div>
                                    </div>
          <!-- for group chat -->
   <div class="user-groups for_group_chat">
                                              <h6>Groups</h6>
                                          <a href="javascript:void(0)" class="create_group"> Create Group <i class="fa fa-plus" aria-hidden="true"></i></a>
                                          <div class="chat_user_list" style="display:none;">
                                            <div class="group-search">
                                             <input type="text" name="group_name" id="group_name" placeholder="Group Name">
                                             <button class="btn btn-danger add_create_group" type="button">Create</button>
                                            </div>
                                             <?php if($_SESSION['roleId']!=6 && !empty($getallstaff)){ ?>
                                             <div class="user_list">
                                                <h5>Staff</h5>
                                                <?php  foreach ($getallstaff as $getallUser_key => $getallUser_value) { ?>
                                                <div class="media" title="<?php echo $getallUser_value['crm_name']; ?>">
                                                   <a class="media-left" href="#!">
                                                   <img class="media-object" src="<?php echo base_url();?>uploads/<?php echo $getallUser_value['crm_profile_pic'];?>" onerror="this.src='<?php echo base_url();?>assets/images/avatar-3.jpg';" alt="Generic placeholder image">
                                                   </a>
                                                   <div class="media-body">
                                                      <div class="f-13 chat-header"><?php echo $getallUser_value['crm_name']; ?></div>
                                                      <div class="checkforgroup checkbox-color checkbox-primary">
                                                      <input type="checkbox" class="for_group" name="our_group_data" id="our_group_data_<?php echo $getallUser_value['id'];?>" value="<?php echo $getallUser_value['id'];?>">
                                                      <label for="our_group_data_<?php echo $getallUser_value['id'];?>"></label>
                                                      </div>
                                                   </div>
                                                </div>
                                                <?php } ?>
                                             </div>
                                             <?php
                                                } ?>
                                             <?php if(!empty($getallUser)){ ?>
                                             <div class="user_list">
                                                <h5>Client</h5>
                                                <?php  foreach ($getallUser as $getallUser_key => $getallUser_value) { ?>
                                                <div class="media" title="<?php echo $getallUser_value['crm_name']; ?>">
                                                   <a class="media-left" href="#!">
                                                   <img class="media-object" src="<?php echo base_url();?>uploads/<?php echo $getallUser_value['crm_profile_pic'];?>" onerror="this.src='<?php echo base_url();?>assets/images/avatar-3.jpg';" alt="Generic placeholder image">
                                                   </a>
                                                   <div class="media-body">
                                                      <div class="f-13 chat-header"><?php echo $getallUser_value['crm_name']; ?></div>
                                                      <div class="checkforgroup checkbox-color checkbox-primary">
                                                      <input type="checkbox" class="for_group" name="our_group_data" id="our_group_data_<?php echo $getallUser_value['id'];?>" value="<?php echo $getallUser_value['id'];?>">
                                                      <label for="our_group_data_<?php echo $getallUser_value['id'];?>"></label>
                                                      </div>
                                                   </div>
                                                </div>
                                                <?php } ?>
                                             </div>
                                             <?php
                                                } ?>
                                          </div>
                                          <!-- for group -->
                                          <!-- for creaated users -->
                                          <div class="created_group_list">
                                             <?php $group_data=$this->db->query("SELECT * FROM chat_group where create_by=".$_SESSION['id']."  ")->result_array(); ?>
                                             <?php foreach ($group_data as $key => $value) {
                                                $group_member_data=$this->db->query("SELECT * FROM user where id in (".$value['members'].",".$value['create_by'].") ")->result_array();
                                                $member_names=array();
                                                foreach ($group_member_data as $men_key => $mem_value)
                                                 {
                                                    array_push($member_names, $mem_value['crm_name']);
                                                 }                                    
                                                ?>
                                             <div <?php if($value['status']!=''){ ?> style="display: none;" <?php } ?> class="media group_chat_list userlist-box box_<?php echo $value['id'];?>" data-id="<?php echo $value['id'];?>" data-username="<?php echo $value['groupname']; ?>" data-toggle="tooltip" data-placement="left" title="<?php echo $value['groupname']; ?>" data-group="group" data-member="<?php echo implode(',',$member_names);?>" data-creater="<?php echo $value['create_by']; ?>" data-session="<?php echo $_SESSION['id'];?>" >
                                                <div class="media-body">
                                                   <div class="f-13 chat-header"><?php echo $value['groupname']; ?></div>
                                                </div>
                                                <div class="badge badge-primary new_message_count_<?php echo $value['id'];?>"></div>
                                             </div>
                                             <?php
                                                } ?>
                                          </div>
                                          <!-- for member of the group -->
                                          <div class="created_group_list">
                                             <?php $group_data=$this->db->query("SELECT * FROM chat_group where find_in_set('".$_SESSION['id']."',members)   ")->result_array(); ?>
                                             <?php foreach ($group_data as $key => $value) {
                                                $group_member_data=$this->db->query("SELECT * FROM user where id in (".$value['members'].",".$value['create_by'].") ")->result_array();
                                                $member_names=array();
                                                foreach ($group_member_data as $men_key => $mem_value)
                                                 {
                                                    array_push($member_names, $mem_value['crm_name']);
                                                 }                                    
                                                ?>
                                             <div <?php if($value['status']!=''){ ?> style="display: none;" <?php } ?> class="media group_chat_list userlist-box box_<?php echo $value['id'];?>" data-id="<?php echo $value['id'];?>" data-username="<?php echo $value['groupname']; ?>" data-toggle="tooltip" data-placement="left" title="<?php echo $value['groupname']; ?>" data-group="group" data-member="<?php echo implode(',',$member_names);?>" data-creater="<?php echo $value['create_by']; ?>" data-session="<?php echo $_SESSION['id'];?>" >
                                                <div class="media-body">
                                                   <div class="f-13 chat-header"><?php echo $value['groupname']; ?></div>
                                                </div>
                                                <div class="badge badge-primary new_message_count_<?php echo $value['id'];?>"></div>
                                             </div>
                                             <?php
                                                } ?>
                                          </div>
<!-- end of chat group -->
                                      <!-- <ul>
                                        <li class="frnds">Friends</li>
                                        <li class="work">Work</li>
                                      </ul> -->
                                    </div>
                                    <!-- for admin and support -->
          <!-- end of group chat -->
                                    <?php 
                                    $common_id_val=array();
                                    ?>
                             <!--  <div class="user-groups">
                                      <h6>Groups</h6>
                                    
                                    </div> -->
                                    <!-- for admin and support -->
                                    <?php if($_SESSION['roleId']==4 || $_SESSION['roleId']==6){ ?>
                                      <div class="user-groups">
                                      <h6>For Support</h6>
                                    <!-- for staff -->
                                     <?php
                                    $i=1;
                                   
                                     foreach ($getalladmin as $getallUser_key => $getallUser_value) {
                                      array_push($common_id_val, $getallUser_value['id']);
                                     
                                      $user_login=$this->db->query("SELECT * FROM activity_log where user_id=".$getallUser_value['id']." AND module='Login'  order by id DESC")->result_array();
                                  if(!empty($user_login))
                                  {
                                    $login_time=$user_login[0]['CreatedTime'];
                                       $user_logout=$this->db->query("SELECT * FROM activity_log where user_id=".$getallUser_value['id']." AND module='Logout'  order by id DESC")->result_array();
                                  if(!empty($user_logout))
                                  {
                                       $logout_time=$user_logout[0]['CreatedTime'];
                                  }
                                  else
                                  {
                                      $logout_time=0;
                                  }
                                       $status="";
                                       if($login_time>$logout_time){
                                         if(date('Y-m-d', $login_time)==date('Y-m-d'))
                                         {
                                          $status="online";
                                           $status_color="bg-success";
                                         }
                                         else
                                         {
                                          $status="offline";
                                           $status_color="bg-danger";
                                         }
                                       }
                                       else
                                       {
                                        $status="offline";
                                         $status_color="bg-danger";
                                       }
                                  }
                                  else
                                  {
                                    $status="offline";
                                    $status_color="bg-danger";
                                  }
                                  //echo $status;
                                      ?>
                                    
                                    <div class="media userlist-box box_<?php echo $getallUser_value['id'];?>" data-id="<?php echo $getallUser_value['id'];?>" data-status="<?php echo $status;?>" data-username="<?php echo $getallUser_value['crm_name']; ?>" data-toggle="tooltip" data-placement="left" title="<?php echo $getallUser_value['crm_name']; ?>">
                                      <a class="media-left" href="#!">
                                        <img class="media-object" src="<?php echo base_url();?>uploads/<?php echo $getallUser_value['crm_profile_pic'];?>" onerror="this.src='<?php echo base_url();?>assets/images/avatar-3.jpg';" alt="Generic placeholder image">
                                        <div class="live-status <?php echo $status_color;?>"></div>
                                      </a>
                                      <div class="media-body">
                                        <div class="f-13 chat-header"><?php echo $getallUser_value['crm_name']; ?></div>
                                      </div>
                                      <div class="badge badge-primary new_message_count_<?php echo $getallUser_value['id'];?>"></div>
                                    </div>
                                  <?php 
                                  $i++;
                                  } ?>
                                    <!-- end of staff -->
                                    </div>
                                    <!-- for client -->
                                    <?php } ?>
                                    <!-- end of admin support -->
                                    <?php if($_SESSION['roleId']!=6 && !empty($getallstaff)){ ?>
                                    <div class="user-groups">
                                      <h6>Staff</h6>
                                    <!-- for staff -->
                                     <?php
                                    $i=1;
                                   
                                     foreach ($getallstaff as $getallUser_key => $getallUser_value) {
                                       array_push($common_id_val, $getallUser_value['id']);
                                      $user_login=$this->db->query("SELECT * FROM activity_log where user_id=".$getallUser_value['id']." AND module='Login'  order by id DESC")->result_array();
                                  if(!empty($user_login))
                                  {
                                    $login_time=$user_login[0]['CreatedTime'];
                                       $user_logout=$this->db->query("SELECT * FROM activity_log where user_id=".$getallUser_value['id']." AND module='Logout'  order by id DESC")->result_array();
                                  if(!empty($user_logout))
                                  {
                                       $logout_time=$user_logout[0]['CreatedTime'];
                                  }
                                  else
                                  {
                                      $logout_time=0;
                                  }
                                       $status="";
                                       if($login_time>$logout_time){
                                         if(date('Y-m-d', $login_time)==date('Y-m-d'))
                                         {
                                          $status="online";
                                           $status_color="bg-success";
                                         }
                                         else
                                         {
                                          $status="offline";
                                           $status_color="bg-danger";
                                         }
                                       }
                                       else
                                       {
                                        $status="offline";
                                         $status_color="bg-danger";
                                       }
                                  }
                                  else
                                  {
                                    $status="offline";
                                    $status_color="bg-danger";
                                  }
                                  //echo $status;
                                      ?>
                                    
                                    <div class="media userlist-box box_<?php echo $getallUser_value['id'];?>" data-id="<?php echo $getallUser_value['id'];?>" data-status="<?php echo $status;?>" data-username="<?php echo $getallUser_value['crm_name']; ?>" data-toggle="tooltip" data-placement="left" title="<?php echo $getallUser_value['crm_name']; ?>">
                                      <a class="media-left" href="#!">
                                        <img class="media-object" src="<?php echo base_url();?>uploads/<?php echo $getallUser_value['crm_profile_pic'];?>" onerror="this.src='<?php echo base_url();?>assets/images/avatar-3.jpg';" alt="Generic placeholder image">
                                        <div class="live-status <?php echo $status_color;?>"></div>
                                      </a>
                                      <div class="media-body">
                                        <div class="f-13 chat-header"><?php echo $getallUser_value['crm_name']; ?></div>
                                      </div>
                                      <div class="badge badge-primary new_message_count_<?php echo $getallUser_value['id'];?>"></div>
                                    </div>
                                  <?php 
                                  $i++;
                                  } ?>
                                    <!-- end of staff -->
                                    </div>
                                    <?php } ?>
                                    <?php if(!empty($getallUser)){ ?>
                                    <!-- for client -->
                                      <div class="user-groups">
                                      <h6>Client</h6>
                                    <?php
                                    $i=1;
                                   
                                     foreach ($getallUser as $getallUser_key => $getallUser_value) {
                                      array_push($common_id_val, $getallUser_value['id']);
                                      $user_login=$this->db->query("SELECT * FROM activity_log where user_id=".$getallUser_value['id']." AND module='Login'  order by id DESC")->result_array();
                                  if(!empty($user_login))
                                  {
                                    $login_time=$user_login[0]['CreatedTime'];
                                       $user_logout=$this->db->query("SELECT * FROM activity_log where user_id=".$getallUser_value['id']." AND module='Logout'  order by id DESC")->result_array();
                                       if(!empty($user_logout))
                                       {
                                               $logout_time=$user_logout[0]['CreatedTime'];
                                       }
                                       else
                                       {
                                               $logout_time=0;
                                       }
                                
                                       $status="";
                                       if($login_time>$logout_time){
                                         if(date('Y-m-d', $login_time)==date('Y-m-d'))
                                         {
                                          $status="online";
                                           $status_color="bg-success";
                                         }
                                         else
                                         {
                                          $status="offline";
                                           $status_color="bg-danger";
                                         }
                                       }
                                       else
                                       {
                                        $status="offline";
                                         $status_color="bg-danger";
                                       }
                                  }
                                  else
                                  {
                                    $status="offline";
                                    $status_color="bg-danger";
                                  }
                                  //echo $status;
                                      ?>
                                    
                                    <div class="media userlist-box box_<?php echo $getallUser_value['id'];?>" data-id="<?php echo $getallUser_value['id'];?>" data-status="<?php echo $status;?>" data-username="<?php echo $getallUser_value['crm_name']; ?>" data-toggle="tooltip" data-placement="left" title="<?php echo $getallUser_value['crm_name']; ?>">
                                      <a class="media-left" href="#!">
                                        <img class="media-object" src="<?php echo base_url();?>uploads/<?php echo $getallUser_value['crm_profile_pic'];?>" onerror="this.src='<?php echo base_url();?>assets/images/avatar-3.jpg';" alt="Generic placeholder image">
                                        <div class="live-status <?php echo $status_color;?>"></div>
                                      </a>
                                      <div class="media-body">
                                        <div class="f-13 chat-header"><?php echo $getallUser_value['crm_name']; ?></div>
                                      </div>
                                      <div class="badge badge-primary new_message_count_<?php echo $getallUser_value['id'];?>"></div>
                                    </div>
                                  <?php 
                                  $i++;
                                  } ?>
                                  </div>
                                  <!-- for client -->
                                  <?php } ?>
                                </div>
                              </div>
                            </div>
                          </div>
                          </div> <!-- new div close -->
                        </div>
                      </div>
                      <div class="page-error">
                        <div class="card text-center">
                          <div class="card-block">
                            <div class="m-t-10">
                              <i class="icofont icofont-warning text-white bg-c-yellow"></i>
                              <h4 class="f-w-600 m-t-25">Not supported</h4>
                              <p class="text-muted m-b-0">Chat not supported in this device</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
      </div>
   </div>
</div>
</div>
<!-- management block -->
<div class="edit_user_group_popup">
  <!-- Modal -->
  <?php 
$group_data=$this->db->query("SELECT * FROM chat_group where create_by=".$_SESSION['id']." ")->result_array(); ?>
<?php foreach ($group_data as $key => $value) {
  $ex_members=explode(',',$value['members']);

                                        $group_member_data=$this->db->query("SELECT * FROM user where id in (".$value['members'].",".$value['create_by'].") ")->result_array();
                                        $member_names=array();
                                        foreach ($group_member_data as $men_key => $mem_value)
                                         {
                                            array_push($member_names, $mem_value['crm_name']);
                                         }                                    
                                       ?>
  <div class="modal fade recurring-msg common-schedule-msg1 chat-popup01 chatpopupnewcls" id="popup_group_edit_<?php echo $value['id']; ?>" role="dialog">
     <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Change Group Option</h4>
           </div>
           <div class="modal-body">
                 <div class="row">
                                    
                    <div class="col-sm-12 form-group common-repeats1 ">
                    <label class="label-column1 col-form-label texlef">Group Name:</label>
                    <div class="label-column2 input-group date">
                   
                    <input type='text' class="" id='group_name_<?php echo $value['id'];?>' name="group_name" value="<?php echo $value['groupname'];?>" />
                    </div>
                    
                    <!-- members list -->
                       <div class="chat_user_list sunchatlist">
                                                                       
                                      <?php if($_SESSION['roleId']!=6 && !empty($getallstaff)){ ?>
                                        <div class="user_list">
                                        <h5>Staff</h5>
                                          <?php  foreach ($getallstaff as $getallUser_key => $getallUser_value) { ?>
                                            <div class="media" title="<?php echo $getallUser_value['crm_name']; ?>">
                                              <a class="media-left" href="#!">
                                                <img class="media-object" src="<?php echo base_url();?>uploads/<?php echo $getallUser_value['crm_profile_pic'];?>" onerror="this.src='<?php echo base_url();?>assets/images/avatar-3.jpg';" alt="Generic placeholder image">
                                                
                                              </a>

                                              <div class="media-body">
                                                <div class="f-13 chat-header"><?php echo $getallUser_value['crm_name']; ?></div>
                                                 <!-- <input type="checkbox" class="for_group" name="our_group_data_<?php echo $value['id'];?>" id="our_group_data_<?php echo $getallUser_value['id'];?>" value="<?php echo $getallUser_value['id'];?>" <?php if(in_array($getallUser_value['id'],$ex_members)){ echo "checked"; } ?>  > -->


<label class="custom_checkbox1">
 <input type="checkbox" class="for_group user_checkbox" name="our_group_data_<?php echo $value['id'];?>" id="our_group_data_<?php echo $getallUser_value['id'];?>" value="<?php echo $getallUser_value['id'];?>" <?php if(in_array($getallUser_value['id'],$ex_members)){ echo "checked"; } ?>  >
  </label>

                                              </div>

                                           
                                            </div>
                                          <?php } ?>
                                        </div>

                                        <?php
                                        } ?>
                                            <?php if(!empty($getallUser)){ ?>
                                        <div class="user_list">
                                        <h5>Client</h5>
                                          <?php  foreach ($getallUser as $getallUser_key => $getallUser_value) { ?>
                                            <div class="media" title="<?php echo $getallUser_value['crm_name']; ?>">
                                              <a class="media-left" href="#!">
                                                <img class="media-object" src="<?php echo base_url();?>uploads/<?php echo $getallUser_value['crm_profile_pic'];?>" onerror="this.src='<?php echo base_url();?>assets/images/avatar-3.jpg';" alt="Generic placeholder image">
                                                
                                              </a>
                                            
                                              <div class="media-body">
                                                <div class="f-13 chat-header"><?php echo $getallUser_value['crm_name']; ?></div>
                                                <label class="custom_checkbox1">
                                                  <input type="checkbox" class="for_group user_checkbox" name="our_group_data_<?php echo $value['id'];?>" id="our_group_data_<?php echo $getallUser_value['id'];?>" value="<?php echo $getallUser_value['id'];?>" <?php if(in_array($getallUser_value['id'],$ex_members)){ echo "checked"; } ?> >
                                                </label>
                                              </div>
                                           
                                            </div>
                                          <?php }  ?>
                                        </div>

                                        <?php
                                        } ?>

                                        
                                      </div>
                    <!-- end -->
                    </div>
                   
    
                  </div>
             </div>
             <div class="modal-footer">
                <button class="btn btn-info edit_create_group" data-id="<?php echo $value['id'];?>" type="button">Update</button>
                <button type="button" class="btn btn-default delete_group" data-id="<?php echo $value['id'];?>" >Delete Group</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
             </div>
          </div>
       </div>
    </div>
    <?php } ?>
    <!-- modal close-->
</div>
<!-- <script src="<?php echo base_url();?>assets/js/mmc-common.js"></script> -->
  <!--   <script src="<?php echo base_url();?>assets/js/mmc-chat.js"></script> -->
    <?php $this->load->view('User_chat/mmc-common_js');?>
  <?php $this->load->view('User_chat/mmc-chat_js');?>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/chat.js"></script>
<script type="text/javascript">
   $(document).ready(function(){

    $('.selector-toggle > a.idea1').click(function(){


          $('#styleSelector12').toggleClass('open');

        });


       $('.edit-toggle1').click(function(){
         $('.proposal-down').slideToggle(300);
       });
   
        $("#legal_form12").change(function() {
          var val = $(this).val();
          if(val === "from2") {
            // alert('hi');
              $(".from2option").show();
          }
          else {
              $(".from2option").hide();
          }
          });

        $('.active14 .mini').click(function(){

          $(this).toggleClass('addnew');

          $('.active14').toggleClass('maxmin');

        });

        $('.active14 .close').click(function(){

          $('.active14').hide();

        });

        
   
  
   });
</script>
<script type="text/javascript">
  //$('.create_group').on('click',function(){
      $(document).on('click','.create_group',function(){
      //$('.chat_user_list').show();
       $('.chat_user_list').toggle();
  });
  //$('.add_create_group').on('click',function(){
     $(document).on('click','.add_create_group',function(){
    var group_name=$('#group_name').val();
    if(group_name=='')
    {
      alert('Enter Group Name');
    }
    else
    {
        var checkboxValues = [];
    $('input[name="our_group_data"]:checked').each(function(index, elem) {
        checkboxValues.push($(elem).val());
    });
      if(checkboxValues.length>0){
        //alert(checkboxValues.join(', '));
         var data = {};
         data['name']=group_name;
   data['member'] =checkboxValues.join(',');
 $('.LoadingImage').show();
         $.ajax({
       url: '<?php echo base_url();?>User_chat/create_group/',
       type : 'POST',
       data : data,
       success: function(data) {
        //alert(data);
        $('.LoadingImage').hide();
        console.log(data);
        if(data!='wrong'){
          //$('.created_group_list').append(data);
          $('.for_group_chat').html('');
          $('.for_group_chat').load("<?php echo base_url(); ?>User_chat/group_chat_lists");
          $('.edit_user_group_popup').html('');
          $('.edit_user_group_popup').load("<?php echo base_url(); ?>User_chat/edit_chatgroup_popup");
          $('.chat_user_list').hide();
        }

       }
     });
      }
      else
      {
        alert('Choose Any checkbox ');
      }
    
    }

  });

$('.group-info').on('click',function(){
  // alert('zzzz');
  //   $('.rsptrspt').html('zzzz');
});

  //$('.edit_create_group').on('click',function(){
 $(document).on('click','.edit_create_group',function(){
    var id=$(this).attr('data-id');
       var group_name=$('#group_name_'+id).val();
    if(group_name=='')
    {
      alert('Enter Group Name');
    }
    else
    {
        var checkboxValues = [];
    $('input[name="our_group_data_'+id+'"]:checked').each(function(index, elem) {
        checkboxValues.push($(elem).val());
    });
      if(checkboxValues.length>0){
        //alert(checkboxValues.join(', '));
         var data = {};
         data['name']=group_name;
   data['member'] =checkboxValues.join(',');
 $('.LoadingImage').show();
         $.ajax({
       url: '<?php echo base_url();?>User_chat/update_group/'+id,
       type : 'POST',
       data : data,
       success: function(data) {
        //alert(data);
        $('.LoadingImage').hide();
        //alert(data);
      //  location.reload();
      $('#popup_group_edit_'+id).modal('hide');
      $('.modal-backdrop.show').hide();
        $('.for_group_chat').html('');
        $('.members_chat_'+id).attr('data-original-title',data);
          $('.for_group_chat').load("<?php echo base_url(); ?>User_chat/group_chat_lists");
          $('.edit_user_group_popup').html('');
          $('.edit_user_group_popup').load("<?php echo base_url(); ?>User_chat/edit_chatgroup_popup");
        // console.log(data);
        // if(data!='wrong'){
        //   $('.created_group_list').append(data);
        //   $('.chat_user_list').hide();
        // }

       }
     });
      }
      else
      {
        alert('Choose Any checkbox ');
      }
    
    }

  });

  $(document).on('click','.delete_group',function(){
    var group_id=$(this).attr('data-id');
          var data = {};
         data['group_id']=group_id;
 $('.LoadingImage').show();
         $.ajax({
       url: '<?php echo base_url();?>User_chat/delete_group_chat/'+group_id,
       type : 'POST',
       data : data,
       success: function(data) {
       
        $('.LoadingImage').hide();     
        $('.box_'+group_id).hide();
        $('#popup_group_edit_'+group_id).modal('hide');
        $('.chatbox_'+group_id).hide();
        $('.modal-backdrop.show').hide();
       }
     });

  });

</script>

