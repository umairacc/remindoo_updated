<?php
if ($dash_id == 0) {
	if (isset($_COOKIE['remindoo_logout']) && $_COOKIE['remindoo_logout'] == 'remindoo_superadmin') {
		$this->load->view('super_admin/superAdmin_header');
	} else if ($_SESSION['user_type'] == 'FC') {
		$this->load->view('includes/client_header');
	} else {
		$this->load->view('includes/header');
	}
}
$succ = $this->session->flashdata('success');
$error = $this->session->flashdata('error');

?>


<link href="<?php echo base_url() ?>assets/menudropdown/fm.selectator.jquery.css" rel="stylesheet" type="text/css">
<style>
	#wrapper {
		padding: 0;
		max-width: 1024px;
		margin: 0 auto;
		background-color: #fff;
	}

	.users_list {
		width: 250px;
		height: 36px;
	}
</style>
<!-- management block -->
<div class="pcoded-content">
	<div class="pcoded-inner-content chat-page">
		<!-- Main-body start -->
		<div class="main-body">
			<div class="page-wrapper">
				<div class="deadline-block">
					<div id="main-chat" class="container-fluid">

						<div class="page-body">
							<div class="row">
								<div class="chat-box">
									<ul class="text-right boxs">

									</ul>
									<div id="sidebar" class="users p-chat-user chat_particular">
										<?php if ($dash_id == 1) { ?>

											<button type="button" class="close chat_close" aria-label="Close">
												<span aria-hidden="true">×</span>
											</button>

										<?php } ?>

										<div class="had-container">
											<div class="card card_main p-fixed users-main ">
												<div class="user-box">
													<div class="card-block">
														<div class="right-icon-control">
															<input type="text" class="form-control  search-text" placeholder="Search Friend">
															<div class="form-icon">
																<i class="icofont icofont-search"></i>
															</div>
														</div>
													</div>

													<div class="user-groups group_section">
														<h6>Groups</h6>
														<?php

														// print_r($_SESSION['user_type']);echo "<br>";
														// print_r($_SESSION['permission']['Chat']);

														if ($_SESSION['permission']['Chat']['create'] == 1 && $_SESSION['user_type'] != 'FC') { ?>
															<a href="javascript:void(0)" class="create_group"> Create Group <i class="fa fa-plus" aria-hidden="true"></i></a>
														<?php  } ?>




														<div class="chat_user_list" style="display:none;">
															<div class="group-search">
																<input type="text" name="group_name" id="group_name" placeholder="Group Name">
																<button class="btn btn-danger add_create_group cratebuttoncls" type="button">Create</button>
																<div class="groupname_error_div" style="color:red"></div>

															</div>

															<a href="javascript:void(0)" class="group_member">Members</a>

															<div id="wrapper">
																<div id="wrapper_inner">
																	<section>
																		<select id="create_users_list" name="create_users_list" class="users_list" multiple>

																			<!--  <option value="">Select</option> -->

																			<?php if (!empty($getallstaff)) { ?>
																				<optgroup label="Staff(s)">

																					<?php foreach ($getallstaff as $getallUser_key => $getallUser_value) {
																						if ($getallUser_value['crm_profile_pic'] != '') {
																							$get_val = base_url() . 'uploads/' . $getallUser_value['crm_profile_pic'];
																						} else {
																							$get_val = base_url() . 'assets/images/avatar-3.jpg';
																						}
																						if ($getallUser_value['id'] != '1' && $getallUser_value['id'] != $_SESSION['id']) {
																					?>

																							<option value="<?php echo $getallUser_value['id']; ?>" data-left="<?php echo $get_val ?>"><?php echo $getallUser_value['crm_name']; ?></option>

																					<?php }
																					} ?>
																				</optgroup>
																			<?php
																			} ?>


																			<?php if (!empty($getallUser)) { ?>
																				<optgroup label="Client(s)">

																					<?php foreach ($getallUser as $getallUser_key => $getallUser_value) {
																						if ($getallUser_value['crm_profile_pic'] != '') {
																							$get_val = base_url() . 'uploads/' . $getallUser_value['crm_profile_pic'];
																						} else {
																							$get_val = base_url() . 'assets/images/avatar-3.jpg';
																						}
																					?>

																						<option value="<?php echo $getallUser_value['id']; ?>" data-left="<?php echo $get_val ?>"><?php echo $getallUser_value['crm_name']; ?></option>

																					<?php } ?>
																				</optgroup>
																			<?php
																			} ?>

																		</select>
																		<input value="activate" id="activate_selectator4" type="button" style="display:none">

																	</section>
																</div>
															</div>
															<div class="user_error_div" style="color:red"> </div>


															<!-- 
                                             <?php if (!empty($getallstaff)) { ?>
                                             <div class="user_list">
                                                <h5>Staff(s)</h5>
                                                <?php foreach ($getallstaff as $getallUser_key => $getallUser_value) {  ?>
                                                <div class="media" title="<?php echo $getallUser_value['crm_name']; ?>">
                                                   <a class="media-left" href="#!">
                                                   <img class="media-object" src="<?php echo base_url(); ?>uploads/<?php echo $getallUser_value['crm_profile_pic']; ?>" onerror="this.src='<?php echo base_url(); ?>assets/images/avatar-3.jpg';" alt="Generic placeholder image">
                                                   </a>
                                                   <div class="media-body">
                                                      <div class="f-13 chat-header"><?php echo $getallUser_value['crm_name']; ?></div>
                                                      <div class="checkforgroup checkbox-color checkbox-primary">
                                                      <input type="checkbox" class="for_group" name="our_group_data" id="our_group_data_<?php echo $getallUser_value['id']; ?>" value="<?php echo $getallUser_value['id']; ?>">
                                                      <label for="our_group_data_<?php echo $getallUser_value['id']; ?>"></label>
                                                      </div>
                                                   </div>
                                                </div>
                                                <?php } ?>
                                             </div>
                                             <?php
												} ?>
                                             <?php if (!empty($getallUser)) { ?>
                                             <div class="user_list">
                                                <h5>Client(s)</h5>
                                                <?php foreach ($getallUser as $getallUser_key => $getallUser_value) {
														if ($getallUser_value['id'] != '1' && $getallUser_value['id'] != $_SESSION['id']) {
												?>
                                                <div class="media" title="<?php echo $getallUser_value['crm_name']; ?>">
                                                   <a class="media-left" href="#!">
                                                   <img class="media-object" src="<?php echo base_url(); ?>uploads/<?php echo $getallUser_value['crm_profile_pic']; ?>" onerror="this.src='<?php echo base_url(); ?>assets/images/avatar-3.jpg';" alt="Generic placeholder image">
                                                   </a>
                                                   <div class="media-body">
                                                      <div class="f-13 chat-header"><?php echo $getallUser_value['crm_name'];
																					if ($getallUser_value['user_type'] == 'FA') {
																						echo '(Firm Admin)';
																					} ?></div>
                                                      <div class="checkforgroup checkbox-color checkbox-primary">
                                                      <input type="checkbox" class="for_group" name="our_group_data" id="our_group_data_<?php echo $getallUser_value['id']; ?>" value="<?php echo $getallUser_value['id']; ?>">
                                                      <label for="our_group_data_<?php echo $getallUser_value['id']; ?>"></label>
                                                      </div>
                                                   </div>
                                                </div>
                                                <?php }
													} ?>
                                             </div>
                                             <?php
												} ?> -->

														</div>





														<!-- for member of the group -->
														<div class="created_group_list">
															<div class="group_append"></div>
															<?php $group_data = $this->db->query("SELECT * FROM chat_group where find_in_set('" . $_SESSION['id'] . "',members)  and status='' and firm_id=" . $_SESSION['firm_id'] . "")->result_array(); ?>
															<?php foreach ($group_data as $key => $value) {
																$group_member_data = $this->db->query("SELECT * FROM user where id in (" . $value['members'] . "," . $value['create_by'] . ") ")->result_array();
																$member_names = array();
																foreach ($group_member_data as $men_key => $mem_value) {
																	array_push($member_names, $mem_value['crm_name']);
																}
															?>
																<div class="media userlist-box box_<?php echo $value['id']; ?>" data-id="<?php echo $value['id']; ?>" data-username="<?php echo $value['groupname']; ?>" data-toggle="tooltip" data-placement="left" title="<?php echo $value['groupname']; ?>" data-group="group" data-member="<?php echo implode(',', $member_names); ?>" data-creater="<?php echo $value['create_by']; ?>" data-session="<?php echo $_SESSION['id']; ?>">
																	<div class="media-body">
																		<div class="f-13 chat-header"><?php echo $value['groupname']; ?></div>
																	</div>
																	<div class="badge badge-primary new_message_count_<?php echo $value['id']; ?>"></div>
																</div>
															<?php
															} ?>
														</div>
														<!-- end of chat group -->
														<!-- <ul>
                                             <li class="frnds">Friends</li>
                                             <li class="work">Work</li>
                                             </ul> -->
													</div>





													<!-- for admin and support -->

													<?php if (!empty($getallstaff) && count($getallstaff) > 0) { ?>
														<!-- end of admin support -->
														<div class="user-groups staff_section">

															<h6>Staff(s)</h6>
															<!-- for staff -->

															<div class="staff_append"></div>
															<?php
															$i = 1;

															$newTime = strtotime('-10 minutes');


															foreach ($getallstaff as $getallUser_key => $getallUser_value) {

																if ($getallUser_value['id'] != $_SESSION['id']) {


																	// $user_login=$this->db->query("SELECT * FROM activity_log where user_id=".$getallUser_value['id']." AND module='Login'  order by id DESC")->result_array();

																	$user_login = $this->db->query(" SELECT `id` FROM user WHERE id =" . $getallUser_value['id'] . " and last_login_time >=" . $newTime . " and firm_id=" . $_SESSION['firm_id'] . " ")->result_array();

																	if (!empty($user_login)) {

																		$status = "online";
																		$status_color = "bg-success";
																	} else {
																		$status = "offline";
																		$status_color = "bg-danger";
																	}
																	//echo $status;
															?>
																	<div class="media userlist-box box_<?php echo $getallUser_value['id']; ?>" data-id="<?php echo $getallUser_value['id']; ?>" data-status="<?php echo $status; ?>" data-username="<?php echo $getallUser_value['crm_name']; ?>" data-toggle="tooltip" data-placement="left" title="<?php echo $getallUser_value['crm_name']; ?>">
																		<a class="media-left" href="#!">
																			<img class="media-object" src="<?php echo base_url(); ?>uploads/<?php echo $getallUser_value['crm_profile_pic']; ?>" onerror="this.src='<?php echo base_url(); ?>assets/images/avatar-3.jpg';" alt="Generic placeholder image">
																			<div class="live-status <?php echo $status_color; ?>"></div>
																		</a>
																		<div class="media-body">
																			<div class="f-13 chat-header"><?php echo $getallUser_value['crm_name']; ?></div>
																		</div>
																		<div class="badge badge-primary new_message_count_<?php echo $getallUser_value['id']; ?>"></div>
																	</div>
															<?php
																	$i++;
																}
															} ?>
															<!-- end of staff -->
														</div>
													<?php } ?>



													<!-- for client -->
													<?php if (!empty($getallUser)) { ?>
														<div class="user-groups client_section">
															<h6>Client(s)</h6>
															<div class="client_append"></div>
															<?php
															$i = 1;

															foreach ($getallUser as $getallUser_key => $getallUser_value) {
																if ($getallUser_value['id'] != $_SESSION['id']) {

																	$user_login = $this->db->query("SELECT * FROM activity_log where user_id=" . $getallUser_value['id'] . " AND module='Login'  order by id DESC")->result_array();
																	if (!empty($user_login)) {
																		$login_time = $user_login[0]['CreatedTime'];
																		$user_logout = $this->db->query("SELECT * FROM activity_log where user_id=" . $getallUser_value['id'] . " AND module='Logout'  order by id DESC")->result_array();

																		$logout_time = isset($user_logout[0]['CreatedTime']) ? $user_logout[0]['CreatedTime'] : '0';
																		$status = "";
																		if ($login_time > $logout_time) {
																			if (date('Y-m-d', $login_time) == date('Y-m-d')) {
																				$status = "online";
																				$status_color = "bg-success";
																			} else {
																				$status = "offline";
																				$status_color = "bg-danger";
																			}
																		} else {
																			$status = "offline";
																			$status_color = "bg-danger";
																		}
																	} else {
																		$status = "offline";
																		$status_color = "bg-danger";
																	}
																	//echo $status;
															?>
																	<div class="media userlist-box box_<?php echo $getallUser_value['id']; ?>" data-id="<?php echo $getallUser_value['id']; ?>" data-status="<?php echo $status; ?>" data-username="<?php echo $getallUser_value['crm_name']; ?>" data-toggle="tooltip" data-placement="left" title="<?php echo $getallUser_value['crm_name']; ?>">
																		<a class="media-left" href="#!">
																			<img class="media-object" src="<?php echo base_url(); ?>uploads/<?php echo $getallUser_value['crm_profile_pic']; ?>" onerror="this.src='<?php echo base_url(); ?>assets/images/avatar-3.jpg';" alt="Generic placeholder image">
																			<div class="live-status <?php echo $status_color; ?>"></div>
																		</a>
																		<div class="media-body">
																			<div class="f-13 chat-header"><?php echo $getallUser_value['crm_name'];
																											if ($getallUser_value['user_type'] == 'FA') {
																												echo '(Firm Admin)';
																											} ?></div>
																		</div>
																		<div class="badge badge-primary new_message_count_<?php echo $getallUser_value['id']; ?>"></div>
																	</div>
															<?php
																	$i++;
																}
															} ?>
														</div>
														<!-- for client -->
													<?php } ?>

												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="page-error">
								<div class="card text-center">
									<div class="card-block">
										<div class="m-t-10">
											<i class="icofont icofont-warning text-white bg-c-yellow"></i>
											<h4 class="f-w-600 m-t-25">Not supported</h4>
											<p class="text-muted m-b-0">Chat not supported in this device</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<!-- Modal -->
<?php
$group_data = $this->db->query("SELECT * FROM chat_group where create_by=" . $_SESSION['id'] . "  and status='' ")->result_array();

// print_r($group_data);

?>
<?php foreach ($group_data as $key => $value) {
	$ex_members = explode(',', $value['members']);

	$group_member_data = $this->db->query("SELECT * FROM user where id in (" . $value['members'] . "," . $value['create_by'] . ") ")->result_array();
	$member_names = array();
	foreach ($group_member_data as $men_key => $mem_value) {
		array_push($member_names, $mem_value['crm_name']);
	}
?>
	<div class="modal fade recurring-msg common-schedule-msg1 adduserpopupcls" id="popup_group_edit_<?php echo $value['id']; ?>" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Change Group Option</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-12 form-group common-repeats1 ">
							<label class="label-column1 col-form-label">Group Name:</label>
							<div class="label-column2 input-group date">
								<input type='text' class="" id='group_name_<?php echo $value['id']; ?>' name="group_name" value="<?php echo $value['groupname']; ?>" />
								<div class="edit_groupname_error_div" style="color: red"></div>
							</div>
							<!-- members list -->
							<div class="chat_user_lists">
								<label class="label-column1 col-form-label">Memebers:</label>
								<div class="label-column2 input-group date">
									<div id="wrapper">
										<div id="wrapper_inner">
											<section>
												<select id="update_users_list_<?php echo $value['id']; ?>" name="update_users_list" class="users_list" multiple>

													<!--  <option value="">Select</option> -->

													<?php if (!empty($getallstaff)) { ?>
														<optgroup label="Staff(s)">

															<?php foreach ($getallstaff as $getallUser_key => $getallUser_value) {
																if ($getallUser_value['crm_profile_pic'] != '') {
																	$get_val = base_url() . 'uploads/' . $getallUser_value['crm_profile_pic'];
																} else {
																	$get_val = base_url() . 'assets/images/avatar-3.jpg';
																}
																if ($getallUser_value['id'] != '1' && $getallUser_value['id'] != $_SESSION['id']) {
															?>

																	<option value="<?php echo $getallUser_value['id']; ?>" data-left="<?php echo $get_val ?>" <?php if (in_array($getallUser_value['id'], $ex_members)) {
																																									echo "selected";
																																								} ?>><?php echo $getallUser_value['crm_name']; ?></option>

															<?php }
															} ?>
														</optgroup>
													<?php
													} ?>


													<?php if (!empty($getallUser)) { ?>
														<optgroup label="Client(s)">

															<?php foreach ($getallUser as $getallUser_key => $getallUser_value) {
																if ($getallUser_value['crm_profile_pic'] != '') {
																	$get_val = base_url() . 'uploads/' . $getallUser_value['crm_profile_pic'];
																} else {
																	$get_val = base_url() . 'assets/images/avatar-3.jpg';
																}
															?>

																<option value="<?php echo $getallUser_value['id']; ?>" data-left="<?php echo $get_val ?>" <?php if (in_array($getallUser_value['id'], $ex_members)) {
																																								echo "checked";
																																							} ?>><?php echo $getallUser_value['crm_name']; ?></option>

															<?php } ?>
														</optgroup>
													<?php
													} ?>

												</select>
												<input value="activate" id="activate_selectator4" type="button" style="display:none">

											</section>
										</div>
									</div>
								</div>
								<div class="edit_user_error_div" style="color: red"></div>

								<!--                      <?php if ($_SESSION['user_type'] != 'FC' && $_SESSION['user_type'] != 'FA'   && !empty($getallstaff)) { ?>
                     <div class="user_list">
                        <h5>For Support</h5>
                        <?php foreach ($getallstaff as $getallUser_key => $getallUser_value) {
																	if ($getallUser_value['id'] != '1' && $getallUser_value['id'] != $_SESSION['id']) {
						?>
                        <div class="media" title="<?php echo $getallUser_value['crm_name']; ?>">
                           <a class="media-left" href="#!">
                           <img class="media-object" src="<?php echo base_url(); ?>uploads/<?php echo $getallUser_value['crm_profile_pic']; ?>" onerror="this.src='<?php echo base_url(); ?>assets/images/avatar-3.jpg';" alt="Generic placeholder image">
                           </a>
                           <div class="media-body">

                            <label class="custom_checkbox1">
                            <input type="checkbox" class="for_group" name="our_group_data_<?php echo $value['id']; ?>" id="our_group_data_<?php echo $getallUser_value['id']; ?>" value="<?php echo $getallUser_value['id']; ?>" <?php if (in_array($getallUser_value['id'], $ex_members)) {
																																																										echo "checked";
																																																									} ?>  ><i></i>
                            </label>
                            <div class="f-13 chat-header"><?php echo $getallUser_value['crm_name']; ?></div>
                              
                           </div>
                        </div>
                        <?php }
																} ?>
                     </div>
                     <?php
															} ?>
                     <?php if (!empty($getallUser)) { ?>
                     <div class="user_list">
                        <h5>For Support</h5>
                        <?php foreach ($getallUser as $getallUser_key => $getallUser_value) {
								if ($getallUser_value['id'] != '1' && $getallUser_value['id'] != $_SESSION['id']) { ?>
                        <div class="media" title="<?php echo $getallUser_value['crm_name']; ?>">
                           <a class="media-left" href="#!">
                           <img class="media-object" src="<?php echo base_url(); ?>uploads/<?php echo $getallUser_value['crm_profile_pic']; ?>" onerror="this.src='<?php echo base_url(); ?>assets/images/avatar-3.jpg';" alt="Generic placeholder image">
                           </a>
                           <div class="media-body">
                            <label class="custom_checkbox1">
                              <input type="checkbox" class="for_group" name="our_group_data_<?php echo $value['id']; ?>" id="our_group_data_<?php echo $getallUser_value['id']; ?>" value="<?php echo $getallUser_value['id']; ?>" <?php if (in_array($getallUser_value['id'], $ex_members)) {
																																																										echo "checked";
																																																									} ?> ><i></i>
                            </label>
                              <div class="f-13 chat-header"><?php echo $getallUser_value['crm_name']; ?></div>
                           </div>
                        </div>
                        <?php }
							} ?>
                     </div>
                     <?php
						} ?> -->


							</div>
							<!-- end -->
						</div>
					</div>
				</div>
				<div class="modal-footer">

					<?php if ($_SESSION['permission']['Chat']['edit'] == 1) { ?>
						<button class="btn btn-info edit_create_group" data-id="<?php echo $value['id']; ?>" type="button">Update</button>
					<?php }
					if ($_SESSION['permission']['Chat']['delete'] == 1) { ?>
						<button type="button" class="btn btn-default delete_group" data-id="<?php echo $value['id']; ?>">Delete Group</button>
					<?php } ?>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
	</div>
<?php } ?>
<!-- modal close-->
<!-- management block -->
<?php if ($dash_id == 0) {
	if (isset($_COOKIE['remindoo_logout']) && $_COOKIE['remindoo_logout'] == 'remindoo_superadmin') {
		$this->load->view('super_admin/superAdmin_footer');
	} else {
		$this->load->view('includes/footer');
	}
}
?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<!-- <script src="<?php echo base_url(); ?>assets/js/mmc-common.js"></script> -->
<!--   <script src="<?php echo base_url(); ?>assets/js/mmc-chat.js"></script> -->
<?php $this->load->view('User_chat/mmc-common_js'); ?>
<?php $this->load->view('User_chat/mmc-chat_js'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/chat.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.edit-toggle1').click(function() {
			$('.proposal-down').slideToggle(300);
		});

		$("#legal_form12").change(function() {
			var val = $(this).val();
			if (val === "from2") {
				// alert('hi');
				$(".from2option").show();
			} else {
				$(".from2option").hide();
			}
		});

		$("#today_deadlineinfo,#thisweek_deadlineinfo,#thismonth_deadlineinfo,#thisyear_deadlineinfo").dataTable({
			//  "scrollX": true
		});
	})
</script>
<script type="text/javascript">
	$('.create_group').on('click', function(e) {


		$('.chat_user_list').toggle();
		$('#group_name').val('');
		var obj = $(this);

		$('.chat_user_list').find('div #selectator_create_users_list').find('.selectator_selected_items .selectator_selected_item_remove').trigger('click').trigger('click');

		// $('.chat_user_list').find('div #selectator_create_users_list').find('div .selectator_selected_item_remove').trigger('click');
		// console.log($('.chat_user_list').find('div #selectator_create_users_list').attr('class'));

		//  $("#create_users_list").val("");
		//  $('#activate_selectator4').trigger('click')


		if ($('.chat_user_list').is(":visible") == false) {
			//console.log('chk');
			obj.html('').html('Create Group <i class="fa fa-plus" aria-hidden="true"></i></a>');
		} else {
			obj.html('').html('Create Group <i class="fa fa-minus" aria-hidden="true"></i></a>');
		}

		//$('.chat_user_list').show();

	});
	$('.add_create_group').on('click', function() {
		$('.groupname_error_div').html('');
		$('.user_error_div').html('');

		var group_name = $('#group_name').val();
		var ids = $('#create_users_list').val();

		if (group_name == '') {
			$('.groupname_error_div').html('Enter Group Name');
			// $('.ids_error_div').html('Select Group Members');
		} else {

			if (ids.length > 0) {
				//alert(checkboxValues.join(', '));
				var data = {};
				data['name'] = group_name;
				data['member'] = ids.join(',');
				$('.LoadingImage').show();
				$.ajax({
					url: '<?php echo base_url(); ?>User_chat/create_group/',
					type: 'POST',
					data: data,
					success: function(data) {
						//alert(data);
						$('.LoadingImage').hide();
						console.log(data);
						if (data != 'wrong') {
							location.reload();
							// $('.created_group_list').append(data);
							// $('.chat_user_list').hide();
						}

					}
				});
			} else {
				$('.user_error_div').html('Select Group Members');
			}

		}


	});

	$('.group-info').on('click', function() {
		// alert('zzzz');
		//   $('.rsptrspt').html('zzzz');
		$('.edit_user_error_div').html('');
		$('.edit_groupname_error_div').html('');
	});

	$('.edit_create_group').on('click', function() {
		$('.edit_user_error_div').html('');
		$('.edit_groupname_error_div').html('');

		var id = $(this).attr('data-id');
		var group_name = $('#group_name_' + id).val();
		if (group_name == '') {
			$('.edit_groupname_error_div').html('Enter Group Name');
		} else {
			//     var checkboxValues = [];
			// $('input[name="our_group_data_'+id+'"]:checked').each(function(index, elem) {
			//     checkboxValues.push($(elem).val());
			// });

			var ids = $('#update_users_list_' + id).val();
			if (ids.length > 0) {
				//alert(checkboxValues.join(', '));
				var data = {};
				data['name'] = group_name;
				data['member'] = ids.join(',');
				$('.LoadingImage').show();
				$.ajax({
					url: '<?php echo base_url(); ?>User_chat/update_group/' + id,
					type: 'POST',
					data: data,
					success: function(data) {
						//alert(data);
						$('.LoadingImage').hide();
						//alert(data);
						location.reload();
						// console.log(data);
						// if(data!='wrong'){
						//   $('.created_group_list').append(data);
						//   $('.chat_user_list').hide();
						// }

					}
				});
			} else {
				$('.edit_user_error_div').html('Select Group Members');
			}

		}

	});

	$(document).on('click', '.delete_group', function() {
		var group_id = $(this).attr('data-id');
		var data = {};
		data['group_id'] = group_id;
		$('.LoadingImage').show();
		$.ajax({
			url: '<?php echo base_url(); ?>User_chat/delete_group_chat/' + group_id,
			type: 'POST',
			data: data,
			success: function(data) {

				$('.LoadingImage').hide();
				$('.box_' + group_id).hide();
				$('#popup_group_edit_' + group_id).modal('hide');
				$('.chatbox_' + group_id).hide();
				$('.modal-backdrop.show').hide();
			}
		});

	});
</script>

<script src="<?php echo base_url() ?>assets/menudropdown/fm.selectator.jquery.js"></script>
<script type="text/javascript">
	$(function() {
		var $activate_selectator = $('#activate_selectator4');
		$activate_selectator.click(function() {
			var $select = $('.users_list');
			if ($select.data('selectator') === undefined) {
				$select.selectator({
					showAllOptionsOnFocus: true,
					searchFields: 'value text subtitle right',
					placeholder: 'Members',
				});
				$activate_selectator.val('destroy');
			} else {
				$select.selectator('destroy');
				$activate_selectator.val('activate');
			}
		});
		$activate_selectator.trigger('click');
	});
</script>