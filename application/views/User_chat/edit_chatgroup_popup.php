 <!-- Modal -->
  <?php 
$group_data=$this->db->query("SELECT * FROM chat_group where create_by=".$_SESSION['id']." ")->result_array(); ?>
<?php foreach ($group_data as $key => $value) {
  $ex_members=explode(',',$value['members']);

                                        $group_member_data=$this->db->query("SELECT * FROM user where id in (".$value['members'].",".$value['create_by'].") ")->result_array();
                                        $member_names=array();
                                        foreach ($group_member_data as $men_key => $mem_value)
                                         {
                                            array_push($member_names, $mem_value['crm_name']);
                                         }                                    
                                       ?>
  <div class="modal fade recurring-msg common-schedule-msg1" id="popup_group_edit_<?php echo $value['id']; ?>" role="dialog">
     <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Change Group Option</h4>
           </div>
           <div class="modal-body">
                 <div class="row">
                                    
                    <div class="col-sm-6 form-group common-repeats1 ">
                    <label class="label-column1 col-form-label">Group Name:</label>
                    <div class="label-column2 input-group date">
                   
                    <input type='text' class="" id='group_name_<?php echo $value['id'];?>' name="group_name" value="<?php echo $value['groupname'];?>" />
                    </div>
                    
                    <!-- members list -->
                       <div class="chat_user_list">
                                                                       
                                      <?php if($_SESSION['roleId']!=6 && !empty($getallstaff)){ ?>
                                        <div class="user_list">
                                        <h5>Staff</h5>
                                          <?php  foreach ($getallstaff as $getallUser_key => $getallUser_value) { ?>
                                            <div class="media" title="<?php echo $getallUser_value['crm_name']; ?>">
                                              <a class="media-left" href="#!">
                                                <img class="media-object" src="<?php echo base_url();?>uploads/<?php echo $getallUser_value['crm_profile_pic'];?>" onerror="this.src='<?php echo base_url();?>assets/images/avatar-3.jpg';" alt="Generic placeholder image">
                                                
                                              </a>

                                              <div class="media-body">
                                                <div class="f-13 chat-header"><?php echo $getallUser_value['crm_name']; ?></div>
                                                 <input type="checkbox" class="for_group" name="our_group_data_<?php echo $value['id'];?>" id="our_group_data_<?php echo $getallUser_value['id'];?>" value="<?php echo $getallUser_value['id'];?>" <?php if(in_array($getallUser_value['id'],$ex_members)){ echo "checked"; } ?>  >
                                              </div>

                                           
                                            </div>
                                          <?php } ?>
                                        </div>

                                        <?php
                                        } ?>
                                            <?php if(!empty($getallUser)){ ?>
                                        <div class="user_list">
                                        <h5>Client</h5>
                                          <?php  foreach ($getallUser as $getallUser_key => $getallUser_value) { ?>
                                            <div class="media" title="<?php echo $getallUser_value['crm_name']; ?>">
                                              <a class="media-left" href="#!">
                                                <img class="media-object" src="<?php echo base_url();?>uploads/<?php echo $getallUser_value['crm_profile_pic'];?>" onerror="this.src='<?php echo base_url();?>assets/images/avatar-3.jpg';" alt="Generic placeholder image">
                                                
                                              </a>
                                            
                                              <div class="media-body">
                                                <div class="f-13 chat-header"><?php echo $getallUser_value['crm_name']; ?></div>
                                                  <input type="checkbox" class="for_group" name="our_group_data_<?php echo $value['id'];?>" id="our_group_data_<?php echo $getallUser_value['id'];?>" value="<?php echo $getallUser_value['id'];?>" <?php if(in_array($getallUser_value['id'],$ex_members)){ echo "checked"; } ?> >
                                              </div>
                                           
                                            </div>
                                          <?php }  ?>
                                        </div>

                                        <?php
                                        } ?>

                                        
                                      </div>
                    <!-- end -->
                    </div>
                   
    
                  </div>
             </div>
             <div class="modal-footer">
                <button class="btn btn-info edit_create_group" data-id="<?php echo $value['id'];?>" type="button">Update</button>
                <button type="button" class="btn btn-default delete_group" data-id="<?php echo $value['id'];?>" >Delete Group</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
             </div>
          </div>
       </div>
    </div>
    <?php } ?>
    <!-- modal close-->