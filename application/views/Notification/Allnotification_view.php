<?php $this->load->view('includes/header');?>
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css?ver=2">
<div class="pcoded-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
          <div class="modal-alertsuccess alert alert-success" style="display:none;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">X</a>
            <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
            <div class="pop-realted1">
              <div class="position-alert1">               
              </div>
            </div>
          </div>

          <div class="modal-alertsuccess alert succ popup_info_msg" style="display:none;">
         <div class="newupdate_alert"><a href="#" class="close" id="close_info_msg">×</a>
         <div class="pop-realted1">
         <div class="position-alert1">
               Please! Select Record...
         </div></div>
         </div>
      </div>

            <!-- Page body start -->
            <div class="page-body all-notifications1 notification-wrapper">
               <div class="row">
                  <!--start-->
                  <div class="col-sm-12 common_form_section2 forservice">
                     <div class="deadline-crm1 floating_set">
                        <ul class="nav nav-tabs all_user1 md-tabs pull-left">
                           <li class="nav-item">
                              <a class="nav-link" href="javascript:;">Notifications</a>
                              <div class="slide"></div>
                           </li>
                        </ul>
                     
                         <div class="f-right">
                                 
                                  
                                  <div class="assign_delete">

                                    <button type="button" id="Read_btn" data-val="4" class="btn status_btn task_btn" style="display: none;">Read</button>
                                    <button type="button" class="btn status_btn task_btn"  data-val="1" id="Unread_btn" style="display: none;">Unread</button>
                                    <?php if($_SESSION['permission']['Task']['delete']=='1'){ ?>
                                    <button type="button" id="Delete_btn"  data-val="0" class="btn del-tsk12 task_btn status_btn" style="display: none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i> Archive </button>
                                    <?php } ?>

                                    <?php if($_SESSION['permission']['Invoices']['edit'] == '1') { ?>
                                      <button type="button" id="read" data-val="1" class="btn status_btn" style="display: none;background-color:#00a2e8 !important;">Read</button>
                                      <button type="button" class="btn status_btn"  data-val="0" id="unread" style="display: none;background-color:#ffc90e !important;">Unread</button>
                                    <?php } ?> 
                                    <?php if($_SESSION['permission']['Invoices']['delete'] == '1') { ?>
                                      <button type="button" id="deleteInvoice_rec" class="del-tsk12 f-right btn btn-primary" data-val="2" style="display:none;background-color:#ed1c24 !important;"><i class="fa fa-trash fa-6" style="color: #fff !important" aria-hidden="true"></i>Archive</button>
                                    <?php } ?>                                 
                                                                   
                                   </div>
                                   
                                  </div>

                                  </div>

                   <div class="all-usera1 user-dashboard-section1 leads_source button_visibility">
                    <table class="table client_table1 text-center display nowrap" id="alltask" cellspacing="0" width="100%">
                      <thead>
                        <tr class="text-uppercase">
                        
                           <th><label class="custom_checkbox1">
                                <input type="checkbox" id="select_alltask">
                              </label>
                              <!-- <select class="browser-default custom-select" style="width:100px">
                                <option value="">None</option>
                                <option value="1">All</option>
                                <option value="2">Read</option>
                                <option value="3">Unread</option>
                              </select> -->

                              <div class="notifidrop">
                              <a class="dropdown-toggle1" href="javascript:;" title="Menu"></a>
                              <ul class="dropdown1 notification_status">
                                <li class><a data-id="" href="javascript:;">None</a></li>
                                <li><a data-id="0" href="javascript:;">All</a></li>
                                <li><a data-id="4" href="javascript:;">Read</a></li>
                                <li><a data-id="1" href="javascript:;">Unread</a></li>
                              </ul>
                              </div>
                          
                            </th>
                          
                            <th></th>
                            <th></th>
                            <th></th>
 
                        </tr>
                      </thead>
                      <tbody>

                       
                     </tbody>
                  </table>
        <input type="hidden" class="rows_selected" id="select_source_count" >                        
               </div>
                     <!-- close -->
                  </div>
               </div>
               <!-- Page body end -->
            </div>
         </div>
         <!-- Main-body end -->
         <div id="styleSelector">
         </div>
      </div>
   </div>
</div>
</div>
</div>
</div>


<div class="modal fade" id="task_confirmation" role="dialog">
    <div class="modal-dialog modal-dialog-notify">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
           <h4>Confirmation</h4>     
          <!-- <h4 class="modal-title">Are you Want Delete the Notification</h4> -->
        </div>
        <div class="modal-body">
        <div class="cont"></div>
        <input type="hidden" id="task_data_value">    
        <input type="hidden" id="task_data_status">     
            
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default" onclick="task_delete_action()" >Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
      </div>

    </div>
  </div>


<div class="modal fade" id="confirmation" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
        <input type="text" name="data_value" style="display: none;">        
        <div class="cont"></div>          
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default" onclick="delete_action()" >Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
      </div>

    </div>
  </div>


<!-- ajax loader -->
<!-- ajax loader end-->
<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script src="<?php echo base_url()?>assets/js/custom/datatable_extension.js"></script>


<script type="text/javascript" src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script> 

<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>

<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>

<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<!-- <script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script> -->
<script type="text/javascript">
var TaskTable_Instance;
var reset_select=0;
var MainStatus_Tab = 'all_task';
$(document).ready(function() {

   var pageLength = "<?php echo get_firm_page_length() ?>"; 

    TaskTable_Instance = $('#alltask').DataTable({

                   "pageLength": pageLength,

                 "processing": true,
                  "serverSide": true,
                  
               
                  'ajax' : {
                  'url':'<?php echo base_url() ?>Notification/get_notification',
                  'type':'POST',
                  'data':function(data){


                    var get_status=$('.notification_status  .active a').attr('data-id');

                    data.searchByStatus=(get_status == 1 ||get_status == 4 )?get_status:'';
      

              
                  }
                  },


              "ordering": false,

               "drawCallback": function( settings ) {


                      var get_status=$('.notification_status  .active a').attr('data-id');

                    if(get_status !== undefined){

                    get_select_value(get_status);
                  }

                   

                       

                    },


    });



      $(".notification_status li a").click(function(event) {

        // $('#select_alltask').trigger('click');
        reset_select=0;
        $('.notification_status li').removeClass('active');

        $(this).parent('li').addClass('active');

       TaskTable_Instance.draw();

      });

      function get_select_value(get_status)
      {
        if(get_status != '' && reset_select==0){
            if(!$('#select_alltask')[0].checked){
                $('#select_alltask').trigger('click');
            }
            else
            {
                 $('#alltask tr').find('.alltask_checkbox').prop('checked',true);
            }
          }
          else
          {
            if($('#select_alltask')[0].checked){
             $('#select_alltask').trigger('click');
           }

          }



      }

    $(document).on("change", "#select_alltask", function(event) {




       var checked = this.checked;

          TaskTable_Instance.column(0,{ search: 'applied' }).nodes().to$().each( function () {
              $(this).find('.alltask_checkbox').prop('checked',checked);


       if(checked)
       {    $(this).find('.alltask_checkbox').prop('checked',true);     
           //$(".alltask_checkbox").attr('checked', this.checked);
          $("#Read_btn").show();
           $("#Unread_btn").show();  
            $("#Delete_btn").show(); 
            $(".scroll_table").addClass("scroll_table_notify");
         
        } 
        else
         {
          reset_select=1;

           // $('.alltask_checkbox').prop('checked',false);
             $(this).find('.alltask_checkbox').prop('checked',false);

          $("#Read_btn").hide();
           $("#Unread_btn").hide();  
            $("#Delete_btn").hide(); 
             // $("#btn_review_send").hide();
             $(".scroll_table").removeClass("scroll_table_notify");      
        }


            });


    });


$(document).on("change",".alltask_checkbox",function(event)
    { 
      var count=0;
      var tr=0;
      var ck_tr=0;
      var task_id=0;
      TaskTable_Instance.column(0).nodes().to$().each(function(index) {
        
      if($(this).find(".alltask_checkbox").is(":checked")){
          ck_tr++;

      }

      
      tr++;

      });
       //  alert("re"+$("#alltask").attr("data-status")+ck_tr);
        if(tr==ck_tr)
          {
            $("#select_alltask").prop("checked",true);
           } 
        else
        {
         $("#select_alltask").prop("checked",false);
        } 
      if(ck_tr)
      {
       
         $("#Read_btn").show();
            $("#Unread_btn").show();  
            $("#Delete_btn").show();
            $(".scroll_table").addClass("scroll_table_notify"); 
      }
      else  
      {
            $("#Read_btn").hide();
            $("#Unread_btn").hide();  
            $("#Delete_btn").hide();
            $(".scroll_table").removeClass("scroll_table_notify");  
      } 

       
    });


});

  function getSelectedRow()
  {
    var alltask = [];

    TaskTable_Instance.column(0).nodes().to$().each(function(index) {
      
      if( $(this).find(".alltask_checkbox").is(":checked") )
      {
        alltask.push( $(this).find(".alltask_checkbox").attr('data-id') );      
      }

    });

    return alltask;

  }

          $(document).on('click' , ".task_btn" , function(e){

            $('#task_data_status').val('');
                $('#task_data_value').val('');

              $('#task_confirmation').modal('show');   
              
               var status=$(this).data('val');
            var alltask = getSelectedRow();
          if(alltask.length <=0) {
             $('.popup_info_msg').show();
             $(".popup_info_msg .position-alert1").html("Please Select Any Records..");
          } else {
            
           var selected_alltask_values = alltask.join(",");

           $('#task_data_status').val(status);
               $('#task_data_value').val(selected_alltask_values);
               if(status==0)
              {
             $('#task_confirmation').find('.cont').html(' Are you sure you want to archive?');  
              }
               if(status==1)
              {
             $('#task_confirmation').find('.cont').html(' Are you sure you want to Unread?');  
              }
               if(status==4)
              {
             $('#task_confirmation').find('.cont').html(' Are you sure you want to Read?');  
              }
          }
        });


          $(document).on('click' , ".act_btn" , function(e){

                $('#task_data_status').val('');
                $('#task_data_value').val('');

              $('#task_confirmation').modal('show');   
              $('#task_confirmation').find('.cont').html(' Are you sure you want to delete?');  
              //return;
               var status=$(this).data('val');
               
               var id=$(this).data('id');

                 var alltask=[];
               alltask.push(id);
   
           var selected_alltask_values = alltask.join(",");

               $('#task_data_status').val(status);
               $('#task_data_value').val(selected_alltask_values);
             });

      function task_delete_action(){

              var status= $('#task_data_status').val();
               
              var selected_alltask_values= $('#task_data_value').val();
             
           // alert(status);
           //  return;
            var formData={'id':selected_alltask_values,'status':status};
          // alert(assign_to);
           $.ajax({
            type: "POST",
            url: "<?php echo base_url().'Notification/updatenoti_status';?>",
            cache: false,
            data: formData,
            beforeSend: function() {
              $(".LoadingImage").show();
            },
            success: function(data) {

               $('#task_confirmation').find('.close').trigger('click'); 

              $(".LoadingImage").hide();
              var json = JSON.parse(data); 
              status=json['status'];
              $(".popup_info_msg").show();
              $(".popup_info_msg .position-alert1").html("Updated Successfully...");
              if(data=='1'){
               setTimeout(function(){ $(".popup_info_msg").hide(); location.reload(); }, 3000); 
                  }           
                 }
              });          
        }

  $(document).ready(function() 
  {
     window.invoice=[]; 
    $(document).on('change','#select_all_invoice',function(event) 
    {  //on click 
      var checked = this.checked;
      invoice=[];
      TaskTable_Instance.column(0).nodes().to$().each(function(index) 
      {    
         if (checked) 
         {
            $('#select_all_repeat').prop('checked', false);
            $('#select_all_repeat').trigger('change');
            $(this).find('.invoice_checkbox').prop('checked', 'checked');
            invoice.push($(this).find('.invoice_checkbox').data('invoice-id'));   
            $("#deleteInvoice_rec").show();   
            $('#read').show();
            $('#unread').show();      
         }
         else
         {
            $(this).find('.invoice_checkbox').prop('checked', false);
             $("#deleteInvoice_rec").hide();  
             $('#read').hide();
             $('#unread').hide();         
         } 
      });         
    });        

    window.repeat_invoice=[]; 
    $(document).on('change','#select_all_repeat',function(event) 
    {  //on click 
      var checked = this.checked;
      repeat_invoice=[]; 
      TaskTable_Instance.column(0).nodes().to$().each(function(index) 
      {    
         if(checked)
         {
            $('#select_all_invoice').prop('checked', false);
            $('#select_all_invoice').trigger('change');
            $(this).find('.invoice_checkbox1').prop('checked', 'checked');
            repeat_invoice.push($(this).find('.invoice_checkbox1').data('invoice-id'));   
            $("#deleteInvoice_rec").show();    
            $('#read').show();
            $('#unread').show();     
         }
         else
         {
            $(this).find('.invoice_checkbox1').prop('checked', false);
            $("#deleteInvoice_rec").hide();  
            $('#read').hide();
            $('#unread').hide();        
         } 
      });        
    });

    $(document).on('click','.invoice_checkbox', function() 
    {
        if($(this).is(':checked', true)) 
        {        
            $("#deleteInvoice_rec").show();    
            $('#read').show();
            $('#unread').show();

            $('.invoice_checkbox1').each(function()
            {
               $(this).prop('checked',false);
            });
        }
        else
        {
            $("#select_all_invoice").prop('checked',false);
            if($("input.invoice_checkbox:checked").length == 0)
            {
              $("#deleteInvoice_rec").hide();  
              $('#read').hide();
              $('#unread').hide();        
            }
        }
     });  

     $(document).on('click','.invoice_checkbox1', function() 
     {
         if($(this).is(':checked', true)) 
         {        
             $("#deleteInvoice_rec").show();    
             $('#read').show();
             $('#unread').show();

             $('.invoice_checkbox').each(function()
             {
                $(this).prop('checked',false);
             });
         }
         else
         {
             $("#select_all_repeat").prop('checked',false);
             if($("input.invoice_checkbox1:checked").length == 0)
             {
               $("#deleteInvoice_rec").hide();  
               $('#read').hide();
               $('#unread').hide();        
             }
         }
      });  
  });  

  $(document).on('click', '#deleteInvoice_rec,#read,#unread',function()
  {    
       $('#confirmation').find('.cont').html('');
       if($(this).attr('id') == 'deleteInvoice_rec')
       {
          $('#confirmation').find('.cont').html('<p> Are you sure you want to delete?</p>');          
       }
       else if($(this).attr('id') == 'read')
       {
          $('#confirmation').find('.cont').html('<p> Are you sure you want to mark as read?</p>');
       }
       else if($(this).attr('id') == 'unread')
       {
          $('#confirmation').find('.cont').html('<p> Are you sure you want to mark as unread?</p>');
       }

       $('input[name="data_value"]').val($(this).data('val'));
       $('#confirmation').modal('show');      
  });

  function delete_action()
  {
     var ids = [];
     var type = "";
     var data_val = $('input[name="data_value"]').val();

     if($('#select_all_invoice').prop('checked') == true)
     {
        ids = invoice;
        type = 'invoice';
     }
     else if($('#select_all_repeat').prop('checked') == true)
     {
        ids = repeat_invoice;
        type = 'repeat-invoice';
     }
     else 
     {
        if($("input.invoice_checkbox:checked").length>0)
        {
          $('.invoice_checkbox').each(function()
          {
             if($(this).prop('checked') == true)
             {
                ids.push($(this).data('invoice-id'));
             }
          });
          type = 'invoice';
        }

        if($("input.invoice_checkbox1:checked").length>0)
        {
           $('.invoice_checkbox1').each(function()
           {
              if($(this).prop('checked') == true)
              {
                 ids.push($(this).data('invoice-id'));
              }
           });
           type = 'repeat-invoice';
        }
     }
     
     ids = JSON.stringify(ids);
     
     $.ajax(
     {
        url:'<?php echo base_url().'Notification/update_invoice_notification'; ?>',
        type:'POST',
        data:{'type':type,'ids':ids,'status':data_val},

        beforeSend: function() 
        {
          $(".LoadingImage").show();
        },

        success:function(data)
        {
           if(data != 0)
           {
             $(".LoadingImage").hide();
             $('.alert-success').find('.position-alert1').html('');
             $('.alert-success').find('.position-alert1').html('Updated Successfully!');
             $('.alert-success').show();
             $('#confirmation').find('.close').trigger('click'); 
             setTimeout(function(){  $('.alert-success').hide(); location.reload(); }, 3000);
           }  
        }
     });
  }

</script>
<script>
  $(function() {
  
  // Dropdown toggle
  $('.dropdown-toggle1').click(function(){
    $(this).next('.dropdown1').toggle();
  });

  $(document).click(function(e) {
    var target = e.target;
    if (!$(target).is('.dropdown-toggle1') && !$(target).parents().is('.dropdown-toggle1')) {
      $('.dropdown1').hide();
    }
  });

  });
</script>

</body>
</html>