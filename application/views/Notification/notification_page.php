<?php $this->load->view('includes/header');?>
<div class="pcoded-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <!--start-->
                  <div class="col-sm-12 common_form_section2 forservice">
                     <div class="deadline-crm1 floating_set">
                        <ul class="nav nav-tabs all_user1 md-tabs pull-left">
                           <li class="nav-item">
                              <a class="nav-link" href="javascript:;">Notifications</a>
                              <div class="slide"></div>
                           </li>
                        </ul>
                     </div>
                     <div class="col-xs-12 public-bill01 editser">
                        <div class="add_new_post0412">
                        <div class="card">
                           <div class="service_view01">
                              <h2>Email Notifications</h2>
                           </div>
                           <div class="service-steps">
                              <div class="inside-step12">
                                 <label class="col-12 col-form-label">Alerts</label>
                                 <div class="checkbox-fade fade-in-primary">
                                    <div class="form-group row radio_bts ">
                                       <label class="col-sm-4 col-form-label">Co-email</label>
                                       <div class="col-sm-8">
                                          <input type="checkbox" class="js-small f-right fields" name="previous_account" id="previous_account" value="on" data-id="preacc">
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="service-steps steps">
                              <div class="inside-step12">
                                 <label class="col-12 col-form-label">Reminders</label>
                                 <div class="checkbox-fade fade-in-primary">
                                    <div class="form-group row radio_bts ">
                                       <label class="col-sm-9 col-form-label">Membership Alerts</label>
                                       <div class="col-sm-3">
                                          <input type="checkbox" class="js-small f-right fields" name="previous_account" id="previous_account" value="on" data-id="preacc">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="checkbox-fade fade-in-primary">
                                    <div class="form-group row radio_bts ">
                                       <label class="col-sm-9 col-form-label">Unread Messages</label>
                                       <div class="col-sm-3">
                                          <input type="checkbox" class="js-small f-right fields" name="previous_account" id="previous_account" value="on" data-id="preacc">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="checkbox-fade fade-in-primary">
                                    <div class="form-group row radio_bts ">
                                       <label class="col-sm-9 col-form-label">Open tasks</label>
                                       <div class="col-sm-3">
                                          <input type="checkbox" class="js-small f-right fields" name="previous_account" id="previous_account" value="on" data-id="preacc">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="checkbox-fade fade-in-primary">
                                    <div class="form-group row radio_bts ">
                                       <label class="col-sm-9 col-form-label">Contracts</label>
                                       <div class="col-sm-3">
                                          <input type="checkbox" class="js-small f-right fields" name="previous_account" id="previous_account" value="on" data-id="preacc">
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="service-steps steps">
                              <div class="inside-step12">
                                 <label class="col-12 col-form-label">Updates</label>
                                 <div class="checkbox-fade fade-in-primary">
                                    <div class="form-group row radio_bts ">
                                       <label class="col-sm-9 col-form-label">AND CO Products Updates</label>
                                       <div class="col-sm-3">
                                          <input type="checkbox" class="js-small f-right fields" name="previous_account" id="previous_account" value="on" data-id="preacc">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="checkbox-fade fade-in-primary">
                                    <div class="form-group row radio_bts ">
                                       <label class="col-sm-9 col-form-label">Hustle&Co Weekly Digest</label>
                                       <div class="col-sm-3">
                                          <input type="checkbox" class="js-small f-right fields" name="previous_account" id="previous_account" value="on" data-id="preacc">
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                          <!--  <div class="service-steps steps">
                              <div class="inside-step12">
                                 <label class="col-12 col-form-label">Weekly Reports</label>
                                 <div class="checkbox-fade fade-in-primary">
                                    <div class="form-group row radio_bts ">
                                       <label class="col-sm-9 col-form-label">Membership Alerts</label>
                                       <div class="col-sm-3">
                                          <input type="checkbox" class="js-small f-right fields" name="previous_account" id="previous_account" value="on" data-id="preacc">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="checkbox-fade fade-in-primary">
                                    <div class="form-group row radio_bts ">
                                       <label class="col-sm-9 col-form-label">Unread Messages</label>
                                       <div class="col-sm-3">
                                          <input type="checkbox" class="js-small f-right fields" name="previous_account" id="previous_account" value="on" data-id="preacc">
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div> -->
                           </div>
                        <div class="card">
                           <div class="service_view01">
                              <h2>Notifications</h2>
                           </div>
                           <div class="service-steps">
                              <div class="inside-step12">
                                 <div class="checkbox-fade fade-in-primary">
                                    <div class="form-group row radio_bts ">
                                       <label class="col-sm-4 col-form-label">Notifications</label>
                                       <div class="col-sm-8">
                                          <input type="checkbox" class="js-small f-right fields" name="previous_account" id="previous_account" value="on" data-id="preacc">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="checkbox-fade fade-in-primary">
                                    <div class="form-group row radio_bts ">
                                       <label class="col-sm-4 col-form-label">Notification sound #bing</label>
                                       <div class="col-sm-8">
                                          <input type="checkbox" class="js-small f-right fields" name="previous_account" id="previous_account" value="on" data-id="preacc">
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           </div>

                        </div>
                     </div>
                     <!-- close -->
                  </div>
               </div>
               <!-- Page body end -->
            </div>
         </div>
         <!-- Main-body end -->
         <div id="styleSelector">
         </div>
      </div>
   </div>
</div>
</div>
</div>
</div>
<div class="modal fade" id="addService" role="dialog">
   <div class="modal-dialog">
      <form name="form1" action="<?php echo base_url(); ?>service/service_add" method="post">
         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Add New Service</h4>
            </div>
            <div class="modal-body">
               <label>Service Name</label>
               <input type="text" name="service" value="">
            </div>
            <div class="modal-footer">
               <button type="submit" name="submit">Submit</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
         </div>
      </form>
   </div>
</div>
<!-- ajax loader -->
<!-- ajax loader end-->
<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<!-- modernizr js -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/switchery/js/switchery.min.js"></script>
</script>
</body>
</html>