<!DOCTYPE html>
<link href="http://remindoo.org/CRMTool/assets/css/jquery.filer.css" type="text/css" rel="stylesheet" />
<link href="<?php echo base_url() ?>/assets/css/timeline-style.css" type="text/css" rel="stylesheet" />
<!-- Tree Select -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/tree_select/style.css?ver=2">
<!-- / -->

<style type="text/css">
	.jFiler-items-list {
		display: none;
	}

	.for_leads_detailed_right {
		padding-top: 4%;
		padding-bottom: 2%;
	}

	.pt-3rem {
		padding-top: 3rem;
	}
</style>


<?php
// print_r();
// exit;
$assign_data = array();
$link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$link_array = explode('/', $link);
$page = end($link_array);

$sql_val = $this->db->query("SELECT * FROM firm_assignees where module_id = " . $page . " AND module_name='LEADS'")->result_array();

foreach ($sql_val as $key1 => $value1) {
	$assign_data[] = $value1['assignees'];
}

?>
<!-- Services for Leads Notes -->
<?php
	$client_services = $this->Common_mdl->getClientServices($client_id);
?>

<?php
/** dev: rspt **/
$this->load->view('includes/header');

$succ = $this->session->flashdata('success');
$error = $this->session->flashdata('error');

$user_team = $this->db->query("select * from team where create_by=" . $_SESSION['id'] . " ")->result_array();

$department = $this->db->query("select * from department_permission where create_by=" . $_SESSION['id'] . " ")->result_array();


?>
<?php /* ***********************************************************/
$country = $leads['country'];
if (is_numeric($country)) {
	$country_name = $this->Common_mdl->select_record('countries', 'id', $country);
} else {
	$country_name = $country;
}
$l_status = $leads['lead_status'];
$status_name = $this->Common_mdl->select_record('leads_status', 'id', $l_status);
// $languages = $this->Common_mdl->select_record('languages','id',$leads['default_language']);
$assigned = array();
$team = array();
$dept = array();
foreach (explode(',', $leads['assigned']) as $leads_key => $leads_value) {
	$assigned[] = $this->Common_mdl->select_record('user', 'id', $leads_value);
}
foreach (explode(',', $leads['team']) as $team_key => $team_value) {
	$team[] = $this->Common_mdl->select_record('team', 'id', $team_value);
}
foreach (explode(',', $leads['dept']) as $dept_key => $dept_value) {
	$dept[] = $this->Common_mdl->select_record('department_permission', 'id', $dept_value);
}


?>
<?php
$leads_notes_tabs = $this->db->query(" select * from leads_notes where lead_id ='" . $leads["id"] . "' order by id desc")->result_array();
$i = 1;
foreach ($leads_notes_tabs as $notes_key => $notes_value) {
	$data['data_uid'] = 'TB-' . $i;
	$this->Common_mdl->update('leads_notes', $data, 'id', $notes_value['id']);
	$i++;
}
$leads_notes = $this->db->query(" select * from leads_notes where lead_id ='" . $leads["id"] . "' order by id desc")->result_array();
?>
<!-- Tree Select -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/tree_select/style.css?ver=2">
<!-- / -->

<div class="modal-alertsuccess alert alert-danger-check succ staff-added dashboard_success_message" style="display:none;">
	<div class="newupdate_alert"> <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
		<div class="pop-realted1">
			<div class="position-alert1">
				Status Changed Successfully...
			</div>
		</div>
	</div>
</div>

<div class="modal-alertsuccess alert  succ popup_info_box" style="display:none;">
	<div class="newupdate_alert">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
		<div class="pop-realted1">
			<div class="position-alert1">
			</div>
		</div>
	</div>
</div>

<!-- for 23-04-2018 for edit leads -->
<!-- for convert to customer popup -->
<div class="leads_convert_customer">
	<div id="popup_convert_to_customer" class="modal fade leads_popup" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Convert To Customer</h4>
				</div>
				<div class="modal-body">
					<div id="new_lead" class="lead-summary2 floating_set new-leadss">
						<div class="space-equal-data col-sm-12">
							<div class="lead-popupsection1 floating_set">
								<div class="col-xs-12 col-sm-6  lead-form1 ">
									<label>Customer User Name</label>
									<div class="">
										<input type="text" name="user_name" id="user_name" value="">
									</div>
								</div>
								<div class="col-xs-12 col-sm-6  lead-form1 ">
									<label>Customer Password</label>
									<div class="">
										<input type="text" name="password" id="password" value="">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input type="button" name="save" id="save" value="Convert">
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end of customer -->

<!-- Leads Notes Modal Starts -->
<div id="leads_note_popup" class="modal fade" role="dialog" data-backdrop="static">
		<div class="modal-dialog modal-dialog-timeline">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header modal-header-timeline">
					<button type="button" class="close" data-dismiss="modal"></button>
					<h4 class="modal-title">Create a Note</h4>
				</div>
				<div class="modal-body modal-body-timeline">
					<form id="add_note_rec" method="post" name="form">
						<div class="editor_note">
							<textarea name="feed_msg" id="leads_notes_msg" class="notes_text_box" placeholder="Type your text here..."></textarea>
							<span class="notes_errormsg error_msg" style="color: red;"></span>
						</div>
						<div class="deadline-crm1 floating_set" id="trig_acts">
							<ol class="all_user1 md-tabs pull-left u-dashboard">
								<li class="nav-item notes">
									<a class="active" data-id="addNotes_tab" href="javascript:void(0)" data-value="notes">Add Notes
									</a>
								</li>
								<li class="nav-item calllog">
									<a data-id="call_tab" href="javascript:void(0)" data-value="calllog"> Call
									</a>
								</li>
								<li class="nav-item email">
									<a data-id="email_tab" href="javascript:void(0)" data-value="email">Email
									</a>
								</li>
								<li class="nav-item smslog">
									<a data-id="sms_tab" href="javascript:void(0)" data-value="smslog">SMS
									</a>
								</li>
								<li class="nav-item form_builder logactivity">
									<a data-id="log_activity_tab" href="javascript:void(0)" data-value="logactivity">Log Activity
									</a>
								</li>
								<li class="nav-item form_builder meeting">
									<a data-id="meeting_tab" href="javascript:void(0)" data-value="meeting">Meeting
									</a>
								</li>
								<!-- <li class="nav-item form_builder">
									<a class="nav-link" data-toggle="tab" href="#task_create_tab">Create Task</a>
								</li> -->
								<li class="nav-item form_builder schedule">
									<a data-id="schedule_tab" href="javascript:void(0)" data-value="schedule">Schedule
									</a>
								</li>
							</ol>
						</div>
						<div class="form-group col-sm-6">
							<label>Services Type</label>
							<div class="dropdown-sin-12 lead-form-st">
								<select name="services_type" id="services_type" class="form-control fields" placeholder="Select Services Type">
									<option value="">--Select Service Type--</option>
									<?php
									foreach ($client_services as $clse_key => $clse_value) {
									?>
										<option value="<?php echo trim($clse_value['service_name']); ?>"><?php echo trim($clse_value['service_name']); ?></option>
									<?php } ?>
								</select>
							</div>
							<span class="services_type_errormsg error_msg" style="color: red;"></span>
						</div>
						<div class="form-group col-sm-6">
							<label>Date</label>
							<input type="text" class="datepicker" name="date" id="call_date" value="<?php echo (new DateTime())->format('d-m-Y'); ?>">
						</div>

						<input type="hidden" name="leads_notes_section" value="notes">
						<input type="hidden" name="leads_notes_id" value="">
						<input type="hidden" name="user_id" value="<?php echo $client_id; ?>">
						<div class="form-group form-group-save">
							<button class="btn btn-success pull-right" id="add_note_rec_submit" type="submit">Save</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
<!-- Leads Notes Modal ends -->

<div class="leads_detailed_tab_profile_popup">
	<div id="update_lead_<?php echo $leads['id']; ?>" class="modal fade leads_popup ui-front" role="dialog">
		<div class="modal-dialog modal-dialog-edit-lead">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header modal-header-edit-lead">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Edit Lead</h4>
				</div>
				<div class="modal-body modal-body-edit-lead">
					<div id="new_lead" class="lead-summary2 floating_set new-leadss">
						<form id="edit_leads_form" name="edit_leads_form" method="post" class="edit_leads_form">
							<input type="hidden" name="kanpan" id="kanpan" value="0">





							<div class="space-equal-data col-sm-6">
								<div class="lead-popupsection1 floating_set">
									<div class="formgroup col-xs-12 col-sm-6">
										<label class="col-form-label">Company</label>
										<?php if (is_numeric($leads['company'])) {
											$company_name = $this->Common_mdl->get_field_value('client', 'crm_company_name', 'id', $leads['company']);
										?>
											<input type="hidden" name="newcompany_name" id="newcompany_name" value="<?php echo $leads['company']; ?>">
										<?php
										} else {
											$company_name = $leads['company'];
										} ?>
										<div class="edit-field-popup1">
											<input type="text" name="company" class="autocomplete_client" id="company" value="<?php echo $company_name; ?>">
											<div class="company_error"></div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-3  lead-form1 ">
										<label>Lead status</label>
										<div class="dropdown-sin-1">
											<select name="lead_status" id="lead_status" placeholder="Nothing Selected">
												<!-- <option value="">Nothing selected</option> -->
												<?php foreach ($leads_status as $ls_value) { ?>
													<option value='<?php echo $ls_value['id']; ?>' <?php if ($leads['lead_status'] == $ls_value['id']) {
																										echo "selected='selected'";
																									} ?>><?php echo $ls_value['status_name']; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
									<div class="formgroup col-xs-12 col-sm-6">
										<label class="">Name</label>
										<div class="">
											<input type="text" name="name" id="name" value="<?php echo $leads['name']; ?>">
										</div>
									</div>
									<div class="formgroup col-xs-12 col-sm-6">
										<label class=" col-form-label">Position</label>
										<div class=" edit-field-popup1">
											<input type="text" name="position" id="position" value="<?php echo $leads['position']; ?>">
										</div>
									</div>
									<div class="formgroup col-xs-12 col-sm-6">
										<label class=" col-form-label">Email Address</label>
										<div class=" edit-field-popup1">
											<input type="text" name="email_address" id="email_address" value="<?php echo $leads['email_address']; ?>">
										</div>
									</div>
									<div class="formgroup col-xs-12 col-sm-6 con-pho">
										<label class=" col-form-label">Phone</label>
										<div class="col-sm-12 hol">
											<?php
											if (
												isset($leads['phone']) && ($leads['phone'] != '') &&
												(strpos($leads['phone'], '-') !== false)
											) {
												$ctry = explode('-', $leads['phone']);
											} else {
												$ctry[0] = "";
												$ctry[1] = $leads['phone'];
											}
											?>
											<div class="dropdown-sin-2">
												<select name="country_code" id="country_code" class="country_code" placeholder="Nothing Selected">
													<option value=''>Nothing selected</option>
													<?php foreach ($countries as $key => $lsource_value) { ?>
														<option value='<?php echo $lsource_value['phonecode']; ?>' <?php if ($ctry[0] == $lsource_value['phonecode']) {
																														echo "selected='selected'";
																													} ?>><?php echo $lsource_value['sortname'] . '-' . $lsource_value['phonecode']; ?></option>
													<?php } ?>

												</select>
											</div>
											<input type="text" name="phone" id="phone" class="telephone_number" value="<?php if (isset($ctry[1]) && $ctry[1] != '') {
																															echo $ctry[1];
																														} ?>">
										</div>
									</div>

									<div class="col-xs-12 col-sm-3  lead-form1 addtree">
										<label>Assigned</label>
										<input type="text" class="tree_select" id="tree_select1" name="assignees[]" placeholder="Select">
										<input type="hidden" id="assign_role" name="assign_role">
									</div>
									<div class="col-xs-12 col-sm-3  lead-form1 ">
										<label>Source</label>
										<div class="dropdown-sin-2">
											<select name="source[]" id="source" class="<?php echo $leads['source']; ?>" placeholder="Nothing Selected" multiple="multiple">
												<?php foreach ($source as $lsource_value) { ?>
													<option value='<?php echo $lsource_value['id']; ?>' <?php if (in_array($lsource_value['id'], explode(',', $leads['source']))) {
																											echo "selected='selected'";
																										} ?>><?php echo $lsource_value['source_name']; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
									<div class="formgroup col-xs-12 col-sm-6">
										<label class=" col-form-label">Country</label>
										<div class=" edit-field-popup1">
											<select name="country" id="country">
												<option value=''>Nothing Selected</option>
												<?php
												$country = $leads['country'];
												if (is_numeric($country)) {
													foreach ($countries as $c_key => $c_value) { ?>
														<option value="<?php echo $c_value['id'] ?>" <?php if ($leads['country'] == $c_value['id']) {
																											echo "selected='selected'";
																										} ?>><?php echo $c_value['name']; ?></option>
													<?php
													}
												} else {
													$country_name = $this->Common_mdl->select_record('countries', 'name', $country);
													foreach ($countries as $c_key => $c_value) { ?>
														<option value="<?php echo $c_value['id'] ?>" <?php if ($country_name['id'] == $c_value['id']) {
																											echo "selected='selected'";
																										} ?>><?php echo $c_value['name']; ?></option>
												<?php
													}
												}
												?>
											</select>
										</div>
									</div>
									<!-- <div class="formgroup col-xs-12 col-sm-6">
										<label class=" col-form-label">State</label>
										<div class=" edit-field-popup1">
											<input type="text" name="state" id="state" value="<?php echo $leads['state']; ?>">
										</div>
									</div> -->
									<div class="formgroup col-xs-12 col-sm-6 clerbot">
										<label class=" col-form-label">City</label>
										<div class=" edit-field-popup1">
											<input type="text" name="city" id="city" value="<?php echo $leads['city']; ?>">
										</div>
									</div>
									<div class="formgroup col-xs-12 col-sm-6">
										<label class=" col-form-label">Zip Code</label>
										<div class=" edit-field-popup1">
											<input type="text" name="zip_code" id="zip_code" value="<?php echo $leads['zip_code']; ?>">
										</div>
									</div>
									<div class="formgroup col-xs-12 col-sm-6">
										<label class=" col-form-label">Website</label>
										<div class=" edit-field-popup1">
											<input type="text" name="website" id="website" value="<?php echo $leads['website']; ?>">
										</div>
									</div>
									<div class="formgroup col-xs-12 col-sm-6">
										<label class=" col-form-label">Address</label>
										<div class="edit-field-popup1">
											<textarea rows="1" name="address" id="address"><?php echo $leads['address']; ?></textarea>
										</div>
									</div>
									
								</div>
							</div>

							<div class="space-equal-data col-sm-6 secondone">
								<div class="lead-popupsection2 floating_set">
									<!-- <div class="formgroup col-xs-12 col-sm-6">
							<label class=" col-form-label">Default Language</label>
							<div class=" edit-field-popup1">
								<select name="default_language" id="default_language">
									<option value="">System Default</option>
									<?php foreach ($Language as $l_key => $l_value) { ?>
									<option value="<?php echo $l_value['id']; ?>" <?php if ($l_value['id'] == $leads['default_language']) {
																						echo "selected='selected'";
																					} ?>><?php echo $l_value['name']; ?></option>
									<?php } ?>
								</select>
							</div>
						</div> -->
									<div class="formgroup col-xs-12 col-sm-12">
										<label class=" col-form-label">Description</label>
										<div class=" edit-field-popup1">
											<textarea rows="3" name="description" id="leads_edit_description"><?php echo $leads['description']; ?></textarea>
										</div>
									</div>
									<div class="tags-allow1 floating_set">
										<h4><i class="fa fa-tags fa-6" aria-hidden="true"></i> Tags</h4>
										<input type="text" value="<?php echo $leads['tags']; ?>" name="tags" id="tags" class="tags" />
									</div>
									<div class="formgroup checkbox-select1 col-sm-12 border-checkbox-section">
										<div class="data_rights1" style="display: none">
											<label class="custom_checkbox1"> <input type="checkbox" name="public" id="public" <?php if ($leads['public'] == 'on') {
																																	echo 'checked';
																																} ?> value="on">
											</label> <span>Public</span>
										</div>
										<div class="data_rights1">
											<label class="custom_checkbox1"> <input type="checkbox" name="contact_today" <?php if ($leads['contact_today'] == 'on') {
																																echo 'checked';
																															} ?> id="contact_update_today" class="contact_update_today" data-id="<?php echo $leads['id']; ?>" value="on"> </label>
											<span>Contacted Today</span>
										</div>
									</div>
									<?php //echo $leads['contact_today']."test test"; 
									?>
									<div class="dedicated-sup col-sm-12 selectDate_update" <?php if ($leads['contact_today'] == 'on') { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php  } ?> id="selectDate_update_<?php echo $leads['id']; ?>">
										<label><i class="fa fa-pencil fa-6" aria-hidden="true"></i> Choose contact Date</label>
										<input type="text" name="contact_update_date" id="contact_update_date" class="dob_picker " value="<?php echo $leads['contact_date']; ?>" placeholder="Select your date" />
									</div>
								</div>
							</div>



							<?php
							if ($leads['custom_fields'] != '') {
							?>
								<div class="space-equal-data col-sm-12 secondone">
									<h3>Custom Fields</h3>
									<div class="lead-popupsection2 floating_set">
										<?php $qry_data = $this->db->query("select * from web_lead_template where id=" . $leads['form_id'])->result_array();




										$form_data = json_decode($qry_data[0]['form_fields']);


										$custom_fields = $leads['custom_fields'];
										$de_custom = json_decode($custom_fields, true);


										$it_val = '';
										$it_label = '';
										foreach ($de_custom as $key => $value) {
											//echo $key."<br>2";
											foreach ($form_data as $fdkey => $fdvalue) {
												//  echo $fdvalue->label."<br>1";
												$value_name = (isset($fdvalue->name)) ? $fdvalue->name : '';

												if ($value_name == $key) {
													//echo $value_name.'<br>';
													$it_val = $fdvalue->type;

													$it_label = $fdvalue->label;

													//  if($value_name=='select')
													// {


													//  $option_value=json_decode(json_encode($fdvalue->values), true);

													// }
													$ex_val = explode("-", $value_name);

													if (isset($ex_val[2]) || isset($ex_val[1])) {
														$it_val1 = $fdvalue->name;
														$val1 = (isset($ex_val[2])) ? $ex_val[2] : '';
														if ($value_name == 'radio-group-' . $val1 || $value_name == 'select-' . $ex_val[1]) {
															// echo 'hai';

															$option_value = json_decode(json_encode($fdvalue->values), true);
															$selected = $value;
														}

														// print_r();
														// echo '<br>';
														// exit;
													}
												}
											}
											//isset($option_value) print_r(expression)


											if ($it_val == 'text')
											// if($it_val=='text_field' || $it_val=='email')
											{
										?>

												<div class="formgroup col-xs-12 col-sm-6">
													<label class=" col-form-label"><?php echo $it_label; ?></label>
													<div class=" edit-field-popup1">
														<input type="text" name="<?php echo $key; ?>" id="<?php echo $key; ?>" value="<?php echo $value; ?>">
													</div>
												</div>

												<?php
											}

											if (isset($ex_val[1])) {
												if ($it_val1 == 'select-' . $ex_val[1])
												// if($it_val=='text_field' || $it_val=='email')
												{
													// print_r($option_value);                
												?>

													<div class="formgroup col-xs-12 col-sm-6">
														<label class=" col-form-label"><?php echo $it_label; ?></label>
														<div class=" edit-field-popup1">
															<!--  <input type="text" name="<?php echo $key; ?>" id="<?php echo $key; ?>" value="<?php echo $value; ?>"> -->
															<select name="<?php echo $it_val1; ?>" id="<?php echo $key; ?>">
																<?php foreach ($option_value as $key => $value) { ?>

																	<option value="<?php echo $value['value'] ?>" <?php echo (isset($selected) && $selected == $value['value']) ? 'selected' : ''; ?>><?php echo $value['label'] ?></option>
																<?php } ?>
															</select>
														</div>
													</div>

												<?php
												}
											}
											// print_r($ex_val[2]);
											if (isset($ex_val[2])) {

												//  echo "check";


												if ($it_val1 == 'radio-group-' . $ex_val[2])
												// if($it_val=='text_field' || $it_val=='email')
												{
													// print_r($option_value);                
												?>

													<div class="formgroup col-xs-12 col-sm-6">
														<label class=" col-form-label"><?php echo $it_label; ?></label>
														<div class=" edit-field-popup1">

															<?php foreach ($option_value as $key => $value) { ?>


																<input type="radio" name="<?php echo $it_val1; ?>" id="<?php echo $key; ?>" value="<?php echo $value['value'] ?>" <?php echo (isset($selected) && $selected == $value['value']) ? 'checked' : ''; ?>><?php echo $value['label'] ?><br>
															<?php } ?>

														</div>
													</div>

												<?php
												}
											}

											if ($it_val == 'textarea')
											//if($it_val=='text_area')
											{
												?>
												<div class="formgroup col-xs-12 col-sm-6">
													<label class=" col-form-label"><?php echo $it_label; ?></label>
													<div class=" edit-field-popup1">
														<textarea rows="3" name="<?php echo $key; ?>" id="<?php echo $key; ?>"><?php echo $value; ?></textarea>
													</div>
												</div>

											<?php
											}

											?>

										<?php


										}
										$data = array();
										?>
									</div>
								</div>

							<?php } ?>							
						</form>
					</div>
				</div>
				<div class="modal-footer modal-footer-edit-lead">
					<input type="hidden" id="for_deatiled_page" name="for_deatiled_page" value="<?php echo $leads['id']; ?>">
					<input type="submit" name="save" id="save" value="Update">
					<a href="#" data-dismiss="modal">Close</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end of leads -->
<input type="hidden" id="leads_id" name="leads_id" value="<?php echo $leads['id']; ?>">

<div class="pcoded-content">
	<div class="pcoded-inner-content">

		<div class="" id="profile_lead_<?php echo $leads['id']; ?>" role="dialog">
			<div class="">
				<div class="realign-quote01 floating_set">
					<div class="leads-section floating_set">
						<div class="deadline-crm1  floating_set ">
							<div class="pull-left">
								<ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard client-top-nav">
									<li class="nav-item">
										<a class="nav-link active hide_activity" data-toggle="tab" href="#profile">Profile</a>
									</li>
									<li class="nav-item it1" value="1"><a class="nav-link hide_activity" data-toggle="tab" href="#proposal_<?php echo $leads['id']; ?>">Proposal</a></li>
									<li class="nav-item"><a class="nav-link hide_activity" data-toggle="tab" href="#tasks_<?php echo $leads['id']; ?>">Tasks</a></li>
									<li class="nav-item "><a class="nav-link hide_activity" data-toggle="tab" href="#attachments_<?php echo $leads['id']; ?>">Attachments</a></li>
									<li class="nav-item  "><a class="nav-link hide_activity" data-toggle="tab" href="#reminder_<?php echo $leads['id']; ?>">Reminders</a></li>
									<li class="nav-item  "><a class="nav-link hide_activity" data-toggle="tab" href="#lead_notes">Notes & Activity Log</a></li>
									<!--   <li class="nav-item "><a class="nav-link" data-toggle="tab" href="#activity-log">Activity Log</a></li> -->
								</ul>
							</div>
						</div>
						<div class="proposal-sent-sample floating_set">
							<div class="management_section floating_set ">
								<div class="all_user-section2 floating_set profile-edit-crm">
									<div class="tab-content lead_details">
										<div id="profile" class="lead-profileG tab-pane fade in active">
											<div class="edit-section">
												<div class="mark-lost1 floating_set">
													<div class="pull-left edit-pull-left for_leads_detailed_right">
														<ul <?php if ($_SESSION['permission']['Leads']['edit'] != 1) { ?> style="display: none;" <?php } ?>>
															<li><a class="btn btn-primary" href="#" data-toggle="modal" data-target="#update_lead_<?php echo $leads['id']; ?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i>Edit
																</a>
															</li>
															<li class="cmn-pro-menu">
																<a href="#" class="btn btn-primary edit-toggle1">More <span class="caret"></span></a>
																<ul class="proposal-down cmn-dropdown-crm">
																	<!--  <li><a href="#" id="lost" data-id="<?php echo $leads['id']; ?>" class="statuschange">Mark as lost</a></li>
													 <li><a href="#" id="junk" data-id="<?php echo $leads['id']; ?>" class="statuschange"> Mark as junk</a></li> -->
																	<?php foreach ($leads_status as $ls_value) { ?>
																		<li><a href="#" id="<?php echo $leads['id']; ?>" data-id="<?php echo $ls_value['id']; ?>" class="convert_to_status"><?php echo $ls_value['status_name']; ?></a></li>
																	<?php } ?>
																	<!-- <li><a href="<?php echo base_url() . 'leads/delete/' . $leads['id'] . '/0'; ?>" onclick="return delete_leads('Are you sure want to delete');" data-id="<?php echo $leads['id'] ?>"> Delete Lead</a></li> -->

																	<!--  <li><a href="#" data-target="#deleteconfirmation" data-toggle="modal" onclick="return delete_data(this,'leads_details_delete');" data-id="<?php echo $leads['id'] ?>"> Delete Lead</a></li> -->

																</ul>
															</li>
															<!-- <li>
																<a href="<?php echo base_url('proposal_page/step_proposal?lead_id=' . $leads['id'] . ''); ?>" id="convert_to_customer" name="convert_to_customer" data-id="<?php echo $leads['id']; ?>" data-status="<?php echo $status_name['status_name']; ?>" class="convert_to_customer btn btn-card btn-primary" <?php if (strtolower($status_name['status_name']) == "customer" || strtolower($status_name['status_name']) == "win") { ?> style="display: none !important;" <?php } ?>>Convert To Customer</a>
															</li> -->
															<?php if (strtolower($status_name['status_name']) != "customer" && strtolower($status_name['status_name']) != "win"): ?>
															<li>
																<a href="<?php echo base_url('Leads/convert_to_customer?lead_id=' . $leads['id'] . ''); ?>" class="btn btn-card btn-primary">Convert to Customer</a>
															</li>
															<?php endif; ?>
														</ul>
													</div>
													<!-- <div class="pull-right convert-custom">
											<a href="#"> Convert to customer </a>
											</div> -->
												</div>
											</div>
											<div class="floating_set general-inform1 leads-t-details">
												<div class="col-xs-12 col-md-6 leads-seperateM">
													<div class="box-newalign1">
														<div class="for-block">
															<h4>General Information</h4>
														</div>
														<div class="alert alert-success succ" style="display:none;">
															<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
														</div>
														<div class="col-xs-12 bot-sec">
															<div class="col-sm-6 custom-fields11">
																<div class="custom-danger">
																	<!-- <h4>Information</h4> -->
																	<?php
																	if (!empty($status_name['status_name'])) {
																	?>
																		<div class="form-status1">
																			<span>Lead Status</span>
																			<strong class="leadstatus lead_status_<?php echo $leads['id']; ?>"><?php echo $status_name['status_name']; ?></strong>
																		</div>
																	<?php
																	}
																	?>
																	<?php
																	if (!empty($leads['position'])) {
																	?>
																		<div class="form-status1">
																			<span>Position</span>
																			<strong><?php echo $leads['position']; ?></strong>
																		</div>
																	<?php
																	}
																	?>
																	<?php
																	if (!empty($leads['email_address'])) {
																	?>
																		<div class="form-status1">
																			<span>Email Address</span>
																			<strong><a href="#"><?php echo $leads['email_address']; ?></a></strong>
																		</div>
																	<?php
																	}
																	?>
																	<?php
																	if (!empty($leads['website'])) {
																	?>
																		<div class="form-status1">
																			<span>Website</span>
																			<strong><a href="#"><?php echo $leads['website']; ?></a></strong>
																		</div>
																	<?php
																	}
																	?>
																	<?php
																	if (!empty($leads['phone'])) {
																	?>
																		<div class="form-status1">
																			<span>Phone</span>
																			<strong><a href="#"><?php echo $leads['phone']; ?></a></strong>
																		</div>
																	<?php
																	}
																	?>

																	<div class="form-status1">
																		<span>Company</span>
																		<strong><?php //echo $leads['company'];
																				if (is_numeric($leads['company'])) {
																					$query = $this->db->query("SELECT * FROM client WHERE autosave_status=0 and id=" . $leads['company'] . " order by id desc ");

																					$results = $query->result_array();
																					if (count($results) > 0) {
																						echo $results[0]['crm_company_name'];
																					}
																				} else {
																					echo $leads['company'];
																				}

																				?></strong>
																	</div>
																	<?php
																	if (!empty($leads['address'])) {
																	?>
																		<div class="form-status1">
																			<span>Address</span>
																			<strong><?php echo $leads['address']; ?></strong>
																		</div>
																	<?php
																	}
																	?>
																	<!-- <?php
																	if (!empty($leads['city'])) {
																	?>
																		<div class="form-status1">
																			<span>City</span>
																			<strong><?php echo $leads['city']; ?></strong>
																		</div>
																	<?php
																	}
																	?> -->
																	<?php
																	if (!empty($leads['state'])) {
																	?>
																		<div class="form-status1">
																			<span>State</span>
																			<strong><?php echo $leads['state']; ?></strong>
																		</div>
																	<?php
																	}
																	?>
																	<!-- <?php
																	if (!empty($country_name['name'])) {
																	?>
																		<div class="form-status1">
																			<span>Country</span>
																			<strong><?php echo $country_name['name']; ?></strong>
																		</div>
																	<?php
																	}
																	?> -->
																	<!-- <?php
																	if (!empty($leads['zip_code'])) {
																	?>
																		<div class="form-status1">
																			<span>Zip code</span>
																			<strong><?php echo $leads['zip_code']; ?></strong>
																		</div>
																	<?php
																	}
																	?> -->

																</div>
															</div>
															<div class="col-sm-6 custom-fields11 general-leads1">
																<div class="custom-danger">
																	<!-- <h4>General Information</h4> -->
																	<!-- <div class="form-status1">
													 <span>Lead Status</span>
													 <strong>New</strong>
													 </div>   -->
																	<div class="form-status1">
																		<span>Source</span>
																		<strong><?php /*if($leads['source']==1)
														{
														  $leads['source'] = 'Google';
														}elseif($leads['source']==2)
														{
														  $leads['source'] = 'Facebook';
														}else{
														  $leads['source'] = '';
														} */
																				$leads_source = array();
																				if ($leads['source'] != '') {
																					foreach (explode(',', $leads['source']) as $source_key => $source_value) {
																						$source_name = $this->Common_mdl->GetAllWithWhere('source', 'id', $source_value);
																						if (!empty($source_name)) {
																							$leads_res = $source_name[0]['source_name'];
																							if ($leads_res != '') {
																								$leads_source[] = $leads_res;
																							}
																						}
																					}
																				}
																				if (count($leads_source) > 0) {
																					echo implode(',', $leads_source);
																				} else {
																					echo "-";
																				}


																				// echo $leads['source'];
																				?></strong>
																	</div>
																	<?php
																	if (!empty($languages['name'])) {
																	?>
																		<div class="form-status1">
																			<span>Default Language</span>
																			<strong><?php echo $languages['name']; ?></strong>
																		</div>
																	<?php
																	}
																	?>
																	<div class="form-status1">
																		<span>Assigned</span>
																		<strong>

																			<?php
																			$task_sql = $this->db->query("select * from add_new_task where  lead_id=" . $leads['id'] . "")->result_array();
																			$ex_val = $this->Common_mdl->get_module_user_data('LEADS', $leads['id']);
																			$var_array = array();
																			if (count($ex_val) > 0) {
																				foreach ($ex_val as $key => $value) {

																					$user_name = $this->Common_mdl->select_record('user', 'id', $value);
																					array_push($var_array, $user_name['crm_name']);
																				}
																			}

																			echo implode(",", $var_array);

																			?></strong>
																	</div>
																	<?php
																	if (!empty($leads['tags'])) {
																	?>
																		<div class="form-status1">
																			<span>Tags</span>
																			<strong>
																				<?php $tag = explode(',', $leads['tags']);
																				foreach ($tag as $t_key => $t_value) { ?>
																					<a href="#"><?php echo $t_value; ?></a>
																				<?php } ?>
																			</strong>
																		</div>
																	<?php
																	}
																	?>

																	<div class="form-status1">
																		<span>Created</span>
																		<strong><?php $date = date('Y-m-d H:i:s', $leads['createdTime']);
																				echo time_elapsed_string($date); ?></strong>
																	</div>
																	<div class="form-status1">
																		<span>Last Contact</span>
																		<strong>-</strong>
																	</div>
																	<div class="form-status1">
																		<span>Public</span>
																		<strong><?php if ($leads['public'] != '') {
																					echo $leads['public'];
																				} else {
																					echo 'No';
																				} ?></strong>
																	</div>
																	<div class="form-status1">
																		<?php
																		if (!empty($country_name['name'])) {
																		?>
																				<span>Country</span>
																				<strong><?php echo $country_name['name']; ?></strong>
																				<?php
																		}
																		?>
																	</div>
																	<div class="form-status1">
																		<?php
																		if (!empty($leads['city'])) {
																		?>
																				<span>City</span>
																				<strong><?php echo $leads['city']; ?></strong>
																				<?php
																		}
																		?>
																	</div>
																	<div class="form-status1">
																		<?php
																		if (!empty($leads['zip_code'])) {
																		?>
																				<span>Zip code</span>
																				<strong><?php echo $leads['zip_code']; ?></strong>
																				<?php
																		}
																		?>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<!-- rspt 18-04-2018 convert to customer -->
												<!-- 18-04-2018 end -->
												<!-- <div class="col-sm-4 custom-fields11">
										 <div class="custom-danger">
										   <h4>Custom Fields</h4>
										   <div class="form-status1">
											 <span>Dedicated support</span>
											 <strong>-</strong>
										   </div>  
										 </div>
										 </div> -->
												<div class="col-xs-12 col-md-6 leads-seperateM">
													<div class="description-lead1 col-sm-12">
														<?php
														if (!empty($leads['description'])) {
														?>
															<div class="box-newalign1">
																<div class="for-block">
																	<h4>Description</h4>
																</div>
																<div class="bot-sec">
																	<p><?php echo $leads['description']; ?> </p>
																</div>
															</div>
														<?php
														}
														?>

													</div>

													<div class="activity1 col-sm-12">
														<div class="box-newalign1">
															<div class="for-block">
																<h4>Latest Activity</h4>
															</div>

															<!-- <span>Gust Toy - created lead</span> -->
															<!-- <span><?php $date = date('Y-m-d H:i:s', $latestrec[0]['CreatedTime']);
																		echo time_elapsed_string($date); ?></span>
											   <span><?php
														$l_id = $latestrec[0]['module_id'];
														$l_name = $this->Common_mdl->select_record('leads', 'id', $l_id);

														echo $l_name['name'] . ' - ' . $latestrec[0]['log']; ?></span> -->
															<?php



															// print_r($latestrecs);
															//  exit;
															?>
															<div class="bot-sec">

																<!-- static activity -->



																<div class="main-timeline">
																	<div class="cd-timeline cd-container">
																		<?php



																		$latestrecs1 = $this->db->query("
								   SELECT * FROM activity_log where module='Lead' and module_id='" . $leads['id'] . "' ORDER BY `id`  DESC ")->result_array();

																		//   print_r($latestrecs1);
																		// exit;

																		$i = 1;
																		if (count($latestrecs1) > 0) {
																			foreach ($latestrecs1 as $late_key => $late_value) {
																				$des = '';
																				//$date=date('d F Y h:i A',$late_value['CreatedTime']);
																				$l_id = $late_value['module_id'];
																				$date = date('d F Y h:i A', $late_value['CreatedTime']);
																				if ($late_value['sub_module'] == '') {
																					$sub_module = 'Lead';
																					$sub_log = $late_value['log'];
																					$l_name = $this->Common_mdl->select_record('leads', 'id', $l_id);
																					if ($late_value['log'] == 'New Lead Created') {
																						$des = "New Lead " . $l_name['name'] . " is Created";
																		?>

																						<div class="cd-timeline-block">
																							<div class="cd-timeline-icon bg-primary">
																								<i class="icofont icofont-tasks-alt"></i>
																							</div>
																							<div class="cd-timeline-content card_main">

																								<div class="p-20">
																									<h6><?php echo (isset($des)) ? $des : '' . "   ";; ?></h6>
																								</div>
																								<div class="leads-logs-wrapper">
																									<span class="cd-date"><?php echo (isset($date)) ? $date : '' . "   "; ?></span>
																									<span class="cd-details">Your Activity Info</span>
																								</div>
																							</div>
																						</div>

																					<?php }
																					if ($late_value['log'] == 'Lead Records Updated') {
																						$des = "New Lead " . $l_name['name'] . " is Updated";
																					?>

																						<div class="cd-timeline-block">
																							<div class="cd-timeline-icon bg-primary">
																								<i class="icofont icofont-tasks-alt"></i>
																							</div>
																							<div class="cd-timeline-content card_main">

																								<div class="p-20">
																									<h6><?php echo (isset($des)) ? $des : '' . "   ";; ?></h6>
																								</div>
																								<div class="leads-logs-wrapper">
																									<span class="cd-date"><?php echo (isset($date)) ? $date : '' . "   "; ?></span>
																									<span class="cd-details">Your Activity Info</span>
																								</div>
																							</div>
																						</div>

																					<?php }
																					if ($late_value['log'] == 'Lead Imported') {
																						$des = 'New Lead is Imported';
																					?>

																						<div class="cd-timeline-block">
																							<div class="cd-timeline-icon bg-primary">
																								<i class="icofont icofont-tasks-alt"></i>
																							</div>
																							<div class="cd-timeline-content card_main">

																								<div class="p-20">
																									<h6><?php echo (isset($des)) ? $des : '' . "   ";; ?></h6>
																								</div>
																								<div class="leads-logs-wrapper">
																									<span class="cd-date"><?php echo (isset($date)) ? $date : '' . "   "; ?></span>
																									<span class="cd-details">Your Activity Info</span>
																								</div>
																							</div>
																						</div>

																					<?php }
																				} else {
																					$sub_module = $late_value['sub_module'];
																					if ($late_value['sub_module'] == 'Lead Attachment') {
																						$ex_val = explode('---', $late_value['log']);
																						$log_activity = $ex_val[0];
																						$log_img = $ex_val[1];
																						$ext_val = explode('.', $log_img);
																						$ext = end($ext_val);
																						$img_array = array("jpeg", "JPEG", "jpg", "JPG", "png", "PNG", "gif", "GIF");
																						if (in_array($ext, $img_array)) {
																							$img_res = 'yes';
																							$img_name = "";
																						} else {
																							$img_res = 'not';
																							$img_name = $log_img;
																						}
																						if ($log_activity == 'Lead Attached') {
																							$des = "You Have Attached " . $img_name . " File  ";
																						} else {
																							$des = "You have Deleted " . $img_name . " File  ";
																						}
																					?>

																						<div class="cd-timeline-block">
																							<div class="cd-timeline-icon bg-primary">
																								<i class="icofont icofont-tasks-alt"></i>
																							</div>
																							<div class="cd-timeline-content card_main">

																								<div class="p-20">
																									<h6><?php echo (isset($des)) ? $des : '' . "   ";; ?></h6>
																								</div>
																								<div class="leads-logs-wrapper">
																									<span class="cd-date"><?php echo (isset($date)) ? $date : '' . "   "; ?></span>
																									<span class="cd-details">Your Activity Info</span>
																								</div>
																							</div>
																						</div>

																					<?php }
																					if ($late_value['sub_module'] == 'Lead Notes') {
																						$ex_val = explode("---", $late_value['log']);
																						$sub_id = $late_value['sub_module_id'];
																						$note_val =  (isset($ex_val[1])) ? $ex_val[1] : '';
																					?>

																						<?php if ($ex_val[0] == 'Lead Notes Added') {

																							$des = "Notes " . $note_val . " is Created";
																						}
																						if ($ex_val[0] == 'Lead Notes Updated') {

																							$des = "Notes " . $note_val . " is Updated";
																						}

																						if ($ex_val[0] == 'Lead Notes Delete') {
																							$des = "Notes " . $note_val . " is Deleted";
																						}

																						?>

																						<?php

																						?>

																						<div class="cd-timeline-block">
																							<div class="cd-timeline-icon bg-primary">
																								<i class="icofont icofont-tasks-alt"></i>
																							</div>
																							<div class="cd-timeline-content card_main">

																								<div class="p-20">
																									<h6><?php echo (isset($des)) ? $des : '' . "   ";; ?></h6>
																								</div>
																								<div class="leads-logs-wrapper">
																									<span class="cd-date"><?php echo (isset($date)) ? $date : '' . "   "; ?></span>
																									<span class="cd-details">Your Activity Info</span>
																								</div>
																							</div>
																						</div>
																					<?php }
																					if ($late_value['sub_module'] == 'Reminder') {
																						$reminder_data = $this->Common_mdl->select_record('lead_reminder', 'id', $late_value['sub_module_id']);
																						$reminder_res = $reminder_data['assigned_staff_id'];
																						$user_data = $this->Common_mdl->select_record('user', 'id', $reminder_res);
																						if ($reminder_res != '') {
																							$res_reminder = "yes";
																						} else {
																							$res_reminder = "no";
																						}
																						$ex_val = explode('---', $late_value['log']);
																						$user_data = $this->Common_mdl->select_record('user', 'id', isset($ex_val[1]));

																						if ($ex_val[0] == 'Reminder Imported') {
																							$des = "You Have Assigned a Reminder To  " . $user_data['crm_name'] . "   ";
																						} elseif ($ex_val[0] == 'Reminder Updated') {
																							$des = "You Have Updated Assigned a Reminder To " . $user_data['crm_name'] . "   ";
																						} else {

																							$des = "You have Deleted Assigned a Reminder of " . $user_data['crm_name'] . "  ";
																						}

																					?>

																						<div class="cd-timeline-block">
																							<div class="cd-timeline-icon bg-primary">
																								<i class="icofont icofont-tasks-alt"></i>
																							</div>
																							<div class="cd-timeline-content card_main">

																								<div class="p-20">
																									<h6><?php echo (isset($des)) ? $des : '' . "   ";; ?></h6>
																								</div>
																								<div class="leads-logs-wrapper">
																									<span class="cd-date"><?php echo (isset($date)) ? $date : '' . "   "; ?></span>
																									<span class="cd-details">Your Activity Info</span>
																								</div>
																							</div>
																						</div>

																					<?php }

																					if ($late_value['sub_module'] == 'Task') {
																						$task_data = $this->Common_mdl->select_record('add_new_task', 'id', $late_value['sub_module_id']);
																						$log_img = $task_data['attach_file'];
																						if ($log_img != '') {
																							$ext_val = explode('.', $log_img);
																							$ext = end($ext_val);
																							$img_array = array("jpeg", "JPEG", "jpg", "JPG", "png", "PNG", "gif", "GIF");
																							if (in_array($ext, $img_array)) {
																								$img_res = 'yes';
																								$img_name = "";
																							} else {
																								$img_res = 'not';
																								$img_name = $log_img;
																							}
																						} else {
																							$img_res = "no";
																						}
																						$ex_val = explode("---", $late_value['log']);
																						if ($ex_val[0] == 'Task Created') {
																							$des = "You Have Created " . $ex_val[1] . " ";
																						} elseif ($ex_val[0] == 'Task Records Updated') {
																							$des = "You Have Updated " . $ex_val[1] . "   ";
																						} else {
																							$ex_val = explode('---', $late_value['log']);

																							$des = "You have Deleted " . $ex_val[1] . "   ";
																						}

																					?>

																						<div class="cd-timeline-block">
																							<div class="cd-timeline-icon bg-primary">
																								<i class="icofont icofont-tasks-alt"></i>
																							</div>
																							<div class="cd-timeline-content card_main">

																								<div class="p-20">
																									<h6>
																										<?php

																										//    exit;
																										echo (isset($des)) ? $des : '' . "   ";; ?></h6>
																								</div>
																								<div class="leads-logs-wrapper">
																									<span class="cd-date"><?php echo (isset($date)) ? $date : '' . "   "; ?></span>
																									<span class="cd-details">Your Activity Info</span>
																								</div>
																							</div>
																						</div>

																		<?php }
																				}

																				$i++;
																			}
																		}
																		?>

																		<!--            <div class="cd-timeline-block">
																			<div class="cd-timeline-icon bg-warning">
																				<i class="icofont icofont-ui-file"></i>
																			</div>
																			<div class="cd-timeline-content card_main">
																				<div class="p-20">
																					<h6>We’d like to introduce our new website</h6>
																				</div>
																				<span class="cd-date">November 12, 2014, 11:27 AM</span>
																				<span class="cd-details">You  posed an artical with public</span>
																			</div>
																		  </div> -->
																	</div>
																	<!-- cd-timeline -->
																</div>
															</div>
															<!-- static activity -->
														</div>
													</div>
												</div>
											</div>
										</div>
										<!-- profile -->
										<div id="proposal_<?php echo $leads['id']; ?>" class="secone tab-pane fade pt-3rem">
											<div class="new-proposal1 floating_set proposal_section_data" <?php if ($_SESSION['permission']['Leads']['create'] != 1) { ?> style="display: none;" <?php } ?>>

												<button type="button" id="delete_proposal_records" class="delete_proposal_records del-tsk12 f-right" style="display: none;"><i class="icofont icofont-ui-delete" aria-hidden="true"></i>Delete</button>
												<a href="<?php echo base_url(); ?>proposal_page/step_proposal?lead_id=<?php echo $leads['id']; ?>">New proposal</a>

											</div>
											<div class="modal-alertsuccess alert alert-success1" style="display:none;">
												<a href="#" class="close" data-dismiss="alert" aria-label="close">X</a>
												<a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
												<div class="pop-realted1">
													<div class="position-alert1">
														Archieved Successfully
													</div>
												</div>
											</div>
											<div class="proposal-table1 floating_set">
												<table id="send_table" class="display nowrap" style="width:100%">
													<thead>
														<tr class="table-header">
															<!-- <th><div class="checkbox-fade fade-in-primary">
																	<label>
																	<input type="checkbox" id="select_all_proposal">
																	<span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
																	</label>
															</div></th> -->
															<th>proposals</th>
															<th>total cost</th>
															<th>created by</th>
															<th>sent on</th>
															<th>status</th>
														</tr>
													</thead>
													<tfoot class="ex_data1">
														<tr>
															<th>
															</th>
														</tr>
													</tfoot>
													<tbody>
														<?php
														/** rs[t 19-04-2018 **/
														if (count($proposal) > 0) {

															foreach ($proposal as $key => $pro_val) { ?>
																<tr>
																	<!-- <td>
																	<div class="checkbox-fade fade-in-primary">
																		<label>
																		<input type="checkbox" class="proposal_checkbox" data-proposal-id="<?php echo $pro_val['id']; ?>">
																		<span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
																		</label>
																	</div>
																	</td> -->
																	<td>
																		<div class="first-info">
																			<span class="info-num"><?php echo $pro_val['proposal_no'] ?></span>
																			<p> <?php $randomstring = generateRandomString('100'); ?><a href="<?php echo base_url(); ?>proposal_page/step_proposal/<?php echo $randomstring; ?>---<?php echo $pro_val['id']; ?>"><?php echo $pro_val['proposal_name']; ?></a></p>
																			<a href="<?php echo base_url(); ?>proposal_page/copy_proposal/<?php echo $randomstring; ?>---<?php echo $pro_val['id']; ?>" class="edit-option">[edit]</a>
																			<?php if ($pro_val['status'] != "archive") { ?>
																				<a href="javascript:;" id="<?php echo $pro_val['id']; ?>" class="edit-option archived" onclick="Archived(this)">[archive]</a>
																			<?php } ?>
																		</div>
																		<p class="com-name"><a href="<?php echo base_url(); ?>client/client_infomation/<?php echo $pro_val['company_id']; ?>" target="_blank"><?php echo $pro_val['company_name']; ?> </a></p>
																	</td>
																	<td><span class="amount"><i class="fa fa-gbp" aria-hidden="true"></i><?php
																																			$ex_price = explode(',', $pro_val['price']);
																																			$integerIDs = array_sum(array_map('intval', explode(',', $pro_val['price'])));
																																			echo $integerIDs; ?></span></td>
																	<td>
																		<div class="acc-tax">
																			<span class="tax-inner">accotax</span>
																			<p>accountants & tax consultants</p>
																		</div>
																	</td>
																	<td class="send_td">
																		<span class="sent-month">
																			<?php //echo $pro_val['created_at'];
																			$timestamp = strtotime($pro_val['created_at']);
																			$newDate = date('d F Y H:i', $timestamp);
																			echo $newDate; //outputs 02-March-2011
																			?>
																		</span>
																	</td>
																	<td>
																		<i class="fa fa-check" aria-hidden="true"></i>
																		<span class="accept"><?php
																								$status = $pro_val['status'];
																								if ($status == 'sent') {
																									$res = 'sent';
																								}
																								if ($status == 'opened') {
																									$res = 'viewed';
																								} else {
																									$res = $status;
																								}
																								echo ucfirst($res);
																								?></span>
																		<div class="modal-alertsuccess alert alert-success" id="delete_all_proposal<?php echo $pro_val['id']; ?>" style="display:none;">
																			<div class="newupdate_alert"> <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
																				<div class="pop-realted1">
																					<div class="position-alert1">
																						Are you sure you want to delete <b><a href="#" class="delete_yes"> Yes </a></b> OR <b><a href="#" id="close" class="delete_proposal">No</a></b></div>
																				</div>
																			</div>
																		</div>
																	</td>
																</tr>

															<?php }
														} else {
															?>
															<tr>
																<td colspan="7" class="no-recfound">No Record Found</td>
															</tr>
														<?php
														}
														/** end 19-04-2018 rspt **/ ?>
													</tbody>
												</table>
												<input type="hidden" class="rows_selected" id="select_proposal_count">

											</div>
										</div>
										<!-- 2tab -->
										<div id="tasks_<?php echo $leads['id']; ?>" class="tab-pane fade pt-3rem lead-tasks-wrap">
											<div class="new-proposal1 floating_set leads_section_data">
												<?php //if($_SESSION['id']==$leads['user_id']){ 
												?>
												<a href="#" class="new_task_class" data-toggle="modal" data-target="#addnew_task_lead_<?php echo $leads['id']; ?>" <?php if ($_SESSION['permission']['Leads']['create'] != 1 || strtolower($status_name['status_name']) == 'customer') { ?> style="display: none;" <?php } ?>>New Task</a>
												<?php //} 
												?>
												<!-- <select name="task_export" id="task_export" class="task_export" data-id="<?php echo $leads['id']; ?>">
										 <option value="">EXPORT</option>
										 <option value="csv">CSV</option>
										 <option value="pdf">PDF</option>
										 <option value="html">HTML</option>
									  </select> -->

												<div class="export_option">
													<div class="btn-group e-dash sve-action1 frpropos">
														<button type="button" class="btn btn-default dropdown-toggle emp-dash" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
															Export
														</button>
														<ul class="dropdown-menu" id="task_export" data-id="<?php echo $leads['id']; ?>">

															<!--  <li><a href="#" >Export</a></li> -->

															<li><a href="#" data-val="csv" class="task_export" data-id="<?php echo $leads['id']; ?>">CSV</a></li>

															<li><a href="#" class="task_export" data-val="pdf" data-id="<?php echo $leads['id']; ?>">PDF</a></li>

															<li><a href="#" class="task_export" data-val="html" data-id="<?php echo $leads['id']; ?>">HTML</a></li>

														</ul>
													</div>
												</div>

											</div>
											<div class="proposal-table1 floating_set task_leadss" id="task_leadss_<?php echo $leads['id']; ?>">
												<table id="example1_<?php echo $leads['id']; ?>" class="display data-available-1 example1" cellspacing="0" width="100%">
													<thead>
														<tr>
															<th>Subject</th>
															<th>Start Date</th>
															<th>End Date</th>
															<th>Tag</th>
															<th>Assigned To</th>
															<th>Priority</th>
															<th>Action</th>
														</tr>
													</thead>
													<tfoot class="ex_data1">
														<tr>
															<th>
															</th>
														</tr>
													</tfoot>
													<tbody>
														<?php
														$task_sql = $this->db->query("select * from add_new_task where  lead_id=" . $leads['id'] . "")->result_array();
														$ex_val =  $this->Common_mdl->get_module_user_data('LEADS', $leads['id']);
														$var_array = array();
														if (count($ex_val) > 0) {
															foreach ($ex_val as $key => $value) {

																$user_name = $this->Common_mdl->select_record('user', 'id', $value);
																array_push($var_array, $user_name['crm_name']);
															}
														}

														foreach ($task_sql as $task_sql_key => $task_sql_value) {

														?>
															<tr>
																<td> <?php echo $task_sql_value['subject']; ?></td>
																<td> <?php if ($task_sql_value['start_date'] != '') {
																			echo date('d-m-Y', strtotime($task_sql_value['start_date']));
																		} else {
																			echo "-";
																		}

																		?></td>
																<td> <?php
																		if ($task_sql_value['end_date'] != '') {
																			echo date('d-m-Y', strtotime($task_sql_value['end_date']));
																		} else {
																			echo "-";
																		}

																		?></td>
																<td> <?php echo str_replace(',', ' , ', $task_sql_value['tag']); ?></td>
																<td>
																	<?php echo implode(',', $var_array);
																	?>

																</td>
																<td><?php echo $task_sql_value['priority']; ?></td>
																<td class="task_list_delete_option">

																	<p class="action_01">

																		<a href="#" data-target="#deleteconfirmation" data-toggle="modal" data-id="<?php echo $task_sql_value['id']; ?>" onclick="return delete_data(this,'delete_task');" <?php if ($_SESSION['permission']['Leads']['delete'] != 1) { ?>style="display:none" ; <?php } ?> title="Delete"><i class="icofont icofont-ui-delete deleteUser" aria-hidden="true"></i></a>
																		<a href="#" data-toggle="modal" data-target="#edit_task_lead_<?php echo $task_sql_value['id']; ?>" <?php if ($_SESSION['permission']['Leads']['edit'] != 1) { ?>style="display:none" ; <?php } ?> title="Edit"><i class="icofont icofont-edit" aria-hidden="true"></i></a>
																	</p>

																</td>
															</tr>
														<?php } ?>
													</tbody>
												</table>
											</div>
										</div>
										<!-- 3tab -->
										<div id="attachments_<?php echo $leads['id']; ?>" class="fileupload tab-pane fade">
											<div class="choose-client01">
												<form name="l_attachment<?php echo $leads['id']; ?>" id="l_attachment<?php echo $leads['id']; ?>" method="post" enctype="multipart/form-data">

													<?php // if($_SESSION['id']==$leads['user_id']){ 
													?>
													<div class="form-group attach-files file-attached1 for_attachment_data" <?php if ($_SESSION['permission']['Leads']['create'] != 1) { ?> style="display: none;" <?php } ?>>
														<div id="input_container" class="custom_upload upload_input<?php echo $leads['id']; ?>">
															<input type="file" name="lead_attachments" id="lead_attachments_70" data-id="<?php echo $leads['id']; ?>" class="lead_attachments" multiple="multiple" />
														</div>
														<!-- <div class="button" onclick="uploadss();">attach file</div>-->
													</div>
													<?php //} 
													?>
													<!-- <div class="cloud-upload1">
										 <a href="#"><i class="fa fa-cloud-upload fa-6" aria-hidden="true"></i> Choose from Dropbox</a>
										 </div> -->
													<!-- <input type="hidden" name="l_a_id" id="l_a_id" value="<?php echo $leads['id']; ?>"> -->

												</form>
												<div class="l_att">
													<div class="mtop " id="lead_attachments">
														<?php $att_rec = $this->Common_mdl->GetAllWithWhere('leads_attachments', 'lead_id', $leads['id']);
														foreach ($att_rec as $att_key => $att_value) { ?>

															<div class="data-quote-03">
																<div class="display-block lead-attachment-wrapper">
																	<div class="col-md-10">
																		<div class="pull-left"><i class="mime mime-pdf"></i></div>
																		<a href="<?php echo base_url() . 'uploads/leads/attachments/' . $att_value['attachments']; ?>" target="_blank"><?php echo $att_value['attachments']; ?></a>
																		<p class="text-muted"></p>
																	</div>

																	<?php if ($_SESSION['permission']['Leads']['delete'] == 1) { ?>
																		<div class="col-md-2 text-right for_attachment_data_delete">
																			<a href="#" data-target="#deleteconfirmation" data-toggle="modal" class="text-danger" data-id="<?php echo $att_value['id']; ?>" onclick="return delete_data(this,'delete_lead_attachment')"><i class="fa fa fa-times"></i>
																			</a>
																		</div>
																	<?php  } ?>
																	<div class="clearfix"></div>
																	<hr>
																</div>
															</div>


														<?php } ?>
													</div>
												</div>
											</div>
										</div>
										<!-- 4tab -->
										<div id="reminder_<?php echo $leads['id']; ?>" class="tab-pane fade pt-3rem lead-reminder-wrap">
											<div class="new-proposal1 floating_set for_reminder_data">

												<a href="#" data-toggle="modal" class="" data-target="#add-reminders_<?php echo $leads['id']; ?>" <?php if ($_SESSION['permission']['Leads']['create'] != 1 || strtolower($status_name['status_name']) == 'customer') { ?> style="display: none" <?php } ?>><i class="fa fa-bell-o fa-6" aria-hidden="true"></i> Add Lead Reminder</a>
												<?php //} 
												?>

												<div class="export_option">
													<div class="btn-group e-dash sve-action1 frpropos">
														<button type="button" class="btn btn-default dropdown-toggle emp-dash" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
															Export
														</button>
														<ul class="dropdown-menu" id="reminder_export" data-id="<?php echo $leads['id']; ?>">

															<!--    <li><a href="#" >Export</a></li> -->

															<li><a href="#" data-val="csv" class="reminder_export" data-id="<?php echo $leads['id']; ?>">CSV</a></li>

															<li><a href="#" class="reminder_export" data-id="<?php echo $leads['id']; ?>">PDF</a></li>

															<li><a href="#" class="reminder_export" data-val="html" data-id="<?php echo $leads['id']; ?>">HTML</a></li>


															<!--     <li id="buttons"><div class="dt-buttons">
											 <button class="dt-button">EXPORT</button>
											 <button class="dt-button" data-val="csv" data-id="<?php echo $leads['id']; ?>">CSV</button>
											 <button class="dt-button" data-val="pdf" data-id="<?php echo $leads['id']; ?>">PDF</button>
											 <button class="dt-button" data-val="html" data-id="<?php echo $leads['id']; ?>">HTML</button>
										   </div>
										 </li> -->

														</ul>
													</div>
												</div>
												<!--       <select name="reminder_export" id="reminder_export" class="reminder_export" data-id="<?php echo $leads['id']; ?>">
										 <option value="">EXPORT</option>
										 <option value="csv">CSV</option>
										 <option value="pdf">PDF</option>
										 <option value="html">HTML</option>
									  </select> -->

											</div>
											<div class="proposal-table1 floating_set response_res">
												<table id="example2" class="data-available-1 display example2" cellspacing="0" width="100%">
													<thead>
														<tr>
															<th>Subject</th>
															<th>Description</th>
															<th>Date</th>
															<th>Remind</th>
															<th>Option</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$sql = $this->db->query("select * from lead_reminder where lead_id = " . $leads['id'] . "")->result_array();

														$ex_val = $this->Common_mdl->get_module_user_data('LEADS', $leads['id']);
														//print_r($assign_user_id);

														foreach ($sql as $sql_key => $sql_value) {
															$assigned_staff_id = $sql_value;

															// $ex_val=explode(',', $assigned_staff_id);
															$var_array = array();
															if (count($ex_val) > 0) {
																foreach ($ex_val as $key => $value) {

																	$user_name = $this->Common_mdl->select_record('user', 'id', $value);
																	array_push($var_array, $user_name['crm_name']);
																}
															}


														?>
															<tr>
																<td> <?php echo $sql_value['subject']; ?></td>
																<td> <?php echo $sql_value['description']; ?></td>
																<td> <?php echo date('d-m-Y', strtotime($sql_value['reminder_date'])) ?></td>
																<td> <?php echo /*$user_name['crm_name'];*/ implode(',', $var_array) ?></td>
																<td class="for_reminder_data_action">
																	<?php //if($_SESSION['id']==$leads['user_id']){ 
																	?>
																	<p class="action_01">
																		<a href="#" data-target="#deleteconfirmation" title="Delete" data-toggle="modal" data-id="<?php echo $sql_value['id']; ?>" onclick="return delete_data(this,'delete_reminder');" <?php if ($_SESSION['permission']['Leads']['delete'] != 1) { ?> style="display: none;" <?php } ?>><i class="icofont icofont-ui-delete deleteUser" aria-hidden="true"></i></a>
																		<a href="#" title="Edit" data-toggle="modal" data-target="#edit-reminders_<?php echo $sql_value['id']; ?>"><i class="icofont icofont-edit" aria-hidden="true" <?php if ($_SESSION['permission']['Leads']['edit'] != 1) { ?> style="display: none;" <?php } ?>></i></a>
																	</p>
																	<?php //} 
																	?>
																</td>
															</tr>
														<?php } ?>
													</tbody>
												</table>
											</div>
										</div>
										<!-- 5tab -->
										<div id="lead_notes" class="tab-pane fade lead-notes-activity">										

											<div class="enter-activity-log floating_set widthfi_right">
												<?php

												$latestrecs = $this->db->query("
								   					SELECT * FROM activity_log where module='Lead' and module_id='" . $leads['id'] . "' ORDER BY `id`  DESC limit 30")->result_array();
												?><?php foreach ($latestrecs as $late_key => $late_value) {
														$date = date('Y-m-d H:i:s', $late_value['CreatedTime']);
														$l_id = $late_value['module_id'];
														$l_name = $this->Common_mdl->select_record('leads', 'id', $l_id);

														//echo $l_name['name'] . ' - ' . $late_value['log'];
													}
													?>
												<?php ?>
												<!-- time line for activity -->
												<div class="pcoded-wrapper">
													<div class="">
														<div class="pcoded-inner-content">
															<!-- Main-body start -->
															<div class="main-body">
																<div class="page-wrapper">
																	<!-- Page-body start -->
																	<div class="page-body">
																		<div class="row history historynew">
																			<div class="col-sm-12">
																				<div class="card">
																					<div class="card-header">
																						<h5>Your Activites</h5>
																						<a href="#" data-toggle="modal" data-target="#leads_note_popup" id="show_add_service_notes_popup" class="lead-note-wrap">Create a Note</a>
																					</div>
																					<!-- for newly added 04-05-2018 -->
																					<div class="row mar_bottls">
																						<div class="col-1">
																							<h5><i class="icofont icofont-file-text m-r-5"></i></h5>
																						</div>
																						<div class="col-3">
																							<input class="form-control form-control-lg" type="text" id="Note-search_timeline" placeholder="Search notes">
																						</div>
																					</div>


																					<!-- end newly added 04-05-2018 -->
																					<div class="card-block">
																						<div class="main-timeline">
																							<div class="cd-timeline cd-container" id="search_for_activity">
																								<?php foreach ($latestrecs as $late_key => $late_value) {
																									//$date=date('Y-m-d H:i:s',$late_value['CreatedTime']);
																									$date = date('d F Y h:i A', $late_value['CreatedTime']);
																									$l_id = $late_value['module_id'];
																									$l_name = $this->Common_mdl->select_record('leads', 'id', $l_id);
																									$user_data = $this->Common_mdl->select_record('user', 'id', $l_name['user_id']);

																									//echo $l_name['name'] . ' - ' . $late_value['log'];
																									/** for login and logout details **/
																									if ($late_value['module'] == 'Login' || $late_value['module'] == 'Logout') {
																								?>
																										<div class="cd-timeline-block activity_timeline_block">
																											<div class="cd-timeline-icon bg-primary">
																												<i class="icofont icofont-tasks-alt"></i>
																											</div>
																											<!-- cd-timeline-img -->
																											<div class="cd-timeline-content card_main">
																												<!--   <img src="<?php echo base_url(); ?>assets/images/timeline/img1.jpg" class="img-fluid width-100" alt="" /> -->
																												<div class="p-20">
																													<h6>
																														<?php if ($late_value['module'] == 'Login') {
																															echo "You Where Logged in ";
																														}
																														if ($late_value['module'] == 'Logout') {
																															echo "You Where Logged out in ";
																														}
																														?><?php echo $date; ?></h6>
																													<div class="timeline-details">

																													</div>
																												</div>
																												<div class="leads-logs-wrapper">
																													<span class="cd-date"><?php echo $date; ?></span>
																													<span class="cd-details">Your Activity Info</span>
																												</div>
																											</div>
																											<!-- cd-timeline-content -->
																										</div>
																										<?php }
																									/** end of login and logout activity **/
																									/** for all leads section **/
																									if ($late_value['module'] == 'Lead') {
																										/*** only for leads section **/
																										if ($late_value['sub_module'] == '' || $late_value['sub_module'] == 0) {
																										?>
																											<div class="cd-timeline-block activity_timeline_block">
																												<div class="cd-timeline-icon bg-primary">
																													<i class="icofont icofont-tasks-alt"></i>
																												</div>
																												<!-- cd-timeline-img -->
																												<div class="cd-timeline-content card_main">
																													<!--  <img src="assets/images/timeline/img1.jpg" class="img-fluid width-100" alt="" /> -->
																													<div class="p-20">
																														<h6>
																															<?php if ($late_value['log'] == 'New Lead Created') {
																																echo "New Lead " . $l_name['name'] . " is Created";
																															}
																															if ($late_value['log'] == 'Lead Records Updated') {
																																echo "New Lead " . $l_name['name'] . " is Updated";
																															}
																															if ($late_value['log'] == 'Lead Imported') {
																																$des = 'New Lead is Imported';
																																echo $des;
																															}
																															?>
																														</h6>
																														<div class="timeline-details">
																															<a href="#"> <i class="icofont icofont-ui-calendar"></i><span><?php echo $date; ?></span> </a>
																															<a href="#">
																																<i class="icofont icofont-ui-user"></i><span><?php if ($late_value['log'] != 'Lead Imported') {
																																													echo $user_data['crm_name'];
																																												}
																																												?></span>
																															</a>
																															<p class="m-t-20"><?php if ($late_value['log'] != 'Lead Imported') {
																																					echo $late_value['log'];
																																				} ?></p>
																														</div>
																													</div>
																													<div class="leads-logs-wrapper">
																														<span class="cd-date"><?php echo $date; ?></span>
																														<span class="cd-details">Your Lead Activity</span>
																													</div>
																												</div>
																												<!-- cd-timeline-content -->
																											</div>
																										<?php
																										}
																										/*** only for leads section **/
																										/** for leads attachement **/
																										if ($late_value['sub_module'] == 'Lead Attachment') {
																											$ex_val = explode('---', $late_value['log']);
																											$log_activity = $ex_val[0];
																											$log_img = $ex_val[1];
																											$ext_val = explode('.', $log_img);
																											$ext = end($ext_val);
																											$img_array = array("jpeg", "JPEG", "jpg", "JPG", "png", "PNG", "gif", "GIF");
																											if (in_array($ext, $img_array)) {
																												$img_res = 'yes';
																												$img_name = "";
																											} else {
																												$img_res = 'not';
																												$img_name = $log_img;
																											}

																										?>
																											<div class="cd-timeline-block activity_timeline_block">
																												<div class="cd-timeline-icon bg-warning">
																													<i class="icofont icofont-tasks-alt bg-warning"></i>
																												</div>
																												<!-- cd-timeline-img -->
																												<div class="cd-timeline-content card_main">
																													<?php if ($img_res == 'yes') { ?>
																														<img src="<?php echo base_url(); ?>uploads/leads/attachments/<?php echo $log_img; ?>" class="img-fluid width-100" alt="" />
																													<?php }
																													?>
																													<div class="p-20">
																														<h6>
																															<?php
																															if ($log_activity == 'Lead Attached') {
																																echo "You Have Attached " . $img_name . " File on ";
																															} else {
																																echo "You have Deleted " . $img_name . " File on ";
																															}
																															?><?php echo $date; ?></h6>
																														<div class="timeline-details">
																														</div>
																													</div>
																													<div class="leads-logs-wrapper">
																														<span class="cd-date"><?php echo $date; ?></span>
																														<span class="cd-details">Your Activity Info</span>
																													</div>
																												</div>
																												<!-- cd-timeline-content -->
																											</div>
																										<?php
																										}
																										/** end of lead attachement **/
																										/** for leads task section **/
																										if ($late_value['sub_module'] == 'Task') {
																											$task_data = $this->Common_mdl->select_record('add_new_task', 'id', $late_value['sub_module_id']);
																											$log_img = $task_data['attach_file'];
																											if ($log_img != '') {
																												$ext_val = explode('.', $log_img);
																												$ext = end($ext_val);
																												$img_array = array("jpeg", "JPEG", "jpg", "JPG", "png", "PNG", "gif", "GIF");
																												if (in_array($ext, $img_array)) {
																													$img_res = 'yes';
																													$img_name = "";
																												} else {
																													$img_res = 'not';
																													$img_name = $log_img;
																												}
																											} else {
																												$img_res = "no";
																											}
																											$ex_val = explode("---", $late_value['log']);

																										?>
																											<div class="cd-timeline-block activity_timeline_block">
																												<div class="cd-timeline-icon bg-warning">
																													<i class="icofont icofont-tasks-alt bg-warning"></i>
																												</div>
																												<!-- cd-timeline-img -->
																												<div class="cd-timeline-content card_main">
																													<?php if ($img_res == 'yes') { ?>
																														<img src="<?php echo base_url(); ?>uploads/leads/tasks/<?php echo $log_img; ?>" class="img-fluid width-100" alt="" />
																													<?php }
																													?>
																													<div class="p-20">
																														<h6>
																															<?php
																															if ($ex_val[0] == 'Task Created') {
																																echo "You Have Created " . isset($ex_val[1]) ? $ex_val[1] : '' . "  on ";
																															} elseif ($ex_val[0] == 'Task Records Updated') {
																																echo "You Have Updated " . isset($ex_val[1]) ? $ex_val[1] : '' . "  on ";
																															} else {
																																$ex_val = explode('---', $late_value['log']);

																																echo "You have Deleted " . isset($ex_val[1]) ? $ex_val[1] : '' . " on ";
																															}
																															?><?php echo $date; ?></h6>
																														<div class="timeline-details">
																															<?php if ($late_value['log'] == 'Task Created' || $late_value['log'] == 'Task Records Updated') {
																															?>
																																<p><?php echo $task_data['description']; ?></p>
																															<?php
																															} ?>
																															<p></p>
																														</div>
																													</div>
																													<div class="leads-logs-wrapper">
																														<span class="cd-date"><?php echo $date; ?></span>
																														<span class="cd-details">Your Task Info</span>
																													</div>
																												</div>
																												<!-- cd-timeline-content -->
																											</div>
																										<?php
																										}
																										/** end of task section **/

																										/** for leads Reminder section **/
																										if ($late_value['sub_module'] == 'Reminder') {
																											$reminder_data = $this->Common_mdl->select_record('lead_reminder', 'id', $late_value['sub_module_id']);
																											$reminder_res = $reminder_data['assigned_staff_id'];
																											$user_data = $this->Common_mdl->select_record('user', 'id', $reminder_res);
																											if ($reminder_res != '') {
																												$res_reminder = "yes";
																											} else {
																												$res_reminder = "no";
																											}
																											$ex_val = explode('---', $late_value['log']);
																											$user_data = $this->Common_mdl->select_record('user', 'id', isset($ex_val[1]) ? $ex_val[1] : '');

																										?>
																											<div class="cd-timeline-block activity_timeline_block">
																												<div class="cd-timeline-icon bg-warning">
																													<i class="icofont icofont-tasks-alt bg-warning"></i>
																												</div>
																												<!-- cd-timeline-img -->
																												<div class="cd-timeline-content card_main">
																													<div class="p-20">
																														<h6>
																															<?php
																															if ($ex_val[0] == 'Reminder Imported') {
																																echo "You Have Assigned a Reminder To  " . $user_data['crm_name'] . "  on ";
																															} elseif ($ex_val[0] == 'Reminder Updated') {
																																echo "You Have Updated Assigned a Reminder To " . $user_data['crm_name'] . "  on ";
																															} else {

																																echo "You have Deleted Assigned a Reminder of " . $user_data['crm_name'] . " on ";
																															}
																															?><?php echo $date; ?></h6>
																														<div class="timeline-details">
																															<?php if ($late_value['log'] == 'Reminder Imported' || $late_value['log'] == 'Reminder Updated') {
																															?>
																																<p><?php echo $reminder_data['description']; ?></p>
																															<?php
																															} ?>
																															<p></p>
																														</div>
																													</div>
																													<div class="leads-logs-wrapper">
																														<span class="cd-date"><?php echo $date; ?></span>
																														<span class="cd-details">Your Reminder Info</span>
																													</div>
																												</div>
																												<!-- cd-timeline-content -->
																											</div>
																										<?php
																										}
																										/** end of Reminder section **/
																										if ($late_value['sub_module'] == 'Lead Notes') {
																											$ex_val = explode("---", $late_value['log']);
																											$sub_id = $late_value['sub_module_id'];

																										?>
																											<div class="cd-timeline-block activity_timeline_block">
																												<div class="cd-timeline-icon bg-primary">
																													<i class="icofont icofont-tasks-alt"></i>
																												</div>
																												<!-- cd-timeline-img -->
																												<div class="cd-timeline-content card_main">
																													<!--  <img src="assets/images/timeline/img1.jpg" class="img-fluid width-100" alt="" /> -->
																													<div class="p-20">
																														<h6>
																															<?php

																															$note_val =  (isset($ex_val[1])) ? $ex_val[1] : '';

																															if ($ex_val[0] == 'Lead Notes Added') {

																																echo "Notes " . $note_val . " is Created";
																															}
																															if ($ex_val[0] == 'Lead Notes Updated') {

																																echo "Notes " . $note_val . " is Updated";
																															}

																															if ($ex_val[0] == 'Lead Notes Delete') {
																																echo "Notes " . $note_val . " is Deleted";
																															} ?>
																														</h6>
																														<div class="timeline-details">
																															<a href="#"> <i class="icofont icofont-ui-calendar"></i><span><?php echo $date; ?></span> </a>
																															<a href="#">
																															<i class="icofont icofont-ui-user"></i><span><?php echo $user_data['crm_name']; ?></span>
																															</a>
																															<p class="m-t-20"><?php echo $l_name['description']; ?></p>
																														</div>
																													</div>
																													<div class="leads-logs-wrapper">
																														<span class="cd-date"><?php echo $date; ?></span>
																														<span class="cd-details">Your Notes Activity</span>
																													</div>
																												</div>
																												<!-- cd-timeline-content -->
																											</div>
																									<?php
																										}
																									}


																									/** for leads section **/
																									?>
																								<?php } ?>
																							</div>
																							<!-- cd-timeline -->
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																	<!-- Page-body end -->
																</div>
															</div>
															<!-- Main-body end -->
															<div id="styleSelector">
															</div>
														</div>
													</div>
												</div>
												<!-- end time line -->
												<!--      <div class="leads_activity_log">
								   <?php
									$latestrecs = $this->db->query(" select * from activity_log where module_id='" . $leads["id"] . "' and module='Lead' order by id asc")->result_array();
									foreach ($latestrecs as $late_key => $late_value) { ?>
								   <div class="activity-created1">
								   <span><?php $date = date('Y-m-d H:i:s', $late_value['CreatedTime']);
											echo time_elapsed_string($date); ?></span>
								   <span><?php
											$l_id = $late_value['module_id'];
											$l_name = $this->Common_mdl->select_record('leads', 'id', $l_id);

											echo $l_name['name'] . ' - ' . $late_value['log']; ?></span>
								   </div>
								   <?php } ?>
								   </div>
								   <div class="notetextarea1">
								   <textarea rows="3" placeholder="Enter Activity" name="activity_log" id="activity_log_<?php echo $leads['id']; ?>" class="activity_log"></textarea>
								   <div id="error<?php echo $leads['id']; ?>" class="error" style="color: red;"></div>
								   <input type="submit" name="add_activity" id="add_activity" data-id="<?php echo $leads['id']; ?>" class="add_activity" value="Add Activity" >
								   </div> -->
											</div>
										</div>
										<!-- 6tab -->
										<!--  <div id="activity-log_<?php echo $leads['id']; ?>" class="tab-pane fade"> -->
										<!-- 7tab -->
									</div>
									<!-- tab-content -->
								</div>
							</div>
						</div>
					</div>
					<!-- profile-edit-crm -->
					<!-- ********************************* -->
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade modal-reminder reminder-modal-lead-4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" id="add-reminders_<?php echo $leads['id']; ?>">
	<div class="modal-dialog modal-dialog-lead-remind" role="document">
		<div class="modal-content">
			<form action="#" id="add_leads_reminder" name="add_leads_reminder" class="add_leads_reminder" method="post">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<!-- <button type="button" class="btn btn-default  close-reminder-modal" data-rel-id="4" data-rel-type="lead" aria-label="Close" value=""> 
						<span aria-hidden="true" data-dismiss="modal">×</span></button> -->
					<h4 class="modal-title" id="myModalLabel"><i class="fa fa-question-circle" data-toggle="tooltip" title="This option allows you to never forget anything about your customers." data-placement="bottom"></i> Add lead reminder</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group col-md-12" app-field-wrapper="subject">
								<label for="subject" class="control-label">Subject</label>
								<input type="text" id="lead-reminder-subject" placeholder="Subject">
							</div>
							<div class="form-group col-md-6" app-field-wrapper="date">
								<label for="date" class="control-label"> <small class="req text-danger">* </small>Date to be notified</label>
								<div class="input-group date">
									<!--  <input type="text" id="add_spl-datepicker" name ="reminder_date"  class="form-control spl-datepicker spl-datepicker_<?php echo $leads['id']; ?>" value=""> -->
									<input class="form-control dob_picker fields spl-datepicker_<?php echo $leads['id']; ?>" type="text" name="reminder_date" id="" placeholder="" value="<?php echo (new DateTime())->format('d-m-Y'); ?>" />
									<div class="input-group-addon">
										<i class="fa fa-calendar calendar-icon"></i>
									</div>
									<div id="reminder_date_err_<?php echo $leads['id']; ?>" style="color:red"></div>
								</div>
							</div>
							<div class="form-group col-md-6" app-field-wrapper="staff">
								<label for="staff" class="control-label"> <small class="req text-danger">* </small>Set reminder to</label>
								<div class="btn-group rem_ass">

									<input type="text" id="tree_select2" class="tree_select" name="assignees[]" placeholder="Select">

									<div id="assign_err_<?php echo $leads['id']; ?>" style="color:red"></div>
								</div>
							</div>
							<div class="form-group col-md-12" app-field-wrapper="description"><label for="description" class="control-label">Description</label>
								<textarea id="reminder_description_<?php echo $leads['id']; ?>" name="reminder_description" class="form-control" rows="4"></textarea>
								<div id="description_err_<?php echo $leads['id']; ?>" style="color:red"></div>
							</div>
							<div class="form-group col-md-12">
								<div class="checkbox checkbox-primary">
									<label class="custom_checkbox1">
										<input type="checkbox" name="notify_by_email_<?php echo $leads['id']; ?>" id="notify_by_email_<?php echo $leads['id']; ?>" value="">
										<i></i></label>
									<span>Send also an email for this reminder</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-info reminder_save" value="" id="reminder_save" data-id="<?php echo  $leads['id']; ?>">Save</button>
					<button type="button" class="btn btn-default close-reminder-modal" data-rel-id="4" data-rel-type="lead" data-dismiss="modal" value="">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="reminder_popups_<?php echo $leads['id']; ?>">
	<?php
	$edit_reminder = $this->db->query("select * from lead_reminder where lead_id = " . $leads['id'] . "")->result_array();
	foreach ($edit_reminder as $e_k_remidner => $e_k_value) {

		$reminder_date  = implode('/', array_reverse(explode('-', $e_k_value['reminder_date'])));

	?>
		<div class="modal fade modal-reminder reminder-modal-lead-4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" id="edit-reminders_<?php echo $e_k_value['id']; ?>">
			<div class="modal-dialog modal-dialog-lead-remind" role="document">
				<div class="modal-content">
					<form action="#" id="edit_leads_reminder" name="edit_leads_reminder" class="edit_leads_reminder" method="post">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">×</button>
							<!-- <button type="button" class="close close-reminder-modal" data-rel-id="4" data-rel-type="lead" aria-label="Close" value=""><span aria-hidden="true" data-dismiss="modal">×</span></button> -->
							<h4 class="modal-title" id="myModalLabel"><i class="fa fa-question-circle" data-toggle="tooltip" title="This option allows you to never forget anything about your customers." data-placement="bottom"></i> Edit lead reminder</h4>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group" app-field-wrapper="date">
										<label for="date" class="control-label"> <small class="req text-danger">* </small>Date to be notified</label>
										<div class="input-group date">
											<!-- edit_spl-datepicker -->
											<input type="text" id="" name="edit_reminder_date" class="form-control dob_picker edit_spl-datepicker_<?php echo $e_k_value['id']; ?>" value="<?php echo implode("-", array_reverse(explode('-', $reminder_date))) ?>">
											<div class="input-group-addon">
												<i class="fa fa-calendar calendar-icon"></i>
											</div>
											<div id="edit_reminder_date_err_<?php echo $e_k_value['id']; ?>" style="color:red"></div>
										</div>
									</div>
									<div class="form-group" app-field-wrapper="staff">
										<label for="staff" class="control-label"> <small class="req text-danger">* </small>Set reminder to</label>
										<div class="btn-group rem_edit">

											<input type="text" class="tree_select" id="tree_select1" name="assignees[]" placeholder="Select">


										</div>
									</div>
									<div class="form-group" app-field-wrapper="description">
										<label for="description" class="control-label">Description</label>
										<textarea id="edit_reminder_description_<?php echo $e_k_value['id']; ?>" name="edit_reminder_description_<?php echo $e_k_value['id']; ?>" class="form-control" rows="4"><?php echo $e_k_value['description']; ?></textarea>
										<div id="description_err_<?php echo $e_k_value['id']; ?>" style="color:red"></div>
									</div>
									<div class="form-group">
										<div class="checkbox checkbox-primary">
											<input type="checkbox" name="edit_notify_by_email_<?php echo $e_k_value['id']; ?>" id="edit_notify_by_email_<?php echo $e_k_value['id']; ?>" value="" <?php if ($e_k_value['email_sent'] == 'yes') {
																																																		echo 'checked="checked"';
																																																	} ?>>
											<label for="notify_by_email">Send also an email for this reminder</label>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-info edit_reminder_save" value="" id="edit_reminder_save" data-id="<?php echo  $e_k_value['id']; ?>" data-value="<?php echo $leads['id']; ?>">Save</button>
							<button type="button" class="btn btn-default close-reminder-modal" data-rel-id="4" data-rel-type="lead" data-dismiss="modal" value="">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	<?php } ?>
</div>
<div class="test_test"></div>
<!-- Modal -->
<div id="addnew_task_lead_<?php echo $leads['id']; ?>" class="addnew_task_lead1  modal fade" role="dialog">
	<div class="modal-dialog modal-dialog-new-lead-task">
		<!-- Modal content-->
		<form name="lead_task" id="lead_task_<?php echo $leads['id']; ?>" class="add_lead_task" method="post" enctype='multipart/form-data' data-id="<?php echo $leads['id']; ?>">
			<div class="modal-content">
				<div class="modal-header modal-header-new-lead-task">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add new task</h4>
				</div>

				<div class="modal-body modal-body-new-lead-task">
					<div class="attached-popup1">
						<div class="form-group checkbox-color checkbox-primary">
							<span class="popup-check" style="display: none;">
								<input type="checkbox" name="public" value="Public" id="p-check_<?php echo $leads['id']; ?>">
								<label for="p-check_<?php echo $leads['id']; ?>">Public </label>
							</span>
							<span class="popup-check" style="display: none;">
								<input type="checkbox" name="billable" value="Billable" id="p-check1_<?php echo $leads['id']; ?>">
								<label for="p-check1_<?php echo $leads['id']; ?>"> Billable</label>
							</span>
						</div>

					</div>
				</div>
				<div class="lead-popupsection2 proposal-data1 floating_set">
					<div class="formgroup col-xs-12 col-sm-4">
						<label>* Subject</label>
						<input type="text" name="task_sub_<?php echo $leads['id']; ?>" id="task_sub_<?php echo $leads['id']; ?>">
						<div class="task_sub_err" id="task_sub_err_<?php echo $leads['id']; ?>" style="color:red;"></div>
					</div>
					<div class="formgroup col-xs-12 col-sm-4" style="display: none">
						<label>Hourly Rate</label>
						<input type="text" name="hour_rate_<?php echo $leads['id']; ?>" id="hour_rate_<?php echo $leads['id']; ?>">
					</div>
					<!-- <div class="formgroup col-xs-12 col-sm-6">
               <input type="hidden" value="<?php //echo $leads['review_manager']
											?>" name='review_manager'>
               </div> -->
					<div class="form-group  col-xs-12 col-sm-4 date_birth">
						<label>Start Date</label>
						<div class="form-birth05">
							<span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
							<input class="form-control dob_picker fields" type="text" name="start_date_<?php echo $leads['id']; ?>" id="start_date_<?php echo $leads['id']; ?>" placeholder="Start date" value="<?php echo (new DateTime())->format('d-m-Y'); ?>" />
							<div class="task_start_err" id="task_start_err_<?php echo $leads['id']; ?>" style="color:red;"></div>
						</div>
					</div>
					<div class="form-group  col-xs-12 col-sm-4 date_birth">
						<label>Due Date</label>
						<div class="form-birth05">
							<span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
							<input class="form-control dob_picker fields" type="text" name="due_date_<?php echo $leads['id']; ?>" id="due_date_<?php echo $leads['id']; ?>" placeholder="Due date" value="" />
							<div class="task_end_err" id="task_end_err_<?php echo $leads['id']; ?>" style="color:red;"></div>
						</div>
					</div>
					<div class="formgroup col-xs-12 col-sm-4">
						<label>Priority</label>
						<select name="priority" class="form-control valid" id="priority_<?php echo $leads['id']; ?>">
							<option value="low">Low</option>
							<option value="medium">Medium</option>
							<option value="high">High</option>
							<option value="super_urgent">Super Urgent</option>
						</select>
					</div>
					<!-- its not work properly so we hide -->
					<div class="formgroup col-xs-12 col-sm-4" style="display:none;">
						<label>Repeat every Week</label>
						<select name="repeat" id="repeat_<?php echo $leads['id']; ?>" class="repeated_too" data-id="<?php echo $leads['id']; ?>">
							<option value=""></option>
							<option value="one_week">week</option>
							<option value="two_week">2 week</option>
							<option value="one_month">1 Month</option>
							<option value="two_month">2 Months</option>
							<option value="three_month">3 Months</option>
							<option value="six_month">6 Months</option>
							<option value="one_yr">1 yr</option>
							<option value="custom">Custom</option>
						</select>
					</div>
					<div class="form-group col-xs-12 col-sm-4 rep_no" style="display:none;" id="rep_no_<?php echo $leads['id']; ?>">
						<input class="form-control fields" type="number" name="repeat_no_<?php echo $leads['id']; ?>" id="repeat_no_<?php echo $leads['id']; ?>" placeholder="" value="1" />
					</div>
					<div class="form-group col-xs-12 col-sm-4 rep_cnt" style="display:none;" id="rep_cnt_<?php echo $leads['id']; ?>">
						<select class="selectpicker" data-live-search="true" id="durations_<?php echo $leads['id']; ?>">
							<option value="days">Day(s)</option>
							<option value="weeks">Week(s)</option>
							<option value="months">Month(s)</option>
							<option value="years">Year(s)</option>
						</select>
					</div>
					<div class="form-group  col-xs-12  col-sm-12 date_birth ends_on_div" id="ends_on_div_<?php echo $leads['id']; ?>" style="display:none;">
						<label>Ends On (Leave blank for never)</label>
						<div class="form-birth05">
							<span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
							<input class="form-control datepicker fields" type="text" name="ends_on_<?php echo $leads['id']; ?>" id="ends_on_<?php echo $leads['id']; ?>" placeholder="Ends on date" value="" />
						</div>
					</div>
					<!-- end of hide requiring -->
					<div class="form-group col-xs-12 col-sm-4" style="display: none">
						<label>Related To</label>
						<div class="dropdown-sin-33">
							<select class="" data-live-search="true" id="related_to_<?php echo $leads['id']; ?>">
								<option value="leads">Leads</option>
								<!-- <option value="client">Client</option> -->
							</select>
						</div>
					</div>
					<div class="form-group col-xs-12 col-sm-4 sel_ass addtree_cls">
						<label>Select Assignee</label>
						<input type="text" id="tree_select" class="tree_select" name="assignees[]" placeholder="Select">

						<div class="task_assign_err_" id="task_assign_err_<?php echo $leads['id']; ?>" style="color:red;"></div>

					</div>
					<div class="tags-allow1 floating_set">
						<h4><i class="fa fa-tags fa-6" aria-hidden="true"></i> Tags</h4>
						<input type="text" value="" name="tags_<?php echo $leads['id']; ?>" id="tags_<?php echo $leads['id']; ?>" class="tags" />
					</div>
					<div class="formgroup col-sm-12 des-ajax1">
						<label>Description</label>
						<textarea rows="3" name="description" id="descr_<?php echo $leads['id']; ?>"></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer modal-footer-new-lead-task">
				<input type="submit" name="save" id="save" class="task_leads" data-id="<?php echo $leads['id']; ?>" value="Save">
				<a href="#" data-dismiss="modal">Close</a>
			</div>
		</form>
	</div>
</div>
</div>
<!-- rspt 18-04-2018 -->
<div class="task_edit_popup">
	<?php $edit_tasks =  $this->db->query("select * from add_new_task where related_to='leads' and lead_id=" . $leads['id'] . "")->result_array();
	foreach ($edit_tasks as $t_key => $t_value) { ?>
		<!-- Modal -->
		<div id="edit_task_lead_<?php echo $t_value['id']; ?>" class="addnew_task_lead1  modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<form name="lead_task" id="lead_task_<?php echo $t_value['id']; ?>" class="add_lead_task" method="post" enctype='multipart/form-data' data-id="<?php echo $t_value['id']; ?>">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Edit task</h4>
						</div>
						<div class="modal-body">
							<div class="attached-popup1">
								<div class="form-group checkbox-color checkbox-primary" <?php if ($leads['public'] != '') {
																						} else { ?> style="display: none;" <?php  } ?>>
									<span class="popup-check">
										<input type="checkbox" name="public" value="Public" id="t_edit_p-check_<?php echo $t_value['id']; ?>" <?php if (isset($t_value['public']) && $t_value['public'] == 'Public') { ?> checked="checked" <?php } ?>>
										<label for="p-check">Public </label>
									</span>
									<span class="popup-check" style="display: none">

										<input type="checkbox" name="billable" value="Billable" checked="" id="t_edit_p-check1_<?php echo $t_value['id']; ?>" <?php if (isset($t_value['billable']) && $t_value['billable'] == 'Billable') { ?> checked="checked" <?php } ?>>
										<label for="p-check1"> Billable</label>
									</span>
								</div>

								<?php if ($t_value['attach_file'] != '') { ?><img src="<?php echo base_url(); ?>uploads/leads/tasks/<?php echo $t_value['attach_file']; ?>" height="50" width="50"> <?php } ?>
							</div>
						</div>
						<div class="lead-popupsection2 proposal-data1 floating_set">
							<div class="formgroup col-xs-12 col-sm-6">
								<label>* Subject</label>
								<input type="text" name="t_edit_task_sub_<?php echo $t_value['id']; ?>" id="t_edit_task_sub_<?php echo $t_value['id']; ?>" value="<?php if (isset($t_value['subject']) && ($t_value['subject'] != '')) {
																																										echo $t_value['subject'];
																																									} ?>">
								<div class="t_edit_task_sub_err" id="t_edit_task_sub_err_<?php echo $t_value['id']; ?>" style="color:red;"></div>
							</div>
							<!--  <div class="formgroup col-xs-12 col-sm-6">
                     <label>Hourly Rate</label>
                     <input type="text" name="t_edit_hour_rate_<?php echo $t_value['id']; ?>" id="t_edit_hour_rate_<?php echo $t_value['id']; ?>" value="<?php if (isset($t_value['hour_rate']) && ($t_value['hour_rate'] != '')) {
																																								echo $t_value['hour_rate'];
																																							} ?>">
                  </div> -->
							<div class="form-group  col-xs-12 col-sm-6 date_birth">
								<label>Start Date</label>
								<div class="form-birth05">
									<span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
									<input class="form-control dob_picker fields" type="text" name="t_edit_start_date_<?php echo $t_value['id']; ?>" id="t_edit_start_date_<?php echo $t_value['id']; ?>" placeholder="Start date" value="<?php if (isset($t_value['start_date']) && ($t_value['start_date'] != '')) {
																																																												echo implode("-", array_reverse(explode('-', $t_value['start_date'])));
																																																											} ?>" />
									<div class="t_edit_task_start_err" id="t_edit_task_start_err_<?php echo $t_value['id']; ?>" style="color:red;"></div>
								</div>
							</div>
							<div class="form-group  col-xs-12 col-sm-6 date_birth">
								<label>Due Date</label>
								<div class="form-birth05">
									<span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
									<input class="form-control dob_picker fields" type="text" name="t_edit_due_date_<?php echo $t_value['id']; ?>" id="t_edit_due_date_<?php echo $t_value['id']; ?>" placeholder="Due date" value="<?php if (isset($t_value['end_date']) && ($t_value['end_date'] != '')) {
																																																										echo implode("-", array_reverse(explode('-', $t_value['end_date'])));
																																																									} ?>" />
									<div class="t_edit_task_end_err" id="t_edit_task_end_err_<?php echo $t_value['id']; ?>" style="color:red;"></div>
								</div>
							</div>
							<div class="formgroup col-xs-12 col-sm-6">
								<label>Priority</label>
								<select name="t_edit_priority" class="form-control valid" id="t_edit_priority_<?php echo $t_value['id']; ?>">
									<option value="low" <?php if (isset($t_value['priority']) && $t_value['priority'] == 'low') { ?> selected="selected" <?php } ?>>Low</option>
									<option value="medium" <?php if (isset($t_value['priority']) && $t_value['priority'] == 'medium') { ?> selected="selected" <?php } ?>>Medium</option>
									<option value="high" <?php if (isset($t_value['priority']) && $t_value['priority'] == 'high') { ?> selected="selected" <?php } ?>>High</option>
									<option value="super_urgent" <?php if (isset($t_value['priority']) && $t_value['priority'] == 'super_urgent') { ?> selected="selected" <?php } ?>>Super Urgent</option>
								</select>
							</div>
							<div class="formgroup col-xs-12 col-sm-6" style="display: none;">
								<label>Repeat every Week</label>
								<select name="t_edit_repeat" id="t_edit_repeat_<?php echo $t_value['id']; ?>" class="t_edit_repeated_too" data-id="<?php echo $t_value['id']; ?>">
									<option value=""></option>
									<option value="one_week" <?php if (isset($t_value['sche_repeats']) && $t_value['sche_repeats'] == 'one_week') { ?> selected="selected" <?php } ?>>week</option>
									<option value="two_week" <?php if (isset($t_value['sche_repeats']) && $t_value['sche_repeats'] == 'two_week') { ?> selected="selected" <?php } ?>>2 week</option>
									<option value="one_month" <?php if (isset($t_value['sche_repeats']) && $t_value['sche_repeats'] == 'one_month') { ?> selected="selected" <?php } ?>>1 Month</option>
									<option value="two_month" <?php if (isset($t_value['sche_repeats']) && $t_value['sche_repeats'] == 'two_month') { ?> selected="selected" <?php } ?>>2 Months</option>
									<option value="three_month" <?php if (isset($t_value['sche_repeats']) && $t_value['sche_repeats'] == 'three_month') { ?> selected="selected" <?php } ?>>3 Months</option>
									<option value="six_month" <?php if (isset($t_value['sche_repeats']) && $t_value['sche_repeats'] == 'six_month') { ?> selected="selected" <?php } ?>>6 Months</option>
									<option value="one_yr" <?php if (isset($t_value['sche_repeats']) && $t_value['sche_repeats'] == 'one_yr') { ?> selected="selected" <?php } ?>>1 yr</option>
									<option value="custom" <?php if (isset($t_value['sche_repeats']) && $t_value['sche_repeats'] == 'custom') { ?> selected="selected" <?php } ?>>Custom</option>
								</select>
							</div>
							<?php if ($t_value['repeat_no'] != '') { ?>
								<div class="form-group col-xs-12 col-sm-6 rep_no" id="t_edit_rep_no_<?php echo $t_value['id']; ?>">
									<input class="form-control fields" type="number" name="t_edit_repeat_no_<?php echo $t_value['id']; ?>" id="t_edit_repeat_no_<?php echo $t_value['id']; ?>" placeholder="" value="<?php echo $t_value['repeat_no']; ?>" />
								</div>
							<?php } ?>
							<?php if ($t_value['durations'] != '') { ?>
								<div class="form-group col-xs-12 col-sm-6 rep_cnt" id="t_edit_rep_cnt_<?php echo $t_value['id']; ?>">
									<select class="selectpicker" data-live-search="true" id="t_edit_durations_<?php echo $t_value['id']; ?>">
										<option value="days" <?php if (isset($t_value['durations']) && $t_value['durations'] == 'days') { ?> selected="selected" <?php } ?>>Day(s)</option>
										<option value="weeks" <?php if (isset($t_value['durations']) && $t_value['durations'] == 'weeks') { ?> selected="selected" <?php } ?>>Week(s)</option>
										<option value="months" <?php if (isset($t_value['durations']) && $t_value['durations'] == 'months') { ?> selected="selected" <?php } ?>>Month(s)</option>
										<option value="years" <?php if (isset($t_value['durations']) && $t_value['durations'] == 'years') { ?> selected="selected" <?php } ?>>Year(s)</option>
									</select>
								</div>
							<?php } ?>
							<?php if ($t_value['ends_on'] != '') { ?>
								<div class="form-group  col-xs-12  col-sm-12 date_birth ends_on_div" id="t_edit_ends_on_div_<?php echo $t_value['id']; ?>">
									<label>Ends On (Leave blank for never)</label>
									<div class="form-birth05">
										<span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
										<input class="form-control datepicker fields" type="text" name="t_edit_ends_on_<?php echo $t_value['id']; ?>" id="t_edit_ends_on_<?php echo $t_value['id']; ?>" placeholder="Ends on date" value="<?php echo $t_value['ends_on']; ?>" />
									</div>
								</div>
							<?php } ?>
							<div class="form-group col-xs-12 col-sm-6">
								<label>Related To</label>
								<div class="dropdown-sin-32 ">
									<select class="" data-live-search="true" id="t_edit_related_to_<?php echo $t_value['id']; ?>">
										<option value="leads" <?php if (isset($t_value['related_to']) && $t_value['related_to'] == 'leads') { ?> selected="selected" <?php } ?>>Leads</option>
										<!--    <option value="client" <?php if (isset($t_value['related_to']) && $t_value['related_to'] == 'client') { ?> selected="selected"<?php } ?>>Client</option> -->
									</select>
								</div>
							</div>
							<div class="form-group col-xs-12 col-sm-6 edit_tasksclass addtree_cls">
								<label>Lead</label>

								<input type="text" class="tree_select" id="tree_select3" name="assignees[]" placeholder="Select">

							</div>
							<div class="tags-allow1 floating_set">
								<h4><i class="fa fa-tags fa-6" aria-hidden="true"></i> Tags</h4>
								<input type="text" value="<?php echo $t_value['tag']; ?>" name="t_edit_tags_<?php echo $t_value['id']; ?>" id="t_edit_tags_<?php echo $t_value['id']; ?>" class="tags" />
							</div>
							<div class="formgroup des-ajax1 col-sm-12">
								<label>Description</label>
								<textarea rows="3" name="t_edit_description" id="t_edit_descr_<?php echo $t_value['id']; ?>"><?php echo $t_value['description']; ?></textarea>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<a href="#" data-dismiss="modal">Close</a>
						<input type="submit" name="save" id="save" class="edit_task_leads" data-id="<?php echo $t_value['id']; ?>" value="Save" data-value="<?php echo $leads['id']; ?>">
					</div>
				</form>
			</div>
		</div>
		<input type="hidden" name="t_edit_img_name_<?php echo $t_value['id']; ?>" id="t_edit_img_name_<?php echo $t_value['id']; ?>" value="<?php echo $t_value['attach_file']; ?>">
	<?php } ?>
</div>


<div class="modal fade" id="deleteconfirmation" role="dialog">
	<div class="modal-dialog modal-dialog-del-lead">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Confirmation</h4>
			</div>
			<div class="modal-body">
				<input type="hidden" id="delete_id" value="" />
				<input type="hidden" id="delete_fun" value="" />

				<p> Are you sure you want to delete ?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" onclick="delete_action()" data-dismiss="modal">Yes</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
			</div>
		</div>

	</div>
</div>

<div class="modal fade" id="add_note_rec_modal" role="dialog">
	<div class="modal-dialog modal-dialog-new-log">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"></button>
				<h4 class="modal-title">Create a new note</h4>
				<input type="hidden" name="hidden_lead_id" id="hidden_lead_id" value="">
			</div>

			<div class="modal-body">
				<div class="editor_note">
					<textarea name="leads_notes_msg" id="leads_notes_msg" placeholder="Type your text here..."></textarea>
					<!-- <span class="notes_errormsg error_msg" style="color: red;"></span> -->
					<div id="error_note" class="error" style="color: red;"></div>
				</div>
			</div>

			<div class="modal-footer">
				<div>
					<button type="button" name="add_note_rec" id="add_note_rec" data-id="" class="add_note_rec btn btn-success">Add</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal-alertsuccess alert info_popup" style="display:none;">
	<div class="newupdate_alert">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
		<div class="pop-realted1">
			<div class="position-alert1">

			</div>
		</div>
	</div>
</div>
<input type="hidden" name="img_name_<?php echo $leads['id']; ?>" id="img_name_<?php echo $leads['id']; ?>" value="">
<?php // } 
?>
<?php /*************************************************************/ ?>
<style type="text/css">
	.ui-datepicker {
		z-index: 9999999999 !important;
	}

	.pcoded-content {
		float: left !important;
		width: 100%;
	}
</style>
<!-- Modal -->
<?php $this->load->view('includes/footer'); ?>
<!-- for file -->
<!-- end file -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<!-- <script src="http://remindoo.org/CRMTool/assets/js/jquery.fileuploads.init.js" type="text/javascript"></script>
<script src="http://remindoo.org/CRMTool/assets/js/jquery.filer.min.js"></script> -->
<!-- -->
<!-- <script type="text/javascript" src="http://remindoo.org/CRMTool/assets/js/note.js"></script>
   <script type="text/javascript" src="<?php echo base_url(); ?>bower_components/moment/js/moment.js"></script>
   <script type="text/javascript" src="http://remindoo.org/CRMTool/assets/js/store.min.js"></script> -->
<!-- <script type="text/javascript" src="http://remindoo.org/CRMTool/assets/js/store.min.js"></script>
 -->
<script type="text/javascript" src="<?php echo base_url(); ?>bower_components/moment/js/moment.js"></script>
<!--     <script type="text/javascript" src="http://remindoo.org/CRMTool/assets/js/note_tab_custom.js"></script> -->
<!-- -->

<!--Tree Select -->

<script src="<?php echo base_url(); ?>assets/tree_select/comboTreePlugin.js"></script>
<script src="<?php echo base_url(); ?>assets/tree_select/icontains.js"></script>

<script type="text/javascript">
	/** 29-08-2018 **/

	var tree_select = <?php echo json_encode(GetAssignees_SelectBox()); ?>;
	var assign_check = <?php echo $assign_check ?>;


	$('.tree_select').comboTree({
		source: tree_select,
		isMultiple: true
	});


	$('.comboTreeItemTitle').click(function() {

		var class_name = '';

		if ($(this).parents('div').hasClass("lead-form1")) {
			class_name = '.lead-form1';
		} else if ($(this).parents('div').hasClass("sel_ass")) {
			class_name = '.sel_ass';
		} else if ($(this).parents('div').hasClass("rem_ass")) {
			class_name = '.rem_ass';
		} else if ($(this).parents('div').hasClass("rem_edit")) {
			class_name = '.rem_edit';
		} else {
			class_name = '.edit_tasksclass';
		}

		// alert(class_name);

		var id = $(this).attr('data-id');
		var id = id.split('_');

		$(class_name + ' .comboTreeItemTitle').each(function() {
			var id1 = $(this).attr('data-id');
			var id1 = id1.split('_');
			//console.log(id[1]+"=="+id1[1]);
			if (id[1] == id1[1]) {
				$(this).toggleClass('disabled');
			}
		});
		$(this).removeClass('disabled');
	});



	var arr1 = <?php echo json_encode($assign_data) ?>;

	// alert(arr1);

	var unique = arr1.filter(function(itm, i, arr1) {
		return i == arr1.indexOf(itm);
	});

	// alert(unique);

	$('.edit_tasksclass .comboTreeItemTitle').each(function() {
		$(this).find("input:checked").trigger('click');
		var id1 = $(this).attr('data-id');
		var id = id1.split('_');
		if (jQuery.inArray(id[0], unique) !== -1) {
			$(this).find("input").trigger('click');
			if (assign_check == 0) {
				$(this).toggleClass('disabled');
			}

		}

	});



	$('.lead-form1 .comboTreeItemTitle').each(function() {
		$(this).find("input:checked").trigger('click');
		var id1 = $(this).attr('data-id');
		var id = id1.split('_');
		if (jQuery.inArray(id[0], unique) !== -1) {
			$(this).find("input").trigger('click');
			if (assign_check == 0) {
				$(this).toggleClass('disabled');
			}

		}

	});


	$('.sel_ass .comboTreeItemTitle').each(function() {
		$(this).find("input:checked").trigger('click');
		var id1 = $(this).attr('data-id');
		var id = id1.split('_');
		if (jQuery.inArray(id[0], unique) !== -1) {
			$(this).find("input").trigger('click');
			if (assign_check == 0) {
				$(this).toggleClass('disabled');
			}

		}

	});

	$('.rem_ass .comboTreeItemTitle').each(function() {
		$(this).find("input:checked").trigger('click');
		var id1 = $(this).attr('data-id');
		var id = id1.split('_');
		if (jQuery.inArray(id[0], unique) !== -1) {
			$(this).find("input").trigger('click');
			if (assign_check == 0) {
				$(this).toggleClass('disabled');
			}

		}

	});

	$('.rem_edit .comboTreeItemTitle').each(function() {
		$(this).find("input:checked").trigger('click');
		var id1 = $(this).attr('data-id');
		var id = id1.split('_');
		if (jQuery.inArray(id[0], unique) !== -1) {
			$(this).find("input").trigger('click');
			if (assign_check == 0) {
				$(this).toggleClass('disabled');
			}

		}

	});
</script>

<!--/ Tree Select -->

<?php $this->load->view('leads/note_tab_custom_js'); ?>
<script type="text/javascript">
	$(document).ready(function() {

		$("#send_table").dataTable({
			"pageLength": "<?php echo get_firm_page_length() ?>",
			// "iDisplayLength": 10,
			// "scrollX": true
			// "scrollY":        "200px",
			// "scrollCollapse": true,
			// "paging":         false
		});

		$(".notes_contact_date").datepicker({
			dateFormat: 'dd-mm-yy'
		}).val();

		// $('#spl-datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
		//$('.spl-datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();

		$("body").delegate(".spl-datepicker", "focusin", function() {
			$(this).datepicker({
				dateFormat: 'dd-mm-yy',
				minDate: 0,
				changeMonth: true,
				changeYear: true,
			}, ).val();
			//var date = $('#datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();


		});

		$(".leads_popup").on('shown.bs.modal', function() {

			$("body").css("overflow-y", "hidden");
		});

		$('.leads_popup').on('hidden.bs.modal', function() {
			// do something…
			$("body").css({
				"overflow": "auto"
			});
		})



		//   var date = $('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
		/*$( ".datepicker" ).datepicker({
		       dateFormat: 'yy-mm-dd'
		   });*/

		$('.tags').tagsinput({
			allowDuplicates: true
		});

		$('.tags').on('itemAdded', function(item, tag) {
			$('.items').html('');
			var tags = $('.tags').tagsinput('items');

		});
	});

	$("#contact_today").click(function() {
		if ($("#contact_today").is(':checked')) {
			$(".selectDate").css('display', 'none');
		} else {
			$(".selectDate").css('display', 'block');
		}
	});

	//$(".contact_update_today").click(function(){
	$(document).on('click', '.contact_update_today', function() {
		var id = $(this).attr("data-id");
		if ($(this).is(':checked')) {

			$("#selectDate_update_" + id).css('display', 'none');
		} else {
			$("#selectDate_update_" + id).css('display', 'block');
		}
	});

	$("#all_leads").dataTable({
		"iDisplayLength": 10,
		"scrollX": true
	});
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$.validator.setDefaults({
			ignore: []
		});
	});
	$(document).ready(function() {

		$("#leads_form").validate({

			// rules: {
			//   lead_status: "required",  
			//   source: "required",  
			//   name: "required",  
			// },
			// messages: {
			//   lead_status: "Please enter your Status",
			//   source: "Please enter your Source",
			//   name: "Please enter your Name"
			// },
			errorPlacement: function(error, element) {
				if (element.attr("name") == "company")
					error.insertAfter(".dropdown-sin-4");
				else if (element.attr("name") == "lead_status")
					error.insertAfter(".dropdown-sin-1");
				else if (element.attr("name") == "source[]")
					error.insertAfter(".dropdown-sin-2");
				else if (element.attr("name") == "assigned[]")
					error.insertAfter(".dropdown-sin-3");

				else
					error.insertAfter(element);
			},
			rules: {
				//  lead_status: "required",  
				source: "required",
				name: "required",
				address: "required",
				description: "required",
				email_address: {
					required: true,
					email: true
				},
				phone: {
					required: true,
					number: true,

					minlength: 10,
					maxlength: 10
				},
				lead_status: {
					required: true,
				},
				company: {
					required: true,
					//   required: function(element) {

					//   if ($("#company").val()=='') {
					//      return false;
					//   }
					//   else {
					//   return true;
					//   }
					// },

				},
				//company:"required",
			},
			messages: {

				lead_status: "Please enter your Status",
				source: "Please enter your Source",
				name: "Please enter your Name",
				address: "Please enter your address",
				description: "Please enter your description",
				email_address: "Please enter Your Email",
				phone: "Please Enter Valid  Phone no",
				company: "Please Select Company",
			},

		});


		$(".edit_leads_form").validate({
			// rules: {
			//   lead_status: "required",  
			//   source: "required",  
			//   name: "required", 
			//   email_address:"required",
			//   phone:"required", 
			// },
			// messages: {
			//   lead_status: "Please enter your Status",
			//   source: "Please enter your Source",
			//   name: "Please enter your Name",
			//   email_address:"Please enter Your Email",
			//   phone:"Please enter Your Phone no",
			// },
			errorPlacement: function(error, element) {
				if (element.attr("name") == "company")
					error.insertAfter(".company_error");
				else if (element.attr("name") == "lead_status")
					error.insertAfter(".dropdown-sin-1");
				else if (element.attr("name") == "source[]")
					error.insertAfter(".dropdown-sin-2");
				else if (element.attr("name") == "assigned[]")
					error.insertAfter(".dropdown-sin-3");

				else
					error.insertAfter(element);
			},
			rules: {
				lead_status: "required",

				"source[]": "required",
				"assignees[]": "required",
				name: "required",
				address: "required",
				description: "required",
				email_address: {
					required: true,
					email: true
				},
				phone: {
					required: true,
					number: true,
					maxlength: 20
				},

				company: {
					required: true,
					//   required: function(element) {

					//   if ($("#company").val()=='') {
					//      return false;
					//   }
					//   else {
					//   return true;
					//   }
					// },

				},
				//company:"required",
			},
			messages: {
				lead_status: "Please enter your Status",
				"source[]": "Please enter your Source",
				"assignees[]": "Please enter your Assign",
				name: "Please enter your Name",
				address: "Please enter your address",
				description: "Please enter your description",
				email_address: "Please enter Your Email",
				phone: "Please enter Your Valid Phone no",
				company: "Please Select Company",
			},
			submitHandler: function(form) {

				var Assignees = [];
				$('.lead-form1 .comboTreeItemTitle input[type="checkbox"]:checked').each(function() {
					var id = $(this).closest('.lead-form1 .comboTreeItemTitle').attr('data-id');
					var id = id.split('_');
					Assignees.push(id[0]);
				});
				//  data['assignee'] = Assignees


				var assign_role = Assignees.join(',');
				$('#assign_role').val(assign_role);
				// data['assign_role'] = assign_role;

				//do something here
				$(".LoadingImage").show();
				var id = $("#leads_id").val();
				$.ajax({
					url: '<?php echo base_url(); ?>leads/editLeads/' + $("#leads_id").val(),
					type: 'post',
					data: $('#edit_leads_form').serialize(),
					success: function(data, status) {
						//alert(data);
						$(".LoadingImage").hide();
						$('#profile').html(data);
						// $('#profile_'+id).load('leads_detailed_tab_profile/'+id);
						$('#update_lead_' + id).modal('hide');
						//newly added ***/
						location.reload();
					}
				});
				//   return false;
			}
		});


		$(document).on('click', '.reminder_save', function(e) {
			e.preventDefault();
			//alert('ggg');
			var id = $(this).attr("data-id");
			var rem_date = $(".spl-datepicker_" + id).val();
			var rem_desc = tinyMCE.get("reminder_description_" + id).getContent();
			var rem_sub = $("#lead-reminder-subject").val();
			//var rem_desc1 = $("#tree_select2").val();

			var staff_id = $("#staff_" + id).val();
			if ($("#notify_by_email_" + id).prop('checked') == true) {
				var email_sent = "yes";
			} else {
				var email_sent = "no";
			}
			//alert(email_sent);
			var data = {};
			data['reminder_date'] = rem_date;
			data['reminder_desc'] = rem_desc;
			data['reminder_sub']  = rem_sub;
			data['lead_id'] = id;
			data['rel_type'] = 'Leads';
			data['email_sent'] = email_sent;
			data['staff_id'] = staff_id;

			var i = 0;
			var j = 0;

			var k = 0;
			if (rem_date == '') {
				i = 1;
				$('#reminder_date_err_' + id).html('Please select reminder date');
			} else {
				i = 0;
				$('#reminder_date_err_' + id).html('');
			}
			// if(rem_desc=='')
			// {
			//   k=1;
			//   $('#description_err_'+id).html('Please enter description');
			// }else{
			//   k=0;
			//   $('#description_err_'+id).html('');
			// }


			var Assignees = [];
			$('.rem_ass .comboTreeItemTitle input[type="checkbox"]:checked').each(function() {
				var id = $(this).closest('.rem_ass .comboTreeItemTitle').attr('data-id');
				var id = id.split('_');
				Assignees.push(id[0]);
			});
			data['assignee'] = Assignees;


			var assign_role = Assignees.join(',');
			//  alert(assign_role);
			if (assign_role == '') {
				j = 1;
				$('#assign_err_' + id).html('Please Select Assignees');
			} else {
				j = 0;
				$('#assign_err_' + id).html('');
			}

			data['assign_role'] = assign_role;

			if (i == 0 && j == 0) {
				$(".LoadingImage").show();

				$.ajax({
					url: '<?php echo base_url(); ?>leads/add_new_reminder/',
					type: 'POST',
					data: data,
					success: function(data) {
						$(".LoadingImage").hide();

						// $(".spl-datepicker_" + id).val('');
						// $("#reminder_description_" + id).val('');
						$("#staff_" + id).val('');
						$('#notify_by_email_' + id).prop('checked', false); // Unchecks it

						$('#add-reminders_' + id).modal('hide');

						$('.response_res').html(data);
						/** rspt 18-04-2018 **/
						$('.reminder_popups_' + id).html('');
						//$('.reminder_popups_'+id).load('reminder_edit_poup/'+id);
						$('.reminder_popups_' + id).load("<?php echo base_url(); ?>leads/reminder_edit_poup/" + id);
						/** end rspt 18-04-2018 **/
						$(".dob_picker").datepicker({
							dateFormat: 'dd-mm-yy',
							minDate: 0,
							changeMonth: true,
							changeYear: true,
						}).val();
						// ('#example2').DataTable()
						var tabletask1 = $(".example2").dataTable({
							"pageLength": "<?php echo get_firm_page_length() ?>",
							"iDisplayLength": 10,
							responsive: true
						});
					},
				});
			}

		});


		//$('.edit_reminder_save').click(function(e){
		$(document).on('click', '.edit_reminder_save', function(e) {
			e.preventDefault();

			//alert('ggg');
			var id = $(this).attr("data-id");
			var l_id = $(this).attr("data-value");
			//alert(id);
			var rem_date = $(".edit_spl-datepicker_" + id).val();
			var rem_desc = $("#edit_reminder_description_" + id).val();
			var staff_id = $("#edit_staff_" + id).val();
			var l_id = l_id;
			if ($("#edit_notify_by_email_" + id).prop('checked') == true) {
				var email_sent = "yes";
			} else {
				var email_sent = "no";
			}
			//alert(email_sent);
			var data = {};
			data['reminder_date'] = rem_date;
			data['reminder_desc'] = rem_desc;
			data['id'] = id;
			data['rel_type'] = 'Leads';
			data['email_sent'] = email_sent;
			data['staff_id'] = staff_id;
			data['lead_id'] = l_id;
			var i = 0;

			if (rem_date == '') {
				i = 1;
				$('#edit_reminder_date_err_' + id).html('Please select reminder date');
			} else {
				i = 0;
				$('#edit_reminder_date_err_' + id).html();
			}
			// if(rem_desc=='')
			// {
			//   j=1;
			//   $('#edit_description_err_'+id).html('Please enter description');
			// }else{
			//   j=0;
			//   $('#edit_description_err_'+id).html();
			// }


			var Assignees = [];
			$('.rem_edit .comboTreeItemTitle input[type="checkbox"]:checked').each(function() {
				var id = $(this).closest('.rem_edit .comboTreeItemTitle').attr('data-id');
				var id = id.split('_');
				Assignees.push(id[0]);
			});
			//  data['assignee'] = Assignees


			var assign_role = Assignees.join(',');
			data['assign_role'] = assign_role;
			// $('#assign_role').val(assign_role);


			if (i == 0) {
				$(".LoadingImage").show();

				$.ajax({
					url: '<?php echo base_url(); ?>leads/edit_reminder/',
					type: 'POST',
					data: data,
					success: function(data) {
						//alert(data);
						$(".LoadingImage").hide();

						$(".edit_spl-datepicker_" + id).val('');
						$("#edit_reminder_description_" + id).val('');
						$("#edit_staff_" + id).val('');
						$('#edit_notify_by_email_' + id).prop('checked', false); // Unchecks it

						$('#edit-reminders_' + id).modal('hide');
						$('.modal-backdrop.show').hide();
						$('.response_res').html(data);
						$('.reminder_popups_' + l_id).html('');
						$('.reminder_popups_' + l_id).load("<?php echo base_url(); ?>leads/reminder_edit_poup/" + l_id);
						$(".dob_picker").datepicker({
							dateFormat: 'dd-mm-yy',
							minDate: 0,
							changeMonth: true,
							changeYear: true,
						}).val();
						// ('#example2').DataTable()
						var tabletask1 = $(".example2").dataTable({
							"pageLength": "<?php echo get_firm_page_length() ?>",
							"iDisplayLength": 10,
							responsive: true
						});


					},
				});
			}

		});


	});


	$(".sortby_filter").click(function() {
		$(".LoadingImage").show();

		var sortby_val = $(this).attr("id");
		var data = {};
		data['sortby_val'] = sortby_val;
		$.ajax({
			url: '<?php echo base_url(); ?>leads/sort_by/',
			type: 'POST',
			data: data,
			success: function(data) {
				$(".LoadingImage").hide();

				$(".sortrec").html(data);
				$("#all_leads").dataTable({
					"iDisplayLength": 10,
				});
			},
		});

	});
	$("#import_leads").validate({
		rules: {
			file: {
				required: true,
				accept: "csv"
			},

			lead_status: "required",
			source: "required",
			name: "required",
		},
		messages: {
			file: {
				required: 'Required!',
				accept: 'Please upload a file with .csv extension'
			},

			lead_status: "Please enter your Status",
			source: "Please enter your Source",
			name: "Please enter your Name"
		},

	});
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.edit-toggle1').click(function() {
			$('.proposal-down').toggle();
		});

		$('#example,.example1,.example2').DataTable({
			"pageLength": "<?php echo get_firm_page_length() ?>",
		});
	})

	// $(document).on("click", function(event){
	//      var $trigger = $(".cmn-pro-menu");
	//      if($trigger !== event.target && !$trigger.has(event.target).length){
	//          $(".proposal-down").slideUp("fast");
	//      }            
	//  });


	$(".statuschange").click(function() {
		var status_val = $(this).attr("id");
		var id = $(this).attr("data-id");
		var data = {};
		data['status_val'] = status_val;
		data['id'] = id;
		$(".LoadingImage").show();
		$.ajax({
			url: '<?php echo base_url(); ?>leads/status_update/',
			type: 'POST',
			data: data,
			success: function(data) {
				$(".LoadingImage").hide();
				$('.dashboard_success_message').show();
				setTimeout(function() {
					$('.dashboard_success_message').hide();
				}, 1000);

				// $('.succ').css('display','block');
				// $('.succ').html('Status has been updated successfully');
				/** for convert to customer **/
				$("#convert_to_customer").css('display', 'block');
				/** end convert to cuc **/
				setTimeout(function() {
					$('.succ').fadeOut('fast');
				}, 1000);
				var cnt = data.split("-");
				$("#Lost").html(cnt[0]);
				$("#Junk").html(cnt[1]);
				$(".LoadingImage").hide();
				$(".proposal-down").css('display', 'none');
				$('.leadstatus').html(status_val);
			},
		});
	});


	$(".status_filter").click(function() {
		$(".LoadingImage").show();
		var filter_val = $(this).attr("id");
		var data = {};
		data['filter_val'] = filter_val;
		$.ajax({
			url: '<?php echo base_url(); ?>leads/filter_status/',
			type: 'POST',
			data: data,
			success: function(data) {

				$(".LoadingImage").hide();
				$(".sortrec").html(data);
				$("#all_leads").dataTable({
					"iDisplayLength": 10,
				});
			},
		});
	});

	$('.add_activity').click(function(e) {
		e.preventDefault();
		var i = $(this).attr("data-id");
		var values = $("#activity_log_" + i).val();
		if (values != '') {
			$(".error").hide();
			var data = {};
			data['values'] = values;
			data['id'] = i;
			data['module'] = "Lead";
			$.ajax({
				url: '<?php echo base_url(); ?>leads/add_activity_log/',
				type: 'POST',
				data: data,
				success: function(data) {
					$(".LoadingImage").hide();
					$('.activity_log').val('');
					$(".leads_activity_log").html(data);
				},
			});
		} else {
			$("#error" + i).html('<span>Please Enter Your Activity log.</span>');
		}
	});



	/* 09-03-2018 */
	$(".note_contact").click(function() {
		var i = $(this).attr("data-id");
		var radioValue = $("input[name='note_contact']:checked").val();
		/*alert(radioValue);
		alert(i);*/
		if (radioValue == "got_touch") {
			$('.note_date_' + i).css('display', 'block');
		} else {
			$('.note_date_' + i).css('display', 'none');
		}
	});

	$(document).on('click', '.add_note_rec_btn', function() {
		$('#hidden_lead_id').val($(this).data('id'));
	});

	$('.add_note_rec').click(function(e) {
		e.preventDefault();
		var i = $('#hidden_lead_id').val();
		// alert(i);
		var val = $("#leads_notes_msg").val();
		var contact_date = $("#note_contact_date_").val();
		//alert(val);
		if (val != '') {
			$(".LoadingImage").show();
			$("#error_note").hide();
			var data = {};
			data['values'] = val;
			data['id'] = i;
			data['contact_date'] = contact_date;
			$.ajax({
				url: '<?php echo base_url(); ?>leads/add_note/',
				type: 'POST',
				data: data,
				success: function(data) {
					$(".LoadingImage").hide();

					$("#leads_notes_msg").val('');
					$(".note_contact").prop('checked', false);
					$('.note_date_' + i).css('display', 'none');
					$(".add_note_rec_load").html(data);
					$('#add_note_rec_modal').modal('hide');
				},
			});
		} else {
			$("#error_note").html('Please Enter Your Notes.');
		}
	});



	$('.lead_attachments').on('change', function() {
		$(".LoadingImage").show();

		var i = $(this).attr("data-id");

		var file_data = $('#lead_attachments_70').prop('files')[0];
		var form_data = new FormData();
		form_data.append('file', file_data);
		form_data.append('id', i);
		/* var data={};
       data['form_data'] = form_data;
       data['id'] = i;*/

		$.ajax({
			url: '<?php echo base_url(); ?>leads/lead_attachment/', // point to server-side controller method

			dataType: 'text', // what to expect back from the server
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'post',
			success: function(response) {
				// alert(response);
				$(".LoadingImage").hide();
				$('.lead_attachments').val('');
				$('#lead_attachments').html(response); // display success response from the server
			},
			error: function(response) {
				$(".LoadingImage").hide();
				$('.lead_attachments').val('');

			}
		});
	});


	function delete_data(obj, fun) {
		$("#delete_id").val($(obj).attr("data-id"));
		$("#delete_fun").val(fun.trim());
	}

	function delete_action() {
		//if(confirm('Are you sure want to delete?')){
		$("#deleteconfirmation").modal("hide");
		$(".LoadingImage").show();

		var id = $("#delete_id").val();
		var fun = $("#delete_fun").val().trim();
		if (!fun) {
			console.log("delete controler undefined..");
			return;
		}
		$.ajax({
			url: '<?php echo base_url(); ?>leads/' + fun + '/', // point to server-side controller method      
			dataType: 'text', // what to expect back from the server       
			data: {
				'id': id
			},
			type: 'post',
			success: function(response) {
				//alert(fun);           
				if (fun == "leads_details_delete") {
					// alert("1");
					setTimeout(function() {
						window.location.href = "<?php echo base_url() ?>leads";
					}, 1500);
				} else if (fun == "delete_lead_attachment") {
					//alert("2");
					$('#lead_attachments').html(response); // display success response from the server
				} else if (fun == "delete_task") {
					//alert("3");
					$('.task_leadss').html(response);
				} else if (fun == "delete_reminder") {

					// alert("remn");
					$('.response_res').html(response);
				} else if (fun == 'delete_note') {
					//alert("5");           
					$('#Note-pad').val('');
					$('.note_d').val('');
					$(".note_contact").prop('checked', false);
					$('.note_contact_date_cnt').css('display', 'none');
					$(".add_note_rec_load").html(response);
				}
				$(".LoadingImage").hide();


			},
			error: function(response) {
				$(".LoadingImage").hide();
			}
		});
		//  }
	}



	function delete_reminder(l_id, id) {
		if (confirm('Are you sure want to delete?')) {
			$(".LoadingImage").show();
			var srt = l_id.split(',');
			var data = {};
			data['r_id'] = srt[1];
			data['l_id'] = srt[0];
			$.ajax({
				url: '<?php echo base_url(); ?>leads/delete_reminder/', // point to server-side controller method
				dataType: 'text', // what to expect back from the server
				data: data,
				type: 'post',
				success: function(response) {
					$(".LoadingImage").hide();
					$('.response_res').html(response); // display success response from the server
					var tabletask1 = $(".example2").dataTable({
						"pageLength": "<?php echo get_firm_page_length() ?>",
						"iDisplayLength": 10,
						responsive: true
					});
				},
				error: function(response) {
					$(".LoadingImage").hide();
				}
			});
		}
	}

	// $(document).on('change','.reminder_export',function(e){
	$(".reminder_export").click(function(e) {

		// alert('check');

		var type = $(this).attr("data-val");
		var leadId = $(this).attr("data-id");
		if (type == 'csv') {
			window.location.href = "<?php echo base_url() . 'leads/reminder_excel?leadid="+leadId+"' ?>";
		} else if (type == 'pdf') {
			window.location.href = "<?php echo base_url() . 'leads/reminder_pdf?leadid="+leadId+"' ?>";
		} else if (type == 'html') {
			window.open(
				"<?php echo base_url() . 'leads/reminder_html?leadid="+leadId+"' ?>",
				'_blank'
			);

		}


	});

	$(document).on('click', '.task_export', function(e) {
		var type = $(this).attr("data-val");
		var leadId = $(this).attr("data-id");
		if (type == 'csv') {
			window.location.href = "<?php echo base_url() . 'leads/l_task_excel?leadid="+leadId+"' ?>";
		} else if (type == 'pdf') {
			window.location.href = "<?php echo base_url() . 'leads/l_task_pdf?leadid="+leadId+"' ?>";
		} else if (type == 'html') {
			window.open(
				"<?php echo base_url() . 'leads/l_task_html?leadid="+leadId+"' ?>",
				'_blank' // <- This is what makes it open in a new window.
			);
			// window.location.href="<?php echo base_url() . 'user/task_html?status="+status+"&priority="+priority+"' ?>";
		}


	});

	// $(document).on('change','.task_leads',function(e)){
	$(document).on("click", ".task_leads", function(event) {
		event.preventDefault();

		var id = $(this).attr("data-id");
		//var formdata =  new FormData("#lead_task_"+leadId);

		if ($("#p-check_" + id).prop('checked') == true) {
			var public_tas = "Public";
		} else {
			var public_tas = "";
		}

		if ($("#p-check1_" + id).prop('checked') == true) {
			var billable_tas = "Billable";
		} else {
			var billable_tas = "";
		}
		if ($("#repeat_no_" + id).val() != '') {
			var repeat_no = $("#repeat_no_" + id).val();
		} else {
			var repeat_no = '';
		}

		if ($("#durations_" + id).val() != '') {
			var durations = $("#durations_" + id).val();
		} else {
			var durations = '';
		}
		if ($("#ends_on_" + id).val() != '') {
			var ends_on = $("#ends_on_" + id).val();
		} else {
			var ends_on = '';
		}
		if ($("#img_name_" + id).val() != '') {
			var file = $("#img_name_" + id).val();
		} else {
			var file = '';
		}
		var Assignees = [];
		$('.sel_ass .comboTreeItemTitle input[type="checkbox"]:checked').each(function() {
			var id = $(this).closest('.sel_ass .comboTreeItemTitle').attr('data-id');
			var id = id.split('_');
			Assignees.push(id[0]);
		});
		//  data['assignee'] = Assignees


		var assign_role = Assignees.join(',');

		if ($("#task_sub_" + id).val() != '' && $("#start_date_" + id).val() != '' && $("#due_date_" + id).val() != '' && assign_role != '') {
			//console.log(file_data);
			var data = {};
			data['task_sub'] = $("#task_sub_" + id).val();
			data['hour_rate'] = $("#hour_rate_" + id).val();
			data['start_date'] = $("#start_date_" + id).val();
			data['due_date'] = $("#due_date_" + id).val();
			data['priority'] = $("#priority_" + id).val();
			data['repeat'] = $("#repeat_" + id).val();
			// data['related_to'] = $("#related_to_"+id).val();
			data['lead_to'] = $("#lead_to_" + id).val();
			data['tags'] = $("#tags_" + id).val();
			data['description'] = $("#descr_" + id).val();
			data['repeat_no'] = repeat_no;
			data['durations'] = durations;
			data['ends_on'] = ends_on;
			data['billable'] = billable_tas;
			data['public'] = public_tas;
			data['lead_id'] = id;
			data['attach_file'] = file;


			//$('#assign_role').val(assign_role);
			data['assign_role'] = assign_role;

			//data['file_data'] = $('#inputfile_'+id).prop('files')[0];   


			$.ajax({
				url: '<?php echo base_url(); ?>leads/leads_task/',
				type: 'post',
				data: data,
				success: function(data, status) {
					$('#task_leadss_' + id).html(data);
					$('#addnew_task_lead_' + id).modal('hide');
					$("#task_sub_" + id).val('');
					$("#hour_rate_" + id).val('');
					$("#start_date_" + id).val('');
					$("#due_date_" + id).val('');
					$("#priority_" + id).val('');
					$("#repeat_" + id).val('');
					$("#related_to_" + id).val('');
					$("#lead_to_" + id).val('');
					$("#lead_to_" + id).closest("div").find(".dropdown-option").removeClass("dropdown-chose");
					$("#tags_" + id).val('');
					$("#descr_" + id).val('');
					$("#repeat_no_" + id).val('1');
					$("#durations_" + id).val('');
					$("#ends_on_" + id).val('');
					$('#inputfile_' + id).val('');
					$("#img_name_" + id).val('');

					var tabletask1 = $("#example1_" + id).dataTable({
						"pageLength": "<?php echo get_firm_page_length() ?>",
						"iDisplayLength": 10,
						responsive: true
					});
					$("#task_sub_err_" + id).hide();
					$("#task_start_err_" + id).hide();

					/** rspt 18-04-2018 **/
					$('.task_edit_popup').html('');
					$('.task_edit_popup').load("<?php echo base_url(); ?>leads/task_edit_popup/" + id);

					//$('.task_edit_popup').load('task_edit_popup/'+id);
					/** end 18-04-2018 **/
				},
				error: function(xhr, desc, err) {


				}
			});
		} else {
			//alert($("#task_sub_"+id).val());
			// alert($("#start_date_"+id).val());

			if ($("#task_sub_" + id).val() == '') {
				$("#task_sub_err_" + id).html('Enter Task Subject');
			} else {
				$("#task_sub_err_" + id).html(' ');
			}

			if ($("#start_date_" + id).val() == '') {
				$("#task_start_err_" + id).html('Enter Task Start Date');
			} else {
				$("#task_start_err_" + id).html(' ');
			}
			if ($("#due_date_" + id).val() == '') {
				$("#task_end_err_" + id).html('Enter Task End Date');
			} else {
				$("#task_end_err_" + id).html(' ');
			}
			//alert(assign_role);
			if (assign_role == '') {
				$("#task_assign_err_" + id).html('Select assignee');
			} else {
				$("#task_assign_err_" + id).html(' ');
			}



		}
	});


	$('.task_images').on('change', function() {
		var id = $(this).attr("data-id");
		var file_data = $('#inputfile_' + id).prop('files')[0];
		console.log($('#inputfile_' + id));
		var form_data = new FormData();
		form_data.append('file', file_data);
		$.ajax({
			//  url: 'ajaxupload/upload_file', // point to server-side controller method
			url: '<?php echo base_url(); ?>leads/leads_task_image/', // point to server-side controller method                      
			dataType: 'text', // what to expect back from the server
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'post',
			success: function(response) {
				$('#img_name').html(response);
			},
			error: function(response) {}
		});
	});



	$(document).on('change', '.repeated_too', function() {
		var id = $(this).attr("data-id");

		var x_cal = $(this).val();
		if (x_cal != '' && x_cal != 'custom') {
			$('#ends_on_div_' + id).css('display', 'block');
			$('#rep_no_' + id).css('display', 'none');
			$('#rep_cnt_' + id).css('display', 'none');
		} else if (x_cal != '' && x_cal == 'custom') {
			$('#rep_no_' + id).css('display', 'block');
			$('#rep_cnt_' + id).css('display', 'block');
			$('#ends_on_div_' + id).css('display', 'block');
		} else {
			$('#ends_on_div_' + id).css('display', 'none');
			$('#rep_no_' + id).css('display', 'none');
			$('#rep_cnt_' + id).css('display', 'none');
		}
	});
</script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript" src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>
<script src="<?php echo base_url() ?>assets/js/tinymce.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.tags').tagsinput({
			allowDuplicates: true
		});

		$('.tags').on('itemAdded', function(item, tag) {
			$('.items').html('');
			var tags = $('.tags').tagsinput('items');

		});


		$(function() {
			$('#date_of_creation1,#date_of_creation2').datepicker({
				dateFormat: 'dd-mm-yy',
				minDate: 0,
				changeMonth: true,
				changeYear: true,
			});
		});

		var slider = document.getElementById("myRange");
		var output = document.getElementById("demo");
		output.innerHTML = slider.value;

		slider.oninput = function() {
			output.innerHTML = this.value;
		}

		CKEDITOR.replace('editor4');

	});


	$(document).on("click", ".edit_task_leads", function(event) {
		event.preventDefault();

		var id = $(this).attr("data-id");
		//alert(id);
		var l_id = $(this).attr("data-value");
		//var formdata =  new FormData("#lead_task_"+leadId);

		if ($("#t_edit_p-check_" + id).prop('checked') == true) {
			var public_tas = "Public";
		} else {
			var public_tas = "";
		}

		if ($("#t_edit_p-check1_" + id).prop('checked') == true) {
			var billable_tas = "Billable";
		} else {
			var billable_tas = "";
		}
		if ($("#t_edit_repeat_no_" + id).val() != '') {
			var repeat_no = $("#t_edit_repeat_no_" + id).val();
		} else {
			var repeat_no = '';
		}

		if ($("#t_edit_durations_" + id).val() != '') {
			var durations = $("#t_edit_durations_" + id).val();
		} else {
			var durations = '';
		}
		if ($("#t_edit_ends_on_" + id).val() != '') {
			var ends_on = $("#t_edit_ends_on_" + id).val();
		} else {
			var ends_on = '';
		}
		/** my **/
		if (typeof ends_on === "undefined") {
			ends_on = '';
		}
		/** end **/
		if ($("#t_edit_img_name_" + id).val() != '') {
			var file = $("#t_edit_img_name_" + id).val();
		} else {
			var file = '';
		}
		if ($("#t_edit_task_sub_" + id).val() != '' && $("#t_edit_start_date_" + id).val() != '' && $("#t_edit_end_date_" + id).val() != '') {
			//console.log(file_data);
			var data = {};
			data['task_sub'] = $("#t_edit_task_sub_" + id).val();
			data['hour_rate'] = $("#t_edit_hour_rate_" + id).val();
			data['start_date'] = $("#t_edit_start_date_" + id).val();
			data['due_date'] = $("#t_edit_due_date_" + id).val();
			data['priority'] = $("#t_edit_priority_" + id).val();
			//     data['repeat'] = $("#t_edit_repeat_"+id).val();
			data['related_to'] = $("#t_edit_related_to_" + id).val();
			data['lead_to'] = $("#t_edit_lead_to_" + id).val();
			data['tags'] = $("#t_edit_tags_" + id).val();
			data['description'] = $("#t_edit_descr_" + id).val();
			/* data['repeat_no'] = repeat_no;
			 data['durations'] = durations;
			 data['ends_on'] = ends_on;*/
			data['billable'] = billable_tas;
			data['public'] = public_tas;
			data['lead_id'] = l_id;
			data['attach_file'] = file;
			data['attach_file'] = file;
			data['id'] = id;


			var Assignees = [];
			$('.edit_tasksclass .comboTreeItemTitle input[type="checkbox"]:checked').each(function() {
				var id = $(this).closest('.edit_tasksclass .comboTreeItemTitle').attr('data-id');
				var id = id.split('_');
				Assignees.push(id[0]);
			});
			//  data['assignee'] = Assignees


			var assign_role = Assignees.join(',');
			data['assign_role'] = assign_role;


			//data['file_data'] = $('#inputfile_'+id).prop('files')[0];   


			$.ajax({
				url: '<?php echo base_url(); ?>leads/update_task/',
				type: 'post',
				data: data,
				success: function(data, status) {
					$('#task_leadss_' + l_id).html(data);
					$('#edit_task_lead_' + l_id).modal('hide');
					$('.modal-backdrop.show').hide();


					$("#t_edit_task_sub_err_" + id).hide();
					$("#t_edit_task_start_err_" + id).hide();
					/** rspt 18-04-2018 **/
					$('.task_edit_popup').html('');
					//$('.task_edit_popup').load('task_edit_popup/'+id);
					$('.task_edit_popup').load("<?php echo base_url(); ?>leads/task_edit_popup/" + l_id);
					/** end 18-04-2018 **/
					var tabletask1 = $("#example1_" + l_id).dataTable({
						"pageLength": "<?php echo get_firm_page_length() ?>",
						"iDisplayLength": 10,
						responsive: true
					});


				},
				error: function(xhr, desc, err) {


				}
			});
		} else {
			//alert($("#task_sub_"+id).val());
			// alert($("#start_date_"+id).val());
			if ($("#t_edit_task_sub_" + id).val() == '') {
				$("#t_edit_task_sub_err_" + id).html('Enter Task Subject');
			} else {
				$("#t_edit_task_sub_err_" + id).hide();
			}
			if ($("#t_edit_start_date_" + id).val() == '') {
				$("#t_edit_task_start_err_" + id).html('Enter Task Start Date');
			} else {
				$("#t_edit_task_start_err_" + id).hide();
			}
			if ($("#t_edit_end_date_" + id).val() == '') {
				$("#t_edit_task_end_err_" + id).html('Enter Task Start Date');
			} else {
				$("#t_edit_task_end_err_" + id).hide();
			}
		}
	});


	$(document).on('change', '.e_task_images', function() {
		var id = $(this).attr("data-id");
		//alert(id);
		var file_data = $('#t_edit_inputfile_' + id).prop('files')[0];
		//console.log(file_data);
		var form_data = new FormData();
		form_data.append('file', file_data);
		$.ajax({
			//  url: 'ajaxupload/upload_file', // point to server-side controller method
			url: '<?php echo base_url(); ?>leads/leads_task_image/', // point to server-side controller method

			dataType: 'text', // what to expect back from the server
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'post',
			success: function(response) {
				$('#t_edit_img_name_' + id).val(response);
			},
			error: function(response) {}
		});
	});



	$(document).on('change', '.t_edit_repeated_too', function() {
		var id = $(this).attr("data-id");

		var x_cal = $(this).val();
		if (x_cal != '' && x_cal != 'custom') {
			$('#t_edit_ends_on_div_' + id).css('display', 'block');
			$('#t_edit_rep_no_' + id).css('display', 'none');
			$('#t_edit_rep_cnt_' + id).css('display', 'none');
		} else if (x_cal != '' && x_cal == 'custom') {
			$('#t_edit_rep_no_' + id).css('display', 'block');
			$('#t_edit_rep_cnt_' + id).css('display', 'block');
			$('#t_edit_ends_on_div_' + id).css('display', 'block');
		} else {
			$('#t_edit_ends_on_div_' + id).css('display', 'none');
			$('#t_edit_rep_no_' + id).css('display', 'none');
			$('#t_edit_rep_cnt_' + id).css('display', 'none');
		}
	});

	/** rspt convert to customer 18-04-2018 */
	// $(document).on('click','.convert_to_customer',function(){
	//  /** 14-08-2018 **/
	//  //$('#popup_convert_to_customer').modal('show');
	//  /** end of 14-08-2018 **/
	//   var id=$(this).attr('data-id');
	//   var status=$(this).attr('data-status');
	//   // alert(id);
	//   // alert(status);
	//   var data={};
	//   data['id']=id;
	//   data['status']=status;
	//   $.ajax({
	//       url: '<?php echo base_url(); ?>leads/convert_to_customer/',
	//       type: 'post',
	//        data: data,
	//        beforeSend:function(){$(".LoadingImage").show();},
	//       success: function (data, status)
	//       {
	//        $(".LoadingImage").hide();
	//         if(parseInt(data))
	//         {  
	//           $(".popup_info_box .position-alert1").html("Successfully Converted...");
	//           $(".popup_info_box").show();
	//           $('.convert_to_customer').css('display','none');
	//           // $('.lead_status_'+id).html(data);
	//           $('.lead_status_'+id).html('customer');

	//         }

	//       }
	//     });
	// });

	$(document).on('click', '.convert_to_status', function() {
		var id = $(this).attr('id');
		var status = $(this).attr('data-id');
		// alert(id);
		// alert(status);
		var data = {};
		data['id'] = id;
		data['status'] = status;
		$(".LoadingImage").show();
		$.ajax({
			url: '<?php echo base_url(); ?>leads/convert_to_status/',
			type: 'post',
			data: data,
			success: function(data, status) {
				$(".LoadingImage").hide();
				// alert(data);
				if (data != 'wrong') {
					$('#profile').html(data);
					//$('.').load()
					$('.leads_detailed_tab_profile_popup').load("<?php echo base_url(); ?>leads/leads_deatiled_tab_popup/" + id);
					// $('.convert_to_customer').css('display','none');
					// $('.lead_status_'+id).html(data);
					// $('.lead_status_'+id).each(function() {
					//      $(this).html(data);
					//   });

				}
				$('.dashboard_success_message').show();
				setTimeout(function() {
					$('.dashboard_success_message').hide();
				}, 1000);

				access_permission_function(); // footer function // 01-08-2018
			}
		});
	});
	/** end of 18-04-2018 **/
</script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dropdown.js"></script>
<script src="<?php echo base_url(); ?>assets/js/mock.js"></script>
<script>
	var Random = Mock.Random;
	var json1 = Mock.mock({
		"data|10-50": [{
			name: function() {
				return Random.name(true)
			},
			"id|+1": 1,
			"disabled|1-2": true,
			groupName: 'Group Name',
			"groupId|1-4": 1,
			"selected": false
		}]
	});

	$('.dropdown-mul-1').dropdown({
		data: json1.data,
		limitCount: 40,
		multipleMode: 'label',
		choice: function() {
			// console.log(arguments,this);
		}
	});

	var json2 = Mock.mock({
		"data|10000-10000": [{
			name: function() {
				return Random.name(true)
			},
			"id|+1": 1,
			"disabled": false,
			groupName: 'Group Name',
			"groupId|1-4": 1,
			"selected": false
		}]
	});

	$('.dropdown-mul-2').dropdown({
		limitCount: 5,
		searchable: false
	});

	/* $('.reminder_select_div').dropdown({     
	   searchable: false
	 });*/

	$('.dropdown-sin-1').dropdown({
		readOnly: true,
		input: '<input type="text" maxLength="20" placeholder="Search">'
	});
	$('.dropdown-sin-42').dropdown({
		limitCount: 50,
		input: '<input type="text" maxLength="20" placeholder="Search">'
	});

	$('.dropdown-sin-32').dropdown({
		limitCount: 50,
		input: '<input type="text" maxLength="20" placeholder="Search">'
	});

	$('.dropdown-sin-33').dropdown({
		limitCount: 50,
		input: '<input type="text" maxLength="20" placeholder="Search">'
	});

	$('.dropdown-sin-2').dropdown({
		limitCount: 50,
		input: '<input type="text" maxLength="20" placeholder="Search">'
	});
	$('.dropdown-sin-3').dropdown({
		limitCount: 50,
		input: '<input type="text" maxLength="20" placeholder="Search">'
	});
	$('.dropdown-sin-4').dropdown({
		limitCount: 50,
		input: '<input type="text" maxLength="20" placeholder="Search">'
	});
</script>
<!-- notes contact date section -->
<script type="text/javascript">
	$('.notes_contact_date').on('change', function() {
		//alert($(this).val());
		var data = {};
		var ajax_lead_id = '<?php echo $leads['id']; ?>';
		data['lead_id'] = ajax_lead_id;
		data['contact_date'] = $(this).val();
		$.ajax({
			url: '<?php echo base_url(); ?>leads/leads_notes_contactdate/',
			type: 'POST',
			data: data,
			success: function(data) {
				// alert(data);
				console.log(data);
			},
		});
	});
	$('.note_contact').on('click', function() {
		var radio_val = $(this).val();
		if (radio_val == 'got_touch') {

		}
		if (radio_val == 'not_touch') {
			/** ajax **/
			var data = {};
			var ajax_lead_id = '<?php echo $leads['id']; ?>';
			data['lead_id'] = ajax_lead_id;
			data['contact_date'] = '';
			$.ajax({
				url: '<?php echo base_url(); ?>leads/leads_notes_contactdate/',
				type: 'POST',
				data: data,
				success: function(data) {
					// alert(data);
					console.log(data);
				},
			});

			/** end of ajax **/

		}
	});
</script>
<!-- end of contact date -->
<!-- for time line search -->
<script type="text/javascript">
	$('#Note-search_timeline').on('keyup', function() {
		//alert('121');
		var txt = $(this).val();
		$('.activity_timeline_block').each(function() {
			if ($(this).text().toUpperCase().indexOf(txt.toUpperCase()) != -1) {
				$(this).show();

				//$(this).unwrap();

			} else {
				$(this).hide();
				//  $(this).wrap( "<!-- -->" );
				// $("<!-- ").insertAfter($(this));
				//  $(this).html('<!--'+$(this).html()+'-->');
			}
		});

	});
</script>
<!-- end of time line search -->

<script>  
  var tinymce_config = {    
    plugins: ' preview  paste importcss searchreplace autolink     visualblocks visualchars fullscreen image link table  hr pagebreak nonbreaking anchor toc  advlist lists imagetools textpattern noneditable ',
    menubar: 'file edit view insert format tools table',
    toolbar: 'fontselect fontsizeselect | forecolor backcolor | bold italic | alignleft aligncenter alignright alignjustify | insertfile image  link | numlist bullist  | hr | fullscreen  preview ',
    file_picker_types: "image"
  };

  $(document).ready(function(){    
    tinymce_config['selector'] = '#leads_notes_msg,#leads_edit_description,#descr_<?php echo $leads['id']; ?>,#reminder_description_<?php echo $leads['id']; ?>,#edit_reminder_description_<?php echo $e_k_value['id']; ?>';
    tinymce.init( tinymce_config );  
  });
</script>

<script>
	$(document).ready(function() {
	function ClearTimelineNotesSection() {
			tinymce.get("leads_notes_msg").setContent('');
			$("select#services_type").val('');
			$("input#call_date").val('');
			$("input[name='leads_notes_id']").val('');
			$(".nav-item.notes a").trigger('click');
		}
	$('ol.all_user1.md-tabs a').click(function() {
			
			$(this).closest('ol').find('a').removeClass('active');
			$(this).addClass('active');
			$('input[name="leads_notes_section"]').val($(this).data('value'));
		});

		$('#show_add_service_notes_popup').click(ClearTimelineNotesSection);
	});
</script>


<!-- proposal list -->
<script type="text/javascript">
	function Archived(archive) {
		var id = $(archive).attr('id');
		$.ajax({
			type: "POST",
			// dataType: "JSON",
			url: "<?php echo base_url() . 'Proposal_pdf/Archive_status'; ?>",
			data: {
				"id": id
			},
			beforeSend: function() {
				$(".LoadingImage").show();
			},
			success: function(data) {
				$(".LoadingImage").hide();
				$('.alert-success1').show();
				setTimeout(function() {
					$('.alert-success1').hide();
					location.reload();
				}, 3000);
				//alert(data);
			}

		});
	}

	$(document).ready(function() {
		$('.proposal_checkbox').on('click', function() {
			if ($(this).is(':checked', true)) {
				$(".delete_proposal_records").show();
			} else {
				$(".delete_proposal_records").hide();
			}
		});

		$('#select_all_proposal').on('click', function() {
			if ($(this).is(':checked', true)) {
				$(".delete_proposal_records").show();
				$(".proposal_checkbox").prop('checked', true);
			} else {
				$(".delete_proposal_records").hide();
				$(".proposal_checkbox").prop('checked', false);
			}
			$("#select_proposal_count").val($("input.proposal_checkbox:checked").length + " Selected");
		});

		$('.proposal_checkbox').on('click', function() {
			$("#select_proposal_count").val($("input.proposal_checkbox:checked").length + " Selected");
		});

		$('#delete_proposal_records').on('click', function() {
			var proposal = [];
			$(".proposal_checkbox:checked").each(function() {
				proposal.push($(this).data('proposal-id'));
			});
			if (proposal.length <= 0) {
				$('.alert-danger-check').show();
			} else {
				$('#delete_all_proposal' + proposal).show();
				$('.delete_proposal').click(function() {
					$('#delete_all_proposal' + proposal).hide();
				});
				$('.delete_yes').click(function() {
					var selected_proposal_values = proposal.join(",");

					$.ajax({
						type: "POST",
						url: "<?php echo base_url() . 'Proposal/deleteProposal'; ?>",
						cache: false,
						data: 'data_id=' + selected_proposal_values,
						beforeSend: function() {
							$(".LoadingImage").show();
						},
						success: function(data) {
							$(".LoadingImage").hide();
							var proposal_ids = data.split(",");
							for (var i = 0; i < proposal_ids.length; i++) {
								$("#" + proposal_ids[i]).remove();
							}
							$(".alert-success-delete").show();
							setTimeout(function() {
								location.reload();
							}, 500);
						}
					});
				});
			}
		});

	});
	$(document).ready(function() {
		$(".dob_picker").datepicker({
			dateFormat: 'dd-mm-yy',
			minDate: 0,
			changeMonth: true,
			changeYear: true,
		}).val();

		$('.new_task_class').click(function(e) {
			var id = $(this).data('target').split("_");
			$('#lead_task_' + id[3] + ' .tags-allow1 span').remove();
		});

		//$(document).on('change','.othercus',function(e){
	});

	function popup_fun(a, b) {
		//alert(a+"-"+b);
		if (b == 1) {
			$('#delete_attch_leads' + a).show();
		} else {
			$('#delete_attch_leads' + a).hide();
		}
	}

	/** 14-08-2018 **/

	$('.country_code').on('change input', function() {
		$(this).val($(this).val().replace(/([^+0-9]+)/gi, ''));
	});
	$('.telephone_number').on('change input', function() {
		$(this).val($(this).val().replace(/([^0-9]+)/gi, ''));
	});
	/** end of 14-08-2018 **/

	$(".autocomplete_client").keyup(debounce(function() {

		//type = $(this).data('type');   

		// if(type =='invoice' )
		var autoTypeNo = 0;
		// if(type =='invoice_number' )autoTypeNo=0;                         
		$(this).autocomplete({
			source: function(request, response) {
				$.ajax({
					url: '<?php echo base_url(); ?>/Leads/lead_SearchCompany',
					//dataType: "json",
					method: 'post',
					data: {
						name_startsWith: request.term,
						//   datatype:type,
						//type: type
					},
					success: function(data) {
						// alert(data);
						var dispn = $.parseJSON(data);
						response($.map(dispn, function(item) {
							var code = item.split("|");
							// alert(code[0]);           
							return {
								label: code[autoTypeNo],
								value: code[autoTypeNo],
								data: item
							}
						}));
					}
				});
			},
			autoFocus: true,
			minLength: 0,

			select: function(event, ui) {
				var names = ui.item.data.split("|");
				// alert(names[2]);

				$('#address').text(names[1]);
				$('#description').val(names[2]);
				$('.zip_code').val(names[3]);

			}
		});
	}, 500));

	
	$(document).on('click', '#add_note_rec_submit', function(e) {
		e.preventDefault();
      var msg = tinyMCE.get('leads_notes_msg').getContent();
      var lead_id = '<?php echo $leads['id']; ?>';
      $.ajax({
        url: "<?php echo base_url() ?>user/save_lead_notes_to_timeline",
        type: "POST",
        data: {
          "msg": msg,
          "lead_id": lead_id
        },
        beforeSend: function() {
          $(".LoadingImage").show();
        },
        success: function(response) {
          $(".LoadingImage").hide();
          $('.info_popup').show();
          $('.info_popup .position-alert1').html('Successfully saved in timeline..');
		  setTimeout(function() {
          $(".info_popup").hide();
          	location.reload();
		  }, 1500);
        }
      });
    });
</script>
<!-- end of pro list