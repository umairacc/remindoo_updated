<?php /* ***********************************************************/

 function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}
  $country = $leads['country'];
  $l_status = $leads['lead_status'];
  $status_name = $this->Common_mdl->select_record('leads_status','id',$l_status);
  $country_name = $this->Common_mdl->select_record('countries','id',$country);
  $languages = $this->Common_mdl->select_record('languages','id',$leads['default_language']);
 // $assigned = $this->Common_mdl->select_record('user','id',$leads['assigned']);
   $assigned = array();
   foreach (explode(',',$leads['assigned']) as $leads_key => $leads_value) {
      $assigned[] = $this->Common_mdl->select_record('user','id',$leads_value);
   }
   

       $assigned = array();
     $team = array();
      $dept = array();
   foreach (explode(',',$leads['assigned']) as $leads_key => $leads_value) {
      $assigned[] = $this->Common_mdl->select_record('user','id',$leads_value);
   }
      foreach (explode(',',$leads['team']) as $team_key => $team_value) {
      $team[] = $this->Common_mdl->select_record('team','id',$team_value);
   }
      foreach (explode(',',$leads['dept']) as $dept_key => $dept_value) {
      $dept[] = $this->Common_mdl->select_record('department_permission','id',$dept_value);
   }
  
  ?>    


  <div class="modal-alertsuccess alert  succ popup_info_box" style="display:none;">
  <div class="newupdate_alert">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
         <div class="pop-realted1">
         <div class="position-alert1">
              </div>
         </div>
         </div>
   </div>

            <div class="edit-section">
              <div class="mark-lost1 floating_set">
                <div class="pull-left edit-pull-left">
                  <ul>
                    <li><a class="btn btn-primary" href="#" data-toggle="modal" data-target="#update_lead_<?php echo $leads['id'];?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i>Edit 
                    </a>
                    </li>
                   <li>
                      <a href="#" class="btn btn-primary edit-toggle1">More <span class="caret"></span></a>
                      <ul class="proposal-down">
                        <!--  <li><a href="#" id="lost" data-id="<?php echo $leads['id'];?>" class="statuschange">Mark as lost</a></li>
                         <li><a href="#" id="junk" data-id="<?php echo $leads['id'];?>" class="statuschange"> Mark as junk</a></li> -->
                <?php foreach($leads_status as $ls_value) { ?>
                      <li><a href="#" id="<?php echo $leads['id'];?>" data-id="<?php echo $ls_value['id'];?>" class="convert_to_status"><?php echo $ls_value['status_name'];?></a></li>
                <?php } ?>
                         <li><a href="<?php echo base_url().'leads/delete/'.$leads['id'].'/0';?>" onclick="return confirm('Are you sure want to delete');" > Delete Lead</a></li>
                      </ul>
                   </li>
                   <li>
                      <button type="button" id="convert_to_customer" name="convert_to_customer" data-id="<?php echo $leads['id'];?>" data-status="<?php echo $status_name['status_name'];?>" class="convert_to_customer btn btn-card btn-primary" <?php if(strtolower($status_name['status_name'])=='customer'){?> style="display: none;" <?php }?> >Convert To Customer</button>
                   </li>
                  </ul>
                </div>
                <!-- <div class="pull-right convert-custom">
                  <a href="#"> Convert to customer </a>
                  </div> -->
              </div>
            </div>
            <div class="floating_set general-inform1 leads-t-details">
            <div class="col-xs-12 col-md-6 leads-seperateM">
              <div class="box-newalign1">
             <div class="for-block">
              <h4 ="nibor">general information</h4>
             </div>
              <div class="alert alert-success succ" style="display:none;">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              </div>
              <div class="col-xs-12 bot-sec">
              <div class="col-sm-6 custom-fields11">
                <div class="custom-danger">
                  <!-- <h4>Information</h4> -->
                  <div class="form-status1">
                    <span>Lead Status</span>
                    <strong class="leadstatus lead_status_<?php echo $leads['id']; ?>"><?php echo $status_name['status_name'];?></strong>
                  </div>
                  <div class="form-status1">
                    <span>Position</span>
                    <strong><?php echo $leads['position'];?></strong>
                  </div>
                  <div class="form-status1">
                    <span>Email Address</span>
                    <strong><a href="#"><?php echo $leads['email_address'];?></a></strong>
                  </div>
                  <div class="form-status1">
                    <span>Website</span>
                    <strong><a href="#"><?php echo $leads['website'];?></a></strong>
                  </div>
                  <div class="form-status1">
                    <span>Phone</span>
                    <strong><a href="#"><?php echo $leads['phone'];?></a></strong>
                  </div>
                  <!-- <div class="form-status1">
                    <span>Company</span>
                    <strong><?php echo $leads['company'];?></strong>
                  </div> -->
                    <div class="form-status1">
                                             <span>Company</span>
                                             <!-- <strong><?php //echo $leads['company'];
  /*
                                             $query=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and id=".$leads['company']." order by id desc ");    
                               $results = $query->result_array();
                               if(count($results)>0)
                               {
                                   echo $results[0]['crm_company_name'];
                               }
                               */
                              
                                             ?></strong> -->
                                             <strong><?php echo $leads['company']; ?></strong>
                    </div>
                  <div class="form-status1">
                    <span>Address</span>
                    <strong><?php echo $leads['address'];?></strong>
                  </div>
                  <div class="form-status1">
                    <span>City</span>
                    <strong><?php echo $leads['city'];?></strong>
                  </div>
                  <div class="form-status1">
                    <span>State</span>
                    <strong><?php echo $leads['state'];?></strong>
                  </div>
                  <div class="form-status1">
                    <span>Country</span>
                    <strong><?php echo $country_name['name'];?></strong>
                  </div>
                  <div class="form-status1">
                    <span>Zip code</span>
                    <strong><?php echo $leads['zip_code'];?></strong>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 custom-fields11 general-leads1">
                <div class="custom-danger">
                  <!-- <h4>General Information</h4> -->
                  <!-- <div class="form-status1">
                    <span>Lead Status</span>
                    <strong>New</strong>
                    </div>   -->
                  <div class="form-status1">
                    <span>Source</span>
                    <strong><?php 
                      //  $source_name = $this->Common_mdl->GetAllWithWhere('source','id',$leads['source']);
                      //  if(!empty($source_name)){
                      //    $leads['source'] = $source_name[0]['source_name'];
                      //     if($leads['source']!='')
                      //     {
                      //       $leads['source'] = $leads['source'];
                      //     }else{
                      //       $leads['source'] = '-';
                      //     }
                      //  }
                      //  else
                      //  {
                      //   $leads['source']='-';
                      //  }
                     
                      // echo $leads['source'];
                     $leads_source=array();
                                                foreach (explode(',',$leads['source']) as $source_key => $source_value) {
                                                   $source_name = $this->Common_mdl->GetAllWithWhere('source','id',$source_value);
                                                     if(!empty($source_name)){
                                                   $leads_res = $source_name[0]['source_name'];
                                                    if($leads_res!='')
                                                    {
                                                      $leads_source[] = $leads_res;
                                                    }
                                                   }
                                                  }
                                                  if(count($leads_source)>0)
                                                  {
                                                    echo implode(',',$leads_source);
                                                  }
                                                  else
                                                  {
                                                    echo "-";
                                                  }
                      ?></strong>
                  </div>
                  <div class="form-status1">
                    <span>Default Language</span>
                    <strong><?php echo $languages['name'];?></strong>
                  </div>
                  <div class="form-status1">
                    <span>Assigned</span>
                    <strong><?php 
                                           //  echo $assigned['crm_name'];
                                             $names=array();
                                             $teams=array();
                                             $departments=array();
                                             if(count($assigned)>0){
                                               foreach ($assigned as $ass_key => $ass_value) {
                                                $names[]=$ass_value['crm_name'];
                                               }
                                                echo implode(',', $names);
                                             }
                                            
                                            if(count($team)>0){
                                               foreach ($team as $ass_key => $ass_value) {
                                                $teams[]=$ass_value['team'];
                                               }
                                                 echo implode(',', $teams);
                                             }
                                           
                                            if(count($dept)>0){
                                               foreach ($dept as $ass_key => $ass_value) {
                                                $departments[]=$ass_value['new_dept'];
                                               }
                                                echo implode(',', $departments);
                                             }
                                            
                                             ?></strong>
                  </div>
                  <div class="form-status1">
                    <span>Tags</span>
                    <strong>
                    <?php $tag =explode(',', $leads['tags']);
                      foreach ($tag as $t_key => $t_value) { ?>
                    <a href="#"><?php echo $t_value;?></a><?php } ?></strong>
                  </div>
                  <div class="form-status1">
                    <span>Created</span>
                    <strong><?php  $date=date('Y-m-d H:i:s',$leads['createdTime']);
                      echo time_elapsed_string($date);?></strong>
                  </div>
                  <div class="form-status1">
                    <span>Last Contact</span>
                    <strong>-</strong>
                  </div>
                  <div class="form-status1">
                    <span>Public</span>
                    <strong><?php if($leads['public']!=''){ echo $leads['public']; }else{ echo 'No'; }?></strong>
                  </div>
                </div>
              </div>
              </div>
            </div>
            </div>
              <!-- rspt 18-04-2018 convert to customer -->

              <!-- 18-04-2018 end -->
              <!-- <div class="col-sm-4 custom-fields11">
                <div class="custom-danger">
                  <h4>Custom Fields</h4>
                  <div class="form-status1">
                    <span>Dedicated support</span>
                    <strong>-</strong>
                  </div>  
                </div>
                </div> -->
            <div class="col-xs-12 col-md-6 leads-seperateM">

              <div class="description-lead1 col-sm-12">
                <div class="box-newalign1">
              <div class="for-block">
                <h4>Description</h4>
              </div>
                <p><?php echo $leads['description'];?> </p>
              </div>
            </div>
              <div class="activity1 col-sm-12">
                <div class="box-newalign1">
                <div class="for-block">
                  <h4>Latest Activity</h4>
                </div>
                <!-- <span>Gust Toy - created lead</span> -->
                <!-- <span><?php  $date=date('Y-m-d H:i:s',$latestrec[0]['CreatedTime']);
                  echo time_elapsed_string($date);?></span>
                <span><?php
                  $l_id = $latestrec[0]['module_id'];
                    $l_name = $this->Common_mdl->select_record('leads','id',$l_id);
                  
                   echo $l_name['name'] . ' - ' . $latestrec[0]['log'];?></span> -->
    <?php 
  // $latestrecs = $this->db->query("SELECT * FROM activity_log where module='Lead' and user_id='".$_SESSION['admin_id']."' and module_id='".$leads['id']."' ORDER BY `id`  DESC limit 5")->result_array();
   $latestrecs = $this->db->query("SELECT * FROM activity_log where module='Lead' and user_id='".$_SESSION['admin_id']."' and module_id='".$leads['id']."' and sub_module!='Lead Notes' ORDER BY `id`  DESC limit 5")->result_array();

          
           ?><?php
            // foreach ($latestrecs as $late_key => $late_value) { 
            //      $date=date('d F Y h:i A',$late_value['CreatedTime']);
            //        $l_id = $late_value['module_id'];
            //        $l_name = $this->Common_mdl->select_record('leads','id',$l_id);
            
            //        }
?>

                   <div class="table-responsive m-t-20">
                        <table class="table m-b-0 f-14 b-solid requid-table" style="display: none;">
                            <thead>
                              <tr class="text-uppercase">
                                 <th class="text-center">#</th>
                                 <th class="text-center">Action</th>
                                 <th class="text-center">Date</th>
                                 <th class="text-center">Description</th>
                              </tr>
                           </thead>
                     <tbody class="text-center text-muted">
                           <?php $i=1;
                           if(count($latestrecs)>0){
                            foreach ($latestrecs as $late_key => $late_value) {
                              $date=date('d F Y h:i A',$late_value['CreatedTime']);
                             if($late_value['sub_module']=='')
                              {
                                $sub_module='Lead';
                                $sub_log=$late_value['log'];
                                $l_name = $this->Common_mdl->select_record('leads','id',$l_id);
                                 if($late_value['log']=='New Lead Created')
                              {
                                $des= "New Lead ".$l_name['name']." is Created";
                              }
                              if($late_value['log']=='Lead Records Updated')
                              {
                                $des= "New Lead ".$l_name['name']." is Updated";
                                } 
                              }
                              else
                              {
                                $sub_module=$late_value['sub_module'];
                                if($late_value['sub_module']=='Lead Attachment')
                                {
                                     $ex_val=explode('---',$late_value['log']);
                                      $log_activity=$ex_val[0];
                                      $log_img=$ex_val[1];
                                      $ext_val = explode('.', $log_img);
                                      $ext=end($ext_val);
                                      $img_array=array("jpeg","JPEG","jpg","JPG","png","PNG","gif","GIF");
                                      if(in_array($ext, $img_array))
                                      {
                                        $img_res='yes';
                                        $img_name="";
                                      }
                                      else
                                      {
                                        $img_res='not';
                                        $img_name=$log_img;
                                      }
                                      if($log_activity=='Lead Attached')
                                      {
                                        $des="You Have Attached ".$img_name." File  ";
                                      }
                                      else
                                      {
                                        $des="You have Deleted ".$img_name." File  ";
                                      }
                                }
                                if($late_value['sub_module']=='Reminder')
                                {
                                    $reminder_data=$this->Common_mdl->select_record('lead_reminder','id',$late_value['sub_module_id']);
                                    $reminder_res=$reminder_data['assigned_staff_id'];
                                    $user_data=$this->Common_mdl->select_record('user','id',$reminder_res);
                                    if($reminder_res!=''){
                                        $res_reminder="yes";
                                      }
                                      else
                                      {
                                        $res_reminder="no";
                                      }
                                      $ex_val=explode('---',$late_value['log']);
                                      $user_data=$this->Common_mdl->select_record('user','id',$ex_val[1]);

                                      if($ex_val[0]=='Reminder Imported')
                                      {
                                        $des= "You Have Assigned a Reminder To  ".$user_data['crm_name']."   ";
                                      }
                                      elseif($ex_val[0]=='Reminder Updated')
                                      {
                                        $des= "You Have Updated Assigned a Reminder To ".$user_data['crm_name']."   ";
                                      }
                                      else
                                      {
                                      
                                        $des= "You have Deleted Assigned a Reminder of ".$user_data['crm_name']."  ";
                                      }

                                  }

                                  if($late_value['sub_module']=='Task')
                                  {
                                      $task_data=$this->Common_mdl->select_record('add_new_task','id',$late_value['sub_module_id']);
                                      $log_img=$task_data['attach_file'];
                                      if($log_img!=''){
                                      $ext_val = explode('.', $log_img);
                                      $ext=end($ext_val);
                                      $img_array=array("jpeg","JPEG","jpg","JPG","png","PNG","gif","GIF");
                                      if(in_array($ext, $img_array))
                                      {
                                        $img_res='yes';
                                        $img_name="";
                                      }
                                      else
                                      {
                                        $img_res='not';
                                        $img_name=$log_img;
                                      }
                                    }
                                    else
                                    {
                                      $img_res="no";
                                    }
                                    $ex_val=explode("---",$late_value['log']);
                                     if($ex_val[0]=='Task Created')
                                    {
                                      $des="You Have Created ".$ex_val[1]."   ";
                                    }
                                    elseif($ex_val[0]=='Task Records Updated')
                                    {
                                      $des="You Have Updated ".$ex_val[1]."   ";
                                    }
                                    else
                                    {
                                      $ex_val=explode('---',$late_value['log']);

                                      $des="You have Deleted ".$ex_val[1]."  ";
                                    }

                                }

                              }
                           ?>
                             <tr>
                                 <td><?php echo $i;?></td>
                                 <td><?php echo $sub_module;?></td>
                                 <td> <i class="icofont icofont-ui-calendar"></i>&nbsp; <?php echo $date;?></td>
                                 <td><?php echo $des;?></td>
                              </tr>
                            <?php
                            $i++;
                            }
                            }
                            else{
                              ?>
                              <tr><td colspan="4" align="text-center">No Record Found</td></tr>
                              <?php
                              } ?>
                           <!--    <tr>
                                 <td>1</td>
                                 <td>Design mockup</td>
                                 <td> <i class="icofont icofont-ui-calendar"></i>&nbsp; 22 December, 16</td>
                                 <td>The standard Lorem Ipsum passage</td>
                              </tr> -->
                             
                           </tbody>
                        </table>
                     </div>
                     <!-- static activity -->

                                    <div class="main-timeline">
                                                            <div class="cd-timeline cd-container">
                                              <?php $i=1;
                                                if(count($latestrecs)>0){
                                                 foreach ($latestrecs as $late_key => $late_value) {
                                                   //$date=date('d F Y h:i A',$late_value['CreatedTime']);
                                                     $date=date('F d Y h:i A',$late_value['CreatedTime']);
                                                  if($late_value['sub_module']=='')
                                                   {
                                                     $sub_module='Lead';
                                                     $sub_log=$late_value['log'];
                                                     $l_name = $this->Common_mdl->select_record('leads','id',$l_id);
                                                      if($late_value['log']=='New Lead Created')
                                                   {
                                                     $des= "New Lead ".$l_name['name']." is Created";
                                                   }
                                                   if($late_value['log']=='Lead Records Updated')
                                                   {
                                                     $des= "New Lead ".$l_name['name']." is Updated";
                                                     }
                                                   if($late_value['log'] =='Lead Imported')
                                                     {
                                                      $des='New Lead is Imported';
                                                     }
                                                   }
                                                   else
                                                   {
                                                     $sub_module=$late_value['sub_module'];
                                                     if($late_value['sub_module']=='Lead Attachment')
                                                     {
                                                          $ex_val=explode('---',$late_value['log']);
                                                           $log_activity=$ex_val[0];
                                                           $log_img=$ex_val[1];
                                                           $ext_val = explode('.', $log_img);
                                                           $ext=end($ext_val);
                                                           $img_array=array("jpeg","JPEG","jpg","JPG","png","PNG","gif","GIF");
                                                           if(in_array($ext, $img_array))
                                                           {
                                                             $img_res='yes';
                                                             $img_name="";
                                                           }
                                                           else
                                                           {
                                                             $img_res='not';
                                                             $img_name=$log_img;
                                                           }
                                                           if($log_activity=='Lead Attached')
                                                           {
                                                             $des="You Have Attached ".$img_name." File  ";
                                                           }
                                                           else
                                                           {
                                                             $des="You have Deleted ".$img_name." File  ";
                                                           }
                                                     }
                                                     if($late_value['sub_module']=='Reminder')
                                                     {
                                                         $reminder_data=$this->Common_mdl->select_record('lead_reminder','id',$late_value['sub_module_id']);
                                                         $reminder_res=$reminder_data['assigned_staff_id'];
                                                         $user_data=$this->Common_mdl->select_record('user','id',$reminder_res);
                                                         if($reminder_res!=''){
                                                             $res_reminder="yes";
                                                           }
                                                           else
                                                           {
                                                             $res_reminder="no";
                                                           }
                                                           $ex_val=explode('---',$late_value['log']);
                                                           $user_data=$this->Common_mdl->select_record('user','id',$ex_val[1]);
                                                
                                                           if($ex_val[0]=='Reminder Imported')
                                                           {
                                                             $des= "You Have Assigned a Reminder To  ".$user_data['crm_name']."   ";
                                                           }
                                                           elseif($ex_val[0]=='Reminder Updated')
                                                           {
                                                             $des= "You Have Updated Assigned a Reminder To ".$user_data['crm_name']."   ";
                                                           }
                                                           else
                                                           {
                                                           
                                                             $des= "You have Deleted Assigned a Reminder of ".$user_data['crm_name']."  ";
                                                           }
                                                
                                                       }
                                                
                                                       if($late_value['sub_module']=='Task')
                                                       {
                                                           $task_data=$this->Common_mdl->select_record('add_new_task','id',$late_value['sub_module_id']);
                                                           $log_img=$task_data['attach_file'];
                                                           if($log_img!=''){
                                                           $ext_val = explode('.', $log_img);
                                                           $ext=end($ext_val);
                                                           $img_array=array("jpeg","JPEG","jpg","JPG","png","PNG","gif","GIF");
                                                           if(in_array($ext, $img_array))
                                                           {
                                                             $img_res='yes';
                                                             $img_name="";
                                                           }
                                                           else
                                                           {
                                                             $img_res='not';
                                                             $img_name=$log_img;
                                                           }
                                                         }
                                                         else
                                                         {
                                                           $img_res="no";
                                                         }
                                                         $ex_val=explode("---",$late_value['log']);
                                                          if($ex_val[0]=='Task Created')
                                                         {
                                                           $des="You Have Created ".$ex_val[1]."   ";
                                                         }
                                                         elseif($ex_val[0]=='Task Records Updated')
                                                         {
                                                           $des="You Have Updated ".$ex_val[1]."   ";
                                                         }
                                                         else
                                                         {
                                                           $ex_val=explode('---',$late_value['log']);
                                                
                                                           $des="You have Deleted ".$ex_val[1]."  ";
                                                         }
                                                
                                                     }
                                                
                                                   }
                                                ?>
                                                                <div class="cd-timeline-block">
                                                                    <div class="cd-timeline-icon bg-primary">
                                                                        <i class="icofont icofont-ui-file"></i>
                                                                    </div>
                                                                    <div class="cd-timeline-content card_main">
                                                                      
                                                                        <div class="p-20">
                                                                            <h6><?php echo $des;?></h6>
                                                                        </div>
                                                                        <span class="cd-date"><?php echo $date;?></span>
                                                                        <span class="cd-details">Your Activity Info</span>
                                                                    </div>
                                                                </div>
                                                <?php
                                                $i++;
                                                }
                                                }
                                                ?>
                                                                
                                                     <!--            <div class="cd-timeline-block">
                                                                    <div class="cd-timeline-icon bg-warning">
                                                                        <i class="icofont icofont-ui-file"></i>
                                                                    </div>
                                                                    <div class="cd-timeline-content card_main">
                                                                        <div class="p-20">
                                                                            <h6>We’d like to introduce our new website</h6>
                                                                        </div>
                                                                        <span class="cd-date">November 12, 2014, 11:27 AM</span>
                                                                        <span class="cd-details">You  posed an artical with public</span>
                                                                    </div>
                                                                  </div> -->
                                                            </div>
                                                            <!-- cd-timeline -->
                                                        </div>
                                        <!-- static activity -->

              </div>
            </div>
          </div>
            </div>
<script type="text/javascript">
  $(document).ready(function(){
      $('.edit-toggle1').click(function(){
        $('.proposal-down').slideToggle(300);
      });
  
  $('#example,.example1,.example2').DataTable();
  })
  
  
  $(".statuschange").click(function(){
  var status_val = $(this).attr("id");
  var id = $(this).attr("data-id");
  var data={};
  data['status_val'] = status_val;
  data['id'] = id;
  
  $.ajax({
      url: '<?php echo base_url();?>leads/status_update/',
      type : 'POST',
      data : data,
      success: function(data) {
        $('.succ').css('display','block');
        $('.succ').html('Status has been updated successfully');
        /** for convert to customer **/
          $("#convert_to_customer").css('display','block');
        /** end convert to cuc **/
         setTimeout(function() {
   $('.succ').fadeOut('fast');
  }, 1000);
          var cnt = data.split("-");
          $("#Lost").html(cnt[0]);
          $("#Junk").html(cnt[1]);
        $(".LoadingImage").hide();
  $(".proposal-down").css('display','none');
  $('.leadstatus').html(status_val);
      },
      });
  });
</script>