<?php $this->load->view('includes/header');
$succ = $this->session->flashdata('success');
$error = $this->session->flashdata('error');

/*function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}*/
?>



<div class="leads-section floating_set">
<div class="proposal-sent08 floating_set">
<div class="addnewclient_pages1 floating_set">
    
<!-- <ul class="nav nav-tabs all_user1 md-tabs floating_set" id="tabs">
<li class="nav-item it0 bbs" value="0"><a class="nav-link active" data-toggle="tab" 
href="#dashboard">Dashboard</a></li>
<li class="nav-item it1" value="1"><a class="nav-link" data-toggle="tab" href="#lead-source">Lead Sources</a></li> 
<li class="nav-item it2 hide" value="2"><a class="nav-link" data-toggle="tab" href="#lead-status">Lead Status</a></li> 
</ul> -->   
<!-- tab close -->
</div>

<div class="management_section floating_set ">



<!-- tab start -->
<div class="tab-content">
<div id="dashboard" class="tab-pane fade in active">

<div class="switch-list01 floating_set">
	<?php if($succ){?>
			<div class="alert alert-success">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo $succ; ?>
			</div>
			<?php }if($error){?>
			<div class="alert alert-danger">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo $error; ?>
			</div>
			<?php } ?>

			<div class="deadline-crm1 floating_set">
	<div class="pull-left">
		<ul class="news-leads">
		<li><a href="#" data-toggle="modal" data-target="#new-lead">NEW LEAD</a></li>
		<li><a href="#" data-toggle="modal" data-target="#import-lead">IMPORT LEADS</a></li>
		<!-- <li class="edit-op"><a href="#"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a></li> -->
		<li class="edit-op"><a href="<?php echo base_url()?>leads/">SWITCH TO LIST</a></li>
	</ul>
	</div>
	<div class="lead-data1 pull-right">
			<!-- <div class="junk-lead">
					<div class="lead-point1">
					<span>8</span>
					<strong>New</strong>
					</div>
					
				</div>	
				
				<div class="junk-lead">
					<div class="lead-point1">
					<span>9</span>
					<strong>Connected</strong>
					</div>
				</div>	
				
				<div class="junk-lead">
					<div class="lead-point1">
					<span>10</span>
					<strong>Qualified</strong>
					</div>
				</div>	
				
				<div class="junk-lead color-junk1">
					<div class="lead-point1">
					<span>6</span>
					<strong>Trial Starded</strong>
					</div>
				</div>
 -->


				<!-- <div class="junk-lead color-junk2">
					<div class="lead-point1">
					<span>0</span>
					<strong>Proposal Sent</strong>
					</div>
				</div> -->

 <?php foreach($leads_status as $ls_value) {
 	$s_id = $ls_value['id'];
 	$sql = $this->db->query('select count(*) as i from leads where lead_status="'.$s_id.'"')->row_array();
 	//echo $this->db->last_query();
  ?>
				<div class="junk-lead color-junk2">
					<div class="lead-point1">
					<span id="<?php echo $ls_value['status_name'];?>"><?php echo $sql['i'];?></span>
					<strong id="<?php echo $ls_value['id'];?>" class="status_filter"><?php echo $ls_value['status_name'];?></strong>
					</div>
				</div>
				<?php } ?>
				
<!-- 				<div class="junk-lead color-junk2">
					<div class="lead-point1">
					<span>0.000%</span>
					<strong>Lost Leads</strong>
					</div>
				</div>

				<div class="junk-lead color-junk2">
					<div class="lead-point1">
					<span>0.000%</span>
					<strong>Junk Leads</strong>
					</div>
				</div> -->

				
			</div>
	
	
	</div>
	
	<!-- <div class="pull-right leads-search1">
		<form>
			<input type="search" name="search" placeholder="Search Leads">
		</form>	
	</div> -->
</div> <!-- switch-list01 -->
 

<div class="lead-summary2 floating_set">		

<div class="sort-by floating_set">
	<span>Sort By:</span> <strong> <span id="date_created" class="sortby_filter"> Data Created </span> <!--already had but include it --><span id="kan_ban_order" class="sortby_filter"> Kan Ban Order </span><!-- end -->  <span id="last_contact" class="sortby_filter"> Last Contact </span></strong>
</div>	

<div class="lead-summary3 floating_set  sortby_kanban">
	<div class="masonry-container floating_set">
          <div class="grid-sizer"></div>

 <?php foreach($leads_status as $ls_value) { ?>
	<div class="new-row-data1 width-junk  accordion-panel">
		<div class="new-quote-lead">
		<div class="junk-box">
		<div class="trial-stated-title">
			<h4> <?php echo $ls_value['status_name'];?></h4>
		</div>	
		<div class="back-set02">
		<?php 
		$ls_id = $ls_value['id'];
		$leads_rec = $this->Common_mdl->GetAllWithWhere('leads','lead_status',$ls_id);
		if(!empty($leads_rec)){
		foreach ($leads_rec as $key => $value) {
			  $getUserProfilepic = $this->Common_mdl->getUserProfilepic($value['assigned']); 

			 /* if($value['source']==1)
                        	   {
                        	   	$value['source'] = 'Google';
                        	   }elseif($value['source']==2)
                        	   {
                        	   	$value['source'] = 'Facebook';
                        	   }else{
                        	   	$value['source'] = '';
                        	   }*/

                        	    $source_name = $this->Common_mdl->GetAllWithWhere('source','id',$value['source']);
                        	   $value['source'] = $source_name[0]['source_name'];
                        	   if($value['source']!='')
                        	   {
                        	   	$value['source'] = $value['source'];
                        	   }else{
                        	   	$value['source'] = '-';
                        	   }

			?>
			
				<div class="user-leads1">
					<h3 data-target="#profile_lead_<?php echo $value['id'];?>" data-toggle="modal"><img src="<?php echo $getUserProfilepic;?>" alt="img">
					 <?php echo "#".$value['id'].'-'.$value['name'];?> </h3>
					<div class="source-google">
						<div class="source-set1">
						<span>Source <?php echo $value['source'];?></span>
						</div>
						<div class="source-set1 source-set3 ">
						<span>Created <?php  $date=date('Y-m-d H:i:s',$value['createdTime']);
						 echo time_elapsed_string($date);?></span>
							</div>
							<!-- rspt 18-04-2018 for get attached count -->
							<?php 
							$res=$this->db->query("select * from leads_attachments where id=".$value['id']."")->result_array();
							//echo count($res)."count count";
							 ?>
							    <div class="text-mutes">    
									<div class="icon-card d-inline-block ">
									<i class="icofont icofont-share text-muted "></i>
									<span class="text-muted m-l-10 ">
									<?php /** for attched count */
									echo count($res);
									?>										
									</span>
									</div>
									<div class="icon-card d-inline-block p-l-20 ">
									<i class="icofont icofont-heart-alt text-c-pink "></i>
									<span class="text-c-pink m-l-10 ">0</span>
									</div>
								</div>
								<!-- rspt 18-04-2018 for tags shown-->
								<div>
									 <strong>
					                    <?php $tag =explode(',', $value['tags']);
					                      foreach ($tag as $t_key => $t_value) { ?>
					                    <a href="#"><?php echo $t_value;?></a><?php } ?>
                    				</strong>
								</div>
								<!-- end 18-04-2018 -->
						
					</div>	
				</div>
				
			
			
			<?php }} else{ ?> <div class="new-row-data1 col-xs-12 col-sm-4"> No records Found </div><?php } ?>
		</div></div>
		</div>
	</div>
		
<?php } ?>
		
	</div>
</div>

						
			


</div> <!-- lead-summary2 -->
					


</div> <!-- dashboard -->


</div> <!-- tab-content -->



</div> 
</div> 
</div> <!-- leads-section -->

<!-- Modal -->
<div id="new-lead" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add New Lead</h4>
      </div>
      <div class="modal-body">
			

		<form id="leads_form" name="leads_form" action="<?php echo base_url()?>leads/addLeads" method="post">	
			<input type="hidden" name="kanpan" id="kanpan" value="1">
			<div class="lead-popupsection1 floating_set">
				<div class="col-xs-12 col-sm-4  lead-form1">
					<label>Lead status</label>
					<select name="lead_status" id="lead_status">
						<option value="">Nothing selected</option>
						<?php foreach($leads_status as $ls_value) { ?>
						<option value='<?php echo $ls_value['id'];?>'><?php echo $ls_value['status_name'];?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-xs-12 col-sm-4  lead-form1">
					<label>Source</label>
					<select name="source" id="source">
						<option value=''>Nothing selected</option>
<?php foreach($source as $lsource_value) { ?>
						<option value='<?php echo $lsource_value['id'];?>'><?php echo $lsource_value['source_name'];?></option>
						<?php } ?>
						<!-- <option value='1'>Google</option>
						<option value-'2'>Facebook</option> -->
					</select>
				</div>	
				<div class="col-xs-12 col-sm-4  lead-form1">
				<label>Assigned</label>
				<select name="assigned" id="assigned"> 
					<option value="">Nothing selected</option>
						<?php
						if(isset($staff_form) && $staff_form!='') 
						{
							foreach($staff_form as $value) 
							{
						?>
							<option value="<?php echo $value['id']; ?>" ><?php echo $value["crm_name"]; ?></option>
						<?php
							}
						}
						?>
                </select>
				</div>	
			</div>

				<div class="tags-allow1 floating_set">
				<h4><i class="fa fa-tags fa-6" aria-hidden="true"></i> Tags</h4>
				<input type="text" value="" name="tags" id="tags" class="tags" />
				</div>
				
			<div class="lead-popupsection2 floating_set">
				<div class="formgroup col-xs-12 col-sm-6">
					<label>Name</label>
					<input type="text" name="name" id="name">
				</div>
				<div class="formgroup col-xs-12 col-sm-6">
					<label>Address</label>
					<textarea rows="1" name="address" id="address"></textarea>
				</div>	
				<div class="formgroup col-xs-12 col-sm-6">
					<label>Position</label>
 					<input type="text" name="position" id="position">
				</div>
				<div class="formgroup col-xs-12 col-sm-6">
					<label>City</label>
					<input type="text" name="city" id="city">
				</div>
				<div class="formgroup col-xs-12 col-sm-6">
					<label>Email Address</label>
					<input type="text" name="email_address" id="email_address">
				</div>
				<div class="formgroup col-xs-12 col-sm-6">
					<label>State</label>
					<input type="text" name="state" id="state">
				</div>
				<div class="formgroup col-xs-12 col-sm-6">
					<label>Website</label>
					<input type="text" name="website" id="website">
				</div>
				<div class="formgroup col-xs-12 col-sm-6">
					<label>Country</label>
					<select name="country" id="country">
						<option value=''>Nothing Selected</option>
						<?php foreach ($countries as $key => $value) { ?>
							<option value="<?php echo $value['id']?>"><?php echo $value['name'];?></option>
						<?php }?>
					</select>
				</div>
				<div class="formgroup col-xs-12 col-sm-6">
					<label>Phone</label>
					<input type="text" name="phone" id="phone">
				</div>
				<div class="formgroup col-xs-12 col-sm-6">
					<label>Zip Code</label>
					<input type="number" name="zip_code" id="zip_code">
				</div>
				<div class="formgroup col-xs-12 col-sm-6">
					<label>Company</label>
					<input type="text" name="company" id="company">
				</div>
				<div class="formgroup col-xs-12 col-sm-6">
					<label>Default Language</label>
					<select name="default_language" id="default_language">
                  					<option value="">System Default</option>
                                  <?php foreach ($Language as $l_key => $l_value) { ?>
                                    <option value="<?php echo $l_value['id'];?>"><?php echo $l_value['name'];?></option>
                                  <?php }?>
					</select>
				</div>
				
				<div class="formgroup col-sm-12">
					<label>Description</label>
					<textarea rows="3" name="description" id="description"></textarea>
				</div>
				<div class="formgroup checkbox-select1 col-sm-12">
					<p>
						<input type="checkbox" name="public" id="public">
						<label>Public</label>
					</p>	
					<p>
						<input type="checkbox" name="contact_today" checked id="contact_today">
						<label>Contacted Today</label>
					</p>	
				</div>
				<div class="dedicated-sup col-sm-12 selectDate" style="display:none;">
					<label><i class="fa fa-pencil fa-6" aria-hidden="true"></i> Choose contact Date</label>
					<input type="text" name="contact_date" id="contact_date" class="datepicker" placeholder="Select your date" />
				</div>
			</div>	

			<div class="col-sm-12 form-submit-lead text-right">
				<a href="#"  data-dismiss="modal">Close</a>
				<input type="submit" name="save" id="save" value="Save">
			</div>

		</form>	
					
				
				
				
				
					
      </div>
      
    </div>

  </div>
</div> <!-- modal -->


<!-- Edit modal-->

<?php foreach ($leads as $key => $value) { ?>
<div id="update_lead_<?php echo $value['id'];?>" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Lead</h4>
      </div>
      <div class="modal-body">
			

		<form id="edit_leads_form" name="edit_leads_form" action="<?php echo base_url()?>leads/editLeads/<?php echo $value['id'];?>" method="post" class="edit_leads_form">	
			<input type="hidden" name="kanpan" id="kanpan" value="1">
			<div class="lead-popupsection1 floating_set">
				<div class="col-xs-12 col-sm-4  lead-form1">
					<label>Lead status</label>
					<select name="lead_status" id="lead_status">
						<option value="">Nothing selected</option>
						<?php foreach($leads_status as $ls_value) { ?>
						<option value='<?php echo $ls_value['id'];?>' <?php  if($value['lead_status']==$ls_value['id']){ echo "selected='selected'"; }?>><?php echo $ls_value['status_name'];?></option>
						<?php } ?>

						<!-- <option value='1' <?php  if($value['lead_status']=='1'){ echo "selected='selected'"; }?>>Customer</option> -->
					</select>
				</div>
				<div class="col-xs-12 col-sm-4  lead-form1">
					<label>Source</label>
					<select name="source" id="source">
						<option value=''>Nothing selected</option>
						<?php foreach($source as $lsource_value) { ?>
						<option value='<?php echo $lsource_value['id'];?>' <?php  if($value['source']==$lsource_value['id']){ echo "selected='selected'"; }?>><?php echo $lsource_value['source_name'];?></option>
						<?php } ?>
						<!-- <option value='1' <?php  if($value['source']=='1'){ echo "selected='selected'"; }?>>Google</option>
						<option value-'2' <?php  if($value['source']=='2'){ echo "selected='selected'"; }?>>Facebook</option> -->
					</select>
				</div>	
				<div class="col-xs-12 col-sm-4  lead-form1">
				<label>Assigned</label>
				<select name="assigned" id="assigned"> 
					<option value="">Nothing selected</option>
						<?php
						if(isset($staff_form) && $staff_form!='') 
						{
							foreach($staff_form as $values) 
							{
						?>
							<option value="<?php echo $values['id']; ?>" <?php  if($value['assigned']==$values['id']) { echo "selected='selected'"; } ?> ><?php echo $values["crm_name"]; ?></option>
						<?php
							}
						}
						?>
                </select>
				</div>	
			</div>

				<div class="tags-allow1 floating_set">
				<h4><i class="fa fa-tags fa-6" aria-hidden="true"></i> Tags</h4>
				<input type="text" value="<?php echo $value['tags'];?>" name="tags" id="tags" class="tags" />
				</div>
				
			<div class="lead-popupsection2 floating_set">
				<div class="formgroup col-xs-12 col-sm-6">
					<label>Name</label>
					<input type="text" name="name" id="name"  value="<?php echo $value['name'];?>">
				</div>
				<div class="formgroup col-xs-12 col-sm-6">
					<label>Address</label>
					<textarea rows="1" name="address" id="address"><?php echo $value['address'];?></textarea>
				</div>	
				<div class="formgroup col-xs-12 col-sm-6">
					<label>Position</label>
 					<input type="text" name="position" id="position" value="<?php echo $value['position'];?>">
				</div>
				<div class="formgroup col-xs-12 col-sm-6">
					<label>City</label>
					<input type="text" name="city" id="city" value="<?php echo $value['city'];?>">
				</div>
				<div class="formgroup col-xs-12 col-sm-6">
					<label>Email Address</label>
					<input type="text" name="email_address" id="email_address" value="<?php echo $value['email_address'];?>">
				</div>
				<div class="formgroup col-xs-12 col-sm-6">
					<label>State</label>
					<input type="text" name="state" id="state" value="<?php echo $value['state'];?>">
				</div>
				<div class="formgroup col-xs-12 col-sm-6">
					<label>Website</label>
					<input type="text" name="website" id="website" value="<?php echo $value['website'];?>">
				</div>
				<div class="formgroup col-xs-12 col-sm-6">
					<label>Country</label>
					<select name="country" id="country">
						<option value=''>Nothing Selected</option>
						<?php foreach ($countries as $c_key => $c_value) { ?>
							<option value="<?php echo $c_value['id']?>" <?php if($value['country']==$c_value['id']){ echo "selected='selected'"; } ?>><?php echo $c_value['name'];?></option>
						<?php }?>
					</select>
				</div>
				<div class="formgroup col-xs-12 col-sm-6">
					<label>Phone</label>
					<input type="text" name="phone" id="phone" value="<?php echo $value['phone'];?>">
				</div>
				<div class="formgroup col-xs-12 col-sm-6">
					<label>Zip Code</label>
					<input type="number" name="zip_code" id="zip_code" value="<?php echo $value['zip_code'];?>">
				</div>
				<div class="formgroup col-xs-12 col-sm-6">
					<label>Company</label>
					<input type="text" name="company" id="company" value="<?php echo $value['company'];?>">
				</div>
				<div class="formgroup col-xs-12 col-sm-6">
					<label>Default Language</label>
					<select name="default_language" id="default_language">
                  					<option value="">System Default</option>
                                  <?php foreach ($Language as $l_key => $l_value) { ?>
                                    <option value="<?php echo $l_value['id'];?>" <?php if($l_value['id']==$value['default_language']){ echo "selected='selected'"; } ?>><?php echo $l_value['name'];?></option>
                                  <?php }?>
					</select>
				</div>
				
				<div class="formgroup col-sm-12">
					<label>Description</label>
					<textarea rows="3" name="description" id="description"><?php echo $value['description'];?></textarea>
				</div>
				<div class="formgroup checkbox-select1 col-sm-12">
					<p>
						<input type="checkbox" name="public" id="public" <?php if($value['public']=='on'){ echo 'checked'; } ?>>
						<label>Public</label>
					</p>	
					<p>
						<input type="checkbox" name="contact_today" <?php if($value['contact_today']=='on'){ echo 'checked'; } ?> id="contact_update_today" class="contact_update_today" data-id="<?php echo $value['id'];?>">
						<label>Contacted Today</label>
					</p>	
				</div>
				<div class="dedicated-sup col-sm-12 selectDate_update" <?php if($value['contact_today']!='') { ?> style="display:none;"<?php }else{ ?>style="display:block;"<?php  } ?> id="selectDate_update_<?php echo $value['id'];?>">
					<label><i class="fa fa-pencil fa-6" aria-hidden="true"></i> Choose contact Date</label>
					<input type="text" name="contact_update_date" id="contact_update_date" class="datepicker" value="<?php echo $value['contact_date'];?>" placeholder="Select your date" />
				</div>
			</div>	

			<div class="col-sm-12 form-submit-lead text-right">
				<a href="#"  data-dismiss="modal">Close</a>
				<input type="submit" name="save" id="save" value="Save">
			</div>

		</form>	
					
				
					
      </div>
      
    </div>

  </div>
</div>
<?php } ?>

<!-- Edit modal-->



<!-- Modal -->
<div id="import-lead" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Import Lead</h4>
      </div>
      <div class="modal-body">
		<div class="download-sample">
			<a href="<?php echo base_url()?>uploads/csv/sample_import_leads.csv">DOWNLOAD SAMPLE</a>
		</div>
		
		<div class="term-condition1">
			<p>1. Your CSV data should be in the format below. The first line fof your CSV file should be the column headers as in the table example. Also make sure that your file is UTF_8 to avoid unnecessary encoding problems.</p>
			<p>2. if the column you are trying to import is date make sure that is formatted in format Y-m-d(2017-10-19)</p>
			<p class="dupli-data">3. Dupolicate email rows wont be imported</p>
		</div>

		<div class="table-lead1">
			<table>
				<thead><tr>
					<th>Name</th>
					<th>Company</th>
					<th>Position</th>
					<th>Description</th>
					<th>Country</th>
					<th>Zip</th>
					<th>City</th>
					<th>State</th>
					<th>Address</th>
					<th>Email</th>
					<th>Website</th>
					<th>Phonenumber</th>
					<th>Tags</th>
					<th>Public</th>
					<th>Contact Today</th>
					<th>Contact Date</th></tr>
				</thead>
			<tbody>
				<tr>
					<td>TLS</td>
					<td>TLS</td>
					<td>Developer</td>
					<td>Sample Description</td>
					<td>India</td>
					<td>698523</td>
					<td>Madurai</td>
					<td>Tamil Nadu</td>
					<td>28 , Villapuram; Madurai</td>
					<td>Test@gmail.com</td>
					<td>www.techleaf.com</td>
					<td>9874563210</td>
					<td>tag1,tag2</td>
					<td>on / off</td>
					<td>on / off</td>
					<td>2018-03-21</td>
				</tr>
			</tbody>
		</table>
	</div>	
		
		<form name="import_leads" id="import_leads" action="<?php echo base_url()?>leads/import_leads" method="post" enctype="multipart/form-data">
			<input type="hidden" name="kanpan" id="kanpan" value="1">
			<div class="file-upload-lead">
				<div class="formdata1">
					<label>choose CSV File</label>
					<!-- <input type="file" name="file"> --> 
					<input type="file" name="file" id="file" accept=".csv"/>

				</div>
				<div class="formdata1">
					<label>Source</label>
					<select name="source" id="source">
						<option value=''>Nothing selected</option>
						<?php foreach($source as $lsource_value) { ?>
						<option value='<?php echo $lsource_value['id'];?>'><?php echo $lsource_value['source_name'];?></option>
						<?php } ?>
						<!-- <option value='1'>Google</option>
						<option value-'2'>Facebook</option> -->
					</select>
				</div>	
				<div class="formdata1">
					<label>Status</label>
					<select name="lead_status" id="lead_status">
						<option value="">Nothing selected</option>
						<?php foreach($leads_status as $ls_value) { ?>
						<option value='<?php echo $ls_value['id'];?>'><?php echo $ls_value['status_name'];?></option>
						<?php } ?>
					</select>
				</div>	
				<div class="formdata1">
					<label>Responsible</label>
					<select name="assigned" id="assigned"> 
					<option value="">Nothing selected</option>
						<?php
						if(isset($staff_form) && $staff_form!='') 
						{
							foreach($staff_form as $value) 
							{
						?>
							<option value="<?php echo $value['id']; ?>" ><?php echo $value["crm_name"]; ?></option>
						<?php
							}
						}
						?>
                </select>
				</div>	
			</div>
			<input type="hidden" name="kanpan" id="kanpan" value="1">
			<div class="file-import-lead">
<!-- 				<input type="import" name="submit" value="IMPORT">
 -->				<input type="submit" name="submit" value="SUBMIT">
			</div>	
			</form>
	   </div>
      
    </div>

  </div>
</div> <!-- modal -->

<?php foreach ($leads as $key => $value) { 
	$country = $value['country'];
	$l_status = $value['lead_status'];
	$status_name = $this->Common_mdl->select_record('leads_status','id',$l_status);
	$country_name = $this->Common_mdl->select_record('countries','id',$country);
	$languages = $this->Common_mdl->select_record('languages','id',$value['default_language']);
	$assigned = $this->Common_mdl->select_record('user','id',$value['assigned']);

	?>
<input type="hidden" id="leads_id" name="leads_id" value="<?php echo $value['id'];?>"> 
<div class="modal fade show leads_popup" id="profile_lead_<?php echo $value['id'];?>" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">×</button>
          <h4 class="modal-title"><?php echo "#".$value['id'].' - '.$value['name'];?></h4>
        </div>

        <div class="lead-deadline1">
        <div class="deadline-crm1  floating_set ">
               <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                      <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab"  href="#profile_<?php echo $value['id'];?>">Profile</a>
                      </li>
						<li class="nav-item it1" value="1"><a class="nav-link" data-toggle="tab" href="#proposal_<?php echo $value['id'];?>">Proposal</a></li> 
						<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tasks_<?php echo $value['id'];?>">Tasks</a></li> 
						<li class="nav-item "><a class="nav-link" data-toggle="tab" href="#attachments_<?php echo $value['id'];?>">Attachments</a></li> 
						<li class="nav-item  "><a class="nav-link" data-toggle="tab" href="#reminder_<?php echo $value['id'];?>">Reminders</a></li> 
						<li class="nav-item  "><a class="nav-link" data-toggle="tab" href="#notes_<?php echo $value['id'];?>">Notes</a></li> 
						<li class="nav-item "><a class="nav-link" data-toggle="tab" href="#activity-log_<?php echo $value['id'];?>">Activity Log</a></li> 
                    </ul>
                    </div>
                 </div>
                    
				<div class="profile-edit-crm">
	                    <div class="tab-content">

                    		<div id="profile_<?php echo $value['id'];?>" class="tab-pane fade in active">
                    			<div class="edit-section">
                    					<div class="mark-lost1 floating_set">	
                    					<div class="pull-left edit-pull-left">
                    						<ul>
                    							<li><a class="btn-primary" href="#" data-toggle="modal" data-target="#update_lead_<?php echo $value['id'];?>">Edit <i class="fa fa-pencil-square-o fa-6" aria-hidden="true">   </i></a></li>
                    							<li>
                    								<a href="#" class="btn btn-success  edit-toggle1">More <span class="caret"></span></a>
                    							<ul class="proposal-down">
                    									<li><a href="#" id="lost" data-id="<?php echo $value['id'];?>" class="statuschange">Mark as lost</a></li>
                    									<li><a href="#" id="junk" data-id="<?php echo $value['id'];?>" class="statuschange"> Mark as junk</a></li>
                    									<li><a href="<?php echo base_url().'leads/delete/'.$value['id'].'/0';?>" onclick="return confirm('Are you sure want to delete');" > Delete Lead</a></li>
                    							</ul>
                    						</li>			
                    							</ul>	
                    					</div>		

                    					<!-- <div class="pull-right convert-custom">
                    						<a href="#"> Convert to customer </a>
                    					</div> -->
                    				</div>
                    				</div>	

                    				



									<div class="floating_set general-inform1">
										<div class="alert alert-success succ" style="display:none;">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>
		<div class="col-sm-6 custom-fields11">
		<div class="custom-danger">
			<h4>Information</h4>
			<div class="form-status1">
				<span>Lead Status</span>
				<strong class="leadstatus"><?php echo $status_name['status_name'];?></strong>
			</div>	
			<div class="form-status1">
				<span>Position</span>
				<strong><?php echo $value['position'];?></strong>
			</div>	
			<div class="form-status1">
				<span>Email Address</span>
				<strong><a href="#"><?php echo $value['email_address'];?></a></strong>
			</div>	
			<div class="form-status1">
				<span>Website</span>
				<strong><a href="#"><?php echo $value['website'];?></a></strong>
			</div>	
			<div class="form-status1">
				<span>Phone</span>
				<strong><a href="#"><?php echo $value['phone'];?></a></strong>
			</div>	
			<div class="form-status1">
				<span>Company</span>
				<strong><?php echo $value['company'];?></strong>
			</div>	
			<div class="form-status1">
				<span>Address</span>
				<strong><?php echo $value['address'];?></strong>
			</div>	
			<div class="form-status1">
				<span>City</span>
				<strong><?php echo $value['city'];?></strong>
			</div>	
			<div class="form-status1">
				<span>State</span>
				<strong><?php echo $value['state'];?></strong>
			</div>	
			<div class="form-status1">
				<span>Country</span>
				<strong><?php echo $country_name['name'];?></strong>
			</div>	
			<div class="form-status1">
				<span>Zip code</span>
				<strong><?php echo $value['zip_code'];?></strong>
			</div>	
		</div>	
		</div>
		
		<div class="col-sm-6 custom-fields11 general-leads1">
		<div class="custom-danger">
			<h4>General Information</h4>
			<!-- <div class="form-status1">
				<span>Lead Status</span>
				<strong>New</strong>
			</div>	 -->
			<div class="form-status1">
				<span>Source</span>
				<strong><?php /*if($value['source']==1)
                        	   {
                        	   	$value['source'] = 'Google';
                        	   }elseif($value['source']==2)
                        	   {
                        	   	$value['source'] = 'Facebook';
                        	   }else{
                        	   	$value['source'] = '';
                        	   } */
                        	    $source_name = $this->Common_mdl->GetAllWithWhere('source','id',$value['source']);
                        	   $value['source'] = $source_name[0]['source_name'];
                        	   if($value['source']!='')
                        	   {
                        	   	$value['source'] = $value['source'];
                        	   }else{
                        	   	$value['source'] = '-';
                        	   }
                        	   echo $value['source'];
                        	   ?></strong>
			</div>	
			<div class="form-status1">
				<span>Default Language</span>
				<strong><?php echo $languages['name'];?></strong>
			</div>	
			<div class="form-status1">
				<span>Assigned</span>
				<strong><?php echo $assigned['crm_name'];?></strong>
			</div>	
			<div class="form-status1">
				<span>Tags</span>
				<strong>
					<?php $tag =explode(',', $value['tags']);
					foreach ($tag as $t_key => $t_value) { ?>
					<a href="#"><?php echo $t_value;?></a><?php } ?></strong>
			</div>	
			<div class="form-status1">
				<span>Created</span>
				<strong><?php  $date=date('Y-m-d H:i:s',$value['createdTime']);
						 echo time_elapsed_string($date);?></strong>
			</div>	
			<div class="form-status1">
				<span>Last Contact</span>
				<strong>-</strong>
			</div>	
			<div class="form-status1">
				<span>Public</span>
				<strong><?php if($value['public']!=''){ echo $value['public']; }else{ echo 'No'; }?></strong>
			</div>	
			</div>
		</div>	
		
		
		<!-- <div class="col-sm-4 custom-fields11">
		<div class="custom-danger">
			<h4>Custom Fields</h4>
			<div class="form-status1">
				<span>Dedicated support</span>
				<strong>-</strong>
			</div>	
		</div>
	</div> -->
	
		<div class="description-lead1 col-sm-12">	
			<span>Description</span>
			<p><?php echo $value['description'];?> </p>
		</div>	
		
		<div class="activity1 col-sm-12">
		<h4>Latest Activity</h4>
		<!-- <span>Gust Toy - created lead</span> -->
		<span><?php  $date=date('Y-m-d H:i:s',$latestrec[0]['CreatedTime']);
						 echo time_elapsed_string($date);?></span>
			<span><?php
			$l_id = $latestrec[0]['module_id'];
				$l_name = $this->Common_mdl->select_record('leads','id',$l_id);

			 echo $l_name['name'] . ' - ' . $latestrec[0]['log'];?></span>
		</div>
			
	</div>	
</div> <!-- profile -->
				

<div id="proposal_<?php echo $value['id'];?>" class="tab-pane fade">
	<div class="new-proposal1 floating_set">
		<a href="#">New proposal</a>
	</div>

		<div class="proposal-table1 floating_set">
			
			<table id="example" class="display data-available-1" cellspacing="0" width="100%">
        
        <thead>
            <tr>
                <th>Proposal</th>
                <th>Subject</th>
                <th>Total</th>
                <th>Date</th>
                <th>Open Till</th>
                <th>Tags</th>
				<th>Date created</th>
				<th>Status</th>
            </tr>
        </thead>
        <tbody>
            
			
            
        </tbody>
    </table>
		
		</div>
</div> <!-- 2tab -->

<div id="tasks_<?php echo $value['id'];?>" class="tab-pane fade">
	<div class="new-proposal1 floating_set">
		<a href="#">New Task</a>
	</div>

		<div class="proposal-table1 floating_set">
			
			<table id="example1" class="display data-available-1" cellspacing="0" width="100%">
        
        <thead>
            <tr>
                <th>Proposal</th>
                <th>Subject</th>
                <th>Total</th>
                <th>Date</th>
                <th>Open Till</th>
                <th>Tags</th>
				<th>Date created</th>
				<th>Status</th>
            </tr>
        </thead>
        <tbody>
            
			
            
        </tbody>
    </table>
		
		</div>
</div> <!-- 3tab -->



<div id="attachments_<?php echo $value['id'];?>" class="fileupload_01 tab-pane fade">
<div class=" upload_input09">
				<input type="file" name="profile_image" id="profile_image">
			</div>
			
			<div class="cloud-upload1">
				<a href="#"><i class="fa fa-cloud-upload fa-6" aria-hidden="true"></i> Choose from Dropbox</a>
			</div>
</div> <!-- 4tab -->

<div id="reminder_<?php echo $value['id'];?>" class="tab-pane fade">
	<div class="new-proposal1 floating_set">
		<a href="#" data-toggle="modal" data-target="#reminder-lead"><i class="fa fa-bell-o fa-6" aria-hidden="true"></i> Add Lead Reminder</a>
	</div>

		<div class="proposal-table1 floating_set">
			
			<table id="example2" class="data-available-1 display" cellspacing="0" width="100%">
        
        <thead>
            <tr>
                <th>Proposal</th>
                <th>Subject</th>
                <th>Total</th>
                <th>Date</th>
                <th>Open Till</th>
                <th>Tags</th>
				<th>Date created</th>
				<th>Status</th>
            </tr>
        </thead>
        <tbody>
            
			
            
        </tbody>
    </table>
		
		</div>
</div> <!-- 5tab -->

<div id="notes_<?php echo $value['id'];?>" class="tab-pane fade">

	<div class="notes-leads floating_set">
	<form>
	
		<div class="notes-list01">
		<div class="notetextarea1">
			<textarea rows="3" id="note_rec_<?php echo $value['id'];?>" name="note_rec" class="note_val"></textarea>

			<input type="submit" name="add note" value="Add Note" class="add_note_rec" data-id="<?php echo $value['id'];?>">


		</div>
		 <div id="error_note<?php echo $value['id'];?>" class="error" style="color: red;"></div>

		 <div class="dedicated-sup col-sm-12 note_date_<?php echo $value['id'];?>" style="display:none;">
					<label><i class="fa fa-pencil fa-6" aria-hidden="true"></i> contact Date</label>
					<input type="text" name="note_contact_date" id="note_contact_date_<?php echo $value['id'];?>" class="datepicker note_d" placeholder="Select your date" />
				</div>
		<div class="checked-unicode">
			<div class="form-group">
				<input type="radio" name="note_contact" class="note_contact" value="got_touch" data-id="<?php echo $value['id'];?>" > <label>I got in touch with this lead</label>
			</div>
			<div class="form-group">
				<input type="radio" name="note_contact" checked="checked" class="note_contact" data-id="<?php echo $value['id'];?>" value="not_touch"><label>I have not contacted this lead</label>
			</div>
		</div>	
				
 

		</div>
		<div class="add_note_rec_load">
			<?php 
			$leads_notes = $this->db->query(" select * from leads_notes where lead_id ='".$value["id"]."' order by id desc")->result_array();
			foreach ($leads_notes as $notes_key => $notes_value) { 
			$getUserProfilepic = $this->Common_mdl->getUserProfilepic($notes_value['user_id']); 
			$u_name = $this->Common_mdl->select_record('user','id',$notes_value['user_id']);

			?>
				
		<div class="gust-toy01">
			<div class="checkbox-profile1">
				<img src="<?php echo $getUserProfilepic;?>" alt="User-Profile-Image">
			</div>
			<div class="timeline-lead01">
				<span>Note added: <?php 
				if($notes_value['createdTime']!=''){
				echo date('Y-m-d H:i:s',$notes_value['createdTime']); } ?></span>
				<strong><?php echo $u_name["crm_name"];?></strong>
				<p>	<?php echo $notes_value['notes'];?></p>
			</div>
			<div class="edit-timeline">
<!-- 				<a href="#"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
 -->				<a href="#" class="delete_note" data-id="<?php echo $notes_value["id"].'-'.$value['id'];?>"><i class="fa fa-times fa-6" aria-hidden="true"></i></a>
			</div>
		</div>

		<?php } ?>
	</div>
		
			</div>	
		
	

</div> <!-- 6tab -->

<div id="activity-log_<?php echo $value['id'];?>" class="tab-pane fade">
	
	<div class="enter-activity-log floating_set ">
		<div class="leads_activity_log">
		<?php 
		$latestrecs = $this->db->query(" select * from activity_log where module_id='".$value["id"]."' and module='Lead' order by id asc")->result_array();
		foreach ($latestrecs as $late_key => $late_value) { ?>
		<div class="activity-created1">
			<span><?php  $date=date('Y-m-d H:i:s',$late_value['CreatedTime']);
						 echo time_elapsed_string($date);?></span>
			<span><?php
				$l_id = $late_value['module_id'];
				$l_name = $this->Common_mdl->select_record('leads','id',$l_id);

			 	echo $l_name['name'] . ' - ' . $late_value['log'];?></span>
		</div>
		<?php } ?>
		</div>

		<div class="notetextarea1">
			<textarea rows="3" placeholder="Enter Activity" name="activity_log" id="activity_log_<?php echo $value['id'];?>" class="activity_log"></textarea>
					<div id="error<?php echo $value['id'];?>" class="error" style="color: red;"></div>

			<input type="submit" name="add_activity" id="add_activity" data-id="<?php echo $value['id'];?>" class="add_activity" value="Add Activity" >
		</div>
	</div>	
			
	</div> <!-- 7tab -->
	      				 

					</div> <!-- tab-content -->
				</div> <!-- profile-edit-crm -->		
		
		<div class="modal-footer">
            <a href="#" data-dismiss="modal">Close</a>
            
          </div>				

			 <!-- ********************************* -->
      </div>
    </div>
  </div>


<?php } ?>

<?php $this->load->view('includes/footer');?>


<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>


<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
 <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {

	    //   var date = $('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
/*$( ".datepicker" ).datepicker({
       dateFormat: 'yy-mm-dd'
   });*/

$('.tags').tagsinput({
   	allowDuplicates: true
   });
   
   $('.tags').on('itemAdded', function(item, tag) {
       $('.items').html('');
var tags = $('.tags').tagsinput('items');
     
   });
});

$("#contact_today").click(function(){
	if($("#contact_today").is(':checked')){
		$(".selectDate").css('display','none');
	}else{
		$(".selectDate").css('display','block');
	}
});

$(".contact_update_today").click(function(){
	var id = $(this).attr("data-id");
	if($(this).is(':checked')){

		$("#selectDate_update_"+id).css('display','none');
	}else{
		$("#selectDate_update_"+id).css('display','block');
	}
});

 $("#all_leads").dataTable({
       "iDisplayLength": 10,
    });

</script>


<script type="text/javascript">
   $(document).ready(function(){

      $( "#leads_form" ).validate({
        rules: {
          lead_status: "required",  
          source: "required",  
          name: "required",  
        },
        messages: {
          lead_status: "Please enter your Status",
          source: "Please enter your Source",
          name: "Please enter your Name"
        },
        
      });


      $( ".edit_leads_form" ).validate({
        rules: {
          lead_status: "required",  
          source: "required",  
          name: "required",  
        },
        messages: {
          lead_status: "Please enter your Status",
          source: "Please enter your Source",
          name: "Please enter your Name"
        },
        
      });
   });


   $(".sortby_filter").click(function(){
   	   	 $(".LoadingImage").show();

   		var sortby_val = $(this).attr("id");
   		var data={};
   		data['sortby_val'] = sortby_val;
   		$.ajax({
   		url: '<?php echo base_url();?>leads/sort_by_kanban/',
   		type : 'POST',
   		data : data,
   		success: function(data) {
   			$(".LoadingImage").hide();

			$(".sortby_kanban").html(data);   
			$("#all_leads").dataTable({
			"iDisplayLength": 10,
			});
   		},
   		});

   });

 $("#import_leads").validate({
 	   rules: {
 	   	  file: {required: true, accept: "csv"},

          lead_status: "required",  
          source: "required",  
          name: "required",  
        },
        messages: {
      	  file: {required: 'Required!', accept: 'Please upload a file with .csv extension'},

          lead_status: "Please enter your Status",
          source: "Please enter your Source",
          name: "Please enter your Name"
        },

});
   	$(".status_filter").click(function(){
  		$(".LoadingImage").show();
	   	var filter_val = $(this).attr("id");
	   	var data={};
		data['filter_val'] = filter_val;
		$.ajax({
   		url: '<?php echo base_url();?>leads/filter_status_kanban/',
   		type : 'POST',
   		data : data,
   		success: function(data) {
   			$(".LoadingImage").hide();
			$(".sortby_kanban").html(data);   
			$("#all_leads").dataTable({
			"iDisplayLength": 10,
			});
   		},
   		});
  	});
</script>

  <script type="text/javascript">
  	$(document).ready(function(){
  			$('.edit-toggle1').click(function(){
  				$('.proposal-down').slideToggle(300);
  			})
			
			$('#example,#example1,#example2').DataTable();
  	})
  

 	$(".statuschange").click(function(){
	var status_val = $(this).attr("id");
	var id = $(this).attr("data-id");
	var data={};
	data['status_val'] = status_val;
	data['id'] = id;

	$.ajax({
   		url: '<?php echo base_url();?>leads/status_update_kanban/',
   		type : 'POST',
   		dataType: 'JSON',
   		data : data,
   		success: function(data) {
   			//alert(JSON.stringify(data));
   			$('.succ').css('display','block');
   			$('.succ').html('Status has been updated successfully');
			setTimeout(function() {
			$('.succ').fadeOut('fast');
			}, 1000);

			$(".sortby_kanban").html(data.html);   

   			/*var cnt = data.split("-");*/
   			$("#Lost").html(data.lost_cnt);
   			$("#Junk").html(data.junk_cnt);
   			$(".LoadingImage").hide();
			$(".proposal-down").css('display','none');
			$('.leadstatus').html(status_val);
   		},
   		});
  	});


  		$(".status_filter").click(function(){
  		$(".LoadingImage").show();
	   	var filter_val = $(this).attr("id");
	   	var data={};
		data['filter_val'] = filter_val;
		$.ajax({
   		url: '<?php echo base_url();?>leads/filter_status/',
   		type : 'POST',
   		data : data,
   		success: function(data) {
   			$(".LoadingImage").hide();
			$(".sortrec").html(data);   
			$("#all_leads").dataTable({
			"iDisplayLength": 10,
			});
   		},
   		});
  	});

  		$('.add_activity').click(function(e){
  			e.preventDefault();
  			var i = $(this).attr("data-id");
  			var values = $("#activity_log_"+i).val();
  			if(values!=''){
  			$(".error").hide();
  			var data={};
			data['values'] = values;
			data['id'] = i;
			data['module'] = "Lead";
			$.ajax({
	   		url: '<?php echo base_url();?>leads/add_activity_log/',
	   		type : 'POST',
	   		data : data,
	   		success: function(data) {
	   			$(".LoadingImage").hide();
	   			$('.activity_log').val('');
				$(".leads_activity_log").html(data);   
	   		},
   		});
		}else{
			$("#error"+i).html('<span>Please Enter Your Activity log.</span>');
		}
  		});
/* 09-03-2018 */
$(".note_contact").click(function(){
	var i = $(this).attr("data-id");
	var radioValue = $("input[name='note_contact']:checked").val();
	/*alert(radioValue);
	alert(i);*/
            if(radioValue=="got_touch"){
               $('.note_date_'+i).css('display','block');
            }else{
            	$('.note_date_'+i).css('display','none');
            }
});

$('.add_note_rec').click(function(e){
	e.preventDefault();
	$(".LoadingImage").show();

	var i = $(this).attr("data-id");
	var val = $("#note_rec_"+i).val();
	var contact_date = $("#note_contact_date_"+i).val();
	//alert(val);
	if(val!='')
	{
		$(".error").hide();
  			var data={};
			data['values'] = val;
			data['id'] = i;
			data['contact_date'] = contact_date;
			$.ajax({
	   		url: '<?php echo base_url();?>leads/add_note/',
	   		type : 'POST',
	   		data : data,
	   		success: function(data) {
	   			$(".LoadingImage").hide();
	   			$('.note_val').val('');
	   			$('.note_d').val('');
	   			$(".note_contact").prop('checked', false);

	   			$('.note_date_'+i).css('display','none');
				$(".add_note_rec_load").html(data);   
	   		},
   		});
	}else{
			$("#error_note"+i).html('<span>Please Enter Your Notes.</span>');
		}
});

$(document).on('click','.delete_note',function(e){
		$(".LoadingImage").show();

		var i = $(this).attr("data-id");
		//var lead_id = $(this).val();
			var data={};
			data['note_id'] = i;
			//data['lead_id'] = lead_id;
			$.ajax({
	   		url: '<?php echo base_url();?>leads/delete_note/',
	   		type : 'POST',
	   		data : data,
	   		success: function(data) {
	   			$(".LoadingImage").hide();
	   			$('.note_val').val('');
	   			$('.note_d').val('');
	   			$(".note_contact").prop('checked', false);

	   			$('.note_date_'+i).css('display','none');
				$(".add_note_rec_load").html(data);   
	   		},
   		});

});
  </script>
<!-- 
  <script type="text/javascript">
  	$(document).ready(function(){
  	var $gridlead = $('.lead-summary3 .masonry-container').masonry({
      itemSelector: '   .new-row-data1',
      percentPosition: true,
      columnWidth: '.lead-summary3  .grid-sizer' 
    });
   
   setTimeout(function(){ $gridlead.masonry('layout'); }, 600);
});
  </script> -->