<script>
;(function($, db) {
    var Note; // Initialize note
 db.set('demo_imported', false); // my custom_declare
    /**
     * Format timestamp in a specified format
     * @param  {int} time Time in integer format
     * @return {string}      Formated time string
     */
    function _formatDate(time) {
        return moment(Number(time)).format('DD MMMM YYYY, h:mm a');
    }

    /**
     * Escape HTML tags
     * @param  {string} html Unescaped content
     * @return {string}      HTML escaped content
     */
    function _escapeHtml(html) {
        return $('<div />').text(html).html();
    }

    // Debouncer to imporve performance
    function _debounce(func, wait, immediate) {
        var timeout;
        return function() {
            var context = this, args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    };

    Note = {
        // DOM selectors
        $list: $('#Note-list'), // Note list wrapper
        $pad: $('#Note-pad'), // Note writing box
        $created: $('#Note-created__date'), // Note created date holder
        $add: $('.Note-add'), // Note create button
        $search: $('#Note-search'), // Note search box

        _length: 0, // Length of notes
        _lastIndex: 0, // Last note index

        _PREFIX: 'TB', // DB record key prefix
        _UID_SEPERATOR: '-', // Seperator between prefix and index

        init: function() {
            // Event must be called before render
            Note.addEvents();
    /**********************************************************/
        //    Note.render_default();

        // $('.list-group-item').each(function(){
        //   alert('aaa');
        // })



    /**********************************************************/
<?php  $leads_notes = $this->db->query(" select * from leads_notes where lead_id ='".$leads["id"]."' order by id desc")->result_array();
        ?>
 var demoNotes = [
                    <?php 
                    if(count($leads_notes) > 0 ){
                    foreach ($leads_notes as $notes_key => $notes_value) {
                        ?>
                        {
                        title:"<?php echo $notes_value['notes_title'];?>",
                        content:"<?php echo $notes_value['notes_title'];?>"+"\n"+"<?php echo preg_replace('~[\r\n]+~', '\n', trim($notes_value['notes']));?>"
                        },
                        <?php
                    }
                  }

                    ?>
               ];
      
<?php if(count($leads_notes)==0){
  ?>
  //var Note;
  //alert('one');
    // Note.create('');
    
    // Note.render();
    // Note.render_default();
     
   <?php
}
else
{
  ?>
  //alert('222');
    // demoNotes.forEach(function(demoNote) {
    //         Note.create(demoNote);
    //     });
    //  Note.render();
    //  db.set('demo_imported', true);

  <?php
}
?>
       
            //Note.render();
        },

        /**
         * Render full note list
         * @return {object} Note module object
         */
        render: function() {
            var templates = [];
            Note._length = 0;

            /**
             * Loop through all DB entry and prepare only valid note for output
             * @param  {string} key    DB entry key
             * @param  {object} entry  DB entry object
             * @return {void}
             */
             
            db.forEach(function(key, entry) {
                if (Note._isValid(key)) {
                    templates.unshift(Note._getNoteTemplate(entry));
                    ++Note._length;

                    if (entry.id > Note._lastIndex) {
                        Note._lastIndex = entry.id;
                    }
                  //  alert(entry.id);
                }
            });

            Note._renderList(templates);
            return this;
        },

        /** my custom render for delete on 1st time **/

        render_default: function() {
            db.forEach(function(key, entry) {
                if (Note._isValid(key)) {
                  Note.delete(entry.id);
                 // $(this).parent().remove();
                  Note.$pad.val(' ');
                  if (!Note._length) {
                      Note.$created.text('');
                  }
                }
            });
            return this;
        },
        /** end of mycustom render **/

        /**
         * Bind event listeners to DOM elements
         */
        addEvents: function() {
            // Note open handler
            Note.$list.on('click', '.Note', function() {
                var note = $(this),
                    noteWrapper = note.parentsUntil('list-group-item'),
                    id = note.data('uid');

                if (id) Note.open(id);
                noteWrapper.addClass('active').siblings().removeClass('active');

            });

            // Note delete handler
            Note.$list.on('click', '.Note-delete', function(e) {
                e.preventDefault();
                Note.delete($(this).data('uid'));
                //$(this).parent().remove();
                $(this).parents('.cd-timeline-block').remove();

                Note.$pad.val(' ');
                if (!Note._length) {
                    Note.$created.text('');
                }
              /** delete notes ajax **/
                var data={};
                var ajax_lead_id = '<?php echo $leads['id'];?>';
                data['lead_id'] = ajax_lead_id;
                data['note_uid']= $(this).data('uid');
                $.ajax({
                    url: '<?php echo base_url();?>leads/delete_note_with_timeline/',
                    type : 'POST',
                    data : data,
                    success: function(data) {
                      //alert(data);
                    },
                    });
              /** end of delete notes ajax **/


            });

            // Note create handler
            Note.$add.on('click', function(e) {
                e.preventDefault();
              //  Note.create({}).render();
                 Note.create({}).render();
            });

            // Create a note on focus if there is no note
            Note.$pad.on('focus', function() {
                if (!Note._length) {
                    Note.create({}).render();
                }
            });

            // Update note on keyup
            Note.$pad.on('keyup', _debounce(function(e) {
                var title,
                    content = Note.$pad.val(),
                    $note = $('.Note[data-uid="' + Note.$pad.data('uid') + '"');

                if (-1 !== content.indexOf('\n')) {
                    title = content.slice(0, content.indexOf('\n'));
                    Note.update(Note.$pad.data('uid'), {title:title, content:content});
                    $note.find('.Note__name').text(title);
                    $note.find('.Note__desc').text(Note._getPartialContent(content, title));

                }
                // alert($note.attr('data-uid'));
                // alert($note.find('.Note__desc').text());

/** ajax **/

 

  var i = '<?php echo $leads['id'];?>';
  //var val = $("#note_rec_"+i).val();
  //var contact_date = new Date();
  var notes_title= $note.find('.Note__name').html();
  var notes_des= $note.find('.Note__desc').html();
  var notes_date=$note.find('.Note__date').html();
 
  var notes_id=$note.attr('data-uid');
  //alert(val);
 
  $(".error").hide();
      var data={};
  //data['values'] = val;
  data['notes_title']=notes_title;
  data['notes_des']=notes_des;
  data['notes_id']=notes_id;
  data['notes_date']=notes_date;
  data['id'] = i;
  //data['contact_date'] = contact_date;
  $.ajax({
      url: '<?php echo base_url();?>leads/add_note_with_timeline/',
      type : 'POST',
      data : data,
      success: function(data) {
        //alert(data);
      },
      });
 


/** end ajax **/

            }, 200));

            // Search note
            Note.$search.on('keyup', _debounce(function() {
                Note.search(Note.$search.val());
            }, 200));
        },

        /**
         * Create note
         * @param  {object} data Note data object
         * @return {object}      Note module object
         */
        create: function(data) {
         // alert(JSON.stringify(data));
            if ('object' !== $.type(data)) {
                return new Error('Invalid data. Object expected.');
            }

            var defaults = {
                id: ++Note._lastIndex,
                title: 'Note Title',
                content: '',
                time: new Date().getTime(),
                leads_id:'<?php echo $leads['id'];?>',
                notes_date:'',
            };

            $.extend(defaults, data);
            store.set(Note._getUID(defaults.id), defaults);
            return this;
        },


        /**
         * Update note
         * @param  {string} key  Note record key
         * @param  {object} note Note data object
         * @return {object}      Note module object
         */
        update: function(key, note) {
            if (!Note._isValid(key)) {
                return new Error('Invalid note key.');
            }
            db.transact(key, function(old) {
                old = $.extend(old, note);
            });
            return this;
        },

        /**
         * Delete note
         * @param  {string} key Note record key
         * @return {object}     Note module object
         */
        delete: function(key) {
            if (!Note._isValid(key)) {
                return new Error('Invalid note key.');
            }
            store.remove(key);
            --Note._length;
            return this;
        },

        /**
         * Open note to writing box and setup UI
         * @param  {string} key Note record key
         * @return {object}     Note module object
         */
        open: function(key) {
            if (!Note._isValid(key)) {
                return new Error('Invalid note key.');
            }
            var note = db.get(key);
            //alert(note.content);
            Note.$pad.val(note.content).data('uid', Note._getUID(note.id));
            Note.$created.text(_formatDate(note.time));
            return this;
        },

        /**
         * Search note in DB and render search result
         * @param  {string} term Search term given by user
         * @return {void}
         */
        search: function(term) {
            var templates = [], termRegx = new RegExp(term, 'gi');

            db.forEach(function(key, entry) {
                if (Note._isValid(key) && (termRegx.test(entry.content) || termRegx.test(entry.title))) {
                    templates.unshift(Note._getNoteTemplate(entry));
                }
            });
            Note._renderList(templates);
        },

        /**
         * Render note list and select first note
         * @param  {array} templates Array of complied note templates
         * @return {void}
         */
        _renderList: function(templates) {
            Note.$list.html(templates).children(':first-child').find('.Note').trigger('click');
            templates.length || Note.$pad.val(''); // Clear note pad when there is no note
        },

        /**
         * Get note list item HTML template
         * @param  {object} data Single note object
         * @return {string}      Output ready note list item
         */
        _getNoteTemplate: function(data) {
          if(data.notes_date!=''){ var zz=_escapeHtml(data.notes_date); }else{ var zz=_formatDate(data.time); }
          // if(data.leads_id=="<?php echo $leads['id'];?>"){
          //   var template = '<li class="list-group-item <?php echo $leads['id'];?>">'
          //       + '<div class="notest112">'
          //       + '<button class="Note-delete" data-uid="' + Note._getUID(data.id) + '" type="button">x</button>'
          //       + '<div class="Note" data-uid="' + Note._getUID(data.id) + '">'
          //       + '<div class="Note__name">' + _escapeHtml(data.title) + '</div>'
          //       + '<div class="Note__desc">' + _escapeHtml(Note._getPartialContent(data.content, data.title)) + '</div>'
          //       + '<span class="Note__date">' + zz + '</span>'
          //       + '</div>'
          //       + '</div>'
          //       + '</li>';

          //   return template;
          // }

          /***************************************************/
            if(data.leads_id=="<?php echo $leads['id'];?>"){
            var template = '<div class="cd-timeline-block"><div class="cd-timeline-icon bg-primary"><i class="icofont icofont-ui-file"></i></div><div class="cd-timeline-content card_main">'
                + ' <div class="p-20">'
                       + '<div class="notest112">'
                + '<button class="Note-delete" style="opacity:1" data-uid="' + Note._getUID(data.id) + '" type="button">x</button>'
                + '<div class="Note" data-uid="' + Note._getUID(data.id) + '">'
                + '<div class="Note__name">' + _escapeHtml(data.title) + '</div>'
                + '<div class="Note__desc">' + _escapeHtml(Note._getPartialContent(data.content, data.title)) + '</div>'
                + '<span class="Note__date" style="display:none;">' + zz + '</span>'
                + '</div>'
                + '</div>'
                +'</div>'  
                +' <span class="cd-date">' + zz + '</span>'
                +'<span class="cd-details">Your Notes Info</span>'
                + '</div>'
                + '</div>';

            return template;
          }
          /****************************************************/
          
        },

        _getPartialContent: function(content, title) {
            return content.slice(title.length);
        },

        // Generate UID
        _getUID: function(id) {
            return Note._PREFIX + Note._UID_SEPERATOR + id;
        },

        /**
         * Check note entry validity
         * @param  {string}  key Entry key
         * @return {Boolean}     Check if it is valid note entry
         */
        _isValid: function(key) {
            return key && (String(key).indexOf(Note._PREFIX) === 0);
        }
    };

    Note.init();

    /**
     * Only for demo purpose, feel free to delete
     */
if (db.get('demo_imported')) {
  //alert(JSON.stringify(db));

        <?php  $leads_notes = $this->db->query(" select * from leads_notes where lead_id ='".$leads["id"]."' order by id desc")->result_array();
        ?>
 var demoNotes = [
                    <?php 
                    if(count($leads_notes) > 0 ){
                    foreach ($leads_notes as $notes_key => $notes_value) {
                        ?>
                        {
                        title:"<?php echo $notes_value['notes_title'];?>",
                        content:"<?php echo $notes_value['notes_title'];?>"+"\n"+"<?php echo preg_replace('~[\r\n]+~', '\n', trim($notes_value['notes']));?>"
                        },
                        <?php
                    }
                  } // end of if
                    ?>
               ];
        demoNotes.forEach(function(demoNote) {
            Note.create(demoNote);
        });

        Note.render();
        db.set('demo_imported', true);


}
    if (!db.get('demo_imported')) {
      // alert(JSON.stringify(db.get('demo_imported')));
    
      
        <?php  $leads_notes = $this->db->query(" select * from leads_notes where lead_id ='".$leads["id"]."' order by id desc")->result_array();
        ?>
 var demoNotes = [
                    <?php 
                    if(count($leads_notes) > 0 ){
                    foreach ($leads_notes as $notes_key => $notes_value) {
                        ?>
                        {
                        title:"<?php echo $notes_value['notes_title'];?>",
                        content:"<?php echo $notes_value['notes_title'];?>"+"\n"+"<?php echo preg_replace('~[\r\n]+~', '\n', trim($notes_value['notes']));?>",
                        leads_id:"<?php echo $leads["id"];?>",
                        notes_date:"<?php echo $notes_value['notes_date']; ?>",
                        },
                        <?php
                    }
                  } // end of if
                    ?>
               ];

            db.forEach(function(key, entry) {
                if (Note._isValid(key)) {
                  Note.delete('TB-'+entry.id);
                 // alert('TB-'+entry.id);
                 // $(this).parent().remove();
                  Note.$pad.val(' ');
                  if (!Note._length) {
                      Note.$created.text('');
                  }
                }
            });
        demoNotes.forEach(function(demoNote) {
            Note.create(demoNote);
        });

        Note.render();
        db.set('demo_imported', true);
    }

    window.TB_Note = Note;
})(jQuery, store);
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#lead_notes').removeClass('active');
    $('#profile').addClass('active');

    $('.hide_activity').click(function(){
      //alert('zzz');
      $('#activity-log').removeClass('active');
    });
});
          // $("ul li.list-group-item").each(function(){
          //     alert('zzz');
          // });
</script>