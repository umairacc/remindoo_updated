<?php

  $assign_data=array();
         
$sql_val=$this->db->query("SELECT * FROM firm_assignees where module_id = ". $lead_id." AND module_name='LEADS'")->result_array();

     foreach ($sql_val as $key1 => $value1) {
     $assign_data[] =$value1['assignees'];
     }
 $assign_check=$this->Common_mdl->assign_check($lead_id);



 $edit_tasks =  $this->db->query("select * from add_new_task where related_to='leads' and lead_id=".$lead_id."")->result_array();
$res=$this->db->query("select * from leads where id=".$lead_id."")->result_array();
$assigned = $this->Common_mdl->select_record('user','id',$res[0]['assigned']);
    $assigned = array();
     $team = array();
      $dept = array();
   foreach (explode(',',$res[0]['assigned']) as $leads_key => $leads_value) {
      $assigned[] = $this->Common_mdl->select_record('user','id',$leads_value);
   }
      foreach (explode(',',$res[0]['team']) as $team_key => $team_value) {
      $team[] = $this->Common_mdl->select_record('team','id',$team_value);
   }
      foreach (explode(',',$res[0]['dept']) as $dept_key => $dept_value) {
      $dept[] = $this->Common_mdl->select_record('department_permission','id',$dept_value);
   }
foreach ($edit_tasks as $t_key => $t_value) { ?>

<!-- Modal -->
<div id="edit_task_lead_<?php echo $t_value['id'];?>" class="addnew_task_lead1  modal fade" role="dialog" >
  <div class="modal-dialog">

    <!-- Modal content-->
     <form name="lead_task" id="lead_task_<?php echo $t_value['id'];?>" class="add_lead_task" method="post" enctype='multipart/form-data' data-id="<?php echo $t_value['id'];?>">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit task</h4>
      </div>
   
      <div class="modal-body">
    
      
      <div class="attached-popup1">
      <div class="form-group check-options cus-checkbox1">
       <span class="popup-check" style="display: none;">
      <input type="checkbox" name="public" value="Public" id="t_edit_p-check_<?php echo $t_value['id'];?>"  <?php if(isset($t_value['public']) && $t_value['public']=='Public') {?> checked="checked"<?php } ?>>
      <label for="p-check">Public </label>
      </span>
       <span class="popup-check" style="display: none;">      
      <input type="checkbox" name="billable" value="Billable" checked="" id="t_edit_p-check1_<?php echo $t_value['id'];?>" <?php if(isset($t_value['billable']) && $t_value['billable']=='Billable') {?> checked="checked"<?php } ?>>
      <label for="p-check1"> Billable</label>
      </span>
      </div>
      <!-- <div class="form-group attach-files file-attached1">
        <input type="file" id="t_edit_inputfile_<?php echo $t_value['id'];?>" class="e_task_images" data-id="<?php echo $t_value['id'];?>" name="attach_file">Attach File </div>
        <?php if($t_value['attach_file']!=''){ ?><img src="<?php echo base_url();?>uploads/leads/tasks/<?php echo $t_value['attach_file'];?>" height="50" width="50"> <?php }?> -->
      </div>
      </div>
    
    
    <div class="lead-popupsection2 proposal-data1 floating_set">
      <div class="formgroup col-xs-12 col-sm-6">
          <label>* Subject</label>
          <input type="text" name="t_edit_task_sub_<?php echo $t_value['id'];?>" id="t_edit_task_sub_<?php echo $t_value['id'];?>" value="<?php if(isset($t_value['subject']) && ($t_value['subject']!='') ){ echo $t_value['subject'];}?>">
          <div class="t_edit_task_sub_err" id="t_edit_task_sub_err_<?php echo $t_value['id'];?>" style="color:red;"></div>
        </div>
        <div class="formgroup col-xs-12 col-sm-6" style="display:none;">
          <label>Hourly Rate</label>
          <input type="text" name="t_edit_hour_rate_<?php echo $t_value['id'];?>" id="t_edit_hour_rate_<?php echo $t_value['id'];?>" value="<?php if(isset($t_value['hour_rate']) && ($t_value['hour_rate']!='') ){ echo $t_value['hour_rate'];}?>">
        </div>
        <div class="form-group  col-xs-12 col-sm-6 date_birth">
                                 <label>Start Date</label>
                 <div class="form-birth05">
                                 <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                 <input class="form-control dob_picker fields" type="text" name="t_edit_start_date_<?php echo $t_value['id'];?>" id="t_edit_start_date_<?php echo $t_value['id'];?>"   placeholder="Start date"  value="<?php if(isset($t_value['start_date']) && ($t_value['start_date']!='') ){ echo $t_value['start_date'];}?>"/>
                                 <div class="t_edit_task_start_err" id="t_edit_task_start_err_<?php echo $t_value['id'];?>" style="color:red;" ></div>
                                 
                 </div>
                              </div>
                <div class="form-group  col-xs-12 col-sm-6 date_birth">
                                 <label>Due Date</label>
                                 <div class="form-birth05">
                 <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                 <input class="form-control dob_picker fields" type="text" name="t_edit_due_date_<?php echo $t_value['id'];?>" id="t_edit_due_date_<?php echo $t_value['id'];?>"  placeholder="Due date" value="<?php if(isset($t_value['end_date']) && ($t_value['end_date']!='') ){ echo $t_value['end_date'];}?>"/>
                                </div>
                              </div>
                <div class="formgroup col-xs-12 col-sm-6">
          <label>Priority</label>
        
                          <select name="t_edit_priority" class="form-control valid" id="t_edit_priority_<?php echo $t_value['id'];?>">
                              <option value="low" <?php if(isset($t_value['priority']) && $t_value['priority']=='low') {?> selected="selected"<?php } ?>>Low</option>
                              <option value="medium" <?php if(isset($t_value['priority']) && $t_value['priority']=='medium') {?> selected="selected"<?php } ?>>Medium</option>
                              <option value="high"  <?php if(isset($t_value['priority']) && $t_value['priority']=='high') {?> selected="selected"<?php } ?>>High</option>
                              <option value="super_urgent" <?php if(isset($t_value['priority']) && $t_value['priority']=='super_urgent') {?> selected="selected"<?php } ?>>Super Urgent</option>
                           </select>
        </div>

          <div class="formgroup col-xs-12 col-sm-6" style="display: none">
          <label>Repeat every Week</label>
          <select name = "t_edit_repeat" id="t_edit_repeat_<?php echo $t_value['id'];?>" class="t_edit_repeated_too" data-id="<?php echo $t_value['id'];?>">
            <option value=""></option>
            <option value="one_week" <?php if(isset($t_value['sche_repeats']) && $t_value['sche_repeats']=='one_week') {?> selected="selected"<?php } ?>>week</option>
            <option value="two_week" <?php if(isset($t_value['sche_repeats']) && $t_value['sche_repeats']=='two_week') {?> selected="selected"<?php } ?>>2 week</option>
            <option value="one_month" <?php if(isset($t_value['sche_repeats']) && $t_value['sche_repeats']=='one_month') {?> selected="selected"<?php } ?>>1 Month</option>
            <option value="two_month" <?php if(isset($t_value['sche_repeats']) && $t_value['sche_repeats']=='two_month') {?> selected="selected"<?php } ?>>2 Months</option>
            <option value="three_month" <?php if(isset($t_value['sche_repeats']) && $t_value['sche_repeats']=='three_month') {?> selected="selected"<?php } ?>>3 Months</option>
            <option value="six_month" <?php if(isset($t_value['sche_repeats']) && $t_value['sche_repeats']=='six_month') {?> selected="selected"<?php } ?>>6 Months</option>
            <option value="one_yr" <?php if(isset($t_value['sche_repeats']) && $t_value['sche_repeats']=='one_yr') {?> selected="selected"<?php } ?>>1 yr</option>
            <option value="custom" <?php if(isset($t_value['sche_repeats']) && $t_value['sche_repeats']=='custom') {?> selected="selected"<?php } ?>>Custom</option>
          </select>
        </div>
   
              

      <?php if($t_value['repeat_no']!=''){?>
      <div class="form-group col-xs-12 col-sm-6 rep_no" id="t_edit_rep_no_<?php echo $t_value['id'];?>">
        <input class="form-control fields" type="number" name="t_edit_repeat_no_<?php echo $t_value['id'];?>" id="t_edit_repeat_no_<?php echo $t_value['id'];?>"  placeholder="" value="<?php echo $t_value['repeat_no'];?>"/>
      </div>  
      <?php } ?>
        
        <?php if($t_value['durations']!=''){?>
        <div class="form-group col-xs-12 col-sm-6 rep_cnt" id="t_edit_rep_cnt_<?php echo $t_value['id'];?>">
          <select class="selectpicker"  id="t_edit_durations_<?php echo $t_value['id'];?>" data-show-subtext="true" data-live-search="true" >
          <option value="days" <?php if(isset($t_value['durations']) && $t_value['durations']=='days') {?> selected="selected"<?php } ?>>Day(s)</option>
          <option value="weeks" <?php if(isset($t_value['durations']) && $t_value['durations']=='weeks') {?> selected="selected"<?php } ?>>Week(s)</option>
          <option value="months" <?php if(isset($t_value['durations']) && $t_value['durations']=='months') {?> selected="selected"<?php } ?>>Month(s)</option>
          <option value="years" <?php if(isset($t_value['durations']) && $t_value['durations']=='years') {?> selected="selected"<?php } ?>>Year(s)</option>
          </select>
        </div>  
        <?php } ?>

        <?php if($t_value['ends_on']!=''){?>
        <div class="form-group  col-xs-12  col-sm-12 date_birth ends_on_div" id="t_edit_ends_on_div_<?php echo $t_value['id'];?>">
        <label>Ends On (Leave blank for never)</label>
        <div class="form-birth05">
        <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
        <input class="form-control datepicker fields" type="text" name="t_edit_ends_on_<?php echo $t_value['id'];?>" id="t_edit_ends_on_<?php echo $t_value['id'];?>"  placeholder="Ends on date" value="<?php echo $t_value['ends_on'];?>"/>
        </div>
        </div>
        <?php } ?>

                
                
                <div class="form-group col-xs-12 col-sm-6" style="display: none;">
          <label>Related To</label>
          <select class="selectpicker" data-show-subtext="true" data-live-search="true" id="t_edit_related_to_<?php echo $t_value['id'];?>">
          <option value="leads" <?php if(isset($t_value['related_to']) && $t_value['related_to']=='leads') {?> selected="selected"<?php } ?>>Leads</option>
        <!--   <option value="client" <?php if(isset($t_value['related_to']) && $t_value['related_to']=='client') {?> selected="selected"<?php } ?>>Client</option> -->
          </select>
        </div>  
        
        <div class="form-group col-xs-12 col-sm-6 edit_tasksclass addtree_cls">
          <label>Lead</label>
                  <input type="text" class="tree_select" id="tree_select4" name="assignees[]"  placeholder="Select">
         <!--  <select class="selectpicker" data-show-subtext="true" data-live-search="true" id="t_edit_lead_to_<?php echo $t_value['id'];?>" >
          <option value="<?php echo $t_value['worker'];?>" ><?php echo $assigned['crm_email_id'];?></option>
          </select> -->
    <!--       <select class="selectpicker" data-show-subtext="true" data-live-search="true" id="t_edit_lead_to_<?php echo $t_value['id'];?>" >
          <?php foreach ($assigned as $key => $value) {
           ?>       
          <option value="<?php echo $value['id'];?>" <?php if(isset($t_value['leads_to']) && $t_value['leads_to']==$value['id']) { ?> selected="selected"<?php } ?> ><?php echo $value['crm_name'];?></option>
          <?php } ?>
          </select> -->
     <!--      <div class="dropdown-sin-32 lead-form-st">
                     <select id="t_edit_lead_to_<?php echo $t_value['id'];?>" name="for_worker[]" multiple placeholder="Select" class="task_assign" >
          
                       <?php if(count($assigned)>0 && !empty($assigned) ){
                        ?>
                         <option disabled>Staff</option>
                        <?php
                         foreach ($assigned as $ass_key => $ass_value) {
                          if(!empty($ass_value['id'])){
                      
                      ?>
                       <option value="<?php echo $ass_value['id'];?>" <?php if(in_array($ass_value['id'], explode(',',$t_value['worker']) )) { ?> selected="selected"<?php } ?> ><?php echo $ass_value['crm_name'];?></option>
                      <?php 
                      }
                      }
                       }
                    
                       if(count($team)>0 && !empty($team)){
                        ?>
                         <option disabled>Team</option>
                        <?php
                          foreach ($team as $ass_key => $ass_value) {
                             if(!empty($ass_value['id'])){
                      
                      ?>
                       <option value="tm_<?php echo $ass_value['id'];?>" <?php if(isset($t_value['team']) && in_array($ass_value['id'], explode(',',$t_value['team']))) { ?> selected="selected" <?php } ?> ><?php echo $ass_value['team'];?></option>
                      <?php 
                    }
                      }
                       }
                              if(count($dept)>0 && !empty($dept)){
                                ?>
                                 <option disabled>Department</option>
                                 <?php
                          foreach ($dept as $ass_key => $ass_value) {
                       if(!empty($ass_value['id'])){
                      ?>
                       <option value="de_<?php echo $ass_value['id'];?>" <?php if(isset($t_value['department']) && in_array($ass_value['id'], explode(',',$t_value['department']))) { ?> selected="selected" <?php } ?> ><?php echo $ass_value['new_dept'];?></option>
                      <?php 
                    }
                      }
                       }

                       ?>
                     </select>
                     </div> -->
        </div>  
        
        <div class="tags-allow1 floating_set">
        <h4><i class="fa fa-tags fa-6" aria-hidden="true"></i> Tags</h4>
        <input type="text" value="<?php echo $t_value['tag'];?>" name="t_edit_tags_<?php echo $t_value['id'];?>" id="t_edit_tags_<?php echo $t_value['id'];?>" class="tags" />
        </div>
        
        <div class="formgroup col-sm-12">
          <label>Description</label>
          <textarea rows="3" name="t_edit_description" id="t_edit_descr_<?php echo $t_value['id'];?>"><?php echo $t_value['description'];?></textarea>
        </div>
    </div>
    </div>
    <div class="modal-footer">
            <a href="#" data-dismiss="modal">Close</a>
            <input type="submit" name="save" id="save" class="edit_task_leads"  data-id="<?php echo $t_value['id'];?>" value="Save" data-value="<?php echo $lead_id;?>">
          </div>
    </form>
    </div>
    </div>
 
  <input type="hidden" name="t_edit_img_name_<?php echo $t_value['id'];?>" id="t_edit_img_name_<?php echo $t_value['id'];?>" value="<?php echo $t_value['attach_file'];?>">

<?php } ?>

<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script src="<?php echo base_url();?>assets/tree_select/comboTreePlugin.js"></script>
<script src="<?php echo base_url();?>assets/tree_select/icontains.js"></script>

<script type="text/javascript">
  $(document).ready(function() {


    var tree_select = <?php echo json_encode( GetAssignees_SelectBox() ); ?>;

         $('.tree_select').comboTree({ 
            source : tree_select,
            isMultiple: true
         });

                  $('.edit_tasksclass .comboTreeItemTitle').click(function(){ 
var id = $(this).attr('data-id');
            var id = id.split('_');

            $('.edit_tasksclass .comboTreeItemTitle').each(function(){
            var id1 = $(this).attr('data-id');
            var id1 = id1.split('_');
            //console.log(id[1]+"=="+id1[1]);
            if(id[1]==id1[1])
            {
               $(this).toggleClass('disabled');
            }
            });
            $(this).removeClass('disabled');
         });         



          var arr1 = <?php echo json_encode($assign_data) ?>;

           var assign_check =  <?php echo $assign_check ?>;
          
          //alert(arr1);

          //  alert(arr1);
         
                 var unique = arr1.filter(function(itm, i, arr1) {
              return i == arr1.indexOf(itm);
          });

                // alert(unique);


                 $('.edit_tasksclass .comboTreeItemTitle').each(function(){
      $(this).find("input:checked").trigger('click');
               var id1 = $(this).attr('data-id');
                var id = id1.split('_');
             if(jQuery.inArray(id[0], unique) !== -1)
               {
                  $(this).find("input").trigger('click');
                    if(assign_check==0)
                  {
                     $(this).toggleClass('disabled');
                  } 
              }
                    
               });




            //   var date = $('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
   //$( ".datepicker" ).datepicker({dateFormat: 'dd/mm/yy',minDate:0});
     $( document ).ready(function() {
   $(".dob_picker").datepicker({ dateFormat: 'dd-mm-yy',minDate:0  }).val();
  //$(document).on('change','.othercus',function(e){
  });

  $('.tags').tagsinput({
      allowDuplicates: true
    });
    
    $('.tags').on('itemAdded', function(item, tag) {
        $('.items').html('');
    var tags = $('.tags').tagsinput('items');
      
    });


    $( function() {
    $('#date_of_creation1,#date_of_creation2').datepicker({dateFormat: 'dd-mm-yy',minDate:0});
  } );

    var slider = document.getElementById("myRange");
    var output = document.getElementById("demo");
    output.innerHTML = slider.value;

    slider.oninput = function() {
    output.innerHTML = this.value;
    }

    CKEDITOR.replace('editor4');

});

  $(document).on('change','.t_edit_repeated_too',function(){
      var id = $(this).attr("data-id");

  var x_cal = $(this).val();
  //alert(x_cal);
  if(x_cal!='' && x_cal!='custom')
  {
    //alert('one');
    $('#t_edit_ends_on_div_'+id).css('display','block');
    $('#t_edit_rep_no_'+id).css('display','none');
    $('#t_edit_rep_cnt_'+id).css('display','none');
  }else if(x_cal!='' && x_cal=='custom'){
   // alert('two');
    $('#t_edit_rep_no_'+id).css('display','block');
    $('#t_edit_rep_cnt_'+id).css('display','block');
    $('#t_edit_ends_on_div_'+id).css('display','block');
  }else{
   // alert('three');
    $('#t_edit_ends_on_div_'+id).css('display','none');
    $('#t_edit_rep_no_'+id).css('display','none');
    $('#t_edit_rep_cnt_'+id).css('display','none');
  }
});
</script>
<script>
   var Random = Mock.Random;
   var json1 = Mock.mock({
     "data|10-50": [{
       name: function () {
         return Random.name(true)
       },
       "id|+1": 1,
       "disabled|1-2": true,
       groupName: 'Group Name',
       "groupId|1-4": 1,
       "selected": false
     }]
   });
   
   $('.dropdown-mul-1').dropdown({
     data: json1.data,
     limitCount: 40,
     multipleMode: 'label',
     choice: function () {
       // console.log(arguments,this);
     }
   });
   
   var json2 = Mock.mock({
     "data|10000-10000": [{
       name: function () {
         return Random.name(true)
       },
       "id|+1": 1,
       "disabled": false,
       groupName: 'Group Name',
       "groupId|1-4": 1,
       "selected": false
     }]
   });
   
   $('.dropdown-mul-2').dropdown({
     limitCount: 5,
     searchable: false
   });
   
   $('.dropdown-sin-1').dropdown({
     readOnly: true,
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });
      $('.dropdown-sin-32').dropdown({
     limitCount: 50,
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });
    </script>