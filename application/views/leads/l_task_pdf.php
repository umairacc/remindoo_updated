<!DOCTYPE html>
<html>
<head>
  <style>
  
         #pdf {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#pdf td, #pdf th {
    border: 1px solid #ddd;
    padding: 8px;
}

#pdf tr:nth-child(even){background-color: #f2f2f2;}

#pdf td
{
  font-size: 13px;
}

#pdf th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #9E9E9E;
    color: white;
    font-size: 14px !important;
    white-space: nowrap;
}

p{
  color: #D7CCC8;
}

caption{
  color: #555;
  font-size: 22px;
  margin-bottom: 20px;
}

  </style>
</head>
<body>
    <table id="example2" class="data-available-1 display example2" cellspacing="0" width="100%">
                <thead>
                  <tr>

                    <th>S.no</th>
                    <th>Subject</th>
                    <th>Start Date</th>
                    <th>Due Date</th>
                    <th>Assignee</th>
                    <th>Tag</th>
                    <th>Priority</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                 $i=1;
                  foreach ($sql as $sql_key => $sql_value) {
                    $assigned_staff_id = $sql_value['worker'];
                    $user_name = $this->Common_mdl->select_record('user','id',$assigned_staff_id);

                  ?>
                      <tr>
                    <td> <?php echo $i;?></td>
                    <td> <?php echo $sql_value['subject'];?></td>
                    <td> <?php echo $sql_value['start_date'];?></td>
                    <td> <?php echo $sql_value['end_date'];?></td>
                    <td> <?php echo $user_name['crm_name'];?></td>
                    <td> <?php echo $sql_value['tag'];?></td>
                    <td> <?php echo $sql_value['priority'];?></td>
                  </tr>
                  <?php $i++;} ?>
                </tbody>
</table>
</body>
</html>