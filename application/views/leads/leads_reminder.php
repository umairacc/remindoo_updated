 <table id="example2" class="data-available-1 display example2" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>Subject</th>
                    <th>Description</th>
                    <th>Date</th>
                    <th>Remind</th>
                    <th>Option</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  $ex_val=$this->Common_mdl->get_module_user_data( 'LEADS' ,$lead_id );
                  foreach ($sql as $sql_key => $sql_value) {
                    // $assigned_staff_id = $sql_value['assigned_staff_id'];
                    // $user_name = $this->Common_mdl->select_record('user','id',$assigned_staff_id);
                      // $ex_val=explode(',', $assigned_staff_id);
                                         $var_array=array();
                                         if(count($ex_val)>0)
                                         {
                                          foreach ($ex_val as $key => $value) {
                                        
                                           $user_name = $this->Common_mdl->select_record('user','id',$value);
                                            array_push($var_array, $user_name['crm_name']);
                                           }
                                          
                                         }

                  ?>
                    <tr>
                      <td> <?php echo $sql_value['subject'];?></td>  
                      <td> <?php echo $sql_value['description'];?></td>
                      <td><?php echo date('d-m-Y',strtotime($sql_value['reminder_date'])) ?></td>
                      <td> <?php echo /*$user_name['crm_name'];*/ implode(',',$var_array) ?></td>
                      <td>
                      <p class="action_01 delete_action">
              

                        <a href="#" data-target="#deleteconfirmation" data-toggle="modal" data-id="<?php echo $sql_value['id'];?>" onclick="return delete_data(this,'delete_reminder');" ><i class="icofont icofont-ui-delete deleteUser" aria-hidden="true" <?php if($_SESSION['permission']['Leads']['delete']!=1){ ?> style="display: none;" <?php } ?>></i></a>

                        <a href="#" data-toggle="modal" data-target="#edit-reminders_<?php echo $sql_value['id'];?>" <?php if($_SESSION['permission']['Leads']['edit']!=1){ ?> style="display: none;" <?php } ?>><i class="icofont icofont-edit" aria-hidden="true"></i></a>                      </p>
                      </td>
                  </tr>
                  <?php } ?>
                </tbody>
</table>