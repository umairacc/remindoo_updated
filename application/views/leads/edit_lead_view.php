<?php $this->load->view('includes/header');
   $succ = $this->session->flashdata('success');
   $error = $this->session->flashdata('error');

   
   ?>
   <!-- Tree Select -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/tree_select/style.css?ver=2">
<!-- / -->

   <div class="pcoded-content leads-section floating_set fr-pdng">
    <div class="lead-post-01">
   <form id="edit_leads_form" name="edit_leads_form"  method="post" class="edit_leads_form">
    <div class="deadline-crm1 floating_set">
      <div class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
        <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
        <li class="nav-item"><a class="nav-link">Edit Lead</a></li></ul>
        
      </div>
      <!-- <div class="modal-footer f-right">
            <input type="hidden" id="for_edit_page" name="for_edit_page" value="<?php echo $leads['id'];?>" >
            <input type="submit" name="save" id="save" value="Update">
    
        </div> -->
    </div>
    <div class="proposal-sent-sample floating_set">
      <div class="management_section floating_set ">
       <div id="new_lead" class="lead-summary2 floating_set new-leadss remove-lead07 lead-bgset3">
               
                  <input type="hidden" name="kanpan" id="kanpan" value="0">



                  <div class="space-equal-data col-sm-6">
                      <div class="lead-popupsection1 floating_set">
                        <div class="formgroup col-xs-12 col-sm-6">
                              <label class="col-form-label">Company</label>
                                <?php if (is_numeric($leads['company']))
                                 {
                                  $company_name=$this->Common_mdl->get_field_value('client','crm_company_name','id',$leads['company']);
                                  ?>
                                  <input type="hidden" name="newcompany_name" id="newcompany_name" class="clr-check-client" value="<?php echo $leads['company']; ?>">
                                  <?php
                                 }
                                 else
                                 {
                                  $company_name=$leads['company'];
                                 }?>
                             <div class="edit-field-popup1">
                                 <input type="text" name="company" id="company" value="<?php echo $company_name;?>"  class="autocomplete_client clr-check-client">
                              </div>
                         </div>
                         <div class="col-xs-12 col-sm-4  lead-form1 ">
                            <label>Lead status</label>
                            <div class="dropdown-sin-1">
                            <select name="lead_status" id="lead_status" class="clr-check-select-cus" placeholder="Nothing Selected" <?php if(strtolower($status_name['status_name'])=='customer'){
                              echo "disabled";
                             }?>>
                               <!-- <option value="">Nothing selected</option> -->
                              <?php foreach($leads_status as $ls_value) { ?>
                            <option value='<?php echo $ls_value['id'];?>' <?php  if($leads['lead_status']==$ls_value['id']){ echo "selected='selected'"; }?>><?php echo $ls_value['status_name'];?></option>
                            <?php } ?>

                            </select>
                            </div>
                         </div>
                         <div class="formgroup col-xs-12 col-sm-6">
                              <label class="">Name</label>
                              <div class="">
                                 <input type="text" name="name" id="name" class="clr-check-client"  value="<?php echo $leads['name'];?>">
                              </div>
                           </div>
                           <div class="formgroup col-xs-12 col-sm-6">
                              <label class=" col-form-label">Position</label>
                              <div class=" edit-field-popup1">
                                  <input type="text" name="position" id="position" class="clr-check-client" value="<?php echo $leads['position'];?>">
                              </div>
                           </div>
                           <div class="formgroup col-xs-12 col-sm-6">
                              <label class=" col-form-label">Email Address</label>
                              <div class=" edit-field-popup1">
                                  <input type="text" name="email_address" id="email_address" class="clr-check-client" value="<?php echo $leads['email_address'];?>">
                              </div>
                           </div>
                           <div class="formgroup col-xs-12 col-sm-6 con-pho">
                            <label class=" col-form-label">Phone</label>
                              <div class="col-sm-12 hol">
                                <?php 
                                 if(isset($leads['phone']) && ($leads['phone']!='') )
                                  { 
                                    $ctry=explode('-', $leads['phone']); 
                                  }else
                                  {
                                    $ctry[0]='';
                                    $ctry[1]=$leads['phone'];

                                  }
                                 ?>
                               <div class="dropdown-sin-2" >
                                  <select name="country_code" id="country_code" class="country_code clr-check-select-cus" placeholder="Nothing Selected" >
                                     <option value=''>Nothing selected</option>
                                     <?php foreach($countries as $key=> $lsource_value) { ?>
                                     <option value='<?php echo $lsource_value['phonecode'];?>' <?php  if( $ctry[0]==$lsource_value['phonecode']){ echo "selected='selected'"; }?> ><?php echo $lsource_value['sortname'].'-'.$lsource_value['phonecode'];?></option>
                                     <?php } ?>
                                   
                                  </select>
                               </div>
                           <input type="text" name="phone" id="phone" class="telephone_number clr-check-client" value="<?php if(isset($ctry[1]) && $ctry[1]!='') { echo $ctry[1]; }?>" >
                           </div>
                          </div>
                          <div class="col-xs-12 col-sm-4  lead-form1 addtree">
                            <label>Assigned</label>
                            <div class="dropdown-sin-31">
                              <input type="text" class="tree_select" id="tree_select1" name="assignees[]"  placeholder="Select">
                              <input type="hidden" id="assign_role" name="assign_role" >
                            </div>
                         </div>
                          
                         <div class="col-xs-12 col-sm-4  lead-form1 ">
                            <label>Source</label>
                            <div class="dropdown-sin-2">
                            <select name="source[]" id="source" class="<?php echo $leads['source'];?> clr-check-select-cus" placeholder="Nothing Selected" multiple="multiple" >
                              <?php foreach($source as $lsource_value) { ?>
                               <option value='<?php echo $lsource_value['id'];?>' <?php  if(in_array($lsource_value['id'], explode(',',$leads['source']))){ echo "selected='selected'"; }?>><?php echo $lsource_value['source_name'];?></option>
                            <?php } ?>
                              
                            </select>
                            </div>
                         </div>
                         <div class="formgroup col-xs-12 col-sm-6">
                          <label class=" col-form-label">Country</label>
                          <div class=" edit-field-popup1 country_select_box">
                             <select name="country" id="country" class="clr-check-select-cus">
                                <option value=''>Nothing Selected</option>
                                <?php
                                 $country = $leads['country'];
                                       if (is_numeric($country))
                                    {
                                 foreach ($countries as $c_key => $c_value) { ?>
                                   <option value="<?php echo $c_value['id']?>" <?php if($leads['country']==$c_value['id']){ echo "selected='selected'"; } ?>><?php echo $c_value['name'];?></option>
                                    <?php
                                    }
                                    }
                                    else
                                    {
                                       $country_name = $this->Common_mdl->select_record('countries','name',$country);
                                         foreach ($countries as $c_key => $c_value) { ?>
                                   <option value="<?php echo $c_value['id']?>" <?php if($country_name['id']==$c_value['id']){ echo "selected='selected'"; } ?>><?php echo $c_value['name'];?></option>
                                   <?php
                                    }
                                    }
                                    ?>
                             </select>
                          </div>
                        </div>
                        <div class="formgroup col-xs-12 col-sm-6">
                            <label class=" col-form-label">State</label>
                            <div class=" edit-field-popup1">
                               <input type="text" name="state" id="state" class="clr-check-client" value="<?php echo $leads['state'];?>">
                            </div>
                         </div>
                         <div class="formgroup col-xs-12 col-sm-6">
                            <label class=" col-form-label">City</label>
                            <div class=" edit-field-popup1">
                                <input type="text" name="city" id="city" class="clr-check-client" value="<?php echo $leads['city'];?>">
                            </div>
                         </div>
                         <div class="formgroup col-xs-12 col-sm-6">
                            <label class=" col-form-label">Zip Code</label>
                            <div class=" edit-field-popup1">
                               <input type="text" name="zip_code" id="zip_code" class="clr-check-client" value="<?php echo $leads['zip_code'];?>">
                            </div>
                          </div>
                          <div class="formgroup col-xs-12 col-sm-6">
                            <label class=" col-form-label">Address</label>
                            <div class="edit-field-popup1">
                                <textarea rows="1" name="address" id="address" class="clr-check-client"><?php echo $leads['address'];?></textarea>
                            </div>
                         </div>
                         <div class="formgroup col-xs-12 col-sm-6">
                            <label class=" col-form-label">Website</label>
                            <div class=" edit-field-popup1">
                               <input type="text" name="website" id="website" class="clr-check-client" value="<?php echo $leads['website'];?>">
                            </div>
                          </div>
                         
                      </div>                  
                  </div>

                  <div class="space-equal-data col-sm-6 secondone">
                    <div class="lead-popupsection2 floating_set">
                     
                 <!--    <div class="formgroup col-xs-12 col-sm-6">
                        <label class=" col-form-label">Default Language</label>
                        <div class=" edit-field-popup1">
                           <select name="default_language" id="default_language">
                              <option value="">System Default</option>
                             <?php foreach ($Language as $l_key => $l_value) { ?>
                                <option value="<?php echo $l_value['id'];?>" <?php if($l_value['id']==$leads['default_language']){ echo "selected='selected'"; } ?>><?php echo $l_value['name'];?></option>
                        <?php }?>
                           </select>
                        </div>
                     </div> -->
                     <div class="formgroup col-sm-12">
                        <label class=" col-form-label">Description</label>
                        <div class=" edit-field-popup1">
                            <textarea rows="3" name="description" id="description" class="clr-check-client"><?php echo $leads['description'];?></textarea>
                        </div>
                     </div>
                     <div class="tags-allow1 floating_set">
                     <div class="lead-tag-wrap">
                        <h4><i class="fa fa-tags fa-6" aria-hidden="true"></i> Tags</h4>
                        <div class="modal-footer f-right">
                          <input type="hidden" id="for_edit_page" name="for_edit_page" value="<?php echo $leads['id'];?>" >
                          <input type="submit" name="save" id="save" value="Update">                  
                        </div>
                      </div>
                         <input type="text" value="<?php echo $leads['tags'];?>" name="tags" id="tags" class="tags" />
                      </div>
                     <div class="formgroup checkbox-select1 col-sm-12 border-checkbox-section">
                        <div class="data_rights1" style="display: none">
                          <label class="custom_checkbox1"> <input type="checkbox" name="public" id="public" <?php if($leads['public']=='on'){ echo 'checked'; } ?> value="on" ></label>
                           <span>Public</span>
                        </div>
                        <div class="data_rights1">
                           <label class="custom_checkbox1"> <input type="checkbox" name="contact_today" <?php if($leads['contact_today']=='on'){ echo 'checked'; } ?> id="contact_update_today" class="contact_update_today" data-id="<?php echo $leads['id'];?>" value="on">
                          </label> <span>Contacted Today</span>
                        </div>
                     </div>
                     <?php //echo $leads['contact_today']."test test"; ?>
                        <div class="dedicated-sup col-sm-12 selectDate_update" <?php if($leads['contact_today']=='on') { ?> style="display:none;"<?php }else{ ?> style="display:block;"<?php  } ?> id="selectDate_update_<?php echo $leads['id'];?>">
                         <label><i class="fa fa-pencil fa-6" aria-hidden="true"></i> Choose contact Date</label>
                         <input type="text" name="contact_update_date" id="contact_update_date" class="datepicker" value="<?php echo $leads['contact_date'];?>" placeholder="Select your date" />
                       </div>
                  </div>

                </div>




                <?php if($leads['custom_fields']!=''){ 
                  ?>
                <div class="space-equal-data col-sm-12 secondone">
                 <h3>Custom Fields</h3>
                   <div class="lead-popupsection2 floating_set">
                 <?php  $qry_data=$this->db->query("select * from web_lead_template where id=".$leads['form_id'])->result_array();

        $form_data=json_decode($qry_data[0]['form_fields']);

        $custom_fields=$leads['custom_fields'];
        $de_custom=json_decode($custom_fields, true);
        $it_val='';$it_label='';
        foreach ($de_custom as $key => $value) {

             // echo $key."<br>2";
           foreach ($form_data as $fdkey => $fdvalue) { 

            $value_name=(isset($fdvalue->name))?$fdvalue->name:'';

            if($value_name==$key)
                 {
                 //echo $value_name.'<br>';
                  $it_val=$fdvalue->type;
                
                  $it_label=$fdvalue->label;

                 //  if($value_name=='select')
                 // {

                  
                 //  $option_value=json_decode(json_encode($fdvalue->values), true);

                 // }
                 $ex_val=explode("-", $value_name);

                  if(isset($ex_val[2])|| isset($ex_val[1]))
                  {
                            $it_val1=$fdvalue->name;
                            $val1=(isset($ex_val[2]))?$ex_val[2]:'';
                   if($value_name=='radio-group-'.$val1 || $value_name=='select-'.$ex_val[1])
                  
                 {
                 // echo 'hai';
                 
                  $option_value=json_decode(json_encode($fdvalue->values), true);
                  $selected=$value;
                 }

                 // print_r();
                 // echo '<br>';
                // exit;
               }

                 

                 }


               //   if($value_name==$key)
               //   {
               //    $it_val=$fdvalue->type;
               //    $it_label=$fdvalue->label;
               //   }
               //        $ex_val=explode("-", $value_name);

                      
               //    if(isset($ex_val[2])|| isset($ex_val[1]))
               //    {
               //              $it_val1=$fdvalue->name;
               //              $val1=(isset($ex_val[2]))?$ex_val[2]:'';
               //     if($value_name=='radio-group-'.$val1 || $value_name=='select-'.$ex_val[1])
                  
               //   {
               //   // echo 'hai';
                 
               //    $option_value=json_decode(json_encode($fdvalue->values), true);
               //    $selected=$value;
               //   }

               //   // print_r();
               //   // echo '<br>';
               //  // exit;
               // }
              }

              if($it_val=='text')
              {
                ?>
               
                         <div class="formgroup col-xs-12 col-sm-6">
                            <label class=" col-form-label"><?php echo $it_label; ?></label>
                            <div class=" edit-field-popup1">
                               <input type="text" name="<?php echo $key; ?>" id="<?php echo $key; ?>" value="<?php echo $value; ?>">
                            </div>
                         </div>
                
                <?php
              }
              if($it_val=='textarea')
              {
                ?>
                 <div class="formgroup col-xs-12 col-sm-6">
                        <label class=" col-form-label"><?php echo $it_label; ?></label>
                        <div class=" edit-field-popup1">
                            <textarea rows="3" name="<?php echo $key; ?>" id="<?php echo $key; ?>"><?php echo $value; ?></textarea>
                        </div>
                </div>

                <?php
              }

                
              if(isset($ex_val[1])){
                       if($it_val1=='select-'.$ex_val[1])
             // if($it_val=='text_field' || $it_val=='email')
              {
                       // print_r($option_value);                
                ?>
               
                         <div class="formgroup col-xs-12 col-sm-6">
                            <label class=" col-form-label"><?php echo $it_label; ?></label>
                            <div class=" edit-field-popup1">
                              <!--  <input type="text" name="<?php echo $key; ?>" id="<?php echo $key; ?>" value="<?php echo $value; ?>"> -->
                              <select  name="<?php echo $it_val1; ?>" id="<?php echo $key; ?>" >
                              <?php foreach ($option_value as $key => $value) { ?>

                              <option value="<?php echo $value['value'] ?>" <?php echo (isset($selected)&& $selected==$value['value']  )?'selected':''; ?> ><?php echo $value['label'] ?></option>
                             <?php } ?>
                              </select>
                            </div>
                         </div>
                
                <?php
              }
            }
               // print_r($ex_val[2]);
               if(isset($ex_val[2])){

              //  echo "check";
 

                           if($it_val1=='radio-group-'.$ex_val[2])
             // if($it_val=='text_field' || $it_val=='email')
              {
                       // print_r($option_value);                
                ?>
               
                         <div class="formgroup col-xs-12 col-sm-6">
                            <label class=" col-form-label"><?php echo $it_label; ?></label>
                            <div class=" edit-field-popup1">
                             
                              <?php foreach ($option_value as $key => $value) { ?>

                      
                              <input type="radio"  name="<?php echo $it_val1; ?>" id="<?php echo $key; ?>" value="<?php echo $value['value'] ?>"  <?php echo (isset($selected)&& $selected==$value['value']  )?'checked':''; ?> ><?php echo $value['label'] ?><br>
                             <?php } ?>
                        
                            </div>
                         </div>
                
                <?php
              }

            }


                ?>
             
                <?php
       
           }
        $data=array();
         ?>
                 </div>
                </div>

                <?php } ?>
                  
                  </form>  
                </div>
              </div> <!-- new lead-->
      </div>
    </div>
   </div>

   <style type="text/css">
   .ui-datepicker
   {
   z-index: 9999999999 !important;
   }
</style>
<!-- Modal -->
<?php $this->load->view('includes/footer');?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
   $(document).ready(function() {
   
   // $('#spl-datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
   //$('.spl-datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
   
    $("body").delegate(".spl-datepicker", "focusin", function(){
          $(this).datepicker({ dateFormat: 'dd/mm/yy' }).val();
           //var date = $('#datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
   
   
      });
   
     $( ".leads_popup" ).on('shown.bs.modal', function(){
   
       $("body").css("overflow-y", "hidden");
   });
   
      $('.leads_popup').on('hidden.bs.modal', function () {
       // do something…
        $("body").css({"overflow":"auto"});
    })
    
   
   
        //   var date = $('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
   /*$( ".datepicker" ).datepicker({
          dateFormat: 'yy-mm-dd'
      });*/
   
   $('.tags').tagsinput({
        allowDuplicates: true
      });
      
      $('.tags').on('itemAdded', function(item, tag) {
          $('.items').html('');
   var tags = $('.tags').tagsinput('items');
        
      });
   });
   
   $("#contact_today").click(function(){
    if($("#contact_today").is(':checked')){
      $(".selectDate").css('display','none');
    }else{
      $(".selectDate").css('display','block');
    }
   });
   
   $(".contact_update_today").click(function(){
    var id = $(this).attr("data-id");
    if($(this).is(':checked')){
   
      $("#selectDate_update_"+id).css('display','none');
    }else{
      $("#selectDate_update_"+id).css('display','block');
    }
   });
   
    $("#all_leads").dataTable({
          "iDisplayLength": 10,
          "scrollX": true
       });
   
</script>

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript" src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>
<script type="text/javascript">
   $(document).ready(function() {
    //$('#kanban_tab').load('leads/kanban_test');
   // $('#kanban_tab').load('leads/sample_test');

   $('.tags').tagsinput({
        allowDuplicates: true
      });
      
      $('.tags').on('itemAdded', function(item, tag) {
          $('.items').html('');
    var tags = $('.tags').tagsinput('items');
        
      });
   
   
      $( function() {
      $('#date_of_creation1,#date_of_creation2').datepicker();
    } );
   
    var slider = document.getElementById("myRange");
    var output = document.getElementById("demo");
    output.innerHTML = slider.value;
   
    slider.oninput = function() {
    output.innerHTML = this.value;
    }
   
    CKEDITOR.replace('editor4');
   
   });
   
   


// $(document).ready(function(){
// $("#reset_function").click(function(){

// $("#leads_form").reset();
// });});
function clear_form_elements(ele) {

   $(ele).find(':input').each(function() {
    
        switch(this.type) {
            case 'password':
            case 'select-multiple':
            case 'select-one':
            case 'text':
            case 'textarea':
                $(this).val('');
                break;
            case 'checkbox':
            case 'radio':
                this.checked = false;
        }

    });

      $('.dropdown-chose-list').each(function(){
        // alert('zzz');
         $(this).html('<span>Nothing Selected</span>');
      
        });

}
   </script>
   <!--- *********************************************** -->

<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
<script src="<?php echo base_url();?>assets/js/mock.js"></script>
 <script>
    var Random = Mock.Random;
    var json1 = Mock.mock({
      "data|10-50": [{
        name: function () {
          return Random.name(true)
        },
        "id|+1": 1,
        "disabled|1-2": true,
        groupName: 'Group Name',
        "groupId|1-4": 1,
        "selected": false
      }]
    });

    $('.dropdown-mul-1').dropdown({
      data: json1.data,
      limitCount: 40,
      multipleMode: 'label',
      choice: function () {
        // console.log(arguments,this);
      }
    });

    var json2 = Mock.mock({
      "data|10000-10000": [{
        name: function () {
          return Random.name(true)
        },
        "id|+1": 1,
        "disabled": false,
        groupName: 'Group Name',
        "groupId|1-4": 1,
        "selected": false
      }]
    });

$(".country_select_box").dropdown({
      input: '<input type="text" maxLength="20" placeholder="Search">'
    });

    $('.dropdown-mul-2').dropdown({
      limitCount: 5,
      searchable: false
    });

    $('.dropdown-sin-1').dropdown({
      readOnly: true,
      input: '<input type="text" maxLength="20" placeholder="Search">'
    });

    $('.dropdown-sin-2').dropdown({
      limitCount: 50,
      input: '<input type="text" maxLength="20" placeholder="Search">'
    });
    $('.dropdown-sin-3').dropdown({
      limitCount: 50,
      input: '<input type="text" maxLength="20" placeholder="Search">'
    });
   $('.dropdown-sin-4').dropdown({
     limitCount: 50,
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });
  </script>
  <script type="text/javascript">
  $(document).ready(function(){    
    $.validator.setDefaults({
        ignore: []
    });
});
   $(document).ready(function(){
   
      $( ".edit_leads_form" ).validate({

        errorPlacement: function(error, element) {
                
                 if (element.attr("name") == "lead_status" )
                    error.insertAfter(".dropdown-sin-1");
                  else if (element.attr("name") == "source[]" )
                    error.insertAfter(".dropdown-sin-2");
                  else if (element.attr("name") == "assignees[]" )
                    error.insertAfter(".dropdown-sin-31");
             
                else
                    error.insertAfter(element);
            },
          rules: {
          lead_status:"required",
         
          "source[]": "required",  
          "assignees[]":"required",
          name: "required",
           address:"required",
          description:"required",
           email_address:{required: true,email:true},
           phone : {required:true,
                         number: true},
 
           company: {
            required:true,
           //   required: function(element) {
         
           //   if ($("#company").val()=='') {
           //      return false;
           //   }
           //   else {
           //   return true;
           //   }
           // },

   },
          //company:"required",
        },
        messages: {
          lead_status: "Please enter your Status",
          "source[]": "Please enter your Source",
          "assignees[]": "Please enter your Assign",
          name: "Please enter your Name",
          address:"Please enter your address",
          description:"Please enter your description",
          email_address:"Please enter Your Email",
          phone:"Please enter Your Phone no",
          company:"Please Select Company",
        },
        submitHandler: function(form) {
       // alert('check');
          var Assignees=[];
      
          $('.comboTreeItemTitle input[type="checkbox"]:checked').each(function(){
            var id = $(this).closest('.comboTreeItemTitle').attr('data-id');
            var id = id.split('_');
            Assignees.push( id[0] );
             });
        
           var assign_role = Assignees.join(',');
           $('#assign_role').val(assign_role);
             $(form).attr('action', '<?php echo base_url()?>leads/editLeads/<?php echo $leads['id'];?>');
              $(form)[0].submit();            
   

      }
        
        

          
      });

    });
/** 14-08-2018 **/
  $('.country_code').on('change input', function() {
  $(this).val($(this).val().replace(/([^+0-9]+)/gi, ''));
});
 $('.telephone_number').on('change input', function() {
  $(this).val($(this).val().replace(/([^0-9]+)/gi, ''));
});
 /** end of 14-08-2018 **/
      </script>



      <!--Tree Select -->

<script src="<?php echo base_url();?>assets/tree_select/comboTreePlugin.js"></script>
<script src="<?php echo base_url();?>assets/tree_select/icontains.js"></script>

<script>
	$(document).ready(function(){
      function editLeadView(){
        $(".clr-check-select-cus").change(function(){
        $(this).next().addClass("clr-check-select-cus-wrap");
      })

      $(".clr-check-select-cus").each(function(){
        if($(this).val() !== ""){
        $(this).next().addClass("clr-check-select-cus-wrap");
        }  
      })
    }

    editLeadView();

		$(".clr-check-client").keyup(function(){
			var clr = $(this).val();
			if(clr.length >= 3){
				$(this).addClass("clr-check-client-outline");
			}else{
				$(this).removeClass("clr-check-client-outline");
			}
		});
      $(".clr-check-client").each(function(i){
      if($(this).val() !== ""){
        $(this).addClass("clr-check-client-outline");
      }  
    })

		$(".clr-check-select").change(function(){
			var clr_select = $(this).val();
         if(clr_select !== ' '){
            $(this).addClass("clr-check-select-client-outline");
         }else{
				$(this).removeClass("clr-check-select-client-outline");
			}
		});

    
	});
</script>

<script type="text/javascript">
         /** 29-08-2018 **/
      
         var tree_select = <?php echo json_encode( GetAssignees_SelectBox() ); ?>;        

         $('.tree_select').comboTree({ 
            source : tree_select,
            isMultiple: true
         });

            $('.comboTreeItemTitle').click(function(){ 
            var id = $(this).attr('data-id');
            var id = id.split('_');

            $('.comboTreeItemTitle').each(function(){
            var id1 = $(this).attr('data-id');
            var id1 = id1.split('_');
            //console.log(id[1]+"=="+id1[1]);
            if(id[1]==id1[1])
            {
               $(this).toggleClass('disabled');
            }
            });
            $(this).removeClass('disabled');
         });
            
        var arr=<?php echo json_encode($asssign_group); ?>;
        var assign_check=<?php echo $assign_check ?>;
           
        //alert(assign_check);

    $('.comboTreeItemTitle').each(function(){
               var id1 = $(this).attr('data-id');
                var id = id1.split('_');
             if(jQuery.inArray(id[0], arr) !== -1)
               {
                  $(this).find("input").trigger('click');
                  if(assign_check==0)
                  {
                     $(this).toggleClass('disabled');
                  }

              }
                    
               });
                  


       $(".autocomplete_client").keyup(debounce(function(){
       
                        //type = $(this).data('type');   
                        
                       // if(type =='invoice' )
                          var autoTypeNo=0;
                        // if(type =='invoice_number' )autoTypeNo=0;                         
                        $(this).autocomplete({
                            source: function( request, response ) {
                                $.ajax({
                                    url : '<?php echo base_url();?>/Leads/lead_SearchCompany',
                                    //dataType: "json",
                                     method: 'post',
                                     data: {
                                        name_startsWith: request.term,
                                     //   datatype:type,
                                         //type: type
                                     },
                                     success: function(data) {
                                  // alert(data);
                                       var dispn = $.parseJSON(data);   
                                        response( $.map(dispn, function( item ) {
                                                var code = item.split("|"); 
                                               // alert(code[0]);           
                                                return {
                                                    label: code[autoTypeNo],
                                                    value: code[autoTypeNo],
                                                    data : item
                                                }
                                            })); 
                                        }
                                });
                            },
                            autoFocus: true,            
                            minLength: 0,                            

                            select: function( event, ui ) 
                            {
                                var names = ui.item.data.split("|");   
                               // alert(names[2]);

                                 $('#address').text(names[1]);
                                 $('#description').val(names[2]);
                                 $('#zip_code').val(names[3]);

                                // $('#supplierAddress').val(names[2]);
                                // $('#supplier_mobno').val(names[3]);
                                // $('#datearrivel').val(names[4]);
         
                                
                        }
                 });
  },500));
       

</script>

<!--/ Tree Select -->



   <!-- **************************************************-->