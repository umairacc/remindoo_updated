<?php $this->load->view('includes/header'); ?>


<div class="realign-quote01">
	<div class="leads-section floating_set">

		<div class="addnewclient_pages1 floating_set">
			<ul class="nav nav-tabs all_user1 md-tabs floating_set" id="tabs">
				<li class="nav-item it0 bbs" value="0"><a class="nav-link active" data-toggle="tab" href="#profile">Profile</a></li>
				<li class="nav-item it1" value="1"><a class="nav-link" data-toggle="tab" href="#proposal">Proposal</a></li>
				<li class="nav-item it2 hide" value="2"><a class="nav-link" data-toggle="tab" href="#tasks">Tasks</a></li>
				<li class="nav-item it2 hide" value="2"><a class="nav-link" data-toggle="tab" href="#attachments">Attachments</a></li>
				<li class="nav-item it2 hide" value="2"><a class="nav-link" data-toggle="tab" href="#reminder">Reminders</a></li>
				<li class="nav-item it2 hide" value="2"><a class="nav-link" data-toggle="tab" href="#notes">Notes</a></li>
				<li class="nav-item it2 hide" value="2"><a class="nav-link" data-toggle="tab" href="#activity-log">Activity Log</a></li>
			</ul>
		</div> <!-- tab close -->


		<div class="management_section floating_set">
			<div class="card-block ">

				

				<!-- tab start -->
				<div class="tab-content">
					<div id="profile" class="tab-pane fade in active">

						<div class="floating_set">
							<div class="convert-customer pull-left">
								<a href="#">Edit <i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
								<div class="dropdown">
									<button class="dropdown-toggle" type="button" data-toggle="dropdown">More
										<span class="caret"></span></button>
									<ul class="dropdown-menu">
										<li><a href="#">mark as lost</a></li>
										<li><a href="#"> mark as junk </a></li>
										<li class="delete-mark1"><a href="#"> delete Lead</a></li>
									</ul>
								</div>
							</div>
							<div class="convert-customer1 pull-right">
								<a href="#"><i class="fa fa-user-o fa-6" aria-hidden="true"></i> CONVERT TO CUSTOMER</a>
							</div>
						</div>

						<div class="floating_set general-inform1">
							<div class="col-sm-4 custom-fields11">
								<h4>Information</h4>
								<div class="form-status1">
									<span>Lead Status</span>
									<strong>New</strong>
								</div>
								<div class="form-status1">
									<span>Position</span>
									<strong>Textile Cutting Machine Operator</strong>
								</div>
								<div class="form-status1">
									<span>Email Address</span>
									<strong><a href="#">admin@gmail.com</a></strong>
								</div>
								<div class="form-status1">
									<span>Website</span>
									<strong><a href="#">facebook.com</a></strong>
								</div>
								<div class="form-status1">
									<span>Phone</span>
									<strong><a href="#">9876451320</a></strong>
								</div>
								<div class="form-status1">
									<span>Company</span>
									<strong>Techleaf</strong>
								</div>
								<div class="form-status1">
									<span>Address</span>
									<strong>567 Forrest oval</strong>
								</div>
								<div class="form-status1">
									<span>City</span>
									<strong>madurai</strong>
								</div>
								<div class="form-status1">
									<span>State</span>
									<strong>Tamilnadu</strong>
								</div>
								<div class="form-status1">
									<span>Country</span>
									<strong>India</strong>
								</div>
								<div class="form-status1">
									<span>Zip code</span>
									<strong>123</strong>
								</div>
							</div>

							<div class="col-sm-4 custom-fields11 general-leads1">
								<h4>General Information</h4>
								<div class="form-status1">
									<span>Lead Status</span>
									<strong>New</strong>
								</div>
								<div class="form-status1">
									<span>Source</span>
									<strong>Facebook</strong>
								</div>
								<div class="form-status1">
									<span>Default Language</span>
									<strong>system default</strong>
								</div>
								<div class="form-status1">
									<span>Assigned</span>
									<strong>Frankie</strong>
								</div>
								<div class="form-status1">
									<span>Tags</span>
									<strong><a href="#">follow up</a><a href="#">tomorrow</a></strong>
								</div>
								<div class="form-status1">
									<span>Created</span>
									<strong>5hrs ago</strong>
								</div>
								<div class="form-status1">
									<span>Last Contact</span>
									<strong>-</strong>
								</div>
								<div class="form-status1">
									<span>Public</span>
									<strong>No</strong>
								</div>

							</div>


							<div class="col-sm-4 custom-fields11">
								<h4>Custom Fields</h4>
								<div class="form-status1">
									<span>Dedicated support</span>
									<strong>-</strong>
								</div>
							</div>

							<div class="description-lead1 col-sm-12">
								<span>Description</span>
								<p>Laudantium quia ipsa sunt quo.Et voluptates est illo soluta. Sint non molestiae laboriosam volumptatem. Numquam ratione animi repellendus voluptatem placeat dicta soluta. </p>
							</div>

							<div class="activity1 col-sm-12">
								<h4>Latest Activity</h4>
								<span>Gust Toy - created lead</span>
							</div>

						</div>
					</div> <!-- 1tab -->

					<div id="proposal" class="tab-pane fade">
						<div class="new-proposal1 floating_set">
							<a href="#">New proposal</a>
						</div>

						<div class="proposal-table1 floating_set">

							<table id="example" class="display" cellspacing="0" width="100%">

								<thead>
									<tr>
										<th>Proposal</th>
										<th>Subject</th>
										<th>Total</th>
										<th>Date</th>
										<th>Open Till</th>
										<th>Tags</th>
										<th>Date created</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>



								</tbody>
							</table>

						</div>
					</div> <!-- 2tab -->

					<div id="tasks" class="tab-pane fade">
						<div class="new-proposal1 floating_set">
							<a href="#">New Task</a>
						</div>

						<div class="proposal-table1 floating_set">

							<table id="example1" class="display" cellspacing="0" width="100%">

								<thead>
									<tr>
										<th>Proposal</th>
										<th>Subject</th>
										<th>Total</th>
										<th>Date</th>
										<th>Open Till</th>
										<th>Tags</th>
										<th>Date created</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>



								</tbody>
							</table>

						</div>
					</div> <!-- 3tab -->



					<div id="attachments" class="fileupload_01 tab-pane fade">
						<div class=" upload_input09">
							<input type="file" name="profile_image" id="profile_image">
						</div>

						<div class="cloud-upload1">
							<a href="#"><i class="fa fa-cloud-upload fa-6" aria-hidden="true"></i> Choose from Dropbox</a>
						</div>
					</div> <!-- 4tab -->

					<div id="reminder" class="tab-pane fade">
						<div class="new-proposal1 floating_set">
							<a href="#" data-toggle="modal" data-target="#reminder-lead"><i class="fa fa-bell-o fa-6" aria-hidden="true"></i> Add Lead Reminder</a>
						</div>

						<div class="proposal-table1 floating_set">

							<table id="example2" class="display" cellspacing="0" width="100%">

								<thead>
									<tr>
										<th>Proposal</th>
										<th>Subject</th>
										<th>Total</th>
										<th>Date</th>
										<th>Open Till</th>
										<th>Tags</th>
										<th>Date created</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>



								</tbody>
							</table>

						</div>
					</div> <!-- 5tab -->

					<div id="notes" class="tab-pane fade">

						<div class="notes-leads floating_set">
							<form>

								<div class="notes-list01">
									<div class="notetextarea1">
										<textarea rows="3"></textarea>
										<input type="submit" name="add note" value="Add Note">
									</div>
									<div class="checked-unicode">
										<div class="form-group">
											<input type="radio" name="radio"><label>I got in touch with this lead</label>
										</div>
										<div class="form-group">
											<input type="radio" name="radio" checked><label>I got in touch with this lead</label>
										</div>
									</div>
								</div>

								<div class="gust-toy01">
									<div class="checkbox-profile1">
										<img src="http://remindoo.org/CRMTool/uploads/1514958997.jpg" alt="User-Profile-Image">
									</div>
									<div class="timeline-lead01">
										<span>Note added: 2017-10-19 06:00:06</span>
										<strong>Gust Toy</strong>
										<p> Laudantium quia ipsa sunt quo.Et voluptates est illo soluta. Sint non molestiae laboriosam volumptatem. Numquam ratione animi repellendus voluptatem placeat dicta soluta.</p>
									</div>
									<div class="edit-timeline">
										<a href="#"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
										<a href="#"><i class="fa fa-times fa-6" aria-hidden="true"></i></a>
									</div>
								</div>

								<div class="gust-toy01">
									<div class="checkbox-profile1">
										<img src="http://remindoo.org/CRMTool/uploads/1514958997.jpg" alt="User-Profile-Image">
									</div>
									<div class="timeline-lead01">
										<span>Note added: 2017-10-19 06:00:06</span>
										<strong>Gust Toy</strong>
										<p> Laudantium quia ipsa sunt quo.Et voluptates est illo soluta. Sint non molestiae laboriosam volumptatem. Numquam ratione animi repellendus voluptatem placeat dicta soluta.</p>
									</div>
									<div class="edit-timeline">
										<a href="#"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
										<a href="#"><i class="fa fa-times fa-6" aria-hidden="true"></i></a>
									</div>
								</div>

						</div>



					</div> <!-- 6tab -->

					<div id="activity-log" class="tab-pane fade">

						<div class="enter-activity-log floating_set">

							<div class="activity-created1">
								<span>5 HRS AGO</span>
								<span>Gust Toy - created lead</span>
							</div>

							<div class="notetextarea1">
								<textarea rows="3" placeholder="Enter Activity"></textarea>
								<input type="submit" name="add note" value="Add Note">
							</div>
						</div>

					</div> <!-- 7tab -->




				</div> <!-- tab-content -->



			</div>
		</div> <!-- management_section -->

	</div> <!-- leads-section -->
</div>


<!-- Modal -->
<div id="reminder-lead" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-info-circle fa-6" aria-hidden="true"></i> Add lead reminder</h4>
			</div>
			<div class="modal-body">
				<div class="datatonotified">
					<form>
						<div class="form-group  date_birth">
							<label>Date to be notified</label>
							<div class="data-position4"><span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
								<input type="text" id="datepicker"></div>
						</div>

						<div class="form-group date_birth">
							<label>Date to be notified</label>
							<select>
								<option>Gust Toy</option>
								<option>Gust</option>
							</select>
						</div>

						<div class="form-group   date_birth">
							<label>Description</label>
							<textarea rows="3"></textarea>
						</div>

						<div class="form-group see-also-pwd">
							<input type="checkbox" name="checkbox">
							<span>Send also an email for his remember</span>
						</div>

						<div class="form-submit-lead text-right">
							<a href="#" data-dismiss="modal">Close</a>
							<a href="#">Save</a>
						</div>





					</form>
				</div>
			</div>

		</div>

	</div>
</div> <!-- modal -->


<?php $this->load->view('includes/footer'); ?>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>



<script>
	$(document).ready(function() {
		$('#example,#example1,#example2').DataTable();

		$("#datepicker").datepicker();

	});
</script>