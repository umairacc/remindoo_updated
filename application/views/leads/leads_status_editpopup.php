<?php foreach ($leads_status as $key => $value) { ?>
  <div id="leads_status_edit_<?php echo $value['id'];?>" class="modal fade" role="dialog">
      <div class="modal-dialog">
         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close close_<?php echo $value['id'];?>" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Edit Status</h4>
            </div>
            <div class="modal-body">
               <div class=" new-task-teams">
                  <!-- <form id="add_new_team" method="post" action="<?php echo base_url()?>/leads/add_new_source" enctype="multipart/form-data"> -->
                  <div class="form-group">
                     <label>New Status</label>
                     <input type="text" class="form-control" name="new_team" id="new_lead_status_<?php echo $value['id'];?>" placeholder="Enter Source name" value="<?php echo $value['status_name'];?>">
                     <label for="new_team" generated="true" class="edit_status_error_<?php echo $value['id'];?>" style="color: red;"></label>
                  </div>
                  <!-- <div class="form-group create-btn">
                     <input type="submit" name="add_task" data-id="<?php echo $value['id']?>" class="btn-primary edit_status_save" value="Update"/>
                  </div> -->
                  <!--  </form> -->
               </div>
            </div>
            <div class="modal-footer">
               <a href="#" data-dismiss="modal">Close</a>
               <input type="submit"  name="add_task" data-firm_id="<?php echo $value['firm_id']?>" data-id="<?php echo $value['id']?>" class="btn-primary edit_status_save" value="Update"/>
          </div>
         </div>
      </div>
   </div>
<?php } ?>