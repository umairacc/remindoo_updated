<table class="table client_table1 text-center display nowrap" id="alltask_status" cellspacing="0" width="100%">
               <thead>
                  <tr class="text-uppercase">
                    <th><div class="checkbox-fade fade-in-primary">
                                <label>
                                <input type="checkbox" id="select_all_status">
                                <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                </label>
                              </div></th>
                     <th>SNO</th>
                     <th>Status</th>
                     <th>Actions</th>
                  </tr> 
               </thead>

               <tbody>
                        <?php $i =1 ; foreach ($leads_status as $key => $value) {
                      ?>
                  <tr>
                   <td> <?php if($value['firm_id']!=0) { ?>
                             <div class="checkbox-fade fade-in-primary">
                                <label>
                                <input type="checkbox" class="status_checkbox" data-status-id="<?php echo $value['id'];?>">
                                <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i> </span>  
                                </label>
                             </div>
                             <?php } ?>
                            </td>
                     <td><?php echo $i;?></td>
                     <td><?php echo $value['status_name'];?></td>
                     <td>
                              <p class="action_01">
                                
                                <?php if($value['firm_id']!=0) { ?>
                                <?php if($_SESSION['permission']['Leads']['edit']=='1') {  ?>
                                <a data-toggle="modal" data-target="#leads_status_edit_<?php echo $value['id']?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                                 <?php } ?>
                                 <?php if($_SESSION['permission']['Leads']['delete']=='1') {  ?>
                                 <a href="javascript:;" data-toggle="modal" data-target="#status_deleteconfirmation" onclick="delete_status(this);" data-firm_id="<?php echo $value['firm_id']?>" data-id='<?php echo $value['id'];?>'><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>
                                 <?php } ?>
                                 
                                 <?php } ?>
                              </p>
                           </td>
                           
                        </tr>
               <?php $i++; } ?>
               </tbody>    
                     </table>
  <script type="text/javascript">
    function confirm_status(id)
{
$('#delete_particular_status'+id).show();
return false;
}
</script>