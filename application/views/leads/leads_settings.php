<?php $this->load->view('includes/header');
   $role = $this->Common_mdl->getRole($_SESSION['id']);
   $succ = $this->session->flashdata('success');
   
   ?>
<style>
   /*span.newonoff {
   background: #4680ff;
   padding: 6px 15px;
   display: inline-block;
   color: #fff;
   border-radius: 5px;
   }*/
   button.btn.btn-info.btn-lg.newonoff {
   padding: 3px 10px;
   height: initial;
   font-size: 15px;
   border-radius: 5px;
   }
   img.user_imgs {
   width: 38px;
   height: 38px;
   border-radius: 50%;
   display: inline-block;
   }
   .new-task-teams {
    box-shadow: none;
  }
</style>
<div class="pcoded-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <div class="col-sm-12">
                     <div class="deadline-crm1 floating_set">
                        <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                           <li class="nav-item">
                              <a class="nav-link" href="<?php echo base_url();?>leads">leads</a>
                              <div class="slide"></div>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link active" data-toggle="tab" href="#newactive">source</a>
                              <div class="slide"></div>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" data-toggle="tab" href="#newinactive">status</a>
                              <div class="slide"></div>
                           </li>
                        </ul>
                        <div class="count-value1 pull-right ">
                           <button id="deleteTriger" class="deleteTri" style="display:none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i> Delete</button>
                           <!-- <button type="button" class="data-turn99" data-toggle="modal" data-target="#myModalcolumn"> <i class="fa fa-pencil fa-6" aria-hidden="true"></i> Edit columns</button> -->
                           <!-- <input type="text" data-column="0"  class="search-input-text"> --> 
                        </div>
                     </div>
                     <!-- Register your self card start -->
                     <div class="col-xs-12 card">
                        <!-- admin start-->
                        <div class="client_section lead-sets col-xs-12 floating_set">
                           <div class="all_user-section floating_set">
                              <div class="import-container-1">
                                 <div class="tab-content">
                                    <div id="newactive" class="tab-pane fade in active">
                                       <div class="pull-right csv-sample01 ">
                                          <a class="btn btn-card btn-primary leadsG leads_add_popup" data-toggle="modal" data-target="#leads_source_add"><i class="fa fa-pencil fa-6" aria-hidden="true"></i> Add New</a>
                                       </div>
                                       <div class="container">
                                          <div class="page-header">
                                             <div class="page-header-title pull-left card-block">
                                                <h4 class="sub-title">Leads Source</h4>
                                             </div>
                                          </div>
                                       </div>
                                       <?php if($succ){?>
                                       <div class="modal-alertsuccess alert alert-success"
                                          >
                                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                          <div class="pop-realted1">
                                             <div class="position-alert1">
                                                <?php echo $succ; ?>
                                             </div>
                                          </div>
                                       </div>
                                       <?php } ?>
                                       <div class="all-usera1 user-dashboard-section1 leads_source">
                                          <table class="table client_table1 text-center display nowrap" id="alltask" cellspacing="0" width="100%">
                                             <thead>
                                                <tr class="text-uppercase">
                                                   <th>SNO</th>
                                                   <th>Source</th>
                                                   <th>Actions</th>
                                                </tr>
                                             </thead>
                                             <tbody>
                                                <?php $i =1 ; foreach ($source as $key => $value) {
                                                   ?>
                                                <tr>
                                                   <td><?php echo $i;?></td>
                                                   <td><?php echo $value['source_name'];?></td>
                                                   <td>
                                                      <p class="action_01">
                                                         <!-- <a href="<?php echo base_url().'leads/source_delete/'.$value['id'];?>" onclick="return confirm('Are you sure want to delete');"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a> -->
                                                         <!--     <a href="<?php echo base_url().'leads/update_source/'.$value['id'];?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a> -->
                                                         <a href="javascript:void(0)" data-id="<?php echo $value['id'];?>" class="leads_source_delete"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a> 
                                                         <a data-toggle="modal" data-target="#leads_source_edit_<?php echo $value['id']?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                                                      </p>
                                                   </td>
                                                </tr>
                                                <?php $i++; } ?>
                                             </tbody>
                                          </table>
                                       </div>
                                    </div>
                                    <!-- for status table rspt 17-04-2018 -->
                                    <div id="newinactive" class="tab-pane fade">
                                       <div class="pull-right csv-sample01 ">
                                          <a class="btn btn-card btn-primary leadsG leads_add_popup" data-toggle="modal" data-target="#leads_status_add"><i class="fa fa-pencil fa-6" aria-hidden="true"></i> Add New</a>
                                       </div>
                                       <div class="container">
                                          <div class="page-header">
                                             <div class="page-header-title pull-left card-block">
                                                <h4 class="sub-title">Leads Status</h4>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="all-usera1 user-dashboard-section1 leads_status">
                                          <table class="table client_table1 text-center display nowrap" id="alltask_status" cellspacing="0" width="100%">
                                             <thead>
                                                <tr class="text-uppercase">
                                                   <th>SNO</th>
                                                   <th>Status</th>
                                                   <th>Actions</th>
                                                </tr>
                                             </thead>
                                             <tbody>
                                                <?php $i =1 ; foreach ($leads_status as $key => $value) {
                                                   ?>
                                                <tr>
                                                   <td><?php echo $i;?></td>
                                                   <td><?php echo $value['status_name'];?></td>
                                                   <td>
                                                      <p class="action_01">
                                                         <!--  <a href="<?php echo base_url().'leads/s_delete/'.$value['id'];?>" onclick="return confirm('Are you sure want to delete');"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a>
                                                            <a href="<?php echo base_url().'leads/update_status/'.$value['id'];?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a> -->
                                                         <a href="javascript:void(0)" data-id="<?php echo $value['id'];?>" class="leads_status_delete"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a> 
                                                         <a data-toggle="modal" data-target="#leads_status_edit_<?php echo $value['id']?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                                                      </p>
                                                   </td>
                                                </tr>
                                                <?php $i++; } ?>
                                             </tbody>
                                          </table>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- end of status table -->
                                 <!-- admin close -->
                              </div>
                              <!-- Register your self card end -->
                           </div>
                        </div>
                     </div>
                     <!-- Page body end -->
                  </div>
               </div>
               <!-- Main-body end -->
               <div id="styleSelector">
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
<!-- <div id="lead_settings" class="addnew_task_lead1 modal fade" role="dialog" >
   <div class="modal-dialog">

      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">update source</h4>
         </div>
         <form id="add_new_team" method="post" action="http://remindoo.org/CRMTool//leads/updatesource/1" enctype="multipart/form-data" novalidate="novalidate">
            <div class="modal-body">
               <div class="form-group">
                  <label>New Source</label>
                  <input type="text" class="form-control" name="new_team" placeholder="Enter Status" value="Google">
               </div>
               
            </div>
            <div class="modal-footer">
               <a href="#" data-dismiss="modal">Close</a>
               <input type="submit" name="add_task" class="btn-primary" value="update">
            </div>
         </form>
      </div>
   </div>
</div> -->
<div id="leads_status_add" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add New Status</h4>
         </div>
         <div class="modal-body">
            <div class=" new-task-teams">
               <!-- <form id="add_new_team" method="post" action="<?php echo base_url()?>/leads/add_new_source" enctype="multipart/form-data"> -->
               <div class="form-group">
                  <label>New Status</label>
                  <input type="text" class="form-control" name="new_team" id="new_lead_status" placeholder="Enter Status name" value="">
                  <label for="new_team" generated="true" class="source_error add_status_error" style="color: red;"></label>
               </div>
               <!-- <div class="form-group create-btn">
                  
               </div> -->

               <!--  </form> -->
            </div>
         </div>
         <div class="modal-footer">
               <a href="#" data-dismiss="modal">Close</a>
               <input type="submit" name="add_task" id="leads_status_save" class="btn-primary" value="create"/>
          </div>
      </div>
   </div>
</div>
<!-- edit status -->
<div class="edit_status">
   <?php foreach ($leads_status as $key => $value) { ?>
   <div id="leads_status_edit_<?php echo $value['id'];?>" class="modal fade" role="dialog">
      <div class="modal-dialog">
         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close close_<?php echo $value['id'];?>" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Edit Status</h4>
            </div>
            <div class="modal-body">
               <div class=" new-task-teams">
                  <!-- <form id="add_new_team" method="post" action="<?php echo base_url()?>/leads/add_new_source" enctype="multipart/form-data"> -->
                  <div class="form-group">
                     <label>New Status</label>
                     <input type="text" class="form-control" name="new_team" id="new_lead_status_<?php echo $value['id'];?>" placeholder="Enter Source name" value="<?php echo $value['status_name'];?>">
                     <label for="new_team" generated="true" class="edit_status_error" style="color: red;"></label>
                  </div>
                  <!-- <div class="form-group create-btn">
                     <input type="submit" name="add_task" data-id="<?php echo $value['id']?>" class="btn-primary edit_status_save" value="Update"/>
                  </div> -->
                  <!--  </form> -->
               </div>
            </div>
            <div class="modal-footer">
               <a href="#" data-dismiss="modal">Close</a>
               <input type="submit" name="add_task" data-id="<?php echo $value['id']?>" class="btn-primary edit_status_save" value="Update"/>
          </div>
         </div>
      </div>
   </div>
   <?php } ?>
</div>
<!-- edit status -->
<div id="leads_source_add" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add New Source</h4>
         </div>
         <div class="modal-body">
            <div class=" new-task-teams">
               <!-- <form id="add_new_team" method="post" action="<?php echo base_url()?>/leads/add_new_source" enctype="multipart/form-data"> -->
               <div class="form-group">
                  <label>New Source</label>
                  <input type="text" class="form-control" name="new_team" id="new_lead_source" placeholder="Enter Source name" value="">
                  <label for="new_team" generated="true" class="source_error add_source_error" style="color: red;"></label>
               </div>
               <!-- <div class="form-group create-btn">
                  <input type="submit" name="add_task" id="leads_source_save" class="btn-primary" value="create"/>
               </div> -->
               <!--  </form> -->
            </div>
         </div>
         <div class="modal-footer">
               <a href="#" data-dismiss="modal">Close</a>
              <input type="submit" name="add_task" id="leads_source_save" class="btn-primary" value="create"/>
          </div>
      </div>
   </div>
</div>
<!-- source edit -->
<div class="edit_source">
   <?php foreach ($source as $key => $value) { ?>
   <div id="leads_source_edit_<?php echo $value['id'];?>" class="modal fade" role="dialog">
      <div class="modal-dialog">
         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close close_<?php echo $value['id'];?>" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Edit Source</h4>
            </div>
            <div class="modal-body">
               <div class=" new-task-teams">
                  <!-- <form id="add_new_team" method="post" action="<?php echo base_url()?>/leads/add_new_source" enctype="multipart/form-data"> -->
                  <div class="form-group">
                     <label>New Source</label>
                     <input type="text" class="form-control" name="new_team" id="new_lead_source_<?php echo $value['id'];?>" placeholder="Enter Source name" value="<?php echo $value['source_name'];?>">
                     <label for="new_team" generated="true" class="edit_source_error" style="color: red;"></label>
                  </div>
                  <!-- <div class="form-group create-btn">
                     <input type="submit" name="add_task" data-id="<?php echo $value['id']?>" class="btn-primary edit_source_save" value="Update"/>
                  </div> -->
                  <!--  </form> -->
               </div>
            </div>
            <div class="modal-footer">
               <a href="#" data-dismiss="modal">Close</a>
              <input type="submit" name="add_task" data-id="<?php echo $value['id']?>" class="btn-primary edit_source_save" value="Update"/>
          </div>
         </div>
      </div>
   </div>
   <?php } ?>
</div>
<!-- end source -->
<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<!-- <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap.js"></script> -->
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
   $(document).ready(function() {
   
   
     $("#alltask").dataTable({
      

     });
   $("#alltask_status").dataTable({
        
     });

   
   
   
   
   });

     $('#leads_source_save').click(function(){
   var lead_source=$("#new_lead_source").val();
   if(lead_source=='')
   {
     $(".add_source_error").html('Please Enter Your Source');
   }
   else
   {
      $(".add_source_error").html('');
       var data={};
   data['new_team']=lead_source;
   $.ajax({
      url: '<?php echo base_url();?>leads/leads_add_new_source/',
      type: 'post',
       data: data,
      success: function (data, status)
      {
         $('#leads_source_add').modal('hide');
         $('.leads_source').html(data);
       //  $('.leads_source').load('leads_source_table');
        $('.edit_source').load('leads_source_editpopup');
           $("#alltask").dataTable({
               // "iDisplayLength": 10,
              // "scrollX": true,
           });
      }
    });
   
   }
   
   });
   
   //$('.edit_source_save').click(function(){
   $(document).on('click','.edit_source_save',function(){
   var id=$(this).attr('data-id');
   //alert(id);
   var lead_source=$("#new_lead_source_"+id).val();
   if(lead_source=='')
   {
     $(".edit_source_error").html('Please Enter Your Source');
   }
   else
   {
      $(".edit_source_error").html('');
       var data={};
   data['new_team']=lead_source;
   $.ajax({
      url: '<?php echo base_url();?>leads/leads_updatesource/'+id,
      type: 'post',
       data: data,
      success: function (data, status)
      {
         //$('#leads_source_edit_'+id).modal('hide');
         $('#leads_source_edit_'+id).modal('hide');
         $('.modal-backdrop.show').hide();
         $('.leads_source').html(data);
         $('.edit_source').html('');
         $('.edit_source').load('leads_source_editpopup');
           $("#alltask").dataTable({
               // "iDisplayLength": 10,
              // "scrollX": true,
           });
      }
    });
   
   }
   
   });
   
   //$('.leads_source_delete').click(function(){
   $(document).on('click','.leads_source_delete',function(){
   var id=$(this).attr('data-id');
   //alert(id);
   var lead_source=$("#new_lead_source_"+id).val();
   if (confirm('Are you sure want to delete')) {
       var data={};
   data['del']='delete';
   $.ajax({
      url: '<?php echo base_url();?>leads/leads_source_delete/'+id,
      type: 'post',
       data: data,
      success: function (data, status)
      {
         //$('#leads_source_edit_'+id).modal('hide');
         $('.leads_source').html(data);
         $('.edit_source').html('');
         $('.edit_source').load('leads_source_editpopup');
           $("#alltask").dataTable({
              //  "iDisplayLength": 10,
              // "scrollX": true,
           });
      }
    });
   
   }
   
   });
   /** end of leads source **/
   
   /** for leads status **/
   $('#leads_status_save').click(function(){
   var lead_status=$("#new_lead_status").val();
   if(lead_status=='')
   {
     $(".add_status_error").html('Please Enter Your Status');
   }
   else
   {
      $(".add_status_error").html('');
       var data={};
   data['new_team']=lead_status;
   $.ajax({
      url: '<?php echo base_url();?>leads/leads_add_new_status/',
      type: 'post',
       data: data,
      success: function (data, status)
      {
         $('#leads_status_add').modal('hide');
         $('.leads_status').html(data);
       //  $('.leads_source').load('leads_source_table');
        $('.edit_status').load('leads_status_editpopup');
           $("#alltask_status").dataTable({
              //  "iDisplayLength": 10,
              // "scrollX": true,
           });
      }
    });
   
   }
   
   });
   
   //$('.edit_source_save').click(function(){
   $(document).on('click','.edit_status_save',function(){
   var id=$(this).attr('data-id');
   //alert(id);
   var lead_status=$("#new_lead_status_"+id).val();
   if(lead_status=='')
   {
     $(".edit_status_error").html('Please Enter Your Status');
   }
   else
   {
      $(".edit_status_error").html('');
       var data={};
   data['new_team']=lead_status;
   $.ajax({
      url: '<?php echo base_url();?>leads/leads_updatestatus/'+id,
      type: 'post',
       data: data,
      success: function (data, status)
      {
         //$('#leads_source_edit_'+id).modal('hide');
         $('#leads_status_edit_'+id).modal('hide');
         $('.modal-backdrop.show').hide();
         $('.leads_status').html(data);
         $('.edit_status').html('');
         $('.edit_status').load('leads_status_editpopup');
            $("#alltask_status").dataTable({
              //  "iDisplayLength": 10,
              // "scrollX": true,
           });
      }
    });
   
   }
   
   });
   
   //$('.leads_source_delete').click(function(){
   $(document).on('click','.leads_status_delete',function(){
   var id=$(this).attr('data-id');
   //alert(id);
   
   if (confirm('Are you sure want to delete')) {
       var data={};
   data['del']='delete';
   $.ajax({
      url: '<?php echo base_url();?>leads/leads_status_delete/'+id,
      type: 'post',
       data: data,
      success: function (data, status)
      {
         //$('#leads_source_edit_'+id).modal('hide');
         $('.leads_status').html(data);
         $('.edit_status').html('');
         $('.edit_status').load('leads_status_editpopup');
           $("#alltask_status").dataTable({
              //  "iDisplayLength": 10,
              // "scrollX": true,
           });
      }
    });
   
   }
   
   });
   
   /** end leads status **/
   $(document).on('click','.leads_add_popup',function(){
         $('#new_lead_source').val('');
         $('#new_lead_status').val('');
   });
</script>