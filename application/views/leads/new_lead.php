<?php $this->load->view('includes/header');
   $succ = $this->session->flashdata('success');
   $error = $this->session->flashdata('error');
   /*echo '<pre>';
   print_r($_SESSION);*/
     $stafff = $this->db->query("select group_concat(`crm_refered_by` separator ',') as refer_ids from client where crm_refered_by !='' ")->row_array();
     //print_r($staffs);
     $dept =  explode(',', $stafff['refer_ids']);
   
     $staff_form = $this->db->query("select * from user where id in ( '" . implode( "','", $dept ) . "' )")->result_array();
     //echo $this->db->last_query();
     //print_r($staffs); 
   
   ?>
<div class="leads-section floating_set">
   <div class="proposal-sent-sample floating_set">
      <div class="addnewclient_pages1 floating_set">

      </div>
      <div class="management_section floating_set ">
         <!-- tab start -->
         <div class="tab-content">
            <div id="dashboard" class="tab-pane fade in active">
               <div class="switch-list01 floating_set">
                  <?php if($succ){?>
                  <div class="alert alert-success">
                     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo $succ; ?>
                  </div>
                  <?php }if($error){?>
                  <div class="alert alert-danger">
                     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo $error; ?>
                  </div>
                  <?php } ?>
                  <div class="deadline-crm1 floating_set">
                     <div class="pull-left header-title">
                        <h4>New Lead</h4>
                     </div>
                  </div>
               </div>

               <div class="lead-summary2 floating_set new-leadss">
                  <form id="leads_form" name="leads_form" action="<?php echo base_url()?>leads/addLeads" method="post">
                   <input type="hidden" name="kanpan" id="kanpan" value="0">
                   <div class="lead-popupsection1 floating_set">
                      <div class="col-xs-12 col-sm-4  lead-form1">
                         <label>Lead status</label>
                         <select name="lead_status" id="lead_status">
                            <option value="">Nothing selected</option>
                            <?php foreach($leads_status as $ls_value) { ?>
                            <option value='<?php echo $ls_value['id'];?>'><?php echo $ls_value['status_name'];?></option>
                            <?php } ?>
                         </select>
                      </div>
                      <div class="col-xs-12 col-sm-4  lead-form1">
                         <label>Source</label>
                         <select name="source" id="source">
                            <option value=''>Nothing selected</option>
                            <?php foreach($source as $lsource_value) { ?>
                            <option value='<?php echo $lsource_value['id'];?>'><?php echo $lsource_value['source_name'];?></option>
                            <?php } ?>
                            <!-- <option value='1'>Google</option>
                               <option value-'2'>Facebook</option> -->
                         </select>
                      </div>
                      <div class="col-xs-12 col-sm-4  lead-form1">
                         <label>Assigned</label>
                         <select name="assigned" id="assigned">
                            <option value="">Nothing selected</option>
                            <?php
                               if(isset($staff_form) && $staff_form!='') 
                               {
                                foreach($staff_form as $value) 
                                {
                               ?>
                            <option value="<?php echo $value['id']; ?>" ><?php echo $value["crm_name"]; ?></option>
                            <?php
                               }
                               }
                               ?>
                         </select>
                      </div>
                   </div>
                   <div class="tags-allow1 floating_set">
                      <h4><i class="fa fa-tags fa-6" aria-hidden="true"></i> Tags</h4>
                      <input type="text" value="" name="tags" id="tags" class="tags" />
                   </div>
                   <div class="lead-popupsection2 floating_set">
                      <div class="formgroup col-xs-12 col-sm-6">
                         <label class="col-sm-4 col-form-label">Name</label>
                         <div class="col-sm-8 edit-field-popup1">
                          <input type="text" name="name" id="name">
                         </div>
                      </div>
                      <div class="formgroup col-xs-12 col-sm-6">
                         <label class="col-sm-4 col-form-label">Address</label>
                         <div class="col-sm-8 edit-field-popup1">
                         <textarea rows="1" name="address" id="address"></textarea>
                         </div>
                      </div>
                      <div class="formgroup col-xs-12 col-sm-6">
                         <label class="col-sm-4 col-form-label">Position</label>
                         <div class="col-sm-8 edit-field-popup1">
                         <input type="text" name="position" id="position">
                         </div>
                      </div>
                      <div class="formgroup col-xs-12 col-sm-6">
                         <label class="col-sm-4 col-form-label">City</label>
                         <div class="col-sm-8 edit-field-popup1">
                         <input type="text" name="city" id="city">
                         </div>
                      </div>
                      <div class="formgroup col-xs-12 col-sm-6">
                         <label class="col-sm-4 col-form-label">Email Address</label>
                         <div class="col-sm-8 edit-field-popup1">
                         <input type="text" name="email_address" id="email_address">
                         </div>
                      </div>
                      <div class="formgroup col-xs-12 col-sm-6">
                         <label class="col-sm-4 col-form-label">State</label>
                         <div class="col-sm-8 edit-field-popup1">
                         <input type="text" name="state" id="state">
                         </div>
                      </div>
                      <div class="formgroup col-xs-12 col-sm-6">
                         <label class="col-sm-4 col-form-label">Website</label>
                         <div class="col-sm-8 edit-field-popup1">
                         <input type="text" name="website" id="website">
                         </div>
                      </div>
                      <div class="formgroup col-xs-12 col-sm-6">
                         <label class="col-sm-4 col-form-label">Country</label>
                         <div class="col-sm-8 edit-field-popup1">
                         <select name="country" id="country">
                            <option value=''>Nothing Selected</option>
                            <?php foreach ($countries as $key => $value) { ?>
                            <option value="<?php echo $value['id']?>"><?php echo $value['name'];?></option>
                            <?php }?>
                         </select>
                         </div>
                      </div>
                      <div class="formgroup col-xs-12 col-sm-6">
                         <label class="col-sm-4 col-form-label">Phone</label>
                         <div class="col-sm-8 edit-field-popup1">
                         <input type="text" name="phone" id="phone">
                         </div>
                      </div>
                      <div class="formgroup col-xs-12 col-sm-6">
                         <label class="col-sm-4 col-form-label">Zip Code</label>
                         <div class="col-sm-8 edit-field-popup1">
                         <input type="number" name="zip_code" id="zip_code">
                         </div>
                      </div>
                      <div class="formgroup col-xs-12 col-sm-6">
                         <label class="col-sm-4 col-form-label">Company</label>
                         <div class="col-sm-8 edit-field-popup1">
                         <input type="text" name="company" id="company">
                         </div>
                      </div>
                      <div class="formgroup col-xs-12 col-sm-6">
                         <label class="col-sm-4 col-form-label">Default Language</label>
                         <div class="col-sm-8 edit-field-popup1">
                         <select name="default_language" id="default_language">
                            <option value="">System Default</option>
                            <?php foreach ($Language as $l_key => $l_value) { ?>
                            <option value="<?php echo $l_value['id'];?>"><?php echo $l_value['name'];?></option>
                            <?php }?>
                         </select>
                         </div>
                      </div>
                      <div class="formgroup col-sm-12">
                         <label class="col-sm-4 col-form-label">Description</label>
                         <div class="col-sm-8 edit-field-popup1">
                         <textarea rows="3" name="description" id="description"></textarea>
                         </div>
                         
                      </div>
                      <div class="formgroup checkbox-select1 col-sm-12 border-checkbox-section">
                     <div class="">
                        <label class="custom_checkbox1"><input type="checkbox" name="public" id="public"></label>
                        <span>Public</span>
                     </div>
                     <div class="">
                        <label class="custom_checkbox1">
                        <input type="checkbox" name="contact_today" checked id="contact_today"></label>
                        <span>Contacted Today</span>
                     </div>
                  </div>
                  <div class="dedicated-sup col-sm-12 selectDate" style="display:none;">
                     <label><i class="fa fa-pencil fa-6" aria-hidden="true"></i> Choose contact Date</label>
                     <input type="text" name="contact_date" id="contact_date" class="datepicker" placeholder="Select your date" />
                  </div>
               </div>
               </div>
               <div class="modal-footer">
                  <a href="#"  data-dismiss="modal">Close</a>
                  <input type="submit" name="save" id="save" value="Save">
               </div>
            </form>
               </div>

            </div>
            <!-- dashboard -->
         </div>
         <!-- tab-content -->
      </div>
   </div>
</div>
<!-- leads-section -->


<style type="text/css">
   .ui-datepicker
   {
   z-index: 9999999999 !important;
   }
</style>
<!-- Modal -->
<?php $this->load->view('includes/footer');?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
   $(document).ready(function() {
   
   // $('#spl-datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
   //$('.spl-datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
   
    $("body").delegate(".spl-datepicker", "focusin", function(){
          $(this).datepicker({ dateFormat: 'dd/mm/yy' }).val();
           //var date = $('#datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
   
   
      });
   
     $( ".leads_popup" ).on('shown.bs.modal', function(){
   
       $("body").css("overflow-y", "hidden");
   });
   
      $('.leads_popup').on('hidden.bs.modal', function () {
       // do something…
        $("body").css({"overflow":"auto"});
    })
    
   
   
        //   var date = $('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
   /*$( ".datepicker" ).datepicker({
          dateFormat: 'yy-mm-dd'
      });*/
   
   $('.tags').tagsinput({
        allowDuplicates: true
      });
      
      $('.tags').on('itemAdded', function(item, tag) {
          $('.items').html('');
   var tags = $('.tags').tagsinput('items');
        
      });
   });
   
   $("#contact_today").click(function(){
    if($("#contact_today").is(':checked')){
      $(".selectDate").css('display','none');
    }else{
      $(".selectDate").css('display','block');
    }
   });
   
   $(".contact_update_today").click(function(){
    var id = $(this).attr("data-id");
    if($(this).is(':checked')){
   
      $("#selectDate_update_"+id).css('display','none');
    }else{
      $("#selectDate_update_"+id).css('display','block');
    }
   });
   
    $("#all_leads").dataTable({
          "iDisplayLength": 10,
          "scrollX": true
       });
   
</script>
<script type="text/javascript">
   $(document).ready(function(){
   
      $( "#leads_form" ).validate({
        rules: {
          lead_status: "required",  
          source: "required",  
          name: "required",  
        },
        messages: {
          lead_status: "Please enter your Status",
          source: "Please enter your Source",
          name: "Please enter your Name"
        },
        
      });
   
   
      $( ".edit_leads_form" ).validate({
        rules: {
          lead_status: "required",  
          source: "required",  
          name: "required",  
        },
        messages: {
          lead_status: "Please enter your Status",
          source: "Please enter your Source",
          name: "Please enter your Name"
        },
        
      });
   
   
   $('.reminder_save').click(function(e){
   e.preventDefault();
   //alert('ggg');
   var id = $(this).attr("data-id");
   //alert(id);
   var rem_date = $(".spl-datepicker_"+id).val();
   var rem_desc = $("#reminder_description_"+id).val();
   var staff_id = $("#staff_"+id).val();
   if($("#notify_by_email_"+id).prop('checked') == true){
   var email_sent = "yes";
   }else{
   var email_sent = "no";
   }
   //alert(email_sent);
   var data = {};
   data['reminder_date'] = rem_date;
   data['reminder_desc'] = rem_desc;
   data['lead_id'] = id;
   data['rel_type'] = 'Leads';
   data['email_sent'] = email_sent;
   data['staff_id'] = staff_id;
   var i=0;
   if(rem_date=='')
   {
     i=1;
     $('#reminder_date_err_'+id).html('Please select reminder date');
   }else{
     i=0;
     $('#reminder_date_err_'+id).html('');
   }
   if(rem_desc=='')
   {
     j=1;
     $('#description_err_'+id).html('Please enter description');
   }else{
     j=0;
     $('#description_err_'+id).html('');
   }
   
   
   if(i==0 && j==0){
       $(".LoadingImage").show();
   
       $.ajax({
       url: '<?php echo base_url();?>leads/add_new_reminder/',
       type : 'POST',
       data : data,
       success: function(data) {
           $(".LoadingImage").hide();
   
         $(".spl-datepicker_"+id).val('');
         $("#reminder_description_"+id).val('');
         $("#staff_"+id).val('');        
         $('#notify_by_email_'+id).prop('checked', false); // Unchecks it
   
         $('#add-reminders_'+id).modal('hide');
   
         $('.response_res').html(data);
         /** rspt 18-04-2018 **/
         $('.reminder_popups_'+id).html('');
         $('.reminder_popups_'+id).load('leads/reminder_edit_poup/'+id);
         /** end rspt 18-04-2018 **/
   
        // ('#example2').DataTable()
           var tabletask1 =$(".example2").dataTable({
            "iDisplayLength": 10,
         responsive: true
         });
       },
       });
   }
   
   });
   
   
   $('.edit_reminder_save').click(function(e){
   e.preventDefault();
   
   //alert('ggg');
   var id = $(this).attr("data-id");
   var l_id = $(this).attr("data-value");
   //alert(id);
   var rem_date = $(".edit_spl-datepicker_"+id).val();
   var rem_desc = $("#edit_reminder_description_"+id).val();
   var staff_id = $("#edit_staff_"+id).val();
   var l_id = l_id;
   if($("#edit_notify_by_email_"+id).prop('checked') == true){
   var email_sent = "yes";
   }else{
   var email_sent = "no";
   }
   //alert(email_sent);
   var data = {};
   data['reminder_date'] = rem_date;
   data['reminder_desc'] = rem_desc;
   data['id'] = id;
   data['rel_type'] = 'Leads';
   data['email_sent'] = email_sent;
   data['staff_id'] = staff_id;
   data['lead_id'] = l_id;
   var i=0;
   
   if(rem_date=='')
   {
     i=1;
     $('#edit_reminder_date_err_'+id).html('Please select reminder date');
   }else{
     i=0;
     $('#edit_reminder_date_err_'+id).html();
   }
   if(rem_desc=='')
   {
     j=1;
     $('#edit_description_err_'+id).html('Please enter description');
   }else{
     j=0;
     $('#edit_description_err_'+id).html();
   }
   
   
   if(i==0 && j==0){
                 $(".LoadingImage").show();
   
       $.ajax({
       url: '<?php echo base_url();?>leads/edit_reminder/',
       type : 'POST',
       data : data,
       success: function(data) {
           $(".LoadingImage").hide();
   
         $(".edit_spl-datepicker_"+id).val('');
         $("#edit_reminder_description_"+id).val('');
         $("#edit_staff_"+id).val('');        
         $('#edit_notify_by_email_'+id).prop('checked', false); // Unchecks it
   
         $('#edit-reminders_'+id).modal('hide');
   
         $('.response_res').html(data);
        // ('#example2').DataTable()
           var tabletask1 =$(".example2").dataTable({
            "iDisplayLength": 10,
         responsive: true
         });
       },
       });
   }
   
   });
   
   
   });
   
   
   $(".sortby_filter").click(function(){
         $(".LoadingImage").show();
   
      var sortby_val = $(this).attr("id");
      var data={};
      data['sortby_val'] = sortby_val;
      $.ajax({
      url: '<?php echo base_url();?>leads/sort_by/',
      type : 'POST',
      data : data,
      success: function(data) {
        $(".LoadingImage").hide();
   
   $(".sortrec").html(data);   
   $("#all_leads").dataTable({
   "iDisplayLength": 10,
   });
      },
      });
   
   });
   $("#import_leads").validate({
     rules: {
        file: {required: true, accept: "csv"},
   
          lead_status: "required",  
          source: "required",  
          name: "required",  
        },
        messages: {
          file: {required: 'Required!', accept: 'Please upload a file with .csv extension'},
   
          lead_status: "Please enter your Status",
          source: "Please enter your Source",
          name: "Please enter your Name"
        },
   
   });
                            
   
</script>
<script type="text/javascript">
   $(document).ready(function(){
      $('.edit-toggle1').click(function(){
        $('.proposal-down').slideToggle(300);
      })
   
   $('#example,.example1,.example2').DataTable();
   })
   
   
   $(".statuschange").click(function(){
   var status_val = $(this).attr("id");
   var id = $(this).attr("data-id");
   var data={};
   data['status_val'] = status_val;
   data['id'] = id;
   
   $.ajax({
      url: '<?php echo base_url();?>leads/status_update/',
      type : 'POST',
      data : data,
      success: function(data) {
        $('.succ').css('display','block');
        $('.succ').html('Status has been updated successfully');
         setTimeout(function() {
    $('.succ').fadeOut('fast');
   }, 1000);
          var cnt = data.split("-");
          $("#Lost").html(cnt[0]);
          $("#Junk").html(cnt[1]);
        $(".LoadingImage").hide();
   $(".proposal-down").css('display','none');
   $('.leadstatus').html(status_val);
      },
      });
   });
   
   
    $(".status_filter").click(function(){
    $(".LoadingImage").show();
      var filter_val = $(this).attr("id");
      var data={};
   data['filter_val'] = filter_val;
   $.ajax({
      url: '<?php echo base_url();?>leads/filter_status/',
      type : 'POST',
      data : data,
      success: function(data) {
   
        $(".LoadingImage").hide();
   $(".sortrec").html(data);   
   $("#all_leads").dataTable({
   "iDisplayLength": 10,
   });
      },
      });
   });
   
    $('.add_activity').click(function(e){
      e.preventDefault();
      var i = $(this).attr("data-id");
      var values = $("#activity_log_"+i).val();
      if(values!=''){
      $(".error").hide();
      var data={};
   data['values'] = values;
   data['id'] = i;
   data['module'] = "Lead";
   $.ajax({
        url: '<?php echo base_url();?>leads/add_activity_log/',
        type : 'POST',
        data : data,
        success: function(data) {
          $(".LoadingImage").hide();
          $('.activity_log').val('');
    $(".leads_activity_log").html(data);   
        },
      });
   }else{
   $("#error"+i).html('<span>Please Enter Your Activity log.</span>');
   }
    });
   
   
   
    /* 09-03-2018 */
   $(".note_contact").click(function(){
   var i = $(this).attr("data-id");
   var radioValue = $("input[name='note_contact']:checked").val();
   /*alert(radioValue);
   alert(i);*/
            if(radioValue=="got_touch"){
               $('.note_date_'+i).css('display','block');
            }else{
              $('.note_date_'+i).css('display','none');
            }
   });
   
   $('.add_note_rec').click(function(e){
   e.preventDefault();
   $(".LoadingImage").show();
   
   var i = $(this).attr("data-id");
   var val = $("#note_rec_"+i).val();
   var contact_date = $("#note_contact_date_"+i).val();
   //alert(val);
   if(val!='')
   {
   $(".error").hide();
      var data={};
   data['values'] = val;
   data['id'] = i;
   data['contact_date'] = contact_date;
   $.ajax({
        url: '<?php echo base_url();?>leads/add_note/',
        type : 'POST',
        data : data,
        success: function(data) {
          $(".LoadingImage").hide();
          $('.note_val').val('');
          $('.note_d').val('');
          $(".note_contact").prop('checked', false);
   
          $('.note_date_'+i).css('display','none');
    $(".add_note_rec_load").html(data);   
        },
      });
   }else{
   $("#error_note"+i).html('<span>Please Enter Your Notes.</span>');
   }
   });
   
   $(document).on('click','.delete_note',function(e){
   $(".LoadingImage").show();
   
   var i = $(this).attr("data-id");
   //var lead_id = $(this).val();
   var data={};
   data['note_id'] = i;
   $.ajax({
        url: '<?php echo base_url();?>leads/delete_note/',
        type : 'POST',
        data : data,
        success: function(data) {
          $(".LoadingImage").hide();
          $('.note_val').val('');
          $('.note_d').val('');
          $(".note_contact").prop('checked', false);
   
          $('.note_date_'+i).css('display','none');
    $(".add_note_rec_load").html(data);   
        },
      });
   
   });
   
   $('.lead_attachments').on('change', function () {
      $(".LoadingImage").show();
   
        var i = $(this).attr("data-id");
   
                   var file_data = $('#lead_attachments'+i).prop('files')[0];
                   var form_data = new FormData();
                   form_data.append('file', file_data);
                   form_data.append('id', i);
                    var data={};
      data['form_data'] = form_data;
      data['id'] = i;
   
                   $.ajax({
                        url: '<?php echo base_url();?>leads/lead_attachment/', // point to server-side controller method
                     
                       dataType: 'text', // what to expect back from the server
                       cache: false,
                       contentType: false,
                       processData: false,
                       data: form_data,
                       type: 'post',
                       success: function (response) {
                          $(".LoadingImage").hide();
                          $('.lead_attachments').val('');
                           $('.l_att').html(response); // display success response from the server
                       },
                       error: function (response) {
                            $(".LoadingImage").hide();
                          $('.lead_attachments').val('');
   
                       }
                   });
               });
   
   function delete_lead_attachment(id,l_id)
   {
   if(confirm('Are you sure want to delete?')){
   $(".LoadingImage").show();
   var data={};
   data['a_id'] = id;
   data['l_id'] = l_id;
   
   $.ajax({
         url: '<?php echo base_url();?>leads/delete_lead_attachment/', // point to server-side controller method
      
        dataType: 'text', // what to expect back from the server
       
        data: data,
        type: 'post',
        success: function (response) {
            $(".LoadingImage").hide();
            $('.l_att').html(response); // display success response from the server
        },
        error: function (response) {
              $(".LoadingImage").hide();
        }
    });
   }
   }
   
   function delete_reminder(l_id,id){
           if(confirm('Are you sure want to delete?')){
           $(".LoadingImage").show();
           var srt = l_id.split(',');
           var data={};
           data['r_id'] = srt[1];
           data['l_id'] = srt[0];
               $.ajax({
                 url: '<?php echo base_url();?>leads/delete_reminder/', // point to server-side controller method
                 dataType: 'text', // what to expect back from the server
                 data: data,
                 type: 'post',
                 success: function (response) {
                 $(".LoadingImage").hide();
                 $('.response_res').html(response); // display success response from the server
                   var tabletask1 =$(".example2").dataTable({
                   "iDisplayLength": 10,
                   responsive: true
                   });
                 },
                 error: function (response) {
                 $(".LoadingImage").hide();
                 }
               });
           }
   }
   
     $(document).on('change','.reminder_export',function(e){
       var type=$(this).val();
         var leadId = $(this).attr("data-id");
    if(type=='csv')
    {
    window.location.href="<?php echo base_url().'leads/reminder_excel?leadid="+leadId+"'?>";
    }else if(type=='pdf')
    {
    window.location.href="<?php echo base_url().'leads/reminder_pdf?leadid="+leadId+"'?>";
    }else if(type=='html')
    {
    window.open(
    "<?php echo base_url().'leads/reminder_html?leadid="+leadId+"'?>",
    '_blank' // <- This is what makes it open in a new window.
    );
    // window.location.href="<?php echo base_url().'user/task_html?status="+status+"&priority="+priority+"'?>";
    }
   
   
     });
   
     $(document).on('change','.task_export',function(e){
       var type=$(this).val();
         var leadId = $(this).attr("data-id");
    if(type=='csv')
    {
    window.location.href="<?php echo base_url().'leads/l_task_excel?leadid="+leadId+"'?>";
    }else if(type=='pdf')
    {
    window.location.href="<?php echo base_url().'leads/l_task_pdf?leadid="+leadId+"'?>";
    }else if(type=='html')
    {
    window.open(
    "<?php echo base_url().'leads/l_task_html?leadid="+leadId+"'?>",
    '_blank' // <- This is what makes it open in a new window.
    );
    // window.location.href="<?php echo base_url().'user/task_html?status="+status+"&priority="+priority+"'?>";
    }
   
   
     });
   
    // $(document).on('change','.task_leads',function(e)){
     $(document).on("click", ".task_leads", function(event){
     event.preventDefault();
   
     var id = $(this).attr("data-id");
     //var formdata =  new FormData("#lead_task_"+leadId);
   
     if($("#p-check_"+id).prop('checked') == true){
     var public_tas = "Public";
     }else{
     var public_tas = "";
     }
   
     if($("#p-check1_"+id).prop('checked') == true){
     var billable_tas = "Billable";
     }else{
     var billable_tas = "";
     }
     if($("#repeat_no_"+id).val()!='')
     {
       var repeat_no = $("#repeat_no_"+id).val();
     }else{
       var repeat_no = '';
     }
   
     if($("#durations_"+id).val()!='')
     {
       var durations = $("#durations_"+id).val();
     }else{
       var durations = '';
     }
     if($("#ends_on_"+id).val()!='')
     {
       var ends_on = $("#ends_on_"+id).val();
     }else{
       var ends_on = '';
     }
   if($("#img_name_"+id).val()!='')
   {
   var file = $("#img_name_"+id).val();
   }else{
   var file = '';
   }
   if($("#task_sub_"+id).val()!='' && $("#start_date_"+id).val()){
   //console.log(file_data);
     var data = {};
     data['task_sub'] = $("#task_sub_"+id).val();
     data['hour_rate'] = $("#hour_rate_"+id).val();
     data['start_date'] = $("#start_date_"+id).val();
     data['due_date'] = $("#due_date_"+id).val();
     data['priority'] = $("#priority_"+id).val();
     data['repeat'] = $("#repeat_"+id).val();
     data['related_to'] = $("#related_to_"+id).val();
     data['lead_to'] = $("#lead_to_"+id).val();
     data['tags'] = $("#tags_"+id).val();
     data['description'] = $("#descr_"+id).val();
     data['repeat_no'] = repeat_no;
     data['durations'] = durations;
     data['ends_on'] = ends_on;
     data['billable'] = billable_tas;
     data['public'] = public_tas;
     data['lead_id'] = id;
     data['attach_file'] = file;
   
     //data['file_data'] = $('#inputfile_'+id).prop('files')[0];   
   
   
     $.ajax({
         url: '<?php echo base_url();?>leads/leads_task/',
         type: 'post',
          data: data,
         success: function (data, status)
         {
           $('#task_leadss_'+id).html(data);
           $('#addnew_task_lead_'+id).modal('hide');
           $("#task_sub_"+id).val('');
           $("#hour_rate_"+id).val('');
           $("#start_date_"+id).val('');
           $("#due_date_"+id).val('');
           $("#priority_"+id).val('');
           $("#repeat_"+id).val('');
           $("#related_to_"+id).val('');
           $("#lead_to_"+id).val('');
           $("#tags_"+id).val('');
           $("#descr_"+id).val('');
           $("#repeat_no_"+id).val('1');
           $("#durations_"+id).val('');
           $("#ends_on_"+id).val('');
           $('#inputfile_'+id).val('');
           $("#img_name_"+id).val('');
   
         var tabletask1 =$("#example1_"+id).dataTable({
            "iDisplayLength": 10,
         responsive: true
         });
          $("#task_sub_err_"+id).hide();
          $("#task_start_err_"+id).hide();
   
          /** rspt 18-04-2018 **/
         $('.task_edit_popup_'+id).html('');
         $('.task_edit_popup_'+id).load('leads/task_edit_poup/'+id);
          /** end 18-04-2018 **/
         },
         error: function (xhr, desc, err)
         {
   
   
         }
     }); 
     } else {
       //alert($("#task_sub_"+id).val());
      // alert($("#start_date_"+id).val());
       if($("#task_sub_"+id).val()==''){
         $("#task_sub_err_"+id).html('Enter Task Subject');
       }else{
         $("#task_sub_err_"+id).hide();
       }
       if($("#start_date_"+id).val()==''){
         $("#task_start_err_"+id).html('Enter Task Start Date');
       }
       else{
          $("#task_start_err_"+id).hide();
       }
     }
     });
   
   
    $('.task_images').on('change', function () {
         var id = $(this).attr("data-id");
   
                    var file_data = $('#inputfile_'+id).prop('files')[0];
                   //console.log(file_data);
                    var form_data = new FormData();
                   form_data.append('file', file_data);
                    $.ajax({
                      //  url: 'ajaxupload/upload_file', // point to server-side controller method
                           url: '<?php echo base_url();?>leads/leads_task_image/', // point to server-side controller method
                      
                        dataType: 'text', // what to expect back from the server
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (response) {
                         $('#img_name').html(response);
                        },
                        error: function (response) {
                        }
                    });
                });
     
   
   
   $(document).on('change','.repeated_too',function(){
       var id = $(this).attr("data-id");
   
   var x_cal = $(this).val();
   if(x_cal!='' && x_cal!='custom')
   {
     $('#ends_on_div_'+id).css('display','block');
     $('#rep_no_'+id).css('display','none');
     $('#rep_cnt_'+id).css('display','none');
   }else if(x_cal!='' && x_cal=='custom'){
     $('#rep_no_'+id).css('display','block');
     $('#rep_cnt_'+id).css('display','block');
     $('#ends_on_div_'+id).css('display','block');
   }else{
     $('#ends_on_div_'+id).css('display','none');
     $('#rep_no_'+id).css('display','none');
     $('#rep_cnt_'+id).css('display','none');
   }
   });
   
   function delete_lead_task(l_id)
   {
   // alert(l_id)
             if(confirm('Are you sure want to delete?')){
   
           var srt = l_id.split(',');
           var data={};
           data['t_id'] = srt[1];
           data['l_id'] = srt[0];
   
                  $.ajax({
                 url: '<?php echo base_url();?>leads/delete_task/', // point to server-side controller method
                 dataType: 'text', // what to expect back from the server
                 data: data,
                 type: 'post',
                 success: function (response) {
                 $(".LoadingImage").hide();
                 $('#task_leadss_'+l_id).html(response); // display success response from the server
                   var tabletask1 =$("#example1_"+l_id).dataTable({
                   "iDisplayLength": 10,
                   responsive: true
                   });
                 },
                 error: function (response) {
                 $(".LoadingImage").hide();
                 }
               });
                }
   
   }
</script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript" src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>
<script type="text/javascript">
   $(document).ready(function() {
   $('.tags').tagsinput({
        allowDuplicates: true
      });
      
      $('.tags').on('itemAdded', function(item, tag) {
          $('.items').html('');
    var tags = $('.tags').tagsinput('items');
        
      });
   
   
      $( function() {
      $('#date_of_creation1,#date_of_creation2').datepicker();
    } );
   
    var slider = document.getElementById("myRange");
    var output = document.getElementById("demo");
    output.innerHTML = slider.value;
   
    slider.oninput = function() {
    output.innerHTML = this.value;
    }
   
    CKEDITOR.replace('editor4');
   
   });
   
   
      $(document).on("click", ".edit_task_leads", function(event){
      event.preventDefault();
   
      var id = $(this).attr("data-id");
      alert(id);
      var l_id = $(this).attr("data-value");
      //var formdata =  new FormData("#lead_task_"+leadId);
   
      if($("#t_edit_p-check_"+id).prop('checked') == true){
      var public_tas = "Public";
      }else{
      var public_tas = "";
      }
   
      if($("#t_edit_p-check1_"+id).prop('checked') == true){
      var billable_tas = "Billable";
      }else{
      var billable_tas = "";
      }
      if($("#t_edit_repeat_no_"+id).val()!='')
      {
        var repeat_no = $("#t_edit_repeat_no_"+id).val();
      }else{
        var repeat_no = '';
      }
   
      if($("#t_edit_durations_"+id).val()!='')
      {
        var durations = $("#t_edit_durations_"+id).val();
      }else{
        var durations = '';
      }
      if($("#t_edit_ends_on_"+id).val()!='')
      {
        var ends_on = $("#t_edit_ends_on_"+id).val();
      }else{
        var ends_on = '';
      }
   if($("#t_edit_img_name_"+id).val()!='')
   {
   var file = $("#t_edit_img_name_"+id).val();
   }else{
    var file = '';
   }
   if($("#t_edit_task_sub_"+id).val()!='' && $("#t_edit_start_date_"+id).val()){
   //console.log(file_data);
      var data = {};
      data['task_sub'] = $("#t_edit_task_sub_"+id).val();
      data['hour_rate'] = $("#t_edit_hour_rate_"+id).val();
      data['start_date'] = $("#t_edit_start_date_"+id).val();
      data['due_date'] = $("#t_edit_due_date_"+id).val();
      data['priority'] = $("#t_edit_priority_"+id).val();
      data['repeat'] = $("#t_edit_repeat_"+id).val();
      data['related_to'] = $("#t_edit_related_to_"+id).val();
      data['lead_to'] = $("#t_edit_lead_to_"+id).val();
      data['tags'] = $("#t_edit_tags_"+id).val();
      data['description'] = $("#t_edit_descr_"+id).val();
      data['repeat_no'] = repeat_no;
      data['durations'] = durations;
      data['ends_on'] = ends_on;
      data['billable'] = billable_tas;
      data['public'] = public_tas;
      data['lead_id'] = l_id;
      data['attach_file'] = file;
      data['attach_file'] = file;
      data['id'] = id;
   
      //data['file_data'] = $('#inputfile_'+id).prop('files')[0];   
   
   
      $.ajax({
          url: '<?php echo base_url();?>leads/update_task/',
          type: 'post',
           data: data,
          success: function (data, status)
          {
            $('#task_leadss_'+l_id).html(data);
            $('#edit_task_lead_'+id).modal('hide');
   
   
          var tabletask1 =$("#example1_"+l_id).dataTable({
             "iDisplayLength": 10,
          responsive: true
          });
           $("#t_edit_task_sub_err_"+id).hide();
           $("#t_edit_task_start_err_"+id).hide();
          },
          error: function (xhr, desc, err)
          {
   
   
          }
      }); 
      } else {
        //alert($("#task_sub_"+id).val());
       // alert($("#start_date_"+id).val());
        if($("#t_edit_task_sub_"+id).val()==''){
          $("#t_edit_task_sub_err_"+id).html('Enter Task Subject');
        }else{
          $("#t_edit_task_sub_err_"+id).hide();
        }
        if($("#t_edit_start_date_"+id).val()==''){
          $("#t_edit_task_start_err_"+id).html('Enter Task Start Date');
        }
        else{
           $("#t_edit_task_start_err_"+id).hide();
        }
      }
      });
   
   
   $('.e_task_images').on('change', function () {
          var id = $(this).attr("data-id");
   //alert(id);
                     var file_data = $('#t_edit_inputfile_'+id).prop('files')[0];
                    //console.log(file_data);
                     var form_data = new FormData();
                    form_data.append('file', file_data);
                     $.ajax({
                       //  url: 'ajaxupload/upload_file', // point to server-side controller method
                            url: '<?php echo base_url();?>leads/leads_task_image/', // point to server-side controller method
                       
                         dataType: 'text', // what to expect back from the server
                         cache: false,
                         contentType: false,
                         processData: false,
                         data: form_data,
                         type: 'post',
                         success: function (response) {
                          $('#t_edit_img_name_'+id).val(response);
                         },
                         error: function (response) {
                         }
                     });
                 });
      
   
   
   $(document).on('change','.t_edit_repeated_too',function(){
        var id = $(this).attr("data-id");
   
    var x_cal = $(this).val();
    if(x_cal!='' && x_cal!='custom')
    {
      $('#t_edit_ends_on_div_'+id).css('display','block');
      $('#t_edit_rep_no_'+id).css('display','none');
      $('#t_edit_rep_cnt_'+id).css('display','none');
    }else if(x_cal!='' && x_cal=='custom'){
      $('#t_edit_rep_no_'+id).css('display','block');
      $('#t_edit_rep_cnt_'+id).css('display','block');
      $('#t_edit_ends_on_div_'+id).css('display','block');
    }else{
      $('#t_edit_ends_on_div_'+id).css('display','none');
      $('#t_edit_rep_no_'+id).css('display','none');
      $('#t_edit_rep_cnt_'+id).css('display','none');
    }
   });
   
   /** rspt convert to customer 18-04-2018 */
   $(document).on('click','.convert_to_customer',function(){
      var id=$(this).attr('data-id');
      var status=$(this).attr('data-status');
      // alert(id);
      // alert(status);
      var data={};
      data['id']=id;
      data['status']=status;
      $.ajax({
          url: '<?php echo base_url();?>leads/convert_to_customer/',
          type: 'post',
           data: data,
          success: function (data, status)
          {
            if(data!='wrong')
            {
              $('.convert_to_customer').css('display','none');
              //$('.lead_status_'+id).html(data);
              $('.lead_status_'+id).each(function() {
                    $(this).html(data);
                });
   
            }
            
          }
        });
   });
   /** end of 18-04-2018 **/
</script>