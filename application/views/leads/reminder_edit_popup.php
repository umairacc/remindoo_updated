<?php 

        $assign_data=array();
         
$sql_val=$this->db->query("SELECT * FROM firm_assignees where module_id = ". $reminder_id." AND module_name='LEADS'")->result_array();

     foreach ($sql_val as $key1 => $value1) {
     $assign_data[] =$value1['assignees'];
     }

 $assign_check=$this->Common_mdl->assign_check($reminder_id);

$edit_reminder = $this->db->query("select * from lead_reminder where lead_id = ".$reminder_id."")->result_array();

$res=$this->db->query("select * from leads where id=".$reminder_id."")->result_array();
    $assigned = array();
     $team = array();
      $dept = array();
   foreach (explode(',',$res[0]['assigned']) as $leads_key => $leads_value) {
      $assigned[] = $this->Common_mdl->select_record('user','id',$leads_value);
   }
      foreach (explode(',',$res[0]['team']) as $team_key => $team_value) {
      $team[] = $this->Common_mdl->select_record('team','id',$team_value);
   }
      foreach (explode(',',$res[0]['dept']) as $dept_key => $dept_value) {
      $dept[] = $this->Common_mdl->select_record('department_permission','id',$dept_value);
   }
foreach($edit_reminder as $e_k_remidner => $e_k_value){ 
        
        $reminder_date  = implode('/', array_reverse(explode('-', $e_k_value['reminder_date'])));

  ?>

<div class="modal fade modal-reminder reminder-modal-lead-4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" id="edit-reminders_<?php echo $e_k_value['id'];?>">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form action="#" id="add_leads_reminder" name="add_leads_reminder" class="add_leads_reminder" method="post" >
        <div class="modal-header">
        
          <button type="button" class="close" data-dismiss="modal">×</button>

         <!--  <button type="button" class="close close-reminder-modal" data-rel-id="4" data-rel-type="lead" aria-label="Close" value=""><span aria-hidden="true" data-dismiss="modal">×</span></button> -->
          <h4 class="modal-title" id="myModalLabel"><i class="fa fa-question-circle" data-toggle="tooltip" title="This option allows you to never forget anything about your customers." data-placement="bottom"></i> Edit lead reminder</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group" app-field-wrapper="date">
                <label for="date" class="control-label"> <small class="req text-danger">* </small>Date to be notified</label>
                <div class="input-group date">
                  <input type="text" id="" name ="edit_reminder_date"  class="form-control dob_picker edit_spl-datepicker_<?php echo $e_k_value['id'];?>" value="<?php /*echo $reminder_date;*/ echo $e_k_value['reminder_date']; ?>">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar calendar-icon"></i>
                  </div>
                  <div id="edit_reminder_date_err_<?php echo $e_k_value['id'];?>" style="color:red"></div>
                </div>
              </div>
              <div class="form-group" app-field-wrapper="staff">
                <label for="staff" class="control-label"> <small class="req text-danger">* </small>Set reminder to</label>
                <div class="btn-group rem_edit">
                 <input type="text" class="tree_select" id="tree_select1" name="assignees[]"  placeholder="Select">
<!--        <div class="edit-popup_date lead-form-st">
               
                    <select id="edit_staff_<?php echo $e_k_value['id'];?>" name="edit_staff" multiple >
                    <?php foreach ($staffs as $s_key => $s_value) {?>
                    
                    <?php } ?>
                       <?php if(count($assigned)>0 && !empty($assigned) ){
                        ?>
                         <option disabled>Staff</option>
                        <?php
                         foreach ($assigned as $ass_key => $ass_value) {
                          if(!empty($ass_value['id'])){
                      
                      ?>
                       <option value="<?php echo $ass_value['id'];?>" <?php 
                                  if(in_array($ass_value['id'], explode(',',$e_k_value['assigned_staff_id'])))
                                 { echo 'selected="selected"'; }?> ><?php echo $ass_value['crm_name'];?></option>
                      <?php 
                      }
                      }
                       }
                    
                       if(count($team)>0 && !empty($team)){
                        ?>
                         <option disabled>Team</option>
                        <?php
                          foreach ($team as $ass_key => $ass_value) {
                             if(!empty($ass_value['id'])){
                      
                      ?>
                       <option value="tm_<?php echo $ass_value['id'];?>" <?php 
                                  if(in_array($ass_value['id'], explode(',',$e_k_value['team'])))
                                 { echo 'selected="selected"'; }?> ><?php echo $ass_value['team'];?></option>
                      <?php 
                    }
                      }
                       }
                              if(count($dept)>0 && !empty($dept)){
                                ?>
                                 <option disabled>Department</option>
                                 <?php
                          foreach ($dept as $ass_key => $ass_value) {
                       if(!empty($ass_value['id'])){
                      ?>
                       <option value="de_<?php echo $ass_value['id'];?>" <?php 
                                  if(in_array($ass_value['id'], explode(',',$e_k_value['department'])))
                                 { echo 'selected="selected"'; }?> ><?php echo $ass_value['new_dept'];?></option>
                      <?php 
                    }
                      }
                       }

                       ?>
                  </select>
        </div> -->
                </div>
              </div>
              <div class="form-group" app-field-wrapper="description"><label for="description" class="control-label">Descriptions</label>
                <textarea style="display: none;" id="sedit_reminder_description_<?php echo $e_k_value['id'];?>" name="sedit_reminder_description_<?php echo $e_k_value['id'];?>" class="form-control" rows="4"><?php echo $e_k_value['description'];?></textarea>
                 <textarea id="edit_reminder_description_<?php echo $e_k_value['id'];?>" name="edit_reminder_description_<?php echo $e_k_value['id'];?>" class="form-control" rows="4"><?php echo $e_k_value['description'];?></textarea>
                <div id="description_err_<?php echo $e_k_value['id'];?>" style="color:red"></div>
              </div>
              <div class="form-group">
                <div class="checkbox checkbox-primary">
                  <input type="checkbox" name="edit_notify_by_email_<?php echo $e_k_value['id'];?>" id="edit_notify_by_email_<?php echo $e_k_value['id'];?>" value=""  <?php if($e_k_value['email_sent']=='yes') { echo 'checked="checked"'; }?> >
                  <label for="notify_by_email">Send also an email for this reminder</label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default close-reminder-modal" data-rel-id="4" data-rel-type="lead" data-dismiss="modal" value="">Close</button>
          <button type="button" class="btn btn-info edit_reminder_save" value="" id="edit_reminder_save"  data-id="<?php echo  $e_k_value['id'];?>"  data-value="<?php echo $reminder_id;?>">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>


<?php } ?> 

<script src="<?php echo base_url();?>assets/tree_select/comboTreePlugin.js"></script>
<script src="<?php echo base_url();?>assets/tree_select/icontains.js"></script>
<script type="text/javascript">
  $( document ).ready(function() {


        var tree_select = <?php echo json_encode( GetAssignees_SelectBox() ); ?>;

         $('.tree_select').comboTree({ 
            source : tree_select,
            isMultiple: true
         });

                  $('.rem_edit .comboTreeItemTitle').click(function(){ 
var id = $(this).attr('data-id');
            var id = id.split('_');

            $('.rem_edit .comboTreeItemTitle').each(function(){
            var id1 = $(this).attr('data-id');
            var id1 = id1.split('_');
            //console.log(id[1]+"=="+id1[1]);
            if(id[1]==id1[1])
            {
               $(this).toggleClass('disabled');
            }
            });
            $(this).removeClass('disabled');
         });  
                  

          var arr1 = <?php echo json_encode($assign_data) ?>;
           var assign_check =  <?php echo $assign_check ?>;

          //alert(arr1);

          //  alert(arr1);
         
                 var unique = arr1.filter(function(itm, i, arr1) {
              return i == arr1.indexOf(itm);
          });

                // alert(unique);


                 $('.rem_edit .comboTreeItemTitle').each(function(){
      $(this).find("input:checked").trigger('click');
               var id1 = $(this).attr('data-id');
                var id = id1.split('_');
             if(jQuery.inArray(id[0], unique) !== -1)
               {
                  $(this).find("input").trigger('click');
                  if(assign_check==0)
                  {
                     $(this).toggleClass('disabled');
                  } 
              }
                    
               });
       


   $(".dob_picker").datepicker({ dateFormat: 'dd-mm-yy',minDate:0  }).val();
  //$(document).on('change','.othercus',function(e){
  });
  $('.edit_reminder_saves').click(function(e){
  e.preventDefault();

  alert('alert edit popup');
  var id = $(this).attr("data-id");
  var l_id = $(this).attr("data-value");
  //alert(id);
  var rem_date = $(".edit_spl-datepicker_"+id).val();

  var rem_desc = $("#edit_reminder_description_"+id).val();
  var staff_id = $("#edit_staff_"+id).val();
  var l_id = l_id;
if($("#edit_notify_by_email_"+id).prop('checked') == true){
  var email_sent = "yes";
}else{
  var email_sent = "no";
}
  //alert(email_sent);
  var data = {};
  data['reminder_date'] = rem_date;
  data['reminder_desc'] = rem_desc;
  data['id'] = id;
  data['rel_type'] = 'Leads';
  data['email_sent'] = email_sent;
  data['staff_id'] = staff_id;
  data['lead_id'] = l_id;
  var i=0;
  if(rem_date=='')
  {
    i=1;

    $('#edit_reminder_date_err_'+id).html('Please select reminder date');
  }else{
    i=0;
    $('#edit_reminder_date_err_'+id).html('');
  }
  if(rem_desc=='')
  {
    j=1;
    $('#edit_description_err_'+id).html('Please enter description');
  }else{
    j=0;
    $('#edit_description_err_'+id).html('');
  }


  if(i==0 && j==0){
                $(".LoadingImage").show();

      $.ajax({
      url: '<?php echo base_url();?>leads/edit_reminder/',
      type : 'POST',
      data : data,
      success: function(data) {
          $(".LoadingImage").hide();

        $(".edit_spl-datepicker_"+id).val('');
        $("#edit_reminder_description_"+id).val('');
        $("#edit_staff_"+id).val('');        
        $('#edit_notify_by_email_'+id).prop('checked', false); // Unchecks it

        $('#edit-reminders_'+id).modal('hide');

        $('.response_res').html(data);
       // ('#example2').DataTable()
          var tabletask1 =$(".example2").dataTable({
           "iDisplayLength": 10,
        responsive: true
        });
      },
      });
  }

 });

</script>

<script>
   var Random = Mock.Random;
   var json1 = Mock.mock({
     "data|10-50": [{
       name: function () {
         return Random.name(true)
       },
       "id|+1": 1,
       "disabled|1-2": true,
       groupName: 'Group Name',
       "groupId|1-4": 1,
       "selected": false
     }]
   });
   
   $('.dropdown-mul-1').dropdown({
     data: json1.data,
     limitCount: 40,
     multipleMode: 'label',
     choice: function () {
       // console.log(arguments,this);
     }
   });
   
   var json2 = Mock.mock({
     "data|10000-10000": [{
       name: function () {
         return Random.name(true)
       },
       "id|+1": 1,
       "disabled": false,
       groupName: 'Group Name',
       "groupId|1-4": 1,
       "selected": false
     }]
   });
   

      $('.edit-popup_date').dropdown({
     limitCount: 50,
     input: '<input type="text" maxLength="20" placeholder="Search">'
   });
   

</script>