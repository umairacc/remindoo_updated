<?php $this->load->view('includes/header'); ?>

<div class="pcoded-content card-removes clientredesign client-infor-design1">
	<section class="client-details-view hidden-user01 floating_set card-removes">
		<div class="main-body floating_set">
		<div class="activity_blog">
			<div class="pull-left">
				<h2>All activity</h2>
			</div>
			
			<div class="pull-right">
				<a href="#" data-toggle="modal" data-target="#note_popup">Create a new note</a>
			</div>	
		</div>



		<!-- Modal start -->
<div id="note_popup" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"></button>
        <h4 class="modal-title">Create a new note</h4>
      </div>
      <div class="modal-body">
         	<div class="editor_note">
         		<textarea name="feed_msg" id="notes_msg" placeholder="Type your text here..."></textarea>
         	</div>

         	<div class="popup_editors">
         		<div class="deadline-crm1 floating_set">
					<ol class="nav nav-tabs1 all_user1 md-tabs pull-left u-dashboard">
					  <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#email">Email</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#call">Call</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#sms">sms</a>
                      </li>
                      <li class="nav-item form_builder">
                        <a class="nav-link" data-toggle="tab" href="#meeting">meeting</a>
                      </li>
                      <li class="nav-item form_builder">
                        <a class="nav-link" data-toggle="tab" href="#log activity">log activity</a>
                      </li>
                      <li class="nav-item form_builder">
                        <a class="nav-link" data-toggle="tab" href="#task_create">create task</a>
                      </li>
                      <li class="nav-item form_builder">
                        <a class="nav-link" data-toggle="tab" href="#schedule">schedule</a>
                      </li>
                    </ol>
				</div>

				<div class="tab-content">
					<div id="email" class="tab-pane fade active">
						<div class="save_popupnotes">
							<button type="submit">Save</button>
						</div> 
					</div>

					<div id="call" class="tab-pane fade">
						<div class="note-picker">
							<input type="text" name="date_picker" class="datepicker ">
						</div>
						<div class="save_popupnotes">
							<button type="submit">Save</button>
						</div> 
					</div>

				</div>
         	</div>



      </div>
      <div class="modal-footer">
       
      </div>
    </div>

  </div>
</div> <!-- modal close -->

	</div>
	</section>
</div>

<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
 

    <script>
</script>