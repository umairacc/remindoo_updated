<script type="text/javascript">

$(document).ready(function() {
   $("#clientwise_filter").select2();
   var example1;
    // $('#example').DataTable( {
    //     dom: 'Bfrtip',
    //      buttons: [{
    //   extend: 'pdf',
    //   title: 'Customized PDF Title',
    //   filename: 'customized_pdf_file_name'
    // }, {
    //   extend: 'excel',
    //    extension: '.xls',
    //   title: 'Customized EXCEL Title',
    //   filename: 'customized_excel_file_name'
    // }, {
    //   extend: 'csv',
    //   filename: 'customized_csv_file_name'
    // },{
    //   extend: 'print',
    //   title: 'Customized EXCEL Title',
    // }]
    //     // buttons: [
    //     //     'copy', 'csv', 'excel', 'pdf', 'print'
    //     // ]
    // } );
} );


// $("#exported_result").change(function(){
// var value=$(this).val();
// if(value=='pdf'){
// $(".buttons-pdf").trigger('click');
// }else if(value="excel"){
// $(".buttons-excel").trigger('click');
// }else if(value="csv"){
// $(".buttons-csv").trigger('click');
// }
// });

// $("#print_report").click(function(){
// $(".buttons-print").trigger('click');
// });

  $(document).ready(function(){ 
       $( function() {
           $( ".date-picker" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();      
       });
   $( function() {
            $( ".date_picker2" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
       });
   $( function() {
          $( ".date_picker1" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
       }); 
     });
</script>
<script type="text/javascript">
   $( document ).ready(function() {
      $(".prodiv12").mintScrollbar({
         onChange: null,
         axis: "auto",
         wheelSpeed: 120,
         disableOnMobile: true
      });
      $('.tags').tagsinput({
          allowDuplicates: true
      });
   });

   $(document).ready(function() {
      $('.box-item').draggable({
       cursor: 'move',
       helper: "clone"
      });

      $("#container1").droppable({
       drop: function(event, ui) {
         var itemid = $(event.originalEvent.toElement).attr("itemid");
         $('.box-item').each(function() {
           if ($(this).attr("itemid") === itemid) {
             $(this).appendTo("#container1");
           }
         });
       }
      });

      $(".remove").click(function(){  
          $(this).parent('.box-item').addClass('replace');
            $('.replace').each(function() {    
                $(this).appendTo("#container1");      
            });
      });

      $("#clear_container").click(function(){
         $("#container2").html('');
      });

      $("#container2").droppable({
       drop: function(event, ui) {
         var itemid = $(event.originalEvent.toElement).attr("itemid");
         $('.box-item').each(function() {
           if ($(this).attr("itemid") === itemid) {
            $(this).addClass('insearch_section');
             $(this).appendTo("#container2");      
           }
         });
       }
      });


      function controll(){

      $('.box-item').each(function() {
       // if ($(this).attr("itemid") === itemid) {
         $(this).addClass('insearch_section');
          $(this).appendTo("#container2");      
       // }
      });    
   
}


//        $(document).ready(function () { 

//  controll();

// });

  $("#run_details").click(function()
  { 
      var client=$("#clientwise_filter").val();
      //var date=$("#datewise_filter").val();
      //var team=$("#teamwise_filter").val();
      var filter=$(".filter").attr('id'); 
      var from_date=$("#From_date").val();
      var to_date=$("#To_date").val();     
      var formdata;
      var url;
      var filename;
      var title;

      $('.fill').each(function(){
         if ($(this).hasClass("selected")) {
            var attr_id = $(this).attr('id');
            let today = new Date();
            if(attr_id == 'all'){
               from_date='';
               to_date='';
            }
            else if(attr_id == 'week'){
               to_date = today.toISOString().split('T')[0];
               from_date = new Date(today.setDate(today.getDate() - 7));
               from_date = from_date.toISOString().split('T')[0];
            }
            else if(attr_id == 'month'){
               to_date = today.toISOString().split('T')[0];
               from_date = new Date(today.setDate(today.getDate() - 30));
               from_date = from_date.toISOString().split('T')[0];
            }
            else if(attr_id == 'three_week'){
               to_date = today.toISOString().split('T')[0];
               from_date = new Date(today.setDate(today.getDate() - 21));
               from_date = from_date.toISOString().split('T')[0];
            }
            else if(attr_id == 'Three_month'){
               to_date = today.toISOString().split('T')[0];
               from_date = new Date(today.setDate(today.getDate() - 90));
               from_date = from_date.toISOString().split('T')[0];
            }
            $("#From_date").val('');
            $("#To_date").val('');     
         }
      });


      if(filter!="" && (from_date!="" || to_date!=""))
      {
         $('#'+filter).removeClass('filter');
         filter = "";     
      }

      var segment = '<?php echo $this->uri->segment(2); ?>';

      if(segment == 'Client_customisation')
      {
          var company_type = [];
             $("#container2 .company_type").each(function() {      
                 company_type.push($(this).attr("itemid"));
             });
          var status=[];
             $("#container2 .status").each(function() {
                status.push($(this).attr("itemid")); 
             });

          formdata = {'company_type':company_type,'status':status,'client':client,'from_date':from_date,'to_date':to_date,'filter':filter};
          url = '<?php echo base_url(); ?>Customisation/client_custom';
          filename = "Client_List";
          title = "Client List";
      }
      else if(segment == 'Task_customisation')
      {
          var priority=[];
          $("#container2 .priority_section").each(function() {
             priority.push($(this).attr("itemid"));
          });
          var status=[];
          $("#container2 .status_section").each(function() {
             status.push($(this).attr("itemid"));
          });

          formdata = {'priority':priority,'status':status,'client':client,'from_date':from_date,'to_date':to_date,'filter':filter};
          url = '<?php echo base_url(); ?>Taskcustomisation/Task_custom';
          filename = "Task_List";
          title = "Iask List";

      }
      else if(segment == 'Proposal_customisation')
      {
          var proposal_status =[];
          $("#container2 .proposal_option").each(function() {
             proposal_status.push($(this).attr("itemid"));
          });

          formdata = {'status':proposal_status,'client':client,'from_date':from_date,'to_date':to_date,'filter':filter};
          url = '<?php echo base_url(); ?>Proposalcustomisation/custom';
          filename = "Proposal_List";
          title = "Proposal List";
      }
      else if(segment == 'Leads_customisation')
      {
          var leads_status=[];
          $("#container2 .leads-status").each(function() {
             leads_status.push($(this).attr("itemid"));
          });

          formdata = {'status':leads_status,'client':client,'from_date':from_date,'to_date':to_date,'filter':filter};
          url = '<?php echo base_url(); ?>Leadscustomisation/custom';
          filename = "Leads_List";
          title = "Leads List";
      }
      else if(segment == 'Service_customisation')
      {
          var service=[];
          $("#container2 .service_option").each(function() {
             service.push($(this).attr("itemid"));
          });

          formdata = {'service':service,'client':client,'from_date':from_date,'to_date':to_date,'filter':filter};
          url = '<?php echo base_url(); ?>Servicecustomisation/custom';
          filename = "Service_List";
          title = "Service List";
      }
      else if(segment == 'Deadline_customisation')
      {
          var deadline=[];
            $("#container2 .deadline_option").each(function() {
               deadline.push($(this).attr("itemid"));
            });

          formdata = {'deadline':deadline,'client':client,'from_date':from_date,'to_date':to_date,'filter':filter};
          url = '<?php echo base_url(); ?>Deadlinecustomisation/custom';
          filename = "Deadline_List";
          title = "Deadline List";
      } 
      else if(segment == 'Invoice_customisation')
      {
          var status=[];
            $("#container2 .invoice-option").each(function() {
               status.push($(this).attr("itemid"));
            });

          formdata = {'status':status,'client':client,'from_date':from_date,'to_date':to_date,'filter':filter};
          url = '<?php echo base_url(); ?>Invoicecustomisation/custom';
          filename = "Invoice_List";
          title = "Invoice List";
      }
      else if(segment == 'timesheet_custom')
      {
          var assignee=[];
            $("#container2 .assignee-option").each(function() {
               assignee.push($(this).attr("itemid"));
            }); 

          formdata = {'assignee':assignee,'client':client,'from_date':from_date,'to_date':to_date,'filter':filter};
          url = '<?php echo base_url(); ?>Timesheet/custom';
          filename = "Timesheet";
          title = "Timesheet";
      }
      else if(segment == 'performance_customisation')
      {
          var service=$("#servicewise_filter").val();
          var option=$(".filtering").attr('id');
          var assignee=[];
            $("#container2 .assignee-option").each(function() {
               assignee.push($(this).attr("itemid"));
            }); 

          formdata = {'assignee':assignee,'client':client,'from_date':from_date,'to_date':to_date,'filter':filter,'service':service,'option':option};
          url = '<?php echo base_url(); ?>Performance/custom';
          filename = "Performance";
          title = "Performance";
      }
     

      if(filter=="" && from_date!="" && to_date =="")
      {
         $('.custom_msg').find('.position-alert1').html("Choose End Date.");
         $('#targetcus_msg').trigger('click');
      }  
      else if(filter=="" && from_date=="" && to_date !="")
      {
         $('.custom_msg').find('.position-alert1').html("Choose Start Date.");
         $('#targetcus_msg').trigger('click');
      }
      else
      { 
         $.ajax({
            url: url,
            type : 'POST',
            data : formdata,                    
            beforeSend: function() {
            $(".LoadingImage").show();
            },
            success: function(data) {
               $(".LoadingImage").hide();   
               $("#datatable_results").html('');
               $("#datatable_results").append(data);
               $("#datatable_results").show();

               $(document).ready(function() {
                var check=0;
                var check1=0;
                var numCols = $('#example thead th').length;
                    example1 = $('#example').DataTable({

                      dom: 'Bfrtip',
                      buttons: [{
                         extend: 'pdf',
                            title: title,
                            filename: filename
                         }, {
                         extend: 'excel',
                            title: title,
                            filename: filename
                         }, {
                         extend: 'csv',
                            filename: filename
                         },{
                         extend: 'print',
                            title: title
                         }], 
            
                initComplete: function () { 
                  var q=0;
                     $('#example tfoot th').find('.filter_check').each( function(){
                     // alert('ok');
                       $(this).attr('id',"all_"+q);
                       q++;
                     });
                  for(i=0;i<numCols;i++){                 
                        var select = $("#all_"+i); 
                      
                      this.api().columns([i]).every( function () {
                        var column = this;
                        column.data().unique().sort().each( function ( d, j ) {    
                          console.log(d);         
                          select.append( '<option value="'+d+'">'+d+'</option>' )
                        });
                      });                   
           
                     $("#all_"+i).formSelect();  
                  }
                             }
                         });
                           for(j=0;j<numCols;j++){  
                               $('#all_'+j).on('change', function(){ 
                                 var result=$(this).attr('id').split('_');      
                                  var c=result[1];
                                   var search = [];              
                                   $.each($('#all_'+c+ ' option:selected'), function(){                
                                   search.push($(this).val());
                                   });      
                                   search = search.join('|');    
                                  

                                   example1.column(c).search(search, true, false).draw();  
                               });
                            }


                                    });

               $("#exported_result").change(function(){ 
               var value=$(this).val();        
                  if(value=='pdf'){
                      $(".buttons-pdf").trigger('click');
                  }else if(value=="excel"){
                      $(".buttons-excel").trigger('click');
                  }else if(value=="csv"){             
                     $(".buttons-csv").trigger('click');
                  }
               });

               $("#print_report").click(function(){ 
                   $(".buttons-print").trigger('click');
               });   
                                              
            }
         }); 
     }    
       });
   });

$('#search-criteria').keyup(function(){
    $('.box_section').hide();
    var txt = $('#search-criteria').val();
    $('.box_section').each(function(){
       if($(this).text().toUpperCase().indexOf(txt.toUpperCase()) != -1){
           $(this).show();
       }
    });
});

$('.fill').click(function(e){
   $('.fill').each(function(){
       $(this).css('background-color','');
       if ($(this).hasClass("selected")) {
         $(this).removeClass("selected");
       }
    });
    $(this).css('background-color','green');
    $(this).addClass("selected");
});
</script>
<script type="text/javascript">
   $(document).ready(function () {     
     $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var ev = document.createEvent('Event');
    ev.initEvent('resize', true, true);
    window.dispatchEvent(ev);
  });     

   });


   $(".fill").click(function(){
      $('.fill').removeClass('filter');
      $(this).addClass('filter');
   });

   $(".fill_details").click(function(){
      $('.fill_details').removeClass('filtering');
      $(this).addClass('filtering');
   });
</script>