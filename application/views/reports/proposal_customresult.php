	
	<div class="sample_section">
	<button type="button" class="print_button btn btn-primary" id="print_report">Print</button>
	<div class="export_option">
	<select id="exported_result">
	<option value="">Select</option>
	<option value="pdf">Pdf</option>
	<option value="excel">Excel</option>
	<option value="csv">CSV</option>
	</select>
	</div>
	</div>
	<table id="example" class="display nowrap" style="width:100%">
	<thead>
	<tr>
	<th>Proposal No</th>
	<th>Proposal Name</th>
	<th>Company Name</th>
	<th>Status</th>
	<th>Created date</th>                                      
	</tr>
	</thead>
	<tfoot>
	<tr>
	<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
	<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
	<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
	<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
	<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>                                      
	</tr>
	</tfoot>
	<tbody>
<?php
if(empty($records)){}else{
foreach($records as $record){ ?>
	<tr>
	<td><?php echo $record['proposal_no']; ?></td>
	<td><?php echo $record['proposal_name']; ?></td>
	<td><?php if($record['company_name']=='0'){
		echo "-";
	}else{
		echo $record['company_name'];	}

	 ?></td>
	<td><?php echo $record['status']; ?></td>
	<td><?php echo date('Y-m-d',strtotime($record['created_at'])); ?></td>	
	</tr>
<?php }} ?>

	</tbody>

	</table>
