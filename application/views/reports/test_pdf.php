<table class="table client_table1 text-center display nowrap printableArea" id="alluser" cellspacing="0" cellpadding="10" width="100%" style="border-collapse:collapse;border:1px solid #ddd;">
   <thead>
      <tr class="text-uppercase">
         <th style="border:1px solid #ddd;">
            S.no
         </th>
         <?php if(isset($column_setting[0]['profile_image'])&&($column_setting[0]['profile_image']==1)){?>
         <th style="border:1px solid #ddd;">Profile</th>
         <?php } ?>      
         <th style="border:1px solid #ddd;">Name</th>
         <th style="border:1px solid #ddd;">Updates</th>
         <th style="border:1px solid #ddd;">Active</th>
         <th style="border:1px solid #ddd;">CompanyStatus</th>
         <th style="border:1px solid #ddd;">User Type</th>
         <th style="border:1px solid #ddd;">Status</th>
      </tr>
   </thead>
   <tbody>
      <?php 
         $s = 1;
         foreach ($client as $getallUserkey => $getallClientvalue) {
         $update=$this->Common_mdl->select_record('update_client','user_id',$getallClientvalue['user_id']);
         $company_status=$this->Common_mdl->select_record('client','user_id',$getallClientvalue['user_id']);
         $getallUservalue=$this->Common_mdl->select_record('user','id',$getallClientvalue['user_id']);
         if($getallUservalue['role']=='6'){
         //$role = 'Staff';
         $edit=base_url().'user/view_staff/'.$getallUservalue['id'];
         $house='Manual';
         }elseif($getallUservalue['role']=='4' && $company_status['status']==0 ){
         //$role = 'Client';
         $edit=base_url().'Client/client_info/'.$getallUservalue['id'];
         $house='Manual';
         }elseif($getallUservalue['role']=='4' && $company_status['status']==1){
         //$role = 'Client';
         $edit=base_url().'Client/client_info/'.$getallUservalue['id'];
         $house='Company House';
         }elseif($getallUservalue['role']=='4' && $company_status['status']==2){
         //$role = 'Client';
         $edit=base_url().'Client/client_info/'.$getallUservalue['id'];
         $house='Import';
         }else{
         $house ='';
         $role = '';
         $edit='#';
         }
         if($getallUservalue['status']=='1'){
         $status_id = 'checkboxid1';  
         $chked = 'selected';
         $status_val = '1';
         $active='Active';
         }elseif($getallUservalue['status']=='2' || $getallUservalue['status']=='0' ){
         $status_id = 'checkboxid2';  
         $chked = 'selected';
         $status_val = '2';
         $active='Inactive';
         } elseif($getallUservalue['status']=='3'){
         $status_id = 'checkboxid3';  
         $chked = 'selected';
         $status_val = '3';
         $active='Frozen';
         }
         $role=$this->Common_mdl->select_record('Role','id',$getallUservalue['role']);
         ?>
      <tr id="<?php echo $getallUservalue["id"]; ?>">
         <td style="border:1px solid #ddd;"><?php echo $s;?></td>
         <?php if(isset($column_setting[0]['profile_image'])&&($column_setting[0]['profile_image']==1)){?>
         <td class="user_imgs" style="border:1px solid #ddd;"><img src="<?php echo base_url().'/uploads/'.$getallUservalue['crm_profile_pic'];?>" alt="img"></td>
         <?php } ?>
         <td style="border:1px solid #ddd;"><?php echo ucfirst($getallUservalue['crm_name']);?></td>
         <td style="border:1px solid #ddd;">                                  
            <?php echo ($update['counts']!='')? ($update['counts']):'0';?>  new
         </td>
         <td style="border:1px solid #ddd;">
            <?php if($getallUservalue['status']=='1'){ echo "Active"; } elseif($getallUservalue['status']=='0'){ echo "Inactive"; }elseif($getallUservalue['status']=='3'){ echo "Frozen"; }else{ echo '-'; } ?>
         </td>
         <td style="border:1px solid #ddd;" id="innac"><?php echo ucfirst($company_status['crm_company_status']);?></td>
         <td style="border:1px solid #ddd;"><?php echo $role['role'];?></td>
         <td style="border:1px solid #ddd;"><?php echo $house;?></td>
      </tr>
      <?php $s++; } ?>
   </tbody>
</table>
<div id ="chart_img">
   <?php
      if(isset($_SESSION['image'])){ ?>
   <img src="<?php echo $_SESSION['image']; ?>" > 
   <?php } ?>
</div>