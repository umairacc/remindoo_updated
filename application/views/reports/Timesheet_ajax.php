<div class="sample_section">
<button type="button" class="print_button btn btn-primary" id="print_report">Print</button>
<div class="export_option">
<select id="exported_result">
<option value="">Select</option>
<option value="pdf">Pdf</option>
<option value="excel">Excel</option>
<option value="csv">CSV</option>
</select>
</div>
</div>
 <table class="table client_table1 text-center display nowrap printableArea" id="example" cellspacing="0" width="100%">
      <thead>
         <tr class="text-uppercase">                                
          <!--  <th>First Name</th>
           <th>Last Name</th> -->
           <th>S.No</th>
           <th>Task Name</th>
           <th>Start Date</th>
           <th>End Date</th>  
           <th>Estimated Hours</th>
           <th>Hours Taken</th>
           <th>Regular Hours</th>
           <th>Overtime Hours</th>
           <th>Total Hours</th>                                
         </tr>
      </thead>
       <tbody>
      
       <?php 

       $i = 1;

       foreach ($task_list as $key => $value) { ?>
       <tr id="<?php echo $value['id']; ?>">
       <td><?php echo $i; ?></td>
        <td><?php echo ucfirst($value['subject']);?></td>
       <td>                                      
       <?php echo date('Y-m-d',strtotime($value['start_date']));?>
       </td>
       <td>
       <?php echo date('Y-m-d',strtotime($value['end_date']));?>
       </td>  
       <td>
       <?php echo $value['hours_estimated'];?>
       </td> 
       <td>
       <?php echo $value['hours_total'];?>
       </td>    
       <td><?php $hours = $value['hours_worked']; echo $hours;   ?></td>
       <td id="innac" class="staff_td">

   <?php



       $overtime_hours=0; 
       echo $overtime_hours;
       ?>
       </td>
       <td class="roles_td">

       <?php 

       $total_hours=$overtime_hours + $hours;
 echo $total_hours; ?>
       </td>
    
       <!--   <td>Manual</td> -->
       </tr>
       <?php $i++; } ?>
      </tbody>
     </table>