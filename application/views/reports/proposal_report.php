<table class="table client_table1 text-center display nowrap" id="alltask" cellspacing="0" width="100%">
<thead>
<tr class="text-uppercase">
<th>S.No</th>
<th>Proposal No</th>
<th>Proposal Name</th>
<th>Proposal Amount</th>
<th>Status</th>
<th>Sent on</th>
</tr>
</thead>
<tbody>
<?php 
$t=1;
foreach ($proposal_list as $key => $value) { ?>
<tr id="<?php echo $value['id']; ?>">
<td>
<?php echo $t; ?>
</td>
<td> <?php echo ucfirst($value['proposal_no']);?></td>
<td><?php echo ucfirst($value['proposal_name']);?></td>
<td><?php echo $value['grand_total'];?></td>
<td><?php echo $value['status']; ?></td>
<td>
<?php //echo $value['created_at'];
$timestamp = strtotime($value['created_at']);
$newDate = date('d F Y H:i', $timestamp); 
echo $newDate; //outputs 02-March-2011
?>
</td>
</tr>
<?php $t++; } ?>
</tbody>
</table>
<div id ="chart_img">
 <?php
 if(isset($_SESSION['proposal_image_sess'])){ ?>
 <img src="<?php echo $_SESSION['proposal_image_sess']; ?>" > 
 <?php } ?>
</div> 
