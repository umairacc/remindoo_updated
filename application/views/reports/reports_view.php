<?php $this->load->view('includes/header');?>
<?php

foreach($expired_invoice as $key => $value) 
{ 
  if($value['invoice_duedate'] == 'Expired')
  {
     unset($expired_invoice[$key]);
  }
}  

?>
<style>
   /* bootstrap hack: fix content width inside hidden tabs */
 
.tab-content > .active,
.pill-content > .active {
height: auto;       /* let the content decide it  */
} /* bootstrap hack end */
div#tabs_priority {
    padding-top: 15px;
}
#chartdiv {
  width: 100%;
  height: 500px;
  font-size: 11px;
}

.amcharts-pie-slice {
  transform: scale(1);
  transform-origin: 50% 50%;
  transition-duration: 0.3s;
  transition: all .3s ease-out;
  -webkit-transition: all .3s ease-out;
  -moz-transition: all .3s ease-out;
  -o-transition: all .3s ease-out;
  cursor: pointer;
  box-shadow: 0 0 30px 0 #000;
}

.amcharts-pie-slice:hover {
  transform: scale(1.1);
  filter: url(#shadow);
}
div#legenddiv
{
      height: 50px !important;
    width: 100% !important;
    position: absolute;
    top: 70% !important;
    opacity: 1;
    visibility: visible;
    white-space: nowrap;
}

div .artificial circle
{
   height: 10px;
   width: 10px; 
   border-radius: 50%;
   display: inline-block;  
   margin-right: 6px;
} 
</style>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/mintScrollbar.css">
<link href="<?php echo base_url();?>assets/css/timeline-style.css" type="text/css" rel="stylesheet" />
<div class="pcoded-content card-removes">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card">
                        <!-- admin start-->
                        <div class="client_section user-dashboard-section1 floating_set">
                           <!-- all-clients -->
                           <div class="all_user-section floating_set">
                              <div class="deadline-crm1 floating_set f4reports">
                                 <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                                    <li class="nav-item">
                                       <a class="nav-link active" data-toggle="tab" data-target="#reports-tab" href="">Reports</a>
                                       <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" data-toggle="tab" data-target="#clients-reports" href="">clients</a>
                                       <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" data-toggle="tab" data-target="#proposals-reports" href="">proposals</a>
                                       <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" data-toggle="tab" data-target="#task-reports" href="">tasks</a>
                                       <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" data-toggle="tab" data-target="#services-reports" href="">services</a>
                                       <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" data-toggle="tab" data-target="#leads-reports" href="">leads</a>
                                       <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" data-toggle="tab" data-target="#deadlines-reports" href="">Deadlines</a>
                                       <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" data-toggle="tab" data-target="#invoices-reports" href="">invoices</a>
                                       <div class="slide"></div>
                                    </li>
                                 </ul>
                                 <div class="toolbar-table f-right report-redesign1">
                                    <div class="filter-task1">
                                       <h2>Filter:</h2>
                                       <ul>
                                          <li><img src="<?php echo base_url(); ?>assets/images/by-date1.png" alt="data"><input type="text" id="datewise_filter" name="de" placeholder="By Date" class="date-picker" value=""><span id="er_date_pick"></span>&nbsp;<button type="button" class="btn btn-primary searchclsnew" id="search" ><i class="fa fa-search"></i></button></li>
                                          <li class="fr-wid">
                                             <img src="<?php echo base_url(); ?>assets/images/by-date2.png" alt="data">
                                            <select id="clientwise_filter" name="stati">
                                                <option value="" hidden="">By Client</option>
                                                <?php foreach($client as $que){ ?>
                                                <option value="<?php echo $que['user_id']; ?>"><?php echo $que['crm_company_name']; ?></option>
                                                <?php   } ?>
                                             </select>
                                          </li>
                                       <!--    <li class="fr-wid">
                                             <img src="<?php echo base_url(); ?>assets/images/by-date2.png" alt="data">
                                             <select id="userwise_filter" name="stati">
                                                <option value="0" hidden="">By User</option>
                                                <?php if(count($staff_form)){ ?>
                                                <option disabled>Staff</option>
                                                <?php foreach($staff_form as $s_key => $s_val){ ?>
                                                <option value="<?php echo $s_val['id'];?>" ><?php echo $s_val['crm_name'];?></option>
                                                <?php }  } ?>
                                            
                                             </select>
                                          </li> -->

                                         <!--   <li class="fr-wid">
                                             <img src="<?php echo base_url(); ?>assets/images/by-date2.png" alt="data">
                                             <select id="teamwise_filter" name="stati">
                                                <option value="0" hidden="">By Team</option>
                                                <?php if(count($team)){ ?>
                                                <option disabled>Team</option>
                                                <?php foreach($team as $t_key => $te){ ?>
                                                <option value="<?php echo $te['id'];?>" ><?php echo $te['team'];?></option>
                                                <?php }  } ?>
                                            
                                             </select>
                                          </li> -->
                                       </ul>
                                    </div>
                                    <div class="year-status">
                                       <!--  <span class="lt-year">last year</span> -->
                                       <ul class="status-list">
                                          <li><a href="javascript:;" class="all fill" id="all"> All </a></li>
                                          <li><a href="javascript:;" class="week fill" id="week"> week </a></li>
                                          <li><a href="javascript:;" class="three_week fill" id="three_week">3 week</a></li>
                                          <li><a href="javascript:;" class="month fill" id="month"> month </a></li>
                                          <li><a href="javascript:;" class="Three_month fill" id="Three_month"> 3 months </a></li>
                                          <!--  <li>year</li> -->
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                              <div id="append_content" class="report-redesign2">
                                 <div class="all_user-section2 floating_set fr-reports">
                                    <div class="tab-content <?php if($_SESSION['permission']['Reports']['view']!='1'){ ?> permission_deined <?php } ?>  ">
                                       <div class="reports-tabs tab-pane fade active" id="reports-tab">
                                          <div class="card-comment">
                                             <div class="card-block-small">
                                                <div class="left-sireport col-xs-12  col-md-4">
                                                   <div class="comment-desc">
                                                      <h6>
                                                        <span> Clients  </span>
                                                         <div class="comment-btn">
                                                            <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>Report/Client">Run</a>
                                                            <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>report/Client_customisation">Customise</a>
                                                         </div>
                                                      </h6>
                                                      <span class="time-frame"><label>Time Frame:</label>
                                                      <select id="filter_client">
                                                         <option value="">Select</option>
                                                         <option value="year">This Year</option>
                                                         <option value="month">This Month</option>
                                                      </select>
                                                      </span>
                                                      <div class="card-block fileters_client new123 client_chart">
                                                        <div id="chart_Donut11" style="width: 50%;"></div>
                                                         <!-- progress bar section -->
                                                        <div class="prodiv12">
                                                  <?php
                                                  if($Private['limit_count']!='0'){ ?>
                                                  <div class="pro-color">
                                                    <span class="procount"> <?php echo $Private['limit_count']; ?></span>
                                                  <div class="fordis">
                                                  <div class="progress progress-xs">
                                                    <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Private['limit_count'].'%' ?>;" aria-valuenow="<?php echo $Private['limit_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo count($client); ?>"></div>
                                                  </div>
                                                  </div>
                                                  </div>
                                                  <?php } ?>
                                                  <?php
                                                  if($Public['limit_count']!='0'){ ?>
                                                  <div class="pro-color">
                                                    <span class="procount"> <?php echo $Public['limit_count']; ?></span>
                                                  <div class="fordis">
                                                  <div class="progress progress-xs">
                                                    <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Public['limit_count'].'%' ?>;" aria-valuenow="<?php echo $Public['limit_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo count($client); ?>"></div>
                                                  </div>
                                                  </div>
                                                  </div>
                                                  <?php } ?>


                                                  <?php
                                                  if($limited['limit_count']!='0'){ ?>
                                                  <div class="pro-color">
                                                    <span class="procount"> <?php echo $limited['limit_count']; ?></span>
                                                  <div class="fordis">
                                                  <div class="progress progress-xs">
                                                    <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $limited['limit_count'].'%' ?>;" aria-valuenow="<?php echo $limited['limit_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo count($client); ?>"></div>
                                                  </div>
                                                  </div>
                                                  </div>
                                                  <?php } ?>

                                                  <?php
                                                  if($Partnership['limit_count']!='0'){ ?>
                                                  <div class="pro-color">
                                                    <span class="procount"> <?php echo $Partnership['limit_count']; ?></span>
                                                  <div class="fordis">
                                                  <div class="progress progress-xs">
                                                    <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Partnership['limit_count'].'%' ?>;" aria-valuenow="<?php echo $Partnership['limit_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo count($client); ?>"></div>
                                                  </div>
                                                  </div>
                                                  </div>
                                                  <?php } ?>

                                                  <?php
                                                  if($self['limit_count']!='0'){ ?>
                                                  <div class="pro-color">
                                                    <span class="procount"> <?php echo $self['limit_count']; ?></span>
                                                  <div class="fordis">
                                                  <div class="progress progress-xs">
                                                    <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $self['limit_count'].'%' ?>;" aria-valuenow="<?php echo $self['limit_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo count($client); ?>"></div>
                                                  </div>
                                                  </div>
                                                  </div>
                                                  <?php } ?>
                                                  <?php
                                                  if($Trust['limit_count']!='0'){ ?>
                                                  <div class="pro-color">
                                                    <span class="procount"> <?php echo $Trust['limit_count']; ?></span>
                                                  <div class="fordis">
                                                  <div class="progress progress-xs">
                                                    <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Trust['limit_count'].'%' ?>;" aria-valuenow="<?php echo $Trust['limit_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo count($client); ?>"></div>
                                                  </div>
                                                  </div>
                                                  </div>
                                                  <?php } ?>

                                                  <?php
                                                  if($Charity['limit_count']!='0'){ ?>
                                                  <div class="pro-color">
                                                    <span class="procount"> <?php echo $Charity['limit_count']; ?></span>
                                                  <div class="fordis">
                                                  <div class="progress progress-xs">
                                                    <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Charity['limit_count'].'%' ?>;" aria-valuenow="<?php echo $Charity['limit_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo count($client); ?>"></div>
                                                  </div>
                                                  </div>
                                                  </div>
                                                  <?php } ?>

                                                  <?php
                                                  if($Other['limit_count']!='0'){ ?>
                                                  <div class="pro-color">
                                                    <span class="procount"> <?php echo $Other['limit_count']; ?></span>
                                                  <div class="fordis">
                                                  <div class="progress progress-xs">
                                                    <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Other['limit_count'].'%' ?>;" aria-valuenow="<?php echo $Other['limit_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo count($client); ?>"></div>
                                                  </div>
                                                  </div>
                                                  </div>
                                                  <?php } ?>
                                                  </div>
                                                         <!-- progress bar section -->
                                                      </div>

                                                   </div>
                                                </div>
                                                <div class="left-sireport col-xs-12 col-md-4">
                                                   <div class="comment-desc">
                                                      <h6>
                                                         <span>Task</span>
                                                         <div class="comment-btn">
                                                            <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>Report/Task">Run</a>
                                                            <a class="btn bg-c-blue btn-round btn-comment " href="<?php echo base_url(); ?>report/Task_customisation">Customise</a>
                                                         </div>
                                                      </h6>
                                                      <span class="time-frame"><label>Time Frame:</label>
                                                      <select id="filter_task">
                                                         <option value="">Select</option>
                                                         <option value="year">This Year</option>
                                                         <option value="month">This Month</option>
                                                      </select>
                                                      </span>
                                                      <div class="card-block fileters_Tasks">
                                                         <div id="pie_chart" style="width: 50%;"></div>
                                                         <!-- progress bar section -->
                                                         <div class="prodiv12">
                                                            <?php
                                                               if($notstarted_tasks['task_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"><?php echo $notstarted_tasks['task_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $notstarted_tasks['task_count'].'%'; ?>;" aria-valuenow="<?php echo $notstarted_tasks['task_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_tasks['task_count']; ?>"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>    
                                                            <?php
                                                               if($completed_tasks['task_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"><?php echo $completed_tasks['task_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $completed_tasks['task_count'].'%'; ?>;" aria-valuenow="<?php echo $completed_tasks['task_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_tasks['task_count']; ?>"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?> 
                                                            <?php
                                                               if($inprogress_tasks['task_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"><?php echo $inprogress_tasks['task_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $inprogress_tasks['task_count'].'%'; ?>;" aria-valuenow="<?php echo $inprogress_tasks['task_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_tasks['task_count']; ?>"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?> 
                                                            <?php
                                                               if($awaiting_tasks['task_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"><?php echo $awaiting_tasks['task_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $awaiting_tasks['task_count'].'%'; ?>;" aria-valuenow="<?php echo $awaiting_tasks['task_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_tasks['task_count']; ?>"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?> 
                                                            <?php
                                                               if($archive_tasks['task_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"><?php echo $archive_tasks['task_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $archive_tasks['task_count'].'%'; ?>;" aria-valuenow="<?php echo $archive_tasks['task_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_tasks['task_count']; ?>"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>                                  
                                                         </div>
                                                         <!-- progress bar section -->
                                                      </div>
                                                   </div>
                                                </div>
 <?php $total_proposal_count = count($accept_proposal)+count($indiscussion_proposal)+count($sent_proposal)+count($viewed_proposal)+count($declined_proposal)+count($archive_proposal)+count($draft_proposal); ?>                                               
                                                <div class="left-sireport col-xs-12 col-md-4">
                                                   <div class="comment-desc">
                                                      <h6>
                                                         <span>Proposal</span>
                                                         <div class="comment-btn">
                                                            <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>Report/Proposal">Run</button>
                                                            <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>report/Proposal_customisation">Customise</a>
                                                         </div>
                                                      </h6>
                                                      <span class="time-frame"><label>Time Frame:</label>
                                                      <select id="filter_proposal">
                                                         <option value="">Select</option>
                                                         <option value="year">This Year</option>
                                                         <option value="month">This Month</option>
                                                      </select>
                                                      </span>
                                                      <div class="card-block filter_proposal">
                                                         <div id="piechart1" style="width: 50%; "></div>
                                                         <!-- progress bar section -->
                                                         <div class="prodiv12">
                                                            <?php
                                                               if(count($accept_proposal)!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo count($accept_proposal); ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo count($accept_proposal).'%' ?>;" aria-valuenow="<?php echo count($accept_proposal); ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_proposal_count; ?>"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if(count($indiscussion_proposal)!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo count($indiscussion_proposal); ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo count($indiscussion_proposal).'%' ?>;" aria-valuenow="<?php echo count($indiscussion_proposal); ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_proposal_count; ?>"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if(count($sent_proposal)!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo count($sent_proposal); ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo count($sent_proposal).'%' ?>;" aria-valuenow="<?php echo count($sent_proposal); ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_proposal_count; ?>"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if(count($viewed_proposal)!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo count($viewed_proposal); ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo count($viewed_proposal).'%' ?>;" aria-valuenow="<?php echo count($viewed_proposal); ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_proposal_count; ?>"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if(count($declined_proposal)!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo count($declined_proposal); ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo count($declined_proposal).'%' ?>;" aria-valuenow="<?php echo count($declined_proposal); ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_proposal_count; ?>"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if(count($archive_proposal)!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo count($archive_proposal); ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo count($archive_proposal).'%' ?>;" aria-valuenow="<?php echo count($archive_proposal); ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_proposal_count; ?>"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if(count($draft_proposal)!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo count($draft_proposal); ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo count($draft_proposal).'%' ?>;" aria-valuenow="<?php echo count($draft_proposal); ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_proposal_count; ?>"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                         </div>
                                                         <!-- progress bar section -->
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="left-sireport col-xs-12  col-md-4">
                                                   <div class="comment-desc">
                                                      <h6>
                                                         <span>Leads</span>
                                                         <div class="comment-btn">
                                                            <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>Report/Leads">Run</a>
                                                            <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>report/Leads_customisation">Customise</a>
                                                         </div>
                                                      </h6>
                                                      <span class="time-frame"><label>Time Frame:</label>
                                                      <select id="filter_leads">
                                                         <option value="">Select</option>
                                                         <option value="year">This Year</option>
                                                         <option value="month">This Month</option>
                                                      </select>
                                                      </span>
                                                      <div class="card-block filter_leads">
                                                         <div id="piechart_1" style="width: 50%;"></div>
                                                         <!-- progress bar section -->
                                                         <div class="prodiv12">
                                                            <?php        
                                                               foreach($leads_status as $status){ 
                                                               $count=$this->Report_model->lead_details($status['id']);
                                                               if($count['lead_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"><?php echo $count['lead_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $count['lead_count'].'%' ?>;" aria-valuenow="<?php echo $count['lead_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $overall_leads['leads_count']; ?>"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php }} ?>
                                                         </div>
                                                         <!-- progress bar section -->
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="left-sireport col-xs-12 col-md-4">
                                                   <div class="comment-desc">
                                                      <h6>
                                                         <span>Service</span>
                                                         <div class="comment-btn">
                                                            <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>Report/Service">Run</button>
                                                            <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>report/Service_customisation">Customise</a>
                                                         </div>
                                                      </h6>
                                                      <span class="time-frame"><label>Time Frame:</label>
                                                      <select id="filter_service">
                                                         <option value="">Select</option>
                                                         <option value="year">This Year</option>
                                                         <option value="month">This Month</option>
                                                      </select>
                                                      </span>

   <?php 

   $total_service_count = "";

   foreach ($services_count as $key => $value) 
   {
      $total_service_count += $value['count']['service_count']; 
   }

   ?>  

   <div class="card-block filter_service">
      <div id="piechart_service" style="width: 50%; "></div>
      <!-- progress bar section -->
      <div class="prodiv12">
         <?php
         
         foreach ($services_count as $key => $value) 
         { 
            if($value['count']['service_count'] != '0')
            {
         ?>
         <div class="pro-color">
            <span class="procount"><?php echo $value['count']['service_count']; ?> </span>
            <div class="fordis">
               <div class="progress progress-xs">
                  <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $value['count']['service_count'].'%'; ?>;" aria-valuenow="<?php echo $value['count']['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_service_count; ?>"></div>
               </div>
            </div>
         </div>
       <?php } } ?>
        
      </div>
      <!-- progress bar section -->
   </div>                                                  
                                                     
                                                       
                                                   </div>
                                                </div>
                                                <div class="left-sireport col-xs-12  col-md-4">
                                                   <div class="comment-desc">
                                                      <h6>
                                                        <span> Deadline Manager</span>
                                                         <div class="comment-btn">
                                                            <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>Report/Dead_line">Run</a>
                                                            <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>report/Deadline_customisation">Customise</a>
                                                         </div>
                                                      </h6>
                                                      <span class="time-frame"><label>Time Frame:</label>
                                                      <select id="filter_deadline">
                                                         <option value="">Select</option>
                                                         <option value="year">This Year</option>
                                                         <option value="month">This Month</option>
                                                      </select>
                                                      </span>
<?php 

$total_deadline_count = "";

foreach ($deadlines_count as $key => $value) 
{
   $total_deadline_count += $value['count']['dead_count']; 
}

?>                                                      
  <div class="card-block filter_deadline">
     <div id="piechart_deadline" style="width: 50%; "></div>
     <!-- progress bar section -->
     <div class="prodiv12">
        <?php
        
        foreach ($deadlines_count as $key => $value) 
        {
           if($value['count']['dead_count'] != '0')
           {
        ?>
        <div class="pro-color">
           <span class="procount"><?php echo $value['count']['dead_count']; ?> </span>
           <div class="fordis">
              <div class="progress progress-xs">
                 <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $value['count']['dead_count'].'%'; ?>;" aria-valuenow="<?php echo $value['count']['dead_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_deadline_count; ?>"></div>
              </div>
           </div>
        </div>
      <?php } } ?>
       
     </div>
     <!-- progress bar section -->
  </div>
                                                   </div>
                                                </div>
                                                <div class="left-sireport col-xs-12 col-md-4">
                                                   <div class="comment-desc">
                                                      <h6>
                                                        <span> Invoice</span>
                                                         <div class="comment-btn">
                                                            <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>Report/Invoice">Run</a>
                                                            <a class="btn bg-c-blue btn-round btn-comment " href="<?php echo base_url(); ?>report/Invoice_customisation">Customise</a>
                                                         </div>
                                                      </h6>
                                                      <span class="time-frame"><label>Time Frame:</label>
                                                      <select id="filter_invoice">
                                                         <option value="">Select</option>
                                                         <option value="year">This Year</option>
                                                         <option value="month">This Month</option>
                                                      </select>
                                                      </span>
                                                      <div class="card-block fileters_invoice">
                                                         <div id="piechart_invoice" style="width: 50%;"></div>
                                                         <!-- progress bar section -->
                                                         <div class="prodiv12">
                                                            <?php
                                                               if(count($approved_invoice)!='0'){
                                                               ?>
                                                            <div class="pro-color">
                                                               <span class="procount"><?php echo count($approved_invoice); ?> </span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo count($approved_invoice).'%'; ?> ;" aria-valuenow="<?php echo count($approved_invoice); ?>" aria-valuemin="0" aria-valuemax="<?php echo count($approved_invoice)+count($cancel_invoice)+count($expired_invoice); ?>"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>   
                                                            <?php
                                                               if(count($cancel_invoice)!='0'){
                                                               ?>
                                                            <div class="pro-color">
                                                               <span class="procount"><?php echo count($cancel_invoice); ?> </span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo count($cancel_invoice).'%'; ?> ;" aria-valuenow="<?php echo count($cancel_invoice); ?>" aria-valuemin="0" aria-valuemax="<?php echo count($approved_invoice)+count($cancel_invoice)+count($expired_invoice); ?>"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>  
                                                            <?php
                                                               if(count($expired_invoice)!='0'){
                                                               ?>
                                                            <div class="pro-color">
                                                               <span class="procount"><?php echo count($expired_invoice); ?> </span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo count($expired_invoice).'%'; ?> ;" aria-valuenow="<?php echo count($expired_invoice); ?>" aria-valuemin="0" aria-valuemax="<?php echo count($approved_invoice)+count($cancel_invoice)+count($expired_invoice); ?>"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>                                               
                                                         </div>
                                                         <!-- progress bar section -->
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="left-sireport col-xs-12 col-md-4">
                                                   <div class="comment-desc sumlast">
                                                      <h6>
                                                         <span>Summary</span>
                                                         <div class="comment-btn">                                                         
                                                            <a class="btn bg-c-blue btn-round btn-comment " href="<?php echo base_url(); ?>Report/Summary">Run</a>
                                                            <a class="btn bg-c-blue btn-round btn-comment " href="<?php echo base_url(); ?>Report/SummaryCustomisation">Customise</a>
                                                         </div>
                                                      </h6>
                                                      <!-- progress bar section -->
                                                      <div class="prodiv">
                                                         <div class="pro-color">
                                                            <span class="procount info"><?php echo $users_count['user_count']; ?></span>
                                                            <div class="fordis">
                                                               <label>Total Users</label>
                                                               <div class="progress progress-xs">
                                                                  <div class="progress-bar progress-bar-info" role="progressbar" style="width: <?php echo $users_count['user_count'].'%'; ?>;" aria-valuenow="<?php echo $users_count['user_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $users_count['user_count']; ?>"></div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="pro-color">
                                                            <span class="procount primary"><?php echo $active_user_count['activeuser_count']; ?></span>
                                                            <div class="fordis">
                                                               <label>Active Users</label>
                                                               <div class="progress progress-xs">
                                                                  <div class="progress-bar progress-bar-primary" role="progressbar" style="width: <?php echo $active_user_count['activeuser_count'].'%'; ?>;" aria-valuenow="<?php echo $active_user_count['activeuser_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $users_count['user_count']; ?>"></div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="pro-color">
                                                            <span class="procount warning"><?php echo $inactive_user_count['inactiveuser_count']; ?></span>
                                                            <div class="fordis">
                                                               <label>Inactive Users</label>
                                                               <div class="progress progress-xs">
                                                                  <div class="progress-bar progress-bar-warning" role="progressbar" style="width: <?php echo $inactive_user_count['inactiveuser_count'].'%'; ?>;" aria-valuenow="<?php echo $inactive_user_count['inactiveuser_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $users_count['user_count']; ?>"></div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="pro-color">
                                                            <span class="procount danger"><?php echo $completed_task_count['task_count']; ?></span>
                                                            <div class="fordis">
                                                               <label>Completed Tasks</label>
                                                               <div class="progress progress-xs">
                                                                  <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $completed_task_count['task_count']; ?>;" aria-valuenow="<?php echo $completed_task_count['task_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_tasks['task_count']; ?>"></div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="pro-color success">
                                                            <span class="procount"><?php echo $overall_leads['leads_count']; ?></span>
                                                            <div class="fordis">
                                                               <label>Over all Leads</label>
                                                               <div class="progress progress-xs">
                                                                  <div class="progress-bar progress-bar-success" role="progressbar" style="width: <?php echo $overall_leads['leads_count'].'%'; ?>;" aria-valuenow="<?php echo $overall_leads['leads_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $overall_leads['leads_count']; ?>"></div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <!-- progress bar section -->
                                                   </div>
                                                </div>
                                                <div class="left-sireport col-xs-12 col-md-4">

                                                <div class="comment-desc sumlast">
                                                      <h6>
                                                         Timesheet
                                                         <div class="comment-btn">                                                        
                                                            <a class="btn bg-c-blue btn-round btn-comment " href="<?php echo base_url(); ?>Timesheet">Run</a>
                                                            <a class="btn bg-c-blue btn-round btn-comment " href="<?php echo base_url(); ?>Timesheet/timesheet_custom">Customise</a>
                                                         </div>
                                                      </h6>
                                                      </div>
                                                </div>


                                                <div class="left-sireport col-xs-12 col-md-4">

                                                <div class="comment-desc sumlast">
                                                      <h6>
                                                       <span>  Individual User Performance</span>
                                                         <div class="comment-btn">                                                        
                                                            <a class="btn bg-c-blue btn-round btn-comment " href="<?php echo base_url(); ?>Reports/user_performation">Run</a>
                                                            <a class="btn bg-c-blue btn-round btn-comment " href="<?php echo base_url(); ?>Reports/performance_customisation">Customise</a>
                                                         </div>
                                                      </h6>


                                                      </div>
                                                  
                                                </div>
                                             </div>
                                              
                                          </div>
                                       </div>
                                       <div class="tab-pane fade legal-disk01" id="clients-reports">
                                          <div class="card-block-small">
                                             <div class="left-sireport col-xs-12  col-md-6">
                                                <div  class="comment-desc">
                                                <h6>
                                                         Legal Form 
                                                        
                                                      </h6>
                                                <div class="card-block fileters_client new123">
                                                <div id="Tabs_client" style="width:100%;">      
                                                </div>
                                             </div>
                                             </div>
                                             </div>
                                            <div class="left-sireport col-xs-12  col-md-6">
                                             <div class="comment-desc">
                                             <h6>
                                                         Status 
                                                         
                                                      </h6>
                                             <div class="card-block fileters_client new123">
                                                <div id="Tabs_Users" style="width:100%;"></div>
                                                </div>
                                             </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="tab-pane fade legal-disk01" id="task-reports">
                                          <div class="card-block-small">
                                          <div class="left-sireport col-xs-12  col-md-6">
                                                <div  class="comment-desc">
                                                <h6>
                                                         Status  
                                                      </h6>
                                                <div class="card-block fileters_client new123">
                                             <div id="task_tab" style="width: 50%;"></div>
                                             <div class="prodiv12">
                                  <?php
                                     if($notstarted_tasks['task_count']!='0'){ ?>
                                  <div class="pro-color">
                                     <span class="procount"><?php echo $notstarted_tasks['task_count']; ?></span>
                                     <div class="fordis">
                                        <div class="progress progress-xs">
                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $notstarted_tasks['task_count'].'%'; ?>;" aria-valuenow="<?php echo $notstarted_tasks['task_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_tasks['task_count']; ?>"></div>
                                        </div>
                                     </div>
                                  </div>
                                  <?php } ?>    
                                  <?php
                                     if($completed_tasks['task_count']!='0'){ ?>
                                  <div class="pro-color">
                                     <span class="procount"><?php echo $completed_tasks['task_count']; ?></span>
                                     <div class="fordis">
                                        <div class="progress progress-xs">
                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $completed_tasks['task_count'].'%'; ?>;" aria-valuenow="<?php echo $completed_tasks['task_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_tasks['task_count']; ?>"></div>
                                        </div>
                                     </div>
                                  </div>
                                  <?php } ?> 
                                  <?php
                                     if($inprogress_tasks['task_count']!='0'){ ?>
                                  <div class="pro-color">
                                     <span class="procount"><?php echo $inprogress_tasks['task_count']; ?></span>
                                     <div class="fordis">
                                        <div class="progress progress-xs">
                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $inprogress_tasks['task_count'].'%'; ?>;" aria-valuenow="<?php echo $inprogress_tasks['task_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_tasks['task_count']; ?>"></div>
                                        </div>
                                     </div>
                                  </div>
                                  <?php } ?> 
                                  <?php
                                     if($awaiting_tasks['task_count']!='0'){ ?>
                                  <div class="pro-color">
                                     <span class="procount"><?php echo $awaiting_tasks['task_count']; ?></span>
                                     <div class="fordis">
                                        <div class="progress progress-xs">
                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $awaiting_tasks['task_count'].'%'; ?>;" aria-valuenow="<?php echo $awaiting_tasks['task_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_tasks['task_count']; ?>"></div>
                                        </div>
                                     </div>
                                  </div>
                                  <?php } ?> 
                                  <?php
                                     if($archive_tasks['task_count']!='0'){ ?>
                                  <div class="pro-color">
                                     <span class="procount"><?php echo $archive_tasks['task_count']; ?></span>
                                     <div class="fordis">
                                        <div class="progress progress-xs">
                                           <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $archive_tasks['task_count'].'%'; ?>;" aria-valuenow="<?php echo $archive_tasks['task_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_tasks['task_count']; ?>"></div>
                                        </div>
                                     </div>
                                  </div>
                                  <?php } ?>                            
                                             </div>
                                             </div>
                                             </div>
                                             </div>
                                             <div class="left-sireport col-xs-12  col-md-6">
                                             <div  class="comment-desc">
                                             <h6> Priority </h6>
                                             <div class="card-block fileters_client new123">
                                                <div id="tabs_priority"></div>
                                             </div>
                                             </div>
                                             </div>
                                             <div class="left-sireport col-xs-12 line-chart123">
                                             <div  class="comment-desc">
                                             <h6>
                                                         This Month Tasks 
                                                         
                                                      </h6>
                                                <div class="card-block fileters_client new123">
                                             <div id="monthly_task" style="width: 1200px; height: 500px"></div>
                                             </div>
                                             </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="tab-pane fade" id="proposals-reports">
                                          <div class="card-block-small">
                                             <div class="row history">
                                                <div class="card">
                                                   <div class="card-header">
                                                      <h5>This Month Proposal History</h5>
                                                   </div>
                                                   <div class="card-block">
                                                      <div class="main-timeline">
                                                         <div class="cd-timeline cd-container">
                                                            <?php 
            $i=1;
            foreach($proposal_history as $p_h){  
            $randomstring=generateRandomString('100');
            if($p_h['status'] == 'opened'){$p_h['status']='viewed';}
            ?>
          <div class="cd-timeline-block" id="<?php echo $i; ?>">
            <div class="cd-timeline-icon bg-primary">
              <i class="icofont icofont-ui-file"></i>
            </div>
            <div class="cd-timeline-content card_main">
              <div class="p-20">
                <div class="timeline-details">
                  <p class="m-t-20 search_word">
                    <a href="<?php echo base_url().'proposal_page/step_proposal/'.$randomstring.'---'.$p_h['id']; ?>"><?php echo $p_h['proposal_name']; ?> </a> 
                  </p>
                </div>
              </div>
              <span class="cd-date"><?php
                $timestamp = strtotime($p_h['created_at']);
                $newDate = date('d F Y H:i', $timestamp); 
                echo $newDate;
                ?></span>
              <span class="cd-date"><?php echo 'Sent By : '.$p_h['sender_company_name']; ?></span>
              <span class="cd-date"><?php echo 'Status : '.ucfirst($p_h['status']); ?></span>
            </div>
          </div>
          <?php $i++; } ?>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="tab-pane fade" id="leads-reports">
                                          <div class="card-block-small">
                                             <div class="row history">
                                                <div class="card">
                                                   <div class="card-header">
                                                      <h5>This Month Leads History</h5>
                                                   </div>
                                                   <div class="card-block">
                                                      <div class="main-timeline">
                                                         <div class="cd-timeline cd-container">
                                                            <?php
                                                               foreach($leads_history as $lead){                              
                                                               ?>
                                                            <div class="cd-timeline-block">
                                                               <div class="cd-timeline-icon bg-primary">
                                                                  <i class="icofont icofont-ui-file"></i>
                                                               </div>
                                                               <div class="cd-timeline-content card_main">
                                                                  <div class="p-20">
                                                                     <div class="timeline-details">
                                                                        <p class="m-t-20"><a href="<?php echo base_url().'leads/leads_detailed_tab/'.$lead['id']; ?>"> <?php echo ucwords($lead['name']); ?></a>
                                                                        </p>
                                                                     </div>
                                                                  </div>
                                                                  <span class="cd-date"><?php
                                                                     $timestamp = $lead['createdTime'];
                                                                     $newDate = date('d F Y H:i', $timestamp); 
                                                                     echo $newDate;
                                                                     ?></span>
                                                                  <span class="cd-date">Status:&nbsp;<?php              
                                                                     echo ucwords($this->Common_mdl->get_price('leads_status','id',$lead['lead_status'],'status_name'));
                                                                     ?></span>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="tab-pane fade com-chart legal-disk01" id="services-reports">
                                          <div class="card-block-small">
                                          <div class="left-sireport col-xs-12  col-md-6">
                                                   <div class="comment-desc">
                                                   <h6>  Services  </h6>
                                             <div id="service_tab" style="width:100%;"></div>
                                             </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="tab-pane fade com-chart legal-disk01" id="deadlines-reports">
                                          <div class="card-block-small">
                                          <div class="left-sireport col-xs-12  col-md-6">
                                                   <div class="comment-desc">
                                                   <h6> This Month Deadlines  </h6>
                                             <div id="Tabs_deadline" style="width:100%;"></div>
                                             </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="tab-pane fade com-chart legal-disk01" id="invoices-reports">
                                          <div class="card-block-small">
                                          <div class="left-sireport col-xs-12  col-md-6">
                                                   <div class="comment-desc">
                                                   <h6> This Month Invoice   </h6>
                                             <div id="Tabs_invoice" style="width:100%;"></div>
                                             </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div> <!--tab content close -->
                                    <!-- home-->
                                    <!-- frozen -->
                                 </div>
                                 <!-- ul after div -->
                              </div>
                           </div>
                           <!-- admin close -->
                        </div>
                        <!-- Register your self card end -->
                     </div>
                  </div>
               </div>
               <!-- Page body end -->
            </div>
         </div>
         <!-- Main-body end -->
         <div id="styleSelector">
         </div>
      </div>
   </div>
</div>
<!-- email all -->
<input type="hidden" name="user_id" id="user_id" value="">
<!--style="display: none" -->
<div class="charts" style="display: none">
   <div class="card-block fileters_client1">
   </div>
   <div class="card-block fileters_Tasks1">
   </div>
   <div class="card-block filter_proposal1">
   </div>
   <div class="card-block filter_leads1">
   </div>
   <div class="card-block filter_service1">
   </div>
   <div class="card-block filter_deadline1">
   </div>
   <div class="card-block fileters_invoice1">
   </div>
</div>
<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<?php $this->load->view('reports/reports_scripts');?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.mintScrollbar.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.mousewheel.min.js"></script>
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/pie.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>

<script>
$( document ).ready(function() { 
var chart = AmCharts.makeChart("chartdiv55", {
  "type": "pie",
  "startDuration": 0,
   "theme": "light",
  "addClassNames": true,
  "legend":{
      "position":"right",
    "marginRight":100,
    "autoMargins":false
  },
  "innerRadius": "30%",
  "defs": {
    "filter": [{
      "id": "shadow",
      "width": "200%",
      "height": "200%",
      "feOffset": {
        "result": "offOut",
        "in": "SourceAlpha",
        "dx": 0,
        "dy": 0
      },
      "feGaussianBlur": {
        "result": "blurOut",
        "in": "offOut",
        "stdDeviation": 5
      },
      "feBlend": {
        "in": "SourceGraphic",
        "in2": "blurOut",
        "mode": "normal"
      }
    }]
  },
  "dataProvider": [{
    "country": "Lithuania",
    "litres": 501.9
  }, {
    "country": "Czech Republic",
    "litres": 301.9
  }, {
    "country": "Ireland",
    "litres": 201.1
  }, {
    "country": "Germany",
    "litres": 165.8
  }, {
    "country": "Australia",
    "litres": 139.9
  }, {
    "country": "Austria",
    "litres": 128.3
  }, {
    "country": "UK",
    "litres": 99
  }, {
    "country": "Belgium",
    "litres": 60
  }, {
    "country": "The Netherlands",
    "litres": 50
  }],
  "valueField": "litres",
  "titleField": "country",
  "export": {
    "enabled": true
  },
  legend: {
    "divId": "legenddiv",
    "autoMargins": false,
    "enabled": true,
    "maxColumns": 1,
    "valueHight": 100
  },
});  

});
</script>

<script type="text/javascript">
   $( document ).ready(function() {
      $(".prodiv12").mintScrollbar({
      onChange: null,
      axis: "auto",
      wheelSpeed: 120,
      disableOnMobile: true
      });
   });
</script>
<!-- Client Chart -->
<script type="text/javascript">

   google.charts.load("current", {"packages":["corechart"]});

   google.charts.setOnLoadCallback(drawChart);
   function drawChart() {
     var data = google.visualization.arrayToDataTable([
       ['Legal Forms','link','Count'],
       ['Private Limited company', '<?php echo base_url();?>Firm_dashboard/private_function', <?php echo $Private['limit_count']; ?>],
       ['Public Limited company',  '<?php echo base_url();?>Firm_dashboard/public_function',   <?php echo $Public['limit_count']; ?>],
       ['Limited Liability Partnership','<?php echo base_url();?>Firm_dashboard/limited_function', <?php echo $limited['limit_count']; ?>],
       ['Partnership', '<?php echo base_url();?>Firm_dashboard/partnership', <?php echo $Partnership['limit_count']; ?>],
       ['Self Assessment','<?php echo base_url();?>Firm_dashboard/self_assesment',   <?php echo $self['limit_count']; ?>],
       ['Trust','<?php echo base_url();?>Firm_dashboard/trust',<?php echo $Trust['limit_count']; ?>],
       ['Charity','<?php echo base_url();?>Firm_dashboard/charity',<?php echo $Charity['limit_count']; ?>],
        ['Other','<?php echo base_url();?>Firm_dashboard/other',<?php echo $Other['limit_count']; ?>],
     ]);  
   
      var view = new google.visualization.DataView(data);
          view.setColumns([0, 2]);
    
   
     var options = {
       chartArea: {left: 0, width: "80%"},
        height: 250,
       legend: { position: 'bottom', show: true, container: $("#large-legend"), alignment: 'start' },
       hAxis : { textStyle : { fontSize: 8} },
       vAxis : { textStyle : { fontSize: 8} },
         width: 350,
        height: 250
      // pieHole: 0.4,
     };  
     var chart_div = new google.visualization.PieChart(document.getElementById('chart_Donut11'));

     // new google.visualization.PieChart(document.getElementById('container'))
     var chart = new google.visualization.PieChart(document.getElementById('chart_Donut11'));
     google.visualization.events.addListener(chart, 'select', function () {
     chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
     //document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '"></a>';
     var image=chart.getImageURI();
        $(document).ready(function(){   
                  $.ajax({
                         type: "POST",
                         url: "<?php echo base_url();?>/Reports/pdf_download",         
                         data: {image: image},
                         success: 
                              function(data){
                     
                              }
                          });
                   });
   });
     chart.draw(view, options);
   
     var selectHandler = function(e) {
          //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
          window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
          }
          // Add our selection handler.
          google.visualization.events.addListener(chart, 'select', selectHandler);
   }


   
    google.charts.setOnLoadCallback(drawChart1);
    function drawChart1() {
      var data = google.visualization.arrayToDataTable([
        ['Tasklist','link','Count'],
        ['Not started', '<?php echo base_url(); ?>Firm_dashboard/notstarted',<?php echo $notstarted_tasks['task_count']; ?>],
        ['Completed',  '<?php echo base_url(); ?>Firm_dashboard/complete',<?php echo $completed_tasks['task_count']; ?>],
        ['Inprogress','<?php echo base_url(); ?>Firm_dashboard/inprogress',<?php echo $inprogress_tasks['task_count']; ?>],
        ['Awaiting Feedback','<?php echo base_url(); ?>Firm_dashboard/awaited',<?php echo $awaiting_tasks['task_count']; ?>],
        ['Archived',  '<?php echo base_url(); ?>Firm_dashboard/archive',<?php echo $archive_tasks['task_count']; ?>] 
      ]);  
   
       var view = new google.visualization.DataView(data);
           view.setColumns([0, 2]);
     
   
      var options = {
         chartArea: {left:0, width: "80%"},
         height:250, 
        legend: { position: 'bottom', show: true, container: $("#large-legend"), alignment: 'start' },
        hAxis : { textStyle : { fontSize: 8} },
        vAxis : { textStyle : { fontSize: 8} },
          width: 350,
          height: 250
       // pieHole: 0.4,
      };  
      var chart_div = new google.visualization.PieChart(document.getElementById('pie_chart'));
      var chart = new google.visualization.PieChart(pie_chart);
      google.visualization.events.addListener(chart, 'select', function () {
      chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
      //document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '"></a>';
      var image=chart.getImageURI();
         $(document).ready(function(){   
                   $.ajax({
                          type: "POST",
                          url: "<?php echo base_url();?>/Reports/task_chart",         
                          data: {chart_image: image},
                          success: 
                               function(data){
                      
                               }
                           });
                    });
    });
      chart.draw(view, options);
       var selectHandler = function(e) {
          //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
            window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
           }
           // Add our selection handler.
           google.visualization.events.addListener(chart, 'select', selectHandler);
    }

   google.charts.setOnLoadCallback(drawChart2);
   
   function drawChart2() {
   
    var data = google.visualization.arrayToDataTable([
      ['Proposal','link','Count'],
      ['Accepted', '<?php echo base_url(); ?>Firm_dashboard/accept', <?php echo count($accept_proposal); ?>],
      ['In Discussion',  '<?php echo base_url(); ?>Firm_dashboard/indiscussion', <?php echo count($indiscussion_proposal); ?>],
      ['Sent','<?php echo base_url(); ?>Firm_dashboard/sent',  <?php echo count($sent_proposal); ?>],
      ['Viewed','<?php echo base_url(); ?>Firm_dashboard/view', <?php echo count($viewed_proposal); ?>],
      ['Declined', '<?php echo base_url(); ?>Firm_dashboard/decline',   <?php echo count($declined_proposal); ?>],
      ['Archived',  '<?php echo base_url(); ?>Firm_dashboard/archieve',  <?php echo count($archive_proposal); ?>],
      ['Draft',  '<?php echo base_url(); ?>Firm_dashboard/draft',  <?php echo count($draft_proposal); ?>]
    ]);     
   
     var view = new google.visualization.DataView(data);
      view.setColumns([0, 2]); 
   
    var options = {
        chartArea: {left:0, width: "80%"},
        height:250, 
   legend: { position: 'bottom', show: true, container: $("#large-legend"), alignment: 'start' },
   hAxis : { textStyle : { fontSize: 8} },
   vAxis : { textStyle : { fontSize: 8} },
     width: 350,
     height: 250
   // pieHole: 0.4,
    };
   
     var chart_div = new google.visualization.PieChart(document.getElementById('piechart1'));
   var chart = new google.visualization.PieChart(document.getElementById('piechart1'));
   google.visualization.events.addListener(chart, 'select', function () {
   chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
   //document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '"></a>';
   var image=chart.getImageURI();
    $(document).ready(function(){  
   // alert(image);
              $.ajax({
                     type: "POST",
                     url: "<?php echo base_url();?>/Report/proposal_chart",         
                     data: {proposal_image: image},
                     success: 
                          function(data){
                 
                          }
                      });
               });
   });
   chart.draw(view, options);
   var selectHandler = function(e) {
     //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
       window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
      }
      // Add our selection handler.
      google.visualization.events.addListener(chart, 'select', selectHandler);
   }
   

   google.charts.setOnLoadCallback(drawChart3);
   
   function drawChart3() {
   
     var data = google.visualization.arrayToDataTable([
       ['Leads','link','Count'],
           <?php      
      foreach($leads_status as $status){
      $status_name=$status['status_name'];
      $count=$this->Report_model->lead_details($status['id']); ?>
            ['<?php echo $status_name; ?>','<?php echo base_url(); ?>/leads?status=<?php $strlower = strtolower(preg_replace("~[\\\\/:*?'&,<>|]~", '', $status_name));
      $lesg = str_replace(' ', '-', $strlower);
      $lesgs = str_replace('--', '-', $lesg); echo $lesgs; ?>',<?php echo $count['lead_count']; ?> ],
            <?php }  ?>
      ]);      
     var view = new google.visualization.DataView(data);
       view.setColumns([0, 2]);
   
     var options = {
          chartArea: {left:0, width: "80%"}, 
          height:250,
    legend: { position: 'bottom', show: true, container: $("#large-legend"), alignment: 'start' },
    hAxis : { textStyle : { fontSize: 8} },
    vAxis : { textStyle : { fontSize: 8} },
      width: 350,
      height: 250
   // pieHole: 0.4,
     };
   
   var chart_div = new google.visualization.PieChart(document.getElementById('piechart_1'));
   var chart = new google.visualization.PieChart(document.getElementById('piechart_1'));
   google.visualization.events.addListener(chart, 'select', function () {
   chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
   //document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '"></a>';
   var image=chart.getImageURI();
     $(document).ready(function(){  
    // alert(image);
               $.ajax({
                      type: "POST",
                      url: "<?php echo base_url();?>/Report/lead_chart",         
                      data: {lead_image: image},
                      success: 
                           function(data){
                  
                           }
                       });
                });
   });
   chart.draw(view, options);
   var selectHandler = function(e) {
       //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
       window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
       }
       // Add our selection handler.
       google.visualization.events.addListener(chart, 'select', selectHandler);
   }


   <?php 

   $services_arr = "";

   foreach($services_count as $key => $value) 
   { 
      $services_arr .= '["'.$key.'","'.base_url().'Firm_dashboard/service/'.$value['id'].'",'.$value['count']['service_count'].'],';
   }  

   $services_arr = substr_replace($services_arr,'', -1);
   
   ?>   

   google.charts.setOnLoadCallback(drawChart4);
   
   function drawChart4() {
   
     var data = google.visualization.arrayToDataTable([
       ['Servicelist','link','Count'], <?php echo $services_arr; ?>  
      ]);  
   
       var view = new google.visualization.DataView(data);
       view.setColumns([0, 2]);    
   
     var options = {
          chartArea: {left:0, width: "80%"}, 
          height: 250,
    legend: { position: 'bottom', show: true, container: $("#large-legend"), alignment: 'start' },
    hAxis : { textStyle : { fontSize: 8} },
    vAxis : { textStyle : { fontSize: 8} },
      width: 350,
      height: 250
   //  pieHole: 0.4,
     };
   
   var chart_div = new google.visualization.PieChart(document.getElementById('piechart_service'));
   var chart = new google.visualization.PieChart(document.getElementById('piechart_service'));
   google.visualization.events.addListener(chart, 'select', function () {
   chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
   //document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '"></a>';
   var image=chart.getImageURI();
     $(document).ready(function(){  
    // alert(image);
               $.ajax({
                      type: "POST",
                      url: "<?php echo base_url();?>/Report/service_chart",         
                      data: {service_image: image},
                      success: 
                           function(data){
                  
                           }
                       });
                });
   });
   chart.draw(view, options);
   var selectHandler = function(e) {
       //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
       window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
       }
       // Add our selection handler.
       google.visualization.events.addListener(chart, 'select', selectHandler);
       }

   <?php 

   $deadlines_arr = "";

   foreach($deadlines_count as $key => $value) 
   { 
      $deadlines_arr .= '["'.$key.'","'.base_url().'Firm_dashboard/service/'.$value['id'].'",'.$value['count']['dead_count'].'],';
   }  

   $deadlines_arr = substr_replace($deadlines_arr,'', -1);
   
   ?>   
   

   google.charts.setOnLoadCallback(drawChart5);
   
   function drawChart5() {
   
     var data = google.visualization.arrayToDataTable([
       ['Dead_line', 'link','Count'], <?php echo $deadlines_arr; ?>      
      ]);      
     var view = new google.visualization.DataView(data);
       view.setColumns([0, 2]);
   
     var options = {
         chartArea: {left:0, width: "80%"}, 
         height:250,
    legend: { position: 'bottom', show: true, container: $("#large-legend"), alignment: 'start' },
    hAxis : { textStyle : { fontSize: 8} },
    vAxis : { textStyle : { fontSize: 8} },
      width: 350,
      height: 250
   // pieHole: 0.4,
     };
   
   var chart_div = new google.visualization.PieChart(document.getElementById('piechart_deadline'));
   var chart = new google.visualization.PieChart(document.getElementById('piechart_deadline'));
   google.visualization.events.addListener(chart, 'select', function () {
   chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
   //document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '"></a>';
   var image=chart.getImageURI();
     $(document).ready(function(){  
    // alert(image);
               $.ajax({
                      type: "POST",
                      url: "<?php echo base_url();?>/Report/deadline_chart",         
                      data: {deadline_image: image},
                      success: 
                           function(data){
                  
                           }
                       });
                });
   });
   chart.draw(view, options);
   var selectHandler = function(e) {       
       window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
       }      
       google.visualization.events.addListener(chart, 'select', selectHandler);
       }
   

   google.charts.setOnLoadCallback(drawChart6);
   
   function drawChart6() {  
     var data = google.visualization.arrayToDataTable([
       ['Invoices', 'link','Count'],             
        ['Approved','<?php echo base_url(); ?>invoice?type=1',<?php echo count($approved_invoice); ?> ],
        ['Cancelled','<?php echo base_url(); ?>invoice?type=3', <?php echo count($cancel_invoice); ?> ],
        ['Pending','<?php echo base_url(); ?>invoice?type=2', <?php echo count($expired_invoice); ?> ]               
      ]);      
     var view = new google.visualization.DataView(data);
       view.setColumns([0, 2]);
   
     var options = {
         chartArea: {left:0, width: "80%"}, 
         height:250,
    legend: { position: 'bottom', show: true, container: $("#large-legend"), alignment: 'start' },
    hAxis : { textStyle : { fontSize: 8} },
    vAxis : { textStyle : { fontSize: 8} },
      width: 350,
      height: 250
   // pieHole: 0.4,
     };
   
   var chart_div = new google.visualization.PieChart(document.getElementById('piechart_invoice'));
   var chart = new google.visualization.PieChart(document.getElementById('piechart_invoice'));
   google.visualization.events.addListener(chart, 'select', function () {
   chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
   //document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '"></a>';
   var image=chart.getImageURI();
     $(document).ready(function(){  
    // alert(image);
               $.ajax({
                      type: "POST",
                      url: "<?php echo base_url();?>Filter/invoice_chart",         
                      data: {invoice_image: image},
                      success: 
                           function(data){
                  
                           }
                       });
                });
   });
   chart.draw(view, options);
   var selectHandler = function(e) {       
       window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
       }      
       google.visualization.events.addListener(chart, 'select', selectHandler);
       }

     google.charts.setOnLoadCallback(drawChart7);
          
     function drawChart7()
     {
         var data = google.visualization.arrayToDataTable([
         ['Tasks','link','Count'],
         ['Low', '<?php echo base_url(); ?>Firm_dashboard/low',<?php echo $low_tasks['task_count']; ?>],
         ['High',  '<?php echo base_url(); ?>Firm_dashboard/high',<?php echo $high_tasks['task_count']; ?>],
         ['Medium','<?php echo base_url(); ?>Firm_dashboard/medium', <?php echo $medium_tasks['task_count']; ?>],
         ['Super Urgent','<?php echo base_url(); ?>Firm_dashboard/super_urgent',<?php echo $super_urgent_tasks['task_count']; ?>]   
         ]);  

         var view = new google.visualization.DataView(data);
          view.setColumns([0, 2]);


         var options = {
         chartArea: {left: 0, top: 0, width: "100%", height: "80%"},     
         legend: { position: 'bottom' },
         hAxis : { textStyle : { fontSize: 8} },
         vAxis : { textStyle : { fontSize: 8} },
         // legend: { position: 'bottom' },
         // hAxis : { textStyle : { fontSize: 8} },
         // vAxis : { textStyle : { fontSize: 8} },
         // pieHole: 0.4,
         };  
         var chart = new google.visualization.PieChart(document.getElementById('tabs_priority'));   
         chart.draw(view, options);   
         var selectHandler = function(e) {
         //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
         window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
         }
         // Add our selection handler.
         google.visualization.events.addListener(chart, 'ready', selectHandler);
     }
     
      //Tab Details Section 
      
      
      google.charts.setOnLoadCallback(drawChart8);
      
      function drawChart8() {
        var data = google.visualization.arrayToDataTable([
          ['Legal Form','link','Count'],
          ['Private Limited company', '<?php echo base_url();?>Firm_dashboard/private_function', <?php echo $Private['limit_count']; ?>],
          ['Public Limited company',  '<?php echo base_url();?>Firm_dashboard/public_function',   <?php echo $Public['limit_count']; ?>],
          ['Limited Liability Partnership','<?php echo base_url();?>Firm_dashboard/limited_function', <?php echo $limited['limit_count']; ?>],
          ['Partnership', '<?php echo base_url();?>Firm_dashboard/partnership', <?php echo $Partnership['limit_count']; ?>],
          ['Self Assessment','<?php echo base_url();?>Firm_dashboard/self_assesment',   <?php echo $self['limit_count']; ?>],
          ['Trust','<?php echo base_url();?>Firm_dashboard/trust',<?php echo $Trust['limit_count']; ?>],
          ['Charity','<?php echo base_url();?>Firm_dashboard/charity',<?php echo $Charity['limit_count']; ?>],
          ['Other','<?php echo base_url();?>Firm_dashboard/other',<?php echo $Other['limit_count']; ?>]
        ]);  
      
         var view = new google.visualization.DataView(data);
             view.setColumns([0,2]);
       
      
        var options = {       
          chartArea: {left: 0, width: "80%"},
           height: 250,
          legend: { position: 'bottom', show: true, container: $("#large-legend"), alignment: 'start' },
          hAxis : { textStyle : { fontSize: 8} },
          vAxis : { textStyle : { fontSize: 8} },
            width: 350,
           height: 250
         // pieHole: 0.4,
        };  
        var chart_div = new google.visualization.PieChart(document.getElementById('Tabs_client'));

        // new google.visualization.PieChart(document.getElementById('container'))
        var chart = new google.visualization.PieChart(document.getElementById('Tabs_client'));
        google.visualization.events.addListener(chart, 'ready', function () {
        chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
        //document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '"></a>';
        var image=chart.getImageURI();
           $(document).ready(function(){   
                     $.ajax({
                            type: "POST",
                            url: "<?php echo base_url();?>/Reports/pdf_download",         
                            data: {image: image},
                            success: 
                                 function(data){
                        
                                 }
                             });
                      });
      });
        chart.draw(view, options);
      
        var selectHandler = function(e) {
             //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
             window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
             }
             // Add our selection handler.
             google.visualization.events.addListener(chart, 'ready', selectHandler);
      }   

      google.charts.setOnLoadCallback(drawChart9);
      function drawChart9() {
      var data = google.visualization.arrayToDataTable([
       ['user status','link','Count'],
       ['active', '<?php echo base_url();?>Firm_dashboard/active', <?php echo $active_user_count['activeuser_count']; ?>],
       ['inactive',  '<?php echo base_url();?>Firm_dashboard/inactive',   <?php echo $inactive_user_count['inactiveuser_count']; ?>],
       ['frozen','<?php echo base_url();?>Firm_dashboard/frozen', <?php echo $frozen_user_count['frozenuser_count']; ?>],     
      ]);  
      
      var view = new google.visualization.DataView(data);
          view.setColumns([0, 2]);
      
      
      var options = {
      chartArea: {'width': '100%', 'height': '80%'},        
       legend: { position: 'bottom' },
       hAxis : { textStyle : { fontSize: 8} },
       vAxis : { textStyle : { fontSize: 8} },
      };  
      var chart = new google.visualization.PieChart(document.getElementById('Tabs_Users'));   
         chart.draw(view, options);   
         var selectHandler = function(e) {
        //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
          window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
         }
         // Add our selection handler.
         google.visualization.events.addListener(chart, 'ready', selectHandler);
      }

         google.charts.setOnLoadCallback(drawChart10);
         function drawChart10() {
           var data = google.visualization.arrayToDataTable([
             ['Task','link','Count'],
             ['Not started', '<?php echo base_url(); ?>Firm_dashboard/notstarted',<?php echo $notstarted_tasks['task_count']; ?>],
             ['Completed',  '<?php echo base_url(); ?>Firm_dashboard/complete',<?php echo $completed_tasks['task_count']; ?>],
             ['Inprogress','<?php echo base_url(); ?>Firm_dashboard/inprogress',<?php echo $inprogress_tasks['task_count']; ?>],
             ['Awaiting Feedback','<?php echo base_url(); ?>Firm_dashboard/awaited',<?php echo $awaiting_tasks['task_count']; ?>],
             ['Archived',  '<?php echo base_url(); ?>Firm_dashboard/archive',<?php echo $archive_tasks['task_count']; ?>]   
           ]);  
        
            var view = new google.visualization.DataView(data);
                view.setColumns([0, 2]);
          
        
           var options = {
              chartArea: {left:0, width: "80%"},
              height:250, 
             legend: { position: 'bottom', show: true, container: $("#large-legend"), alignment: 'start' },
             hAxis : { textStyle : { fontSize: 8} },
             vAxis : { textStyle : { fontSize: 8} },
               width: 350,
               height: 250
            // pieHole: 0.4,
           }; 

            var chart = new google.visualization.PieChart(document.getElementById('task_tab'));   
              chart.draw(view, options);   
              var selectHandler = function(e) {
             //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
               window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');              
              }
              // Add our selection handler.
              google.visualization.events.addListener(chart, 'ready', selectHandler);
           }

      google.charts.setOnLoadCallback(drawChart11);
      
      function drawChart11() {
      var data = google.visualization.arrayToDataTable([
      <?php
         $start_array=array();
         $end_array=array();
         $start_montharray=array();
         $end_montharray=array();
         $j=1;
         foreach($task_list_this_month as $value){ 
               $start_date=explode('-',$value['start_date']);          
               $start=$start_date['2']; 
               $start_month=$start_date['1'];
               $end_date=explode('-',$value['end_date']);
               $end=$end_date['2'];
               $end_month=$end_date['1'];          }
               $month['month']=array(); 
               $month['start_date']=array(); 
               $month['end_date']=array(); 
               $month_other=array(); 
          foreach($task_list_this_month as $value){ 
               $start_date=explode('-',$value['start_date']);          
               $start=$start_date['2']; 
               $start_month=$start_date['1'];
               $end_date=explode('-',$value['end_date']);
               $end=$end_date['2'];
               $end_month=$end_date['1']; ?> 
      <?php  if(date('m')=='01'){ ?>
       ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='01'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end;  } ?>],
         <?php } if(date('m')=='02'){ ?>
      ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='02'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
        <?php } if(date('m')=='03'){ ?>
      ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='03'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
        <?php } if(date('m')=='04'){ ?>
      ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='04'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
        <?php } if(date('m')=='05'){ ?>
      ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='05'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],   
      <?php } ?>
      <?php  if(date('m')=='06'){ ?>
      ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='06'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
      <?php } ?>
        <?php  if(date('m')=='07'){ ?>
        ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='07'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
        <?php } ?>
          <?php  if(date('m')=='08'){ ?>
      ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='08'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
        <?php } if(date('m')=='09'){ ?>
      ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='09'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
        <?php } if(date('m')=='10'){ ?>
      ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='10'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
        <?php } if(date('m')=='11'){ ?>
      ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='11'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
        <?php } if(date('m')=='12'){ ?>
      ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='12'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
        <?php  } ?>
      
      <?php } ?>     
      ], true);
      var options = {
      legend:'none',
      'width':1150,
      'height':500, 
       axisTitlesPosition:'in',
       candlestick: {
                  fallingColor: { strokeWidth: 0, fill: '#3366cc' }, // red
                  risingColor: { strokeWidth: 0, fill: '#3366cc' }   // green
               }
      };
      var chart = new google.visualization.CandlestickChart(document.getElementById('monthly_task'));
      chart.draw(data, options);
      }

      google.charts.setOnLoadCallback(drawChart12);  

      function drawChart12()
      {   
          var data = google.visualization.arrayToDataTable([
          ['Service','link','Count'], <?php echo $services_arr; ?>]);  
      
          var view = new google.visualization.DataView(data);
          view.setColumns([0, 2]);    

          var options = {
          chartArea: {left: 0, top: 0, width: "100%", height: "100%"}, 
          legend: { position: 'bottom' },
          hAxis : { textStyle : { fontSize: 8} },
          vAxis : { textStyle : { fontSize: 8} },
          /*legend: { position: 'bottom' },
          hAxis : { textStyle : { fontSize: 8} },
          vAxis : { textStyle : { fontSize: 8} },
          pieHole: 0.4,*/
          };

          var chart = new google.visualization.PieChart(document.getElementById('service_tab'));   
          chart.draw(view, options);   
          var selectHandler = function(e) {
          //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
          window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
          }
          // Add our selection handler.
          google.visualization.events.addListener(chart, 'ready', selectHandler);
      }

      google.charts.setOnLoadCallback(drawChart13);   
      
      function drawChart13() 
      {
           var data = google.visualization.arrayToDataTable([
               ['Deadlines', 'link','Count'], <?php echo $deadlines_arr; ?>]);      
           var view = new google.visualization.DataView(data);
               view.setColumns([0, 2]);
           
           var options = {
            chartArea: {left: 0, top: 0, width: "100%", height: "100%"
            }, 
            legend: { position: 'bottom' },
            hAxis : { textStyle : { fontSize: 8} },
            vAxis : { textStyle : { fontSize: 8} },
            // width:1200,
            // height:500,
            // legend: { position: 'bottom' },
            // hAxis : { textStyle : { fontSize: 8} },
            // vAxis : { textStyle : { fontSize: 8} },
            // pieHole: 0.4,
            };
            var chart = new google.visualization.PieChart(document.getElementById('Tabs_deadline'));   
            chart.draw(view, options);   
            var selectHandler = function(e) {
            //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
            window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
            }
            // Add our selection handler.
            google.visualization.events.addListener(chart, 'ready', selectHandler);
              
      }

      google.charts.setOnLoadCallback(drawChart14);
      
      function drawChart14() {  
        var data = google.visualization.arrayToDataTable([
          ['Invoice', 'link','Count'],             
           ['Approved','<?php echo base_url(); ?>invoice?type=1',<?php echo $approved_invoice_month['invoice_count']; ?> ],
           ['Cancelled','<?php echo base_url(); ?>invoice?type=3', <?php echo $cancel_invoice_month['invoice_count']; ?> ],
           ['Pending','<?php echo base_url(); ?>invoice?type=2', <?php echo $pending_invoice_month['invoice_count']; ?> ]               
         ]);      
        var view = new google.visualization.DataView(data);
          view.setColumns([0, 2]);

              
        var options = {
            chartArea: {left: 0, top: 0, width: "100%", height: "100%" }, 
            // width:1200,
            // height:500,
       // legend: { position: 'bottom' },
       // hAxis : { textStyle : { fontSize: 8} },
       // vAxis : { textStyle : { fontSize: 8} },
      // pieHole: 0.4,
        };
      
      var chart = new google.visualization.PieChart(document.getElementById('Tabs_invoice'));   
            chart.draw(view, options);   
            var selectHandler = function(e) {
           //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
             window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
            }
            // Add our selection handler.
            google.visualization.events.addListener(chart, 'ready', selectHandler);
         }    
   
</script>
<!-- Proposal Chart -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/jquery.tagsinput.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/email.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> 
<script type="text/javascript" language="javascript" src="<?php echo base_url()?>assets/js/jquery.tagsinput.js"></script>
<script type="text/javascript">
   $(document).ready(function(){ 
       $( function() {
           $( ".date-picker" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();      
       });
   $( function() {
            $( ".date_picker2" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
       });
   $( function() {
          $( ".date_picker1" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
       }); 
     });
   
   $("#client_customisation").click(function(){
    var fromdate=$("#from_date").val();
    var todate=$("#to_date").val();
    var legal_form=$("#legal_form").val();
    var status=$("#status").val();
    var formData={'fromdate':fromdate,'todate':todate,'legal_form':legal_form,'status':status}
   $.ajax({
         type: "POST",
         url: "<?php echo base_url();?>/Reports/Client_customization",         
         data: formData,
         beforeSend: function() {
           $(".LoadingImage").show();
         },
         success: function(data) {      
             $(".LoadingImage").hide(); 
             if(data!='1'){         
               $(".fileters_client1").html('');
               $(".fileters_client1").append(data); 
             }       
               $("#client_customization").modal('hide'); 
   $(".prodiv12").mintScrollbar({
onChange: null,
axis: "auto",
wheelSpeed: 120,
disableOnMobile: true
});      
            $("#client_download_sess").modal({show:true, backdrop: 'static', keyboard: false});  
                     
         }
       });
   });
   
   $("#task_customisation").click(function(){
    var fromdate=$("#task_from_date").val();
    var todate=$("#task_to_date").val();
    var priority=$("#priority").val();
    var status=$("#task_status").val();
    var formData={'fromdate':fromdate,'todate':todate,'priority':priority,'status':status}
   $.ajax({
         type: "POST",
         url: "<?php echo base_url();?>/Reports/Task_customization",         
         data: formData,
         beforeSend: function() {
           $(".LoadingImage").show();
         },
         success: function(data) {      
             $(".LoadingImage").hide(); 
             if(data!='1'){         
               $(".fileters_Tasks1").html('');
               $(".fileters_Tasks1").append(data); 
             }        
               $("#task_customization").modal('hide');   
               $(".prodiv12").mintScrollbar({
onChange: null,
axis: "auto",
wheelSpeed: 120,
disableOnMobile: true
});    
               $("#task_download_sess").modal({show:true, backdrop: 'static', keyboard: false}); 
         }
       });
   });
   $("#proposal_customisation").click(function(){
    var fromdate=$("#proposal_from_date").val();
    var todate=$("#proposal_to_date").val();
    var status=$("#proposal_status").val();
    var formData={'fromdate':fromdate,'todate':todate,'status':status}
   $.ajax({
         type: "POST",
         url: "<?php echo base_url();?>/Report/Proposal_customization",         
         data: formData,
         beforeSend: function() {
           $(".LoadingImage").show();
         },
         success: function(data) {    
             $(".LoadingImage").hide(); 
             if(data!='1'){         
               $(".filter_proposal1").html('');
               $(".filter_proposal1").append(data); 
             }       
               $("#proposal_customization").modal('hide'); 
               $(".prodiv12").mintScrollbar({
onChange: null,
axis: "auto",
wheelSpeed: 120,
disableOnMobile: true
});      
               $("#proposal_download_sess").modal({show:true, backdrop: 'static', keyboard: false});
         }
       });
   });
   $("#leads_customisation").click(function(){
    var fromdate=$("#leads_from_date").val();
    var todate=$("#leads_to_date").val();
    var status=$("#leads_status").val();
    var formData={'fromdate':fromdate,'todate':todate,'status':status}
   $.ajax({
         type: "POST",
         url: "<?php echo base_url();?>/Report/Leads_customization",         
         data: formData,
         beforeSend: function() {
           $(".LoadingImage").show();
         },
         success: function(data) {     
             $(".LoadingImage").hide(); 
             if(data!='1'){         
               $(".filter_leads1").html('');
               $(".filter_leads1").append(data); 
             }         
               $("#leads_customization").modal('hide');  
               $(".prodiv12").mintScrollbar({
onChange: null,
axis: "auto",
wheelSpeed: 120,
disableOnMobile: true
});     
               $("#leads_download_sess").modal({show:true, backdrop: 'static', keyboard: false});      
         }
       });
   });
   
   $("#service_customisation").click(function(){
    var fromdate=$("#service_from_date").val();
    var todate=$("#service_to_date").val();
    var status=$("#service").val();
    var formData={'fromdate':fromdate,'todate':todate,'status':status}
   $.ajax({
         type: "POST",
         url: "<?php echo base_url();?>/Report/service_customization",         
         data: formData,
         beforeSend: function() {
           $(".LoadingImage").show();
         },
         success: function(data) {     
             $(".LoadingImage").hide(); 
             if(data!='1'){         
               $(".filter_service1").html('');
               $(".filter_service1").append(data); 
             }   
              
               $("#service_customization").modal('hide'); 
               
               $("#service_download_sess").modal({show:true, backdrop: 'static', keyboard: false});   
         }
       });
   });
   
   $("#deadline_customisation").click(function(){
    var fromdate=$("#deadline_from_date").val();
    var todate=$("#deadline_to_date").val();
    var company_type=$('#company_type').val();
    var deadline_type=$("#deadline_type").val();
    var formData={'fromdate':fromdate,'todate':todate,'company_type':company_type,'deadline_type':deadline_type}
    $.ajax({
         type: "POST",
         url: "<?php echo base_url();?>/Report/deadline_customization",         
         data: formData,
         beforeSend: function() {
           $(".LoadingImage").show();
         },
         success: function(data) {      
             $(".LoadingImage").hide(); 
             if(data!='1'){         
               $(".filter_deadline1").html('');
               $(".filter_deadline1").append(data); 
             }     
               $("#deadline_customization").modal('hide');  
               $(".prodiv12").mintScrollbar({
onChange: null,
axis: "auto",
wheelSpeed: 120,
disableOnMobile: true
});
               $("#deadline_download_sess").modal({show:true, backdrop: 'static', keyboard: false});
         }
       });
   });
   
   $("#summary_customisation").click(function(){
    var fromdate=$("#summary_from_date").val();
    var todate=$("#summary_to_date").val();
     var summary_option=$('#summary_option').val();
    var formData={'fromdate':fromdate,'todate':todate,'summary_option':summary_option}
    $.ajax({
         type: "POST",
         url: "<?php echo base_url();?>/Filter/summary_customization",         
         data: formData,
         beforeSend: function() {
           $(".LoadingImage").show();
         },
         success: function(data) {      
             $(".LoadingImage").hide();          
               $("#summary_customization").modal('hide');  
               
               $("#summary_download_sess").modal({show:true, backdrop: 'static', keyboard: false});          
         }
       });
   });
      $(document).on("change","#filter_client",function(){
        id=$(this).val();
      if(id=='month'){      
      $.ajax({
       type: "POST",
       url: "<?php echo base_url(); ?>Filter/client_filter",
       data:  { 'id':id }, 
       beforeSend: function() {
         $(".LoadingImage").show();
       },
       success: function(data){    
          $(".client_chart").html('');
          $(".client_chart").append(data);   
          $(".LoadingImage").hide();
          $(".prodiv12").mintScrollbar({
            onChange: null,
            axis: "auto",
            wheelSpeed: 120,
            disableOnMobile: true
            });     
        }
       });   
      }else{
      $.ajax({
       type: "POST",
       url: "<?php echo base_url(); ?>Filter/client_filter_year",
       data:  { 'id':id }, 
       beforeSend: function() {
         $(".LoadingImage").show();
       },
       success: function(data){   
          $(".client_chart").html('');
          $(".client_chart").append(data);   
          $(".LoadingImage").hide(); 
          $(".prodiv12").mintScrollbar({
            onChange: null,
            axis: "auto",
            wheelSpeed: 120,
            disableOnMobile: true
            });    
       }
       });   
      }
      });
   
   
      $(document).on("change","#filter_task",function(){
        id=$(this).val();
      if(id=='month'){      
      $.ajax({
       type: "POST",
       url: "<?php echo base_url(); ?>Filter/task_filter",
       data:  { 'id':id }, 
       beforeSend: function() {
         $(".LoadingImage").show();
       },
       success: function(data){     
         $(".fileters_Tasks").html('');
         $(".fileters_Tasks").append(data);   
         $(".LoadingImage").hide();
         $(".prodiv12").mintScrollbar({
            onChange: null,
            axis: "auto",
            wheelSpeed: 120,
            disableOnMobile: true
            });     
       }
       });   
      }else{
      $.ajax({
       type: "POST",
       url: "<?php echo base_url(); ?>Filter/task_filter_year",
       data:  { 'id':id }, 
       beforeSend: function() {
         $(".LoadingImage").show();
       },
       success: function(data){    
         $(".fileters_Tasks").html('');
          $(".fileters_Tasks").append(data);   
          $(".LoadingImage").hide();
          $(".prodiv12").mintScrollbar({
            onChange: null,
            axis: "auto",
            wheelSpeed: 120,
            disableOnMobile: true
            });     
         }
       });   
      }
      });
   
     $(document).on("change","#filter_proposal",function()
     {     
      id=$(this).val();
      if(id=='month'){      
      $.ajax({
       type: "POST",
       url: "<?php echo base_url(); ?>Filter/proposal_filter",
       data:  { 'id':id }, 
       beforeSend: function() {
         $(".LoadingImage").show();
       },
       success: function(data){   
         $(".filter_proposal").html('');
         $(".filter_proposal").append(data);   
         $(".LoadingImage").hide();
         $(".prodiv12").mintScrollbar({
            onChange: null,
            axis: "auto",
            wheelSpeed: 120,
            disableOnMobile: true
            });     
       }
       });   
      }else{
      $.ajax({
       type: "POST",
       url: "<?php echo base_url(); ?>Filter/proposal_filter_year",
       data:  { 'id':id }, 
       beforeSend: function() {
         $(".LoadingImage").show();
       },
       success: function(data){   
         $(".filter_proposal").html('');
          $(".filter_proposal").append(data);   
          $(".LoadingImage").hide();
          $(".prodiv12").mintScrollbar({
            onChange: null,
            axis: "auto",
            wheelSpeed: 120,
            disableOnMobile: true
            });     
       }
       });
      
      }
      });
   
   
   $(document).on("change","#filter_leads",function()
   {
    
      id=$(this).val();
      if(id=='month'){      
      $.ajax({
       type: "POST",
       url: "<?php echo base_url(); ?>Filter/leads_filter",
       data:  { 'id':id }, 
       beforeSend: function() {
         $(".LoadingImage").show();
       },
       success: function(data){    
         $(".filter_leads").html('');
         $(".filter_leads").append(data);   
         $(".LoadingImage").hide();
         $(".prodiv12").mintScrollbar({
            onChange: null,
            axis: "auto",
            wheelSpeed: 120,
            disableOnMobile: true
            });     
       }
       });   
      }else{
      $.ajax({
       type: "POST",
       url: "<?php echo base_url(); ?>Filter/leads_filter_year",
       data:  { 'id':id }, 
       beforeSend: function() {
         $(".LoadingImage").show();
       },
       success: function(data){
       //  console.log(data);   
         $(".filter_leads").html('');
          $(".filter_leads").append(data);   
          $(".LoadingImage").hide();
          $(".prodiv12").mintScrollbar({
            onChange: null,
            axis: "auto",
            wheelSpeed: 120,
            disableOnMobile: true
            });
        
       }
       });
      
      }
      });

      $(document).on("change","#filter_service",function()
      {
        id=$(this).val();
      if(id=='month'){      
      $.ajax({
       type: "POST",
       url: "<?php echo base_url(); ?>Filter/service_filter",
       data:  { 'id':id }, 
       beforeSend: function() {
         $(".LoadingImage").show();
       },
       success: function(data){
         console.log(data);   
         $(".filter_service").html('');
         $(".filter_service").append(data);   
         $(".LoadingImage").hide();
         $(".prodiv12").mintScrollbar({
            onChange: null,
            axis: "auto",
            wheelSpeed: 120,
            disableOnMobile: true
            });
        
       }
       });
      
      }else{
      $.ajax({
       type: "POST",
       url: "<?php echo base_url(); ?>Filter/service_filter_year",
       data:  { 'id':id }, 
       beforeSend: function() {
         $(".LoadingImage").show();
       },
       success: function(data){
        console.log(data);   
         $(".filter_service").html('');
          $(".filter_service").append(data);   
          $(".LoadingImage").hide();
          $(".prodiv12").mintScrollbar({
            onChange: null,
            axis: "auto",
            wheelSpeed: 120,
            disableOnMobile: true
            });
        
       }
       });
      
      }
      });


      $(document).on("change","#filter_deadline",function()
      {  
      id=$(this).val();
      if(id=='month'){
      
      $.ajax({
       type: "POST",
       url: "<?php echo base_url(); ?>Filter/deadline_filter",
       data:  { 'id':id }, 
       beforeSend: function() {
         $(".LoadingImage").show();
       },
       success: function(data){
        // console.log(data);   
         $(".filter_deadline").html('');
         $(".filter_deadline").append(data);   
         $(".LoadingImage").hide();
         $(".prodiv12").mintScrollbar({
            onChange: null,
            axis: "auto",
            wheelSpeed: 120,
            disableOnMobile: true
            });
        
       }
       });
      
      }else{
      $.ajax({
       type: "POST",
       url: "<?php echo base_url(); ?>Filter/deadline_filter_year",
       data:  { 'id':id }, 
       beforeSend: function() {
         $(".LoadingImage").show();
       },
       success: function(data){
       //  console.log(data);   
         $(".filter_deadline").html('');
          $(".filter_deadline").append(data);   
          $(".LoadingImage").hide();
          $(".prodiv12").mintScrollbar({
            onChange: null,
            axis: "auto",
            wheelSpeed: 120,
            disableOnMobile: true
            });
        
       }
       });
      
      }
      });
   
      $(document).on("change","#filter_invoice",function()
      { 

         id=$(this).val();
      if(id=='month'){     
      $.ajax({
       type: "POST",
       url: "<?php echo base_url(); ?>Filter/invoice_filter",
       data:  { 'id':id }, 
       beforeSend: function() {
         $(".LoadingImage").show();
       },
       success: function(data){    
         $(".fileters_invoice").html('');
         $(".fileters_invoice").append(data);   
         $(".LoadingImage").hide();
         $(".prodiv12").mintScrollbar({
            onChange: null,
            axis: "auto",
            wheelSpeed: 120,
            disableOnMobile: true
            });    
       }
       });   
      }else{
      $.ajax({
       type: "POST",
       url: "<?php echo base_url(); ?>Filter/invoice_filter_year",
       data:  { 'id':id }, 
       beforeSend: function() {
         $(".LoadingImage").show();
       },
       success: function(data){    
         $(".fileters_invoice").html('');
          $(".fileters_invoice").append(data);   
          $(".LoadingImage").hide();
          $(".prodiv12").mintScrollbar({
            onChange: null,
            axis: "auto",
            wheelSpeed: 120,
            disableOnMobile: true
            });    
       }
       });
      
      }
      });
          
   
   
     function exportexcel()
   {
       var url="<?php echo base_url(); ?>Reports/excel_download";   
       window.location = url;  
   }
   
     function exportexcel_sess()
   {
       var url="<?php echo base_url(); ?>Reports/excel_download_sess";   
       window.location = url;  
   }
   
   function exporttaskexcel(){
       var url="<?php echo base_url(); ?>Reports/exceltask_download";   
       window.location = url; 
   }
   
   function exporttaskexcel_sess(){
       var url="<?php echo base_url(); ?>Reports/exceltask_download_sess";   
       window.location = url; 
   }
   
   function exportproposalexcel(){
       var url="<?php echo base_url(); ?>Report/excelproposal_download";   
       window.location = url; 
   }
   
   function exportproposalexcel_sess(){
       var url="<?php echo base_url(); ?>Report/excelproposal_download_sess";   
       window.location = url; 
   }
   
   function exportleadsexcel(){
       var url="<?php echo base_url(); ?>Report/excelleads_download";   
       window.location = url; 
   }
   
   function exportleadsexcel_sess(){
       var url="<?php echo base_url(); ?>Report/excelleads_download_sess";   
       window.location = url; 
   }
   
   function service_exportexcel(){
       var url="<?php echo base_url(); ?>Report/excelservice_download";   
       window.location = url; 
   }
   
   function service_exportexcel1(){
       var url="<?php echo base_url(); ?>Report/excelservice_download_sess";   
       window.location = url; 
   }
   
   function deadline_exportexcel(){
      var url="<?php echo base_url(); ?>Report/exceldeadline_download";   
       window.location = url; 
   }
   
   function deadline_exportexcel_sess(){
      var url="<?php echo base_url(); ?>Reports/exceldeadline_download_sess";   
       window.location = url; 
   }
   
   function exportexcel_summary(){
      var url="<?php echo base_url(); ?>Filter/excelsummary_download";   
       window.location = url; 
   }
   function exportexcel_summary_sess(){
      var url="<?php echo base_url(); ?>Filter/excelsummary_download_sess";   
       window.location = url; 
   }
   
   function invoice_exportexcel(){
      var url="<?php echo base_url(); ?>Filter/excelinvoice_download";   
       window.location = url; 
   }
   
   function invoice_exportexcel_sess(){
      var url="<?php echo base_url(); ?>Filter/excelinvoice_download_sess";   
       window.location = url; 
   }
   
   
   $("#invoice_customisation").click(function(){
    var fromdate=$("#invoice_from_date").val();
    var todate=$("#invoice_to_date").val();
    var status=$('#invoice_status').val();
    var formData={'fromdate':fromdate,'todate':todate,'status':status}
    $.ajax({
         type: "POST",
         url: "<?php echo base_url();?>Filter/invoice_customization",         
         data: formData,
         beforeSend: function() {
           $(".LoadingImage").show();
         },
         success: function(data) {      
             $(".LoadingImage").hide(); 
             if(data!='1'){         
               $(".fileters_invoice1").html('');
               $(".fileters_invoice1").append(data); 
             }     
               $("#invoice_customization").modal('hide');  
               $("#invoice_download_sess").modal({show:true, backdrop: 'static', keyboard: false});
         }
       });
   });
  
   
</script>


<script type="text/javascript">
   $(document).ready(function () {
     
     $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var ev = document.createEvent('Event');
    ev.initEvent('resize', true, true);
    window.dispatchEvent(ev);
  });    
   
   
   });

   $(document).on('click','.nav-link',function()
   {
       var div = $(this).attr('data-target');

       $(div).find('g').each(function()
       {
            if(typeof($(this).attr('column-id')) != 'undefined')
            { 
               if(typeof($(this).find('text').html()) == 'undefined')
               {                   
                  $(this).find('circle').css('background-color',$(this).find('circle').attr('fill'));
                  $(this).find('circle').after('<g><text text-anchor="start" x="101" y="230.35" font-family="Arial" font-size="11" stroke="none" stroke-width="0" fill="#222222">'+$(this).attr('column-id')+'</text></g>');

                  var heading = $(this).parents('svg').next('div').find('table thead th:first-child').html(); 

                  if(heading == 'Task')
                  {
                     $('#task_tab').append('<div class="artificial">'+$(this).html()+'</div>');
                  }
                  else if(heading == 'Legal Form')
                  {
                     $('#Tabs_client').append('<div class="artificial">'+$(this).html()+'</div>');
                  }
                  else if(heading == 'user status')
                  {
                     $('#Tabs_Users').append('<div class="artificial">'+$(this).html()+'</div>');
                  }
                  else if(heading == 'Tasks')
                  {
                     $('#tabs_priority').append('<div class="artificial">'+$(this).html()+'</div>');
                  }
                  else if(heading == 'Service')
                  {
                     $('#service_tab').append('<div class="artificial">'+$(this).html()+'</div>');
                  }
                  else if(heading == 'Deadlines')
                  {
                     $('#Tabs_deadline').append('<div class="artificial">'+$(this).html()+'</div>');
                  }
                  else if(heading == 'Invoice')
                  {
                     $('#Tabs_invoice').append('<div class="artificial">'+$(this).html()+'</div>');
                  }

                  $(this).html('');
                  $(this).removeAttr('column-id');
               }
            }
       });
   });

</script>