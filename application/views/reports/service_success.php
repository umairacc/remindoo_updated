<div id="piechart_service1" style="width: 100%;"></div>
<script type="text/javascript">
     google.charts.load('current', {'packages':['corechart']});
     google.charts.setOnLoadCallback(drawChart2);
  
     function drawChart2() {
  
       var data = google.visualization.arrayToDataTable([
         ['Service', 'Count'], 
         <?php
         $status=$_SESSION['service'];
          if($status=='conf_statement'){    ?>     
          ['Confirmation Statement', <?php echo $conf_statement['service_count']; ?> ],
           ['Others', <?php echo $others['service_count']; ?> ],

          <?php } if($status=='accounts'){?>
          ['Accounts', <?php echo $accounts['service_count']; ?> ],
          ['Others', <?php echo $others['service_count']; ?> ],
           <?php }  if($status=='company_tax_return'){?>
          ['Company Tax Return', <?php echo $company_tax_return['service_count']; ?> ],
          ['Others', <?php echo $others['service_count']; ?> ],          
           <?php } if($status=='personal_tax_return'){?>
          ['Personal Tax Return', <?php echo $personal_tax_return['service_count']; ?> ],
          ['Others', <?php echo $others['service_count']; ?> ],          
           <?php }  if($status=='vat'){?>
          ['VAT Returns', <?php echo $vat['service_count']; ?> ],
          ['Others', <?php echo $others['service_count']; ?> ],
           <?php } if($status=='payroll'){?>
          ['Payroll', <?php echo $payroll['service_count']; ?> ],
          ['Others', <?php echo $others['service_count']; ?> ],
           <?php } if($status=='workplace'){?>
          ['WorkPlace Pension - AE', <?php echo $workplace['service_count']; ?> ],
          ['Others', <?php echo $others['service_count']; ?> ],
           <?php }  if($status=='cis'){?>          
          ['CIS - Contractor', <?php echo $cis['service_count']; ?> ],
          ['Others', <?php echo $others['service_count']; ?> ],
           <?php } if($status=='cissub'){?>          
          ['CIS - Sub Contractor', <?php echo $cissub['service_count']; ?> ],
          ['Others', <?php echo $others['service_count']; ?> ],
           <?php }  if($status=='p11d'){?>          
          ['P11D', <?php echo $p11d['service_count']; ?> ],
          ['Others', <?php echo $others['service_count']; ?> ],
           <?php } if($status=='management'){?>   
          ['Management Accounts', <?php echo $management['service_count']; ?> ],
          ['Others', <?php echo $others['service_count']; ?> ],
           <?php } if($status=='bookkeep'){?>   
          ['Bookkeeping', <?php echo $bookkeep['service_count']; ?> ],
           ['Others', <?php echo $others['service_count']; ?> ],
           <?php } if($status=='investgate'){?>   
          ['Investigation Insurance', <?php echo $investgate['service_count']; ?> ],
          ['Others', <?php echo $others['service_count']; ?> ],
           <?php }  if($status=='registered'){?>   
          ['Registered Address', <?php echo $registered['service_count']; ?> ],
          ['Others', <?php echo $others['service_count']; ?> ],
           <?php }  if($status=='taxadvice'){?>   
          ['Tax Advice', <?php echo $taxadvice['service_count']; ?> ],
          ['Others', <?php echo $others['service_count']; ?> ],
           <?php }  if($status=='taxinvest'){?>   
          ['Tax Investigation', <?php echo $taxinvest['service_count']; ?> ],
          ['Others', <?php echo $others['service_count']; ?> ],
           <?php } ?>             
        ]);      
  
       var options = {
            chartArea: {left: 0, width: "100%"},
        height: 250,
       legend: { position: 'bottom', show: true, container: $("#large-legend") },
      hAxis : { textStyle : { fontSize: 8} },
      vAxis : { textStyle : { fontSize: 8} },
       //  pieHole: 0.4,
       };
  
    var chart_div = new google.visualization.PieChart(document.getElementById('piechart_service1'));
    var chart = new google.visualization.PieChart(piechart_service1);
    google.visualization.events.addListener(chart, 'ready', function () {
    chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
    //document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '"></a>';
    var image=chart.getImageURI();
       $(document).ready(function(){  
      // alert(image);
                 $.ajax({
                        type: "POST",
                        url: "<?php echo base_url();?>/Report/service_chart_sess",         
                        data: {service_image_sess: image},
                        success: 
                             function(data){
                    
                             }
                         });
                  });
  });
    chart.draw(data, options);
  }
  </script>