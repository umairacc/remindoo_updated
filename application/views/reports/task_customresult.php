	
	<div class="sample_section">
	<button type="button" class="print_button btn btn-primary" id="print_report">Print</button>
	<div class="export_option">
	<select id="exported_result">
	<option value="">Select</option>
	<option value="pdf">Pdf</option>
	<option value="excel">Excel</option>
	<option value="csv">CSV</option>
	</select>
	</div>
	</div>
	<table id="example" class="display nowrap" style="width:100%">
	<thead>
	<tr>
	<th>Task Name</th>
	<th>Start Date</th>
	<th>End Date</th>
	<th>Priority</th>
	<th>Status</th>                                      
	</tr>
	</thead>

<tfoot>
	<tr class="table-header">
	<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
	<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
	<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
	<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
	<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>                                      
	</tr>
	</tfoot>		
	<tbody>
<?php
if(empty($records)){}else{
foreach($records as $record){ ?>
	<tr>
	<td><?php echo $record['subject']; ?></td>
	<td><?php echo date('Y-m-d',strtotime($record['start_date'])); ?></td>
	<td><?php echo date('Y-m-d',strtotime($record['end_date'])); ?></td>
	<td><?php echo $record['priority']; ?></td>
	<td><?php echo $record['task_status']; ?></td>	
	</tr>
<?php }}  ?>

	</tbody>

	</table>
