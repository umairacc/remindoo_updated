<div id="piechart_deadline1" style="width: 100%;"></div>

<script type="text/javascript">

     google.charts.load('current', {'packages':['corechart']});
     google.charts.setOnLoadCallback(drawChart2);
  
     function drawChart2() {
  
       var data = google.visualization.arrayToDataTable([
         ['Service', 'Count'],   
         <?php
         $status=$_SESSION['deadline_type'];
          if($status=='conf_statement'){    ?>      
          ['Confirmation Statement', <?php echo $de_conf_statement['dead_count']; ?> ],
            ['Others', <?php echo $de_others['dead_count']; ?> ],
          <?php } ?>
           <?php  if($status=='accounts'){?>
          ['Accounts', <?php echo $de_accounts['dead_count']; ?> ],
           ['Others', <?php echo $de_others['dead_count']; ?> ],
            <?php } if($status=='company_tax_return'){ ?>
          ['Company Tax Return', <?php echo $de_company_tax_return['dead_count']; ?> ], 
           ['Others', <?php echo $de_others['dead_count']; ?> ],
           <?php } if($status=='personal_tax_return'){ ?>
          ['Personal Tax Return', <?php echo $de_personal_tax_return['dead_count']; ?> ],
           ['Others', <?php echo $de_others['dead_count']; ?> ],
            <?php } if($status=='vat'){?>
          ['VAT Returns', <?php echo $de_vat['dead_count']; ?> ],
           ['Others', <?php echo $de_others['dead_count']; ?> ],
            <?php } if($status=='payroll'){?>
          ['Payroll', <?php echo $de_payroll['dead_count']; ?> ],
           ['Others', <?php echo $de_others['dead_count']; ?> ],
            <?php } if($status=='workplace'){ ?>
          ['WorkPlace Pension - AE', <?php echo $de_workplace['dead_count']; ?> ],
           ['Others', <?php echo $de_others['dead_count']; ?> ],
            <?php } if($status=='cis'){?>
          ['CIS - Contractor', <?php echo $de_cis['dead_count']; ?> ],
           ['Others', <?php echo $de_others['dead_count']; ?> ],
            <?php } if($status=='cissub'){?>
          ['CIS - Sub Contractor', <?php echo $de_cissub['dead_count']; ?> ],
           ['Others', <?php echo $de_others['dead_count']; ?> ],
            <?php } if($status=='p11d'){?>
          ['P11D', <?php echo $de_p11d['dead_count']; ?> ],
           ['Others', <?php echo $de_others['dead_count']; ?> ],
            <?php } if($status=='management'){?>
          ['Management Accounts', <?php echo $de_management['dead_count']; ?> ],
           ['Others', <?php echo $de_others['dead_count']; ?> ],
            <?php } if($status=='bookkeep'){?>
          ['Bookkeeping', <?php echo $de_bookkeep['dead_count']; ?> ],
           ['Others', <?php echo $de_others['dead_count']; ?> ],
            <?php } if($status=='investgate'){?>
          ['Investigation Insurance', <?php echo $de_investgate['dead_count']; ?> ],
           ['Others', <?php echo $de_others['dead_count']; ?> ],
            <?php } if($status=='registered'){?>
          ['Registered Address', <?php echo $de_registered['dead_count']; ?> ],
           ['Others', <?php echo $de_others['dead_count']; ?> ],
            <?php } if($status=='taxadvice'){?>
          ['Tax Advice', <?php echo $de_taxadvice['dead_count']; ?> ],
           ['Others', <?php echo $de_others['dead_count']; ?> ],
            <?php } if($status=='taxinvest'){?>
          ['Tax Investigation', <?php echo $de_taxinvest['dead_count']; ?> ],
           ['Others', <?php echo $de_others['dead_count']; ?> ],
            <?php } ?>         
        ]);      
  
       var options = {
             chartArea: {left: 0, width: "100%"},
        height: 250,
       legend: { position: 'bottom', show: true, container: $("#large-legend") },
      hAxis : { textStyle : { fontSize: 8} },
      vAxis : { textStyle : { fontSize: 8} },
       //  pieHole: 0.4,
       };
  
    var chart_div = new google.visualization.PieChart(document.getElementById('piechart_deadline1'));
    var chart = new google.visualization.PieChart(piechart_deadline1);
    google.visualization.events.addListener(chart, 'ready', function () {
    chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
    //document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '"></a>';
    var image=chart.getImageURI();
       $(document).ready(function(){  
      // alert('image');
                 $.ajax({
                        type: "POST",
                        url: "<?php echo base_url();?>/Report/deadline_chart_sess",         
                        data: {deadline_image_sess: image},
                        success: 
                             function(data){
                   // alert('ok');
                             }
                         });
                  });
  });
    chart.draw(data, options);
  }
  </script>
