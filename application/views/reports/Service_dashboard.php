<?php $this->load->view('includes/header');?>
<style>
   /* bootstrap hack: fix content width inside hidden tabs */
.tab-content > .tab-pane,
.pill-content > .pill-pane {
display: block;     /* undo display:none          */
height: 0;          /* height:0 is also invisible */ 
overflow-y: hidden; /* no-overflow                */
}
.tab-content > .active,
.pill-content > .active {
height: auto;       /* let the content decide it  */
} /* bootstrap hack end */
.box-container {
   height: 200px;
}
 tfoot {
    display: table-header-group;
}

#container1.box-item{
   width: 100%; 
   /*font-size:10px;*/
   z-index: 1000
}

.box-item {
   /*width: 100%; */
   font-size:10px;
   z-index: 1000
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #fff;
    min-width: 86px;
    overflow: auto;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    left: -92px;
    width: 150px;
}

/*.dt-buttons{
      display: none;
   }*/
.run-section{
   position: relative;
}
.running{
   position: absolute;
   display:   block;
   height: 100%;
   top:0;
}


.wrong{
  border: 1px solid red;
}


div#container112 {
    height: 358px;
    position: relative;
    display: block;
    margin-top: -235px;
    z-index: 99;
}
</style>
<?php $services = $this->Common_mdl->getServicelist();  ?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/mintScrollbar.css">
<div class="pcoded-content card-removes">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card">
                        <!-- admin start-->
                        <div class="client_section user-dashboard-section1 floating_set">
                           <!-- all-clients -->
                           <div class="all_user-section floating_set">
                              <div class="deadline-crm1 floating_set f4reports rdashboard">
                                 <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                                    <li class="nav-item">
                                       <a class="nav-link active" data-toggle="tab" data-target="#reports-tab" href="">Reports</a>
                                       <div class="slide"></div>
                                    </li>
                                 </ul>
                                 <div class="run-btn f-right">
                                 <button data-toggle="modal" data-target="#myModal" class="btn btn-danger fr-sch" >Schedule</button>
                                       <!-- <button class="btn btn-success" id="run_details">Run</button>
                                       <butto class="btn btn-danger">&times;</butto> -->
                                 </div>
                              </div>
                              <div class="for-reportdash">
                                 <div class="left-one">
                                    <div class="pcoded-search">
                                       <span class="searchbar-toggle"> </span>
                                       <div class="pcoded-search-box ">
                                          <input type="text" placeholder="Search" class="search-criteria" id="search-criteria">
                                          <span class="search-icon"><i class="ti-search" aria-hidden="true"></i></span>
                                       </div>
                                          <div id="container1" class="panel-body box-container">
                                         
                                             <?php 

                                             foreach($services as $key => $value)
                                             {
                                                
                                             ?>

                                             <div itemid="<?php echo $value['services_subnames']; ?>" class="btn btn-default box-item box_section"><?php echo $value['service_name']; ?><span class="remove" data-role="remove">x</span></div>

                                      <?php  } ?>
                                          </div>


                                          <div id="container112" class=""></div>
                                    </div>                                           
                                 </div>
                                 <div class="right-one">
                             <!--     <div class="run-section"> -->
                                   <div class="filter-data">
                                          <div class="filter-head"><h4>DROP FILTERS HERE
                                          </h4><button class="btn btn-danger f-right" id="clear_container">clear</button>
                                          <div id="container2" class="panel-body box-container">
                                         </div>
                                         <div id="container212" class="">
                                             
                                          </div>
                                          </div>                                   
                                         
                                    </div>
                                   <!--   <div class="running"> </div> -->
                                 <!--    </div> -->
                                   
                                   

                                    <div id="datatable_results" >
                                          
  
  <div class="sample_section">
<div class="export_option">
 <div class="btn-group e-dash sve-action1 frpropos">

  <button type="button" class="btn btn-default dropdown-toggle emp-dash" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select
 
  </button>
  <ul class="dropdown-menu" >
    <li id="buttons"></li>
  </ul>
</div>
</div>
  </div>
  <table id="example" class="display nowrap" style="width:100%">
  <thead>
  <tr>
  <th>S.no<div class="sortMask"></div></th>    
  <th>Company Name<div class="sortMask"></div></th>
  <th>Services/Service Due Dates<div class="sortMask"></div></th>                                  
  </tr>
  </thead>

  <tfoot>
<tr class="table-header"> 
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
</tr>
</tfoot>
  <tbody>
<?php
if(empty($records)){}else{

  $s = 1;
$service_array=array();
$servicedue_date_array=array();
foreach($records as $service){ 

// echo '<pre>';
// print_r($service_array);
// print_r($servicedue_date_array);
// echo '</pre>';
(isset(json_decode($service['conf_statement'])->tab) && $service['conf_statement'] != '') ? $jsnvat =  json_decode($service['conf_statement'])->tab : $jsnvat = '';
  ($jsnvat!='') ? $conf_statement = "On" : $conf_statement = "Off";
//accounts
 (isset(json_decode($service['accounts'])->tab) && $service['accounts'] != '') ? $jsnvat =  json_decode($service['accounts'])->tab : $jsnvat = '';
  ($jsnvat!='') ? $accounts = "On" : $accounts = "Off";
//company_tax_return
 (isset(json_decode($service['company_tax_return'])->tab) && $service['company_tax_return'] != '') ? $jsnvat =  json_decode($service['company_tax_return'])->tab : $jsnvat = '';
  ($jsnvat!='') ? $company_tax_return = "On" : $company_tax_return = "Off";
//personal_tax_return
 (isset(json_decode($service['personal_tax_return'])->tab) && $service['personal_tax_return'] != '') ? $jsnvat =  json_decode($service['personal_tax_return'])->tab : $jsnvat = '';
  ($jsnvat!='') ? $personal_tax_return = "On" : $personal_tax_return = "Off";
//payroll
 (isset(json_decode($service['payroll'])->tab) && $service['payroll'] != '') ? $jsnvat =  json_decode($service['payroll'])->tab : $jsnvat = '';
  ($jsnvat!='') ? $payroll = "On" : $payroll = "Off";
//workplace
 (isset(json_decode($service['workplace'])->tab) && $service['workplace'] != '') ? $jsnvat =  json_decode($service['workplace'])->tab : $jsnvat = '';
  ($jsnvat!='') ? $workplace = "On" : $workplace = "Off";
//vat
 (isset(json_decode($service['vat'])->tab) && $service['vat'] != '') ? $jsnvat =  json_decode($service['vat'])->tab : $jsnvat = '';
  ($jsnvat!='') ? $vat = "On" : $vat = "Off";
//cis
 (isset(json_decode($service['cis'])->tab) && $service['cis'] != '') ? $jsnvat =  json_decode($service['cis'])->tab : $jsnvat = '';
  ($jsnvat!='') ? $cis = "On" : $cis = "Off";
//cissub
 (isset(json_decode($service['cissub'])->tab) && $service['cissub'] != '') ? $jsnvat =  json_decode($service['cissub'])->tab : $jsnvat = '';
  ($jsnvat!='') ? $cissub = "On" : $cissub = "Off";
//p11d
 (isset(json_decode($service['p11d'])->tab) && $service['p11d'] != '') ? $jsnvat =  json_decode($service['p11d'])->tab : $jsnvat = '';
  ($jsnvat!='') ? $p11d = "On" : $p11d = "Off";
//bookkeep
 (isset(json_decode($service['bookkeep'])->tab) && $service['bookkeep'] != '') ? $jsnvat =  json_decode($service['bookkeep'])->tab : $jsnvat = '';
  ($jsnvat!='') ? $bookkeep = "On" : $bookkeep = "Off";
//management
 (isset(json_decode($service['management'])->tab) && $service['management'] != '') ? $jsnvat =  json_decode($service['management'])->tab : $jsnvat = '';
  ($jsnvat!='') ? $management = "On" : $management = "Off";
//investgate
 (isset(json_decode($service['investgate'])->tab) && $service['investgate'] != '') ? $jsnvat =  json_decode($service['investgate'])->tab : $jsnvat = '';
  ($jsnvat!='') ? $investgate = "On" : $investgate = "Off";
//registered
 (isset(json_decode($service['registered'])->tab) && $service['registered'] != '') ? $jsnvat =  json_decode($service['registered'])->tab : $jsnvat = '';
  ($jsnvat!='') ? $registered = "On" : $registered = "Off";
//taxadvice
 (isset(json_decode($service['taxadvice'])->tab) && $service['taxadvice'] != '') ? $jsnvat =  json_decode($service['taxadvice'])->tab : $jsnvat = '';
  ($jsnvat!='') ? $taxadvice = "On" : $taxadvice = "Off";
//taxinvest
  (isset(json_decode($service['taxinvest'])->tab) && $service['taxinvest'] != '') ? $jsnvat =  json_decode($service['taxinvest'])->tab : $jsnvat = '';
  ($jsnvat!='') ? $taxinvest = "On" : $taxinvest = "Off"; ?>

  
  <?php 
  // $status=explode(',',$_SESSION['service']);
  // for($i=0;$i<count($status);$i++){
  //   if($status[$i]=='conf_statement'){
    if($conf_statement=='On'){ 
      array_push($service_array,'Confirmation Statements');
      $confirmation_start_date=$service['crm_confirmation_statement_date'];
      $confirmation_due_date=$service['crm_confirmation_statement_due_date'];
        if($confirmation_due_date!=''){
          $confirmation_due_date=$service['crm_confirmation_statement_due_date'];
        }else if($confirmation_start_date!=''){
          $confirmation_due_date=$service['crm_confirmation_statement_date'];
        }else{ $confirmation_due_date = '-'; }
      array_push($servicedue_date_array,$confirmation_due_date);
    } 
  //}
  //
    if($accounts=='On'){ 
      array_push($service_array,'Accounts');
      $accounts_due_date=$service['crm_ch_accounts_next_due'];
        if($accounts_due_date!=''){
          $accounts_due_date=$service['crm_ch_accounts_next_due'];
        }else{
          $accounts_due_date='-';
        }
      array_push($servicedue_date_array,$accounts_due_date);
    } 
 // }
  //if($status[$i]=='company_tax_return'){
    if($company_tax_return=='On'){ 
      array_push($service_array,'Company Tax Return');
      $company_tax_due_date=$service['crm_accounts_tax_date_hmrc']; 
        if($company_tax_due_date!=''){
          $company_tax_due_date=$service['crm_accounts_tax_date_hmrc']; 
        }else{
          $company_tax_due_date='-'; 
        }
      array_push($servicedue_date_array,$company_tax_due_date);
    } 
 // }
  //if($status[$i]=='personal_tax_return'){
    if($personal_tax_return=='On'){ 
    array_push($service_array,'Personal Tax Return');
    $personal_tax_due_date=$service['crm_personal_due_date_return']; 
      if($personal_tax_due_date!=''){
        $personal_tax_due_date=$service['crm_personal_due_date_return']; 
      }else{
        $personal_tax_due_date='-'; 
      }
    array_push($servicedue_date_array,$personal_tax_due_date);
    } 
 // }
 // if($status[$i]=='vat'){
    if($vat=='On'){ 
    array_push($service_array,'VAT Returns');
    $vat_due_date=$service['crm_vat_due_date']; 
      if($vat_due_date!=''){
        $vat_due_date=$service['crm_vat_due_date']; 
      }else{
        $vat_due_date='-';  
      }
      array_push($servicedue_date_array,$vat_due_date);
    } 
 // }
 // if($status[$i]=='payroll'){
    if($payroll=='On'){ 
    array_push($service_array,'Payroll');
    $payroll_due_date=$service['crm_rti_deadline']; 
      if($payroll_due_date!=''){
        $payroll_due_date=$service['crm_rti_deadline']; 
      }else{
        $payroll_due_date='-';  
      }
      array_push($servicedue_date_array,$payroll_due_date);
    }
 // }  
 // if($status[$i]=='workplace'){
    if($workplace=='On'){ 
    array_push($service_array,'WorkPlace Pension - AE');
    $work_place_due_date=$service['crm_pension_subm_due_date']; 
      if($work_place_due_date!=''){
        $work_place_due_date=$service['crm_pension_subm_due_date']; 
      }else{
        $work_place_due_date='-';   
      }
      array_push($servicedue_date_array,$work_place_due_date);
    } 
//  }
 // if($status[$i]=='cis'){
    if($cis=='On'){ 
    array_push($service_array,'CIS - Contractor');
    $cis_due_date='-'; 
    array_push($servicedue_date_array,$cis_due_date);
    } 
 // }
 // if($status=='cissub'){
    if($cissub=='On'){ 
    array_push($service_array,'CIS - Sub Contractor');
    $cis_sub_due_date='-'; 
    array_push($servicedue_date_array,$cis_sub_due_date);
    } 
 // }
//  if($status[$i]=='p11d'){
    if($p11d=='On'){ 
    array_push($service_array,'P11D');
    $p11d_due_date=$service['crm_next_p11d_return_due']; 
      if($p11d_due_date!=''){
        $p11d_due_date=$service['crm_next_p11d_return_due']; 
      }else{
        $p11d_due_date='-';   
      }
      array_push($servicedue_date_array,$p11d_due_date);
    } 
 // }
  //if($status[$i]=='management'){
    if($management=='On'){ 
    array_push($service_array,'Management Accounts');
    $management_due_date=$service['crm_next_manage_acc_date']; 
      if($management_due_date!=''){
        $management_due_date=$service['crm_next_manage_acc_date']; 
      }else{
        $management_due_date='-';   
      }
      array_push($servicedue_date_array,$management_due_date);
    } 
 // }
 // if($status[$i]=='bookkeep'){
    if($bookkeep=='On'){ 
    array_push($service_array,'Bookkeeping');
    $bookkeep_due_date=$service['crm_next_booking_date']; 
      if($bookkeep_due_date!=''){
        $bookkeep_due_date=$service['crm_next_booking_date']; 
      }else{
        $bookkeep_due_date='-';   
      }
      array_push($servicedue_date_array,$bookkeep_due_date);
    }
//  }
  //if($status[$i]=='investgate'){ 
    if($investgate=='On'){ 
    array_push($service_array,'Investigation Insurance');
    $investigate_due_date=$service['crm_insurance_renew_date']; 
      if($investigate_due_date!=''){
        $investigate_due_date=$service['crm_insurance_renew_date']; 
      }else{
        $investigate_due_date='-'; 
      }
      array_push($servicedue_date_array,$investigate_due_date);
    } 
 // }
 // if($status[$i]=='registered'){
    if($registered=='On'){ 
    array_push($service_array,'Registered Address');
    $registered_due_date=$service['crm_registered_renew_date']; 
      if($registered_due_date!=''){
        $registered_due_date=$service['crm_registered_renew_date'];
      }else{
        $registered_due_date='-';
      }
      array_push($servicedue_date_array,$registered_due_date);
    } 
  //}
 // if($status[$i]=='taxadvice'){
    if($taxadvice=='On'){ 
    array_push($service_array,'Tax Advice');
    $taxadvice_due_date=$service['crm_investigation_end_date']; 
      if($taxadvice_due_date!=''){
        $taxadvice_due_date=$service['crm_investigation_end_date']; 
      }else{
        $taxadvice_due_date='-';  
      }
      array_push($servicedue_date_array,$taxadvice_due_date);
    } 
 // }
 // if($status[$i]=='taxinvest'){
    if($taxinvest=='On'){ 
    array_push($service_array,'Tax Investigation');
    $taxinvest_due_date=$service['crm_investigation_end_date']; 
      if($taxinvest_due_date!=''){
        $taxinvest_due_date=$service['crm_investigation_end_date']; 
      }else{
        $taxinvest_due_date='-'; 
      }
      array_push($servicedue_date_array,$taxinvest_due_date);
    }

    foreach($services as $key => $value) 
    {
        if($value['id'] > "16")
        {
           (isset(json_decode($service[$value['services_subnames']])->tab) && $service[$value['services_subnames']] != '') ? $tab =  json_decode($service[$value['services_subnames']])->tab : $tab = '';
            ($tab!='') ? $tab_status = "On" : $tab_status = "Off"; 

            if($tab_status=='On')
            { 
              array_push($service_array,$value['service_name']);            
              $due_date = $service['crm_'.$value['services_subnames'].'_statement_date'];
              
                if($due_date!='')
                {
                  array_push($servicedue_date_array,$due_date);
                }
            } 
        }
    }    
 
//}
// echo '<pre>';
// print_r($service_array);
// print_r($servicedue_date_array);
// echo '</pre>';

   ?> 
<tr id="<?php echo $service["id"]; ?>">
<td><?php echo $s;?></td>
<td><?php echo ucfirst($service['crm_company_name']);?></td>      
<td colspan="2">
<?php for($i=0;$i<count($service_array);$i++){ ?>
<?php echo $service_array[$i].'/'.$servicedue_date_array[$i]; ?>
 <?php } ?>
 </td>

</tr>
<?php $s++;//;

  
 $service_array=array(); $servicedue_date_array=array();
 //echo "hai".$s;
  }
 } ?>

  </tbody>

  </table>


                                    </div>




                                 </div>
                              </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

<?php $this->load->view('reports/schedule_report');?>
<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/custom-google-chart.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.mintScrollbar.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.mousewheel.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
<script src="<?php echo base_url();?>assets/js/mock.js"></script>
<script type="text/javascript">
  
  $('.dropdown-sin-4').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
   });

    $('.dropdown-sin-6').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
   });

    $('.dropdown-sin-2').dropdown({
   
     input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
   });
     $('.dropdown-sin-3').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
   });


$(document).ready(function() {

  var example1;
    // $('#example').DataTable( {
    //     dom: 'Bfrtip',
    //    buttons: [{
    //                     extend: 'pdf',
    //                        title: 'Service List',
    //                        filename: 'Service Details'
    //                     }, {
    //                     extend: 'excel',
    //                        extension: '.xls',
    //                        title: 'Service List',
    //                        filename: 'Service Details'
    //                     }, {
    //                     extend: 'csv',
    //                        filename: 'Service Details'
    //                     },{
    //                     extend: 'print',
    //                        title: 'Service Details',
    //                     }]  
    //     // buttons: [
    //     //     'copy', 'csv', 'excel', 'pdf', 'print'
    //     // ]
    // } );


    var check=0;
    var check1=0;
    var numCols = $('#example thead th').length;
        var table10 = $('#example').DataTable({
        // dom: 'Bfrtip',
        // buttons: [{
        // extend: 'pdf',
        // title: 'Service List',
        // filename: 'Service Details'
        // }, {
        // extend: 'excel',
        // extension: '.xls',
        // title: 'Service List',
        // filename: 'Service Details'
        // }, {
        // extend: 'csv',
        // filename: 'Service Details'
        // },{
        // extend: 'print',
        // title: 'Service Details',
        // }],
       initComplete: function () { 
                  var q=0;
                     $('#example tfoot th').find('.filter_check').each( function(){
                     // alert('ok');
                       $(this).attr('id',"all_"+q);
                       q++;
                     });
                  for(i=0;i<numCols;i++){                 
                        var select = $("#all_"+i); 
                      
                      this.api().columns([i]).every( function () {
                        var column = this;
                        column.data().unique().sort().each( function ( d, j ) {    
                          console.log(d);         
                          select.append( '<option value="'+d+'">'+d+'</option>' )
                        });
                      });                   
           
                     $("#all_"+i).formSelect();  
                  }
        }
    });
      for(j=0;j<numCols;j++){  
          $('#all_'+j).on('change', function(){ 
            var result=$(this).attr('id').split('_');      
             var c=result[1];
              var search = [];              
              $.each($('#all_'+c+ ' option:selected'), function(){                
              search.push($(this).val());
              });    
              alert(search) ;
              search = search.join('|');  

              example1.column(c).search(search, true, false).draw();  
          });
       }


 example1 = $('#example').DataTable();
  var buttons = new $.fn.dataTable.Buttons(example1, {

     buttons: [{
       extend: 'pdf',
       title: 'Service List',
       filename: 'Service List',
           
     }, {
       extend: 'excel',
       title: 'Service List',
       filename: 'Service List',
       extension: '.xls',
             
     }, {
       extend: 'csv',
       filename: 'Service List',
       
     },{
       extend: 'print',
       title: 'Service List',
       
     }]
}).container().appendTo($('#buttons')); 

} );


$("#exported_result").change(function(){
var value=$(this).val();
if(value=='pdf'){
$(".buttons-pdf").trigger('click');
}else if(value="excel"){
$(".buttons-excel").trigger('click');
}else if(value="csv"){
$(".buttons-csv ").trigger('click');
}
});


$("#print_report").click(function(){
$(".buttons-print").trigger('click');
});
</script>


<script type="text/javascript">
   $( document ).ready(function() {

$(".prodiv12").mintScrollbar({
onChange: null,
axis: "auto",
wheelSpeed: 120,
disableOnMobile: true
});

   $('.tags').tagsinput({
      allowDuplicates: true
   });
   });


   $(document).ready(function() {

  $('.box-item').draggable({
    cursor: 'move',
    helper: "clone"
  });

  $("#container1").droppable({
    drop: function(event, ui) {
      var itemid = $(event.originalEvent.toElement).attr("itemid");
      $('.box-item').each(function() {
        if ($(this).attr("itemid") === itemid) {
          $(this).appendTo("#container1");
        }
      });
    }
  });

$(".remove").click(function(){  
    $(this).parent('.box-item').addClass('replace');
      $('.replace').each(function() {    
          $(this).appendTo("#container1");      
      });
});

$("#clear_container").click(function(){

   $(".insearch_section .remove").trigger('click');

});




  $("#container2").droppable({

    drop: function(event, ui) {
      var itemid = $(event.originalEvent.toElement).attr("itemid");
      $('.box-item').each(function() {
        if ($(this).attr("itemid") === itemid) {
         $(this).addClass('insearch_section');
          $(this).appendTo("#container2");      
        }
      });
    }
  });


//   $("#run_details").click(function(){
//    $("#container2 .box-item").each(function() {
// console.log($(this).attr("itemid"));
//    });
//   });


  /*$("#run_details").click(function(){

   var id=1;
 
         $.ajax({
            url: '<?php echo base_url(); ?>Reports/chart',
            type : 'POST',
            data : {'id':id},                    
            beforeSend: function() {
            $(".LoadingImage").show();
            },
            success: function(data) {
               $(".LoadingImage").hide();   
               $("#datatable_results").html('');
               $("#datatable_results").append(data);
               $("#datatable_results").show();

               $(document).ready(function() {
                  // $('#example').DataTable( {
                  //    dom: 'Bfrtip',
                  //    buttons: [{
                  //       extend: 'pdf',
                  //          title: 'Client List',
                  //          filename: 'Client Details'
                  //       }, {
                  //       extend: 'excel',
                  //          title: 'Client List',
                  //          filename: 'Client Details'
                  //       }, {
                  //       extend: 'csv',
                  //          filename: 'Client Details'
                  //       },{
                  //       extend: 'print',
                  //          title: 'Client Details',
                  //       }]           
                  // });
               });

               // $("#exported_result").change(function(){
               // var value=$(this).val();        
               //    if(value=='pdf'){
               //        $(".buttons-pdf").trigger('click');
               //    }else if(value=="excel"){
               //        $(".buttons-excel").trigger('click');
               //    }else if(value=="csv"){             
               //       $(".buttons-csv").trigger('click');
               //    }
               // });

               // $("#print_report").click(function(){
               //     $(".buttons-print").trigger('click');
               // });                                        
            }
         }); 
       });*/
 



});




$('#search-criteria').keyup(function(){
    $('.box_section').hide();
    var txt = $('#search-criteria').val();
    $('.box_section').each(function(){
       if($(this).text().toUpperCase().indexOf(txt.toUpperCase()) != -1){
           $(this).show();
       }
    });
});


function controll(){
 //console.log('eeeee');
      $('.box-item').each(function() {
       // if ($(this).attr("itemid") === itemid) {
         $(this).addClass('insearch_section');
          $(this).clone().appendTo("#container2");      
       // }
      });


      //  $('.box-item').each(function() {
      // //  if ($(this).attr("itemid") === itemid) {
      //     $(this).appendTo("#container1");
      // //  }
      // });
   
}


  $(document).ready(function () { 

   $(".filter-data").find('*').prop('disabled', true);

      controll();

$("#container2").find('*').prop('disabled', true);

//$(".remove").css('display','none');
  });

</script>



<script type="text/javascript">
   $(document).ready(function () {     
     $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var ev = document.createEvent('Event');
    ev.initEvent('resize', true, true);
    window.dispatchEvent(ev);
  }); 
   });
</script>