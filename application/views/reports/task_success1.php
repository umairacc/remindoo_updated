<div id="pie_chart" style="width: 100%;"></div>

<div class="prodiv12">
  <?php
   if($notstarted_tasks['task_count']!='0'){ ?>
<div class="pro-color">
   <span class="procount"><?php echo $notstarted_tasks['task_count']; ?></span>
   <div class="fordis">
      <div class="progress progress-xs">
         <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $notstarted_tasks['task_count'].'%'; ?>;" aria-valuenow="<?php echo $notstarted_tasks['task_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_tasks['task_count']; ?>"></div>
      </div>
   </div>
</div>
<?php } ?>    
<?php
   if($completed_tasks['task_count']!='0'){ ?>
<div class="pro-color">
   <span class="procount"><?php echo $completed_tasks['task_count']; ?></span>
   <div class="fordis">
      <div class="progress progress-xs">
         <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $completed_tasks['task_count'].'%'; ?>;" aria-valuenow="<?php echo $completed_tasks['task_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_tasks['task_count']; ?>"></div>
      </div>
   </div>
</div>
<?php } ?> 
<?php
   if($inprogress_tasks['task_count']!='0'){ ?>
<div class="pro-color">
   <span class="procount"><?php echo $inprogress_tasks['task_count']; ?></span>
   <div class="fordis">
      <div class="progress progress-xs">
         <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $inprogress_tasks['task_count'].'%'; ?>;" aria-valuenow="<?php echo $inprogress_tasks['task_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_tasks['task_count']; ?>"></div>
      </div>
   </div>
</div>
<?php } ?> 
<?php
   if($awaiting_tasks['task_count']!='0'){ ?>
<div class="pro-color">
   <span class="procount"><?php echo $awaiting_tasks['task_count']; ?></span>
   <div class="fordis">
      <div class="progress progress-xs">
         <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $awaiting_tasks['task_count'].'%'; ?>;" aria-valuenow="<?php echo $awaiting_tasks['task_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_tasks['task_count']; ?>"></div>
      </div>
   </div>
</div>
<?php } ?> 
<?php
   if($archive_tasks['task_count']!='0'){ ?>
<div class="pro-color">
   <span class="procount"><?php echo $archive_tasks['task_count']; ?></span>
   <div class="fordis">
      <div class="progress progress-xs">
         <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $archive_tasks['task_count'].'%'; ?>;" aria-valuenow="<?php echo $archive_tasks['task_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_tasks['task_count']; ?>"></div>
      </div>
   </div>
</div>
<?php } ?>                                     
</div>

<script type="text/javascript">
 google.charts.load("current", {packages:["corechart"]});
  google.charts.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['Task','link','Count'],
      ['Not started', '<?php echo base_url(); ?>Firm_dashboard/notstarted',<?php echo $notstarted_tasks['task_count']; ?>],
      ['Completed',  '<?php echo base_url(); ?>Firm_dashboard/complete', <?php echo $completed_tasks['task_count']; ?>],
      ['Inprogress','<?php echo base_url(); ?>Firm_dashboard/inprogress',<?php echo $inprogress_tasks['task_count']; ?>],
      ['Awaiting Feedback','<?php echo base_url(); ?>Firm_dashboard/awaited',<?php echo $awaiting_tasks['task_count']; ?>],
      ['Archived',  '<?php echo base_url(); ?>Firm_dashboard/archive',<?php echo $archive_tasks['task_count']; ?>]   
    ]);  

     var view = new google.visualization.DataView(data);
         view.setColumns([0, 2]);
   

    var options = {
         chartArea: {left: 0, width: "80%"},
        height: 250,
       legend: { position: 'bottom', show: true, container: $("#large-legend") , alignment: 'start' },
      hAxis : { textStyle : { fontSize: 8} },
      vAxis : { textStyle : { fontSize: 8} },
       width: 350,
      height: 250
      //pieHole: 0.4,
    };  
  var chart = new google.visualization.PieChart(document.getElementById('pie_chart'));
   
         chart.draw(view, options);
   
         var selectHandler = function(e) {
        //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
          window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
         }
         // Add our selection handler.
         google.visualization.events.addListener(chart, 'select', selectHandler);
      }
</script>