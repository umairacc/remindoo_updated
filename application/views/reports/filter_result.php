<div class="all_user-section2 floating_set fr-reports">
                                    <div class="tab-content">
                                       <div class="reports-tabs tab-pane fade active" id="reports-tab">
                                          <div class="card-comment">
                                             <div class="card-block-small">
                                                <div class="left-sireport col-xs-12  col-md-4">
                                                   <div class="comment-desc">
                                                      <h6>
                                                         Clients 
                                                         <div class="comment-btn">
                                                             <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>Report/Client">Run</button>
                                                            <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>report/Client_customisation">Customise</a>
                                                         </div>
                                                      </h6>
                                                      <span class="time-frame"><label>Time Frame:</label>
                                                      <select id="filter_client">
                                                         <option value="year">This Year</option>
                                                         <option value="month">This Month</option>
                                                      </select>
                                                      </span>
                                                      <div class="card-block fileters_client new123">
                                                         <div id="chart_Donut11" style="width: 100%;"></div>
                                                         <!-- progress bar section -->
                                                         <div class="prodiv12">
                                                            <?php
                                                               if($Private['limit_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo $Private['limit_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Private['limit_count'].'%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if($Public['limit_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo $Public['limit_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Public['limit_count'].'%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if($limited['limit_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo $limited['limit_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $limited['limit_count'].'%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if($Partnership['limit_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo $Partnership['limit_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Partnership['limit_count'].'%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if($self['limit_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo $self['limit_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $self['limit_count'].'%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if($Trust['limit_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo $Trust['limit_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Trust['limit_count'].'%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if($Charity['limit_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo $Charity['limit_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Charity['limit_count'].'%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if($Other['limit_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo $Other['limit_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Other['limit_count'].'%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                         </div>
                                                         <!-- progress bar section -->
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="left-sireport col-xs-12 col-md-4">
                                                   <div class="comment-desc">
                                                      <h6>
                                                         Task
                                                         <div class="comment-btn">
                                                             <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>Report/Task">Run</a>
                                                            <a class="btn bg-c-blue btn-round btn-comment " href="<?php echo base_url(); ?>report/Task_customisation">Customise</a>
                                                         </div>
                                                      </h6>
                                                      <span class="time-frame"><label>Time Frame:</label>
                                                      <select id="filter_task">
                                                         <option value="year">This Year</option>
                                                         <option value="month">This Month</option>
                                                      </select>
                                                      </span>
                                                      <div class="card-block fileters_Tasks">
                                                         <div id="pie_chart" style="width: 100%;"></div>
                                                         <!-- progress bar section -->
                                                         <div class="prodiv12">
                                                            <?php
                                                               if($notstarted_tasks['task_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"><?php echo $notstarted_tasks['task_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $notstarted_tasks['task_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>    
                                                            <?php
                                                               if($completed_tasks['task_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"><?php echo $completed_tasks['task_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $completed_tasks['task_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?> 
                                                            <?php
                                                               if($inprogress_tasks['task_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"><?php echo $inprogress_tasks['task_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $inprogress_tasks['task_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?> 
                                                            <?php
                                                               if($awaiting_tasks['task_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"><?php echo $awaiting_tasks['task_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $awaiting_tasks['task_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?> 
                                                            <?php
                                                               if($testing_tasks['task_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"><?php echo $testing_tasks['task_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $testing_tasks['task_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>                                  
                                                         </div>
                                                         <!-- progress bar section -->
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="left-sireport col-xs-12 col-md-4">
                                                   <div class="comment-desc">
                                                      <h6>
                                                         Proposal
                                                         <div class="comment-btn">
                                                            <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>Report/Proposal">Run</a>
                                                            <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>report/Proposal_customisation">Customise</a>
                                                         </div>
                                                      </h6>
                                                      <span class="time-frame"><label>Time Frame:</label>
                                                      <select id="filter_proposal">
                                                         <option value="year">This Year</option>
                                                         <option value="month">This Month</option>
                                                      </select>
                                                      </span>
                                                      <div class="card-block filter_proposal">
                                                         <div id="piechart1" style="width: 100%; "></div>
                                                         <!-- progress bar section -->
                                                         <div class="prodiv12">
                                                            <?php
                                                               if($accept_proposal['proposal_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo $accept_proposal['proposal_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $accept_proposal['proposal_count'].'%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if($indiscussion_proposal['proposal_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo $indiscussion_proposal['proposal_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $indiscussion_proposal['proposal_count'].'%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if($sent_proposal['proposal_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo $sent_proposal['proposal_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $sent_proposal['proposal_count'].'%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if($viewed_proposal['proposal_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo $viewed_proposal['proposal_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $viewed_proposal['proposal_count'].'%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if($declined_proposal['proposal_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo $declined_proposal['proposal_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $declined_proposal['proposal_count'].'%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if($archive_proposal['proposal_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo $archive_proposal['proposal_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $archive_proposal['proposal_count'].'%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if($draft_proposal['proposal_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo $draft_proposal['proposal_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $draft_proposal['proposal_count'].'%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                         </div>
                                                         <!-- progress bar section -->
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="left-sireport col-xs-12  col-md-4">
                                                   <div class="comment-desc">
                                                      <h6>
                                                         Leads
                                                         <div class="comment-btn">
                                                             <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>Report/Leads">Run</a>
                                                            <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>report/Leads_customisation">Customise</a>
                                                         </div>
                                                      </h6>
                                                      <span class="time-frame"><label>Time Frame:</label>
                                                      <select id="filter_leads">
                                                         <option value="year">This Year</option>
                                                         <option value="month">This Month</option>
                                                      </select>
                                                      </span>
                                                      <div class="card-block filter_leads">
                                                         <div id="piechart_11" style="width: 100%;"></div>
                                                         <!-- progress bar section -->
                                                         <div class="prodiv12">
                                                         <?php        
                                                            foreach($leads_status as $status){ 
                                                               $client=$_SESSION['client'];
                                                               $user=$_SESSION['user'];
                                                              // echo $user;
                                                               //echo $status['id'];
                                                            $count=$this->AllFilter_model->leads_details($status['id'],$client,$user);
                                                               if($count['lead_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                            <?php echo $status['status_name']; ?>
                                                               <span class="procount"><?php echo $count['lead_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $count['lead_count'].'%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php }} ?>
                                                         </div>
                                                         <!-- progress bar section -->
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="left-sireport col-xs-12 col-md-4">
                                                   <div class="comment-desc">
                                                      <h6>
                                                         Service
                                                         <div class="comment-btn">
                                                             <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>Report/Service">Run</button>
                                                            <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>report/Service_customisation">Customise</a>
                                                         </div>
                                                      </h6>
                                                      <span class="time-frame"><label>Time Frame:</label>
                                                      <select id="filter_service">
                                                         <option value="year">This Year</option>
                                                         <option value="month">This Month</option>
                                                      </select>
                                                      </span>
                                                      <div class="card-block filter_service">
                                                         <div id="piechart_service" style="width: 100%; "></div>
                                                         <!-- progress bar section -->
                                                         <div class="prodiv12">
                                                            <?php
                                                               if($conf_statement['service_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"><?php echo $conf_statement['service_count']; ?> </span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $conf_statement['service_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if($accounts['service_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"><?php echo $accounts['service_count']; ?> </span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $accounts['service_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if($company_tax_return['service_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"><?php echo $company_tax_return['service_count']; ?> </span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $company_tax_return['service_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if($personal_tax_return['service_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"><?php echo $personal_tax_return['service_count']; ?> </span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $personal_tax_return['service_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if($vat['service_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"><?php echo $vat['service_count']; ?> </span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $vat['service_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if($payroll['service_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"><?php echo $payroll['service_count']; ?> </span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $payroll['service_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if($workplace['service_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"><?php echo $workplace['service_count']; ?> </span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $workplace['service_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if($cis['service_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"><?php echo $cis['service_count']; ?> </span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $cis['service_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if($cissub['service_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"><?php echo $cissub['service_count']; ?> </span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $cissub['service_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if($p11d['service_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"><?php echo $p11d['service_count']; ?> </span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $p11d['service_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if($management['service_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"><?php echo $management['service_count']; ?> </span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $management['service_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if($bookkeep['service_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"><?php echo $bookkeep['service_count']; ?> </span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $bookkeep['service_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if($investgate['service_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"><?php echo $investgate['service_count']; ?> </span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $investgate['service_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if($registered['service_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"><?php echo $registered['service_count']; ?> </span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $registered['service_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if($taxadvice['service_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"><?php echo $taxadvice['service_count']; ?> </span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $taxadvice['service_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                               if($taxinvest['service_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"><?php echo $taxinvest['service_count']; ?> </span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $taxinvest['service_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                         </div>
                                                         <!-- progress bar section -->
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="left-sireport col-xs-12  col-md-4">
                                                   <div class="comment-desc">
                                                      <h6>
                                                         Deadline Manager
                                                         <div class="comment-btn">
                                                           <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>Report/Dead_line">Run</a>
                                                            <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>report/Deadline_customisation">Customise</a>
                                                         </div>
                                                      </h6>
                                                      <span class="time-frame"><label>Time Frame:</label>
                                                      <select id="filter_deadline">
                                                         <option value="year">This Year</option>
                                                         <option value="month">This Month</option>
                                                      </select>
                                                      </span>
                                                      <div class="card-block filter_deadline">
                                                         <div id="piechart_deadline" style="width: 100%; "></div>
                                                         <!-- progress bar section -->
                                                         <div class="prodiv12">
                                                            <?php if($de_accounts['dead_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo $de_accounts['dead_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_accounts['dead_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php if($de_company_tax_return['dead_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo $de_company_tax_return['dead_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_company_tax_return['dead_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php if($de_conf_statement['dead_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo $de_conf_statement['dead_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_conf_statement['dead_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php if($de_personal_tax_return['dead_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo $de_personal_tax_return['dead_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_personal_tax_return['dead_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php if($de_vat['dead_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo $de_vat['dead_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_vat['dead_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php if($de_payroll['dead_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo $de_payroll['dead_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_payroll['dead_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>    <?php if($de_workplace['dead_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo $de_workplace['dead_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_workplace['dead_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>    <?php if($de_cis['dead_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo $de_cis['dead_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_cis['dead_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>    <?php if($de_cissub['dead_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo $de_cissub['dead_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_cissub['dead_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>    <?php if($de_p11d['dead_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo $de_p11d['dead_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_p11d['dead_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php if($de_management['dead_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo $de_management['dead_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_management['dead_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php if($de_bookkeep['dead_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo $de_bookkeep['dead_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_bookkeep['dead_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php if($de_investgate['dead_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo $de_investgate['dead_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_investgate['dead_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php if($de_registered['dead_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo $de_registered['dead_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_registered['dead_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php if($de_taxadvice['dead_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo $de_taxadvice['dead_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_taxadvice['dead_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                            <?php if($de_taxinvest['dead_count']!='0'){ ?>
                                                            <div class="pro-color">
                                                               <span class="procount"> <?php echo $de_taxinvest['dead_count']; ?></span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_taxinvest['dead_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                         </div>
                                                         <!-- progress bar section -->
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="left-sireport col-xs-12 col-md-4">
                                                   <div class="comment-desc">
                                                      <h6>
                                                         Invoice
                                                         <div class="comment-btn">
                                                            <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>Report/Invoice">Run</a>
                                                            <a class="btn bg-c-blue btn-round btn-comment " href="<?php echo base_url(); ?>report/Invoice_customisation">Customise</a>
                                                         </div>
                                                      </h6>
                                                      <span class="time-frame"><label>Time Frame:</label>
                                                      <select id="filter_invoice">
                                                         <option value="year">This Year</option>
                                                         <option value="month">This Month</option>
                                                      </select>
                                                      </span>
                                                      <div class="card-block fileters_invoice">
                                                         <div id="piechart_invoice" style="width: 100%;"></div>
                                                         <!-- progress bar section -->
                                                         <div class="prodiv12">
                                                            <?php
                                                               if($approved_invoice['invoice_count']!='0'){
                                                               ?>
                                                            <div class="pro-color">
                                                               <span class="procount"><?php echo $approved_invoice['invoice_count']; ?> </span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $approved_invoice['invoice_count'].'%'; ?> ;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>   
                                                            <?php
                                                               if($cancel_invoice['invoice_count']!='0'){
                                                               ?>
                                                            <div class="pro-color">
                                                               <span class="procount"><?php echo $cancel_invoice['invoice_count']; ?> </span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $cancel_invoice['invoice_count'].'%'; ?> ;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>  
                                                            <?php
                                                               if($expired_invoice['invoice_count']!='0'){
                                                               ?>
                                                            <div class="pro-color">
                                                               <span class="procount"><?php echo $expired_invoice['invoice_count']; ?> </span>
                                                               <div class="fordis">
                                                                  <div class="progress progress-xs">
                                                                     <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $expired_invoice['invoice_count'].'%'; ?> ;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <?php } ?>                                               
                                                         </div>
                                                         <!-- progress bar section -->
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="left-sireport col-xs-12 col-md-4">
                                                   <div class="comment-desc sumlast">
                                                      <h6>
                                                         Summary
                                                         <div class="comment-btn">
                                                             <a class="btn bg-c-blue btn-round btn-comment " href="<?php echo base_url(); ?>Report/Summary">Run</a>
                                                            <a class="btn bg-c-blue btn-round btn-comment " href="<?php echo base_url(); ?>Report/SummaryCustomisation">Customise</a>
                                                         </div>
                                                      </h6>
                                                      <!-- progress bar section -->
                                                      <div class="prodiv">
                                                         <div class="pro-color">
                                                            <span class="procount primary"><?php echo $active_user_count['activeuser_count']; ?></span>
                                                            <div class="fordis">
                                                               <label>Active Users</label>
                                                               <div class="progress progress-xs">
                                                                  <div class="progress-bar progress-bar-primary" role="progressbar" style="width: <?php echo $active_user_count['activeuser_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="pro-color">
                                                            <span class="procount warning"><?php echo $inactive_user_count['inactiveuser_count']; ?></span>
                                                            <div class="fordis">
                                                               <label>Inactive Users</label>
                                                               <div class="progress progress-xs">
                                                                  <div class="progress-bar progress-bar-warning" role="progressbar" style="width: <?php echo $inactive_user_count['inactiveuser_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="pro-color">
                                                            <span class="procount danger"><?php echo $completed_task_count['task_count']; ?></span>
                                                            <div class="fordis">
                                                               <label>Completed Tasks</label>
                                                               <div class="progress progress-xs">
                                                                  <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $completed_task_count['task_count'].'%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="pro-color success">
                                                            <span class="procount"><?php echo $overall_leads['leads_count']; ?></span>
                                                            <div class="fordis">
                                                               <label>Over all Leads</label>
                                                               <div class="progress progress-xs">
                                                                  <div class="progress-bar progress-bar-success" role="progressbar" style="width: <?php echo $overall_leads['leads_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <!-- progress bar section -->
                                                   </div>
                                                </div>

                                                  <div class="left-sireport col-xs-12 col-md-4">
                                                   <div class="comment-desc sumlast">
                                                      <h6>
                                                      Timesheet
                                                         <div class="comment-btn">                                                        
                                                           <a class="btn bg-c-blue btn-round btn-comment " href="<?php echo base_url(); ?>Timesheet">Run</a>
                                                            <a class="btn bg-c-blue btn-round btn-comment " href="<?php echo base_url(); ?>Timesheet/timesheet_custom">Customise</a>
                                                         </div>
                                                      </h6>
                                                   </div>
                                                </div>
                                                <div class="left-sireport col-xs-12 col-md-4">
                                                   <div class="comment-desc sumlast">
                                                      <h6>
                                                        <span>  Individual User Performance</span>
                                                            <div class="comment-btn">                                                        
                                                               <a class="btn bg-c-blue btn-round btn-comment " href="<?php echo base_url(); ?>Reports/user_performation">Run</a>
                                                               <a class="btn bg-c-blue btn-round btn-comment " href="<?php echo base_url(); ?>Reports/performance_customisation">Customise</a>
                                                            </div>
                                                       </h6>
                                                   </div>                                                 
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="tab-pane fade" id="clients-reports">
                                          <div class="card-block-small">
                                             <div class="left-sireport col-xs-12  col-md-6">
                                                <div  class="comment-desc">
                                                <h6>
                                                         Legal Form 
                                                        
                                                      </h6>
                                                <div class="card-block fileters_client new123">
                                                <div id="Tabs_client" style="width:100%;">      
                                                </div>

                                                 <!-- <div class="prodiv12">
                                                <?php
                                                   if($Private['limit_count']!='0'){ ?>
                                                <div class="pro-color">
                                                   <span class="procount"> <?php echo $Private['limit_count']; ?></span>
                                                   <div class="fordis">
                                                      <div class="progress progress-xs">
                                                         <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Private['limit_count'].'%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <?php } ?>
                                                <?php
                                                   if($Public['limit_count']!='0'){ ?>
                                                <div class="pro-color">
                                                   <span class="procount"> <?php echo $Public['limit_count']; ?></span>
                                                   <div class="fordis">
                                                      <div class="progress progress-xs">
                                                         <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Public['limit_count'].'%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <?php } ?>
                                                <?php
                                                   if($limited['limit_count']!='0'){ ?>
                                                <div class="pro-color">
                                                   <span class="procount"> <?php echo $limited['limit_count']; ?></span>
                                                   <div class="fordis">
                                                      <div class="progress progress-xs">
                                                         <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $limited['limit_count'].'%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <?php } ?>
                                                <?php
                                                   if($Partnership['limit_count']!='0'){ ?>
                                                <div class="pro-color">
                                                   <span class="procount"> <?php echo $Partnership['limit_count']; ?></span>
                                                   <div class="fordis">
                                                      <div class="progress progress-xs">
                                                         <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Partnership['limit_count'].'%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <?php } ?>
                                                <?php
                                                   if($self['limit_count']!='0'){ ?>
                                                <div class="pro-color">
                                                   <span class="procount"> <?php echo $self['limit_count']; ?></span>
                                                   <div class="fordis">
                                                      <div class="progress progress-xs">
                                                         <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $self['limit_count'].'%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <?php } ?>
                                                <?php
                                                   if($Trust['limit_count']!='0'){ ?>
                                                <div class="pro-color">
                                                   <span class="procount"> <?php echo $Trust['limit_count']; ?></span>
                                                   <div class="fordis">
                                                      <div class="progress progress-xs">
                                                         <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Trust['limit_count'].'%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <?php } ?>
                                                <?php
                                                   if($Charity['limit_count']!='0'){ ?>
                                                <div class="pro-color">
                                                   <span class="procount"> <?php echo $Charity['limit_count']; ?></span>
                                                   <div class="fordis">
                                                      <div class="progress progress-xs">
                                                         <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Charity['limit_count'].'%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <?php } ?>
                                                <?php
                                                   if($Other['limit_count']!='0'){ ?>
                                                <div class="pro-color">
                                                   <span class="procount"> <?php echo $Other['limit_count']; ?></span>
                                                   <div class="fordis">
                                                      <div class="progress progress-xs">
                                                         <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Other['limit_count'].'%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <?php } ?>
                                             </div> --> 
                                             </div>
                                             </div>
                                             </div>
                                            <div class="left-sireport col-xs-12  col-md-6">
                                             <div class="comment-desc">
                                             <h6>
                                                         Status 
                                                         
                                                      </h6>
                                             <div class="card-block fileters_client new123">
                                                <div id="Tabs_Users" style="width:100%;"></div>
                                                </div>
                                             </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="tab-pane fade" id="task-reports">
                                          <div class="card-block-small">
                                          <div class="left-sireport col-xs-12  col-md-6">
                                                <div  class="comment-desc">
                                                <h6>
                                                         Status 
                                                      </h6>
                                                <div class="card-block fileters_client new123">
                                             <div id="Tabs_task"></div>
                                             <div class="prodiv12">
                                                <?php
                                                   if($notstarted_tasks['task_count']!='0'){ ?>
                                                <div class="pro-color">
                                                   <span class="procount"><?php echo $notstarted_tasks['task_count']; ?></span>
                                                   <div class="fordis">
                                                      <div class="progress progress-xs">
                                                         <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $notstarted_tasks['task_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <?php } ?>    
                                                <?php
                                                   if($completed_tasks['task_count']!='0'){ ?>
                                                <div class="pro-color">
                                                   <span class="procount"><?php echo $completed_tasks['task_count']; ?></span>
                                                   <div class="fordis">
                                                      <div class="progress progress-xs">
                                                         <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $completed_tasks['task_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <?php } ?> 
                                                <?php
                                                   if($inprogress_tasks['task_count']!='0'){ ?>
                                                <div class="pro-color">
                                                   <span class="procount"><?php echo $inprogress_tasks['task_count']; ?></span>
                                                   <div class="fordis">
                                                      <div class="progress progress-xs">
                                                         <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $inprogress_tasks['task_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <?php } ?> 
                                                <?php
                                                   if($awaiting_tasks['task_count']!='0'){ ?>
                                                <div class="pro-color">
                                                   <span class="procount"><?php echo $awaiting_tasks['task_count']; ?></span>
                                                   <div class="fordis">
                                                      <div class="progress progress-xs">
                                                         <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $awaiting_tasks['task_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <?php } ?> 
                                                <?php
                                                   if($testing_tasks['task_count']!='0'){ ?>
                                                <div class="pro-color">
                                                   <span class="procount"><?php echo $testing_tasks['task_count']; ?></span>
                                                   <div class="fordis">
                                                      <div class="progress progress-xs">
                                                         <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $testing_tasks['task_count'].'%'; ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <?php } ?>                                 
                                             </div>
                                             </div>
                                             </div>
                                             </div>
                                             <div class="left-sireport col-xs-12  col-md-6">
                                             <div  class="comment-desc">
                                             <h6>
                                                         Priority 
                                                      </h6>
                                                <div class="card-block fileters_client new123">
                                             <div id="Tabs_priority"></div>
                                             </div>
                                             </div>
                                             </div>
                                             <div class="left-sireport col-xs-12 line-chart123">
                                             <div  class="comment-desc">
                                             <h6>
                                                         This Month Tasks 
                                                         
                                                      </h6>
                                                <div class="card-block fileters_client new123">
                                             <div id="monthly_task" style="width: 1200px; height: 500px"></div>
                                             </div>
                                             </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="tab-pane fade" id="proposals-reports">
                                          <div class="card-block-small">
                                             <div class="row history">
                                                <div class="card">
                                                   <div class="card-header">
                                                      <h5>This Month Proposal History</h5>
                                                   </div>
                                                   <div class="card-block">
                                                      <div class="main-timeline">
                                                         <div class="cd-timeline cd-container">
                                                            <?php 
                                                               foreach($proposal_history as $p_h){  
                                                               $randomstring=generateRandomString('100');
                                                               ?>
                                                            <div class="cd-timeline-block">
                                                               <div class="cd-timeline-icon bg-primary">
                                                                  <i class="icofont icofont-ui-file"></i>
                                                               </div>
                                                               <div class="cd-timeline-content card_main">
                                                                  <div class="p-20">
                                                                     <div class="timeline-details">
                                                                        <p class="m-t-20">
                                                                           <!-- <a href="<?php //base_url().'proposal_page/step_proposal/'?><?php //echo $randomstring; ?><?php echo '---'; ?> <?php //echo $p_h['id']; ?>">--><?php echo $p_h['proposal_name']; ?><!-- </a> -->
                                                                        </p>
                                                                     </div>
                                                                  </div>
                                                                  <span class="cd-date"><?php
                                                                     $timestamp = strtotime($p_h['created_at']);
                                                                     $newDate = date('d F Y H:i', $timestamp); 
                                                                     echo $newDate;
                                                                     ?></span>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="tab-pane fade" id="leads-reports">
                                          <div class="card-block-small">
                                             <div class="row history">
                                                <div class="card">
                                                   <div class="card-header">
                                                      <h5>This Month Leads History</h5>
                                                   </div>
                                                   <div class="card-block">
                                                      <div class="main-timeline">
                                                         <div class="cd-timeline cd-container">
                                                            <?php 
                                                               foreach($leads_history as $lead){  
                                                               // $randomstring=generateRandomString('100');
                                                               ?>
                                                            <div class="cd-timeline-block">
                                                               <div class="cd-timeline-icon bg-primary">
                                                                  <i class="icofont icofont-ui-file"></i>
                                                               </div>
                                                               <div class="cd-timeline-content card_main">
                                                                  <div class="p-20">
                                                                     <div class="timeline-details">
                                                                        <p class="m-t-20">
                                                                           <!-- <a href="<?php //base_url().'proposal_page/step_proposal/'?><?php //echo $randomstring; ?><?php echo '---'; ?> <?php //echo $p_h['id']; ?>">--><?php echo $lead['name']; ?><!-- </a> -->
                                                                        </p>
                                                                     </div>
                                                                  </div>
                                                                  <span class="cd-date"><?php
                                                                     $timestamp = $lead['createdTime'];
                                                                     $newDate = date('d F Y H:i', $timestamp); 
                                                                     echo $newDate;
                                                                     ?></span>
                                                               </div>
                                                            </div>
                                                            <?php } ?>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="tab-pane fade com-chart" id="services-reports">
                                          <div class="card-block-small">
                                          <div class="left-sireport col-xs-12  col-md-6">
                                                   <div class="comment-desc">
                                                   <h6>
                                                         Services
                                                         
                                                      </h6>
                                             <div id="Tabs_service" style="width:100%;"></div>
                                             </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="tab-pane fade com-chart" id="deadlines-reports">
                                          <div class="card-block-small">
                                          <div class="left-sireport col-xs-12  col-md-6">
                                                   <div class="comment-desc">
                                                   <h6>
                                                         This Month Tasks 
                                                         
                                                      </h6>
                                             <div id="Tabs_deadline" style="width:100%;"></div>
                                             </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="tab-pane fade com-chart" id="invoices-reports">
                                          <div class="card-block-small">
                                          <div class="left-sireport col-xs-12  col-md-6">
                                                   <div class="comment-desc">
                                                   <h6>
                                                         This Month Tasks 
                                                         
                                                      </h6>
                                             <div id="Tabs_invoice" style="width:100%;"></div>
                                             </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div> <!--tab content close -->
                                    <!-- home-->
                                    <!-- frozen -->
                                 </div>
                                 <!-- ul after div -->
                              </div>
                           </div>
                           <!-- admin close -->
                        </div>

<div class="modal fade" id="client_download" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reports</h4>
         </div>
         <div class="modal-body">        
            <a href="<?php echo base_url(); ?>Reports/chart" class="btn btn-primary" id="client_pdf">Pdf</a>
            <a href="javascript:;" onclick="exportexcel()" class="btn btn-primary">Excel</a>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="invoice_download" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reports</h4>
         </div>
         <div class="modal-body">        
            <a href="<?php echo base_url(); ?>Filter/invoice" class="btn btn-primary" id="client_pdf">Pdf</a>
            <a href="javascript:;" onclick="invoice_exportexcel()" class="btn btn-primary">Excel</a>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="invoice_download_sess" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reports</h4>
         </div>
         <div class="modal-body">        
            <a href="<?php echo base_url(); ?>Filter/invoice_sess" class="btn btn-primary" id="client_pdf">Pdf</a>
            <a href="javascript:;" onclick="invoice_exportexcel_sess()" class="btn btn-primary">Excel</a>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="summary_download" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reports</h4>
         </div>
         <div class="modal-body">        
            <a href="<?php echo base_url(); ?>Filter/summary" class="btn btn-primary" id="client_pdf">Pdf</a>
            <a href="javascript:;" onclick="exportexcel_summary()" class="btn btn-primary">Excel</a>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="summary_download_sess" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reports</h4>
         </div>
         <div class="modal-body">        
            <a href="<?php echo base_url(); ?>Filter/summary_sess" class="btn btn-primary" id="client_pdf">Pdf</a>
            <a href="javascript:;" onclick="exportexcel_summary_sess()" class="btn btn-primary">Excel</a>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="summary_customization" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Customization</h4>
         </div>
         <div class="modal-body">
            <div class="col-md-12 cust1-popup">
               <div class="col-md-6">
                  <label>From Date</label>
                  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                  <input type="text" class="date-picker" id="summary_from_date" value="" placeholder="dd/mm/yyyy">
               </div>
               <div class="col-md-6">
                  <label>To Date</label>
                  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                  <input type="text" class="date-picker" id="summary_to_date" value="" placeholder="dd/mm/yyyy">
               </div>
            </div>
            <div class="col-md-12 cust-popup">
               <div class="col-md-6">
                  <label>Priority</label>  
               </div>
               <div class="col-md-6">
                  <select id="summary_option">
                     <option value="all">All</option>
                     <option value="user">User</option>
                     <option value="leads">Leads</option>
                     <option value="task">Tasks</option>
                  </select>
               </div>
            </div>
            <!--  <div class="col-md-12 cust-popup">
               <div class="col-md-6">
                <label>Status</label>
                </div>
                <div class="col-md-6">
                <select id="status" name="stati">
                <option value="all" hidden="">Any</option>          
                <option value="1">Active</option>
                <option value="2">In-Active</option>
                <option value="3">Fozen</option>
                </select> 
                </div>
               </div>   -->       
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default select_template" id="summary_customisation">Submit</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="invoice_customization" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Customization</h4>
         </div>
         <div class="modal-body">
            <div class="col-md-12 cust1-popup">
               <div class="col-md-6">
                  <label>From Date</label>
                  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                  <input type="text" class="date-picker" id="invoice_from_date" value="" placeholder="dd/mm/yyyy">
               </div>
               <div class="col-md-6">
                  <label>To Date</label>
                  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                  <input type="text" class="date-picker" id="invoice_to_date" value="" placeholder="dd/mm/yyyy">
               </div>
            </div>
            <div class="col-md-12 cust-popup">
               <div class="col-md-6">
                  <label>Priority</label>  
               </div>
               <div class="col-md-6">
                  <select id="invoice_status">
                     <option value="all">All</option>
                     <option value="Expired">Expired</option>
                     <option value="approve">Approve</option>
                     <option value="decline">Decline</option>
                  </select>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default select_template" id="invoice_customisation">Submit</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="client_download_sess" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reports</h4>
         </div>
         <div class="modal-body">        
            <a href="<?php echo base_url(); ?>Reports/chart_sess" class="btn btn-primary" id="client_pdf">Pdf</a>
            <a href="javascript:;" onclick="exportexcel_sess()" class="btn btn-primary">Excel</a>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="task_download" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reports</h4>
         </div>
         <div class="modal-body">        
            <a href="<?php echo base_url(); ?>Reports/task_pdf_download" class="btn btn-primary" id="client_pdf">Pdf</a>
            <a href="javascript:;" onclick="exporttaskexcel()" class="btn btn-primary">Excel</a>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="task_download_sess" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reports</h4>
         </div>
         <div class="modal-body">        
            <a href="<?php echo base_url(); ?>Reports/task_pdf_download_sess" class="btn btn-primary" id="client_pdf">Pdf</a>
            <a href="javascript:;" onclick="exporttaskexcel_sess()" class="btn btn-primary">Excel</a>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="proposal_download" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reports</h4>
         </div>
         <div class="modal-body">        
            <a href="<?php echo base_url(); ?>Report/proposalpdf_download" class="btn btn-primary" id="client_pdf">Pdf</a>
            <a href="javascript:;" onclick="exportproposalexcel()" class="btn btn-primary">Excel</a>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="proposal_download_sess" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reports</h4>
         </div>
         <div class="modal-body">        
            <a href="<?php echo base_url(); ?>Report/proposalpdf_download_sess" class="btn btn-primary" id="client_pdf">Pdf</a>
            <a href="javascript:;" onclick="exportproposalexcel_sess()" class="btn btn-primary">Excel</a>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="leads_download" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reports</h4>
         </div>
         <div class="modal-body">        
            <a href="<?php echo base_url(); ?>Report/leadspdf_download" class="btn btn-primary" id="client_pdf">Pdf</a>
            <a href="javascript:;" onclick="exportleadsexcel()" class="btn btn-primary">Excel</a>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="leads_download_sess" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reports</h4>
         </div>
         <div class="modal-body">        
            <a href="<?php echo base_url(); ?>Report/leadspdf_download_sess" class="btn btn-primary" id="client_pdf">Pdf</a>
            <a href="javascript:;" onclick="exportleadsexcel_sess()" class="btn btn-primary">Excel</a>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="service_download" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reports</h4>
         </div>
         <div class="modal-body service_session_not">        
            <a href="<?php echo base_url(); ?>Report/services" class="btn btn-primary" id="client_pdf">Pdf</a>
            <a href="javascript:;" onclick="service_exportexcel()" class="btn btn-primary">Excel</a>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="service_download_sess" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reports</h4>
         </div>
         <div class="modal-body service_session" >        
            <a href="<?php echo base_url(); ?>Report/services_sess" class="btn btn-primary" id="client_pdf">Pdf</a>
            <a href="javascript:;" onclick="service_exportexcel1()" class="btn btn-primary">Excel</a>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="deadline_download" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reports</h4>
         </div>
         <div class="modal-body service_session_not">        
            <a href="<?php echo base_url(); ?>Report/deadline" class="btn btn-primary" id="client_pdf">Pdf</a>
            <a href="javascript:;" onclick="deadline_exportexcel()" class="btn btn-primary">Excel</a>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="deadline_download_sess" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reports</h4>
         </div>
         <div class="modal-body service_session_not">        
            <a href="<?php echo base_url(); ?>Reports/deadline_sess" class="btn btn-primary" id="client_pdf">Pdf</a>
            <a href="javascript:;" onclick="deadline_exportexcel_sess()" class="btn btn-primary">Excel</a>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!-- filter add -->
<div class="modal fade" id="client_customization" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Customization</h4>
         </div>
         <div class="modal-body">
            <div class="col-md-12 cust1-popup">
               <div class="col-md-6">
                  <label>From Date</label>
                  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                  <input type="text" class="date-picker" id="from_date" value="" placeholder="dd/mm/yyyy">
               </div>
               <div class="col-md-6">
                  <label>To Date</label>
                  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                  <input type="text" class="date-picker" id="to_date" value="" placeholder="dd/mm/yyyy">
               </div>
            </div>
            <div class="col-md-12 cust-popup">
               <div class="col-md-6">
                  <label>Priority</label>  
               </div>
               <div class="col-md-6">
                  <select id="legal_form">
                     <option value="all">All</option>
                     <option value="Private Limited company">Private Limited company</option>
                     <option value="Public Limited company">Public Limited company</option>
                     <option value="Limited Liability Partnership">Limited Liability Partnership</option>
                     <option value="Partnership">Partnership</option>
                     <option value="Self Assessment">Self Assessment</option>
                     <option value="Trust">Trust</option>
                     <option value="Charity">Charity</option>
                     <option value="Other">Other</option>
                  </select>
               </div>
            </div>
            <div class="col-md-12 cust-popup">
               <div class="col-md-6">
                  <label>Status</label>
               </div>
               <div class="col-md-6">
                  <select id="status" name="stati">
                     <option value="all" hidden="">Any</option>
                     <option value="1">Active</option>
                     <option value="2">In-Active</option>
                     <option value="3">Fozen</option>
                  </select>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default select_template" id="client_customisation">Submit</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="task_customization" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Customization</h4>
         </div>
         <div class="modal-body">
            <div class="col-md-12 cust1-popup">
               <div class="col-md-6">
                  <label>From Date</label>
                  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                  <input type="text" class="date_picker1" id="task_from_date" value="" placeholder="dd/mm/yyyy">
               </div>
               <div class="col-md-6">
                  <label>To Date</label>
                  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                  <input type="text" class="date_picker2" id="task_to_date" value="" placeholder="dd/mm/yyyy">
               </div>
            </div>
            <div class="col-md-12 cust-popup">
               <div class="col-md-6">
                  <label>Priority</label>  
               </div>
               <div class="col-md-6">
                  <select name="priority" class="valid" id="priority">
                     <option value="all">All</option>
                     <option value="low">Low</option>
                     <option value="medium">Medium</option>
                     <option value="high">High</option>
                     <option value="super_urgent">Super Urgent</option>
                  </select>
               </div>
            </div>
            <div class="col-md-12 cust-popup">
               <div class="col-md-6">
                  <label>Status</label>
               </div>
               <div class="col-md-6">
                  <select name="task_status" class=" valid" id="task_status">
                     <option value="all">All</option>
                     <option value="notstarted">Not started</option>
                     <option value="inprogress">In Progress</option>
                     <option value="awaiting">Awaiting Feedback</option>
                     <option value="testing">Testing</option>
                     <option value="complete">Complete</option>
                  </select>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default select_template" id="task_customisation">Submit</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="proposal_customization" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Customization</h4>
         </div>
         <div class="modal-body">
            <div class="col-md-12 cust1-popup">
               <div class="col-md-6">
                  <label>From Date</label>
                  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                  <input type="text" class="date_picker1" id="proposal_from_date" value="" placeholder="dd/mm/yyyy">
               </div>
               <div class="col-md-6">
                  <label>To Date</label>
                  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                  <input type="text" class="date_picker2" id="proposal_to_date" value="" placeholder="dd/mm/yyyy">
               </div>
            </div>
            <div class="col-md-12 cust-popup">
               <div class="col-md-6">
                  <label>Status</label>  
               </div>
               <div class="col-md-6">
                  <select name="status" class="valid" id="proposal_status">
                     <option value="all">All</option>
                     <option value="sent">Sent</option>
                     <option value="opened">Viewed</option>
                     <option value="accepted">Accept</option>
                     <option value="declined">Decline</option>
                     <option value="in discussion">InDiscussion</option>
                     <option value="draft">Draft</option>
                  </select>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default select_template" id="proposal_customisation">Submit</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="leads_customization" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Customization</h4>
         </div>
         <div class="modal-body">
            <div class="col-md-12 cust1-popup">
               <div class="col-md-6">
                  <label>From Date</label>
                  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                  <input type="text" class="date_picker1" id="leads_from_date" value="" placeholder="dd/mm/yyyy">
               </div>
               <div class="col-md-6">
                  <label>To Date</label>
                  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                  <input type="text" class="date_picker2" id="leads_to_date" value="" placeholder="dd/mm/yyyy">
               </div>
            </div>
            <div class="col-md-12 cust-popup">
               <div class="col-md-6">
                  <label>Status</label>  
               </div>
               <div class="col-md-6">
                  <select name="status" class="form-control valid" id="leads_status">
                     <option value="all">All</option>
                     <?php foreach($leads_status as $ls_value) { ?>
                     <option value='<?php echo $ls_value['id'];?>'><?php echo $ls_value['status_name'];?></option>
                     <?php } ?>
                  </select>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default select_template" id="leads_customisation">Submit</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="service_customization" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Customization</h4>
         </div>
         <div class="modal-body">
            <div class="col-md-12 cust1-popup">
               <div class="col-md-6">
                  <label>From Date</label>
                  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                  <input type="text" class="date_picker1" id="service_from_date" value="" placeholder="dd/mm/yyyy">
               </div>
               <div class="col-md-6">
                  <label>To Date</label>
                  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                  <input type="text" class="date_picker2" id="service_to_date" value="" placeholder="dd/mm/yyyy">
               </div>
            </div>
            <div class="col-md-12 cust-popup">
               <div class="col-md-6">
                  <label>Status</label>  
               </div>
               <div class="col-md-6">
                  <select name="status" class="form-control valid" id="service">
                     <option value="all">All</option>
                     <option value="conf_statement">Confirmation Statement</option>
                     <option value="accounts">Accounts</option>
                     <option value="company_tax_return">Company Tax Return</option>
                     <option value="personal_tax_return">Personal Tax Return</option>
                     <option value="payroll">Payroll</option>
                     <option value="workplace">WorkPlace Pension - AE</option>
                     <option value="vat">VAT Returns</option>
                     <option value="cis">CIS - Contractor</option>
                     <option value="cissub">CIS - Sub Contractor</option>
                     <option value="p11d">P11D</option>
                     <option value="bookkeep">Bookkeeping</option>
                     <option value="management">Management Accounts</option>
                     <option value="investgate">Investigation Insurance</option>
                     <option value="registered">Registered Address</option>
                     <option value="taxadvice">Tax Advice</option>
                     <option value="taxinvest">Tax Investigation</option>
                  </select>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default select_template" id="service_customisation">Submit</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="deadline_customization" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Customization</h4>
         </div>
         <div class="modal-body">
            <div class="col-md-12 cust1-popup">
               <div class="col-md-6">
                  <label>From Date</label>
                  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                  <input type="text" class="date_picker1" id="deadline_from_date" value="" placeholder="dd/mm/yyyy">
               </div>
               <div class="col-md-6">
                  <label>To Date</label>
                  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                  <input type="text" class="date_picker2" id="deadline_to_date" value="" placeholder="dd/mm/yyyy">
               </div>
            </div>
            <div class="col-md-12 cust-popup">
               <div class="col-md-6">
                  <label>Company Type</label>  
               </div>
               <div class="col-md-6">
                  <select class="form-control valid" name="company_type" id="company_type">
                     <option value="" data-id="1">All</option>
                     <option value="Private Limited company" data-id="2">Private Limited company</option>
                     <option value="Public Limited company" data-id="3">Public Limited company</option>
                     <option value="Limited Liability Partnership" data-id="4">Limited Liability Partnership</option>
                     <option value="Partnership" data-id="5">Partnership</option>
                     <option value="Self Assessment" data-id="6">Self Assessment</option>
                     <option value="Trust" data-id="7">Trust</option>
                     <option value="Charity" data-id="8">Charity</option>
                     <option value="Other" data-id="9">Other</option>
                  </select>
               </div>
            </div>
            <div class="col-md-12 cust-popup">
               <div class="col-md-6">
                  <label>Deadline Type</label>
               </div>
               <div class="col-md-6">
                  <select class="form-control valid" name="deadline_type" id="deadline_type">
                     <option value="">ALL</option>
                     <option value="conf_statement">Confirmation Statement</option>
                     <option value="accounts">Accounts</option>
                     <option value="company_tax_return">Company Tax Return</option>
                     <option value="personal_tax_return">Personal Tax Return</option>
                     <option value="payroll">Payroll</option>
                     <option value="workplace">WorkPlace Pension - AE</option>
                     <option value="vat">VAT Returns</option>
                     <!--  <option value="cis">CIS - Contractor</option>
                        <option value="cissub">CIS - Sub Contractor</option> -->
                     <option value="p11d">P11D</option>
                     <option value="bookkeep">Bookkeeping</option>
                     <option value="management">Management Accounts</option>
                     <option value="investgate">Investigation Insurance</option>
                     <option value="registered">Registered Address</option>
                     <option value="taxadvice">Tax Advice</option>
                     <option value="taxinvest">Tax Investigation</option>
                  </select>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default select_template" id="deadline_customisation">Submit</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!--style="display: none" -->
<div class="charts" style="display: none">
   <div class="card-block fileters_client1">
   </div>
   <div class="card-block fileters_Tasks1">
   </div>
   <div class="card-block filter_proposal1">
   </div>
   <div class="card-block filter_leads1">
   </div>
   <div class="card-block filter_service1">
   </div>
   <div class="card-block filter_deadline1">
   </div>
   <div class="card-block fileters_invoice1">
   </div>
</div>
<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<?php $this->load->view('reports/reports_scripts');?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/custom-google-chart.js"></script><!--<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.scrollbox.min.js"></script>-->
<script src="<?php echo base_url();?>assets/js/jquery.nicescroll.min.js"></script>
<script type="text/javascript">
   $( document ).ready(function() {
   //     $(".prodiv12").niceScroll({
   
   //    zindex: "auto",
   //   cursoropacitymin: 1,
   //   cursoropacitymax: 0,
   //   cursorcolor: "#ccc",
   //   cursorwidth: "6px",
   //   cursorborder: "1px solid #fff",
   //   cursorborderradius: "5px",
   //   scrollspeed: 60,
   //   mousescrollstep: 8 * 3,
   //   // options here
   
   // });
   });
</script>
<!-- Client Chart -->
<script type="text/javascript">
   google.charts.load("current", {packages:["corechart"]});
   google.charts.setOnLoadCallback(drawChart);
   function drawChart() {
     var data = google.visualization.arrayToDataTable([
       ['Legal Form','link','Count'],
       ['Private Limited company', '<?php echo base_url();?>Firm_dashboard/private_function', <?php echo $Private['limit_count']; ?>],
       ['Public Limited company',  '<?php echo base_url();?>Firm_dashboard/public_function',   <?php echo $Public['limit_count']; ?>],
       ['Limited Liability Partnership','<?php echo base_url();?>Firm_dashboard/limited_function', <?php echo $limited['limit_count']; ?>],
       ['Partnership', '<?php echo base_url();?>Firm_dashboard/partnership', <?php echo $Partnership['limit_count']; ?>],
       ['Self Assessment','<?php echo base_url();?>Firm_dashboard/self_assesment',   <?php echo $self['limit_count']; ?>],
       ['Trust','<?php echo base_url();?>Firm_dashboard/trust',<?php echo $Trust['limit_count']; ?>],
       ['Charity','<?php echo base_url();?>Firm_dashboard/charity',<?php echo $Charity['limit_count']; ?>],
        ['Other','<?php echo base_url();?>Firm_dashboard/other',<?php echo $Other['limit_count']; ?>],
     ]);  
   
      var view = new google.visualization.DataView(data);
          view.setColumns([0, 2]);
    
   
      var options = {
         chartArea: {left:0, width: "80%"},
         height:250, 
        legend: { position: 'bottom', show: true, container: $("#large-legend"), alignment: 'start' },
        hAxis : { textStyle : { fontSize: 8} },
        vAxis : { textStyle : { fontSize: 8} },
          width: 350,
          height: 250
       // pieHole: 0.4,
      };  

      var chart = new google.visualization.PieChart(document.getElementById('chart_Donut11'));   
         chart.draw(view, options);   
         var selectHandler = function(e) {
        //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
          window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
         }
         // Add our selection handler.
         google.visualization.events.addListener(chart, 'select', selectHandler);
      }
   //   var chart_div = new google.visualization.PieChart(document.getElementById('chart_Donut11'));
   //   var chart = new google.visualization.PieChart(chart_Donut11);
   //   google.visualization.events.addListener(chart, 'ready', function () {
   //   chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
   //   //document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '"></a>';
   //   var image=chart.getImageURI();
   //      $(document).ready(function(){   
   //                $.ajax({
   //                       type: "POST",
   //                       url: "<?php echo base_url();?>/Reports/pdf_download",         
   //                       data: {image: image},
   //                       success: 
   //                            function(data){
                     
   //                            }
   //                        });
   //                 });
   // });
   //   chart.draw(view, options);
   
   //   var selectHandler = function(e) {
   //        //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
   //        window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
   //        }
   //        // Add our selection handler.
   //        google.visualization.events.addListener(chart, 'select', selectHandler);
   // }
</script>
<!-- Client Chart -->
<!-- Task Chart -->
<script type="text/javascript">
   google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ['Task','link','Count'],
        ['Not started', '<?php echo base_url(); ?>Firm_dashboard/notstarted',   <?php echo $notstarted_tasks['task_count']; ?>],
        ['Completed',  '<?php echo base_url(); ?>Firm_dashboard/complete',    <?php echo $completed_tasks['task_count']; ?>],
        ['Inprogress','<?php echo base_url(); ?>Firm_dashboard/inprogress',  <?php echo $inprogress_tasks['task_count']; ?>],
        ['Awaiting Feedback','<?php echo base_url(); ?>Firm_dashboard/awaited', <?php echo $awaiting_tasks['task_count']; ?>],
        ['Testing',  '<?php echo base_url(); ?>Firm_dashboard/tested', <?php echo $testing_tasks['task_count']; ?>]    
      ]);  
   
       var view = new google.visualization.DataView(data);
           view.setColumns([0, 2]);
     
   
       var options = {
         chartArea: {left:0, width: "80%"},
         height:250, 
        legend: { position: 'bottom', show: true, container: $("#large-legend"), alignment: 'start' },
        hAxis : { textStyle : { fontSize: 8} },
        vAxis : { textStyle : { fontSize: 8} },
          width: 350,
          height: 250
       // pieHole: 0.4,
      };  

       var chart = new google.visualization.PieChart(document.getElementById('pie_chart'));   
         chart.draw(view, options);   
         var selectHandler = function(e) {
        //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
          window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
         }
         // Add our selection handler.
         google.visualization.events.addListener(chart, 'select', selectHandler);
      }
    //   var chart_div = new google.visualization.PieChart(document.getElementById('pie_chart'));
    //   var chart = new google.visualization.PieChart(pie_chart);
    //   google.visualization.events.addListener(chart, 'ready', function () {
    //   chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
    //   //document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '"></a>';
    //   var image=chart.getImageURI();
    //      $(document).ready(function(){   
    //                $.ajax({
    //                       type: "POST",
    //                       url: "<?php echo base_url();?>/Reports/task_chart",         
    //                       data: {chart_image: image},
    //                       success: 
    //                            function(data){
                      
    //                            }
    //                        });
    //                 });
    // });
    //   chart.draw(view, options);
    //    var selectHandler = function(e) {
    //       //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
    //         window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
    //        }
    //        // Add our selection handler.
    //        google.visualization.events.addListener(chart, 'select', selectHandler);
    // }
</script>
<!-- Task Chart-->
<!-- Proposal Chart-->
<script type="text/javascript">
   google.charts.load('current', {'packages':['corechart']});
   google.charts.setOnLoadCallback(drawChart2);
   
   function drawChart2() {
   
    var data = google.visualization.arrayToDataTable([
      ['Proposal','link','Count'],
      ['Accepted', '<?php echo base_url(); ?>Firm_dashboard/accept',   <?php echo $accept_proposal['proposal_count']; ?>],
      ['In Discussion',  '<?php echo base_url(); ?>Firm_dashboard/indiscussion',    <?php echo $indiscussion_proposal['proposal_count']; ?>],
      ['Sent','<?php echo base_url(); ?>Firm_dashboard/sent',  <?php echo $sent_proposal['proposal_count']; ?>],
      ['Viewed','<?php echo base_url(); ?>Firm_dashboard/view', <?php echo $viewed_proposal['proposal_count']; ?>],
      ['Declined', '<?php echo base_url(); ?>Firm_dashboard/decline',   <?php echo $declined_proposal['proposal_count']; ?>],
      ['Archived',  '<?php echo base_url(); ?>Firm_dashboard/archieve',  <?php echo $archive_proposal['proposal_count']; ?>],
      ['Draft',  '<?php echo base_url(); ?>Firm_dashboard/draft',  <?php echo $draft_proposal['proposal_count']; ?>]
    ]);     
   
     var view = new google.visualization.DataView(data);
      view.setColumns([0, 2]); 
     var options = {
         chartArea: {left:0, width: "80%"},
         height:250, 
        legend: { position: 'bottom', show: true, container: $("#large-legend"), alignment: 'start' },
        hAxis : { textStyle : { fontSize: 8} },
        vAxis : { textStyle : { fontSize: 8} },
          width: 350,
          height: 250
       // pieHole: 0.4,
      };  

     var chart = new google.visualization.PieChart(document.getElementById('piechart1'));   
         chart.draw(view, options);   
         var selectHandler = function(e) {
        //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
          window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
         }
         // Add our selection handler.
         google.visualization.events.addListener(chart, 'select', selectHandler);
      }
   
   //   var chart_div = new google.visualization.PieChart(document.getElementById('piechart1'));
   // var chart = new google.visualization.PieChart(piechart1);
   // google.visualization.events.addListener(chart, 'ready', function () {
   // chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
   // //document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '"></a>';
   // var image=chart.getImageURI();
   //  $(document).ready(function(){  
   // // alert(image);
   //            $.ajax({
   //                   type: "POST",
   //                   url: "<?php echo base_url();?>/Report/proposal_chart",         
   //                   data: {proposal_image: image},
   //                   success: 
   //                        function(data){
                 
   //                        }
   //                    });
   //             });
   // });
   // chart.draw(view, options);
   // var selectHandler = function(e) {
   //   //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
   //     window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
   //    }
   //    // Add our selection handler.
   //    google.visualization.events.addListener(chart, 'select', selectHandler);
   // }
   
</script>
<script type="text/javascript">
   google.charts.load('current', {'packages':['corechart']});
   google.charts.setOnLoadCallback(drawChart2);
   
   function drawChart2() {
   
      var data = google.visualization.arrayToDataTable([
       ['Leads','link','Count'],
             <?php      
      foreach($leads_section as $stat){
      $status_name=$stat['status_name'];
      $user=$_SESSION['user'];
      $client=$_SESSION['client'];
    //  echo $stat['id'];
      $counts=$this->AllFilter_model->leads_details($stat['id'],$client,$user);

      // echo'<pre>';
      // print_r($count); echo '</pre>'; ?>
            ['<?php echo $status_name; ?>','<?php echo base_url(); ?>/leads?status=<?php $strlower = strtolower(preg_replace("~[\\\\/:*?'&,<>|]~", '', $status_name));
      $lesg = str_replace(' ', '-', $strlower);
      $lesgs = str_replace('--', '-', $lesg); echo $lesgs; ?>',<?php echo $counts['lead_count']; ?> ],
            <?php }  ?>
      ]); 
          
     var view = new google.visualization.DataView(data);
       view.setColumns([0, 2]);
   
     var options = {
         chartArea: {left:0, width: "80%"},
         height:250, 
        legend: { position: 'bottom', show: true, container: $("#large-legend"), alignment: 'start' },
        hAxis : { textStyle : { fontSize: 8} },
        vAxis : { textStyle : { fontSize: 8} },
          width: 350,
          height: 250
       // pieHole: 0.4,
      };   
      var chart = new google.visualization.PieChart(document.getElementById('piechart_11'));   
         chart.draw(view, options);   
         var selectHandler = function(e) {
        //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
          window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
         }
         // Add our selection handler.
         google.visualization.events.addListener(chart, 'select', selectHandler);
      }
   
   // var chart_div = new google.visualization.PieChart(document.getElementById('piechart_1'));
   // var chart = new google.visualization.PieChart(piechart_1);
   // google.visualization.events.addListener(chart, 'ready', function () {
   // chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
   // //document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '"></a>';
   // var image=chart.getImageURI();
   //   $(document).ready(function(){  
   //  // alert(image);
   //             $.ajax({
   //                    type: "POST",
   //                    url: "<?php echo base_url();?>/Report/lead_chart",         
   //                    data: {lead_image: image},
   //                    success: 
   //                         function(data){
                  
   //                         }
   //                     });
   //              });
   // });
   // chart.draw(view, options);
   // var selectHandler = function(e) {
   //     //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
   //     window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
   //     }
   //     // Add our selection handler.
   //     google.visualization.events.addListener(chart, 'select', selectHandler);
   // }
   
</script>
<script type="text/javascript">
   google.charts.load('current', {'packages':['corechart']});
   google.charts.setOnLoadCallback(drawChart2);
   
   function drawChart2() {
   
     var data = google.visualization.arrayToDataTable([
       ['Service','link','Count'],             
        ['Confirmation Statement','<?php echo base_url(); ?>Firm_dashboard/confirm_statement',<?php echo $conf_statement['service_count']; ?> ],
        ['Accounts', '<?php echo base_url(); ?>Firm_dashboard/accounts',<?php echo $accounts['service_count']; ?> ],
        ['Company Tax Return','<?php echo base_url(); ?>Firm_dashboard/company_tax', <?php echo $company_tax_return['service_count']; ?> ],
        ['Personal Tax Return','<?php echo base_url(); ?>Firm_dashboard/personal_tax', <?php echo $personal_tax_return['service_count']; ?> ],
        ['VAT Returns','<?php echo base_url(); ?>Firm_dashboard/vat', <?php echo $vat['service_count']; ?> ],
        ['Payroll','<?php echo base_url(); ?>Firm_dashboard/payroll', <?php echo $payroll['service_count']; ?> ],
        ['WorkPlace Pension - AE','<?php echo base_url(); ?>Firm_dashboard/workplace', <?php echo $workplace['service_count']; ?> ],
        ['CIS - Contractor','<?php echo base_url(); ?>Firm_dashboard/cis',0 ],
        ['CIS - Sub Contractor','<?php echo base_url(); ?>Firm_dashboard/cis_sub',0 ],
        ['P11D','<?php echo base_url(); ?>Firm_dashboard/p11d', <?php echo $p11d['service_count']; ?> ],
        ['Management Accounts','<?php echo base_url(); ?>Firm_dashboard/management',  <?php echo $management['service_count']; ?> ],
        ['Bookkeeping','<?php echo base_url(); ?>Firm_dashboard/bookkeeping', <?php echo $bookkeep['service_count']; ?> ],
        ['Investigation Insurance','<?php echo base_url(); ?>Firm_dashboard/investication', <?php echo $investgate['service_count']; ?> ],
        ['Registered Address','<?php echo base_url(); ?>Firm_dashboard/register', <?php echo $registered['service_count']; ?> ],
        ['Tax Advice','<?php echo base_url(); ?>Firm_dashboard/tax',  <?php echo $taxadvice['service_count']; ?> ],
        ['Tax Investigation','<?php echo base_url(); ?>Firm_dashboard/tax_inves',  <?php echo $taxinvest['service_count']; ?> ],          
      ]);  
   
       var view = new google.visualization.DataView(data);
       view.setColumns([0, 2]);    
   
      var options = {
         chartArea: {left:0, width: "80%"},
         height:250, 
        legend: { position: 'bottom', show: true, container: $("#large-legend"), alignment: 'start' },
        hAxis : { textStyle : { fontSize: 8} },
        vAxis : { textStyle : { fontSize: 8} },
          width: 350,
          height: 250
       // pieHole: 0.4,
      };  
      var chart = new google.visualization.PieChart(document.getElementById('piechart_service'));   
         chart.draw(view, options);   
         var selectHandler = function(e) {
        //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
          window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
         }
         // Add our selection handler.
         google.visualization.events.addListener(chart, 'select', selectHandler);
      }
   
   // var chart_div = new google.visualization.PieChart(document.getElementById('piechart_service'));
   // var chart = new google.visualization.PieChart(piechart_service);
   // google.visualization.events.addListener(chart, 'ready', function () {
   // chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
   // //document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '"></a>';
   // var image=chart.getImageURI();
   //   $(document).ready(function(){  
   //  // alert(image);
   //             $.ajax({
   //                    type: "POST",
   //                    url: "<?php echo base_url();?>/Report/service_chart",         
   //                    data: {service_image: image},
   //                    success: 
   //                         function(data){
                  
   //                         }
   //                     });
   //              });
   // });
   // chart.draw(view, options);
   // var selectHandler = function(e) {
   //     //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
   //     window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
   //     }
   //     // Add our selection handler.
   //     google.visualization.events.addListener(chart, 'select', selectHandler);
   //     }
   
</script>
<script type="text/javascript">
   google.charts.load('current', {'packages':['corechart']});
   google.charts.setOnLoadCallback(drawChart2);
   
   function drawChart2() {
   
     var data = google.visualization.arrayToDataTable([
       ['Service', 'link','Count'],             
        ['Confirmation Statement','<?php echo base_url(); ?>Firm_dashboard/confirm_statement',<?php echo $de_conf_statement['dead_count']; ?> ],
        ['Accounts','<?php echo base_url(); ?>Firm_dashboard/accounts', <?php echo $de_accounts['dead_count']; ?> ],
        ['Company Tax Return','<?php echo base_url(); ?>Firm_dashboard/company_tax', <?php echo $de_company_tax_return['dead_count']; ?> ],
        ['Personal Tax Return','<?php echo base_url(); ?>Firm_dashboard/personal_tax', <?php echo $de_personal_tax_return['dead_count']; ?> ],
        ['VAT Returns','<?php echo base_url(); ?>Firm_dashboard/vat', <?php echo $de_vat['dead_count']; ?> ],
        ['Payroll','<?php echo base_url(); ?>Firm_dashboard/payroll', <?php echo $de_payroll['dead_count']; ?> ],
        ['WorkPlace Pension - AE','<?php echo base_url(); ?>Firm_dashboard/workplace', <?php echo $de_workplace['dead_count']; ?> ],
        ['CIS - Contractor','<?php echo base_url(); ?>Firm_dashboard/cis',0],
        ['CIS - Sub Contractor','<?php echo base_url(); ?>Firm_dashboard/cis_sub',0],
        ['P11D','<?php echo base_url(); ?>Firm_dashboard/p11d', <?php echo $de_p11d['dead_count']; ?> ],
        ['Management Accounts','<?php echo base_url(); ?>Firm_dashboard/management', <?php echo $de_management['dead_count']; ?> ],
        ['Bookkeeping','<?php echo base_url(); ?>Firm_dashboard/bookkeeping', <?php echo $de_bookkeep['dead_count']; ?> ],
        ['Investigation Insurance','<?php echo base_url(); ?>Firm_dashboard/investication', <?php echo $de_investgate['dead_count']; ?> ],
        ['Registered Address','<?php echo base_url(); ?>Firm_dashboard/register', <?php echo $de_registered['dead_count']; ?> ],
        ['Tax Advice','<?php echo base_url(); ?>Firm_dashboard/tax', <?php echo $de_taxadvice['dead_count']; ?> ],
        ['Tax Investigation','<?php echo base_url(); ?>Firm_dashboard/tax_inves', <?php echo $de_taxinvest['dead_count']; ?> ]         
      ]);      
     var view = new google.visualization.DataView(data);
       view.setColumns([0, 2]);
   
    var options = {
         chartArea: {left:0, width: "80%"},
         height:250, 
        legend: { position: 'bottom', show: true, container: $("#large-legend"), alignment: 'start' },
        hAxis : { textStyle : { fontSize: 8} },
        vAxis : { textStyle : { fontSize: 8} },
          width: 350,
          height: 250
       // pieHole: 0.4,
      };  
      
      var chart = new google.visualization.PieChart(document.getElementById('piechart_deadline'));   
         chart.draw(view, options);   
         var selectHandler = function(e) {
        //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
          window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
         }
         // Add our selection handler.
         google.visualization.events.addListener(chart, 'select', selectHandler);
      }
   
   // var chart_div = new google.visualization.PieChart(document.getElementById('piechart_deadline'));
   // var chart = new google.visualization.PieChart(piechart_deadline);
   // google.visualization.events.addListener(chart, 'ready', function () {
   // chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
   // //document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '"></a>';
   // var image=chart.getImageURI();
   //   $(document).ready(function(){  
   //  // alert(image);
   //             $.ajax({
   //                    type: "POST",
   //                    url: "<?php echo base_url();?>/Report/deadline_chart",         
   //                    data: {deadline_image: image},
   //                    success: 
   //                         function(data){
                  
   //                         }
   //                     });
   //              });
   // });
   // chart.draw(view, options);
   // var selectHandler = function(e) {       
   //     window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
   //     }      
   //     google.visualization.events.addListener(chart, 'select', selectHandler);
   //     }
   
</script>
<script type="text/javascript">
   google.charts.load('current', {'packages':['corechart']});
   google.charts.setOnLoadCallback(drawChart2);
   
   function drawChart2() {  
     var data = google.visualization.arrayToDataTable([
       ['Service', 'link','Count'],             
        ['Approved','<?php echo base_url(); ?>invoice',<?php echo $approved_invoice['invoice_count']; ?> ],
        ['Cancelled','<?php echo base_url(); ?>invoice', <?php echo $cancel_invoice['invoice_count']; ?> ],
        ['Expired','<?php echo base_url(); ?>invoice', <?php echo $expired_invoice['invoice_count']; ?> ]               
      ]);      
     var view = new google.visualization.DataView(data);
       view.setColumns([0, 2]);
   
      var options = {
         chartArea: {left:0, width: "80%"},
         height:250, 
        legend: { position: 'bottom', show: true, container: $("#large-legend"), alignment: 'start' },
        hAxis : { textStyle : { fontSize: 8} },
        vAxis : { textStyle : { fontSize: 8} },
          width: 350,
          height: 250
       // pieHole: 0.4,
      };  

      var chart = new google.visualization.PieChart(document.getElementById('piechart_invoice'));   
         chart.draw(view, options);   
         var selectHandler = function(e) {
        //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
          window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
         }
         // Add our selection handler.
         google.visualization.events.addListener(chart, 'select', selectHandler);
      }
   
   // var chart_div = new google.visualization.PieChart(document.getElementById('piechart_invoice'));
   // var chart = new google.visualization.PieChart(piechart_invoice);
   // google.visualization.events.addListener(chart, 'ready', function () {
   // chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
   // //document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '"></a>';
   // var image=chart.getImageURI();
   //   $(document).ready(function(){  
   //  // alert(image);
   //             $.ajax({
   //                    type: "POST",
   //                    url: "<?php echo base_url();?>Filter/invoice_chart",         
   //                    data: {invoice_image: image},
   //                    success: 
   //                         function(data){
                  
   //                         }
   //                     });
   //              });
   // });
   // chart.draw(view, options);
   // var selectHandler = function(e) {       
   //     window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
   //     }      
   //     google.visualization.events.addListener(chart, 'select', selectHandler);
   //     }
   
</script>
<!-- Proposal Chart -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/jquery.tagsinput.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/email.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> 
<script type="text/javascript" language="javascript" src="<?php echo base_url()?>assets/js/jquery.tagsinput.js"></script>
<script type="text/javascript">
   $(document).ready(function(){ 
       $( function() {
           $( ".date-picker" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();      
       });
   $( function() {
            $( ".date_picker2" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
       });
   $( function() {
          $( ".date_picker1" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
       }); 
     });
   
   $("#client_customisation").click(function(){
    var fromdate=$("#from_date").val();
    var todate=$("#to_date").val();
    var legal_form=$("#legal_form").val();
    var status=$("#status").val();
    var formData={'fromdate':fromdate,'todate':todate,'legal_form':legal_form,'status':status}
   $.ajax({
         type: "POST",
         url: "<?php echo base_url();?>/Reports/Client_customization",         
         data: formData,
         beforeSend: function() {
           $(".LoadingImage").show();
         },
         success: function(data) {      
             $(".LoadingImage").hide(); 
             if(data!='1'){         
               $(".fileters_client1").html('');
               $(".fileters_client1").append(data); 
             }       
               $("#client_customization").modal('hide'); 
               $(".prodiv12").mintScrollbar({
onChange: null,
axis: "auto",
wheelSpeed: 120,
disableOnMobile: true
});     
            $("#client_download_sess").modal({show:true, backdrop: 'static', keyboard: false});  
                     
         }
       });
   });
   
   $("#task_customisation").click(function(){
    var fromdate=$("#task_from_date").val();
    var todate=$("#task_to_date").val();
    var priority=$("#priority").val();
    var status=$("#task_status").val();
    var formData={'fromdate':fromdate,'todate':todate,'priority':priority,'status':status}
   $.ajax({
         type: "POST",
         url: "<?php echo base_url();?>/Reports/Task_customization",         
         data: formData,
         beforeSend: function() {
           $(".LoadingImage").show();
         },
         success: function(data) {      
             $(".LoadingImage").hide(); 
             if(data!='1'){         
               $(".fileters_Tasks1").html('');
               $(".fileters_Tasks1").append(data); 
             }  
             $(".prodiv12").mintScrollbar({
onChange: null,
axis: "auto",
wheelSpeed: 120,
disableOnMobile: true
});      
               $("#task_customization").modal('hide');       
               $("#task_download_sess").modal({show:true, backdrop: 'static', keyboard: false}); 
         }
       });
   });
   $("#proposal_customisation").click(function(){
    var fromdate=$("#proposal_from_date").val();
    var todate=$("#proposal_to_date").val();
    var status=$("#proposal_status").val();
    var formData={'fromdate':fromdate,'todate':todate,'status':status}
   $.ajax({
         type: "POST",
         url: "<?php echo base_url();?>/Report/Proposal_customization",         
         data: formData,
         beforeSend: function() {
           $(".LoadingImage").show();
         },
         success: function(data) {    
             $(".LoadingImage").hide(); 
             if(data!='1'){         
               $(".filter_proposal1").html('');
               $(".filter_proposal1").append(data); 
             }  
             $(".prodiv12").mintScrollbar({
onChange: null,
axis: "auto",
wheelSpeed: 120,
disableOnMobile: true
});     
               $("#proposal_customization").modal('hide');       
               $("#proposal_download_sess").modal({show:true, backdrop: 'static', keyboard: false});
         }
       });
   });
   $("#leads_customisation").click(function(){
    var fromdate=$("#leads_from_date").val();
    var todate=$("#leads_to_date").val();
    var status=$("#leads_status").val();
    var formData={'fromdate':fromdate,'todate':todate,'status':status}
   $.ajax({
         type: "POST",
         url: "<?php echo base_url();?>/Report/Leads_customization",         
         data: formData,
         beforeSend: function() {
           $(".LoadingImage").show();
         },
         success: function(data) {     
             $(".LoadingImage").hide(); 
             if(data!='1'){         
               $(".filter_leads1").html('');
               $(".filter_leads1").append(data); 
             }    
             $(".prodiv12").mintScrollbar({
onChange: null,
axis: "auto",
wheelSpeed: 120,
disableOnMobile: true
});     
               $("#leads_customization").modal('hide');       
               $("#leads_download_sess").modal({show:true, backdrop: 'static', keyboard: false});      
         }
       });
   });
   
   $("#service_customisation").click(function(){
    var fromdate=$("#service_from_date").val();
    var todate=$("#service_to_date").val();
    var status=$("#service").val();
    var formData={'fromdate':fromdate,'todate':todate,'status':status}
   $.ajax({
         type: "POST",
         url: "<?php echo base_url();?>/Report/service_customization",         
         data: formData,
         beforeSend: function() {
           $(".LoadingImage").show();
         },
         success: function(data) {     
             $(".LoadingImage").hide(); 
             if(data!='1'){         
               $(".filter_service1").html('');
               $(".filter_service1").append(data); 
             }    
             $(".prodiv12").mintScrollbar({
onChange: null,
axis: "auto",
wheelSpeed: 120,
disableOnMobile: true
});    
               $("#service_customization").modal('hide');  
               $("#service_download_sess").modal({show:true, backdrop: 'static', keyboard: false});   
         }
       });
   });
   
   $("#deadline_customisation").click(function(){
    var fromdate=$("#deadline_from_date").val();
    var todate=$("#deadline_to_date").val();
    var company_type=$('#company_type').val();
    var deadline_type=$("#deadline_type").val();
    var formData={'fromdate':fromdate,'todate':todate,'company_type':company_type,'deadline_type':deadline_type}
    $.ajax({
         type: "POST",
         url: "<?php echo base_url();?>/Report/deadline_customization",         
         data: formData,
         beforeSend: function() {
           $(".LoadingImage").show();
         },
         success: function(data) {      
             $(".LoadingImage").hide(); 
             if(data!='1'){         
               $(".filter_deadline1").html('');
               $(".filter_deadline1").append(data); 
             }     
             $(".prodiv12").mintScrollbar({
onChange: null,
axis: "auto",
wheelSpeed: 120,
disableOnMobile: true
});
               $("#deadline_customization").modal('hide');  
               $("#deadline_download_sess").modal({show:true, backdrop: 'static', keyboard: false});
         }
       });
   });
   
   $("#summary_customisation").click(function(){
    var fromdate=$("#summary_from_date").val();
    var todate=$("#summary_to_date").val();
     var summary_option=$('#summary_option').val();
    var formData={'fromdate':fromdate,'todate':todate,'summary_option':summary_option}
    $.ajax({
         type: "POST",
         url: "<?php echo base_url();?>/Filter/summary_customization",         
         data: formData,
         beforeSend: function() {
           $(".LoadingImage").show();
         },
         success: function(data) {      
             $(".LoadingImage").hide();          
               $("#summary_customization").modal('hide');  
               $("#summary_download_sess").modal({show:true, backdrop: 'static', keyboard: false}); 
               $(".prodiv12").mintScrollbar({
onChange: null,
axis: "auto",
wheelSpeed: 120,
disableOnMobile: true
});         
         }
       });
   });
      $("#filter_client").change(function(){
      if($(this).val()=='month'){
      id=$(this).val();
      $.ajax({
       type: "POST",
       url: "<?php echo base_url(); ?>Filter/client_filter",
       data:  { 'id':id }, 
       beforeSend: function() {
         $(".LoadingImage").show();
       },
       success: function(data){    
         $(".fileters_client").html('');
          $(".fileters_client").append(data);   
          $(".LoadingImage").hide();   
   //            $(".prodiv12").niceScroll({
   
   //  zindex: "auto",
   //   cursoropacitymin: 1,
   //   cursoropacitymax: 0,
   //   cursorcolor: "#ccc",
   //   cursorwidth: "6px",
   //   cursorborder: "1px solid #fff",
   //   cursorborderradius: "5px",
   //   scrollspeed: 60,
   //   mousescrollstep: 8 * 3,
   //   // options here
   
   // });  
        }
       });   
      }else{
      $.ajax({
       type: "POST",
       url: "<?php echo base_url(); ?>Filter/client_filter_year",
       data:  { 'id':id }, 
       beforeSend: function() {
         $(".LoadingImage").show();
       },
       success: function(data){   
         $(".fileters_client").html('');
          $(".fileters_client").append(data);   
          $(".LoadingImage").hide();     
       }
       });   
      }
      });
   
   
       $("#filter_task").change(function(){
      if($(this).val()=='month'){
      id=$(this).val();
      $.ajax({
       type: "POST",
       url: "<?php echo base_url(); ?>Filter/task_filter",
       data:  { 'id':id }, 
       beforeSend: function() {
         $(".LoadingImage").show();
       },
       success: function(data){     
         $(".fileters_Tasks").html('');
         $(".fileters_Tasks").append(data);   
         $(".LoadingImage").hide();     
       }
       });   
      }else{
      $.ajax({
       type: "POST",
       url: "<?php echo base_url(); ?>Filter/task_filter_year",
       data:  { 'id':id }, 
       beforeSend: function() {
         $(".LoadingImage").show();
       },
       success: function(data){    
         $(".fileters_Tasks").html('');
          $(".fileters_Tasks").append(data);   
          $(".LoadingImage").hide();     
         }
       });   
      }
      });
   
   
     $("#filter_proposal").change(function(){
      if($(this).val()=='month'){
      id=$(this).val();
      $.ajax({
       type: "POST",
       url: "<?php echo base_url(); ?>Filter/proposal_filter",
       data:  { 'id':id }, 
       beforeSend: function() {
         $(".LoadingImage").show();
       },
       success: function(data){   
         $(".filter_proposal").html('');
         $(".filter_proposal").append(data);   
         $(".LoadingImage").hide();     
       }
       });   
      }else{
      $.ajax({
       type: "POST",
       url: "<?php echo base_url(); ?>Filter/proposal_filter_year",
       data:  { 'id':id }, 
       beforeSend: function() {
         $(".LoadingImage").show();
       },
       success: function(data){   
         $(".filter_proposal").html('');
          $(".filter_proposal").append(data);   
          $(".LoadingImage").hide();     
       }
       });
      
      }
      });
   
   
   
     $("#filter_leads").change(function(){
      if($(this).val()=='month'){
      id=$(this).val();
      $.ajax({
       type: "POST",
       url: "<?php echo base_url(); ?>Filter/leads_filter",
       data:  { 'id':id }, 
       beforeSend: function() {
         $(".LoadingImage").show();
       },
       success: function(data){    
         $(".filter_leads").html('');
         $(".filter_leads").append(data);   
         $(".LoadingImage").hide();     
       }
       });   
      }else{
      $.ajax({
       type: "POST",
       url: "<?php echo base_url(); ?>Filter/leads_filter_year",
       data:  { 'id':id }, 
       beforeSend: function() {
         $(".LoadingImage").show();
       },
       success: function(data){
       //  console.log(data);   
         $(".filter_leads").html('');
          $(".filter_leads").append(data);   
          $(".LoadingImage").hide();
        
       }
       });
      
      }
      });
   
       $("#filter_service").change(function(){
      if($(this).val()=='month'){
      id=$(this).val();
      $.ajax({
       type: "POST",
       url: "<?php echo base_url(); ?>Filter/service_filter",
       data:  { 'id':id }, 
       beforeSend: function() {
         $(".LoadingImage").show();
       },
       success: function(data){
         console.log(data);   
         $(".filter_service").html('');
         $(".filter_service").append(data);   
         $(".LoadingImage").hide();
   //       $(".prodiv12").niceScroll({
   
   //  zindex: "auto",
   //   cursoropacitymin: 1,
   //   cursoropacitymax: 0,
   //   cursorcolor: "#ccc",
   //   cursorwidth: "6px",
   //   cursorborder: "1px solid #fff",
   //   cursorborderradius: "5px",
   //   scrollspeed: 60,
   //   mousescrollstep: 8 * 3,
   //   // options here
   
   // }); 
        
       }
       });
      
      }else{
      $.ajax({
       type: "POST",
       url: "<?php echo base_url(); ?>Filter/service_filter_year",
       data:  { 'id':id }, 
       beforeSend: function() {
         $(".LoadingImage").show();
       },
       success: function(data){
        console.log(data);   
         $(".filter_service").html('');
          $(".filter_service").append(data);   
          $(".LoadingImage").hide();
        
       }
       });
      
      }
      });
   
          $("#filter_deadline").change(function(){
      if($(this).val()=='month'){
      id=$(this).val();
      $.ajax({
       type: "POST",
       url: "<?php echo base_url(); ?>Filter/deadline_filter",
       data:  { 'id':id }, 
       beforeSend: function() {
         $(".LoadingImage").show();
       },
       success: function(data){
        // console.log(data);   
         $(".filter_deadline").html('');
         $(".filter_deadline").append(data);   
         $(".LoadingImage").hide();
        
       }
       });
      
      }else{
      $.ajax({
       type: "POST",
       url: "<?php echo base_url(); ?>Filter/deadline_filter_year",
       data:  { 'id':id }, 
       beforeSend: function() {
         $(".LoadingImage").show();
       },
       success: function(data){
       //  console.log(data);   
         $(".filter_deadline").html('');
          $(".filter_deadline").append(data);   
          $(".LoadingImage").hide();
        
       }
       });
      
      }
      });
   
      $("#filter_invoice").change(function(){
      if($(this).val()=='month'){
      id=$(this).val();
      $.ajax({
       type: "POST",
       url: "<?php echo base_url(); ?>Filter/invoice_filter",
       data:  { 'id':id }, 
       beforeSend: function() {
         $(".LoadingImage").show();
       },
       success: function(data){    
         $(".fileters_invoice").html('');
         $(".fileters_invoice").append(data);   
         $(".LoadingImage").hide();    
       }
       });   
      }else{
      $.ajax({
       type: "POST",
       url: "<?php echo base_url(); ?>Filter/invoice_filter_year",
       data:  { 'id':id }, 
       beforeSend: function() {
         $(".LoadingImage").show();
       },
       success: function(data){    
         $(".fileters_invoice").html('');
          $(".fileters_invoice").append(data);   
          $(".LoadingImage").hide();    
       }
       });
      
      }
      });
          
   
   
     function exportexcel()
   {
       var url="<?php echo base_url(); ?>Reports/excel_download";   
       window.location = url;  
   }
   
     function exportexcel_sess()
   {
       var url="<?php echo base_url(); ?>Reports/excel_download_sess";   
       window.location = url;  
   }
   
   function exporttaskexcel(){
       var url="<?php echo base_url(); ?>Reports/exceltask_download";   
       window.location = url; 
   }
   
   function exporttaskexcel_sess(){
       var url="<?php echo base_url(); ?>Reports/exceltask_download_sess";   
       window.location = url; 
   }
   
   function exportproposalexcel(){
       var url="<?php echo base_url(); ?>Report/excelproposal_download";   
       window.location = url; 
   }
   
   function exportproposalexcel_sess(){
       var url="<?php echo base_url(); ?>Report/excelproposal_download_sess";   
       window.location = url; 
   }
   
   function exportleadsexcel(){
       var url="<?php echo base_url(); ?>Report/excelleads_download";   
       window.location = url; 
   }
   
   function exportleadsexcel_sess(){
       var url="<?php echo base_url(); ?>Report/excelleads_download_sess";   
       window.location = url; 
   }
   
   function service_exportexcel(){
       var url="<?php echo base_url(); ?>Report/excelservice_download";   
       window.location = url; 
   }
   
   function service_exportexcel1(){
       var url="<?php echo base_url(); ?>Report/excelservice_download_sess";   
       window.location = url; 
   }
   
   function deadline_exportexcel(){
      var url="<?php echo base_url(); ?>Report/exceldeadline_download";   
       window.location = url; 
   }
   
   function deadline_exportexcel_sess(){
      var url="<?php echo base_url(); ?>Reports/exceldeadline_download_sess";   
       window.location = url; 
   }
   
   function exportexcel_summary(){
      var url="<?php echo base_url(); ?>Filter/excelsummary_download";   
       window.location = url; 
   }
   function exportexcel_summary_sess(){
      var url="<?php echo base_url(); ?>Filter/excelsummary_download_sess";   
       window.location = url; 
   }
   
   function invoice_exportexcel(){
      var url="<?php echo base_url(); ?>Filter/excelinvoice_download";   
       window.location = url; 
   }
   
   function invoice_exportexcel_sess(){
      var url="<?php echo base_url(); ?>Filter/excelinvoice_download_sess";   
       window.location = url; 
   }
   
   
   $("#invoice_customisation").click(function(){
    var fromdate=$("#invoice_from_date").val();
    var todate=$("#invoice_to_date").val();
    var status=$('#invoice_status').val();
    var formData={'fromdate':fromdate,'todate':todate,'status':status}
    $.ajax({
         type: "POST",
         url: "<?php echo base_url();?>Filter/invoice_customization",         
         data: formData,
         beforeSend: function() {
           $(".LoadingImage").show();
         },
         success: function(data) {      
             $(".LoadingImage").hide(); 
             if(data!='1'){         
               $(".fileters_invoice1").html('');
               $(".fileters_invoice1").append(data); 
             }     
               $("#invoice_customization").modal('hide');  
               $("#invoice_download_sess").modal({show:true, backdrop: 'static', keyboard: false});
         }
       });
   });
   //Tab Details Section 
   
   google.charts.load("current", {packages:["corechart"]});
   google.charts.setOnLoadCallback(drawChart);

   function drawChart() {
     var data = google.visualization.arrayToDataTable([
       ['Legal Form','link','Count'],
       ['Private Limited company', '<?php echo base_url();?>Firm_dashboard/private_function', <?php echo $Private['limit_count']; ?>],
       ['Public Limited company',  '<?php echo base_url();?>Firm_dashboard/public_function',   <?php echo $Public['limit_count']; ?>],
       ['Limited Liability Partnership','<?php echo base_url();?>Firm_dashboard/limited_function', <?php echo $limited['limit_count']; ?>],
       ['Partnership', '<?php echo base_url();?>Firm_dashboard/partnership', <?php echo $Partnership['limit_count']; ?>],
       ['Self Assessment','<?php echo base_url();?>Firm_dashboard/self_assesment',   <?php echo $self['limit_count']; ?>],
       ['Trust','<?php echo base_url();?>Firm_dashboard/trust',<?php echo $Trust['limit_count']; ?>],
       ['Charity','<?php echo base_url();?>Firm_dashboard/charity',<?php echo $Charity['limit_count']; ?>],
        ['Other','<?php echo base_url();?>Firm_dashboard/other',<?php echo $Other['limit_count']; ?>],
     ]);  
   
      var view = new google.visualization.DataView(data);
          view.setColumns([0, 2]);
    
   
     var options = {
      chartArea: {'width': '100%', 'height': '80%'},       
       legend: { position: 'bottom' },
       hAxis : { textStyle : { fontSize: 8} },
        vAxis : { textStyle : { fontSize: 8} },
     };  
         var chart = new google.visualization.PieChart(document.getElementById('Tabs_client'));   
         chart.draw(view, options);   
         var selectHandler = function(e) {
        //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
          window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
         }
         // Add our selection handler.
         google.visualization.events.addListener(chart, 'select', selectHandler);
      }
   
</script>
<script type="text/javascript">
   google.charts.load("current", {packages:["corechart"]});
   google.charts.setOnLoadCallback(drawChart);
   function drawChart() {
   var data = google.visualization.arrayToDataTable([
    ['user status','link','Count'],
    ['active', '<?php echo base_url();?>Firm_dashboard/active', <?php echo $active_user_count['activeuser_count']; ?>],
    ['inactive',  '<?php echo base_url();?>Firm_dashboard/inactive',   <?php echo $inactive_user_count['inactiveuser_count']; ?>],
    ['frozen','<?php echo base_url();?>Firm_dashboard/frozen', <?php echo $frozen_user_count['frozenuser_count']; ?>],     
   ]);  
   
   var view = new google.visualization.DataView(data);
       view.setColumns([0, 2]);
   
   
   var options = {
   chartArea: {'width': '100%', 'height': '80%'},        
    legend: { position: 'bottom' },
    hAxis : { textStyle : { fontSize: 8} },
    vAxis : { textStyle : { fontSize: 8} },
   };  
   var chart = new google.visualization.PieChart(document.getElementById('Tabs_Users'));   
      chart.draw(view, options);   
      var selectHandler = function(e) {
     //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
       window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
      }
      // Add our selection handler.
      google.visualization.events.addListener(chart, 'select', selectHandler);
   }
</script>
<script type="text/javascript">
   google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task','link','Count'],
          ['Not started', '<?php echo base_url(); ?>Firm_dashboard/notstarted',   <?php echo $notstarted_tasks['task_count']; ?>],
          ['Completed',  '<?php echo base_url(); ?>Firm_dashboard/complete',    <?php echo $completed_tasks['task_count']; ?>],
          ['Inprogress','<?php echo base_url(); ?>Firm_dashboard/inprogress',  <?php echo $inprogress_tasks['task_count']; ?>],
          ['Awaiting Feedback','<?php echo base_url(); ?>Firm_dashboard/awaited', <?php echo $awaiting_tasks['task_count']; ?>],
          ['Testing',  '<?php echo base_url(); ?>Firm_dashboard/tested', <?php echo $testing_tasks['task_count']; ?>]    
        ]);  
     
         var view = new google.visualization.DataView(data);
             view.setColumns([0, 2]);
       
     
        var options = {
           chartArea: {left: 0},     
          legend: { position: 'bottom' },
       hAxis : { textStyle : { fontSize: 8} },
        vAxis : { textStyle : { fontSize: 8} },
          // legend: { position: 'bottom' },
          // hAxis : { textStyle : { fontSize: 8} },
          // vAxis : { textStyle : { fontSize: 8} },
         // pieHole: 0.4,
        };  
         var chart = new google.visualization.PieChart(document.getElementById('Tabs_task'));   
           chart.draw(view, options);   
           var selectHandler = function(e) {
          //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
            window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
           }
           // Add our selection handler.
           google.visualization.events.addListener(chart, 'select', selectHandler);
        }
</script>
<script type="text/javascript">  
   google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task','link','Count'],
          ['Low', '<?php echo base_url(); ?>Firm_dashboard/low',   <?php echo $low_tasks['task_count']; ?>],
          ['High',  '<?php echo base_url(); ?>Firm_dashboard/high',    <?php echo $high_tasks['task_count']; ?>],
          ['Medium','<?php echo base_url(); ?>Firm_dashboard/medium',  <?php echo $medium_tasks['task_count']; ?>],
          ['Super Urgent','<?php echo base_url(); ?>Firm_dashboard/super_urgent', <?php echo $super_urgent_tasks['task_count']; ?>]   
        ]);  
     
         var view = new google.visualization.DataView(data);
             view.setColumns([0, 2]);
       
     
        var options = {
           chartArea: {left: 0},     
          legend: { position: 'bottom' },
       hAxis : { textStyle : { fontSize: 8} },
        vAxis : { textStyle : { fontSize: 8} },
          // legend: { position: 'bottom' },
          // hAxis : { textStyle : { fontSize: 8} },
          // vAxis : { textStyle : { fontSize: 8} },
         // pieHole: 0.4,
        };  
         var chart = new google.visualization.PieChart(document.getElementById('Tabs_priority'));   
           chart.draw(view, options);   
           var selectHandler = function(e) {
          //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
            window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
           }
           // Add our selection handler.
           google.visualization.events.addListener(chart, 'select', selectHandler);
        }
</script>
<script type="text/javascript">
   google.charts.load('current', {'packages':['corechart']});
   google.charts.setOnLoadCallback(drawChart);
   
   function drawChart() {
   var data = google.visualization.arrayToDataTable([
   <?php
      $start_array=array();
      $end_array=array();
      $start_montharray=array();
      $end_montharray=array();
      $j=1;
      foreach($task_list_this_month as $value){ 
            $start_date=explode('-',$value['start_date']);          
            $start=$start_date['2']; 
            $start_month=$start_date['1'];
            $end_date=explode('-',$value['end_date']);
            $end=$end_date['2'];
            $end_month=$end_date['1'];          }
            $month['month']=array(); 
            $month['start_date']=array(); 
            $month['end_date']=array(); 
            $month_other=array(); 
       foreach($task_list_this_month as $value){ 
            $start_date=explode('-',$value['start_date']);          
            $start=$start_date['2']; 
            $start_month=$start_date['1'];
            $end_date=explode('-',$value['end_date']);
            $end=$end_date['2'];
            $end_month=$end_date['1']; ?> 
   <?php  if(date('m')=='01'){ ?>
    ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='01'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end;  } ?>],
      <?php } if(date('m')=='02'){ ?>
   ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='02'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
     <?php } if(date('m')=='03'){ ?>
   ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='03'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
     <?php } if(date('m')=='04'){ ?>
   ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='04'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
     <?php } if(date('m')=='05'){ ?>
   ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='05'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],   
   <?php } ?>
   <?php  if(date('m')=='06'){ ?>
   ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='06'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
   <?php } ?>
     <?php  if(date('m')=='07'){ ?>
     ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='07'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
     <?php } ?>
       <?php  if(date('m')=='08'){ ?>
   ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='08'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
     <?php } if(date('m')=='09'){ ?>
   ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='09'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
     <?php } if(date('m')=='10'){ ?>
   ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='10'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
     <?php } if(date('m')=='11'){ ?>
   ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='11'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
     <?php } if(date('m')=='12'){ ?>
   ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='12'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
     <?php  } ?>
   
   <?php } ?>     
   ], true);
   var options = {
   legend:'none',
   'width':1200,
   'height':500, 
    candlestick: {
               fallingColor: { strokeWidth: 0, fill: '#3366cc' }, // red
               risingColor: { strokeWidth: 0, fill: '#3366cc' }   // green
            }
   };
   var chart = new google.visualization.CandlestickChart(document.getElementById('monthly_task'));
   chart.draw(data, options);
   }
</script>
<script type="text/javascript">
   google.charts.load('current', {'packages':['corechart']});
   google.charts.setOnLoadCallback(drawChart2);   
   function drawChart2() {   
     var data = google.visualization.arrayToDataTable([
       ['Service','link','Count'],             
        ['Confirmation Statement','<?php echo base_url(); ?>Firm_dashboard/confirm_statement',<?php echo $conf_statement['service_count']; ?> ],
        ['Accounts', '<?php echo base_url(); ?>Firm_dashboard/accounts',<?php echo $accounts['service_count']; ?> ],
        ['Company Tax Return','<?php echo base_url(); ?>Firm_dashboard/company_tax', <?php echo $company_tax_return['service_count']; ?> ],
        ['Personal Tax Return','<?php echo base_url(); ?>Firm_dashboard/personal_tax', <?php echo $personal_tax_return['service_count']; ?> ],
        ['VAT Returns','<?php echo base_url(); ?>Firm_dashboard/vat', <?php echo $vat['service_count']; ?> ],
        ['Payroll','<?php echo base_url(); ?>Firm_dashboard/payroll', <?php echo $payroll['service_count']; ?> ],
        ['WorkPlace Pension - AE','<?php echo base_url(); ?>Firm_dashboard/workplace', <?php echo $workplace['service_count']; ?> ],
        ['CIS - Contractor','<?php echo base_url(); ?>Firm_dashboard/cis', <?php echo $cis['service_count']; ?> ],
        ['CIS - Sub Contractor','<?php echo base_url(); ?>Firm_dashboard/cis_sub', <?php echo $cissub['service_count']; ?> ],
        ['P11D','<?php echo base_url(); ?>Firm_dashboard/p11d', <?php echo $p11d['service_count']; ?> ],
        ['Management Accounts','<?php echo base_url(); ?>Firm_dashboard/management',  <?php echo $management['service_count']; ?> ],
        ['Bookkeeping','<?php echo base_url(); ?>Firm_dashboard/bookkeeping', <?php echo $bookkeep['service_count']; ?> ],
        ['Investigation Insurance','<?php echo base_url(); ?>Firm_dashboard/investication', <?php echo $investgate['service_count']; ?> ],
        ['Registered Address','<?php echo base_url(); ?>Firm_dashboard/register', <?php echo $registered['service_count']; ?> ],
        ['Tax Advice','<?php echo base_url(); ?>Firm_dashboard/tax',  <?php echo $taxadvice['service_count']; ?> ],
        ['Tax Investigation','<?php echo base_url(); ?>Firm_dashboard/tax_inves',  <?php echo $taxinvest['service_count']; ?> ],          
      ]);  
   
       var view = new google.visualization.DataView(data);
       view.setColumns([0, 2]);    
   
     var options = {
          chartArea: {left: 0, top: 0, width: "100%", height: "100%"}, 
          legend: { position: 'bottom' },
       hAxis : { textStyle : { fontSize: 8} },
        vAxis : { textStyle : { fontSize: 8} },
   // legend: { position: 'bottom' },
   //  hAxis : { textStyle : { fontSize: 8} },
   //  vAxis : { textStyle : { fontSize: 8} },
   //  pieHole: 0.4,
     };
   
    var chart = new google.visualization.PieChart(document.getElementById('Tabs_service'));   
         chart.draw(view, options);   
         var selectHandler = function(e) {
        //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
          window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
         }
         // Add our selection handler.
         google.visualization.events.addListener(chart, 'select', selectHandler);
      }
</script>
<script type="text/javascript">
   google.charts.load('current', {'packages':['corechart']});
   google.charts.setOnLoadCallback(drawChart2);   
   function drawChart2() {   
     var data = google.visualization.arrayToDataTable([
       ['Service', 'link','Count'],             
        ['Confirmation Statement','<?php echo base_url(); ?>Firm_dashboard/confirm_statement',<?php echo $de_conf_statement['dead_count']; ?> ],
        ['Accounts','<?php echo base_url(); ?>Firm_dashboard/accounts', <?php echo $de_accounts['dead_count']; ?> ],
        ['Company Tax Return','<?php echo base_url(); ?>Firm_dashboard/company_tax', <?php echo $de_company_tax_return['dead_count']; ?> ],
        ['Personal Tax Return','<?php echo base_url(); ?>Firm_dashboard/personal_tax', <?php echo $de_personal_tax_return['dead_count']; ?> ],
        ['VAT Returns','<?php echo base_url(); ?>Firm_dashboard/vat', <?php echo $de_vat['dead_count']; ?> ],
        ['Payroll','<?php echo base_url(); ?>Firm_dashboard/payroll', <?php echo $de_payroll['dead_count']; ?> ],
        ['WorkPlace Pension - AE','<?php echo base_url(); ?>Firm_dashboard/workplace', <?php echo $de_workplace['dead_count']; ?> ],
        ['CIS - Contractor','<?php echo base_url(); ?>Firm_dashboard/cis', <?php echo $de_cis['dead_count']; ?> ],
        ['CIS - Sub Contractor','<?php echo base_url(); ?>Firm_dashboard/cis_sub', <?php echo $de_cissub['dead_count']; ?> ],
        ['P11D','<?php echo base_url(); ?>Firm_dashboard/p11d', <?php echo $de_p11d['dead_count']; ?> ],
        ['Management Accounts','<?php echo base_url(); ?>Firm_dashboard/management', <?php echo $de_management['dead_count']; ?> ],
        ['Bookkeeping','<?php echo base_url(); ?>Firm_dashboard/bookkeeping', <?php echo $de_bookkeep['dead_count']; ?> ],
        ['Investigation Insurance','<?php echo base_url(); ?>Firm_dashboard/investication', <?php echo $de_investgate['dead_count']; ?> ],
        ['Registered Address','<?php echo base_url(); ?>Firm_dashboard/register', <?php echo $de_registered['dead_count']; ?> ],
        ['Tax Advice','<?php echo base_url(); ?>Firm_dashboard/tax', <?php echo $de_taxadvice['dead_count']; ?> ],
        ['Tax Investigation','<?php echo base_url(); ?>Firm_dashboard/tax_inves', <?php echo $de_taxinvest['dead_count']; ?> ]         
      ]);      
     var view = new google.visualization.DataView(data);
       view.setColumns([0, 2]);
   
     var options = {
         chartArea: {left: 0, top: 0, width: "100%", height: "100%"}, 
         legend: { position: 'bottom' },
       hAxis : { textStyle : { fontSize: 8} },
        vAxis : { textStyle : { fontSize: 8} },
         // width:1200,
         // height:500,
    // legend: { position: 'bottom' },
    // hAxis : { textStyle : { fontSize: 8} },
    // vAxis : { textStyle : { fontSize: 8} },
   // pieHole: 0.4,
     };
   var chart = new google.visualization.PieChart(document.getElementById('Tabs_deadline'));   
         chart.draw(view, options);   
         var selectHandler = function(e) {
        //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
          window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
         }
         // Add our selection handler.
         google.visualization.events.addListener(chart, 'select', selectHandler);
      }
</script>
<script type="text/javascript">
   google.charts.load('current', {'packages':['corechart']});
   google.charts.setOnLoadCallback(drawChart2);
   
   function drawChart2() {  
     var data = google.visualization.arrayToDataTable([
       ['Service', 'link','Count'],             
        ['Approved','<?php echo base_url(); ?>invoice',<?php echo $approved_invoice['invoice_count']; ?> ],
        ['Cancelled','<?php echo base_url(); ?>invoice', <?php echo $cancel_invoice['invoice_count']; ?> ],
        ['Expired','<?php echo base_url(); ?>invoice', <?php echo $expired_invoice['invoice_count']; ?> ]               
      ]);      
     var view = new google.visualization.DataView(data);
       view.setColumns([0, 2]);
   
     var options = {
         chartArea: {left: 0, top: 0, width: "100%", height: "100%"},      
     };
   
   var chart = new google.visualization.PieChart(document.getElementById('Tabs_invoice'));   
         chart.draw(view, options);   
         var selectHandler = function(e) {
        //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
          window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
         }
         // Add our selection handler.
         google.visualization.events.addListener(chart, 'select', selectHandler);
      }
   
</script>
<script type="text/javascript">
   $(document).ready(function () {
     
     $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var ev = document.createEvent('Event');
    ev.initEvent('resize', true, true);
    window.dispatchEvent(ev);
  });
   
   
   });
</script>