<table class="table client_table1 text-center display nowrap" id="alltask" cellspacing="0" width="100%">
<thead>
<tr class="text-uppercase">
<th>
S.No
</th>
<th>Task Name</th>
<th>Start Date</th>
<th>Due Date</th>
<th>Status</th>
<th>Priority</th>

<th>Assignto</th>
</tr>
</thead>
<tbody>
<?php 
$t=1;
foreach ($task_list as $key => $value) {
error_reporting(0);
// $value['end_date'] = date('d-m-Y');
if($value['start_date']!='' && $value['end_date']!=''){    
$start  = date_create(implode('-', array_reverse(explode('-', $value['start_date']))));
$end  = date_create(implode('-', array_reverse(explode('-', $value['end_date']))));

//$end  = date_create(); // Current time and date
$diff    = date_diff ( $start, $end );

$y =  $diff->y;
$m =  $diff->m;
$d =  $diff->d;
$h =  $diff->h;
$min =  $diff->i;
$sec =  $diff->s;
}
else{
$y =  0;
$m =  0;
$d =  0;
$h =  0;
$min = 0;
$sec =  0;
}

$date_now = date("Y-m-d"); // this format is string comparable
$d_rec = implode('-', array_reverse(explode('-', $value['end_date'])));
if ($date_now > $d_rec) {
//echo 'priya';
$d_val = "EXPIRED";
}else{
$d_val = $y .' years '. $m .' months '. $d .' days '. $h .' Hours '. $min .' min '. $sec .' sec ';
}


if($value['worker']=='')
{
$value['worker'] = 0;
}
$staff=$this->db->query("SELECT * FROM staff_form WHERE user_id in (".$value['worker'].")")->result_array();
if($value['task_status']=='notstarted')
{
$percent = 0;
$stat = 'Not Started';
} if($value['task_status']=='inprogress')
{
$percent = 25;
$stat = 'In Progress';
} if($value['task_status']=='awaiting')
{
$percent = 50;
$stat = 'Awaiting for a feedback';
} if($value['task_status']=='testing')
{
$percent = 75;
$stat = 'Testing';
} if($value['task_status']=='complete')
{
$percent = 100;
$stat = 'Complete';
}
$exp_tag = explode(',', $value['tag']);
$explode_worker=explode(',',$value['worker']);
?>
<tr id="<?php echo $value['id']; ?>">

<td>
<?php echo $t; ?>
</td>
<td> <?php echo ucfirst($value['subject']);?></td>
<td><?php echo $value['start_date'];?></td>
<td><?php echo $value['end_date'];?></td>
<td><?php echo $stat;?></td>
<td>
<?php echo $value['priority'];?>
</td>
<td class="user_imgs" id="task_<?php echo $value['id'];?>">
<?php
$assignto=array();
foreach($explode_worker as $key => $val){     
$getUserProfilepic = $this->Common_mdl->getUserProfileName($val);
array_push($assignto,$getUserProfilepic);
} ?>
<?php echo implode(',',$assignto); ?>
</td>
</tr>
<?php $t++; } ?>
</tbody>
</table>

<div id ="chart_img">
 <?php
 if(isset($_SESSION['chart_image_sess'])){ ?>
 <img src="<?php echo $_SESSION['chart_image_sess']; ?>" > 
 <?php } ?>
</div> 
