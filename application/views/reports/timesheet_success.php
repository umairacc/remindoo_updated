
 <div id="chart_div" style="width: 1200px; height: 500px"></div>

    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      <?php
      $start_array=array();
      $end_array=array();
      $start_montharray=array();
      $end_montharray=array();
      $j=1;
      foreach($task_list_this_month as $value){ 
            $start_date=explode('-',$value['start_date']);          
            $start=$start_date['2']; 
            $start_month=$start_date['1'];
            $end_date=explode('-',$value['end_date']);
            $end=$end_date['2'];
            $end_month=$end_date['1'];
          }
      $month['month']=array(); 
       $month['start_date']=array(); 
        $month['end_date']=array(); 
      $month_other=array();
       foreach($task_list_this_month as $value){ 
        // echo '<pre>';
        // print_r($value);
        // echo '</pre>';

       //  for($i=0;$i<$end_month;$i++){
            $start_date=explode('-',$value['start_date']);          
            $start=$start_date['2']; 
            $start_month=$start_date['1'];
            $end_date=explode('-',$value['end_date']);
            $end=$end_date['2'];
             $end_month=$end_date['1'];

             // if($start_month==$end_month){
             //  array_push($month['month'],$start_month);
             //  array_push($month['start_date'],$start);
             //  array_push($month['end_date'],$end);
             // }else{             
             //  $month_other['month']=$start_month+1;
             //  $month_other['start_date']=$start;
             //  $month_other['end_date']=$end;
             // } ?>  
<?php //echo date('m'); ?>
  <?php  if($start_month=='01'){ ?>
       ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='01'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
         <?php } if($start_month=='02'){ ?>
      ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')';?>', <?php if($start_month=='02'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
        <?php } if($start_month=='03'){ ?>
      ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='03'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
        <?php } if($start_month=='04'){ ?>
      ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='04'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
        <?php } if($start_month=='05'){ ?>
      ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='05'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],   
      <?php } ?>
    <?php  if($start_month=='06'){ ?>
      ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='06'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
      <?php } ?>
        <?php  if($start_month=='07'){ ?>
        ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')';?>', <?php if($start_month=='07'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
        <?php } ?>
          <?php  if($start_month=='08'){ ?>
      ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='08'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
        <?php } if($start_month=='09'){ ?>
      ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='09'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
        <?php } if($start_month=='10'){ ?>
      ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='10'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
        <?php } if($start_month=='11'){ ?>
      ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='11'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
        <?php } if($start_month=='12'){ ?>
      ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='12'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
        <?php  } ?>
    
    <?php } ?>
      // Treat first row as data as well.
    ], true);

    var options = {
      legend:'none',
       candlestick: {
                  fallingColor: { strokeWidth: 0, fill: '#3366cc' }, // red
                  risingColor: { strokeWidth: 0, fill: '#3366cc' }   // green
               }
    };

    var chart = new google.visualization.CandlestickChart(document.getElementById('chart_div'));

    chart.draw(data, options);
  }
    </script>