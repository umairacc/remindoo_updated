<div id="piechart_1" style="width:100%;"></div>

<div class="prodiv12">
<?php      
       foreach($leads_status as $status){ 
       $count=$this->Report_model->lead_details($status['id'],$_SESSION['start'],$_SESSION['end']);
       if($count['lead_count']!='0'){ ?>
    <div class="pro-color">
       <span class="procount"><?php echo $count['lead_count']; ?></span>
       <div class="fordis">
          <div class="progress progress-xs">
             <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $count['lead_count'].'%' ?>;" aria-valuenow="<?php echo $count['lead_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $overall_leads['leads_count']; ?>"></div>
          </div>
       </div>
    </div>
    <?php }} ?>
</div>

<script type="text/javascript">
     google.charts.load('current', {'packages':['corechart']});
     google.charts.setOnLoadCallback(drawChart2);
  
     function drawChart2() {
  
       var data = google.visualization.arrayToDataTable([
         ['Leads','link','Count'],
             <?php      
              foreach($leads_status as $status){
              $status_name=$status['status_name'];
              $start_date=$_SESSION['start'];
               $end_date=$_SESSION['end'];
                            $count=$this->Report_model->leads_detailsWithDate($status['id'],$start_date,$end_date); ?>
              ['<?php echo $status_name; ?>','<?php echo base_url(); ?>/leads?status=<?php $strlower = strtolower(preg_replace("~[\\\\/:*?'&,<>|]~", '', $status_name));
        $lesg = str_replace(' ', '-', $strlower);
        $lesgs = str_replace('--', '-', $lesg); echo $lesgs; ?>',<?php echo $count['lead_count']; ?> ],
              <?php }  ?>
        ]);      
       var view = new google.visualization.DataView(data);
         view.setColumns([0, 2]);

       var options = {
             chartArea: {left: 0, width: "80%"},
        height: 250,
       legend: { position: 'bottom', show: true, container: $("#large-legend"), alignment: 'start'  },
      hAxis : { textStyle : { fontSize: 8} },
      vAxis : { textStyle : { fontSize: 8} },
       width: 350,
      height: 250

    //  pieHole: 0.4,
       };

    var chart = new google.visualization.PieChart(document.getElementById('piechart_1'));
   
         chart.draw(view, options);
   
         var selectHandler = function(e) {
        //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
          window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
         }
         // Add our selection handler.
         google.visualization.events.addListener(chart, 'select', selectHandler);
      }
  
</script>