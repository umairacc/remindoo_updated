<div class="sample_section">
<button type="button" class="print_button btn btn-primary" id="print_report">Print</button>
<div class="export_option">
<select id="exported_result">
<option value="">Select</option>
<option value="pdf">Pdf</option>
<option value="excel">Excel</option>
<option value="csv">CSV</option>
</select>
</div>
</div>
  <table id="example" class="display nowrap" style="width:100%">
  <thead>
<tr>
<th>Activity Date <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
<div class="sortMask"></div>
<select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th>User <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
<div class="sortMask"></div>
<select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th>Module <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
<div class="sortMask"></div>
<select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th>Activity <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
<div class="sortMask"></div>
<select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th  style="display: none;"></th>
<th  style="display: none;"></th>
<th  style="display: none;">Activity Date </th>
<th  style="display: none;"> User </th>
<th  style="display: none;">Module </th>
<th  style="display: none;">Activity </th>
<th  style="display: none;"></th>
<th  style="display: none;"></th>
</tr>
</thead>
<tbody>
 <?php 
    //print_r($user_activity);die;
     foreach ($user_activity as $activity_key => $activity_value) { 
    
        $rec=$this->Common_mdl->select_record('user','id',$activity_value['user_id']);
        $role=$this->Common_mdl->select_record('Role','id',$rec['role']);                                         
    
        ?>
 <tr>
    <td><?php echo date("d-m-Y h:i:s A ",$activity_value['CreatedTime']);?></td>
    <td><?php echo $role['role'];?></td>
    <td><?php echo $activity_value['module'];?></td>
    <td><?php echo $activity_value['log'];?></td>
    <td style="display: none;"></td>
    <td style="display: none;"></td>
    <td style="display: none;"><?php echo date("d-m-Y h:i:s A ",$activity_value['CreatedTime']);?></td>
    <td style="display: none;"> <?php echo $role['role'];?></td>
    <td style="display: none;"><?php echo $activity_value['module'];?></td>
    <td style="display: none;"><?php echo $activity_value['log'];?></td>
    <td style="display: none;"></td>
    <td style="display: none;"></td>
 </tr>
 <?php } ?>
</tbody>
</table>