<div id="piechart_11" style="width: 100%;"></div>
<script type="text/javascript">
google.charts.load('current', {'packages':['corechart']});
     google.charts.setOnLoadCallback(drawChart2);
  
     function drawChart2() {
  
       var data = google.visualization.arrayToDataTable([
         ['Leads', 'Count'],
             <?php      
              foreach($leads_status as $status){
              $status_name=$status['status_name'];
              $status_id=$status['id'];
              $status=$_SESSION['leads_status'];
              $last_Year_start=$_SESSION['leads_last_Year_start'];
              $last_month_end=$_SESSION['leads_last_month_end'];
               if($status=='all'){                  
                    $count=$this->Report_model->leads_details($status,$last_Year_start,$last_month_end);
                }else{ 
                      if($status==$status_id){            
                        $count=$this->Report_model->leads_details($status,$last_Year_start,$last_month_end); ?>
                      ['<?php echo $status_name; ?>', <?php echo $count['lead_count']; ?> ],
                  <?php    }                  
                }                 
              ?>             
              <?php }  ?>
              <?php $others_count=$this->Report_model->leads_details_new($status,$last_Year_start,$last_month_end); ?>
              ['others', <?php echo $others_count['lead_count']; ?> ],
        ]);      
  
       var options = {
              chartArea: {left: 0, width: "100%"},
        height: 250,
       legend: { position: 'bottom', show: true, container: $("#large-legend") },
      hAxis : { textStyle : { fontSize: 8} },
      vAxis : { textStyle : { fontSize: 8} },
      //   pieHole: 0.4,
       };
  
    var chart_div = new google.visualization.PieChart(document.getElementById('piechart_11'));
    var chart = new google.visualization.PieChart(piechart_11);
    google.visualization.events.addListener(chart, 'ready', function () {
    chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
    //document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '"></a>';
    var image=chart.getImageURI();
       $(document).ready(function(){  
      // alert(image);
                 $.ajax({
                        type: "POST",
                        url: "<?php echo base_url();?>/Report/lead_chart_sess",         
                        data: {lead_image_sess: image},
                        success: 
                             function(data){
                    
                             }
                         });
                  });
  });
    chart.draw(data, options);
  }
  </script>