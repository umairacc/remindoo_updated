<style type="text/css">
  
  .modal-open .modal 
  {
      overflow: visible;
  }

</style>
  <div class="modal-alertsuccess alert alert-danger-check succ dashboard_success_message" style="display:none;">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
         <div class="pop-realted1">
         <div class="position-alert1">
              <p id="summary_new"></p>             
         </div>
         </div>
    </div>

     <div class="modal-alertsuccess alert alert-warning Error-alert" id="group-error" style="display:none;">
   <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   <div class="pop-realted1">
      <div class="position-alert1">
   Group name required
      </div>
   </div>
</div>


   <div class="modal-alertsuccess alert alert-warning Error-alert" id="user-error" style="display:none;">
   <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   <div class="pop-realted1">
      <div class="position-alert1">
       Choose any users
      </div>
   </div>
</div>

<div class="modal fade recurring-msg common-schedule-msg1 report-mymodal1" id="myModal" role="dialog">
<div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
       <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">×</button>
          <h4 class="modal-title">Schedule Report</h4>
       </div>
       <div class="modal-body">
          <div class="row">
             <div class="form-group col-sm-12">
<!-- 01-09-2018 -->
             <?php
             if(isset($schedule_records) && ($schedule_records['id']!='')){ ?>
             <input type="hidden" id="id" class="schedule_id" value="<?php echo $schedule_records['id']; ?>">
               <input type="hidden" id="every_week" class="every_week" value="<?php echo $schedule_records['every_week']; ?>">
            <input type="hidden" id="days" class="days" value="<?php echo $schedule_records['days']; ?>">

            <input type="hidden" id="schedule_on_date" class="days" value="<?php echo date("M d,Y",strtotime($schedule_records['schedule_on_date'])); ?>">

            <input type="hidden" id="message" class="message" value="<?php echo $schedule_records['message']; ?>">

            <input type="hidden" id="ends" class="ends_record" value="<?php echo $schedule_records['ends']; ?>">

            <input type="hidden" id="users_records" class="users_records" value="<?php echo $schedule_records['users']; ?>">

            <input type="hidden" id="groups_records" class="groups_records" value="<?php echo $schedule_records['groups']; ?>">

            <input type="hidden" id="email_record" class="email_record" value="<?php echo $schedule_records['emails']; ?>">
            <input type="hidden" id="monthly_date_record" class="monthly_date_record" value="<?php echo date("M d,Y",strtotime($schedule_records['monthly_date'].date('-m-Y'))); ?>">

            <?php }  ?>

                <label class="label-column1 col-form-label">Repeats:</label>
                <div class="label-column2">
                   <select name="repeats" class="form-control repeats schd-reports" id="repeats" data-date="" data-day="" data-month="">
                  <!--    <option value="">Select</option> -->
                      <option value="week" <?php if(isset($schedule_records['repeats']) &&($schedule_records['repeats']=='week')){ echo "selected"; } ?>>Weekly</option>
                      <option value="day" <?php if(isset($schedule_records['repeats']) && ($schedule_records['repeats']=='day')){ echo "selected"; } ?>>Daily</option>
                      <option value="month" <?php if(isset($schedule_records['repeats']) && ($schedule_records['repeats']=='month')){ echo "selected";  } ?>>Monthly</option>
                      <option value="year" <?php if(isset($schedule_records['repeats']) && ($schedule_records['repeats']=='year')){ echo "selected"; } ?>>Yearly</option>
                   </select>
                </div>
             </div>

<!-- 01-09-2018 -->
             <div id="weekly" class="every_popup1">
                <div class="form-group  every-week01 floating_set">
                   <label class="label-column1  col-form-label">Every:</label>
                   <div class="label-column2">
                      <!-- <input type="text" class="form-control week-text" name="every" placeholder="" value=""> -->
                      <input type="number" class="form-control week-text" name="every" placeholder="" value="">                      
                      <p>weeks</p>
                   </div>                                     
                </div>
                <span id="counting_er"></span> <br>
                <div class="form-group days-list floating_set ">
                   <label class="label-column1 col-form-label">On:</label>
                   <div class="label-column2">
                      <div class="days-left">
                         <div class="checkbox-color checkbox-primary">
                            <input type="checkbox" class="border-checkbox dayss" name="days[]" value="Sunday" id="day-check1">
                            <label for="day-check1" class="border-checkbox-label">Sun</label>
                         </div>
                         <div class="checkbox-color checkbox-primary">
                            <input type="checkbox" class="border-checkbox dayss" name="days[]" value="Monday" id="day-check2"><label class="border-checkbox-label" for="day-check2">Mon</label>
                         </div>
                         <div class="checkbox-color checkbox-primary">
                            <input type="checkbox" class="border-checkbox dayss" name="days[]" value="Tuesday" id="day-check3"><label class="border-checkbox-label" for="day-check3">Tue</label> 
                         </div>
                         <div class="checkbox-color checkbox-primary">
                            <input type="checkbox" class="border-checkbox dayss" name="days[]" value="Wednesday" id="day-check4"><label class="border-checkbox-label" for="day-check4">Wed</label>
                         </div>
                         <div class="checkbox-color checkbox-primary">
                            <input type="checkbox" class="border-checkbox dayss" name="days[]" value="Thursday" id="day-check5"><label class="border-checkbox-label" for="day-check5">Thur</label> 
                         </div>
                         <div class="checkbox-color checkbox-primary">
                            <input type="checkbox" class="border-checkbox dayss" name="days[]" value="Friday" id="day-check6"><label class="border-checkbox-label" for="day-check6">Fri</label>
                         </div>
                         <div class="checkbox-color checkbox-primary">
                            <input type="checkbox" class="border-checkbox dayss" name="days[]" value="Saturday" id="day-check7"><label class="border-checkbox-label" for="day-check7">Sat</label>
                         </div>
                      </div>
                   </div>
                   <span id="days_er"></span>
                </div>
             </div>
             <div class="col-sm-12  form-group left-align radio_button01">
                <label class="label-column1 left-label">Ends:</label>
                <div class="forms-option label-column2">
                   <div class="radio-width01">
                      <input type="radio" name="end" value="after" class="ends" id="end-date">
                      <label for="end-date">After</label>
                      <!-- <input type="text" class="form-control message-count" name="message" placeholder="" value="" > -->
                      <input type="number" class="form-control message-count" name="message" placeholder="" value="">
                      <span>messages</span> 
                   </div>
                   <div class="radio-width01 on-optin">
                      <input type="radio" name="end" value="on" class="ends" id="end-date3">
                      <label for="end-date3">On </label>
                      <div class="input-group date">
                         <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                         <input type="text" class="hasDatepicker" id="modalendDate" name="sche_on_date">
                      </div>
                   </div>
                   <div class="no-end radio-width01">
                      <input type="radio" name="end" value="noend" class="ends" id="end-date1" checked="checked">
                      <label for="end-date1">No end date</label>
                   </div>
                </div>                
             </div>
             <span id="ends_er" style="padding-left: 2.5%;"></span>
              <div class="form-group col-sm-12">
                <label class="label-column1 col-form-label">Users:</label>
                <div class="label-column2">
                <div class="dropdown-sin-2">
                <?php $users=explode(',',$schedule_records['users']); ?>
                   <select name="users[]" class="form-control repeats schd-reports" id="users" data-date="" data-day="" data-month="" multiple="multiple">
                    <?php //if(isset($schedule_records['users']) && $schedule_records['users']==''){ ?>
                    <option value="">Select Users</option>   
                    <?php //} ?>                   
                    <?php   if(count($getallUser) > 0){   

                    foreach($getallUser as $key => $val){  ?>
                    <option value="<?php echo $val['id'];?>"  <?php if(isset($schedule_records['users']) && $schedule_records['users']!=""){ if(in_array($val['id'], $users)){ echo "selected"; } } ?>><?php echo $val["crm_name"];?> </option>
                    <?php }} ?>
                   </select>
                   </div>
                   <span id="users_er"></span>
                </div>
                
             </div>
              <div class="form-group col-sm-12">
                <label class="label-column1 col-form-label">Groups:</label>
                <div class="label-column2 groups_section">
                  <div class="dropdown-sin-4">
                   <select name="groups" class="form-control repeats schd-reports" id="groups" data-date="" data-day="" data-month="">
                    <option value="">Select Group</option>
                   <?php                     
                   foreach($groups as $group){
                   ?>
                   <option value="<?php echo $group['id']; ?>" <?php if(isset($schedule_records['groups']) && $schedule_records['groups']!="" && $schedule_records['groups'] == $group['id']){ echo "selected"; } ?>><?php echo $group['groupname']; ?></option>
                      <?php } ?>
                   </select>

                  </div>
                
                <span class="keyboard-tag1">
                   <a  href="javascript:;" class="btn btn-primary" data-toggle="modal" data-target="#Groups_section"  style="    padding: 6px 7px 7px 9px;
  text-align: center;" data-backdrop="static" data-keyboard="false"><i class="fa fa-plus" aria-hidden="true"></i></a></span>
</div>            
             </div>
             


          <input type="hidden" name="reported_email" id="reported_email" value="">

               <div class="tags-allow1 floating_set">
                      <h4><i class="fa fa-tags fa-6" aria-hidden="true"></i> Email</h4>
                      <input type="text" value="" name="tags" id="tags" class="tags" value=""/>
                      <span id="emails_er"></span>
                   </div>

               <div class=" left-align col-sm-12 summ">
                <label class="label-column1 left-label">Summary:</label>
                <div class="label-column2">
                   <p id="summary"></p>
                   <input type="hidden" name="recuring_sus_msg" id="recuring_sus_msg">
                </div>
             </div>
          </div>
       </div>
       <div class="modal-footer">
          <button type="button" name="button" class="btn btn-default   <?php if(isset($schedule_records['id']) &&($schedule_records['id']!='')){ ?>submitBtn_update<?php }else{ ?> submitBtn_new <?php } ?>" id="submitBtn">Schedule</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
       </div>
    </div>
 </div>
</div>



<div id="Groups_section" class="modal fade" role="dialog">
<div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">Create Group</h4>
    </div>
    <div class="modal-body">
        <div class="discount-types">                               
                

               <!--     <div class="line-items1 currency_update"> -->
                 <!--    <h3></h3> -->
                  <div class="pricing_listing">
                     <label>Group Name</label>
                   
                         <input type="text" name="group_name" id="group_name" class="form-control" placeholder="Group Name">
                        
                        </div>
               <!--    </div> -->

                   <div class="pricing_listing" >
                      <label>Users List</label>
                      <div class="pricing_listing">
                      <div class="dropdown-sin-2">
                      <select name="our_group_data[]" id="our_group_data" multiple="multiple" placeholder="Select Group Members">

                            <?php //if($_SESSION['roleId']!=6 && !empty($getallstaff)){ ?>


                            <?php  //foreach ($getallstaff as $getallUser_key => $getallUser_value) { ?>


                          <!--   <option value="<?php echo $getallUser_value['id'];?>"><?php echo $getallUser_value['crm_name']; ?></option> -->

                            <?php //} } ?>
                            <?php if(!empty($getallUser)){ ?>                                          

                            <?php  foreach ($getallUser as $getallUser_key => $getallUser_value) { ?>
                            <option value="<?php echo $getallUser_value['id'];?>"><?php echo $getallUser_value['crm_name']; ?></option>

                            <?php } ?>                                       
                            <?php
                            } ?>
                                           </select>
                                           </div> </div>
                      </div>

                  </div> 
    </div>
    <div class="modal-footer">
     <button type="button" name="submit" class="btn btn-primary add_create_group" id="group_add">Save</button>
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>

</div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>bower_components/jquery/js/jquery.min.js"></script>

<script type="text/javascript">  

      $('.add_create_group').on('click',function()
      {
        var group_name=$('#group_name').val();     

        if(group_name=='')
        {
           $("#group-error").show();
           setTimeout(function(){ $("#group-error").hide(); }, 3000);
        }
        else
        {
            var checkboxValues = [];
           $('#our_group_data').each(function(index, elem) {
            checkboxValues.push($(elem).val());
            });
            if($("#our_group_data").val() == '')
            {
               $("#user-error").show();
               setTimeout(function(){ $("#user-error").hide(); }, 3000);
            }
            else
            {
              var groupname=$('#group_name').val();
              var users=$("#our_group_data").val();

                $.ajax({
                   url: '<?php echo base_url(); ?>Report/group_insert',
                   type : 'POST',
                   data : {'group_name':groupname,'our_group_data':users},                    
                   beforeSend: function() {
                   $(".LoadingImage").show();
                   },
                   success: function(data)
                   {
                      $(".LoadingImage").hide();
                      var json=JSON.parse(data);
                      var select_box=json['content'];
                      $(".groups_section").html(select_box);
                      $("#Groups_section").modal('hide');
                   }
                 });
             }
        }

      });     

 </script>

 <script type="text/javascript">
   
    function getReportType()
    {
        var segment = '<?php echo $this->uri->segment(2); ?>';
        var reportType = {Client:"client",Task:"task",Proposal:"proposal",Leads:"leads",Service:"service",Dead_line:"deadline",invoice:"invoice",Summary:"summary",Timesheet:"timesheet",user_performation:"user_performance"}; 
        var return_data = reportType[segment];

        return return_data;
    }


    $(document).on('click','.submitBtn_new,.submitBtn_update',function(event)
    {  
           event.preventDefault();
            
            var reports_type = getReportType();
            var every=$('#repeats').val();        
            var users=$("#users").val().toString();         
            var groups=$("#groups").val();
            var email=$(".tags").val();
            var ends=$("input[name='end']:checked").val(); 

            var counting=$(".week-text").val();
            var month_date=$(".month_date").val();

            var days=[];
            $(".dayss:checked").each(function() {
             days.push($(this).val());
            });

            var days=days.toString();
            var length=days.length;
            var content='on';
            if(length=='0'){
               var days='';
               var content='';
            }

            var id = $("#id").val();
                    
           if(every=='week'){
             $('#summary').html('');
             $('#summary_new').html('');
             $('#recuring_sus_msg').val('');
             $('#summary').html('Every'+' '+counting+' '+every+','+'on '+days);
             $('#summary_new').html('Every'+' '+counting+' '+every+','+'on '+days);
             $('#recuring_sus_msg').val('Every'+' '+counting+' '+every+','+'on '+days);
           } else if(every=='day'){
             $('#summary').html('Every'+' '+counting+' '+every);
             $('#summary_new').html('Every'+' '+counting+' '+every);
             $('#recuring_sus_msg').val('Every'+' '+counting+' '+every);
           } else if(every=='month'){
             $('#summary').html('Every'+' '+counting+' '+every+','+'on day '+month_date);
             $('#summary_new').html('Every'+' '+counting+' '+every+','+'on day '+month_date);
             $('#recuring_sus_msg').val('Every'+' '+counting+' '+every+','+'on day '+month_date);
           } else if(every=='year'){
             $('#summary').html('Every'+' '+counting+' '+every);
             $('#summary_new').html('Every'+' '+counting+' '+every);
             $('#recuring_sus_msg').val('Every'+' '+counting+' '+every );
           }
           
           if(email!='')
           {
             var myArray = email.split(',');
             for(var i=0;i<myArray.length;i++)
             {
                var j=i+1;
                if(isValidEmailAddress(myArray[i]))
                {
                   
                }
                else
                { 
                  $( "div.bootstrap-tagsinput span:nth-child("+j+")").addClass("wrong");                  
                }     
             }
             var n = $( ".wrong" ).length; 
             if(n>'0')
             {
                $('#emails_er').html("Provide valid email address.").css('color','red');
             }
             else
             {
                $('#emails_er').html("");
             }
           }
           else
           {
              var n = 0;
              if(typeof(id)=='undefined')
              {
                 $('#emails_er').html("Required.").css('color','red'); 
              }             
           } 
           if(n=='0' && counting!="" && ends!="" && email!="" && (users!="" || groups!=""))
           {
               $('#counting_er').html("");
               $('#days_er').html("");
               $('#ends_er').html("");
               $('#emails_er').html("");
               $('#users_er').html("");

               var er = 0;

               if(ends == 'on' && $('input[name="sche_on_date"]').val() == "")
               { 
                  $('#ends_er').html("Date is required.").css('color','red');
                  er = 1;
               }
               if(every=='month' && $('input[name="every_date"]').val() == "")
               {
                  $('#counting_er').html("Date is required.").css('color','red');
                  er = 1;
               }
               if(every=='week')
               {
                  if(days =="")
                  {
                     $('#days_er').html("Required.").css('color','red');
                     er = 1;  
                  }                                 
               }
               if(er == 0)
               {
                  $('#counting_er').html("");
                  $('#days_er').html("");
                  $('#ends_er').html("");
                  $('#emails_er').html("");
                  $('#users_er').html("");              

                   var repeats=$(".week-text").val();
                   if(repeats=='')
                   {
                     var repeats=0;
                   }   

                    var message=$(".message-count").val();
                    var schedule_on_date=$("#modalendDate").val();

                    if(typeof($(".month_date").val())=='undefined')
                    {      
                        var monthly_date=0;
                    }
                    else
                    {          
                        var monthly_date=$(".month_date").val();
                    }

                    var first = $("#reported_email").val().split(',');       
                    var second = $(".tags").val().split(',');    
                    var a =$.merge( $.merge( [], first ), second );
                    email = a.filter(function(itm, i, a) {
                    return i == a.indexOf(itm);
                    });
                    email=email.toString(); 

                    if(typeof(id)=='undefined')
                    {
                       var formData={'repeats':every,'every_week':repeats,'days':days,'ends':ends,'message':message,'schedule_on_date':schedule_on_date,'users':users,'groups':groups,'email':email,'reports_type':reports_type,'monthly_date':monthly_date}
                       var url = '<?php echo base_url(); ?>Report/schdule_insert';
                    }
                    else
                    {
                       var formData={'repeats':every,'every_week':repeats,'days':days,'ends':ends,'message':message,'schedule_on_date':schedule_on_date,'users':users,'groups':groups,'email':email,'reports_type':reports_type,'monthly_date':monthly_date,'id':id}
                       var url = '<?php echo base_url(); ?>Report/schdule_update';
                    }
                         $.ajax({
                           url: url,
                           type : 'POST',
                           data : formData,                    
                           beforeSend: function() {
                           $(".LoadingImage").show();
                           },
                           success: function(data) 
                           { 
                             if(data!=0)
                             {
                                 $(".LoadingImage").hide();                      
                                 $('.dashboard_success_message').show();
                                 setTimeout(function(){ $('.dashboard_success_message').hide();  
                                 $('#myModal').find('.modal-header').children('.close').trigger('click');
                                 }, 3000);
                                 location.reload();
                             }
                             else
                             {
                                 setTimeout(function(){ 
                                 $('#myModal').find('.modal-header').children('.close').trigger('click');
                                 }, 3000);
                                 location.reload();
                             }
                           }

                         });
               }      
            }
            else
            { 
                if(counting =="")
                { 
                   $('#counting_er').html("Required.").css('color','red');
                }
                else
                {
                   $('#counting_er').html("");
                }                 
                if(ends =="")
                {
                   $('#ends_er').html("Required.").css('color','red');
                }
                else
                {
                   $('#ends_er').html("");
                }              
                
                if(users =="" && groups =="")
                {
                   $('#users_er').html("Select any user or group.").css('color','red');
                }
                else
                {
                   $('#users_er').html("");
                }
                         
            }
    });
   

  function isValidEmailAddress(emailAddress) 
  {
      var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
      return pattern.test(emailAddress);
  }
  
  $(document).ready(function () { 
  $(document).on('change','#repeats',function(){
  
  var vals=$(this).val();
  if(vals=='week'){
    $('#weekly').html('<div class="form-group  every-week01 floating_set"><label class="label-column1  col-form-label">Every</label><div class="label-column2"><input type="number" class="form-control week-text" name="every" placeholder="" value=""><p>week</p></div></div><span id="counting_er"></span> <br><div class="form-group  days-list floating_set"><label class="label-column1 col-form-label">On</label><div class="label-column2"><div class="checkbox-color checkbox-primary"><input type="checkbox" class="border-checkbox dayss"  name="days[]" value="Sunday" id="day-check01"><label for="day-check01" class="border-checkbox-label">Sun</label></div><div class="checkbox-color checkbox-primary"><input type="checkbox" class="border-checkbox  dayss" name="days[]" value="Monday" id="day-check02"><label for="day-check02" class="border-checkbox-label">Mon</label></div><div class="checkbox-color checkbox-primary"><input type="checkbox" class="border-checkbox  dayss" name="days[]" value="Tuesday" id="day-check03"><label for="day-check03" class="border-checkbox-label">Tue</label></div><div class="checkbox-color checkbox-primary"><input type="checkbox" class="border-checkbox  dayss" name="days[]" value="Wednesday" id="day-check04"><label for="day-check04" class="border-checkbox-label">Wed</label></div><div class="checkbox-color checkbox-primary"><input type="checkbox" class="border-checkbox  dayss" name="days[]" value="Thursday" id="day-check05"><label for="day-check05" class="border-checkbox-label">Thur</label></div><div class="checkbox-color checkbox-primary"><input type="checkbox" class="border-checkbox  dayss" name="days[]" value="Friday" id="day-check06"><label for="day-check06" class="border-checkbox-label">Fri</label></div><div class="checkbox-color checkbox-primary"><input type="checkbox" class="border-checkbox dayss" name="days[]" value="Saturday" id="day-check07"><label for="day-check07" class="border-checkbox-label">Sat</label></div></div></div><span id="days_er"></span>');
  
  } else if(vals=='day'){
    $('#weekly').html('<div class="form-group  every-week01 floating_set"><label class="label-column1  col-form-label">Every</label><div class="label-column2"><input type="number" class="form-control week-text" name="every" placeholder="" value=""><p>day</p></div></div><span id="counting_er"></span> <br>');
    
  } else if(vals=='month'){
    
    $('#weekly').html('<div class="form-group  every-week01 floating_set"><label class="label-column1  col-form-label">Every</label><div class="label-column2"><input type="number" class="form-control week-text" name="every" placeholder="" value=""><p>month</p></div><div class="col-sm-6 form-group common-repeats1 test-test"> <label class="label-column1 col-form-label">Date:</label> <div class="label-column2 input-group date"> <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span> <input type="text" class="dob_picker month_date"  name="every_date" id="week-text" placeholder="dd-mm-yyyy"> </div> </div></div><span id="counting_er"></span> <br></div>');
    // $("#testtest").datepicker();
   $('.dob_picker').datepicker({ dateFormat: 'dd-mm-yy',minDate:0, changeMonth: true,
       changeYear: true, });
  } else if(vals=='year'){
    $('#weekly').html('<div class="form-group  every-week01 floating_set"><label class="label-column1  col-form-label">Every</label><div class="label-column2"><input type="number" class="form-control week-text" name="every" placeholder="" value=""><p>year</p></div></div><span id="counting_er"></span> <br>');
  }
  
  });
  });

 </script>


 <script type="text/javascript">
     
 <?php 

 if(isset($schedule_records)){ ?>
  $(document).ready(function () {    

      setTimeout(function(){$("#repeats").trigger('change').val('<?php echo $schedule_records['repeats']; ?>'); },300);

       $("#users").val($("#users_records").val());

       $("#groups").val($("#groups_records").val());

       $("#tags").val($("#email_record").val());

       $("#reported_email").val($("#email_record").val());


        var result1 = $("#email_record").val().split(',');
       var count1=result1.length;    
        var email_section = [];
       for(i=0;i<count1;i++){
        
           var content='<span class="tag label label-info">'+ result1[i] +'<span data-role="remove" class="remove_item"></span></span>';
           $(".bootstrap-tagsinput").prepend( content );
       }

  });

  $(document).on('click','.fr-sch',function()
  {
      $(".week-text").val($("#every_week").val());
      if($("#repeats").val()=='month'){ 
         $(".month_date").val($("#monthly_date_record").val());
      }

      if($("#repeats").val()=='week'){
       var result = $("#days").val().split(',');
       var count=result.length;       
       for(i=0;i<count;i++){
            $('input:checkbox[value="' + result[i] + '"]').attr('checked', true);
       }

      }     

       if($("#ends").val()=='after'){
          $('input:radio[value="after"]').attr('checked', true);
          $(".message-count").val($("#message").val());
       }

        if($("#ends").val()=='on'){
          $('input:radio[value="on"]').attr('checked', true);
          $("#modalendDate").val($("#schedule_on_date").val());
       }

        if($("#ends").val()=='noend'){
          $('input:radio[value="noend"]').attr('checked', true);
         // $(".message-count").val($("#message").val());
       }
  });

  $(document).ready(function() {
      $(".remove_item").click(function(){

      var value=$(this).parent(".tag").html().split('<span');


      var removeItem =value[0]; 


      $(this).parent(".tag").remove();

      var emails = [];
      $("span.label-info").each(function(i, sel){
      var selectedVal = $(this).html().split('<span');

      emails.push(selectedVal[0]);
      }); 



      var email=emails.toString();

      var result=email.split(',');


      var result1 = result.filter(function(elem){
      return elem != removeItem; 
      });  

      $("#reported_email").val(result);        

     });
 });

     <?php } ?>


 </script>
