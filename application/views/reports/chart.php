 <script type="text/javascript" src="http://remindoo.org/CRMTool/bower_components/jquery/js/jquery.min.js"></script>
<?php
$pie_chart_data = array(array("Age Range", "Salary Average", "Total Credits Average"));
// foreach($results as $result)

//  {
$agerange=2;
$agerange1=6;
$agerange2=4;

$salary_average=9;
$salary_average1=10;
$salary_average2=15;

$credits_average=100;

$credits_average1=50;
$credits_average2=20;


 $column_chart_data[]=array($agerange, round($salary_average,2), round($credits_average,2));

 $column_chart_data[] =array($agerange1, round($salary_average1,2), round($credits_average1,2));

  // $column_chart_data[] =
 //}
$column_chart_data = json_encode($column_chart_data);


$HTML =<<<XYZ
<!--Load the AJAX API-->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
 
     // Load the Visualization API and the charts package.
      google.load('visualization', '1.0', {'packages':['corechart']});
 
      // Column Chart
      google.setOnLoadCallback(drawColumnChart);
 
      function drawColumnChart() {
        var data = google.visualization.arrayToDataTable([["Age Range","Salary Average","Total Credits Average"],["16-20",67531.96,27190.76],["21-30",67826.45,27519.29],["31-40",64797.41,26518.38],["41-50",67120.44,30169.27],["51-60",70980.97,32099.15],["61-70",69606.1,28444.99],["71-80",66535.97,28491.04],["81-90",66519.99,26383.24]]);
 
         var options = {
          title: 'Total Credits And Salary Averages Within Age Range',
          titleTextStyle: {fontName: 'Lato', fontSize: 18, bold: true},
          hAxis: {title: 'Age Range', titleTextStyle: {color: '#3E4827'}},
          height: 500,
          chartArea:{left:50,top:50,width:'100%',height:'85%'},
          legend: { position: "top" },
          colors:['#91A753','#C6D9AC']
        };
 
        var chart = new google.visualization.ColumnChart(document.getElementById('column_chart_div'));
        chart.draw(data, options);
      }
 
      // Make the charts responsive
      $(document).ready(function(){
        $(window).resize(function(){
          drawColumnChart();
        });
      });
 document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '">Printable version</a>';
    </script>
 
    <div id="column_chart_div"></div>

    <div id='png'></div>
XYZ;
 
    echo $HTML;
 
?>




<form method='post' action='<?php echo base_url(); ?>Reports/saveChartPDF' id='savePDFForm'>
 <input type='hidden' id='htmlContentHidden' name='htmlContent' value=''>
 <input type='button' id="downloadBtn" value='Save to PDF'>
</form>

<script>
 $(document).ready(function() {
        $("#downloadBtn").on("click", function() {
 alert('ok');
            var htmlContent = $("#column_chart_div").html();
            $("#htmlContentHidden").val(htmlContent);
 
            // submit the form
            $('#savePDFForm').submit();
        });
      });
</script>