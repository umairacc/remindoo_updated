<table class="table client_table1 text-center display nowrap printableArea" id="alluser" cellspacing="0" width="100%"  cellpadding="10" style="border-collapse:collapse;border:1px solid #ddd;">
<thead>
<tr class="text-uppercase">
<th style="border:1px solid #ddd;">
S.no
</th>   
<th style="border:1px solid #ddd;">Invoice No</th>
<th style="border:1px solid #ddd;">Client Email</th>
<th style="border:1px solid #ddd;">Invoice Amount</th>
<th style="border:1px solid #ddd;">Payment Status</th>
<th style="border:1px solid #ddd;">Invoice Date</th>
</tr>
</thead>
 <tbody>
 <?php
 if(count($invoice)){
 $s=1;
 foreach($invoice as $inv){
 	$client_id=$inv['client_id'];
 	$amount_details=$this->db->query("select grand_total,payment_status from amount_details where client_id='".$client_id."'")->row_array();
 	$user_details=$this->db->query("select crm_email_id from user where id='".$inv['client_email']."'")->row_array();
  ?>
<tr>
<td style="border:1px solid #ddd;"><?php echo $s;?></td>
<td style="border:1px solid #ddd;"><?php echo ucfirst($inv['invoice_no']);?></td>  
<td style="border:1px solid #ddd;"><?php echo $user_details['crm_email_id'];?></td>                               
<td style="border:1px solid #ddd;"><?php echo $amount_details['grand_total']; ?></td> 
<td style="border:1px solid #ddd;"><?php echo $amount_details['payment_status']; ?></td>
<td style="border:1px solid #ddd;" id="innac"><?php echo date('d-m-y',$inv['created_at']);?></td>
</tr>
<?php $s++; }

}else{ ?>
<tr><td>No records found</td></tr>
<?php	} ?>
</tbody>
</table>

<div id ="chart_img">
 <?php
 if(isset($_SESSION['invoice_image'])){ ?>
 <img src="<?php echo $_SESSION['invoice_image']; ?>" > 
 <?php } ?>
</div> 