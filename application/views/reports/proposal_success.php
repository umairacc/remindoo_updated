<div id="piechart11" style="width: 100%;"></div>




<script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
     google.charts.setOnLoadCallback(drawChart2);
  
     function drawChart2() {
  
       var data = google.visualization.arrayToDataTable([
         ['Proposal', 'Count'],
          <?php
          $status=$_SESSION['proposal_status'];
          if($status=='accepted'){?>
          ['Accepted',    <?php echo $accept_proposal['proposal_count']; ?>],
          ['Others',  <?php echo $others['proposal_count']; ?>],
          <?php } if($status=='in discussion'){?>
          ['In Discussion',    <?php echo $indiscussion_proposal['proposal_count']; ?>],
          ['Others',  <?php echo $others['proposal_count']; ?>],
          <?php } if($status=='sent'){ ?>
          ['Sent',  <?php echo $sent_proposal['proposal_count']; ?>],
          ['Others',  <?php echo $others['proposal_count']; ?>],
          <?php } if($status=='opened'){ ?>
          ['Viewed', <?php echo $viewed_proposal['proposal_count']; ?>],
          ['Others',  <?php echo $others['proposal_count']; ?>],
          <?php }  if($status=='decline'){?>
          ['Declined',   <?php echo $declined_proposal['proposal_count']; ?>],
          ['Others',  <?php echo $others['proposal_count']; ?>],
          <?php }  if($status=='archive'){?>         
          ['Archived',  <?php echo $archive_proposal['proposal_count']; ?>],
          ['Others',  <?php echo $others['proposal_count']; ?>],
          <?php }  if($status=='draft'){?>
          ['Draft',  <?php echo $draft_proposal['proposal_count']; ?>],
          ['Others',  <?php echo $others['proposal_count']; ?>],
          <?php } ?>

       ]);      
  
       var options = {
             chartArea: {left: 0, width: "100%"},
        height: 250,
       legend: { position: 'bottom', show: true, container: $("#large-legend") },
      hAxis : { textStyle : { fontSize: 8} },
      vAxis : { textStyle : { fontSize: 8} },
     //    pieHole: 0.4,
       };
  
        var chart_div = new google.visualization.PieChart(document.getElementById('piechart11'));
    var chart = new google.visualization.PieChart(piechart11);
    google.visualization.events.addListener(chart, 'ready', function () {
    chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
    //document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '"></a>';
    var image=chart.getImageURI();
       $(document).ready(function(){  
      // alert(image);
                 $.ajax({
                        type: "POST",
                        url: "<?php echo base_url();?>/Report/proposal_chart_sess",         
                        data: {proposal_image_sess: image},
                        success: 
                             function(data){
                    
                             }
                         });
                  });
  });
    chart.draw(data, options);
  }
  
</script>