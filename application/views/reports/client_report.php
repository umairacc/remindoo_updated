
<table class="table client_table1 text-center display nowrap printableArea" id="alluser" cellspacing="0" width="100%" >
<thead>
<tr class="text-uppercase">
<th>
S.no
</th>
<?php if(isset($column_setting[0]['profile_image'])&&($column_setting[0]['profile_image']==1)){?>
<th>Profile</th>
<?php } ?>      
<th>Name</th>
<th>Updates</th>
<th>Active</th>
<th>CompanyStatus</th>
<th>User Type</th>
<th>Status</th>
</tr>
</thead>
 <tbody>
<?php 
$s = 1;
foreach ($client as $getallUserkey => $getallClientvalue) {
$update=$this->Common_mdl->select_record('update_client','user_id',$getallClientvalue['user_id']);
$company_status=$this->Common_mdl->select_record('client','user_id',$getallClientvalue['user_id']);
$getallUservalue=$this->Common_mdl->select_record('user','id',$getallClientvalue['user_id']);
if($getallUservalue['role']=='6'){
//$role = 'Staff';
$edit=base_url().'user/view_staff/'.$getallUservalue['id'];
$house='Manual';
}elseif($getallUservalue['role']=='4' && $company_status['status']==0 ){
//$role = 'Client';
$edit=base_url().'Client/client_info/'.$getallUservalue['id'];
$house='Manual';
}elseif($getallUservalue['role']=='4' && $company_status['status']==1){
//$role = 'Client';
$edit=base_url().'Client/client_info/'.$getallUservalue['id'];
$house='Company House';
}elseif($getallUservalue['role']=='4' && $company_status['status']==2){
//$role = 'Client';
$edit=base_url().'Client/client_info/'.$getallUservalue['id'];
$house='Import';
}else{
$house ='';
$role = '';
$edit='#';
}
if($getallUservalue['status']=='1'){
$status_id = 'checkboxid1';  
$chked = 'selected';
$status_val = '1';
$active='Active';
}elseif($getallUservalue['status']=='2' || $getallUservalue['status']=='0' ){
$status_id = 'checkboxid2';  
$chked = 'selected';
$status_val = '2';
$active='Inactive';
} elseif($getallUservalue['status']=='3'){
$status_id = 'checkboxid3';  
$chked = 'selected';
$status_val = '3';
$active='Frozen';
}
$role=$this->Common_mdl->select_record('Role','id',$getallUservalue['role']);
?>
<tr id="<?php echo $getallUservalue["id"]; ?>">
<td><?php echo $s;?></td>
<?php if(isset($column_setting[0]['profile_image'])&&($column_setting[0]['profile_image']==1)){?>
<td class="user_imgs"><img src="<?php echo base_url().'/uploads/'.$getallUservalue['crm_profile_pic'];?>" alt="img"></td>
<?php } ?>
<td><?php echo ucfirst($getallUservalue['crm_name']);?></td>                               
<td>                                  
<?php echo ($update['counts']!='')? ($update['counts']):'0';?>  new
</td> <td>
<?php if($getallUservalue['status']=='1'){ echo "Active"; } elseif($getallUservalue['status']=='0'){ echo "Inactive"; }elseif($getallUservalue['status']=='3'){ echo "Frozen"; }else{ echo '-'; } ?>
</td>
<td id="innac"><?php echo ucfirst($company_status['crm_company_status']);?></td>
<td><?php echo $role['role'];?></td>
<td><?php echo $house;?></td>
</tr>
<?php $s++; } ?>
</tbody>
</table>
 <div id ="chart_img">
 <?php
 if(isset($_SESSION['image_sess'])){ ?>
 <img src="<?php echo $_SESSION['image_sess']; ?>" > 
 <?php } ?>
</div> 

