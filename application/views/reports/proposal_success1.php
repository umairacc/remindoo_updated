<div id="piechart1" style="width:100%;"></div>
<?php $total_proposal_count = count($accept_proposal)+count($indiscussion_proposal)+count($sent_proposal)+count($viewed_proposal)+count($declined_proposal)+count($archive_proposal)+count($draft_proposal); ?>
<div class="prodiv12">
   <?php
      if(count($accept_proposal)!='0'){ ?>
   <div class="pro-color">
      <span class="procount"> <?php echo count($accept_proposal); ?></span>
      <div class="fordis">
         <div class="progress progress-xs">
            <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo count($accept_proposal).'%' ?>;" aria-valuenow="<?php echo count($accept_proposal); ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_proposal_count; ?>"></div>
         </div>
      </div>
   </div>
   <?php } ?>
   <?php
      if(count($indiscussion_proposal)!='0'){ ?>
   <div class="pro-color">
      <span class="procount"> <?php echo count($indiscussion_proposal); ?></span>
      <div class="fordis">
         <div class="progress progress-xs">
            <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo count($indiscussion_proposal).'%' ?>;" aria-valuenow="<?php echo count($indiscussion_proposal); ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_proposal_count; ?>"></div>
         </div>
      </div>
   </div>
   <?php } ?>
   <?php
      if(count($sent_proposal)!='0'){ ?>
   <div class="pro-color">
      <span class="procount"> <?php echo count($sent_proposal); ?></span>
      <div class="fordis">
         <div class="progress progress-xs">
            <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo count($sent_proposal).'%' ?>;" aria-valuenow="<?php echo count($sent_proposal); ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_proposal_count; ?>"></div>
         </div>
      </div>
   </div>
   <?php } ?>
   <?php
      if(count($viewed_proposal)!='0'){ ?>
   <div class="pro-color">
      <span class="procount"> <?php echo count($viewed_proposal); ?></span>
      <div class="fordis">
         <div class="progress progress-xs">
            <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo count($viewed_proposal).'%' ?>;" aria-valuenow="<?php echo count($viewed_proposal); ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_proposal_count; ?>"></div>
         </div>
      </div>
   </div>
   <?php } ?>
   <?php
      if(count($declined_proposal)!='0'){ ?>
   <div class="pro-color">
      <span class="procount"> <?php echo count($declined_proposal); ?></span>
      <div class="fordis">
         <div class="progress progress-xs">
            <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo count($declined_proposal).'%' ?>;" aria-valuenow="<?php echo count($declined_proposal); ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_proposal_count; ?>"></div>
         </div>
      </div>
   </div>
   <?php } ?>
   <?php
      if(count($archive_proposal)!='0'){ ?>
   <div class="pro-color">
      <span class="procount"> <?php echo count($archive_proposal); ?></span>
      <div class="fordis">
         <div class="progress progress-xs">
            <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo count($archive_proposal).'%' ?>;" aria-valuenow="<?php echo count($archive_proposal); ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_proposal_count; ?>"></div>
         </div>
      </div>
   </div>
   <?php } ?>
   <?php
      if(count($draft_proposal)!='0'){ ?>
   <div class="pro-color">
      <span class="procount"> <?php echo count($draft_proposal); ?></span>
      <div class="fordis">
         <div class="progress progress-xs">
            <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo count($draft_proposal).'%' ?>;" aria-valuenow="<?php echo count($draft_proposal); ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_proposal_count; ?>"></div>
         </div>
      </div>
   </div>
   <?php } ?>
</div>

<script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
     google.charts.setOnLoadCallback(drawChart2);
  
     function drawChart2() {
  
       var data = google.visualization.arrayToDataTable([
        ['Proposal','link','Count'],
        ['Accepted', '<?php echo base_url(); ?>Firm_dashboard/accept', <?php echo count($accept_proposal); ?>],
        ['In Discussion',  '<?php echo base_url(); ?>Firm_dashboard/indiscussion', <?php echo count($indiscussion_proposal); ?>],
        ['Sent','<?php echo base_url(); ?>Firm_dashboard/sent',  <?php echo count($sent_proposal); ?>],
        ['Viewed','<?php echo base_url(); ?>Firm_dashboard/view', <?php echo count($viewed_proposal); ?>],
        ['Declined', '<?php echo base_url(); ?>Firm_dashboard/decline',   <?php echo count($declined_proposal); ?>],
        ['Archived',  '<?php echo base_url(); ?>Firm_dashboard/archieve',  <?php echo count($archive_proposal); ?>],
        ['Draft',  '<?php echo base_url(); ?>Firm_dashboard/draft',  <?php echo count($draft_proposal); ?>]
       ]);     

        var view = new google.visualization.DataView(data);
         view.setColumns([0, 2]); 
  
       var options = {
              chartArea: {left: 0, width: "80%"},
        height: 250,
       legend: { position: 'bottom', show: true, container: $("#large-legend") , alignment: 'start' },
      hAxis : { textStyle : { fontSize: 8} },
      vAxis : { textStyle : { fontSize: 8} },
       width: 350,
      height: 250
    //  pieHole: 0.4,
       };
  
       var chart = new google.visualization.PieChart(document.getElementById('piechart1'));
   
         chart.draw(view, options);
   
         var selectHandler = function(e) {
        //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
          window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
         }
         // Add our selection handler.
         google.visualization.events.addListener(chart, 'select', selectHandler);
      }
</script>