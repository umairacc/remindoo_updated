 <table id="example" class="display nowrap" style="width:100%">
  <thead>
  <tr>
  <th>S.no</th>    
  <th>Invoice No</th>
  <th>Client Company Name</th> 
  <th>Client Email</th>     
  <th>Invoice Date</th>     
  <th>Invoice Amount</th>   
  <th>Created date</th>    
  </tr>
  </thead>
  <tbody>
  <?php
  $i=1;
  foreach($records as $record){ 
    $rec=$this->Report_model->email_find($record['client_email']);
  ?>
  <tr>
    <td><?php echo $i; ?></td>  
    <td><?php echo $record['invoice_no']; ?></td> 
    <td><?php echo $rec['crm_company_name']; ?></td>  
    <td><?php echo $rec['crm_email']; ?></td> 
    <td><?php echo date('Y-m-d',$record['invoice_date']); ?></td> 
    <td><?php echo $record['grand_total']; ?></td>  
<td><?php echo date('Y-m-d',strtotime($record['created_at'])); ?></td>  
  </tr>
  <?php $i++; } ?>


    <?php
  $m=$i;
  foreach($draft as $draf){ 
    $rec=$this->Report_model->email_find($draf['to_id']);
  ?>
  <tr>
    <td><?php echo $i; ?></td>  
    <td><?php echo $draf['invoice_no']; ?></td> 
    <td><?php echo $rec['crm_company_name']; ?></td>  
    <td><?php echo $rec['crm_email']; ?></td> 
    <td><?php echo $draf['date']; ?></td> 
    <td><?php echo $draf['status']; ?></td>  
<td><?php echo date('Y-m-d',strtotime($draf['created_at'])); ?></td>  
  </tr>
  <?php $m++; } ?>
  </tbody>
  </table>