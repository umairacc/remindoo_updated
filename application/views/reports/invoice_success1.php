<div id="piechart_invoice" style="width: 100%;"></div>
<?php $total_invoice_count = $approved_invoice['invoice_count']+$cancel_invoice['invoice_count']+$expired_invoice['invoice_count']; ?> 
  <div class="prodiv12">
<?php
if($approved_invoice['invoice_count']!='0'){
?>
<div class="pro-color">
<span class="procount"><?php echo $approved_invoice['invoice_count']; ?> </span>
<div class="fordis">
<div class="progress progress-xs">
<div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $approved_invoice['invoice_count'].'%'; ?> ;" aria-valuenow="<?php echo $approved_invoice['invoice_count']; ?> " aria-valuemin="0" aria-valuemax="<?php echo $total_invoice_count; ?>"></div>
</div>
</div>
</div>
  <?php } ?>   

  <?php
if($cancel_invoice['invoice_count']!='0'){
?>
<div class="pro-color">
<span class="procount"><?php echo $cancel_invoice['invoice_count']; ?> </span>
<div class="fordis">
<div class="progress progress-xs">
<div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $cancel_invoice['invoice_count'].'%'; ?> ;" aria-valuenow="<?php echo $cancel_invoice['invoice_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_invoice_count; ?>"></div>
</div>
</div>
</div>
  <?php } ?>  

  <?php
if($expired_invoice['invoice_count']!='0'){
?>
<div class="pro-color">
<span class="procount"><?php echo $expired_invoice['invoice_count']; ?> </span>
<div class="fordis">
<div class="progress progress-xs">
<div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $expired_invoice['invoice_count'].'%'; ?> ;" aria-valuenow="<?php echo $expired_invoice['invoice_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_invoice_count; ?>"></div>
</div>
</div>
</div>
  <?php } ?>                                               
</div>
   <script type="text/javascript">
     google.charts.load('current', {'packages':['corechart']});
     google.charts.setOnLoadCallback(drawChart2);
  
     function drawChart2() {  
       var data = google.visualization.arrayToDataTable([
         ['Service', 'link','Count'],             
          ['Approved','<?php echo base_url(); ?>invoice?type=1',<?php echo $approved_invoice['invoice_count']; ?> ],
          ['Cancelled','<?php echo base_url(); ?>invoice?type=3', <?php echo $cancel_invoice['invoice_count']; ?> ],
          ['Expired','<?php echo base_url(); ?>invoice?type=2', <?php echo $expired_invoice['invoice_count']; ?> ]               
        ]);      
       var view = new google.visualization.DataView(data);
         view.setColumns([0, 2]);

       var options = {
            chartArea: {left: 0, width: "80%"},
        height: 250,
       legend: { position: 'bottom', show: true, container: $("#large-legend") },
      hAxis : { textStyle : { fontSize: 8} },
      vAxis : { textStyle : { fontSize: 8} },
       width: 350,
      height: 250
     // pieHole: 0.4,
       };
  
     var chart = new google.visualization.PieChart(document.getElementById('piechart_invoice'));
   
         chart.draw(view, options);
   
         var selectHandler = function(e) {
        //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
          window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
         }
         // Add our selection handler.
         google.visualization.events.addListener(chart, 'select', selectHandler);
      }
 
  </script>