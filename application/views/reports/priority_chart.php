
<div id="Tabs_priority"></div>

<script type="text/javascript">  

       google.charts.load('current', {'packages':['corechart']});
       google.charts.setOnLoadCallback(drawChart);
       
       function drawChart()
       {
			var data = google.visualization.arrayToDataTable([
			['Task','link','Count'],
			['Low', '<?php echo base_url(); ?>Firm_dashboard/low', <?php echo $low_tasks['task_count']; ?>],
			['High',  '<?php echo base_url(); ?>Firm_dashboard/high',    <?php echo $high_tasks['task_count']; ?>],
			['Medium','<?php echo base_url(); ?>Firm_dashboard/medium',  <?php echo $medium_tasks['task_count']; ?>],
			['Super Urgent','<?php echo base_url(); ?>Firm_dashboard/super_urgent', <?php echo $super_urgent_tasks['task_count']; ?>]   
			]);  

			var view = new google.visualization.DataView(data);
			 view.setColumns([0, 2]);


			var options = {
			chartArea: {left: 0, top: 0, width: "100%", height: "80%"},     
			legend: { position: 'bottom' },
			hAxis : { textStyle : { fontSize: 8} },
			vAxis : { textStyle : { fontSize: 8} },
			// legend: { position: 'bottom' },
			// hAxis : { textStyle : { fontSize: 8} },
			// vAxis : { textStyle : { fontSize: 8} },
			// pieHole: 0.4,
			};  
			var chart = new google.visualization.PieChart(document.getElementById('Tabs_priority'));   
			chart.draw(view, options);   
			var selectHandler = function(e) {
			//  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
			window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
			}
			// Add our selection handler.
			google.visualization.events.addListener(chart, 'ready', selectHandler);
        }
        
</script>