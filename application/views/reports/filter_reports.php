<div class="all_user-section2 floating_set fr-reports">
<div class="tab-content <?php if($_SESSION['permission']['Reports']['view']!='1'){ ?> permission_deined <?php } ?>  ">
   <div class="reports-tabs tab-pane fade active" id="reports-tab">
      <div class="card-comment">
         <div class="card-block-small">
            <div class="left-sireport col-xs-12  col-md-4">
               <div class="comment-desc">
                  <h6>
                    <span> Clients  </span>
                     <div class="comment-btn">
                        <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>Report/Client">Run</a>
                        <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>report/Client_customisation">Customise</a>
                     </div>
                  </h6>
                  <span class="time-frame"><label>Time Frame:</label>
                  <select id="filter_client">
                     <option value="">Select</option>
                     <option value="year">This Year</option>
                     <option value="month">This Month</option>
                  </select>
                  </span>
                  <div class="card-block fileters_client new123 client_chart">
                    <div id="chart_Donut11" style="width: 50%;"></div>
                     <!-- progress bar section -->
                    <div class="prodiv12">
                    <?php
                    if($Private['limit_count']!='0'){ ?>
                    <div class="pro-color">
                      <span class="procount"> <?php echo $Private['limit_count']; ?></span>
                    <div class="fordis">
                    <div class="progress progress-xs">
                      <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Private['limit_count'].'%' ?>;" aria-valuenow="<?php echo $Private['limit_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo count($client); ?>"></div>
                    </div>
                    </div>
                    </div>
                    <?php } ?>
                    <?php
                    if($Public['limit_count']!='0'){ ?>
                    <div class="pro-color">
                      <span class="procount"> <?php echo $Public['limit_count']; ?></span>
                    <div class="fordis">
                    <div class="progress progress-xs">
                      <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Public['limit_count'].'%' ?>;" aria-valuenow="<?php echo $Public['limit_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo count($client); ?>"></div>
                    </div>
                    </div>
                    </div>
                    <?php } ?>


                    <?php
                    if($limited['limit_count']!='0'){ ?>
                    <div class="pro-color">
                      <span class="procount"> <?php echo $limited['limit_count']; ?></span>
                    <div class="fordis">
                    <div class="progress progress-xs">
                      <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $limited['limit_count'].'%' ?>;" aria-valuenow="<?php echo $limited['limit_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo count($client); ?>"></div>
                    </div>
                    </div>
                    </div>
                    <?php } ?>

                    <?php
                      if($Partnership['limit_count']!='0'){ ?>
                      <div class="pro-color">
                        <span class="procount"> <?php echo $Partnership['limit_count']; ?></span>
                      <div class="fordis">
                      <div class="progress progress-xs">
                        <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Partnership['limit_count'].'%' ?>;" aria-valuenow="<?php echo $Partnership['limit_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo count($client); ?>"></div>
                      </div>
                      </div>
                      </div>
                      <?php } ?>

                      <?php
                      if($self['limit_count']!='0'){ ?>
                      <div class="pro-color">
                        <span class="procount"> <?php echo $self['limit_count']; ?></span>
                      <div class="fordis">
                      <div class="progress progress-xs">
                        <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $self['limit_count'].'%' ?>;" aria-valuenow="<?php echo $self['limit_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo count($client); ?>"></div>
                      </div>
                      </div>
                      </div>
                      <?php } ?>
                      <?php
                      if($Trust['limit_count']!='0'){ ?>
                      <div class="pro-color">
                        <span class="procount"> <?php echo $Trust['limit_count']; ?></span>
                      <div class="fordis">
                      <div class="progress progress-xs">
                        <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Trust['limit_count'].'%' ?>;" aria-valuenow="<?php echo $Trust['limit_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo count($client); ?>"></div>
                      </div>
                      </div>
                      </div>
                      <?php } ?>

                      <?php
                      if($Charity['limit_count']!='0'){ ?>
                      <div class="pro-color">
                        <span class="procount"> <?php echo $Charity['limit_count']; ?></span>
                      <div class="fordis">
                      <div class="progress progress-xs">
                        <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Charity['limit_count'].'%' ?>;" aria-valuenow="<?php echo $Charity['limit_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo count($client); ?>"></div>
                      </div>
                      </div>
                      </div>
                      <?php } ?>

                      <?php
                      if($Other['limit_count']!='0'){ ?>
                      <div class="pro-color">
                        <span class="procount"> <?php echo $Other['limit_count']; ?></span>
                      <div class="fordis">
                      <div class="progress progress-xs">
                        <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Other['limit_count'].'%' ?>;" aria-valuenow="<?php echo $Other['limit_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo count($client); ?>"></div>
                      </div>
                      </div>
                      </div>
                      <?php } ?>
                      </div>
                             <!-- progress bar section -->
                          </div>

                           </div>
                        </div>
                        <div class="left-sireport col-xs-12 col-md-4">
                           <div class="comment-desc">
                              <h6>
                                 <span>Task</span>
                                 <div class="comment-btn">
                                    <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>Report/Task">Run</a>
                                    <a class="btn bg-c-blue btn-round btn-comment " href="<?php echo base_url(); ?>report/Task_customisation">Customise</a>
                                 </div>
                              </h6>
                              <span class="time-frame"><label>Time Frame:</label>
                              <select id="filter_task">
                                 <option value="">Select</option>
                                 <option value="year">This Year</option>
                                 <option value="month">This Month</option>
                              </select>
                              </span>
                              <div class="card-block fileters_Tasks">
                                 <div id="pie_chart" style="width: 50%;"></div>
                                 <!-- progress bar section -->
                                 <div class="prodiv12">
                                    <?php
                                       if($notstarted_tasks['task_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"><?php echo $notstarted_tasks['task_count']; ?></span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $notstarted_tasks['task_count'].'%'; ?>;" aria-valuenow="<?php echo $notstarted_tasks['task_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_tasks['task_count']; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>    
                                    <?php
                                       if($completed_tasks['task_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"><?php echo $completed_tasks['task_count']; ?></span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $completed_tasks['task_count'].'%'; ?>;" aria-valuenow="<?php echo $completed_tasks['task_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_tasks['task_count']; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?> 
                                    <?php
                                       if($inprogress_tasks['task_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"><?php echo $inprogress_tasks['task_count']; ?></span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $inprogress_tasks['task_count'].'%'; ?>;" aria-valuenow="<?php echo $inprogress_tasks['task_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_tasks['task_count']; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?> 
                                    <?php
                                       if($awaiting_tasks['task_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"><?php echo $awaiting_tasks['task_count']; ?></span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $awaiting_tasks['task_count'].'%'; ?>;" aria-valuenow="<?php echo $awaiting_tasks['task_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_tasks['task_count']; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?> 
                                    <?php
                                       if($archive_tasks['task_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"><?php echo $archive_tasks['task_count']; ?></span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $archive_tasks['task_count'].'%'; ?>;" aria-valuenow="<?php echo $archive_tasks['task_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_tasks['task_count']; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>                                  
                                 </div>
                                 <!-- progress bar section -->
                              </div>
                           </div>
                        </div>
<?php $total_proposal_count = count($accept_proposal)+count($indiscussion_proposal)+count($sent_proposal)+count($viewed_proposal)+count($declined_proposal)+count($archive_proposal)+count($draft_proposal); ?>                                               
                        <div class="left-sireport col-xs-12 col-md-4">
                           <div class="comment-desc">
                              <h6>
                                 <span>Proposal</span>
                                 <div class="comment-btn">
                                    <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>Report/Proposal">Run</button>
                                    <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>report/Proposal_customisation">Customise</a>
                                 </div>
                              </h6>
                              <span class="time-frame"><label>Time Frame:</label>
                              <select id="filter_proposal">
                                 <option value="">Select</option>
                                 <option value="year">This Year</option>
                                 <option value="month">This Month</option>
                              </select>
                              </span>
                              <div class="card-block filter_proposal">
                                 <div id="piechart1" style="width: 50%; "></div>
                                 <!-- progress bar section -->
                                 <div class="prodiv12">
                                    <?php
                                       if(count($accept_proposal)!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"> <?php echo count($accept_proposal); ?></span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo count($accept_proposal).'%' ?>;" aria-valuenow="<?php echo count($accept_proposal); ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_proposal_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                    <?php
                                       if(count($indiscussion_proposal)!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"> <?php echo count($indiscussion_proposal); ?></span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo count($indiscussion_proposal).'%' ?>;" aria-valuenow="<?php echo count($indiscussion_proposal); ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_proposal_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                    <?php
                                       if(count($sent_proposal)!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"> <?php echo count($sent_proposal); ?></span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo count($sent_proposal).'%' ?>;" aria-valuenow="<?php echo count($sent_proposal); ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_proposal_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                    <?php
                                       if(count($viewed_proposal)!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"> <?php echo count($viewed_proposal); ?></span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo count($viewed_proposal).'%' ?>;" aria-valuenow="<?php echo count($viewed_proposal); ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_proposal_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                    <?php
                                       if(count($declined_proposal)!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"> <?php echo count($declined_proposal); ?></span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo count($declined_proposal).'%' ?>;" aria-valuenow="<?php echo count($declined_proposal); ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_proposal_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                    <?php
                                       if(count($archive_proposal)!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"> <?php echo count($archive_proposal); ?></span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo count($archive_proposal).'%' ?>;" aria-valuenow="<?php echo count($archive_proposal); ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_proposal_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                    <?php
                                       if(count($draft_proposal)!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"> <?php echo count($draft_proposal); ?></span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo count($draft_proposal).'%' ?>;" aria-valuenow="<?php echo count($draft_proposal); ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_proposal_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                 </div>
                                 <!-- progress bar section -->
                              </div>
                           </div>
                        </div>
                        <div class="left-sireport col-xs-12  col-md-4">
                           <div class="comment-desc">
                              <h6>
                                 <span>Leads</span>
                                 <div class="comment-btn">
                                    <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>Report/Leads">Run</a>
                                    <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>report/Leads_customisation">Customise</a>
                                 </div>
                              </h6>
                              <span class="time-frame"><label>Time Frame:</label>
                              <select id="filter_leads">
                                 <option value="">Select</option>
                                 <option value="year">This Year</option>
                                 <option value="month">This Month</option>
                              </select>
                              </span>
                              <div class="card-block filter_leads">
                                 <div id="piechart_1" style="width: 50%;"></div>
                                 <!-- progress bar section -->
                                 <div class="prodiv12">
                                    <?php    
                                       $date = isset($_POST['date'])?$_POST['date']:""; 
                                       $client = isset($_POST['client'])?$_POST['client']:"";

                                       foreach($leads_status as $status){ 
                                       $count=$this->Report_model->lead_details($status['id'],$date,$date,$client);
                                       if($count['lead_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"><?php echo $count['lead_count']; ?></span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $count['lead_count'].'%' ?>;" aria-valuenow="<?php echo $count['lead_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $overall_leads['leads_count']; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php }} ?>
                                 </div>
                                 <!-- progress bar section -->
                              </div>
                           </div>
                        </div>
                        <div class="left-sireport col-xs-12 col-md-4">
                           <div class="comment-desc">
                              <h6>
                                 <span>Service</span>
                                 <div class="comment-btn">
                                    <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>Report/Service">Run</button>
                                    <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>report/Service_customisation">Customise</a>
                                 </div>
                              </h6>
                              <span class="time-frame"><label>Time Frame:</label>
                              <select id="filter_service">
                                 <option value="">Select</option>
                                 <option value="year">This Year</option>
                                 <option value="month">This Month</option>
                              </select>
                              </span>

<?php $total_service_count = $conf_statement['service_count']+$accounts['service_count']+$company_tax_return['service_count']+$personal_tax_return['service_count']+$payroll['service_count']+$workplace['service_count']+$vat['service_count']+$cis['service_count']+$cissub['service_count']+$p11d['service_count']+$bookkeep['service_count']+$management['service_count']+$investgate['service_count']+$registered['service_count']+$taxadvice['service_count']+$taxinvest['service_count']; ?>                                                      
                              <div class="card-block filter_service">
                                 <div id="piechart_service" style="width: 50%; "></div>
                                 <!-- progress bar section -->
                                 <div class="prodiv12">
                                    <?php
                                       if($conf_statement['service_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"><?php echo $conf_statement['service_count']; ?> </span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $conf_statement['service_count'].'%'; ?>;" aria-valuenow="<?php echo $conf_statement['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_service_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                    <?php
                                       if($accounts['service_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"><?php echo $accounts['service_count']; ?> </span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $accounts['service_count'].'%'; ?>;" aria-valuenow="<?php echo $accounts['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_service_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                    <?php
                                       if($company_tax_return['service_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"><?php echo $company_tax_return['service_count']; ?> </span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $company_tax_return['service_count'].'%'; ?>;" aria-valuenow="<?php echo $company_tax_return['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_service_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                    <?php
                                       if($personal_tax_return['service_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"><?php echo $personal_tax_return['service_count']; ?> </span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $personal_tax_return['service_count'].'%'; ?>;" aria-valuenow="<?php echo $personal_tax_return['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_service_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                    <?php
                                       if($vat['service_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"><?php echo $vat['service_count']; ?> </span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $vat['service_count'].'%'; ?>;" aria-valuenow="<?php echo $vat['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_service_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                    <?php
                                       if($payroll['service_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"><?php echo $payroll['service_count']; ?> </span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $payroll['service_count'].'%'; ?>;" aria-valuenow="<?php echo $payroll['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_service_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                    <?php
                                       if($workplace['service_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"><?php echo $workplace['service_count']; ?> </span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $workplace['service_count'].'%'; ?>;" aria-valuenow="<?php echo $workplace['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_service_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                    <?php
                                       if($cis['service_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"><?php echo $cis['service_count']; ?> </span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $cis['service_count'].'%'; ?>;" aria-valuenow="<?php echo $cis['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_service_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                    <?php
                                       if($cissub['service_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"><?php echo $cissub['service_count']; ?> </span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $cissub['service_count'].'%'; ?>;" aria-valuenow="<?php echo $cissub['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_service_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                    <?php
                                       if($p11d['service_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"><?php echo $p11d['service_count']; ?> </span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $p11d['service_count'].'%'; ?>;" aria-valuenow="<?php echo $p11d['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_service_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                    <?php
                                       if($management['service_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"><?php echo $management['service_count']; ?> </span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $management['service_count'].'%'; ?>;" aria-valuenow="<?php echo $management['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_service_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                    <?php
                                       if($bookkeep['service_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"><?php echo $bookkeep['service_count']; ?> </span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $bookkeep['service_count'].'%'; ?>;" aria-valuenow="<?php echo $bookkeep['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_service_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                    <?php
                                       if($investgate['service_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"><?php echo $investgate['service_count']; ?> </span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $investgate['service_count'].'%'; ?>;" aria-valuenow="<?php echo $investgate['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_service_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                    <?php
                                       if($registered['service_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"><?php echo $registered['service_count']; ?> </span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $registered['service_count'].'%'; ?>;" aria-valuenow="<?php echo $registered['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_service_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                    <?php
                                       if($taxadvice['service_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"><?php echo $taxadvice['service_count']; ?> </span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $taxadvice['service_count'].'%'; ?>;" aria-valuenow="<?php echo $taxadvice['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_service_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                    <?php
                                       if($taxinvest['service_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"><?php echo $taxinvest['service_count']; ?> </span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $taxinvest['service_count'].'%'; ?>;" aria-valuenow="<?php echo $taxinvest['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_service_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                 </div>
                                 <!-- progress bar section -->
                              </div>
                           </div>
                        </div>
                        <div class="left-sireport col-xs-12  col-md-4">
                           <div class="comment-desc">
                              <h6>
                                <span> Deadline Manager</span>
                                 <div class="comment-btn">
                                    <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>Report/Dead_line">Run</a>
                                    <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>report/Deadline_customisation">Customise</a>
                                 </div>
                              </h6>
                              <span class="time-frame"><label>Time Frame:</label>
                              <select id="filter_deadline">
                                 <option value="">Select</option>
                                 <option value="year">This Year</option>
                                 <option value="month">This Month</option>
                              </select>
                              </span>
<?php $total_deadline_count = $de_conf_statement['service_count']+$de_accounts['service_count']+$de_company_tax_return['service_count']+$de_personal_tax_return['service_count']+$de_payroll['service_count']+$de_workplace['service_count']+$de_vat['service_count']+$de_cis['service_count']+$de_cissub['service_count']+$de_p11d['service_count']+$de_bookkeep['service_count']+$de_management['service_count']+$de_investgate['service_count']+$de_registered['service_count']+$de_taxadvice['service_count']+$de_taxinvest['service_count']; ?>
                              <div class="card-block filter_deadline">
                                 <div id="piechart_deadline" style="width: 50%; "></div>
                                 <!-- progress bar section -->
                                 <div class="prodiv12">
                                    <?php if($de_accounts['service_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"> <?php echo $de_accounts['service_count']; ?></span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_accounts['service_count'].'%'; ?>;" aria-valuenow="<?php echo $de_accounts['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_deadline_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                    <?php if($de_company_tax_return['service_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"> <?php echo $de_company_tax_return['service_count']; ?></span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_company_tax_return['service_count'].'%'; ?>;" aria-valuenow="<?php echo $de_company_tax_return['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_deadline_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                    <?php if($de_conf_statement['service_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"> <?php echo $de_conf_statement['service_count']; ?></span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_conf_statement['service_count'].'%'; ?>;" aria-valuenow="<?php echo $de_conf_statement['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_deadline_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                    <?php if($de_personal_tax_return['service_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"> <?php echo $de_personal_tax_return['service_count']; ?></span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_personal_tax_return['service_count'].'%'; ?>;" aria-valuenow="<?php echo $de_personal_tax_return['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_deadline_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                    <?php if($de_vat['service_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"> <?php echo $de_vat['service_count']; ?></span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_vat['service_count'].'%'; ?>;" aria-valuenow="<?php echo $de_vat['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_deadline_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                    <?php if($de_payroll['service_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"> <?php echo $de_payroll['service_count']; ?></span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_payroll['service_count'].'%'; ?>;" aria-valuenow="<?php echo $de_payroll['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_deadline_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>    <?php if($de_workplace['service_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"> <?php echo $de_workplace['service_count']; ?></span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_workplace['service_count'].'%'; ?>;" aria-valuenow="<?php echo $de_workplace['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_deadline_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>    <?php if($de_cis['service_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"> <?php echo $de_cis['service_count']; ?></span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_cis['service_count'].'%'; ?>;" aria-valuenow="<?php echo $de_cis['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_deadline_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>    <?php if($de_cissub['service_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"> <?php echo $de_cissub['service_count']; ?></span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_cissub['service_count'].'%'; ?>;" aria-valuenow="<?php echo $de_cissub['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_deadline_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>    <?php if($de_p11d['service_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"> <?php echo $de_p11d['service_count']; ?></span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_p11d['service_count'].'%'; ?>;" aria-valuenow="<?php echo $de_p11d['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_deadline_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                    <?php if($de_management['service_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"> <?php echo $de_management['service_count']; ?></span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_management['service_count'].'%'; ?>;" aria-valuenow="<?php echo $de_management['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_deadline_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                    <?php if($de_bookkeep['service_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"> <?php echo $de_bookkeep['service_count']; ?></span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_bookkeep['service_count'].'%'; ?>;" aria-valuenow="<?php echo $de_bookkeep['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_deadline_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                    <?php if($de_investgate['service_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"> <?php echo $de_investgate['service_count']; ?></span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_investgate['service_count'].'%'; ?>;" aria-valuenow="<?php echo $de_investgate['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_deadline_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                    <?php if($de_registered['service_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"> <?php echo $de_registered['service_count']; ?></span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_registered['service_count'].'%'; ?>;" aria-valuenow="<?php echo $de_registered['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_deadline_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                    <?php if($de_taxadvice['service_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"> <?php echo $de_taxadvice['service_count']; ?></span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_taxadvice['service_count'].'%'; ?>;" aria-valuenow="<?php echo $de_taxadvice['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_deadline_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                    <?php if($de_taxinvest['service_count']!='0'){ ?>
                                    <div class="pro-color">
                                       <span class="procount"> <?php echo $de_taxinvest['service_count']; ?></span>
                                       <div class="fordis">
                                          <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $de_taxinvest['service_count'].'%'; ?>;" aria-valuenow="<?php echo $de_taxinvest['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_deadline_count; ?>"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                 </div>
                                 <!-- progress bar section -->
                              </div>
                           </div>
                        </div>
                        <div class="left-sireport col-xs-12 col-md-4">
                           <div class="comment-desc">
                              <h6>
                                <span> Invoice</span>
                                 <div class="comment-btn">
                                    <a class="btn bg-c-blue btn-round btn-comment" href="<?php echo base_url(); ?>Report/Invoice">Run</a>
                                    <a class="btn bg-c-blue btn-round btn-comment " href="<?php echo base_url(); ?>report/Invoice_customisation">Customise</a>
                                 </div>
                              </h6>
                              <span class="time-frame"><label>Time Frame:</label>
                              <select id="filter_invoice">
                                 <option value="">Select</option>
                                 <option value="year">This Year</option>
                                 <option value="month">This Month</option>
                              </select>
                              </span>
                              <div class="card-block fileters_invoice">
                                 <div id="piechart_invoice" style="width: 50%;"></div>
                                 <!-- progress bar section -->
                                 <!-- <div class="prodiv12"> -->
                                   <?php //$total_invoice_count = $approved_invoice['invoice_count']+$cancel_invoice['invoice_count']+$expired_invoice['invoice_count']; ?> 
                                     <div class="prodiv12">
                                   <?php
                                   if($approved_invoice['invoice_count']!='0'){
                                   ?>
                                   <div class="pro-color">
                                   <span class="procount"><?php echo $approved_invoice['invoice_count']; ?> </span>
                                   <div class="fordis">
                                   <div class="progress progress-xs">
                                   <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $approved_invoice['invoice_count'].'%'; ?> ;" aria-valuenow="<?php echo $approved_invoice['invoice_count']; ?> " aria-valuemin="0" aria-valuemax="<?php echo $total_invoice_count; ?>"></div>
                                   </div>
                                   </div>
                                   </div>
                                     <?php } ?>   

                                     <?php
                                   if($cancel_invoice['invoice_count']!='0'){
                                   ?>
                                   <div class="pro-color">
                                   <span class="procount"><?php echo $cancel_invoice['invoice_count']; ?> </span>
                                   <div class="fordis">
                                   <div class="progress progress-xs">
                                   <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $cancel_invoice['invoice_count'].'%'; ?> ;" aria-valuenow="<?php echo $cancel_invoice['invoice_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_invoice_count; ?>"></div>
                                   </div>
                                   </div>
                                   </div>
                                     <?php } ?>  

                                     <?php
                                   if($expired_invoice['invoice_count']!='0'){
                                   ?>
                                   <div class="pro-color">
                                   <span class="procount"><?php echo $expired_invoice['invoice_count']; ?> </span>
                                   <div class="fordis">
                                   <div class="progress progress-xs">
                                   <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $expired_invoice['invoice_count'].'%'; ?> ;" aria-valuenow="<?php echo $expired_invoice['invoice_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_invoice_count; ?>"></div>
                                   </div>
                                   </div>
                                   </div>
                                     <?php } ?>                                               
                                   </div>                                            
                                <!--  </div> -->
                                 <!-- progress bar section -->
                              </div>
                           </div>
                        </div>
                        <div class="left-sireport col-xs-12 col-md-4">
                           <div class="comment-desc sumlast">
                              <h6>
                                 <span>Summary</span>
                                 <div class="comment-btn">                                                         
                                    <a class="btn bg-c-blue btn-round btn-comment " href="<?php echo base_url(); ?>Report/Summary">Run</a>
                                    <a class="btn bg-c-blue btn-round btn-comment " href="<?php echo base_url(); ?>Report/SummaryCustomisation">Customise</a>
                                 </div>
                              </h6>
                              <!-- progress bar section -->
                              <div class="prodiv">
                                 <div class="pro-color">
                                    <span class="procount info"><?php echo $users_count['user_count']; ?></span>
                                    <div class="fordis">
                                       <label>Total Users</label>
                                       <div class="progress progress-xs">
                                          <div class="progress-bar progress-bar-info" role="progressbar" style="width: <?php echo $users_count['user_count'].'%'; ?>;" aria-valuenow="<?php echo $users_count['user_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $users_count['user_count']; ?>"></div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="pro-color">
                                    <span class="procount primary"><?php echo $active_user_count['activeuser_count']; ?></span>
                                    <div class="fordis">
                                       <label>Active Users</label>
                                       <div class="progress progress-xs">
                                          <div class="progress-bar progress-bar-primary" role="progressbar" style="width: <?php echo $active_user_count['activeuser_count'].'%'; ?>;" aria-valuenow="<?php echo $active_user_count['activeuser_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $users_count['user_count']; ?>"></div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="pro-color">
                                    <span class="procount warning"><?php echo $inactive_user_count['inactiveuser_count']; ?></span>
                                    <div class="fordis">
                                       <label>Inactive Users</label>
                                       <div class="progress progress-xs">
                                          <div class="progress-bar progress-bar-warning" role="progressbar" style="width: <?php echo $inactive_user_count['inactiveuser_count'].'%'; ?>;" aria-valuenow="<?php echo $inactive_user_count['inactiveuser_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $users_count['user_count']; ?>"></div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="pro-color">
                                    <span class="procount danger"><?php echo $completed_task_count['task_count']; ?></span>
                                    <div class="fordis">
                                       <label>Completed Tasks</label>
                                       <div class="progress progress-xs">
                                          <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $completed_task_count['task_count']; ?>;" aria-valuenow="<?php echo $completed_task_count['task_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_tasks['task_count']; ?>"></div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="pro-color success">
                                    <span class="procount"><?php echo $overall_leads['leads_count']; ?></span>
                                    <div class="fordis">
                                       <label>Over all Leads</label>
                                       <div class="progress progress-xs">
                                          <div class="progress-bar progress-bar-success" role="progressbar" style="width: <?php echo $overall_leads['leads_count'].'%'; ?>;" aria-valuenow="<?php echo $overall_leads['leads_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $overall_leads['leads_count']; ?>"></div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!-- progress bar section -->
                           </div>
                        </div>
                        <div class="left-sireport col-xs-12 col-md-4">

                        <div class="comment-desc sumlast">
                              <h6>
                                 Timesheet
                                 <div class="comment-btn">                                                        
                                    <a class="btn bg-c-blue btn-round btn-comment " href="<?php echo base_url(); ?>Timesheet">Run</a>
                                    <a class="btn bg-c-blue btn-round btn-comment " href="<?php echo base_url(); ?>Timesheet/timesheet_custom">Customise</a>
                                 </div>
                              </h6>
                              </div>
                        </div>


                        <div class="left-sireport col-xs-12 col-md-4">

                        <div class="comment-desc sumlast">
                              <h6>
                               <span>  Individual User Performance</span>
                                 <div class="comment-btn">                                                        
                                    <a class="btn bg-c-blue btn-round btn-comment " href="<?php echo base_url(); ?>Reports/user_performation">Run</a>
                                    <a class="btn bg-c-blue btn-round btn-comment " href="<?php echo base_url(); ?>Reports/performance_customisation">Customise</a>
                                 </div>
                              </h6>


                              </div>
                          
                        </div>
                     </div>
                      
                  </div>
               </div>
               <div class="tab-pane fade legal-disk01" id="clients-reports">
                  <div class="card-block-small">
                     <div class="left-sireport col-xs-12  col-md-6">
                        <div  class="comment-desc">
                        <h6>
                                 Legal Form 
                                
                              </h6>
                        <div class="card-block fileters_client new123">
                        <div id="Tabs_client" style="width:100%;">      
                        </div>
                     </div>
                     </div>
                     </div>
                    <div class="left-sireport col-xs-12  col-md-6">
                     <div class="comment-desc">
                     <h6>
                                 Status 
                                 
                              </h6>
                     <div class="card-block fileters_client new123">
                        <div id="Tabs_Users" style="width:100%;"></div>
                        </div>
                     </div>
                     </div>
                  </div>
               </div>
               <div class="tab-pane fade legal-disk01" id="task-reports">
                  <div class="card-block-small">
                  <div class="left-sireport col-xs-12  col-md-6">
                        <div  class="comment-desc">
                        <h6>
                                 Status  
                              </h6>
                        <div class="card-block fileters_client new123">
                     <div id="task_tab"></div>
                     <div class="prodiv12">
          <?php
             if($notstarted_tasks['task_count']!='0'){ ?>
          <div class="pro-color">
             <span class="procount"><?php echo $notstarted_tasks['task_count']; ?></span>
             <div class="fordis">
                <div class="progress progress-xs">
                   <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $notstarted_tasks['task_count'].'%'; ?>;" aria-valuenow="<?php echo $notstarted_tasks['task_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_tasks['task_count']; ?>"></div>
                </div>
             </div>
          </div>
          <?php } ?>    
          <?php
             if($completed_tasks['task_count']!='0'){ ?>
          <div class="pro-color">
             <span class="procount"><?php echo $completed_tasks['task_count']; ?></span>
             <div class="fordis">
                <div class="progress progress-xs">
                   <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $completed_tasks['task_count'].'%'; ?>;" aria-valuenow="<?php echo $completed_tasks['task_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_tasks['task_count']; ?>"></div>
                </div>
             </div>
          </div>
          <?php } ?> 
          <?php
             if($inprogress_tasks['task_count']!='0'){ ?>
          <div class="pro-color">
             <span class="procount"><?php echo $inprogress_tasks['task_count']; ?></span>
             <div class="fordis">
                <div class="progress progress-xs">
                   <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $inprogress_tasks['task_count'].'%'; ?>;" aria-valuenow="<?php echo $inprogress_tasks['task_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_tasks['task_count']; ?>"></div>
                </div>
             </div>
          </div>
          <?php } ?> 
          <?php
             if($awaiting_tasks['task_count']!='0'){ ?>
          <div class="pro-color">
             <span class="procount"><?php echo $awaiting_tasks['task_count']; ?></span>
             <div class="fordis">
                <div class="progress progress-xs">
                   <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $awaiting_tasks['task_count'].'%'; ?>;" aria-valuenow="<?php echo $awaiting_tasks['task_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_tasks['task_count']; ?>"></div>
                </div>
             </div>
          </div>
          <?php } ?> 
          <?php
             if($archive_tasks['task_count']!='0'){ ?>
          <div class="pro-color">
             <span class="procount"><?php echo $archive_tasks['task_count']; ?></span>
             <div class="fordis">
                <div class="progress progress-xs">
                   <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $archive_tasks['task_count'].'%'; ?>;" aria-valuenow="<?php echo $archive_tasks['task_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_tasks['task_count']; ?>"></div>
                </div>
             </div>
          </div>
          <?php } ?>                            
                     </div>
                     </div>
                     </div>
                     </div>
                     <div class="left-sireport col-xs-12  col-md-6">
                     <div  class="comment-desc">
                     <h6> Priority </h6>
                     <div class="card-block fileters_client new123">
                        <div id="tabs_priority"></div>
                     </div>
                     </div>
                     </div>
                     <div class="left-sireport col-xs-12 line-chart123">
                     <div  class="comment-desc">
                     <h6>
                                 This Month Tasks 
                                 
                              </h6>
                        <div class="card-block fileters_client new123">
                     <div id="monthly_task" style="width: 1200px; height: 500px"></div>
                     </div>
                     </div>
                     </div>
                  </div>
               </div>
               <div class="tab-pane fade" id="proposals-reports">
                  <div class="card-block-small">
                     <div class="row history">
                        <div class="card">
                           <div class="card-header">
                              <h5>This Month Proposal History</h5>
                           </div>
                           <div class="card-block">
                              <div class="main-timeline">
                                 <div class="cd-timeline cd-container">
                                    <?php 
                                       $i=1;
                                       foreach($proposal_history as $p_h){  
                                       $randomstring=generateRandomString('100');
                                       if($p_h['status'] == 'opened'){$p_h['status']='viewed';}
                                       ?>
                                     <div class="cd-timeline-block" id="<?php echo $i; ?>">
                                       <div class="cd-timeline-icon bg-primary">
                                         <i class="icofont icofont-ui-file"></i>
                                       </div>
                                       <div class="cd-timeline-content card_main">
                                         <div class="p-20">
                                           <div class="timeline-details">
                                             <p class="m-t-20 search_word">
                                               <a href="<?php echo base_url().'proposal_page/step_proposal/'.$randomstring.'---'.$p_h['id']; ?>"><?php echo $p_h['proposal_name']; ?> </a> 
                                             </p>
                                           </div>
                                         </div>
                                         <span class="cd-date"><?php
                                           $timestamp = strtotime($p_h['created_at']);
                                           $newDate = date('d F Y H:i', $timestamp); 
                                           echo $newDate;
                                           ?></span>
                                         <span class="cd-date"><?php echo 'Sent By : '.$p_h['sender_company_name']; ?></span>
                                         <span class="cd-date"><?php echo 'Status : '.ucfirst($p_h['status']); ?></span>
                                       </div>
                                     </div>
                                     <?php $i++; } ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="tab-pane fade" id="leads-reports">
                  <div class="card-block-small">
                     <div class="row history">
                        <div class="card">
                           <div class="card-header">
                              <h5>This Month Leads History</h5>
                           </div>
                           <div class="card-block">
                              <div class="main-timeline">
                                 <div class="cd-timeline cd-container">
                                   <?php
                                      foreach($leads_history as $lead){                              
                                      ?>
                                   <div class="cd-timeline-block">
                                      <div class="cd-timeline-icon bg-primary">
                                         <i class="icofont icofont-ui-file"></i>
                                      </div>
                                      <div class="cd-timeline-content card_main">
                                         <div class="p-20">
                                            <div class="timeline-details">
                                               <p class="m-t-20"><a href="<?php echo base_url().'leads/leads_detailed_tab/'.$lead['id']; ?>"> <?php echo ucwords($lead['name']); ?></a>
                                               </p>
                                            </div>
                                         </div>
                                         <span class="cd-date"><?php
                                            $timestamp = $lead['createdTime'];
                                            $newDate = date('d F Y H:i', $timestamp); 
                                            echo $newDate;
                                            ?></span>
                                         <span class="cd-date">Status:&nbsp;<?php              
                                            echo ucwords($this->Common_mdl->get_price('leads_status','id',$lead['lead_status'],'status_name'));
                                            ?></span>
                                      </div>
                                   </div>
                                   <?php } ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="tab-pane fade com-chart legal-disk01" id="services-reports">
                  <div class="card-block-small">
                  <div class="left-sireport col-xs-12  col-md-6">
                           <div class="comment-desc">
                           <h6>  Services  </h6>
                     <div id="service_tab" style="width:100%;"></div>
                     </div>
                     </div>
                  </div>
               </div>
               <div class="tab-pane fade com-chart legal-disk01" id="deadlines-reports">
                  <div class="card-block-small">
                  <div class="left-sireport col-xs-12  col-md-6">
                           <div class="comment-desc">
                           <h6> This Month Deadlines  </h6>
                     <div id="Tabs_deadline" style="width:100%;"></div>
                     </div>
                     </div>
                  </div>
               </div>
               <div class="tab-pane fade com-chart legal-disk01" id="invoices-reports">
                  <div class="card-block-small">
                  <div class="left-sireport col-xs-12  col-md-6">
                           <div class="comment-desc">
                           <h6> This Month Invoice   </h6>
                     <div id="Tabs_invoice" style="width:100%;"></div>
                     </div>
                     </div>
                  </div>
               </div>
            </div> <!--tab content close -->
            <!-- home-->
            <!-- frozen -->
             </div>
             <!-- ul after div -->
          </div>
       </div>
       <!-- admin close -->
    </div>
    <!-- Register your self card end -->
 </div>

 <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
 <script type="text/javascript">

   google.charts.load("current", {"packages":["corechart"]});

   google.charts.setOnLoadCallback(drawChart);
   function drawChart() {
     var data = google.visualization.arrayToDataTable([
       ['Legal Form','link','Count'],
       ['Private Limited company', '<?php echo base_url();?>Firm_dashboard/private_function', <?php echo $Private['limit_count']; ?>],
       ['Public Limited company',  '<?php echo base_url();?>Firm_dashboard/public_function',   <?php echo $Public['limit_count']; ?>],
       ['Limited Liability Partnership','<?php echo base_url();?>Firm_dashboard/limited_function', <?php echo $limited['limit_count']; ?>],
       ['Partnership', '<?php echo base_url();?>Firm_dashboard/partnership', <?php echo $Partnership['limit_count']; ?>],
       ['Self Assessment','<?php echo base_url();?>Firm_dashboard/self_assesment',   <?php echo $self['limit_count']; ?>],
       ['Trust','<?php echo base_url();?>Firm_dashboard/trust',<?php echo $Trust['limit_count']; ?>],
       ['Charity','<?php echo base_url();?>Firm_dashboard/charity',<?php echo $Charity['limit_count']; ?>],
        ['Other','<?php echo base_url();?>Firm_dashboard/other',<?php echo $Other['limit_count']; ?>],
     ]);  
   
      var view = new google.visualization.DataView(data);
          view.setColumns([0, 2]);
    
   
     var options = {
       chartArea: {left: 0, width: "80%"},
        height: 250,
       legend: { position: 'bottom', show: true, container: $("#large-legend"), alignment: 'start' },
       hAxis : { textStyle : { fontSize: 8} },
       vAxis : { textStyle : { fontSize: 8} },
         width: 350,
        height: 250
      // pieHole: 0.4,
     };  
     var chart_div = new google.visualization.PieChart(document.getElementById('chart_Donut11'));

     // new google.visualization.PieChart(document.getElementById('container'))
     var chart = new google.visualization.PieChart(document.getElementById('chart_Donut11'));
     google.visualization.events.addListener(chart, 'select', function () {
     chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
     //document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '"></a>';
     var image=chart.getImageURI();
        $(document).ready(function(){   
                  $.ajax({
                         type: "POST",
                         url: "<?php echo base_url();?>/Reports/pdf_download",         
                         data: {image: image},
                         success: 
                              function(data){
                     
                              }
                          });
                   });
   });
     chart.draw(view, options);
   
     var selectHandler = function(e) {
          //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
          window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
          }
          // Add our selection handler.
          google.visualization.events.addListener(chart, 'select', selectHandler);
   }


   
    google.charts.setOnLoadCallback(drawChart1);
    function drawChart1() {
      var data = google.visualization.arrayToDataTable([
        ['Task','link','Count'],
        ['Not started', '<?php echo base_url(); ?>Firm_dashboard/notstarted',<?php echo $notstarted_tasks['task_count']; ?>],
        ['Completed',  '<?php echo base_url(); ?>Firm_dashboard/complete',<?php echo $completed_tasks['task_count']; ?>],
        ['Inprogress','<?php echo base_url(); ?>Firm_dashboard/inprogress',<?php echo $inprogress_tasks['task_count']; ?>],
        ['Awaiting Feedback','<?php echo base_url(); ?>Firm_dashboard/awaited',<?php echo $awaiting_tasks['task_count']; ?>],
        ['Archived',  '<?php echo base_url(); ?>Firm_dashboard/archive',<?php echo $archive_tasks['task_count']; ?>] 
      ]);  
   
       var view = new google.visualization.DataView(data);
           view.setColumns([0, 2]);
     
   
      var options = {
         chartArea: {left:0, width: "80%"},
         height:250, 
        legend: { position: 'bottom', show: true, container: $("#large-legend"), alignment: 'start' },
        hAxis : { textStyle : { fontSize: 8} },
        vAxis : { textStyle : { fontSize: 8} },
          width: 350,
          height: 250
       // pieHole: 0.4,
      };  
      var chart_div = new google.visualization.PieChart(document.getElementById('pie_chart'));
      var chart = new google.visualization.PieChart(pie_chart);
      google.visualization.events.addListener(chart, 'select', function () {
      chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
      //document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '"></a>';
      var image=chart.getImageURI();
         $(document).ready(function(){   
                   $.ajax({
                          type: "POST",
                          url: "<?php echo base_url();?>/Reports/task_chart",         
                          data: {chart_image: image},
                          success: 
                               function(data){
                      
                               }
                           });
                    });
    });
      chart.draw(view, options);
       var selectHandler = function(e) {
          //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
            window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
           }
           // Add our selection handler.
           google.visualization.events.addListener(chart, 'select', selectHandler);
    }

   google.charts.setOnLoadCallback(drawChart2);
   
   function drawChart2() {
   
    var data = google.visualization.arrayToDataTable([
      ['Proposal','link','Count'],
      ['Accepted', '<?php echo base_url(); ?>Firm_dashboard/accept', <?php echo count($accept_proposal); ?>],
      ['In Discussion',  '<?php echo base_url(); ?>Firm_dashboard/indiscussion', <?php echo count($indiscussion_proposal); ?>],
      ['Sent','<?php echo base_url(); ?>Firm_dashboard/sent',  <?php echo count($sent_proposal); ?>],
      ['Viewed','<?php echo base_url(); ?>Firm_dashboard/view', <?php echo count($viewed_proposal); ?>],
      ['Declined', '<?php echo base_url(); ?>Firm_dashboard/decline',   <?php echo count($declined_proposal); ?>],
      ['Archived',  '<?php echo base_url(); ?>Firm_dashboard/archieve',  <?php echo count($archive_proposal); ?>],
      ['Draft',  '<?php echo base_url(); ?>Firm_dashboard/draft',  <?php echo count($draft_proposal); ?>]
    ]);     
   
     var view = new google.visualization.DataView(data);
      view.setColumns([0, 2]); 
   
    var options = {
        chartArea: {left:0, width: "80%"},
        height:250, 
   legend: { position: 'bottom', show: true, container: $("#large-legend"), alignment: 'start' },
   hAxis : { textStyle : { fontSize: 8} },
   vAxis : { textStyle : { fontSize: 8} },
     width: 350,
     height: 250
   // pieHole: 0.4,
    };
   
     var chart_div = new google.visualization.PieChart(document.getElementById('piechart1'));
   var chart = new google.visualization.PieChart(document.getElementById('piechart1'));
   google.visualization.events.addListener(chart, 'select', function () {
   chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
   //document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '"></a>';
   var image=chart.getImageURI();
    $(document).ready(function(){  
   // alert(image);
              $.ajax({
                     type: "POST",
                     url: "<?php echo base_url();?>/Report/proposal_chart",         
                     data: {proposal_image: image},
                     success: 
                          function(data){
                 
                          }
                      });
               });
   });
   chart.draw(view, options);
   var selectHandler = function(e) {
     //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
       window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
      }
      // Add our selection handler.
      google.visualization.events.addListener(chart, 'select', selectHandler);
   }
   

   google.charts.setOnLoadCallback(drawChart3);
   
   function drawChart3() {
   
     var data = google.visualization.arrayToDataTable([
       ['Leads','link','Count'],
           <?php      
      foreach($leads_status as $status){
      $status_name=$status['status_name'];
      $count=$this->Report_model->lead_details($status['id'],$date,$date,$client); ?>
            ['<?php echo $status_name; ?>','<?php echo base_url(); ?>/leads?status=<?php $strlower = strtolower(preg_replace("~[\\\\/:*?'&,<>|]~", '', $status_name));
      $lesg = str_replace(' ', '-', $strlower);
      $lesgs = str_replace('--', '-', $lesg); echo $lesgs; ?>',<?php echo $count['lead_count']; ?> ],
            <?php }  ?>
      ]);      
     var view = new google.visualization.DataView(data);
       view.setColumns([0, 2]);
   
     var options = {
          chartArea: {left:0, width: "80%"}, 
          height:250,
    legend: { position: 'bottom', show: true, container: $("#large-legend"), alignment: 'start' },
    hAxis : { textStyle : { fontSize: 8} },
    vAxis : { textStyle : { fontSize: 8} },
      width: 350,
      height: 250
   // pieHole: 0.4,
     };
   
   var chart_div = new google.visualization.PieChart(document.getElementById('piechart_1'));
   var chart = new google.visualization.PieChart(document.getElementById('piechart_1'));
   google.visualization.events.addListener(chart, 'select', function () {
   chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
   //document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '"></a>';
   var image=chart.getImageURI();
     $(document).ready(function(){  
    // alert(image);
               $.ajax({
                      type: "POST",
                      url: "<?php echo base_url();?>/Report/lead_chart",         
                      data: {lead_image: image},
                      success: 
                           function(data){
                  
                           }
                       });
                });
   });
   chart.draw(view, options);
   var selectHandler = function(e) {
       //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
       window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
       }
       // Add our selection handler.
       google.visualization.events.addListener(chart, 'select', selectHandler);
   }
   

   google.charts.setOnLoadCallback(drawChart4);
   
   function drawChart4() {
   
     var data = google.visualization.arrayToDataTable([
       ['Service','link','Count'],             
        ['Confirmation Statement','<?php echo base_url(); ?>Firm_dashboard/confirm_statement',<?php echo $conf_statement['service_count']; ?> ],
        ['Accounts', '<?php echo base_url(); ?>Firm_dashboard/accounts',<?php echo $accounts['service_count']; ?> ],
        ['Company Tax Return','<?php echo base_url(); ?>Firm_dashboard/company_tax', <?php echo $company_tax_return['service_count']; ?> ],
        ['Personal Tax Return','<?php echo base_url(); ?>Firm_dashboard/personal_tax', <?php echo $personal_tax_return['service_count']; ?> ],
        ['VAT Returns','<?php echo base_url(); ?>Firm_dashboard/vat', <?php echo $vat['service_count']; ?> ],
        ['Payroll','<?php echo base_url(); ?>Firm_dashboard/payroll', <?php echo $payroll['service_count']; ?> ],
        ['WorkPlace Pension - AE','<?php echo base_url(); ?>Firm_dashboard/workplace', <?php echo $workplace['service_count']; ?> ],
        ['CIS - Contractor','<?php echo base_url(); ?>Firm_dashboard/cis', <?php echo $cis['service_count']; ?> ],
        ['CIS - Sub Contractor','<?php echo base_url(); ?>Firm_dashboard/cis_sub', <?php echo $cissub['service_count']; ?> ],
        ['P11D','<?php echo base_url(); ?>Firm_dashboard/p11d', <?php echo $p11d['service_count']; ?> ],
        ['Management Accounts','<?php echo base_url(); ?>Firm_dashboard/management',  <?php echo $management['service_count']; ?> ],
        ['Bookkeeping','<?php echo base_url(); ?>Firm_dashboard/bookkeeping', <?php echo $bookkeep['service_count']; ?> ],
        ['Investigation Insurance','<?php echo base_url(); ?>Firm_dashboard/investication', <?php echo $investgate['service_count']; ?> ],
        ['Registered Address','<?php echo base_url(); ?>Firm_dashboard/register', <?php echo $registered['service_count']; ?> ],
        ['Tax Advice','<?php echo base_url(); ?>Firm_dashboard/tax',  <?php echo $taxadvice['service_count']; ?> ],
        ['Tax Investigation','<?php echo base_url(); ?>Firm_dashboard/tax_inves',  <?php echo $taxinvest['service_count']; ?> ],          
      ]);  
   
       var view = new google.visualization.DataView(data);
       view.setColumns([0, 2]);    
   
     var options = {
          chartArea: {left:0, width: "80%"}, 
          height: 250,
    legend: { position: 'bottom', show: true, container: $("#large-legend"), alignment: 'start' },
    hAxis : { textStyle : { fontSize: 8} },
    vAxis : { textStyle : { fontSize: 8} },
      width: 350,
      height: 250
   //  pieHole: 0.4,
     };
   
   var chart_div = new google.visualization.PieChart(document.getElementById('piechart_service'));
   var chart = new google.visualization.PieChart(document.getElementById('piechart_service'));
   google.visualization.events.addListener(chart, 'select', function () {
   chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
   //document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '"></a>';
   var image=chart.getImageURI();
     $(document).ready(function(){  
    // alert(image);
               $.ajax({
                      type: "POST",
                      url: "<?php echo base_url();?>/Report/service_chart",         
                      data: {service_image: image},
                      success: 
                           function(data){
                  
                           }
                       });
                });
   });
   chart.draw(view, options);
   var selectHandler = function(e) {
       //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
       window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
       }
       // Add our selection handler.
       google.visualization.events.addListener(chart, 'select', selectHandler);
       }
   

   google.charts.setOnLoadCallback(drawChart5);
   
   function drawChart5() {
   
     var data = google.visualization.arrayToDataTable([
       ['Service', 'link','Count'],             
        ['Confirmation Statement','<?php echo base_url(); ?>Firm_dashboard/confirm_statement',<?php echo $de_conf_statement['service_count']; ?> ],
        ['Accounts','<?php echo base_url(); ?>Firm_dashboard/accounts', <?php echo $de_accounts['service_count']; ?> ],
        ['Company Tax Return','<?php echo base_url(); ?>Firm_dashboard/company_tax', <?php echo $de_company_tax_return['service_count']; ?> ],
        ['Personal Tax Return','<?php echo base_url(); ?>Firm_dashboard/personal_tax', <?php echo $de_personal_tax_return['service_count']; ?> ],
        ['VAT Returns','<?php echo base_url(); ?>Firm_dashboard/vat', <?php echo $de_vat['service_count']; ?> ],
        ['Payroll','<?php echo base_url(); ?>Firm_dashboard/payroll', <?php echo $de_payroll['service_count']; ?> ],
        ['WorkPlace Pension - AE','<?php echo base_url(); ?>Firm_dashboard/workplace', <?php echo $de_workplace['service_count']; ?> ],
        ['CIS - Contractor','<?php echo base_url(); ?>Firm_dashboard/cis', <?php echo $de_cis['service_count']; ?> ],
        ['CIS - Sub Contractor','<?php echo base_url(); ?>Firm_dashboard/cis_sub', <?php echo $de_cissub['service_count']; ?> ],
        ['P11D','<?php echo base_url(); ?>Firm_dashboard/p11d', <?php echo $de_p11d['service_count']; ?> ],
        ['Management Accounts','<?php echo base_url(); ?>Firm_dashboard/management', <?php echo $de_management['service_count']; ?> ],
        ['Bookkeeping','<?php echo base_url(); ?>Firm_dashboard/bookkeeping', <?php echo $de_bookkeep['service_count']; ?> ],
        ['Investigation Insurance','<?php echo base_url(); ?>Firm_dashboard/investication', <?php echo $de_investgate['service_count']; ?> ],
        ['Registered Address','<?php echo base_url(); ?>Firm_dashboard/register', <?php echo $de_registered['service_count']; ?> ],
        ['Tax Advice','<?php echo base_url(); ?>Firm_dashboard/tax', <?php echo $de_taxadvice['service_count']; ?> ],
        ['Tax Investigation','<?php echo base_url(); ?>Firm_dashboard/tax_inves', <?php echo $de_taxinvest['service_count']; ?> ]         
      ]);      
     var view = new google.visualization.DataView(data);
       view.setColumns([0, 2]);
   
     var options = {
         chartArea: {left:0, width: "80%"}, 
         height:250,
    legend: { position: 'bottom', show: true, container: $("#large-legend"), alignment: 'start' },
    hAxis : { textStyle : { fontSize: 8} },
    vAxis : { textStyle : { fontSize: 8} },
      width: 350,
      height: 250
   // pieHole: 0.4,
     };
   
   var chart_div = new google.visualization.PieChart(document.getElementById('piechart_deadline'));
   var chart = new google.visualization.PieChart(document.getElementById('piechart_deadline'));
   google.visualization.events.addListener(chart, 'select', function () {
   chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
   //document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '"></a>';
   var image=chart.getImageURI();
     $(document).ready(function(){  
    // alert(image);
               $.ajax({
                      type: "POST",
                      url: "<?php echo base_url();?>/Report/deadline_chart",         
                      data: {deadline_image: image},
                      success: 
                           function(data){
                  
                           }
                       });
                });
   });
   chart.draw(view, options);
   var selectHandler = function(e) {       
       window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
       }      
       google.visualization.events.addListener(chart, 'select', selectHandler);
       }
   

   google.charts.setOnLoadCallback(drawChart6);
   
   function drawChart6() {  
     var data = google.visualization.arrayToDataTable([
        ['Invoices', 'link','Count'],             
        ['Approved','<?php echo base_url(); ?>invoice?type=1',<?php echo $approved_invoice['invoice_count']; ?> ],
        ['Cancelled','<?php echo base_url(); ?>invoice?type=3', <?php echo $cancel_invoice['invoice_count']; ?> ],
        ['Pending','<?php echo base_url(); ?>invoice?type=2', <?php echo $expired_invoice['invoice_count']; ?> ]               
      ]);      
     var view = new google.visualization.DataView(data);
       view.setColumns([0, 2]);
   
     var options = {
         chartArea: {left:0, width: "80%"}, 
         height:250,
    legend: { position: 'bottom', show: true, container: $("#large-legend"), alignment: 'start' },
    hAxis : { textStyle : { fontSize: 8} },
    vAxis : { textStyle : { fontSize: 8} },
      width: 350,
      height: 250
   // pieHole: 0.4,
     };
   
   var chart_div = new google.visualization.PieChart(document.getElementById('piechart_invoice'));
   var chart = new google.visualization.PieChart(document.getElementById('piechart_invoice'));
   google.visualization.events.addListener(chart, 'select', function () {
   chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
   //document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '"></a>';
   var image=chart.getImageURI();
     $(document).ready(function(){  
    // alert(image);
               $.ajax({
                      type: "POST",
                      url: "<?php echo base_url();?>Filter/invoice_chart",         
                      data: {invoice_image: image},
                      success: 
                           function(data){
                  
                           }
                       });
                });
   });
   chart.draw(view, options);
   var selectHandler = function(e) {       
       window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
       }      
       google.visualization.events.addListener(chart, 'select', selectHandler);
       }

     google.charts.setOnLoadCallback(drawChart7);
          
     function drawChart7()
     {
         var data = google.visualization.arrayToDataTable([
         ['Task','link','Count'],
         ['Low', '<?php echo base_url(); ?>Firm_dashboard/low',<?php echo $low_tasks['task_count']; ?>],
         ['High',  '<?php echo base_url(); ?>Firm_dashboard/high',<?php echo $high_tasks['task_count']; ?>],
         ['Medium','<?php echo base_url(); ?>Firm_dashboard/medium', <?php echo $medium_tasks['task_count']; ?>],
         ['Super Urgent','<?php echo base_url(); ?>Firm_dashboard/super_urgent',<?php echo $super_urgent_tasks['task_count']; ?>]   
         ]);  

         var view = new google.visualization.DataView(data);
          view.setColumns([0, 2]);


         var options = {
         chartArea: {left: 0, top: 0, width: "100%", height: "80%"},     
         legend: { position: 'bottom' },
         hAxis : { textStyle : { fontSize: 8} },
         vAxis : { textStyle : { fontSize: 8} },
         // legend: { position: 'bottom' },
         // hAxis : { textStyle : { fontSize: 8} },
         // vAxis : { textStyle : { fontSize: 8} },
         // pieHole: 0.4,
         };  
         var chart = new google.visualization.PieChart(document.getElementById('tabs_priority'));   
         chart.draw(view, options);   
         var selectHandler = function(e) {
         //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
         window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
         }
         // Add our selection handler.
         google.visualization.events.addListener(chart, 'ready', selectHandler);
     }
     
      //Tab Details Section 
      
      
      google.charts.setOnLoadCallback(drawChart8);
      
      function drawChart8() {
        var data = google.visualization.arrayToDataTable([
          ['Legal Form','link','Count'],
          ['Private Limited company', '<?php echo base_url();?>Firm_dashboard/private_function', <?php echo $Private['limit_count']; ?>],
          ['Public Limited company',  '<?php echo base_url();?>Firm_dashboard/public_function',   <?php echo $Public['limit_count']; ?>],
          ['Limited Liability Partnership','<?php echo base_url();?>Firm_dashboard/limited_function', <?php echo $limited['limit_count']; ?>],
          ['Partnership', '<?php echo base_url();?>Firm_dashboard/partnership', <?php echo $Partnership['limit_count']; ?>],
          ['Self Assessment','<?php echo base_url();?>Firm_dashboard/self_assesment',   <?php echo $self['limit_count']; ?>],
          ['Trust','<?php echo base_url();?>Firm_dashboard/trust',<?php echo $Trust['limit_count']; ?>],
          ['Charity','<?php echo base_url();?>Firm_dashboard/charity',<?php echo $Charity['limit_count']; ?>],
          ['Other','<?php echo base_url();?>Firm_dashboard/other',<?php echo $Other['limit_count']; ?>]
        ]);  
      
         var view = new google.visualization.DataView(data);
             view.setColumns([0,2]);
       
      
        var options = {       
          chartArea: {left: 0, width: "80%"},
           height: 250,
          legend: { position: 'bottom', show: true, container: $("#large-legend"), alignment: 'start' },
          hAxis : { textStyle : { fontSize: 8} },
          vAxis : { textStyle : { fontSize: 8} },
            width: 350,
           height: 250
         // pieHole: 0.4,
        };  
        var chart_div = new google.visualization.PieChart(document.getElementById('Tabs_client'));

        // new google.visualization.PieChart(document.getElementById('container'))
        var chart = new google.visualization.PieChart(document.getElementById('Tabs_client'));
        google.visualization.events.addListener(chart, 'ready', function () {
        chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
        //document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '"></a>';
        var image=chart.getImageURI();
           $(document).ready(function(){   
                     $.ajax({
                            type: "POST",
                            url: "<?php echo base_url();?>/Reports/pdf_download",         
                            data: {image: image},
                            success: 
                                 function(data){
                        
                                 }
                             });
                      });
      });
        chart.draw(view, options);
      
        var selectHandler = function(e) {
             //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
             window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
             }
             // Add our selection handler.
             google.visualization.events.addListener(chart, 'ready', selectHandler);
      }   

      google.charts.setOnLoadCallback(drawChart9);
      function drawChart9() {
      var data = google.visualization.arrayToDataTable([
       ['user status','link','Count'],
       ['active', '<?php echo base_url();?>Firm_dashboard/active', <?php echo $active_user_count['activeuser_count']; ?>],
       ['inactive',  '<?php echo base_url();?>Firm_dashboard/inactive',   <?php echo $inactive_user_count['inactiveuser_count']; ?>],
       ['frozen','<?php echo base_url();?>Firm_dashboard/frozen', <?php echo $frozen_user_count['frozenuser_count']; ?>],     
      ]);  
      
      var view = new google.visualization.DataView(data);
          view.setColumns([0, 2]);
      
      
      var options = {
      chartArea: {'width': '100%', 'height': '80%'},        
       legend: { position: 'bottom' },
       hAxis : { textStyle : { fontSize: 8} },
       vAxis : { textStyle : { fontSize: 8} },
      };  
      var chart = new google.visualization.PieChart(document.getElementById('Tabs_Users'));   
         chart.draw(view, options);   
         var selectHandler = function(e) {
        //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
          window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
         }
         // Add our selection handler.
         google.visualization.events.addListener(chart, 'ready', selectHandler);
      }

         google.charts.setOnLoadCallback(drawChart10);
         function drawChart10() {
           var data = google.visualization.arrayToDataTable([
             ['Task','link','Count'],
             ['Not started', '<?php echo base_url(); ?>Firm_dashboard/notstarted',<?php echo $notstarted_tasks['task_count']; ?>],
             ['Completed',  '<?php echo base_url(); ?>Firm_dashboard/complete',<?php echo $completed_tasks['task_count']; ?>],
             ['Inprogress','<?php echo base_url(); ?>Firm_dashboard/inprogress',<?php echo $inprogress_tasks['task_count']; ?>],
             ['Awaiting Feedback','<?php echo base_url(); ?>Firm_dashboard/awaited',<?php echo $awaiting_tasks['task_count']; ?>],
             ['Archived',  '<?php echo base_url(); ?>Firm_dashboard/archive',<?php echo $archive_tasks['task_count']; ?>]   
           ]);  
        
            var view = new google.visualization.DataView(data);
                view.setColumns([0, 2]);
          
        
           var options = {
              chartArea: {left: 0, height: "80%"},     
             legend: { position: 'bottom' },
          hAxis : { textStyle : { fontSize: 8} },
           vAxis : { textStyle : { fontSize: 8} },
             // legend: { position: 'bottom' },
             // hAxis : { textStyle : { fontSize: 8} },
             // vAxis : { textStyle : { fontSize: 8} },
            // pieHole: 0.4,
           };  
            var chart = new google.visualization.PieChart(document.getElementById('task_tab'));   
              chart.draw(view, options);   
              var selectHandler = function(e) {
             //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
               window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
              }
              // Add our selection handler.
              google.visualization.events.addListener(chart, 'ready', selectHandler);
           }

      google.charts.setOnLoadCallback(drawChart11);
      
      function drawChart11() {
      var data = google.visualization.arrayToDataTable([
      <?php
         $start_array=array();
         $end_array=array();
         $start_montharray=array();
         $end_montharray=array();
         $j=1;
         foreach($task_list_this_month as $value){ 
               $start_date=explode('-',$value['start_date']);          
               $start=$start_date['2']; 
               $start_month=$start_date['1'];
               $end_date=explode('-',$value['end_date']);
               $end=$end_date['2'];
               $end_month=$end_date['1'];          }
               $month['month']=array(); 
               $month['start_date']=array(); 
               $month['end_date']=array(); 
               $month_other=array(); 
          foreach($task_list_this_month as $value){ 
               $start_date=explode('-',$value['start_date']);          
               $start=$start_date['2']; 
               $start_month=$start_date['1'];
               $end_date=explode('-',$value['end_date']);
               $end=$end_date['2'];
               $end_month=$end_date['1']; ?> 
      <?php  if(date('m')=='01'){ ?>
       ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='01'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end;  } ?>],
         <?php } if(date('m')=='02'){ ?>
      ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='02'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
        <?php } if(date('m')=='03'){ ?>
      ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='03'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
        <?php } if(date('m')=='04'){ ?>
      ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='04'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
        <?php } if(date('m')=='05'){ ?>
      ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='05'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],   
      <?php } ?>
      <?php  if(date('m')=='06'){ ?>
      ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='06'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
      <?php } ?>
        <?php  if(date('m')=='07'){ ?>
        ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='07'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
        <?php } ?>
          <?php  if(date('m')=='08'){ ?>
      ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='08'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
        <?php } if(date('m')=='09'){ ?>
      ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='09'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
        <?php } if(date('m')=='10'){ ?>
      ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='10'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
        <?php } if(date('m')=='11'){ ?>
      ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='11'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
        <?php } if(date('m')=='12'){ ?>
      ['<?php echo $value['subject'].'('.date("M d", strtotime($value['start_date'])).'-'.date("M d", strtotime($value['end_date'])).')'; ?>', <?php if($start_month=='12'){ ?><?php echo $start; ?>,<?php echo $start; ?>,<?php echo $end; ?>, <?php echo $end; } ?>],
        <?php  } ?>
      
      <?php } ?>     
      ], true);
      var options = {
      legend:'none',
      'width':1150,
      'height':500, 
       axisTitlesPosition:'in',
       candlestick: {
                  fallingColor: { strokeWidth: 0, fill: '#3366cc' }, // red
                  risingColor: { strokeWidth: 0, fill: '#3366cc' }   // green
               }
      };
      var chart = new google.visualization.CandlestickChart(document.getElementById('monthly_task'));
      chart.draw(data, options);
      }

      google.charts.setOnLoadCallback(drawChart12);   
      function drawChart12() {   
        var data = google.visualization.arrayToDataTable([
          ['Service','link','Count'],             
           ['Confirmation Statement','<?php echo base_url(); ?>Firm_dashboard/confirm_statement',<?php echo $conf_statement['service_count']; ?> ],
           ['Accounts', '<?php echo base_url(); ?>Firm_dashboard/accounts',<?php echo $accounts['service_count']; ?> ],
           ['Company Tax Return','<?php echo base_url(); ?>Firm_dashboard/company_tax', <?php echo $company_tax_return['service_count']; ?> ],
           ['Personal Tax Return','<?php echo base_url(); ?>Firm_dashboard/personal_tax', <?php echo $personal_tax_return['service_count']; ?> ],
           ['VAT Returns','<?php echo base_url(); ?>Firm_dashboard/vat', <?php echo $vat['service_count']; ?> ],
           ['Payroll','<?php echo base_url(); ?>Firm_dashboard/payroll', <?php echo $payroll['service_count']; ?> ],
           ['WorkPlace Pension - AE','<?php echo base_url(); ?>Firm_dashboard/workplace', <?php echo $workplace['service_count']; ?> ],
           ['CIS - Contractor','<?php echo base_url(); ?>Firm_dashboard/cis', <?php echo $cis['service_count']; ?> ],
           ['CIS - Sub Contractor','<?php echo base_url(); ?>Firm_dashboard/cis_sub', <?php echo $cissub['service_count']; ?> ],
           ['P11D','<?php echo base_url(); ?>Firm_dashboard/p11d', <?php echo $p11d['service_count']; ?> ],
           ['Management Accounts','<?php echo base_url(); ?>Firm_dashboard/management',  <?php echo $management['service_count']; ?> ],
           ['Bookkeeping','<?php echo base_url(); ?>Firm_dashboard/bookkeeping', <?php echo $bookkeep['service_count']; ?> ],
           ['Investigation Insurance','<?php echo base_url(); ?>Firm_dashboard/investication', <?php echo $investgate['service_count']; ?> ],
           ['Registered Address','<?php echo base_url(); ?>Firm_dashboard/register', <?php echo $registered['service_count']; ?> ],
           ['Tax Advice','<?php echo base_url(); ?>Firm_dashboard/tax',  <?php echo $taxadvice['service_count']; ?> ],
           ['Tax Investigation','<?php echo base_url(); ?>Firm_dashboard/tax_inves',  <?php echo $taxinvest['service_count']; ?> ],          
         ]);  
      
          var view = new google.visualization.DataView(data);
          view.setColumns([0, 2]);    
      
        var options = {
             chartArea: {left: 0, top: 0, width: "100%", height: "100%"}, 
             legend: { position: 'bottom' },
          hAxis : { textStyle : { fontSize: 8} },
           vAxis : { textStyle : { fontSize: 8} },
      // legend: { position: 'bottom' },
      //  hAxis : { textStyle : { fontSize: 8} },
      //  vAxis : { textStyle : { fontSize: 8} },
      //  pieHole: 0.4,
        };
      
       var chart = new google.visualization.PieChart(document.getElementById('service_tab'));   
            chart.draw(view, options);   
            var selectHandler = function(e) {
           //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
             window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
            }
            // Add our selection handler.
            google.visualization.events.addListener(chart, 'ready', selectHandler);
         }

      google.charts.setOnLoadCallback(drawChart13);   
      function drawChart13() {   
        var data = google.visualization.arrayToDataTable([
          ['Service', 'link','Count'],             
           ['Confirmation Statement','<?php echo base_url(); ?>Firm_dashboard/confirm_statement',<?php echo $de_conf_statement['service_count']; ?> ],
           ['Accounts','<?php echo base_url(); ?>Firm_dashboard/accounts', <?php echo $de_accounts['service_count']; ?> ],
           ['Company Tax Return','<?php echo base_url(); ?>Firm_dashboard/company_tax', <?php echo $de_company_tax_return['service_count']; ?> ],
           ['Personal Tax Return','<?php echo base_url(); ?>Firm_dashboard/personal_tax', <?php echo $de_personal_tax_return['service_count']; ?> ],
           ['VAT Returns','<?php echo base_url(); ?>Firm_dashboard/vat', <?php echo $de_vat['service_count']; ?> ],
           ['Payroll','<?php echo base_url(); ?>Firm_dashboard/payroll', <?php echo $de_payroll['service_count']; ?> ],
           ['WorkPlace Pension - AE','<?php echo base_url(); ?>Firm_dashboard/workplace', <?php echo $de_workplace['service_count']; ?> ],
           ['CIS - Contractor','<?php echo base_url(); ?>Firm_dashboard/cis', <?php echo $de_cis['service_count']; ?> ],
           ['CIS - Sub Contractor','<?php echo base_url(); ?>Firm_dashboard/cis_sub', <?php echo $de_cissub['service_count']; ?> ],
           ['P11D','<?php echo base_url(); ?>Firm_dashboard/p11d', <?php echo $de_p11d['service_count']; ?> ],
           ['Management Accounts','<?php echo base_url(); ?>Firm_dashboard/management', <?php echo $de_management['service_count']; ?> ],
           ['Bookkeeping','<?php echo base_url(); ?>Firm_dashboard/bookkeeping', <?php echo $de_bookkeep['service_count']; ?> ],
           ['Investigation Insurance','<?php echo base_url(); ?>Firm_dashboard/investication', <?php echo $de_investgate['service_count']; ?> ],
           ['Registered Address','<?php echo base_url(); ?>Firm_dashboard/register', <?php echo $de_registered['service_count']; ?> ],
           ['Tax Advice','<?php echo base_url(); ?>Firm_dashboard/tax', <?php echo $de_taxadvice['service_count']; ?> ],
           ['Tax Investigation','<?php echo base_url(); ?>Firm_dashboard/tax_inves', <?php echo $de_taxinvest['service_count']; ?> ]         
         ]);      
        var view = new google.visualization.DataView(data);
          view.setColumns([0, 2]);
      
        var options = {
            chartArea: {left: 0, top: 0, width: "100%", height: "100%"
   }, 
            legend: { position: 'bottom' },
          hAxis : { textStyle : { fontSize: 8} },
           vAxis : { textStyle : { fontSize: 8} },
            // width:1200,
            // height:500,
       // legend: { position: 'bottom' },
       // hAxis : { textStyle : { fontSize: 8} },
       // vAxis : { textStyle : { fontSize: 8} },
      // pieHole: 0.4,
        };
      var chart = new google.visualization.PieChart(document.getElementById('Tabs_deadline'));   
            chart.draw(view, options);   
            var selectHandler = function(e) {
           //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
             window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
            }
            // Add our selection handler.
            google.visualization.events.addListener(chart, 'ready', selectHandler);
         }

      google.charts.setOnLoadCallback(drawChart14);
      
      function drawChart14() {  
        var data = google.visualization.arrayToDataTable([
          ['Invoice', 'link','Count'],             
           ['Approved','<?php echo base_url(); ?>invoice?type=1',<?php echo $approved_invoice_month['invoice_count']; ?> ],
           ['Cancelled','<?php echo base_url(); ?>invoice?type=3', <?php echo $cancel_invoice_month['invoice_count']; ?> ],
           ['Pending','<?php echo base_url(); ?>invoice?type=2', <?php echo $pending_invoice_month['invoice_count']; ?> ]               
         ]);      
        var view = new google.visualization.DataView(data);
          view.setColumns([0, 2]);

              
        var options = {
            chartArea: {left: 0, top: 0, width: "100%", height: "100%" }, 
            // width:1200,
            // height:500,
       // legend: { position: 'bottom' },
       // hAxis : { textStyle : { fontSize: 8} },
       // vAxis : { textStyle : { fontSize: 8} },
      // pieHole: 0.4,
        };
      
      var chart = new google.visualization.PieChart(document.getElementById('Tabs_invoice'));   
            chart.draw(view, options);   
            var selectHandler = function(e) {
           //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
             window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
            }
            // Add our selection handler.
            google.visualization.events.addListener(chart, 'ready', selectHandler);
         }    
   
</script>