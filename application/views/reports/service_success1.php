<div id="piechart_service" style="width:100%;">

</div>

<?php 

$total_service_count = "";

foreach ($services_count as $key => $value) 
{
   $total_service_count += $value['count']['service_count']; 
}

?>  

<div class="prodiv12">
    <?php
    
    foreach ($services_count as $key => $value) 
    { 
       if($value['count']['service_count'] != '0')
       {
    ?>
    <div class="pro-color">
       <span class="procount"><?php echo $value['count']['service_count']; ?> </span>
       <div class="fordis">
          <div class="progress progress-xs">
             <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?php echo $value['count']['service_count'].'%'; ?>;" aria-valuenow="<?php echo $value['count']['service_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $total_service_count; ?>"></div>
          </div>
       </div>
    </div>
  <?php } } ?>
</div>

<script type="text/javascript">

     google.charts.load('current', {'packages':['corechart']});
     google.charts.setOnLoadCallback(drawChart2);

     <?php 

     $services_arr = "";

     foreach($services_count as $key => $value) 
     { 
        $services_arr .= '["'.$key.'","'.base_url().'Firm_dashboard/service/'.$value['id'].'",'.$value['count']['service_count'].'],';
     }  

     $services_arr = substr_replace($services_arr,'', -1);
     
     ?>
  
     function drawChart2() 
     {
  
       var data = google.visualization.arrayToDataTable([
         ['Service','link','Count'],<?php echo $services_arr; ?>          
        ]);  

         var view = new google.visualization.DataView(data);
         view.setColumns([0, 2]);    
  
       var options = {
              chartArea: {left: 0, width: "80%"},
        height: 250,
       legend: { position: 'bottom', show: true, container: $("#large-legend"), alignment: 'start'  },
      hAxis : { textStyle : { fontSize: 8} },
      vAxis : { textStyle : { fontSize: 8} },
       width: 350,
      height: 250
    //  pieHole: 0.4,
       };


         var chart = new google.visualization.PieChart(document.getElementById('piechart_service'));
   
         chart.draw(view, options);
   
         var selectHandler = function(e) {
        //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
          window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
         }
         // Add our selection handler.
         google.visualization.events.addListener(chart, 'select', selectHandler);
      }
  
  
  </script>