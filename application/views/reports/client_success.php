<div id="chart_Donut11" style="width: 100%;"></div>
 <?php $client = $this->Report_model->client_list(); ?>
 <div class="prodiv12">
   <?php
   if($Private['limit_count']!='0'){ ?>
   <div class="pro-color">
     <span class="procount"> <?php echo $Private['limit_count']; ?></span>
   <div class="fordis">
   <div class="progress progress-xs">
     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Private['limit_count'].'%' ?>;" aria-valuenow="<?php echo $Private['limit_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo count($client); ?>"></div>
   </div>
   </div>
   </div>
   <?php } ?>
   <?php
   if($Public['limit_count']!='0'){ ?>
   <div class="pro-color">
     <span class="procount"> <?php echo $Public['limit_count']; ?></span>
   <div class="fordis">
   <div class="progress progress-xs">
     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Public['limit_count'].'%' ?>;" aria-valuenow="<?php echo $Public['limit_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo count($client); ?>"></div>
   </div>
   </div>
   </div>
   <?php } ?>


   <?php
   if($limited['limit_count']!='0'){ ?>
   <div class="pro-color">
     <span class="procount"> <?php echo $limited['limit_count']; ?></span>
   <div class="fordis">
   <div class="progress progress-xs">
     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $limited['limit_count'].'%' ?>;" aria-valuenow="<?php echo $limited['limit_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo count($client); ?>"></div>
   </div>
   </div>
   </div>
   <?php } ?>

   <?php
   if($Partnership['limit_count']!='0'){ ?>
   <div class="pro-color">
     <span class="procount"> <?php echo $Partnership['limit_count']; ?></span>
   <div class="fordis">
   <div class="progress progress-xs">
     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Partnership['limit_count'].'%' ?>;" aria-valuenow="<?php echo $Partnership['limit_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo count($client); ?>"></div>
   </div>
   </div>
   </div>
   <?php } ?>

   <?php
   if($self['limit_count']!='0'){ ?>
   <div class="pro-color">
     <span class="procount"> <?php echo $self['limit_count']; ?></span>
   <div class="fordis">
   <div class="progress progress-xs">
     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $self['limit_count'].'%' ?>;" aria-valuenow="<?php echo $self['limit_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo count($client); ?>"></div>
   </div>
   </div>
   </div>
   <?php } ?>
   <?php
   if($Trust['limit_count']!='0'){ ?>
   <div class="pro-color">
     <span class="procount"> <?php echo $Trust['limit_count']; ?></span>
   <div class="fordis">
   <div class="progress progress-xs">
     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Trust['limit_count'].'%' ?>;" aria-valuenow="<?php echo $Trust['limit_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo count($client); ?>"></div>
   </div>
   </div>
   </div>
   <?php } ?>

   <?php
   if($Charity['limit_count']!='0'){ ?>
   <div class="pro-color">
     <span class="procount"> <?php echo $Charity['limit_count']; ?></span>
   <div class="fordis">
   <div class="progress progress-xs">
     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Charity['limit_count'].'%' ?>;" aria-valuenow="<?php echo $Charity['limit_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo count($client); ?>"></div>
   </div>
   </div>
   </div>
   <?php } ?>

   <?php
   if($Other['limit_count']!='0'){ ?>
   <div class="pro-color">
     <span class="procount"> <?php echo $Other['limit_count']; ?></span>
   <div class="fordis">
   <div class="progress progress-xs">
     <div class="progress-bar progress-bar-danger" role="progressbar" style="width:  <?php echo $Other['limit_count'].'%' ?>;" aria-valuenow="<?php echo $Other['limit_count']; ?>" aria-valuemin="0" aria-valuemax="<?php echo count($client); ?>"></div>
   </div>
   </div>
   </div>
   <?php } ?>
</div>
 <script type="text/javascript" src="http://remindoo.org/CRMTool/bower_components/jquery/js/jquery.min.js"></script>
 <script type="text/javascript">
 //image_ajax();
 </script>
<!-- <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/custom-google-chart.js"></script>

<!-- <script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1.1','packages':['bar']}]}"></script> -->
<script type="text/javascript">
  google.charts.load("current", {packages:["corechart"]});
  google.charts.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['Legal Form','link','Count'],
     
           ['Public Limited company', '<?php echo base_url(); ?>Firm_dashboard/public_function',<?php echo $Public['limit_count']; ?>],         
     
           ['Limited Liability Partnership','<?php echo base_url(); ?>Firm_dashboard/limited_function',  <?php echo $limited['limit_count']; ?>],      
   
           ['Private Limited company','<?php echo base_url(); ?>Firm_dashboard/private_function',<?php echo $Private['limit_count']; ?>],       
           ['Partnership','<?php echo base_url(); ?>Firm_dashboard/partnership', <?php echo $Partnership['limit_count']; ?>],  
                ['Self Assessment','<?php echo base_url(); ?>Firm_dashboard/self_assesment',    <?php echo $self['limit_count']; ?>],
     
           ['Trust','<?php echo base_url(); ?>Firm_dashboard/trust',<?php echo $Trust['limit_count']; ?>],
   
          ['Charity','<?php echo base_url(); ?>Firm_dashboard/charity',<?php echo $Charity['limit_count']; ?>],
        
   
          ['Other','<?php echo base_url(); ?>Firm_dashboard/other',<?php echo $Other['limit_count']; ?>],
        
      
    ]);
  
     var view = new google.visualization.DataView(data);
     view.setColumns([0, 2]);    

    var options = {
       //title: 'Client Lists',
          chartArea: {left: 0, width: "80%"},
        height: 250,
       legend: { position: 'bottom', show: true, container: $("#large-legend"), alignment: 'start'  },
      hAxis : { textStyle : { fontSize: 8} },
      vAxis : { textStyle : { fontSize: 8} },
       width: 350,
      height: 250
     // pieHole: 0.4,
    };

     var chart = new google.visualization.PieChart(document.getElementById('chart_Donut11'));
   
         chart.draw(view, options);
   
         var selectHandler = function(e) {
        //  window.location = data.getValue(chart.getSelection()[0]['row'], 1,'_blank' );
          window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
         }
         // Add our selection handler.
         google.visualization.events.addListener(chart, 'select', selectHandler);
      }
  
   
  function image_ajax(image){
  //alert('ok12');
  $.ajax({
       type: "POST",
       url: "<?php echo base_url();?>Reports/pdf_download_sess",         
       data: {image_sess: image},
       success: 
         function(data){
         }
     });
}
</script>