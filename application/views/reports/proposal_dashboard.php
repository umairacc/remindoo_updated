<?php $this->load->view('includes/header');?>
<style>
   /* bootstrap hack: fix content width inside hidden tabs */
.tab-content > .tab-pane,
.pill-content > .pill-pane {
display: block;     /* undo display:none          */
height: 0;          /* height:0 is also invisible */ 
overflow-y: hidden; /* no-overflow                */
}
.tab-content > .active,
.pill-content > .active {
height: auto;       /* let the content decide it  */
} /* bootstrap hack end */
.box-container {
   height: 200px;
}
 tfoot {
    display: table-header-group;
}
#container1.box-item{
   width: 100%; 
   /*font-size:10px;*/
   z-index: 1000
}


.dropdown-content {
    display: none;
    position: absolute;
    background-color: #fff;
    min-width: 86px;
    overflow: auto;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    left: -92px;
    width: 150px;
}

.box-item {
   /*width: 100%; */
   font-size:10px;
   z-index: 1000
}

/*.dt-buttons{
      display: none;
   }*/
.run-section{
   position: relative;
}
.running{
   position: absolute;
   display:   block;
   height: 100%;
   top:0;
}

.wrong{
  border: 1px solid red;
}

div#container112 {
    height: 358px;
    position: relative;
    display: block;
    margin-top: -235px;
    z-index: 99;
}
</style>
 
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/mintScrollbar.css">
<div class="pcoded-content card-removes">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card">
                        <!-- admin start-->
                        <div class="client_section user-dashboard-section1 floating_set">
                           <!-- all-clients -->
                           <div class="all_user-section floating_set">
                              <div class="deadline-crm1 floating_set f4reports rdashboard">
                                 <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                                    <li class="nav-item">
                                       <a class="nav-link active" data-toggle="tab" data-target="#reports-tab" href="">Reports</a>
                                       <div class="slide"></div>
                                    </li>
                                 </ul>
                                 <div class="run-btn f-right">
                                 
                                 <button data-toggle="modal" data-target="#myModal" class="btn btn-danger fr-sch" >Schedule</button>
                                    <!--    <button class="btn btn-success" id="run_details">Run</button>
                                       <butto class="btn btn-danger">&times;</butto> -->
                                 </div>
                              </div>
                              <div class="for-reportdash">
                                 <div class="left-one">
                                    <div class="pcoded-search">
                                       <span class="searchbar-toggle"> </span>
                                       <div class="pcoded-search-box ">
                                          <input type="text" placeholder="Search" class="search-criteria" id="search-criteria">
                                          <span class="search-icon"><i class="ti-search" aria-hidden="true"></i></span>
                                       </div>
                                          <div id="container1" class="panel-body box-container">
                                         <div itemid="sent" class="btn btn-default box-item box_section">Sent<span class="remove" data-role="remove">x</span></div>

                                          <div itemid="opened" class="btn btn-default box-item box_section">Viewed<span class="remove" data-role="remove">x</span></div>

                                          <div itemid="accepted" class="btn btn-default box-item box_section">Accepted<span class="remove" data-role="remove">x</span></div>

                                          <div itemid="declined" class="btn btn-default box-item box_section">Declined<span class="remove" data-role="remove">x</span></div>


                                          <div itemid="in discussion" class="btn btn-default box-item box_section">InDiscussion<span class="remove" data-role="remove">x</span></div>

                                           <div itemid="draft" class="btn btn-default box-item box_section">Draft<span class="remove" data-role="remove">x</span></div>
                                          
                                          </div>


                                          <div id="container112" class=""></div>
                                    </div>                                           
                                 </div>
                                 <div class="right-one">
                             <!--     <div class="run-section"> -->
                                   <div class="filter-data">
                                          <div class="filter-head"><h4>DROP FILTERS HERE
                                          </h4><button class="btn btn-danger f-right" id="clear_container">clear</button>
                                          <div id="container2" class="panel-body box-container">
                                         </div>
                                         <div id="container212" class="">
                                             
                                          </div>
                                          </div>                                   
                                         
                                    </div>
                                   <!--   <div class="running"> </div> -->
                                 <!--    </div> -->
                                   
                                   

                                    <div id="datatable_results" >

                              <div class="sample_section">
<div class="export_option">
 <div class="btn-group e-dash sve-action1 frpropos">

  <button type="button" class="btn btn-default dropdown-toggle emp-dash" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select
 
  </button>
  <ul class="dropdown-menu" >
    <li id="buttons"></li>
  </ul>
</div>
</div>
  </div>
  </div>
  <table id="example" class="display nowrap" style="width:100%">
  <thead>
  <tr>
  <th>Proposal No<div class="sortMask"></div></th>
  <th>Proposal Name<div class="sortMask"></div></th>
  <th>Company Name<div class="sortMask"></div></th>
  <th>Status<div class="sortMask"></div></th>
  <th>Created date<div class="sortMask"></div></th>                                      
  </tr>
  </thead>
    <tfoot>
<tr class="table-header"> 
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
</tr>
</tfoot>
  <tbody>
<?php
if(empty($records)){}else{
foreach($records as $record){ ?>
  <tr>
  <td><?php echo $record['proposal_no']; ?></td>
  <td><?php echo $record['proposal_name']; ?></td>
  <td><?php if($record['company_name']=='0'){
    echo "-";
  }else{
    echo $record['company_name']; }

   ?></td>
  <td><?php echo $record['status']; ?></td>
  <td><?php echo date('Y-m-d',strtotime($record['created_at'])); ?></td>  
  </tr>
<?php }} ?>

  </tbody>

  </table>   </div>
                             </div>
                          </div>
                          </div>
                       </div>
                    </div>
                 </div>
              </div>
           </div>
        </div>
     </div>
  </div>
   
<?php $this->load->view('reports/schedule_report');?>
<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/custom-google-chart.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.mintScrollbar.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.mousewheel.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
<script src="<?php echo base_url();?>assets/js/mock.js"></script>
<script type="text/javascript">

  $('.dropdown-sin-4').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
   });

    $('.dropdown-sin-6').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
   });

    $('.dropdown-sin-2').dropdown({
   
     input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
   });
     $('.dropdown-sin-3').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
   });

$(document).ready(function() {

  var example1;
    // $('#example').DataTable( {
    //     dom: 'Bfrtip',
    //       buttons: [{
    //                     extend: 'pdf',
    //                        title: 'Proposal List',
    //                        filename: 'Proposal Details'
    //                     }, {
    //                     extend: 'excel',
    //                      extension: '.xls',
    //                        title: 'Proposal List',
    //                        filename: 'Proposal Details'
    //                     }, {
    //                     extend: 'csv',
    //                        filename: 'Proposal Details'
    //                     },{
    //                     extend: 'print',
    //                        title: 'Proposal Details',
    //                     }]    
    //     // buttons: [
    //     //     'copy', 'csv', 'excel', 'pdf', 'print'
    //     // ]
    // } );
var check=0;
    var check1=0;
    var numCols = $('#example thead th').length;
        example1 = $('#example').DataTable({
        // dom: 'Bfrtip',
        // buttons: [{
        // extend: 'pdf',
        //    title: 'Proposal List',
        //    filename: 'Proposal Details'
        // }, {
        // extend: 'excel',
        //  extension: '.xls',
        //    title: 'Proposal List',
        //    filename: 'Proposal Details'
        // }, {
        // extend: 'csv',
        //    filename: 'Proposal Details'
        // },{
        // extend: 'print',
        //    title: 'Proposal Details',
        // }]  ,
       initComplete: function () { 
                  var q=0;
                     $('#example tfoot th').find('.filter_check').each( function(){
                     // alert('ok');
                       $(this).attr('id',"all_"+q);
                       q++;
                     });
                  for(i=0;i<numCols;i++){                 
                        var select = $("#all_"+i); 
                      
                      this.api().columns([i]).every( function () {
                        var column = this;
                        column.data().unique().sort().each( function ( d, j ) {    
                          console.log(d);         
                          select.append( '<option value="'+d+'">'+d+'</option>' )
                        });
                      });                   
           
                     $("#all_"+i).formSelect();  
                  }
        }
    });
      for(j=0;j<numCols;j++){  
          $('#all_'+j).on('change', function(){ 
            var result=$(this).attr('id').split('_');      
             var c=result[1];
              var search = [];              
              $.each($('#all_'+c+ ' option:selected'), function(){                
              search.push($(this).val());
              });    
            //  alert(search) ;
              search = search.join('|');  

              example1.column(c).search(search, true, false).draw();  
          });
       }

  example1 = $('#example').DataTable();
  var buttons = new $.fn.dataTable.Buttons(example1, {

     buttons: [{
       extend: 'pdf',
       title: 'Proposal List',
       filename: 'Proposal List',
           
     }, {
       extend: 'excel',
       title: 'Proposal List',
       filename: 'Proposal List',
       extension: '.xls',
             
     }, {
       extend: 'csv',
       filename: 'Proposal List',
       
     },{
       extend: 'print',
       title: 'Proposal List',
       
     }]
}).container().appendTo($('#buttons')); 


} );


$("#exported_result").change(function(){
var value=$(this).val();
if(value=='pdf'){
$(".buttons-pdf").trigger('click');
}else if(value="excel"){
$(".buttons-excel").trigger('click');
}else if(value="csv"){
$(".buttons-csv ").trigger('click');
}
});


$("#print_report").click(function(){
$(".buttons-print").trigger('click');
});
</script>

<script type="text/javascript">
   $( document ).ready(function() {
$(".prodiv12").mintScrollbar({
onChange: null,
axis: "auto",
wheelSpeed: 120,
disableOnMobile: true
});

   $('.tags').tagsinput({
      allowDuplicates: true
   });
   });


   $(document).ready(function() {

  $('.box-item').draggable({
    cursor: 'move',
    helper: "clone"
  });

  $("#container1").droppable({
    drop: function(event, ui) {
      var itemid = $(event.originalEvent.toElement).attr("itemid");
      $('.box-item').each(function() {
        if ($(this).attr("itemid") === itemid) {
          $(this).appendTo("#container1");
        }
      });
    }
  });

$(".remove").click(function(){  
    $(this).parent('.box-item').addClass('replace');
      $('.replace').each(function() {    
          $(this).appendTo("#container1");      
      });
});

$("#clear_container").click(function(){

   $(".insearch_section .remove").trigger('click');

});




  $("#container2").droppable({

    drop: function(event, ui) {
      var itemid = $(event.originalEvent.toElement).attr("itemid");
      $('.box-item').each(function() {
        if ($(this).attr("itemid") === itemid) {
         $(this).addClass('insearch_section');
          $(this).appendTo("#container2");      
        }
      });
    }
  });


//   $("#run_details").click(function(){
//    $("#container2 .box-item").each(function() {
// console.log($(this).attr("itemid"));
//    });
//   });


 /* $("#run_details").click(function(){

   var id=1;
 
         $.ajax({
            url: '<?php echo base_url(); ?>Reports/chart',
            type : 'POST',
            data : {'id':id},                    
            beforeSend: function() {
            $(".LoadingImage").show();
            },
            success: function(data) {
               $(".LoadingImage").hide();   
               $("#datatable_results").html('');
               $("#datatable_results").append(data);
               $("#datatable_results").show();

               $(document).ready(function() {
                  $('#example').DataTable( {
                     dom: 'Bfrtip',
                     buttons: [{
                        extend: 'pdf',
                           title: 'Proposal List',
                           filename: 'Proposal Details'
                        }, {
                        extend: 'excel',
                           title: 'Client List',
                           filename: 'Client Details'
                        }, {
                        extend: 'csv',
                           filename: 'Client Details'
                        },{
                        extend: 'print',
                           title: 'Client Details',
                        }]           
                  });
               });

               $("#exported_result").change(function(){
               var value=$(this).val();        
                  if(value=='pdf'){
                      $(".buttons-pdf").trigger('click');
                  }else if(value=="excel"){
                      $(".buttons-excel").trigger('click');
                  }else if(value=="csv"){             
                     $(".buttons-csv").trigger('click');
                  }
               });

               $("#print_report").click(function(){
                   $(".buttons-print").trigger('click');
               });                                        
            }
         }); 
       });*/
 



});




$('#search-criteria').keyup(function(){
    $('.box_section').hide();
    var txt = $('#search-criteria').val();
    $('.box_section').each(function(){
       if($(this).text().toUpperCase().indexOf(txt.toUpperCase()) != -1){
           $(this).show();
       }
    });
});


function controll(){
 //console.log('eeeee');
      $('.box-item').each(function() {
       // if ($(this).attr("itemid") === itemid) {
         $(this).addClass('insearch_section');
          $(this).clone().appendTo("#container2");      
       // }
      });


      //  $('.box-item').each(function() {
      // //  if ($(this).attr("itemid") === itemid) {
      //     $(this).appendTo("#container1");
      // //  }
      // });
   
}


  $(document).ready(function () { 

   $(".filter-data").find('*').prop('disabled', true);

      controll();

$("#container2").find('*').prop('disabled', true);

//$(".remove").css('display','none');
  });


</script>


<script type="text/javascript">
   $(document).ready(function () {     
     $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var ev = document.createEvent('Event');
    ev.initEvent('resize', true, true);
    window.dispatchEvent(ev);
  }); 
   });
</script>