<?php $this->load->view('includes/header');?>

<style>
   /* bootstrap hack: fix content width inside hidden tabs */
.tab-content > .tab-pane,
.pill-content > .pill-pane {
display: block;     /* undo display:none          */
height: 0;          /* height:0 is also invisible */ 
overflow-y: hidden; /* no-overflow                */
}
.tab-content > .active,
.pill-content > .active {
height: auto;       /* let the content decide it  */
} /* bootstrap hack end */
/*.box-container {
   height: 200px;
}*/

#container1.box-item{
   width: 100%; 
   /*font-size:10px;*/
   z-index: 1000
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #fff;
    min-width: 86px;
    overflow: auto;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    left: -92px;
    width: 150px;
}

.box-item {
   /*width: 100%; */
   font-size:10px;
   z-index: 1000
}

 tfoot {
    display: table-header-group;
}

.dt-buttons{
      display: none;
   }

.filter{
      color:#F508C6;
   }

   .filtering{
      color:#F508C6;
   }
</style>
<div class="modal fade" id="cus_msg" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" style="padding: 0px -10px 0px 0px;">&times;</button>
      <div class="custom_msg">             
             <div class="pop-realted1">
             <div class="position-alert1" style="text-align: center;"></div>
             </div>
      </div>      
   </div>     
</div>
</div>
</div>
<button id="targetcus_msg" data-toggle="modal" data-target="#cus_msg" style="display: none;"></button>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/mintScrollbar.css">
<div class="pcoded-content card-removes">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card">
                        <!-- admin start-->
                        <div class="client_section user-dashboard-section1 floating_set testreports">
                           <!-- all-clients -->
                           <div class="all_user-section floating_set">
                              <div class="deadline-crm1 floating_set f4reports rdashboard">
                                 <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                                    <li class="nav-item">
                                       <a class="nav-link active" data-toggle="tab" data-target="#reports-tab" href="">Reports</a>
                                       <div class="slide"></div>
                                    </li>
                                 </ul>
                              <div class="f-right client-customize1">
                              <div class="toolbar-table f-right report-redesign1">
                                    <div class="filter-task1">
                                       <ul>
                                         <!--  <li><img src="http://remindoo.org/CRMTool/assets/images/by-date1.png" alt="data"><input type="text" id="datewise_filter" name="de" placeholder="By Date" class="date-picker" value=""></li> -->
                                          <li class="fr-wid">
                                             <img src="http://remindoo.org/CRMTool/assets/images/by-date2.png" alt="data">                                           
                                            <select id="clientwise_filter" name="stati">
                                                <option value="">By Client</option>
                                                <?php foreach($client as $que){ ?>
                                                <option value="<?php echo $que['id']; ?>"><?php echo $que['crm_company_name']; ?></option>
                                                <?php   } ?>
                                             </select>
                                          </li>
                                          <!-- <li class="fr-wid">
                                             <img src="http://remindoo.org/CRMTool/assets/images/by-date2.png" alt="data">
                                             <select id="userwise_filter" name="stati">
                                                <option value="0" hidden="">By User</option>
                                                <?php if(count($staff_form)){ ?>
                                                <option disabled>Staff</option>
                                                <?php foreach($staff_form as $s_key => $s_val){ ?>
                                                <option value="<?php echo $s_val['id'];?>" ><?php echo $s_val['crm_name'];?></option>
                                                <?php }  } ?>
                                            
                                             </select>
                                          </li> -->

                                          <!--  <li class="fr-wid">
                                             <img src="http://remindoo.org/CRMTool/assets/images/by-date2.png" alt="data">
                                             <select id="teamwise_filter" name="stati">
                                                <option value="0" hidden="">By Team</option>
                                                <?php if(count($team)){ ?>
                                                <option disabled>Team</option>
                                                <?php foreach($team as $t_key => $te){ ?>
                                                <option value="<?php echo $te['id'];?>" ><?php echo $te['team'];?></option>
                                                <?php }  } ?>
                                            
                                             </select>
                                          </li> -->
                                          <?php 

                                                $service_list = $this->Common_mdl->getServicelist(); 
                                          ?> 
                                          <li class="fr-wid">
                                            <img src="<?php echo base_url(); ?>assets/images/by-date2.png" alt="data">
                                          <select class="form-control" name="related_to_services" id="servicewise_filter" placeholder="Select">
                                          <option selected="" value="">None</option>
                                          <?php foreach($service_list as $key => $value) { ?>
                                          <option value="<?php echo $value['id']; ?>"><?php echo $value['service_name']; ?></option>
                                          <?php } ?>
                                          </select>
                                         </li>
                                       </ul>
                                       
                                    </div>
                                    <div class="year-status">

                                        <ul class="status-list">
                                             <li><a href="javascript:;" class="all fill_details filtering" id="Performance"> Performance </a></li>
                                             <li><a href="javascript:;" class="week fill_details" id="activitylog"> Activity Log </a></li>
                                       </ul>
                                       <!--  <span class="lt-year">last year</span> -->
                                       <ul class="status-list">
                                          <li><a href="javascript:;" class="all fill" id="all"> All </a></li>
                                          <li><a href="javascript:;" class="week fill" id="week"> week </a></li>
                                          <li><a href="javascript:;" class="three_week fill" id="three_week">3 week</a></li>
                                          <li><a href="javascript:;" class="month fill" id="month"> month </a></li>
                                          <li><a href="javascript:;" class="Three_month fill" id="Three_month"> 3 months </a></li>
                                          <!--  <li>year</li> -->
                                       </ul>
                                    </div>
                                    <div class="time-date">
                                          <div class="label-column2 input-group date">
                                             <span class="lable12">From:</span>
                                             <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                             <input type="text" class="date_picker1" id="From_date" name="" placeholder="yyyy-mm-dd" value="">
                                          </div>
                                          <div class="label-column2 input-group date">
                                          <span class="lable12">To:</span>
                                             <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                             <input type="text" class="date_picker2" id="To_date" name="" placeholder="yyyy-mm-dd" value="">
                                          </div>                                    
                                    </div>

                                  
                                    <div class="run-btn">
                                    <!-- <button data-toggle="modal" data-target="#myModal" class="btn btn-danger fr-sch" >Schedule</button> -->
                                    <button class="btn btn-success" id="run_details">Run</button>
                                    <a href="<?php echo base_url();?>reports" class="btn btn-danger btn-red">&times;</a>
                                    </div>

                                 </div> 
                                 
                              </div>
                              <div class="for-reportdash">
                                 <div class="left-one">
                                    <div class="pcoded-search">
                                       <span class="searchbar-toggle"> </span>
                                       <div class="pcoded-search-box ">
                                          <input type="text" placeholder="Search" class="search-criteria" id="search-criteria">
                                          <span class="search-icon"><i class="ti-search" aria-hidden="true"></i></span>
                                       </div>
                                          <div id="container1" class="panel-body box-container">
                                            <?php foreach ($firm_users as $key => $value) {  ?>
                                                 <div itemid="<?php echo $value['id']; ?>" class="btn btn-default box-item box_section assignee-option"><?php echo $value['crm_name']; ?><span class="remove" data-role="remove">x</span></div>
                                            <?php } ?>                                            
                                          </div>
                                    </div>                                           
                                 </div>
                                 <div class="right-one">
                                    <div class="filter-data">
                                          <div class="filter-head"><h4>DROP FILTERS HERE
                                          </h4><button class="btn btn-danger f-right" id="clear_container">clear</button>
                                          <div id="container2" class="panel-body box-container">
                                         </div>
                                          </div>                                   
                                         
                                    </div>
                                    

                                    <div id="datatable_results" style="display: none;">
                                         

                                    </div>




                                 </div>
                              </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   

<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<?php //$this->load->view('reports/reports_scripts');?>
<script src="https://cdn.jsdelivr.net/bootstrap.tagsinput/0.4.2/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<!--  <script type="text/javascript" src="<?php echo base_url();?>assets/js/custom-google-chart.js"></script>  -->
<script src="<?php echo base_url();?>assets/js/jquery.mintScrollbar.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.mousewheel.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script> -->
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<?php $this->load->view('reports/custom_scripts');?>