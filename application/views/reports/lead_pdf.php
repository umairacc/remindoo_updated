<table class="table client_table1 text-center display nowrap" id="alltask" cellspacing="0" width="100%" cellpadding="10" style="border-collapse:collapse;border:1px solid #ddd;">
<thead>
<tr class="text-uppercase">
<th style="border:1px solid #ddd;">S.No</th>
<th style="border:1px solid #ddd;">Lead Name</th>
<th style="border:1px solid #ddd;">Company Name</th>
<th style="border:1px solid #ddd;">Email</th>
<th style="border:1px solid #ddd;">Assigned</th>
<th style="border:1px solid #ddd;">Status</th>
</tr>
</thead>
<tbody>
<?php 
$t=1;
foreach ($lead_list as $key => $value) { 


$getUserProfilepic=array();
$ex_assign=explode(',',$value['assigned']);
foreach ($ex_assign as $ex_key => $ex_value) {
$getUserProfilepic[] = $this->Common_mdl->getUserProfileName($ex_value);
}

 ?>                             
<tr id="<?php echo $value['id']; ?>">
<td style="border:1px solid #ddd;">
<?php echo $t; ?>
</td>
<td style="border:1px solid #ddd;"> <?php echo ucfirst($value['name']);?></td>
<td style="border:1px solid #ddd;"><?php 
//echo $value['company'];
if(is_numeric($value['company']))
{
$client_query=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and id='".$value['company']."' order by id desc ")->result_array();
if(count($client_query)>0)
{
echo $client_query[0]['crm_company_name'];
}
else
{
echo '-';
}
}
else
{
echo $value['company'];
}

?></td>
<td style="border:1px solid #ddd;"><?php echo $value['email_address'];?></td>
<td style="border:1px solid #ddd;"> <?php echo implode(',',$getUserProfilepic);    ?></td>
<td style="border:1px solid #ddd;">
<?php //echo $value['created_at'];
echo $this->Report_model->lead_status($value['lead_status']);
?>
</td>
</tr>
<?php $t++; } ?>
</tbody>
</table>
<div id ="chart_img">
 <?php
 if(isset($_SESSION['lead_image'])){ ?>
 <img src="<?php echo $_SESSION['lead_image']; ?>" > 
 <?php } ?>
</div> 
