<?php $this->load->view('includes/header');?>
<?php 
/*$team = $this->db->query("select * from team where create_by=".$_SESSION['id']." ")->result_array();

$department=$this->db->query("select * from department_permission where create_by=".$_SESSION['id']." ")->result_array();*/


?>
<style type="text/css">
  .wrong{
  border: 1px solid red;
}
</style>
  
<div class="pcoded-content card-removes">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card">

                      <div class="modal-alertsuccess alert alert-warning" style="display:none;">
                           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                           <div class="pop-realted1">
                              <div class="position-alert1">
                                <!-- Proposal Messsage Missing  -->
                              </div>
                           </div>
                        </div>
                        <!-- admin start-->
                        <div class="client_section user-dashboard-section1 floating_set time-sheet">
                           <!-- all-clients -->
                           <div class="all_user-section floating_set">
                              <div class="deadline-crm1 floating_set">
                              <ul class="md-tabs tabs112 all_user1 floating_set" id="proposal_dashboard">
                                <li class="nav-item">
                                   <a class="nav-link" href="javascript:;">Tasks
                                   </a>
                                </li>
                              </ul> 
                               <div class="run-btn f-right reporttimesheet">

                               <a href="<?php echo base_url(); ?>Timesheet/timesheet_custom" class="btn btn-success fr-sch" style="padding: 5px;
    font-size: 13px;" >Customise</a> 
                                 <button data-toggle="modal" data-target="#myModal" class="btn btn-danger fr-sch" >Schedule</button>
                                       <!-- <button class="btn btn-success" id="run_details">Run</button>
                                       <butto class="btn btn-danger">&times;</butto> -->
                                 </div>
                              </div>
                              <div class="all_user-section2 floating_set">
                                 <div class="tab-content">
                                    <div id="allusers" class="tab-pane fade in active">
                                       <div id="task"></div>
                                       <div class="client_section3 table-responsive ">
                                          <div class="status_succ"></div>
                                          <div class="all-usera1 data-padding1 ">
                                             <div class="">
                                             <div class="filter-task1">
                                              <ul>
                                  <li><!-- <div class="dropdown-sin-4" id="period_filet_div"> --> 
                                  <select  name="period" id="period_filter" class="form-control"  placeholder="Timesheet Period">
                                  <option value="">Period</option>
                                  <option value="7">Weekly</option>
                                  <option value="30">Monthly</option>
                                  <option value="90">3 Months</option>
                                  <option value="365">Yearly</option>
                                  </select><!-- </div> --></li><li> <!-- <div class="dropdown-sin-4" id="roles_filter_div">  -->
                                <!--   <div class="dropdown-sin-5"> -->
                                  <select id="roles_filter" class="form-control">
                                  <option value="">All User</option>                                  
                               
                                  <?php
                                    if(isset($roles) && count($roles)>0) 
                                    { ?>
                                      <option disabled>Roles</option> 
                                      <?php 
                                      
                                     foreach($roles as $value) 
                                     {
                                        $user_id = $this->Common_mdl->get_price('user','role',$value['id'],'id');
                                        if(!empty($user_id))
                                        {
                                           $roles_arr[strtolower($value["role"])][] = $user_id;
                                        }   
                                     } 

                                     foreach ($roles_arr as $key1 => $value1) 
                                     {
                                        $imp = implode(',', $value1);
                                    ?>
                                 <option value="<?php echo $imp; ?>"><?php echo ucwords($key1); ?></option> 
                                 <?php  
                                     }
                                    } ?>    
                                  </select>  
                                <!--   </div>
                                 -->
                                 <!-- </div> --></li>

                                 <li> <!-- <div class="dropdown-sin-4" id="staff_filter_div"> -->

                                                     <!--     <div class="dropdown-sin-2"> -->
                                  <select id="staff_filter" class="form-control"> 
                                    <option value="">Report to</option>
                                     <?php
                                     if(isset($firm_users) && count($firm_users)>0) 
                                     { ?>
                                         <option disabled>Firm Users</option> 
                                         <?php 
                                          
                                        foreach($firm_users as $value) 
                                        {
                                         
                                       ?>
                                    <option value="<?php echo $value['id']; ?>" ><?php echo $value["crm_name"]; ?></option>
                                    <?php    
                                       
                                        } 
                                   } ?>
                                 
                                    </select>
                             <!--   </div> --><!-- </div> --></li>
                        </ul>
                      </div>

                            <div class="append_content">               
                                                  <table class="table client_table1 text-center display nowrap printableArea" id="alluser" cellspacing="0" width="100%">
                                                   <thead>
                                                      <tr class="text-uppercase">                                
                                                       <!--  <th>First Name</th>
                                                        <th>Last Name</th> -->
                                                        <th>S.No<div class="sortMask"></div></th>
                                                        <th>Task Name<div class="sortMask"></div></th>
                                                        <th>Start Date<div class="sortMask"></div></th>
                                                        <th>End Date<div class="sortMask"></div></th>  
                                                        <th>Regular Hours<div class="sortMask"></div></th>
                                                        <th>Overtime Hours<div class="sortMask"></div></th>
                                                        <th>Total Hours<div class="sortMask"></div></th>                                
                                                      </tr>
                                                   </thead>
                                                    <tbody>
                                                   
                                                    <?php 

                                                    $i = 1;

                                                    foreach ($task_list as $key => $value) { ?>
                                                    <tr id="<?php echo $value['id']; ?>">
                                                    <td><?php echo $i; ?></td>
                                                    <td><?php echo ucfirst($value['subject']);?></td>
                                                    <td>                                      
                                                     <?php echo date('Y-m-d',strtotime($value['start_date']));?>
                                                    </td>
                                                    <td>
                                                    <?php echo date('Y-m-d',strtotime($value['end_date']));?>
                                                    </td>                                                    
                                                 <td>
                                                    <?php
                                                     $hours = $this->Common_mdl->get_working_hours($value['id'],$_SESSION['id']); 
                                                     echo $hours;
                                                    ?>
                                                    </td>
                                                    <td id="innac" class="staff_td">

                                                <?php



                                                    $overtime_hours=0; 
                                                    echo $overtime_hours;
                                                    ?>
                                                    </td>
                                                    <td class="roles_td">

                                                    <?php 

                                                    $total_hours=$overtime_hours + $hours;
                                             echo $total_hours; ?>
                                                    </td>
                                                 
                                                    <!--   <td>Manual</td> -->
                                                    </tr>
                                                    <?php $i++; } ?>
                                                   </tbody>
                                                  </table>
                                                  </div>
                          
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <!-- home-->
                                    <!-- frozen -->
                                 </div>
                              </div>
                              <!-- admin close -->
                           </div>
                           <!-- Register your self card end -->
                        </div>
                     </div>
                  </div>
                  <!-- Page body end -->
               </div>
            </div>
            <!-- Main-body end -->
            <div id="styleSelector">
            </div>
<!-- <div class="col-xs-12 time_stats">
<h3>time stats</h3>
<div class="col-md-12">
<div class="col-md-2">
<button type="button" id="7" class="btn btn-default change">last 7 days</button>
</div>
<div class="col-md-2">
<button type="button" id="30" class="btn btn-default change">last 30 days</button>
</div>
<div class="col-md-2">
<button type="button" id="90" class="btn btn-default change">last 90 days</button>
</div>
<div class="col-md-2">
 <div class="btn-group e-dash sve-action1">
                   <button type="button" class="btn btn-default"><a href="javascript:;" id="365" class="change" data-toggle="modal" data-target="#Send_proposal">view by Month</a></button>
                   <button type="button" class="btn btn-default dropdown-toggle emp-dash" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                   <span class="caret"></span>
                   <span class="sr-only">Toggle Dropdown</span>
                   </button>
                   <ul class="dropdown-menu">                                           
                     <li><a href="javascript:;" id="jan" class="change" >Jan</a></li>
                     <li><a href="javascript:;" id="feb" class="change" >Feb</a></li>
                     <li><a href="javascript:;" id="mar" class="change">Mar</a></li>
                     <li><a href="javascript:;" id="apr" class="change" >Apr</a></li>  
                     <li><a href="javascript:;" id="may" class="change" >May</a></li>
                     <li><a href="javascript:;" id="june" class="change" >June</a></li>
                     <li><a href="javascript:;" id="july" class="change">July</a></li>
                     <li><a href="javascript:;" id="sep" class="change" >Sep</a></li>  
                     <li><a href="javascript:;" id="oct" class="change">Oct</a></li>
                     <li><a href="javascript:;" id="nov" class="change">Nov</a></li>
                     <li><a href="javascript:;" id="dec" class="change">Dec</a></li>                               
                   </ul>
                 </div>
</div>
</div>
<div class="col-xs-12 line_charts">

 <div id="chart_div" style="width: 1200px; height: 500px"></div>
 </div>
 </div> -->
         </div>
      </div>
   </div>
</div>

<?php $this->load->view('reports/schedule_report');?>
<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/custom-google-chart.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.mintScrollbar.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.mousewheel.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
<script src="<?php echo base_url();?>assets/js/mock.js"></script>

<script type="text/javascript">
 
  $('.dropdown-sin-4').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
   });

    $('.dropdown-sin-6').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
   });

    $('.dropdown-sin-2').dropdown({
   
     input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
   });
     $('.dropdown-sin-3').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
   });


$( document ).ready(function() {
$(".prodiv12").mintScrollbar({
onChange: null,
axis: "auto",
wheelSpeed: 120,
disableOnMobile: true
});

   $('.tags').tagsinput({
      allowDuplicates: true
   });
   });

   

    </script>



<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
<script src="<?php echo base_url();?>assets/js/mock.js"></script>


<script type="text/javascript">
   $(document).ready(function(){  
     $( function() {
    $( "#datepicker" ).datepicker();
     $( "#datepicker1" ).datepicker();
   } );

     $("#alluser").DataTable({

                });
   });

   $("#staff_filter,#roles_filter,#period_filter").change(function(){ 
      var staff=$('#staff_filter').val();
      var role=$("#roles_filter").val();
      var period=$("#period_filter").val();
       console.log(staff);
        console.log(role);
         console.log(period);
         var formData={'staff':staff,'role':role,'period':period};
        $.ajax({
          url: '<?php echo base_url(); ?>Timesheet/task_filter',
          type : 'POST',
          data : formData,                    
            beforeSend: function() {
              $(".LoadingImage").show();
            },
            success: function(data) { console.log(data);
               $(".LoadingImage").hide();
               $(".append_content").html('');
               $(".append_content").append(data);
            /*  $('.dropdown-sin-4').dropdown({
              limitCount: 5,
              input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
              });*/
                $("#alluser").DataTable({

                });
            }
        });

   });
$(".period_changes li.dropdown-option").click(function(){
   var time_period=$(this).attr('data-value');
    var id=$(this).parents('.period_changes').find('.periodss').attr('id');
    //console.log(id);
      $.ajax({
          url: '<?php echo base_url(); ?>Timesheet/update_timeperiod',
          type : 'POST',
          data : {'time_period':time_period,'id':id},                    
            beforeSend: function() {
              $(".LoadingImage").show();
            },
            success: function(data) {
               $(".LoadingImage").hide();             
              var json = JSON.parse(data);
              data=json['status'];
              if(data=='1'){
                $(".position-alert1").html('');
                 $(".position-alert1").html('Time Period Updated Successfully');
                 $(".alert-warning").show();
                  setTimeout(function() {        
                 $(".alert-warning").hide();
                 location.reload();
                }, 2000); 

              }
            }
        });
   });

var template=[];

$(".roles_changes li.dropdown-option").click(function(){
var worker=$(this).parents('.roles_td').prev('.staff_td').find('.worker').val();
var id=$(this).parents('.roles_td').prev('.staff_td').find('.worker').attr('id');

// console.log(worker);
// console.log(id);

  if($(this).hasClass( "dropdown-option" )){
    template.push($(this).attr('data-value'));
    //console.log(template);
  }
var roles=template;

if(worker!=''){
   $.ajax({
          url: '<?php echo base_url(); ?>Timesheet/update_reportto',
          type : 'POST',
          data : {'worker':worker,'id':id,'roles':template},                    
            beforeSend: function() {
              $(".LoadingImage").show();
            },
            success: function(data) {
               $(".LoadingImage").hide();             
              var json = JSON.parse(data);
              data=json['status'];
              if(data=='1'){
                $(".position-alert1").html('');
                 $(".position-alert1").html('Time sheet settings changed');
                 $(".alert-warning").show();
                  setTimeout(function() {        
                 $(".alert-warning").hide();
                 location.reload();
                }, 2000); 

              }
            }
        });
}else{
   $(this).prev('.staff_td').find(".dropdown-display").css('border', '1px solid red');
}
});



var template1=[];

$(".staff_changes li.dropdown-option").click(function(){
var roles=$(this).parents('.staff_td').next('.roles_td').find('#roles').val();
var id=$(this).parents('.staff_changes').find('.worker').attr('id');
  if($(this).hasClass( "dropdown-option" )){
    template1.push($(this).attr('data-value'));
    console.log(template1);
  }
var worker=template1;

if(roles!=''){
 
   $.ajax({
          url: '<?php echo base_url(); ?>Timesheet/update_reportto',
          type : 'POST',
          data : {'worker':worker,'id':id,'roles':roles},                    
            beforeSend: function() {
              $(".LoadingImage").show();
            },
            success: function(data) {
               $(".LoadingImage").hide();             
              var json = JSON.parse(data);
              data=json['status'];
              if(data=='1'){
                $(".position-alert1").html('');
                 $(".position-alert1").html('Time sheet settings changed');
                 $(".alert-warning").show();
                  setTimeout(function() {        
                 $(".alert-warning").hide();
                 location.reload();
                }, 2000); 

              }
            }
        });
 }else{
   $(this).next('.roles_td').find(".dropdown-display").css('border', '1px solid red');
 }
});   
   $(document).on('click','#close',function(e)
    {
    $('.alert-success').hide();
    return false;
    });

   $(".change").click(function(){
        var output=$(this).attr('id');
        if(output=='7'){
          var url='<?php echo base_url();?>Timesheet/last_seven';
          var formData={'days':'7'};
        }else if(output=='30'){
           var url='<?php echo base_url();?>Timesheet/last_thirty';
           var formData={'days':'30'};
        }else if(output=='90'){
          var url='<?php echo base_url();?>Timesheet/last_ninty';
          var formData={'days':'90'};
        }else if(output=='365'){
          var url='<?php echo base_url();?>Timesheet/last_year';
          var formData={'days':'365'};
        }else{
          var url='<?php echo base_url();?>Timesheet/month_wise';
          var formData={'days':output};
        }

         $.ajax({
          url: url,
          type : 'POST',
          data : formData,                    
            beforeSend: function() {
              $(".LoadingImage").show();
            },
            success: function(data) {
               $(".LoadingImage").hide();             
              $(".line_charts").html('');
              $(".line_charts").append(data);
            }
        });


  });


    
</script>


