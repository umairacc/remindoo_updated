<?php $this->load->view('includes/header');?>
<style>
   /* bootstrap hack: fix content width inside hidden tabs */
.tab-content > .tab-pane,
.pill-content > .pill-pane {
display: block;     /* undo display:none          */
height: 0;          /* height:0 is also invisible */ 
overflow-y: hidden; /* no-overflow                */
}
.tab-content > .active,
.pill-content > .active {
height: auto;       /* let the content decide it  */
} /* bootstrap hack end */
.box-container {
   height: 200px;
}
 tfoot {
    display: table-header-group;
}

#container1.box-item{
   width: 100%; 
   /*font-size:10px;*/
   z-index: 1000
}

.box-item {
   /*width: 100%; */
   font-size:10px;
   z-index: 1000
}

.dt-buttons{
      display: none;
   }
.run-section{
   position: relative;
}
.running{
   position: absolute;
   display:   block;
   height: 100%;
   top:0;
}

div#container112 {
    height: 358px;
    position: relative;
    display: block;
    margin-top: -235px;
    z-index: 99;
}

.export_option select#exported_result1 {
    width: 100%;
    margin-top: 5px;
}

.export_option select#exported_result1 {
    border-radius: 50px;
    background: transparent;
    color: #000;
    border: 1px solid #000;
}

.export_option select#exported_result2 {
    width: 100%;
    margin-top: 5px;
}

.export_option select#exported_result2 {
    border-radius: 50px;
    background: transparent;
    color: #000;
    border: 1px solid #000;
}
.example2_filter
{
  margin-left: 90px;
}
</style>
<div class="modal fade" id="cus_msg" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" style="padding: 0px -10px 0px 0px;">&times;</button>
      <div class="custom_msg">             
             <div class="pop-realted1">
             <div class="position-alert1" style="text-align: center;"></div>
             </div>
      </div>      
   </div>     
</div>
</div>
</div>
<button id="targetcus_msg" data-toggle="modal" data-target="#cus_msg" style="display: none;"></button>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/mintScrollbar.css">
<div class="pcoded-content card-removes">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card">
                        <!-- admin start-->
                        <div class="client_section user-dashboard-section1 floating_set">
                           <!-- all-clients -->
                           <div class="all_user-section floating_set">
                              <div class="deadline-crm1 floating_set f4reports rdashboard">
                                 <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                                    <li class="nav-item">
                                       <a class="nav-link active" data-toggle="tab" data-target="#reports-tab" href="">Reports</a>
                                       <div class="slide"></div>
                                    </li>
                                 </ul>
                             <?php  $this->load->view('reports/report_customization_filters'); ?>
                              <div class="for-reportdash">
                                 <div class="left-one">
                                    <div class="pcoded-search">
                                       <span class="searchbar-toggle"> </span>
                                       <div class="pcoded-search-box ">
                                          <input type="text" placeholder="Search" class="search-criteria" id="search-criteria">
                                          <span class="search-icon"><i class="ti-search" aria-hidden="true"></i></span>
                                       </div>
                                          <div id="container1" class="panel-body box-container">

                                          <div itemid="user" class="btn btn-default box-item box_section summary_option">User<span class="remove" data-role="remove">x</span></div>
                                          <div itemid="task" class="btn btn-default box-item box_section summary_option">Task<span class="remove" data-role="remove">x</span></div>
                                          <div itemid="leads" class="btn btn-default box-item box_section summary_option">Leads<span class="remove" data-role="remove">x</span></div>  

                                          </div>
                                            


                                         <!--  <div id="container112" class=""></div> -->
                                    </div>                                           
                                 </div>
                                 <div class="right-one">
                             <!--     <div class="run-section"> -->
                                   <div class="filter-data">
                                          <div class="filter-head"><h4>DROP FILTERS HERE
                                          </h4><button class="btn btn-danger f-right" id="clear_container">clear</button>
                                          <div id="container2" class="panel-body box-container">
                                         </div>
                                       <!--   <div id="container212" class=""> -->
                                             
                                          <!-- </div> -->
                                          </div>                                   
                                         
                                    </div>
                                   <!--   <div class="running"> </div> -->
                                 <!--    </div> -->
                                   
                                     

                                    <div id="datatable_results" style="display: none;">
                                          
                                    
  

                                    </div>




                                 </div>
                              </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<?php //$this->load->view('reports/reports_scripts');?>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/custom-google-chart.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.mintScrollbar.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.mousewheel.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

<script type="text/javascript">

 $(document).ready(function(){ 
       $( function() {
           $( ".date-picker" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();      
       });
   $( function() {
            $( ".date_picker2" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
       });
   $( function() {
          $( ".date_picker1" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
       }); 
     });


</script>

<script type="text/javascript">
   $( document ).ready(function() {
$(".prodiv12").mintScrollbar({
onChange: null,
axis: "auto",
wheelSpeed: 120,
disableOnMobile: true
});

   $('.tags').tagsinput({
      allowDuplicates: true
   });
   });


   $(document).ready(function() {

  $('.box-item').draggable({
    cursor: 'move',
    helper: "clone"
  });

  $("#container1").droppable({
    drop: function(event, ui) {
      var itemid = $(event.originalEvent.toElement).attr("itemid");
      $('.box-item').each(function() {
        if ($(this).attr("itemid") === itemid) {
          $(this).appendTo("#container1");
        }
      });
    }
  });

$(".remove").click(function(){  
    $(this).parent('.box-item').addClass('replace');
      $('.replace').each(function() {    
          $(this).appendTo("#container1");      
      });
});

$("#clear_container").click(function(){

  $("#container2").html('');

});




  $("#container2").droppable({

    drop: function(event, ui) {
      var itemid = $(event.originalEvent.toElement).attr("itemid");
      $('.box-item').each(function() {
        if ($(this).attr("itemid") === itemid) {
         $(this).addClass('insearch_section');
          $(this).appendTo("#container2");      
        }
      });
    }
  });


//   $("#run_details").click(function(){
//    $("#container2 .box-item").each(function() {
// console.log($(this).attr("itemid"));
//    });
//   });


  $(document).on('click',"#run_details",function(){ 

    var client=$("#clientwise_filter").val();
    //var date=$("#datewise_filter").val();
    //var team=$("#teamwise_filter").val();
    var filter=$(".filter").attr('id'); 
    var from_date=$("#From_date").val();
    var to_date=$("#To_date").val();

     var summary=[];
      $("#container2 .summary_option").each(function() {
         summary.push($(this).attr("itemid"));
      });

   if (summary.length === 0) 
   {
      summary.push('all');  
   }

   if(filter!="" && (from_date!="" || to_date!=""))
   {
      $('#'+filter).removeClass('filter');
      filter = "";     
   }

   if(filter=="" && from_date!="" && to_date =="")
   {
      $('.custom_msg').find('.position-alert1').html("Choose End Date.");
      $('#targetcus_msg').trigger('click');
   }  
   else if(filter=="" && from_date=="" && to_date !="")
   {
      $('.custom_msg').find('.position-alert1').html("Choose Start Date.");
      $('#targetcus_msg').trigger('click');
   }
   else
   {

    var formData={'from_date':from_date,'to_date':to_date,'summary':summary,'filter':filter,'client':client};
    
    $.ajax({
         type: "POST",
         url: "<?php echo base_url();?>/Filter/summary_customization",         
         data: formData,
         beforeSend: function() {
           $(".LoadingImage").show();
         },
         success: function(data) {    
             $(".LoadingImage").hide();          
                  $("#datatable_results").html('');
              $("#datatable_results").append(data);
               $("#datatable_results").show();                

                       var table10 = $('#example').DataTable({
                          //   "scrollX": true,
                           dom: 'Bfrtip',
                            buttons: [{
                         extend: 'pdf',
                         title: 'Firm Users List',
                         filename: 'Firm_Users_List'
                       }, {
                         extend: 'excel',                           
                         title: 'Firm Users List',
                         filename: 'Firm_Users_List'
                       }, {
                         extend: 'csv',
                         title: 'Firm Users List',
                         filename: 'Firm_Users_List'
                       },{
                         extend: 'print',
                         title: 'Firm Users List',
                       }],
                          
                       });

                       var table20 = $('#example1').DataTable({ 
                    //     "scrollX": true,
                       dom: 'Bfrtip',
                       buttons: [{
                         extend: 'pdf',
                            title: 'Completed Tasks List',
                            filename: 'Completed_Tasks_List'
                         }, {
                         extend: 'excel',                           
                            title: 'Completed Tasks List',
                            filename: 'Completed_Tasks_List'
                         }, {
                         extend: 'csv',
                            title: 'Completed Tasks List',
                            filename: 'Completed_Tasks_List'
                         },{
                         extend: 'print',
                            title: 'Completed Tasks Details',
                         }],       
                     
                   });

                      var table30 = $('#example2').DataTable({
                     //    "scrollX": true,
                      dom: 'Bfrtip',
                        buttons: [{
                        extend: 'pdf',
                           title: 'Leads List',
                           filename: 'Leads Details'
                        }, {
                        extend: 'excel',                          
                           title: 'Leads List',
                           filename: 'Leads Details'
                        }, {
                        extend: 'csv',
                        title: 'Leads List',
                           filename: 'Leads Details'
                        },{
                        extend: 'print',
                           title: 'Leads Details',
                        }],
                    
                  });
                  
                    $("#exported_result").change(function(){
                    var value=$(this).val();
                    if(value=='pdf'){
                    $("div #example_wrapper .buttons-pdf").trigger('click');
                    }else if(value="excel"){
                    $("div #example_wrapper .buttons-excel").trigger('click');
                    }else if(value="csv"){
                    $("div #example_wrapper .buttons-csv ").trigger('click');
                    }
                    });

                    $("#print_report").click(function(){
                    $("div #example_wrapper .buttons-print").trigger('click');
                    });


                    $("#exported_result1").change(function(){
                    var value=$(this).val(); 
                    if(value=='pdf'){
                    $("div #example1_wrapper .buttons-pdf").trigger('click');
                    }else if(value="excel"){
                    $("div #example1_wrapper .buttons-excel").trigger('click');
                    }else if(value="csv"){
                    $("div #example1_wrapper .buttons-csv ").trigger('click');
                    }
                    });

                    $("#print_report1").click(function(){
                    $("div #example1_wrapper .buttons-print").trigger('click');
                    });

                    $("#exported_result2").change(function(){
                    var value=$(this).val(); 
                    if(value=='pdf'){
                    $("div #example2_wrapper .buttons-pdf").trigger('click');
                    }else if(value="excel"){
                    $("div #example2_wrapper .buttons-excel").trigger('click');
                    }else if(value="csv"){
                    $("div #example2_wrapper .buttons-csv ").trigger('click');
                    }
                    });

                    $("#print_report2").click(function(){
                    $("div #example2_wrapper .buttons-print").trigger('click');
                    });                  

               
           }
      });
  }

});

$('#search-criteria').keyup(function(){
    $('.box_section').hide();
    var txt = $('#search-criteria').val();
    $('.box_section').each(function(){
       if($(this).text().toUpperCase().indexOf(txt.toUpperCase()) != -1){
           $(this).show();
       }
    });
});


function controll(){
 //console.log('eeeee');
      $('.box-item').each(function() {
       // if ($(this).attr("itemid") === itemid) {
         $(this).addClass('insearch_section');
          $(this).clone().appendTo("#container2");      
       // }
      });


      //  $('.box-item').each(function() {
      // //  if ($(this).attr("itemid") === itemid) {
      //     $(this).appendTo("#container1");
      // //  }
      // });
   
}


//   $(document).ready(function () { 

//    $(".filter-data").find('*').prop('disabled', true);

//       controll();

// $("#container2").find('*').prop('disabled', true);

// //$(".remove").css('display','none');
//   });

$(".fill").click(function(){
      $('.fill').removeClass('filter');
      $(this).addClass('filter');
   });

});
</script>



<script type="text/javascript">
   $(document).ready(function () {     
     $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var ev = document.createEvent('Event');
    ev.initEvent('resize', true, true);
    window.dispatchEvent(ev);
  }); 
   });
</script>