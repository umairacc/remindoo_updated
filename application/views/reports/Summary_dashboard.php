<?php $this->load->view('includes/header');?>
<style>
   /* bootstrap hack: fix content width inside hidden tabs */
.tab-content > .tab-pane,
.pill-content > .pill-pane {
display: block;     /* undo display:none          */
height: 0;          /* height:0 is also invisible */ 
overflow-y: hidden; /* no-overflow                */
}
.tab-content > .active,
.pill-content > .active {
height: auto;       /* let the content decide it  */
} /* bootstrap hack end */
.box-container {
   height: 200px;
}
 tfoot {
    display: table-header-group;
}

#container1.box-item{
   width: 100%; 
   /*font-size:10px;*/
   z-index: 1000
}

.box-item {
   /*width: 100%; */
   font-size:10px;
   z-index: 1000
}
 .dropdown-content {
    display: none;
    position: absolute;
    background-color: #fff;
    min-width: 86px;
    overflow: auto;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    left: -92px;
    width: 150px;
}
/*.dt-buttons{
      display: none;
   }*/
.run-section{
   position: relative;
}
.running{
   position: absolute;
   display:   block;
   height: 100%;
   top:0;
}
.wrong{
  border: 1px solid red;
}
div#container112 {
    height: 358px;
    position: relative;
    display: block;
    margin-top: -235px;
    z-index: 99;
}
.emp-dash
{
  display: none;
}
</style>

<link rel="stylesheet" href="<?php echo base_url();?>assets/css/mintScrollbar.css">
<div class="pcoded-content card-removes">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card">
                        <!-- admin start-->
                        <div class="client_section user-dashboard-section1 floating_set">
                           <!-- all-clients -->
                           <div class="all_user-section floating_set">
                              <div class="deadline-crm1 floating_set f4reports rdashboard">
                                 <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                                    <li class="nav-item">
                                       <a class="nav-link active" data-toggle="tab" data-target="#reports-tab" href="">Reports</a>
                                       <div class="slide"></div>
                                    </li>
                                 </ul>
                                 <div class="run-btn f-right">
                                 <button data-toggle="modal" data-target="#myModal" class="btn btn-danger fr-sch" >Schedule</button>
                                       <!-- <button class="btn btn-success" id="run_details">Run</button>
                                       <butto class="btn btn-danger">&times;</butto> -->
                                 </div>
                              </div>
                              <div class="for-reportdash">
                                 <div class="left-one">
                                    <div class="pcoded-search">
                                       <span class="searchbar-toggle"> </span>
                                       <div class="pcoded-search-box ">
                                          <input type="text" placeholder="Search" class="search-criteria" id="search-criteria">
                                          <span class="search-icon"><i class="ti-search" aria-hidden="true"></i></span>
                                       </div>
                                          <div id="container1" class="panel-body box-container">
                                          <div itemid="user" class="btn btn-default box-item box_section">User<span class="remove" data-role="remove">x</span></div>
                                          <div itemid="Task" class="btn btn-default box-item box_section">Task<span class="remove" data-role="remove">x</span></div>
                                          <div itemid="leads" class="btn btn-default box-item box_section">Leads<span class="remove" data-role="remove">x</span></div>  
                                          </div>

                                          
                                          

                                          <div id="container112" class=""></div>
                                    </div>                                           
                                 </div>
                                 <div class="right-one">
                             <!--     <div class="run-section"> -->
                                   <div class="filter-data">
                                          <div class="filter-head"><h4>DROP FILTERS HERE
                                          </h4><button class="btn btn-danger f-right" id="clear_container">clear</button>
                                          <div id="container2" class="panel-body box-container">
                                         </div>
                                         <div id="container212" class="">
                                             
                                          </div>
                                          </div>                                   
                                         
                                    </div>


                                   <!--   <div class="running"> </div> -->
                                 <!--    </div> -->
                                   
                                   

                                    <div id="datatable_results" >
                                          
                                    
  <div class="sample_section">
 <div class="export_option">
 <div class="btn-group e-dash sve-action1 frpropos">

  <button type="button" class="btn btn-default dropdown-toggle emp-dash" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select
 
  </button>
  <ul class="dropdown-menu" >
    <li id="buttons"></li>
  </ul>
</div>
</div>

  </div>
  
<table class="table client_table1 text-center display nowrap printableArea" id="example" cellspacing="0" width="100%"  cellpadding="10" style="border-collapse:collapse;border:1px solid #ddd;">
<thead>
<tr class="text-uppercase">
<th style="border:1px solid #ddd;">
S.no<div class="sortMask"></div>
</th>
<?php if(isset($column_setting[0]['profile_image'])&&($column_setting[0]['profile_image']==1)){?>
<th style="border:1px solid #ddd;">Profile<div class="sortMask"></div></th>
<?php } ?>      
<th style="border:1px solid #ddd;">Name<div class="sortMask"></div></th>
<th style="border:1px solid #ddd;">Updates<div class="sortMask"></div></th>
<!-- <th style="border:1px solid #ddd;">Active</th> -->
<!-- <th style="border:1px solid #ddd;">CompanyStatus</th> -->
<th style="border:1px solid #ddd;">User Type<div class="sortMask"></div></th>
<th style="border:1px solid #ddd;">Status<div class="sortMask"></div></th>
</tr>
</thead>

<tfoot>
<tr class="table-header">
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<?php if(isset($column_setting[0]['profile_image'])&&($column_setting[0]['profile_image']==1)){?>
<th style="border:1px solid #ddd;">Profile</th>
<?php } ?>   
<th ><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th ><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<!-- <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th> -->
</tr>
</tfoot>
 <tbody>
<?php 

if(count($firm_users)>0){
$s = 1;
foreach ($firm_users as $getallUserkey => $getallClientvalue) {
$update=$this->Common_mdl->select_record('update_client','user_id',$getallClientvalue['id']);
$company_status=$this->Common_mdl->select_record('client','user_id',$getallClientvalue['id']);
$getallUservalue=$this->Common_mdl->select_record('user','id',$getallClientvalue['id']);
if($getallUservalue['role']=='6'){
//$role = 'Staff';
$edit=base_url().'user/view_staff/'.$getallUservalue['id'];
$house='Manual';
}elseif($getallUservalue['role']=='4' && $company_status['status']==0 ){
//$role = 'Client';
$edit=base_url().'Client/client_info/'.$getallUservalue['id'];
$house='Manual';
}elseif($getallUservalue['role']=='4' && $company_status['status']==1){
//$role = 'Client';
$edit=base_url().'Client/client_info/'.$getallUservalue['id'];
$house='Company House';
}elseif($getallUservalue['role']=='4' && $company_status['status']==2){
//$role = 'Client';
$edit=base_url().'Client/client_info/'.$getallUservalue['id'];
$house='Import';
}else{
$house ='';
$role = '';
$edit='#';
}
if($getallUservalue['status']=='1'){
$status_id = 'checkboxid1';  
$chked = 'selected';
$status_val = '1';
$active='Active';
}elseif($getallUservalue['status']=='2' || $getallUservalue['status']=='0' ){
$status_id = 'checkboxid2';  
$chked = 'selected';
$status_val = '2';
$active='Inactive';
} elseif($getallUservalue['status']=='3'){
$status_id = 'checkboxid3';  
$chked = 'selected';
$status_val = '3';
$active='Frozen';
}
$role=$this->Common_mdl->select_record('Role','id',$getallUservalue['role']);
?>
<tr id="<?php echo $getallUservalue["id"]; ?>">
<td style="border:1px solid #ddd;"><?php echo $s;?></td>
<?php if(isset($column_setting[0]['profile_image'])&&($column_setting[0]['profile_image']==1)){?>
<td style="border:1px solid #ddd;" class="user_imgs"><img src="<?php echo base_url().'/uploads/'.$getallUservalue['crm_profile_pic'];?>" alt="img"></td>
<?php } ?>
<td style="border:1px solid #ddd;"><?php echo ucfirst($getallUservalue['crm_name']);?></td>                               
<td style="border:1px solid #ddd;">                                  
<?php echo ($update['counts']!='')? ($update['counts']):'0';?>  new
</td> 
<!-- <td style="border:1px solid #ddd;" id="innac">
  <?php 
        if($company_status['crm_company_status'] == '1')
        {
          echo "Active";
        }
        else if(strlen($company_status['crm_company_status']) > '1')
        {
          echo ucwords($company_status['crm_company_status']);
        } 
        else
        {
           echo "Inactive";
        }
  ?>
</td> -->
<td style="border:1px solid #ddd;"><?php echo $role['role'];?></td>
<!-- <td style="border:1px solid #ddd;"><?php echo $house;?></td> -->
<td>
<?php if($getallUservalue['status']=='1'){ echo "Active"; } elseif($getallUservalue['status']=='0'){ echo "Inactive"; }elseif($getallUservalue['status']=='3'){ echo "Frozen"; }else{ echo '-'; } ?>
</td>
</tr>
<?php $s++; }

}else{ ?>
<tr><td style="border:1px solid #ddd;">No records found</td></tr>
<?php } ?>
</tbody>
</table>

<div class="sample_section">
<div class="export_option">
 <div class="btn-group e-dash sve-action1 frpropos">

  <button type="button" class="btn btn-default dropdown-toggle emp-dash" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select
 
  </button>
  <ul class="dropdown-menu" >
    <li id="buttons1"></li>
  </ul>
</div>
</div>
  </div>



<table class="table client_table1 text-center display nowrap" id="example1" cellspacing="0" width="100%"   cellpadding="10" style="border-collapse:collapse;border:1px solid #ddd;">
<thead>
<tr class="text-uppercase">
<th style="border:1px solid #ddd;">
S.No<div class="sortMask"></div>
</th>
<th style="border:1px solid #ddd;">Task Name<div class="sortMask"></div></th>
<th style="border:1px solid #ddd;">Start Date<div class="sortMask"></div></th>
<th style="border:1px solid #ddd;">Due Date<div class="sortMask"></div></th>
<th style="border:1px solid #ddd;">Status<div class="sortMask"></div></th>
<th style="border:1px solid #ddd;">Priority<div class="sortMask"></div></th>

<th style="border:1px solid #ddd;">Assignto<div class="sortMask"></div></th>
</tr>
</thead>

<tfoot>
<tr class="table-header">
<th></th>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th ><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th ><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
</tr>
</tfoot>
<tbody>
<?php 
if(count($completed_tasks) > 0){
$t=1;
foreach ($completed_tasks as $key => $value) {
error_reporting(0);
// $value['end_date'] = date('d-m-Y');
if($value['start_date']!='' && $value['end_date']!=''){    
$start  = date_create(implode('-', array_reverse(explode('-', $value['start_date']))));
$end  = date_create(implode('-', array_reverse(explode('-', $value['end_date']))));

//$end  = date_create(); // Current time and date
$diff    = date_diff ( $start, $end );

$y =  $diff->y;
$m =  $diff->m;
$d =  $diff->d;
$h =  $diff->h;
$min =  $diff->i;
$sec =  $diff->s;
}
else{
$y =  0;
$m =  0;
$d =  0;
$h =  0;
$min = 0;
$sec =  0;
}

$date_now = date("Y-m-d"); // this format is string comparable
$d_rec = implode('-', array_reverse(explode('-', $value['end_date'])));
if ($date_now > $d_rec) {
//echo 'priya';
$d_val = "EXPIRED";
}else{
$d_val = $y .' years '. $m .' months '. $d .' days '. $h .' Hours '. $min .' min '. $sec .' sec ';
}


if($value['worker']=='')
{
$value['worker'] = 0;
}
$staff=$this->db->query("SELECT * FROM staff_form WHERE user_id in (".$value['worker'].")")->result_array();
if($value['task_status']=='notstarted')
{
$percent = 0;
$stat = 'Not Started';
} if($value['task_status']=='inprogress')
{
$percent = 25;
$stat = 'In Progress';
} if($value['task_status']=='awaiting')
{
$percent = 50;
$stat = 'Awaiting for a feedback';
} if($value['task_status']=='testing')
{
$percent = 75;
$stat = 'Testing';
} if($value['task_status']=='complete')
{
$percent = 100;
$stat = 'Complete';
}
$exp_tag = explode(',', $value['tag']);
$explode_worker=explode(',',$value['worker']);
?>
<tr id="<?php echo $value['id']; ?>">

<td style="border:1px solid #ddd;">
<?php echo $t; ?>
</td>
<td style="border:1px solid #ddd;"> <?php echo ucfirst($value['subject']);?></td>
<td style="border:1px solid #ddd;"><?php echo date('Y-m-d',strtotime($value['start_date']));?></td>
<td style="border:1px solid #ddd;"><?php echo date('Y-m-d',strtotime($value['end_date']));?></td>
<td style="border:1px solid #ddd;"><?php echo $stat;?></td>
<td style="border:1px solid #ddd;">
<?php echo $value['priority'];?>
</td>
<td style="border:1px solid #ddd;" class="user_imgs" id="task_<?php echo $value['id'];?>">
<?php 
   
   $assignees_det = $this->Common_mdl->getAssignee('TASK',$value['id']); 
   
   foreach ($assignees_det as $key => $value) 
   {
      $assignees[] = ucwords($value['crm_name']);
   }

   echo implode(',', array_unique($assignees));  

?>  
</td>
</tr>
<?php $t++; }

}else{ ?>
<tr><td colspan="5">No records Found</td></tr>
<?php } ?>
</tbody>
</table>
<div class="sample_section">
<div class="export_option">
 <div class="btn-group e-dash sve-action1 frpropos">

  <button type="button" class="btn btn-default dropdown-toggle emp-dash" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select
 
  </button>
  <ul class="dropdown-menu" >
    <li id="buttons2"></li>
  </ul>
</div>
</div>
  </div>
<table class="table client_table1 text-center display nowrap" id="example2" cellspacing="0" width="100%"   cellpadding="10" style="border-collapse:collapse;border:1px solid #ddd;">
<thead>
<tr class="text-uppercase">
<th style="border:1px solid #ddd;">S.No<div class="sortMask"></div></th>
<th style="border:1px solid #ddd;">Lead Name<div class="sortMask"></div></th>
<th style="border:1px solid #ddd;">Company Name<div class="sortMask"></div></th>
<th style="border:1px solid #ddd;">Email<div class="sortMask"></div></th>
<th style="border:1px solid #ddd;">Assigned<div class="sortMask"></div></th>
<th style="border:1px solid #ddd;">Status<div class="sortMask"></div></th>
</tr>
</thead>

<tfoot>
<tr class="table-header">
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th ><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th ><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
<th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
</tr>
</tfoot>
<tbody>
<?php 
if(count($lead_details)){
$t=1;
foreach ($lead_details as $key => $value) { 


$getUserProfilepic=array();
$ex_assign=explode(',',$value['assigned']);
foreach ($ex_assign as $ex_key => $ex_value) {
$getUserProfilepic[] = $this->Common_mdl->getUserProfileName($ex_value);
}

 ?>                             
<tr id="<?php echo $value['id']; ?>">
<td style="border:1px solid #ddd;">
<?php echo $t; ?>
</td>
<td style="border:1px solid #ddd;"> <?php echo ucfirst($value['name']);?></td>
<td style="border:1px solid #ddd;"><?php 
//echo $value['company'];
if(is_numeric($value['company']))
{
$client_query=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and id='".$value['company']."' order by id desc ")->result_array();
if(count($client_query)>0)
{
echo $client_query[0]['crm_company_name'];
}
else
{
echo '-';
}
}
else
{
echo $value['company'];
}

?></td>
<td style="border:1px solid #ddd;"><?php echo $value['email_address'];?></td>
<td style="border:1px solid #ddd;"><?php 
   
   $assignees_det = $this->Common_mdl->getAssignee('LEADS',$value['id']); 
   
   foreach ($assignees_det as $key => $val) 
   {
      $assignees[] = ucwords($val['crm_name']);
   }

   echo implode(',', array_unique($assignees));  

?>  </td>
<td style="border:1px solid #ddd;">
<?php //echo $value['created_at'];
echo $this->Report_model->lead_status($value['lead_status']);
?>
</td>
</tr>
<?php $t++; }
}else{ ?>
<tr><td style="border:1px solid #ddd;" colspan="5">No records Found</td></tr>
  <?php } ?>
</tbody>
</table>

                                    </div>




                                 </div>
                              </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

<?php $this->load->view('reports/schedule_report');?>
<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/custom-google-chart.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.mintScrollbar.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.mousewheel.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
<script src="<?php echo base_url();?>assets/js/mock.js"></script>
<script type="text/javascript">
 
  $('.dropdown-sin-4').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
   });

    $('.dropdown-sin-6').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
   });

    $('.dropdown-sin-2').dropdown({
   
     input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
   });
     $('.dropdown-sin-3').dropdown({
     limitCount: 5,
     input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
   });
$(document).ready(function() {


  var check=0;
    var check1=0;
    var numCols = $('#example thead th').length;
        var table10 = $('#example').DataTable({
       //   "scrollX": true,
        dom: 'Bfrtip',
         buttons: [{
      extend: 'pdf',
      title: 'Firm Users List',
      filename: 'Firm_Users_List'
    }, {
      extend: 'excel',
        extension: '.xls',
      title: 'Firm Users List',
      filename: 'Firm_Users_List'
    }, {
      extend: 'csv',
      filename: 'Firm_Users_List'
    },{
      extend: 'print',
      title: 'Firm Users List',
    }],
       initComplete: function () { 
                  var q=0;
                     $('#example tfoot th').find('.filter_check').each( function(){                    
                       $(this).attr('id',"all_"+q);
                       q++;
                     });
                  for(i=0;i<numCols;i++){                 
                        var select = $("#all_"+i); 
                      
                      this.api().columns([i]).every( function () {
                        var column = this;
                        column.data().unique().sort().each( function ( d, j ) {    
                          console.log(d);         
                          select.append( '<option value="'+d+'">'+d+'</option>' )
                        });
                      });                   
           
                     $("#all_"+i).formSelect();  
                  }
        }
    });
      for(j=0;j<numCols;j++){  
          $('#all_'+j).on('change', function(){ 
            var result=$(this).attr('id').split('_');      
             var c=result[1];
              var search = [];              
              $.each($('#all_'+c+ ' option:selected'), function(){                
              search.push($(this).val());
              });      
              search = search.join('|');    
             

              table10.column(c).search(search, true, false).draw();  
          });
       }
    // $('#example').DataTable( {
    //     dom: 'Bfrtip',     

    //     buttons: [{
    //       extend: 'pdf',
    //          title: 'Client List',
    //          filename: 'Client Details'
    //       }, {
    //       extend: 'excel',
    //         extension: '.xls',
    //          title: 'Client List',
    //          filename: 'Client Details'
    //       }, {
    //       extend: 'csv',
    //          filename: 'Client Details'
    //       },{
    //       extend: 'print',
    //          title: 'Client Details',
    //       }]  
    //     // buttons: [
    //     //     'copy', 'csv', 'excel', 'pdf', 'print'
    //     // ]
    // } );

    //     $('#example1').DataTable( {
    //     dom: 'Bfrtip',     

    //     buttons: [{
    //       extend: 'pdf',
    //          title: 'Completed Tasks List',
    //          filename: 'Task Details'
    //       }, {
    //       extend: 'excel',
    //         extension: '.xls',
    //          title: 'Completed Tasks List',
    //          filename: 'Task Details'
    //       }, {
    //       extend: 'csv',
    //          filename: 'Task Details'
    //       },{
    //       extend: 'print',
    //          title: 'Completed Tasks Details',
    //       }]  
    //     // buttons: [
    //     //     'copy', 'csv', 'excel', 'pdf', 'print'
    //     // ]
    // } );

    var check=0;
    var check1=0;
    var numCols = $('#example1 thead th').length;
        var table20 = $('#example1').DataTable({
     //     "scrollX": true,
        dom: 'Bfrtip',
        buttons1: [{
          extend: 'pdf',
             title: 'Completed Tasks List',
             filename: 'Completed_Tasks_List'
          }, {
          extend: 'excel',
            extension: '.xls',
             title: 'Completed Tasks List',
             filename: 'Completed_Tasks_List'
          }, {
          extend: 'csv',
             filename: 'Completed_Tasks_List'
          },{
          extend: 'print',
             title: 'Completed Tasks Details',
          }],       
       initComplete: function () { 
                  var q=0;
                     $('#example1 tfoot th').find('.filter_check').each( function(){                    
                       $(this).attr('id',"leads_"+q);
                       q++;
                     });
                  for(i=0;i<numCols;i++){                 
                        var select = $("#leads_"+i); 
                      
                      this.api().columns([i]).every( function () {
                        var column = this;
                        column.data().unique().sort().each( function ( d, j ) {    
                          console.log(d);         
                          select.append( '<option value="'+d+'">'+d+'</option>' )
                        });
                      });                   
           
                     $("#leads_"+i).formSelect();  
                  }
        }
    });
      for(j=0;j<numCols;j++){  
          $('#leads_'+j).on('change', function(){ 
            var result=$(this).attr('id').split('_');      
             var c=result[1];
              var search = [];              
              $.each($('#leads_'+c+ ' option:selected'), function(){                
              search.push($(this).val());
              });      
              search = search.join('|');  

              table20.column(c).search(search, true, false).draw();  
          });
       }




    var check=0;
    var check1=0;
    var numCols = $('#example2 thead th').length;
        var table30 = $('#example2').DataTable({
       //    "scrollX": true,
        dom: 'Bfrtip',
          buttons2: [{
          extend: 'pdf',
             title: 'Leads List',
             filename: 'Leads Details'
          }, {
          extend: 'excel',
            extension: '.xls',
             title: 'Leads List',
             filename: 'Leads Details'
          }, {
          extend: 'csv',
             filename: 'Leads Details'
          },{
          extend: 'print',
             title: 'Leads Details',
          }],
       initComplete: function () { 
                  var q=0;
                     $('#example2 tfoot th').find('.filter_check').each( function(){                    
                       $(this).attr('id',"task_"+q);
                       q++;
                     });
                  for(i=0;i<numCols;i++){                 
                        var select = $("#task_"+i); 
                      
                      this.api().columns([i]).every( function () {
                        var column = this;
                        column.data().unique().sort().each( function ( d, j ) {    
                          console.log(d);         
                          select.append( '<option value="'+d+'">'+d+'</option>' )
                        });
                      });                   
           
                     $("#task_"+i).formSelect();  
                  }
        }
    });
      for(j=0;j<numCols;j++){  
          $('#task_'+j).on('change', function(){ 
            var result=$(this).attr('id').split('_');      
             var c=result[1];
              var search = [];              
              $.each($('#task_'+c+ ' option:selected'), function(){                
              search.push($(this).val());
              });      
              search = search.join('|');  

              table30.column(c).search(search, true, false).draw();  
          });
       }


    //         $('#example2').DataTable( {
    //     dom: 'Bfrtip',     

    //     buttons: [{
    //       extend: 'pdf',
    //          title: 'Leads List',
    //          filename: 'Leads Details'
    //       }, {
    //       extend: 'excel',
    //         extension: '.xls',
    //          title: 'Leads List',
    //          filename: 'Leads Details'
    //       }, {
    //       extend: 'csv',
    //          filename: 'Leads Details'
    //       },{
    //       extend: 'print',
    //          title: 'Leads Details',
    //       }]  
    //     // buttons: [
    //     //     'copy', 'csv', 'excel', 'pdf', 'print'
    //     // ]
    // } );



} );


$("#exported_result").change(function(){
var value=$(this).val();
if(value=='pdf'){
$(".buttons-pdf").trigger('click');
}else if(value="excel"){
$(".buttons-excel").trigger('click');
}else if(value="csv"){
$(".buttons-csv ").trigger('click');
}
});


$("#print_report").click(function(){
$(".buttons-print").trigger('click');
});
</script>


<script type="text/javascript">
   $( document ).ready(function() {
$(".prodiv12").mintScrollbar({
onChange: null,
axis: "auto",
wheelSpeed: 120,
disableOnMobile: true
});

   $('.tags').tagsinput({
      allowDuplicates: true
   });
   });


   $(document).ready(function() {



  $('.box-item').draggable({
    cursor: 'move',
    helper: "clone"
  });

  $("#container1").droppable({
    drop: function(event, ui) {
      var itemid = $(event.originalEvent.toElement).attr("itemid");
      $('.box-item').each(function() {
        if ($(this).attr("itemid") === itemid) {
          $(this).appendTo("#container1");
        }
      });
    }
  });

$(".remove").click(function(){  
    $(this).parent('.box-item').addClass('replace');
      $('.replace').each(function() {    
          $(this).appendTo("#container1");      
      });
});

$("#clear_container").click(function(){

   $(".insearch_section .remove").trigger('click');

});




  $("#container2").droppable({

    drop: function(event, ui) {
      var itemid = $(event.originalEvent.toElement).attr("itemid");
      $('.box-item').each(function() {
        if ($(this).attr("itemid") === itemid) {
         $(this).addClass('insearch_section');
          $(this).appendTo("#container2");      
        }
      });
    }
  });


//   $("#run_details").click(function(){
//    $("#container2 .box-item").each(function() {
// console.log($(this).attr("itemid"));
//    });
//   });


  /*$("#run_details").click(function(){

   var id=1;
 
         $.ajax({
            url: '<?php echo base_url(); ?>Reports/chart',
            type : 'POST',
            data : {'id':id},                    
            beforeSend: function() {
            $(".LoadingImage").show();
            },
            success: function(data) {
               $(".LoadingImage").hide();   
               $("#datatable_results").html('');
               $("#datatable_results").append(data);
               $("#datatable_results").show();

               $(document).ready(function() {
                  $('#example').DataTable( {
                     dom: 'Bfrtip',
                     buttons: [{
                        extend: 'pdf',
                           title: 'Client List',
                           filename: 'Client Details'
                        }, {
                        extend: 'excel',
                           title: 'Client List',
                           filename: 'Client Details'
                        }, {
                        extend: 'csv',
                           filename: 'Client Details'
                        },{
                        extend: 'print',
                           title: 'Client Details',
                        }]           
                  });
               });

               $("#exported_result").change(function(){
               var value=$(this).val();        
                  if(value=='pdf'){
                      $(".buttons-pdf").trigger('click');
                  }else if(value=="excel"){
                      $(".buttons-excel").trigger('click');
                  }else if(value=="csv"){             
                     $(".buttons-csv").trigger('click');
                  }
               });

               $("#print_report").click(function(){
                   $(".buttons-print").trigger('click');
               });                                        
            }
         }); 
       });
 

*/

});




$('#search-criteria').keyup(function(){
    $('.box_section').hide();
    var txt = $('#search-criteria').val();
    $('.box_section').each(function(){
       if($(this).text().toUpperCase().indexOf(txt.toUpperCase()) != -1){
           $(this).show();
       }
    });
});


function controll(){
 //console.log('eeeee');
      $('.box-item').each(function() {
       // if ($(this).attr("itemid") === itemid) {
         $(this).addClass('insearch_section');
          $(this).clone().appendTo("#container2");      
       // }
      });


      //  $('.box-item').each(function() {
      // //  if ($(this).attr("itemid") === itemid) {
      //     $(this).appendTo("#container1");
      // //  }
      // });
   
}


  $(document).ready(function () { 

   $(".filter-data").find('*').prop('disabled', true);

      controll();

$("#container2").find('*').prop('disabled', true);

//$(".remove").css('display','none');
  });

</script>



<script type="text/javascript">
   $(document).ready(function () {     
     $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var ev = document.createEvent('Event');
    ev.initEvent('resize', true, true);
    window.dispatchEvent(ev);
  }); 
   });
</script>
