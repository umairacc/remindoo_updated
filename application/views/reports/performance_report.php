<div class="sample_section">
<button type="button" class="print_button btn btn-primary" id="print_report">Print</button>
<div class="export_option">
<select id="exported_result">
<option value="">Select</option>
<option value="pdf">Pdf</option>
<option value="excel">Excel</option>
<option value="csv">CSV</option>
</select>
</div>
</div>
	<table id="example" class="display nowrap" style="width:100%">
	  <thead>
	  <tr>
	<th> Task Name/lead Name  <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
	<div class="sortMask"></div>
	<select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
	<th> Staff Name  <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
	<div class="sortMask"></div>
	<select multiple="true" class="filter_check" id="" style="display: none;"> </select></th> 
	<th> Working Hours  <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
	<div class="sortMask"></div>
	<select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
	<!-- <th> Team  <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
	<div class="sortMask"></div>
	<select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
	<th> Department  <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
	<div class="sortMask"></div>
	<select multiple="true" class="filter_check" id="" style="display: none;"> </select></th> -->
	<th> Status  <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /> 
	<div class="sortMask"></div>
	<select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>

	 <!--  <th style="display: none;"> Task Name/lead Name </th>
	  <th style="display: none;"> Staff Name  </th> 
	  <th style="display: none;"> Working Hours  </th> -->
	 <!--  <th style="display: none;"> Team </th>
	  <th style="display: none;"> Department  </th> -->
	<!--   <th style="display: none;"> Status </th> -->
	  </tr>
	  </thead>
	<!--  <tfoot>
	  <tr>
	  <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
	  <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
	  <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
	  <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th> 
	  </tr>
	  </tfoot>   -->  
	  <tbody>


	<?php

	  foreach ($tasks_list as $key => $value) 
	  {  ?>
	    <tr id="<?php echo $value['id']; ?>">
	    <td>Task-<?php echo ucwords($value['subject']); ?></td>
	    <td><?php echo $value['assignees'];  ?></td>
	    <td><?php echo $value['hours_worked']; ?></td>
	    <td><?php echo $value['status']; ?></td>
	   <!--  <td style="display: none;"><?php echo ucwords($value['subject']); ?></td>
	    <td style="display: none;"><?php echo $value['staff_name']; ?></td>
	    <td style="display: none;"><?php echo $value['hours_worked']; ?></td>
	    <td style="display: none;"><?php echo $value['status']; ?></td> -->
	    </tr>
	<?php    
	    
	  }
	?>

	<?php
	foreach($leads as $record){ ?>

	  <tr id="<?php echo $record['id']; ?>">
	  <td>Lead-<?php echo $record['name']; ?></td>	  
	  <td><?php echo $record['assignees'];  ?></td>
	   <td>-</td>
	   <!-- <td style="display: none;"><?php echo implode(',',$teams1);?></td>
	    <td style="display: none;"><?php echo implode(',',$depts1);?></td> -->
	  <td><?php echo $this->Report_model->leads_status($record['lead_status']); ?></td>
	  <!-- <td><?php echo date('d-m-Y',$record['createdTime']); ?></td> --> 

	<!--   <td style="display: none;">Lead-<?php echo $record['name']; ?></td>
	  
	  <td style="display: none;"><?php  
	     $assignees = $this->Common_mdl->getAssignee('LEADS',$record['id']);
	     foreach ($assignees as $key1 => $value1) 
	     {
	        $crm_name[] = ucwords($value1['crm_name']);
	     }
	     echo implode(',', $crm_name);
	   ?></td>
	   <td style="display: none;">-</td> -->
	 <!--   <td style="display: none;"><?php echo implode(',',$teams1);?></td>
	    <td style="display: none;"><?php echo implode(',',$depts1);?></td> -->
	  <!-- <td style="display: none;"><?php echo $this->Report_model->leads_status($record['lead_status']); ?></td> -->
	  </tr>
	<?php } ?>

	  </tbody>

	  </table>