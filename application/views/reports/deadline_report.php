 <style type="text/css">

 @media table {
    tr {page-break-inside: avoid;}
}
</style>
 <table class="table client_table1 text-center display nowrap printableArea" id="alluser" cellspacing="0" cellpadding="5" width="100%" style="border:1px solid #ccc;border-collapse:collapse;">
<thead>
<tr class="text-uppercase" style="border:1px solid #ccc;">
<th style="border:1px solid #ccc;">Client Name</th>
<th style="border:1px solid #ccc;">Client Type</th>
<th style="border:1px solid #ccc;">Company Name</th>
<th style="border:1px solid #ccc;">Deadline Type</th>
<th style="border:1px solid #ccc;">Date</th>
</tr>
</thead>
 <tbody>                               
<?php 
$current_date=date("Y-m-d");
$s=1;
foreach ($getCompany_today as $getCompanykey => $getCompanyvalue) {  
$getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
?>
<?php
$status=$_SESSION['deadline_type'];
 if($status=='conf_statement'){
 if($getCompanyvalue['crm_confirmation_statement_due_date']!=''){ ?>
<tr>

<td style="border:1px solid #ccc;"><?php echo $getusername['crm_name'];?></td>
<td style="border:1px solid #ccc;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
<td style="border:1px solid #ccc;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
<td style="border:1px solid #ccc;">Confirmation statement Due Date</td>
<td style="border:1px solid #ccc;"><?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])): ''); ?></td><!-- confi-->
</tr>
<?php }} ?>
<?php
if($status=='accounts'){
 if($getCompanyvalue['crm_ch_accounts_next_due']!=''){ ?>
<tr style="border:1px solid #ccc;">

<td style="border:1px solid #ccc;"><?php echo $getusername['crm_name'];?></td>
<td style="border:1px solid #ccc;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
<td style="border:1px solid #ccc;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
<td style="border:1px solid #ccc;">Accounts Due Date</td>
<td style="border:1px solid #ccc;"><?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])): ''); ?></td>
<!-- account -->
</tr>
<?php }} ?>
<?php
if($status=='company_tax_return'){ 
if($getCompanyvalue['crm_accounts_tax_date_hmrc']!=''){ ?>
<tr class="for_table" style="border:1px solid #ccc;">

<td style="border:1px solid #ccc;"><?php echo $getusername['crm_name'];?></td>
<td style="border:1px solid #ccc;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
<td style="border:1px solid #ccc;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
<td style="border:1px solid #ccc;">Company Tax Return Due Date</td>
<td style="border:1px solid #ccc;"><?php echo (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])): ''); ?></td>
<!--company tax -->
</tr>
<?php }} ?>
<?php 
if($status=='personal_tax_return'){
if($getCompanyvalue['crm_personal_due_date_return']!=''){ ?>
<tr class="for_table" style="border:1px solid #ccc;">

<td style="border:1px solid #ccc;"><?php echo $getusername['crm_name'];?></td>
<td style="border:1px solid #ccc;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
<td style="border:1px solid #ccc;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
<td style="border:1px solid #ccc;">Personal Tax Return Due Date</td>
<td style="border:1px solid #ccc;"><?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])): ''); ?></td>
<!-- crm personal tax -->
</tr>
<?php }} ?>
<?php
if($status=='vat'){
 if($getCompanyvalue['crm_vat_due_date']!=''){ ?>
<tr class="for_table" style="border:1px solid #ccc;">

<td style="border:1px solid #ccc;"><?php echo $getusername['crm_name'];?></td>
<td style="border:1px solid #ccc;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
<td style="border:1px solid #ccc;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
<td style="border:1px solid #ccc;">VAT Due Date</td>
<td style="border:1px solid #ccc;"><?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])): ''); ?></td>
<!-- vat -->
</tr>
<?php }} ?>
<?php 
if($status=='payroll'){
if($getCompanyvalue['crm_rti_deadline']!=''){ ?>
<tr class="for_table" style="border:1px solid #ccc;">

<td style="border:1px solid #ccc;"><?php echo $getusername['crm_name'];?></td>
<td style="border:1px solid #ccc;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
<td style="border:1px solid #ccc;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
<td style="border:1px solid #ccc;">Payroll Due Date</td>
<td style="border:1px solid #ccc;"><?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])): ''); ?></td>
<!-- payrol -->
</tr>
<?php }} ?>
<?php 
if($status=='workplace'){
if($getCompanyvalue['crm_pension_subm_due_date']!=''){ ?>
<tr class="for_table" style="border:1px solid #ccc;">

<td style="border:1px solid #ccc;"><?php echo $getusername['crm_name'];?></td>
<td style="border:1px solid #ccc;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
<td style="border:1px solid #ccc;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
<td style="border:1px solid #ccc;">WorkPlace Pension - AE Due Date</td>
<td style="border:1px solid #ccc;"><?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])): ''); ?></td>
<!--workplace -->
</tr>
<?php }} ?>

<?php
if($status=='cis'){
?>
<tr class="for_table" style="border:1px solid #ccc;">

<td style="border:1px solid #ccc;"><?php echo $getusername['crm_name'];?></td>
<td style="border:1px solid #ccc;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
<td style="border:1px solid #ccc;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
<td style="border:1px solid #ccc;">CIS - Contractor Due Date</td>
<td style="border:1px solid #ccc;"></td>
<!-- CIS - Contractor -->
</tr>
   <?php } ?>
   <?php if($status=='cissub'){ ?>                                      
<tr class="for_table" style="border:1px solid #ccc;">

<td style="border:1px solid #ccc;"><?php echo $getusername['crm_name'];?></td>
<td style="border:1px solid #ccc;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
<td style="border:1px solid #ccc;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
<td style="border:1px solid #ccc;">CIS - Sub Contractor Due Date</td>
<td style="border:1px solid #ccc;"></td>
<!-- CIS - Sub Contractor-->
</tr>
<?php } ?>
<?php
if($status=='p11d'){ 
if($getCompanyvalue['crm_next_p11d_return_due']!=''){ ?>
<tr class="for_table" style="border:1px solid #ccc;">

<td style="border:1px solid #ccc;"><?php echo $getusername['crm_name'];?></td>
<td style="border:1px solid #ccc;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
<td style="border:1px solid #ccc;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
<td style="border:1px solid #ccc;">P11D Due Date</td>
<td style="border:1px solid #ccc;"><?php echo (strtotime($getCompanyvalue['crm_next_p11d_return_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])): ''); ?></td>
<!-- p11d -->
</tr>
<?php }} ?>
<?php if($status=='management'){
if($getCompanyvalue['crm_next_manage_acc_date']!=''){ ?>
<tr class="for_table" style="border:1px solid #ccc;">

<td style="border:1px solid #ccc;"><?php echo $getusername['crm_name'];?></td>
<td style="border:1px solid #ccc;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
<td style="border:1px solid #ccc;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
<td style="border:1px solid #ccc;">Management Accounts Due Date</td>
<td style="border:1px solid #ccc;"><?php echo (strtotime($getCompanyvalue['crm_next_manage_acc_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])): ''); ?></td>
<!-- manage account -->
</tr>
<?php }} ?>
<?php 
if($status=='bookkeep'){
if($getCompanyvalue['crm_next_booking_date']!=''){ ?>
<tr class="for_table" style="border:1px solid #ccc;">

<td style="border:1px solid #ccc;"><?php echo $getusername['crm_name'];?></td>
<td style="border:1px solid #ccc;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
<td style="border:1px solid #ccc;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
<td style="border:1px solid #ccc;">Bookkeeping Due Date</td>
<td style="border:1px solid #ccc;"><?php echo (strtotime($getCompanyvalue['crm_next_booking_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])): ''); ?></td>
<!-- booking-->
</tr>
<?php }} ?>
<?php
if($status=='investgate'){ 
 if($getCompanyvalue['crm_insurance_renew_date']!=''){ ?>
<tr class="for_table" style="border:1px solid #ccc;">

<td style="border:1px solid #ccc;"><?php echo $getusername['crm_name'];?></td>
<td style="border:1px solid #ccc;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
<td style="border:1px solid #ccc;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
<td style="border:1px solid #ccc;">Investigation Insurance Due Date</td>
<td style="border:1px solid #ccc;"><?php echo (strtotime($getCompanyvalue['crm_insurance_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])): ''); ?></td>
<!-- insurance -->
</tr>
<?php }} ?>
<?php if($status=='registered'){
 if($getCompanyvalue['crm_registered_renew_date']!=''){ ?>
<tr class="for_table" style="border:1px solid #ccc;">

<td style="border:1px solid #ccc;"><?php echo $getusername['crm_name'];?></td>
<td style="border:1px solid #ccc;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
<td style="border:1px solid #ccc;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
<td style="border:1px solid #ccc;">Registered Address Due Date</td>
<td style="border:1px solid #ccc;"><?php echo (strtotime($getCompanyvalue['crm_registered_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])): ''); ?></td>
<!-- registed -->
</tr>
<?php }} ?>
<?php if($status=='taxadvice'){
if($getCompanyvalue['crm_investigation_end_date']!=''){ ?>
<tr class="for_table" style="border:1px solid #ccc;">

<td style="border:1px solid #ccc;"><?php echo $getusername['crm_name'];?></td>
<td style="border:1px solid #ccc;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
<td style="border:1px solid #ccc;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
<td style="border:1px solid #ccc;">Tax Advice Due Date</td>
<td style="border:1px solid #ccc;"><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
<!-- tax advice-->
</tr>
<?php }} ?>
<?php if($status=='taxinvest'){ 
if($getCompanyvalue['crm_investigation_end_date']!=''){ ?>
<tr class="for_table" style="border:1px solid #ccc;">

<td style="border:1px solid #ccc;"><?php echo $getusername['crm_name'];?></td>
<td style="border:1px solid #ccc;"><?php echo $getCompanyvalue['crm_legal_form'];?></td>
<td style="border:1px solid #ccc;"><?php echo $getCompanyvalue['crm_company_name'];?></td>
<td style="border:1px solid #ccc;">Tax Investigation Due Date</td>
<td style="border:1px solid #ccc;"><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
<!-- tax inve-->
</tr>
<?php }} ?>
<?php $s++;  }  ?>
</tbody>
</table>

 <div id ="chart_img">
 <?php
 if(isset($_SESSION['deadline_image_sess'])){ ?>
 <img src="<?php echo $_SESSION['deadline_image_sess']; ?>" > 
 <?php } ?>
</div> 