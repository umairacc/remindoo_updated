<?php
// The function header by sending raw excel
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=deadline-list.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
 <table class="table client_table1 text-center display nowrap printableArea" id="alluser" cellspacing="0" cellpadding="5" width="100%" style="border:1px solid #ccc;border-collapse:collapse;">
<thead>
<tr class="text-uppercase" style="border:1px solid #ccc;">
<th>Client Name</th>
<th>Client Type</th>
<th>Company Name</th>
<th>Deadline Type</th>
<th>Date</th>
</tr>
</thead>
 <tbody>                               
<?php 
$current_date=date("Y-m-d");
$s=1;
foreach ($getCompany_today as $getCompanykey => $getCompanyvalue) {  
$getusername=$this->Common_mdl->select_record('user','id',$getCompanyvalue['user_id']);
?>
<?php if($getCompanyvalue['crm_confirmation_statement_due_date']!=''){ ?>
<tr>

<td><?php echo $getusername['crm_name'];?></td>
<td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
<td><?php echo $getCompanyvalue['crm_company_name'];?></td>
<td>Confirmation statement Due Date</td>
<td><?php echo (strtotime($getCompanyvalue['crm_confirmation_statement_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_confirmation_statement_due_date'])): ''); ?></td><!-- confi-->
</tr>
<?php } ?>
<?php if($getCompanyvalue['crm_ch_accounts_next_due']!=''){ ?>
<tr style="border:1px solid #ccc;">

<td><?php echo $getusername['crm_name'];?></td>
<td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
<td><?php echo $getCompanyvalue['crm_company_name'];?></td>
<td>Accounts Due Date</td>
<td><?php echo (strtotime($getCompanyvalue['crm_ch_accounts_next_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_ch_accounts_next_due'])): ''); ?></td>
<!-- account -->
</tr>
<?php } ?>
<?php if($getCompanyvalue['crm_accounts_tax_date_hmrc']!=''){ ?>
<tr class="for_table" style="border:1px solid #ccc;">

<td><?php echo $getusername['crm_name'];?></td>
<td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
<td><?php echo $getCompanyvalue['crm_company_name'];?></td>
<td>Company Tax Return Due Date</td>
<td><?php echo (strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_accounts_tax_date_hmrc'])): ''); ?></td>
<!--company tax -->
</tr>
<?php } ?>
<?php if($getCompanyvalue['crm_personal_due_date_return']!=''){ ?>
<tr class="for_table" style="border:1px solid #ccc;">

<td><?php echo $getusername['crm_name'];?></td>
<td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
<td><?php echo $getCompanyvalue['crm_company_name'];?></td>
<td>Personal Tax Return Due Date</td>
<td><?php echo (strtotime($getCompanyvalue['crm_personal_due_date_return']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_personal_due_date_return'])): ''); ?></td>
<!-- crm personal tax -->
</tr>
<?php } ?>
<?php if($getCompanyvalue['crm_vat_due_date']!=''){ ?>
<tr class="for_table" style="border:1px solid #ccc;">

<td><?php echo $getusername['crm_name'];?></td>
<td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
<td><?php echo $getCompanyvalue['crm_company_name'];?></td>
<td>VAT Due Date</td>
<td><?php echo (strtotime($getCompanyvalue['crm_vat_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_vat_due_date'])): ''); ?></td>
<!-- vat -->
</tr>
<?php } ?>
<?php if($getCompanyvalue['crm_rti_deadline']!=''){ ?>
<tr class="for_table" style="border:1px solid #ccc;">

<td><?php echo $getusername['crm_name'];?></td>
<td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
<td><?php echo $getCompanyvalue['crm_company_name'];?></td>
<td>Payroll Due Date</td>
<td><?php echo (strtotime($getCompanyvalue['crm_rti_deadline']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_rti_deadline'])): ''); ?></td>
<!-- payrol -->
</tr>
<?php } ?>
<?php if($getCompanyvalue['crm_pension_subm_due_date']!=''){ ?>
<tr class="for_table" style="border:1px solid #ccc;">

<td><?php echo $getusername['crm_name'];?></td>
<td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
<td><?php echo $getCompanyvalue['crm_company_name'];?></td>
<td>WorkPlace Pension - AE Due Date</td>
<td><?php echo (strtotime($getCompanyvalue['crm_pension_subm_due_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_pension_subm_due_date'])): ''); ?></td>
<!--workplace -->
</tr>
<?php } ?>

<tr class="for_table" style="border:1px solid #ccc;">

<td><?php echo $getusername['crm_name'];?></td>
<td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
<td><?php echo $getCompanyvalue['crm_company_name'];?></td>
<td>CIS - Contractor Due Date</td>
<td></td>
<!-- CIS - Contractor -->
</tr>
                                         
<tr class="for_table" style="border:1px solid #ccc;">

<td><?php echo $getusername['crm_name'];?></td>
<td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
<td><?php echo $getCompanyvalue['crm_company_name'];?></td>
<td>CIS - Sub Contractor Due Date</td>
<td></td>
<!-- CIS - Sub Contractor-->
</tr>
<?php if($getCompanyvalue['crm_next_p11d_return_due']!=''){ ?>
<tr class="for_table" style="border:1px solid #ccc;">

<td><?php echo $getusername['crm_name'];?></td>
<td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
<td><?php echo $getCompanyvalue['crm_company_name'];?></td>
<td>P11D Due Date</td>
<td><?php echo (strtotime($getCompanyvalue['crm_next_p11d_return_due']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_p11d_return_due'])): ''); ?></td>
<!-- p11d -->
</tr>
<?php } ?>
<?php if($getCompanyvalue['crm_next_manage_acc_date']!=''){ ?>
<tr class="for_table" style="border:1px solid #ccc;">

<td><?php echo $getusername['crm_name'];?></td>
<td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
<td><?php echo $getCompanyvalue['crm_company_name'];?></td>
<td>Management Accounts Due Date</td>
<td><?php echo (strtotime($getCompanyvalue['crm_next_manage_acc_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_manage_acc_date'])): ''); ?></td>
<!-- manage account -->
</tr>
<?php } ?>
<?php if($getCompanyvalue['crm_next_booking_date']!=''){ ?>
<tr class="for_table" style="border:1px solid #ccc;">

<td><?php echo $getusername['crm_name'];?></td>
<td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
<td><?php echo $getCompanyvalue['crm_company_name'];?></td>
<td>Bookkeeping Due Date</td>
<td><?php echo (strtotime($getCompanyvalue['crm_next_booking_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_next_booking_date'])): ''); ?></td>
<!-- booking-->
</tr>
<?php } ?>
<?php if($getCompanyvalue['crm_insurance_renew_date']!=''){ ?>
<tr class="for_table" style="border:1px solid #ccc;">

<td><?php echo $getusername['crm_name'];?></td>
<td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
<td><?php echo $getCompanyvalue['crm_company_name'];?></td>
<td>Investigation Insurance Due Date</td>
<td><?php echo (strtotime($getCompanyvalue['crm_insurance_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_insurance_renew_date'])): ''); ?></td>
<!-- insurance -->
</tr>
<?php } ?>
<?php if($getCompanyvalue['crm_registered_renew_date']!=''){ ?>
<tr class="for_table" style="border:1px solid #ccc;">

<td><?php echo $getusername['crm_name'];?></td>
<td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
<td><?php echo $getCompanyvalue['crm_company_name'];?></td>
<td>Registered Address Due Date</td>
<td><?php echo (strtotime($getCompanyvalue['crm_registered_renew_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_registered_renew_date'])): ''); ?></td>
<!-- registed -->
</tr>
<?php } ?>
<?php if($getCompanyvalue['crm_investigation_end_date']!=''){ ?>
<tr class="for_table" style="border:1px solid #ccc;">

<td><?php echo $getusername['crm_name'];?></td>
<td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
<td><?php echo $getCompanyvalue['crm_company_name'];?></td>
<td>Tax Advice Due Date</td>
<td><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
<!-- tax advice-->
</tr>
<?php } ?>
<?php if($getCompanyvalue['crm_investigation_end_date']!=''){ ?>
<tr class="for_table" style="border:1px solid #ccc;">

<td><?php echo $getusername['crm_name'];?></td>
<td><?php echo $getCompanyvalue['crm_legal_form'];?></td>
<td><?php echo $getCompanyvalue['crm_company_name'];?></td>
<td>Tax Investigation Due Date</td>
<td><?php echo (strtotime($getCompanyvalue['crm_investigation_end_date']) !='' ?  date('Y-m-d', strtotime($getCompanyvalue['crm_investigation_end_date'])): ''); ?></td>
<!-- tax inve-->
</tr>
<?php } ?>
<?php $s++;  }  ?>
</tbody>
</table>
