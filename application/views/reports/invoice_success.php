  <div id="piechart_invoice1" style="width: 100%;"></div>

   <script type="text/javascript">
     google.charts.load('current', {'packages':['corechart']});
     google.charts.setOnLoadCallback(drawChart2);
  
     function drawChart2() {  
       var data = google.visualization.arrayToDataTable([
         ['Service', 'link','Count'],             
          ['Approved','<?php echo base_url(); ?>invoice',<?php echo $approved_invoice['invoice_count']; ?> ],
          ['Cancelled','<?php echo base_url(); ?>invoice', <?php echo $cancel_invoice['invoice_count']; ?> ],
          ['Expired','<?php echo base_url(); ?>invoice', <?php echo $expired_invoice['invoice_count']; ?> ]               
        ]);      
       var view = new google.visualization.DataView(data);
         view.setColumns([0, 2]);

       var options = {
              chartArea: {left: 0, width: "100%"},
        height: 250,
       legend: { position: 'bottom', show: true, container: $("#large-legend") },
      hAxis : { textStyle : { fontSize: 8} },
      vAxis : { textStyle : { fontSize: 8} },
     // pieHole: 0.4,
       };
  
    var chart_div = new google.visualization.PieChart(document.getElementById('piechart_invoice1'));
    var chart = new google.visualization.PieChart(piechart_invoice1);
    google.visualization.events.addListener(chart, 'ready', function () {
    chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
    //document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '"></a>';
    var image=chart.getImageURI();
       $(document).ready(function(){  
      // alert(image);
                 $.ajax({
                        type: "POST",
                        url: "<?php echo base_url();?>Filter/invoice_chart_sess",         
                        data: {invoice_image_sess: image},
                        success: 
                             function(data){
                    
                             }
                         });
                  });
  });
    chart.draw(view, options);
    var selectHandler = function(e) {       
         window.open( data.getValue(chart.getSelection()[0]['row'], 1), '_blank');
         }      
         google.visualization.events.addListener(chart, 'select', selectHandler);
         }
 
  </script>