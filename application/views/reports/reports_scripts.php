<script type="text/javascript">	

	$("#search").click(function()
	{
		var date=$('#datewise_filter').val();
		//var client=$("#clientwise_filter").val();
		//var user=$("#userwise_filter").val();
		
		//var filter=$(".filter").attr('id');
		if(date!="")
		{
		    $('#er_date_pick').html("");	

		    var formData={'date':date}
			$.ajax(
			{
				type: "POST",
				url: "<?php echo base_url();?>Filter_section/date_wise_filter",         
				data: formData,
				beforeSend: function() {
				$(".LoadingImage").show();
				},
				success: function(data) 
				{ 
				  selectli();
				  $('#clientwise_filter').val('');
				  $(".LoadingImage").hide(); 
				  $("#append_content").html('');
				  $("#append_content").append(data);			  	 
				}
			});
		}
		else
		{
           $('#er_date_pick').html("Required").css("color","red");
		}	
	});
	$("#clientwise_filter").change(function()
	{
		var client=$(this).val();
		//var date=$("#datewise_filter").val();
		//var user=$("#userwise_filter").val();
		//var filter=$(".filter").attr('id');
		var formData={'client':client}
		$.ajax(
		{
			type: "POST",
			url: "<?php echo base_url();?>Filter_section/date_wise_filter",      
			data: formData,
			beforeSend: function() {
			$(".LoadingImage").show();
			},
			success: function(data) 
			{      
			  selectli();
			  $('#datewise_filter').val('');
			  $(".LoadingImage").hide(); 
			  $("#append_content").html('');
			  $("#append_content").append(data);		 
			}
		});

	});
	
	$("#all").click(function()
	{
		$('.fill').removeClass('filter');
		$(this).addClass('filter');
	//	var filter=$(this).attr('id');
		//var date=$("#datewise_filter").val();
		//var client=$("#clientwise_filter").val();
		//var user=$("#userwise_filter").val();
	//	var team=$("#teamwise_filter").val();
		//var formData={'client':client,'date':date}
		$.ajax(
		{
			type: "POST",
			url: "<?php echo base_url();?>Filter_section/all_filter",         
			//data: formData,
			beforeSend: function() {
			$(".LoadingImage").show();
			},
			success: function(data) 
			{      
			  selectli();
			  $('#datewise_filter').val('');
			  $("#clientwise_filter").val('');
			  $(".LoadingImage").hide(); 
			  $("#append_content").html('');
			  $("#append_content").append(data);
			}

		});
	});

	$("#week").click(function()
	{
		$('.fill').removeClass('filter');
		$(this).addClass('filter');
		/*var filter=$(this).attr('id');
		var date=$("#datewise_filter").val();
		var client=$("#clientwise_filter").val();
		var user=$("#userwise_filter").val();
		var team=$("#teamwise_filter").val();*/
			//var formData={'filter':filter,'client':client,'user':user,'team':team}
		$.ajax(
		{
			type: "POST",
			url: "<?php echo base_url();?>Filter_section/week_filter",         
			//data: formData,
			beforeSend: function() {
			$(".LoadingImage").show();
			},
			success: function(data)
			{      
			  selectli();
			  $('#datewise_filter').val('');
			  $("#clientwise_filter").val('');
			  $(".LoadingImage").hide(); 
			  $("#append_content").html('');
			  $("#append_content").append(data);
			}

		});
	});

	$("#three_week").click(function()
	{
		$('.fill').removeClass('filter');
		$(this).addClass('filter');
		/*var filter=$(this).attr('id');
		var date=$("#datewise_filter").val();
		var client=$("#clientwise_filter").val();
		var user=$("#userwise_filter").val();
		var team=$("#teamwise_filter").val();
		var formData={'filter':filter,'client':client,'user':user,'team':team}*/
		$.ajax(
		{
			type: "POST",
			url: "<?php echo base_url();?>Filter_section/threeweek_filter",         
		//	data: formData,
			beforeSend: function() {
			$(".LoadingImage").show();
			},
			success: function(data) 
			{      
			  selectli();
			  $('#datewise_filter').val('');
			  $("#clientwise_filter").val('');
			  $(".LoadingImage").hide(); 
			  $("#append_content").html('');
			  $("#append_content").append(data);
			}

		});
	});

	$("#month").click(function()
	{
		$('.fill').removeClass('filter');
		$(this).addClass('filter');
		/*var filter=$(this).attr('id');
		var date=$("#datewise_filter").val();
		var client=$("#clientwise_filter").val();
		var user=$("#userwise_filter").val();
		var team=$("#teamwise_filter").val();
		var formData={'filter':filter,'client':client,'user':user,'team':team}*/
		$.ajax(
		{
			type: "POST",
			url: "<?php echo base_url();?>Filter_section/month_filter",         
			//data: formData,
			beforeSend: function() {
			$(".LoadingImage").show();
			},
			success: function(data)
			{      
			  selectli();
			  $('#datewise_filter').val('');
			  $("#clientwise_filter").val('');
			  $(".LoadingImage").hide(); 
			  $("#append_content").html('');
			  $("#append_content").append(data);
			}
		});
	});

	$("#Three_month").click(function()
	{
		$('.fill').removeClass('filter');
		$(this).addClass('filter');
		/*var filter=$(this).attr('id');
		var date=$("#datewise_filter").val();
		var client=$("#clientwise_filter").val();
		var user=$("#userwise_filter").val();
		var team=$("#teamwise_filter").val();
		var formData={'filter':filter,'client':client,'user':user,'team':team}*/
		$.ajax(
		{
			type: "POST",
			url: "<?php echo base_url();?>Filter_section/threemonth_filter",         
			//data: formData,
			beforeSend: function() {
			$(".LoadingImage").show();
			},
			success: function(data) 
			{      
			  selectli();
			  $('#datewise_filter').val('');
			  $("#clientwise_filter").val('');
			  $(".LoadingImage").hide(); 
			  $("#append_content").html('');
			  $("#append_content").append(data);
			}
		});
	});

	function selectli()
	{
		  $('div .f4reports').children('ul').find('li').each(function()
		  {
		  	 var a = $(this).children('a');

		  	 if($(a).attr('data-target') == '#reports-tab')
		  	 {
		  	 	if(!$(a).hasClass('active'))
		  	 	{
			  	 	$(a).addClass('active');
			  	 	$(a).attr('aria-expanded','true');
			  	} 	
		  	 }
		  	 else
		  	 {
		  	 	$(a).removeClass('active');
		  	 	$(a).attr('aria-expanded','false');
		  	 }	
		  }); 
	}

/*	$("#userwise_filter").change(function(){
		var user=$(this).val();
		var date=$("#datewise_filter").val();
		var client=$("#clientwise_filter").val();
		var filter=$(".filter").attr('id');
		var team=$("#teamwise_filter").val();
		if(date!='0'){
			var url='<?php echo base_url(); ?>Filter_section/date_wise_filter';
		}else if(typeof filter!='undefined'){
			if(filter=='all'){
				var url='<?php echo base_url(); ?>Filter_section/all_filter';
			}
			if(filter=='week'){
				var url='<?php echo base_url(); ?>Filter_section/week_filter';
			}
			if(filter=='three_week'){
				var url='<?php echo base_url(); ?>Filter_section/threeweek_filter';
			}
			if(filter=='month'){
				var url='<?php echo base_url(); ?>Filter_section/month_filter';
			}
			if(filter=='Three_month'){
				var url='<?php echo base_url(); ?>Filter_section/threemonth_filter';
			}
		}else{	
		var filter='';		
			var url='<?php echo base_url(); ?>Filter_section/all_filter';
		}

		 var formData={'date':date,'client':client,'user':user,'filter':filter,'team':team}
			$.ajax({
			type: "POST",
			url: url,         
			data: formData,
			beforeSend: function() {
			$(".LoadingImage").show();
			},
			success: function(data) {      
			  $(".LoadingImage").hide(); 
			  $("#append_content").html('');
			  $("#append_content").append(data);		 
			}
			});

	});*/

</script>