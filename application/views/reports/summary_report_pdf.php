<table class="table client_table1 text-center display nowrap printableArea" id="example" cellspacing="0" width="100%"  cellpadding="10" style="border-collapse:collapse;border:1px solid #ddd;">
<thead>
<tr class="text-uppercase">
<th style="border:1px solid #ddd;">
S.no
</th>
<?php if(isset($column_setting[0]['profile_image'])&&($column_setting[0]['profile_image']==1)){?>
<th style="border:1px solid #ddd;">Profile</th>
<?php } ?>      
<th style="border:1px solid #ddd;">Name</th>
<th style="border:1px solid #ddd;">Updates</th>
<th style="border:1px solid #ddd;">Active</th>
<th style="border:1px solid #ddd;">CompanyStatus</th>
<th style="border:1px solid #ddd;">User Type</th>
<th style="border:1px solid #ddd;">Status</th>
</tr>
</thead>
 <tbody>
<?php 

if(count($client_details)>0){
$s = 1;
foreach ($client_details as $getallUserkey => $getallClientvalue) {
$update=$this->Common_mdl->select_record('update_client','user_id',$getallClientvalue['id']);
$company_status=$this->Common_mdl->select_record('client','user_id',$getallClientvalue['id']);
$getallUservalue=$this->Common_mdl->select_record('user','id',$getallClientvalue['id']);
if($getallUservalue['role']=='6'){
//$role = 'Staff';
$edit=base_url().'user/view_staff/'.$getallUservalue['id'];
$house='Manual';
}elseif($getallUservalue['role']=='4' && $company_status['status']==0 ){
//$role = 'Client';
$edit=base_url().'Client/client_info/'.$getallUservalue['id'];
$house='Manual';
}elseif($getallUservalue['role']=='4' && $company_status['status']==1){
//$role = 'Client';
$edit=base_url().'Client/client_info/'.$getallUservalue['id'];
$house='Company House';
}elseif($getallUservalue['role']=='4' && $company_status['status']==2){
//$role = 'Client';
$edit=base_url().'Client/client_info/'.$getallUservalue['id'];
$house='Import';
}else{
$house ='';
$role = '';
$edit='#';
}
if($getallUservalue['status']=='1'){
$status_id = 'checkboxid1';  
$chked = 'selected';
$status_val = '1';
$active='Active';
}elseif($getallUservalue['status']=='2' || $getallUservalue['status']=='0' ){
$status_id = 'checkboxid2';  
$chked = 'selected';
$status_val = '2';
$active='Inactive';
} elseif($getallUservalue['status']=='3'){
$status_id = 'checkboxid3';  
$chked = 'selected';
$status_val = '3';
$active='Frozen';
}
$role=$this->Common_mdl->select_record('Role','id',$getallUservalue['role']);
?>
<tr id="<?php echo $getallUservalue["id"]; ?>">
<td style="border:1px solid #ddd;"><?php echo $s;?></td>
<?php if(isset($column_setting[0]['profile_image'])&&($column_setting[0]['profile_image']==1)){?>
<td style="border:1px solid #ddd;" class="user_imgs"><img src="<?php echo base_url().'/uploads/'.$getallUservalue['crm_profile_pic'];?>" alt="img"></td>
<?php } ?>
<td style="border:1px solid #ddd;"><?php echo ucfirst($getallUservalue['crm_name']);?></td>                               
<td style="border:1px solid #ddd;">                                  
<?php echo ($update['counts']!='')? ($update['counts']):'0';?>  new
</td> <td>
<?php if($getallUservalue['status']=='1'){ echo "Active"; } elseif($getallUservalue['status']=='0'){ echo "Inactive"; }elseif($getallUservalue['status']=='3'){ echo "Frozen"; }else{ echo '-'; } ?>
</td>
<td style="border:1px solid #ddd;" id="innac"><?php echo ucfirst($company_status['crm_company_status']);?></td>
<td style="border:1px solid #ddd;"><?php echo $role['role'];?></td>
<td style="border:1px solid #ddd;"><?php echo $house;?></td>
</tr>
<?php $s++; }

}else{ ?>
<tr><td style="border:1px solid #ddd;">No records found</td></tr>
<?php } ?>
</tbody>
</table>

<div class="sample_section">
 <!--  <button type="button" class="print_button btn btn-primary" id="print_report">Print</button> -->
  <div class="export_option">
 <!--  <select id="exported_result">
  <option value="">Select</option>
  <option value="pdf">Pdf</option>
  <option value="excel">Excel</option>
  <option value="csv">CSV</option>
  </select> -->
  </div>
  </div>



<table class="table client_table1 text-center display nowrap" id="example1" cellspacing="0" width="100%"   cellpadding="10" style="border-collapse:collapse;border:1px solid #ddd;">
<thead>
<tr class="text-uppercase">
<th style="border:1px solid #ddd;">
S.No
</th>
<th style="border:1px solid #ddd;">Task Name</th>
<th style="border:1px solid #ddd;">Start Date</th>
<th style="border:1px solid #ddd;">Due Date</th>
<th style="border:1px solid #ddd;">Status</th>
<th style="border:1px solid #ddd;">Priority</th>

<th style="border:1px solid #ddd;">Assignto</th>
</tr>
</thead>
<tbody>
<?php 
if(count($task_details) > 0){
$t=1;
foreach ($task_details as $key => $value) {
error_reporting(0);
// $value['end_date'] = date('d-m-Y');
if($value['start_date']!='' && $value['end_date']!=''){    
$start  = date_create(implode('-', array_reverse(explode('-', $value['start_date']))));
$end  = date_create(implode('-', array_reverse(explode('-', $value['end_date']))));

//$end  = date_create(); // Current time and date
$diff    = date_diff ( $start, $end );

$y =  $diff->y;
$m =  $diff->m;
$d =  $diff->d;
$h =  $diff->h;
$min =  $diff->i;
$sec =  $diff->s;
}
else{
$y =  0;
$m =  0;
$d =  0;
$h =  0;
$min = 0;
$sec =  0;
}

$date_now = date("Y-m-d"); // this format is string comparable
$d_rec = implode('-', array_reverse(explode('-', $value['end_date'])));
if ($date_now > $d_rec) {
//echo 'priya';
$d_val = "EXPIRED";
}else{
$d_val = $y .' years '. $m .' months '. $d .' days '. $h .' Hours '. $min .' min '. $sec .' sec ';
}


if($value['worker']=='')
{
$value['worker'] = 0;
}
$staff=$this->db->query("SELECT * FROM staff_form WHERE user_id in (".$value['worker'].")")->result_array();
if($value['task_status']=='notstarted')
{
$percent = 0;
$stat = 'Not Started';
} if($value['task_status']=='inprogress')
{
$percent = 25;
$stat = 'In Progress';
} if($value['task_status']=='awaiting')
{
$percent = 50;
$stat = 'Awaiting for a feedback';
} if($value['task_status']=='testing')
{
$percent = 75;
$stat = 'Testing';
} if($value['task_status']=='complete')
{
$percent = 100;
$stat = 'Complete';
}
$exp_tag = explode(',', $value['tag']);
$explode_worker=explode(',',$value['worker']);
?>
<tr id="<?php echo $value['id']; ?>">

<td style="border:1px solid #ddd;">
<?php echo $t; ?>
</td>
<td style="border:1px solid #ddd;"> <?php echo ucfirst($value['subject']);?></td>
<td style="border:1px solid #ddd;"><?php echo $value['start_date'];?></td>
<td style="border:1px solid #ddd;"><?php echo $value['end_date'];?></td>
<td style="border:1px solid #ddd;"><?php echo $stat;?></td>
<td style="border:1px solid #ddd;">
<?php echo $value['priority'];?>
</td>
<td style="border:1px solid #ddd;" class="user_imgs" id="task_<?php echo $value['id'];?>">
<?php
$assignto=array();
foreach($explode_worker as $key => $val){     
$getUserProfilepic = $this->Common_mdl->getUserProfileName($val);
array_push($assignto,$getUserProfilepic);
} ?>
<?php echo implode(',',$assignto); ?>
</td>
</tr>
<?php $t++; }

}else{ ?>
<tr><td colspan="5">No records Found</td></tr>
<?php } ?>
</tbody>
</table>
<div class="sample_section">
  <!-- <button type="button" class="print_button btn btn-primary" id="print_report">Print</button> -->
  <div class="export_option">
 <!--  <select id="exported_result">
  <option value="">Select</option>
  <option value="pdf">Pdf</option>
  <option value="excel">Excel</option>
  <option value="csv">CSV</option>
  </select> -->
  </div>
  </div>
<table class="table client_table1 text-center display nowrap" id="example2" cellspacing="0" width="100%"   cellpadding="10" style="border-collapse:collapse;border:1px solid #ddd;">
<thead>
<tr class="text-uppercase">
<th style="border:1px solid #ddd;">S.No</th>
<th style="border:1px solid #ddd;">Lead Name</th>
<th style="border:1px solid #ddd;">Company Name</th>
<th style="border:1px solid #ddd;">Email</th>
<th style="border:1px solid #ddd;">Assigned</th>
<th style="border:1px solid #ddd;">Status</th>
</tr>
</thead>
<tbody>
<?php 
if(count($lead_details)){
$t=1;
foreach ($lead_details as $key => $value) { 


$getUserProfilepic=array();
$ex_assign=explode(',',$value['assigned']);
foreach ($ex_assign as $ex_key => $ex_value) {
$getUserProfilepic[] = $this->Common_mdl->getUserProfileName($ex_value);
}

 ?>                             
<tr id="<?php echo $value['id']; ?>">
<td style="border:1px solid #ddd;">
<?php echo $t; ?>
</td>
<td style="border:1px solid #ddd;"> <?php echo ucfirst($value['name']);?></td>
<td style="border:1px solid #ddd;"><?php 
//echo $value['company'];
if(is_numeric($value['company']))
{
$client_query=$this->db->query("SELECT * FROM client WHERE autosave_status=0 and id='".$value['company']."' order by id desc ")->result_array();
if(count($client_query)>0)
{
echo $client_query[0]['crm_company_name'];
}
else
{
echo '-';
}
}
else
{
echo $value['company'];
}

?></td>
<td style="border:1px solid #ddd;"><?php echo $value['email_address'];?></td>
<td style="border:1px solid #ddd;"> <?php echo implode(',',$getUserProfilepic);    ?></td>
<td style="border:1px solid #ddd;">
<?php //echo $value['created_at'];
echo $this->Report_model->lead_status($value['lead_status']);
?>
</td>
</tr>
<?php $t++; }
}else{ ?>
<tr><td style="border:1px solid #ddd;" colspan="5">No records Found</td></tr>
  <?php } ?>
</tbody>
</table>