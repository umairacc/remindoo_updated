<div id="pie_chart1" style="width: 100%;"></div>



                                                  
<script type="text/javascript">
 google.charts.load("current", {packages:["corechart"]});
  google.charts.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['Task', 'Count'],
      <?php 
      $status=$_SESSION['task_status'];
      if($status=='notstarted'){ ?>
      ['Not started',     <?php echo $notstarted_tasks['task_count']; ?>],
      ['Others',    <?php echo $others['task_count']; ?>],
      <?php }  if($status=='complete'){ ?>
      ['Completed',      <?php echo $completed_tasks['task_count']; ?>],
         ['Others',    <?php echo $others['task_count']; ?>],
      <?php } if($status=='inprogress'){ ?>
      ['Inprogress',  <?php echo $inprogress_tasks['task_count']; ?>],
       ['Others',    <?php echo $others['task_count']; ?>],
      <?php }   if($status=='awaiting'){ ?>
      ['Awaiting Feedback', <?php echo $awaiting_tasks['task_count']; ?>],
       ['Others',    <?php echo $others['task_count']; ?>],
      <?php }  if($status=='testing'){?>
      ['Testing',    <?php echo $testing_tasks['task_count']; ?>],
      ['Others',    <?php echo $others['task_count']; ?>],
      <?php } ?>
    
    ]);  
    var options = {
          chartArea: {left: 0, width: "100%"},
        height: 250,
       legend: { position: 'bottom', show: true, container: $("#large-legend") },
      hAxis : { textStyle : { fontSize: 8} },
      vAxis : { textStyle : { fontSize: 8} },
     // pieHole: 0.4,
    };  
    var chart_div = new google.visualization.PieChart(document.getElementById('pie_chart1'));
    var chart = new google.visualization.PieChart(pie_chart1);
    google.visualization.events.addListener(chart, 'ready', function () {
    chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
    //document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '"></a>';
    var image=chart.getImageURI();
       $(document).ready(function(){   
                 $.ajax({
                        type: "POST",
                        url: "<?php echo base_url();?>/Reports/task_chart_sess",         
                        data: {chart_image_sess: image},
                        success: 
                             function(data){
                    
                             }
                         });
                  });
  });
    chart.draw(data, options);
  }
</script>