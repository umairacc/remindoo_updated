<?php $this->load->view('includes/header');?>
<style>
   /* bootstrap hack: fix content width inside hidden tabs */
.tab-content > .tab-pane,
.pill-content > .pill-pane {
display: block;     /* undo display:none          */
height: 0;          /* height:0 is also invisible */ 
overflow-y: hidden; /* no-overflow                */
}
.tab-content > .active,
.pill-content > .active {
height: auto;       /* let the content decide it  */
} /* bootstrap hack end */
.box-container {
   height: 200px;
}
 tfoot {
    display: table-header-group;
}
#container1.box-item{
   width: 100%; 
   /*font-size:10px;*/
   z-index: 1000
}

.box-item {
   /*width: 100%; */
   font-size:10px;
   z-index: 1000
}


   .filter{
      color:#F508C6;
   }

      .dropdown-content {
    display: none;
    position: absolute;
    background-color: #fff;
    min-width: 86px;
    overflow: auto;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    left: -92px;
    width: 150px;
}
.dt-buttons{
      display: none;
   }
</style>
<div class="modal fade" id="cus_msg" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" style="padding: 0px -10px 0px 0px;">&times;</button>
      <div class="custom_msg">             
             <div class="pop-realted1">
             <div class="position-alert1" style="text-align: center;"></div>
             </div>
      </div>      
   </div>     
</div>
</div>
</div>
<button id="targetcus_msg" data-toggle="modal" data-target="#cus_msg" style="display: none;"></button>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/mintScrollbar.css">
<div class="pcoded-content card-removes">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card">
                        <!-- admin start-->
                        <div class="client_section user-dashboard-section1 floating_set">
                           <!-- all-clients -->
                           <div class="all_user-section floating_set">
                              <div class="deadline-crm1 floating_set f4reports rdashboard">
                                 <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                                    <li class="nav-item">
                                       <a class="nav-link active" data-toggle="tab" data-target="#reports-tab" href="">Reports</a>
                                       <div class="slide"></div>
                                    </li>
                                 </ul>
                              <?php   $this->load->view('reports/report_customization_filters'); ?>
                              <div class="for-reportdash">
                                 <div class="left-one">
                                    <div class="pcoded-search">
                                       <span class="searchbar-toggle"> </span>
                                       <div class="pcoded-search-box ">
                                          <input type="text" placeholder="Search" class="search-criteria" id="search-criteria">
                                          <span class="search-icon"><i class="ti-search" aria-hidden="true"></i></span>
                                       </div>
                                          <div id="container1" class="panel-body box-container">
                                          <?php
                                          foreach($leads_status as $lead){?>
                                          <div itemid="<?php echo $lead['id']; ?>" class="btn btn-default box-item box_section leads-status"><?php echo $lead['status_name']; ?><span class="remove" data-role="remove">x</span></div>
                                             <?php } ?>                                             
                                          </div>
                                    </div>                                           
                                 </div>
                                 <div class="right-one">
                                    <div class="filter-data">
                                          <div class="filter-head"><h4>DROP FILTERS HERE
                                          </h4><button class="btn btn-danger f-right" id="clear_container">clear</button>
                                           <div id="container2" class="panel-body box-container">
                                         </div>
                                          </div>                                   
                                        
                                    </div>
                                    

                                    <div id="datatable_results" style="display: none;">
                                     

                                    </div>




                                 </div>
                              </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<?php //$this->load->view('reports/reports_scripts');?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/custom-google-chart.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.mintScrollbar.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.mousewheel.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script> -->
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<?php $this->load->view('reports/custom_scripts');?>