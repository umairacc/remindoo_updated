<table class="table client_table1 text-center display nowrap" id="alltask" cellspacing="0" width="100%" cellpadding="10" style="border-collapse:collapse;border:1px solid #ddd;">
<thead>
<tr class="text-uppercase">
<th style="border:1px solid #ddd;">S.No</th>
<th style="border:1px solid #ddd;">Proposal No</th>
<th style="border:1px solid #ddd;">Proposal Name</th>
<th style="border:1px solid #ddd;">Proposal Amount</th>
<th style="border:1px solid #ddd;">Status</th>
<th style="border:1px solid #ddd;">Sent on</th>
</tr>
</thead>
<tbody>
<?php 
$t=1;
foreach ($proposal_list as $key => $value) { ?>
<tr id="<?php echo $value['id']; ?>">
<td style="border:1px solid #ddd;">
<?php echo $t; ?>
</td>
<td style="border:1px solid #ddd;"> <?php echo ucfirst($value['proposal_no']);?></td>
<td style="border:1px solid #ddd;"><?php echo ucfirst($value['proposal_name']);?></td>
<td style="border:1px solid #ddd;"><?php echo $value['grand_total'];?></td>
<td style="border:1px solid #ddd;"><?php echo $value['status']; ?></td>
<td style="border:1px solid #ddd;">
<?php //echo $value['created_at'];
$timestamp = strtotime($value['created_at']);
$newDate = date('d F Y H:i', $timestamp); 
echo $newDate; //outputs 02-March-2011
?>
</td>
</tr>
<?php $t++; } ?>
</tbody>
</table>
<div id ="chart_img">
 <?php
 if(isset($_SESSION['proposal_image'])){ ?>
 <img src="<?php echo $_SESSION['proposal_image']; ?>" > 
 <?php } ?>
</div> 
