<?php $this->load->view('includes/client_header'); ?>
<link href="<?php echo base_url(); ?>assets/css/jquery.filer.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/colreorder/1.5.1/css/colReorder.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/buttons.dataTables.min.css?ver=2">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/dataTables.css">

<div class="pcoded-content pcodedteam-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
               <div class="col-sm-12">  
               	<div class="deadline-crm1 floating_set">  
               	  <ul class="nav nav-tabs all_user1 md-tabs floating_set u-dashboard">
               	      <li class="nav-item borbottomcls">
               	        <a class="nav-link"  href="javascript:void(0)">Invoices</a>
               	        <div class="slide"></div>
               	      </li>
               	  </ul>
               	 </div>

            <!-- Main-body start -->
             <div class="all-usera1 data-padding1 ">
             <table id="display_service1" class="display nowrap" style="width:100%">
                <thead>
                    <tr>                        
                    <th>S No<div class="sortMask"></div></th> 
                    <th>Invoice Number <div class="sortMask"></div></th>        
                    <th>Date<div class="sortMask"></div> </th>    
                    <th>Bill Amount<div class="sortMask"></div> </th> 
                    <th>Due Date<div class="sortMask"></div></th>
                    <th>Payment Status<div class="sortMask"></div></th>
                   </tr>
                 </thead>       
                 <tbody class="lead-annual">
                  <?php 
                    if($invoice)
                    {  
                    	$s = 0;
                      foreach ($invoice as $value) { ?>

                      <tr class="client_info" id="client_info">  
                       <td><?php echo ++$s; ?></td>                       
                       <td><?php echo $value['invoice_no'];?></td>                     
                       <td><?php echo date("M, d, Y", $value['invoice_date']);?></td> 
                       <td><?php echo $value['grand_total'].' '.$firm_currency;?></td> 
                       <td><?php if($value['invoice_duedate'] == 'Expired') {
                          echo $value['invoice_duedate'];
                        } else {
                          echo date("M, d, Y", $value['invoice_duedate']);  
                        } ?></td>                       
                        <td><?php echo ucfirst($value['payment_status']);?></td>
                        <?php } ?>                        
                      </tr>   
             <?php  }  

                  if($repeatinvoice)
                  {  
                    foreach ($repeatinvoice as $key => $repeatVal) { ?>
                    <tr class="client_info1" id="client_info1">  
                       <td><?php echo ++$s; ?></td> 
                       <td><?php echo $repeatVal['reference'];?></td>
                       <td><?php if(empty($repeatVal['updated_at'])){ echo date("M, d, Y", $repeatVal['created_at']); }else{ echo date("M, d, Y", $repeatVal['updated_at']); } ?></td>
                       <td><?php echo $repeatVal['grand_total'].' '.$firm_currency; ?></td>                 
                       <td>-</td>
                      <td><?php echo ucfirst($repeatVal['approve']);?></td>                     
                    </tr>   
         <?php  } }  ?>  
                </tbody>
             </table>
          </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>
</div>

<?php $this->load->view('includes/client_footer'); ?>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url()?>assets/js/custom/buttons.colVis.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>
<script src="<?php echo base_url()?>assets/js/custom/datatable_extension.js"></script>
<script type="text/javascript">

  var display_service1;

  $(document).ready(function()
  {   
  	  display_service1 = $('#display_service1').DataTable();  
  });  

</script>