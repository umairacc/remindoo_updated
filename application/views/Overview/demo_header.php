<!DOCTYPE html>
<?php $uri = $this->uri->segment('2');
$isBasicTheme = ( isset( $theme ) && $theme == 'basic' ) ? true : false;
$this->Common_mdl->role_identify();
$Company_types = array();
// $this->load->helper(['comman']);
$assigned_client = Get_Assigned_Datas('CLIENT');

if(count($assigned_client)>0)
{   
    foreach ($assigned_client as $key => $value) 
    {
        $rec = $this->Invoice_model->selectRecord('client','id', $value);
        
        if(!empty($rec['user_id']))
        {
          $arr[] = $rec['user_id'];
        }
    }

    $clients_user_ids = implode(',', $arr);
}    

?>
<html lang="en">
   <head>
      <title>CRM </title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="description" content="#">
      <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
      <meta name="author" content="#">
      <link rel="icon" href="<?php echo base_url();?>assets/images/favicon.ico" type="image/x-icon"> 
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>bower_components/bootstrap/css/bootstrap.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/icon/themify-icons/themify-icons.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/common_style.css?ver=4">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/common_responsive.css?ver=2">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/ui_style.css?ver=3">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/pages/menu-search/css/component.css">      
      <?php if( !$isBasicTheme ) { ?>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/pages/j-pro/css/demo.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/pages/j-pro/css/font-awesome.min.css">          
        <link type="text/css" href="<?php echo base_url();?>bower_components/bootstrap-timepicker/css/bootstrap-timepicker.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap-datetimepicker.css">         
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">    
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery.mCustomScrollbar.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>bower_components/owl.carousel/css/owl.carousel.css">    
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>/assets/css/dataTables.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>/assets/css/jquery.dropdown.css">
        <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">    
        <link href="<?php echo base_url(); ?>css/jquery.signaturepad.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>bower_components/bootstrap-daterangepicker/css/daterangepicker.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>bower_components/datedropper/css/datedropper.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>bower_components/switchery/css/switchery.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-colorpicker.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-slider.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/medium-editor/medium-editor.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/medium-editor/template.min.css">   
      <?php } ?>
        <link rel="stylesheet" type="text/css" href="<?php echo  base_url()?>/assets/icon/simple-line-icons/css/simple-line-icons.css">
        <link rel="stylesheet" type="text/css" href="<?php echo  base_url()?>/assets/icon/icofont/css/icofont.css">   
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/mobile_responsive.css?ver=2"> 
<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css">   -->
      <style>
         .common_form_section1  .col-xs-12.col-sm-6.col-md-6 {
         float: left !important;
         }
         .common_form_section1 {
         background: #fff;
         box-shadow: 1px 2px 5px #dbdbdb;
         border-radius: 5px;
         padding: 30px;
         }
         .common_form_section1 label{
         display: block;
         margin-bottom: 7px;
         font-size: 15px;
         }
         .common_form_section1 input{
         width: 100%;
         }
         .col-sm-12{
         float: left;
         }
         .col-sm-8 {
         float: left;
         }
      </style>
      <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/custom/opt-profile.css?<?php echo time(); ?>">
      <script type="text/javascript"> 
      var base_url = "<?php echo base_url()?>";
      </script>
   </head>
  <!--  <div class="LoadingImage">
   <div class="preloader3 loader-block">
      <div class="circ1 loader-primary loader-md"></div>
      <div class="circ2 loader-primary loader-md"></div>
      <div class="circ3 loader-primary loader-md"></div>
      <div class="circ4 loader-primary loader-md"></div>
   </div>
   </div> -->
   <div id="action_result" class="modal-alertsuccess alert succ dashboard_success_message" style="display:none;">
      <div class="newupdate_alert">
          <a href="#" class="close" id="close_action_result">×</a>
            <div class="pop-realted1">
                <div class="position-alert1">
                    Success !!! Staff Assign have been changed successfully...
                </div>
            </div>
      </div>
</div>

   <div id="action_result" class="modal-alertsuccess alert succ proposal_success_message" style="display:none;">
   <div class="newupdate_alert">
         <a href="#" class="close" id="close_action_result">×</a>
         <div class="pop-realted1">
         <div class="position-alert1">
              Proposal Send Successfully!!!
         </div>
         </div>
         </div>
</div>
   <style>
      .LoadingImage {
      display : none;
      position : fixed;
      z-index: 100;
      /*background-image : url('<?php echo site_url()?>assets/images/ajax-loader.gif');*/
      background-color:rgba(65, 56, 57, 0.4);
      opacity : 1;
      background-repeat : no-repeat;
      background-position : center;
      left : 0;
      bottom : 0;
      right : 0;
      top : 0;
      }
   </style>
   <body>
      <?php 
         $uri_seg_menu = $this->uri->segment('1'); 
         $uri_seg_menu1 = $this->uri->segment('2'); 
         $uri_seg_menu2 = $this->uri->segment('3');         
         $active_class1='';
         $active_class2='';
         $active_class3='';
         $active_class4='';
         $active_class5='';
         $active_class6='';
         $active_class7='';
         $active_class8='';
         $active_class9='';
         $active_class10='';
         $active_class11='';
         $active_class12='';
         $active_class13='';
         $active_class14='';
         $active_class15='';         
         $active_class20 ='';
         $active_class31='';
         $active_class32='';
         $active_class33='';
         $active_class34='';       
         $active_class35='';
         $active_class36='';         
         $client_list ='';
         $import_csv ='';
         $addnew ='';
         $addnew_cmp ='';
         $timeline='';         
         if($uri_seg_menu=='document')
         {
          $active_class9="head-active";
         }
         if($uri_seg_menu == 'timeline')
         {
          $active_class5 = "head-active";
          $timeline = "head-active";
         }         
          if($uri_seg_menu1 == 'column_setting_new')
          {
            $active_class12 = "head-active";
            $active_class8 = "head-active";
          }
          if($uri_seg_menu1 == 'team_management'){
             $active_class13 = "head-active";
          }
          if($uri_seg_menu1 == 'staff_custom_form')
          {
            $active_class14 = "head-active";
            $active_class8 = "head-active";         
          }
          if($uri_seg_menu1 == 'user_profile')
          {
            $active_class15 = "head-active";
            $active_class8 = "head-active";
          }
          if($uri_seg_menu1 == 'lead_settings')
          {
            $active_class35 = "head-active";
            $active_class8 = "head-active";
          }
          if($uri_seg_menu1 == 'new_task') {
          $active_class20 = "head-active";
          $active_class2 = "head-active";
         }
          if($uri_seg_menu1 == 'task_list') 
         {
           $active_class15 = "head-active";
          $active_class2 = "head-active";
         }
         /** for chat **/
         if($uri_seg_menu == 'user_chat')
         {
             $active_class36 = "head-active";
             $active_class8 = "head-active";
         }
         /** end of chat **/         
         /** rspt 19-04-2018 leads heading **/
         if($uri_seg_menu=='leads' || $uri_seg_menu=='web_to_lead')
         {
         // $active_class3 = "head-active";
           if(($uri_seg_menu1=='' || $uri_seg_menu1 == 'kanban') && $uri_seg_menu == 'leads')
           {
             $active_class31 = "head-active";
             $active_class3 = "head-active";
           }
           if($uri_seg_menu1 == 'web_leads_view')
           {
              $active_class32 = "head-active";
              $active_class33 = "head-active";
              $active_class3 = "head-active";
           }
           if($uri_seg_menu=='web_to_lead' && $uri_seg_menu1=='')
           {
             $active_class32 = "head-active";
             $active_class34 = "head-active";
             $active_class3 = "head-active";
           }
           if($uri_seg_menu1 == 'web_leads_update_contents')
           {
             $active_class32 = "head-active";
             $active_class3 = "head-active";
           }
           if($uri_seg_menu1 == 'lead_settings')
           {
           //  $active_class35 = "head-active";
           }
           if($uri_seg_menu1 == 'add_status' || $uri_seg_menu1 == 'add_source' || $uri_seg_menu1 == 'update_status' || $uri_seg_menu1 == 'update_source')
           {
             // $active_class35 = "head-active";
           }         
         }
         /** for proposal **/         
         if($uri_seg_menu=='proposal')
         {
           $active_class4 = "head-active";
         }         
         /** end 19-04-2018 **/
         //echo $active_class2;die; 
         if($uri_seg_menu == 'user' && $uri_seg_menu1 == ''){
            $active_class5 = "head-active";
            $client_list = "head-active";
         }
         if($uri_seg_menu1=='addnewclient')
         {
            $active_class5 = "head-active";
            $addnew = "head-active";
         }if($uri_seg_menu1=='import_csv')
         {
           $active_class5 = "head-active";
           $import_csv = "head-active";
         }
         if($uri_seg_menu2!='' && $uri_seg_menu1)
         {
          if($uri_seg_menu=='user'){
             $active_class5 = "head-active";
             $addnew_cmp = "head-active";
          }            
         }
          /*if($uri_seg_menu == 'client' || $uri_seg_menu == 'Client'){
          $active_class5 = "head-active";
         } if($uri_seg_menu == 'user' && $uri_seg_menu1 == 'import_csv'){
          $active_class5 = "head-active";
         }*/ 
         if($uri_seg_menu1 == 'admin_setting' || $uri_seg_menu1 == 'firm_field' || $uri_seg_menu1 == 'column_setting' || $uri_seg_menu1 == 'custom_fields'){
             $active_class8 = "head-active";
            if($uri_seg_menu1 == 'admin_setting')
                  $active_class10 = "head-active";
            if($uri_seg_menu1 == 'firm_field')
                  $active_class11 = "head-active";
         }
          if($uri_seg_menu1 == 'team_management' || $uri_seg_menu1 == 'staff_list' || $uri_seg_menu == 'department' || $uri_seg_menu == 'team' ){
             $active_class8 = "head-active";
         } if($uri_seg_menu == 'staff' && $uri_seg_menu1 == ''){
             $active_class8 = "head-active";
         }
         if($uri_seg_menu == 'staff' && $uri_seg_menu1 == 'update_staff_details'){
               $active_class1 = "head-active";
         }
           // 31/05/2018 add active class pradeep
           // My disk
          if($uri_seg_menu =='Firm_dashboard')
          {
            $active_class1 = "head-active";
          }
          if($uri_seg_menu =='staff' && $uri_seg_menu1 == 'Dashboard')
          {
            $active_class1 = "head-active";
          }
          // Tasks
           if(($uri_seg_menu1=='new_task' || $uri_seg_menu1 == 'task_list') && $uri_seg_menu == 'user')
           {
             $active_class2 = "head-active";
             
           }
           // CRM
           if($uri_seg_menu =='leads' || $uri_seg_menu == 'web_to_lead')
          {
            $active_class3 = "head-active";
          }
          if($uri_seg_menu =='web_to_lead' && $uri_seg_menu1 == 'web_leads_view')
          {
            $active_class3 = "head-active";
          }
          // proposal
          if($uri_seg_menu =='proposal')
          {
            $active_class4 = "head-active";
          }
          // clients
          if($uri_seg_menu =='user' || $uri_seg_menu == 'user#' || $uri_seg_menu == 'timeline')
          {
            $active_class5 = "head-active";
          }
          if($uri_seg_menu =='client' && $uri_seg_menu1 == 'addnewclient')
          {
            $active_class5 = "head-active";
          }
          if($uri_seg_menu =='user' && $uri_seg_menu1 == 'import_csv')
          {
            $active_class5 = "head-active";
          }
          //Deadline Manager
          if($uri_seg_menu =='Deadline_manager')
          {
            $active_class6 = "head-active";
          }
          //Reports
          if($uri_seg_menu =='reports')
          {
            $active_class7 = "head-active";
          }
          //settings
          if($uri_seg_menu =='user' && $uri_seg_menu1 == 'admin_setting')
          {
            $active_class8 = "head-active";
          }
          if($uri_seg_menu =='staff' && $uri_seg_menu1 == 'staff_custom_form')
          {
            $active_class8 = "head-active";
          }
          if($uri_seg_menu =='Tickets')
          {
            $active_class8 = "head-active";
          }
          if($uri_seg_menu =='leads' && $uri_seg_menu1 == 'lead_settings')
          {
            $active_class8 = "head-active";
          }
          if($uri_seg_menu =='user_chat')
          {
            $active_class8 = "head-active";
          }
          if($uri_seg_menu =='user' && $uri_seg_menu1 == 'firm_field')
          {
            $active_class8 = "head-active";
          }
          if($uri_seg_menu =='user' && $uri_seg_menu1 == 'column_setting_new')
          {
            $active_class8 = "head-active";
          }
          if($uri_seg_menu =='team' && $uri_seg_menu1 == 'team_management')
          {
            $active_class8 = "head-active";
          }
          // Documents
          if($uri_seg_menu =='documents')
          {
            $active_class9 = "head-active";
          }
          //Invoice
          if($uri_seg_menu =='Invoice')
          {
            $active_class10 = "head-active";
          }
         ?>

                   <?php ?>
    <?php  ?>
      <!-- Pre-loader start -->
      <div class="theme-loader">
         <div class="ball-scale">
            <div class='contain'>
               <div class="ring">
                  <div class="frame"></div>
               </div>
               <div class="ring">
                  <div class="frame"></div>
               </div>
               <div class="ring">
                  <div class="frame"></div>
               </div>
               <div class="ring">
                  <div class="frame"></div>
               </div>
               <div class="ring">
                  <div class="frame"></div>
               </div>
               <div class="ring">
                  <div class="frame"></div>
               </div>
               <div class="ring">
                  <div class="frame"></div>
               </div>
               <div class="ring">
                  <div class="frame"></div>
               </div>
               <div class="ring">
                  <div class="frame"></div>
               </div>
               <div class="ring">
                  <div class="frame"></div>
               </div>
            </div>
         </div>
      </div>

      <?php //} ?>
      <!-- Pre-loader end -->
      <div id="pcoded" class="pcoded" >
      <div class="pcoded-overlay-box"></div>
      <div class="pcoded-container navbar-wrapper">
      <nav class="navbar header-navbar pcoded-header">
         <div class="navbar-wrapper">
            <div class="navbar-logo">
				<a class="mobile-menu" id="mobile-collapse" href="javascript:void(0);">
					<i class="ti-menu"></i>
				</a>
               <a href="<?php echo base_url();?>">
               <img class="img-fluid" src="<?php echo base_url();?>assets/images/color_logo.png" alt="Theme-Logo" />
               </a>
               <a class="mobile-options">
               <i class="ti-more"></i>
               </a>
            </div>
            <div class="navbar-container container-fluid">
               <div class="nav-left ">
                   <ul class='header-dynamic-cnt dynamic_header' id="owl-demo1">
                  <?php

                    $PARENT_LI = "<li class='dropdown comclass'><a href='{URL}' class='dropdown-toggle'> <img src='{ICON_PATH}' class='menicon'>{MENU_LABEL} </a> {CHILD_CONTENT} </li>";

                    $CHILD_UL = "<ul class='dropdown-menu1'>{CHILD_CONTENT}</ul>";

                    $CHILD_LI ="<li class='column-setting0 atleta'><a href='{URL}'>{MENU_LABEL}</a> {CHILD_CONTENT}  </li>";

                    echo $this->Demo_model->getHeader( $PARENT_LI , $CHILD_UL , $CHILD_LI );

                  ?>
                  </ul>
               </div>
               <ul class="nav-right">
                  <?php                      
                                   
                      $query3 = $this->Common_mdl->section_menu('deadline_notification',$_SESSION['id']); 

                      if($query3=='0')
                      {    
                        $Deadline_notification = $this->Common_mdl->ReceiveNotification('Deadline_Manager',$_SESSION['id']);
                        $deadline_not_count = count($Deadline_notification);
                      }
                      else
                      {
                        $deadline_not_count = 0;
                      }

                      $query=$this->Common_mdl->section_menu('client_notification',$_SESSION['id']);  
                        if($query=='0'){ 
                             $noti=$this->db->query("select * from user where autosave_status='1' order by id desc")->result_array();
                             $count1=count($noti);
                        }else{
                             $count1=0;
                        }

                      $query1=$this->Common_mdl->section_menu('invoice_notification',$_SESSION['id']);  
                      if($query1=='0' && !empty($clients_user_ids)){
                        $invoice_noti = $this->db->query('SELECT * from Invoice_details where send_statements="send" AND client_email IN ('.$clients_user_ids.') and views=0')->result_array(); 
                        $repeatinvoice_noti = $this->db->query('SELECT * from repeatInvoice_details where send_statements="send" AND client_email IN ('.$clients_user_ids.') and views=0')->result_array();
                        $invoice_notify = $this->db->query('SELECT * from Invoice_details where send_statements="send" AND client_email IN ('.$clients_user_ids.') and views = "1"')->result_array(); 
                        $repeatinvoice_notify = $this->db->query('SELECT * from repeatInvoice_details where send_statements="send" AND client_email IN ('.$clients_user_ids.') and views = "1"')->result_array(); 
                        $count2=count($invoice_noti)+count($invoice_notify);
                        $count3=count($repeatinvoice_noti)+count($repeatinvoice_notify);
                      }else{
                        $count2=0;
                        $count3=0;
                      }

                      $query2=$this->Common_mdl->section_menu('companyhouse_notification',$_SESSION['id']);  
                      if($query2=='0'){
                         $queries=$this->Common_mdl->header_notification(); 
                         $count4=count($queries);
                      }else{
                          $count4=0;
                      }
                      if($_SESSION['role']=='5'){ 
                          $query10=$this->Common_mdl->section_menu('companyhouse_notification',$_SESSION['id']);
                            if(empty($query10) || $query10=='0'){                          
                               $queries1=$this->Common_mdl->get_manager_notification($_SESSION['id']); 
                               $xxx=count($queries1);
                               $tsk_re=$this->db->query("select mn.user_id,mn.id,u.crm_name,ant.subject,ant.id as task_id from Manager_notification as mn left join add_new_task ant on ant.id=mn.task_id left join user u on u.id=mn.user_id where mn.status=0 and  FIND_IN_SET($_SESSION[id],manager_id) and mn.review_status=0 and mn.views=0")->result_array();
                              $tsk_re_tot=count($tsk_re); 
                           }else{
                               $xxx=0;
                               $tsk_re_tot=0;
                               $count14=0;
                           }                         
                         }else{
                          $xxx=0;
                          $tsk_re_tot=0;                          
                         }       

                      $rejected_tsk=$this->db->query("select mn.user_id,mn.message,mn.id,u.crm_name,ant.subject from Manager_notification as mn left join add_new_task ant on ant.id=mn.task_id left join user u on u.id=mn.user_id where mn.status=0 and mn.review_status=2 and mn.user_id=$_SESSION[id] and mn.overall_status=0")->result_array();

                      $count14=count($rejected_tsk); 

                      $query11=$this->Common_mdl->section_menu('task_notification',$_SESSION['id']);
                        if(empty($query11) || $query11=='0')
                        {   
                            $Task_Module_Notification = $this->Common_mdl->ReceiveNotification('Task',$_SESSION['id']);

                            $task_count = count( $Task_Module_Notification );
                        }
                        else
                        {
                            $task_count=0;
                        }               
                      
                      $proposal_notification = $this->Common_mdl->section_menu('proposal_notification',$_SESSION['id']);

                      if($proposal_notification == 0)  
                      {
                         $Proposal_notification = $this->Common_mdl->ReceiveNotification('Proposal',$_SESSION['id']);
                         $proposal_not_count = count($Proposal_notification);
                      }
                      else
                      {
                         $proposal_not_count = 0;
                      }   
              
                      ?>
                  <li class="header-notification">
                     <a href="#">
                       <i class="ti-bell"></i>
                       <span class="badge bg-c-pink sectionsss">
                         <?php echo $count1+$count2+$count3+$deadline_not_count+$count4+$xxx+$tsk_re_tot+$count14+$task_count+$proposal_not_count; ?>                   
                       </span>
                     </a>  


              <div class="show-notification  header_notify">
                  <div class="notification-title">
                      <h6>Notifications</h6>

                      <label class="label"><?php echo $count1+$count2+$count3+$deadline_not_count+$count4+$xxx+$tsk_re_tot+$count14+$task_count+$proposal_not_count; ?></label>
                  </div>

                  <ul class="nav nav-tabs">
                      <li class="nav-item">
                          <a class="nav-link active" data-toggle="tab" href="#nmenuo">Unread</a>
                      </li>

                         <li class="nav-item">
                          <a class="nav-link" data-toggle="tab" href="#rmenut">Read</a>
                      </li>

                      <li class="nav-item">
                          <a class="nav-link" data-toggle="tab" href="#nmenut">Archive</a>
                      </li>
                    <!--   <li class="nav-item">
                          <a class="nav-link" data-toggle="tab" href="#nmenuth">Menu 2</a>
                      </li> -->
                  </ul>

                   
                  <!-- Tab panes -->
                  <div class="tab-content">
                   <div class="tab-pane" id="nmenut">
                        <div class="company_notification">
                        <ul>
                          <?php
                              $con='';
                              $query11=$this->Common_mdl->section_menu('task_notification',$_SESSION['id']);
                              if(empty($query11) || $query11=='0'){                                    

                             if( count( $Task_Module_Notification ) )
                             {

                              
                                $ActiveNotification = array_keys( array_column( $Task_Module_Notification , 'status' ) , 2 );
                                foreach ( array_reverse($ActiveNotification) as $key => $Rowindex )
                                {

                                  $Data = $Task_Module_Notification[$Rowindex];
                                  ?>
                                  <li class="open_tsk" >
                                    <div class="media">
                                      <div class="media-body">
                                           <b>   <?php
                                          if($Data['task_id']!='' && $Data['task_id']!=0 ) { ?>
                                             <a href="<?php echo base_url()?>user/task_details/<?php echo $Data['task_id'] ?>" >
                                            <?php echo $Data['content']; ?>
                                            </a>
                                          
                                           <?php } else{
                                            echo $Data['content'];

                                           }
                                            
                                            ?></b>
                                      </div>
                                        <div class="media-footer">

                                            
                                        
                                      <a href="<?php echo base_url() ?>user/new_tasks/<?php echo $Data['id'];?>"   ><i class="fa fa-share" aria-hidden="true"></i></a>                                        

                                           
                                            <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $Data['id'];?>" id="remove-notification" data-type="remove" data-section="task" data-source='notification_management'><i class="fa fa-times" aria-hidden="true"></i></a>
                                        </div>
                                      </div>
                                  </li>
                                  <?php

                                }

                             }
                                    

                                }   ?>
                         <?php
                        foreach ($rejected_tsk as $v) {  ?>
                        <li class="rejected_tsk" data-id="<?php echo $v['id']?>">
                           <div class="media">
                              <div class="media-body" >
                                 <b>Your <?=$v['subject']?> Review is Decline</b>
                                 <p><?=$v['message']?></p>
                               </div>
                           </div>
                        </li>
                        <?php }?>
      
                      <?php 
                          $noti=$this->db->query("select * from user where autosave_status='1' order by id desc")->result_array();
                          foreach($noti as $key => $val){

                                $stat=$this->Common_mdl->archive_check($v['id'],'user');
                                if($stat=='1')
                                {
                                  $con='style="display:none;"';
                                }
                                else
                                {
                                  $con='style="display:block"';
                                }  
                            ?>
                            <li <?php $query=$this->Common_mdl->section_menu('client_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?> <?php echo $con; ?>>
                               <div class="media">
                                  <div class="media-body">
                                     <a href="<?php echo base_url().'Client/client_info/'.$val['id'];?>">
                                        <h5 class="notification-user">Add Clients</h5>
                                        <span class="notification-time"><?php 
                                        if(isset($val['CreatedTime']) && $val['CreatedTime']!=''){ // 22-08-2018 
                                           $date=date('Y-m-d H:i:s',$val['CreatedTime']);
                                           echo time_elapsed_string($date);
                                           }?>  Status:<?php echo $stat; ?></span>
                                     </a> 
                                     </div>
                                       <div class="media-footer">
                                     <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $val['id']?>" data-type="archive" data-section="user"> <i class="fa fa-archive" aria-hidden="true"></i></a>
                                     <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $val['id']?>" id="remove-notification" data-type="remove" data-section='user'><i class="fa fa-times" aria-hidden="true"></i></a>
                                  </div>
                               </div>
                            </li>
                            <?php } ?>
                        <?php 
                        $queries=$this->Common_mdl->header_notification();  
                        $i=1;                      
                        foreach($queries as  $value)
                        {    
                         // echo $i;                      
                          $company_name=$this->Common_mdl->getcompany_name($value['id']);
                           
                           if( count( array_diff($value, ['']) ) > 0)
                           {
                             $stat = $this->Common_mdl->GetAllWithWheretworow('notification_archive','type','company_house','notification_id',$value['id']);
                             /*$query=$this->Common_mdl->section_menu('companyhouse_notification',$_SESSION['id']);  */
                             if( count( $stat ) )
                             { 
                                $con = 'style="display:block"';
                             }
                             else
                             {
                                $con = 'style="display:none;"';
                             } 
                        ?>
                        <!-- 
                          <?php $query=$this->Common_mdl->section_menu('companyhouse_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?>
                         -->
                         <li <?php echo $con; ?>>
                           <div class="media">
                              <div class="media-body">
                                 <a href="javascript:;" class="data_source_element"  id="<?php echo $value['id']; ?>"
                                 data-companyname="<?php if(isset($value['crm_company_name']) && $value['crm_company_name']!=''){ echo $value['crm_company_name'];  }else{ echo ''; }?>"
                                 data-companynumber="<?php if(isset($value['crm_company_number']) && $value['crm_company_number']!=''){ echo $value['crm_company_number']; }else{ echo ''; }?>" 
                                 data-incorporationdate="<?php if(isset($value['crm_incorporation_date']) && $value['crm_incorporation_date']!=''){ echo $value['crm_incorporation_date']; } ?>"
                                 data-registeraddress="<?php if(isset($value['crm_register_address']) && $value['crm_register_address']!=''){ echo $value['crm_register_address']; } ?>" 
                                 data-companytype="<?php if(isset($value['crm_company_type']) && $value['crm_company_type']!=''){ echo $value['crm_company_type']; } ?>"
                                 data-accounts_next_made_up_to="<?php if(isset($value['accounts_next_made_up_to']) && $value['accounts_next_made_up_to']!=''){ echo $value['accounts_next_made_up_to']; } ?>"

                                 data-accounts_next_due="<?php if(isset($value['accounts_next_due']) && $value['accounts_next_due']!=''){ echo $value['accounts_next_due']; } ?>"

                                 data-confirmation_next_made_up_to="<?php if(isset($value['confirmation_next_made_up_to']) && $value['confirmation_next_made_up_to']!=''){ echo $value['confirmation_next_made_up_to']; } ?>"

                                 data-confirmation_next_due="<?php if(isset($value['confirmation_next_due']) && $value['confirmation_next_due']!=''){ echo $value['confirmation_next_due']; } ?>">
                                    <h5 class="notification-user">
                                      Company House Data Updates Available-<?php echo $company_name; ?>
                                    </h5>
                                 </a>   
                              </div>
                              <div class="media-footer">
                                  
                                  <a href="javascript:;" class="companyhouse_update"><i class="fa fa-save" aria-hidden="true"></i></a>

                                  <!-- <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $value['id']?>" id="remove-notification" data-type="remove" data-section='company_house'><i class="fa fa-times" aria-hidden="true"></i></a> -->                                 
                                 <!--  <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $value['id']?>" data-type="archive" data-section="company_house"> <i class="fa fa-archive" aria-hidden="true"></i></a>     -->                           
                             </div>
                           </div>
                        </li>

                        <?php }
                              }  ?>


                        <?php 
                              
                        if($query3 == '0')
                        {
                            foreach($Deadline_notification as $key => $value) 
                            {
                                if($value['status'] == '2')
                                {

                        ?>
                             <li>
                              <div class="media">
                                 <div class="media-body">
                                    <a href="javascript:void(0);">
                                       <h5 class="notification-user"></h5>
                                       <span class="notification-time"><?php echo $value['content']; ?></span>
                                    </a>  
                                     </div>
                                      <div class="media-footer">   
                                       <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $value['id']?>" id="remove-notification" data-type="remove" data-section='deadline' data-source="notification_management"><i class="fa fa-times" aria-hidden="true"></i></a>
                                     </div>
                              </div>
                             </li>
                        <?php             
                                }
                            } 
                        }

                        ?>     

                        <?php 
                              
                        if($proposal_notification == '0')
                        {
                            foreach($Proposal_notification as $key => $value) 
                            {
                                if($value['status'] == '2')
                                {

                        ?>
                             <li>
                              <div class="media">
                                 <div class="media-body">
                                    <a href="javascript:void(0);">
                                       <h5 class="notification-user"></h5>
                                       <span class="notification-time"><?php echo $value['content']; ?></span>
                                    </a>  
                                     </div>
                                      <div class="media-footer">     
                                       <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $value['id']?>" id="remove-notification" data-type="remove" data-section='proposal' data-source="notification_management"><i class="fa fa-times" aria-hidden="true"></i></a>
                                     </div>
                              </div>
                             </li>
                        <?php             
                                }
                            } 
                        }

                        ?>     

                       <!--  Invoice Send Statements  -->   
                        <?php
                         
                              if(count($invoice_noti)>0)
                              {
                                foreach ($invoice_noti as $invoice) 
                                { 
                                    $stat=$this->Common_mdl->archive_check($invoice['client_id'],'invoice');
                                    
                                    if($stat=='0')
                                    {
                                     ?>
                                      <li>
                                         <div class="media">
                                            <div class="media-body">
                                               <a href="<?php echo base_url().'Invoice/sendInvoiceBill/'.$invoice['client_id'];?>">
                                                  <h5 class="notification-user">Invoice</h5>
                                                  <span class="notification-time"><?php 
                                                  if(isset($invoice['created_at'])&& $invoice['created_at']!='0000-00-00 00:00:00'){
                                                     $date=date('Y-m-d H:i:s',strtotime($invoice['created_at']));
                                                     echo time_elapsed_string($date);
                                                   }
                                                     ?><?php// echo $stat; ?></span>
                                               </a>
                                                </div>
                                     <div class="media-footer">                                        
                                        <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $invoice['client_id']?>" id="remove-notification" data-type="remove" data-section='invoice'><i class="fa fa-times" aria-hidden="true"></i></a>
                                            </div>
                                         </div>
                                      </li>                               
                                   
                        <?php } } } ?>
                        <?php 
                            if(count($repeatinvoice_noti)>0)
                            {
                               foreach ($repeatinvoice_noti as $repeat_invoice) 
                               {                                 
                                 $stat=$this->Common_mdl->archive_check($repeat_invoice['client_id'],'repeat-invoice');
                                  if($stat=='0')
                                  {
                               
                              ?>
                              <li>
                                 <div class="media">
                                    <div class="media-body">
                                       <a href="<?php echo base_url().'Invoice/sendInvoiceBill/'.$repeat_invoice['client_id'];?>">
                                          <h5 class="notification-user">Invoice</h5>
                                          <span class="notification-time"><?php 
                                             $date=date('Y-m-d H:i:s',$repeat_invoice['created_at']);
                                             echo time_elapsed_string($date);?></span>
                                       </a>
                                        </div>
                                         <div class="media-footer">
                                 <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $repeat_invoice['client_id']; ?>" id="remove-notification" data-type="remove" data-section='repeat-invoice'><i class="fa fa-times" aria-hidden="true"></i></a>
                                    </div>
                                 </div>
                              </li>
                        <?php } } }  

                              if(count($invoice_notify)>0)
                              {
                                foreach ($invoice_notify as $invoice) 
                                {         

                                $stat=$this->Common_mdl->archive_check($invoice['client_id'],'invoice');
                                
                                if($stat =='0')
                                {                          
                         ?>
                                      <li>
                                         <div class="media">
                                            <div class="media-body">
                                               <a href="<?php echo base_url().'Invoice/sendInvoiceBill/'.$invoice['client_id'];?>">
                                                  <h5 class="notification-user">Invoice</h5>
                                                  <span class="notification-time"><?php 
                                                  if(isset($invoice['created_at'])&& $invoice['created_at']!='0000-00-00 00:00:00'){
                                                     $date=date('Y-m-d H:i:s',strtotime($invoice['created_at']));
                                                     echo time_elapsed_string($date);
                                                   }
                                                     ?><?php// echo $stat; ?></span>
                                               </a>
                                                </div>
                                     <div class="media-footer">                                     
                                        <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $invoice['client_id']?>" id="remove-notification" data-type="remove" data-section='invoice'><i class="fa fa-times" aria-hidden="true"></i></a>
                                            </div>
                                         </div>
                                      </li>                               
                                   
                        <?php } } }  

                            if(count($repeatinvoice_notify)>0)
                            { 
                               foreach ($repeatinvoice_notify as $repeat_invoice) 
                               {
                                  
                               $stat=$this->Common_mdl->archive_check($repeat_invoice['client_id'],'repeat-invoice');
                                                                
                                if($stat =='0')
                                {  
                        ?>
                              <li>
                                 <div class="media">
                                    <div class="media-body">
                                       <a href="<?php echo base_url().'Invoice/sendInvoiceBill/'.$repeat_invoice['client_id'];?>">
                                          <h5 class="notification-user">Invoice</h5>
                                          <span class="notification-time"><?php 
                                             $date=date('Y-m-d H:i:s',$repeat_invoice['created_at']);
                                             echo time_elapsed_string($date);?></span>
                                       </a>
                                        </div>
                                         <div class="media-footer">
                                 <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $repeat_invoice['client_id']; ?>" id="remove-notification" data-type="remove" data-section='repeat-invoice'><i class="fa fa-times" aria-hidden="true"></i></a>
                                    </div>
                                 </div>
                              </li>
                        <?php     } } } ?>

                     </ul>
                   </div>
                   </div>

                      <div class="tab-pane active" id="nmenuo">
                        <div class="company_notification">
                        <ul >
                          <?php
                           $query11=$this->Common_mdl->section_menu('task_notification',$_SESSION['id']);
                            if(empty($query11) || $query11=='0'){                               
                           if( count( $Task_Module_Notification ) )
                             {
                                $ActiveNotification = array_keys( array_column( $Task_Module_Notification , 'status' ) , 1  );
                                foreach (array_reverse($ActiveNotification) as $key => $Rowindex)
                                {

                                  $Data = $Task_Module_Notification[$Rowindex];
                                  ?>
                                  <li class="open_tsk" >
                                    <div class="media">
                                      <div class="media-body" >
                                           <b>   <?php
                                          if($Data['task_id']!='' && $Data['task_id']!=0 ) { ?>
                                               <a class="notifi_status" data-id="<?php echo $Data['id']?>" data-task_id="<?php echo $Data['task_id']?>" href="javascript:;" >
                                               
                                            <?php echo $Data['content']; ?>
                                            </a>
                                          
                                           <?php } else{
                                            echo $Data['content'];

                                           }
                                            
                                            ?></b>
                                      </div>
                                        <div class="media-footer">
                                            <!--    </div> -->
                                           
                                        
                                      <a href="<?php echo base_url() ?>user/new_tasks/<?php echo $Data['id'];?>"   ><i class="fa fa-share" aria-hidden="true"></i></a>
                                           

                                            <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $Data['id'];?>" data-type="archive" data-section="task" data-source='notification_management'> <i class="fa fa-archive" aria-hidden="true"></i></a>
                                            <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $Data['id'];?>" id="remove-notification" data-type="remove" data-section="task" data-source='notification_management'><i class="fa fa-times" aria-hidden="true"></i></a>
                                        </div>
                                      </div>
                                  </li>
                                  <?php

                                }

                             }                           
                        } 
                      ?>
                         <?php
                        foreach ($rejected_tsk as $v) { ?>
                        <li class="rejected_tsk" data-id="<?php echo $v['id']?>">
                           <div class="media">
                              <div class="media-body" >
                               <b>Your <?=$v['subject']?> Review is Decline</b>
                                 <p><?=$v['message']?></p>
                               </div>
                           </div>
                        </li>
                        <?php }?>
       
                      <?php 
                          $noti=$this->db->query("select * from user where autosave_status='1' order by id desc")->result_array();
                          foreach($noti as $key => $val){

                                    $stat=$this->Common_mdl->archive_check($v['id'],'user');
                                   if($stat==0)
                                   {
                                    $con='style="display:none"';
                                   }
                                   else
                                   {
                                    $con='style="display:block"';
                                   } 
                            ?>
                            <li <?php $query=$this->Common_mdl->section_menu('client_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?> <?php echo $con; ?>>
                               <div class="media">
                                  <div class="media-body">
                                     <a href="<?php echo base_url().'Client/client_info/'.$val['id'];?>">
                                        <h5 class="notification-user">Add Clients</h5>
                                        <span class="notification-time"><?php 
                                        if(isset($val['CreatedTime']) && $val['CreatedTime']!=''){ // 22-08-2018 
                                           $date=date('Y-m-d H:i:s',$val['CreatedTime']);
                                           echo time_elapsed_string($date);
                                           }?>  Status:<?php echo $stat; ?></span>
                                     </a> 
                                      </div>
                                   <div class="media-footer">
                                     <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $val['id']?>" data-type="archive" data-section="user"> <i class="fa fa-archive" aria-hidden="true"></i></a>
                                     <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $val['id']?>" id="remove-notification" data-type="remove" data-section='user'><i class="fa fa-times" aria-hidden="true"></i></a>
                                  </div>
                               </div>
                            </li>
                            <?php } ?>
                        <?php 
                        $queries=$this->Common_mdl->header_notification();                        
                        foreach($queries as  $value)
                        {                          
                          $company_name=$this->Common_mdl->getcompany_name($value['id']);
                           
                           if( count( array_diff( $value , [''] ) ) > 0 )
                           {

                               $stat = $this->Common_mdl->GetAllWithWheretworow('notification_archive','type','company_house','notification_id',$value['id']);

                               if( count($stat) == 0 )
                               {
                                  $con = 'style="display:block"';
                               }
                               else
                               {
                                  $con = 'style="display:none"';
                               }
                        ?>
                        <!-- <?php $query=$this->Common_mdl->section_menu('companyhouse_notification',$_SESSION['id']);  if($query=='1'){  ?> style="display: none;" <?php } ?> -->

                         <li  <?php echo $con; ?> >
                           <div class="media">
                              <div class="media-body">
                                 <a href="javascript:;" class="data_source_element"  id="<?php echo $value['id']; ?>" 
                                 data-companyname="<?php if(isset($value['crm_company_name']) && $value['crm_company_name']!=''){ echo $value['crm_company_name'];  }else{ echo ''; }?>"
                                 data-companynumber="<?php if(isset($value['crm_company_number']) && $value['crm_company_number']!=''){ echo $value['crm_company_number']; }else{ echo ''; }?>" 
                                 data-incorporationdate="<?php if(isset($value['crm_incorporation_date']) && $value['crm_incorporation_date']!=''){ echo $value['crm_incorporation_date']; } ?>"
                                 data-registeraddress="<?php if(isset($value['crm_register_address']) && $value['crm_register_address']!=''){ echo $value['crm_register_address']; } ?>" 
                                 data-companytype="<?php if(isset($value['crm_company_type']) && $value['crm_company_type']!=''){ echo $value['crm_company_type']; } ?>"
                                 data-accounts_next_made_up_to="<?php if(isset($value['accounts_next_made_up_to']) && $value['accounts_next_made_up_to']!=''){ echo $value['accounts_next_made_up_to']; } ?>"
                                 data-accounts_next_due="<?php if(isset($value['accounts_next_due']) && $value['accounts_next_due']!=''){ echo $value['accounts_next_due']; } ?>"
                                 data-confirmation_next_made_up_to="<?php if(isset($value['confirmation_next_made_up_to']) && $value['confirmation_next_made_up_to']!=''){ echo $value['confirmation_next_made_up_to']; } ?>"
                                 data-confirmation_next_due="<?php if(isset($value['confirmation_next_due']) && $value['confirmation_next_due']!=''){ echo $value['confirmation_next_due']; } ?>" >
                                    <h5 class="notification-user">Company House Data Updates Available-<?php echo $company_name; ?></h5>
                                 </a>
                               <!--   <a href="#" data-toggle="modal" data-target="#modalarchive" data-id="<?php echo $value['id']?>" data-type="service" > <i class="fa fa-archive" aria-hidden="true"></i></a>      -->                       
                                
                              </div>
                              <div class="media-footer"> 
                                
                                  <a href="javascript:;" class="companyhouse_update"><i class="fa fa-save" aria-hidden="true"></i></a>

                                 <!--  <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $value['id']?>" id="remove-notification" data-type="remove" data-section='company_house'><i class="fa fa-times" aria-hidden="true"></i></a> -->                                  
                                  <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $value['id']?>" data-type="archive" data-section="company_house"> <i class="fa fa-archive" aria-hidden="true"></i></a>
                             </div>
                           </div>
                        </li>

                        <?php }
                              }  ?>


                        <?php 
                              
                        if($query3 == '0')
                        {
                            foreach($Deadline_notification as $key => $value) 
                            {
                                if($value['status'] == '1')
                                {

                        ?>
                             <li>
                              <div class="media">
                                 <div class="media-body">
                                    <a href="javascript:void(0);">
                                       <h5 class="notification-user"></h5>
                                       <span class="notification-time"><?php echo $value['content']; ?></span>
                                    </a>  
                                     </div>
                                      <div class="media-footer">

                                      <a href="javascript:;" data-user="<?php echo base_url() ?>client/client_infomation/<?php echo $value['sender_id'];?>" data-toggle="modal" data-target="#notification_modal" id="forward-notification" data-id="<?php echo $value['id']?>" data-type="read" data-section="deadline" data-source="notification_management"><i class="fa fa-share" aria-hidden="true"></i></a>
                                                                      
                                      <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $value['id']?>" data-type="archive" data-section="deadline" data-source="notification_management"> <i class="fa fa-archive" aria-hidden="true"></i></a>

                                       <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $value['id']?>" id="remove-notification" data-type="remove" data-section='deadline' data-source="notification_management"><i class="fa fa-times" aria-hidden="true"></i></a>

                                     </div>
                              </div>
                             </li>
                        <?php             
                                }
                            } 
                        }

                        ?> 

                        <?php 
                              
                        if($proposal_notification == '0')
                        {
                            foreach($Proposal_notification as $key => $value) 
                            {
                                if($value['status'] == '1')
                                {

                        ?>
                             <li>
                              <div class="media">
                                 <div class="media-body">
                                    <a href="javascript:void(0);">
                                       <h5 class="notification-user"></h5>
                                       <span class="notification-time"><?php echo $value['content']; ?></span>
                                    </a>  
                                     </div>
                                      <div class="media-footer">    

                                      <a href="javascript:;" data-user="<?php echo base_url(); ?>proposal" data-toggle="modal" data-target="#notification_modal" id="forward-notification" data-id="<?php echo $value['id']?>" data-type="read" data-section="proposal" data-source="notification_management"><i class="fa fa-share" aria-hidden="true"></i></a>                              
                                      <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $value['id']?>" data-type="archive" data-section="proposal" data-source="notification_management"> <i class="fa fa-archive" aria-hidden="true"></i></a>

                                       <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $value['id']?>" id="remove-notification" data-type="remove" data-section='proposal' data-source="notification_management"><i class="fa fa-times" aria-hidden="true"></i></a>
                                     </div>
                              </div>
                             </li>
                        <?php             
                                }
                            } 
                        }

                        ?>        
                        <!--  Invoice Send Statements  -->   
                        <?php
                         
                              if(count($invoice_noti)>0)
                              {
                                foreach ($invoice_noti as $invoice) 
                                { 
                                    $stat=$this->Common_mdl->archive_check($invoice['client_id'],'invoice');
                               
                                    if($stat!='0')
                                    {
                                      
                                  ?>
                                    <li>
                                       <div class="media">
                                          <div class="media-body">
                                             <a href="javascript:;">
                                                <h5 class="notification-user">Invoice</h5>
                                                <span class="notification-time"><?php 
                                                if(isset($invoice['created_at'])&& $invoice['created_at']!='0000-00-00 00:00:00'){
                                                   $date=date('Y-m-d H:i:s',strtotime($invoice['created_at']));
                                                   echo time_elapsed_string($date);
                                                 }
                                                   ?></span>
                                             </a>
                                              </div>
                                   <div class="media-footer">
                                      <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $invoice['client_id']?>" id="forward-notification" data-user="<?php echo base_url().'Invoice/sendInvoiceBill/'.$invoice['client_id'];?>" data-type="read" data-section='invoice'><i class="fa fa-share" aria-hidden="true"></i></a>
                                      <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $invoice['client_id']?>" data-type="archive" data-section="invoice"> <i class="fa fa-archive" aria-hidden="true"></i></a>
                                    
                                      <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $invoice['client_id']; ?>" id="remove-notification" data-type="remove" data-section='invoice'><i class="fa fa-times" aria-hidden="true"></i></a>
                                          </div>
                                       </div>
                                    </li>
                        <?php } } }?>
                        <?php 
                           if(count($repeatinvoice_noti)>0)
                           {
                              foreach ($repeatinvoice_noti as $repeat_invoice) 
                              {
                                 $stat=$this->Common_mdl->archive_check($repeat_invoice['client_id'],'repeat-invoice');
                                 
                                 if($stat!='0')
                                 {
                                     

                              ?>
                        <li>
                           <div class="media">
                              <div class="media-body">
                                 <a href="javascript:;">
                                    <h5 class="notification-user">Invoice</h5>
                                    <span class="notification-time"><?php 
                                       $date=date('Y-m-d H:i:s',$repeat_invoice['created_at']);
                                       echo time_elapsed_string($date);?> <?php// echo $stat; ?></span>
                                 </a> 
                                  </div>
                                   <div class="media-footer">

                                   <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $repeat_invoice['client_id']?>" id="forward-notification" data-user="<?php echo base_url().'Invoice/sendInvoiceBill/'.$repeat_invoice['client_id'];?>" data-type="read" data-section='repeat-invoice'><i class="fa fa-share" aria-hidden="true"></i></a>
                                   
                                   <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $repeat_invoice['client_id']; ?>" data-type="archive" data-section="repeat-invoice"> <i class="fa fa-archive" aria-hidden="true"></i></a>
                                   
                                  <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $repeat_invoice['client_id']; ?>" id="remove-notification" data-type="remove" data-section='repeat-invoice'><i class="fa fa-times" aria-hidden="true"></i></a>
                              </div>
                           </div>
                        </li>
                        <?php } } } ?>
                     </ul>
                   </div>
                 </div>


                      <div class="tab-pane" id="rmenut">
                        <div class="company_notification">
                        <ul >
                          <?php
                           $query11=$this->Common_mdl->section_menu('task_notification',$_SESSION['id']);
                            if(empty($query11) || $query11=='0'){
                            
                           $Task_Module_Notification1 = $this->db->query("select * from notification_management where module='Task' and receiver_user_id = ".$_SESSION['id']." and status IN(4)")->result_array();

                           if( count( $Task_Module_Notification1 ) )
                             {
                           // print_r($Task_Module_Notification1);
                                $readNotification = array_keys( array_column( $Task_Module_Notification1 , 'status' ) , 4  );
                              //  print_r($readNotification);
                                foreach (array_reverse($readNotification) as $key => $Rowindex)
                                {

                                  $Data = $Task_Module_Notification[$Rowindex];
                                  ?>
                                  <li class="open_tsk" >
                                    <div class="media">
                                      <div class="media-body" >
                                          <?php
                                          if($Data['task_id']!='' && $Data['task_id']!=0 ) { ?>
                                             <a href="<?php echo base_url()?>user/task_details/<?php echo $Data['task_id'] ?>" >
                                            <?php echo $Data['content']; ?>
                                            </a>
                                          
                                           <?php } else{
                                            echo $Data['content'];

                                           }
                                            
                                            ?>
                                      </div>
                                        <div class="media-footer">
                                            <!--    </div> -->
                                  
                                            
                                          <a href="<?php echo base_url() ?>user/new_tasks/<?php echo $Data['id'];?>"  ><i class="fa fa-share" aria-hidden="true"></i>
                                            </a> 
                                           

                                            <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $Data['id'];?>" data-type="archive" data-section="task" data-source='notification_management'> <i class="fa fa-archive" aria-hidden="true"></i></a>
                                            <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $Data['id'];?>" id="remove-notification" data-type="remove" data-section="task" data-source='notification_management'><i class="fa fa-times" aria-hidden="true"></i></a>
                                        </div>
                                      </div>
                                  </li>
                                  <?php

                                }

                             }                           
                        } 
                      ?>


                       <?php 
                             
                       if($query3 == '0')
                       {
                           foreach($Deadline_notification as $key => $value) 
                           {
                               if($value['status'] == '4')
                               {

                       ?>
                            <li>
                             <div class="media">
                                <div class="media-body">
                                   <a href="javascript:void(0);">
                                      <h5 class="notification-user"></h5>
                                      <span class="notification-time"><?php echo $value['content']; ?></span>
                                   </a>  
                                    </div>
                                     <div class="media-footer">                                  
                                     <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $value['id']?>" data-type="archive" data-section="deadline" data-source="notification_management"> <i class="fa fa-archive" aria-hidden="true"></i></a>

                                      <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $value['id']?>" id="remove-notification" data-type="remove" data-section='deadline' data-source="notification_management"><i class="fa fa-times" aria-hidden="true"></i></a>
                                    </div>
                             </div>
                            </li>
                       <?php             
                               }
                           } 
                       }

                       ?>     

                       <?php 
                             
                       if($proposal_notification == '0')
                       {
                           foreach($Proposal_notification as $key => $value) 
                           {
                               if($value['status'] == '4')
                               {

                       ?>
                            <li>
                             <div class="media">
                                <div class="media-body">
                                   <a href="javascript:void(0);">
                                      <h5 class="notification-user"></h5>
                                      <span class="notification-time"><?php echo $value['content']; ?></span>
                                   </a>  
                                    </div>
                                     <div class="media-footer">                                  
                                     <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $value['id']?>" data-type="archive" data-section="proposal" data-source="notification_management"> <i class="fa fa-archive" aria-hidden="true"></i></a>

                                      <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $value['id']?>" id="remove-notification" data-type="remove" data-section='proposal' data-source="notification_management"><i class="fa fa-times" aria-hidden="true"></i></a>
                                    </div>
                             </div>
                            </li>
                       <?php             
                               }
                           } 
                       }

                       ?>     

                      <!--  Invoice Send Statements  -->   
                       <?php

                       if($query1=='0' && !empty($clients_user_ids))
                       {   
                             if(count($invoice_notify)>0)
                             {
                               foreach ($invoice_notify as $invoice) 
                               {         

                               $stat=$this->Common_mdl->archive_check($invoice['client_id'],'invoice');
                               
                               if($stat!='0')
                               {                          
                        ?>
                                     <li>
                                        <div class="media">
                                           <div class="media-body">
                                              <a href="<?php echo base_url().'Invoice/sendInvoiceBill/'.$invoice['client_id'];?>">
                                                 <h5 class="notification-user">Invoice</h5>
                                                 <span class="notification-time"><?php 
                                                 if(isset($invoice['created_at'])&& $invoice['created_at']!='0000-00-00 00:00:00'){
                                                    $date=date('Y-m-d H:i:s',strtotime($invoice['created_at']));
                                                    echo time_elapsed_string($date);
                                                  }
                                                    ?><?php// echo $stat; ?></span>
                                              </a>
                                               </div>
                                    <div class="media-footer">
                                       <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $invoice['client_id']?>" data-type="archive" data-section="invoice"> <i class="fa fa-archive" aria-hidden="true"></i></a>
                                       <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $invoice['client_id']?>" id="remove-notification" data-type="remove" data-section='invoice'><i class="fa fa-times" aria-hidden="true"></i></a>
                                           </div>
                                        </div>
                                     </li>                               
                                  
                       <?php } } }  

                           if(count($repeatinvoice_notify)>0)
                           { 
                              foreach ($repeatinvoice_notify as $repeat_invoice) 
                              {
                                 
                              $stat=$this->Common_mdl->archive_check($repeat_invoice['client_id'],'repeat-invoice');
                                                               
                               if($stat!='0')
                               {  
                       ?>
                             <li>
                                <div class="media">
                                   <div class="media-body">
                                      <a href="<?php echo base_url().'Invoice/sendInvoiceBill/'.$repeat_invoice['client_id'];?>">
                                         <h5 class="notification-user">Invoice</h5>
                                         <span class="notification-time"><?php 
                                            $date=date('Y-m-d H:i:s',$repeat_invoice['created_at']);
                                            echo time_elapsed_string($date);?></span>
                                      </a>
                                       </div>
                                        <div class="media-footer">

                                      <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" id="archive-notification" data-id="<?php echo $repeat_invoice['client_id']; ?>" data-type="archive" data-section="repeat-invoice"> <i class="fa fa-archive" aria-hidden="true"></i></a>
                                <a href="javascript:;" data-toggle="modal" data-target="#notification_modal" data-id="<?php echo $repeat_invoice['client_id']; ?>" id="remove-notification" data-type="remove" data-section='repeat-invoice'><i class="fa fa-times" aria-hidden="true"></i></a>
                                   </div>
                                </div>
                             </li>
                       <?php     } } } }  ?>

                     </ul>

                   </div>
                 </div>

                  <div class="notification-footer">
                    <a href="<?php echo base_url() ?>user/notification" class="btn btn-info" >Notification Setting</a>
                    
                        <a href="<?php echo base_url()?>Notification/notification_list" class="btn btn-success">View All</a>
                   </div>
                   

                  <?php $result=$this->db->query('select * from user where id="'.$_SESSION['id'].'"')->result_array();?>
                  <li class="user-profile header-notification">
                     <a href="#!">
                     <?php   $getUserProfilepic = $this->Common_mdl->getUserProfilepic($_SESSION['id']);
                        ?>
                     <img src="<?php echo $getUserProfilepic;?>" alt="img">
            
                     <span><?php 
                        if(isset($_SESSION['username'])&&$_SESSION['username']!=''){ echo $result[0]['username']; } ?> </span>
                     <i class="ti-angle-down"></i>
                     </a>
                   <ul class="tgl-opt">
                   <?php 
                    if(isset($_SESSION['SA_TO_FA']) && $_SESSION['SA_TO_FA']==1)
                    {
                      ?>
                    <li class="atleta">
                      <a href="<?php echo base_url();?>Super_admin/Switch_to_SuperAdmin">
                       <i class="ti-layout-sidebar-left"></i> Switch To Super Admin
                      </a> 
                    </li>
                    <?php }
                    else { ?> 
                    <li class="atleta">
                       <a href="<?php echo base_url();?>login/logout">
                       <i class="ti-layout-sidebar-left"></i> Logout
                       </a> 
                    </li>
                  <?php } ?>
                  </ul>
                  </li>
               </ul>
               <!-- search -->
               <div id="morphsearch" class="morphsearch">
                  <form class="morphsearch-form">
                     <input class="morphsearch-input" type="search" placeholder="Search..." />
                     <button class="morphsearch-submit" type="submit">Search</button>
                  </form>
                  <div class="morphsearch-content">
                     <div class="dummy-column">
                        <h2>People</h2>
                        <a class="dummy-media-object" href="#!">
                           <img src="<?php echo base_url();?>assets/images/avatar-1.jpg" alt="Sara Soueidan" />
                           <h3>Sara Soueidan</h3>
                        </a>
                        <a class="dummy-media-object" href="#!">
                           <img src="<?php echo base_url();?>assets/images/avatar-2.jpg" alt="Shaun Dona" />
                           <h3>Shaun Dona</h3>
                        </a>
                     </div>
                     <div class="dummy-column">
                        <h2>Popular</h2>
                        <a class="dummy-media-object" href="#!">
                           <img src="<?php echo base_url();?>assets/images/avatar-3.jpg" alt="PagePreloadingEffect" />
                           <h3>Page Preloading Effect</h3>
                        </a>
                        <a class="dummy-media-object" href="#!">
                           <img src="<?php echo base_url();?>assets/images/avatar-4.jpg" alt="DraggableDualViewSlideshow" />
                           <h3>Draggable Dual-View Slideshow</h3>
                        </a>
                     </div>
                     <div class="dummy-column">
                        <h2>Recent</h2>
                        <a class="dummy-media-object" href="#!">
                           <img src="<?php echo base_url();?>assets/images/avatar-5.jpg" alt="TooltipStylesInspiration" />
                           <h3>Tooltip Styles Inspiration</h3>
                        </a>
                        <a class="dummy-media-object" href="#!">
                           <img src="<?php echo base_url();?>assets/images/avatar-6.jpg" alt="NotificationStyles" />
                           <h3>Notification Styles Inspiration</h3>
                        </a>
                     </div>
                  </div>
                  <!-- /morphsearch-content -->
                  <span class="morphsearch-close"><i class="icofont icofont-search-alt-1"></i></span>
               </div>
               <!-- search end -->
            </div>
         </div>
      </nav>

      <div class="modal-alertsuccess alert succ header_popup_info_msg" style="display:none;">
         <div class="header_newupdate_alert"><a href="#" class="close" id="close_info_msg">×</a>
         <div class="header_pop-realted1">
         <div class="header_position-alert1">
               Please! Select Record...
         </div></div>
         </div>
  </div> 

      <div class="pcoded-main-container">
      <div class="pcoded-wrapper">
      <nav class="pcoded-navbar">
         <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
         <div class="pcoded-inner-navbar main-menu">
            <div class="">
               <div class="main-menu-header">
                  <img class="img-40 img-radius" src="<?php echo base_url();?>assets/images/avatar-4.jpg" alt="User-Profile-Image">
                  <div class="user-details">
                     <span><?php 
                        if(isset($_SESSION['username'])&&$_SESSION['username']!=''){ echo $_SESSION['username']; } ?></span>
                     <span id="more-details">UX Designer<i class="ti-angle-down"></i></span>
                  </div>
               </div>
               <div class="main-menu-content">
                  <ul>
                     <li class="more-details">
                        <!-- <a href="user-profile.html"><i class="ti-user"></i>View Profile</a>
                           <a href="#!"><i class="ti-settings"></i>Settings</a> -->
                        <a href="<?php echo base_url();?>login/logout"><i class="ti-layout-sidebar-left"></i>Logout</a>
                     </li>
                  </ul>
               </div>
            </div>
            <div class="pcoded-search">
               <span class="searchbar-toggle">  </span>
               <div class="pcoded-search-box ">
                  <input type="text" placeholder="Search">
                  <span class="search-icon"><i class="ti-search" aria-hidden="true"></i></span>
               </div>
            </div>
            <div class="pcoded-navigatio-lavel" data-i18n="nav.category.navigation"></div>
            <ul class="pcoded-item pcoded-left-item">
               <li class="pcoded-hasmenu1">
                  <a href="<?php echo base_url().'user'?>">
                  <span class="pcoded-micon"><i class="ti-home"></i><b>D</b></span>
                  <span class="pcoded-mtext">Dashboard</span>
                  </a>
               </li>
            </ul>
            <ul class="pcoded-item pcoded-left-item performance_list">
               <li class="pcoded-hasmenu3">
                  <a href="<?php echo base_url().'user/column_setting_new'?>">
                  <span class="pcoded-micon"><i class="fa fa-comment fa-6" aria-hidden="true"></i><b>c</b></span>
                  <span class="pcoded-mtext">cloumn setting</span>
                  </a>
               </li>
            </ul>
            <ul class="pcoded-item pcoded-left-item">
               <li class="pcoded-hasmenu pcoded-hasmenu4">
                  <a href="javascript:void(0)">
                  <span class="pcoded-micon"><i class="fa fa-plus fa-6" aria-hidden="true"></i><b>P</b></span>
                  <span class="pcoded-mtext">Clients</span>
                  </a>
                  <ul class="pcoded-submenu">
                     <li class="">
                        <a href="<?php echo base_url()?>client/add_client">
                        <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                        <span class="pcoded-mtext" data-i18n="nav.dash.default">Add clients</span>
                        <span class="pcoded-mcaret"></span>
                        </a>
                     </li>
                     <li class="">
                        <a href="<?php echo base_url()?>client/addnewclient">
                        <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                        <span class="pcoded-mtext" data-i18n="nav.dash.ecommerce">Add client from companyhouse</span>
                        <span class="pcoded-mcaret"></span>
                        </a>
                     </li>
                  </ul>
               </li>
            </ul>
            <ul class="pcoded-item pcoded-left-item performance_list">
               <li class="pcoded-hasmenu5">
                  <a href="<?php echo base_url()?>user/import_csv">
                  <span class="pcoded-micon"><i class="fa fa-user fa-6" aria-hidden="true"></i><b>P</b></span>
                  <span class="pcoded-mtext">import clients</span>
                  </a>
               </li>
            </ul>
         </div>
      </nav>
      <?php 
         function time_elapsed_string($datetime, $full = false) {
         $now = new DateTime;
         $ago = new DateTime($datetime);
         $diff = $now->diff($ago);
         
         $diff->w = floor($diff->d / 7);
         $diff->d -= $diff->w * 7;
         
         $string = array(
             'y' => 'year',
             'm' => 'month',
             'w' => 'week',
             'd' => 'day',
             'h' => 'hour',
             'i' => 'minute',
             's' => 'second',
         );
         foreach ($string as $k => &$v) {
             if ($diff->$k) {
                 $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
             } else {
                 unset($string[$k]);
             }
         }
         
         if (!$full) $string = array_slice($string, 0, 1);
         return $string ? implode(', ', $string) . ' ago' : 'just now';
         }
         ?>  
         <?php 
         $noti=$this->db->query("select * from user where autosave_status='1' order by id desc")->result_array();
      foreach($noti as $key => $val){?>
      <div id="modaldelete<?php echo $val['id']?>" class="modal fade" role="dialog">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title" style="text-align-last: center">Notification Delete</h4>
               </div>
               <div class="modal-body">
                  <p>Are you sure want to delete?</p>
               </div>
               <div class="modal-footer">
                  <a href="<?php echo base_url().'user/delete/'.$val['id'];?>"><button type="button" class="btn btn-default">Delete</button></a>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
            </div>
         </div>
      </div>
      <?php }?>
      <?php if(isset($invoice_info['client_id'])){ ?>
      <div id="modalInvoicedelete<?php if(isset($invoice_info['client_id'])){ echo $invoice_info['client_id']; }?>" class="modal fade" role="dialog">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title" style="text-align-last: center">Notification Delete</h4>
               </div>
               <div class="modal-body">
                  <p>Are you sure want to delete?</p>
               </div>
               <div class="modal-footer">
                  <a href="<?php echo base_url().'invoice/DeleteInvoice/'.$invoice_info['client_id'];?>"><button type="button" class="btn btn-default">Delete</button></a>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
            </div>
         </div>
      </div>
      <?php } ?>
      <?php if(isset($Repeatinvoice_info['client_id'])){ ?>
      <div id="modalrepeatInvoicedelete<?php if(isset($Repeatinvoice_info['client_id'])){ echo $Repeatinvoice_info['client_id']; }?>" class="modal fade" role="dialog">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title" style="text-align-last: center">Notification Delete</h4>
               </div>
               <div class="modal-body">
                  <p>Are you sure want to delete?</p>
               </div>
               <div class="modal-footer">
                  <a href="<?php echo base_url().'invoice/DeleteRepeatInvoice/'.$Repeatinvoice_info['client_id'];?>"><button type="button" class="btn btn-default">Delete</button></a>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
            </div>
         </div>
      </div>
      <?php } ?>
      <?php //if($useremail['role'] == '1') { 
         if(count($invoice_noti)>0){                                   
         foreach ($invoice_noti as $invoice) { ?> 
      <div id="modalInvoiceBilldelete<?php echo $invoice['client_id']?>" class="modal fade" role="dialog">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title" style="text-align-last: center">Notification Delete</h4>
               </div>
               <div class="modal-body">
                  <p>Are you sure want to delete?</p>
               </div>
               <div class="modal-footer">
                  <a href="<?php echo base_url().'invoice/DeleteInvoice/'.$invoice['client_id'];?>"><button type="button" class="btn btn-default">Delete</button></a>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
            </div>
         </div>
      </div>
      <?php } }  

         if(count($invoice_notify)>0){                                   
         foreach ($invoice_notify as $invoice) { ?> 
      <div id="modalInvoiceBilldelete<?php echo $invoice['client_id']?>" class="modal fade" role="dialog">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title" style="text-align-last: center">Notification Delete</h4>
               </div>
               <div class="modal-body">
                  <p>Are you sure want to delete?</p>
               </div>
               <div class="modal-footer">
                  <a href="<?php echo base_url().'invoice/DeleteInvoice/'.$invoice['client_id'];?>"><button type="button" class="btn btn-default">Delete</button></a>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
            </div>
         </div>
      </div>
     <?php  } }  ?> 

    

      <?php if(count($repeatinvoice_noti)>0){
        foreach ($repeatinvoice_noti as $repeat_invoice) { ?>
      <div id="modalInvoiceRepBilldelete<?php echo $repeat_invoice['client_id']?>" class="modal fade" role="dialog">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title" style="text-align-last: center">Notification Delete</h4>
               </div>
               <div class="modal-body">
                  <p>Are you sure want to delete?</p>
               </div>
               <div class="modal-footer">
                  <a href="<?php echo base_url().'invoice/DeleteRepeatInvoice/'.$repeat_invoice['client_id'];?>"><button type="button" class="btn btn-default">Delete</button></a>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
            </div>
         </div>
      </div>
      <?php } } 

      if(count($repeatinvoice_notify)>0){ 
              foreach ($repeatinvoice_notify as $repeat_invoice) { ?>
            <div id="modalInvoiceRepBilldelete<?php echo $repeat_invoice['client_id']?>" class="modal fade" role="dialog">
               <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" style="text-align-last: center">Notification Delete</h4>
                     </div>
                     <div class="modal-body">
                        <p>Are you sure want to delete?</p>
                     </div>
                     <div class="modal-footer">
                        <a href="<?php echo base_url().'invoice/DeleteRepeatInvoice/'.$repeat_invoice['client_id'];?>"><button type="button" class="btn btn-default">Delete</button></a>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                     </div>
                  </div>
               </div>
            </div>
   <?php   }}    ?>
  <?php $fun = $this->uri->segment(2);?>
<div class="modal fade" id="manager_service_accept" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
        <input type="hidden" name="services_name" id="services_name" value="">
         <input type="hidden" name="service_user_id" id="service_user_id" value="">
         <input type="hidden" name="notifications_id" id="notifications_id" value="">
          <p>Do you want to accept Service Request?.</p>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="accept_service">Yes</button>
         <button type="button" class="btn btn-primary" id="send_proposal">Send Proposal</button>
          <button type="button" class="btn btn-default close_popup" data-dismiss="modal">No</button>
        </div>
      </div>
      
    </div>
  </div>
  <div class="modal fade" id="notification_modal" role="dialog">
    <div class="modal-dialog modal-notify">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">  
          <input type="hidden" name="notification_id" id="notification_id" value="">
          <input type="hidden" name="notification_type" id="notification_type" value="">
          <input type="hidden" name="notification_section" id="notification_section" value="">
          <input type="hidden" name="notification_users" id="notification_users" value="">
          <input type="hidden" name="SorceFrom" id="SorceFrom" value="">

          <p id="show_message">Do you want to accept Service Request?.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="confirm_button">Yes</button>
          <button type="button" class="btn btn-default close_popup" data-dismiss="modal">No</button>
        </div>
      </div>
      
    </div>
  </div>


<!-- 
  <div id="forward_task" class="modal fade" role="dialog">
<div class="modal-dialog">
 <div class="modal-content">
    <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
       <h4 class="modal-title" style="text-align-last: center">Assign to</h4>
    </div>
    <div class="modal-body">

        <input type="hidden" name="notification_id" id="fwd_notification_id" value="">
          <input type="hidden" name="notification_type" id="fwd_notification_type" value="">
          <input type="hidden" name="notification_section" id="fwd_notification_section" value="">
          <input type="hidden" name="notification_users" id="fwd_notification_users" value="">

           <?php 
      if($_SESSION['role']==5){
         $staff_list=$this->db->query("select * from  assign_manager where manager='".$_SESSION['id']."'")->row_array();
         $staff=$staff_list['staff'];
         if($staff==''){
          $staff=0;
         }
        $staff_form = $this->db->query('select * from user where id in ('.$staff.')')->result_array();

      }else{

             $staff_form = $this->Common_mdl->GetAllWithWheretwo('user','role','6','firm_admin_id',$_SESSION['id']);
      }

       $team = $this->db->query("select * from team where create_by=".$_SESSION['id']." ")->result_array();
       $department=$this->db->query("select * from department_permission where create_by=".$_SESSION['id']." ")->result_array();

       ?>
     
       <div class="dropdown-sin-21234">
          <select multiple placeholder="select" name="workers[]" id="workers" class="workers">
          
  <?php    


   if(count($staff_form)){ ?>
          <option disabled>Staff</option>
             <?php foreach($staff_form as $s_key => $s_val){ ?>
              <option value="<?php echo $s_val['id'];?>"   ><?php echo $s_val['crm_name'];?></option>
             <?php } 
             } ?>
      <?php if(count($team)){ ?>
          <option disabled>Team</option>
             <?php foreach($team as $s_key => $s_val){ ?>
              <option value="tm_<?php echo $s_val['id'];?>"  ><?php echo $s_val['team'];?></option>
             <?php } 
             } ?>
          <?php if(count($department)){ ?>
          <option disabled>Department</option>
             <?php foreach($department as $s_key => $s_val){ ?>
              <option value="de_<?php echo $s_val['id'];?>" ><?php echo $s_val['new_dept'];?></option>
             <?php } 
             } ?>
          </select>
       </div>
    </div>
    <div class="modal-footer profileEdit">
        <button type="button" class="btn btn-primary" id="forward">Save</button>
          <button type="button" class="btn btn-default close_popup" data-dismiss="modal">No</button>
    </div>
 </div>
</div>
</div> -->

  <div id="review_task" class="modal fade" role="dialog">
<div class="modal-dialog">
 <!-- Modal content-->
 <div class="modal-content">
    <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
       <h4 class="modal-title" style="text-align-last: center">Task</h4>
    </div>
    <div class="modal-body">
          <p class="task_info">Task Review by </p>


          <input type="hidden" name="review_task_id" id="review_task_id">
          <input type="hidden" name="review_notification_type" id="review_notification_type">
          <input type="hidden" name="review_notification_users" id="review_notification_users">
          <input type="hidden" name="review_notification_section" id="review_notification_section">
          <input type="hidden" name="review_notification_id" id="review_notification_id">
          <input type="hidden" name="review_notification_source" id="review_notification_source">
       </div>
    </div>
    <div class="modal-footer profileEdit">
        <button type="button" class="btn btn-primary" id="review_accept_btn">Accept</button>
         <button type="button" class="btn btn-primary" id="send_review_btn" style="<?php if($_SESSION['user_type']=='FA'){ echo 'display:none'; } ?>">Send Higher</button>
          <button type="button" class="btn btn-default close_popup" data-dismiss="modal">Close</button>
    </div>
 </div>
</div>




<div class="modal fade" id="manager_task_review" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>

        <div class="modal-body">
        <input type="hidden" id="review_notification_id">

         <p>Do you want to accept Service Request?.</p>
         <br>
         <b>Notes</b>
         <textarea id="review_response_notes"></textarea>
        </div>

        <div class="modal-footer" id="model_footer_button">
          <button type="button" class="btn btn-primary"  onclick="review_response('1')">Yes</button>
          <button type="button" class="btn btn-default" onclick="review_response('0')">No</button>
          <!-- <button type="button" class="btn btn-default close_popup" data-dismiss="modal" onclick="review_response(1)">No</button> -->
        </div>

      </div>
      
    </div>
  </div>


<div class="modal fade" id="popup-2" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Feedback Form</h4>
        </div>

        <div class="modal-body">
      <form action="<?php echo base_url();?>User/feedMail" id="feed_form" method="post" name="feed_form">
         <style>
            input.error {
            border: 1px dotted red;
            }
            label.error{
            width: 100%;
            color: red;
            font-style: italic;
            margin-left: 120px;
            margin-bottom: 5px;
            }
         </style>
  
         <label><b>Your content</b></label>
         <textarea name="feed_msg" id="feed_msg" placeholder="Type your text here..."></textarea>
         <!-- <br><br> -->  

         <div class="modal-footer">
          <div class="feedback-submit mail_bts1">
            <div class="feed-submit text-left">
               <input id="feed" name="submit" type="submit" value="submit">
            </div>
         </div>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>      
  
      </form>
    </div>
        
      </div>
      
    </div>
  </div>



  <div id="Confirmation_popup" class="modal fade" role="dialog" style="display:none;">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal">&times;</button>
             <h4 class="modal-title" style="text-align-last: center">Confirmation</h4>
          </div>
          <div class="modal-body">
             <label class="information"></label>
          </div>
          <div class="modal-footer">             
              <button class="Confirmation_OkButton">Save</button>
              <button class="Confirmation_CancelButton" data-dismiss="modal">Close</button>              
          </div>
        </div>
    </div>
  </div>



      <input type="hidden" name="user_id" id="user_id" value="<?php if(isset($client[0]['user_id']) && ($client[0]['user_id']!='') ){ echo $client[0]['user_id'];}?>" class="fields">
      <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script> 
      <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-ui/js/jquery-ui.min.js"></script> 
      <script type="text/javascript" src="<?php echo base_url();?>bower_components/popper.js/js/popper.min.js"></script>  
      <script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap/js/bootstrap.min.js"></script> 
      <!-- Required Jquery -->
  <!--     <script src="<?php echo base_url();?>assets/js/timezones.full.js"></script> -->
      <!-- j-pro js -->
      <!-- <script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.ui.min.js"></script> -->
      <script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.maskedinput.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.j-pro.js"></script>
      <!-- jquery slimscroll js -->
      <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
      <!-- modernizr js -->
      <script type="text/javascript" src="<?php echo base_url();?>bower_components/switchery/js/switchery.min.js"></script>
      <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> 
<!-- <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/fixedheader/3.1.2/js/dataTables.fixedHeader.min.js"></script> -->
   <!--    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-datetimepicker.min.js"></script> -->
    <!--   <script type="text/javascript" src="<?php echo base_url();?>assets/js/daterangepicker.js"></script> -->
      <script type="text/javascript" src="<?php echo base_url();?>assets/js/datedropper.min.js"></script>
      <!-- <script type="text/javascript" src="https://remindoo.uk/assets/js/masonry.pkgd.min.js"></script> -->
      <!-- Custom js -->
   <!--    <script src="<?php echo base_url();?>assets/pages/wysiwyg-editor/wysiwyg-editor.js"></script>  -->
      <script src="<?php echo base_url();?>assets/js/pcoded.min.js"></script> 
      <script src="<?php echo base_url();?>assets/js/demo-12.js"></script>
      <!-- <script type="text/javascript" src="<?php echo base_url();?>assets/js/masonry.pkgd.min.js"></script> -->
      <script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>
      <script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script> 
      <script type="text/javascript" src="<?php echo base_url();?>assets/js/common_script.js"></script>
      <script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
    <!--   <script type="text/javascript" src="<?php echo base_url();?>assets/js/custom-picker.js"></script>  -->
      <script src="<?php echo base_url();?>assets/js/mock.js"></script>
      <script src="<?php echo base_url();?>assets/js/hierarchy-select.min.js"></script>
<!-- <script src="https://use.fontawesome.com/86c8941095.js"></script> -->
  
    <?php $url = $this->uri->segment('2'); ?>
    <?php if ($url == 'step_proposal' || $url == 'copy_proposal' || $url == 'template_design' || $url=='templates') { ?>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <?php } ?>
    <script src="<?php echo base_url();?>assets/js/bootbox.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/debounce.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap-colorpicker.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap-slider.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.js"></script>
    <script src="https://cdn.jsdelivr.net/medium-editor/latest/js/medium-editor.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/creative.tools.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/html2canvas.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/image-edit.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/editor.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/email.css">
    <script src="https://cdn.ckeditor.com/4.11.1/standard-all/ckeditor.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>


  <script type="text/javascript">
$(document).ready(function (){

        $(".theme-loader").remove();
      
      /*importent for header link  popup */
          $(".dynamic_header").find("li a[href^='#']").attr('data-toggle',"modal");

          $(".dynamic_header").find("li a[href^='#']").click(function(){
            
            if($(this).attr('href').indexOf("#default-Modal") != -1 )
            {
              $('#searchCompany').prop('readonly',false).val('');
              $('#searchresult').text('').show();
              $('#companyprofile').text('');
              $('#selectcompany').hide();
              $('.all_layout_modal .close').show();        
            }

        });  


     $('.dropdown-sin-21234').dropdown({
         limitCount: 5,
      input: '<input type="text" maxLength="20" placeholder="Search">'
    });




      $('th').on("click.DT", function (e) {
      //stop Propagation if clciked outsidemask
      //becasue we want to sort locally here
        if (!$(e.target).hasClass('sortMask')) {
          e.stopImmediatePropagation();
        }
      });
 $(document).on("click",".close_notification",function(){})
});

function review_response(res)
{
  var id=$("#review_notification_id").val();
  var notes=$("#review_response_notes").val();
  $.ajax({
    url:"<?=base_url()?>user/manager_review_response",
    data:{"mn_id":id,"res":res,"notes":notes},
    type:"POST",
    success:function(res){location.reload();}
    });
}

$(document).ready(function() {

  $(document).on("click",".notifi_status",function(e){
      var task_id=$(this).data('task_id');
      var id=$(this).data('id');
     
      $.ajax({
        url: '<?php echo base_url();?>user/update_notification/',
              type: 'post',

              data: { 'id':id },
     
              success: function( data ){
               window.location.href = "<?php echo base_url()?>user/task_details/"+task_id;
              }
       });
    });

  $('.select-dropdown span input[type="checkbox"]').wrap( '<label class="custom_checkbox1"></label>');
$('.custom_checkbox1 input').after( "<i></i>" )

  
          CKEDITOR.editorConfig = function (config) {
          config.language = 'en';
          config.uiColor = '#fff';
          config.height = 300;
          config.toolbarCanCollapse = true;
          config.toolbarLocation = 'bottom';
          };
           CKEDITOR.replace('feed_msg'); 
//}catch(e){alert(e);}
        });
        </script>


    <script>
    var switcheryElm = [];
    $( document ).ready(function() {   

        var date = $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy',minDate:0 }).val();

        // Multiple swithces
        var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));

        var index = 1;
        elem.forEach(function(html) {
            var switchery = new Switchery(html, {
                color: '#22b14c',
                jackColor: '#fff',
                size: 'small'
            });
            //var index =((!html.getAttribute('id')) ? '1' : html.getAttribute('id') );
            ///console.log(html.getAttribute('id') );
            html.setAttribute('data-switcheryId',index);
            switcheryElm[index] = switchery;
            index++;
        });

        $('#accordion_close').on('click', function(){
                $('#accordion').slideToggle(300);
                $(this).toggleClass('accordion_down');
        });


        $('.chat-single-box .chat-header .close').on('click', function(){
          $('.chat-single-box').hide();
        });

    
    var resrow = $('#responsive-reorder').DataTable({
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        responsive: true
    });
});
$(".rejected_tsk").click(function(){
  $.ajax({
    url:"<?= base_url()?>user/rejected_tsk_seen_status/"+$(this).attr("data-id"),
    type:'get',
    success:function(){location.reload();}
  })
});
</script>
</body>
<!-- chat box -->
  <!-- modal 1-->
      <div class="modal fade all_layout_modal" id="default-Modal" tabindex="-1" role="dialog">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h4 class="modal-title">Search Companines House</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <div class="input-group input-group-button input-group-primary modal-search">

                     <input type="text" class="form-control" placeholder="Search here..." id="searchCompany">
                     <button class="btn btn-primary input-group-addon" id="basic-addon1">
                     <i class="ti-search" aria-hidden="true"></i>
                     </button>
                  </div>
                  <div class="british_gas1 trading_limited" id="searchresult">
                  </div>
                  <div class="british_view" style="display:none" id="companyprofile">                     
                  </div>
                  <div class="main_contacts" id="selectcompany" style="display:none">
                     <h3>british gas trading limited</h3>
                     <div class="trading_tables">
                        <div class="trading_rate">
                           <p>
                              <span><strong>BARBARO,Gab</strong></span>
                              <span>Born 1971</span>
                              <span><a href="#">Use as Main Contact</a></span>
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- modalbody -->
            </div>
         </div>
      </div>
        <!-- modal-close -->
        <!--modal 2-->
      <div class="modal fade all_layout_modals" id="exist_person" tabindex="-1" role="dialog">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h4 class="modal-title">Select Existing Person</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <div class="main_contacts" id="main_contacts" style="display:block">     
                  </div>
               </div>
               <!-- modalbody -->
            </div>
         </div>
      </div>
          <!-- modal-close -->
          <!-- modal 2-->
<div id="Confirmation_popup" class="modal fade" role="dialog" style="display:none;">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal">&times;</button>
             <h4 class="modal-title" style="text-align-last: center">Confirmation</h4>
          </div>
          <div class="modal-body">
             <label class="information"></label>
          </div>
          <div class="modal-footer">             
              <button class="Confirmation_OkButton">Save</button>
              <button class="Confirmation_CancelButton" data-dismiss="modal">Close</button>              
          </div>
        </div>
    </div>
  </div>

</html>
<script>
//class="modal fade all_layout_modal show"
function company_model_close(){
  //console.log('aaa');
    $('.all_layout_modal').modal('hide');
 //   $('button.close').trigger('click');
    $('body').removeClass('modal-open');
  $('.modal-backdrop').remove();

  $("#default-Modal").hide();
    // $('.modal-backdrop.show').hide();
  // $('.modal-backdrop.show').each(function(){
  //  $(this).remove();
  // });
}

function debounce(func, wait, immediate) {
  var timeout;
  return function() {
    var context = this, args = arguments;
    var later = function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};

$('#searchCompany').keyup(debounce(function(){
  $(".form_heading").html("<h2>Adding Client via Companies House</h2>");
        var $this=$(this);
        var term =$this.val() ;
        type_check(term);
    },500));

function type_check(term)
{ 
     $.ajax({
          url: '<?php echo base_url();?>client/SearchCompany/',
          type: 'post',
          data: { 'term':term },
          beforeSend : function()    {           
              $(".LoadingImage").show();
           },
          success: function( data ){
              $("#searchresult").html(data);
               $(".LoadingImage").hide();
              },
              error: function( errorThrown ){
                  console.log( errorThrown );
              }
          });
}


 // $("#searchCompany").keyup(function(){


 //  $(".form_heading").html("<h2>Adding Client via Companies House</h2>");
 //       var currentRequest = null;    
 //       var term = $(this).val();
 //       //var term = $(this).val().replace(/ +?/g, '');
 //       //var term = $(this).val($(this).val().replace(/ +?/g, ''));
 //       $("#searchresult").show();
 //       $('#selectcompany').hide();
 //        $(".LoadingImage").show();
 //         currentRequest = $.ajax({
 //          url: '<?php echo base_url();?>client/SearchCompany/',
 //          type: 'post',
 //          data: { 'term':term },
 //          beforeSend : function()    {           
 //          if(currentRequest != null) {
 //              currentRequest.abort();
 //          }
 //      },
 //          success: function( data ){
 //              $("#searchresult").html(data);
 //               $(".LoadingImage").hide();
 //              },
 //              error: function( errorThrown ){
 //                  console.log( errorThrown );
 //              }
 //          });
 //clearTimeout(myVar);
        //  setTimeout(function(){ checked(term); }, 3000);
 //     });





 function checked(term){
  $.ajax({
          url: '<?php echo base_url();?>client/SearchCompany/',
          type: 'post',
          data: { 'term':term },
          beforeSend : function()    {           
             $(".LoadingImage").show();
           },
          success: function( data ){
              $("#searchresult").html(data);
               $(".LoadingImage").hide();
              },
              error: function( errorThrown ){
                  console.log( errorThrown );
              }
          });

 }

function addmorecontact(id)
{
    $(".all_layout_modal").modal('hide');
    $(".LoadingImage").show();
    $(".modal-backdrop").css("display","none");
    var url = $(location).attr('href').split("/").splice(0, 8).join("/");

    var segments = url.split( '/' );
    var fun = segments[5];
    var values= '<?php echo $_SESSION['roleId']; ?>';
    //alert(values);
if(fun!='' && fun !='addnewclient'  && fun !='addnewclient#')
{

  <?php // if($_SESSION['roleId']=='1'){ ?>
   // $(location).attr('href', '<?php echo base_url();?>firm/add_firm/'+id);
 <?php //}else{ ?>

    $(location).attr('href', '<?php echo base_url();?>client/addnewclient/'+id);
<?php //} ?>
 

   
}  else{
    <?php // if($_SESSION['roleId']=='1'){ ?>
   // $(location).attr('href', '<?php echo base_url();?>firm/add_firm/'+id);
   <?php // }else{ ?>
   $(location).attr('href', '<?php echo base_url();?>client/addnewclient/'+id);
    <?php  //} ?>
   $("#companyss").attr("href","#");

$('.contact_form').show();
 $('.all_layout_modal').modal('hide');
 
     $('.nav-link').removeClass('active');
  //   $('.main_contacttab').addClass('active');
//$('.contact_form').show();

    $("#company").removeClass("show");
    $("#company").removeClass("active");
    $("#required_information").removeClass("active");
    $(".main_contacttab").addClass("active");
    $("#main_Contact").addClass("active");
    $("#main_Contact").addClass("show"); 
}      
                
  /* $("#companyss").attr("href","#");

$('.contact_form').show();
 $('.all_layout_modal').modal('hide');
 
     $('.nav-link').removeClass('active');
  //   $('.main_contacttab').addClass('active');
//$('.contact_form').show();

$("#company").removeClass("show");
$("#company").removeClass("active");
$("#required_information").removeClass("active");
 $(".main_contacttab").addClass("active");
$("#main_Contact").addClass("active");
$("#main_Contact").addClass("show");
*/
//$( "." ).tabs( { disabled: [1, 2] } );
  // $(".it3").attr("style", "display:none");
//$(".it3").tabs({disabled: true });
}

var xhr = null;
   function getCompanyRec(companyNo)
   {
      $(".LoadingImage").show();
       if( xhr != null ) {
                  xhr.abort();
                  xhr = null;
          }
       
      xhr = $.ajax({
          url: '<?php echo base_url();?>client/CompanyDetails/',
          type: 'post',
          data: { 'companyNo':companyNo },
          success: function( data ){
              //alert(data);
              $("#searchresult").hide();
              $('#searchCompany').prop('readonly',true);
              $('#companyprofile').show();
              $("#companyprofile").html(data);
              $(".LoadingImage").hide();
              
              },
              error: function( errorThrown ){
                  console.log( errorThrown );
              }
          });}


      function getCompanyView(companyNo)
   {
      //alert(companyNo);
      $('.modal-search').hide();
      $(".LoadingImage").show();
     var user_id= $("#user_id").val();
     
       if( xhr != null ) {
                  xhr.abort();
                  xhr = null;
          }
   
       
      xhr = $.ajax({
          url: '<?php echo base_url();?>client/selectcompany/',
          type: 'post',
          dataType: 'JSON',
          data: { 'companyNo':companyNo,'user_id': user_id},
          success: function( data ){
              //alert(data.html);
              $("#searchresult").hide();
              $('#companyprofile').hide();
              $('#selectcompany').show();
              $("#selectcompany").html(data.html);
              $('.main_contacts').html(data.html);
   // append form field value
   $("#company_house").val('1');
   //alert($("#company_house").val());
   $("#company_name").val(data.company_name);
   $("#company_name1").val(data.company_name);
   $("#company_number").val(data.company_number);
   $("#companynumber").val(data.company_number);
   $("#company_url").val(data.company_url);
   $("#company_url_anchor").attr("href",data.company_url);
   $("#officers_url").val(data.officers_url);
   $("#officers_url_anchor").attr("href",data.officers_url);
   $("#company_status").val(data.company_status);
   $("#company_type").val(data.company_type);

   // console.log('comapny type ');
   // console.log(data.company_type);
   // console.log('comapny type test ');
   //$("#company_sic").append(data.company_sic);
   $("#company_sic").val(data.company_sic);
   $("#sic_codes").val(data.sic_codes);
   
   $("#register_address").val(data.address1+"\n"+data.address2+"\n"+data.locality+"\n"+data.postal);
   $("#allocation_holder").val(data.allocation_holder);
   $("#date_of_creation").val(data.date_of_creation);
   $("#period_end_on").val(data.period_end_on);
   $("#next_made_up_to").val(data.next_made_up_to);
   $("#next_due").val(data.next_due);
   $("#accounts_due_date_hmrc").val(data.accounts_due_date_hmrc);
   $("#accounts_tax_date_hmrc").val(data.accounts_tax_date_hmrc);
   $("#confirm_next_made_up_to").val(data.confirm_next_made_up_to);
   $("#confirm_next_due").val(data.confirm_next_due);
   
   $("#tradingas").val(data.company_name);
   $("#business_details_nature_of_business").val(data.nature_business);
   $("#user_id").val(data.user_id);

 //  $(".edit-button-confim").show(); //for hide edit option button 
   $(".LoadingImage").hide();
              
              },
              error: function( errorThrown ){
                  console.log( errorThrown );
              }
          });}
   
   
       function getmaincontact(companyNo,appointments,i,user_id,obj)
   {
      $(".LoadingImage").show();

        var data = { 'companyNo':companyNo,'appointments': appointments,'count':i,'user_id':user_id};
        var exist_contact = 0;
        var modal = $(obj).closest('.all_layout_modal');

        if( modal.attr('id') == "company_house_contact" ) //check it is edit page popup
        {
          exist_contact = 1;
          var cnt = $("#append_cnt").val();

            if(cnt=='')
            {
              cnt = 1;
            }
           else
           {
            cnt = parseInt(cnt)+1;
           }
           data['cnt'] = cnt;
        $("#append_cnt").val(cnt);

        }
 
      xhr = $.ajax({
          url: '<?php echo base_url()?>client/maincontact/',
          type: 'post',
          data: data,
          success: function( data ){

            modal.find('#selectcompany #usecontact'+i).html('<span class="succ_contact"><a href="#">Added</a></span>');

            if(exist_contact)
            {

              $('.contact_form').append(data);
                    if(i=='First')
                    {
                           $(".contact_form .common_div_remove-"+cnt).find('.make_primary_section').trigger('click');
                    }
                  /** 05-07-2018 rs **/
                  $('.make_a_primary').each(function(){
                     var countofdiv=$('.make_a_primary').length;
                     if(countofdiv>1){
                     var id=$(this).attr('id').split('-')[1];

                      $('.for_remove-'+id).remove();
                       $('.for_row_count-'+id).append('<div class="btn btn-danger remove for_remove-'+id+'"><a href="javascript:void(0)" class="contact_remove" id="remove-'+id+'">Remove</a></div>');
                  }
                  });
                  var start = new Date();
              start.setFullYear(start.getFullYear() - 70);
              var end = new Date();
              end.setFullYear(end.getFullYear());
                $(".date_picker_dob").datepicker({ dateFormat: 'dd-mm-yy', changeMonth: true,
                  changeYear: true,yearRange: start.getFullYear() + ':' + end.getFullYear() }).val();
                modal.modal('hide');
                sorting_contact_person( $('.contact_form').find(".CONTENT_CONTACT_PERSON").filter(":last") );
              }
            $(".LoadingImage").hide();

                      },
              error: function( errorThrown ){
                  console.log( errorThrown );
              }
          });
    }
function backto_company()
{
   $('#companyprofile').show();
   $('.main_contacts').hide();
}
 
</script>

<script type="text/javascript">
   
    $(function(){
      
      var headheight = $('.header-navbar').outerHeight();
      var navbar = $('.remin-admin');
  });
   
</script>

<script type="text/javascript">
  $(document).ready(function(){    
      $("a#mobile-collapse").click(function () {
          $('#pcoded.pcoded.iscollapsed').toggleClass('addlay');
    });
     
      $(".nav-left .data1 li.comclass a.com12").click(function () {
      $(this).parent('.nav-left .data1 li.comclass').find('ul.dropdown-menu1').slideToggle();
      $(this).parent('.nav-left .data1 li.comclass').siblings().find('ul.dropdown-menu1:visible').slideUp();      

    });
    
    // $(document).on( 'click', 'li.column-setting0', function() {
    //   $(this).children('ul.dropdown-menu.hide-dropdown01').slideToggle();
    // });

    // var $grid = $('.masonry-container').masonry({
    //   itemSelector: '.accordion-panel',
    //   percentPosition: true,
    //   columnWidth: '.grid-sizer' 
    // });

    // $(document).on( 'click', 'td.splwitch .switchery', function() {
    //   setTimeout(function(){ $grid.masonry('layout'); }, 600);
    // });

    // $(".basic-border1 input").keypress(function(){
    //     setTimeout(function(){ $grid.masonry('layout'); }, 600);
    // });

    //   $(document).on( 'click', 'ol .nav-item a,.nav-item a', function() {
    //       setTimeout(function(){ $grid.masonry('layout'); }, 600);
    //   });

    //   $(document).on( 'change', '.main-pane-border1', function() {
    //     setTimeout(function(){ $grid.masonry('layout'); }, 600);
    //   });

    //   $(document).on( 'click', '.btn', function() {
    //     setTimeout(function(){ $grid.masonry('layout'); }, 600);
    //    });

    //   $(document).on( 'click', '.switching', function() {
    //     setTimeout(function(){ $grid.masonry('layout'); }, 600);
    //    });

    //   $(document).on( 'click', '.service-table-client .checkall', function() {
    //     setTimeout(function(){ $grid.masonry('layout'); }, 600);
    //    });

    //   $(document).on( 'click', '.service-table-client .switchery', function() {
    //     setTimeout(function(){ $grid.masonry('layout'); }, 600);
    //    });

    //   setTimeout(function(){ $grid.masonry('layout'); }, 600);
    
      var test = $('body').height();
      $('test').scrollTop(test);

   });

  $(".client_request").click(function(){
    //alert('ok');
    $("#services_name").val($(this).data('service'));
    $("#service_user_id").val($(this).attr('id'));
    $("#notifications_id").val($(this).data('id'));

    console.log($(this).data('service'));
     console.log($(this).attr('id'));
      console.log($(this).data('id'));
    $("#manager_service_accept").modal('show');
  });
$("#enable_reassign").change(function(){
  if($(this).prop("checked"))
  {   $(".review_notes").hide();
     $("#reassign_selectbox").show();
  }else{
     $("#reassign_selectbox").hide();
     $(".review_notes").show();
  } 
  });

  $(".staff_request").click(function(){
    //alert('ok');
    $("#review_notification_id").val($(this).data('id'));
  });

  $("#accept_service").click(function(){
    //alert('ok');
    var services_name=$("#services_name").val();
   var user_id=$("#service_user_id").val();
   var id=$("#notifications_id").val();
   var data={'user_id':user_id,'service_name':services_name,'id':id}

   $.ajax({
       url: '<?php echo base_url();?>Sample/service_update/',
    type: "POST",
    data: data,
    success: function(data)  
    {
      if(data=='1'){
        $(".close_popup").trigger('click');
      }

    }
   });
  });


  $("#send_proposal").click(function(){
        var services_name=$("#services_name").val();
   var user_id=$("#service_user_id").val();
   var id=$("#notifications_id").val();
   var data={'user_id':user_id,'service_name':services_name,'id':id}

   $.ajax({
       url: '<?php echo base_url();?>Sample/proposal_add/',
    type: "POST",
    data: data,
    success: function(data)  
    {
      if(data!=' '){
        $(".close_popup").trigger('click');
        $(".proposal_success_message").show();
         $('.header_notify').load("<?php echo base_url(); ?>sample/content_update/");
         $(".sectionsss").load("<?php echo base_url(); ?>sample/header_update/");
        setTimeout(function(){ $(".proposal_success_message").hide(); }, 3000);
      }
    }
   });
  });


$(".companyhouse_update").click(function(){

  source = $(this).closest('.media').find('a.data_source_element');

var id=source.attr('id');
//alert(id);
var companyname=source.data('companyname');
//alert(companyname);
var companynumber=source.data('companynumber');
//alert(companynumber);
var incorporationdate=source.data('incorporationdate');
//alert(incorporationdate);
var registeraddress=source.data('registeraddress');
//alert(registeraddress);
var companytype=source.data('companytype');
//alert(companytype);

var accounts_next_made_up_to=source.data('accounts_next_made_up_to');
//alert(companynumber);
var accounts_next_due=source.data('accounts_next_due');
//alert(incorporationdate);
var confirmation_next_made_up_to=source.data('confirmation_next_made_up_to');
//alert(registeraddress);
var confirmation_next_due=source.data('confirmation_next_due');
//alert(companytype);

var formData={'id':id,'companyname':companyname,'companynumber':companynumber,'incorporationdate':incorporationdate,'registeraddress':registeraddress,'companytype':companytype,'accounts_next_made_up_to':accounts_next_made_up_to,'accounts_next_due':accounts_next_due,'confirmation_next_made_up_to':confirmation_next_made_up_to,'confirmation_next_due':confirmation_next_due }

$.ajax({
    url: '<?php echo base_url(); ?>/client/companyhouse_update',
    type : 'POST',
    data : formData,                    
      beforeSend: function() {
        $(".LoadingImage").show();
      },
      success: function(data) {
        if(data=='1'){
          location.reload();
        }
      }
    });

});


$(document).on("click","#remove-notification,#archive-notification,#forward-notification",function(){
  var type=$(this).data('type');
  var id=$(this).data('id');
  var section=$(this).data('section');
  var users=$(this).data('user');
  var source  = $(this).data('source');

  /*console.log(id);
  console.log(section);
  console.log(users);*/
  if(type=="remove"){
    $("#confirm_button").removeAttr('class'); 
    $("#show_message").html('Do you want to remove notification?');
    $("#confirm_button").addClass('btn btn-primary remove_notify');
    $("#notification_id").val(id);
    $("#notification_modal").modal('show');   
    $("#notification_type").val(type);
    $("#notification_section").val(section);
    $("#notification_users").val(users);
    $("#SorceFrom").val(source);
  }else if(type=="archive"){
    $("#confirm_button").removeAttr('class');
    $("#show_message").html('Do you want to archive notification?');
    $("#confirm_button").addClass('btn btn-primary archive_notify');
    $("#notification_id").val(id);
    $("#notification_modal").modal('show');
    $("#notification_type").val(type);
    $("#notification_section").val(section);
    $("#notification_users").val(users);
    $("#SorceFrom").val(source);
  }else if(type=="read"){
    $("#confirm_button").removeAttr('class');
    $("#show_message").html('Do you want to forward notification?');
    $("#confirm_button").addClass('btn btn-primary read_notify');
    $("#notification_id").val(id);
    $("#notification_modal").modal('show');
    $("#notification_type").val(type);
    $("#notification_section").val(section);
    $("#notification_users").val(users);
    $("#SorceFrom").val(source);
  }else{
    $("#forward").removeAttr('class');
    $("#show_message").html('Do you want to forward notification?');
    $("#forward").addClass('btn btn-primary forward_notify');
    $("#fwd_notification_id").val(id);
    $("#forward_task").modal('show');
    $("#fwd_notification_type").val(type);
    $("#fwd_notification_section").val(section);
    $("#fwd_notification_users").val(users);
    $("#SorceFrom").val(source);
  }
});


$(document).on("click","#review-notification",function(){

  // var type=$(this).data('type');
  var task_id=$(this).data('task_id');
   var task_ifo=$(this).data('task_info');
   $('#review_task_id').val(task_id);
  $('.task_info').html('').html(task_ifo);

        var type=$(this).data('type');
        var id=$(this).data('id');
        var section=$(this).data('section');
        var users=$(this).data('user');
        var source  = $(this).data('source');


      $("#review_notification_type").val(type);
      $("#review_notification_section").val(section);
   $("#review_notification_users").val(users);
    $("#review_notification_id").val(id);
  $("#review_notification_source").val(source);



  //alert(task_id);
  // var section=$(this).data('section');
  // var users=$(this).data('user');
  // var source  = $(this).data('source');

  
});


$(document).on('click','.remove_notify,.archive_notify,#confirm_button,.read_notify',function(){
 //  alert('ok');
    var type   =$("#notification_type").val();
    var section=$("#notification_section").val();
    var users  =$("#notification_users").val();
    var id     =$("#notification_id").val();
    var source = $("#SorceFrom").val();

 /* console.log(type);
    console.log(section);
    console.log(users);
    console.log(id);*/

    if(section=='company-house' && type=="remove"){
/*
      console.log($("#remove-notification").attr('class'));
      console.log($("#remove-notification").parents('.company_notification').attr('class'));
      console.log( $("#remove-notification").parents('.company_notification').find('.companyhouse_update').attr('class'));*/
      $("#remove-notification").parents('.company_notification').find('.companyhouse_update').trigger('click');
    }else{
  
    var formData={'type':type,'section':section,'users':users,'id':id,'source':source};
    $.ajax({
    url: '<?php echo base_url(); ?>Sample/notification_section',
    type : 'POST',
    data : formData,                    
      beforeSend: function() {
        $(".LoadingImage").show();
      },
      success: function(data) {
        if(data=='1'){
         /*****/    $('.header_notify').load("<?php echo base_url(); ?>sample/content_update/");
             $(".sectionsss").load("<?php echo base_url(); ?>sample/header_update/");          
          //location.reload();
        } 
        $('#notification_modal .close_popup').trigger('click');

        if(users!="" && (section == 'proposal' || section == 'deadline' || section == 'invoice' || section == 'repeat-invoice'))
        { 
            if(section == 'proposal')
            {
              <?php $_SESSION['firm_seen'] = 'accept'; ?>
            }
            window.location.href = users;
        }
         $(".LoadingImage").hide();
      }
    });
  }
});



$(document).on('click','.forward_notify',function(){
    var worker=$("#workers").val();
    var task_id=$("#fwd_notification_id").val();
      var data = {};
      data['task_id'] = task_id;
      data['worker'] = worker;
      $(".LoadingImage").show();
        $.ajax({
               type: "POST",
               url: "<?php echo base_url();?>user/update_assignees/",
               data: data,
               success: function(response) {
                  // alert(response); die();
                  $(".LoadingImage").hide();
               //$('#task_'+id).html(response);
             //  $('.task_'+id).html(response);
                    $.ajax({
               type: "POST",
               url: "<?php echo base_url();?>sample/update_task_view/",
               data: {'id':task_id},
               success: function(response) {

                /*****/    $('.header_notify').load("<?php echo base_url(); ?>sample/content_update/");

                $(".sectionsss").load("<?php echo base_url(); ?>sample/header_update/");
  $(".LoadingImage").hide();
                 //     $('#action_result').show();
                 //  $("#action_result .position-alert1").html('Success !!! Staff Assign have been changed successfully...');
                 // setTimeout(function(){ $('#action_result').hide();location.reload(); }, 1500);

                  }

                });



               
               },
            });
});
</script>

<?php $uri_seg_menu = $this->uri->segment('1'); 


//echo $this->uri->segment(2);
if($this->uri->segment(2)!='proposal' || $this->uri->segment(2)!='proposal_feedback'){
//if($_SESSION['roleId']=='4'){ // only for client login users.
//if($_SESSION['roleId']=='1' || $_SESSION['roleId']=='4' ){ //remove 12 and put 4 
if( strtolower($uri_seg_menu)=='tickets'){ // REMOVED BY AJITH  $uri_seg_menu=='task_list' || 
  ?>  
<?php

             $login_user_id=$_SESSION['userId'];
        if($_SESSION['user_type']=='FU' || $_SESSION['user_type']=='FA')
        {
           
          
            $client_data=$this->db->query("SELECT * FROM user where user_type='FU' AND autosave_status!='1' and crm_name!='' and id=".$login_user_id." order by id DESC")->result_array();
           
        
            $data['getalladmin']=$this->db->query("SELECT * FROM user where crm_name!='' and user_type='FA' and firm_id=".$_SESSION['firm_id']." order by id DESC")->result_array();
            $user_val=array('-1');

          


                  $ques=Get_Assigned_Datas('CLIENT');
                foreach ($ques as $key => $value) {
                 $res1=Get_Module_Assigees( 'CLIENT' , $value );
                 if (in_array($_SESSION['userId'], $res1))
         
                  {
                    $user_val[]=$value;
                    }
                 }


        

                   $task_user_val=$this->db->query("SELECT * FROM client where id in (".implode(',',$user_val).")  order by id DESC")->result_array();
                   $task_user_val = array_column($task_user_val,'user_id');
                  // $task_user_val = implode(',',$task_user_val);

             
                    $data['getallstaff']=array();
                    $data['getallUser']=array();
          
                    
          
            $newarray=array_values(array_unique($task_user_val));
            if(!empty($newarray)){

                $data['getallUser']=$this->db->query("SELECT * FROM user where id in (".implode(',',$newarray).") and firm_id='".$_SESSION['firm_id']."' and user_type='FC' order by id DESC")->result_array();

                $data['getallstaff']=$this->db->query("SELECT * FROM user where  firm_id='".$_SESSION['firm_id']."' and user_type!='FC' and user_type!='SA' order by id DESC")->result_array();

               

                if($_SESSION['user_type']=='FA')
                {

                    $data['getallUser']=$this->db->query("SELECT * FROM user  where firm_id='".$_SESSION['firm_id']."' and user_type='FC' order by id DESC")->result_array();

                   $data_superadmin=$this->db->query("SELECT * FROM user where id ='1' and user_type!='FC' order by id DESC")->row_array();

                  $data['getallstaff'][]= $data_superadmin;
                }

               
            }
           
        }else{
            $data['getallstaff']=array();
            $data['getallUser']=array();
            $data['getalladmin']=$this->db->query("SELECT * FROM user where crm_name!='' and user_type='FA' and firm_id=".$_SESSION['firm_id']." order by id DESC")->result_array();
            $data['getallUser']=$this->db->query("SELECT * FROM user where id='".$login_user_id."'  and firm_id='".$_SESSION['firm_id']."' and user_type='FC' order by id DESC")->result_array();

        }

        
        $data['column_setting']=$this->Common_mdl->getallrecords('column_setting_new');
        $data['client'] =$this->db->query('select * from column_setting_new where id=1')->row_array();
        $data['feedback'] =$this->db->query("select id,username,crm_email_id from user where username!='' and username!='0' and  crm_email_id!='' and crm_email_id!='0' "  )->result();



         $this->load->view('User_chat/chat_page_new',$data); 
      
       ?>

 <script type="text/javascript">
$(document).ready(function(){
  //alert('zzz');
//$('#main-chat #sidebar .user-box .userlist-box').trigger('click');
//$('#main-chat .chat-box').find('.chat-single-box .mini i').trigger('click');
//alert($('#main-chat .chat-box').find('.chat-single-box .mini').length+".trigger('click')");
//BY AJITH FOr show all chat box 
});
</script>
<?php } 
//} 
} ?>

<?php 


if($this->uri->segment(2)!='task_list'){
/** for admin login users **/
//if($_SESSION['roleId']=='1'){ 
if(strtolower($uri_seg_menu)!='user_chat'){

  if($_SESSION['roleId']=='1' || $_SESSION['roleId']=='4' || $_SESSION['roleId']=='6'){ 
  // if($uri_seg_menu!='user_chat' && $uri_seg_menu!='Firm_dashboard' && $uri_seg_menu!='leads' && $uri_seg_menu!='proposal') {
  //  if($uri_seg_menu=='Deadline_manager'){
    

   if($_SESSION['roleId']==1)
        {
            $login_user_id=$_SESSION['userId'];
     

        $data['getallUser']=$this->db->query("SELECT * FROM user where role='4' AND autosave_status!='1' and crm_name!='' and firm_admin_id=".$login_user_id." order by id DESC")->result_array();

         $user_staff_data=$this->db->query("SELECT * FROM user where role='6' AND autosave_status!='1' and crm_name!='' and firm_admin_id=".$login_user_id." order by id DESC")->result_array();
        
        $leads_data=$this->db->query("SELECT * FROM leads where user_id=".$login_user_id." order by id DESC")->result_array();
        $taskdata=$this->db->query("SELECT * FROM add_new_task where create_by=".$login_user_id." order by id DESC")->result_array();
$assign='';$worker='';
$user_staff=array();
                foreach ($leads_data as $leads_key => $leads_value) {
                    if($leads_value['assigned']!=''){
                        $assign.=$leads_value['assigned'].",";
                    }

                }
                foreach ($taskdata as $task_key => $task_value) {
                    if($task_value['worker']!=''){
                        $worker.=$task_value['worker'].",";
                    }
                }
                foreach ($user_staff_data as $user_staff_key => $user_staff_value) {
                    //user_staff
                    array_push($user_staff,$user_staff_value['id']);
                   
                }
                $leadstask=array_values(array_unique(array_merge(array_filter(explode(',',$assign)),array_filter(explode(',', $worker)))));
                $newarray=array_values(array_unique(array_merge(array_filter($leadstask),array_filter($user_staff))));

                if(!empty($newarray)){
                $data['getallstaff']=$this->db->query("SELECT * FROM user where role='6' AND autosave_status!='1' and crm_name!='' and id in (".implode(',',$newarray).") order by id DESC")->result_array();
              }
              else
              {
                $data['getallstaff']='';
              }

               // echo "SELECT * FROM user where role='6' AND autosave_status!='1' and crm_name!='' and id in (".implode(',',$newarray).") order by id DESC";

        }
        if($_SESSION['roleId']==4)
        {
            $login_user_id=$_SESSION['userId'];
            $client_data=$this->db->query("SELECT * FROM user where role='4' AND autosave_status!='1' and crm_name!='' and id=".$login_user_id." order by id DESC")->result_array();
           
             $firm_id=$client_data[0]['firm_admin_id'];
         
            $data['getalladmin']=$this->db->query("SELECT * FROM user where crm_name!='' and id=".$firm_id." order by id DESC")->result_array();

            /** extra added for staff shown **/
        $data['getallUser']=$this->db->query("SELECT * FROM user where role='4' AND autosave_status!='1' and crm_name!='' and firm_admin_id=".$login_user_id." order by id DESC")->result_array();

         $user_staff_data=$this->db->query("SELECT * FROM user where role='6' AND autosave_status!='1' and crm_name!='' and firm_admin_id=".$login_user_id." order by id DESC")->result_array();
        
        $leads_data=$this->db->query("SELECT * FROM leads where user_id=".$login_user_id." order by id DESC")->result_array();
        $taskdata=$this->db->query("SELECT * FROM add_new_task where create_by=".$login_user_id." order by id DESC")->result_array();
$assign='';$worker='';
$user_staff=array();
                foreach ($leads_data as $leads_key => $leads_value) {
                    if($leads_value['assigned']!=''){
                        $assign.=$leads_value['assigned'].",";
                    }

                }
                foreach ($taskdata as $task_key => $task_value) {
                    if($task_value['worker']!=''){
                        $worker.=$task_value['worker'].",";
                    }
                }
                foreach ($user_staff_data as $user_staff_key => $user_staff_value) {
                    //user_staff
                    array_push($user_staff,$user_staff_value['id']);
                   
                }
                $leadstask=array_values(array_unique(array_merge(array_filter(explode(',',$assign)),array_filter(explode(',', $worker)))));
                $newarray=array_values(array_unique(array_merge(array_filter($leadstask),array_filter($user_staff))));

                if(!empty($newarray)){
                $data['getallstaff']=$this->db->query("SELECT * FROM user where role='6' AND autosave_status!='1' and crm_name!='' and id in (".implode(',',$newarray).") order by id DESC")->result_array();
                }
                else
                {
                   $data['getallstaff']='';
                }
                /** for staff end **/

        }

        if($_SESSION['roleId']==6)
        {
            $login_user_id=$_SESSION['userId'];
            $client_data=$this->db->query("SELECT * FROM user where role='6' AND autosave_status!='1' and crm_name!='' and id=".$login_user_id." order by id DESC")->result_array();
           
             $firm_id=$client_data[0]['firm_admin_id'];
         
            $data['getalladmin']=$this->db->query("SELECT * FROM user where crm_name!='' and id=".$firm_id." order by id DESC")->result_array();

            $leads_data=$this->db->query("SELECT * FROM leads where find_in_set('".$login_user_id."',assigned)  order by id DESC")->result_array();
            $taskdata=$this->db->query("SELECT * FROM add_new_task where find_in_set('".$login_user_id."',worker) order by id DESC")->result_array();
            $for_assigned=array();
            foreach ($leads_data as $leads_key => $leads_value) {
               array_push($for_assigned, $leads_value['user_id']);
            }
            foreach ($taskdata as $task_key => $task_value) {
               array_push($for_assigned, $task_value['create_by']);
            }
            $newarray=array_values(array_unique($for_assigned));
            if(!empty($newarray)){
                $data['getallUser']=$this->db->query("SELECT * FROM user where id in (".implode(',',$newarray).")  order by id DESC")->result_array();
            }
            else
            {
                $data['getallUser']='';
            }

        }

        // if($this->uri->segment(2)=='chat_page'){
        //     $this->load->view('User_chat/chat_page_admin',$data); 
        // }

        /** for newly added 18-05-2018 **/
    //  }
  }

}}

/** 09-07-2018 for permission **/
//$this->load->view('includes/footer_permission_script');

/** end of permission **/

?>

<script type="text/javascript">
  $(document).ready(function(){
      $('.dataTables_wrapper').each(function(){
var datatable_id = $(this).attr('id');
var datatable_id_cur = datatable_id.replace('_wrapper','');
if($(this).find('.dataTables_scrollBody').prop("scrollHeight") > $(this).find('.dataTables_scrollBody').height()){
    $(this).find('.dataTables_scrollHeadInner').css({"padding-right": "21px", "min-width": "calc(100% - 21px)"}); 
}
$('#'+ datatable_id_cur).DataTable().columns.adjust();
setTimeout(function(){$('#'+ datatable_id_cur).DataTable().columns.adjust();}, 1000);
});


// $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
// var target_id = $(this).attr('href');
// var href = target_id.substring(target_id.indexOf("#") + 1); 
// var datatable_id = $("#"+href).find('table').attr('id');
// var datatable_id_cur = datatable_id.replace('_wrapper','');
// if($(this).find('.dataTables_scrollBody').prop("scrollHeight") > $(this).find('.dataTables_scrollBody').height()){
//     $(this).find('.dataTables_scrollHeadInner').css({"padding-right": "21px", "min-width": "calc(100% - 21px)"}); 
// }
// $('#'+ datatable_id_cur).DataTable().columns.adjust();
// setTimeout(function(){$('#'+ datatable_id_cur).DataTable().columns.adjust();}, 1000);

// });

});
</script>

<script type="text/javascript">
  $(document).ready(function(){
      $(document).on('show.bs.modal', '.modal', function (event) {
            var zIndex = 1040 + (10 * $('.modal:visible').length);
            $(this).css('z-index', zIndex);
            setTimeout(function() {
                $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
            }, 0);
        });

      // $('.modal').modal({backdrop: 'static', keyboard: false,show: false});

    $(document).on('click', '.modal .hasDatepicker', function(){
    $('.ui-datepicker').addClass('uidate_visible');
    });

    $(document).on('click', '.modal button[data-dismiss="modal"]', function(){
    $('.ui-datepicker').removeClass('uidate_visible');
    });
  });

  $(document).ready(function(){
      $(".modal-alertsuccess").each(function(){ 
          if($(this).find('.newupdate_alert').length < 1){
            $(this).wrapInner( "<div class='newupdate_alert'></div>");
          } 
      });
    /*importent for datatable scroll*/
    $(document).ready(function(){
      $('table.dataTable').wrap('<div class="scroll_table"></div>');

    $(document).on('click', '.multiselect .selectBox', function(e) {
    e.stopImmediatePropagation();
    $('#checkboxes').show();
    });

    $('.dataTables_wrapper .toolbar-table').addClass('select_visibles');

    });
    /*important for datatable scroll*/

    $(document).mouseup(function(e) {
    var container = $(".multiple-select-dropdown");
    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) 
    {
        //console.log('ok');
        $('.themicond').removeClass('shown');
        $(".multiple-select-dropdown").removeClass('Show_content');
    }

    });
    $(document).on('click','.dt-button-background',function(e){
         $('.dt-buttons').find('.dt-button-collection').detach();
          $('.dt-buttons').find('.dt-button-background').detach();
    });

    $('th .themicond').on('click', function(e) {
        if ($(this).find(".dropdown-content").hasClass("Show_content")) {
          alert('showcontent is visible');
        $(this).parent().find('.dropdown-content').removeClass('Show_content');
    }
    });
 

      $('.modal').on("shown.bs.modal", function() {
           var winheight1 = $(this).find('.modal-dialog').outerHeight();
          
           if(winheight1 < 400) {
                   $(this).find('.modal-dialog').addClass('normal_heights');
            }
      }); 

      $('.modal').on("hide.bs.modal", function() {
           var winheight1 = $(this).find('.modal-dialog').outerHeight();
          
           if(winheight1 < 400) {
                   $(this).find('.modal-dialog').removeClass('normal_heights');
            }
      });           
    

  });
var Conf_Confirm_Popup =   function(conf){

    if( 'OkButton' in conf ) 
    {
      var text = ('Text' in conf.OkButton   ? conf.OkButton.Text : 'Yes' );
      var fun = ('Handler' in conf.OkButton ? conf.OkButton.Handler : function(){} );

      $('#Confirmation_popup .Confirmation_OkButton').html(text).unbind().on('click', fun );

    }

     if( 'CancelButton' in  conf ) 
    {
      var text = ('Text' in conf.CancelButton ? conf.CancelButton.Text : 'No' );
      var fun = ('Handler' in  conf.CancelButton ? conf.CancelButton.Handler : function(){} );
    
      $('#Confirmation_popup .Confirmation_CancelButton').html(text).unbind().on('click', fun);
    }

     if( 'Info' in conf )
    {
      $('#Confirmation_popup .information').html( conf.Info );
    }

  };
  var Show_LoadingImg = function()
  {
    $('.LoadingImage').show();
  };
  var Hide_LoadingImg = function()
  {
    $('.LoadingImage').hide();
  };


    $(document).on('click','#send_review_btn',function(){

      // alert('ccc');
       var rec_id = $('#review_task_id').val();
        var  task_id=[];
       task_id.push(rec_id);
         $.ajax({
        type: "POST",
        url: "<?php echo base_url().'user/manager_notify';?>",        
        data: {task_id:task_id,},
        beforeSend: function() {

          $(".LoadingImage").show();
        },
        success: function(data) {
          $(".LoadingImage").hide();

          $('#review_task .close_popup').trigger('click');

               $(".header_popup_info_msg").show();
               $('.header_popup_info_msg .header_position-alert1').html('').html('Success !!! Task  have been Send successfully...');
               
          
                
                //TaskTable_Instance.cell( cell ).invalidate().draw();


               setTimeout(function () {
                $(".header_popup_info_msg").hide();
                }, 1500);
        }
      });


    });

  $(document).on('click','#review_accept_btn',function(){

    var type   =$("#review_notification_type").val();
    var section=$("#review_notification_section").val();
    var users  =$("#review_notification_users").val();
    var id     =$("#review_notification_id").val();
   var source  =$("#review_notification_source").val();


         var rec_id = $('#review_task_id').val();

             $.ajax({
            url: '<?php echo base_url();?>user/task_statusChange/',
            type: 'post',
            data:{'rec_id':rec_id,'status':'complete',},
            
            beforeSend:function(){$(".LoadingImage").show();},
            success: function( data ){
             
               $(".LoadingImage").hide();

               var formData={'type':type,'section':section,'users':users,'id':id,'source':source};
            $.ajax({
            url: '<?php echo base_url(); ?>Sample/notification_section',
            type : 'POST',
            data : formData,                    
              beforeSend: function() {
                $(".LoadingImage").show();
              },
              success: function(data) {
                if(data=='1'){
                 /*****/    $('.header_notify').load("<?php echo base_url(); ?>sample/content_update/");
                     $(".sectionsss").load("<?php echo base_url(); ?>sample/header_update/");
                   $(".LoadingImage").hide();
                  //location.reload();
                }
              }
            });
                  $('#review_task .close_popup').trigger('click');

               $(".header_popup_info_msg").show();
               $('.header_popup_info_msg .header_position-alert1').html('').html('Success !!! Task status have been changed successfully...');
               
          
                
                //TaskTable_Instance.cell( cell ).invalidate().draw();


               setTimeout(function () {
                $(".header_popup_info_msg").hide();
                }, 1500);
              // $('#review_task .close_popup').trigger('click');
             
                },
                error: function( errorThrown ){
                    console.log( errorThrown );
                }
            });  

});
</script>