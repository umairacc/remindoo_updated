<?php $this->load->view('includes/client_header'); ?>
<link href="<?php echo base_url(); ?>assets/css/jquery.filer.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/colreorder/1.5.1/css/colReorder.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/buttons.dataTables.min.css?ver=2">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/dataTables.css">
<style type="text/css">
  .fa-eye
  {
    color:#24adbf !important;
    font-size:20px !important;
  }

  .google-visualization-table-tr-head 
  {
    background-color: #c0c0c0 !important;
  }
  
</style>
<div class="pcoded-content pcode-mydisk">
<div class="pcoded-inner-content">

  <div class="modal-alertsuccess alert alert-success-delete" style="display:none;">
   <div class="newupdate_alert">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <div class="pop-realted1">
           <div class="position-alert1">
              YOU HAVE SUCCESSFULLY DELETED                              
           </div>
        </div>
     </div>  
     </div>  

<!-- Main-body start -->
<div class="main-body myDesk client_panel_wrapper">
   <div class="page-wrapper">
      <div class="card desk-dashboard">
         <div class="inner-tab123" >
          <div class="common-reduce01 floating_set mainbgwhitecls">
          <div class="graybgclsnew button_visibility">
            <div class="deadline-crm1 floating_set">
              <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard" id="proposal_dashboard">
                  <li class="nav-item basic checking active">                    
                      <a href="javascript:void(0);" class="nav-link active"  data-tag="Basics">Basics</a>
                  </li>                                
                  <li class="nav-item proposals checking">
                      <a href="javascript:void(0);" class="nav-link"  data-tag="Proposals">Proposals</a>
                  </li>
                  <li class="nav-item checking">
                      <a href="javascript:void(0);" class="nav-link deads"  data-tag="Deadline-Manager">Deadline Manager</a>
                  </li>
                   <li class="nav-item reminder checking">
                      <a href="javascript:void(0);" class="nav-link"  data-tag="Reminder">Reminder</a>
                  </li>
              </ul>
              <div class="delbuttoncls">
                 <button type="button" id="delete_proposal_records" class="delete_proposal_records del-tsk12 f-right" data-toggle="modal" data-target="#delete_all_proposal" style="display: none;">Delete</button>
              </div>
          </div>

          <div class="tab-content">        

          <div id="Basics" class="proposal_send tab-pane fade in active">
             <div class="floating_set space-disk1">
                <div class="col-md-12">
                   <div class="card">
                      <div class="card-block user-detail-card">
                         <div class="row">
                            <div class="col-sm-4">
                               <div class="height-profile">
                                  <div class="height-profile1"> <?php  $getUserProfilepic = $this->Common_mdl->getUserProfilepic($user_details['id']); ?><img src="<?php echo $getUserProfilepic;?> " alt="" class="img-fluid p-b-10"></div>
                               </div>                                   
                            </div>
                            <div class="col-sm-8 user-detail">
                               <div class="row">
                                  <div class="col-sm-3">
                                     <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-user"></i>Name :</h6>
                                  </div>
                                  <div class="col-sm-9">
                                     <h6 class="m-b-30"><?php echo ucwords($user_details['crm_name']);?></h6>
                                  </div>
                               </div>
                               <div class="row">
                                  <div class="col-sm-3">
                                     <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-email"></i>User ID</h6>
                                  </div>
                                  <div class="col-sm-9">
                                     <h6 class="m-b-30"><a href="javascript:void(0);"><?php echo '#'.$user_details['id']; ?></a></h6>
                                  </div>
                               </div>                             
                               <div class="row">
                                  <div class="col-sm-3">
                                     <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-touch-phone"></i>Phone :</h6>
                                  </div>
                                  <div class="col-sm-9">
                                     <h6 class="m-b-30"> <?php echo $user_details['crm_phone_number']; ?></h6>
                                  </div>
                               </div>
                               <div class="row">
                                  <div class="col-sm-3">
                                     <h6 class="f-w-400 m-b-30"><i class="icofont icofont-web"></i>Website :</h6>
                                  </div>
                                  <div class="col-sm-9">
                                     <h6 class="m-b-30"><a href="<?php echo $user_details['crm_business_website']; ?>" ><?php echo $user_details['crm_business_website']; ?></a></h6>
                                  </div>
                               </div>
                            </div>
                         </div>
                      </div>
                   </div>
                </div>
                <!-- Summery Start -->
             
                <!-- summary end -->
             </div>
             <!-- pro summary -->
          </div>         
          <div id="Proposals" class="tab-pane fade client_view_proposal">
              <div class="floating_set space-disk1">
                 <div class="col-md-12 col-xl-12 commonctab card">
                    <div class="card-block user-detail-card">                       
                        <table id="display_service1" class="display nowrap" style="width:100%">
                        <thead>
                        <tr>
                        <th class="delete-annuals1 proposal_chk_TH Exc-colvis">
                              <label class="custom_checkbox1">
                              <input class="border-checkbox" type="checkbox" id="check-box"> <i></i></label>
                        </th>
                        <th class="sno_TH">S No<div class="sortMask"></div></th> 
                        <th class="proposalname_TH">Proposal Name <div class="sortMask"></div></th>        
                        <th class="proposal_from_TH">Proposal From<div class="sortMask"></div> </th>    
                        <th class="bill_amount_TH">Bill Amount<div class="sortMask"></div> </th> 
                        <th class="proposal_date_TH">Proposal Date<div class="sortMask"></div></th>
                        <th class="status_TH">Status<div class="sortMask"></div></th>
                        <th class="action_TH Exc-colvis"><strong>Action</strong></th>     
                      </tr>
                      </thead>       
                   <tbody class="lead-annual">
                   <?php
                   $i=1;
                    foreach($proposals as $value){ 
                       $random_string = $this->Common_mdl->generateRandomString();
                    ?>                        
                     <tr id="<?php echo $value['id']; ?>">
                       <td class="delete-annuals1" style="padding-left: 10px;">
                           <label class="custom_checkbox1"> <input class="border-checkbox proposal_list" type="checkbox" id="checkbox_<?php echo $i; ?>" name="checkbox[]" value="<?php echo $value['id']; ?>"><i></i></label>                            
                         </td>
                         <td class="delete-annuals3"><?php echo $i; ?></td>
                         <td class="delete-annuals" data-search="<?php echo ucwords($value['proposal_name']); ?>">
                           <?php echo ucwords($value['proposal_name']); ?> 
                         </td>
                         <td class="delete-annuals3" data-search="<?php echo ucwords($this->Common_mdl->get_price('firm','firm_id',$value['firm_id'],'crm_company_name')); ?>"><?php echo ucwords($this->Common_mdl->get_price('firm','firm_id',$value['firm_id'],'crm_company_name')); ?></td>
                         <td class="delete-annuals3" data-search="<?php echo $value['grand_total']." ".$value['currency']; ?>"><?php echo $value['grand_total']." ".$value['currency']; ?></td>  
                         <td class="delete-annuals3" data-search="<?php echo date('d M,Y',strtotime($value['created_at'])); ?>">
                            <p><?php echo date('d M,Y',strtotime($value['created_at'])); ?></p>
                         </td>   
                         <td class="delete-annuals3" data-search="<?php echo $value['status']; ?>"><?php echo ucfirst($value['status']); ?></td>                           
                         <td>
                          <div class="sentclsbuttonbot">                                     
                             <a href="<?php echo base_url().'Proposal/proposal/'.$random_string.'---'.$value['id']; ?>"><i class="fa fa-eye fa-6" aria-hidden="true"></i></a>
                             <a href="javascript:;" data-toggle="modal" data-target="#delete_all_proposal" id="proposal_<?php echo $value['id']; ?>" class="delete_ico"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true"></i></a>
                          </div>
                         </td>                 
                      </tr>
                      <?php $i++; } ?>
                      </tbody>
                      </table>
                  </div>
                  </div>
              </div>              
          </div>
          <div id="Deadline-Manager" class="tab-pane fade">
               <div class="job-card">  
               <?php 
                
               if(count($service_list)>0)
               {
                 $deadline_chart = array();
                 $icpfont_class = array('icofont-presentation-alt st-icon bg-c-yellow','icofont-files st-icon bg-c-blue','icofont-paper st-icon bg-c-green','icofont-file-document st-icon bg-c-pink','icofont-file-text st-icon bg-c-yellow','icofont-file-document st-icon bg-c-blue');
                 $columns_check = array('1'=> 'crm_confirmation_statement_due_date','2' =>'crm_ch_accounts_next_due','3'=>'crm_accounts_tax_date_hmrc','4'=>'crm_personal_due_date_return','5' => 'crm_vat_due_date','6' => 'crm_rti_deadline','7' => 'crm_pension_subm_due_date', '8' => '', '9' => '','10'=>'crm_next_booking_date','11' => 'crm_next_p11d_return_due','12' => 'crm_next_manage_acc_date','13' => 'crm_insurance_renew_date','14'=>'crm_registered_renew_date','15'=>'crm_investigation_end_date','16' => 'crm_investigation_end_date'); 
                 $count = 0;

                 foreach ($service_list as $key => $value) {  

                 if($count/6=='1'){ $count = 0; }
                 ?>
                  
                  <div class="col-md-12 col-xl-4">
                     <div class="card widget-statstic-card">
                        <div class="card-header">
                           <div class="media">
                              <div class="media-body media-middle">
                                 <div class="company-name">
                                    <p><!-- <a href="<?php echo base_url().'Deadlines/'.$value['id']; ?>"> --><?php echo $value['service_name']; ?><!-- </a> --></p>
                                 </div>
                              </div>
                           </div>
                        </div> 
                        <div class="card-block">
                           <i class="icofont <?php echo $icpfont_class[$count]; ?>"></i>
                           <ul class="text-muted">
                             <?php
                            
                            if(!empty($oneyear))
                            {     
                               if(!empty($oneyear[$columns_check[$value['id']]])){    

                                  $deadline_chart[$value['service_name']]['date'] = (strtotime($oneyear[$columns_check[$value['id']]])) !='' ?  date('Y-m-d', strtotime($oneyear[$columns_check[$value['id']]])): '';                            
                                ?>
                                <li><?php (strtotime($oneyear[$columns_check[$value['id']]])) !='' ? $date = date('Y-m-d', strtotime($oneyear[$columns_check[$value['id']]])): $date = '-'; echo "Due Date : ".$date; ?></li>
                            <?php }  else{

                             $deadline_chart[$value['service_name']]['date'] = "";
                                 } }

                             $deadline_chart[$value['service_name']]['amount_due'] = $this->Common_mdl->getServiceCharge($value['id'],$_SESSION['client_uid']);   
                             ?>     
                                      
                           </ul>
                        </div>                    
                     </div>
                  </div>       

                <?php $count++; }  ?>

                  <!-- fullwidth map -->
                  <div class="floating_set set-timenone">
                  <div class="desk-dashboard widmar floating_set mydisk-realign1">
                     <div class="card-header">
                        <h5>Deadlines</h5>
                     </div>
                    
                     <div class="card-block deadline">
                        <div id="columnchart_values" style="width: 100%; height: 350px;"></div>
                     </div>
                
                  </div>
                  <!-- fullwidth map-->
                </div>
                  <!--tab end-->

                <?php }else{ ?>
                   <div class="col-md-12 col-xl-12">
                      <div class="card widget-statstic-card">
                         <div class="card-header">
                          No Services Enabled.
                         </div>
                       </div>
                     </div>
                <?php } ?>  
             </div>
            
          </div>
          <div id="Reminder" class="tab-pane fade client_view_proposal">
            <div class="floating_set space-disk1">
               <div class="col-md-12 col-xl-12 commonctab card">
                  <div class="card-block user-detail-card"> 
            <table class="table client_table1 text-center display nowrap" id="allreminder" cellspacing="0" width="100%">
              <thead>
                <tr class="text-uppercase">
                  <th>S.no</th>
                  <th>Reminder Subject</th>
                  <th>Reminder Message</th>                      
                  <th>Reminder Sent on</th>              
                </tr>
              </thead>
              <tbody>
                <?php $i=1; foreach ($reminder_details as $key => $value) {
                
                  ?>
                <tr id="<?php echo $value['id']; ?>">  
                <td><?php echo $i; ?>                  
                  <td><?php echo $value['reminder_subject'];?></td>
                  <td><?php echo $value['reminder_message'];?></td>
                  <td><?php echo date('d-m-Y',strtotime($value['created_at']));?></td>                         
                </tr>
                <?php $i++; } ?>
              </tbody>
            </table>
          </div>
          </div>
         </div>
         </div>
         </div>

     </div>
 </div>
</div>
</div>
</div>

<div class="modal fade" id="delete_all_proposal" role="dialog">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Confirmation</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="delete_ids" id="delete_ids" value="">
        <p>Do you want to delete ?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default delete_yes">Yes</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
      </div>
    </div>
    
  </div>
</div>

<?php $this->load->view('includes/client_footer'); ?>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url()?>assets/js/custom/buttons.colVis.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>
<script src="<?php echo base_url()?>assets/js/custom/datatable_extension.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

  var display_service1;
  var allreminder;

  $(document).on('click', '.Settings_Button', AddClass_SettingPopup);
  $(document).on('click', '.Button_List', AddClass_SettingPopup );

  $(document).on('click','.dt-button.close',function()
  {
    $('.dt-buttons').find('.dt-button-collection').detach();
    $('.dt-buttons').find('.dt-button-background').detach();
  });

  function AddClass_SettingPopup()
  {
    $('button.Button_List').closest('.dt-button-collection').addClass('Settings_List');
    $('button.buttons-columnVisibility').closest('.dt-button-collection').addClass('columnVisibility_List');
  }

 $(document).ready(function()
 {  
     $('.pull-left .nav-item a').click(function(){
         $('.pull-left .nav-item a').removeClass('active');
         $(this).addClass('active');
         var tagid = $(this).data('tag');
         $('.tab-pane').removeClass('active').addClass('hide');
         $('#'+tagid).addClass('active').removeClass('hide');
     });
     
       
     display_service1 = $('#display_service1').DataTable(
     {
        "dom": '<"toolbar-table" B>lfrtip',
            buttons: 
            [
             {
                 extend: 'collection',
                 background:false,
                 text: '<i class="fa fa-cog" aria-hidden="true"></i>',
                 className:'Settings_Button',
                 buttons:[
                         { 
                           text: '<i class="fa fa-filter" aria-hidden="true"></i>Reset Filter',
                           className:'Button_List',
                           action: function ( e, dt, node, config )
                           {
                               Trigger_To_Reset_Filter('display_service1');
                           }
                         },
                         {
                             extend: 'colvis',
                             text: '<i class="fa fa-cogs" aria-hidden="true"></i>Column Visibility',              
                             columns: ':not(.Exc-colvis)',
                             className:'Button_List',
                             prefixButtons:[
                             {text:'X',className:'close'}]
                         }
                 ]
             }
           ],
           order: [],//ITS FOR DISABLE SORTING   
         
           fixedHeader: 
           {
             header: true,
             footer: true
           },
          
           colReorder: 
           {
             realtime: false,         
             fixedColumnsLeft : 2,
             fixedColumnsRight:1

           },
         
      });  

      allreminder = $('#allreminder').DataTable(
      {
         "dom": '<"toolbar-table" B>lfrtip',
             buttons: 
             [
              {
                  extend: 'collection',
                  background:false,
                  text: '<i class="fa fa-cog" aria-hidden="true"></i>',
                  className:'Settings_Button',
                  buttons:[
                          { 
                            text: '<i class="fa fa-filter" aria-hidden="true"></i>Reset Filter',
                            className:'Button_List',
                            action: function ( e, dt, node, config )
                            {
                                Trigger_To_Reset_Filter('allreminder');
                            }
                          },
                          {
                              extend: 'colvis',
                              text: '<i class="fa fa-cogs" aria-hidden="true"></i>Column Visibility',              
                              columns: ':not(.Exc-colvis)',
                              className:'Button_List',
                              prefixButtons:[
                              {text:'X',className:'close'}]
                          }
                  ]
              }
            ],
            order: [],//ITS FOR DISABLE SORTING   
          
            fixedHeader: 
            {
              header: true,
              footer: true
            },
           
            colReorder: 
            {
              realtime: false,         
              fixedColumnsLeft : 2,
              fixedColumnsRight:1

            },
          
       });  


      function  Trigger_To_Reset_Filter(table)
      {            
         $('div #'+table+'_filter').find('input[type="search"]').val('');

         if(table == 'display_service1')
         {
            Redraw_Table(display_service1);
         }
         else if(table == 'allreminder')
         {
            Redraw_Table(allreminder);
         }                   
      }  

  });

    $(document).ready(function()
    {
      window.allproposal = []; 
    });

    function getSelectedRow(id)
    {              
       allproposal.push(id);   
       allproposal = $.unique(allproposal); 
       return allproposal;
    }

    function return_indice(id)
    { 
       for(var i=0;i<allproposal.length;i++)
       {
          if(id == allproposal[i])
          {
             return i;
          }          
       }
    }

    function dounset(id)
    {  
       var indice = return_indice(id);
        
       if(indice > -1) 
       {
          allproposal.splice(indice,1);
       }
   
       return allproposal;
    }

   $('.custom_checkbox1 input[type="checkbox"]').click(function()
   {  
       var wholeid;
       var whole_id;
       var all_prop;

       if($(this).attr('id')) 
       {      
         whole_id = $(this).attr('id');
         wholeid = whole_id.search('check-box'); 
       }       

       if(wholeid != 0)
       { 
           var id = $(this).val();

           if($(this).is(":checked"))
           { 
              all_prop = getSelectedRow(id); 
              $("#delete_ids").val(JSON.stringify(all_prop));     
           }
           else
           { 
              var props = dounset(id); 
              $("#delete_ids").val(JSON.stringify(props)); 

              if($('#check-box').is(':checked'))
              {
                 $('#check-box').prop('checked',false);
              } 
           }   

           if(allproposal.length>0)
           {
              $('#delete_proposal_records').show();
           }
           else
           {
              $('#delete_proposal_records').hide();
           }
       }
       else if(wholeid == 0)
       { 
          var tbl = $('#'+whole_id).parent('label').parent('th').parent('tr').parent('thead').parent('table').attr('id');

          if($('#'+whole_id).is(':checked'))
          { 
             $('#delete_proposal_records').show();             

             $('#'+tbl+' tbody tr').each(function(index)
             {
                 if($('#'+tbl+' tbody tr').is(':visible'))
                 {
                   $(this).find('.custom_checkbox1 input[type="checkbox"]').prop('checked',true);
                   var tr_id = $(this).attr('id');
                   all_prop = getSelectedRow(tr_id);          
                 }        
             });

             $("#delete_ids").val(JSON.stringify(all_prop));
          }
          else
          { 
             $('#'+tbl+' tbody tr').each(function(index)
             {
                 if($('#'+tbl+' tbody tr').is(':visible'))
                 {
                   $(this).find('.custom_checkbox1 input[type="checkbox"]').prop('checked',false);
                 }
             });

             allproposal = [];
             $("#delete_ids").val('');
             $('#delete_proposal_records').hide();
          }
       }         
   });  

    $(document).on('click','.delete_ico', function()
    {
        var id = $(this).attr('id');
        id = id.split('_');        
        $("#delete_ids").val(JSON.stringify(id[1].trim()));
    });

    $(document).on('click','.delete_yes' , function() 
    {    
         var prop=$("#delete_ids").val();
         var selected_proposal_values = prop;  

         $.ajax(
         {
             type: "POST",
             url: "<?php echo base_url().'Overview/deleteProposal';?>",
             cache: false,
             data: {'data_id':selected_proposal_values},

             beforeSend: function() 
             {
               $(".LoadingImage").show();
             },
             success: function(data) 
             {
               $(".LoadingImage").hide();
               
               if(data == 1)  
               {
                  $(".alert-success-delete").show();
                  setTimeout(function(){location.reload(); }, 2000);     
               }  
             }
         });
   });

   <?php 

       $deadline_data = '';

       if(!empty($deadline_chart))
       {        
          $deadline_data = '[';

          foreach ($deadline_chart as $key => $value) 
          {
             if($value['date'] != "")
             {
                $deadline_data .= "['".$key."','".date('F d,Y',strtotime($value['date']))."','".$value['amount_due']." ".$firm_currency."'],";
             }
             else
             {
                $deadline_data .= "['".$key."','','".$value['amount_due']." ".$firm_currency."'],";
             }
          }

          $com_pos = strrpos($deadline_data, ',');
          $deadline_data = substr_replace($deadline_data, '', $com_pos);
          $deadline_data .= ']';
       }
   ?>
  
   google.charts.load('current', {'packages':['table']});
   google.charts.setOnLoadCallback(drawChart);
  
  function drawChart() 
  {
       var data = new google.visualization.DataTable();    
       data.addColumn('string','Services');
       data.addColumn('string', 'Deadline');                     
       data.addColumn('string', 'Amount Due'); 
       var arr = <?php echo $deadline_data; ?>;   
       data.addRows(arr);     

       // Set chart options      

       var chart = new google.visualization.Table(document.querySelector('#columnchart_values'));         
       chart.draw(data, {
           showRowNumber: true,
           height: '50%',
           width: '100%',
           allowHtml: true
       });
  }


</script>