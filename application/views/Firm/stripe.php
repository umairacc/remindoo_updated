<?php $this->load->view('super_admin/superAdmin_header'); ?> 
<style type="text/css">
	.form-error
	{
		color: red;
	}
</style>
<div class="modal-alertsuccess alert succ popup_info_msg" style="<?php if(empty($_SESSION['msg'])){ echo "display: none"; } ?>">
   <div class="newupdate_alert"><a href="#" class="close" id="close_info_msg">×</a>
   <div class="pop-realted1">
   <div class="position-alert1"> 
   <?php if($_SESSION['msg']!=""){ echo $_SESSION['msg']; } ?>      
   </div>
   </div>
   </div>
</div>
<div class="pcoded-content card-removes firmlisttop">
 <div class="pcoded-inner-content">
    <!-- Main-body start -->
    <div class="main-body">
       <div class="page-wrapper">
          <!-- Page body start -->
          <div class="page-body">
              <div class="row">
                <div class="col-sm-12">
                   <!-- Register your self card start -->
                   <div class="card">                      

	                 <div class="all_user-section floating_set clientredesign select-check">
	                    <div class="deadline-crm1 floating_set">
	                       <ul class="nav nav-tabs1 all_user1 md-tabs pull-left u-dashboard">
                          	<li class="nav-item">
                          	   <a class="nav-link active" href="javascript:void(0);">Stripe Settings</a>
                          	   <div class="slide"></div>
                          	</li>                                           
	                       </ul>
	                    </div>
	                 </div>

	                 <div class="all_user-section floating_set pros-editemp"> 
	                    <div class="all_user-section2 floating_set">
	                       <div class="tab-content">
	                          <div id="allusers" class="tab-pane fade in active">            
	                             <div class="client_section3 table-responsive">     
	                                <div class="all-usera1 data-padding1 ">               
	                                 <div class="">
                              
	                                 <form action="<?php echo base_url().'Firm/keys_update'; ?>" class="require-validation add" method="post">
	                                 <div class=" suballemailclsnew">    
	                                 	  <!--start-->
	                                 	<div class="col-sm-12">
	                                 	<!-- Register your self card start -->
	                                 	<div class="col-12 card">
	                                 	<!-- admin start-->
	                                 	<div class="client_section user-dashboard-section1 floating_set newmailclsnew">     
	                                           
                                       <div class="columns">
                                       	 <div class="edit-tempsect col-md-12 tempsectcls1">	
                             		    
                             		    	 <div class="newemailset">
                             		    	 	<div class="">          
                             		    	 	<label>Publish Key</label>
                             		    	 	</div>
                             		    	 	<input type="text" name="publish_key" class="form-control" data-validation="required" value="<?php if(!empty($stripe->publish_key)){ echo $stripe->publish_key; } ?>">
                             		    	 </div>

                                       <div class="newemailset">
                                        <div class="">          
                                        <label>Secret Key</label>
                                        </div>
                                        <input type="text" name="secret_key" class="form-control" data-validation="required" value="<?php if(!empty($stripe->secret_key)){ echo $stripe->secret_key; } ?>">
                                       </div>                     		    	

	                                 	    </div>
	                                             <!-- Page body end -->
	                                       </div>
	                                       </div>
	                                       <!-- Main-body end -->
	                                       <div id="styleSelector"></div>
	                                 </div>
	                                 
	                                 <button type="submit" class="btn btn-primary saveclsset" name="save">Save</button>
	                           
	                                 </form>
	                                     
	                                </div>
	                                </div>                     
	                             </div>
	                          </div>
	                       </div>
	                    </div>
	                 </div>                       
                 </div>
             </div>
         </div>
     </div>
 </div>
</div>
</div>
</div>

<?php $this->load->view('super_admin/superAdmin_footer',['INCLUDE'=>['DT_JS']]);?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.79/jquery.form-validator.js"></script>
<script>
  $.validate({
    lang: 'en'
  });  

 $(document).on('click','#close_info_msg',function()
 { 
    $('.popup_info_msg').hide();  
 });

</script>