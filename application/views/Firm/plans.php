<?php $this->load->view('super_admin/superAdmin_header'); ?>  
  
 <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  

  <div class="modal-alertsuccess alert" <?php if(empty($_SESSION['flash_msg'])){ echo 'style="display:none;"'; } ?>>
    <div class="newupdate_alert">
     <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
     <div class="pop-realted1">
        <div class="position-alert1">
          <?php if(!empty($_SESSION['flash_msg'])){ echo $_SESSION['flash_msg']; } ?>
         </div>
        </div>
     </div>
  </div>

   <div class="modal fade" id="deleteconfirmation" role="dialog">
     <div class="modal-dialog">    
       <!-- Modal content-->
       <div class="modal-content">
         <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal">&times;</button>
           <h4 class="modal-title">Confirmation</h4>
         </div>
         <div class="modal-body">
         <input  type="hidden" name="delete_template_id" id="delete_template_id" value="">
           <p> Are you sure want to delete ?</p>
         </div>
         <div class="modal-footer">
           <a type="button" class="btn btn-default" data-dismiss="modal" id="yes">Yes</a>
           <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
         </div>
       </div>
     </div>
  </div>      

  <div class="pcoded-content card-removes firmlisttop">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
                <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card">
                        <!-- admin start-->
                        <div class="client_section user-dashboard-section1 floating_set">
                              
                              </div> <!-- all-clients -->

                           <!-- clients details content -->

                           <div class="all_user-section floating_set clientredesign select-check">
                              <div class="deadline-crm1 floating_set">
                                 <ul class="nav nav-tabs1 all_user1 md-tabs pull-left u-dashboard">
                                    <li class="nav-item">
                                       <a class="nav-link active" href="javascript:;">All Plans</a>
                                       <div class="slide"></div>
                                    </li>                        
                                 </ul>                              

                               <div class="setting-realign1">
                               <div class="count-value1 cv pull-right">                         
                               <a class="btn btn-primary" href="<?php echo base_url().'firm/add_plan'; ?>">Add Plan</a>                                    
                               </div>                                                                   
                               </div> 
                             </div>                               
                              
                              <!-- table content Open -->
                              <div class="all_user-section2 floating_set">
                              
                                 <div class="tab-content table_content data_resetfilter">
                                   <div id="allusers" class="tab-pane fade in active">
                                       <div class="client_section3 table-responsive">                                          
                                          <div class="alluser_al data-padding1 ">    
                                             <div class="insert_firm_table">
                                             <table id="plans">
                                              <thead>
                                               <tr>
                                                 <th>S.No <div class="sortMask"></div></th>
                                                 <th>Plan Name <div class="sortMask"></div></th>
                                                 <th>Subscription Amount <div class="sortMask"></div></th>
                                                 <th>Plan Status <div class="sortMask"></div></th>
                                                 <th>Tax Amount<div class="sortMask"></div></th>
                                                 <th>Plan Type <div class="sortMask"></div></th>
                                                 <th>Action <div class="sortMask"></div></th>
                                               </tr>
                                              </thead>
                                              <tbody>
                                               <?php if(count($plans)>0){
                                                   $s = 0;
                                                   foreach ($plans as $key => $value) { ?>
                                                    <tr>
                                                    <td><?php echo ++$s; ?></td>
                                                    <td><?php echo ucwords($value['plan_name']); ?></td>
                                                    <td><?php echo $value['plan_amount'].' '.$currency; ?></td>
                                                    <td><?php if($value['status'] == '1'){ echo "Active"; }else{ echo "Inactive"; } ?></td>
                                                    <td><?php echo $value['tax'].' '.$currency; ?></td>
                                                    <td><?php if($value['type'] == '1'){ echo "Monthly"; }else{ echo "Yearly"; } ?></td>
                                                    <td>
                                                      <a href="<?php echo base_url().'firm/edit_plan/'.$value['id']; ?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>    
                                                      <a data-toggle="modal" data-target="#deleteconfirmation" data-href="<?php echo base_url().'firm/delete_plan/'.$value['id']; ?>" onclick="get_confirm(this);"><i class="fa fa-trash fa-6" aria-hidden="true"></i></a>
                                                    </td> 
                                                     </tr>                                                   
                                                <?php   
                                                   } }
                                                ?>
                                              </tbody>                                             
                                             </table>
                                             </div>
                                          </div>
                                       </div>
                                  </div>
                                  </div>
                                </div>
                               <!-- END OF  table content Open -->
                           </div>
                            <!-- clients details content -->

                           <!-- Register your self card end -->
                        </div>
                     </div>
                  </div>
                  <!-- Page body end -->
               </div>
            </div>
            <!-- Main-body end -->  
            <div id="styleSelector">
            </div>
         </div>
      </div>
   </div>

<?php $this->load->view('super_admin/superAdmin_footer',['INCLUDE'=>['DT_JS']]);?>

<script type="text/javascript">

  var table;

  $(document).ready(function()
  {
     $('#plans').DataTable();
  });

  function get_confirm(obj)
  {
     var href = $(obj).data('href');      
     $('#deleteconfirmation').find('#yes').attr('href',href);   
  }

  $(document).on('click','#yes',function()
  {
     var url = $(this).attr('href'); 
     window.location.href = url;
  });

</script>