<?php
 $this->load->view('super_admin/superAdmin_header');

 $Company_types = array('LTD' => 'Private Limited company', 'PLTD' => 'Public Limited company', 'LLP' => 'Limited Liability Partnership', 'P`ship' => 'Partnership', 'S.A'=>'Self Assessment','TT'=>'Trust','Charity'=>'Charity','other'=>'Other');
?>

  <link href="<?php echo base_url();?>assets/css/timepicki.css" rel="stylesheet">
  
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/buttons.dataTables.min.css?ver=2">
  
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/colreorder/1.5.1/css/colReorder.dataTables.min.css">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/jquery.tagsinput.css">

  <style type="text/css">
    

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #fff;
    min-width: 86px;
    overflow: auto;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 999;
    left: -92px;
    width: 150px;
}
.count-value1 button#bulk_archive_button{
  background-image: url(<?php echo base_url();?>'assets/images/archive.png');
  background-color: #ffc90e;
}
.tableToolBar_selectDiv{
  width: 250px !important;
}
  </style>

  <!-- show info  -->
  <div class="modal-alertsuccess alert info_popup" style="display:none;">
    <div class="newupdate_alert">
     <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
     <div class="pop-realted1">
        <div class="position-alert1 info-text">
         </div>
        </div>
     </div>
  </div>
  <!-- end show info  -->

  <div class="pcoded-content card-removes firmlisttop firm-super-admin">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
                <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card">
                        <!-- admin start-->
                        <div class="client_section user-dashboard-section1 floating_set">
                              
                              </div> <!-- all-clients -->

                           <!-- clients details content -->

                           <div class="all_user-section floating_set clientredesign select-check">
                              <div class="deadline-crm1 floating_set">
                                 <ul class="nav nav-tabs1 all_user1 md-tabs pull-left u-dashboard">
                                    <li class="nav-item">
                                       <a class="nav-link active" href="javascript:;"><img class="themicon" src="<?php echo base_url();?>assets/images/tabicon1.png" alt="themeicon" />All Firm</a>
                                       <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" href="<?php echo base_url();?>Firm/add_firm"><img class="themicon" src="<?php echo base_url();?>assets/images/tabicon2.png" alt="themeicon" />New Firm</a>
                                       <div class="slide"></div>
                                    </li>
                                                                 
                                 </ul>



                                 

                                   
                                   <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard don1 renewdesign">
                                   <?php 
                                   $i =1;
                                   foreach ($Firms_Status as $key => $value)
                                   { 
                                      if($i==1)
                                      {
                                        $active = 'active';
                                      }
                                      else
                                      {
                                        $active = '';
                                      }

                                     ?>
                                    <li class="nav-item">
                                       <a class="nav-link color<?php echo $i;?> tap_click <?php echo $active;?>" href="javascript:void(0);"  data-id="<?php echo $key;?>">
                                       <?php echo $value['label'];?>
                                       <span><?php echo $value['count']; ?></span>
                                       </a>
                                       <div class="slide"></div>
                                    </li>

                                   
                                   <?php
                                    $i++;
                                    }
                                   ?>
                                    
                                 </ul>
                                   <div class="count-value1 pull-right userlist-count01 ">
                                  
                                    <button type="button" id="bulk_delete_button"  class="del-tsk12 f-right"
                                     style="display:none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i>Delete</button> 

                                     <button type="button" id="bulk_archive_button"   class="del-tsk12 f-right" style="display:none;" ><i class="fa fa-archive fa-6" aria-hidden="true"></i>Archive</button>

                                     <!-- <button type="button" id="send_mail_sms" data-toggle="modal" data-target="#popup_send_mail_sms" data-child-class="user_checkbox"  class="del-tsk12 f-right" style="display:none;" ><i class="fa fa-paper-plane-o" aria-hidden="true"></i>Send</button>

                                     <button type="button" id="block_service" data-toggle="modal" data-target="#popup_service_block" data-child-class="user_checkbox" class="del-tsk12 f-right" style="display:none"><i class="fa fa-ban" aria-hidden="true"></i>Service Block</button>

                                     <button type="button" id="block_reminder" data-toggle="modal" data-target="#popup_reminder_block" data-child-class="user_checkbox" class="del-tsk12 f-right" style="display:none"><i class="fa fa-ban" aria-hidden="true"></i>Reminder Block</button> -->

                                     <button type="button" id="bulk_unarchive_button"  class="del-tsk12 f-right" style="display:none;" ><i class="fa fa-archive fa-6" aria-hidden="true"></i>Un Archive</button>


                                    <!-- <button id="deleteActive_records" class="deleteActive_records" style="display:none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i> Delete</button> -->
                                 </div>
                                 
                              </div>                             
                              
                              <!-- table content Open -->
                              <div class="all_user-section2 floating_set">
                              
                                 <div class="tab-content table_content data_resetfilter">
                                   <div id="allusers" class="tab-pane fade in active">
                                       <div id="task"></div>
                                       <div class="client_section3 table-responsive  button_visibility super_button-visible">
                                          <div class="status_succ"></div>
                                          <div class="alluser_al data-padding1 ">    
                                             <div class="insert_firm_table">
                                             <table id="FIRM_LIST_TABLE">
                                             <thead>
                                               <tr>
                                                 <th class="SELECT_ROW_TH Exc-colvis"> 
                                                    <label class="custom_checkbox1">
                                                       <input type="checkbox" class="select_all_firm"><i></i>
                                                    </label>
                                                 </th>
                                                 <th class="FIRM_TH hasFilter">FIRM
                                                  <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                  <div class="sortMask"></div>
                                                  </th>
                                                 <th class="TYPE_TH hasFilter">TYPE
                                                  <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                  <div class="sortMask"></div>
                                                  </th>
                                                 <th class="COMPANY_NO_TH hasFilter">Company No
                                                   <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                  <div class="sortMask"></div>
                                                 </th>
                                                 <th class="DOB_TH hasFilter">Incorporation Date
                                                  <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                  <div class="sortMask"></div>
                                                  </th>
                                                 <th class="PHONE_TH hasFilter">FIRM Contact No
                                                  <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                  <div class="sortMask"></div>
                                                  </th>
                                                 <th class="MAIN_MAIL_TH hasFilter">FIRM MAIL
                                                  <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                  <div class="sortMask"></div>
                                                  </th>
                                                 <th class="STATUS_TH hasFilter">STATUS
                                                  <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                  <div class="sortMask"></div>
                                                  <select class="filter_check" style="display: none;" multiple="true">
                                                    <?php 
                                                    unset($Firms_Status['all']);
                                                    foreach ($Firms_Status as $key => $value)
                                                    {
                                                      echo "<option value=".$value['label'].">".$value['label']."</option>";
                                                    }
                                                    ?>
                                                  </select>              
                                                 </th>
                                                 
                                                 <th class="CREATE_DATE_TH hasFilter">
                                                  Create Date
                                                  <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                  <div class="sortMask"></div>
                                                 </th>
                                                <th class="Subscribed_Plan_TH hasFilter">
                                                  Subscribed Plan
                                                  <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />   
                                                  <div class="sortMask"></div>
                                                </th>


                                                 <th class="action_TH Exc-colvis">Action</th>
                                                 <th class="crm_legal_form_primary_search_TH Exc-colvis" style="display: none;">
                                                </th>
                                                <th class="Status_primary_search_TH Exc-colvis" style="display: none;">
                                                </th>                                                 
                                                <th class="Created_date_primary_search_TH Exc-colvis" style="display: none;">
                                                </th>
                                               </tr>
                                             </thead>
                                              <?php
                                                //$this->load->view('users/Client_list_table');
                                              foreach ($firm_admin as $key => $FIRM_ADMIN) 
                                              {

                                              $FIRM                 = $this->db->query("select * from firm where user_id =".$FIRM_ADMIN['id']." ")->row_array();
                                              $subscribed_plan      = $this->Firm_setting_mdl->Get_FirmSubscribed_Plan( $FIRM['firm_id'] );
                                              $subscribed_plan_name = (!empty( $subscribed_plan['plan_name'] )?$subscribed_plan['plan_name']:'');
                                              $date_of_creation     = Change_Date_Format ( $FIRM['crm_incorporation_date']);
                                              ?>
                                              <tr>
                                              <td>
                                                <label class="custom_checkbox1">
                                                   <input type="checkbox" class="select_firm" value="<?php echo $FIRM['firm_id'];?>"><i></i>
                                                </label>
                                              </td>
                                                  <?php
                                                  if($FIRM['crm_company_name']!='')
                                                  {
                                                      $NAME =  $FIRM['crm_company_name'];
                                                  }
                                                  else
                                                  {
                                                    $NAME =  $FIRM_ADMIN['crm_name'];
                                                  }
                                                   ?>
                                              <td data-search="<?php echo $NAME;?>">
                                                <a href="<?php echo base_url().'firm/view_firm/'.$FIRM_ADMIN['id']?>">
                                                  <?php echo $NAME; ?>
                                                </a>
                                              </td>
                                              <?php $FIRM_TYPE =  array_search($FIRM['crm_legal_form'], $Company_types);?>

                                               <td data-search="<?php echo $FIRM_TYPE;?>"><?php echo $FIRM_TYPE;?></td>
                                               <td data-search="<?php echo $FIRM['crm_company_number'];?>">
                                                <?php echo $FIRM['crm_company_number'];?>                                                 
                                               </td>
                                               <td data-search="<?php echo $date_of_creation;?>">
                                                <?php echo $date_of_creation;?>                                                 
                                               </td>
                                               <td data-search="<?php echo $FIRM['firm_contact_no'];?>">
                                                <?php echo $FIRM['firm_contact_no'];?>
                                               </td>
                                               <td data-search="<?php echo $FIRM['firm_mailid'];?>">
                                                <?php echo $FIRM['firm_mailid'];?>
                                               </td>
                                               <!-- reason for set label in  data-search used client side pdf,csv,pint -->
                                               <td data-search="<?php echo $Firms_Status[ $FIRM['status'] ]['label'];?>"> 
                                               <select class="firm_status" data-id="<?php echo $FIRM['firm_id'];?>">
                                                <?php 
                                                //echo $Firms_Status[ $FIRM['status'] ]['label'];

                                                foreach ( $Firms_Status as $key => $value )
                                                    { 
                                                      $select="";
                                                      if($FIRM['status'] == $key ) 
                                                        {
                                                          $select ="selected='selected'";
                                                        }
                                                      echo "<option value=".$key." $select>".$value['label']."</option>";
                                                    }
                                                ?> 
                                                </select>
                                               </td>
                                               <td data-search="<?php echo date('d-m-Y' , $FIRM_ADMIN['CreatedTime']);?>"> 
                                                <?php echo date('d-m-Y' , $FIRM_ADMIN['CreatedTime']);?> 
                                               </td>

                                               <td data-search="<?php echo $subscribed_plan_name;?>"> 
                                                <?php echo $subscribed_plan_name;?> 
                                               </td>
                                              
                                               <td>
                                               <div class="dropdown">
                                                  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <span class="dropdwn-action"></span>
                                                    <span class="dropdwn-action"></span>
                                                    <span class="dropdwn-action"></span>
                                                  </button>
                                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a href="javascript:void(0)" class="dropdown-item firm_delete" data-id="<?php echo $FIRM['firm_id'];?>" data-acion="delete">
                                                      <i class="icofont icofont-ui-delete" aria-hidden="true" ></i>
                                                    </a>
                                                    <a class="dropdown-item" href="<?php echo base_url().'Firm/add_firm/'.$FIRM_ADMIN['id'];?>"><i class="icofont icofont-edit" aria-hidden="true"></i>
                                                    </a>

                                                    <a href="javascript:void(0);" class="dropdown-item firm_archive <?php if($FIRM['status']==3){echo 'disabled';}?>" data-id="<?php echo $FIRM['firm_id'];?>" data-action="archive">
                                                        <i class="icofont icofont-archive archieve_click" aria-hidden="true"></i>
                                                    </a>
                                                    <a class="dropdown-item" href="<?php echo base_url().'login/SA_LoginAs_FA/'.$FIRM['firm_id']; ?>" class="login_us_others">
                                                      <i class="icofont icofont-sign-in" aria-hidden="true"></i>
                                                    </a>
                                                  </div>
                                                </div>
                                                
                                               </td>
                                               <td style="display: none;" data-search="<?php echo $FIRM['crm_legal_form']; ?>"></td>
                                               <td style="display: none;" data-search="<?php echo $FIRM['status'];?>" class="Status_primary_search_TD">
                                               </td>
                                               <!-- D'nt move this its should stay last , for from-to search-->
                                               <td style="display: none;" data-search="<?php echo date('Ymd' , $FIRM_ADMIN['CreatedTime']);?>"> 
                                               </td>

                                            <?php
                                              }
                                              ?>
                                              </table>
                                             </div>
                                          </div>
                                       </div>
                                    </div>


                                    </div>
                              </div>
                               <!-- END OF  table content Open -->
                           </div>
                            <!-- clients details content -->

                           <!-- Register your self card end -->
                        </div>
                     </div>
                  </div>
                  <!-- Page body end -->
               </div>
            </div>
            <!-- Main-body end -->  
            <div id="styleSelector">
            </div>
         </div>
      </div>
   </div>

<?php $this->load->view('super_admin/superAdmin_footer',['INCLUDE'=>['DT_JS']]);?>
<script>
var FirmTable_Instance;
 /*for settings button collection modifycation*/
 
$(document).on('click', '.Settings_Button', AddClass_SettingPopup);
$(document).on('click', '.Button_List', AddClass_SettingPopup );

$(document).on('click','.dt-button.close',function()
{
  $('.dt-buttons').find('.dt-button-collection').detach();
  $('.dt-buttons').find('.dt-button-background').detach();
});
function AddClass_SettingPopup()
{
  $('button.Button_List').closest('.dt-button-collection').addClass('Settings_List');
  $('button.buttons-columnVisibility').closest('.dt-button-collection').addClass('columnVisibility_List');
}
/*for settings button collection modifycation*/
/*FOR DATE (FROM-TO) Search For Each Draw*/

$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
      
        //console.log("inside");

        var CreateTime =  data[data.length-1].trim() ;
        if(!CreateTime)
        {         
          return true;
        }
        var minDate = $("#FirmCreate_FromDate");
       
        var maxDate = $("#FirmCreate_ToDate");
        

         if ( minDate.length && minDate.val().length ) 
         {
            //console.log(minDate);

            minDate = minDate.val().split('-');
            minDate.reverse();
           
            
            var minDate = minDate[0]+minDate[1]+minDate[2];
           
            if (parseInt( CreateTime) < parseInt(minDate) )
            { 
              return false;
            }
         }

        if (maxDate.length && maxDate.val().length)
        {

          maxDate = maxDate.val().split('-');
          maxDate.reverse();

          maxDate =  maxDate[0]+maxDate[1]+maxDate[2];

         //console.log("inside maxDate"+new Date(maxDate[0]+"-"+maxDate[1]+"-"+maxDate[2]));

          if ( parseInt( CreateTime ) > parseInt( maxDate ) )
          {
            return false;
          }
        }


          //console.log("return true"+CreateTime);

    return true;
    }
);

/*FOR DATE (FROM-TO) Search For Each Draw*/

/*for COLUMN oredering*/
        <?php 
          $column_setting = SuperAdmin_column_settings('firm_list');
        ?>
        var column_order = <?php echo $column_setting['order'] ?>;
        
        var hidden_coulmns = <?php echo $column_setting['hidden'] ?>;

        var column_ordering = [];

        $.each(column_order,function(i,v){
         
          var index = $('#FIRM_LIST_TABLE thead th.'+v).index();

          if(index!==-1)
          {
             // alert(index);
            column_ordering.push(index);
          }
        });
       // console.log( column_ordering.length +"th" );        
/*for COLUMN oredering*/


var Rerender_cell = function(obj)
{
    var cell = obj.closest('td');

    cell.attr( 'data-search',obj.find('option:selected').text().trim() );
      
    FirmTable_Instance.cell( cell ).invalidate().draw();

    var cell2 =  obj.closest('tr').find('td.Status_primary_search_TD').attr('data-search',obj.val());
    FirmTable_Instance.cell( cell2 ).invalidate().draw();
    
}

function toggle_action_button(i,table='')
{
  if(i)
  {
            $("#bulk_delete_button").show();
            
            if(table==3)
            {
              $("#bulk_unarchive_button").show();
            }
            else
            {
              $("#bulk_archive_button").show();
            }
  }
  else
  {
            $("#bulk_delete_button").hide();
          
            if(table==3)
            {
              $("#bulk_unarchive_button").hide();
            }
            else
            {
              $("#bulk_archive_button").hide();
            }
  }
}

function get_selected_rows()
{   
  var ids =[];
  FirmTable_Instance.column('.SELECT_ROW_TH',{search:'applied'}).nodes().to$().each(function(index) {
    if($(this).find(".select_firm").is(":checked"))
    {
      ids.push($(this).find(".select_firm").val());
    }
  });
  return ids;
}

function Trigger_To_Reset_Filter()
{
    $('div.toolbar-table .tableToolBar_selectDiv li.dropdown-chose').each(function(){
      $(this).trigger('click');
    });
    $('div.toolbar-table input').val(' ');

    $('.select_all_firm').prop( 'checked',false );

    $('.select_all_firm').trigger( 'change' );

    Redraw_Table( FirmTable_Instance );
}


function Add_TableToolBar()
{

      //$(".first_box").find(".searching_box").attr('type','hidden');

      $("div.toolbar-table").prepend('<div class="filter-task1 sets"><h2>Filter:</h2><ul><li><span class="for-part"><label class="f2t">From</label><input type="text" id="FirmCreate_FromDate" class="date_picker_dob" name="from_date_picker" placeholder="By Date" readonly></span><span class="for-part"><label class="f2t">To</label><input type="text" id="FirmCreate_ToDate" name="to_date_picker" class="date_picker_dob" placeholder="By Date" readonly></span><a href="javascript:;" class="ToolBar_DateWiseFilter allusers-box"><i class="fa fa-search"></i></a></li><li><div class="tableToolBar_selectDiv"><select placeholder="By Priority" multiple="true" id="ToolBar_Priority" data-searchCol="crm_legal_form_primary_search_TH"><option value="Private Limited company">Private Limited company</option><option value="Public Limited company">Public Limited company</option><option value="Limited Liability Partnership" >Limited Liability Partnership</option><option value="Partnership">Partnership</option><option value="Self Assessment" >Self Assessment</option><option value="Trust">Trust</option><option value="Charity" >Charity</option><option value="Other" >Other</option></select></div></li></ul></div>');      

       //$("#FirmCreate_FromDate, #FirmCreate_ToDate").datepicker({autoclose: true,dateFormat: 'dd-mm-yy'});


       $("#FirmCreate_FromDate").datepicker({
         dateFormat: 'dd-mm-yy',
         changeMonth: true,
         changeYear: true,
         numberOfMonths: 1,
         onSelect: function(selected) {
           $("#FirmCreate_ToDate").datepicker("option","minDate", selected)
         }
     });
     $("#FirmCreate_ToDate").datepicker({ 
         dateFormat: 'dd-mm-yy',
         changeMonth: true,
         changeYear: true,
         numberOfMonths: 1,
         onSelect: function(selected) {
            $("#FirmCreate_FromDate").datepicker("option","maxDate", selected)
         }
     });  

      $(".tableToolBar_selectDiv").dropdown();
}  

function AddColumn_Filters(e) 
{
        var api = this.api();

              api.columns('.hasFilter').every( function () {

                        var column = this;
                        var TH = $(column.header());
                        var Filter_Select = TH.find('.filter_check');

                    if( !Filter_Select.length )
                    {

                      Filter_Select = $('<select multiple="true" class="filter_check" style="display:none"></select>').appendTo( TH );
                      var unique_data = [];
                        column.nodes().each( function ( d, j ) { 
                          var dataSearch = $(d).attr('data-search');
                          
                            if( jQuery.inArray(dataSearch, unique_data) === -1 )
                            {
                              //console.log(d);
                              Filter_Select.append( '<option  value="'+dataSearch+'">'+dataSearch+'</option>' );
                              unique_data.push( dataSearch );
                            }

                        });
                    }
                    

                    Filter_Select.on( 'change', function () {

                        var search =  $(this).val(); 
                        //console.log( search );
                        if(search.length) search= '^('+search.join('|') +')$'; 

                       var  class_name = $(this).closest('th').attr('class').match(/\w+(?=_TH)/);  

                       var cur_column = api.column( '.'+class_name+'_TH' );
                       
                        cur_column.search( search, true, false ).draw();
                    } );
                      
                        Filter_Select.formSelect(); 
         

                      }); 
                    
         
}


  function initialize_ClientTable()
{

FirmTable_Instance = $('#FIRM_LIST_TABLE').DataTable({
          "pageLength": 100,
          "dom": '<"toolbar-table select_visibles" B>lfrtip',
           buttons: [
            {

                extend: 'collection',
                background:false,
                text: '<i class="fa fa-cog" aria-hidden="true"></i>',
                className:'Settings_Button',
                buttons:[
                        { 
                          text: '<i class="fa fa-filter" aria-hidden="true"></i>Reset Filter',
                          className:'Button_List',
                          action: function ( e, dt, node, config )
                          {
                              Trigger_To_Reset_Filter();
                          }
                        },
                        {
                            extend: 'colvis',
                            text: '<i class="fa fa-cogs" aria-hidden="true"></i>Column Visibility',              
                            columns: ':not(.Exc-colvis)',
                            className:'Button_List',
                            prefixButtons:[
                            {text:'X',className:'close'}]
                        },                        
                        DataTable_Export_Buttons
                ]
            }
          ],         
          order: [],//ITS FOR DISABLE SORTING
          columnDefs: 
          [
            {"orderable": false,"targets": ['SELECT_ROW_TH','action_TH']}
          ],
          colReorder: 
          {
            realtime: false,
            order:column_ordering,
            fixedColumnsLeft : 1,
            fixedColumnsRight:4
          },
          "iDisplayLength": 10,

      initComplete: AddColumn_Filters});
    
      Add_TableToolBar();

      ColVis_Hide( FirmTable_Instance , hidden_coulmns );

      Filter_IconWrap();

      Change_Sorting_Event( FirmTable_Instance ); 
      
      ColReorder_Backend_Update ( FirmTable_Instance , 'firm_list' , 1 );

      ColVis_Backend_Update ( FirmTable_Instance , 'firm_list' , 1 );  
}

function ChangeFirm_Status( Ids , Status ,CallBack = null , parm_obj = null)
{

  if( ! (Ids.length > 0 &&  Status!='') ) 
  {
    console.log('ids or status is empty');
    return;
  }

  $.ajax({
    url:"<?php echo base_url();?>Firm/ChangeFirm_Status",
    type : 'POST',
    dataType:'json',
    data:{ids:Ids,status:Status},
    beforeSend:Show_LoadingImg,
    success:function(res){
      Hide_LoadingImg();
      console.log( res );

      if( ! res.result  )
      {
        console.log('Some think oops..');
        return;
      }

      var status = res.status;

        $('.tap_click').each(function(){
          var index = $(this).attr('data-id');          
          $(this).find('span').html(status[index]['count']);
        });
      
      var infoText = 'Status Changed'
      if(parm_obj!=null)
        {
          parm_obj.closest('tr').find('.firm_archive').removeClass('disabled');
        }

      if( Status == 3 )
      {
        infoText = "Archive";
        if(parm_obj!=null)
        {
          parm_obj.closest('tr').find('.firm_archive').addClass('disabled');
        }
      }
      else if( Status == 'unarchive')
      {
        infoText = "Un Archive";        
      }

      $('.info_popup .info-text').html( infoText + ' Success Fully..');
      $('.info_popup').show();
      setTimeout(function(){$('.info_popup').hide();} ,2000 );
      
      if(CallBack != null )
      {
        CallBack( parm_obj );
      }

    }
});
}

function delete_firms( Ids )
{
  if(Ids.length)
  {
    $.ajax({
    url:"<?php echo base_url();?>Firm/Delete_Firms",
    type : 'POST',
    dataType:'json',
    data:{ids:Ids},
    beforeSend:Show_LoadingImg,
    success:function(res)
    {
     if( res.result )
     {
      Hide_LoadingImg();
      $('.info_popup .info-text').html('Frim Deleted Success Fully..');
      $('.info_popup').show();
      setTimeout(function(){location.reload();} ,2000 );
      
     }
     else
     {
      console.log('Some thing oops');
     }
    }
    });  
  }
}
$(document).on('click','.firm_delete',function(){
  var id = $(this).attr('data-id');
  var action = function(){
  delete_firms([id]);
  $('#Confirmation_popup').modal('hide');  
  };
  Conf_Confirm_Popup({'OkButton':{'Handler':action},'Info':'Do you want to delete this Firm ?'});  
  $('#Confirmation_popup').modal('show');
});



$('#bulk_delete_button').on('click',function(){
  var ids = get_selected_rows();
  var action = function(){   
   delete_firms(ids);
   $('#Confirmation_popup').modal('hide');
  };

  Conf_Confirm_Popup({'OkButton':{'Handler':action},'Info':'Do you want to delete these Firms ?'});  

  $('#Confirmation_popup').modal('show');
  
});

//$('.dataTables_wrapper .toolbar-table').addClass('select_visibles');

$('.firm_status').change(function(){ 
//  console.log($(this).attr('data-id') +"select"+ $(this).val() );
  ChangeFirm_Status( [$(this).attr('data-id')] , $(this).val() , Rerender_cell , $(this) );
   });

$(document).on('click','.firm_archive',function(){

  var id = $(this).attr('data-id');
  var obj = $(this).closest('tr').find('select.firm_status');
  var action = function(){        
    obj.val(3);
    ChangeFirm_Status( [id] , 3 , Rerender_cell , obj );
    $('#Confirmation_popup').modal('hide');
  };

  Conf_Confirm_Popup({'OkButton':{'Handler':action},'Info':'Do you want to archive this Firm ?'});  

  $('#Confirmation_popup').modal('show');

});

$('#bulk_archive_button').on('click',function(){
  var ids = get_selected_rows();
  var action = function(){   

   ChangeFirm_Status( ids , 3 , function(){setTimeout(function(){location.reload();},2000);});
    $('#Confirmation_popup').modal('hide');
  };

  Conf_Confirm_Popup({'OkButton':{'Handler':action},'Info':'Do you want to archive these Firms ?'});  

  $('#Confirmation_popup').modal('show');
  
});

$('#bulk_unarchive_button').on('click',function(){
  var ids = get_selected_rows();
  var action = function(){   

   ChangeFirm_Status( ids , 'unarchive' , function(){setTimeout(function(){location.reload();},2000);});
    $('#Confirmation_popup').modal('hide');
  };

  Conf_Confirm_Popup({'OkButton':{'Handler':action},'Info':'Do you want to un-archive these Firms ?'});  

  $('#Confirmation_popup').modal('show');
  
});



  function FilterFrom_OtherPage()
{
  <?php if(isset($_SESSION['firm_seen'])){ ?>

      var value='<?php echo $_SESSION['firm_seen'];  ?>';
      value = value.trim();

      console.log(value);
      if(value=='')
      { 
         $("#alluser_filter input").val('private');
         $("#alluser_filter input").trigger('keyup');
        // $("#alluser_filter input").trigger('change');
      }
      if(value=='public_limited')
      {
           $("#alluser_filter input").val('public');
            //console.log($("#alluser_filter input").val());
           $("#alluser_filter input").trigger('keyup');
      }
      if(value=='limited_liablity')
      { 
          $("#alluser_filter input").val('limited liability');
          $("#alluser_filter input").trigger('keyup');
      }

      if(value=='partnership')
      { 
          $("#alluser_filter input").val('partnership');
         $("#alluser_filter input").trigger('keyup');
      }

      if(value=='self')
      {
          $("#alluser_filter input").val('self');
         $("#alluser_filter input").trigger('keyup');
      }

      if(value=='trust')
      { 
          $("#alluser_filter input").val('trust');
         $("#alluser_filter input").trigger('keyup');
       }
      if(value=='charity')
      { 
          $("#alluser_filter input").val('charity');
         $("#alluser_filter input").trigger('keyup');
      }
      if(value=='other')
      { 
          $("#alluser_filter input").val('other');
         $("#alluser_filter input").trigger('keyup');
      }   
      var Table_status_filter = '';

     if(value=='active')
     { 
          Table_status_filter ='1'
     }
     else if(value=='allfirm')
     { 
          Table_status_filter ='all'
     }
    else if(value=='inactive')
    { 
          Table_status_filter ='0'     
     }
     else if(value=='frozen')
     { 
          Table_status_filter ='2'
     }
       else if(value=='archive')
     { 
          Table_status_filter ='3'
     }
    
     if(Table_status_filter!='')
     {
     // console.log( Table_status_filter +"session");

     $('.tap_click').each(function(){  

        // if( $(this).attr('data-id').trim() == Table_status_filter)
        // {
        //   console.log( "indide the siv" ) ; 
        //   $(this).trigger('click');          
        // }

        Trigger_To_Reset_Filter();

      $('.tap_click').removeClass('active');
      $(this).addClass('active');
      var status = Table_status_filter;
      if(status == "all")
      {
        return;
      }
      else
      {
        FirmTable_Instance.column('.Status_primary_search_TH').search('^'+status+"$",true,false).draw();
      }

     });

     }
     <?php  }

     if(isset($_SESSION['firm_service'])){ ?>
     // alert('check');
       var value='<?php echo $_SESSION['firm_service'];  ?>';
       //console.log(value);
       var status='ON';
       Trigger_To_Reset_Filter();
       FirmTable_Instance.column('.'+value+'_TH').search('^'+status+"$",true,false).draw();


   <?php  }

      ?>



}


initialize_ClientTable();



$('.insert_firm_table .ToolBar_DateWiseFilter').click(function(){
    FirmTable_Instance.draw();
  });
 

  $('.insert_firm_table').on('change',' #ToolBar_Priority',function(){

    var column = $(this).attr('data-searchCol');
    
    var value = $(this).val().join('|');
    
    FirmTable_Instance.column('.'+column).search(value,true,false).draw();

  });

    FilterFrom_OtherPage();

  $('.tap_click').click(function(){

      Trigger_To_Reset_Filter();

      $('.tap_click').removeClass('active');
      $(this).addClass('active');
      var status = $(this).attr('data-id').trim();
      if(status == "all")
      {
        return;
      }
      else
      {
        FirmTable_Instance.column('.Status_primary_search_TH').search('^'+status+"$",true,false).draw();
      }

    });

 $(document).on("change", ".select_all_firm", function(event) {

  var IsArchive=$('.tap_click.active').attr("data-id").trim();
 
    var checked = this.checked;
     $(".LoadingImage").show();
     var Selected_Num = 0;
    FirmTable_Instance.column('.SELECT_ROW_TH').nodes().to$().each(function(index) {
        
          if (checked)
         {
           Selected_Num++;
         }
           
        $(this).find('.select_firm').prop('checked',checked);

        toggle_action_button( checked , IsArchive );
        
    });
    $(".LoadingImage").hide();
});

  $(document).on("change",".select_firm",function(event)
    {
      var IsArchive=$('.tap_click.active').attr("data-id").trim();
      
      var tr=0;
      var ck_tr=0;
      FirmTable_Instance.column('.SELECT_ROW_TH').nodes().to$().each(function(index) {
      if( $(this).find(".select_firm").is(":checked") )
        {
          ck_tr++;
        }
        tr++;
      });

        if(tr==ck_tr)
        {
          $(".select_all_firm").prop("checked",true);
        } 
        else
        {
          $(".select_all_firm").prop("checked",false);
        } 
      if(ck_tr)
      {
        toggle_action_button(1,IsArchive);
      }
      else  
      {
        toggle_action_button(0,IsArchive);
      } 
       
    });

  

</script>