<?php 
   
$this->load->view('super_admin/superAdmin_header');
  // $this->load->view('includes/header');

   if(isset($_GET['action']))
   {
      $action = $_GET['action'];
   }
   else
   {
      $action = 'mannual';
   }
      $uri_seg = $this->uri->segment(3);
   ?>
   <link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<style>
/** 28-08-2018 for hide edit option on company house basic detailes **/
   .edit-button-confim {
       display: none;
   }
   span.primary-inner.field_hide{
      display: none !important;
   }

   li.show-block
   {
      display: none;
      float: left;
      width: 100%;
   }
   .inner-views
   {
      display: block;
   }
      .disabled{
   opacity: 0.5;
   pointer-events: none;    
   }   
/** end of 28-08-2018 **/
</style>
<div class="modal-alertsuccess  alert alert-success-reminder succs" style="display: none;">
   <div class="newupdate_alert">
   <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   <div class="pop-realted1">
      <div class="position-alert1">Reminder Added Successfully.</div>
   </div></div>
</div>
<div class="pcoded-content card-removes clientredesign firmclientinform  sa-firm view-sa-firm">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card">
                        
                       
                        <form id="insert_form" class="validation modal-addnew1" method="post" action="" enctype="multipart/form-data">
                           <!-- 06-07-2018 for addreminders ids-->
                           <input type="hidden" id="add_reminder_ids" name="add_reminder_ids" value=''>
                           <!-- end of 06-07-2018 -->
                           <div class="remin-admin">
                              <div class="space-required">
                                 <div class="deadline-crm1 floating_set">
                                    <!-- <ul class="nav nav-tabs1 all_user1 md-tabs pull-left u-dashboard">
                                       <li class="nav-item">
                                          <a class="nav-link" href="<?php echo base_url().'Firm';?>">
                                          <img class="themicon" src="<?php echo base_url();?>assets/images/tabicon1.png" alt="themeicon" />
                                       All Firm</a>
                                          <div class="slide"></div>
                                       </li>
                                       <li class="nav-item">
                                          <a class="nav-link active" href="#">
                                             <img class="themicon" src="<?php echo base_url();?>assets/images/tabicon2.png" alt="themeicon" />View Firm Details</a>
                                          <div class="slide"></div>
                                       </li>
                                    </ul> -->
                                    <div class="Footer common-clienttab pull-right">
                                       <ul class="nav nav-tabs1 all_user1 md-tabs pull-left u-dashboard">
                                          <li class="nav-item">
                                             <a class="nav-link" href="<?php echo base_url().'Firm';?>">
                                             <img class="themicon" src="<?php echo base_url();?>assets/images/tabicon1.png" alt="themeicon" />
                                          All Firm</a>
                                             <div class="slide"></div>
                                          </li>
                                          <li class="nav-item">
                                             <a class="nav-link active" href="#">
                                                <img class="themicon" src="<?php echo base_url();?>assets/images/tabicon2.png" alt="themeicon" />View Firm Details</a>
                                             <div class="slide"></div>
                                          </li>
                                       </ul>
                                       <div>
                                          <div class="change-client-bts1">
                                                               
                                          </div>
                                          <div class="divleft">
                                             <button  class="signed-change2"  type="button" value="Previous Tab" text="Previous Tab">Previous 
                                             </button>
                                          </div>
                                          <div class="divright">
                                             <button  class="signed-change3" type="button" value="Next Tab"  text="Next Tab">Next
                                             </button>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              </div>

                             
                              <div class="space-required">
                                 <div class="document-center floating_set">
                                    <div class="Companies_House floating_set">
                                       <div class="pull-left form_heading">
                                          <h2><?php if(isset($firm['crm_company_name']) && ($firm['crm_company_name']!='') ){ echo $firm['crm_company_name'];}?></h2>
                                       </div>
                                    </div>
                                    <div class="addnewclient_pages1 floating_set bg_new1">
                                       <ol class="nav nav-tabs all_user1 md-tabs floating_set" id="tabs">
                                         
                                          <?php $uri=$this->uri->segment(3); ?>

                                          <li class="nav-item it0 active" value="0"><a class="nav-link
                                             active" data-id="#required_information" href="javascript:void(0);">
                                             Required information</a>
                                          </li>
                                
                                          <li class="nav-item it18 basic_details_tab" value="18"><a class="nav-link basic_deatils_id" data-id="#basic_details" href="javascript:void(0);"> Basic Details</a>
                                          </li>

                                          <li class="nav-item it1 for_contact_tab" value="1"><a class="nav-link main_contacttab" data-id="#main_Contact" href="javascript:void(0);">
                                             Contact</a>
                                          </li>
                                          <!-- <li class="nav-item it2 hide" value="2">
                                            <a class="nav-link" data-toggle="tab" data-id="#service" href="javascript:void(0);"> Service</a>
                                          </li> -->
                                         <!--  <li class="nav-item it2 hide" value="2"><a class="nav-link" data-toggle="tab" href="#assignto" style="display: none;"> Assign to</a></li> -->
                                          <li class="nav-item it2 hide" value="2">
                                             <a class="nav-link" data-id="#amlchecks" href="javascript:void(0);"> AML Checks</a>
                                          </li>
                                          <li class="nav-item it2 hide business-info-tab" value="2" style="display: none;">
                                             <a class="nav-link" data-id="#business-det" href="javascript:void(0);"> Business Details</a>
                                          </li>

                                          <li class="nav-item it10 other_tabs" value="10"><a class="nav-link" data-id="#other" href="javascript:void(0);">
                                                <span id="other_tab_cnt"></span>
                                                <span id="old_cnt"></span> Other</a>
                                          </li>
                                          
                                          <li class="nav-item it2 hide" value="2">
                                             <a class="nav-link" data-id="#package_tab" href="javascript:void(0);">Package</a>
                                          </li>

                                          <input type="hidden" id="cnt_of_other" name="cnt_of_other" >
                                          <input type="hidden" class="service_value" id="service_value" >
                                       </ol>
                                       <!-- tab close -->
                                    </div>
                                 </div>
                              </div>
                
                           <div class="newupdate_design">    
                           <div class="management_section floating_set realign newmanage-animation">
                              <div class="card-block accordion-block">
                                 <!-- tab start -->
                                 <div class="tab-content card">
                                    <div id="required_information" class="columns-two tab-pane fade in active ">
                                       <div class="space-required">
                                          <div class="main-pane-border1" id="required_information_error">
                                             <div class="alert-ss"></div>
                                          </div>
                                       </div>
                                       <div class="space-required">
                                          <div class="white-space01">
                                             <div class="pane-border1">
                                                <div class="management_form1">                                                
                                                   <div class="basic-info-client1">
                                                     <div class="form-group row name_fields">
                                                        <label class="col-sm-4 col-form-label">Name</label>
                                                        <div class="col-sm-8">
                                                        <?php if(isset($firm['crm_company_name']) && ($firm['crm_company_name']!='') ){ ?>
                                                        <span class="small-info">
                                                           <?php echo $firm['crm_company_name'];?>
                                                        </span>
                                                        <?php } ?>
                                                         
                                                        </div>
                                                     </div>

                                                   <div class="form-group row name_fields sorting">
                                                      <label class="col-sm-4 col-form-label">Legal Form</label>
                                                      <div class="col-sm-8">
                                                         <?php if(isset($firm['crm_legal_form']) && ($firm['crm_legal_form']!='') ){ ?>
                                                      <span class="small-info">
                                                         <?php echo $firm['crm_legal_form'];?>
                                                      </span>
                                                      <?php } ?>

                                                      </div>
                                                   </div>

                                                  <div class="form-group row name_fields sorting">
                                                    <label class="col-sm-4 col-form-label">Subscribed Plan</label>
                                                    <div class="col-sm-8">
                                                    <?php if(!empty($subscribed_plan['plan_name']) ){ ?>
                                                      <span class="small-info">
                                                         <?php echo $subscribed_plan['plan_name'];?>
                                                      </span>
                                                    <?php } ?>
                                                    </div>
                                                  </div>

                                                   <div class="form-group row name_fields sorting">
                                                      <label class="col-sm-4 col-form-label">Firm Primary Mail ID</label>
                                                      <div class="col-sm-8">
                                                      <?php if(!empty($firm['firm_mailid']) ){ ?>
                                                        <span class="small-info">
                                                           <?php echo $firm['firm_mailid'];?>
                                                        </span>
                                                      <?php } ?>
                                                      </div>
                                                   </div>
                                                   <div class="form-group row name_fields sorting">
                                                      <label class="col-sm-4 col-form-label">Firm Primary Contact No</label>
                                                      <div class="col-sm-8">
                                                      <?php if(!empty($firm['firm_contact_no']) ){ ?>
                                                        <span class="small-info">
                                                           <?php echo $country_sort_name.'-'.$firm['firm_contact_no'];?>
                                                        </span>
                                                      <?php } ?>
                                                      </div>
                                                   </div>
                                                   

                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <!-- 1tab close-->

                                <!-- 2tab -->
                                <div id="basic_details" class="fullwidth-animation3 tab-pane fade  ">
                                <div class="space-required">
                                  <div class="main-pane-border1" id="basic_details_error">
                                    <div class="alert-ss"></div>
                                  </div>
                                </div>

                                <div id="details" class="accord-proposal1 tab-pane">
                                <div class="masonry-container floating_set">
                                <div class="grid-sizer"></div>

                                <div class="accordion-panel">
                                <div class="box-division03">
                                  <div class="accordion-heading" role="tab" id="headingOne">
                                  <h3 class="card-title accordion-title">
                                  <a class="accordion-msg">Important Information</a>
                                  </h3>
                                  </div>
                                <div id="collapse" class="panel-collapse">
                                <div class="basic-info-client1" id="important_details_section">

                                <?php if(isset($firm['crm_company_name']) && $firm['crm_company_name']!=''){  ?>
                                  <div class="form-group row name_fields sorting" >
                                  <?php $company_roles=(isset($firm['status'])&&$firm['status']==1)? 'readonly' :''?>

                                  <label class="col-sm-4 col-form-label">Company Name </label>
                                    <div class="col-sm-8">
                                      <span class="small-info"><?php echo $firm['crm_company_name'];  ?></span>
                                    </div>
                                  </div>
                                <?php } ?>


                                <?php if(isset($firm['crm_company_number']) && $firm['crm_company_number']!=''){ ?>
                                <div class="form-group row name_fields sorting">
                                <label class="col-sm-4 col-form-label">Company Number</label>
                                <div class="col-sm-8">

                                <span class="small-info"><?php if(isset($firm['crm_company_number'])){ echo $firm['crm_company_number']; } ?></span>
                                </div>
                                </div>
                                <?php } ?>

                                <div class="clearfix"></div>

                                

                                <?php if(isset($firm['crm_incorporation_date']) && $firm['crm_incorporation_date']!=''){ ?>
                                <div class="form-group row name_fields sorting">
                                <label class="col-sm-4 col-form-label">Incorporation Date</label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php echo Change_Date_Format($firm['crm_incorporation_date']); ?></span>
                                </div>
                                </div>
                                <?php } ?>

                                <?php if(isset($firm['crm_registered_in']) && $firm['crm_registered_in']!=''){ ?>
                                <div class="form-group row name_fields sorting">
                                <label class="col-sm-4 col-form-label">Registered In</label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php echo $firm['crm_registered_in']; ?></span>
                                </div>
                                </div>

                                <?php } ?>

                                <?php if(isset($firm['crm_address_line_one']) && $firm['crm_address_line_one']!=''){ ?>
                                <div class="form-group row name_fields sorting" >
                                <label class="col-sm-4 col-form-label">Address Line 1</label>
                                <div class="col-sm-8">
                                <p class="for-address"><?php echo $firm['crm_address_line_one'];   ?></p>
                                </div>
                                </div>
                                <?php } ?>

                                <?php if(isset($firm['crm_address_line_two']) && $firm['crm_address_line_two']!=''){ ?>

                                <div class="form-group row name_fields sorting"  >
                                <label class="col-sm-4 col-form-label">Address Line 2</label>
                                <div class="col-sm-8">
                                <p class="for-address"><?php echo $firm['crm_address_line_two'];  ?></p>
                                </div>
                                </div>

                                <?php } ?>

                                <?php if(isset($firm['crm_address_line_three']) && $firm['crm_address_line_three']!=''){ ?>
                                <div class="form-group row name_fields sorting" >
                                <label class="col-sm-4 col-form-label">Address Line 3</label>
                                <div class="col-sm-8">
                                <p class="for-address"><?php echo $firm['crm_address_line_three'];  ?></p>
                                </div>
                                </div>
                                <?php } ?>

                                <?php if(isset($firm['crm_town_city']) && $firm['crm_town_city']!=''){ ?>
                                <div class="form-group row name_fields sorting" >
                                  <label class="col-sm-4 col-form-label">Town/City</label>
                                  <div class="col-sm-8">
                                    <span class="small-info"><?php echo $firm['crm_town_city'];  ?></span>
                                  </div>
                                </div>
                                <?php } ?>

                                <?php if(isset($firm['crm_post_code']) && $firm['crm_post_code']!=''){ ?>
                                <div class="form-group row name_fields sorting clerbot" >
                                <label class="col-sm-4 col-form-label">Post Code</label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php  echo $firm['crm_post_code'];  ?></span>
                                </div>
                                </div>
                                <?php } ?>
                                <?php if(isset($firm['crm_company_status']) && $firm['crm_company_status']!=''){ ?>
                                <div class="form-group row name_fields sorting" >
                                <label class="col-sm-4 col-form-label">Company Status</label>
                                <div class="col-sm-8">
                                <span class="small-info">
                                <?php  

                                $Defult_select= !empty($firm['status'])?$firm['status']:'';

                                foreach ($Firms_Status as $key => $value)
                                {  
                                if($Defult_select == $key )
                                {

                                echo $value['label'];
                                }
                                }
                                ?>

                                </span>
                                </div>
                                </div>
                                <?php } ?>
                                <?php if(isset($firm['crm_company_type']) && $firm['crm_company_type']!=''){ ?>
                                <div class="form-group row name_fields sorting" >
                                <label class="col-sm-4 col-form-label">Company Type</label>
                                <div class="col-sm-8">
                                <span class="small-info company_slcs"><?php echo $firm['crm_company_type']; ?></span>
                                </div>
                                </div>
                                <?php } ?>

                                <?php if(isset($firm['crm_company_sic']) && ($firm['crm_company_sic']!='') ){ ?>

                                <div class="form-group row name_fields sorting">
                                <label class="col-sm-4 col-form-label">Company S.I.C</label>
                                <div class="col-sm-8">

                                <span class="small-info company_slcs"><?php 

                                echo $firm['crm_company_sic'];


                                ?></span>
                                </div>
                                </div>

                                <?php } ?>
                                <?php if(isset($firm['crm_letter_sign']) && $firm['crm_letter_sign']!=''){ ?>

                                <div class="form-group row name_fields sorting" >
                                <label class="col-sm-4 col-form-label">Letter of Enagagement Sign Date</label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php echo $firm['crm_letter_sign'];  ?></span>
                                </div>
                                </div>
                                <?php } ?>

                                <?php if(isset($firm['crm_business_website']) && $firm['crm_business_website']!=''){ ?>

                                <div class="form-group row name_fields sorting" >
                                <label class="col-sm-4 col-form-label"> Business Websites </label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php echo $firm['crm_business_website'];  ?></span>
                                </div>
                                </div>
                                <?php } ?>

                                <?php if(isset($firm['crm_company_url']) && $firm['crm_company_url']!=''){ ?>

                                <div class="form-group row name_fields sorting" >
                                <label class="col-sm-4 col-form-label"> Company URL </label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php echo $firm['crm_company_url'];  ?></span>
                                </div>
                                </div>
                                <?php } ?>


                                <?php if(isset($firm['crm_officers_url']) && $firm['crm_officers_url']!=''){ ?>

                                <div class="form-group row name_fields sorting" >
                                <label class="col-sm-4 col-form-label"> Officers URL </label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php echo $firm['crm_officers_url'];  ?></span>
                                </div>
                                </div>
                                <?php } ?>


                                <?php if(isset($firm['crm_accounting_system']) && $firm['crm_accounting_system']!=''){ ?>

                                <div class="form-group row name_fields sorting" >
                                <label class="col-sm-4 col-form-label">  Accounting System In Uses  </label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php echo $firm['crm_accounting_system'];  ?></span>
                                </div>
                                </div>
                                <?php } ?>

                                <div class="form-group row name_fields sorting">
                                <label class="col-sm-4 col-form-label">Client Hashtag Section</label>
                                <div class="col-sm-8">
                                <?php if(isset($firm['crm_hashtag']) && $firm['crm_hashtag']!=''){ echo $firm['crm_hashtag'];  } ?>                     
                                </div>
                                </div>
                                </div>
                                </div>
                                </div>
                                </div>


                                <div class="accordion-panel">
                                <div class="box-division03">
                                <div class="accordion-heading" role="tab" id="headingOne">
                                <h3 class="card-title accordion-title">
                                <a class="accordion-msg">Basic Details Important Information</a>
                                </h3>
                                </div>
                                <div id="collapse" class="panel-collapse">
                                <div class="basic-info-client1" id="important_details_section">

                                <?php if(isset($firm['crm_company_name']) && $firm['crm_company_name']!=''){  ?>
                                <div class="form-group row name_fields sorting">
                                <?php $company_roles=(isset($firm['status'])&&$firm['status']==1)? 'readonly' :''?>

                                <label class="col-sm-4 col-form-label">Company Name </label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php if(isset($firm['crm_company_name'])){ echo $firm['crm_company_name']; } ?></span>
                                </div>
                                </div>
                                <?php } ?>


                                <?php if(isset($firm['crm_company_number']) && $firm['crm_company_number']!=''){ ?>
                                <div class="form-group row name_fields sorting" >
                                <label class="col-sm-4 col-form-label">Company Number</label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php if(isset($firm['crm_company_number'])){ echo $firm['crm_company_number']; } ?></span>
                                </div>
                                </div>
                                <?php } ?>
                                <?php if(isset($firm['crm_address_line_one']) && $firm['crm_address_line_one']!=''){ ?>
                                <div class="form-group row name_fields sorting" >
                                <label class="col-sm-4 col-form-label">Address Line 1</label>
                                <div class="col-sm-8">
                                <p class="for-address"><?php if(isset($firm['crm_address_line_one'])){ echo $firm['crm_address_line_one']; }  ?></p>
                                </div>
                                </div>
                                <?php } ?>

                                <?php if(isset($firm['crm_address_line_two']) && $firm['crm_address_line_two']!=''){ ?>

                                <div class="form-group row name_fields sorting"  >
                                <label class="col-sm-4 col-form-label">Address Line 2</label>
                                <div class="col-sm-8">
                                <p class="for-address"><?php if(isset($firm['crm_address_line_two'])){ echo $firm['crm_address_line_two']; }  ?></p>
                                </div>
                                </div>
                                <?php } ?>
                                <?php if(isset($firm['crm_company_status']) && $firm['crm_company_status']!=''){ ?>
                                <div class="form-group row name_fields sorting" >
                                <label class="col-sm-4 col-form-label">Company Status</label>
                                <div class="col-sm-8">
                                <span class="small-info">
                                <?php  

                                $Defult_select= !empty($firm['status'])?$firm['status']:'';

                                foreach ($Firms_Status as $key => $value)
                                {  
                                if($Defult_select == $key )
                                {

                                echo $value['label'];
                                }
                                }
                                ?>
                                </span>
                                </div>
                                </div>
                                <?php } ?>
                                <?php if(isset($firm['crm_company_type']) && $firm['crm_company_type']!=''){ ?>
                                <div class="form-group row name_fields sorting">
                                <label class="col-sm-4 col-form-label">Company Type</label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php if(isset($firm['crm_company_type'])){ echo $firm['crm_company_type']; } ?></span>
                                </div>
                                </div>
                                <?php } ?>

                                </div>
                                </div>
                                </div>
                                </div>

                                </div>
                                <!-- accordion-panel -->
                                </div>

                                </div>
                                    <!-- 2tab close -->


                                    <!-- 3tab start -->
                                <div id="main_Contact" class="fullwidth-animation tab-pane fade" >
                                <div class="space-required">
                                      <div class="main-pane-border1 main_Contact" id="main_Contact_error">
                                         <div class="alert-ss"></div>
                                      </div>
                                   </div>
                                      
                                <?php 
                                $i=1;

                                /*echo "<pre>";
                                print_r($contactRec);die;*/
                                foreach ($contactRec as $contactRec_key => $contactRec_value) {

                                $title = $contactRec_value['title'];
                                $first_name = $contactRec_value['first_name'];
                                $middle_name = $contactRec_value['middle_name'];
                                $surname = $contactRec_value['surname']; 
                                $preferred_name = $contactRec_value['preferred_name']; 
                                $last_name = $contactRec_value['last_name'];
                                $mobile = $contactRec_value['mobile']; 
                                $main_email = $contactRec_value['main_email']; 
                                $nationality = $contactRec_value['nationality']; 
                                $psc = $contactRec_value['psc']; 
                                $shareholder = $contactRec_value['shareholder']; 
                                $ni_number = $contactRec_value['ni_number']; 
                                $contact_type = $contactRec_value['contact_type']; 
                                $address_line1 = $contactRec_value['address_line1']; 
                                $address_line2 = $contactRec_value['address_line2']; 
                                $town_city = $contactRec_value['town_city']; 
                                $post_code = $contactRec_value['post_code']; 
                                $landline = $contactRec_value['landline']; 
                                $work_email   = $contactRec_value['work_email']; 
                                $date_of_birth   = $contactRec_value['date_of_birth']; 
                                $nature_of_control   = $contactRec_value['nature_of_control']; 
                                $marital_status   = $contactRec_value['marital_status'];

                                $utr_number   = $contactRec_value['utr_number']; 
                                $id   = $contactRec_value['id']; 
                                // $client_id   = $contactRec_value['client_id']; 
                                $make_primary   = $contactRec_value['make_primary']; 

                                $contact_names = $this->Common_mdl->numToOrdinalWord($i).' Contact ';
                                $name=ucfirst($contactRec_value['first_name']).' '.ucfirst($contactRec_value['surname']);
                                if($make_primary==1)
                                {
                                $name .= "-Primary Contact";
                                }
                                ?>
                                <div class="space-required">
                                <div class="masonry-container floating_set">
                                <div class="grid-sizer"></div>
                                <div class="accordion-panel remove<?php echo $id;?> contactcc ">
                                <div class="box-division03">
                                <div class="accordion-heading" role="tab" id="headingOne">
                                <h3 class="card-title accordion-title">
                                <a class="accordion-msg"><?php echo $name;  ?></a>
                                <!-- <a href="#" data-toggle="modal" data-target="#modalcontact<?php echo $id;?>"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a> -->
                                </h3>
                                </div>
                                <!-- <div class="data-primary-03 floating_set form-radio">
                                <?php if($make_primary=='0'){?>
                                <span class="make_primary" onclick="return make_a_primary(<?php echo $id.','.$client_id;?>);">Make a Primary Contact</span>
                                <?php } ?>
                                </div> -->
                                <div class="primary-info03 floating_set">
                                <div id="collapse" class="panel-collapse">
                                <div class="basic-info-client1">
                                <div class="left-primary remove-space-set1">
                                <?php if(isset($title) && $title!='') { ?>
                                <div class="form-group row name_fields">
                                <label class="col-sm-4 col-form-label">Title</label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php if(isset($title)) { echo $title; }?></span>
                                </div> 
                                </div>
                                <?php } ?>


                                <?php if(isset($first_name) && $first_name!='') { ?>
                                <div class="form-group row name_fields">
                                <label class="col-sm-4 col-form-label">First Name</label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php echo $first_name;?></span>
                                </div>
                                </div>
                                <?php } ?>

                                <?php if(isset($middle_name) && $middle_name!='') { ?>
                                <div class="form-group row name_fields">
                                <label class="col-sm-4 col-form-label">Middle Name</label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php echo $middle_name;?></span>
                                </div>
                                </div>
                                <?php } ?>

                                <?php if(isset($last_name) && $last_name!='') { ?>
                                <div class="form-group row name_fields">
                                <label class="col-sm-4 col-form-label">Last Name</label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php echo $last_name;?></span>
                                </div>
                                </div>
                                <?php } ?>

                                <?php if(isset($surname) && $surname!='') { ?>
                                <div class="form-group row name_fields">
                                <label class="col-sm-4 col-form-label">Surname</label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php echo $surname;?></span>
                                </div>
                                </div>
                                <?php } ?>
                                <?php if(isset($preferred_name) && $preferred_name!='') { ?>
                                <div class="form-group row name_fields">
                                <label class="col-sm-4 col-form-label">Prefered Name</label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php echo $preferred_name;?></span>
                                </div>
                                </div>
                                <?php } ?>
                                <?php if(isset($mobile) && $mobile!='') { ?>
                                <div class="form-group row name_fields">
                                <label class="col-sm-4 col-form-label">Mobile</label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php echo $mobile;?></span>
                                </div>
                                </div>
                                <?php } ?>

                                <?php if(isset($main_email) && $main_email!='') { ?>
                                <div class="form-group row name_fields">
                                <label class="col-sm-4 col-form-label">Main E-Mail address</label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php echo $main_email;?></span>
                                </div>
                                </div>
                                <?php } ?>
                                <?php if(isset($nationality) && $nationality!='') { ?>

                                <div class="form-group row name_fields">
                                <label class="col-sm-4 col-form-label">Nationality</label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php echo $nationality;?></span>
                                </div>
                                </div>
                                <?php } ?> 
                                <?php if(isset($psc) && $psc!='') { ?>
                                <div class="form-group row name_fields">
                                <label class="col-sm-4 col-form-label">PSC</label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php echo $psc;?></span>
                                </div>
                                </div>
                                <?php } ?>

                                <?php if(isset($shareholder) && $shareholder!='') { ?>
                                <div class="form-group row name_fields">
                                <label class="col-sm-4 col-form-label">Shareholder</label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php echo $shareholder;?></span>
                                </div>
                                </div>
                                <?php } ?>
                                <?php if(isset($ni_number) && $ni_number!='') { ?>
                                <div class="form-group row name_fields">
                                <label class="col-sm-4 col-form-label">National Insurance Number</label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php echo $ni_number;?></span>
                                </div>
                                </div>
                                <?php } ?>

                                <?php if(isset($contactRec_value['country_of_residence']) && $contactRec_value['country_of_residence']!='') { ?>
                                <div class="form-group row name_fields">
                                <label class="col-sm-4 col-form-label">Country Of Residence</label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php if(isset($contactRec_value['country_of_residence']) && ($contactRec_value['country_of_residence']!='') ){ echo $contactRec_value['country_of_residence'];}?></span>
                                </div>
                                </div>

                                <?php } ?>
                                </div>
                                <div class="left-primary right-primary">
                                <?php if(isset($contact_type) && $contact_type!='') { ?>


                                <div class="form-group row name_fields">
                                <label class="col-sm-4 col-form-label">Contact Type</label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php echo $contact_type;?></span>
                                </div>
                                </div>
                                <?php } ?>
                                <?php if(isset($contactRec_value['other_custom']) && $contactRec_value['other_custom']!='') { ?>
                                <div class="form-group row name_fields">
                                <label class="col-sm-4 col-form-label">Other(Custom)</label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php if(isset($contactRec_value['other_custom']) && ($contactRec_value['other_custom']!='') ){ echo $contactRec_value['other_custom'];}?></span>
                                </div>
                                </div> 
                                <?php } ?>
                                <?php if(isset($address_line1) && $address_line1!='') { ?>

                                <div class="form-group row name_fields">
                                <label class="col-sm-4 col-form-label">Address Line1</label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php  echo $address_line1;?></span>
                                </div>
                                </div>
                                <?php } ?>
                                <?php if(isset($address_line2) && $address_line2!='') { ?>
                                <div class="form-group row name_fields">
                                <label class="col-sm-4 col-form-label">Address line2</label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php  echo $address_line2;?></span>
                                </div>
                                </div>
                                <?php } ?>
                                <?php if(isset($town_city) && $town_city!='') { ?>
                                <div class="form-group row name_fields">
                                <label class="col-sm-4 col-form-label">Town/City</label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php  echo $town_city;?></span>
                                </div>
                                </div>
                                <?php } ?>
                                <?php if(isset($post_code) && $post_code!='') { ?>
                                <div class="form-group row name_fields">
                                <label class="col-sm-4 col-form-label">Post Code</label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php  echo $post_code;?></span>
                                </div>
                                </div> 

                                <?php } ?>



                                <?php 
                                $land=json_decode($contactRec_value['landline']);
                                //print_r( $land);
                                if(!empty($land)){                                
                                ?>

                                
                                <div class="form-group row name_fields">
                                <label class="col-sm-4 col-form-label">Landline</label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php  echo implode(',', $land);?></span>
                                </div>
                                </div>

                                <?php } ?>

                                <?php 
                                $work=json_decode($contactRec_value['work_email']);
                                if(!empty($work)){
                                ?>
                                
                                <div class="form-group row name_fields">
                                <label class="col-sm-4 col-form-label">Work email</label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php  echo implode(',', $work);?></span>
                                </div>
                                </div>

                                <?php  } ?>
                                <?php if(isset($date_of_birth) && $date_of_birth!='') { ?>

                                <div class="form-group row name_fields">
                                <label class="col-sm-4 col-form-label">Date of Birth</label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php  echo $date_of_birth;?></span>
                                </div>
                                </div>
                                <?php } ?>

                                <?php if(isset($nature_of_control) && $nature_of_control!='') { ?>

                                <div class="form-group row name_fields">
                                <label class="col-sm-4 col-form-label">Nature of Control</label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php  echo $nature_of_control;?></span>
                                </div>
                                </div>
                                <?php } ?>
                                <?php if(isset($marital_status) && $marital_status!='') { ?>
                                <div class="form-group row name_fields">
                                <label class="col-sm-4 col-form-label">Marital Status</label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php  echo $marital_status;?></span>
                                </div>
                                </div>
                                <?php } ?>
                                <?php if(isset($utr_number) && $utr_number!='') { ?>
                                <div class="form-group row name_fields">
                                <label class="col-sm-4 col-form-label">Utr number</label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php  echo $utr_number;?></span>
                                </div>
                                </div>
                                <?php } ?>

                                <?php if(isset($contactRec_value['occupation']) && ($contactRec_value['occupation']!='') ){  ?>
                                <div class="form-group row name_fields">
                                <label class="col-sm-4 col-form-label">Occupation</label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php if(isset($contactRec_value['occupation']) && ($contactRec_value['occupation']!='') ){ echo $contactRec_value['occupation'];}?></span>
                                </div>
                                </div>
                                <?php } ?>
                                <?php if(isset($contactRec_value['appointed_on']) && ($contactRec_value['appointed_on']!='') ){  ?>
                                <div class="form-group row name_fields">
                                <label class="col-sm-4 col-form-label">Appointed On</label>
                                <div class="col-sm-8">
                                <span class="small-info"><?php if(isset($contactRec_value['appointed_on']) && ($contactRec_value['appointed_on']!='') ){ echo date('d-m-Y',strtotime($contactRec_value['appointed_on']));}?></span>
                                </div>
                                </div>
                                <?php } ?>  

                                </div>
                                </div>
                                <!-- <div class="sav-btn">
                                <span id="succ<?php echo $id;?>" class="succ" style="color:green; display:none;">Contact Updated successfully!!!</span>
                                <input type="button" value="save" class="contactupdate" id="perferred_name<?php echo $id;?>"  data-id="<?php echo $id;?>">
                                </div> -->
                                </div>
                                </div>
                                </div>
                                </div>






                                </div>

                                </div>
                                <input type="hidden" name="make_primary[]" id="make_primary[]" value="<?php echo $make_primary;?>">
                                <?php $i++;  } ?> 
                                <input type="hidden" name="incre" id="incre" value="<?php echo $i-1;?>">
                                <!--                                   </form>
                                -->
                                <div class="contact_form"></div>
                                <input type="hidden" name="append_cnt" id="append_cnt" value="">
                                </div>

                                    <!-- 3tab close -->
                                   
                                    <!-- 4tab open -->
                              

                                <!-- 4tab close -->
                                    <!-- 4tab -->
                                <div id="amlchecks" class="tab-pane fade ">
                                <div class="space-required">
                                      <div class="main-pane-border1" id="amlchecks_error">
                                         <div class="alert-ss"></div>
                                      </div>
                                   </div>

                                   <div class="accordion-panel">
                                <div class="box-division03">
                                <div class="accordion-heading" role="tab" id="headingOne">
                                <h3 class="card-title accordion-title">
                                <a class="accordion-msg">Anti Money Laundering Checks     </a>
                                </h3>
                                </div>
                                <div id="collapse" class="panel-collapse">
                                <div class="basic-info-client1">
                                <?php if(isset($firm['crm_assign_client_id_verified']) && ($firm['crm_assign_client_id_verified']=='on') ){?>
                                <div class="form-group row radio_bts ">
                                <label class="col-sm-4 col-form-label">Client ID Verified</label>
                                <div class="col-sm-8">
                                <input type="checkbox" class="js-small f-right fields" name="client_id_verified" id="client_id_verified" <?php if(isset($firm['crm_assign_client_id_verified']) && ($firm['crm_assign_client_id_verified']=='on') ){?> checked="checked"<?php } ?> readonly>
                                </div>
                                </div>
                                <?php } ?>
                                <?php if(isset($firm['crm_assign_type_of_id']) && ($firm['crm_assign_type_of_id']!='') ){?>
                                <div class="form-group row assign_cus_type">
                                <label class="col-sm-4 col-form-label">Type of ID Provided</label>
                                <div class="col-sm-8">

                                <span class="small-info"><?php if(isset($firm['crm_assign_type_of_id'])) { echo $firm['crm_assign_type_of_id']; } ?></span>
                                </div>
                                </div>
                                <?php } ?>
                                <!-- 29-08-2018 for multiple image upload option -->
                                <?php if(!empty($firm['proof_attach_file'])  ){?>
                                   <div class="form-group row" >
                                         <label class="col-sm-4 col-form-label">Attachment</label>
                                         <!-- <div class="col-sm-8">
                                            <input type="file" name="proof_attach_file[]" id="proof_attach_file" multiple="multiple" >
                                         </div> -->
                                  
                                   <div class="attach-files showtm f-right">
                                  
                                   <div class="jFiler-items jFiler-row">
                                       <ul class="jFiler-items-list jFiler-items-grid">
                                      <?php
                                      $ex_attach=array_filter(explode(',', $firm['proof_attach_file']));
                                   foreach ($ex_attach as $attach_key => $attach_value) {
                                         $replace_val=str_replace(base_url(),'',$attach_value);
                                         $ext = explode(".", $replace_val);
                                       
                                         $res=$this->Common_mdl->geturl_image_or_not($ext[1]);
                                        // echo $res;
                                         if($res=='image'){
                                            ?>
                                         <li class="jFiler-item for_img_<?php echo $attach_key; ?>" data-jfiler-index="3" style="">
                                           <input type="hidden" name="already_upload_img[]" value="<?php echo $attach_value; ?>" >
                                             <div class="jFiler-item-container">
                                                <div class="jFiler-item-inner">
                                                   <div class="jFiler-item-thumb">
                                                      <div class="jFiler-item-status"></div>
                                                      <div class="jFiler-item-info">                                        
                                                                                     
                                                      </div>
                                                      <div class="jFiler-item-thumb-image"><img src="<?php echo base_url().'uploads/client_proof/'.$attach_value;?>" draggable="false"></div>
                                                   
                                                </div>
                                              
                                             </div>
                                          </li>
                                            <?php
                                            } // if image
                                            else{ ?>
                                          <li class="jFiler-item jFiler-no-thumbnail for_img_<?php echo $attach_key; ?>" data-jfiler-index="2" style="">
                                           <input type="hidden" name="already_upload_img[]" value="<?php echo $attach_value; ?>" >
                                             <div class="jFiler-item-container">
                                                <div class="jFiler-item-inner">
                                                   <div class="jFiler-item-thumb">
                                                      <div class="jFiler-item-status"></div>
                                                      <a href="<?php echo $attach_value; ?>" target="_blank" ><div class="jFiler-item-info">                                                               </div></a>
                                                      <div class="jFiler-item-thumb-image"><span class="jFiler-icon-file f-file f-file-ext-odt" style="background-color: rgb(63, 79, 211);"><?php echo $attach_value; ?></span></div>
                                                   </div>
                                                   
                                                </div>
                                             </div>
                                          </li>
                                         <?php } //else end

                                           } // foreach
                                           ?>
                                           </ul>
                                           </div>
                                  </div>
                                   </div>

                                   <?php } ?>
                                <!-- en dof image upload for type of ids -->
                                <?php
                                $new_arr=array();
                                $all_val=(isset($firm['crm_assign_type_of_id']) && $firm['crm_assign_type_of_id']!='')? explode(',',$firm['crm_assign_type_of_id']):$new_arr; 
                                ?>
                                <div class="form-group row name_fields spanassign" <?php if(in_array('Other_Custom',$all_val) && (isset($firm['crm_assign_client_id_verified']) && ($firm['crm_assign_client_id_verified']=='on'))) { }else { ?> style="display:none" <?php } ?> >
                                <label class="col-sm-4 col-form-label">Other Custom</label>
                                <div class="col-sm-8">
                                <!--  <input type="text" name="assign_other_custom" id="assign_other_custom" placeholder="" value="<?php if(isset($firm['crm_assign_other_custom']) && ($firm['crm_assign_other_custom']!='') ){ echo $firm['crm_assign_other_custom'];}?>" class="fields"> -->
                                <span class="small-info"><?php if(isset($firm['crm_assign_other_custom']) && ($firm['crm_assign_other_custom']!='') ){ echo $firm['crm_assign_other_custom'];}?></span>
                                </div>
                                </div>
                                <?php if(isset($firm['crm_assign_proof_of_address']) && ($firm['crm_assign_proof_of_address']=='on') ){?>
                                <div class="form-group row radio_bts ">
                                <label class="col-sm-4 col-form-label">Proof of Address</label>
                                <div class="col-sm-8">
                                <input type="checkbox" class="js-small f-right fields" name="proof_of_address" id="proof_of_address" <?php if(isset($firm['crm_assign_proof_of_address']) && ($firm['crm_assign_proof_of_address']=='on') ){?> checked="checked"<?php } ?> readonly>
                                </div>
                                </div>
                                <?php } ?>
                                <?php if(isset($firm['crm_assign_meeting_client']) && ($firm['crm_assign_meeting_client']=='on') ){?>
                                <div class="form-group row radio_bts ">
                                <label class="col-sm-4 col-form-label">Meeting with the Client</label>
                                <div class="col-sm-8">
                                <input type="checkbox" class="js-small f-right fields" name="meeting_client" id="meeting_client" <?php if(isset($firm['crm_assign_meeting_client']) && ($firm['crm_assign_meeting_client']=='on') ){?> checked="checked"<?php } ?> readonly>
                                </div>  
                                </div>
                                <?php } ?>
                                </div>
                                </div>
                                </div>
                                </div>
                                </div>
                                    <!-- 4tab close -->
                                    <!-- 5tab start -->
                                    <div id="other" class="tab-pane fade">

                                    <div class="accordion-heading" role="tab" id="headingOne">
                                    <h3 class="card-title accordion-title">
                                    <a class="accordion-msg">Other</a>
                                    </h3>
                                    </div>

                                    
                                     <div class="accordion-panel">
                                          <div class="box-division03">
                                             <div class="accordion-heading" role="tab" id="headingOne">
                                                <h3 class="card-title accordion-title">
                                                   <a class="accordion-msg">
                                                      <label>Previous Accounts</label>
                                                      <input type="checkbox" class="js-small f-right fields" name="previous_account" id="previous_account" value="on" <?php if(isset($firm['crm_previous_accountant']) && ($firm['crm_previous_accountant']=='on') ){?> checked="checked"<?php } ?> data-id="preacc">
                                                   </a>
                                                </h3>
                                             </div>
                                              <div id="collapse" class="panel-collapse">
                                                <div class="basic-info-client1" id="others_section">

                                                
                                                <?php 
                                                (isset($firm['crm_previous_accountant']) && $firm['crm_previous_accountant'] == 'on') ? $pre =  "block" : $pre = 'none';                     

                                                ?>
                                                <div id="enable_preacc" style="display:<?php echo $pre;?>;">

                                                <?php if(isset($firm['crm_other_name_of_firm']) && ($firm['crm_other_name_of_firm']!='') ){ ?>
                                                <div class="form-group row name_fields">
                                                <label class="col-sm-4 col-form-label">Name of the Firm</label>
                                                <div class="col-sm-8">

                                                <span class="small-info"><?php echo $firm['crm_other_name_of_firm'];?></span>
                                                </div>
                                                </div>
                                                <?php } ?>

                                                <?php if(isset($firm['crm_other_address']) && ($firm['crm_other_address']!='') ){ ?>
                                                <div class="form-group row ">
                                                <label class="col-sm-4 col-form-label">Address</label>
                                                <div class="col-sm-8">

                                                <p class="for-address"><?php  echo $firm['crm_other_address'];?></p>
                                                </div>
                                                </div>
                                                <?php } ?>
                                                <!-- preacc close-->
                                                <?php if(isset($firm['crm_other_contact_no']) && ($firm['crm_other_contact_no']!='') ){  ?>
                                                <div class="form-group row name_fields others_details">
                                                <label class="col-sm-4 col-form-label">Contact Tel</label>
                                                <div class="col-sm-8">

                                                <span class="small-info"><?php 
                                                  $code = explode('-', $firm['crm_other_contact_no'],2);
                                                  echo $code[0]."  ".$code[1];
                                                 ?>
                                                 </span>
                                                </div>
                                                </div>
                                                <?php } ?>

                                                <?php if(isset($firm['crm_email']) && ($firm['crm_email']!='') ){  ?>
                                                <div class="form-group row name_fields others_details" >
                                                <label class="col-sm-4 col-form-label">Email Address</label>
                                                <div class="col-sm-8">
                                                <span class="small-info"><?php if(isset($firm['crm_email']) && ($firm['crm_email']!='') ){ echo $firm['crm_email'];}?></span>
                                                </div>
                                                </div>
                                                <?php } ?>


                                                </div>

                                                </div>
                                                </div> 
                                          </div>
                                    </div>                                             





                                     




                                    <!-- accordion-panel -->
                                    <div class="accordion-panel">
                                    <div class="box-division03">
                                    <div class="accordion-heading" role="tab" id="headingOne">
                                    <h3 class="card-title accordion-title">
                                    <a class="accordion-msg">Additional Information - Internal Notes</a>
                                    </h3>
                                    </div>
                                    <div id="collapse" class="panel-collapse">
                                    <div class="basic-info-client1">
                                    <?php if(isset($firm['crm_other_internal_notes']) && ($firm['crm_other_internal_notes']!='') ){ ?>
                                    <div class="form-group row ">
                                    <label class="col-sm-4 col-form-label">Notes</label>
                                    <div class="col-sm-8">                      
                                    <p class="for-address"><?php if(isset($firm['crm_other_internal_notes']) && ($firm['crm_other_internal_notes']!='') ){ echo $firm['crm_other_internal_notes'];}?></p>
                                    </div>
                                    </div>
                                    <?php } ?>

                                    <div class="form-group row name_fields">
                                    <label class="col-sm-4 col-form-label">Number Of Clients Subscribed</label>
                                    <div class="col-sm-8">                        
                                    <span class="small-info"><?php if(isset($firm['no_of_clients_allowed'])){ echo $firm['no_of_clients_allowed']; }?></span>
                                    </div>
                                    </div>

                                    </div>
                                    </div>
                                    </div>
                                    </div>
                                    <!-- accordion-panel -->
                                    <!-- accordion-panel -->
                                    <div class="accordion-panel">
                                    <div class="box-division03">
                                    <div class="accordion-heading" role="tab" id="headingOne">
                                    <h3 class="card-title accordion-title">
                                    <a class="accordion-msg" style="display: inline-block;">Firm Admin Login                                  
                                    </a>
                                    </h3>

                                    </div>
                                    <div id="collapse" class="panel-collapse show_login" >
                                    <div class="basic-info-client1">
                                    <!-- 30-08-2018 -->
                                    <?php if(isset($user['username']) && ($user['username']!='') ){ ?>
                                    <div class="form-group row name_fields">
                                    <label class="col-sm-4 col-form-label">Username</label>
                                    <div class="col-sm-8">                        
                                    <span class="small-info"><?php if(isset($user['username']) && ($user['username']!='') ){ echo $user['username'];}?></span>
                                    </div>
                                    </div>
                                    <?php } ?>

                                  

                                    <!-- end of 30-08-2018 -->
                                    </div>
                                    </div>
                                    </div>
                                    </div>
                                    <!-- accordion-panel -->



                                    <!-- accordion-panel -->  

                                    <div class="accordion-panel" style="display: none;">
                                    <div class="box-division03">
                                    <div class="accordion-heading" role="tab" id="headingOne">
                                    <h3 class="card-title accordion-title">
                                    <a class="accordion-msg">Notes if any other</a>
                                    </h3>
                                    </div>
                                    <div id="collapse" class="panel-collapse">
                                    <div class="basic-info-client1">
                                    <?php if(isset($firm['crm_other_any_notes']) && ($firm['crm_other_any_notes']!='') ){  ?>
                                    <div class="form-group row ">
                                    <label class="col-sm-4 col-form-label">Notes</label>
                                    <div class="col-sm-8">
                                    <p class="for-address"><?php if(isset($firm['crm_other_any_notes']) && ($firm['crm_other_any_notes']!='') ){ echo $firm['crm_other_any_notes'];}?></p>
                                    </div>
                                    </div>
                                    <?php } ?>

                                    </div>
                                    </div>
                                    </div>
                                    </div>

                                    </div>
                                    <!-- other tab close -->  

                                    <!-- package Section -->
                                    <div id="package_tab" class="fullwidth-animation3 tab-pane fade ">
                                       <div class="space-required">
                                          <div class="main-pane-border1" id="basic_details_error">
                                             <div class="alert-ss"></div>
                                          </div>
                                       </div>
                                       <div class="space-required">
                                          <div class="white-space01">
                                             <div class="pane-border1 management_accordion col-sm-6">
                                                <?php
                                                /*in new firm that hide*/
                                                $is_percl   =  'none';
                                                $is_perus   =  'none';
                                                $has_trial  =  'none';


                                                /*in edit page check which type plan*/
                                                if( !empty( $subscribed_plan ) && $subscribed_plan['unlimited']== 1 )
                                                   $is_perus = 'block !important';
                                                else if( !empty( $subscribed_plan ) && $subscribed_plan['unlimited']!= 1  )
                                                   $is_percl = 'block !important';
                                                if( !empty( $subscribed_plan['trial'] ) && $subscribed_plan['trial']==1 )
                                                   $has_trial = 'block !important';                                                   

                                                ?>
                                              
                                                <div class="form-group row radio_bts date_birth">
                                                  <label class="col-sm-4 col-form-label">Subscription Plan</label>
                                                  <div class="col-sm-8">
                                                    <span class="small-info">
                                                      <?php 
                                                        //active plans 
                                                        if( !empty( $subscribed_plan['id'] ) )
                                                        {
                                                          echo $subscribed_plan['plan_name'];
                                                        } 
                                                        ?>
                                                    </span>
                                                  </div>
                                                </div>

                                                <div class="form-group row radio_bts date_birth" style="display: <?php echo $is_percl;?>">
                                                  <label class="col-sm-4 col-form-label">Client Limit</label>
                                                  <div class="col-sm-8">
                                                    <span class="small-info">
                                                      <?php
                                                      if( !empty( $firm['no_of_clients_allowed'] ) )
                                                        {
                                                          echo $firm['no_of_clients_allowed'];
                                                        }
                                                      ?>
                                                    </span>
                                                  </div>
                                                </div>

                                                <div class="form-group row radio_bts date_birth" style="display: <?php echo $is_perus;?>">
                                                  <label class="col-sm-4 col-form-label">User Limit</label>
                                                  <div class="col-sm-8">
                                                    <span class="small-info">
                                                      <?php
                                                      if( !empty( $firm['no_of_users_allowed'] ) )
                                                        {
                                                          echo $firm['no_of_users_allowed'];
                                                        }
                                                      ?>
                                                    </span>
                                                  </div>
                                                </div>

                                                <div class="form-group row radio_bts date_birth" style="display: <?php echo $has_trial;?>">
                                                  <label class="col-sm-4 col-form-label">Trial End Date</label>
                                                  <div class="col-sm-8">
                                                    <span class="small-info">
                                                      <?php
                                                      if(!empty( $firm['trial_end_date'] ) )
                                                        {
                                                          echo $firm['trial_end_date'];
                                                        }
                                                      ?>
                                                    </span>
                                                  </div>
                                                </div>


                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <!-- package Section --> 
                              
                                  <!-- 6tab start -->

                                  <!-- Business Details -->
                                  <div id="business-det" class="tab-pane fade ">
                                  <div class="masonry-container floating_set ">
                                  <div class="grid-sizer"></div>
                                  <!-- accordion-panel -->
                                  <div class="accordion-panel">
                                  <div class="box-division03">
                                  <div class="accordion-heading" role="tab" id="headingOne">
                                    <h3 class="card-title accordion-title">
                                      <a class="accordion-msg">Business Details</a>
                                    </h3>
                                  </div>
                                  <div id="collapse" class="panel-collapse">
                                  <div class="basic-info-client1">

                                  <?php  if(!empty($legalform['trading_as'])){ ?>
                                  <div class="form-group row radio_bts ">
                                  <label class="col-sm-4 col-form-label">Trading As</label>
                                  <div class="col-sm-8">
                                  <span class="small-info"><?php echo $legalform['trading_as'];?></span>

                                  </div>
                                  </div>
                                  <?php } ?>

                                  <?php  if(!empty($legalform['trading_as'])){ ?>

                                  <div class="form-group row radio_bts date_birth">
                                  <label class="col-sm-4 col-form-label">Commenced Trading</label>
                                  <div class="col-sm-8">
                                  <span class="small-info"><?php echo $legalform['commenced_trading']; ?></span>
                                  </div>
                                  </div>
                                  <?php } ?>

                                  <?php  if(!empty($legalform['register_sa'])){ ?>                                                         
                                  <div class="form-group row radio_bts date_birth">
                                  <label class="col-sm-4 col-form-label">Registered for SA</label>
                                  <div class="col-sm-8">
                                  <span class="small-info"><?php echo $legalform['register_sa']; ?></span>
                                  </div>
                                  </div>
                                  <?php } ?>
                                  <?php  if(!empty($legalform['bus_turnover'])){ ?>
                                  <div class="form-group row radio_bts ">
                                    <label class="col-sm-4 col-form-label">Turn Over</label>
                                    <div class="col-sm-8">
                                      <span class="small-info"><?php echo $legalform['bus_turnover']; ?></span>
                                    </div>
                                  </div>
                                  <?php } ?>

                                  <?php  if(!empty($legalform['bus_nuture_of_business'])){ ?>

                                    <div class="form-group row radio_bts ">
                                      <label class="col-sm-4 col-form-label">Nuture Of Business</label>
                                      <div class="col-sm-8">
                                      <span class="small-info"><?php echo $legalform['bus_nuture_of_business']; ?></span>
                                      </div>
                                    </div>
                                  <?php } ?>
                                  </div>
                                  </div>
                                  </div>
                                  </div>
                                  <!-- accordion-panel -->   
                                  </div>
                                  </div>
                                  <!-- Business Details -->
                                  <!-- 6tab close -->
                                    
                                 </div>
                                 <!-- tab-content -->
                              </div>
                </div>
                           </div>
                           <!-- managementclose -->
                        </form>
                        <!-- admin close-->
                     </div>
                  </div>
               </div>
            </div>
            <!-- Page body end -->
         </div>
      </div>
      <!-- Main-body end -->
      <div id="styleSelector">
      </div>
   </div>
</div>
</div>
</div>
</div>
</div>

<!-- modal 1-->

<!-- modal-close -->
<!-- modal 2-->

<div class="modal fade" id="myAlert" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Alert</h4>
         </div>
         <div class="modal-body">
            <p>  
               Do you want to Exit? 
            </p>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default no" data-dismiss="modal">No</button> 
            <button type="button" class="btn btn-default exit">Yes</button>        
         </div>
      </div>
   </div>
</div><!-- add reminder -->
<!-- ajax loader -->
<!-- ajax loader end-->

<?php $this->load->view('super_admin/superAdmin_footer');?>

<!-- <script type="text/javascript" src="https://remindoo.uk/assets/js/add_client/checkbox_toggleAction.js"></script> -->

<!-- <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script>
 --><!-- <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-ui/js/jquery-ui.min.js"></script> -->
<!-- <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
 -->
 <!-- <script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap/js/bootstrap.min.js"></script> -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/masonry/3.3.1/masonry.pkgd.min.js"></script>
<!-- <script type="text/javascript" src="<?php echo base_url();?>bower_components/popper.js/js/popper.min.js"></script>
 --><!-- j-pro js -->
<!-- <script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.ui.min.js"></script> -->
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.j-pro.js"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<!-- modernizr js -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/modernizr/js/css-scrollbars.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/advance-elements/custom-picker.js"></script>
<script src="<?php echo base_url();?>assets/js/pcoded.min.js"></script>
<script src="<?php echo base_url();?>assets/js/demo-12.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/common_script.js"></script>
<!-- <script type="text/javascript" src="<?php echo base_url();?>assets/js/client_page.js"></script>
 -->
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript">   
  
   // prev click
   $('.signed-change2').click(function(){
      $('ol.nav-tabs .active').closest('li').prevAll('li:visible').eq(0).find('a').trigger('click');
      //console.log( $('ol.nav-tabs .active').closest('li').prevAll('li:visible').eq(0).find('a').attr('class') );
   });
   // next click
   $('.signed-change3').click(function(){
      $('ol.nav-tabs .active').closest('li').nextAll("li:visible").eq(0).find('a').trigger('click');
      //console.log( $('ol.nav-tabs .active').closest('li').nextAll("li:visible").eq(0).find('a').attr('class') );
   });

    $(".nav-tabs li.nav-item a.nav-link").click(function(){

      var id = $(this).attr('data-id');
      $(this).closest(".nav-tabs").find("a.nav-link").removeClass('active');
      $(this).addClass('active');
      
      $(this).closest(".nav-tabs").find("li.nav-item").removeClass("active");
      $(this).parent().addClass("active");      

      $(".newupdate_design .tab-content .tab-pane").removeClass("active show");
      $(".newupdate_design .tab-content").find(id).addClass("active show");
      
      var items = $(".nav-tabs li.nav-item:visible");
         items.removeClass("active-previous");
      var item_len = items.length - 1;

      for( var i=0;i<=item_len;i++)
      {
         if( $(items[i]).hasClass('active') )
         {
            break;
         }
         $(items[i]).addClass("active-previous");
      }

      if(i==item_len)
      {
         $('.signed-change3').hide();
      }
      else
      {
         $('.signed-change3').show();    
      }

      if(i==0)
      {
         $('.signed-change2').hide();
      }
      else
      {
         $('.signed-change2').show();    
      }



   });




     $(document).ready(function(){

       $('.step-tabs').on('click', function(){
           $('html,body').animate({scrollTop: $(this).offset().top}, 800);
       }); 
   
   
   
  /* $('.btnNext').click(function(){   
   
             $('.nav-tabs > .bbs').nextAll('li').not(".hide").first().find('a').trigger('click');
   
       }
   );
   
   
     $('.btnPrevious').click(function(){
     $('.nav-tabs > .bbs').prevAll('li').not(".hide").first().find('a').trigger('click');
   });

  
   $(".nav-tabs a").click(function(event) {
       event.preventDefault();
       $(this).parent().addClass("bbs");
       $(this).parent().siblings().removeClass("bbs");
   });*/
   
     $('.js-small').parent('.col-sm-8').addClass('addbts-radio');
     $('.accordion-views .toggle').click(function(e) {
      $(this).next('.inner-views').slideToggle(300);
      $(this).toggleClass('this-active');
   });

      var selectVal = "<?php echo $firm['crm_company_type'];?>";
     
   if((selectVal == "Private Limited company") || (selectVal == "Public Limited company") || (selectVal == "Limited Liability Partnership"))
      {
         $('.business-info-tab').hide();
         $('.basic_details_tab').show();
      }
      else if((selectVal == "Partnership") || (selectVal == "Self Assessment"))
      {
         $('.business-info-tab').show();
         $('.basic_details_tab').hide();
      }
      else
      {
         $('.business-info-tab').hide();
         $('.basic_details_tab').hide();
      }

   });
</script>
   
</body>
</html>