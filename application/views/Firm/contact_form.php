<?php
$j = 1;
//foreach ($rec as $key => $value) {
$contact_names = $this->Common_mdl->numToOrdinalWord($cnt) . ' Contact ';

$value['title'] = $title;
$value['first_name'] = $first_name;
$value['surname'] = $surname;
$value['preferred_name'] = $preferred_name;
$value['address_line1'] = $address_line1;
$value['address_line2'] = $address_line2;
$value['premises'] = $premises;
$value['region'] = $region;
$value['country'] = $country;
$value['locality'] = $locality;
$value['post_code'] = $post_code;
$value['nationality'] = $nationality;
$value['occupation'] = $occupation;
$value['appointed_on'] = $appointed_on;
$value['country_of_residence'] = $country_of_residence;
$value['date_of_birth'] = $date_of_birth;
$value['psc'] = $psc;
$value['nature_of_control'] = $nature_of_control;
$value['contact_from'] = $contact_from;
$cnt;
//$value['created_date'] = time();

?>
<!--      <input type="hidden" name="contact_id[]" id="contact_id" value="<?php //echo $value['id'];
                                                                            ?>">
 -->

<div class="space-required new_update1 <?php echo $contact_from; ?>">
    <div class="update-data01 make_a_primary common_div_remove-<?php echo $cnt; ?>" id="common_div_remove-<?php echo $cnt; ?>">
        <?php
        /*this run while data from company_house*/
        if (!empty($contact_id)) { ?>
            <input type="hidden" class="contact_table_id" name="contact_table_id[]" value="<?php echo $contact_id; ?>">
        <?php } ?>
        <div class="main-contact append_contact">
            <span class="h4">
                <!-- <?php echo $contact_names; ?> --> Contact Person details</span>
            <div class="dead-primary1 for_row_count-<?php echo $cnt; ?>">
                <!-- sync with company details button -->
                <div class="radio radio-inline">
                    <a href="javascript:void(0)" class="sync_with_company active" data-id="sync_with_company<?php echo $cnt; ?>"><span>SYNC WITH COMPANY</span></a>
                </div>
                <input type="hidden" name="make_primary_loop[]" id="make_primary_loop" value="<?php echo $cnt; ?>">
                <div class="radio radio-inline">
                    <label>
                        <i class="helper" style="display: none;"></i>
                        <a href="javascript:void(0)" class="make_primary_section" data-id="make_primary<?php echo $cnt; ?>"><span>Make a primary</span></a>
                        <input type="radio" name="make_primary" id="make_primary<?php echo $cnt; ?>" value="<?php echo $cnt; ?>">

                        <!--  <?php if (isset($value['contact_from']) && ($value['contact_from'] != '')) { ?>
                                                <a href="javascript:void(0)" class="Edit_contact"  data-id="edit<?php echo $cnt; ?>" ><span>Edit</span></a>
                                                <?php } ?> -->
                    </label>
                </div>
            </div>

        </div>

        <div class="primary-info addnewclient">




            <span class="primary-inner  <?php if (isset($title) && $title != '') { ?>light-color_company<?php } ?>">
                <label>title</label>
                <!-- <input type="text" class="text-info title"  name="title[]" id="title" value="<?php echo $title; ?>"> -->
                <select class="text-info title" name="title" id="title">

                    <option value="Mr" <?php if (isset($title) && $title == 'Mr') { ?> selected="selected" <?php } ?>>Mr</option>
                    <option value="Mrs" <?php if (isset($title) && $title == 'Mrs') { ?> selected="selected" <?php } ?>>Mrs</option>
                    <option value="Miss" <?php if (isset($title) && $title == 'Miss') { ?> selected="selected" <?php } ?>>Miss</option>
                    <option value="Dr" <?php if (isset($title) && $title == 'Dr') { ?> selected="selected" <?php } ?>>Dr</option>
                    <option value="Ms" <?php if (isset($title) && $title == 'Ms') { ?> selected="selected" <?php } ?>>Ms</option>
                    <option value="Prof" <?php if (isset($title) && $title == 'Prof') { ?> selected="selected" <?php } ?>>Prof</option>
                </select>
            </span>


            <span class="primary-inner    <?php
                                            if (isset($value['first_name']) && ($value['first_name'] != '')) { ?>light-color_company<?php } ?>">
                <label>first name
                    <span class="Hilight_Required_Feilds">*</span>
                </label>
                <input type="text" name="first_name[<?php echo $cnt; ?>]" placeholder="" value="<?php if (isset($value['first_name']) && ($value['first_name'] != '')) {
                                                                                                    echo $value['first_name'];
                                                                                                } ?>" class="text-info">
            </span>



            <span class="primary-inner <?php if (isset($value['middle_name']) && ($value['middle_name'] != '')) { ?>light-color_company<?php } ?>">
                <label>middle name</label>
                <input type="text" name="middle_name[]" id="middle_name" placeholder="" value="<?php if (isset($value['middle_name']) && ($value['middle_name'] != '')) {
                                                                                                    echo $value['middle_name'];
                                                                                                } ?>" class="text-info">
            </span>


            <span class="primary-inner <?php if (isset($value['last_name']) && ($value['last_name'] != '')) { ?>light-color_company<?php } ?>">
                <label>Last name</label>
                <input type="text" name="last_name[]" id="last_name" placeholder="" value="<?php if (isset($value['last_name']) && ($value['last_name'] != '')) {
                                                                                                echo $value['last_name'];
                                                                                            } ?>" class="text-info">
            </span>


            <span class="primary-inner   <?php if (isset($value['surname']) && ($value['surname'] != '')) { ?>light-color_company<?php } ?>">
                <label>surname</label>
                <input type="text" name="surname[]" id="surname" placeholder="" value="<?php if (isset($value['surname']) && ($value['surname'] != '')) {
                                                                                            echo $value['surname'];
                                                                                        } ?>" class="text-info">
            </span>


            <span class="primary-inner <?php if (isset($value['preferred_name']) && ($value['preferred_name'] != '')) { ?>light-color_company<?php } ?>">
                <label>prefered name</label>
                <input type="text" name="preferred_name[]" id="preferred_name" placeholder="" value="<?php if (isset($value['preferred_name']) && ($value['preferred_name'] != '')) {
                                                                                                            echo $value['preferred_name'];
                                                                                                        } ?>" class="text-info">
            </span>

            <span class="primary-inner   <?php if (isset($value['mobile']) && ($value['mobile'] != '')) { ?>light-color_company<?php } ?>">
                <label>mobile</label>
                <input type="text" name="mobile_number[<?php echo $cnt; ?>]" pattern="\d{3}[\-]\d{3}[\-]\d{4}" class="text-info" value="<?php if (isset($value['mobile']) && ($value['mobile'] != '')) {
                                                                                                                                            echo $value['mobile'];
                                                                                                                                        } ?>">
            </span>


            <span class="primary-inner   <?php if (isset($value['main_email']) && ($value['main_email'] != '')) { ?>light-color_company<?php } ?>">
                <label>main E-Mail address</label>
                <input type="email" name="main_email[<?php echo $cnt; ?>]" class="text-info" value="<?php if (isset($value['main_email']) && ($value['main_email'] != '')) {
                                                                                                        echo $value['main_email'];
                                                                                                    } ?>">
            </span>


            <span class="primary-inner   <?php if (isset($value['nationality']) && ($value['nationality'] != '')) { ?>light-color_company<?php } ?>">
                <label>Nationality</label>
                <input type="text" name="nationality[]" id="nationality" class="text-info" value="<?php if (isset($value['nationality']) && ($value['nationality'] != '')) {
                                                                                                        echo $value['nationality'];
                                                                                                    } ?>">
            </span>


            <span class="primary-inner  <?php if (isset($value['psc']) && ($value['psc'] != '')) { ?>light-color_company<?php } ?>">
                <label>PSC</label>
                <input type="text" name="psc[]" id="psc" class="text-info" value="<?php if (isset($value['psc']) && ($value['psc'] != '')) {
                                                                                        echo $value['psc'];
                                                                                    } ?>">
            </span>


            <span class="primary-inner">
                <label>shareholder</label>
                <select name="shareholder" id="shareholder">
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
            </span>

            <span class="primary-inner <?php if (isset($value['ni_number']) && ($value['ni_number'] != '')) { ?>light-color_company<?php } ?>">
                <label>national insurance number</label>
                <input type="text" name="ni_number[]" id="ni_number" class="text-info" value="<?php if (isset($value['ni_number']) && ($value['ni_number'] != '')) {
                                                                                                    echo $value['ni_number'];
                                                                                                } ?>">
            </span>


            <span class="primary-inner  <?php if (isset($value['country_of_residence']) && ($value['country_of_residence'] != '')) { ?>light-color_company<?php } ?>">
                <label>Country Of Residence</label>
                <input type="text" name="country_of_residence[]" id="country_of_residence" class="text-info" value="<?php if (isset($value['country_of_residence']) && ($value['country_of_residence'] != '')) {
                                                                                                                        echo $value['country_of_residence'];
                                                                                                                    } ?>">
            </span>


            <span class="primary-inner contact_type">
                <label>Contact type</label>
                <select name="contact_type" id="contact_type" class="othercus">
                    <option value="Director" selected="selected">Director</option>
                    <option value="Director/Shareholder">Director/Shareholder</option>
                    <option value="Shareholder">Shareholder</option>
                    <option value="Accountant">Accountant</option>
                    <option value="Bookkeeper">Bookkeeper</option>
                    <option value="Other">Other(Custom)</option>
                </select>
            </span>


            <span class="primary-inner spnMulti  <?php if (isset($value['other_custom']) && ($value['other_custom'] != '')) { ?>light-color_company<?php } ?>" id="others_customs" style="display:none">
                <label>Other(Custom)</label>
                <input type="text" class="text-info" name="other_custom[]" id="other_custom" value="<?php if (isset($value['other_custom']) && ($value['other_custom'] != '')) {
                                                                                                        echo $value['other_custom'];
                                                                                                    } ?>">
            </span>


            <span class="primary-inner <?php if (isset($value['address_line1']) && ($value['address_line1'] != '')) { ?>light-color_company<?php } ?>">
                <label>address line1</label>
                <input type="text" name="address_line1[]" id="address_line1" class="text-info" value="<?php if (isset($value['address_line1']) && ($value['address_line1'] != '')) {
                                                                                                            echo $value['address_line1'];
                                                                                                        } ?>">
            </span>


            <span class="primary-inner <?php if (isset($value['address_line2']) && ($value['address_line2'] != '')) { ?>light-color_company<?php } ?>">
                <label>adress line2</label>
                <input type="text" name="address_line2[]" id="address_line2" class=" text-info" value="<?php if (isset($value['address_line2']) && ($value['address_line2'] != '')) {
                                                                                                            echo $value['address_line2'];
                                                                                                        } ?>">
            </span>
            <span class="primary-inner <?php if (isset($value['town_city']) && ($value['town_city'] != '')) { ?>light-color_company<?php } ?>">
                <label>town/city</label>
                <input type="text" name="town_city[]" id="town_city" class=" text-info" value="<?php if (isset($value['town_city']) && ($value['town_city'] != '')) {
                                                                                                    echo $value['town_city'];
                                                                                                } ?>">
            </span>


            <span class="primary-inner <?php if (isset($value['post_code']) && ($value['post_code'] != '')) { ?>light-color_company<?php } ?>">
                <label>post code</label>
                <input type="text" name="post_code[]" id="post_code" class="text-info" value="<?php if (isset($value['post_code']) && ($value['post_code'] != '')) {
                                                                                                    echo $value['post_code'];
                                                                                                } ?>">
            </span>

            <span class="primary-inner ">
                <div class="update-primary pack_add_row_wrpr_landline">

                    <label>landline</label>
                    <div class="remove_one_row">
                        <select name="pre_landline[<?php echo $cnt; ?>][0]" id="pre_landline">
                            <option value="mobile">Mobile</option>
                            <option value="work">Work</option>
                            <option value="home">Home</option>
                            <option value="main">Main</option>
                            <option value="workfax">Work Fax</option>
                            <option value="homefax">Home Fax</option>
                        </select>

                        <div class="land-spaces  <?php if (isset($value['landline']) && ($value['landline'] != '')) { ?>light-color_company<?php } ?>">
                            <input type="text" class="text-info" name="landline[<?php echo $cnt; ?>][0]" value="<?php if (isset($value['landline']) && ($value['landline'] != '')) {
                                                                                                                    echo $value['landline'];
                                                                                                                } ?>">
                        </div>
                        <button type="button" class="btn btn-primary yk_pack_addrow_landline" data-id="<?php echo $cnt; ?>" data-length="0">Add Landline</button>

                        <!--  <span class="success  text-left <<?php if (isset($value['contact_from']) && ($value['contact_from'] != '')) { ?>field_hide <?php } else { ?>light-color_company<?php } ?>">
                              </span> -->

                    </div>

                </div>

            </span>





            <span class="primary-inner  work-email_section">
                <div class="update-primary work_lightmail pack_add_row_wrpr_email">
                    <label>work email</label>
                    <div class="remove_one_row ">
                        <input type="text" class="text-info" name="work_email[<?php echo $cnt; ?>][0]" value="<?php if (isset($value['work_email']) && ($value['work_email'] != '')) {
                                                                                                                    echo $value['work_email'];
                                                                                                                } ?>">


                        <button type="button" class="btn btn-primary yk_pack_addrow_email" data-id="<?php echo $cnt; ?>" data-length="0">Add Email</button>

                        <!-- <span class="success<?php if (isset($value['contact_from']) && ($value['contact_from'] != '')) { ?> field_hide <?php } ?>">
                              </span> -->
                    </div>
                </div>
            </span>

            <span class="primary-inner date_birth  <?php if (isset($value['date_of_birth']) && ($value['date_of_birth'] != '')) { ?>light-color_company<?php } ?>">
                <label>date of birth</label>
                <div class="picker-appoint"><span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span><input type="text" class="text-info date_picker_dob fields" placeholder="dd-mm-yyyy" name="date_of_birth[<?php echo $cnt; ?>]" id="date_of_birth" value="<?php if (isset($value['date_of_birth']) && ($value['date_of_birth'] != '')) {
                                                                                                                                                                                                                                                                                                echo $value['date_of_birth'];
                                                                                                                                                                                                                                                                                            } ?>">
                </div>
            </span>


            <span class="primary-inner   <?php if (isset($value['nature_of_control']) && ($value['nature_of_control'] != '')) { ?>light-color_company<?php } ?>">
                <label>nature of control</label>
                <input type="text" class="text-info" id="nature_of_control" name="nature_of_control[]" id="nature_of_control" value="<?php if (isset($value['nature_of_control']) && ($value['nature_of_control'] != '')) {
                                                                                                                                            echo $value['nature_of_control'];
                                                                                                                                        } ?>">
            </span>

            <span class="primary-inner">
                <label>marital status</label>

                <select name="marital_status" id="marital_status">

                    <option value="Single">Single</option>
                    <option value="Living_together">Living together</option>
                    <option value="Engaged">Engaged</option>
                    <option value="Married">Married</option>
                    <option value="Civil_partner">Civil partner</option>
                    <option value="Separated">Separated</option>
                    <option value="Divorced">Divorced</option>
                    <option value="Widowed">Widowed</option>
                </select>
            </span>


            <span class="primary-inner  <?php if (isset($value['utr_number']) && ($value['utr_number'] != '')) { ?>light-color_company<?php } ?>">
                <label>utr number</label>
                <input type="number" class="text-info" id="utr_number" name="utr_number[]" id="utr_number" value="<?php if (isset($value['utr_number']) && ($value['utr_number'] != '')) {
                                                                                                                        echo $value['utr_number'];
                                                                                                                    } ?>">
            </span>


            <span class="primary-inner  <?php if (isset($value['occupation']) && ($value['occupation'] != '')) { ?>light-color_company<?php } ?>">
                <label>Occupation</label>
                <input type="text" class="text-info" id="occupation" name="occupation[]" value="<?php if (isset($value['occupation']) && ($value['occupation'] != '')) {
                                                                                                    echo $value['occupation'];
                                                                                                } ?>">
            </span>



            <span class="primary-inner date_birth  <?php if (isset($value['appointed_on']) && ($value['appointed_on'] != '')) { ?>light-color_company<?php } ?> ">
                <label>Appointed On</label>
                <div class="picker-appoint">
                    <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                    <input type="text" placeholder="dd-mm-yyyy" class="text-info date_picker_dob" id="appointed_on" name="appointed_on[]" value="<?php if (isset($value['appointed_on']) && ($value['appointed_on'] != '')) {
                                                                                                                                                        echo $value['appointed_on'];
                                                                                                                                                    } ?>">
                </div>
            </span>

            <!--  <input type="hidden" name="make_primary[]" id="make_primary" value="0"> -->
            <?php $j++; // } 
            ?>

        </div>

    </div>
</div>