<?php 

 $firm=$client[0];
 $this->load->view('super_admin/superAdmin_header');
  // $this->load->view('includes/header');

   if(isset($_GET['action']))
   {
      $action = $_GET['action'];
   }
   else
   {
      $action = 'mannual';
   }
      $uri_seg = $this->uri->segment(3);
   ?>

<div class="modal-alertsuccess  alert alert-success-reminder succs" style="display: none;">
   <div class="newupdate_alert">
   <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   <div class="pop-realted1">
      <div class="position-alert1">Reminder Added Successfully.</div>
   </div></div>
</div>
<div class="pcoded-content card-removes clientredesign firminfocls">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card">
                        <!-- admin start-->
                        <?php 
                           /*echo "<pre>";
                           print_r($firm);echo "</pre>";*/ ?>
                        <div class="modal-alertsuccess alert alert-success" style="display:none;">
                          <div class="newupdate_alert"> <a href="javascript:;" class="close alert_close"  aria-label="close">x</a>
                           <div class="pop-realted1">
                              <div class="position-alert1 sample_check">
                                 YOU HAVE SUCCESSFULLY ADDED CLIENT VIA MANUAL ADD - DO YOU WANT TO 
                                 <div class="alertclsnew">
                                    <a href="<?php echo base_url(); ?>client/add_firm"> ADD ANOTHER Firm </a>                             
                                    <a href="<?php echo base_url(); ?>firm/user"> GO BACK</a>
                                 </div>
                              </div>
                           </div></div>
                        </div>
                      
                        <div class="modal-alertsuccess alert alert-danger" style="display:none;">
                           <div class="newupdate_alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                           <div class="pop-realted1">
                              <div class="position-alert1">
                                 Please fill the required fields.
                              </div>
                           </div></div>
                        </div>

                        <form id="insert_form" class="validation modal-addnew1" method="post" action="" enctype="multipart/form-data">
                           <!-- 06-07-2018 for addreminders ids-->
                           <input type="hidden" id="add_reminder_ids" name="add_reminder_ids" value=''>
                           <!-- end of 06-07-2018 -->
                           <div class="remin-admin">
                              <div class="space-required">
                                 <div class="deadline-crm1 floating_set">
                                    <ul class="nav nav-tabs1 all_user1 md-tabs pull-left u-dashboard">
                                       <li class="nav-item">
                                          <a class="nav-link" href="<?php echo base_url().'Firm';?>">
                                          <img class="themicon" src="<?php echo base_url();?>assets/images/tabicon1.png" alt="themeicon" />
                                       All Firm</a>
                                          <div class="slide"></div>
                                       </li>
                                       <li class="nav-item">
                                          <a class="nav-link active" href="#addnewclient">
                                             <img class="themicon" src="<?php echo base_url();?>assets/images/tabicon2.png" alt="themeicon" />Edit Firm</a>
                                          <div class="slide"></div>
                                       </li>
                          
                                    </ul>
                                    <div class="Footer common-clienttab pull-right">
                                       <div class="change-client-bts1">
                                       
                                        
                                       <input type="submit" class="signed-change1"  value="Save">
      
                                       </div>
                                       <div class="divleft">
                                          <button  class="signed-change2"  type="button" value="Previous Tab" text="Previous Tab">Previous 
                                          </button>
                                       </div>
                                       <div class="divright">
                                          <button  class="signed-change3" type="button" value="Next Tab"  text="Next Tab">Next
                                          </button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              </div>

                             
                              <div class="space-required">
                                 <div class="document-center floating_set">
                                    <div class="Companies_House floating_set">
                                       <div class="pull-left form_heading">                                          
                                          <h2><?php if(isset($firm['crm_company_name']) && ($firm['crm_company_name']!='') ){ echo $firm['crm_company_name'];}?></h2>
                                       </div>
                                    </div>
                                    <div class="addnewclient_pages1 floating_set bg_new1">
                                       <ol class="nav nav-tabs all_user1 md-tabs floating_set" id="tabs">
                                         
                                          <li class="nav-item it0" value="0"><a class="nav-link
                                             active" data-toggle="tab" href="#required_information">
                                             Required information</a>
                                          </li>
                                          
                                          <li class="nav-item it18 basic_details_tab" value="18"><a class="nav-link basic_deatils_id" data-toggle="tab" href="#basic_details"> Basic Details</a></li>

                                          <li class="nav-item it1 for_contact_tab" value="1"><a class="nav-link main_contacttab" data-toggle="tab" href="#main_Contact">
                                             Contact</a>
                                          </li>
                                     
                                          <li class="nav-item it2 hide" value="2">
                                             <a class="nav-link" data-toggle="tab" href="#amlchecks"> AML Checks</a>
                                          </li>
                                          <li class="nav-item it2 hide business-info-tab" value="2" style="display: none;">
                                             <a class="nav-link" data-toggle="tab" href="#business-det"> Business Details</a>
                                          </li>

                                          <li class="nav-item it10 other_tabs" value="10"><a class="nav-link" data-toggle="tab" href="#other">
                                                <span id="other_tab_cnt"></span>
                                                <span id="old_cnt"></span> Other</a>
                                          </li>
                                        
                                          <input type="hidden" id="cnt_of_other" name="cnt_of_other" >
                                          <input type="hidden" class="service_value" id="service_value" >
                                       </ol>
                                       <!-- tab close -->
                                    </div>
                                 </div>
                              </div>
                       
                           <div class="newupdate_design">    
                           <div class="management_section floating_set realign newmanage-animation">
                              <div class="card-block accordion-block">
                                 <!-- tab start -->
                                 <div class="tab-content card">
                                                        <input type="hidden" name="user_id" id="user_id" value="<?php if(isset($client[0]['user_id']) && ($client[0]['user_id']!='') ){ echo $client[0]['user_id'];}?>" class="fields">

                  <div id="required_information" class="columns-two tab-pane fade in active <?php /*if( $firm === false  ){ echo "active";} */ ?>">
                                       <div class="space-required">
                                          <div class="main-pane-border1" id="required_information_error">
                                             <div class="alert-ss"></div>
                                          </div>
                                       </div>
                                       <div class="space-required">
                                          <div class="white-space01">
                                             <div class="pane-border1">
                                                <div class="management_form1">
                                                   <!--  <div class="form-titles"><a href="#" class="waves-effect" data-toggle="modal" data-target="#default-Modal"><h2>View on Companies House</h2></a></div> -->
                                                   <!--  <form> -->
                                                   <div class="form-group row name_fields">
                                                      <label class="col-sm-4 col-form-label">Name</label>
                                                      <div class="col-sm-8">
                                                         <input type="text" name="company_name" id="company_name" placeholder="" value="<?php $class='';if(isset($firm['crm_company_name']) && ($firm['crm_company_name']!='') ){ $class='light-color_company'; echo $firm['crm_company_name'];}?>" class="fields <?php echo $class;?>">
                                                      </div>
                                                   </div>
                                                   <div class="form-group row">
                                                      <label class="col-sm-4 col-form-label">Legal Form</label>
                                                      <div class="col-sm-8">
                                                         <select name="legal_form" class="form-control fields sel-legal-form" id="legal_form">
                                                            <!--  <option value="">Select</option> -->
                                                            <option value="Private Limited company" <?php if(isset($firm['crm_legal_form']) && $firm['crm_legal_form']=='Private Limited company') {?> selected="selected"<?php } ?> data-id="1">Private Limited company</option>
                                                            <option value="Public Limited company" <?php if(isset($firm['crm_legal_form']) && $firm['crm_legal_form']=='Public Limited company') {?> selected="selected"<?php } ?> data-id="1">Public Limited company</option>
                                                            <option value="Limited Liability Partnership" <?php if(isset($firm['crm_legal_form']) && $firm['crm_legal_form']=='Limited Liability Partnership') {?> selected="selected"<?php } ?> data-id="1">Limited Liability Partnership</option>
                                                            <option value="Partnership" <?php if(isset($firm['crm_legal_form']) && $firm['crm_legal_form']=='Partnership') {?> selected="selected"<?php } ?> data-id="2">Partnership</option>
                                                            <option value="Self Assessment" <?php if(isset($firm['crm_legal_form']) && $firm['crm_legal_form']=='Self Assessment') {?> selected="selected"<?php } ?> data-id="2">Self Assessment</option>
                                                            <option value="Trust" <?php if(isset($firm['crm_legal_form']) && $firm['crm_legal_form']=='Trust') {?> selected="selected"<?php } ?> data-id="1">Trust</option>
                                                            <option value="Charity" <?php if(isset($firm['crm_legal_form']) && $firm['crm_legal_form']=='Charity') {?> selected="selected"<?php } ?> data-id="1">Charity</option>
                                                            <option value="Other" <?php if(isset($firm['crm_legal_form']) && $firm['crm_legal_form']=='Other') {?> selected="selected"<?php } ?> data-id="1">Other</option>
                                                         </select>
                                                      </div>
                                                   </div>
                                                      <div class="form-group row">
                                                      <label class="col-sm-4 col-form-label">Firm Primary Mail ID</label>
                                                         <div class="col-sm-8">
                                                            <?php 
                                                            $firm_mailid = "";
                                                            if(!empty($firm['firm_mailid']))
                                                            {
                                                               $firm_mailid = $firm['firm_mailid'];
                                                            }
                                                            ?>
                                                            <input type="text" name="firm_mailid" value="<?php echo $firm_mailid;?>">
                                                         </div>
                                                      </div>

                                                      <div class="form-group row">
                                                      <label class="col-sm-4 col-form-label">Firm Primary Contact No </label>
                                                         <div class="col-sm-8">
                                                            <?php 
                                                            $firm_contact_no = "";
                                                            if(!empty($firm['firm_contact_no']))
                                                            {
                                                               $firm_contact_no = $firm['firm_contact_no'];
                                                            }
                                                            ?>
                                                            <input type="text" name="firm_contact_no" value="<?php echo $firm_contact_no;?>">
                                                         </div>
                                                      </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!--  <div class="button-group">
                                          <div class="floatleft">
                                             <button type="button" class="btn btn-primary btnNext">Next</button>
                                          </div>
                                          
                                          </div> -->
                                    </div>
                                    <!-- 1tab close-->

                                   <!-- 2nd tab start -->
                                   
                                    <div id="basic_details" class="fullwidth-animation3 tab-pane fade ">
                                    <div class="space-required">
                                          <div class="main-pane-border1" id="basic_details_error">
                                             <div class="alert-ss"></div>
                                          </div>
                                       </div>
                                       <div class="space-required">
                                          <div class="white-space01">
                                             <div class="pane-border1 management_accordion">
                                                <div class="management_form1 management_accordion" id="important_details_section">
                                             
                                                <div class="form-group row name_fields sorting  <?php if(isset($firm['crm_company_url']) && ($firm['crm_company_url']!='') ){?>light-color_company<?php } ?>">
                                                   <label class="col-sm-4 col-form-label">Company Name</label>
                                                   <div class="col-sm-8 edit-field-popup1">
                                                     
                                                      <a href="<?php if(isset($firm['crm_company_url']) && ($firm['crm_company_url']!='') ){ echo $firm['crm_company_url'];} else { echo "#"; }?>" id="company_url_anchor" target="_blank"><?php if(isset($firm['crm_company_name']) && ($firm['crm_company_name']!='') ){ echo $firm['crm_company_name'];}?></a> 

                                                      <input type="hidden" name="company_url" id="company_url" value="<?php if(isset($firm['crm_company_url']) && ($firm['crm_company_url']!='') ){ echo $firm['crm_company_url'];}?>" class="fields" placeholder="www.google.com"> 

                                                      <input type="hidden" name="company_name1" id="company_name1" placeholder="Accotax Limited" value="<?php if(isset($firm['crm_company_name']) && ($firm['crm_company_name']!='') ){ echo $firm['crm_company_name'];}?>" class="fields edit_classname" >
                                                   
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row sorting newappend-03  <?php if(isset($firm['crm_company_number']) && ($firm['crm_company_number']!='') ){?>light-color_company<?php } ?>"  >
                                                   <label class="col-sm-4 col-form-label">
                                                      Company Number
                                                   </label>
                                                
                                                   <a href="<?php if(isset($firm['crm_company_url']) && ($firm['crm_company_url']!='') ){ echo $firm['crm_company_url'];} else { echo "#"; }?>" id="company_url_anchor" target="_blank"><?php if(isset($firm['crm_company_number']) && ($firm['crm_company_number']!='') ){ echo $firm['crm_company_number'];}?></a> 
                                                   
                                                   <div class="col-sm-8 edit-field-popup1">
                                                      <input type="hidden" name="company_number" id="company_number" placeholder="Company number" value="<?php if(isset($firm['crm_company_number']) && ($firm['crm_company_number']!='') ){ echo $firm['crm_company_number'];}?>" class="fields edit_classname">
                                                   </div>
                                                  
                                                </div>

                                                <div class="form-group row date_birth name_fields sorting clerbot <?php if(isset($firm['crm_incorporation_date']) && ($firm['crm_incorporation_date']!='') ){?>light-color_company<?php } ?>">
                                                   <label class="col-sm-4 col-form-label">Incorporation Date</label>
                                                   <div class="col-sm-8 edit-field-popup1">
                                                      <!--  <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span> -->
                                                      <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                      <input class="fields edit_classname dob_picker" type="text" name="date_of_creation" id="date_of_creation" placeholder="dd-mm-yyyy" value="<?php if(isset($firm['crm_incorporation_date']) && ($firm['crm_incorporation_date']!='') ){ echo $firm['crm_incorporation_date'];}?>">
                                                    
                                                   </div>
                                                </div>

                                                <div class="form-group row name_fields sorting <?php if(isset($firm['crm_registered_in']) && ($firm['crm_registered_in']!='') ){?>light-color_company<?php } ?>">

                                                   <label class="col-sm-4 col-form-label">
                                                        Registered in
                                                   </label>
                                                   <div class="col-sm-8 edit-field-popup1">
                                                      <input type="text" name="registered_in" id="registered_in" placeholder="" value="<?php if(isset($firm['crm_registered_in']) && ($firm['crm_registered_in']!='')  ){ echo $firm['crm_registered_in'];}?>" class="fields edit_classname" >
                                                   </div>
                                                </div>

                                                <div class="form-group row name_fields sorting <?php if(isset($firm['crm_address_line_one']) && ($firm['crm_address_line_one']!='') ){?>light-color_company<?php } ?>">
                                                   <label class="col-sm-4 col-form-label">
                                                      Address Line 1
                                                   </label>
                                                   <div class="col-sm-8 edit-field-popup1">
                                                      <input type="text" name="address_line_one" id="address_line_one" placeholder="" value="<?php if(isset($firm['crm_address_line_one']) && ($firm['crm_address_line_one']!='')  ){ echo $firm['crm_address_line_one'];}?>" class="fields edit_classname">
                                                     
                                                   </div>
                                                </div>

                                                <div class="form-group row name_fields sorting <?php if(isset($firm['crm_address_line_two']) && ($firm['crm_address_line_two']!='') ){?>light-color_company<?php } ?>">
                                                   <label class="col-sm-4 col-form-label">
                                                   Address Line 2
                                                   </label>
                                                   <div class="col-sm-8 edit-field-popup1">
                                                      <input type="text" name="address_line_two" id="address_line_two" placeholder="" value="<?php if(isset($firm['crm_address_line_two']) && ($firm['crm_address_line_two']!='')  ){ echo $firm['crm_address_line_two'];}?>" class="fields edit_classname">
                                                     
                                                   </div>
                                                </div>
                                                <div class="form-group row name_fields sorting <?php if(isset($firm['crm_address_line_three']) && ($firm['crm_address_line_three']!='') ){?>light-color_company<?php } ?>" >
                                                   <label class="col-sm-4 col-form-label">
                                                      Address Line 3
                                                   </label>
                                                   <div class="col-sm-8 edit-field-popup1">
                                                      <input type="text" name="address_line_three" id="address_line_three" placeholder="" value="<?php if(isset($firm['crm_address_line_three']) && ($firm['crm_address_line_three']!='')  ){ echo $firm['crm_address_line_three'];}?>" class="fields edit_classname" >
                                                     
                                                   </div>
                                                </div>
                                                <div class="form-group row name_fields sorting <?php if(isset($firm['crm_town_city']) && ($firm['crm_town_city']!='') ){?>light-color_company<?php } ?>">
                                                   <label class="col-sm-4 col-form-label">
                                                      Town/City
                                                   </label>
                                                   <div class="col-sm-8 edit-field-popup1">
                                                      <input type="text" name="crm_town_city" id="crm_town_city" placeholder="" value="<?php if(isset($firm['crm_town_city']) && ($firm['crm_town_city']!='')  ){ echo $firm['crm_town_city'];}?>" class="fields edit_classname">
                                                   </div>
                                                </div>
                                                <div class="form-group row name_fields sorting <?php if(isset($firm['crm_post_code']) && ($firm['crm_post_code']!='') ){?>light-color_company<?php } ?>">
                                                   <label class="col-sm-4 col-form-label">
                                                      Post Code
                                                   </label>
                                                   <div class="col-sm-8 edit-field-popup1">
                                                      <input type="text" name="crm_post_code" id="crm_post_code" placeholder="" value="<?php if(isset($firm['crm_post_code']) && ($firm['crm_post_code']!='')  ){ echo $firm['crm_post_code'];}?>" class="fields edit_classname">
                                                   </div>
                                                </div>
                                                <div class="form-group row name_fields sorting  <?php if(isset($firm['crm_company_status']) && ($firm['crm_company_status']!='') ){?>light-color_company<?php } ?>">
                                                   <label class="col-sm-4 col-form-label">
                                                      Company Status
                                                   </label>
                                                   <div class="col-sm-8 edit-field-popup1">
                                                   <select name="company_status" id="company_status" placeholder="active" class="fields edit_classname">
                                                      <?php 
                                                      array_shift($Firms_Status);
                                                      $Defult_select= !empty($firm['status'])?$firm['status']:1;

                                                      foreach ($Firms_Status as $key => $value)
                                                      {  $sel = "";
                                                         if($Defult_select == $key )
                                                         {
                                                            $sel = "selected='selected'";                          
                                                         }
                                                         echo "<option value='".$key."' ".$sel.">".$value['label']."</option>";
                                                      }

                                                      ?>
                                                   </select>
                                                      

                                                
                                                   </div>
                                                </div>
                                                <div class="form-group row name_fields sorting <?php if(isset($firm['crm_company_type']) && ($firm['crm_company_type']!='') ){?>light-color_company<?php } ?>">
                                                   <label class="col-sm-4 col-form-label">
                                                      Company Type
                                                   </label>
                                                   <div class="col-sm-8 edit-field-popup1">
                                                      <input type="text" name="company_type" id="company_type" placeholder="Private Limited Company" value="<?php if(isset($firm['crm_company_type']) && ($firm['crm_company_type']!='') ){ echo $firm['crm_company_type'];}?>" class="fields edit_classname">
                                                      
                                                   </div>
                                                </div>
                                                <div class="form-group row name_fields sorting <?php if(isset($firm['crm_company_sic']) && ($firm['crm_company_sic']!='') ){?>light-color_company<?php } ?>">
                                                   <label class="col-sm-4 col-form-label">
                                                      Company S.I.C
                                                   </label>
                                                   <div class="col-sm-8 edit-field-popup1">
                                                      <input type="text" name="company_sic" id="company_sic" value="<?php if(isset($firm['crm_company_sic']) && ($firm['crm_company_sic']!='') ){ echo $firm['crm_company_sic'];}?>" class="fields edit_classname"> 
                                                      
                                                      <input type="hidden" name="sic_codes" id="sic_codes" value="">
                                                   </div>
                                                </div>
                                                <div class="form-group row date_birth sorting <?php if(isset($firm['crm_engagement_letter']) && ($firm['crm_engagement_letter']!='') ){?>light-color_company<?php } ?>">
                                                   <label class="col-sm-4 col-form-label">
                                                      Letter of Engagement Sign Date
                                                   </label>
                                                   <div class="col-sm-8">
                                                      <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                      <input class="form-control fields dob_picker" placeholder="dd-mm-yyyy" type="text" name="engagement_letter" id="engagement_letter" value="<?php if(isset($firm['crm_engagement_letter']) && ($firm['crm_engagement_letter']!='') ){ echo $firm['crm_engagement_letter'];}?>"/>
                                                   </div>
                                                </div>
                                                <div class="form-group row name_fields sorting <?php if(isset($firm['crm_business_website']) && ($firm['crm_business_website']!='') ){?>light-color_company<?php } ?>" >
                                                   <label class="col-sm-4 col-form-label">
                                                      Business Websites
                                                   </label>
                                                   <div class="col-sm-8">
                                                      <input type="text" class="fields" name="business_website" id="business_website" placeholder="Business Website" value="<?php if(isset($firm['crm_business_website']) && ($firm['crm_business_website']!='') ){ echo $firm['crm_business_website'];}?>">
                                                   </div>
                                                </div>
                                                <!-- rsptrspt-->
                                                
                                                <div class="form-group row sorting  <?php if(isset($firm['crm_company_url']) && ($firm['crm_company_url']!='') ){?>light-color_company<?php } ?>">
                                                   <label class="col-sm-4 col-form-label">
                                                     Company URL
                                                    </label>
                                                   <div class="col-sm-8">
                                                      <input type="text" name="company_url" id="company_url" value="<?php if(isset($firm['crm_company_url']) && ($firm['crm_company_url']!='') ){ echo $firm['crm_company_url'];}?>" class="fields" placeholder="www.google.com">
                                                   </div>
                                                </div>
                                               
                                             
                                                <div class="form-group row sorting <?php if(isset($firm['crm_officers_url']) && ($firm['crm_officers_url']!='') ){?>light-color_company<?php } ?>">
                                                   <label class="col-sm-4 col-form-label">
                                                      Officers URL
                                                   </label>
                                                   <div class="col-sm-8">
                                                      <input type="text" name="officers_url" id="officers_url" value="<?php if(isset($firm['crm_officers_url']) && ($firm['crm_officers_url']!='') ){ echo $firm['crm_officers_url'];}?>" class="fields" placeholder="www.google.com">
                                                     
                                                   </div>
                                                </div>
                                              
                                             
                                                <div class="form-group row name_fields sorting">
                                                   <label class="col-sm-4 col-form-label">
                                                      Accounting System In Uses
                                                   </label>
                                                   <div class="col-sm-8">
                                                      <input type="text" class="fields" name="accounting_system_inuse" id="accounting_system_inuse" value="<?php if(isset($firm['crm_accounting_system_inuse']) && ($firm['crm_accounting_system_inuse']!='') ){ echo $firm['crm_accounting_system_inuse'];}?>">
                                                   </div>
                                                </div>


                                                 <div class="form-group row name_fields sorting">
                                                   <label class="col-sm-4 col-form-label">
                                                      Client Hashtag Section
                                                   </label>
                                                   <div class="col-sm-8">
                                                      <input type="text" value="" name="client_hashtag" id="tags" class="tags" />
                                                   </div>
                                                </div>
                                                
                                       </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div> 
                                    <!-- 2nd Tab close -->

                                    <!-- 3tab start -->
                                    <div id="main_Contact" class="fullwidth-animation tab-pane fade" >
                                    <div class="space-required">
                                          <div class="main-pane-border1 main_Contact" id="main_Contact_error">
                                             <div class="alert-ss"></div>
                                          </div>
                                       </div>
                                       <div class="space-required remove_updatespace">
                                          <div class="main-pane-border1">
                                             <div class="pane-border1">
                                                <div class="management_form1 management_accordion">
                                                   <!-- <form>-->
                                                   <div class="new_bts">
                                                       
                                                         <!--  <select name="person" id="personss" class="form-control fields">
                                                            <option value="" selected="selected">--Select--</option>
                                                            <?php  if($uri_seg!=''){ ?>
                                                            <option value="opt1" <?php if(isset($firm['crm_person'])  && $firm['crm_person']=='opt1') {?> selected="selected"<?php } ?>>Select Existing Person</option>
                                                            <?php } ?>
                                                            <option value="opt2" <?php if(isset($firm['crm_person']) && $firm['crm_person']=='opt2') {?> selected="selected"<?php } ?>>Select New Person</option> -->
                                                         <!-- <option value="opt3" <?php if(isset($firm['crm_person']) && $firm['crm_person']=='opt3') {?> selected="selected"<?php } ?>>Select others</option> -->
                                                         <!-- </select> -->
                                                         <?php  if($uri_seg!=''){ ?>
                                                         <a href="javascript:;" id="" class="btn btn-primary add_existing_person personss"  data-id="opt1" >Add Existing Person</a> 
                                                         <?php } ?> 
                                                         <a href="javascript:;" id="Add_New_Contact" class="">Add New Person</a>

                                                       
                                                      <!-- 01-09-2018 -->
                                                      <!-- <div class="col-sm-8 for_contact_validation">
                                                     
                                                      </div> -->
                                                      <!-- 01-09-2018 -->
                                                   </div>
                                                   <!-- primary info -->
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!-- primary info -->
                                       <!-- <div class="contact_form companies-house-form"></div> -->
                                       <!-- 05-07-2018 -->
                                       <!--   <input type="hidden" name="append_cnt" id="append_cnt" value="">  -->
                                       <!-- 05-07-2018-->
                                       <!--                                               <input type="hidden" name="incre" id="incre" value="">
                                          -->
                                       <div id="contactss"></div>
                                       <!--                                                    <form name="contact_info" id="contact_info" method="post">
                                          -->
                                      <div class="contact_form companies-house-form">
                                      
                                    
                                       <?php 
                                          $i=1;
                                          
                                          /*echo "<pre>";
                                          print_r($contactRec);die;*/
                                          foreach ($contactRec as $contactRec_key => $contactRec_value) {
                                          
                                             // echo "<pre>";
                                             // print_r($contactRec_value);
                                          
                                                $title = $contactRec_value['title'];
                                                $first_name = $contactRec_value['first_name'];
                                                $middle_name = $contactRec_value['middle_name'];
                                                $surname = $contactRec_value['surname']; 
                                                $preferred_name = $contactRec_value['preferred_name']; 
                                                $mobile = $contactRec_value['mobile']; 
                                                $main_email = $contactRec_value['main_email']; 
                                                $nationality = $contactRec_value['nationality']; 
                                                $psc = $contactRec_value['psc']; 
                                                $shareholder = $contactRec_value['shareholder']; 
                                                $ni_number = $contactRec_value['ni_number']; 
                                                $contact_type = $contactRec_value['contact_type']; 
                                                $address_line1 = $contactRec_value['address_line1']; 
                                                $address_line2 = $contactRec_value['address_line2']; 
                                                $town_city = $contactRec_value['town_city']; 
                                                $post_code = $contactRec_value['post_code']; 
                                                $landline = $contactRec_value['landline']; 
                                                $work_email   = $contactRec_value['work_email']; 
                                                //$date_of_birth   = $contactRec_value['date_of_birth']; 
                                                $date_of_birth   = $contactRec_value['date_of_birth']; 
                                                $nature_of_control   = $contactRec_value['nature_of_control']; 
                                                $marital_status   = $contactRec_value['marital_status']; 
                                                $utr_number   = $contactRec_value['utr_number']; 
                                                $id   = $contactRec_value['id']; 
                                               // $client_id   = $contactRec_value['client_id']; 
                                                $make_primary   = $contactRec_value['make_primary']; 
                                          
                                                $contact_names = $this->Common_mdl->numToOrdinalWord($i).' Contact ';
                                                $name=ucfirst($contactRec_value['first_name']).' '.ucfirst($contactRec_value['surname']);
                                            ?>
                                       <div class="space-required new_update1">
                                          <div class="update-data01 make_a_primary  common_div_remove-<?php echo $i; ?>" id="common_div_remove-<?php echo $i; ?>">

                                             <div class="client-info-circle1 floating_set">
                                                <div class=" remove<?php echo $id;?> contactcc ">
                                                   <div class="main-contact append_contact">
                                                      <span class="h4"><?php echo $name;?></span>
                                                      <div class="dead-primary1 for_row_count-<?php echo $i; ?>">

                                                        <input type="hidden" name="make_primary_loop[]" id="make_primary_loop" value="<?php echo $i; ?>">
                                                         <?php if($make_primary=='0'){?>
                                                         <!--  <a href="javascript:void();"><span onclick="return make_a_primary(<?php echo $id.','.$client_id;?>);">Make a Primary Contact</span>
                                                            </a> -->
                                                         <div class="radio radio-inline">
                                                            <label>
                                                           <!--  <input type="radio" name="" id="" value="" onclick="return make_a_primary(<?php echo $id.','.$client_id;?>);"> -->
                                                            <input type="radio" name="make_primary" id="make_primary<?php echo $i;?>" value="<?php echo $i;?>">

                                                            <i class="helper" style="display: none;"></i>
                                                            </label>
                                                            <a href="javascript:void(0)" class="make_primary_section"  data-id="make_primary<?php echo $i;?>" ><span>Make a primary</span></a>
                                                         </div>
                                                         <?php }else{
                                                            /** 01-09-2018 **/
                                                            ?>
                                                             <div class="radio radio-inline">
                                                            <label>
                                                             <input type="radio" name="make_primary" id="make_primary<?php echo $i;?>" value="<?php echo $i;?>" checked>
                                                              <i class="helper" style="display: none;"></i>
                                                            </label>
                                                            <a href="javascript:void(0)" class="make_primary_section" data-id="make_primary<?php echo $i;?>" ><span>Primary Contact</span></a>
                                                         </div>
                                                           
                                                            <?php
                                                            } ?>
                                                      </div>
                                                      <!-- <a href="#" data-toggle="modal" data-target="#modalcontact<?php echo $id;?>"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true" ></i></a> -->
                                                   </div>
                                                   <div class="primary-info02 common-spent01 addnewclient">
                                                      <div class="primary-info03 floating_set ">
                                                         <div id="collapse" class="panel-collapse">
                                                            <div class="basic-info-client15">
                                                                
                                                                  <span class="primary-inner">
                                                                     <label>title</label>
                                                                     <!-- <input type="text" class="text-info title"  name="title[]" id="title<?php echo $id;?>" value="<?php echo $title;?>">  -->
                                                                     <select class="text-info title"  name="title" id="title<?php echo $id;?>">
                                                                        <option value="Mr" <?php if(isset($title) && $title=='Mr') {?> selected="selected"<?php } ?>>Mr</option>
                                                                        <option value="Mrs" <?php if(isset($title) && $title=='Mrs') {?> selected="selected"<?php } ?>>Mrs</option>
                                                                        <option value="Miss" <?php if(isset($title) && $title=='Miss') {?> selected="selected"<?php } ?>>Miss</option>
                                                                        <option value="Dr" <?php if(isset($title) && $title=='Dr') {?> selected="selected"<?php } ?>>Dr</option>
                                                                        <option value="Ms" <?php if(isset($title) && $title=='Ms') {?> selected="selected"<?php } ?>>Ms</option>
                                                                        <option value="Prof" <?php if(isset($title) && $title=='Prof') {?> selected="selected"<?php } ?>>Prof</option>
                                                                     </select>
                                                                  </span>

                                                                  <span class="primary-inner <?php  if(isset($first_name) && ($first_name!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>first name</label>
                                                                  <input type="text" class="text-info" name="first_name[<?php echo $i;?>]" id="first_name" value="<?php echo $first_name;?>">
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($middle_name) && ($middle_name!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>middle name</label>
                                                                  <input type="text" class="text-info" name="middle_name[]" id="middle_name<?php echo $id;?>" value="<?php echo $middle_name;?>">
                                                                  </span>
                                                                    <span class="primary-inner <?php  if(isset($last_name) && ($last_name!='') ){ ?>light-color_company<?php } ?>">
                                                                    <label>Last name</label>
                                                                    <input type="text" name="last_name[]" id="last_name" placeholder="" value="<?php if(isset($last_name) && ($last_name!='') ){ echo $last_name;}?>" class="text-info">
                                                                    </span>
                                                                  <span class="primary-inner <?php  if(isset($surname) && ($surname!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>surname</label>
                                                                  <input type="text" class="text-info" name="surname[]" id="surname<?php echo $id;?>" value="<?php echo $surname;?>">
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($preferred_name) && ($preferred_name!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>prefered name</label>
                                                                  <input type="text" class="text-info" name="preferred_name[]" id="preferred_name<?php echo $id;?>" value="<?php echo $preferred_name;?>">
                                                                  </span> 
                                                                  <span class="primary-inner <?php  if(isset($mobile) && ($mobile!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>mobile</label>
                                                                  <input type="text" class="text-info" name="mobile[<?php echo $i;?>]" id="mobile<?php echo $id;?>" value="<?php echo $mobile;?>">
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($main_email) && ($main_email!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>main E-Mail address</label>
                                                                  <input type="text" class="text-info" name="main_email[<?php echo $i;?>]" id="email_id"  value="<?php echo $main_email;?>">
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($nationality) && ($nationality!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>Nationality</label>
                                                                  <input type="text" class="text-info" name="nationality[]" id="nationality<?php echo $id;?>"  value="<?php echo $nationality;?>">
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($psc) && ($psc!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>PSC</label>
                                                                  <input type="text" class="text-info" name="psc[]" id="psc<?php echo $id;?>"  value="<?php echo $psc;?>">
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($shareholder) && ($shareholder!='') ){ ?>light-color_company<?php } ?>">
                                                                     <label>shareholder</label>
                                                                     <select name="shareholder" id="shareholder<?php echo $id;?>">
                                                                        <option value="yes" <?php if(isset($shareholder) && $shareholder=='yes') {?> selected="selected"<?php } ?>>Yes</option>
                                                                        <option value="no" <?php if(isset($shareholder) && $shareholder=='no') {?> selected="selected"<?php } ?>>No</option>
                                                                     </select>
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($ni_number) && ($ni_number!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>national insurance number</label>
                                                                  <input type="text" class="text-info" name="ni_number[]" id="ni_number<?php echo $id;?>"  value="<?php echo $ni_number;?>">
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($contactRec_value['country_of_residence']) && ($contactRec_value['country_of_residence']!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>Country Of Residence</label>
                                                                  <input type="text" name="country_of_residence[]" id="country_of_residence<?php echo $id;?>" class="text-info" value="<?php if(isset($contactRec_value['country_of_residence']) && ($contactRec_value['country_of_residence']!='') ){ echo $contactRec_value['country_of_residence'];}?>">
                                                                  </span>
                                                               
                                                               
                                                                  <span class="primary-inner contact_type <?php  if(isset($contact_type) && ($contact_type!='') ){ ?>light-color_company<?php } ?>">
                                                                     <label>contact type</label>
                                                                     <select name="contact_type" id="contact_type<?php echo $id;?>" class="othercus">
                                                                        <option value="Director" <?php if(isset($contact_type) && $contact_type=='Director') {?> selected="selected"<?php } ?>>Director</option>
                                                                        <option value="Director/Shareholder" <?php if(isset($contact_type) && $contact_type=='Director/Shareholder') {?> selected="selected"<?php } ?>>Director/Shareholder</option>
                                                                        <option value="Shareholder" <?php if(isset($contact_type) && $contact_type=='Shareholder') {?> selected="selected"<?php } ?>>Shareholder</option>
                                                                        <option value="Accountant" <?php if(isset($contact_type) && $contact_type=='Accountant') {?> selected="selected"<?php } ?>>Accountant</option>
                                                                        <option value="Bookkeeper" <?php if(isset($contact_type) && $contact_type=='Bookkeeper') {?> selected="selected"<?php } ?>>Bookkeeper</option>
                                                                        <option value="Other" <?php if(isset($contact_type) && $contact_type=='Other') {?> selected="selected"<?php } ?>>Other(Custom)</option>
                                                                     </select>
                                                                  </span>
                                                                  <span class="primary-inner spnMulti" id="others_customs" style="display:none">
                                                                  <label>Other(Custom)</label>
                                                                  <input type="text" class="text-info" name="other_custom[]" id="other_custom<?php echo $id;?>"  value="<?php if(isset($contactRec_value['other_custom']) && ($contactRec_value['other_custom']!='') ){ echo $contactRec_value['other_custom'];}?>">
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($address_line1) && ($address_line1=!'') ){ ?>light-color_company<?php } ?>">
                                                                  <label>address line1</label>
                                                                  <input type="text" class="text-info" name="address_line1[]" id="address_line1<?php echo $id;?>"  value="<?php echo $address_line1;?>">
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($address_line2) && ($address_line2!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>address line2</label>
                                                                  <input type="text" class="text-info" name="address_line2[]" id="address_line2<?php echo $id;?>"  value="<?php echo $address_line2;?>">
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($town_city) && ($town_city!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>town/city</label>
                                                                  <input type="text" class="text-info" name="town_city[]" id="town_city<?php echo $id;?>"  value="<?php echo $town_city;?>">
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($post_code) && ($post_code!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>post code</label>
                                                                  <input type="text" class="text-info" name="post_code[]" id="post_code<?php echo $id;?>"  value="<?php echo $post_code;?>">
                                                                  </span>
                                                  <span class="primary-inner landlnecls <?php  if(isset($contactRec_value['landline']) && ($contactRec_value['landline']!='') ){ ?>light-color_company<?php } ?>">
                                                  <div class="cmn-land-append1 update-primary pack_add_row_wrpr_landline">
                                                                     <label>landline</label>
                                                                     <div class="remove_one_row">
                                                                  <?php 
                                                                     $land = json_decode($contactRec_value['landline']);
                                                                     $pre_landline = json_decode($contactRec_value['pre_landline']);
                                                                     //print_r( $land);
                                                                     if(!empty($land)){
                                                                     foreach($land as $key =>$val) {
                                                                        
                                                                        $preselect = ['mobile'=>'','work'=>'','home'=>'','main'=>'','workfax'=>'','homefax'=>''];
                                                                        foreach ($preselect as $preselect_key => $preselect_val)
                                                                        {
                                                                           if( $preselect_key == $pre_landline[$key])
                                                                              {
                                                                                 $preselect[$key] = "selected='selected'";
                                                                              }
                                                                        }
                                                                     ?>
                                                                  
                                                   

                                                                     <select name="pre_landline<?php echo $i;?>[<?php echo $key;?>]" id="pre_landline<?php echo $id;?>">
                                                                        <option value="mobile" <?php echo $preselect['mobile'];?>>Mobile</option>
                                                                        <option value="work" <?php echo $preselect['mobile'];?>>Work</option>
                                                                        <option value="home" <?php echo $preselect['home'];?>>Home</option>
                                                                        <option value="main" <?php echo $preselect['main'];?>>Main</option>
                                                                        <option value="workfax" <?php echo $preselect['workfax'];?>>Work Fax</option>
                                                                        <option value="homefax" <?php echo $preselect['homefax'];?>>Home Fax</option>
                                                                     </select>
                                                                     <div class="land-spaces">   <input type="text" class="text-info" name="landline<?php echo $i;?>[<?php echo $key;?>]" id="landline<?php echo $id;?>"  value="<?php echo $val;?>"> </div>
                                                                 
                                                
                                                                  <?php } } else {?>
                                                                  
                                                                     <select name="pre_landline<?php echo $i;?>" id="pre_landline<?php echo $id;?>">
                                                                        <option value="mobile">Mobile</option>
                                                                        <option value="work">Work</option>
                                                                        <option value="home">Home</option>
                                                                        <option value="main">Main</option>
                                                                        <option value="workfax">Work Fax</option>
                                                                        <option value="homefax">Home Fax</option>
                                                                     </select>
                                                                     <div class="land-spaces">    <input type="text" class="text-info" name="landline<?php echo $i;?>[0]" id="landline<?php echo $id;?>"  value=""> </div>
                                                                  
                                                  
                                                                  <?php } ?>
                                                  
                                                                  <button type="button" class="btn btn-primary yk_pack_addrow_landline" data-id="<?php echo $i;?>">Add Landline</button>
                                                               
                                                               </div>
                                                               </div>
                                                </span>
                                                
                                                <span class="primary-inner displylinblock <?php  if(isset($contactRec_value['work_email']) && ($contactRec_value['work_email']!='') ){ ?>light-color_company <?php } ?>">
                                                                    <div class="update-primary pack_add_row_wrpr_email">
                                                                           <label>Work email</label>
                                                                        <div class="remove_one_row ">
                                                                  <?php 
                                                                     $work=json_decode($contactRec_value['work_email']);
                                                                                     if(!empty($land)){
                                                                     foreach($work as $key =>$val) {
                                                                     ?>
                                                                  
                                                                                    <input type="text" class="text-info" name="work_email<?php echo$i;?>[<?php echo $key;?>]" id="work_email<?php echo $id;?>"  value="<?php echo $val;?>">

                                                                  <?php } } else {?>
                                                                  <input type="text" class="text-info" name="work_email<?php echo$i;?>[0]" id="work_email<?php echo $id;?>"  value="">
                                                                  <?php } ?>
                                                                  <button type="button" class="btn btn-primary yk_pack_addrow_email" data-id="<?php echo $i;?>">Add Email</button>
                                                                     </div>
                                                                  </div>
                                                </span>  

                                                                  <span class="primary-inner date_birth <?php  if(isset($date_of_birth) && ($date_of_birth!='') ){ ?>light-color_company<?php } ?>">
                                                                     <label>dateof birth</label>
                                                                     <div class="picker-appoint">
                                                                        <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span> <input type="text" class="text-info date_picker_dob" name="date_of_birth[]"  placeholder="dd-mm-yyyy" value="<?php if(isset($date_of_birth) && ($date_of_birth!='') ){ echo date('d-m-Y',strtotime($date_of_birth));}?>">
                                                                     </div>
                                                                  </span>


                                                                  <span class="primary-inner <?php  if(isset($nature_of_control) && ($nature_of_control!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>nature of control</label>
                                                                  <input type="text" class="text-info" name="nature_of_control[]" id="nature_of_control<?php echo $id;?>"  value="<?php echo $nature_of_control;?>">
                                                                  </span>


                                                                  <span class="primary-inner <?php  if(isset($marital_status) && ($marital_status!='') ){ ?>light-color_company<?php } ?>">
                                                                     <label>marital status</label>
                                                                     <!-- <input type="text" class="text-info" name="marital_status" id="marital_status<?php// echo $id;?>"  value="<?php //echo $marital_status;?>"> -->
                                                                     <select name="marital_status" id="marital_status<?php echo $id;?>">
                                                                        <option value="Single" <?php if(isset($marital_status) && $marital_status=='Single') {?> selected="selected"<?php } ?>>Single</option>
                                                                        <option value="Living_together" <?php if(isset($marital_status) && $marital_status=='Living_together') {?> selected="selected"<?php } ?>>Living together</option>
                                                                        <option value="Engaged" <?php if(isset($marital_status) && $marital_status=='Engaged') {?> selected="selected"<?php } ?>>Engaged</option>
                                                                        <option value="Married" <?php if(isset($marital_status) && $marital_status=='Married') {?> selected="selected"<?php } ?>>Married</option>
                                                                        <option value="Civil_partner" <?php if(isset($marital_status) && $marital_status=='Civil_partner') {?> selected="selected"<?php } ?>>Civil partner</option>
                                                                        <option value="Separated" <?php if(isset($marital_status) && $marital_status=='Separated') {?> selected="selected"<?php } ?>>Separated</option>
                                                                        <option value="Divorced" <?php if(isset($marital_status) && $marital_status=='Divorced') {?> selected="selected"<?php } ?>>Divorced</option>
                                                                        <option value="Widowed" <?php if(isset($marital_status) && $marital_status=='Widowed') {?> selected="selected"<?php } ?>>Widowed</option>
                                                                     </select>
                                                                  </span>
                                                                  <span class="primary-inner <?php  if(isset($utr_number) && ($utr_number=!'') ){ ?>light-color_company<?php } ?>">
                                                                  <label>utr number</label>
                                                                  <input type="text" class="text-info" name="utr_number[]" id="utr_number<?php echo $id;?>"  value="<?php echo $utr_number;?>">
                                                                  </span>


                                                                  <span class="primary-inner <?php  if(isset($contactRec_value['occupation']) && ($contactRec_value['occupation']!='') ){ ?>light-color_company<?php } ?>">
                                                                  <label>Occupation</label>
                                                                  <input type="text" class="text-info" id="occupation<?php echo $id;?>"  name="occupation[]" value="<?php if(isset($contactRec_value['occupation']) && ($contactRec_value['occupation']!='') ){ echo $contactRec_value['occupation'];}?>">
                                                                  </span>

                                                                  <span class="primary-inner date_birth <?php  if(isset($contactRec_value['appointed_on']) && ($contactRec_value['appointed_on']!='') ){ ?>light-color_company<?php } ?>">
                                                                     <label>Appointed On</label>
                                                                     <div class="picker-appoint">
                                                                        <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                                        <input type="text" class="text-info" id="appointed_on<?php echo $id;?>"  name="appointed_on[]" value="<?php if(isset($contactRec_value['appointed_on']) && ($contactRec_value['appointed_on']!='') ){ echo $contactRec_value['appointed_on'];}?>">
                                                                     </div>
                                                                  </span>                                                              
                                                            
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>                                  
                                       <?php $i++;  } ?> 
                                        </div>
                                       <!-- 05-07-2018 rs -->
                                       <input type="hidden" name="incre" id="incre" value="<?php echo $i;?>">
                                       <!-- end of 05-07-2018 -->
                                       <!--                        </form>
                                          -->
                                       <div class="contact_formss"></div>
                                       <input type="hidden" name="append_cnt" id="append_cnt" value="<?php echo $i-1; ?>">
                                       <!-- change into botton 05-07-2018 -->   
                                    </div>

                                    <!-- 4tab -->


                                       <div id="amlchecks" class="tab-pane fade ">
                                    <div class="space-required">
                                          <div class="main-pane-border1" id="amlchecks_error">
                                             <div class="alert-ss"></div>
                                          </div>
                                       </div>

                                       <div class="masonry-container floating_set ">
                                          <div class="grid-sizer"></div>
                                          <div class="accordion-panel">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Anti Money Laundering Checks</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1">
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Client ID Verified</label>
                                                         <div class="col-sm-8">
                                                             <input type="checkbox" class="js-small f-right fields" name="client_id_verified" id="client_id_verified" <?php if(isset($fir['crm_assign_client_id_verified']) && ($firm['crm_assign_client_id_verified']=='on') ){?> checked="checked"<?php } ?> >
                                                         </div>
                                                      </div>
                                                      <div class="form-group row assign_cus_type client_id_verified_data" >
                                                         <label class="col-sm-4 col-form-label">Type of ID Provided</label>
                                                         <div class="col-sm-8">
                                                         <div class="dropdown-sin-11 lead-form-st">
                                                            <select name="type_of_id[]" id="person" class="form-control fields assign_cus" multiple="multiple" placeholder="Select">
                                                               <option value="" disabled="disabled">--Select--</option>
                                                               <option value="Passport" <?php echo (isset($firm['crm_assign_type_of_id']) && in_array('Passport',explode(",", $firm['crm_assign_type_of_id']))) ?'selected':''; ?>  >Passport</option>
                                                               <option value="Driving_License"
                                                                <?php echo (isset($firm['crm_assign_type_of_id']) && in_array('Driving_License',explode(",", $firm['crm_assign_type_of_id']))) ?'selected':''; ?>
                                                                >Driving License</option>
                                                               <option value="Other_Custom"
                                                                <?php echo (isset($firm['crm_assign_type_of_id']) && in_array('Other_Custom',explode(",", $firm['crm_assign_type_of_id']))) ?'selected':''; ?>
                                                                >Other Custom</option>
                                                            </select>
                                                         </div>
                                                         </div>
                                                      </div>
                                             <!-- 29-08-2018 for multiple image upload option -->
                                             <div class="form-group row" >
                                                   <label class="col-sm-4 col-form-label">Attachement</label>
                                                   <div class="col-sm-8">
                                                    
                                                      <div class="custom_upload">
                                                      <label for="proof_attach_file" class="other-file"></label>
                                                      <input type="file" name="proof_attach_file[]" id="proof_attach_file" multiple="multiple" >
                                                   </div>

                                                   </div>
                                             </div>

                                             <!-- en dof image upload for type of ids -->
                                                      <div class="form-group row name_fields spanassign for_other_custom_choose" style=<?php echo (isset($firm['crm_assign_type_of_id']) && in_array('Other_Custom',explode(",", $firm['crm_assign_type_of_id']))) ?'':'display:none'; ?>>
                                                         <label class="col-sm-4 col-form-label">Other Custom</label>
                                                         <div class="col-sm-8">
                                                            <input type="text" name="assign_other_custom" id="assign_other_custom" placeholder="" value="<?php if(isset($firm['crm_assign_other_custom']) && ($firm['crm_assign_other_custom']!='') ){ echo $firm['crm_assign_other_custom'];}?>" class="fields">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Proof of Address</label>
                                                         <div class="col-sm-8">
                                                            <input type="checkbox" class="js-small f-right fields" name="proof_of_address" id="proof_of_address" <?php if(isset($firm['crm_assign_proof_of_address']) && ($firm['crm_assign_proof_of_address']=='on') ){?> checked="checked"<?php } ?> >
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Meeting with the Client</label>
                                                         <div class="col-sm-8">
                                                             <input type="checkbox" class="js-small f-right fields" name="meeting_client" id="meeting_client" <?php if(isset($firm['crm_assign_meeting_client']) && ($firm['crm_assign_meeting_client']=='on') ){?> checked="checked"<?php } ?>>
                                                         </div>
                                                      </div>

                                                       <?php if(isset($firm['proof_attach_file'])  ){?>
                                       <div class="form-group row" >
                                             <label class="col-sm-4 col-form-label">Attachment</label>
                                             <div class="col-sm-8">
                                               <!--  <input type="file" name="proof_attach_file[]" id="proof_attach_file" multiple="multiple" > -->
                                             </div>
                                      
                                       <div class="attach-files showtm f-right">
                                       <?php 
                                       if(isset($firm['proof_attach_file'])){
                                          ?>
                                       <div class="jFiler-items jFiler-row">
                                           <ul class="jFiler-items-list jFiler-items-grid">
                                          <?php
                                          $ex_attach=array_filter(explode(',', $firm['proof_attach_file']));
                                       foreach ($ex_attach as $attach_key => $attach_value) {
                                             $replace_val=str_replace(base_url(),'',$attach_value);
                                             $ext = explode(".", $replace_val);
                                           
                                             $res=$this->Common_mdl->geturl_image_or_not($ext[1]);
                                            // echo $res;
                                             if($res=='image'){
                                                ?>
                                             <li class="jFiler-item for_img_<?php echo $attach_key; ?>" data-jfiler-index="3" style="">
                                               <input type="hidden" name="already_upload_img[]" value="<?php echo $attach_value; ?>" >
                                                 <div class="jFiler-item-container">
                                                    <div class="jFiler-item-inner">
                                                       <div class="jFiler-item-thumb">
                                                          <div class="jFiler-item-status"></div>
                                                          <div class="jFiler-item-info">                                        
                                                                                         
                                                          </div>
                                                          <div class="jFiler-item-thumb-image"><img src="<?php echo $attach_value;?>" draggable="false"></div>
                                                       
                                                    </div>
                                                   <!-- <div class="jFiler-item-assets jFiler-row">
                                                          <ul class="list-inline pull-left">
                                                             <li>
                                                                <div class="jFiler-jProgressBar" style="display: none;">
                                                                   <div class="bar"></div>
                                                                </div>
                                                               
                                                             </li>
                                                          </ul>
                                                          <ul class="list-inline pull-right">
                                                             <li><a class="icon-jfi-trash jFiler-item-trash-action remove_file" data-id="<?php echo $attach_key ?>"></a></li>
                                                          </ul>
                                                       </div>-->
                                                 </div>
                                              </li>
                                                <?php
                                                } // if image
                                                else{ ?>
                                              <li class="jFiler-item jFiler-no-thumbnail for_img_<?php echo $attach_key; ?>" data-jfiler-index="2" style="">
                                               <input type="hidden" name="already_upload_img[]" value="<?php echo $attach_value; ?>" >
                                                 <div class="jFiler-item-container">
                                                    <div class="jFiler-item-inner">
                                                       <div class="jFiler-item-thumb">
                                                          <div class="jFiler-item-status"></div>
                                                          <a href="<?php echo $attach_value; ?>" target="_blank" ><div class="jFiler-item-info">                                                               </div></a>
                                                          <div class="jFiler-item-thumb-image"><span class="jFiler-icon-file f-file f-file-ext-odt" style="background-color: rgb(63, 79, 211);"><?php echo $attach_value; ?></span></div>
                                                       </div>
                                                      
                                                    </div>
                                                 </div>
                                              </li>
                                             <?php } //else end

                                               } // foreach
                                               ?>
                                               </ul>
                                               </div>
                                               <?php
                                          }
                                       ?>
                                      </div>
                                       </div>

                                       <?php } ?>
                    
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- accordion-panel -->   
                                       </div>
                                    </div>
                                    <!-- 4tab close -->
                                    <!-- 5tab start -->
                                     <div id="other" class="firm_infoother tab-pane fade">
                                       <div class="space-required ">
                                          <div class="main-pane-border1" id="other_error">
                                             <div class="alert-ss"></div>
                                          </div>
                                       </div>
                                       
                                       <div class="accordion-panel">
                                          <div class="box-division03">
                                             <div class="accordion-heading" role="tab" id="headingOne">
                                                <h3 class="card-title accordion-title">
                                                   <a class="accordion-msg">Other</a>
                                                </h3>
                                             </div>
                                             <div id="collapse" class="panel-collapse">
                                                <div class="basic-info-client1" id="others_section">
                                                   <div class="form-group row radio_bts others_details widthhu_cls" >
                                                      <label class="col-sm-4 col-form-label">Previous Accounts</label>
                                                      <div class="col-sm-8">
                                                         <input type="checkbox" class="js-small f-right fields" name="previous_account" id="previous_account" value="on" <?php if(isset($firm['crm_previous_accountant']) && ($firm['crm_previous_accountant']=='on') ){?> checked="checked"<?php } ?> data-id="preacc">
                                                      </div>
                                                   </div>
                                                   <?php 
                                                      (isset($firm['crm_previous_accountant']) && $firm['crm_previous_accountant'] == 'on') ? $pre =  "block" : $pre = 'none';
                                                      
                                                      
                                                      ?>
                                                   <div class="form-group row name_fields preacc_content_toggle others_details nopadd_clss widthhu_cls" style="display:<?php echo $pre;?>;">
                                                      <div class="form-group row name_fields">
                                                         <label class="col-sm-4 col-form-label">Name of the Firm </label>
                                                         <div class="col-sm-8">
                                                            <input type="text" name="name_of_firm" id="name_of_firm" placeholder="" value="<?php if(isset($firm['crm_other_name_of_firm']) && ($firm['crm_other_name_of_firm']!='') ){ echo $firm['crm_other_name_of_firm'];}?>" class="fields">
                                                         </div>
                                                      </div>
                                                      <div class="form-group row ">
                                                         <label class="col-sm-4 col-form-label">Address </label>
                                                         <div class="col-sm-8">
                                                            <textarea rows="4" name="other_address" id="other_address" class="form-control fields"><?php if(isset($firm['crm_other_address']) && ($firm['crm_other_address']!='') ){ echo $firm['crm_other_address'];}?></textarea>
                                                         </div>
                                                      </div>
                                                   
                                                   <!-- preacc close-->
                                                   <div class="form-group row name_fields others_details">
                                                      <label class="col-sm-4 col-form-label">Contact Tel</label>
                                                      <div class="col-sm-8">
                                                         <!-- <input type="number" name="other_contact_no" id="other_contact_no" placeholder="" value="<?php if(isset($user[0]['crm_phone_number']) && ($user[0]['crm_phone_number']!='') ){ echo $user[0]['crm_phone_number'];}?>" class="fields"> -->
                                                         <?php 


                                                         ?>
                                                         <input type="number" class="con-code" name="cun_code" name="cun_code" value="<?php if(isset($user[0]['crm_phone_number']) && ($user[0]['crm_phone_number']!='') ){ echo substr($user[0]['crm_phone_number'], 0, 2);} ?> ">
                                                         <input type="number" name="pn_no_rec" id="pn_no_rec" class="fields" value="<?php if(isset($user[0]['crm_phone_number']) && ($user[0]['crm_phone_number']!='') ){ echo substr($user[0]['crm_phone_number'], 2);} ?>">
                                                      </div>
                                                   </div>
                                                   <div class="form-group row name_fieldsothers_details" >
                                                      <label class="col-sm-4 col-form-label">Email Address</label>
                                                      <div class="col-sm-8">
                                                         <input type="email" name="emailid" id="emailid" placeholder="" value="<?php if(isset($firm['crm_email']) && ($firm['crm_email']!='') ){ echo $firm['crm_email'];}?>" class="fields">
                                                      </div>
                                                   </div>
                                                   
                                                  
                                                   </div><!-- for previous account hide event -->
                                                   
                                        
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!-- accordion-panel -->
                                       <div class="accordion-panel">
                                          <div class="box-division03">
                                             <div class="accordion-heading" role="tab" id="headingOne">
                                                <h3 class="card-title accordion-title">
                                                   <a class="accordion-msg">Additional Information - Internal Notes</a>
                                                </h3>
                                             </div>
                                             <div id="collapse" class="panel-collapse">
                                                <div class="basic-info-client1">
                                                   <div class="form-group row ">
                                                      <label class="col-sm-4 col-form-label">Notes</label>
                                                      <div class="col-sm-8">
                                                         <textarea rows="4" name="other_internal_notes" id="other_internal_notes" class="form-control fields"><?php if(isset($firm['crm_other_internal_notes']) && ($firm['crm_other_internal_notes']!='') ){ echo $firm['crm_other_internal_notes'];}?></textarea>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!-- accordion-panel -->
                                         <!-- accordion-panel for client login-->
                                       <div class="accordion-panel span_clsgreen">
                                          <div class="box-division03">
                                             <div class="accordion-heading" role="tab" id="headingOne">
                                                <h3 class="card-title accordion-title">
                                                   <a class="accordion-msg" style="display: block;">Client Login
                                                   <input type="checkbox" class="js-small f-right fields" name="show_login" id="show_login" 
                                                      data-id="show_login">
                                                   </a>
                                                </h3>
                                             </div>
                                             <div id="collapse" class="panel-collapse show_login" style="display: none">
                                                <div class="basic-info-client1" id="other_details1">
                                                      <!-- 30-08-2018 -->
                                                   <div class="form-group row name_fields others_details" id="<?php echo $this->Common_mdl->get_order_details(88); ?>">
                                                      <label class="col-sm-4 col-form-label">Username</label>
                                                      <div class="col-sm-8">
                                                         <input type="text" name="user_name" id="user_name" placeholder="" value="<?php if(isset($user[0]['username']) && ($user[0]['username']!='') ){ echo $user[0]['username'];}?>" data-id="<?php echo $user[0]['id'] ?>" class="fields user_name">
                                                          <span class="v_err" style="color:red;display:none">User name already Exists</span>
                                                      </div>
                                                   </div>
                                                   <div class="form-group row name_fields others_details" id="<?php echo $this->Common_mdl->get_order_details(89); ?>">
                                                      <label class="col-sm-4 col-form-label">Password</label>
                                                      <div class="col-sm-8">
                                                         <input type="password" name="password" id="password" placeholder="" value="<?php if(isset($user[0]['confirm_password']) && ($user[0]['confirm_password']!='') ){ echo $user[0]['confirm_password'];}?>" class="fields">
                                                      </div>
                                                   </div>
                                                   <!-- end of 30-08-2018 -->
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!-- accordion-panel -->
                                      
                                       <!-- accordion-panel -->
                                       <div class="accordion-panel" style="display: none;">
                                          <div class="box-division03">
                                             <div class="accordion-heading" role="tab" id="headingOne">
                                                <h3 class="card-title accordion-title">
                                                   <a class="accordion-msg">Notes if any other</a>
                                                </h3>
                                             </div>
                                             <div id="collapse" class="panel-collapse">
                                                <div class="basic-info-client1">
                                                   <div class="form-group row ">
                                                      <label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_order_details(90); ?></label>
                                                      <div class="col-sm-8">
                                                         <textarea rows="4" name="other_any_notes" id="other_any_notes" class="form-control fields"><?php if(isset($firm['crm_other_any_notes']) && ($firm['crm_other_any_notes']!='') ){ echo $firm['crm_other_any_notes'];}?></textarea>
                                                      </div>
                                                   </div>
                                                   <?php
                                                     // echo render_custom_fields_one( 'other',$rel_id);  ?>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <!-- 5tab close -->
                                    <!-- 6tab start -->
                                    <div id="business-det" class="tab-pane fade ">
                                       <div class="masonry-container floating_set ">
                                          <div class="grid-sizer"></div>
                                          <!-- accordion-panel -->
                                          <div class="accordion-panel">
                                             <div class="box-division03">
                                                <div class="accordion-heading" role="tab" id="headingOne">
                                                   <h3 class="card-title accordion-title">
                                                      <a class="accordion-msg">Business Details</a>
                                                   </h3>
                                                </div>
                                                <div id="collapse" class="panel-collapse">
                                                   <div class="basic-info-client1">
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Trading As</label>
                                                         <div class="col-sm-8">
                                                            <input type="text" class="fields" name="bus_tradingas" id="bus_tradingas" >
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts date_birth">
                                                         <label class="col-sm-4 col-form-label">Commenced Trading</label>
                                                         <div class="col-sm-8">
                                                            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                            <input class="form-control fields dob_picker" placeholder="dd-mm-yyyy" type="text" name="bus_commencedtrading" id="bus_commencedtrading" />
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts date_birth">
                                                         <label class="col-sm-4 col-form-label">Registered for SA</label>
                                                         <div class="col-sm-8">
                                                            <span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
                                                            <input class="form-control fields dob_picker" placeholder="dd-mm-yyyy" type="text" name="bus_regist" id="bus_regist" />
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Turn Over</label>
                                                         <div class="col-sm-8">
                                                            <input type="text" class="fields" name="bus_turnover" id="bus_turnover" >
                                                         </div>
                                                      </div>
                                                      <div class="form-group row radio_bts ">
                                                         <label class="col-sm-4 col-form-label">Nuture Of Business</label>
                                                         <div class="col-sm-8">
                                                            <input type="text" class="fields" name="bus_nutureofbus" id="bus_nutureofbus" >
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- accordion-panel -->   
                                       </div>
                                    </div>
                                    <!-- 6 tab close -->
                                    
                                    
                                 </div>
                                 <!-- tab-content -->
                              </div>
                       </div>
                           </div>
                           <!-- managementclose -->
                        </form>
                        <!-- admin close-->
                     </div>
                  </div>
               </div>
            </div>
            <!-- Page body end -->
         </div>
      </div>
  
   </div>
</div>
</div>
</div>
</div>
</div>

<!-- modal 1-->

<!-- modal-close -->
<!-- modal 2-->

<div class="modal fade" id="myAlert" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Alert</h4>
         </div>
         <div class="modal-body">
            <p>  
               Do you want to Exit? 
            </p>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default no" data-dismiss="modal">No</button> 
            <button type="button" class="btn btn-default exit">Yes</button>        
         </div>
      </div>
   </div>
</div><!-- add reminder -->
<div id="edit_confirmation1" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" style="text-align-last: center">Confirmation</h4>
         </div>
         <div class="modal-body">
            <p>Are you want edit this information</p>
         </div>
         <div class="modal-footer profileEdit">
            <input type="hidden" name="hidden">
            <a href="javascript:void();" id="acompany_name" data-dismiss="modal" class="delcontact">Yes</a>
            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
         </div>
      </div>
   </div>
</div>
<!-- ajax loader -->
<!-- ajax loader end-->

<?php $this->load->view('super_admin/superAdmin_footer');?>

<script type="text/javascript" src="https://remindoo.uk/assets/js/add_client/checkbox_toggleAction.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script>
<!-- <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-ui/js/jquery-ui.min.js"></script> -->
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/masonry/3.3.1/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/popper.js/js/popper.min.js"></script>
<!-- j-pro js -->
<!-- <script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.ui.min.js"></script> -->
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.j-pro.js"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<!-- modernizr js -->
<script type="text/javascript" src="<?php echo base_url();?>bower_components/modernizr/js/css-scrollbars.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/advance-elements/custom-picker.js"></script>
<script src="<?php echo base_url();?>assets/js/pcoded.min.js"></script>
<script src="<?php echo base_url();?>assets/js/demo-12.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/common_script.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/client_page.js"></script>
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
<script type="text/javascript">
         /** 29-08-2018 **/
      
         
         


        



      $('.assign_cus_type').find('.dropdown-sin-11').on('click',function(){

      $(this).find('.dropdown-main > ul > li').click(function(){
           var custom=$(this).attr('data-value');
           var customclass=$(this).attr('class');
          if(custom=='Other_Custom')
         {   
            //if(customclass=='dropdown-option dropdown-chose'){
            if (customclass.indexOf('dropdown-chose') > -1){
                   $('.for_other_custom_choose').css('display','none');
            }
            else
            {
               $('.for_other_custom_choose').css('display','');
            }          
         }
         else{     
          }
      
        
         });
         $(this).find('.dropdown-clear-all').on('click',function(){
            $('.for_other_custom_choose').css('display','none');
         });
      });
$('.tags').tagsinput({
        allowDuplicates: true
      });

</script>


<script>
   /** rs **/
   $(document).on('change','.datepicker',function()
   {
     var date_val=$(this).val();
     var custom=$(this).attr('data-filed-custom');
       var fi_data_to = $(this).attr("data-fieldto");
              var fi_data_id  = $(this).attr("data-fieldid");
     if(typeof custom != "undefined"){
   
             $("[name^=custom_fields]").each(function() {
           var data_to = $(this).attr("data-fieldto");
              var data_id  = $(this).attr("data-fieldid");
   if((fi_data_id==data_id) && (fi_data_to==data_to) )
   {
     $(this).attr('value',date_val);
   }
           // Do stuff
         });
     }
     else
     {
       $("input[name="+$(this).attr('name')+"]").each(function() {
                              $(this).attr('value',date_val);
                             });
     }
     //$(this).attr('value','sdasd');
   
   });

   $(document).on('change','.dob_picker',function()
   {
     var date_val=$(this).val();
     var custom=$(this).attr('data-filed-custom');
       var fi_data_to = $(this).attr("data-fieldto");
              var fi_data_id  = $(this).attr("data-fieldid");
     if(typeof custom != "undefined"){
   
             $("[name^=custom_fields]").each(function() {
           var data_to = $(this).attr("data-fieldto");
              var data_id  = $(this).attr("data-fieldid");
   if((fi_data_id==data_id) && (fi_data_to==data_to) )
   {
     $(this).attr('value',date_val);
   }
           // Do stuff
         });
     }
     else
     {
       $("input[name="+$(this).attr('name')+"]").each(function() {
                              $(this).attr('value',date_val);
                             });
     }
     //$(this).attr('value','sdasd');
   
   });
   
   /*** end of rs **/
   
     
      $( document ).ready(function() {

       $(".dob_picker").datepicker({ dateFormat: 'dd-mm-yy', changeMonth: true,
           changeYear: true,  }).val();   
       });
   
      $('#auth_code').keyup(function () {
            $('.confirmation_auth_code').val($(this).val());
            $('.accounts_auth_code').val($(this).val());
        });
      
         $( document ).ready(function() { 
              var today = new Date();
                   $('#terms_signed').datepicker({
                       dateFormat: 'dd-mm-yy',
                       autoclose:true,
                       endDate: "today",              
                       changeMonth: true,
                       changeYear: true,
                   }).on('changeDate', function (ev) {
                           $(this).datepicker('hide');
                   });         
         
             $('#terms_signed').keyup(function () {
                 if (this.value.match(/[^0-9]/g)) {
                     this.value = this.value.replace(/[^0-9^-]/g, '');
                 }
             });  
      
             var today = new Date();
             $('.dob_picker').datepicker({
                 dateFormat: 'dd-mm-yy',
                 autoclose:true,
                 endDate: "today",
               //  minDate:0, 
                 changeMonth: true,
           changeYear: true,
             }).on('changeDate', function (ev) {
                     $(this).datepicker('hide');
                 });
         
         
             $('.dob_picker').keyup(function () {
                 if (this.value.match(/[^0-9]/g)) {
                     this.value = this.value.replace(/[^0-9^-]/g, '');
                 }
             });
         
         
             var date = $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy',
               //minDate:0,
                changeMonth: true,
           changeYear: true, }).val();
         
             
             $('#accordion_close').on('click', function(){
                     $('#accordion').slideToggle(300);
                     $(this).toggleClass('accordion_down');
             });
         // Other custom label
      
       $(document).on('change','.othercus',function(e){ 
         var custom = $(':selected',this).val();
         var client_id = $('#user_id').val();        
            if(custom=='Other')
            {
                $(this).closest('.contact_type').next('.spnMulti').show();
            } else {
                $('.contact_type').next('span').hide();
            } 
         }); 
      //Assign Other custom label
      $(document).on('change','.assign_cus',function(e){      
         var custom = $(':selected',this).val();
         var client_id = $('#user_id').val();                 
         if(custom=='Other_Custom')
         {      
                $(this).closest('.assign_cus_type').next('.spanassign').show();
          } else {
         $(this).closest('.assign_cus_type').next('.spanassign').hide();
          }
      
         }); 

      
         });

   /** end of 29-08-2018 **/
</script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript">



      var validateEmail = function ($email,ele = null) {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return this.optional(element) || emailReg.test( $email );
};


var phonenumber = function(inputtxt,ele=null)
{
   var Regex = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;

  if(this.optional(ele) || Regex.test(inputtxt) )
        {
            return true;
        }else
        {    
           return false;
        }
};

   
   //console.log(switcheryElm.length+"le"+switcheryElm['services_checkAll']);
    //  .datepicker({ dateFormat: 'dd-mm-yy' }) 
   
    $("form#insert_form").on('submit',function(){

         $(document).find('input[name^="work_email"] , input[name^="main_email"]').each(function(){
            $(this).rules("add", 
                  {
                      email:true,
                      messages: {
                          email:"Enter Valid Email Address."
                      }
                  });
         });

         $(document).find('input[name^="landline"] ,input[name^="mobile_number"] ').each(function(){
            $(this).rules("add", 
                  {
                      contact_no:true,
                                         
                  });
         });   

         $(document).find('input[name^="first_name"] ').each(function(){
            $(this).rules("add", 
                  {
                      required:true,
                      messages: {
                          required: "First Name is required.",
                      }
                  });
         }); 

    });

    var check_firm_type_for_validation = function(element)
    {
      var selectVal=$("#legal_form").val();
      if((selectVal == "Private Limited company") || (selectVal == "Public Limited company") || (selectVal == "Limited Liability Partnership")) {
         return true;
       }
       else
       {
         return false;
       }
   };
   var check_previous_tab_open = function(element){
                               var clickCheckbox = document.querySelector('#previous_account');
                               if(clickCheckbox.checked) // true
                               {
                                 return true;
                               }
                               else
                               {
                                 return false;
                               }
                            };
      var check_username = function(value,element){

         var result = $.ajax({
              type: "POST",
              url: '<?php echo base_url();?>user/check_username/',
              data : {'val':value,'id':$('#user_id').val()},
              async: false
          }).responseText;

         return (result==1)?true:false;
      };                            

   jQuery.validator.addMethod("contact_no",phonenumber, "Enter Valid Contact Number.");
   jQuery.validator.addMethod("check_username",check_username, "User Name already Exit.");

   $("#insert_form").validate({     
            ignore: false,     
            errorPlacement: function(error, element) {
               if ( element.attr("name") == "append_cnt")
                    error.insertAfter("#Add_New_Contact");
              else
                  error.insertAfter(element);
            },
            success: function(label,element) {          
                              label.removeClass('required-errors'); 
            },
              rules: {
                           company_name: {required: true},     
                           firm_mailid: {required: true,email:true},
                           firm_contact_no: {required: true,contact_no:true},                    
                           company_name1:{ required : check_firm_type_for_validation}, 
                           company_number:{required :check_firm_type_for_validation},
                           company_status:{required: check_firm_type_for_validation},
                           emailid: {required : check_previous_tab_open,
                              email : check_previous_tab_open},                        
                           append_cnt:{min:1 },
                           make_primary:{required:true},
                           user_name: {required: true,check_username:true},
                           password:{required: true,minlength : 5},
                           confirm_password : { required: true,minlength : 5,equalTo : "#password"},

                           },
                           errorElement: "span" , 
                           errorClass: "field-error",                             
                            messages: {
                             make_primary:"Select any one Contact as Primary",
                             append_cnt:"Please Add Any Contact Person",     
                             company_name: "Give name",
                             firm_mailid : {required:"Enter Firm Mail Id"},
                             firm_contact_no: {required:"Enter Firm Contact Number"},
                             company_name1: "Give company name",
                             company_number: "Give company number",
                             company_status: "Give company status",
                             user_name:{required:"Enter User Name"},
                             password: {required: "Please enter your password "},
                             confirm_password :{required: "Please enter your Confirm password "},                         
                             emailid:{
                             required:"Please enter a email address",
                             email:"Please enter a valid email address" },
                             profile_image: {required: 'Required!', accept: 'Not an image!'},                      
                            },

   
                           
                           submitHandler: function(form) {

                            
                                 
                                 
                            var formData = new FormData($("#insert_form")[0]);
                            var user_id = $('#user_id').val();
                               $(".LoadingImage").show();                            
                               $.ajax({
                                    url: '<?php echo base_url();?>Firm/updates_client/'+user_id,
                                   dataType : 'json',
                                   type : 'POST',
                                   data : formData,
                                   contentType : false,
                                   processData : false,
                                   success: function(data) {
                                   
                                        if(data == '0')
                                        {                                       
                                           $('.alert-danger').show();
                                           $('.alert-success').hide();
                                          // for_add_reminderintotask(data);
                                       }
                                       else
                                       {
   
                                       contact_add(data);
                                    
                                       }                                      
                                  
                                   },
                                   error: function() {
                                      
                                     }
                               });
                                                             return false;
   
                           } ,
                           
                           invalidHandler: function(e, validator) {
                                 if(validator.errorList.length)
                                 {
                                      console.log( jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id'));
                                     $('#tabs a[href="#' + jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
                                 }
                                 if(jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id')=='other'){

                                      $(".other_tabs").find('.nav-link').trigger('click');
                                   }

                           }

                       });
   
   

/*function for_add_reminderintotask(id)
{
var data={};
data['user_id']=id;
$.ajax({
     url: '<?php echo base_url();?>client/insert_remindertask/',    
     type : 'POST',
     data :data ,  
     success: function(data) {
     },
     error: function() {        
       }
 });
}*/   
   
   function contact_add(id){
 $(".LoadingImage").show();   
  var incre=$('#incre').val();   
   if(incre==''){
            incre = 1;
           }else{
            incre = parseInt(incre)+1;
           }
   
        $("#incre").val(incre);  
      var data={};
      var title=[];
   $("select[name=title]").each(function(){
    title.push($(this).val());
   }); 
      //data['title'] = arr('title');
      data['title'] = title;
      data['first_name'] = arr('first_name');
      data['middle_name'] = arr('middle_name');
      data['last_name'] = arr('last_name');
      data['surname'] = arr('surname');
      data['preferred_name'] = arr('preferred_name');
      data['mobile'] = arr('mobile_number');
      data['main_email'] = arr('main_email');
      //data['work_email'] = arr('work_email');
      for (var i = 1; i <=incre; i++) {
      data['landline'+i] = arr('landline'+i);
      data['work_email'+i] = arr('work_email'+i);
   }
      data['nationality'] = arr('nationality');
      data['psc'] = arr('psc');
      //data['shareholder'] = $("input[name='shareholder[]']:checked"). val();
    var pre_landline=[];   
   for (var i = 1; i <=incre; i++) {
       
   $("select[name=pre_landline"+i+"]").each(function(){
    pre_landline.push($(this).val());
   });
   data['pre_landline'+i] = pre_landline;
   }
   
   
       var usertype=[];
   $("select[name=shareholder]").each(function(){
    usertype.push($(this).val());
   }); 
   
    var marital=[];
   $("select[name=marital_status]").each(function(){
    marital.push($(this).val());
   }); 
   
   var contacttype=[];
   $("select[name=contact_type]").each(function(){
    contacttype.push($(this).val());
   }); 
   
      data['ni_number'] = arr('ni_number');
      //data['content_type'] = arr('content_type');
      data['address_line1'] = arr('address_line1');
      data['address_line2'] = arr('address_line2');
      data['town_city'] = arr('town_city');
      data['post_code'] = arr('post_code');
      //data['landline'] = arr('landline');
      data['date_of_birth'] = arr('date_of_birth');
      data['nature_of_control'] = arr('nature_of_control');
      //data['marital_status'] = arr('marital_status');
      data['utr_number'] = arr('utr_number');
      data['occupation'] = arr('occupation');
      data['appointed_on'] = arr('appointed_on');
      data['country_of_residence'] = arr('country_of_residence');
      data['other_custom'] = arr('other_custom');
      data['user_id'] = id;
      data['marital_status'] = marital;
      data['contact_type'] = contacttype;
      data['shareholder'] = usertype;
      var selValue = $('input[name=make_primary]:checked').val(); 
      //data['make_primary'] = arr('make_primary');
   // alert(selValue);
      data['make_primary'] = selValue;
      data['make_primary_loop'] = arr('make_primary_loop');
      data['event'] = 'add';
   
   var cnt = $("#append_cnt").val();
   
            if(cnt==''){
            cnt = 1;
           }else{
            cnt = parseInt(cnt)+1;
           }
        $("#append_cnt").val(cnt);
   
   
   
      data['cnt'] =cnt;
       $.ajax({
                                   url: '<?php echo base_url();?>firm/update_firm_contacts/',
                                  
                                   type : 'POST',
                                   data : data,
                                   success: function(data) {
                                    window.location.href = "<?php echo base_url()?>firm";
                                    // add_assign to(id); 
                                  // $(".LoadingImage").hide();
   
                                   },
                               });
   //});
   }

   function arr(name){
   var values = $("input[name='"+name+"[]']")
                 .map(function(){return $(this).val();}).get();
                 return values;
   }
   
   /*
   function add_assignto(clientId)
   {
   var data = {};
                               $(".LoadingImage").show();
   
   
          var team=[];
   $("select[name=team]").each(function(){
    team.push($(this).val());
   }); 
   data['team'] = team;
   
    data['allocation_holder'] = arr('allocation_holder');
    data['clientId'] = clientId;
    $.ajax({
      url: '<?php echo base_url();?>client/add_assignto/',
    type: "POST",
    data: data,
    success: function(data)  
    {
         add_responsibleuser(clientId);
    }
   });
   }
   
   
   function add_responsibleuser(clientId)
   {
                                 $(".LoadingImage").show();
   
   var data = {};
   // data['assign_managed'] = arr('assign_managed');
   //  data['manager_reviewer'] = arr('manager_reviewer');
   
        var assign_managed=[];
   $("select[name=assign_managed]").each(function(){
    assign_managed.push($(this).val());
   }); 
   
    var manager_reviewer=[];
   $("select[name=manager_reviewer]").each(function(){
    manager_reviewer.push($(this).val());
   }); 
   
   data['assign_managed'] = assign_managed;
    data['manager_reviewer'] = manager_reviewer;
   
    data['clientId'] = clientId;
    $.ajax({
      url: '<?php echo base_url();?>client/add_responsibleuser/',
    type: "POST",
    data: data,
    success: function(data)  
    {
      add_assigntodepart(clientId);
       //location.reload();  
      // window.location.href="<?php echo base_url().'user'?>"
    }
   });
   }
   
   function add_assigntodepart(clientId)
   {
   var data = {};
   
                               $(".LoadingImage").show();
   
    var depart=[];
   $("select[name=depart]").each(function(){
    depart.push($(this).val());
   }); 
   data['depart'] = depart;
    data['allocation_holder'] = arr('allocation_holder_dept');
    data['clientId'] = clientId;
    $.ajax({
      url: '<?php echo base_url();?>client/add_assigntodepart/',
    type: "POST",
    data: data,
    success: function(data)  
    {
         add_responsiblemember(clientId);
    }
   });
   }
   
   function add_responsiblemember(clientId)
   {
   var data = {};
   //  data['assign_managed'] = arr('assign_managed');
   //  data['manager_reviewer'] = arr('manager_reviewer');
   
   //      var assign_managed_member=[];
   // $("select[name=assign_managed_member]").each(function(){
   //  assign_managed_member.push($(this).val());
   // });                                $(".LoadingImage").show();
   
   
    var manager_reviewer_member=[];
   $("select[name=manager_reviewer_member]").each(function(){
    manager_reviewer_member.push($(this).val());
   }); 
   
   //data['assign_managed_member'] = assign_managed_member;
   data['assign_managed_member'] = arr('assign_managed_member');
    data['manager_reviewer_member'] = manager_reviewer_member;
   
    data['clientId'] = clientId;
    $.ajax({
      url: '<?php echo base_url();?>client/add_responsiblemember/',
    type: "POST",
    data: data,
    success: function(data)  
    {
      for_add_reminderintotask(clientId);
   //alert(save_exit);
   // if(save_exit=='true')
   // {
   // window.location.href="<?php echo base_url().'user'?>";
   // }else{
   
      if($("#user_ids").val()==''){
      $('.alert-success').show();
      <?php 
      if(isset($_SESSION['client_added'])){ ?>
      setTimeout(function(){ window.history.go(-1); }, 2000);
      <?php } ?>
      
   }else{
   
      $('.alert-success-userid').show();
       <?php 
      if(isset($_SESSION['client_section'])){ ?>
       setTimeout(function(){ window.history.go(-3); }, 2000);
       <?php } ?>
   }
      $('.alert-danger').hide();
      $(".LoadingImage").hide();
   var u_d = <?php if($uri_seg!=''){ echo $uri_seg; }else{ echo '0'; }?>;
   if(u_d!='0')
   {
   $.ajax({
      url: '<?php echo base_url();?>client/update_client_statuss/',
    type: "POST",
    data: {status : '1',id:u_d},
    success: function(data)  
    {
    } 
   });
   }
       //location.reload();  
      // window.location.href="<?php echo base_url().'user'?>";
   // }
   }
   });
   }*/
</script>

<script type="text/javascript" src="<?php echo base_url()?>assets/js/add_client/checkbox_toggleAction.js"></script>
<script>
   $(document).ready(function(){
       $('.step-tabs').on('click', function(){
           $('html,body').animate({scrollTop: $(this).offset().top}, 800);
       }); 
   });
   
   
   $('.btnNext').click(function(){   
   
             $('.nav-tabs > .bbs').nextAll('li').not(".hide").first().find('a').trigger('click');
   
       }
   );
   
   
     $('.btnPrevious').click(function(){
     $('.nav-tabs > .bbs').prevAll('li').not(".hide").first().find('a').trigger('click');
   });
</script>
<script>
   $(document).ready(function() {
   $(".nav-tabs a").click(function(event) {
       event.preventDefault();
       $(this).parent().addClass("bbs");
       $(this).parent().siblings().removeClass("bbs");
   });
   
    $(".th").click(function(event) {
   
        $("#ss").addClass("bbs");
   });
   });
</script>
<script type="text/javascript">   
   
   /**************************************************
   PACK ADD ROW 
   ***************************************************/  

   
   /**************************************************
   PACK REMOVE ROW 
   ***************************************************/  
   
   $(document).on('click','.yk_pack_delrow',function(e)
   {
   //$(this).parents('tr').remove();
       $(this).parents('tr').remove();
        console.log('team');
   var team_select = getSelectVal("select[name='team']");
     
responsple_interlink("team",team_select.join());    
   });
   
   /**************************************************
   depart ADD ROW 
   ***************************************************/  
 
   
   /**************************************************
   depart REMOVE ROW 
   ***************************************************/  
   
   $(document).on('click','.yk_pack_delrow_depart',function(e)
   {
      /*alert($(this).closest('tbody tr:nth-child(1)').find("td:nth-child(1) select").length);
      $(this).closest('tbody tr:first-child').find("td:first-child select").trigger('change'); */        
      $(this).parents('tr').remove();
       console.log('depart');
      var dept_select = getSelectVal("select[name='depart']");   
      responsple_interlink("dept",dept_select.join());  
   });
   
   
   /**************************************************
   PACK ADD ROW 
   ***************************************************/  
   
   
   
  
   
   $(document).on('click','.yk_pack_delrow_user',function(e)
   {
      $(this).parents('tr').remove();
      console.log('manager_reviewer');
      var staff_select = getSelectVal("select[name='manager_reviewer']");  
      responsple_interlink("staff",staff_select.join()); 

   
   });
   
   /**************************************************
   member ADD ROW 
   ***************************************************/  
   
   
   
   /**************************************************
   member REMOVE ROW 
   ***************************************************/  
   
   $(document).on('click','.yk_pack_delrow_member',function(e)
   {
   $(this).parents('tr').remove();
   
   });
   
   /**************************************************
   landline ADD ROW 
   ***************************************************/  
   
   //$('.yk_pack_addrow_landline').click(function(e)
  /* $(document).on('click','.yk_pack_addrow_landline',function(e)
   {
      alert('addrow_landline');
   
   e.preventDefault();
   var id=$(this).data('id');
    $(this).parents('.pack_add_row_wrpr_landline').before('<span class="primary-inner success ykpackrow_landline add-delete-work "><label>landline</label><div class="data_adds"><select name="pre_landline'+id+'" id="pre_landline"><option value="mobile">Mobile</option><option value="work">Work</option><option value="home">Home</option><option value="main">Main</option><option value="workfax">Work Fax</option><option value="homefax">Home Fax</option></select><input type="number" class="text-info" name="landline'+id+'[]" id="landline" value=""></div><div class="text-right danger-make1"><a href="javascript:;" class="btn btn-danger yk_pack_delrow_landline"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true"></i></a></div></span>');
   
   
   });*/
    $(document).on('click','.yk_pack_addrow_landline',function(e)
   {
 //  alert('ok');
  // $(".yk_pack_addrow_landline").remove();

  $(this).addClass('yyyyyy');
   
   e.preventDefault();
   var id=$(this).data('id');
   var i=1;
   var parent = $(this).parents('.pack_add_row_wrpr_landline');

   var len = parent.find('.yk_pack_addrow_landline').length;
    parent.append('<span class="primary-inner success ykpackrow_landline add-delete-work width_check" id="'+len+'"><div class="data_adds"><select name="pre_landline'+id+'" id="pre_landline"><option value="mobile">Mobile</option><option value="work">Work</option><option value="home">Home</option><option value="main">Main</option><option value="workfax">Work Fax</option><option value="homefax">Home Fax</option></select><input type="text" class="text-info" name="landline'+id+'['+len+']" value=""></div><div class="text-right danger-make1"><a href="javascript:;" class="btn btn-danger yk_pack_delrow_landline" id="'+len+'"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true"></i></a></div>   <button type="button" class="btn btn-primary yk_pack_addrow_landline" data-id="'+id+'">Add Landline</button></span>');
   i++;
  // alert($('.yk_pack_addrow_landline').length);
    if(parent.find('.yk_pack_addrow_landline').length > 2){
     // alert('ok');
      $(this).parents('.primary-inner').addClass('check_width');

    }

   
   });
   
   /**************************************************
   landline REMOVE ROW 
   ***************************************************/  
   
     $(document).on('click','.yk_pack_delrow_landline',function(e)
   {
    
      var parent = $(this).parents('.pack_add_row_wrpr_landline');
      $(this).parents('.ykpackrow_landline').remove();

      //  alert($('.yk_pack_addrow_landline').length);
       if( parent.find('.yk_pack_addrow_landline').length == 1)
       {
       //  alert('true');
            parent.find('.yk_pack_addrow_landline').removeClass('yyyyyy');
       }
       else{
           //  alert('false');
                 parent.find('span:last-child .yk_pack_addrow_landline').removeClass('yyyyyy');
          }    //   $(this).parents('.remove_one_row').last('.width_check').addClass('class1');
          
      // alert($('.yk_pack_addrow_landline').length);

     //    $(this).parents('.remove_one_row').last('.ykpackrow_landline').find('.yk_pack_addrow_landline').removeClass('yyyyyy');
   });
   
   /**************************************************
   email ADD ROW 
   ***************************************************/  
   
   //$('.yk_pack_addrow_email').click(function(e)
   /*$(document).on('click','.yk_pack_addrow_email',function(e)
   {
   e.preventDefault();
   var id=$(this).data('id');
    $(this).parents('.pack_add_row_wrpr_email').before('<span class="primary-inner success ykpackrow_email add-delete-work"><label>Work email</label><input type="email" class="text-info" name="work_email'+id+'[]" id="work_email"  value=""><div class="text-right danger-make1"><a href="javascript:;" class="btn btn-danger yk_pack_delrow_email"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true"></i></a></div></span>');
   
   
   });*/
     $(document).on('click','.yk_pack_addrow_email',function(e)
   {
     //alert('op');
       $(this).addClass('yyyyyy');
      e.preventDefault();
      var id=$(this).data('id');
      var parent = $(this).parents('.pack_add_row_wrpr_email');
      
      var len = parent.find('input[name^="work_email"]').length;
     
      parent.append('<span class="primary-inner success ykpackrow_email add-delete-work"><input type="email" class="text-info" name="work_email'+id+'['+len+']" value=""><div class="text-right danger-make1"><a href="javascript:;" class="btn btn-danger yk_pack_delrow_email"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true"></i></a></div> <button type="button" class="btn btn-primary yk_pack_addrow_email" data-id="'+id+'">Add Email</button></span>');
       // alert($('.yk_pack_addrow_email').length);
     if(parent.find('.yk_pack_addrow_email').length > 2)
     {
         //   alert('ok');
         $(this).parents('.primary-inner').addClass('check_width');

      }



   });
   
   /**************************************************
   email REMOVE ROW 
   ***************************************************/  
   
     
   $(document).on('click','.yk_pack_delrow_email',function(e)
   {
         var parent = $(this).closest('.pack_add_row_wrpr_email');
         $(this).parents('.ykpackrow_email').remove();

       if(parent.find('.yk_pack_addrow_email').length == 1)
       {
            parent.find('.yk_pack_addrow_email').removeClass('yyyyyy');
       }else{
           //  alert('false');
                 parent.find('span:last-child .yk_pack_addrow_email').removeClass('yyyyyy');
          }    
   });  
   
   
   
</script>
<script type="text/javascript">

</script>
<script type="text/javascript">
   $(document).ready(function(){
        var start = new Date();
    start.setFullYear(start.getFullYear() - 70);
    var end = new Date();
    end.setFullYear(end.getFullYear());
     $(".date_picker_dob").datepicker({ dateFormat: 'dd-mm-yy',changeMonth: true,
        changeYear: true,yearRange: start.getFullYear() + ':' + end.getFullYear() }).val();
   $("#select_responsible_type").change(function(){


    // alert('ok');
   var rec = $(this).val();
   if(rec=='staff')
   {
      $('.responsible_user_tbl').show();
      $('.responsible_team_tbl').show();
      $('.responsible_department_tbl').show();
    //  $('.responsible_member_tbl').show();
   
   }else if(rec=='team')
   {
      $('.responsible_team_tbl').show();
      $('.responsible_user_tbl').show();
      $('.responsible_department_tbl').show();
     // $('.responsible_member_tbl').show();
   }else if(rec=='departments')
   {
      $('.responsible_department_tbl').show();
      $('.responsible_user_tbl').show();
      $('.responsible_team_tbl').show();
      //$('.responsible_member_tbl').show();
   }else if(rec=='members')
   {
     // $('.responsible_member_tbl').show();
      $('.responsible_user_tbl').show();
      $('.responsible_team_tbl').show();
      $('.responsible_department_tbl').show();
   
   }
   else{
         $('.responsible_user_tbl').hide();
         $('.responsible_team_tbl').hide();
         $('.responsible_department_tbl').hide();
       //  $('.responsible_member_tbl').hide();
   }
   });
   });
   
   function make_a_primary(id,clientId)
   {
         $(".LoadingImage").show();
   
   //alert(id);
   var data = {};
   data['contact_id'] = id;
   data['clientId'] = clientId;
   
   $.ajax({
      url: '<?php echo base_url();?>client/update_primary_contact/',
       type: "POST",
       data: data,
       success: function(data)  
       {
           $("#contactss").html(data);
               $(".LoadingImage").hide();
               $(".contactcc").hide();  
          
       }
     });
   
   }
   
   
    // $("#personss").change(function(){
    //   var person = $(':selected',this).val();
     $(".personss").click(function(){
      $('.for_contact_validation').html('');
      var person = $(this).attr('data-id');
   
   
     // alert(person);
   //alert('legal_form');
      if(person=='opt1'){
   
    //$('#exist_person').modal('show');
   $('.all_layout_modal').modal('show');
   $('.main_contacts').show();
    var company_no=$("#company_number").val();
    getCompanyView(company_no);
   
     // $('.all_layout_modal1').modal('show');
      }else if(person=='opt2')
      {
   
          $("#Add_New_Contact").show();
          var cnt = $("#append_cnt").val();
   
               if(cnt==''){
               cnt = 1;
              }else{
               cnt = parseInt(cnt)+1;
              }
           $("#append_cnt").val(cnt);
   
          $.ajax({
             url: '<?php echo base_url();?>client/new_contact/',
             type: 'post',
             
             data: { 'cnt':cnt,'incre':'1'},
             success: function( data ){
                 $('.contact_form').append(data);
                   $(".date_picker").datepicker({ dateFormat: 'dd-mm-yy',
                    // minDate:0,
                      changeMonth: true,
        changeYear: true, }).val();
                     var start = new Date();
    start.setFullYear(start.getFullYear() - 70);
    var end = new Date();
    end.setFullYear(end.getFullYear());
                   $(".date_picker_dob").datepicker({ dateFormat: 'dd-mm-yy',
                     //changeMonth: true,
        changeYear: true,yearRange: start.getFullYear() + ':' + end.getFullYear() }).val();
   
                        /** for append remove button **/
                 if(cnt>1){
                  $('.for_remove-'+cnt).remove();
                 $('.for_row_count-'+cnt).append('<div class="btn btn-danger remove for_remove-'+cnt+'"><a href="javascript:void(0)" class="contact_remove" id="remove-'+cnt+'">Remove</a></div>');
                 }
               var countofdiv=$('.make_a_primary').length;
               //alert(countofdiv);
   //             if(countofdiv>1)
   //             {
   //                 $('.for_remove-1').remove();
   // $('.for_row_count-1').append('<div class="remove for_remove-1"><a href="javascript:void(0)" class="contact_remove" id="remove-1">Remove</a></div>');
   //             }
                    $('.make_a_primary').each(function(){
                     var countofdiv=$('.make_a_primary').length;
                     if(countofdiv>1){
                     var id=$(this).attr('id').split('-')[1];
                     $('.for_remove-'+id).remove();
                     $('.for_row_count-'+id).append('<div class="btn btn-danger remove for_remove-'+id+'"><a href="javascript:void(0)" class="contact_remove" id="remove-'+id+'">Remove</a></div>');
                  }
                  });
                    
                 /** end of remove button **/
                  // $('.').datepicker({
                  // format: 'dd-mm-yyyy',
                  // startDate: '-3d'
                  // });
   
                 },
                 error: function( errorThrown ){
                     console.log( errorThrown );
                 }
             });
      }
   
      });   
   
   
   
   
    $("#Add_New_Contact").click(function(){
      $('.for_contact_validation').html('');
      $('#main_Contact_error').html('');
       var cnt = $("#append_cnt").val();
   
               if(cnt==''){
               cnt = 1;
              }else{
               cnt = parseInt(cnt)+1;
              }
           $("#append_cnt").val(cnt);
   
          $.ajax({
             url: '<?php echo base_url();?>client/new_contact/',
             type: 'post',
             
             data: { 'cnt':cnt,'incre':'1'},

              beforeSend: function() {
                        $(".LoadingImage").show();
                      },
             success: function( data ){
   
                 $('.contact_form').append(data);

                     $(".LoadingImage").hide();
                   var start = new Date();
    start.setFullYear(start.getFullYear() - 70);
    var end = new Date();
    end.setFullYear(end.getFullYear());
                 $(".date_picker_dob").datepicker({ dateFormat: 'dd-mm-yy',changeMonth: true,
        changeYear: true,yearRange: start.getFullYear() + ':' + end.getFullYear() }).val();
                 /** for append remove button 04-07-2018**/
                 if(cnt>1){
                  $('.for_remove-'+cnt).remove();
                 $('.for_row_count-'+cnt).append('<div class="btn btn-danger remove for_remove-'+cnt+'"><a href="javascript:void(0)" class="contact_remove" id="remove-'+cnt+'">Remove</a></div>');
                 }
               var countofdiv=$('.make_a_primary').length;
               //alert(countofdiv);
   //             if(countofdiv>1)
   //             {
   //       $('.for_remove-1').remove();
   // $('.for_row_count-1').append('<div class="remove for_remove-1"><a href="javascript:void(0)" class="contact_remove" id="remove-1">Remove</a></div>');
   //             }
               
                 $('.make_a_primary').each(function(){
                     var countofdiv=$('.make_a_primary').length;
                     if(countofdiv>1){
                     var id=$(this).attr('id').split('-')[1];
                     $('.for_remove-'+id).remove();
                   $('.for_row_count-'+id).append('<div class="btn btn-danger remove for_remove-'+id+'"><a href="javascript:void(0)" class="contact_remove" id="remove-'+id+'">Remove</a></div>');
                  }
   
                  });
                 /** end of remove button **/
                   $(".date_picker").datepicker({ dateFormat: 'dd-mm-yy',
                     //minDate:0,
                      changeMonth: true,
        changeYear: true, }).val();
                  // $('.').datepicker({
                  // format: 'dd-mm-yyyy',
                  // startDate: '-3d'
                  // });
   
                 },
                 error: function( errorThrown ){
                     console.log( errorThrown );
                 }
             });
    });
   
      $(document).on("click", ".fields.edit_classname.datepicker.hasDatepicker", function() {
   
         // alert('hi');
   
      $(".date_picker").datepicker({ dateFormat: 'dd/mm/yy',
         //minDate:0,
          changeMonth: true,
        changeYear: true, }).val();
         
      }); 
   
   
   
   
   
</script>
<style type="text/css">
   .disabled{
   opacity: 0.5;
   pointer-events: none;    
   }
</style>
<script type="text/javascript">
   $(document).on('click','.add_merge',function(e)
   {        
      var target = $(this).attr('target');
      var name=$(this).html();
      $('[name='+target+']').append(name);          
   });  
   
   
   
   $(document).on('keyup',".allocation_holder_cls",function(){     
   
         var album_text = [];
   
   $("input[name='allocation_holder[]']").each(function() {
       var value = $(this).val();
       if (value) {
           album_text.push(value);
       }
   });
   if (album_text.length === 0) {
     $('.yk_pack_addrow_td').css('display','none');
   }
   
   else {
     $('.yk_pack_addrow_td').css('display','block');
   } 
       
   
   
   });
   
   $(document).on('keyup',".allocation_holder_dept_cls",function(){
   var album_text = [];
   $("input[name='allocation_holder_dept[]']").each(function() {
       var value = $(this).val();
       if (value) {
           album_text.push(value);
       }
   });
   if (album_text.length === 0) {
     $('.yk_pack_addrow_tr').css('display','none');
   }
   
   else {
     $('.yk_pack_addrow_tr').css('display','block');
   }
   });
    
   
</script>
<script type="text/javascript">
   $(document).ready(function(){  
        $('#refer_exist_client').keyup(function(){  
         // alert('text');
             var query = $(this).val();  
             if(query != '')  
             {  
                  $.ajax({  
                       url:"<?php echo base_url();?>client/search_client/",  
                       method:"POST",  
                       data:{query:query},  
                       success:function(data)  
                       {  
                            $('#searchresultclient').fadeIn();  
                            $('#searchresultclient').html(data);  
                       }
                  });  
             }
             if(query=='')
             {
               $('#searchresultclient').fadeOut();
             }  
        });  
        $(document).on('click', '.client_name', function(){  
             $('#refer_exist_client').val($(this).text()); 
             //$('#user_id').val($(this).data('id')); 
             $('#searchresultclient').fadeOut();  
        });  
   });
</script>
<script type="text/javascript">
   /*$(document).ready(function(){  
        $(document).on('change','#placeholder',function(e){ 
         // alert('text');
             var query = $(this).val();  
             if(query != '')  
             {  
                  $.ajax({  
                       url:"<?php echo base_url();?>client/get_placeholder/",  
                       method:"POST",  
                       data:{query:query},  
                       success:function(data)  
                       {  
                            $('.append_placeholder').fadeIn();  
                            $('.append_placeholder').html(data); 
       
                       }
                  });  
             }
             if(query=='')
             {
               $('.append_placeholder').fadeOut();
             }  
        }); 
   
   
   });*/
</script>
<!-- <script type="text/javascript">
   $(document).ready(function() {
       $(".reminder_close").click(function(){
   
         alert($('.alert-success-re').length());
        //  console.log($('.alert-success-re').length());
          // alert("button");
         // $("div .custom_remain").trigger('click');
       }); 
   });
   
   
   
      $(document).ready(function(){
      
      
      $(".edit_field_form").submit(function () {
      
            var clikedForm = $(this); // Select Form
            var user_id = $('#user_id').val();
            if(clikedForm.find("[name='days']").val()=='')
            {
             $('.error_msg_days').show();
            }
            else
            {
               $('.error_msg_days').hide();
            }
            if(clikedForm.find("[name='subject']").val()=='')
            {
             $('.error_msg_subject').show();
            }
            else
            {
               $('.error_msg_subject').hide();
            }

            if(clikedForm.find("[name='email_template']").val()=='')
            {
             $('.error_msg_email').show();
            }
            else
            {
               $('.error_msg_email').hide();
            }      
            if (clikedForm.find("[name='name']").val() == '') {
               //$('.error').show();
               $(this).find('.error').show();
              //alert('Enter Valid name');
             
            } else {$('.error').hide();}
            if(clikedForm.find("[name='days']").val()!='' && clikedForm.find("[name='subject']").val()!='' && clikedForm.find("[name='name']").val() != '' && clikedForm.find("[name='email_template']").val() != ''){
                  
               $.ajax({
               type: "POST",
               url: '<?php echo base_url();?>client/add_custom_reminder/',
               data: $(this).serialize()+"&user_id="+user_id,
               success: function (data) { 
                  // Inserting html into the result div
                  //console.log(data);
                                          if(data != 0){
                                             /** 05-07-2018 reminder ids **/
                                             var get_val=$('#add_reminder_ids').val();
                                             if(get_val!='')
                                             {
                                                var res_val=get_val+","+data;
                                             }
                                             else{
                                                var res_val=data;
                                             }                   
                                             $('#add_reminder_ids').val(res_val);
                                             /** 06-07-2018 for ids **/
                                             // $('#new_user')[0].reset();
                                              // $('.alert-success-re').show();
                                              // $('.alert-danger-re').hide();
   
                                             //location.reload(); 
                                          }
                                          else{
                                             // alert('failed');
                                              // $('.alert-danger-re').show();
                                              // $('.alert-success-re').hide();
                                          }
                                          $('.alert-success-reminder').show();
                                           setTimeout(function() { 
                                             $('.alert-success-reminder').hide();
                                             //$('#add-reminder').css('display','none');
                                              $('#add-reminder').modal('hide');
                                                $('.modal-backdrop.show').hide();
                                           },1000);
   
               },
               error: function(jqXHR, text, error){
                  // Displaying if there are any errors
                     $('#result').html(error);           
              }
          });
            return false;
            }
            else
            {
                return false;
            }
           
           
      
          });
      });
</script> -->

<script type="text/javascript">
   $(function() {
               $('.click_code03').on('click', function( e ) {
             var text = $(this).parents('.edit-field-popup1').find('.edit_classname').attr('id');
             $('[name="hidden"]').val(text);
           });
   });
   
   $(document).ready(function () {
           $('.profileEdit a').click(function (elem) {
         $('#'+$('[name="hidden"]').val()).attr("readonly", false); 
         var id=$('[name="hidden"]').val();
         if('date_of_creation'==id || 'confirm_next_made_up_to'== id || 'confirm_next_due'==id || 'next_made_up_to'==id || 'next_due'==id ){
   
         $('#'+id).addClass('dob_picker');
          $(".dob_picker").datepicker({ dateFormat: 'dd-mm-yy',
            //minDate:0,
             changeMonth: true,
        changeYear: true, }).val();
       } 
         $('.modal').modal('hide');
           });
   });
</script>

<script type="text/javascript">
   $(document).ready(function(){
   //$('.sel-legal-form').trigger('change');
  /* <?php if($uri_seg!=''){ ?>
           $('.basic_details_tab').show();
      
            $('.basic_details_tab').addClass('bbs');
            $('.basic_deatils_id').addClass('active');
            $('#basic_details').addClass('active');
            $('.for_contact_tab').removeClass('bbs');
            $('.main_contacttab').removeClass('active');
            $('#main_Contact').removeClass('active');
      <?php } ?>*/
   });
    $(document).on('change', '.sel-legal-form', function() {
      var selectVal = $(this).find(':selected').val();
      // company type value
      <?php if($uri_seg==''){ ?>
      $('#company_type').val(selectVal);
      $('#company_type').prop('readonly', true);
      <?php } ?>
      // alert(selectVal);
   
      if((selectVal == "Private Limited company") || (selectVal == "Public Limited company") || (selectVal == "Limited Liability Partnership")) {
   
         $('.filing-alcat-tab').show();
         $('.business-info-tab').hide();
         // $('.business-info-tab').show();
         /** 16-08-2018 shown shown basic details tab **/
         $('.basic_details_tab').show();
         <?php if( $firm !== false ){ ?>
            $('.basic_details_tab').addClass('bbs');
            $('.basic_deatils_id').addClass('active');
            $('#basic_details').addClass('active');
            $('.for_contact_tab').removeClass('bbs');
            $('.main_contacttab').removeClass('active');
            $('#main_Contact').removeClass('active');
         <?php } ?>
         /** end of 16-08-2018 **/
      }
      else if((selectVal == "Partnership") || (selectVal == "Self Assessment")) {
   
         $('.business-info-tab').show();
         $('.filing-alcat-tab').hide();
         /** 16-08-2018 shown hide basic details tab **/
         $('.basic_details_tab').hide();
           <?php if(  $firm !== false  ){ ?>
         $('.for_contact_tab').addClass('bbs');
           $('.main_contacttab').addClass('active');
            $('#main_Contact').addClass('active');
            $('.basic_details_tab').removeClass('bbs');
              $('.basic_deatils_id').removeClass('bbs');
             $('#basic_details').removeClass('active');
            <?php } ?>
         /** end of 16-08-2018 **/
      }
      else
      {
         $('.business-info-tab').hide();
         $('.filing-alcat-tab').hide();
         /** 16-08-2018 shown hide basic details tab **/
         $('.basic_details_tab').hide();
                 <?php if(  $firm !== false  ){ ?>
     $('.for_contact_tab').addClass('bbs');
           $('.main_contacttab').addClass('active');
            $('#main_Contact').addClass('active');
            $('.basic_details_tab').removeClass('bbs');
              $('.basic_deatils_id').removeClass('bbs');
             $('#basic_details').removeClass('active');
            <?php } ?>
         /** end of 16-08-2018 **/
      }
    });  
   
    $(document).on('click','.contact_remove',function(){
      console.log('end end');
    var countno=$(this).attr('id').split('-')[1];
    $('.common_div_remove-'+countno).remove();
       var countofdiv=$('.make_a_primary').length;
       if(countofdiv==1)
       {
         $('.contact_remove').css('display','none');
         $('.btn.btn-danger.remove').css('display','none');
       }
       else
       {
        $('.contact_remove').css('display',''); 
        $('.btn.btn-danger.remove').css('display','');
       }
   
    });
   
   
    $(document).ready(function(){
   $('.make_a_primary').each(function(){
      var countofdiv=$('.make_a_primary').length;
      if(countofdiv>1){
      var id=$(this).attr('id').split('-')[1];
      $('.for_remove-'+id).remove();
    $('.for_row_count-'+id).append('<div class="btn btn-danger remove for_remove-'+id+'"><a href="javascript:void(0)" class="contact_remove" id="remove-'+id+'">Remove</a></div>');
   }
   
   });
    });
     $(document).ready(function() {
   
      $('.dropdown-sin-2').dropdown({
     
        input: '<input type="text" maxLength="20" placeholder="Search">'
      });
      $('.dropdown-sin-3').dropdown({
     
        input: '<input type="text" maxLength="20" placeholder="Search">'
      });
      $('.dropdown-sin-11').dropdown({
     
        input: '<input type="text" maxLength="20" placeholder="Search">'
      });
      
   
   });
     $(document).on('change','#source',function(){
            var value=$(this).val();
            if(value=='Existing Client')
            {
               $('.dropdown_source').css('display','');
               $('.textfor_source').css('display','none');
            }
            else
            {
               $('.dropdown_source').css('display','none');
               $('.textfor_source').css('display','');
            }
     });

   
    $('.decimal_days').keyup(function(evt){
      var text = $(this).val();
      var test_value = text.replace(/[^0-9]+/g, "");
      $(this).val(test_value);
    });

   $('.decimal').keyup(function(){
    console.log('#####');
    var val = $(this).val();
    if(isNaN(val)){
         val = val.replace(/[^0-9\.]/g,'');
         if(val.split('.').length>2) 
             val =val.replace(/\.+$/,"");
    }
    $(this).val(val); 
});

$(document).ready(function () {
  //called when key is pressed in textbox
  $(".due-days").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
              return false;
    }
   });
});


/** 11-09-2018 **/
   $(document).on('keyup  blur change',"input",function(){
   var name=$(this).attr('name');
   var thisval=$(this).val();
   var type=$(this).attr('type');
  // console.log(thisval);
   //console.log(name);
   //console.log(type);
   var vallen=$("[name^="+name+"]").length;
   //alert(vallen);
   if(vallen >1 && (type=="text" || type=="number")){
          $("[name^="+name+"]").each(function() {
           $(this).val(thisval);
         });
       }


 });
   $(document).on('keyup keypress blur change',"textarea",function(){
   var name=$(this).attr('name');
   var thisval=$(this).val();
   var vallen=$("[name^="+name+"]").length;
   console.log(thisval);
   console.log(name);
   if(vallen >1){
          $("[name^="+name+"]").each(function() {
           $(this).val(thisval);
         });
       }
  
   });

/** end of 11-09-2018 **/
/** 14-09-2018 for client section **/
$(document).on('click','.make_primary_section',function(){

    $(".LoadingImage").show();
  $('.for_contact_validation').html('');
   var its_data_id=$(this).attr('data-id');
   $('#'+its_data_id).trigger('click');
   $('.make_primary_section').each(function(){
      $(this).html('<span>Make A Primary</span>');
   });
   $(this).html('<span style="color:red;">Primary Contact</span>');
 $(".LoadingImage").hide();
});

         
$(document).ready(function() {   
 $('.alert-ss').parent().addClass('cmn-errors1');
});

$(".alert_close").click(function(){
   window.location.href = '<?php echo base_url(); ?>user';
});

</script>

<script type="text/javascript">
  $(document).ready(function(){
     
  $('.js-small').parent('.col-sm-8').addClass('addbts-radio');
  $('.accordion-views .toggle').click(function(e) {
$(this).next('.inner-views').slideToggle(300);
$(this).toggleClass('this-active');
});
  });
</script>
   
</body>
</html>