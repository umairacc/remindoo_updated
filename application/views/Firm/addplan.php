<?php $this->load->view('super_admin/superAdmin_header'); ?> 
<style type="text/css">
  .form-error
  {
    color: red;
  }
</style>
<div class="pcoded-content card-removes firmlisttop">
 <div class="pcoded-inner-content">
    <!-- Main-body start -->
    <div class="main-body">
       <div class="page-wrapper">
          <!-- Page body start -->
          <div class="page-body">
              <div class="row">
                <div class="col-sm-12">
                   <!-- Register your self card start -->
                   <div class="card">                      

                   <div class="all_user-section floating_set clientredesign select-check">
                      <div class="deadline-crm1 floating_set">
                         <ul class="nav nav-tabs1 all_user1 md-tabs pull-left u-dashboard">
                            <?php if($this->uri->segment(2) == 'add_plan'){ ?>
                            <li class="nav-item">
                               <a class="nav-link active" href="javascript:void(0);"><img class="themicon" src="<?php echo base_url();?>assets/images/tabicon2.png" alt="themeicon" />Add New Plan</a>
                               <div class="slide"></div>
                            </li>
                            <?php }else if($this->uri->segment(2) == 'edit_plan'){ ?>
                              <li class="nav-item">
                                 <a class="nav-link active" href="javascript:void(0);">Edit Plan</a>
                                 <div class="slide"></div>
                              </li>
                            <?php } ?>                                                               
                         </ul>
                      </div>
                   </div>

                   <div class="all_user-section floating_set pros-editemp"> 
                      <div class="all_user-section2 floating_set">
                         <div class="tab-content">
                            <div id="allusers" class="tab-pane fade in active">            
                               <div class="client_section3 table-responsive">     
                                  <div class="all-usera1 data-padding1 ">               
                                   <div class="">
                               <?php if($this->uri->segment(2) == 'add_plan'){ ?>
                                   <form action="<?php echo base_url().'firm/insertplan'; ?>" class="require-validation add" method="post" id="plan_form">
                             <?php }else if($this->uri->segment(2) == 'edit_plan'){ ?>  
                                   <form action="<?php echo base_url().'firm/updateplan'; ?>" class="require-validation edit" method="post" id="plan_form">
                             <?php } ?>
                                   <div class=" suballemailclsnew">    
                                      <!--start-->
                                    <div class="col-sm-12">
                                    <!-- Register your self card start -->
                                    <div class="col-12 card">
                                    <!-- admin start-->
                                    <div class="client_section user-dashboard-section1 floating_set newmailclsnew">     
                                             
                                       <div class="columns">
                                         <div class="edit-tempsect col-md-7">   

                                       <?php if(count($plandetails)>0){ ?>
                                        <input type="hidden" name="plan_id" value="<?php echo $plandetails[0]['id']; ?>">
                                       <?php } ?>
                                       <div class="newemailset">
                                        <div class="">          
                                        <label>Plan Name</label>
                                        </div>
                                        <input type="text" name="plan_name" class="form-control" data-validation="required" value="<?php if(count($plandetails)>0){ echo $plandetails[0]['plan_name']; } ?>">
                                        <span id="plan_name_er"></span>
                                       </div>

                                       <div class="newemailset">
                                        <div class="">          
                                        <label>Plan Type</label>
                                        </div>
                                        <div class="select">          
                                        <select  name="type" class="form-control" data-validation="required">
                                        <option value="">Select</option>
                                        <option value="1" <?php if(count($plandetails)>0 && $plandetails[0]['type'] == '1'){ echo "selected"; } ?>>Monthly</option>
                                        <option value="2" <?php if(count($plandetails)>0 && $plandetails[0]['type'] == '2'){ echo "selected"; } ?>>Yearly</option>
                                          </select>                                     
                                            </div>        
                                           </div> 

                                           <div class="newemailset">
                                            <div class="">          
                                            <label>Subscription Amount</label>
                                            </div>
                                            <input type="text" name="plan_amount" class="form-control" data-validation="required,number" data-validation-allowing="range[1;10000000000000],float,integer" value="<?php if(count($plandetails)>0){ echo $plandetails[0]['plan_amount']; } ?>">
                                           </div>

                                           <div class="newemailset">
                                            <div class="">          
                                            <label>Plan Description</label>
                                            </div>
                                            <textarea name="description" class="form-control" data-validation="required"><?php if(count($plandetails)>0){ echo $plandetails[0]['description']; } ?></textarea>
                                           </div>   

                                          <div class="newemailset unlimit">
                                            <div class="">          
                                            <label>Unlimited Option</label>   
                                            </div>
                                            <input type="radio" name="unlimited" value="1" <?php if(count($plandetails)>0 && $plandetails[0]['unlimited'] == '1'){ echo "checked"; } ?>>&nbsp;&nbsp;Clients<br>
                                            <input type="radio" name="unlimited" value="0"  <?php if(count($plandetails)>0 && $plandetails[0]['unlimited'] == '0'){ echo "checked"; } ?>>&nbsp;&nbsp;User<br>
                                            <input type="radio" name="unlimited" value="">&nbsp;&nbsp;None
                                            <br><span id="option_er"></span>
                                          </div>

                                          <div class="newemailset client">
                                            <div class="">          
                                            <label>Amount Per Client</label>
                                            </div>
                                            <input type="text" name="cost_per_client" class="form-control" data-validation="" value="<?php if(count($plandetails)>0){ echo $plandetails[0]['cost_per_client']; } ?>">
                                          </div>

                                          <div class="newemailset user">
                                            <div class="">          
                                            <label>Amount Per User</label>
                                            </div>
                                            <input type="text" name="cost_per_user" class="form-control" data-validation="" value="<?php if(count($plandetails)>0){ echo $plandetails[0]['cost_per_user']; } ?>">
                                          </div>

                                          <div class="newemailset limit">
                                            <div class="">          
                                            <label>Limited Option</label>    
                                            </div>
                                            <input type="radio" name="limited" value="1" <?php if(count($plandetails)>0 && $plandetails[0]['limited'] == '1'){ echo "checked"; } ?>>&nbsp;&nbsp;Clients<br>
                                            <input type="radio" name="limited" value="0"  <?php if(count($plandetails)>0 && $plandetails[0]['limited'] == '0'){ echo "checked"; } ?>>&nbsp;&nbsp;User<br>
                                            <input type="radio" name="limited" value="">&nbsp;&nbsp;None
                                          </div>

                                          <div class="newemailset clientlimit" style="display: none;">
                                            <div class="">          
                                            <label>Client Limit</label>
                                            </div>
                                            <input type="text" name="client_limit" class="form-control" data-validation="" value="<?php if(count($plandetails)>0){ echo $plandetails[0]['client_limit']; } ?>">
                                          </div>

                                          <div class="newemailset userlimit" style="display: none;">
                                            <div class="">          
                                            <label>User Limit</label>
                                            </div>
                                            <input type="text" name="user_limit" class="form-control" data-validation="" value="<?php if(count($plandetails)>0){ echo $plandetails[0]['user_limit']; } ?>">
                                          </div>                                         
                                          <div class="newemailset">
                                            <div class="">          
                                            <label>Tax Amount</label>
                                            </div>
                                            <input type="text" name="tax" class="form-control" data-validation="required,number" data-validation-allowing="float,integer" value="<?php if(count($plandetails)>0){ echo $plandetails[0]['tax']; } ?>">
                                          </div>

                                          <div class="newemailset">
                                            <div class="">          
                                            <label>Discount Amount</label>
                                            </div>
                                            <input type="text" name="discount" class="form-control" data-validation="required,number" data-validation-allowing="float,integer" value="<?php if(count($plandetails)>0){ echo $plandetails[0]['discount']; } ?>">
                                          </div>

                                          <div class="newemailset">
                                            <div class="">          
                                            <label>Trial</label>
                                            </div>
                                            <input type="radio" name="trial" value="1" <?php if(count($plandetails)>0 && $plandetails[0]['trial'] == '1'){ echo "checked"; } ?>>&nbsp;&nbsp;Yes<br>
                                            <input type="radio" name="trial" value="0"  <?php if(count($plandetails)>0 && $plandetails[0]['trial'] == '0'){ echo "checked"; } ?>>&nbsp;&nbsp;No
                                          </div>

                                          <div class="newemailset tdays" style="display: none;">
                                            <div class="">          
                                            <label>Trial Days</label>
                                            </div>
                                            <input type="text" name="trial_days" class="form-control" data-validation="required,number" value="<?php if(count($plandetails)>0){ echo $plandetails[0]['trial_days']; } ?>">
                                          </div>

                                          <div class="newemailset">
                                            <div class="">          
                                            <label>Status</label>
                                            </div>
                                            <div class="select">
                                               <select name="status" class="form-control" data-validation="required">
                                                <option value="">Select</option>
                                                <option value="1" <?php if(count($plandetails)>0 && $plandetails[0]['status'] == '1'){ echo "selected"; } ?>>Active</option>
                                                <option value="0" <?php if(count($plandetails)>0 && $plandetails[0]['status'] == '0'){ echo "selected"; } ?>>Inactive</option>
                                              </select>                                     
                                           </div>       
                                          </div>

                                     </div>
                                               <!-- Page body end -->
                                         </div>
                                         </div>
                                         <!-- Main-body end -->
                                         <div id="styleSelector"></div>
                                   </div>
                                   
                                   <button type="submit" class="btn btn-primary saveclsset">Save</button>
                             
                                   </form>
                                       
                                  </div>
                                  </div>                     
                               </div>
                            </div>
                         </div>
                      </div>
                   </div>                       
                 </div>
             </div>
         </div>
     </div>
 </div>
</div>
</div>
</div>

<?php $this->load->view('super_admin/superAdmin_footer',['INCLUDE'=>['DT_JS']]);?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.79/jquery.form-validator.js"></script>
<script>
  $.validate({
    lang: 'en'
  });  

  $('input[name="trial"]').on('click',function()
  {
      if($(this).val() == '1')
      {
          $('.tdays').show();
      }
      else
      {
          $('.tdays').hide();
      }
  });

  $('input[name="unlimited"]').on('click',function()
  {        
      if($(this).val() == '1')
      {
          if($('input[name="limited"]:checked').val() == '1')
          {
             $('#option_er').html('Both Limited And Unlimited Options Cannot Be Same.').css('color','red');
          }          
          else
          {
             $('.user').show();
             $('input[name="cost_per_user"]').attr('data-validation','required,number');
             $('input[name="cost_per_user"]').attr('data-validation-allowing','range[1;10000000000000],float,integer');
             $('input[name="cost_per_client"]').attr('data-validation','');
             $('.client').hide();
             $('#option_er').html('');

             if($('input[name="limited"]:checked').val() == '0')
             {
                $('.user').hide();
                $('.userlimit').show();
                $('.clientlimit').hide();
                $('input[name="user_limit"]').attr('data-validation','required,number');
                $('input[name="client_limit"]').attr('data-validation','');
                $('.user').hide();             
                $('#option_er').html('');
             }
          }
      }
      else if($(this).val() == '0')
      {
          if($('input[name="limited"]:checked').val() == '0')
          {
             $('#option_er').html('Both Limited And Unlimited Options Cannot Be Same.').css('color','red');
          }          
          else
          {
             $('.client').show();
             $('input[name="cost_per_client"]').attr('data-validation','required,number');
             $('input[name="cost_per_client"]').attr('data-validation-allowing','range[1;10000000000000],float,integer');
             $('input[name="cost_per_user"]').attr('data-validation','');
             $('.user').hide();
             $('#option_er').html('');

             if($('input[name="limited"]:checked').val() == '1')
             {
                $('.client').hide();
                $('.userlimit').hide();
                $('.clientlimit').show();
                $('input[name="client_limit"]').attr('data-validation','required,number');
                $('input[name="user_limit"]').attr('data-validation','');
                $('.client').hide();              
                $('#option_er').html('');
             }
          }
      }
      else if($(this).val() == '')
      {
          $('.client').show();
          $('.user').show();
          $('input[name="cost_per_user"]').attr('data-validation','required,number');
          $('input[name="cost_per_user"]').attr('data-validation-allowing','range[1;10000000000000],float,integer');
          $('input[name="cost_per_client"]').attr('data-validation','required,number');
          $('input[name="cost_per_client"]').attr('data-validation-allowing','range[1;10000000000000],float,integer');

          $('#option_er').html('');

          if($('input[name="limited"]:checked').val() == '0')
          {
             $('.user').hide(); 
          }
          else if($('input[name="limited"]:checked').val() == '1')
          {
             $('.client').hide(); 
          }
      }             
  });

  $('input[name="limited"]').on('click',function()
  {        
      if($(this).val() == '1')
      {
          if($('input[name="unlimited"]:checked').val() == '1')
          {
             $('#option_er').html('Both Limited And Unlimited Options Cannot Be Same.').css('color','red');
          }          
          else
          {
              $('.userlimit').hide();
              $('.clientlimit').show();
              $('input[name="client_limit"]').attr('data-validation','required,number');
              $('input[name="user_limit"]').attr('data-validation','');
              $('.client').hide();              
              $('#option_er').html('');

              if($('input[name="unlimited"]:checked').val() == '0')
              {
                 $('.client').hide(); 
              }
              else if($('input[name="unlimited"]:checked').val() == '')
              {
                 $('.user').show(); 
              }
          }
      }
      else if($(this).val() == '0')
      {
          if($('input[name="unlimited"]:checked').val() == '0')
          {
             $('#option_er').html('Both Limited And Unlimited Options Cannot Be Same.').css('color','red');
          }          
          else
          {
             $('.userlimit').show();
             $('.clientlimit').hide();
             $('input[name="user_limit"]').attr('data-validation','required,number');
             $('input[name="client_limit"]').attr('data-validation','');
             $('.user').hide();             
             $('#option_er').html('');

             if($('input[name="unlimited"]:checked').val() == '1')
             {
                $('.user').hide(); 
             }
             else if($('input[name="unlimited"]:checked').val() == '')
             {
                $('.client').show(); 
             }
          }
      }
      else if($(this).val() == '')
      {
          $('.client').show();
          $('.user').show();
          $('input[name="cost_per_user"]').attr('data-validation','required,number');
          $('input[name="cost_per_user"]').attr('data-validation-allowing','range[1;10000000000000],float,integer');
          $('input[name="cost_per_client"]').attr('data-validation','required,number');
          $('input[name="cost_per_client"]').attr('data-validation-allowing','range[1;10000000000000],float,integer');

          $('.userlimit').hide();
          $('.clientlimit').hide();
          $('#option_er').html('');

          if($('input[name="unlimited"]:checked').val() == '0')
          {
             $('.user').hide(); 
          }
          else if($('input[name="unlimited"]:checked').val() == '1')
          {
             $('.client').hide(); 
          }
      }         
  });

  $('#plan_form').on('submit',function(event)
  { 
      if($(this).hasClass('add'))
      {
         var formdata = {'plan_name': $('input[name="plan_name"]').val()};
      }
      else if($(this).hasClass('edit'))
      {
         var formdata = {'plan_name': $('input[name="plan_name"]').val(),'id': $('input[name="plan_id"]').val()};
      }

      $.ajax(
      {
         url: '<?php echo base_url().'firm/check_distinct'; ?>',
         type: 'POST',
         data: formdata,
         async:false,

         beforeSend: function() 
         {
            $(".LoadingImage").show();
         },

         success:function(data)
         {
            var res = JSON.parse(data);
            $(".LoadingImage").hide();

            if(res.count>0)
            { 
                 $('#plan_name_er').html('Already Exists.').css('color','red');
                 event.preventDefault();
            }
            else if($('input[name="limited"]:checked').val() == $('input[name="unlimited"]:checked').val() && $('input[name="limited"]:checked').val() != undefined)
            {
                $('#option_er').html('Both Limited And Unlimited Options Cannot Be Same.').css('color','red');
                event.preventDefault();
            }
            else
            {
               $('#plan_name_er').html('');
               $('#option_er').html('');
               $(".LoadingImage").show();
            }           
         }
      });
        
  });

  <?php if($this->uri->segment(2) == 'edit_plan'){ ?>
   
   $(document).ready(function()
   {
      $('select[name="type"]').attr('disabled','disabled');

      <?php if($plandetails[0]['trial'] == '1'){ ?>                 
          $('.tdays').show();     
      <?php }else{  ?>
          $('.tdays').hide();
      <?php } ?>

      <?php if($plandetails[0]['unlimited'] == '1'){ ?>     
          $('.user').show();
          $('.client').hide(); 
          $('input[name="cost_per_user"]').attr('data-validation','required,number');
          $('input[name="cost_per_user"]').attr('data-validation-allowing','range[1;10000000000000],float,integer');
      <?php }else if($plandetails[0]['unlimited'] == '0'){ ?>      
          $('.client').show();
          $('.user').hide();
          $('input[name="cost_per_client"]').attr('data-validation','required,number');
          $('input[name="cost_per_client"]').attr('data-validation-allowing','range[1;10000000000000],float,integer');
      <?php } ?>

      <?php if($plandetails[0]['limited'] == '1'){ ?> 
          $('.clientlimit').show();
          $('.userlimit').hide(); 
          $('input[name="client_limit"]').attr('data-validation','required,number');   
          $('.client').hide();       
      <?php }else if($plandetails[0]['limited'] == '0'){ ?>     
          $('.userlimit').show();
          $('.clientlimit').hide();
          $('input[name="user_limit"]').attr('data-validation','required,number');
          $('.user').hide();
      <?php } ?>

   });

  <?php } ?>

</script>