<?php $this->load->view('includes/header');?>
<style>
   /*span.newonoff {
   background: #4680ff;
   padding: 6px 15px;
   display: inline-block;
   color: #fff;
   border-radius: 5px;
   }*/
   button.btn.btn-info.btn-lg.newonoff {
   padding: 3px 10px;
   height: initial;
   font-size: 15px;
   border-radius: 5px;
   }
   /* #datepicker{
   height: 38px;
   width: 110px;
   background-color: #E8E8E7;
   border: none;
   }*/
   /*.but{
   height: 38px;
   width: 83px;
   padding-top: 6px;
   }*/
   .dropdown-content {
   display: none;
   position: absolute;
   background-color: #fff;
   min-width: 86px;
   overflow: auto;
   box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
   z-index: 1;
   left: -92px;
   width: 150px;
   }
   .dropdown {
   position: relative;
   display: inline-block;
   }/*
   .dropdown-content {
   display: none;
   position: absolute;
   background-color: #f1f1f1;
   min-width: 86px;
   overflow: auto;
   box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
   z-index: 1;
   }
   */
   .dropdown-content a {
   color: black;
   padding: 9px 22px;
   text-decoration: none;
   display: block;
   }
   /*
   .dropdown a:hover {
   background-color: #4c7ffe;
   color: #fff;
   }*/
   .show {display:block;}
   .hide{
   display: none;
   }
</style>
<div class="pcoded-content card-removes">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card">
                        <!-- admin start-->
                        <div class="client_section user-dashboard-section1 floating_set">
                
                           <div class="all_user-section floating_set">   
                              <div class="all_user-section2 floating_set">
                             
                                 <div class="tab-content">
                                    <div id="allusers" class="tab-pane fade <?php if(isset($_SESSION['firm_seen'])){ if($_SESSION['firm_seen']=='alluser'){ ?> in active  <?php }}else{ ?> in active <?php } ?>">
                                     
                                       <div id="task"></div>
                                       <div class="client_section3 table-responsive ">
                                          <div class="status_succ"></div>
                                          <div class="all-usera1 data-padding1 ">
                                             <div class="">
													<table id="display_service1" class="display nowrap" style="width:100%">
													<thead>
													<tr class="table-header">
														<th>Services</th>
													
													</tr>
													</thead>
													<tbody><?php 
													$i=1;
													foreach($service_details as $service){
													(isset(json_decode($service['conf_statement'])->tab) && $service['conf_statement'] != '') ? $jsnvat =  json_decode($service['conf_statement'])->tab : $jsnvat = '';
													  ($jsnvat!='') ? $conf_statement = "Enable" : $conf_statement = "Disable";
													//accounts
													 (isset(json_decode($service['accounts'])->tab) && $service['accounts'] != '') ? $jsnvat =  json_decode($service['accounts'])->tab : $jsnvat = '';
													  ($jsnvat!='') ? $accounts = "Enable" : $accounts = "Disable";
													//company_tax_return
													 (isset(json_decode($service['company_tax_return'])->tab) && $service['company_tax_return'] != '') ? $jsnvat =  json_decode($service['company_tax_return'])->tab : $jsnvat = '';
													  ($jsnvat!='') ? $company_tax_return = "Enable" : $company_tax_return = "Disable";
													//personal_tax_return
													 (isset(json_decode($service['personal_tax_return'])->tab) && $service['personal_tax_return'] != '') ? $jsnvat =  json_decode($service['personal_tax_return'])->tab : $jsnvat = '';
													  ($jsnvat!='') ? $personal_tax_return = "Enable" : $personal_tax_return = "Disable";
													//payroll
													 (isset(json_decode($service['payroll'])->tab) && $service['payroll'] != '') ? $jsnvat =  json_decode($service['payroll'])->tab : $jsnvat = '';
													  ($jsnvat!='') ? $payroll = "Enable" : $payroll = "Disable";
													//workplace
													 (isset(json_decode($service['workplace'])->tab) && $service['workplace'] != '') ? $jsnvat =  json_decode($service['workplace'])->tab : $jsnvat = '';
													  ($jsnvat!='') ? $workplace = "Enable" : $workplace = "Disable";
													//vat
													 (isset(json_decode($service['vat'])->tab) && $service['vat'] != '') ? $jsnvat =  json_decode($service['vat'])->tab : $jsnvat = '';
													  ($jsnvat!='') ? $vat = "Enable" : $vat = "Disable";
													//cis
													 (isset(json_decode($service['cis'])->tab) && $service['cis'] != '') ? $jsnvat =  json_decode($service['cis'])->tab : $jsnvat = '';
													  ($jsnvat!='') ? $cis = "Enable" : $cis = "Disable";
													//cissub
													 (isset(json_decode($service['cissub'])->tab) && $service['cissub'] != '') ? $jsnvat =  json_decode($service['cissub'])->tab : $jsnvat = '';
													  ($jsnvat!='') ? $cissub = "Enable" : $cissub = "Disable";
													//p11d
													 (isset(json_decode($service['p11d'])->tab) && $service['p11d'] != '') ? $jsnvat =  json_decode($service['p11d'])->tab : $jsnvat = '';
													  ($jsnvat!='') ? $p11d = "Enable" : $p11d = "Disable";
													//bookkeep
													 (isset(json_decode($service['bookkeep'])->tab) && $service['bookkeep'] != '') ? $jsnvat =  json_decode($service['bookkeep'])->tab : $jsnvat = '';
													  ($jsnvat!='') ? $bookkeep = "Enable" : $bookkeep = "Disable";
													//management
													 (isset(json_decode($service['management'])->tab) && $service['management'] != '') ? $jsnvat =  json_decode($service['management'])->tab : $jsnvat = '';
													  ($jsnvat!='') ? $management = "Enable" : $management = "Disable";
													//investgate
													 (isset(json_decode($service['investgate'])->tab) && $service['investgate'] != '') ? $jsnvat =  json_decode($service['investgate'])->tab : $jsnvat = '';
													  ($jsnvat!='') ? $investgate = "Enable" : $investgate = "Disable";
													//registered
													 (isset(json_decode($service['registered'])->tab) && $service['registered'] != '') ? $jsnvat =  json_decode($service['registered'])->tab : $jsnvat = '';
													  ($jsnvat!='') ? $registered = "Enable" : $registered = "Disable";
													//taxadvice
													 (isset(json_decode($service['taxadvice'])->tab) && $service['taxadvice'] != '') ? $jsnvat =  json_decode($service['taxadvice'])->tab : $jsnvat = '';
													  ($jsnvat!='') ? $taxadvice = "Enable" : $taxadvice = "Disable";
													//taxinvest
													  (isset(json_decode($service['taxinvest'])->tab) && $service['taxinvest'] != '') ? $jsnvat =  json_decode($service['taxinvest'])->tab : $jsnvat = '';
													  ($jsnvat!='') ? $taxinvest = "Enable" : $taxinvest = "Disable"; 
														
													?>
													<tr>
													<td><table>
													<tr> <td colspan="2"><?php echo $service['crm_company_name']; ?></td>
													<tr><td><?php echo 'Confirmation Statement'; ?></td><td><?php echo $conf_statement; ?></td></tr>
													<tr><td><?php echo 'Accounts'; ?></td><td><?php echo $accounts; ?></td></tr>
													<tr><td><?php echo 'Company Tax Return'; ?></td><td><?php echo $company_tax_return; ?></td></tr>
													<tr><td><?php echo 'Personal Tax Return'; ?></td><td><?php echo $personal_tax_return; ?></td></tr>
													<tr><td><?php echo 'Pay Roll'; ?></td><td><?php echo $payroll; ?></td></tr>
													<tr><td><?php echo 'WorkPlace Pension - AE'; ?></td><td><?php echo $workplace; ?></td></tr>
													<tr><td><?php echo 'Vat'; ?></td><td><?php echo $vat; ?></td></tr>
													<tr><td><?php echo 'CIS - Contractor'; ?></td><td><?php echo $cis; ?></td></tr>
													<tr><td><?php echo 'CIS - Sub Contractor'; ?></td><td><?php echo $cissub; ?></td></tr>
													<tr><td><?php echo 'P11D'; ?></td><td><?php echo $p11d; ?></td></tr>
													<tr><td><?php echo 'Bookkeeping'; ?></td><td><?php echo $bookkeep; ?></td></tr>
													<tr><td><?php echo 'Management Accounts'; ?></td><td><?php echo $management; ?></td></tr>
													<tr><td><?php echo 'Investigation Insurance'; ?></td><td><?php echo $investgate; ?></td></tr>
													<tr><td><?php echo 'Registered Address'; ?></td><td><?php echo $registered; ?></td></tr>
													<tr><td><?php echo 'Tax Advice'; ?></td><td><?php echo $taxadvice; ?></td></tr>
													<tr><td><?php echo 'Tax Investigation'; ?></td><td><?php echo $taxinvest; ?></td></tr>
													</table>
													</td>												
													<?php $i++;} ?>
													</tbody>
													</table>
												</div>
													</div>
														</div>
															</div>
																	</div>
																		</div>
																			</div>
																				</div>
									</div>
														</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
<?php $this->load->view('includes/footer');?>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> 
<script>
   $(document).ready(function() {
       $('#display_service1').DataTable({
         // "scrollX": true
     });
    });
     </script>