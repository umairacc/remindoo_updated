                 <!--start-->
                <!--   <div class="col-sm-12 common_form_section2 forservice">       -->                    
                   <!--   <div class="deadline-crm1 floating_set">
                        <ul class="nav nav-tabs all_user1 md-tabs pull-left">
                           <li class="nav-item">
                              <a class="nav-link" href="javascript:;">Edit Services</a>
                              <div class="slide"></div>
                           </li>
                        </ul> -->
                        <div class="new_confirm-box1">
                        <div class="form-group f-left">


                        <div class="new-bxlayouts">
                          <label>
                            <span><?php echo $service['service_name'];?> </span>


                            <?php if( !$is_workflow ) { ?>
                                 <div class="layout-box02">
                                <span class="task-title-sp price_details euro-position">

                                  <strong class="euro-icon1">
                                    <span class="currency_symbol">
                                      <?php echo (!empty($currency['currency'])?$currency['currency']:'&#163;')?>
                                    </span>
                                  </strong>

                                  <b class="price_tag"> 
                                    <?php echo $service['price']; ?>
                                  </b>
                                </span>

                                <?php if( $_SESSION['permission']['Services']['edit'] == 1){ ?>
                                   <a href="#Edit_Service_Price_popup" data-toggle="modal" class="service-edit-icon" >
                                    <i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i>
                                   </a>
                                <?php } ?>

                                </div>
                            <?php } ?>

                            </label>

                            <div class="form-group f-right">
                              <?php if( $_SESSION['permission']['Services']['create'] == 1)
                                { ?>
                                <button type="button" name="add_task" class="btn btn-primary add_step"  id="<?php echo $service['id']; ?>">
                                  New Step
                                </button>
                                <?php 
                                }
                                ?>
                            </div>

                          <input type="hidden" class="service_list_id " value="<?php echo $service['id'];?>">
                        </div>



                      </div>
                              
                              
                         <div class="sortable edit_after service_edits-new service_cust">                                         
                        <?php
                        $i=1;
                        foreach($records as $rec){ ?>

                          <div class="service-steps to-do-list card-sub checked" id="<?php echo $rec['id']; ?>">
                            <div class="service-bgcover1">
                            <div class="service-titlesteps1"><h4>step <?php echo $i; ?></h4></div>
                            <div class="inside-step">
                              <div class="newservice-design01">                        
                                
                                <div class="new-descri">
                                <span class="task-title-sp"><?php echo ucfirst($rec['title']); ?></span>
                              </div>
                                
                                <div class="check-task">
                                  
                                <span class="f-right">
                                 
                                <p class="action_01">
                                   <a href="javascript:;" class="title-addnewone"><i class="fa fa-angle-down" aria-hidden="true"></i></a>

                                  <?php if( $_SESSION['permission']['Services']['delete'] == 1)
                                      { ?>
                                         <a href="javascript:;" class="title_delete" id="<?php echo $rec['id']; ?>"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true"></i></a>
                                      <?php 
                                      }
                                      if( $_SESSION['permission']['Services']['edit'] == 1)
                                      {
                                      ?>   
                                        <a href="javascript:;" class="title_edit" id="<?php echo $rec['id']; ?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                                      <?php
                                      }
                                      ?>
                               
                                <?php if( $_SESSION['permission']['Services']['create'] == 1)
                                { ?>
                                  <a href="javascript:;" class="step_section_add" id="<?php echo $rec['id']; ?>"><i class="fa fa-plus fa-6" aria-hidden="true"></i></a> 
                                <?php 
                                }
                                ?>
                                 </p>
                                </span>
                                </div>
                              </div>

                              <div class="dropdown_con">
                                <ul>
                                <?php
                                $ii=1;
                                foreach($steps_details as $steps){

                                  if($steps['step_id']== $rec['id'])
                                  {
                                  ?>

                                  <li>
                                    <div class="newservice-design01">                        
                                      <div class="check-task">
                                      <span class="task-title-sp"><?php echo $steps['step_content']; ?></span>
                                      <span class="new-ones1">
                                      <p class="action_01">
                                      <?php if( $_SESSION['permission']['Services']['delete'] == 1)
                                      { ?>
                                        <a href="javascript:;" class="step_section_delete" id="<?php echo $steps['id']; ?>"><i class="fa fa-trash fa-6 deleteUser" aria-hidden="true"></i></a>
                                      <?php 
                                      }
                                      if( $_SESSION['permission']['Services']['edit'] == 1)
                                      {
                                      ?>   
                                        <a href="javascript:;" class="step_section_edit" id="<?php echo $steps['id']; ?>"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>
                                      <?php
                                      }
                                      ?>
                                      </p>
                                      </span>
                                      </div>
                                    </div>
                                  </li>
                                  <?php 
                                    $ii++;
                                    }
                                  }

                                  if($ii==1){ ?>
                                    <div>
                                      <h4>No Sup Steps</h4>
                                    </div>
                                  <?php } ?>
                                </ul>
                              </div>

                            </div>
                          </div>
                          </div>
                      <?php $i++; }
                      if($i==1)
                      {
                        ?>
                        <div>
                          <div class="no-stepsavailable"><h4>No Steps</h4></div>
                        </div>
                        <?php
                      }
                       ?>
                     
                      </div>
                    </div>
<div class="modal fade" id="Edit_Service_Price_popup" role="dialog">
<div class="modal-dialog modal-edit-service">
<form id="change_service_price_form" method="post">
  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">Change Service Price</h4>
    </div>
    <div class="modal-body modal-work-service">


    <div class="euro-position">
                                       <strong class="euro-icon1"> <span><?php echo (!empty($currency['currency'])?$currency['currency']:'&#163;')?></span></strong>
                                        <input type="text" name="service_price" value="<?php echo $service['price']; ?>" placeholder="Enter price">
      <input type="hidden" name="service_id" value="<?php echo $service['id']; ?>">
                                     </div>
     

    </div>
    <div class="modal-footer">
     <button type="submit" name="submit">Submit</button>
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
  </form>
</div>
</div>
                <!--      	  </div> -->
                     	<!-- close -->
                 <!--  	</div> -->
               <!-- </div> -->
               <!-- Page body end -->
