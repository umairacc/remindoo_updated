<?php 
    if( !empty( $_SESSION['is_superAdmin_login'] ) )
    {
      $this->load->view('super_admin/superAdmin_header');
    }
    else
    {
      $this->load->view('includes/header');
    }

    $page = !empty( $_SESSION['show_work_flow'] ) ? $_SESSION['show_work_flow'] : 0;
    
?>
<link href="<?php echo base_url();?>assets/css/jquery.filer.css" type="text/css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/timepicki.css" rel="stylesheet">
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<style type="text/css">
  #workflow_list_table
  {
    display: table !important;
    width: 50% !important;
  }
  .input-group-service {
    display: flex;
    align-items: center;
  }
  .input-group-addon-service {
    font-size: 26px;
    margin-left: 10px;
    color: #1abc9c;
    cursor: pointer;
  }
  .opacity-3 {
    opacity: 0.3;
  }
</style>
<!-- show info  -->
  <div class="modal-alertsuccess alert info_popup" style="display:none;">
    <div class="newupdate_alert">
     <a href="#" class="close">x</a>
     <div class="pop-realted1">
        <div class="position-alert1 info-text">
         </div>
        </div>
     </div>
  </div>
  <!-- end show info  -->
<div class="pcoded-content  newservices-disk1">
   <div class="pcoded-inner-content serviceadnewtsk">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <!--start-->
                  <div class="col-sm-12 common_form_section2 forservice ">                    
                     
                    
                      <div class="deadline-crm1 floating_set newservices-disk">
                        <ul class="nav nav-tabs all_user1 md-tabs pull-left client-top-nav">
                           <li class="nav-item list_services_tab">
                              <a class="nav-link <?php if( !$page ) echo 'active';?>" data-toggle="tab" href="#services_list">Services</a>
                              <div class="slide"></div>
                           </li> 
                           <li class="nav-item list_workflow_tab">
                              <a class="nav-link <?php if( $page ) echo 'active';?>" data-toggle="tab" href="#workfow_list">Workflow</a>
                              <div class="slide"></div>
                           </li> 
                           <li class="nav-item cus_service_tab"  style="display: none;">
                              <a class="nav-link" data-toggle="tab" href="#services_edit">Custom Service</a>
                              <div class="slide"></div>
                           </li>
                           <!-- <li class="nav-item cus_workflow_tab"  style="display: none;">
                              <a class="nav-link" data-toggle="tab" href="#workflow_edit">Custom WorkFlow</a>
                              <div class="slide"></div>
                           </li> -->
                        </ul>
                       
                      </div>
                      <div class="col-sm-12">
                          <div class="add_new_post04 tab-content">                            

                                <!-- <div class="radio radio-inline">
      				                <label><input type="radio" name="servicestatus" class="default_radio" value="1" checked="checked"><i class="helper"></i>Default</label>
      				                </div>
      				                <div class="radio radio-inline">
      				                <label><input type="radio" name="servicestatus" class="custom_radio" value="2"><i class="helper"></i>Custom</label>
      				                </div> -->
      				                <!-- <div class="com-service default tab-pane fade in active" id="services_list">
          				                <div class="services-lists new_confirm-box1">
                                        <?php 
                                            foreach($service as $ser){ 
                                               /* if( $enabled_service[ $ser['id'] ] ==1 )
                                                {*/
                                        ?>
              				                	<div class="checkbox-fade fade-in-primary">                        
              										<label class="check-task">
                  										<span class="task-title-sp"><?php echo $ser['service_name'];?></span>
                  										<span class="f-right hidden-phone">
                  										  <a href="javascript:;" class="cust-opt edit_service" id="<?php echo $ser['id'];?>" >Customize Service</a>
                  										</span>
              										</label>
                                                </div>
                                        <?php 
                                                /*}*/
                                            } 
                                        ?>								
          				                </div> 
                                    </div> -->
                                    <div class="com-service default tab-pane fade <?php if( !$page ) echo 'in active';?>" id="services_list">
                                        <table id="services_list_table" class="table table-responsive service-table-flow" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Service Name</th>
                                                    <?php if($_SESSION['firm_id']!=0){ ?>
                                                    <th>Status</th>
                                                    <th>Estimated Time</th>
                                                  <?php } ?>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php 
                                            
                                            $json = json_decode($services['services'],true);
                                            foreach($service_lists as $ser) {
                                            ?>
                                                <tr>
                                                    <td>
                                                        <a  href="#" class="task-title-sp edit_service" data-id="<?php echo $ser['id'];?>" style="color: #000"><?php echo $ser['service_name'];?></a>
                                                    </td>
                                                    <?php if($_SESSION['firm_id']!=0){ ?>
                                                    <td>
                                                        <input type="checkbox" name="services[]" id="services[<?php echo $ser['id']; ?>]" data-id="<?php echo $ser['id']; ?>" class="services_check" value="1" <?php if($json[$ser['id']] == "1") { echo "checked"; } ?>>
                                                    </td>
                                                    <td>
                                                      <form class="service-estimated-time-form" action="" method="post">
                                                        <div class="input-group-service">
                                                          <input type="hidden" name="service_id" value="<?php echo $ser['id'] ?>">
                                                          <input type="number" class="clr-check-select" name="estimated_time" id="service-estimated-time-<?php echo $ser['id']; ?>"
                                                            value="<?php echo $ser['estimated_time'] ?>" placeholder="Estimated no. of hours" min="0" required> 
                                                          <div class="input-group-addon-service save-task-estimation-time" title="Save">
                                                            <i class="ti-save"></i>   
                                                          </div>
                                                        </div>
                                                      </form>
                                                    </td>
                                                  <?php } ?>
                                                  <!--   <td>
                                                        <span class="f-right hidden-phone">
                                                            <a href="javascript:;" class="cust-opt edit_service" id="<?php echo $ser['id'];?>"> <i class="fa fa-edit" ></i> </a>

                                                        </span>
                                                    </td>  -->
                                                </tr>
                                            <?php 
                                            } 
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="com-service default tab-pane fade <?php if( $page ) echo 'in active';?>" id="workfow_list">
                                       <div class="form-group f-right">
                                          <button type="button"  class="btn btn-primary work-flow-btn" id="work_flow_add_button">New Workflow</button>
                                      </div>
                                      <div>
                                        <table id="workflow_list_table" class="table table-responsive work-flow-table" width="100%">
                                          <thead>
                                            <tr>
                                              <th>Name</th>                                            
                                              <th>Action</th>
                                            </tr>
                                          </thead>
                                          <tbody>

                                          <?php
                                          foreach($work_flow as $work_flow_data)
                                          {
                                          ?><tr>
                                            <td>
                                              <a href="#" class="edit_service work_flow" data-id="<?php echo $work_flow_data['id'];?>">
                                                <?php echo $work_flow_data['service_name'];?>
                                              </a>
                                            </td>
                                            <td>
                                              <p class="action_01">
                                                <a href="javascript:;" class="cust-opt edit_work_flow" data-id="<?php echo $work_flow_data['id'];?>" data-name="<?php echo $work_flow_data['service_name'];?>">
                                                  <i class="icofont icofont-edit" ></i>
                                                </a>
                                                <a href="javascript:void(0)" data-id="<?php echo $work_flow_data['id'];?>" class='Delete_WorkFlow'><i class="icofont icofont-ui-delete" aria-hidden="true" ></i></a>
                                              </p>
                                            </td>
                                            </tr> 
                                          <?php
                                          }
                                          ?>
                                          </tbody>
                                        </table>
                                        <table>
                                          <thead>
                                            <tr></tr>
                                          </thead>
                                          <tbody>
                                            <tr></tr>
                                          </tbody>
                                        </table>
                                      </div>
                                    </div> 
                                  <div id="services_edit" data-section="service" class="services_edit tab-pane fade">
                                  </div>

                                  
                          </div>
                    	</div>
               </div>
            </div>
         </div>        
      </div>
   </div>
</div>
</div>


<div class="modal fade" id="addService" role="dialog">
    <div class="modal-dialog">
    <form name="form1" action="<?php echo base_url(); ?>service/service_add" method="post">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add New Service</h4>
        </div>
        <div class="modal-body">
        <label>Service Name</label>
         <input type="text" name="service" value="">
        </div>
        <div class="modal-footer">
         <button type="submit" name="submit">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      </form>
    </div>
  </div>

  <div class="modal fade" id="AddWorkFlow" role="dialog">
    <div class="modal-dialog modal-dialog-workflow">      
      <div class="modal-content">
        <div class="modal-header modal-header-workflow">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add New Workflow</h4>
        </div>
        <div class="modal-body modal-body-workflow">
        <label>Name</label>
         <input type="text" id="workflow_name" value="" class="clr-check-client">
         <label class="error" style="display: none;color: red" >Please Enter Any Text</label>
         <input type="hidden" id="workflow_id" value="">
        </div>
        <div class="modal-footer modal-footer-workflow">
          <button type="submit" id="submit_workflow">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>



  <div class="modal fade" id="deleteService" role="dialog">
    <div class="modal-dialog">
    <form name="form1" action="<?php echo base_url(); ?>service/service_delete" method="post">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Service</h4>
        </div>
        <div class="modal-body">
        Do you want to delete?
        <!-- <label>Service Name</label>-->
         <input type="hidden" name="id" id="service_id" value=""> 
        </div>
        <div class="modal-footer">
         <button type="submit" name="submit">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">no</button>
        </div>
      </div>
      </form>
    </div>
  </div>



    <div class="modal fade" id="delete_Steps" role="dialog">
    <div class="modal-dialog">
   <!--  <form name="form1" action="<?php //echo base_url(); ?>service/steps_delete" method="post"> -->
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Steps</h4>
        </div>
        <div class="modal-body">
        Do you want to delete?
        <!-- <label>Service Name</label>-->
         <input type="hidden" name="id" id="step_details_id" value=""> 
        </div>
        <div class="modal-footer">
         <button type="button" name="button" id="steps_details_delete">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">no</button>
        </div>
      </div>
     <!--  </form> -->
    </div>
  </div>



<div class="modal fade" id="addStep" role="dialog">
    <div class="modal-dialog modal-edit-service">
   <!--  <form name="form1" action="<?php //echo base_url(); ?>service/service_add" method="post"> -->
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Step Title</h4>
        </div>
        <div class="modal-body">
        <label>Step Title</label>
        <input type="hidden" name="service_id" id="services_id" value="">
         <input type="text" name="service_title" id="service_title" value="">
         <label class="error" style="display: none;color: red" >Please Enter Any Text</label>
        </div>
        <div class="modal-footer">
         <button type="button" id="steps_add">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
     <!--  </form> -->
    </div>
  </div>


  <div class="modal fade" id="EditStep" role="dialog">
    <div class="modal-dialog modal-edit-service">
   <!--  <form name="form1" action="<?php //echo base_url(); ?>service/service_add" method="post"> -->
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Step Title</h4>
        </div>
        <div class="modal-body">
        <label>Step Title</label>
        <input type="hidden" name="service_id" id="step_id" value="">
         <input type="text" name="service_title" id="title" value="">
         <label class="error" style="display: none;color: red" >Please Enter Any Text</label>

        </div>
        <div class="modal-footer">
         <button type="button" id="steps_edit">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
     <!--  </form> -->
    </div>
  </div> 


  <div class="modal fade" id="EditStepDetails" role="dialog">
    <div class="modal-dialog modal-edit-service">
   <!--  <form name="form1" action="<?php //echo base_url(); ?>service/service_add" method="post"> -->
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Step Title</h4>
        </div>
        <div class="modal-body">
        <label>Step Title</label>
        <input type="hidden" name="edit_id" id="edit_id" value="">
         <input type="text" name="step_content" id="step_content" value="">
         <label class="error" style="display: none;color: red" >Please Enter Any Text</label>

        </div>
        <div class="modal-footer">
         <button type="button" id="steps_details_edit">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
     <!--  </form> -->
    </div>
  </div> 


  


  <div class="modal fade" id="addContents" role="dialog">
    <div class="modal-dialog modal-edit-service">
   <!--  <form name="form1" action="<?php //echo base_url(); ?>service/service_add" method="post"> -->
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add New Steps</h4>
        </div>
        <div class="modal-body">
        <label>Steps</label>
        <input type="hidden" name="stepid" id="stepid" value="">
         <input type="text" name="steps_content" id="steps_content" value="">
         <label class="error" style="display: none;color: red" >Please Enter Any Text</label>

        </div>
        <div class="modal-footer">
         <button type="button" id="add_steps_section">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
     <!--  </form> -->
    </div>
  </div>

<?php 
  if( !empty( $_SESSION['is_superAdmin_login'] ) )
    {
      $this->load->view('super_admin/superAdmin_footer');
    }
    else
    {      
      $this->load->view('includes/session_timeout');
      $this->load->view('includes/footer');
    }
?>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/Sortable.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/sortable-custom.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

        var elem = Array.prototype.slice.call(document.querySelectorAll('.services_check'));
   
        elem.forEach(function(html) {
            var switchery = new Switchery(html, {
               color        : '#22b14c',
               jackColor    : '#fff',
               size         : 'small'
            });
        });

        $(document).on('change', '.services_check', function() {
            var serv = $(this).attr('data-id');
            var serv_id;
            if($(this).prop('checked') == true)
            {
                serv_id = parseInt(1);
            }
            else
            {
                serv_id = parseInt(0);
            }
            // alert(serv_id);
            $.ajax({
                url : "<?php echo base_url('service/update_service_check'); ?>",
                type : "POST",
                data : { "serv" : serv, "serv_id" : serv_id },
                beforeSend : function() {   $(".LoadingImage").show();  },
                success : function(data) {
                    $(".LoadingImage").hide();
                },
                error : function(error) {
                    $(".LoadingImage").hide();
                }
            });
        });
    
    $(document).on("click",".title-addnewone",function() {
         $(this).closest('.inside-step').find('.dropdown_con').slideToggle(300);
    });
    
    $('.close').click(function(){$(this).closest('div.alert').hide();});

    $('.list_services_tab, .list_workflow_tab').click(function(){$('.cus_service_tab').hide();});

    $(document).on("click",".add_step",function(){ 
                        $("#addStep").find('#service_title').val('');                       
                        $("#addStep").find('#services_id').val( $(this).attr('id') );
                        $("#addStep").find('label.error').hide();
                        $("#addStep").modal('show');
                      });
     $(document).on("click",".title_edit",function(){
                         var id=$(this).attr('id');                      
                           $.ajax({
                              url: '<?php echo base_url(); ?>Service/steps_details',
                              type : 'POST',
                              data : {'id':id},
                              dataType:'json',                    
                              beforeSend: function() {
                              $(".LoadingImage").show();
                              },
                              success: function(data) {
                                  title = data['title'];
                                  id = data['id'];
                                  
                                  $("#EditStep").find('#step_id').val(id);
                                  $("#EditStep").find('#title').val(title);
                                   $("#EditStep").find('label.error').hide();
                                  $("#EditStep").modal('show');
                                  $(".LoadingImage").hide();
                              }
                        });                      
                      });
     
    $(document).on("click",".title_delete",function(){
                       var id=$(this).attr('id');
                       var action = function(){
                                                $.ajax({
                                                  url: '<?php echo base_url(); ?>Service/edit_delete_steps',
                                                  type : 'POST',
                                                  data : {'id':id ,'data': {'status':1} ,'is_workflow':is_workflow()},
                                                  dataType:'json',                    
                                                    beforeSend: function() {
                                                      $(".LoadingImage").show();
                                                    },
                                                    success: function(data) {
                                                      $('#Confirmation_popup').modal('hide');
                                                      show_info(data.result,'Steps deleted Successfuly');
                                                      $(".services_edit").html(data.content);
                                                      trigger_sortable();
                                                      $(".LoadingImage").hide();
                                                    }
                                                  });
                                              };         
   Conf_Confirm_Popup({'OkButton':{'Handler':action},'Info':'Do you want to delete this step ?'});  
    $('#Confirmation_popup').modal('show');
                      });

    $(document).on("click",".Delete_WorkFlow",function(){
                       var id=$(this).attr('data-id');
                       var data = {'status':1};
                       var action = function(){
                                                $.ajax({
                                                  url: '<?php echo base_url(); ?>/Service/add_update_workflow',
                                                  type : 'POST',
                                                  data : {'id':id ,'data':data },
                                                  dataType:'json',                    
                                                    beforeSend: function() {
                                                      $(".LoadingImage").show();
                                                    },
                                                    success: function(data) {
                                                      $('#Confirmation_popup').modal('hide');
                                                      show_info(data.result,'WorkFlow deleted Successfuly');
                                                      location.reload();
                                                      $(".LoadingImage").hide();
                                                    }
                                                  });
                                              };         
   Conf_Confirm_Popup({'OkButton':{'Handler':action},'Info':'Do you want to delete this workFlow ?'});  
    $('#Confirmation_popup').modal('show');
                      });

     $(document).on("click",".step_section_add",function(){
                         // alert($(this).attr('id'));
                         $("#addContents").find('#steps_content').val('');
                          $("#addContents").find('#stepid').val($(this).attr('id'));
                          $("#addContents").find('label.error').hide();
                          $("#addContents").modal('show');
                      });
      

      $(document).on("click",".step_section_edit",function(){
                   var id=$(this).attr('id');
                      $.ajax({
                        url: '<?php echo base_url(); ?>Service/steps_secion_details',
                        type : 'POST',
                        data : {'id':id},
                        dataType:'json',                    
                        beforeSend: function() {
                        $(".LoadingImage").show();
                        },
                        success: function(data) {
                            id=data['id'];  
                            title=data['step_name'];
                            $("#EditStepDetails").find('#edit_id').val(id);
                            $("#EditStepDetails").find('#step_content').val(title);
                            $("#EditStepDetails").find('label.error').hide();
                            $("#EditStepDetails").modal('show');
                            $(".LoadingImage").hide();
                        }
                  });
                });

      $(document).on("click",".step_section_delete",function(){
                        var id   = $(this).attr('id');
                        var data = {'status' : '1'};
          var action = function(){
          $.ajax({
                url: '<?php echo base_url(); ?>/Service/steps_section_details_update_delete',
                type : 'POST',
                data : {'id':id ,'data':data , 'is_workflow':is_workflow()}, 
                dataType:'json',                                       
                  beforeSend: function() {
                    $(".LoadingImage").show();
                  },
                  success: function(data) {
                    $('#Confirmation_popup').modal('hide');
                    show_info(data.result,'Steps Detail deleted Successfuly');
                    $(".services_edit").html(data.content);
                    trigger_sortable();
                    $(".LoadingImage").hide();
                  }
                });
};     
   Conf_Confirm_Popup({'OkButton':{'Handler':action},'Info':'Do you want to delete this step detail ?'});  
    $('#Confirmation_popup').modal('show');
                      });
  /*   $("input[name$='servicestatus']").click(function() {
          var test = $(this).val();
          if(test=='1')
          {
            $(".default").show();
            $(".custom").hide();
          }
          else
          {
            $(".custom").show();
            $(".default").hide();
          }
      }); 
    });
 $(".customize_service").click(function(){
          var id=$(this).attr('id');
             $.ajax({
                  url: '<?php echo base_url(); ?>/Service/custom_change',
                  type : 'POST',
                  data : {'id':id },                    
                    beforeSend: function() {
                      $(".LoadingImage").show();
                    },
                    success: function(data) {               
                    $(".LoadingImage").hide();
                    var json = JSON.parse(data); 
                    content=json['content'];  
                    $(".custom").html('');
                    $(".custom").append(content);
                    $(".default_radio").prop('checked',false);
                    $(".custom_radio").prop('checked',true);
                    $(".default").hide();
                    $(".custom").show();
                  }
            });
    });*/

$(document).on("click",".edit_service",function(){
      var id=$(this).attr('data-id');  
      var is_workflow = ($(this).hasClass('work_flow')?1:0);
       $.ajax({
                url: '<?php echo base_url(); ?>Service/service_details',
                type : 'POST',
                data : {'id':id,'is_workflow':is_workflow},                    
                beforeSend: function()
                {
                  $(".LoadingImage").show();
                },
                success: function(data)
                {
                    /*if( is_workflow )
                    {
                      $(".cus_workflow_tab").show();
                      $(".cus_workflow_tab a").trigger('click');
                      $(".workflow_edit").html(data);
                    }
                    else
                    {
                    }*/
                      $(".services_edit").html(data);
                      if( is_workflow )
                      {
                        $(".cus_service_tab a").text('WorkFlow Details').trigger('click');
                        $("#services_edit").attr('data-section','work_flow');
                      }
                      else
                      {
                        $(".cus_service_tab a").text('Service Details').trigger('click');
                        $("#services_edit").attr('data-section','service');
                        attach_serviceCharge_validation();
                      }

                      $(".cus_service_tab").show();
                      trigger_sortable();
                      $(".LoadingImage").hide();
                }
          });
    });
      
  


      function delete_service(del){
      var id=$(del).attr('id');
      $("#deleteService").find('#service_id').val(id);
      $("#deleteService").modal('show');   
     }


     $("#steps_add").click(function(){
      var id=$("#services_id").val();
      var title=$("#service_title").val();

      if( !check_empty( $("#service_title") ) )
      {
        return;
      }
          $.ajax({
                url: '<?php echo base_url(); ?>Service/add_title',
                type : 'POST',
                data : {'id':id,'title':title,'is_workflow':is_workflow()},
                dataType:'json',                    
                  beforeSend: function() {
                    $(".LoadingImage").show();
                  },
                  success: function(data) {
                    $("#addStep").modal('hide');
                    show_info(data.result,'Steps Addted Successfully');
                    $(".services_edit").html(data.content);
                    trigger_sortable();
                    $(".LoadingImage").hide();

                  }
            });
     });
 
$("#steps_edit").click(function(){
  var step_id=$("#step_id").val();
  var data ={ 'title' : $("#title").val() };
    if( !check_empty( $("#title") ) )
      {
        return;
      }
     $.ajax({
                url: '<?php echo base_url(); ?>/Service/edit_delete_steps',
                type : 'POST',
                data : {'id':step_id,'data':data,'is_workflow':is_workflow()}, 
                dataType:'json',                   
                  beforeSend: function()
                  {
                    $(".LoadingImage").show();
                  },
                  success: function(data)
                  {
                       $("#EditStep").modal('hide');
                       show_info(data.result,'Steps Updated Successfully');
                       $(".services_edit").html(data.content);
                       trigger_sortable();
                       $(".LoadingImage").hide();                       
  
                  }

               });
       });



$("#add_steps_section").click(function(){
  var step_id=$("#stepid").val();
  var steps_title=$("#steps_content").val();
  if( !check_empty($("#steps_content")) )
  {
    return;
  }
     $.ajax({
          url: '<?php echo base_url(); ?>Service/steps_secion_add',
          type : 'POST',
          data : {'id':step_id,'step_content':steps_title,'is_workflow':is_workflow()},
          dataType:'json',                    
          beforeSend: function() {
          $(".LoadingImage").show();
          },
          success: function(data) {
            $("#addContents").modal('hide');
             show_info(data.result,'Steps Detail Addted Successfully');
             $(".services_edit").html(data.content);
             trigger_sortable();
             $(".LoadingImage").hide();
          }
    });
});

$("#work_flow_add_button").click(function(){  
  $("#AddWorkFlow").find("h4.modal-title").text("Add New WorkFlow");
  $("#workflow_id").val('');
  $("#workflow_name").val('');
  $("#AddWorkFlow").modal('show');
});
$(".edit_work_flow").click(function(){  
  $("#AddWorkFlow").find("h4.modal-title").text("Edit WorkFlow");
  $("#workflow_id").val( $(this).data('id') );
  $("#workflow_name").val( $(this).data('name') );
  $("#AddWorkFlow").modal('show');
});


$("#submit_workflow").click(function(){

  var id=$("#workflow_id").val();
  var data ={'service_name' : $("#workflow_name").val() };
  if( !check_empty($("#workflow_name")) )
  {
    return;
  }
  $.ajax({
          url: '<?php echo base_url(); ?>Service/add_update_workflow',
          type : 'POST',
          data : {'id':id,'data':data },
          dataType:'json',                    
          beforeSend: function() {
          $(".LoadingImage").show();
          },
          success: function(data) {
            $("#AddWorkFlow").modal('hide');
             $(".LoadingImage").hide();
             show_info(data.result,'WorkFlow Save Successfully');
             location.reload();
          }
    });
});


$("#steps_details_edit").click(function(){
var id           = $('#edit_id').val();
var step_content = $('#step_content').val();
if(!check_empty($('#step_content')))
{
  return;
}
$.ajax({
      url: '<?php echo base_url(); ?>Service/steps_section_details_update_delete',
      type : 'POST',
      data : {'id':id,'data':{'step_content':step_content},'is_workflow':is_workflow()}, 
      dataType:'json',                   
      beforeSend: function() {
      $(".LoadingImage").show();
      },
      success: function(data) {
              $("#EditStepDetails").modal('hide');
             show_info(data.result,'Steps Detail Updated Successfully');
             $(".services_edit").html(data.content);
             trigger_sortable();
             $(".LoadingImage").hide();
      }
});

});



/*$(document).on('click', '.defalut_service', function(){
 // $("").click(function(){
    var id=$(this).attr('id');
   // alert(id);
           $.ajax({
                url: '<?php echo base_url(); ?>/Service/bydefault_change',
                type : 'POST',
                data : {'id':id },                    
                  beforeSend: function() {
                    $(".LoadingImage").show();
                  },
                  success: function(data) {
              $(".LoadingImage").hide();
              location.reload();      
              }
                });
  });*/

 });



    function is_workflow()
      {
        if( $("#services_edit").attr('data-section') == 'work_flow' )
        {
          return 1;
        }
        else
        {
          return 0;
        }
      }

function trigger_sortable()
{
  
    $(".sortable" ).sortable(
   {
    update: function(event, ui) {       
       
       var idsInOrder = [];
       $("div.card-sub").each(function() { 
              idsInOrder.push( $(this).attr('id') ); 
        }); 
       var s_id = $('.services_edit').find('.service_list_id').val();
        
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>/Service/update_orderNumber",
            data:  { 'datas' : idsInOrder ,'id':s_id ,'is_workflow':is_workflow()}, 
            dataType:'json',
            beforeSend: function() {
               $(".LoadingImage").show();
            },
            success: function(data){
              //$(".edit_service").trigger('click');
              
                $(".services_edit").html(data.content);
                trigger_sortable();
              
               $(".LoadingImage").hide();               
              }
            });
    }
  });   

}
function show_info(result,content)
{
  if(result)
  {
    $('.info_popup .info-text').text(content+'!...');
  }
  else
  {
    $('.info_popup .info-text').text('Some Thing Oops Try Again!...');
  }
  $('.info_popup').show();

  setTimeout(function(){$('.info_popup').hide();},1500);
}
function check_empty(obj)
{
  if(obj.val()=='')
  {
    obj.closest('.modal-body').find('label.error').show();
    return false;
  }
  else
  {
    return true;
  }
}
function attach_serviceCharge_validation()
{
  console.log( $('#change_service_price_form').length+"edit form" );

  $('#change_service_price_form').validate(
  {
    rules:
    {
      service_price:
      {
        required:true,
        number:true,
        min:1
      }
    },
    messages:
    {
      service_price: { number: "Enter Valid Price" }
    },
    submitHandler: function(form)
    {
      var formData = new FormData( $('#change_service_price_form')[0] );
      $.ajax({
               url: '<?php echo base_url();?>service/UpdateService_price/',
               dataType : 'json',
               type : 'POST',
               data : formData,
               contentType : false,
               processData : false,
               beforeSend:function(){ $(".LoadingImage").show(); },
               success:function(data)
               {
                $("#Edit_Service_Price_popup").modal('hide');
                show_info(data.result,'Service Price Updated Successfully');
                if( data.result )
                {
                  var price = $("#Edit_Service_Price_popup").find("input[name='service_price']").val();
                  $(".price_details .price_tag").text(price);
                }

                $(".LoadingImage").hide();
               }
            });   
    }
    });
}

$(function () {
  $(document).on('click', '.save-task-estimation-time', function () {
    $(this).closest('form').submit();
  });

  $('.service-estimated-time-form').submit(function (e) { 
    e.preventDefault();
    var _this = $(this);
    var icon = _this.find('.input-group-addon i');
    var data = _this.serialize();
    $.ajax({
      type: "post",
      url: "<?php echo base_url('service/update_estimated_time') ?>",
      data: data,
      beforeSend: function() {
        icon.addClass('.opacity-3');
      },
      success: function (response) {
        var obj = JSON.parse(response);
        swal('', obj.message, obj.status);
      },
      complete: function() {
        icon.removeClass('.opacity-3');
      },
      error: function () {
        swal('', 'Server error occurred!', 'error');
      }
    });
  });
});

</script>

<script>
  $(document).ready(function(){
    $(".clr-check-select").keyup(function(){
      var clr = $(this).val();
			if(clr.length >= 3){
				$(this).addClass("clr-check-select-client-outline");
			}else{
				$(this).removeClass("clr-check-select-client-outline");
			}
  });
    $(".clr-check-select").each(function(i){
      if($(this).val() !== ""){
          $(this).addClass("clr-check-select-client-outline");
      } 
    });
})
  $(".clr-check-client").keyup(function(){
      var clr = $(this).val();
			if(clr.length >= 3){
				$(this).addClass("clr-check-client-outline");
			}else{
				$(this).removeClass("clr-check-client-outline");
			}
  });
    $(".clr-check-client").each(function(i){
      if($(this).val() !== ""){
          $(this).addClass("clr-check-client-outline");
      } 
    });
    
</script>

</body>
</html>