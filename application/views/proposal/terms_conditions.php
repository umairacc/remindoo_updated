<?php $this->load->view('includes/header');?>

<div class="pcoded-content card-removes">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card">
                        <!-- admin start-->
                        <div class="client_section user-dashboard-section1 floating_set">
                           <!-- all-clients -->
                           <div class="all_user-section floating_set">
                              <div class="deadline-crm1 floating_set f4reports">
                                 <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                                    <li class="nav-item">
                                       <a class="nav-link active" data-toggle="tab" data-target="#reports-tab" href="">Terms and conditions</a>
                                       <div class="slide"></div>
                                       </li>
                                   </ul>
                                  </div>
                           	<?php
                          

                           		//echo "hai".$content['id'];
                           	if($content['id']==''){ ?>
                                 <form action="<?php echo base_url(); ?>/proposal/terms_insert" method="post" name="form1">
                             <?php }else { ?>
                              <form action="<?php echo base_url(); ?>/proposal/terms_update" method="post" name="form1">
                              <input type="hidden" name="id" id="terms_id" value="<?php echo $content['id']; ?>">
                             <?php }  ?>
                            <div class="all_user-section2 floating_set fr-reports">
                            	 <textarea rows="5" type="text" class="form-control" name="description" id="description" placeholder="Write something"><?php echo $content['terms_content']; ?></textarea>

                            </div>
                            <div class="f-right newstyelcls">
                              <?php 
                            if( $_SESSION['permission']['Term_and_Condition']['create'] == 1 && $_SESSION['permission']['Term_and_Condition']['edit'] == 1 )
                            {
                            ?>
                              <button type="submit" name="submit" class="btn btn-primary" id="submit">Save</button>
                            <?php
                            }
                            ?>
                            </div>
                            </form>
                            </div>

                         </div>


                      </div>

                   </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>


<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>

<script type="text/javascript" src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>
<script type="text/javascript">
$( document ).ready(function() {
          CKEDITOR.editorConfig = function (config) {
          config.language = 'es';
          config.uiColor = '#fff';
          config.height = 300;
          config.toolbarCanCollapse = true;
          config.toolbarLocation = 'bottom';
          };
          
           CKEDITOR.replace( 'description'); 
         
        } );</script>