<?php $this->load->view('includes/header');
   $succ = $this->session->flashdata('success');
   $error = $this->session->flashdata('error');
   ?>
    <style type="text/css">
   .to-do-list.sortable-moves {
     padding-left: 0;
   }
   </style>
<!-- management block -->
<div class="pcoded-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <div class="deadline-crm1 floating_set single-txt12">
               <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                  <li class="nav-item">
                     dashboard
                     <div class="slide"></div>
                  </li>
               </ul>
            </div>
            <div class="card desk-dashboard">
            <div class="accident-toggle"><h2>Tasks</h2></div>
               <div class="revenue-task row">
                  <!-- card1 start -->
                  <div class="col-md-6 col-xl-3">
                     <div class="card widget-card-1">
                        <div class="card-block-small">
                        <span class="text-c-green f-w-600">Tasks</span>
                           <span class="bg-c-green ">This Month</span>
                           
                           <h4><?php echo $tasks['task_count']; ?> <span class="tsk-nm">Total Tasks</span></h4>
                        </div>
                     </div>
                  </div>
                  <!-- card1 end -->
                  <!-- card1 start -->
                  <div class="col-md-6 col-xl-3">
                     <div class="card widget-card-1">
                        <div class="card-block-small">
                        <span class="text-c-pink f-w-600">Closed Tasks</span>
                           <span class="bg-c-pink ">This Month</span>
                           
                           <h4><?php echo $closed_tasks['task_count']; ?><span class="tsk-nm">Closed Tasks</span></h4>
                        </div>
                     </div>
                  </div>
                  <!-- card1 end -->
                  <!-- card1 start -->
                  <div class="col-md-6 col-xl-3">
                     <div class="card widget-card-1">
                        <div class="card-block-small">
                        <span class="text-c-yellow f-w-600">Pending Tasks</span>
                           <span class="bg-c-yellow ">This Month</span>
                           
                           <h4><?php echo $pending_tasks['task_count']; ?><span class="tsk-nm">Total Pending Tasks</span></h4>
                        </div>
                     </div>
                  </div>
                  <!-- card1 end -->
                  <!-- card1 start -->
                  <div class="col-md-6 col-xl-3">
                     <div class="card widget-card-1">
                        <div class="card-block-small">
                        <span class="text-c-blue f-w-600">Cancelled Tasks</span>
                           <span class="bg-c-blue ">This Month</span>
                           
                           <h4><?php echo $cancelled_tasks['task_count']; ?><span class="tsk-nm">Total Cancelled Tasks</span></h4>
                        </div>
                     </div>
                  </div>
                  <!-- card1 end -->
               </div>
               <!-- due details section -->

            <div class="job-card">
              <div class="card">
               <div class="card-header">
                  <div class="media">
                     <div class="media-body media-middle">
                        <div class="company-name">
                           <p>VAT</p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="card-block">
                  <ul class="text-muted">
                    <?php
                  foreach($oneyear as $date){ 
                    if($date['crm_vat_due_date']!='')?>
                    <li><?php echo $date['crm_company_name']; ?>-<?php echo (strtotime($date['crm_vat_due_date']) !='' ?  date('Y-m-d', strtotime($date['crm_vat_due_date'])): ''); ?></li>
                    <?php } ?>                    
                  </ul>
                </div>
              </div>
              <div class="card">
               <div class="card-header">
                  <div class="media">
                     <div class="media-body media-middle">
                        <div class="company-name">
                           <p>Accounts</p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="card-block">
                  <ul class="text-muted">
                             <?php
                  foreach($oneyear as $date){ 
                    if($date['crm_ch_accounts_next_due']!='')?>
                    <li><?php echo $date['crm_company_name']; ?>-<?php echo $date['crm_ch_accounts_next_due']; ?></li>
                    <?php } ?> 
                    </ul>
                </div>
              </div>
              <div class="card">
               <div class="card-header">
                  <div class="media">
                     <div class="media-body media-middle">
                        <div class="company-name">
                           <p>Company Tax Returns</p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="card-block">
                  <ul class="text-muted">
                            <?php
                  foreach($oneyear as $date){ 
                    if($date['crm_accounts_tax_date_hmrc']!='')?>
                    <li><?php echo $date['crm_company_name']; ?>-<?php echo $date['crm_accounts_tax_date_hmrc']; ?></li>
                    <?php } ?> 
                  </ul>
                </div>
              </div>
              <div class="card">
               <div class="card-header">
                  <div class="media">
                     <div class="media-body media-middle">
                        <div class="company-name">
                           <p>Personal Tax Returns</p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="card-block">
                  <ul class="text-muted">
                             <?php
                  foreach($oneyear as $date){ 
                    if($date['crm_personal_due_date_return']!='')?>
                    <li><?php echo $date['crm_company_name']; ?>-<?php echo $date['crm_personal_due_date_return']; ?></li>
                    <?php } ?> 
                          </ul>
                </div>
              </div>
              <div class="card">
               <div class="card-header">
                  <div class="media">
                     <div class="media-body media-middle">
                        <div class="company-name">
                           <p>Payroll</p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="card-block">
                  <ul class="text-muted">
                            <?php
                  foreach($oneyear as $date){ 
                    if($date['crm_rti_deadline']!='')?>
                    <li><?php echo $date['crm_company_name']; ?>-<?php echo $date['crm_rti_deadline']; ?></li>
                    <?php } ?> 
                          </ul>
                </div>
              </div>
              <div class="card">
               <div class="card-header">
                  <div class="media">
                     <div class="media-body media-middle">
                        <div class="company-name">
                           <p>Confimation Statements</p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="card-block">
                  <ul class="text-muted">

                   <?php
                  foreach($oneyear as $date){ 
                    if($date['crm_confirmation_statement_due_date']!='')?>
                    <li><?php echo $date['crm_company_name']; ?>-<?php echo $date['crm_confirmation_statement_due_date']; ?></li>
                    <?php } ?> 
                            
                          </ul>
                </div>
              </div>

          <!-- due details section -->
            </div>
            </div>
            <div class="card desk-dashboard">
            <div class="accident-toggle"><h2>Invoices</h2></div>
            <div class="revenue-task row">
                  <!-- card1 start -->
                  <div class="col-md-6 col-xl-3">
                     <div class="card widget-card-1">
                        <div class="card-block-small">
                        <span class="text-c-green f-w-600">Invoices</span>
                           <span class="bg-c-green ">This Month</span>
                           
                           <h4><?php echo $invoice['invoice_count']; ?><span class="tsk-nm">Total Invoices</span></h4>
                        </div>
                     </div>
                  </div>
                  <!-- card1 end -->
                  <!-- card1 start -->
                  <div class="col-md-6 col-xl-3">
                     <div class="card widget-card-1">
                        <div class="card-block-small">
                        <span class="text-c-pink f-w-600">Paid Invoices</span>
                           <span class="bg-c-pink ">This Month</span>
                           
                           <h4><?php echo $approved_invoice['invoice_count']; ?><span class="tsk-nm">Approved Invoices</span></h4>
                        </div>
                     </div>
                  </div>
                  <!-- card1 end -->
                  <!-- card1 start -->
                  <div class="col-md-6 col-xl-3">
                     <div class="card widget-card-1">
                        <div class="card-block-small">
                        <span class="text-c-yellow f-w-600">Unpaid Invoices</span>
                           <span class="bg-c-yellow ">This Month</span>
                           
                           <h4><?php echo $cancel_invoice['invoice_count']; ?><span class="tsk-nm">Declined Invoices</span></h4>
                        </div>
                     </div>
                  </div>
                  <!-- card1 end -->
                  <!-- card1 start -->
                  <div class="col-md-6 col-xl-3">
                     <div class="card widget-card-1">
                        <div class="card-block-small">
                        <span class="text-c-blue f-w-600">Cancelled Invoices</span>
                           <span class="bg-c-blue ">This Month</span>                           
                           <h4><?php echo $expired_invoice['invoice_count']; ?><span class="tsk-nm">Expired Invoices</span></h4>
                        </div>
                     </div>
                  </div>
                  <!-- card1 end -->
               </div>

               <!-- due details section -->
            <div class="job-card count-details">
              <div class="card">
               <div class="card-header">
                  <div class="media">
                     <div class="media-body media-middle">
                        <div class="company-name">
                           <p>Number of Client</p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="card-block">
                 <p class="text-muted"><?php echo $client['client_count']; ?></p>
                </div>
              </div>
              <div class="card">
               <div class="card-header">
                  <div class="media">
                     <div class="media-body media-middle">
                        <div class="company-name">
                           <p>Number of Active Client</p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="card-block">
                  <p class="text-muted"><?php echo $active_client['client_count']; ?></p>
                </div>
              </div>
              <div class="card">
               <div class="card-header">
                  <div class="media">
                     <div class="media-body media-middle">
                        <div class="company-name">
                           <p>Non Active Client</p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="card-block">
                  <p class="text-muted"><?php echo $nonactive_client['client_count']; ?></p>
                </div>
              </div>
              <div class="card">
               <div class="card-header">
                  <div class="media">
                     <div class="media-body media-middle">
                        <div class="company-name">
                           <p>Archive Clients</p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="card-block">
                  <p class="text-muted"><?php echo $archive_client['client_count']; ?></p>
                </div>
              </div>
              <div class="card">
               <div class="card-header">
                  <div class="media">
                     <div class="media-body media-middle">
                        <div class="company-name">
                           <p>New added This month</p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="card-block">
                  <p class="text-muted"><?php echo $new_client['client_count']; ?></p>
                </div>
              </div>

          <!-- due details section -->

         </div>
      </div>
      <div class="cards12 col-xs-12">
      

   <div class="col-xl-6">
      <!-- To Do List card start -->
      <div class="card">
         <div class="card-header">
            <h5>My Tasks
            </h5>
            <div class="card-header-right"> <i class="icofont icofont-spinner-alt-5"></i> </div>
         </div>
         <div class="card-block">
          <h3>Task Name <span class="f-right">Start Date/End Date</span></h3>
            <div class="new-task">
            <?php 
            foreach($task_list as $task){

              if(strtotime($task['start_date']) < time()){
$date = date('y-m-j&\nb\sp;g:i:s');

$start_date=date('Y-m-d',strtotime($task['start_date']));
$end_date=$task['end_date'];
//echo $start_date;
//echo $end_date;

$date1 = strtotime(''.$start_date.' 00:00:00');
$date2 = strtotime(''.$end_date.' 00:00:00');
$today = time();

$dateDiff = $date2 - $date1;
// echo $dateDiff;
// echo $today;
// echo $date1;
$dateDiffForToday = $today - $date1;
//echo $dateDiffForToday;

$percentage = $dateDiffForToday / $dateDiff * 100;
$percentageRounded = round($percentage);

echo $percentageRounded . '%';
}else{

  $percentageRounded='0';
  echo $percentageRounded . '%';
}
            ?>
                <div class="to-do-list">
                  <div class="checkbox-fade fade-in-primary">
                     <label class="check-task">
                    <!--  <input type="checkbox" value="">
                     <span class="cr">
                     <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                     </span> -->
                     <span><?php echo $task['subject']; ?></span> 
                     <span class="bg-c-pink">Over Due</span>
                     <div class="progress">
                        <div class="progress-bar bg-c-blue" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $percentageRounded.'%'; ?>">
                        </div>
                      </div>
                     </label>
                  </div>
                  <span class="f-right end-dateop"><?php echo $task['start_date']; ?>/<?php echo date('d-m-Y',strtotime($task['end_date'])); ?></span>
                  <!-- <div class="f-right">
                     <a href="#" class="delete_todolist"><i class="icofont icofont-ui-delete"></i></a>
                  </div> -->
               </div>
          <?php } ?>            
               
              
            </div>
         </div>
      </div>
      <!-- To Do List card end -->
   </div>
   <div class="col-xl-6">
      <!-- To Do Card List card start -->
      <div class="card">
         <div class="card-header">
            <h5>My To Do items <span class="f-right">
              <ul class="nav nav-tabs  tabs" role="tablist">
               <li class="nav-item">
                  <a class="nav-link active" data-toggle="tab" href="#home1" role="tab">New Td Do</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#profile1" role="tab">View All</a>
               </li>
            </ul>
            </span></h5>
         </div>
         <div class="card-block widnewtodo">
        
            <div class="tab-content tabs card-block">
             <div class="tab-pane active" id="home1" role="tabpanel">
              <button type="button" class="btn btn-info btn-sm f-right" data-toggle="modal" data-target="#myTodo">New Todo's</button>
               <div class="new-task" id="draggablePanelList">
                <div class="no-todo">
                  <h5 class="bg-c-green12"> <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Latest to do's</h5>
                   <div class="sortable" id="5">
              <?php
                foreach($todo_list as $todo){ ?>
               <div class="to-do-list card-sub" id="<?php echo $todo['id']; ?>">
                  <div class="checkbox-fade fade-in-primary">
                     <label class="check-task">
                     <input type="checkbox" value="" id="<?php echo $todo['id']; ?>" onclick="todo_status(this)">
                     <span class="cr">
                     <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                     </span>
                     <span><?php echo $todo['todo_name']; ?></span> 
                     </label>
                  </div>
                  <div class="f-right">
                    <a href="javascript:;" data-toggle="modal" data-target="#EditTodo_<?php echo $todo['id']; ?>" class=""><i class="icofont icofont-ui-edit"></i></a>
                     <a href="javascript:;" data-toggle="modal" data-target="#DeleteTodo_<?php echo $todo['id']; ?>" class=""><i class="icofont icofont-ui-delete"></i></a>
                  </div>
               </div>
               <?php } ?>
               </div>
                 
                </div>
                <h5 class="bg-c-green12"> <i class="fa fa-check"></i> Latest finished to do's</h5>
                <div class="completed_todos">                
                 <?php
                foreach($todo_completedlist as $todo){ ?>
                <div class="to-do-list card-sub" id="1">
                  <div class="checkbox-fade fade-in-primary">
                     <label class="check-task done-task">
                     <input type="checkbox" value="" checked="checked" id="<?php echo $todo['id']; ?>" onclick="todo_status(this)" >
                     <span class="cr">
                     <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                     </span>
                     <span><?php echo $todo['todo_name']; ?></span> 
                     </label>
                  </div>
                  <div class="f-right">
                    <a href="javascript:;" data-toggle="modal" data-target="#EditTodo_<?php echo $todo['id']; ?>" class=""><i class="icofont icofont-ui-edit"></i></a>
                     <a href="javascript:;" data-toggle="modal" data-target="#DeleteTodo_<?php echo $todo['id']; ?>" class=""><i class="icofont icofont-ui-delete"></i></a>
                  </div>
               </div>
                <?php } ?>
                <!--  <p>No todos found</p> -->
                </div>
               
            </div>
             </div>
             <div class="tab-pane" id="profile1" role="tabpanel">
                <div class="new-task">             
               <?php
               foreach($todolist as $todo){ ?>
                 <div class="to-do-list sortable-moves">
               <div class="to-do-list card-sub" id="1">
                  <div class="checkbox-fade fade-in-primary">
                     <label class="check-task <?php if($todo['status']=='completed'){ ?> done-task <?php } ?>">
                     <input type="checkbox" value="" <?php if($todo['status']=='completed'){ ?> checked="checked" <?php } ?> id="<?php echo $todo['id']; ?>" onclick="todo_status(this)" >
                     <span class="cr">
                     <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                     </span>
                     <span><?php echo $todo['todo_name']; ?></span> 
                     </label>
                  </div>
                  <div class="f-right">
                    <a href="javascript:;" data-toggle="modal" data-target="#EditTodo_<?php echo $todo['id']; ?>" class=""><i class="icofont icofont-ui-edit"></i></a>
                     <a href="javascript:;" data-toggle="modal" data-target="#DeleteTodo_<?php echo $todo['id']; ?>" class=""><i class="icofont icofont-ui-delete"></i></a>
                  </div>
               </div>
               </div>
              <?php } ?>
               
            </div>
             </div>
          </div>
         </div>
      </div>
      <!-- To Do Card List card end -->
   </div>
   </div>

   <!--chart sec -->

    <div class="card desk-dashboard teo col-xs-12 col-md-6">
      <div class="accident-toggle"><h2>Tasks</h2></div>
      <span class="time-frame"><label>Time Frame:</label>
      <select id="proposal">
        <option value="month">This Month</option>
        <option value="year">This Year</option>
      </select>
       <div class="card-block proposal">
     <div id="piechart" style="width: 100%; height: 300px;"></div>
     </div>
    </div>

    <div class="card desk-dashboard widmar col-xs-12 col-md-6">
      <div class="accident-toggle"><h2>Deadlines</h2></div>
      <span class="time-frame"><label>Time Frame:</label>
      <select id="filter_deadline">
        <option value="month">This Month</option>
        <option value="year">This Year</option>
      </select>
     <div class="card-block deadline">
        <div id="columnchart_values" style="width: 100%; height: 250px;"></div>
       </div>
    </div>

    <div class="card desk-dashboard  col-xs-12 col-md-6">
      <div class="accident-toggle"><h2>Timesheets</h2></div>
      <span class="time-frame"><label>Time Frame:</label>
      <select>
        <option>This Month</option>
        <option>This Year</option>
      </select>
    <div class="card-block">
     <div id="chart_Donut" style="width: 100%; height: 300px;"></div>
    </div>
    </div>

    <div class="card desk-dashboard widmar col-xs-12 col-md-6">
      <div class="accident-toggle"><h2>Clients</h2></div>
      <span class="time-frame"><label>Time Frame:</label>
      <select id="filter_client">
        <option value="year">This Year</option>
        <option value="month">This Month</option>        
      </select>
    <div class="card-block fileters_client">
     <div id="chart_Donut11" style="width: 100%; height: 300px;"></div>
    </div>
    </div>

   <!-- chart sec -->

   </div>
</div>
<!-- management block -->
  <div class="modal fade" id="myTodo" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add New Todo's</h4>
        </div>
        <div class="modal-body">
        <label>Todo's Name</label>
          <input type="text" name="todo_name" id="todo_name" value="">
        </div>
        <div class="modal-footer">
           <button type="button" class="btn btn-default" id="todo_save">Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<?php
foreach($todolist as $todo){
?>


 <div class="modal fade" id="EditTodo_<?php echo $todo['id']; ?>" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Todos</h4>
        </div>
        <div class="modal-body">
         <input type="hidden" name="id" id="todo_id" value="<?php echo $todo['id']; ?>">
         <input type="text" name="todo_name" class="todo_name" value="<?php echo $todo['todo_name']; ?>">
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default save" id="<?php echo $todo['id']; ?>">Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>



   <div class="modal fade" id="DeleteTodo_<?php echo $todo['id']; ?>" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete todos</h4>
        </div>
        <div class="modal-body">      
          <p>Do you want to Delete Todo's ?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default delete" id="<?php echo $todo['id']; ?>">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<?php } ?>

<?php $this->load->view('includes/footer');?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/todo.js"></script>
  <!-- jquery sortable js -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/Sortable.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/sortable-custom.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/custom-google-chart.js"></script>
<script type="text/javascript">

   $(document).ready(function() {
   
         //   var date = $('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
   /*$( ".datepicker" ).datepicker({
          dateFormat: 'yy-mm-dd'
      });*/
   
   $('.tags').tagsinput({
       allowDuplicates: true
      });
      
      $('.tags').on('itemAdded', function(item, tag) {
          $('.items').html('');
   var tags = $('.tags').tagsinput('items');
        
      });
   });
   
   $("#contact_today").click(function(){
     if($("#contact_today").is(':checked')){
       $(".selectDate").css('display','none');
     }else{
       $(".selectDate").css('display','block');
     }
   });
   
   $(".contact_update_today").click(function(){
     var id = $(this).attr("data-id");
     if($(this).is(':checked')){
   
       $("#selectDate_update_"+id).css('display','none');
     }else{
       $("#selectDate_update_"+id).css('display','block');
     }
   });
   
    $("#all_leads").dataTable({
          "iDisplayLength": 10,
       });
   
</script>
<script type="text/javascript">
   $(document).ready(function(){
   
      $( "#leads_form" ).validate({
        rules: {
          lead_status: "required",  
          source: "required",  
          name: "required",  
        },
        messages: {
          lead_status: "Please enter your Status",
          source: "Please enter your Source",
          name: "Please enter your Name"
        },
        
      });
   
   
      $( ".edit_leads_form" ).validate({
        rules: {
          lead_status: "required",  
          source: "required",  
          name: "required",  
        },
        messages: {
          lead_status: "Please enter your Status",
          source: "Please enter your Source",
          name: "Please enter your Name"
        },
        
      });
   });
   
   
   $(".sortby_filter").click(function(){
         $(".LoadingImage").show();
   
      var sortby_val = $(this).attr("id");
      var data={};
      data['sortby_val'] = sortby_val;
      $.ajax({
      url: '<?php echo base_url();?>leads/sort_by/',
      type : 'POST',
      data : data,
      success: function(data) {
        $(".LoadingImage").hide();
   
      $(".sortrec").html(data);   
      $("#all_leads").dataTable({
      "iDisplayLength": 10,
      });
      },
      });
   
   });
   $("#import_leads").validate({
     rules: {
        file: {required: true, accept: "csv"},
   
          lead_status: "required",  
          source: "required",  
          name: "required",  
        },
        messages: {
          file: {required: 'Required!', accept: 'Please upload a file with .csv extension'},
   
          lead_status: "Please enter your Status",
          source: "Please enter your Source",
          name: "Please enter your Name"
        },
   
   });
                            
   
</script>

<script type="text/javascript">
   $(document).ready(function(){
       $('.edit-toggle1').click(function(){
         $('.proposal-down').slideToggle(300);
       });
   
        $("#legal_form12").change(function() {
          var val = $(this).val();
          if(val === "from2") {
            // alert('hi');
              $(".from2option").show();
          }
          else {
              $(".from2option").hide();
          }
          });
   })
</script>
  <script>
   $( function() {

    $( ".sortable" ).sortable(
   {
         update: function(event, ui) {       
       var arr = [];
     $("div.sortable ").each(function() { 
         var idsInOrder = [];
       // idsInOrder.push($(this).attr('id')) 
        var ul_id = $(this).attr('id');
      //  alert(ul_id);

        $("div#"+$(this).attr('id')+" div.card-sub").each(function() { 
        idsInOrder.push($(this).attr('id')) 
    });
   // alert(idsInOrder);
   // arr.push({[ul_id]:idsInOrder});   
     arr[ul_id] = idsInOrder;   
    });

$.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>/Firm_dashboard/update_type_todolist",
    data:  { 'datas' : arr }, 
    beforeSend: function() {
      $(".LoadingImage").show();
    },
    success: function(data){
      $(".LoadingImage").hide();               
    }
    });
}
      });
    //$( ".sortable" ).disableSelection();

  } );

$("#todo_save").click(function(){
  var todo_name=$("#todo_name").val();
  console.log(todo_name);
$.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>/Firm_dashboard/todolist_add",
    data:  { 'todo_name' : todo_name }, 
    beforeSend: function() {
      $(".LoadingImage").show();
    },
    success: function(data){
       $("#myTodo").modal('hide');
       $(".LoadingImage").hide();
      var json = JSON.parse(data);
      data=json['content'];      
       $(".sortable").html('');
       $(".sortable").append(data);        
    }
    });

});

function todo_status(todo){
  var id=$(todo).attr('id');
  //alert(id);
    $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>/Firm_dashboard/todos_status",
    data:  { 'id' : id}, 
    beforeSend: function() {
      $(".LoadingImage").show();
    },
    success: function(data){       
       $(".LoadingImage").hide();
       var json = JSON.parse(data);
       data=json['content'];  
       complete_data=json['completed_content'];      
       $(".sortable").html('');
       $(".sortable").append(data); 
       $(".completed_todos").html('');
       $(".completed_todos").append(complete_data);        
     }
   });
}

$('.save').click(function(){

  var id=$(this).attr('id');
 // console.log(id);
 var todo_name= $("div#EditTodo_"+$(this).attr('id')+" .todo_name").val();
$.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>/Firm_dashboard/todolist_edit",
    data:  { 'todo_name' : todo_name,'id':id }, 
    beforeSend: function() {
      $(".LoadingImage").show();
    },
    success: function(data){
       $("#EditTodo_"+$(this).attr('id')+"").modal('hide');
       $(".LoadingImage").hide();
      location.reload();
    }
    });

//console.log(todo_name);

});

$('.delete').click(function(){
  var id=$(this).attr('id');
$.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>/Firm_dashboard/todolist_delete",
    data:  { 'id':id }, 
    beforeSend: function() {
      $(".LoadingImage").show();
    },
    success: function(data){
       $("#DeleteTodo_"+$(this).attr('id')+"").modal('hide');
       $(".LoadingImage").hide();
      location.reload();
    }
    });
});

$("#filter_client").change(function(){
if($(this).val()=='month'){
id=$(this).val();
$.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>/Firm_dashboard/client_filter",
    data:  { 'id':id }, 
    beforeSend: function() {
      $(".LoadingImage").show();
    },
    success: function(data){
      console.log(data);   
      $(".fileters_client").html('');
       $(".fileters_client").append(data);   
       $(".LoadingImage").hide();
     
    }
    });

}else{
$.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>/Firm_dashboard/client_filter_year",
    data:  { 'id':id }, 
    beforeSend: function() {
      $(".LoadingImage").show();
    },
    success: function(data){
      console.log(data);   
      $(".fileters_client").html('');
       $(".fileters_client").append(data);   
       $(".LoadingImage").hide();
     
    }
    });
 
}
});
$("#filter_deadline").change(function(){
  var id=$(this).val();
  if($(this).val()=='year'){    
      $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/Firm_dashboard/deadline_filter_year",
      data:  { 'id':id }, 
      beforeSend: function() {
        $(".LoadingImage").show();
      },
      success: function(data){
       // console.log(data);   
        $(".deadline").html('');
         $(".deadline").append(data);   
         $(".LoadingImage").hide();     
      }
      });
  }else{
    $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>/Firm_dashboard/deadline_filter_month",
        data:  { 'id':id }, 
        beforeSend: function() {
          $(".LoadingImage").show();
        },
        success: function(data){
         // console.log(data);   
          $(".deadline").html('');
           $(".deadline").append(data);   
           $(".LoadingImage").hide();     
        }
        });
  }
});

$("#proposal").change(function(){
var id=$(this).val();
  if($(this).val()=='year'){    
      $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>/Firm_dashboard/filter_proposal_year",
      data:  { 'id':id }, 
      beforeSend: function() {
        $(".LoadingImage").show();
      },
      success: function(data){
       // console.log(data);   
        $(".proposal").html('');
         $(".proposal").append(data);   
         $(".LoadingImage").hide();     
      }
      });
  }else{
    $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>/Firm_dashboard/filter_proposal_month",
        data:  { 'id':id }, 
        beforeSend: function() {
          $(".LoadingImage").show();
        },
        success: function(data){
         // console.log(data);   
          $(".proposal").html('');
           $(".proposal").append(data);   
           $(".LoadingImage").hide();     
        }
        });
  }
});


     google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Accepted',     <?php echo $accept_proposal['proposal_count']; ?>],
          ['In Discussion',      <?php echo $indiscussion_proposal['proposal_count']; ?>],
          ['Sent',  <?php echo $sent_proposal['proposal_count']; ?>],
          ['Viewed', <?php echo $viewed_proposal['proposal_count']; ?>],
          ['Declined',    <?php echo $declined_proposal['proposal_count']; ?>],
           ['Archived',    <?php echo $archive_proposal['proposal_count']; ?>]
        ]);

        var options = {
          title: 'My Daily Activities'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }


  </script>

<script type="text/javascript">

      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Legal Form', 'Count'],
          ['Private Limited company',     <?php echo $Private['limit_count']; ?>],
          ['Public Limited company',      <?php echo $Public['limit_count']; ?>],
          ['Limited Liability Partnership',  <?php echo $limited['limit_count']; ?>],
          ['Partnership', <?php echo $Partnership['limit_count']; ?>],
          ['Self Assessment',    <?php echo $self['limit_count']; ?>],
          ['Trust',<?php echo $Trust['limit_count']; ?>],
          ['Charity',<?php echo $Charity['limit_count']; ?>],
           ['Other',<?php echo $Other['limit_count']; ?>],
        ]);

        var options = {
          title: 'Client Lists',
          pieHole: 0.4,
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart_Donut11'));
        chart.draw(data, options);
      }
</script>

<script type="text/javascript">
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
     
      var data = google.visualization.arrayToDataTable([
        ["Element", "Density", { role: "style" } ],
        ["Confirmation", <?php echo $confirmation['deadline_count']; ?>, "#b87333"],
        ["Accounts", <?php echo $account['deadline_count']; ?>, "silver"],
       ["Personal Tax", <?php echo $personal_due['deadline_count']; ?>, "silver"],
        ["VAT ", <?php echo $vat['deadline_count']; ?>, "gold"],
        ["Payroll", <?php echo $payroll['deadline_count']; ?>, "red"],
         ["WorkPlace Pension - AE", <?php echo $pension['deadline_count']; ?>, "brown"],
          ["CIS", <?php echo $registeration['deadline_count']; ?>, "brown"],
          ["CIS-Sub", 0, "brown"],
          ["P11D", <?php echo $plld['deadline_count']; ?>, "blue"],
           ["Management Accounts", <?php echo $management['deadline_count']; ?>, "black"],
            ["Bookkeeping", <?php echo $booking['deadline_count']; ?>, "purple"],
             ["Investigation Insurance", <?php echo $insurance['deadline_count']; ?>, "yellow"],
              ["Registeration", <?php echo $registeration['deadline_count']; ?>, "green"],
               ["Tax Advice", <?php echo $investigation['deadline_count']; ?>, "pink"],
                ["Tax Investigation", <?php echo $investigation['deadline_count']; ?>, "color:#F20FDA"]
      ]);      

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var options = {
        title: "Deadline Chart",
        width: 600,
        height: 400,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
      chart.draw(view, options);
  }
  </script>
