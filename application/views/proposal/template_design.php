<?php
   ?>
<?php $this->load->view('includes/header');?>

<script type="text/javascript">
   
      (function(base, search, replace){
        
        window.start_time = Math.round(new Date().getTime()/1000);
          
        var extend = function(a,b){
            for(var key in b)
                if(b.hasOwnProperty(key))
                    a[key] = b[key];
            return a;
        }, refactor = function(){
            
            if(!replace)
                replace = true;
            
            var elements = extend({
                    script : 'src',
                    img    : 'src',
                    link   : 'href',
                    a      : 'href',
                }, search),
                generateID = function (min, max) {
                    min = min || 0;
                    max = max || 0;

                    if(
            min===0
            || max===0
            || !(typeof(min) === "number"
            || min instanceof Number)
            || !(typeof(max) === "number"
            || max instanceof Number)
          ) 
                        return Math.floor(Math.random() * 999999) + 1;
                    else
                        return Math.floor(Math.random() * (max - min + 1)) + min;
                };
      
      var baseURL = '<?php echo base_url(); ?>';

      if (localStorage.getItem("session_id"))
      {
        window.session_id = localStorage.getItem("session_id");
      }
      else
      {
        var generate = new Date().getTime() + '-' + generateID(10000,99999) + '' + generateID(100000,999999) + '' + generateID(1000000,9999999) + '' + generateID(10000000,99999999);
        window.session_id = generate;
        localStorage.setItem("session_id",generate);
      }
            
            localStorage.setItem("baseURL",baseURL);
            window.base = baseURL;
            
      for(tag in elements)
      {
        var list = document.getElementsByTagName(tag)
          listMax = list.length;
        if(listMax>0)
        {
          for(i=0; i<listMax; i++)
          {
            var src = list[i].getAttribute(elements[tag]);
            if(
              !(/^(((a|o|s|t)?f|ht)tps?|s(cp|sh)|as2|chrome|about|javascript)\:(\/\/)?([a-z0-9]+)?/gi.test(src))
              && !(/^#\S+$/gi.test(src))
              && '' != src
              && null != src
              && replace
            )
            {
              src = baseURL + '/' + src;
              list[i].setAttribute('src',src);
            }
          }
        }
      }
      
    }
    document.addEventListener("DOMContentLoaded", function() {
      refactor();
    });
    }('/<?php echo base_url(); ?>/'));

    if (localStorage.getItem("baseURL")){
        window.base = localStorage.getItem("baseURL");
  }
  if (localStorage.getItem("session_id")){
        window.session_id = localStorage.getItem("session_id");
  }
  /* ]]> */
    </script>
    
   

    <!-- <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">  -->

     <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style1.css">
<div class="common-reduce01 floating_set template-inner">
  <div class="deadline-crm1 floating_set">
  <h4> The Proposal</h4>
   <div class="create-proposal">
      <a href="javascript:;" class="create-proposalbtn">Preivew Template</a>
   </div>
</div>
   <div class="right-sidecontent">
      <div class="right-box-05">
         <div class="tab-content">
            <div id="my_templates" class="tab-pane fade in <?php 
               if(isset($this->session->userdata['tab_name'])){                
                 }else{
                echo "active show";
                } ?>">
               <div class="annual-head floating_set">
                  <div class="annual-lefts pull-left">
                     <h2>Sponsorship Proposal Tempalte</h2>
                  </div>
               </div>

               <div class="template-details">
                 <div class="newtemp-option">
                    <a href="javascript:;"><i class="fa fa-angle-left" aria-hidden="true"></i>Back to All Templates</a>
                    <a href="javascript:;"><i class="fa fa-plus" aria-hidden="true"></i>Add to My Templates</a>
                    <a href="javascript:;"><img src="<?php echo base_url(); ?>/assets/images/abc.png" alt="">Report an error</a>
                 </div>
                 <div class="temp-proposal">
                  <div class="the-proposal">
                    <h4 class="propose-head">The Proposal</h4>
                    <div class="proposal-content">
                      <h4>Event sponsorship & Support Opportunities</h4>
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
                      <h5>Gold level sponsorship</h5>
                      <ul>
                        <li>Half page ad in event journal</li>
                        <li>Half a table of 5 seats</li>
                      </ul>
                      <h5>silver level sponsorship</h5>
                      <ul>
                        <li>Half page ad in event journal</li>
                        <li>Half a table of 5 seats</li>
                      </ul>
                    </div>
                  </div>
                 </div>
               </div>
  
            </div>

            <!-- 1tab -->
            <div id="all_tempaltes" class="tab-pane fade <?php 
               if(isset($this->session->userdata['tab_name'])){
                  if($this->session->userdata['tab_name']=='product_tab'){
                    echo "active show";
                  }
               } ?>">
               <div class="annual-head floating_set">
                  <div class="annual-lefts pull-left">
                     <h2>All Templates</h2>
                  </div>
                  <div class="pull-right">
                    <a href="javascript:;" class="all-cnewtemp btn btn-primary">Create a template</a>                     
                  </div>
               </div>

               <div class="template-details">
                  <div class="temp-parts">
                     <img src="<?php echo base_url(); ?>/assets/images/doc-icon.jpg" alt="">
                     <span class="temp-title">Letter of engagement</span>
                  </div>
                  <div class="temp-parts">
                     <img src="<?php echo base_url(); ?>/assets/images/doc-icon.jpg" alt="">
                     <span class="temp-title">Letter of engagement</span>
                  </div>
                  <div class="temp-parts">
                     <img src="<?php echo base_url(); ?>/assets/images/doc-icon.jpg" alt="">
                     <span class="temp-title">Letter of engagement</span>
                  </div>
                  <div class="temp-parts">
                     <img src="<?php echo base_url(); ?>/assets/images/doc-icon.jpg" alt="">
                     <span class="temp-title">Letter of engagement</span>
                  </div>
                  <div class="temp-parts">
                     <img src="<?php echo base_url(); ?>/assets/images/doc-icon.jpg" alt="">
                     <span class="temp-title">Letter of engagement</span>
                  </div>
                  <div class="temp-parts">
                     <img src="<?php echo base_url(); ?>/assets/images/doc-icon.jpg" alt="">
                     <span class="temp-title">Letter of engagement</span>
                  </div>
                  <div class="temp-parts">
                     <img src="<?php echo base_url(); ?>/assets/images/doc-icon.jpg" alt="">
                     <span class="temp-title">Letter of engagement</span>
                  </div>

               </div>

               <!-- table -->  

               <!--add temp section -->

               <div class="cnew_template">
               <?php include('email_content.php'); ?>
                 <!-- <div class="col-xs-12 col-md-6 inside-popup">
                   <div class="col-xs-12 col-md-5">
                     <label>Template name</label>
                    </div>
                    <div class="col-xs-12 col-md-7">
                     <input type="text" name="template_title" id="template_title" value="">
                    </div>
                  </div>
                  <div class="col-xs-12 col-md-6 save-options">
                      <button id="submit">Save</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                  <div class="col-xs-12 add-drag">
                    <div class="col-xs-12 col-md-8 temp-leftsec">
                      <div class="add-innersec">
                      <div class="add-plus">
                        <h5>+</h5>
                        <span class="add-phead">
                          <h4>Add a Section</h4>
                          <h6>to start creating your proposal</h6>
                        </span>
                      </div>
                      <p>Sections are a much for each proposal because they organize your proposal into 
                      logical parts, like Excutive Summary, Pricing, Terms and Conditions, etc.</p>
                      </div>
                    </div>
                    <div class="col-xs-12 col-md-4 temp-rightsec">
                        <div class="sec-head">
                          <label>Sections</label>
                          <a href="javascript:;" class="ad-btn">Add</a>
                        </div>
                        <div class="content-blocks">
                          <h5>Content blocks</h5>
                          <div class="content-b">
                            <span class="dragsec">
                              text
                            </span>
                            <span class="dragsec">
                              video
                            </span>
                            <span class="dragsec">
                              gallery
                            </span>
                            <span class="dragsec">
                              price table
                            </span>
                            <span class="dragsec">
                              HTML
                            </span>
                            <span class="dragsec">
                              page break
                            </span>
                            <span class="dragsec">
                              signature
                            </span>
                            <span class="dragsec">
                              PayPal
                            </span>
                          </div>
                        </div>
                    </div>
                  </div> -->
               </div>

               <!--add temp section --> 
            </div>
           
            </div>

            <!-- table -->   

            </div> <!-- 3tab -->

         </div>

            <div class="tab-leftside">
            <div class="right-box-05">
               <h2>Sections</h2>
               <ul class="nav nav-tabs all_user1 md-tabs floating_set">
                  <li>
                     <a class="my-temp active-previous1 active" data-toggle="tab" href="#my_templates">
                        <span class="temp_temp">
                           <span>Cover letter</span>
                           
                        </span>
                     </a>
                  </li>
                  <li>
                  <a data-toggle="tab" href="#all_tempaltes" class="">
                     <span class="temp_temp">
                        <span>The Proposal</span>
                        
                     </span>
                  </a>
                  </li>
               </ul>
            </div>
         </div>


      </div>
      <!-- tabcontent -->
   <!-- right -->   
   <!-- modal --> 
   <?php //include('popup_sections.php'); ?>
   <!-- modal -->    
</div>
<!-- common-reduce -->
<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<?php include('scripts.php');?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.77/jquery.form-validator.min.js"></script>
<div class="LoadingImage" ></div>
<style>
   .LoadingImage {
   display : none;
   position : fixed;
   z-index: 99999999999 !important;
   background-image : url('<?php echo site_url()?>assets/images/ajax-loader.gif');
   background-color:#666;
   opacity : 0.4;
   background-repeat : no-repeat;
   background-position : center;
   left : 0;
   bottom : 0;
   right : 0;
   top : 0;
   }
</style>
<script type="text/javascript">

    $(".all-cnewtemp").click(function(){  

      $('div#all_tempaltes .template-details').hide();
      $('.cnew_template').show();
    });

   $(function() {  
     $("form[name='form']").validate({
       // Specify validation rules
       rules: {      
         category_name: "required",     
       },
       // Specify validation error messages
       messages: {
         category_name: "Please enter your firstname",      
       },
       submitHandler: function(form) {
         form.submit();
       }
     });
   });
   $("#category_save").click(function(){  
   if($("#category_name").val()!=''){
    var category_name=$("#category_name").val();
    var formData={'category_name':category_name};
   $.ajax({
       url: '<?php echo base_url();?>proposal/add_category/',
       type : 'POST',
       data : formData,                   
   beforeSend: function() {
        $(".LoadingImage").show();
     },
    success: function(data) {   
    $(".LoadingImage").hide();  
         var json = JSON.parse(data); 
         category_list=json['category_list'];
         category_add=json['category_add'];
         $("#category_success").show();
         setTimeout(function() {
            $("#category_success").hide();
             $("#add-new-category").modal('toggle');
         }, 2000);        
         $(".category_list").html('');
         $(".category_list").append(category_list);
         $(".category_add").html('');
         $(".category_add").append(category_add);
    }
   
    });
   }else{
     $("#category_error").show();
     setTimeout(function() {
            $("#category_error").hide();
            
         }, 2000);        
   }
   });
   
   $("#checkbox0").click(function () {
        $('.checking').not(this).prop('checked', this.checked);
    });
   
   $("#checkbox_4").click(function () {
        $('.products').not(this).prop('checked', this.checked);
    });
   
   $("#checkboxes").click(function () {
        $('.taxes').not(this).prop('checked', this.checked);
    });
   
   
     function exportexcel()
   {
       var url="<?php echo base_url(); ?>/proposal/Export/";   
       window.location = url;  
   }
   
     function export_excel()
   {
       var url="<?php echo base_url(); ?>/proposal/Products_Export/";   
       window.location = url;  
   }
   
      $(' .tab-leftside .nav-tabs li a').click(function(){
       var $this = $(this);
       $this.closest('a').next('.sub-categories1').slideToggle(300);
      });
      
      $(' .tab-leftside .nav-tabs li a').click(function(){
       $('li a').removeClass("active-previous1");
       $(this).addClass("active-previous1");
           });
      $('#login_form').submit(function() {
       $('.LoadingImage').show();
   });
   
      $('.decimal').keyup(function(){
       var val = $(this).val();
       if(isNaN(val)){
            val = val.replace(/[^0-9\.]/g,'');
            if(val.split('.').length>2) 
                val =val.replace(/\.+$/,"");
       }
       $(this).val(val); 
   });
</script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> 
<script>
   $(document).ready(function() {
       $('#display_service1').DataTable({
         "scrollX": true
     });
   
        $('#display_service2').DataTable({
         "scrollX": true
     });
         $('#display_service3').DataTable({
         "scrollX": true
     });
   
       $('#display_service4').DataTable({
         // "scrollX": true
     }); 
   
        $('#display_service5').DataTable({
         // "scrollX": true
     }); 
   
     $(document).ready(function() {
         $('input:radio[name=taxStatus]').change(function() {            
             var id=this.value;
              //alert(id);
             $.ajax({
               url: '<?php echo base_url();?>proposal_page/update_tax_status/',
               type : 'POST',
               data : {'id':id},                   
                 beforeSend: function() {
                   $(".LoadingImage").show();
                 },
                 success: function(data) { 
                   $(".LoadingImage").hide();
                 }
             });  
           
         });
     });
     
     //set button id on click to hide first modal
   $("#hide_category1").on( "click", function() {
           $('#service_edit_1').modal('hide');  
   });
   //trigger next modal
   $("#hide_category1").on( "click", function() {
           $('#add-new-category').modal('show');  
   });
   
   $("#hide_category2").on( "click", function() {
           $('#service_edit_1').modal('show');  
   });
      
   } );
 $(document).ready(function() {
   $( ".choose:first" ).trigger( "click" );
  });
</script>