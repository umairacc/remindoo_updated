<?php $this->load->view('includes/header');
   $succ = $this->session->flashdata('success');
   ?>
<style>	
   .columns  .editor {
   float: left;
   width: 65%;
   position: relative;
   z-index: 1;
   }
   .columns  .contacts {
   float: right;
   width: 35%;
   box-sizing: border-box;
   padding: 0 0 0 20px;
   }
   #contactList {
   list-style-type: none;
   margin: 0 !important;
   padding: 0;
   }
   #contactList li {
   background: #FAFAFA;
   margin-bottom: 1px;
   height: 36px;
   line-height: 30px;
   cursor: pointer;
   }
   #contactList li:nth-child(2n) {
   background: #F3F3F3;
   }
   #contactList li:hover {
   background: #FFFDE3;
   border-left: 5px solid #DCDAC1;
   margin-left: -5px;
   }
   div#myTemplate {
   z-index: 1041;
   }
   div.edit_template {
   z-index: 1041;
   }
   .my-error-class {
   border-color:1px red;
   }
</style>
<div class="pcoded-content pcodedteam-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
                  <!--start-->
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="deadline-crm1 floating_set single-txt12">
                                    <ul class="nav nav-tabs all_user1 md-tabs pull-left u-dashboard">
                                       <li class="nav-item">
                                          Proposal Template
                                          <div class="slide"></div>
                                       </li>
                                    </ul>
                     </div>
                     <div class="col-12 card">
                        <!-- admin start-->
                        <div class="client_section user-dashboard-section1 floating_set edit-mailtemp">
                           <div class="modal-alertsuccess alert alert-success" style="display:none;">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                              <div class="pop-realted1">
                                 <div class="position-alert1">
                                    YOU HAVE SUCCESSFULLY ADDED TEMPLATE
                                 </div>
                              </div>
                           </div>
                           <div class="modal-alertsuccess alert alert-success-update" style="display:none;">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                              <div class="pop-realted1">
                                 <div class="position-alert1">
                                    YOU HAVE SUCCESSFULLY UPDATED                              
                                 </div>
                              </div>
                           </div>
                           <div class="modal-alertsuccess alert alert-danger" style="display:none;">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <div class="pop-realted1">
                                 <div class="position-alert1">
                                    Please Fill All the required fills.
                                 </div>
                              </div>
                           </div>
                           
                           <div class="columns">
                           	<div class="edit-tempsect col-xs-12">
                              <div class="col-md-6">
                                 <div class="col-md-5">
                                    <label>Template Title</label>
                                 </div>
                                 <input type="hidden" name="id" id="id" value="<?php if(isset($value['id'])){
                                    echo $value['id'];
                                    } ?>">
                                 <div class="col-md-7">
                                    <input type="text" name="template_title" id="template_title" value="<?php if(isset($value['template_title'])){
                                       echo $value['template_title'];
                                       } ?>">
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="col-md-5">
                                    <label>Template Type</label>
                                 </div>
                                 <div class="col-md-7">
                                    <select name="template_type" id="template_type">
                                       <option value="none">Select</option>
                                       <option value="invoice_template" <?php if($value['template_type']=='invoice_template'){ ?> selected="selected" <?php } ?>>Invoice Template
                                       </option>
                                    </select>
                                 </div>
                              </div>
                             </div>
                              <div class="com-div123 col-xs-12">
                              <div class="editor">
                                 <div cols="10" id="editor1" name="editor1" rows="10"  contenteditable="true">
                                    <?php if(isset($value['template_content'])){
                                       echo $value['template_content'];
                                       } ?>
                                 </div>
                                 <p id="error" style="color: red; display: none;">Template Content Required</p>
                              </div>
                              <div class="contacts">
                                 <!-- <h3>List of Droppable Contacts</h3> -->
                                 <ul id="contactList">
                                    <li>
                                       <div class="contact h-card" data-contact="0" draggable="true" tabindex="0">User Name</div>
                                    </li>
                                    <li>
                                       <div class="contact h-card" data-contact="1" draggable="true" tabindex="0">Client Name</div>
                                    </li>
                                    <li>
                                       <div class="contact h-card" data-contact="2" draggable="true" tabindex="0">Accountant Name</div>
                                    </li>
                                    <li>
                                       <div class="contact h-card" data-contact="3" draggable="true" tabindex="0">Task Number</div>
                                    </li>
                                    <li>
                                       <div class="contact h-card" data-contact="4" draggable="true" tabindex="0">Task Name</div>
                                    </li>
                                    <li>
                                       <div class="contact h-card" data-contact="5" draggable="true" tabindex="0">Task Due Date</div>
                                    </li>
                                    <li>
                                       <div class="contact h-card" data-contact="6" draggable="true" tabindex="0">Task Client</div>
                                    </li>
                                    <li>
                                       <div class="contact h-card" data-contact="7" draggable="true" tabindex="0">Firm Name</div>
                                    </li>
                                    <li>
                                       <div class="contact h-card" data-contact="8" draggable="true" tabindex="0"> Accountant Number</div>
                                    </li>
                                    <li>
                                       <div class="contact h-card" data-contact="9" draggable="true" tabindex="0">	Invoice Amount	</div>
                                    </li>
                                    <li>
                                       <div class="contact h-card" data-contact="10" draggable="true" tabindex="0">Firm Name</div>
                                    </li>
                                    <li>
                                       <div class="contact h-card" data-contact="11" draggable="true" tabindex="0">Price Table</div>
                                    </li>
                                    <li>
                                       <div class="contact h-card" data-contact="12" draggable="true" tabindex="0"> Invoice Due Date</div>
                                    </li>
                                 </ul>
                              </div>
                              </div>
                           </div>
                           <div class="save-btn btn99 col-12">
                           <button class="btn btn-primary" onclick="goBack()">Cancel</button>
                           	<button id="update" class="btn btn-primary">Save</button>
                           </div>
                        </div>
                        <!-- Page body end -->
                     </div>
                  </div>
                  <!-- Main-body end -->
                  <div id="styleSelector">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- ajax loader -->
<div class="LoadingImage" ></div>
<style>
   .LoadingImage {
   display : none;
   position : fixed;
   z-index: 100;
   background-image : url('<?php echo site_url()?>assets/images/ajax-loader.gif');
   background-color:#666;
   opacity : 0.4;
   background-repeat : no-repeat;
   background-position : center;
   left : 0;
   bottom : 0;
   right : 0;
   top : 0;
   }
</style>
<!-- ajax loader end-->
<?php $this->load->view('includes/session_timeout');?>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/popper.js/js/popper.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.j-pro.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/switchery/js/switchery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/modernizr/js/css-scrollbars.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/advance-elements/custom-picker.js"></script>
<script src="<?php echo base_url();?>assets/js/pcoded.min.js"></script>
<script src="<?php echo base_url();?>assets/js/demo-12.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/common_script.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
<script src="<?php echo base_url();?>assets/js/mock.js"></script>
<script src="https://cdn.ckeditor.com/4.9.0/standard-all/ckeditor.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript">
   $(document).ready(function(){
      $( "#proposal_template" ).validate({
      		errorClass: "my-error-class",
        rules: {
          template_title: "required",  
          template_content: "required",  
          template_type: "required"
        },
        messages: {
          template_title: "Please enter your Title",
          template_content: "Please enter your Title",
          template_type: "Please Select Template Type"
        },        
      });
   });
   CKEDITOR.replace( 'editor1', {
   extraPlugins: 'divarea,hcard,sourcedialog,justify'
   });
   'use strict';
   var CONTACTS = [
   { name: 'Username' },
   { name: 'Client Name' },
   { name: 'Accountant Name' },
   { name: 'Task Number' },
   { name: 'Task Name' },
   { name: ' Task Due Date'},
   { name: ' Task Client' },
   { name: 'Firm Name' },
   { name: 'accountant number' },
   { name: 'invoice amount' },
   { name: 'firm name' },
   { name: 'Price Table' },	
   { name: 'invoice due date' },			
   ];
   CKEDITOR.disableAutoInline = true;		
   CKEDITOR.plugins.add( 'hcard', {
   requires: 'widget',
   init: function( editor ) {
   	editor.widgets.add( 'hcard', {
   		allowedContent: 'span(!h-card); a[href](!u-email,!p-name); span(!p-tel)',
   		requiredContent: 'span(h-card)',
   		pathName: 'hcard',
   		upcast: function( el ) {
   			return el.name == 'span' && el.hasClass( 'h-card' );
   		}
   	} );				
   	editor.addFeature( editor.widgets.registered.hcard );				
   	editor.on( 'paste', function( evt ) {
   		var contact = evt.data.dataTransfer.getData( 'contact' );
   		if ( !contact ) {
   			return;
   		}
   		evt.data.dataValue =						
   			'<span class="content h-card">' +
   				'['+contact.name+']'  +
   			'</span>';
   	} );
   }
   });
   CKEDITOR.on( 'instanceReady', function() {			
   CKEDITOR.document.getById( 'contactList' ).on( 'dragstart', function( evt ) {			
   	var target = evt.data.getTarget().getAscendant( 'div', true );
   	
   	CKEDITOR.plugins.clipboard.initDragDataTransfer( evt );
   	var dataTransfer = evt.data.dataTransfer;				
   	dataTransfer.setData( 'contact', CONTACTS[ target.data( 'contact' ) ] );	
   	dataTransfer.setData( 'text/html', target.getText() );				
   } );
   } );
   $("#template_title").keyup(function(){
   $("#template_title").removeAttr("style");
   });
   $("#template_type").change(function(){
   $("#template_type").removeAttr("style");
   });
   
   $("#update").click(function(){      
        var template_content=$(".cke_wysiwyg_div").html();
        // console.log(templated_content);
   
          $('.cke_wysiwyg_div > span.h-card').each(function(){
           str += $(this).html();        
          });
   
          $("span.cke_reset").each(function () {
          $(this).remove();
          });
   
          $("span.cke_widget_wrapper").each(function () {
          $(this).replaceWith($(this).text());
          });
   
         var template_content=$(".cke_wysiwyg_div").html();
          //console.log(newString);
        var  template_type=$("#template_type").val();
        var  template_title=$("#template_title").val();
        var id=$("#id").val();
        var formData={'template_content':template_content,'template_title':template_title,'template_type':template_type,'id':id};       
         $.ajax({
                     url: '<?php echo base_url();?>proposal/update_template/',
                     type : 'POST',
                     data : formData,                   
               beforeSend: function() {
                    $(".loading-image").show();
                 },
                 success: function(msg) {
                  $(".loading-image").hide();
                    if(msg=='0'){
                        $('.alert-danger').show();
                    }else{
                    	 $('.alert-success-update').show();
                      setTimeout(function() { 
                        $('.alert-success-update').hide();
                         window.location.href="<?php echo base_url().'proposal/template'?>"
                                }, 1000);                   
                    }
                 }
   
                  });
   });
</script> 
<script>
function goBack() {
    window.history.back();
}
</script>

<script type="text/javascript">
   $('#timepicker1').timepicker();
</script> 
</body>
</html>