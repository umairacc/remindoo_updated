<table class="price-table">
<tbody>
<tr>
<td>
<div style="float: left;width: 33.3%;padding: 8px;" class="columns" ondblclick="editable_content(this);">
  <table style="float: left;width: 100%;list-style-type: none;border: 1px solid #eee; margin: 0; padding: 0;  -webkit-transition: 0.3s;transition: 0.3s;" cellpadding="10" class="price">
  <tr>
    <td style="float: left;width: 100%;background-color: #4CAF50;padding: 20px;text-align: center;display: block;color: white;font-size: 25px;" class="header">Basic
    </td>
  </tr>
  <tr>
    <td style="float: left;width: 100%; background-color: #eee;font-size: 20px;text-align: center;padding: 20px;" class="grey">$ 9.99 / year</td>
  </tr>
  <tr>
    <td style="float: left;width: 100%;border-bottom: 1px solid #eee; padding: 20px;display: block; text-align: center;">10GB Storage</td>
  </tr>
  <tr>
    <td style="float: left;width: 100%;border-bottom: 1px solid #eee; padding: 20px;display: block;display: block; text-align: center;">10 Emails</td>
</tr>
<tr>
    <td style="float: left;width: 100%;border-bottom: 1px solid #eee; padding: 20px;display: block; text-align: center;">10 Domains</td>
</tr>
<tr>
    <td style="float: left;width: 100%;border-bottom: 1px solid #eee; padding: 20px;display: block; text-align: center;">1GB Bandwidth</td>
</tr>
<tr>
    <td style="float: left;width: 100%; background-color: #eee; font-size: 20px;display: block;    text-align: center;
    padding: 10px 0;"><!-- <a href="#" style="background-color: #4CAF50; border: none; color: white; padding: 10px 25px; text-align: center; text-decoration: none;
    font-size: 18px;" class="grey"> -->
    <table cellpadding="5">
    <tr>
    <td style="background-color: #4CAF50;width:95%;text-align:center;margin-left:30px;">
    <a href="#" style="width:60%;display: block; border: none; color: white; padding: 10px 25px; text-align: center; text-decoration: none;
    font-size: 18px;" class="button">
    Sign Up</a></td>
    </tr>
    </table></td>
</tr>
  </table>
</div>
</td>
<td>

<div style="float: left;width: 33.3%;padding: 8px;" class="columns" ondblclick="editable_content(this);">
  <ul style="float: left;width: 100%; list-style-type: none;border: 1px solid #eee; margin: 0; padding: 0;  -webkit-transition: 0.3s;transition: 0.3s;" class="price">
    <li style="float: left;width: 100%;background-color: #4CAF50; padding: 20px;display: block;      text-align: center; color: white;    font-size: 25px;" class="header">Pro</li>
    <li style="float: left;width: 100%; background-color: #eee;font-size: 20px;text-align: center;padding: 20px;" class="grey">$ 24.99 / year</li>
    <li style="float: left;width: 100%;border-bottom: 1px solid #eee;display: block; padding: 20px; text-align: center;">25GB Storage</li>
    <li style="float: left;width: 100%;border-bottom: 1px solid #eee;display: block; padding: 20px; text-align: center;">25 Emails</li>
    <li style="float: left;width: 100%;border-bottom: 1px solid #eee;display: block; padding: 20px; text-align: center;">25 Domains</li>
    <li style="float: left;width: 100%;border-bottom: 1px solid #eee;display: block; padding: 20px; text-align: center;">2GB Bandwidth</li>
    <li style="float: left;width: 100%; background-color: #eee; font-size: 20px;display: block;    text-align: center;
    padding: 10px 0;"><!-- <a href="#" style="background-color: #4CAF50; border: none; color: white; padding: 10px 25px; text-align: center; text-decoration: none;
    font-size: 18px;" class="grey"> --><a href="#" class="button" style="float: left;width: 100%;display: block;background-color: #4CAF50; border: none; color: white; padding: 10px 25px; text-align: center; text-decoration: none;
    font-size: 18px;">Sign Up</a></li>
  </ul>
</div>
</td>
<td>

<div style="float: left;width: 33.3%;padding: 8px;" class="columns" ondblclick="editable_content(this);">
  <ul style="float: left;width: 100%; list-style-type: none;border: 1px solid #eee; margin: 0; padding: 0;  -webkit-transition: 0.3s;transition: 0.3s;" class="price">
    <li style="float: left;width: 100%;background-color: #4CAF50; padding: 20px;display: block;     text-align: center;  color: white;    font-size: 25px;" class="header">Premium</li>
    <li style="float: left;width: 100%; background-color: #eee;font-size: 20px;display: block;text-align: center;padding: 20px;" class="grey">$ 49.99 / year</li>
    <li style="float: left;width: 100%;border-bottom: 1px solid #eee;display: block; padding: 20px; text-align: center;">50GB Storage</li>
    <li style="float: left;width: 100%;border-bottom: 1px solid #eee;display: block; padding: 20px; text-align: center;">50 Emails</li>
    <li style="float: left;width: 100%;border-bottom: 1px solid #eee;display: block; padding: 20px; text-align: center;">50 Domains</li>
    <li style="float: left;width: 100%;border-bottom: 1px solid #eee;display: block; padding: 20px; text-align: center;">5GB Bandwidth</li>
    <li style="float: left;width: 100%; background-color: #eee; font-size: 20px;display: block;    text-align: center;
    padding: 10px 0;"><!-- <a href="#" style="background-color: #4CAF50; border: none; color: white; padding: 10px 25px; text-align: center; text-decoration: none;
    font-size: 18px;" class="grey"> --><a href="#" style="float: left;width: 100%;display: block;background-color: #4CAF50; border: none; color: white; padding: 10px 25px; text-align: center; text-decoration: none;
    font-size: 18px;" class="button">Sign Up</a></li>
  </ul>
</div>
</td>
</tr>
</tbody>
</table>