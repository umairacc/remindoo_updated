<style type="text/css">
   .done_field{
      display: none;
   }
   * {
    box-sizing: border-box;
}

.columns {
    float: left;
    width: 33.3%;
    padding: 8px;
}

.price {
    list-style-type: none;
    border: 1px solid #eee;
    margin: 0;
    padding: 0;
    -webkit-transition: 0.3s;
    transition: 0.3s;
}

.price:hover {
    box-shadow: 0 8px 12px 0 rgba(0,0,0,0.2)
}

.price .header {
    background-color: #111;
    color: white;
    font-size: 25px;
}

.price li {
    border-bottom: 1px solid #eee;
    padding: 20px;
    text-align: center;
}

.price .grey {
    background-color: #eee;
    font-size: 20px;
}

.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 10px 25px;
    text-align: center;
    text-decoration: none;
    font-size: 18px;
}

.ScrollStyle
{
    max-height: 600px !important;
    overflow-y: scroll !important;    
}

p,.ready-for-edit
{
  text-align: justify;  
  word-break: break-all !important;
  white-space: normal !important;
  text-overflow: string !important; 
}

body 
{
   overflow: scroll !important;
}

@media only screen and (max-width: 600px) {
    .columns {
        width: 100%;
    }
}
.newproposal_lists {
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
}
.newproposal_lists div#tabs-4 {
    width: 25%;
    padding: 0 15px;
}
.newproposal_lists .page-body.task-d.taskpro_posal {
    width: 75% !important;
    max-width: initial !important;
}
li.doc_val a
{
  width: 100% !important;
}
@media(max-width:767px){
.newproposal_lists div#tabs-4 {
    width: 100%;
}
.newproposal_lists .page-body.task-d.taskpro_posal {
    width: 100% !important;
}
}

/* price table */

 .table-scroll {
   border: 1px solid #e3e0e1;
 }
 span.service-title {
   background: #d8d8d8;
   width: 100%;
   display: block;
   padding: 10px 15px;
   font-weight: 600;
   color: #333;
 } 
 
 .content-blog{
   margin: 20px 0;
 }
 .grand-total p {
   text-align: right;
 }
 .grand-total {
   text-align: right;
   padding: 10px 20px;
 }
 .grand-total span {
   min-width: 101px;
   display: inline-block;
   width: 110px;
   text-align: left;
 }
 .grand-total span ,.grand-total strong{
   display: inline-block;
   vertical-align: top;
   vertical-align: middle;
   line-height: 10px;
 }
 .grand-total p{
   text-align: right;
   display: block;
 }
 .grand-total .boder-top {
   padding-top: 10px;
   border-top: 1px solid #dbdbdb;
   font-weight: 600;
   margin-top: 15px;
   font-size: 14px;
   display: block;
 }
 .sig-img,.sig-right{
   width: 49%;
   display: inline-block;
   vertical-align: top;  
 }
 .sig-right{
   text-align: right;
 }
 .signature-blog p{
   margin-top: 35px;
   height: 34px;
   border-bottom:1px  solid #dbdbdb;
   padding-bottom: 8px;
   width: 230px;
 }
 .signature-blog table{
   width: 100%;
 }
 .signature-blog span{
   display: block;
   font-size: 14px;
   color: #000;
   margin-top: 10px;
 }
 .upper{
   text-transform: uppercase;
 }
 .signature-blog td:first-child{
   width: 60%;
 }
 .signature-blog td:last-child{
   width: 40%;
 } 
 .pdf-table .table-scroll{
   margin-top: 20px;
 }

 .pdf-table .table-scroll td{
  vertical-align: top;
}   
.col-span1,.col-span2,.col-span3,.col-span4,.row1,.row3{
  display: inline-block;
  text-align: left;
  vertical-align: top;
}

.col-span1{
  width: 294.8px;
}
.col-span2{
  width: 129px;
}
.col-span3{
  width:76.5px;
}
.col-span4{
  width: 79px;
}  
.row3 {
  width: 510px;
  border-right: 1px solid #dbdbdb;
}
.full-acc {
    display: flex;
    flex-wrap: wrap;
}
.row1{
  width: 90px;
}
.col-span1,.col-span2,.col-span3,.col-span4{
 border-right: 1px solid #dbdbdb;
 font-size: 13px;
}
.tab-topbar{
  font-size: 15px;
  font-weight: normal;
  display: block;
  font-weight: 600;
  vertical-align: top;
  border-bottom: 1px solid #dbdbdb;
}   
.col-span4:last-child{
  border-right: none;
} 
.cmn-spaces,.col-span1,.col-span2,.col-span3,.col-span4{
  padding: 7px;
  font-size: 13px;
}
.full-acc{
  border-bottom: 1px solid #dbdbdb;
}
.col-span3:last-child{
  border-right: none;
}
.tab-topbar .col-span3{
  width: 78px;
}
.cmn-spaces,.price-row,.full-acc{
  border-bottom: 1px solid #dbdbdb;
}
.cmn-spaces:last-child,.price-row:last-child,.col-span1,.col-span2,.col-span3{
  border-bottom: none;
}
div *{
  line-height: 19px;
}
.row1 {
  color: #22b14c ;
}



.tab-topbar .col-span1 {
    width: 47%;
}
.tab-topbar .col-span2 {
    width: 23%;
}
.tab-topbar .col-span3 {
    width: 12%;
}
.tab-topbar .col-span4 {
    width: 15%;
}

.row3 {
    width: 83.6%;
}

.row3 .col-span1 {
    width: 56.9%;
}

.row3 .col-span2 {
    width: 28.4%;
}

.row3 .col-span3 {
    width: 14%;
}

.row1.cmn-spaces {
    width: 15%;
}
/* price table */
</style>
<?php 

  $firm_currency = $this->Common_mdl->get_price('admin_setting','firm_id',$_SESSION['firm_id'],'crm_currency');
  $cur_symbols = $this->Common_mdl->getallrecords('crm_currency');

  foreach ($cur_symbols as $key => $value) 
  {
     $currency_symbols[$value['country']] = $value['currency'];
  }

?>


<html lang="en">
   <head>
      <title>CRM </title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="description" content="#">
      <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
      <meta name="author" content="#">
      <!-- Favicon icon -->
      <link rel="icon" href="<?php echo base_url(); ?>assets/images/favicon.ico" type="image/x-icon">
      <!-- Google font-->
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
      <!-- Required Fremwork -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>bower_components/bootstrap/css/bootstrap.min.css">
      <!-- themify-icons line icon -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/icon/themify-icons/themify-icons.css">
      <!-- ico font -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/icon/icofont/css/icofont.css">
      <!-- flag icon framework css -->
      <!-- Menu-Search css -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/pages/menu-search/css/component.css">
      <!-- jpro forms css -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/pages/j-pro/css/demo.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/pages/j-pro/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>bower_components/bootstrap/css/bootstrap.min.css">
      <!--  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/pages/j-pro/css/j-pro-modern.css"> -->
      <!-- Style.css -->
      <link type="text/css" href="<?php echo base_url(); ?>bower_components/bootstrap-timepicker/css/bootstrap-timepicker.min.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/pages/advance-elements/css/bootstrap-datetimepicker.css">
      <!-- Date-range picker css  -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>bower_components/bootstrap-daterangepicker/css/daterangepicker.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>bower_components/datedropper/css/datedropper.min.css" />
      <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
      <link rel="stylesheet" href="https:/resources/demos/style.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>bower_components/switchery/css/switchery.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.mCustomScrollbar.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>bower_components/owl.carousel/css/owl.carousel.css">
      <!--     <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/dataTables.bootstrap4.min.css">
         <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/buttons.dataTables.min.css?ver=2">
         <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/responsive.bootstrap4.min.css"> -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/ui_style.css?ver=3">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/common_style.css?ver=4">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/common_responsive.css?ver=2">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/dataTables.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/jquery.dropdown.css">
      <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
      <link rel="stylesheet" type="text/css" href="https://bootswatch.com/superhero/bootstrap.min.css">
      <link href="<?php echo base_url(); ?>css/jquery.signaturepad.css" rel="stylesheet">
      <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-colorpicker.min.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-slider.min.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/medium-editor/medium-editor.min.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/medium-editor/template.min.css">
      <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style1.css"> -->
      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/icon/simple-line-icons/css/simple-line-icons.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/icon/icofont/css/icofont.css">
      <style>
         .common_form_section1  .col-xs-12.col-sm-6.col-md-6 {
         float: left !important;
         }
         .common_form_section1 {
         background: #fff;
         box-shadow: 1px 2px 5px #dbdbdb;
         border-radius: 5px;
         padding: 30px;
         }
         .common_form_section1 label{
         display: block;
         margin-bottom: 7px;
         font-size: 15px;
         }
         .common_form_section1 input{
         width: 100%;
         }
         .col-sm-12{
         float: left;
         }
         .col-sm-8 {
         float: left;
         }
         .pcoded-content.card12.pre-pro {
            float: left;
            width: 100%;
        }
        .pcoded-content.card12.pre-pro table {
            display: table;
        }
        .pcoded-content.card12.pre-pro {
            background: #F6F7FB;
        }
        .modal-backdrop.show {
          opacity: 1;
      }
      .modal-alertsuccess a.close 
      {  
        margin-top: 5px;
        margin-right: 15px;
      } 
      .pop-realted1 
      {
         padding: 10px;
         background-color: #fff;
      }      
      </style>
   </head>
   <div class="LoadingImage"></div>
   <style>
      .LoadingImage {
      display : none;
      position : fixed;
      z-index: 100;
      background-image : url('<?php echo base_url(); ?>assets/images/ajax-loader.gif');
      background-color:#666;
      opacity : 0.4;
      background-repeat : no-repeat;
      background-position : center;
      left : 0;
      bottom : 0;
      right : 0;
      top : 0;
      }
      .pcoded-content.card12.pre-pro {
    min-height: 100vh;
}

@import url('https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900&display=swap');

@import url('https://fonts.googleapis.com/css?family=Lato:300,400,700,900&display=swap');

@import url('https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap');
@import url('https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900&display=swap');


/*
font-family: 'Poppins', sans-serif;
font-family: 'Playfair Display', serif;
font-family: 'Lato', sans-serif;
font-family: 'Montserrat', sans-serif;

*/
input[type=checkbox] { display: inline; }
input[type=checkbox]:before { font-family: DejaVu Sans; }


table {
    width: 100%;
}

h1,h2,h3{
  font-family: 'Lato', sans-serif;

}
body{
  font-family: 'Poppins', sans-serif;
}
.header-top{
  padding-bottom: 10px;
  position: fixed;
  top: -45px;
  left: -45px;
  width: 100%;
}
.account-tax{
  width: 200px;
  height: 70px;
  display: table;
  margin:0 auto;
  position: relative;
}
.pdf-image{
  display: table-cell;
  text-align: center;
  vertical-align: middle;
}
.pdf-title{
  padding:220px 30px 15px;
  text-align: center;
}
.prepared{
  margin: 70px 0 0;
  color: #777;
}
.prepared {
    text-align: right;
    float: right;
    padding-right: 0;
}
.inline-table{
  max-width: 540px;
    text-align: left;
}
.pdf-title h2 {
    margin-bottom: 50px;
    font-size: 25px;
    font-weight: 600;
    color: #000;
}
p{
  font-size: 14px;
  line-height: 18px;
  margin: 6px 0;
}
.first-blog h3 {
    margin-bottom: 8px;
    color: #333;
    font-size: 16px;
}
.first-blog  a{
  color: #333;
}
.first-blog p{
  margin: 3px 0;
  padding-left: 25px;
}
.pdf-table h2 {
    font-weight: 600;
    font-size: 22px;
    margin: 0 0 12px;
}
.table-scroll {
    border: 1px solid #e3e0e1;
}

span.service-title {
    background: #d8d8d8;
    width: 100%;
    display: block;
    padding: 10px 15px;
    font-weight: 600;
    color: #333;
}
table {
  border-collapse: collapse;
}
.table-scroll th:first-child{
  width:53%;
}
.table-scroll th:nth-child(2){
  width: 18%;
}
.table-scroll th:nth-child(3){
  width: 9%;
}
.table-scroll th:nth-child(4){
  width: 20%;
}
.table-scroll th{
  text-align:left;
}
.price1{
    width: 66.3%;
}
.price2 {
    width: 22.5%;
}
.price3{
    width: 11.2%;
}
.table-scroll th {
    border-right: 1px solid #dbdbdb;
    font-size: 15px;
    font-weight: normal;
    background: #f3f3f3;
}
.table-scroll  td,
.table-scroll td tbody tr td { 
  border-right: 1px solid #dbdbdb;
}
.table-scroll td tbody td,.table-scroll th:last-child,td:last-child,
.table-scroll td tbody tr td:last-child{
    border-right: none;
}
tr{
  border-bottom:1px solid #dbdbdb;
}
td,th{
  padding: 8px 10px;
}
td,td p{
  font-size: 13px;
}
.remove-space{
  padding: 0;
}
.table-scroll td tbody  td{
  border-bottom: 1px solid #dbdbdb;
}
.bg{
  background-color: #f1f4f3;
  color: #000;
}
td,p{
  word-wrap: break-word;
}
tr .border-bts{
  border-bottom: 1px solid #dbdbdb;
}
 .border-bts{
  color:#4680ff;
 }
tr:last-child .border-bts{
  border-bottom: none;
}
p{
  font-size: 13px;
  line-height: 21px;
  text-align: justify;
}
.content-blog{
  margin: 20px 0;
}
.grand-total p {
    text-align: right;
}
.grand-total {
  text-align: right;
    padding: 10px 20px;
}
.grand-total span {
    min-width: 101px;
    display: inline-block;
    width: 110px;
    text-align: left;
}
.grand-total span ,.grand-total strong{
  display: inline-block;
  vertical-align: top;
}
.grand-total p{
  text-align: right;
  display: block;
}
.grand-total .boder-top {
    padding-top: 10px;
    border-top: 1px solid #dbdbdb;
    font-weight: 600;
    margin-top: 15px;
    font-size: 14px;
    display: block;
}

.sig-img,.sig-right{
  width: 49%;
  display: inline-block;
  vertical-align: top;  
}
.sig-right{
  text-align: right;
}
.signature-blog p{
  margin-top: 35px;
  height: 34px;
  border-bottom:1px  solid #dbdbdb;
  padding-bottom: 8px;
  width: 230px;
}
.signature-blog table{
  width: 100%;
}
.signature-blog span{
  display: block;
  font-size: 14px;
  color: #000;
  margin-top: 10px;
}
.upper{
  text-transform: uppercase;
}
.signature-blog td:first-child{
  width: 60%;
}
.signature-blog td:last-child{
  width: 40%;
}
   </style>
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/responsive.dataTables.css">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/buttons.dataTables.min.css?ver=2">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/progress-circle.css">
   <body>
   <div class="modal fade" id="accept_proposal" role="dialog">
      <div class="modal-dialog modal-accept-proposal">
         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal"></button>
               <h4 class="modal-title">Accept Proposal</h4>
            </div>
            <div class="modal-body">
               <p class="success_msg" style="color:red;display: none;"> Proposal Accepted </p>
               <input type="hidden" name="id" id="proposal_id" value="<?php echo $records['id']; ?>">
               <p>Do you accept the proposal ?</p>

               <input class="styled-checkbox detail" id="accept_terms" type="checkbox" value="4103">
               <label for="accept_terms">Accept the<a href="<?php echo base_url().'proposal/termsconditions/'.$records['firm_id']; ?>" target="_blank"> Terms and Conditions</a> </label>

               <!-- <input type="checkbox" name="accept_terms" id="accept_terms">Accept the<a href="<?php echo base_url(); ?>proposal/termsconditions" target="_blank"> Terms and Conditions</a> -->

               <p id="terms_error" style="display: none; color:red;"></p>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" id="proposal_accept">Yes</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            </div>
         </div>
      </div>
   </div>


 <div class="modal fade" id="edit_optional" role="dialog">
      <div class="modal-dialog">
         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header">
              <!--  <button type="button" class="close" data-dismiss="modal">&times;</button> -->
               <h4 class="modal-title">Remove optional section</h4>
            </div>
            <div class="modal-body">
              <!--  <p class="success_msg" style="color:red;display: none;"> Proposal Accepted </p> -->
               <input type="hidden" name="id" id="proposal_id" value="<?php echo $records['id']; ?>">
               <p>Do you want to remove this item</p>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" id="yes">Yes</button>
               <button type="button" class="btn btn-default"  id="no">No</button>
            </div>
         </div>
      </div>
   </div>


<div class="modal fade" id="edit_optional1" role="dialog">
      <div class="modal-dialog">
         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header">
              <!--  <button type="button" class="close" data-dismiss="modal">&times;</button> -->
               <h4 class="modal-title">Reselect optional section</h4>
            </div>
            <div class="modal-body">
              <!--  <p class="success_msg" style="color:red;display: none;"> Proposal Accepted </p> -->
               <input type="hidden" name="id" id="proposal_id" value="<?php echo $records['id']; ?>">
               <p>Do you want to select this item</p>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" id="add_yes">Yes</button>
               <button type="button" class="btn btn-default"  id="add_no">No</button>
            </div>
         </div>
      </div>
   </div>

 <div class="modal fade" id="edit_quantity" role="dialog">
      <div class="modal-dialog">
         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Edit Quantity</h4>
            </div>
            <div class="modal-body">
              <!--  <p class="success_msg" style="color:red;display: none;"> Proposal Accepted </p> -->
               <input type="hidden" name="id" id="proposal_id" value="<?php echo $records['id']; ?>">
               <p>Do you want to Edit Quantity</p>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" id="yes_quantity">Yes</button>
               <button type="button" class="btn btn-default" id="no_quanity">No</button>
            </div>
         </div>
      </div>
   </div>



   <div class="modal fade" id="decline_proposal" role="dialog">
      <div class="modal-dialog modal-decline_proposal">
         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal"></button>
               <h4 class="modal-title">Decline Proposal</h4>
            </div>
            <div class="modal-body">
               <p class="success_msg" style="color:red;display: none;"> Proposal Declined </p>
               <input type="hidden" name="id" id="proposal_id" value="<?php echo $records['id']; ?>">
               <p>Do you decline the proposal ?</p>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" id="proposal_decline">Yes</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            </div>
         </div>
      </div>
   </div>
   <div class="modal fade" id="password_popup" role="dialog">
      <div class="modal-dialog">
         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header">
               <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
               <h4 class="modal-title">Proposal Access key</h4>
            </div>
            <div class="modal-body">
               <p class="wrong_msg" style="color:red;display: none;"> Proposal Key Wrong </p>
               <label>Enter Access key:</label>
               <input type="hidden" name="id" id="proposal_id" value="<?php echo $records['id']; ?>">
               <input type="password" name="pdf_password" id="pdf_password" value="">
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" id="access_key">Check</button>
               <!--  <button type="button" class="btn btn-default" data-dismiss="modal">No</button> -->
            </div>
         </div>
      </div>
   </div>
   <div class="theme-loader">
      <div class="ball-scale">
         <div class='contain'>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
            <div class="ring">
               <div class="frame"></div>
            </div>
         </div>
      </div>
   </div>

<div class="modal-alertsuccess alert alert-success accept" style="display:none;">
     <div class="newupdate_alert">
     <a href="#" class="close" data-dismiss="alert" aria-label="close">X</a>
      <div class="pop-realted1">
         <div class="position-alert1">
         Proposal Accepted
         </div>
      </div>
      </div>
 </div>

 <div class="modal-alertsuccess alert alert-success zero-alert" style="display:none;">
      <div class="newupdate_alert">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">X</a>
       <div class="pop-realted1">
          <div class="position-alert1">
          Quantity Cannot Be Empty Or Zero
          </div>
       </div>
       </div>
  </div>

   <div class="modal-alertsuccess alert alert-success qty_notification" style="display:none;">
    <div class="newupdate_alert">
     <a href="#" class="close" data-dismiss="alert" aria-label="close">X</a>
      <div class="pop-realted1">
         <div class="position-alert1">
         Updated successfully
         </div>
      </div>
   </div>
   </div>

   <div class="modal-alertsuccess alert alert-success info" style="display:none;">
    <div class="newupdate_alert">
     <a href="#" class="close" data-dismiss="alert" aria-label="close">X</a>
      <div class="pop-realted1">
         <div class="position-alert1">        
         </div>
      </div>
    </div>
   </div>

   <div class="modal-alertsuccess alert alert-success decline" style="display:none;">
    <div class="newupdate_alert">
     <a href="#" class="close" data-dismiss="alert" aria-label="close">X</a>
      <div class="pop-realted1">
         <div class="position-alert1">
         Proposal Declined
         </div>
      </div>
    </div>
   </div>
   <div class="pcoded-content card12 pre-pro card-question1 preview-proposal-wrapper">
      <div class="pcoded-inner-content">
         <input type="hidden" name="password"  id="password" value="<?php echo $records['password']; ?>">
         <!-- Main-body start -->
         <div class="main-body floating_set">
            <div class="page-wrapper">
               <div class="preview-proposal-header">
               
               <a href="<?php echo base_url(); ?>proposal/proposal_feedback/<?php echo $records['id']; ?>" class="qus-ans btn btn-sm btn-primary f-right">Question? Discuss Now</a>

               <div class="deadline-crm1 floating_set pro_posal">
                  <div class="fixed-div fixed-divproposal">
                   <span class="fixed-tx" style="margin-top: -15px;">
                   <?php if($records['status']!='accepted'){ ?>

                   <a href="<?php echo base_url(); ?>uploads/proposal_pdf/<?php echo $records['pdf_file']; ?>" download><i class="fa fa-download" aria-hidden="true"></i>Download PDF</a>

                   <?php }else{ ?>
                    <a href="<?php echo base_url(); ?>proposal_pdf/signed_document/<?php echo $records['id']; ?>"><i class="fa fa-download" aria-hidden="true"></i>Download PDF</a>
                 <?php  } ?>

                   </span>
                   <?php if($records['status']!='accepted'){ ?>
                   <span class="accpt-pro" style="margin-top: -15px;">
                   <span class="dec-text" <?php if($records['status']=='declined'){ echo 'style="display:none;"';  } ?>><a href="javascript:;" data-toggle="modal" data-target="#decline_proposal">Decline</a> or </span>
                   <a href="javascript:;" data-toggle="modal" data-target="#accept_proposal" class="ac-proposal btn btn-sm btn-primary f-right">Accept</a>
                   </span>
                  <?php } ?>
                </div>
                
             </div>
             <div class="deadline-crm1 floating_set pro_posal edit_pro_posal">
                <center>
                <ul class=""><!-- nav nav-tabs all_user1 md-tabs u-dashboard -->
                  <li class="nav-item"><a class="nav-link"><?php echo $records['proposal_name']; ?></a></li>
                </ul> 
                </center>
             </div>
             </div>
           
            </div>
            <!-- Page body start -->
            <div class=" newproposal_lists">
         <div class="page-body task-d taskpro_posal ScrollStyle">
               <div class=" card5">
               
                <input type="hidden" name="currency_symbol" id="currency_symbol" value="<?php echo $currency_symbols[$firm_currency]; ?>">

               <textarea name="images_section" id="images_section" style="display: none;"><?php echo $records['images_section']; ?></textarea>

                 <input type="hidden" name="id" id="id" value="<?php if(isset($records['id']) && $records['id']!=''){ echo $records['id']; } ?>">

         <input type="hidden" name="item_name" id="item_name" value="<?php if(isset($records['item_name']) && $records['item_name']!=''){ echo $records['item_name']; } ?>">

         <input type="hidden" name="service_category" id="service_category" value="<?php if(isset($records['service_category']) && $records['service_category']!=''){ echo $records['service_category']; } ?>">

         <input type="hidden" name="price" id="price" value="<?php if(isset($records['price']) && $records['price']!=''){ echo $records['price']; } ?>">

         <input type="hidden" name="qty" id="qty" value="<?php if(isset($records['qty']) && $records['qty']!=''){ echo $records['qty']; } ?>">

         <input type="hidden" name="tax" id="tax" value="<?php if(isset($records['tax']) && $records['tax']!=''){ echo $records['tax']; } ?>">
         
         <input type="hidden" name="discount" id="discount" value="<?php if(isset($records['discount']) && $records['discount']!=''){ echo $records['discount']; } ?>">

         <input type="hidden" name="unit" id="unit" value="<?php if(isset($records['unit']) && $records['unit']!=''){ echo $records['unit']; } ?>">

         <input type="hidden" name="description" id="description" value="<?php if(isset($records['description']) && $records['description']!=''){ echo $records['description']; } ?>">

         <input type="hidden" name="product_name" id="product_name" value="<?php if(isset($records['product_name']) && $records['product_name']!=''){ echo $records['product_name']; } ?>">

         <input type="hidden" name="product_category_name" id="product_category_name" value="<?php if(isset($records['product_category_name']) && $records['product_category_name']!=''){ echo $records['product_category_name']; } ?>">

         <input type="hidden" name="product_price" id="product_price" value="<?php if(isset($records['product_price']) && $records['product_price']!=''){ echo $records['product_price']; } ?>">

         <input type="hidden" name="product_qty" id="product_qty" value="<?php if(isset($records['product_qty']) && $records['product_qty']!=''){ echo $records['product_qty']; } ?>">

         <input type="hidden" name="product_tax" id="product_tax" value="<?php if(isset($records['product_tax']) && $records['product_tax']!=''){ echo $records['product_tax']; } ?>">
         <input type="hidden" name="product_discount" id="product_discount" value="<?php if(isset($records['product_discount']) && $records['product_discount']!=''){ echo $records['product_discount']; } ?>">            
         <input type="hidden" name="product_description" id="product_description" value="<?php if(isset($records['product_description']) && $records['product_description']!=''){ echo $records['product_description']; } ?>">


          <input type="hidden" name="subscription_name" id="subscription_name" value="<?php if(isset($records['subscription_name']) && $records['subscription_name']!=''){ echo $records['subscription_name']; } ?>">
         <input type="hidden" name="subscription_category_name" id="subscription_category_name" value="<?php if(isset($records['subscription_category_name']) && $records['subscription_category_name']!=''){ echo $records['subscription_category_name']; } ?>">
         <input type="hidden" name="subscription_price" id="subscription_price" value="<?php if(isset($records['subscription_price']) && $records['subscription_price']!=''){ echo $records['subscription_price']; } ?>">
         <input type="hidden" name="subscription_unit" id="subscription_unit" value="<?php if(isset($records['subscription_unit']) && $records['subscription_unit']!=''){ echo $records['subscription_unit']; } ?>">
         <input type="hidden" name="subscription_tax" id="subscription_tax" value="<?php if(isset($records['subscription_tax']) && $records['subscription_tax']!=''){ echo $records['subscription_tax']; } ?>">
         <input type="hidden" name="subscription_discount" id="subscription_discount" value="<?php if(isset($records['subscription_discount']) && $records['subscription_discount']!=''){ echo $records['subscription_discount']; } ?>">   

         <input type="hidden" name="subscription_description" id="subscription_description" value="<?php if(isset($records['subscription_description']) && $records['subscription_description']!=''){ echo $records['subscription_description']; } ?>">

                  <center id="proposal_contents" class="ques-align1">                          

                     <div id="proposal">
                     <?php 

                       $proposal_content_new  = $records['proposal_contents'];  
                       $signature = $this->db->query('select * from pdf_signatures where proposal_no = "'.$records['id'].'" Order By id Desc Limit 1')->row_array();

                       if($records['email_signature'] == 'on')
                       {
                          $replace_data = '<div class="get-options choose ui-draggable ui-draggable-handle" data-id="e_signature"><img src="'.base_url().$signature['signature_path'].'" style="width:200px;display: block;"></div>';
                       }
                       else
                       {
                          $replace_data = '<div class="get-options choose ui-draggable ui-draggable-handle" data-id="e_signature"></div>';
                       }

                       $doc = new DOMDocument();      
                       $doc->loadHTML(preg_replace("/\s\s+/", "", $proposal_content_new));                
                       $xpath = new DOMXPath($doc);      
                       $elementsThatHaveData_id = $xpath->query('//*[@data-id="e_signature"]');     

                       foreach($elementsThatHaveData_id as $value) 
                       {   
                          $string = $doc->saveHTML($value);
                          $string = preg_replace("/>\s+/", ">", $string);
                        
                          $proposal_content_new = str_replace($string,$replace_data,preg_replace("/\s\s+/", "", $proposal_content_new)); 
                       } 
                       
                       echo html_entity_decode($proposal_content_new); 

                     ?>
                     </div>                          
                   
                  </center>

                    
               </div>
            </div>

                  <?php
                       if((isset($records['document_attachment']) && $records['document_attachment']!='') || (isset($records['attachment']) && $records['attachment']!='') )
                       {   ?>
                  <!-- Image Code -->
                  <div id="tabs-4">
                    <ul class="ques-quote1">

                    
                    <?php
                       if(isset($records['document_attachment']) && $records['document_attachment']!='')
                       {         

                       $images=explode(',',$records['document_attachment']);
                       
                       for($i=0;$i<count($images);$i++){
                           $imgExts = array("gif", "jpg", "jpeg", "png", "tiff", "tif");
                           $urls = $images[$i];
                           $urlExt = pathinfo($urls, PATHINFO_EXTENSION);
                           if (in_array($urlExt, $imgExts)) {
                            // echo 'Yes, '.$url.' is an Image'; ?>



                      
                    <li class="documents"> 

                     <a href="<?php echo $urls; ?>" download><img src="<?php echo $urls; ?>"  alt=""></a>
                    <span class="tooltiptext"><?php echo  substr($urls,38); ?></span>
                    <!--  <span class="closeicon1 document_attachment_images" id="<?php echo $urls; ?>" ><span class="close-ico">&times;</span></span> -->

                    </li>
                    <?php  }else{
                       $imgExts1 = array("pdf", "doc", "docs", "xlsx", "xls", "csv");
                       $urls = $images[$i];
                       $urlExt1 = pathinfo($urls, PATHINFO_EXTENSION);
                       if (in_array($urlExt1, $imgExts1)) {
                       
                        //echo 'Yes, '.$url.' is an Document'; ?>   
                    <li class="documents">     
                    <a href="<?php echo base_url(); ?>assets/images/doc-icon.jpg" id="<?php echo $i; ?>" class="document_attachment_images" download><img src="<?php echo base_url(); ?>assets/images/doc-icon.jpg" alt=""></a>
                    <span class="tooltiptext"><?php echo  substr($urls,38); ?></span>
                    <!-- <span class="closeicon1 document_attachment_images" id="<?php echo $urls; ?>"><span class="close-ico">&times;</span> -->
                    </span>
                    </li>
                    <?php }
                       }
                       }
                       } ?>
                       
                         <input type="hidden" name="attach"  id="attach"   value="<?php if(isset($records['attachment']) && $records['attachment']!='')
                       { echo $records['attachment']; } ?>"> 
                       
                      <?php if(isset($records['attachment']) && $records['attachment']!='')
                       {
                       
                       $images=explode(',',$records['attachment']);
                       
                       for($i=0;$i<count($images);$i++){
                       $imgExts = array("gif", "jpg", "jpeg", "png", "tiff", "tif");
                       $urls = $images[$i];
                       $urlExt = pathinfo($urls, PATHINFO_EXTENSION);
                       if (in_array($urlExt, $imgExts)) {
                        // echo 'Yes, '.$url.' is an Image'; ?>

                       
                    <li class="documents">
                    <?php
                       $url1=pathinfo($urls)['filename'];
                               $strlower = preg_replace("~[\\\\/:*?'&,<>|]~", '', $url1);
                               $lesg = str_replace(' ', '_', $strlower);
                             //  $lesg_s = str_replace('-', '_', $lesg);
                               $lesgs = str_replace('.', '_', $lesg);
                       
                               //echo pathinfo($url)['extension']; // "ext"
                       //$lesgs=pathinfo($url)['filename']; // "file"
                       $ext = pathinfo($urls, PATHINFO_EXTENSION);
                                ?>
                    <a href="<?php echo base_url(); ?>attachment/<?php echo $lesgs.'.'.$ext; ?>"  download><img src="<?php echo base_url(); ?>attachment/<?php echo $lesgs.'.'.$ext; ?>" alt=""></a>
                    <span class="tooltiptext"><?php echo  $urls; ?></span>
                   <!--  <span class="closeicon1 attachment_images" id="<?php echo  $urls; ?>"><span class="close-ico">&times;</span></span> -->
                   </li>
                    <?php  }else{
                       $imgExts1 = array("pdf", "doc", "docs", "xlsx", "xls", "csv");
                       $urls = $images[$i];
                       $urlExt1 = pathinfo($urls, PATHINFO_EXTENSION);
                       if (in_array($urlExt1, $imgExts1)) {
                       
                        //echo 'Yes, '.$url.' is an Document'; ?>   
                    <li class="documents doc_val">     
                    <a href="<?php echo  base_url().'attachment/'.$urls; ?>" download><?php echo  $urls; ?></a>
                    <span class="tooltiptext"><?php echo  $urls; ?></span>
                    <!-- <span class="closeicon1 attachment_images" id="<?php echo  $urls; ?>"><span class="close-ico">&times;</span></span> -->
                    </li>
                    <?php }
                       }
                       }
                       }         
                       ?>


                    </ul>
                  </div>
                  <?php } ?>
                  </div>
         </div>
            </div>
         </div>
         
         </div>
     
   <!-- Warning Section Starts -->
   
   <!-- Warning Section Ends -->
   <?php //$this->load->view('includes/session_timeout');?>
   <?php $fun = $this->uri->segment(2);?>
   <!-- Required Jquery -->
   <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-ui/js/jquery-ui.min.js"></script>
   <script src="<?php echo base_url();?>assets/js/timezones.full.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>bower_components/popper.js/js/popper.min.js"></script>  
   <script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap/js/bootstrap.min.js"></script>  
   <!-- j-pro js -->
   <!-- <script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.ui.min.js"></script> -->
   <script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.maskedinput.min.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.j-pro.js"></script>
   <!-- jquery slimscroll js -->
   <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
   <!-- modernizr js -->
   <script type="text/javascript" src="<?php echo base_url();?>bower_components/switchery/js/switchery.min.js"></script>
   <!--  
      <script type="text/javascript" src="<?php echo base_url();?>bower_components/modernizr/js/css-scrollbars.js"></script> -->
   <!-- <script src="<?php echo base_url();?>assets/js/mmc-common.js"></script>
      <script src="<?php echo base_url();?>assets/js/mmc-chat.js"></script>
      <script type="text/javascript" src="<?php echo base_url();?>assets/js/chat.js"></script> -->
   <!-- Custom js -->
   <script src="<?php echo base_url();?>assets/js/pcoded.min.js"></script>
   <script src="<?php echo base_url();?>assets/js/demo-12.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>assets/js/masonry.pkgd.min.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script> 
   <script type="text/javascript" src="<?php echo base_url();?>assets/js/common_script.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
   <script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
   <script src="<?php echo base_url();?>assets/js/mock.js"></script>
   <!-- <script src="https://use.fontawesome.com/86c8941095.js"></script> -->
   <?php $url = $this->uri->segment('2');?>
   <?php if ($url == 'step_proposal' || $url == 'template_design' || $url=='templates') { ?>
   <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
   <?php } ?>
   <script src="<?php echo base_url();?>assets/js/bootbox.min.js"></script>
   <script src="<?php echo base_url();?>assets/js/debounce.js"></script>
   <script src="<?php echo base_url();?>assets/js/bootstrap-colorpicker.min.js"></script>
   <!--   <script src="<?php //echo base_url();?>assets/js/bootstrap-slider.min.js"></script> -->
   <script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.js"></script>
   <script src="//cdn.jsdelivr.net/medium-editor/latest/js/medium-editor.min.js"></script>
   <script src="<?php echo base_url(); ?>assets/js/creative.tools.js"></script>
   <script src="<?php echo base_url(); ?>assets/js/html2canvas.js"></script>
   <script src="<?php echo base_url(); ?>assets/js/image-edit.js"></script>
   <script src="<?php echo base_url(); ?>assets/js/editor.js"></script>


    <script src="<?php echo base_url(); ?>assets/signature/js/numeric-1.2.6.min.js"></script> 
    <script src="<?php echo base_url(); ?>assets/signature/js/bezier.js"></script>
    <script src="<?php echo base_url(); ?>assets/signature/js/jquery.signaturepad.js"></script>     
    <script type='text/javascript' src="https://github.com/niklasvh/html2canvas/releases/download/0.4.1/html2canvas.js"></script>
    <script src="<?php echo base_url(); ?>assets/signature/js/json2.min.js"></script> 
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.number.min.js"></script>
   <!--   <script src="<?php //echo base_url();?>js/jquery.signaturepad.js"></script>
      <script src="<?php //echo base_url();?>js/json2.min.js"></script> -->
   <script>
      $(document).ready(function() {


        $('#signArea').signaturePad({drawOnly:true, drawBezierCurves:true, lineTop:90});
      });

      function clearSignature()
      { 
          $('#signArea').signaturePad().clearCanvas();
      }
      

      
      //  $("ul#owl-demo1 li").on('click',function() {
      
       
      
      //    $(this).find('ul.dropdown-menu').slideToggle();
      //  });
      
      // });





      $(document).ready(function() 
      {    
          $('.currency_symbol').text('');     
          $('.currency_symbol').html($("#currency_symbol").val());
          var image=$("#images_section").val();
         
          $('.images').attr('src', image);
         // $("#images_div").each(function(){
         //      $(this).html('');
         //      var image=$("#images_section").val();

         //      var content='<img src="'+image+'" class="images">';
         //    console.log(content);
         //      $(this).html('<div class="image_content">'+content+'></div>');
         //    });


        //  $(".image_content").html('');
        //  console.log($(".image_content").attr('class'));

        

        // $(".image_content").append(content);


       







         // $('#proposal_contents_details p').each(function() {
         //    var $this = $(this);
         //    if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
         //    $this.remove();
         //    });
          //  $('#proposal_contents_details [contenteditable]').removeAttr('contenteditable');

          $("span.cke_reset").each(function () {
          $(this).remove();
          });

          $("div.save-remove").each(function () {
          $(this).remove();
          });  

          $("div.overly").each(function () {
          $(this).remove();
          }); 

          $("button.copy").each(function () {
          $(this).remove();
          });    


          $("div.popover-lg").each(function () {
          $(this).remove();
          });          


          $("span.cke_widget_wrapper").each(function () {
          $(this).replaceWith($(this).text());
          });

           $(".quantity_class").each(function () {
          $(this).css('display','none');
          });

            $(".quantity_cla").each(function () {
          $(this).css('display','inline-block');
          });
         // console.log(demo);

      //    $("#proposal_contents").html(demo);

      });
      
      $( document ).ready(function() {
      
      
          var date = $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
      
          // Multiple swithces
          var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));
      
          elem.forEach(function(html) {
              var switchery = new Switchery(html, {
                  color: '#1abc9c',
                  jackColor: '#fff',
                  size: 'small'
              });
          });
      
          $('#accordion_close').on('click', function(){
                  $('#accordion').slideToggle(300);
                  $(this).toggleClass('accordion_down');
          });
      
      
          $('.chat-single-box .chat-header .close').on('click', function(){
            $('.chat-single-box').hide();
          });
      
      
      var resrow = $('#responsive-reorder').DataTable({
          rowReorder: {
              selector: 'td:nth-child(2)'
          },
          responsive: true
      });
      });
   </script>
  
   <!-- chat box -->
   <!-- <div class="chat-box cus-tinybox">
      <ul class="text-right boxs">
         <li class="chat-single-box card-shadow bg-white active chatbox_2340" data-id="2340">
            <div class="had-container">
               <div class="chat-header p-10 bg-gray">
                  <div class="user-info d-inline-block f-left">
                     <div class="box-live-status d-inline-block m-r-10 bg-danger"></div>
                     <a href="#">demouser2</a>
                  </div>
                  <div class="box-tools d-inline-block"><a href="#" class="mini"><i class="icofont icofont-minus f-20 m-r-10"></i></a><a class="close" href="#"><i class="icofont icofont-close f-20"></i></a></div>
               </div>
               <div class="chat-body p-10">
                  <div class="message-scrooler">
                     <input type="hidden" id="ajax_value_2340" name="ajax_value">
                     <div class="messages our_message_2340"></div>
                  </div>
               </div>
               <div class="chat-footer b-t-muted">
                  <div class="input-group write-msg"><input type="text" class="form-control input-value" name="text_message" data-id="2340" id="text_message_2340" placeholder="Type a Message"><span class="input-group-btn"><button id="paper-btn" class="btn btn-primary message_send" data-id="2340" type="button"><i class="icofont icofont-paper-plane"></i></button></span></div>
               </div>
            </div>
         </li>
      </ul>
      </div> -->
   <!-- chat box -->
   <!-- modal 1-->
   <div class="modal fade all_layout_modal" id="default-Modal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h4 class="modal-title">Search Companines House</h4>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <div class="modal-body">
               <div class="input-group input-group-button input-group-primary modal-search">
                  <input type="text" class="form-control" placeholder="Search here..." id="searchCompany">
                  <button class="btn btn-primary input-group-addon" id="basic-addon1">
                  <i class="ti-search" aria-hidden="true"></i>
                  </button>
               </div>
               <div class="british_gas1 trading_limited" id="searchresult">
               </div>
               <div class="british_view" style="display:none" id="companyprofile">
                  <!-- <div class="br_company_profile">
                     <h3>british gas trading limited</h3>
                     <div class="company_numbers">
                     <span>Company Number</span>    
                     <p>03078711</p>
                     </div>
                     <div class="company_numbers">
                     <span>Company Status</span>    
                     <p>Active</p>
                     </div>    
                     <div class="company_numbers">
                     <span>Incorporated Date</span>    
                     <p>06/07/1995</p>
                     </div>
                     <div class="company_numbers">
                     <span>Accounts Reference Date</span>    
                     <p>31/12</p>
                     </div>
                     <div class="company_numbers">
                     <span>Charges</span>    
                     <p><strong>1</strong></p>
                     </div>
                     <div class="company_numbers">
                     <span>Current Directors</span>    
                     <p>R.O.Y. International PO Box 13056 ISL-61130 TEL-AVIV ISRAEL</p>
                     </div>
                     <div class="company_numbers">
                     <span>Registered Directors</span>    
                     <p>The Israel Philatelic Service 12 Shderot Yerushalayim 68021 Tel Aviv - Yafo ISRAEL</p>
                     </div>
                     <div class="company_chooses">
                     <a href="#">select company</a>
                     <a href="#">View on Companies House</a>
                     </div>
                     </div> -->
               </div>
               <div class="main_contacts" id="selectcompany" style="display:none">
                  <h3>british gas trading limited</h3>
                  <div class="trading_tables">
                     <div class="trading_rate">
                        <p>
                           <span><strong>BARBARO,Gab</strong></span>
                           <span>Born 1971</span>
                           <span><a href="#">Use as Main Contact</a></span>
                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <!-- modalbody -->
         </div>
      </div>
   </div>
   <!-- modal-close -->
   <!--modal 2-->
   <div class="modal fade all_layout_modals" id="exist_person" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h4 class="modal-title">Select Existing Person</h4>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <div class="modal-body">
               <div class="main_contacts" id="main_contacts" style="display:block">     
               </div>
            </div>
            <!-- modalbody -->
         </div>
      </div>
   </div>


   <div class="modal fade EmailSignature_cls" id="EmailSignature" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>

                </button>
                 <h4 class="modal-title" id="myModalLabel">E-Signature</h4>

            </div>
            <div class="modal-body">
             <?php $this->load->view('proposal/my_sign.php'); ?>
            </div>
            <div class="modal-footer">
                <button id="btnSaveSign" class="btn btn-default">Save Signature</button>
                <button  id="clearSig" class="btn btn-primary save" onclick="clearSignature();">Clear Signature</button>
                <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                <!-- <button type="button" class="btn btn-primary save">Save changes</button> -->
            </div>
        </div>
    </div>
</div>
   <!-- modal-close -->
   <!-- modal 2-->
 </body>
</html>
<script type="text/javascript">

  function Quantity_checking(qtyy)
  {
     $("#edit_quantity").modal({backdrop: 'static', keyboard: false, show: true});

     $("#yes_quantity").click(function()
     {
        $("#edit_quantity").modal('hide');
        $(qtyy).css('display','none');
        $(qtyy).next(".update_quantity").css('display','block');
        $(qtyy).prev().prev('.quantity_class').css('display','inline-block');
        $(qtyy).prev('.quantity_cla').css('display','none');
     });

     localStorage.setItem("alert_section", "alert");

     $("#no_quanity").click(function()
     {
        $(qtyy).prop('checked', false);
        $("#edit_quantity").modal('hide');

     });
  }
 
 function edit_optional(option)
 {
     var parent_element = $(this).parent().parent().parent().parent();

     if($(option).is(":checked"))
     {
        $("#edit_optional1").modal({backdrop: 'static', keyboard: false, show: true});
     }
     else
     {
        $("#edit_optional").modal({backdrop: 'static', keyboard: false, show: true});
     }

     $("#yes, #add_yes").click(function()
     {  
        $("#edit_optional").modal('hide');
        $("#edit_optional1").modal('hide');
         
        if($(option).data('id')=='service')
        {
            var arr1=$("#item_name").val().split(',');
            var arr2=$("#service_category").val().split(',');
            var arr3=$("#price").val().split(',');
            var arr4=$("#qty").val().split(',');
            var arr5=$("#tax").val().split(',');
            var arr6=$("#discount").val().split(',');
            var arr7=$("#unit").val().split(',');
            var arr8=$("#description").val().split(',');

            arr1.splice($(option).attr('id'), 1);
            arr2.splice($(option).attr('id'), 1);
            arr3.splice($(option).attr('id'), 1);
            arr4.splice($(option).attr('id'), 1);
            arr5.splice($(option).attr('id'), 1);
            arr6.splice($(option).attr('id'), 1);
            arr7.splice($(option).attr('id'), 1);
            arr8.splice($(option).attr('id'), 1);

            var item_name =arr1;
            var service_category=arr2;
            var price=arr3;
            var qty=arr4;
            var tax=arr5;
            var discount=arr6;
            var unit=arr7;
            var description=arr8;

            if($(option).is(":checked"))
            {
               $(parent_element).find('.quantity').val($.trim($(parent_element).find('.quantity_cla').html()));
               $(parent_element).find('.services_price').val($.trim($(parent_element).find('.price_section').html()));
               $(option).removeClass('Remove_service');           
               $(parent_element).removeAttr('style');   
               $(option).attr('checked','checked');                      
            }
            else
            {
               $(parent_element).find('.quantity').val(0);
               //$(option).parents('.service_tr').find('.edit_service_tax').val(0);
               //$(option).parents('.service_tr').find('.edit_service_discount').val(0); 
               $(parent_element).find('.services_price').val(0); 
               $(option).addClass('Remove_service');
               $(parent_element).css('background-color','#ddd');
               $(option).removeAttr('checked');
            }
            // $(option).parents('.service_tr').css('background-color','#ddd');
            $(parent_element).find('.optional_check').prop('checked',false);
            // $(option).parents('.service_tr').find('.optional_check').prop("disabled", true);
            // $(option).parents('.service_tr').find('.check_quantity').prop("disabled", true);
         /*   if(item_name.length==0)
            {
             var item_name='';
            }

            if(price.length==0)
            {
             var price='';       
            }

            if(qty.length==0)
            {
             var qty='';       
            }

            if(unit.length==0)
            {
             var unit='';       
            }

            if(tax.length==0)
            {
             var tax='';       
            }

            if(discount.length==0)
            {
             var discount='';       
            }

            if(description.length==0)
            {
             var description='';       
            }*/
            edit_checking();

            var proposal=$("#proposal").html();
            var id=$("#id").val();
            //var formData={'item_name':item_name,'price':price,'unit':unit,'qty':qty,'tax':tax,'discount':discount,'description':description,'service_category':service_category,'proposal_contents':proposal,'id':id};

            var formData={'proposal_contents':proposal,'id':id};

            $.ajax(
            {
              url: '<?php echo base_url(); ?>/proposal_pdf/optional_section_service',
              type : 'POST',
              data : formData, 

              beforeSend: function() 
              {
                $(".LoadingImage").show();
              },
              success: function(data) 
              {
                $(".LoadingImage").hide();       
                location.reload(); 
              }
            });
        }

        if($(option).data('id')=='product')
        {
           var arr1=$("#product_name").val().split(',');
           var arr2=$("#product_category_name").val().split(',');
           var arr3=$("#product_price").val().split(',');
           var arr4=$("#product_qty").val().split(',');
           var arr5=$("#product_tax").val().split(',');
           var arr6=$("#product_description").val().split(',');
           var arr7=$("#product_discount").val().split(',');
          
           arr1.splice($(option).attr('id'), 1);
           arr2.splice($(option).attr('id'), 1);
           arr3.splice($(option).attr('id'), 1);
           arr4.splice($(option).attr('id'), 1);
           arr5.splice($(option).attr('id'), 1);
           arr6.splice($(option).attr('id'), 1);
           arr7.splice($(option).attr('id'), 1);
         
           var product_name=arr1;
           var product_category_name=arr2;
           var product_price=arr3;
           var product_qty=arr4;
           var product_tax=arr5;
           var product_description=arr6;
           var product_discount=arr7;

         if ($(option).is(":checked"))
         { 
            $(parent_element).find('.quantity').val($.trim($(parent_element).find('.quantity_cla').html()));
            $(parent_element).find('.products_price').val($.trim($(parent_element).find('.price_section').html()));
            $(option).removeClass('Remove_service');           
            $(parent_element).removeAttr('style');
            $(option).attr('checked','checked');
         }
         else
         {
            $(parent_element).find('.quantity').val(0);
            //$(option).parents('.product_tr').find('.edit_product_tax').val(0);
            // console.log(tax);
            //$(option).parents('.product_tr').find('.edit_product_discount').val(0); 
            $(parent_element).find('.products_price').val(0); 
            $(option).addClass('Remove_service');
            $(parent_element).css('background-color','#ddd');
            $(option).removeAttr('checked');
         }

         //$(option).parents('.product_tr').css('background-color','#ddd'); 
         $(parent_element).find('.optional_check').prop('checked',false);
        // $(option).parents('.product_tr').find('.optional_check').prop("disabled", true);
        // $(option).parents('.product_tr').find('.check_quantity').prop("disabled", true);
         edit_checking();
         var proposal=$("#proposal").html();
         
         //    if(product_name.length==0){
         //    var product_name='';
         //    console.log('empty');
         // }

         //    if(product_price.length==0){
         //    var product_price='';
         //    console.log('empty');
         // }

         //    if(product_qty.length==0){
         //    var product_qty='';
         //    console.log('empty');
         // }
         //    if(product_tax.length==0){
         //    var product_tax='';
         //    console.log('empty');
         // }
         //    if(product_discount.length==0){
         //    var product_discount='';
         //    console.log('empty');
         // }
         //    if(product_description.length==0){
         //    var product_description='';
         //    console.log('empty');
         // }
         //    if(product_category_name.length==0){
         //    var product_category_name='';
         //    console.log('empty');
         // }

         var id=$("#id").val();


         // var formData={'product_name':product_name,'product_price':product_price,'product_qty':product_qty,'product_tax':product_tax,'product_discount':product_discount,'product_description':product_description,'product_category_name':product_category_name,'proposal_contents':proposal,'id':id};
         var formData={'proposal_contents':proposal,'id':id};

          $.ajax(
          {
              url: '<?php echo base_url(); ?>/proposal_pdf/optional_section_product',
              type : 'POST',
              data : formData,   

              beforeSend: function() 
              {
                $(".LoadingImage").show();
              },
              success: function(data) 
              {
                $(".LoadingImage").hide();
                location.reload();
              }
          });
        }   

        if($(option).data('id')=='subscription')
        {
           var arr1=$("#subscription_name").val().split(',');
           var arr2=$("#subscription_category_name").val().split(',');
           var arr3=$("#subscription_price").val().split(',');
           var arr4=$("#subscription_unit").val().split(',');
           var arr5=$("#subscription_tax").val().split(',');
           var arr6=$("#subscription_discount").val().split(',');
           var arr7=$("#subscription_description").val().split(',');
           
           arr1.splice($(option).attr('id'), 1);
           arr2.splice($(option).attr('id'), 1);
           arr3.splice($(option).attr('id'), 1);
           arr4.splice($(option).attr('id'), 1);
           arr5.splice($(option).attr('id'), 1);
           arr6.splice($(option).attr('id'), 1);
           arr7.splice($(option).attr('id'), 1);
           // arr8.splice($(option).attr('id'), 1);
           var subscription_name= arr1;
           var subscription_category_name= arr2;
           var subscription_price= arr3;
           var subscription_unit= arr4;
           var subscription_tax= arr5;
           var subscription_discount= arr6;
           var subscription_description= arr7;
          
           if ($(option).is(":checked"))
           {
              $(parent_element).find('.quantity').val($.trim($(parent_element).find('.quantity_cla').html()));
              $(parent_element).find('.subscription_prices').val($.trim($(parent_element).find('.price_section').html()));
              $(option).removeClass('Remove_service');           
              $(parent_element).removeAttr('style');
              $(option).attr('checked','checked');
           }
           else
           {
              $(option).addClass('Remove_service');
              $(parent_element).find('.subscription_prices').val(0);
              $(parent_element).find('.quantity').val(0);
              //$(option).parents('.subscription_tr').find('.edit_subscription_tax').val(0);
              //$(option).parents('.subscription_tr').find('.edit_subscription_discount').val(0); 
              $(parent_element).css('background-color','#ddd'); 
              $(option).removeAttr('checked');
           }

           $(parent_element).find('.optional_check').prop('checked',false);

           // $(option).parents('.subscription_tr').find('.optional_check').prop("disabled", true);
           // $(option).parents('.subscription_tr').find('.check_quantity').prop("disabled", true);

           edit_checking();
           var proposal=$("#proposal").html();
           var id=$("#id").val();

         /*  if(subscription_name.length==0)
           {
              var subscription_name='';
           }
           
           if(subscription_price.length==0)
           {
              var subscription_price='';
           }

           if(subscription_unit.length==0)
           {
              var subscription_unit='';
           }
           
           if(subscription_tax.length==0)
           {
              var subscription_tax='';
           }
           
           if(subscription_discount.length==0)
           {
              var subscription_discount='';
           }
           
           if(subscription_description.length==0)
           {
              var subscription_description='';
           }
           
           if(subscription_category_name.length==0)
           {
              var subscription_category_name='';
           }*/

           // var formData={'subscription_name':subscription_name,'subscription_price':subscription_price,'subscription_unit':subscription_unit,'subscription_tax':subscription_tax,'subscription_discount':subscription_discount,'subscription_description':subscription_description,'subscription_category_name':subscription_category_name,'proposal_contents':proposal,'id':id};

           var formData={'proposal_contents':proposal,'id':id};

           $.ajax(
           {
             url: '<?php echo base_url(); ?>/proposal_pdf/optional_section_subscription',
             type : 'POST',
             data : formData,                    
             beforeSend: function() 
             {
               $(".LoadingImage").show();
             },
             success: function(data) 
             {
               $(".LoadingImage").hide();
               location.reload();  
             }
           });
        }     
     });

     $("#no").click(function()
     {
        $(option).prop('checked', true);

        $("#edit_optional").modal('hide');
        $("#edit_optional1").modal('hide');

     });

     $("#add_no").click(function()
     {
        $(option).prop('checked', false);

        $("#edit_optional").modal('hide');
        $("#edit_optional1").modal('hide');

     });
 }

 function edit_checking()
 {  
    var sum=0;    
    var parent;
    var qty;
    var tax;
    var discount;
    var price;
    var amount;
    var tax_amount;
    var final;

    if($('.services_price').val()!='')
    { 
       $('.services_price').each(function() 
       {
          parent = $(this).parent().parent().parent().parent();

          if($(parent).find('.quantity').val() == "" || $(parent).find('.quantity').val() <= '0')
          {
              $(".zero-alert").modal('show');
              location.reload();
              die();
          }
          else if($(parent).find('.optional_check').prop('checked') == true || $(parent).find('.optional_check').attr('class') == undefined)
          {            
              $(parent).find('.quantity_cla').html('');
              $(parent).find('.quantity_cla').html($(parent).find('.quantity').val());              
              qty = $.trim($(parent).find('.quantity').val());        
              tax = Number($(parent).find('.edit_service_tax').val()); 
              discount = Number($(parent).find('.edit_service_discount').val()); 
              price = Number($(this).val()); 
              amount = Number(price * qty).toFixed(2);   
              tax = Number(tax/100).toFixed(2);       
              discount = Number(discount/100).toFixed(2);  

              tax_amount = Number(Number(amount) + Number(amount*tax)).toFixed(2); 
              final = Number(Number(tax_amount) - Number(tax_amount*discount)).toFixed(2); 
              
              sum += Number(final);             
          }
       });
    }

    var sum1=0;

    if($('.products_price').val()!='')
    {  
        $('.products_price').each(function() 
        {
          parent = $(this).parent().parent().parent().parent();
          
          if($(parent).find('.quantity').val() == "" || $(parent).find('.quantity').val() <= '0')
          {
              $(".zero-alert").modal('show');
              location.reload();
              die();
          }
          else if($(parent).find('.optional_check').prop('checked') == true || $(parent).find('.optional_check').attr('class') == undefined)
          {        
              $(parent).find('.quantity_cla').html('');
              $(parent).find('.quantity_cla').html($(parent).find('.quantity').val());              
              qty = $.trim($(parent).find('.quantity').val());             
              tax = Number($(parent).find('.edit_product_tax').val()); 
              discount = Number($(parent).find('.edit_product_discount').val()); 
              price = Number($(this).val()); 
              amount = Number(price * qty).toFixed(2);  
              tax = Number(tax/100).toFixed(2);       
              discount = Number(discount/100).toFixed(2);  

              tax_amount = Number(Number(amount) + Number(amount*tax)).toFixed(2); 
              final = Number(Number(tax_amount) - Number(tax_amount*discount)).toFixed(2); 
              
              sum1 += Number(final);             
          }                 
        });    
    }
  
    var sum2=0;

    if($('.subscription_prices').val()!='')
    {    
        $('.subscription_prices').each(function() 
        {  
          parent = $(this).parent().parent().parent().parent();

          if($(parent).find('.quantity').val() == "" || $(parent).find('.quantity').val() <= '0')
          {
              $(".zero-alert").modal('show');
              location.reload();
              die();
          }
          else if($(parent).find('.optional_check').prop('checked') == true || $(parent).find('.optional_check').attr('class') == undefined)
          {
             $(parent).find('.quantity_cla').html('');
             $(parent).find('.quantity_cla').html($(parent).find('.quantity').val());              
             qty = $.trim($(parent).find('.quantity').val());            
             tax = Number($(parent).find('.edit_subscription_tax').val()); 
             discount = Number($(parent).find('.edit_subscription_discount').val()); 
             price = Number($(this).val()); 
             amount = Number(price * qty).toFixed(2);  
             tax = Number(tax/100).toFixed(2);       
             discount = Number(discount/100).toFixed(2);  

             tax_amount = Number(Number(amount) + Number(amount*tax)).toFixed(2); 
             final = Number(Number(tax_amount) - Number(tax_amount*discount)).toFixed(2); 
            
             sum2 += Number(final);
          }         
        });
    }

     var cal = Number(Number(sum) + Number(sum1) + Number(sum2)).toFixed(2);
     var sub_total = cal;

     if($("#tax_option").val()=='total_tax')
     {
        var overalltax = Number($('.over_alltax').val()); 
        var taxes = Number(overalltax/100);  
        cal = Number(Number(cal) + (Number(cal*taxes))).toFixed(2); 
        $('.ptag_tax').html('');    
        $('.ptag_tax').html(Number(cal*taxes).toFixed(2));  
     }

     if($("#discount_option").val()=='total_discount')
     {
         if($('#discount_rate').val() != '')
         {
            var overalldiscount = Number(Number(sub_total)*(Number($('#discount_rate').val())/Number(100))).toFixed(2);  
         }
         else
         {
            var overalldiscount = Number($('.over_alldiscount').val());
         }

         cal = Number(Number(cal) - Number(overalldiscount)).toFixed(2);
         $('.ptag_discount').html('');    
         $('.ptag_discount').html(Number(overalldiscount).toFixed(2));
     }

     $(".Sub_total").html('');
     //var value=Number(sub_total).tofixed(2)
     $(".Sub_total").html(Number(sub_total).toFixed(2));
     $(".sub_total_field").val(Number(sub_total).toFixed(2));
     $(".grand_total_1").html('');
     $(".grand_total_1").html(Number(cal).toFixed(2));
     $(".grand_total_field").val(Number(cal).toFixed(2));

      var product_qtys = [];
      $("input.product_qtys").each(function(i, sel)
      {
        var selectedVal = $(sel).val();
        product_qtys.push(selectedVal);
      });

      var service_qty = [];
      $("input.service_qty").each(function(i, sel)
      {
        var selectedVal = $(sel).val();
        service_qty.push(selectedVal);
      });     

      var subscription_qty = [];
      $("input.subscription_qty").each(function(i, sel)
      {
        var selectedVal = $(sel).val();
        subscription_qty.push(selectedVal);
      });

      var grand_total=$(".grand_total_field").val();      
      var total_amount=$(".sub_total_field").val();        
      var id=$("#id").val();      

     $('#proposal p').each(function() 
     {
       var $this = $(this);
       if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
       $this.remove();
     });
     
     $('#proposal [contenteditable]').removeAttr('contenteditable');

     $("#proposal span.cke_reset").each(function () 
     {
       $(this).remove();
     });

     $("#proposal div.save-remove").each(function () 
     {
       $(this).remove();
     });  

     $("#proposal div.overly").each(function () 
     {
       $(this).remove();
     }); 

     $("#proposal button.copy").each(function () 
     {
       $(this).remove();
     });        

     $("#proposal span.cke_widget_wrapper").each(function () 
     {
       $(this).replaceWith($(this).text());
     });

     $("#proposal .quantity_class").each(function ()
     {
        $(this).css('display','none');
       //  $(this).removeClass('checking_test');
     });

     $("#proposal .quantity_cla").each(function () 
     {
        $(this).css('display','inline-block');
     });

    $("#proposal .check_quantity").each(function () 
    {
        $(this).prop('checked',false);
    });

   $(".edit_quantity").css('display','block');
   $(".update_quantity").css('display','none');
       
   // $("#proposal").removeClass('checking_test');
   var proposals=$("#proposal").html(); 

   var formData={'qty':service_qty,'product_qty':product_qtys,'grand_total':grand_total,'total_amount':total_amount,'proposal_contents':proposals,'id':id};

     $.ajax(
     {
        url: '<?php echo base_url(); ?>/proposal_pdf/quantity_update',
        type : 'POST',
        data : formData,                    
        beforeSend: function() 
        {
          $(".LoadingImage").show();
        },
        success: function(data) 
        {
          $(".LoadingImage").hide();

          if(localStorage.getItem("alert_section")=='alert')
          {
            $(".qty_notification").show();
            //setTimeout(function(){ location.reload(); }, 2000);                   
            localStorage.removeItem('alert_section');
          }
        }
     });
 }


</script>
<script>
   $("#searchCompany").keyup(function(){
   
   
    $(".form_heading").html("<h2>Adding Client via Companies House</h2>");
         var currentRequest = null;    
         var term = $(this).val();
         //var term = $(this).val().replace(/ +?/g, '');
         //var term = $(this).val($(this).val().replace(/ +?/g, ''));
         $("#searchresult").show();
         $('#selectcompany').hide();
          $(".LoadingImage").show();
         currentRequest = $.ajax({
            url: '<?php echo base_url();?>client/SearchCompany/',
            type: 'post',
            data: { 'term':term },
            beforeSend : function()    {           
            if(currentRequest != null) {
                currentRequest.abort();
            }
        },
            success: function( data ){
                $("#searchresult").html(data);
                 $(".LoadingImage").hide();
                },
                error: function( errorThrown ){
                    console.log( errorThrown );
                }
            });
       });
   
   function addmorecontact(id)
   {
   $(".LoadingImage").hide();
   $(".modal-backdrop").css("display","none");
      var url = $(location).attr('href').split("/").splice(0, 8).join("/");
   
      var segments = url.split( '/' );
      var fun = segments[5];
   
   if(fun!='' && fun !='addnewclient'  && fun !='addnewclient#')
   {
      $(location).attr('href', '<?php echo base_url();?>client/addnewclient/'+id);
   }  else{
     $(location).attr('href', '<?php echo base_url();?>client/addnewclient/'+id);
     $("#companyss").attr("href","#");
   
   $('.contact_form').show();
   $('.all_layout_modal').modal('hide');
   
       $('.nav-link').removeClass('active');
    //   $('.main_contacttab').addClass('active');
   //$('.contact_form').show();
   
   $("#company").removeClass("show");
   $("#company").removeClass("active");
   $("#required_information").removeClass("active");
   $(".main_contacttab").addClass("active");
   $("#main_Contact").addClass("active");
   $("#main_Contact").addClass("show"); 
   }      
                  
    /* $("#companyss").attr("href","#");
   
   $('.contact_form').show();
   $('.all_layout_modal').modal('hide');
   
       $('.nav-link').removeClass('active');
    //   $('.main_contacttab').addClass('active');
   //$('.contact_form').show();
   
   $("#company").removeClass("show");
   $("#company").removeClass("active");
   $("#required_information").removeClass("active");
   $(".main_contacttab").addClass("active");
   $("#main_Contact").addClass("active");
   $("#main_Contact").addClass("show");
   */
   //$( "." ).tabs( { disabled: [1, 2] } );
    // $(".it3").attr("style", "display:none");
   //$(".it3").tabs({disabled: true });
   }
   
   var xhr = null;
     function getCompanyRec(companyNo)
     {
        $(".LoadingImage").show();
         if( xhr != null ) {
                    xhr.abort();
                    xhr = null;
            }
         
        xhr = $.ajax({
            url: '<?php echo base_url();?>client/CompanyDetails/',
            type: 'post',
            data: { 'companyNo':companyNo },
            success: function( data ){
                //alert(data);
                $("#searchresult").hide();
                $('#companyprofile').show();
                $("#companyprofile").html(data);
                $(".LoadingImage").hide();
                
                },
                error: function( errorThrown ){
                    console.log( errorThrown );
                }
            });}
   
   
        function getCompanyView(companyNo)
     {
   
        //$('.modal-search').hide();
        $(".LoadingImage").show();
       var user_id= $("#user_id").val();
       
         if( xhr != null ) {
                    xhr.abort();
                    xhr = null;
            }
     
         
        xhr = $.ajax({
            url: '<?php echo base_url();?>client/selectcompany/',
            type: 'post',
            dataType: 'JSON',
            data: { 'companyNo':companyNo,'user_id': user_id},
            success: function( data ){
                //alert(data.html);
                $("#searchresult").hide();
                $('#companyprofile').hide();
                $('#selectcompany').show();
                $("#selectcompany").html(data.html);
                $('.main_contacts').html(data.html);
     // append form field value
     $("#company_house").val('1');
   
     //alert($("#company_house").val());
     $("#company_name").val(data.company_name);
     $("#company_name1").val(data.company_name);
     $("#company_number").val(data.company_number);
     $("#companynumber").val(data.company_number);
     $("#company_url").val(data.company_url);
     $("#company_url_anchor").attr("href",data.company_url);
     $("#officers_url").val(data.officers_url);
     $("#officers_url_anchor").attr("href",data.officers_url);
     $("#company_status").val(data.company_status);
     $("#company_type").val(data.company_type);
     //$("#company_sic").append(data.company_sic);
     $("#company_sic").val(data.company_sic);
     $("#sic_codes").val(data.sic_codes);
     
     $("#register_address").val(data.address1+"\n"+data.address2+"\n"+data.locality+"\n"+data.postal);
     $("#allocation_holder").val(data.allocation_holder);
     $("#date_of_creation").val(data.date_of_creation);
     $("#period_end_on").val(data.period_end_on);
     $("#next_made_up_to").val(data.next_made_up_to);
     $("#next_due").val(data.next_due);
     $("#accounts_due_date_hmrc").val(data.accounts_due_date_hmrc);
     $("#accounts_tax_date_hmrc").val(data.accounts_tax_date_hmrc);
     $("#confirm_next_made_up_to").val(data.confirm_next_made_up_to);
     $("#confirm_next_due").val(data.confirm_next_due);
     
     $("#tradingas").val(data.company_name);
     $("#business_details_nature_of_business").val(data.nature_business);
     $("#user_id").val(data.user_id);
   
     $(".edit-button-confim").show();
     $(".LoadingImage").hide();
                
                },
                error: function( errorThrown ){
                    console.log( errorThrown );
                }
            });}
     
     
        function getmaincontact(companyNo,appointments,i,birth)
     {
        $(".LoadingImage").show();
        var user_id= $("#user_id").val();
         if( xhr != null ) {
                    xhr.abort();
                    xhr = null;
            }
   
            var cnt = $("#append_cnt").val();
   
              if(cnt==''){
              cnt = 1;
             }else{
              cnt = parseInt(cnt)+1;
             }
          $("#append_cnt").val(cnt);
   
          var incre=$("#incre").val();
          if(incre==''){
              incre = 1;
             }else{
              incre = parseInt(incre)+1;
             }
          $("#incre").val(incre);
        xhr = $.ajax({
            url: '<?php echo base_url();?>client/maincontact/',
            type: 'post',
            
            data: { 'companyNo':companyNo,'appointments': appointments,'user_id': user_id,'cnt':cnt,'birth':birth,'incre':incre},
            success: function( data ){
   
              $("#usecontact"+i).html('<span class="succ_contact"><a href="#">Added</a></span>');
                //$("#default-Modal,.modal-backdrop.show").hide();
                  // $('#default-Modal').modal('hide');
                   // $('#exist_person').modal('hide');
         $('.contact_form').append(data);
     $(".LoadingImage").hide();
     
     // append form field value
     /*$("#title").val(data.title);
     $("#first_name").val(data.first_name);
     $("#middle_name").val(data.middle_name);
     $("#last_name").val(data.last_name);
     $("#postal_address").val(data.premises+"\n"+data.address_line_1+"\n"+data.address_line_2+"\n"+data.region+"\n"+data.locality+"\n"+data.country+"\n"+data.postal_code);
     $(".LoadingImage").hide();
     */
                
                },
                error: function( errorThrown ){
                    console.log( errorThrown );
                }
            });}
   function backto_company()
   {
     $('#companyprofile').show();
     $('.main_contacts').hide();
   }
   
</script>
<script type="text/javascript">
   $(function(){
     
     var headheight = $('.header-navbar').outerHeight();
     var navbar = $('.remin-admin');
     
   
   
   // if ($(window).width() > 1024) {
   //                $(window).scroll(function() {
   //                    if ($(window).scrollTop() >= 100) {
   //                        navbar.addClass('navbar-scroll');
   //          $('.remin-admin.navbar-scroll').css({"top":headheight+"px"});
           
   //                    } else {
   
   //                        navbar.removeClass('navbar-scroll');
   //                    }
   //                });
   //            }
   
     // if ($(window).width() > 1024) {
     //   $(document).scroll(function() {
     //     var scrollheight1 = $(this).scrollTop();
     //     console.log(scrollheight1);
     //     if (scrollheight1 > 200) {
     //         navbar.addClass('navbar-scroll');
     //         $('.remin-admin.navbar-scroll').css({"top":headheight+"px"});
     //     } else {
     //       navbar.removeClass('navbar-scroll');
     //     }
     //   });
     // }
   
   });
   
</script>
<script type="text/javascript">
   $(document).ready(function(){
   
     var $grid = $('.masonry-container').masonry({
       itemSelector: '.accordion-panel',
       percentPosition: true,
       columnWidth: '.grid-sizer' 
     });
   
     $(document).on( 'click', 'td.splwitch .switchery', function() {
       setTimeout(function(){ $grid.masonry('layout'); }, 600);
     });
   
       $(document).on( 'click', 'ol .nav-item a', function() {
           setTimeout(function(){ $grid.masonry('layout'); }, 600);
       });
   
       $(document).on( 'change', '.main-pane-border1', function() {
         setTimeout(function(){ $grid.masonry('layout'); }, 600);
       });
   
       $(document).on( 'click', '.btn', function() {
         setTimeout(function(){ $grid.masonry('layout'); }, 600);
        });
   
       $(document).on( 'click', '.switching', function() {
         setTimeout(function(){ $grid.masonry('layout'); }, 600);
        });
   
       $(document).on( 'click', '.service-table-client .checkall', function() {
         setTimeout(function(){ $grid.masonry('layout'); }, 600);
        });
   
       $(document).on( 'click', '.service-table-client .switchery', function() {
         setTimeout(function(){ $grid.masonry('layout'); }, 600);
        });
   
       setTimeout(function(){ $grid.masonry('layout'); }, 600);
   
       var test = $('body').height();
       $('test').scrollTop(test);
   
    });
</script>
<!-- <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap.js"></script> -->
<!-- <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>  -->
<script src="<?php echo base_url();?>js/jquery.workout-timer.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/dataTables.responsive.min.js"></script>
<script>


   $("#proposal_accept").click(function(){  
     $("#terms_error").hide();

    if($('#accept_terms').is(":checked")){

         $(this).attr('disabled',true);
        $('#EmailSignature').modal('show');

         <?php  

         $this->session->set_userdata('accept_id', $records['id']); 

         $lead_id = $this->Common_mdl->get_price('proposals','id',$_SESSION['accept_id'],'lead_id'); 

         ?>

         var lead_id = "<?php echo $lead_id; ?>";

           $("#btnSaveSign").click(function(e)
           {
                  html2canvas([document.getElementById('sign-pad')], {
                  onrendered: function (canvas) {
                  var canvas_img_data = canvas.toDataURL('image/png');
                  var img_data = canvas_img_data.replace(/^data:image\/(png|jpg);base64,/, "");
                  //ajax call to save image inside folder
                  $.ajax({
                    url: '<?php echo base_url(); ?>/proposal/save_accept_sign',
                    data: { img_data:img_data },
                    type: 'post',
                    dataType: 'json',
                    success: function (response) 
                    {
                      if(response.status == 1)
                      { 
                        $("#imgData").html('Thank you! Your signature was saved');
                        $("#imgData").show();
                        setTimeout(function()
                        {  
                          $("#imgData").hide();                    
                          $("#EmailSignature").find('.modal-header').children('button').trigger('click');
                          $('#accept_proposal').find('.modal-header').children('button').trigger('click');
                        }, 2000);
                      }

                      var status='accepted';
                      var id=$("#proposal_id").val();
                      console.log(id);
                      var formData={'id':id,'status':status};

                         $.ajax({
                            url: '<?php echo base_url(); ?>proposals/status_change',
                            type : 'POST',
                            data : formData,                    
                              beforeSend: function() {
                                $(".LoadingImage").show();
                              },
                              success: function(data) 
                              {
                                 $(".LoadingImage").hide();
                                 var res = JSON.parse(data);

                                 if(res['status'] == 1)
                                 {
                                    $(".accept").show();
                                    
                                    setTimeout(function()
                                    {
                                      $(".accept").hide();
                                      
                                      if(lead_id != '0' && typeof(lead_id)!="undefined")
                                      {
                                          window.location.href = '<?php echo base_url(); ?>user/online_registeration/'+id;    
                                      }
                                      else
                                      {
                                          location.reload();
                                      }
                                    }, 3000);
                                 }                                 
                                 else if(res['msg'] != "")
                                 {
                                     $('.info').find('div .position-alert1').html(res['msg']+' Contact Admin.');
                                     $('.info').show();
                                     setTimeout(function(){ location.reload(); }, 4000);
                                 }
                                 else
                                 {
                                    location.reload();
                                 }                                
                              }
                          });                 
                     }
                  });
                }
              });
            });


       }else{

        $("#terms_error").html('please accept the terms and conditions');
        $("#terms_error").show();

       }
   });
   
   $("#proposal_decline").click(function(){


      $(this).attr('disabled',true);
      //  alert('ok');
     // alert($(this).val());
      var status='declined';
      var id=$("#proposal_id").val();
    //  alert(id);
      var formData={'id':id,'status':status};
         $.ajax({
            url: '<?php echo base_url(); ?>proposals/status_change',
            type : 'POST',
            data : formData,                    
              beforeSend: function() {
                $(".LoadingImage").show();
              },
              success: function(data) 
              {
                 $(".LoadingImage").hide();
                 var res = JSON.parse(data);
                 
                 if(res['status'] == 1)
                 {
                    $(".decline").show();
                    setTimeout(function(){  $(".decline").hide();  location.reload();   }, 3000);
                 }
              }
          });
   });
    
    $(document).ready(function(){
   var password_condition=$("#password").val();
   //alert(password_condition);
   if(password_condition=='on'){
   $(function() {
    $('#password_popup').modal({
      show: true,
      keyboard: false,
      backdrop: 'static'
    });
   });
   }
    });
   
    $("#access_key").click(function(){
   var proposal_id=$("#proposal_id").val();
   var pdf_password=$("#pdf_password").val();
   //alert(proposal_id);
   //alert(pdf_password);
   var formData={'id':proposal_id,'password':pdf_password};
         $.ajax({
            url: '<?php echo base_url(); ?>proposal_pdf/access_key',
            type : 'POST',
            data : formData,                    
              beforeSend: function() {
                $(".LoadingImage").show();
              },
              success: function(data) {
               // alert(data);
                 $(".LoadingImage").hide();
                 if(data=='1'){
                  $("#password_popup").modal('toggle');
                }else{
                 $(".wrong_msg").show();
                 setTimeout(function(){  $(".wrong_msg").hide();   }, 3000);
               }
                // alert(data);
              }
          });
   
    });

       $(".close").click(function(){
    $(".alert").hide();
   });







  




$( document ).ready(function() {
    console.log( "ready!" );




   // console.log($(".optional_check").attr());


    $('.optional_check').each(function() { 

   if($(this).attr('class')=='optional_check Remove_service'){

       $(this).prop('checked', false);

   }else{
        $(this).prop('checked', true);
   }

});


    

      //$(".optional_check").attr('checked', true);



     //     $('.quantity').each(function() { 

     //  $(this).val($(this).parents('.quantity_class').next('.quantity_cla').html());

     // }); 





     $('.quantity').each(function() { 

      $(this).val($(this).parents('.quantity_class').next('.quantity_cla').html());

     });   





    });



 $('.quantity').keyup(function(evt){
      var text = $(this).val();
      var test_value = text.replace(/[^0-9]+/g, "");
      $(this).val(test_value);
    });

   $('.quantity').keyup(function(){
    console.log('#####');
    var val = $(this).val();
    if(isNaN(val)){
         val = val.replace(/[^0-9\.]/g,'');
         if(val.split('.').length>2) 
             val =val.replace(/\.+$/,"");
    }
    $(this).val(val); 
});


   $('.quantity').keypress(function(e){ 
   if (this.value.length == 0 && e.which == 48 ){
      return false;
   }
});



   function maxLengthCheck(object) {
if (object.value.length > object.maxLength)
  object.value = object.value.slice(0, object.maxLength)
}

function isNumeric (evt) {
var theEvent = evt || window.event;
var key = theEvent.keyCode || theEvent.which;
key = String.fromCharCode (key);
var regex = /[0-9]|\./;
if ( !regex.test(key) ) {
  theEvent.returnValue = false;
  if(theEvent.preventDefault) theEvent.preventDefault();
}
}

 var myScrollFunc = function() 
 {
     var y = window.scrollY;

     if(y >= 800) 
     {
       myID.className = "bottomMenu show"
     }     
 };

 window.addEventListener("scroll", myScrollFunc);
    
</script>