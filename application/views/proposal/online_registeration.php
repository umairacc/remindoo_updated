<html lang="en">
<head>

    <title>CRM </title>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <meta name="description" content="#">

    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">

    <meta name="author" content="#">

       <div class="LoadingImage">
   <div class="preloader3 loader-block">
      <div class="circ1 loader-primary loader-md"></div>
      <div class="circ2 loader-primary loader-md"></div>
      <div class="circ3 loader-primary loader-md"></div>
      <div class="circ4 loader-primary loader-md"></div>
   </div>
   </div>
    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url(); ?>assets/images/favicon.ico" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>bower_components/bootstrap/css/bootstrap.min.css">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/icon/themify-icons/themify-icons.css">
    <!-- ico font -->    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/icon/icofont/css/icofont.css">
    <!-- flag icon framework css -->  
    <!-- Menu-Search css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/pages/menu-search/css/component.css">
    <!-- jpro forms css -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/pages/j-pro/css/demo.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/pages/j-pro/css/font-awesome.min.css">
    <!-- Style.css -->  
    <link type="text/css" href="<?php echo base_url(); ?>bower_components/bootstrap-timepicker/css/bootstrap-timepicker.min.css" /> 
    <!-- Date-range picker css  -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>bower_components/bootstrap-daterangepicker/css/daterangepicker.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>bower_components/datedropper/css/datedropper.min.css" />

  
<!--     <link rel="stylesheet" href="https:/resources/demos/style.css">
 -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>bower_components/switchery/css/switchery.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.mCustomScrollbar.css">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>bower_components/owl.carousel/css/owl.carousel.css">

<!--     <link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/buttons.dataTables.min.css?ver=2">
<link rel="stylesheet" type="text/css" href="http://remindoo.org/CRMTool/assets/css/responsive.bootstrap4.min.css"> -->
  
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/ui_style.css?ver=3">
  
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/common_style.css?ver=4">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/common_responsive.css?ver=2">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/dataTables.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.dropdown.css">

    <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">

<!--     <link rel="stylesheet" type="text/css" href="https://bootswatch.com/superhero/bootstrap.min.css">
 -->
    <link href="<?php echo base_url(); ?>css/jquery.signaturepad.css" rel="stylesheet">
       
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-colorpicker.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-slider.min.css">
<!--    <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/medium-editor/medium-editor.min.css"> -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/medium-editor/template.min.css">
     <!-- <link rel="stylesheet" href="http://remindoo.org/CRMTool/assets/css/style1.css"> -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/icon/simple-line-icons/css/simple-line-icons.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/icon/icofont/css/icofont.css">
	
	 <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css">   
	 
<style>
.accordion {
    background-color: #303549;
    color: #fff;
    cursor: pointer;
    padding: 18px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 15px;
    transition: 0.4s;
} 

.panel {
    padding: 0 18px;
    display: none;
    background-color: white;
    overflow: hidden;
}

.LoadingImage {
    display: none;
    position: fixed;
    z-index: 100;
     background-image: url('<?php echo base_url(); ?>assets/images/ajax-loader.gif'); 
    background-color: rgba(65, 56, 57, 0.4);
    opacity: 1;
    background-repeat: no-repeat;
    background-position: center;
    left: 0;
    bottom: 0;
    right: 0;
    top: 0;
}

.modal-alertsuccess a.close 
{  
  margin-top: 5px;
  margin-right: 15px;
} 
.pop-realted1 
{
   padding: 10px;
   background-color: #fff;
}
</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/responsive.dataTables.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/buttons.dataTables.min.css?ver=2">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/progress-circle.css">
</head>
<body>
    
    <div class="modal-alertsuccess alert alert-success registered" style="display:none;">
      <div class="newupdate_alert">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">X</a>
          <div class="pop-realted1">
             <div class="position-alert1"></div>
          </div>
      </div>
   </div>

    <div class="theme-loader">

        <div class="ball-scale">

            <div class='contain'>

                <div class="ring">

                    <div class="frame"></div>

                </div>

                <div class="ring">

                    <div class="frame"></div>

                </div>

                <div class="ring">

                    <div class="frame"></div>

                </div>

                <div class="ring">

                    <div class="frame"></div>

                </div>

                <div class="ring">

                    <div class="frame"></div>

                </div>

                <div class="ring">

                    <div class="frame"></div>

                </div>

                <div class="ring">

                    <div class="frame"></div>

                </div>

                <div class="ring">

                    <div class="frame"></div>

                </div>

                <div class="ring">

                    <div class="frame"></div>

                </div>

                <div class="ring">

                    <div class="frame"></div>

                </div>

            </div>

        </div>

    </div>
    <div class="pcoded-main-container online-registration ">
<div class="pcoded-content card12">
   <div class="pcoded-inner-content back2messge">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
               <div class="page-body task-d">
                 <div class="newupdate_design online-register-wrapper">    
                           <div class="management_section floating_set realign newmanage-animation">
          
<form id="insert_form" class="validation modal-addnew1" method="post" action="" enctype="multipart/form-data">
<div class="floating_set online-re1">		  
<h3>Online Registeration</h3>

<div class="f-right"> <input type="submit" class="signed-change1 sv_ex btn btn-primary" id="save_exit" value ="Save & Exit">
 </div> 
 </div>
 
<input type="hidden" name="proposal_id" value="<?php echo $id=$this->uri->segment(3); ?>">

<?php

  $proposal_id = $this->uri->segment(3); 
  $user = $this->Common_mdl->select_record('user','id',$proposal['company_id']);
  $client = $this->Common_mdl->select_record('client','user_id',$user['id']);   
  $_SESSION['id'] = $proposal['user_id'];

?>
<input type="hidden" name="user_id" value="<?php if(!empty($proposal['company_id'])){ echo $proposal['company_id']; } ?>">
<?php 
$vvv=array();
$content='';
$id=$this->uri->segment(3);

$query=$this->Common_mdl->get_price('proposals','id',$id,'item_name');

$services=explode(',',$query);
 ?>

<input type="hidden" name="workplace[tab]" value="<?php  if(in_array('WorkPlace Pension - AE ', $services )){?>on<?php } ?>">
<input type="hidden" name="companytax[tab]" value="<?php if(in_array('Company Tax Return', $services)){?>on<?php } ?>">
<input type="hidden" name="cis[tab]" value="<?php if(in_array('CIS - Contractor', $services)){?>on<?php } ?>">
<input type="hidden" name="cissub[tab]" value="<?php if(in_array('CIS - Sub Contractor', $services)){?>on<?php } ?>">
<input type="hidden" name="confirm[tab]" value="<?php if(in_array('Confirmation Statement', $services)){?>on<?php } ?>">
<input type="hidden" name="payroll[tab]" value="<?php if(in_array('Payroll', $services)){?>on<?php } ?>">
<input type="hidden" name="vat[tab]" value="<?php if(in_array('VAT Returns', $services)){?>on<?php } ?>">
<input type="hidden" name="personaltax[tab]" value="<?php if(in_array('Personal Tax Return', $services)){?>on<?php } ?>">
<input type="hidden" name="accounts[tab]" value="<?php if(in_array('Accounts', $services)){?>on<?php } ?>">
<input type="hidden" name="bookkeep[tab]" value="<?php if(in_array('Bookkeeping', $services)){?>on<?php } ?>">
<input type="hidden" name="registered[tab]" value="<?php if(in_array('Registered Address', $services)){?>on<?php } ?>">
<input type="hidden" name="taxinvest[tab]" value="<?php if(in_array('Tax Advice/Investigation', $services)){?>on<?php } ?>">
<input type="hidden" name="p11d[tab]" value="<?php if(in_array('P11D', $services)){?>on<?php } ?>">
<input type="hidden" name="management[tab]" value="<?php if(in_array('Management Accounts', $services)){?>on<?php } ?>">
<input type="hidden" name="investgate[tab]" value="<?php if(in_array('Investigation Insurance', $services)){?>on<?php } ?>">
<input type="hidden" name="taxadvice[tab]" value="<?php if(in_array('Tax Advice/Investigation', $services)){?>on<?php } ?>">

<?php 
  $other_services = $this->db->query('SELECT * FROM service_lists WHERE id>16')->result_array();
  foreach($other_services as $key => $value) 
  {    
?>
  <input type="hidden" name="<?php echo $value['services_subnames'].'[tab]'; ?>" value="<?php if(in_array($value['service_name'], $services)){?>on<?php } ?>">
<?php  
  }
?>

<div class="masonry-animation03 floating_set">
<div class="accordion-panel columns-two">
<div class="box-division03">
<div class="accordion-heading">
<h3 class="accordion-msg">Required information</h3>
</div>

<div class="basic-info-client1">
<div class="form-group row name_fields">
<label class="col-sm-4 col-form-label">Name</label>
<div class="col-sm-8">
<input type="text" name="company_name" id="company_name" placeholder="" value="<?php if(!empty($client['crm_company_name'])) { echo $client['crm_company_name']; }?>" class="fields">
</div>
</div>
<div class="form-group row">
<label class="col-sm-4 col-form-label">Legal Form</label>
<div class="col-sm-8">
<select name="legal_form" class="form-control fields" id="legal_form">
<option value="Private Limited company" data-id="1">Private Limited company</option>
<option value="Public Limited company"  data-id="1">Public Limited company</option>
<option value="Limited Liability Partnership" data-id="1">Limited Liability Partnership</option>
<option value="Partnership"  data-id="2">Partnership</option>
<option value="Self Assessment" data-id="2">Self Assessment</option>
<option value="Trust"  data-id="1">Trust</option>
<option value="Charity"  data-id="1">Charity</option>
<option value="Other"  data-id="1">Other</option>
</select>
</div>
</div>
</div>
</div>
</div>

<div class="accordion-panel column-add01">
<div class="box-division03">
<div class="accordion-heading">
<h3 class="accordion-msg">Basic Details</h3>
</div>
 
 <div class="basic-info-client1">
<div class="form-group row name_fields sorting" id="">
<label class="col-sm-4 col-form-label">Company Name</label>
<div class="col-sm-8 edit-field-popup1">
<input type="text" name="company_name1" id="company_name1" placeholder="Accotax Limited" value="<?php if(!empty($client['crm_company_name1'])) { echo $client['crm_company_name1']; }?>" class="fields edit_classname" ></div>
</div>
<div class="form-group row sorting" id="">
<label class="col-sm-4 col-form-label">Company Number</label>                                               

<div class="col-sm-8 edit-field-popup1">
<input type="number" name="company_number" id="company_number" placeholder="Company number" value="<?php if(!empty($client['crm_company_number'])) { echo $client['crm_company_number']; }?>" class="fields edit_classname"  >
</div>
</div>                                                   
<div class="form-group row date_birth name_fields sorting" id="">
<label class="col-sm-4 col-form-label">Incorporation Date</label>
<div class="col-sm-8 edit-field-popup1">
<span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
<input class="fields edit_classname <?php //echo $datepicker;?> date_picker_dob" type="text" name="date_of_creation" id="date_of_creation" placeholder="dd-mm-yyyy" value="<?php if(!empty($client['crm_incorporation_date'])) { echo $client['crm_incorporation_date']; }?>">
</div>
</div>
<div class="form-group row name_fields sorting" id="">
<label class="col-sm-4 col-form-label">Registered In</label>
<div class="col-sm-8 edit-field-popup1">
<input type="text" name="registered_in" id="registered_in" placeholder="" value="<?php if(!empty($client['crm_registered_in'])) { echo $client['crm_registered_in']; }?>" class="fields edit_classname" >   
</div>
</div>
<div class="form-group row name_fields sorting" id="">
<label class="col-sm-4 col-form-label">Address Line 1</label>
<div class="col-sm-8 edit-field-popup1">
<input type="text" name="address_line_one" id="address_line_one" placeholder="" value="<?php if(!empty($client['crm_address_line_one'])) { echo $client['crm_address_line_one']; }?>" class="fields edit_classname" >
</div>
</div>
<div class="form-group row name_fields sorting"  id="">
   <label class="col-sm-4 col-form-label">Address Line 2</label>
   <div class="col-sm-8 edit-field-popup1">
      <input type="text" name="address_line_two" id="address_line_two" placeholder="" value="<?php if(!empty($client['crm_address_line_two'])) { echo $client['crm_address_line_two']; }?>" class="fields edit_classname" >
   </div>
</div>
<div class="form-group row name_fields sorting" id="">
<label class="col-sm-4 col-form-label">Address Line 3</label>
<div class="col-sm-8 edit-field-popup1">
<input type="text" name="address_line_three" id="address_line_three" placeholder="" value="<?php if(!empty($client['crm_address_line_three'])) { echo $client['crm_address_line_three']; }?>" class="fields edit_classname">
</div>
</div>
<div class="form-group row name_fields sorting" id="">
<label class="col-sm-4 col-form-label">Town/City</label>
<div class="col-sm-8 edit-field-popup1">
<input type="text" name="crm_town_city" id="crm_town_city" placeholder="" value="<?php if(!empty($client['crm_town_city'])) { echo $client['crm_town_city']; }?>" class="fields edit_classname" >
</div>
</div>
<div class="form-group row name_fields sorting" id="">
<label class="col-sm-4 col-form-label">Post Code</label>
<div class="col-sm-8 edit-field-popup1">
<input type="text" name="crm_post_code" id="crm_post_code" placeholder="" value="<?php if(!empty($client['crm_post_code'])) { echo $client['crm_post_code']; }?>" class="fields edit_classname">
</div>
</div>
<div class="form-group row name_fields sorting" id="">
<label class="col-sm-4 col-form-label">Company Status</label>
<div class="col-sm-8 edit-field-popup1">
<input type="text" name="company_status" id="company_status" placeholder="active" value="<?php if(!empty($client['crm_company_status'])) { echo $client['crm_company_status']; }?>" class="fields edit_classname" >

</div>
</div>
<div class="form-group row name_fields sorting" id="">
<label class="col-sm-4 col-form-label">Company Type</label>
<div class="col-sm-8 edit-field-popup1">
<input type="text" name="company_type" id="company_type" placeholder="Private Limited Company" value="<?php if(!empty($client['crm_legal_form'])) { echo $client['crm_legal_form']; }?>" class="fields edit_classname">
</div>
</div>
<div class="form-group row name_fields sorting" id="">
<label class="col-sm-4 col-form-label">Company SIC</label>
<div class="col-sm-8 edit-field-popup1">
<input type="text" name="company_sic" id="company_sic" value="<?php if(!empty($client['crm_company_sic'])) { echo $client['crm_company_sic']; }?>" class="fields edit_classname" > 
<input type="hidden" name="sic_codes" id="sic_codes" value="">
</div>
</div>
<div class="form-group row date_birth sorting" id="">
<label class="col-sm-4 col-form-label">Letter of Enagagement Sign Date</label>
<div class="col-sm-8">
<span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
<input class="form-control fields dob_picker" placeholder="dd-mm-yyyy" type="text" name="engagement_letter" id="engagement_letter" value="<?php if(!empty($client['crm_letter_sign'])) { echo $client['crm_letter_sign']; }?>"/>
</div>
</div>
<div class="form-group row name_fields sorting" id="">
<label class="col-sm-4 col-form-label">Business Websites</label>
<div class="col-sm-8">
<input type="text" class="fields" name="business_website" id="business_website" placeholder="Business Website" value="<?php if(!empty($client['business_website'])) { echo $client['business_website']; }?>">
</div>
</div>
                                         
<div class="form-group row sorting" id="">
<label class="col-sm-4 col-form-label">Company URL</label>
<div class="col-sm-8">
<input type="text" name="company_url" id="company_url" value="<?php if(!empty($client['crm_company_url'])) { echo $client['crm_company_url']; }?>" class="fields" placeholder="www.google.com">
</div>
</div>                                           

    <div class="form-group row sorting" id="">
    <label class="col-sm-4 col-form-label">Officers URL</label>
    <div class="col-sm-8">

    <input type="text" name="officers_url" id="officers_url" value="<?php if(!empty($client['crm_officers_url'])) { echo $client['crm_officers_url']; }?>" class="fields" placeholder="www.google.com">

    </div>
    </div>
                                              
                                               
<div class="form-group row name_fields sorting" id="">
<label class="col-sm-4 col-form-label"><?php echo $this->Common_mdl->get_name_details(17); ?></label>
<div class="col-sm-8">
<input type="text" class="fields" name="accounting_system_inuse" id="accounting_system_inuse" value="<?php if(!empty($client['accounting_system_inuse'])) { echo $client['accounting_system_inuse']; }?>">
</div>
</div>
</div>
</div>
</div>
 
<div class="accordion-panel">
<div class="box-division03">
<div class="accordion-heading">
<h3 class="accordion-msg">Other Details</h3>
</div>
  
 <div class="basic-info-client1">
<div class="form-group row" style="display: none;">
<label class="col-sm-4 col-form-label">Profile Image</label>
<div class="col-sm-8">
<div class="custom_upload">
  <input type="file" name="profile_image" id="profile_image" class="fields">
  </div>
</div>
</div>
<div class="form-group row name_fields radio_button02" style="display: none;">
<label class="col-sm-4 col-form-label">Gender</label>
<div class="col-sm-8 gender-01">
  <p  class="radio radio-inline">
     <label><input type="radio" class="fields" id="test1" name="gender" value="male" <?php if(!empty($client['crm_gender']) && $client['crm_gender'] == "male") { echo "checked"; } ?>><i class="helper"></i> <span>Male</span> </label>
  </p>
  <p  class="radio radio-inline"><label><input type="radio" class="fields" id="test2" name="gender" value="female"  <?php if(!empty($client['crm_gender']) && $client['crm_gender'] == "female") { echo "checked"; } ?>><i class="helper"></i> <span>Female</span></label></p>
</div>
</div>

<div class="form-group row radio_bts others_details" id="" style="display: none;">
<label class="col-sm-4 col-form-label">Previous Accounts</label>
<div class="col-sm-8">
<input type="checkbox" class="js-small f-right fields" name="previous_account" id="previous_account" value="on" data-id="preacc">
</div>
</div>
<div id="enable_preacc" style="display:none;">
<div class="form-group row name_fields">
<label class="col-sm-4 col-form-label">Name of the Firm</label>
<div class="col-sm-8">
<input type="text" name="name_of_firm" id="name_of_firm" placeholder="" value="" class="fields">
</div>
</div>
<div class="form-group row ">
<label class="col-sm-4 col-form-label">Address</label>
<div class="col-sm-8">
<textarea rows="4" name="other_address" id="other_address" class="form-control fields"></textarea>
</div>
</div>

<div class="form-group row name_fields others_details" id="">
<label class="col-sm-4 col-form-label">Contact Tel</label>
<div class="col-sm-8">
<input type="number" class="con-code" name="cun_code" name="cun_code" value="">
<input type="number" name="pn_no_rec" id="pn_no_rec" class="fields" value="">
</div>
</div>
<div class="form-group row name_fields others_details" id="">
<label class="col-sm-4 col-form-label">Email Address</label>
<div class="col-sm-8">
<input type="email" name="emailid" id="emailid" placeholder="" value="" class="fields">
</div>
</div>
<div class="form-group row radio_bts others_details" id="">
<label class="col-sm-4 col-form-label">Chase for Information</label>
<div class="col-sm-8">
<input type="checkbox" class="js-small f-right fields" name="chase_for_info" id="chase_for_info" data-id="chase_for_info">
</div>
</div>
</div>
<div class="form-group row name_fields others_details" id="<?php echo $this->Common_mdl->get_order_details(88); ?>">
<label class="col-sm-4 col-form-label">Username</label>
<div class="col-sm-8">
<input type="text" name="user_name" id="user_name" placeholder="" value="" class="fields">
<span class="v_err" style="color:red;display:none">User name already Exists</span>
</div>
</div>
<div class="form-group row name_fields others_details" id="<?php echo $this->Common_mdl->get_order_details(89); ?>">
<label class="col-sm-4 col-form-label">Password</label>
<div class="col-sm-8">
<input type="password" name="password" id="password" placeholder="Password" value="" class="fields">
</div>
</div>
<div class="form-group row name_fields others_details" id="<?php  $query=$this->Common_mdl->get_order_details(89); echo $query+1; ?>">
<label class="col-sm-4 col-form-label">Confirm Passowrd</label>
<div class="col-sm-8">
<input type="password" name="confirm_password" id="confirm_password" placeholder="confirm password" value="" class="fields"> 
</div>
</div>
    
	</div>
</div>
</div>




<div class="accordion-panel">
<div class="box-division03">
<div class="accordion-heading">
<h3 class="accordion-msg">Proof Upload</h3>
</div>

 
<div class="basic-info-client1">
<div class="form-group row radio_bts ">
<label class="col-sm-4 col-form-label">Client ID Verified</label>
<div class="col-sm-8">
<input type="checkbox" class="js-small f-right fields" name="client_id_verified" id="client_id_verified" >
</div>
</div>
<div class="form-group row assign_cus_type client_id_verified_data" style="display: none;">
<label class="col-sm-4 col-form-label">Type of ID Provided</label>
<div class="col-sm-8">
<div class="dropdown-sin-11 lead-form-st">
<select name="type_of_id[]" id="person" class="form-control fields assign_cus" multiple="multiple" placeholder="Select">
<option value="" disabled="disabled">--Select--</option>
<option value="Passport">Passport</option>
<option value="Driving_License">Driving License</option>
<option value="Other_Custom">Other Custom</option>
</select>
</div>
</div>
</div>
<!-- 29-08-2018 for multiple image upload option -->
<div class="form-group row" >
<label class="col-sm-4 col-form-label">Attachement</label>
<div class="col-sm-8">
<div class="custom_upload">
<input type="file" name="proof_attach_file[]" id="proof_attach_file" multiple="multiple" >
</div></div>
</div>
<!-- en dof image upload for type of ids -->
<div class="form-group row name_fields spanassign for_other_custom_choose" style="display:none">
<label class="col-sm-4 col-form-label">Other Custom</label>
<div class="col-sm-8">
<input type="text" name="assign_other_custom" id="assign_other_custom" placeholder="" value="" class="fields">
</div>
</div>
<div class="form-group row radio_bts ">
<label class="col-sm-4 col-form-label">Proof of Address</label>
<div class="col-sm-8">
<input type="checkbox" class="js-small f-right fields" name="proof_of_address" id="proof_of_address">
</div>
</div>
<div class="form-group row radio_bts ">
<label class="col-sm-4 col-form-label">Meeting with the Client</label>
<div class="col-sm-8">
<input type="checkbox" class="js-small f-right fields" name="meeting_client" id="meeting_client">
</div>
</div>
</div>
</div>
</div>
 
 
  <div class="accordion-panel">
<div class="box-division03">
<div class="accordion-heading">
<h3 class="accordion-msg">Main Contact</h3>
</div>

 
 
<div class="basic-info-client1">
<!-- <div class="left-primary remove-space-set1"> -->
<span class="primary-inner">
<label>title</label>
<!-- <input type="text" class="text-info title"  name="title[]" id="title<?php echo $id;?>" value="<?php echo $title;?>">  -->
<select class="text-info title"  name="title" id="title">
<option value="Mr">Mr</option>
<option value="Mrs">Mrs</option>
<option value="Miss">Miss</option>
<option value="Dr">Dr</option>
<option value="Ms">Ms</option>
<option value="Prof">Prof</option>
</select>
</span>
<span class="primary-inner">
<label>first name</label>
<input type="text" class="text-info" name="first_name[]" id="first_name" value="">
</span>
<span class="primary-inner">
<label>middle name</label>
<input type="text" class="text-info" name="middle_name[]" id="middle_name" value="">
</span>
<span class="primary-inner">
<label>surname</label>
<input type="text" class="text-info" name="surname[]" id="surname" value="">
</span>
<span class="primary-inner">
<label>prefered name</label>
<input type="text" class="text-info" name="preferred_name[]" id="preferred_name" value="">
</span>
<span class="primary-inner">
<label>mobile</label>
<input type="text" class="text-info" name="mobile[]" id="mobile" value="">
</span>
<span class="primary-inner">
<label>main E-Mail address</label>
<input type="text" class="text-info" name="main_email[]" id="email_id"  value="">
</span>
<span class="primary-inner">
<label>Nationality</label>
<input type="text" class="text-info" name="nationality[]" id="nationality"  value="">
</span>
<span class="primary-inner">
<label>PSC</label>
<input type="text" class="text-info" name="psc[]" id="psc"  value="">
</span>
<span class="primary-inner">
<label>shareholder</label>
<select name="shareholder" id="shareholder">
<option value="yes">Yes</option>
<option value="no" >No</option>
</select>
</span>

<span class="primary-inner">
<label>national insurance number</label>
<input type="text" class="text-info" name="ni_number[]" id="ni_number"  value="">
</span>

<span class="primary-inner">
<label>Country Of Residence</label>
<input type="text" name="country_of_residence[]" id="country_of_residence" class="text-info" value="">
</span>
 
<!-- <input type="hidden" name="ni_number" id="ni_number" class="form-control fields" value=""> -->
<!-- <div class="left-primary right-primary"> -->
<span class="primary-inner contact_type">
<label>contact type</label>
<select name="contact_type" id="contact_type" class="othercus">
<option value="Director" >Director</option>
<option value="Director/Shareholder">Director/Shareholder</option>
<option value="Shareholder">Shareholder</option>
<option value="Accountant" >Accountant</option>
<option value="Bookkeeper" >Bookkeeper</option>
<option value="Other" >Other(Custom)</option>
</select>
</span>
<span class="primary-inner spnMulti" id="others_customs" style="display:none">
<label>Other(Custom)</label>
<input type="text" class="text-info" name="other_custom[]" id="other_custom"  value="">
</span>
<span class="primary-inner">
<label>address line1</label>
<input type="text" class="text-info" name="address_line1[]" id="address_line1"  value="">
</span>
<span class="primary-inner">
<label>address line2</label>
<input type="text" class="text-info" name="address_line2[]" id="address_line2"  value="">
</span>
<span class="primary-inner">
<label>town/city</label>
<input type="text" class="text-info" name="town_city[]" id="town_city"  value="">
</span>
<span class="primary-inner">
<label>post code</label>
<input type="text" class="text-info" name="post_code[]" id="post_code"  value="">
</span>

<span class="primary-inner">
<div class="update-primary">
<label>landline</label>
<select name="pre_landline<?php echo $i;?>" id="pre_landline" class="pre-landline">
<option value="mobile">Mobile</option>
<option value="work">Work</option>
<option value="home">Home</option>
<option value="main">Main</option>
<option value="workfax">Work Fax</option>
<option value="homefax">Home Fax</option>
</select>
<div class="land-spaces">   <input type="text" class="text-info" name="landline" id="landline"  value=""> </div>
</div>
</span>

<!-- <span class="success pack_add_row_wrpr_landline">
<button type="button" class="btn btn-primary yk_pack_addrow_landline" data-id="">Add Landline</button></span> -->                                                                
<span class="primary-inner">
<label>Work email</label>
<input type="text" class="text-info" name="work_email[]" id="work_email"  value="">
</span>

<!-- <span class="success pack_add_row_wrpr_email">
<button type="button" class="btn btn-primary yk_pack_addrow_email" data-id="">Add Email</button></span> -->
<span class="primary-inner date_birth">
<label>dateof birth</label>
<div class="picker-appoint">
<span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span> <input type="text" class="text-info date_picker_dob" name="date_of_birth[]"  placeholder="dd-mm-yyyy" value="">
</div>
</span>
<span class="primary-inner">
<label>nature of control</label>
<input type="text" class="text-info" name="nature_of_control[]" id="nature_of_control"  value="">
</span>
<span class="primary-inner">
<label>marital status</label>
<!-- <input type="text" class="text-info" name="marital_status" id="marital_status<?php// echo $id;?>"  value="<?php //echo $marital_status;?>"> -->
<select name="marital_status" id="marital_status">
<option value="Single" >Single</option>
<option value="Living_together" >Living together</option>
<option value="Engaged">Engaged</option>
<option value="Married" >Married</option>
<option value="Civil_partner" >Civil partner</option>
<option value="Separated" >Separated</option>
<option value="Divorced">Divorced</option>
<option value="Widowed" >Widowed</option>
</select>
</span>
<span class="primary-inner">
<label>utr number</label>
<input type="text" class="text-info" name="utr_number[]" id="utr_number"  value="">
</span>
<span class="primary-inner">
<label>Occupation</label>
<input type="text" class="text-info" id="occupation"  name="occupation[]" value="">
</span>
<span class="primary-inner date_birth">
<label>Appointed On</label>
<div class="picker-appoint">
<span class="input-group-addon"><span class="icofont icofont-ui-calendar"></span></span>
<input type="text" class="text-info" id="appointed_on"  name="appointed_on[]" value="">
</div>
</span>
 
</div>
</div>
</div>
</div>
 </form>  
 </div>  



                 </div>
				 </div>
              </div>
         </div>
      </div>
   </div>
</div>
 
<?php $fun = $this->uri->segment(2);?>

   <!-- Required Jquery -->

    <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-ui/js/jquery-ui.min.js"></script>

    <script src="<?php echo base_url();?>assets/js/timezones.full.js"></script>

   
    
    <script type="text/javascript" src="<?php echo base_url();?>bower_components/popper.js/js/popper.min.js"></script>  

     <script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap/js/bootstrap.min.js"></script>  

    <!-- j-pro js -->

    <!-- <script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.ui.min.js"></script> -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.maskedinput.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/pages/j-pro/js/jquery.j-pro.js"></script>

    <!-- jquery slimscroll js -->

    <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>

    <!-- modernizr js -->
    <script type="text/javascript" src="<?php echo base_url();?>bower_components/switchery/js/switchery.min.js"></script>
 <!--  
    <script type="text/javascript" src="<?php echo base_url();?>bower_components/modernizr/js/css-scrollbars.js"></script> -->

  <!-- <script src="<?php echo base_url();?>assets/js/mmc-common.js"></script>
   <script src="<?php echo base_url();?>assets/js/mmc-chat.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/chat.js"></script> -->

    <!-- Custom js -->


    <script src="<?php echo base_url();?>assets/js/pcoded.min.js"></script>

    <script src="<?php echo base_url();?>assets/js/demo-12.js"></script>
  
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/masonry.pkgd.min.js"></script>

    

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>

  <script type="text/javascript" src="<?php echo base_url();?>bower_components/owl.carousel/js/owl.carousel.min.js"></script> 

  <script type="text/javascript" src="<?php echo base_url();?>assets/js/common_script.js"></script>

  <script type="text/javascript" src="<?php echo base_url();?>bower_components/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>

  <?php if ($url == 'step_proposal' || $url == 'template_design' || $url=='templates') { ?>
      <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
  <?php } ?>

  <script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
<script src="<?php echo base_url();?>assets/js/mock.js"></script>
<!-- <script src="https://use.fontawesome.com/86c8941095.js"></script> -->
  
    <?php $url = $this->uri->segment('2');?>
    

    <script src="<?php echo base_url();?>assets/js/bootbox.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/debounce.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap-colorpicker.min.js"></script>
  <!--   <script src="<?php //echo base_url();?>assets/js/bootstrap-slider.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.js"></script>
    <script src="//cdn.jsdelivr.net/medium-editor/latest/js/medium-editor.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/creative.tools.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/html2canvas.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/image-edit.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/editor.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.dropdown.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>
<script type="text/javascript">
         /** 29-08-2018 **/
      
      $('.assign_cus_type').find('.dropdown-sin-11').on('click',function(){

      $(this).find('.dropdown-main > ul > li').click(function(){
           var custom=$(this).attr('data-value');
           var customclass=$(this).attr('class');
          if(custom=='Other_Custom')
         {   
            //if(customclass=='dropdown-option dropdown-chose'){
            if (customclass.indexOf('dropdown-chose') > -1){
                   $('.for_other_custom_choose').css('display','none');
            }
            else
            {
               $('.for_other_custom_choose').css('display','');
            }          
         }
         else{     
          }
      
        
         });
         $(this).find('.dropdown-clear-all').on('click',function(){
            $('.for_other_custom_choose').css('display','none');
         });
      });

      </script>
<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    });
}


   $( document ).ready(function() { 


        $(document).find('input[type="text"],input[type="number"]').each(function()
        {
            if($(this).val()!="")
            {
               $(this).css('color','#22b14c');
            }
        });
         //minDate:0,
       $(".dob_picker").datepicker({ dateFormat: 'dd-mm-yy', changeMonth: true,
           changeYear: true,  }).val();

         $(".date_picker_dob").datepicker({ dateFormat: 'dd-mm-yy', changeMonth: true,
           changeYear: true,  }).val();

       
       // $(".dob_picker").datepicker({ dateFormat: 'dd-mm-yy' }).val();
      //$(document).on('change','.othercus',function(e){
      });

      $(document).on('change','.othercus',function(e){
      //$(".othercus").change(function(){
         var custom = $(':selected',this).val();
         var client_id = $('#user_id').val();
          //alert(custom);
          
       if(custom=='Other')
         {
        // $('.contact_type').next('span').show();
        $(this).closest('.contact_type').next('.spnMulti').show();
       } else {
      $('.contact_type').next('span').hide();
       }    
         
         }); 

      function validateEmail($email) {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return emailReg.test( $email );
}
   $(document).ready(function(){
   
   
    //  .datepicker({ dateFormat: 'dd-mm-yy' }) 
   
    var save_exit = 'false';
   //alert(save_exit);
   $("#insert_form").validate({
   
          ignore: false,
 
            errorPlacement: function(error, element) {
                if (element.attr("name") == "append_cnt" )
                    //error.insertAfter(".for_contact_validation");
                      $('.for_contact_validation').append(error);
                 else if(element.attr("name")=="make_primary")
                      $('.for_contact_validation').append(error)
                 else
                    error.insertAfter(element);
            },
                           rules: {
                           company_name: {required: true},
                           //company_name1: {required: true},
                           company_name1:{ required : function(element){
                              var selectVal=$("#legal_form").val();
                           if((selectVal == "Private Limited company") || (selectVal == "Public Limited company") || (selectVal == "Limited Liability Partnership")) {
                              return true;
                            }
                            else
                            {
                              return false;
                            }
                     }
                    },
   
                           //company_number: {required: true},
                           company_number:{required : function(element){
                              var selectVal=$("#legal_form").val();
                           if((selectVal == "Private Limited company") || (selectVal == "Public Limited company") || (selectVal == "Limited Liability Partnership")) {
                              return true;
                            }
                            else
                            {
                              return false;
                            }
                     }},
                           company_status: {required: true},
                           user_name: {required: true},
                           emailid: {
                            //  required: true,
                            //  email:true
                            required : function(element){
                               var clickCheckbox = document.querySelector('#previous_account');
                               if(clickCheckbox.checked) // true
                               {
                                 return true;
                               }
                               else
                               {
                                 return false;
                               }
                           },
                           email : function(element){
                               var clickCheckbox = document.querySelector('#previous_account');
                               if(clickCheckbox.checked) // true
                               {
                                 return true;
                               }
                               else
                               {
                                 return false;
                               }
                           },
                           },
                          /* name:{required: true},
                           email_id:{required: true,email:true},*/
                           profile_image: {required: false, accept: "jpg|jpeg|png|gif"},
                           // gender:{required: true},
                           /*packages:{required: true},*/
                           password:{required: true,minlength : 5},
                           confirm_password : { minlength : 5,equalTo : "#password"},
                           refered_by: { required : true},
                           "main_email[]": { required : true},
                           "first_name[]": { required : true},

                           "proof_attach_file[]" :{ required : true},
                         
                           append_cnt:{
                                min:1
                           },
                           


                           },
                           errorElement: "span" , 
                           errorClass: "field-error",                             
                            messages: {
                              make_primary:"Select any one Contact as Primary",
                              append_cnt:"Please Add Contact",
                              // "first_name[]":"This field is Required",
                              // "main_email[]":"This field is Required",
                             company_name: "Give name",
                             company_name1: "Give company name",
                             company_number: "Give company number",
                             company_status: "Give company status",
                              refered_by: "Give Refered By Details",
                            /* name: "Give client name",
                             email_id:{
       required:"Please enter a email address",
       email:"Please enter a valid email address"
   
   },*/
    emailid:{
       required:"Please enter a email address",
       email:"Please enter a valid email address"
   
   },


     email_id: { required:"Please enter a email address",
       email:"Please enter a valid email address" },
                           first_name: {  required:"Please enter a First name", },



   profile_image: {required: 'Required!', accept: 'Not an image!'},
 //  gender:"Select gender",
   /*packages:"Enter a package",*/
   user_name:"Enter a Username",
   password: {required: "Please enter your password "},
   first_name 
                            },
   
                           
                           submitHandler: function(form) {
                                 save_exit = 'false';
                              
                            var formData = new FormData($("#insert_form")[0]);  
            
                               $(".LoadingImage").show();
                               $.ajax({ 
                                   url: '<?php echo base_url();?>client/insert_proposal_client/',
                                   dataType : 'json',
                                   type : 'POST',
                                   data : formData,
                                   contentType : false,
                                   processData : false,
                                   success: function(data) 
                                   {   
                                      if(data.return == '0')
                                      {
                                          // alert('failed');
                                           $('.alert-danger').show();
                                           $('.alert-success').hide();
                                           for_add_reminderintotask('<?php echo $proposal['company_id']; ?>');
                                       }
                                       else
                                       {   
                                           contact_add(data.return);
                                           // for_add_reminderintotask(data); // responsible member
                                       }                                     
                                  
                                   },
                                   error: function() {
                                        //contact_add(data);
                                       // $(".LoadingImage").hide();
                                     //  alert('faileddd');
                                     }
                               });
                              // }
                              //  return false;
   
                           } ,
                            /* invalidHandler: function(e,validator) {
           for (var i=0;i<validator.errorList.length;i++){   
               $(validator.errorList[i].element).parents('.panel-collapse.collapse').collapse('show');
           }
       }*/
        invalidHandler: function(e, validator) {
              if(validator.errorList.length)
           $('#tabs a[href="#' + jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
   
           }
   
                       });
   
   });
   
   
   // $("#insert_form1").validate({
   
   //        ignore: false,
     
   //                         rules: {
   //                         company_name: {required: true},
   //                        // company_number: {required: true},
                   
   //                         },
   //                         errorElement: "span", 
   //                         errorClass: "field-error",                             
   //                          messages: {
   //                           company_name: "Give company name",
   //                           //company_number: "Give company number",
                             
   //                          },
   
                           
   //                           submitHandler: function(form) {
   //                               save_exit = 'false';
   
   //                          var formData = new FormData($("#insert_form1")[0]);
   //                            // $(".LoadingImage").show();
   //                             $.ajax({
   //                                 url: '<?php echo base_url();?>client/insert_client/',
   //                                 dataType : 'json',
   //                                 type : 'POST',
   //                                 data : formData,
   //                                 contentType : false,
   //                                 processData : false,
   //                                 success: function(data) {
   
   //                                   $("#myReminders .close").click();
   // //alert(data);
   //                                      if(data == '0'){
   //                                        // alert('failed');
   //                                         $('.alert-danger').show();
   //                                         $('.alert-success').hide();
   //                                     }else{
   
   //                                    // contact_add(data);
   //                                     }
                                  
   //                                 },
   //                                 error: function() {
   //                                      //contact_add(data);
   //                                     // $(".LoadingImage").hide();
   //                                   //  alert('faileddd');
   //                                   }
   //                             });
   
   //                             return false;
   
   //                         } ,
   
   //                                 // location.reload();
                       
                                 
   //                          /* invalidHandler: function(e,validator) {
   //         for (var i=0;i<validator.errorList.length;i++){   
   //             $(validator.errorList[i].element).parents('.panel-collapse.collapse').collapse('show');
   //         }
   //     }*/
   //      invalidHandler: function(e, validator) {
   //            if(validator.errorList.length)
   //         $('#tabs a[href="#' + jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
   
   //         }
   
   //                     });
   
   function for_add_reminderintotask(id)
   { 
       var data = {'user_id':id};
      
       $.ajax({
       url: '<?php echo base_url();?>client/insert_remindertask/',      
       type : 'POST',
       data :data ,
    
       success: function(data) 
       {
          $(".LoadingImage").hide();

          if(data == 1)
          {            
             <?php if(!isset($_SESSION['permission'])){ unset($_SESSION['id']); } ?>
             $('.registered').find('.position-alert1').html("Registered Successfully!");   
             $('.registered').show();     
          }
          else
          {
             $('.registered').find('.position-alert1').html("Registration Failed.");
             $('.registered').show();            
          }          

          setTimeout(function(){ window.location.replace('<?php echo base_url().'login'; ?>');},3000);          
      
       },
       error: function() 
       {
            //contact_add(data);
           // $(".LoadingImage").hide();
         //  alert('faileddd');
       }
     });
   
  }
   
   
   function contact_add(id)
   { 
      $(".LoadingImage").show();   
      var incre=$('#incre').val();
   
      if(incre==''){
      incre = 1;
      }else{
      incre = parseInt(incre)+1;
      }
   
     $("#incre").val(incre);
  
     var data={};
     var title=[];

     $("select[name=title]").each(function(){
      title.push($(this).val());
     }); 
      
      data['title'] = title;
      data['user_id'] = '<?php echo $user['id']; ?>';
      data['client_id'] = $('#user_id').val();
      data['first_name'] = arr('first_name');
      data['middle_name'] = arr('middle_name');
      data['last_name'] = arr('last_name');
      data['surname'] = arr('surname');
      data['preferred_name'] = arr('preferred_name');
      data['mobile'] = arr('mobile_number');
      data['main_email'] = arr('main_email');
     
      for (var i = 1; i <=incre; i++) {
      data['landline'+i] = arr('landline'+i);
      data['work_email'+i] = arr('work_email'+i);
      }

      data['nationality'] = arr('nationality');
      data['psc'] = arr('psc');
      
      var pre_landline=[];   

       for (var i = 1; i <=incre; i++) {
           
       $("select[name=pre_landline"+i+"]").each(function(){
        pre_landline.push($(this).val());
       });
       data['pre_landline'+i] = pre_landline;
       }   
   
      var usertype=[];
      $("select[name=shareholder]").each(function(){
      usertype.push($(this).val());
      }); 

      var marital=[];
      $("select[name=marital_status]").each(function(){
      marital.push($(this).val());
      }); 

      var contacttype=[];
      $("select[name=contact_type]").each(function(){
      contacttype.push($(this).val());
      }); 
   
      data['ni_number'] = arr('ni_number');
      //data['content_type'] = arr('content_type');
      data['address_line1'] = arr('address_line1');
      data['address_line2'] = arr('address_line2');
      data['town_city'] = arr('town_city');
      data['post_code'] = arr('post_code');
      //data['landline'] = arr('landline');
      data['date_of_birth'] = arr('date_of_birth');
      data['nature_of_control'] = arr('nature_of_control');
      //data['marital_status'] = arr('marital_status');
      data['utr_number'] = arr('utr_number');
      data['occupation'] = arr('occupation');
      data['appointed_on'] = arr('appointed_on');
      data['country_of_residence'] = arr('country_of_residence');
      data['other_custom'] = arr('other_custom');
      data['client_id'] = id;
      data['marital_status'] = marital;
      data['contact_type'] = contacttype;
      data['shareholder'] = usertype;
      var selValue = $('input[name=make_primary]:checked').val(); 
      //data['make_primary'] = arr('make_primary');
   // alert(selValue);
      data['make_primary'] = selValue;
      data['make_primary_loop']=arr('make_primary_loop');
      data['event']='add';
   
       var cnt = $("#append_cnt").val();
   
      if(cnt==''){
      cnt = 1;
      }else{
      cnt = parseInt(cnt)+1;
      }
      $("#append_cnt").val(cnt);
      data['cnt'] =cnt;  

       $.ajax(
       {
         url: '<?php echo base_url();?>client/update_client_contacts/',
        
         type : 'POST',
         data : data,
         success: function(data) 
         {
           add_assignto(id);  
         },
       });
   
   }

   function arr(name)
   {
      var values = $("input[name='"+name+"[]']").map(function(){return $(this).val();}).get();
      return values;
   }
   
   
   function add_assignto(clientId)
   {
      var proposal_id = <?php echo $this->uri->segment(3); ?>;
      var data = {};
      $(".LoadingImage").show();   
    
      data['clientId'] = clientId;
      data['proposal_id'] = proposal_id;

      $.ajax(
      {
        url: '<?php echo base_url();?>client/add_assignto/',
        type: "POST",
        data: data,
        success: function(data)  
        {
            for_add_reminderintotask(clientId);
        }
      });
   }
   
   
 
  
  


     $(document).on('change','.js-small',function(){
   
     //  alert('Test');
   
      // var $grid = $('.masonry-container').masonry( 'reload' );
   
     $(this).closest('div').find('.field').prop('disabled', !$(this).is(':checked'));
   
     var check= $(this).data('id');
     var check1=$(this).attr('id');
     /** 10-09-2018 **/
     
                    // task investigation 
     if($(this).is(':checked')&&(check1=='investigation_create_task_reminder')){
     // alert('reminder chacked');
       $('.investigation_add_custom_reminder_label').css('display','');
     }
     else if(check1=='investigation_create_task_reminder')
     {
       $('.investigation_add_custom_reminder_label').css('display','none');
     }
               // cissub 
     if($(this).is(':checked')&&(check1=='cissub_create_task_reminder')){
       $('.cissub_add_custom_reminder_label').css('display','');
     }
     else if(check1=='cissub_create_task_reminder')
     {
       $('.cissub_add_custom_reminder_label').css('display','none');
     }
          // register 
     if($(this).is(':checked')&&(check1=='registered_create_task_reminder')){
       $('.registered_add_custom_reminder_label').css('display','');
     }
     else if(check1=='registered_create_task_reminder')
     {
       $('.registered_add_custom_reminder_label').css('display','none');
     }
     // bookkeep 
     if($(this).is(':checked')&&(check1=='bookkeep_create_task_reminder')){
       $('.bookkeep_add_custom_reminder_label').css('display','');
     }
     else if(check1=='bookkeep_create_task_reminder')
     {
       $('.bookkeep_add_custom_reminder_label').css('display','none');
     }
     // managemnt 
     if($(this).is(':checked')&&(check1=='manage_create_task_reminder')){
       $('.manage_add_custom_reminder_label').css('display','');
     }
     else if(check1=='manage_create_task_reminder')
     {
       $('.manage_add_custom_reminder_label').css('display','none');
     }
     // p11d 
     if($(this).is(':checked')&&(check1=='p11d_create_task_reminder')){
       $('.p11d_add_custom_reminder_label').css('display','');
     }
     else if(check1=='p11d_create_task_reminder')
     {
       $('.p11d_add_custom_reminder_label').css('display','none');
     }
      // cis
     if($(this).is(':checked')&&(check1=='cis_create_task_reminder')){
       $('.cis_add_custom_reminder_label').css('display','');
     }
     else if(check1=='cis_create_task_reminder')
     {
       $('.cis_add_custom_reminder_label').css('display','none');
     }
     // workplace pension ae
     if($(this).is(':checked')&&(check1=='pension_create_task_reminder')){
       $('.pension_create_task_reminder_label').css('display','');
     }
     else if(check1=='pension_create_task_reminder')
     {
       $('.pension_create_task_reminder_label').css('display','none');
     }
      // confirmation statement
     if($(this).is(':checked')&&(check1=='create_task_reminder')){
       $('.custom_remain').css('display','');
     }
     else if(check1=='create_task_reminder')
     {
       $('.custom_remain').css('display','none');
     }
      // PayRole
     if($(this).is(':checked')&&(check1=='payroll_create_task_reminder')){
       $('.payroll_add_custom_reminder_label').css('display','');
     }
     else if(check1=='payroll_create_task_reminder')
     {
       $('.payroll_add_custom_reminder_label').css('display','none');
     }
     // vat
     if($(this).is(':checked')&&(check1=='vat_create_task_reminder')){
       $('.vat_add_custom_reminder_label').css('display','');
     }
     else if(check1=='vat_create_task_reminder')
     {
       $('.vat_add_custom_reminder_label').css('display','none');
     }
     // personal tax
     if($(this).is(':checked')&&(check1=='personal_task_reminder')){
       $('.personal_custom_reminder_label').css('display','');
     }
     else if(check1=='personal_task_reminder')
     {
       $('.personal_custom_reminder_label').css('display','none');
     }
     //accounts
     if($(this).is(':checked')&&(check1=='accounts_create_task_reminder')){
       $('.accounts_custom_reminder_label').css('display','');
     }
     else if(check1=='accounts_create_task_reminder')
     {
       $('.accounts_custom_reminder_label').css('display','none');
     }
     /** 07-09-2018 **/
     // insurance
     if($(this).is(':checked')&&(check1=='insurance_create_task_reminder')){
       $('.insurance_add_custom_reminder_label').css('display','');
     }
     else if(check1=='insurance_create_task_reminder')
     {
       $('.insurance_add_custom_reminder_label').css('display','none');
     }
     // company tax
     if($(this).is(':checked')&&(check1=='company_create_task_reminder')){
       $('.company_custom_reminder_label').css('display','');
     }
     else if(check1=='company_create_task_reminder')
     {
       $('.company_custom_reminder_label').css('display','none');
     }
     /** en dof 07-09-2018 **/
      /** start 27-08-2018 **/
      if($(this).is(':checked')&&(check1=='client_id_verified')){ // amlcheck checkbox change enabled 
        $('.client_id_verified_data').css('display','');
        var person=$('#person').val();
        //alert(person);
        if(person!=''){
        
            result = person.toString().split(',');
            if($.inArray("Other_Custom",result)!=-1)
            {
            $('.for_other_custom_choose').css('display','');
            }
            else{
            $('.for_other_custom_choose').css('display','none');
            }
        
         }

      }
      else if(check1=='client_id_verified')
      {
         $('.client_id_verified_data').css('display','none');
         $('.for_other_custom_choose').css('display','none'); // for for other custom textbox hide
      }

    });
</script>

<script>
    $( document ).ready(function() {   

        var date = $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy',minDate:0 }).val();

        // Multiple swithces
        var elem = Array.prototype.slice.call(document.querySelectorAll('.js-small'));

        elem.forEach(function(html) {
            var switchery = new Switchery(html, {
                color: '#22b14c',
                jackColor: '#fff',
                size: 'small'
            });
        });
		
		$('.dropdown-sin-11').dropdown({
     
        input: '<input type="text" maxLength="20" placeholder="Search">'
      });
	  
	  var start = new Date();
    start.setFullYear(start.getFullYear() - 70);
    var end = new Date();
    end.setFullYear(end.getFullYear());
      $(".date_picker_dob").datepicker({ dateFormat: 'dd-mm-yy', changeMonth: true,
        changeYear: true,yearRange: start.getFullYear() + ':' + end.getFullYear() }).val();
	  
	   $('.js-small').parent('.col-sm-8').addClass('addbts-radio');
	   
		 });
		 </script>

</body>
</html>
