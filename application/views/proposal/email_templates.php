<?php 

if($_SESSION['firm_id'] == 0)
{
   $this->load->view('super_admin/superAdmin_header');
}
else
{
   $this->load->view('includes/header');
}

?>

<style type="text/css">

   .dropdown-content 
   {
     display: none;
     position: absolute;
     background-color: #fff;
     min-width: 86px;
     overflow: auto;
     box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
     z-index: 1;
     left: -92px;
     width: 150px;
  }

  .modal-alertsuccess a.close 
  {  
    margin-top: 5px;
    margin-right: 15px;
  } 
  .pop-realted1 
  {
     padding: 10px;
     background-color: #fff;
  }
 

</style>

<div class="pcoded-content pcodedteam-content">
   <div class="pcoded-inner-content">
      <!-- Main-body start -->
      <div class="main-body">
         <div class="page-wrapper">
            <!-- Page body start -->
            <div class="page-body">
               <div class="row">
             <?php echo $this->session->flashdata('alert-currency'); ?>    
                  <!--start-->
                  <div class="col-sm-12">
                     <!-- Register your self card start -->
                     <div class="card propose-template">
                        <!-- admin start-->                         
                          
                           <div class="modal-alertsuccess alert alert-success" <?php if(empty($_SESSION['success_msg'])){ echo 'style="display:none;"'; } ?>>
                              <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                              <div class="pop-realted1">
                                 <div class="position-alert1">
                                    <?php if(!empty($_SESSION['success_msg'])){ echo $_SESSION['success_msg']; } ?>
                                 </div>
                              </div>
                           </div>
                           
                           <div class="modal-alertsuccess alert alert-danger" style="display:none;">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <div class="pop-realted1">
                                 <div class="position-alert1">
                                    
                                 </div>
                              </div>
                           </div>
                           <!-- List -->

                      <div class="deadline-crm1 floating_set">  
                            <ul class="nav nav-tabs all_user1 md-tabs floating_set u-dashboard">
                              <li class="nav-item borbottomcls">
                                <a class="nav-link"  href="javascript:void(0)">Email Templates</a>
                                <div class="slide"></div>
                              </li>
                            </ul>
                      <?php if($_SESSION['permission']['Email_Template']['create'] == '1') { ?>     
                      <div class="setting-realign1">
                      <div class="count-value1 cv pull-right">                          
                      <a class="btn btn-primary" href="javascript:void(0);" onclick="getframe('<?php echo base_url().'Email_template/add_template'; ?>');">Add Email Template</a> 
                      <a class="btn btn-success" onclick="getframe('<?php echo base_url().'Email_template/add_reminder'; ?>');" href="javascript:void(0);">Add Reminder Template</a>  
                      </div>
                      </div> 
                     <?php } ?>
                      </div>

                      <div class="deadline-crm1 floating_set col-md-2 pull-right">
                      <button type="button" data-toggle="modal" data-target="#deleteconfirmation" id="delete_template" class="btn btn-danger del-tsk12 delete"  style="display: none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i> Delete </button>
                      </div>
                    </div>
                    
                    <div class="all_user-section floating_set pros-editemp">                               
                     <div class="all_user-section6 floating_set">
            <div class="tab-content">
            <div id="allusers" class="tab-pane fade in active">
            <div id="task"></div>
            <div class="client_section3 table-responsive">
            <div class="status_succ"></div>
            <div class="all-usera1 data-padding1 ">                                 
                        <div class="">
            <table class="table client_table1 text-center display nowrap printableArea" id="display_service1" cellspacing="0" width="100%">
              <thead>
                <tr class="text-uppercase">
                <?php if($_SESSION['permission']['Email_Template']['delete'] == '1' && $_SESSION['firm_id'] == '0') { ?>
                <th class="">                                  
                    <label class="custom_checkbox1" style="margin-left: 5px;">
                    <input type="checkbox" id="select_all_templates">
                    <i></i>
                    </label>                                        
                </th>   
                <?php }else if($_SESSION['firm_id'] != '0'){ ?>
                  <th></th>
                <?php } ?>
                <th>S.no
                <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                </th>
                <th>Template Title
                <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                </th>
                <th>Template Type
                <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                </th>
                <th>Status
                <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                </th>
                <th>Created At
                <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                </th>
                <th>Status Action
                <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><!-- <div class="sortMask"></div> -->
                <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                <?php if($_SESSION['permission']['Email_Template']['edit'] == '1') { ?>
                <th>Action</th>
                <?php } ?>
                </tr>
              </thead>
                <tfoot>
                  <tr class="table-header">
                  <?php if($_SESSION['permission']['Email_Template']['delete'] == '1') { ?>
                  <th></th>
                  <?php } ?>
                  <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                  <th ><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                  <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                  <th ><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                  <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                  <th ></th>
                  <?php if($_SESSION['permission']['Email_Template']['edit'] == '1') { ?>
                  <th></th>  
                  <?php } ?>                 
                  </tr>
                    </tfoot>
              <tbody>
                <?php
                $i=1;
                foreach($email_templates as $value){ ?>
                <tr>
                <?php if($_SESSION['permission']['Email_Template']['delete'] == '1') { ?>
                <td>
                <?php if($value['firm_id'] == $_SESSION['firm_id']) { ?>
                <label class="custom_checkbox1">
                <input type="checkbox" class="template_checkbox" data-template-id="<?php echo $value['id'];?>"><i></i>   
                </label>
                <?php } ?>
                </td>
               <?php } ?>
                <td><?php echo $i; ?></td>
                <td><?php if($value['reminder_heading'] == ''){ echo ucwords($value['title']); }else{ echo ucwords($value['reminder_heading']); }  ?></td>
                <td>
                <?php                 
                if($value['reminder_heading'] == ''){  echo "Email Template"; } else{ echo "Reminder Template"; }    
                ?>                                    
                </td>
                <td><?php if($value['status'] == '1'){ echo "Active"; } else { echo "inactive"; } ?></td>
                <td><?php echo date('d-m-Y h:i a',strtotime($value['created_at'])); ?></td>
                <td>
                <select <?php if($_SESSION['permission']['Email_Template']['edit'] != '1') { echo "disabled"; } ?> name="status" class="test btn btn-xs green dropdown-toggle" id="status" onchange="UpdateStatus('<?php echo $value['id']; ?>',this);" > 
                <?php $status_action=$value['status'];?>    
                <option value="none">Select</option>
                <option value="active" <?php if($status_action == '1'){ echo "selected='selected'"; } ?>>Active</option>
                <option value="inactive" <?php if($status_action == '0'){ echo "selected='selected'"; } ?>>Inactive</option>
                </select>
                </td>
                <?php if($_SESSION['permission']['Email_Template']['edit'] == '1') { ?>
                <td>
                <?php                                    
                                  
                     if($value['reminder_heading'] != '')
                     {
                        $uris = 'edit_reminder/'.$value['id'];
                     }
                     else
                     {
                        $uris = 'edit_template/'.$value['id'];                        
                     }
                ?>                
                <a class="edit_Template" href="javascript:void(0);" onclick="getframe('<?php echo base_url().'Email_template/'.$uris;?>');"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>           
                </td>   
                <?php } ?>                                                       
                </tr>                
                <?php $i++; }  ?>   
              </tbody>
                           </table>
                        <input type="hidden" class="rows_selected" id="select_template_count" >     
                        </div>
                     </div>
                     <!-- List -->           
                  </div>
               </div>
            </div>
         </div>
      </div>      

       <div class="modal fade" id="deleteconfirmation" role="dialog">
         <div class="modal-dialog">    
           <!-- Modal content-->
           <div class="modal-content">
             <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Confirmation</h4>
             </div>
             <div class="modal-body">
             <input  type="hidden" name="delete_template_id" id="delete_template_id" value="">
               <p> Are you sure want to delete ?</p>
             </div>
             <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal" id="delete-yes">Yes</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
             </div>
           </div>
         </div>
      </div>      

                  
                        <!-- Page body end -->
                     </div>
                  </div>
                  <!-- Main-body end -->
                  <div id="styleSelector">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>



<?php 

$this->load->view('includes/session_timeout');

if($_SESSION['firm_id'] == 0)
{
   $this->load->view('super_admin/superAdmin_footer');
}
else
{
   $this->load->view('includes/footer');
}

?>
 
<script type="text/javascript"  src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

<script type="text/javascript">

    $(document).ready(function()
    {
         <?php if(!empty($_SESSION['fail_msg'])){ ?>
              $('.alert-danger').show();
         <?php } ?>
    });

    var table10;

    $(document).ready(function()
    {           
      var check=0;
      var check1=0;
      var numCols = $('#display_service1 thead th').length;
      table10 = $('#display_service1').DataTable(
      {
          "dom": '<"top"fl>rt<"bottom"ip><"clear">',
          initComplete: function () { 
                 var q=1;
                    $('#display_service1 thead th').find('.filter_check').each( function(){
                    // alert('ok');
                      $(this).attr('id',"all_"+q);
                      q++;
                    });
                 for(i=1;i<numCols;i++){                 
                       var select = $("#all_"+i); 
                     
                     this.api().columns([i]).every( function () {
                       var column = this;
                       column.data().unique().sort().each( function ( d, j ) {    
                     //    console.log(d);         
                         select.append( '<option value="'+d+'">'+d+'</option>' )
                       });
                     }); 
                  
          
                    $("#all_"+i).formSelect();  
                 }
              }
       });

        for(j=1;j<numCols;j++)
        {  
           $('#all_'+j).on('change', function(){ 
             var result=$(this).attr('id').split('_');      
              var c=result[1];
               var search = [];              
               $.each($('#all_'+c+ ' option:selected'), function(){                
               search.push($(this).val());
               });      
               search = search.join('|');    
              

               table10.column(c).search(search, true, false).draw();  
           });
        }     
    });
 

  $(document).on('click','.DT', function (e) 
  {
      if(!$(e.target).hasClass('sortMask')) 
      {      
          e.stopImmediatePropagation();
      }
  });

   $(document).on('click', 'th .themicond', function()
   { 
        if ($(this).parent().find(".dropdown-content").hasClass("Show_content")) {
           $(this).parent().find('.dropdown-content').removeClass('Show_content');
        }else{
           $('.dropdown-content').removeClass('Show_content');
           $(this).parent().find('.dropdown-content').addClass('Show_content');
        }
            $(this).parent().find('.select-wrapper').toggleClass('special');
        if( $(this).parent().find('.select-dropdown span input[type="checkbox"]').parent().hasClass('custom_checkbox1')){
        }else{
            $(this).parent().find('.select-dropdown span input[type="checkbox"]').wrap( '<label class="custom_checkbox1"></label>');
            $(this).parent().find('.custom_checkbox1 input').after( "<i></i>" );
        }       
   });   
   
   $(document).ready(function()
   {
     window.alltemplate = [];
   });

   function getSelectedRow(id)
   {              
      alltemplate.push(id);   
      return $.unique(alltemplate);
   }

   function return_indice(id)
   { 
      for(var i=0;i<alltemplate.length;i++)
      {
         if(id == alltemplate[i])
         {
            return i;
         }          
      }
   }

   function dounset(id)
   {
      alltemplate = $.unique(alltemplate);
      var index = return_indice(id);
       
      if(index > -1) 
      {
         alltemplate.splice(index, 1);
      }

      return alltemplate;
   }

  $('.template_checkbox').click(function()
  {  
       var id = $(this).data('template-id');
       var all_temp;       

       if($(this).is(":checked"))
       {         
          all_temp = getSelectedRow(id);
       }
       else
       {
          $('#select_all_templates').prop('checked',false);
          all_temp = dounset(id);  
       }

      showdelete(all_temp); 
  }); 

  $('#delete-yes').click(function() 
  { 
       $('#delete_template').hide();
       var record_ids = $("#delete_template_id").val();

       $.ajax(
       {
          url: '<?php echo base_url(); ?>Email_template/delete_template',
          type: 'POST',
          data: {'record_ids':record_ids},

          beforeSend: function() 
          {          
             $(".LoadingImage").show();
          },
          success:function(status)
          { 
             $(".LoadingImage").hide();
             
             if(status == 1)
             {           
                $('.alert-success').show();
                $('.position-alert1').html("Template(s) Deleted Successfully!");
             }
             else
             {
              $('.alert-danger').show();
             }

             location.reload(); 
          }

       });
  });

  $(document).on('change','#select_all_templates', function() 
  { 
      var all_temp;

      if($(this).is(':checked', true)) 
      {  
          table10.column(0).nodes().to$().each(function(index) 
          {                      
              $(this).find(".template_checkbox").prop('checked',true);
              var id = $(this).find(".template_checkbox").data('template-id');  
              all_temp = getSelectedRow(id);                            
          }); 
      }
      else
      {
          table10.column(0).nodes().to$().each(function(index) 
          {                      
              $(this).find(".template_checkbox").prop('checked',false);
          });
          all_temp = [];
          alltemplate = [];
      } 
      showdelete(all_temp);        
  });   

  function showdelete($array)
  { 
     $("#delete_template_id").val(JSON.stringify($array));

     if($("#delete_template_id").val().length == "2" || $("#delete_template_id").val().length == "0")
     {
        $('#delete_template').hide();
     }
     else
     {               
        $('#delete_template').show();
     }          
  }

  function UpdateStatus(rid,obj)
  {
      var status = obj.value;

      $.ajax(
      {
         url : '<?php echo base_url().'Email_template/updateStatus'; ?>',
         type : 'POST',
         data : {'rid':rid,'status':status},

         success:function(status)
         {
            if(status != 0)
            {
               $('.alert-success').show();
               $('.position-alert1').html(status);
               location.reload();
            }
            else
            {
              $('.alert-danger').show();
            }
         }

      });
  }

  function getframe($url)
  {
     var windowObjectReference;
     var WindowFeatures = "width=850,height=600,left=50.resizable=yes,menubar=no,titlebar=no,status=no,location=no";
     windowObjectReference = window.open($url, '', WindowFeatures);
  }

  /*function getframe(heading,$url)
  {
     $('#template_iframe').find('.modal-title').html('');
     $('#template_iframe').find('.modal-title').html(heading);

     $.ajax(
     {
        url:$url,
        async:'true',
        type:'POST',     

        success:function(response)
        {
           $('#template_iframe').find('.modal-body').html('');
           $('#template_iframe').find('.modal-body').html(response);
           $('#temp_butt').trigger('click');
        }

     });
  }   */

</script>