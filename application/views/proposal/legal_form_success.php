<div id="chart_Donut11" style="width: 100%; height: 409px;"></div>
 <script type="text/javascript" src="http://remindoo.org/CRMTool/bower_components/jquery/js/jquery.min.js"></script>
 <script type="text/javascript">
 //image_ajax();
 </script>
<!-- <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/custom-google-chart.js"></script>

<!-- <script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1.1','packages':['bar']}]}"></script> -->
<script type="text/javascript">
  google.charts.load("current", {packages:["corechart"]});
  google.charts.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['Legal Form', 'Count'],
      <?php
      $legal_form=$_SESSION['client_legal_form'];
        if($legal_form!='all'){       
        if($legal_form=='Public Limited company')  {  ?>
           ['Public Limited company',      <?php echo $Public['limit_count']; ?>],
           ['others',      <?php echo $others['limit_count']; ?>],
        <?php } 
        if($legal_form=='Limited Liability Partnership')  {  ?>
           ['Limited Liability Partnership',  <?php echo $limited['limit_count']; ?>],
           ['others',      <?php echo $others['limit_count']; ?>],
        <?php }
        if($legal_form=='Private Limited company')  {   ?>
           ['Private Limited company',     <?php echo $Private['limit_count']; ?>],
           ['others',      <?php echo $others['limit_count']; ?>],
        <?php } 
        if($legal_form=='Partnership')  {  ?>        
           ['Partnership', <?php echo $Partnership['limit_count']; ?>],
           ['others',      <?php echo $others['limit_count']; ?>],
        <?php } 
         if($legal_form=='Self Assessment')  {   ?>
           ['Self Assessment',    <?php echo $self['limit_count']; ?>],
           ['others',      <?php echo $others['limit_count']; ?>],
      <?php } 
        if($legal_form=='Trust')  {    ?>
           ['Trust',<?php echo $Trust['limit_count']; ?>],
           ['others',      <?php echo $others['limit_count']; ?>],
      <?php }
        if($legal_form=='Charity')  {    ?>
          ['Charity',<?php echo $Charity['limit_count']; ?>],
          ['others',      <?php echo $others['limit_count']; ?>],
      <?php  if($legal_form=='Other')  {    ?>
          ['Other',<?php echo $Other['limit_count']; ?>],
          ['others',      <?php echo $others['limit_count']; ?>],
       <?php }} ?>
    ]);
  
    var options = {
      title: 'Client Lists',
      pieHole: 0.4,
    };
  
    var chart_div = new google.visualization.PieChart(document.getElementById('chart_Donut11'));
    var chart = new google.visualization.PieChart(chart_Donut11);
    google.visualization.events.addListener(chart, 'ready', function () {
    chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
    //document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '"></a>';
    var image=chart.getImageURI();
	  image_ajax(image);
                 
  });
    chart.draw(data, options);
  }
  function image_ajax(image){
  //alert('ok12');
  $.ajax({
       type: "POST",
       url: "<?php echo base_url();?>Reports/pdf_download",         
       data: {image: image},
       success: 
         function(data){
         }
     });
} 


</script>