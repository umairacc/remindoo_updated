<script type="text/javascript">
	$(document).ready(function() {
		window.cursorPos = "";
		window.textNode = "";
		window.target = "";
		window.exist_html = "";

		function getEditTd() {
			var thead = $("#proposal_contents_details").find('table thead tr').children('td #dd-head');
			var tbody = $("#proposal_contents_details").find('table tbody tr').children('td #dd-body');
			var tfoot = $("#proposal_contents_details").find('table tfoot tr').children('td #dd-footer');
			var edit_td;

			if ($(thead).hasClass('ui-sortable-disabled')) {
				edit_td = $(thead).children('table .editable-open').find('tbody tr td');
			} else if ($(tbody).hasClass('ui-sortable-disabled')) {
				edit_td = $(tbody).children('table .editable-open').find('tbody tr td');
			} else if ($(tfoot).hasClass('ui-sortable-disabled')) {
				edit_td = $(tfoot).children('table .editable-open').find('tbody tr td');
			}

			return edit_td;
		}

		$(document).on('dragend', '#price_table', function() {
			var td = getEditTd();
			$(td).children('div .editable-content').children('p:last-child').append('[Price Table]');
		});

		$(document).on('mousedown', '.medium-editor-element', function(event) {
			var range;
			target = $(event.target);

			if (document.caretPositionFromPoint) {
				range = document.caretPositionFromPoint(event.clientX, event.clientY);
				textNode = range.offsetNode;
				cursorPos = range.offset;
			} else if (document.caretRangeFromPoint) {
				range = document.caretRangeFromPoint(event.clientX, event.clientY);
				textNode = range.startContainer;
				cursorPos = range.startOffset;
			}

			exist_html = $(textNode).text();
		});

		/*document.addEventListener('paste', function(event) 
       {
          var textpasting = event.clipboardData.getData('text/plain');

          setTimeout(function()
          {        
   	       if(edit_template == 'on' && textpasting != null && textpasting != false && textpasting != undefined && textNode != null && textNode != false && textNode != undefined && cursorPos != null && cursorPos != false && cursorPos != undefined && exist_html.length == $(textNode).text().length)
   	       { 
   	         var alldata = $(textNode).text();
   	         var textBefore = $(textNode).text().substring(0,cursorPos); 
   	         var textAfter  = alldata.substring(cursorPos,alldata.length);

   	         if(textBefore.length > '0')
   	         {
   	         	 textpasting = textBefore.concat(textpasting);
   	         }

   	         if(textAfter.length > '0')
   	         {
   	         	 textpasting = textpasting.concat(textAfter);
   	         }

   	         $(target).html('');
   	         $(target).html(textpasting);
   	       }
   	   },2000);    

       }, false);*/

		$(document).bind('paste', '.medium-editor-element', function(event) {
			$(".LoadingImage").show();
			var textpasting = event.originalEvent.clipboardData.getData('text/plain');

			var alldata = $(textNode).text();
			var textBefore = $(textNode).text().substring(0, cursorPos);
			var textAfter = alldata.substring(cursorPos, alldata.length);

			if (textBefore.length > '0') {
				textpasting = textBefore.concat(textpasting);
			}

			if (textAfter.length > '0') {
				textpasting = textpasting.concat(textAfter);
			}

			$(target).html('');
			$(target).html(textpasting);
			$(".LoadingImage").hide();
			// 	setTimeout(function()
			// 	{     
			//        if(textpasting != null && textpasting != false && textpasting != undefined && textNode != null && textNode != undefined && cursorPos != null && cursorPos != false && cursorPos != undefined && exist_html.length == $(textNode).text().length)
			//        { 

			//        }
			//        $(".LoadingImage").hide();
			//    },2000);   
		});

		$(document).on('click', '.save-link,.save-img', function() {
			if ($(this).hasClass('save-img')) {
				$('.alert-tem').hide();
			} else if ($(this).hasClass('save-link')) {
				var url = $(this).parent().find('input[data-type="url"]');

				if (checkUrl(url) == true) {
					$('.alert-tem').hide();
				}
			}
		});

		$(document).on('click', '.popover-lg button', function() {
			var timeout;

			if ($(this).hasClass('btn-info')) {
				timeout = setInterval(function() {
					if ($('#fileInput').parent().is(':visible') == false) {
						$('.alert-tem').hide();
						clearInterval(timeout);
					}
				}, 1000);
			} else if ($(this).attr('id') == 'DeleteDynamicImage1') {
				timeout = setInterval(function() {
					if ($('#images_div img').attr('src').indexOf('placeholder_image_upload.png') != '-1') {
						$('.alert-tem').hide();
						clearInterval(timeout);
					}
				}, 1000);
			}
		});

		function checkUrl(object) {
			regexp = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;

			if ($(object).val() != "" && regexp.test($(object).val())) {
				return true;
			} else {
				$(object).attr('style', 'border:1px solid red !important;');
			}
		}
	});
</script>