<?php
  $cur_symbols = $this->Common_mdl->getallrecords('crm_currency');

  foreach ($cur_symbols as $key => $value) 
  {
     $currency_symbols[$value['country']] = $value['currency'];
  }
?>

<?php $this->load->view('includes/header');?>

 <!--   Datatable header by Ram -->
 <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/email.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css?ver=2"> -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/colreorder/1.5.1/css/colReorder.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/buttons.dataTables.min.css?ver=2">
 <!--   End by Ram -->

<style type="text/css">
 .dropdown-content {
    display: none;
    position: absolute;
    background-color: #fff;
    min-width: 86px;
    overflow: auto;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    left: -92px;
    width: 150px;
}

</style>
<!-- <link rel="stylesheet" type="text/css" href="<?php echo  base_url()?>assets/css/_label-badges.scss"> -->

                        <div class="modal-alertsuccess alert alert-success1" style="display:none;">
                        <div class="newupdate_alert">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                           <div class="pop-realted1">
                              <div class="position-alert1">
                                Unarchieved Successfully
                              </div>
                           </div>
                        </div>
                        </div>
                        <div class="modal-alertsuccess alert alert-danger-check_proposal" style="display:none;">
                          <div class="newupdate_alert">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <div class="pop-realted1">
                                <div class="position-alert1">
                                Please select records.
                                </div>
                            </div>
                          </div>
                        </div>
                           <div class="modal-alertsuccess alert alert-danger-check_send" style="display:none;">
                            <div class="newupdate_alert">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <div class="pop-realted1">
                                 <div class="position-alert1">
                                    Please select records.
                                 </div>
                              </div>
                           </div>
                           </div>
                           <div class="modal-alertsuccess alert alert-danger-check_view" style="display:none;">
                            <div class="newupdate_alert">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <div class="pop-realted1">
                                 <div class="position-alert1">
                                    Please select records.
                                 </div>
                              </div>
                           </div>
                           </div>
                           <div class="modal-alertsuccess alert alert-danger-check_indiscussion" style="display:none;">
                            <div class="newupdate_alert">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <div class="pop-realted1">
                                 <div class="position-alert1">
                                    Please select records.
                                 </div>
                              </div>
                           </div>
                           </div>
                           <div class="modal-alertsuccess alert alert-danger-check_archieve" style="display:none;">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <div class="pop-realted1">
                                 <div class="position-alert1">
                                    Please select records.
                                 </div>
                              </div>
                           </div>
                           <div class="modal-alertsuccess alert alert-danger-check_draft" style="display:none;">
                            <div class="newupdate_alert">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <div class="pop-realted1">
                                 <div class="position-alert1">
                                    Please select records.
                                 </div>
                              </div>
                           </div></div>
                           <div class="modal-alertsuccess alert alert-danger-check_expired" style="display:none;">
                            <div class="newupdate_alert">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <div class="pop-realted1">
                                 <div class="position-alert1">
                                    Please select records.
                                 </div>
                              </div>
                           </div>
                           </div>
                           <div class="modal-alertsuccess alert alert-danger-check_accept" style="display:none;">
                            <div class="newupdate_alert">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <div class="pop-realted1">
                                 <div class="position-alert1">
                                    Please select records.
                                 </div>
                              </div>
                           </div>
                           </div>
                           <div class="modal-alertsuccess alert alert-danger-check_decline" style="display:none;">
                            <div class="newupdate_alert">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <div class="pop-realted1">
                                 <div class="position-alert1">
                                    Please select records.
                                 </div>
                              </div>
                           </div>
                           </div>
                        <div class="modal-alertsuccess alert alert-success-delete" style="display:none;">
                         <div class="newupdate_alert">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <div class="pop-realted1">
                                 <div class="position-alert1">
                                    YOU HAVE SUCCESSFULLY DELETED                              
                                 </div>
                              </div>
                           </div>  
                           </div>   

                <div class="modal fade" id="delete_proposal_id" role="dialog">
                  <div class="modal-dialog">
                  
                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Confirmation</h4>
                      </div>
                      <div class="modal-body">
                        <input type="hidden" name="delete_ids" id="delete_ids" value="">
                        <p>Do you want to delete ?</p>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default delete_proposal">Yes</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal" id="del_close">No</button>
                      </div>
                    </div>
                    
                  </div>
                </div>
              <!--   <div class="modal-alertsuccess" id="delete_proposal_id" style="display:none;">
                <div class="newupdate_alert">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <input type="hidden" name="delete_ids" id="delete_ids" value="">
                  <div class="pop-realted1">
                  <div class="position-alert1">
                  Are you sure want to delete <b><a href="javascript:;" class="delete_proposal"> Yes </a></b> OR <b><a href="javascript:;" id="close">No</a></b></div>
                  </div></div>   </div> -->  



                        
                <div class="common-reduce01 floating_set mainbgwhitecls">
                  <div class="graybgclsnew">
                 <div class="deadline-crm1 floating_set">
                    <?php $this->load->view('proposal/proposal_navigation_tabs'); ?>            
                    <div class="create-proposal pro-dash newclscret">
                      <!-- <a href="<?php echo base_url();?>proposal_page/step_proposal" class="create-proposalbtn btn btn-primary">create proposal</a> -->
                     <?php if($_SESSION['permission']['Proposal_Dashboad']['delete'] == '1') { ?>
                      <button class="btn btn-danger del-tsk12 f-right delete" data-toggle="modal" data-target="#delete_proposal_id" id="proposal_delete_items"  style="display: none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i>Delete</button>
                    <?php } ?>

                    <!--   <button class="btn btn-danger del-tsk12 f-right delete" id="send_delete_items" style="display: none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i>Delete</button>

                      <button class="btn btn-danger del-tsk12 f-right delete" id="view_delete_items" style="display: none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i>Delete</button>

                      <button class="btn btn-danger del-tsk12 f-right delete" id="accept_delete_items" style="display: none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i>Delete</button>

                      <button class="btn btn-danger del-tsk12 f-right delete" id="decline_delete_items" style="display: none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i>Delete</button>

                      <button class="btn btn-danger del-tsk12 f-right delete" id="indiscussion_delete_items" style="display: none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i>Delete</button>

                      <button class="btn btn-danger del-tsk12 f-right delete" id="draft_delete_items" style="display: none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i>Delete</button>

                      <button class="btn btn-danger del-tsk12 f-right delete" id="expired_delete_items" style="display: none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i>Delete</button>

                      <button class="btn btn-danger del-tsk12 f-right delete" id="archive_delete_items" style="display: none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i>Delete</button>
 -->
                    </div>
                    

                    
                  </div>
   <div class="tab-leftside ">
      <div class="right-box-05 leftsidesection">
         <h2>Proposals</h2>      
         <?php
         $sum=0;
            $send_array=array();
            $send_count=0;
            $send_amount=0;
            $view_array=array();
            $view_count=0;
            $view_amount=0;
            $draft_array=array();
            $draft_count=0;
            $draft_amount=0;
            $indiscussion_array=array();
            $indiscussion_count=0;
            $indiscussion_amount=0;
            $decline_array=array();
            $decline_count=0;
            $decline_amount=0;
            $accepted_array=array();
            $accepted_count=0;
            $accepted_amount=0;
            $expired_array=array();
            $expired_count=0;
            $expired_amount=0;
            $archive_array=array();
            $archive_count=0;
            $archive_amount=0;

          
             foreach($proposal as $key => $value ){
              if($value['status']=='sent')
              {
                $send_count+=1;
                $send_amount+= array_sum(array_map('intval', explode(',', $value['price'])));
                array_push($send_array, $value['price']);
            
              }
              if($value['status']=='opened')
              {
                $view_count+=1;
                $view_amount+= array_sum(array_map('intval', explode(',', $value['price'])));
                array_push($view_array, $value['price']);
            
              }
              if($value['status']=='draft')
              {
                $draft_count+=1;
                $draft_amount+= array_sum(array_map('intval', explode(',', $value['price'])));
                array_push($draft_array, $value['price']);
            
              }
              if($value['status']=='in discussion')
              {
                $indiscussion_count+=1;
                $indiscussion_amount+= array_sum(array_map('intval', explode(',', $value['price'])));
                array_push($indiscussion_array, $value['price']);
            
              }
              if($value['status']=='declined')
              {
                $decline_count+=1;
                $decline_amount+= array_sum(array_map('intval', explode(',', $value['price'])));
                array_push($decline_array, $value['price']);
            
              }
                if($value['status']=='expired')
              {
                $expired_count+=1;
                $expired_amount+= array_sum(array_map('intval', explode(',', $value['price'])));
                array_push($expired_array, $value['price']);
            
              }

               if($value['status']=='archive')
              {
                $archive_count+=1;
                $archive_amount+= array_sum(array_map('intval', explode(',', $value['price'])));
                array_push($archive_array, $value['price']);
            
              }

                if($value['status']=='accepted')
              {
                $accepted_count+=1;
                $accepted_amount+= array_sum(array_map('intval', explode(',', $value['price'])));
                array_push($accepted_array, $value['price']);
            
              }

               $sum=$send_count+$view_count+$decline_count+$accepted_count;

            
             }

              // echo $sum;
              //  echo $archive_count;
              //  echo $expired_count;
              //  echo $indiscussion_count;
              //  echo $draft_count;
               $sum1=$sum +$archive_count+$expired_count+$indiscussion_count+$draft_count;
             //  echo $sum1;
             ?>
         <ul class="nav nav-tabs all_user1 md-tabs floating_set prosaltempmlateulcls proposal_tabs">
            <li>
               <a class="<?php 
                  if(isset($this->session->userdata['tab_name'])){                
                    }else{
                   echo "active-previous1 active";
                   } ?>" data-toggle="tab" href="javascript:;" class="task_status_val" data-id="proposals" data-searchCol="status_TH">
               <span class="sent-left"><span>Proposal</span><strong>List of proposals</strong></span>
               <span class="sent-proposalcount badge badge-primary"><?php echo $sum1; ?></span>
               </a>
               <div class="sub-categories1">
                  <h3>By Status</h3>
                  <ul class="category_list mail-proposal">
                     <li><a href="javascript:;" class="task_status_val" data-id="" data-searchCol="status_TH" data-toggle="tab">All <span class="badge badge-primary"><?php echo $sum1; ?></span></a></li>
                     <li><a href="javascript:;" data-id="sent" class="task_status_val" data-searchCol="status_TH" data-toggle="tab">Sent <span class="badge badge-primary"><?php echo $send_count; ?></span></a></li>
                     <li><a href="javascript:;" class="task_status_val" data-id="viewed" data-searchCol="status_TH" data-toggle="tab">Viewed <span class="badge badge-primary"><?php echo $view_count;?></span></a></li>
                     <li><a href="javascript:;" class="task_status_val" data-id="accepted" data-searchCol="status_TH" data-toggle="tab">Accepted <span class="badge badge-primary"><?php echo $accepted_count; ?></span></a></li>
                     <li><a href="javascript:;" class="task_status_val" data-id="declined" data-searchCol="status_TH" data-toggle="tab">Declined <span class="badge badge-primary"><?php echo $decline_count;?></span></a></li>
                  </ul>
                  <!-- <div class="send-to">
                     <a href="javascript:;" data-toggle="modal" data-target="#add-new-category"><i class="fa fa-plus fa-6" aria-hidden="true"></i> Add new category</a>
                     </div> -->
               </div>
            </li>
            <li><a data-toggle="tab" href="javascript:;" class="task_status_val"  data-id="discussion" data-searchCol="status_TH"><span class="sent-left"><span>In Discussion</span><strong>List of proposals in discussion</strong></span><span class="sent-proposalcount badge badge-primary"><?php echo $indiscussion_count; ?></span></a>
            </li>
            <li><a data-toggle="tab" href="javascript:;" class="task_status_val" data-id="draft" data-searchCol="status_TH"><span class="sent-left"><span>Draft</span><strong>List of draft proposals</strong></span><span class="sent-proposalcount badge badge-primary"><?php echo $draft_count; ?></span></a></li>
            <li><a data-toggle="tab" href="javascript:;" class="task_status_val" data-id="expired" data-searchCol="status_TH"><span class="sent-left"><span>Expired</span><strong>List of expired proposals</strong></span><span class="sent-proposalcount badge badge-primary"><?php echo $expired_count; ?></span></a></li>
            <li><a data-toggle="tab" class="archive task_status_val" href="javascript:;" data-id="archive" data-searchCol="status_TH"><span class="sent-left"><span>Archive</span><strong>List of closed proposals </strong></span> <span class="sent-proposalcount badge badge-primary"><?php echo $archive_count; ?></span></a></li>
         </ul>
      </div>
   </div>
   <div class="right-sidecontent">
      <div class="right-box-05">
         <div class="tab-content pro-temp123 rightsideclstab graybgcls <?php if($_SESSION['permission']['Proposal_Dashboad']['view']!=1){ ?> permission_deined <?php 
                            } ?>">
            <div id="start_tab" class="button_visibility tab-pane fade in <?php 
               if(isset($this->session->userdata['tab_name'])){                
                 }else{
                echo "active show";
                } ?>">
               <div class="annual-head floating_set">
                  <div class="annual-lefts pull-left">
                     <h2>Proposals</h2>
                     <!-- <a type="button" id="delete_proposal_records" class="btn btn-primary">Delete</a> -->
                  </div>
                  <!--  <div class="annual-rights pull-right">
                     <a href="javascript:;" data-toggle="modal" data-target="#import-service">Import</a>/                      
                     <a href="javascript:;" onclick="exportexcel()">Export</a>
                     </div> -->
               </div>
               <table id="display_service1" class="display nowrap" style="width:100%">
                  <thead>
                     <tr class="table-header">
                        <th class="proposal_chk_TH Exc-colvis">
                          <label class="custom_checkbox1">
                          <input type="checkbox" id="select_all_proposal">
                          <i></i>  
                          </label>
                        </th>
                        <th class="proposal_senton proposal_TH hasFilter">proposals
                        <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                       <!--  <select multiple="true" class="filter_check" id="" style="display: none;"> </select> -->
                        </th>
                       <!--  <th style="display: none;">sent proposals
                        <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th> -->
                        <th class="total_cost_TH hasFilter">total cost
                        <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <!-- <select multiple="true" class="filter_check" id="" style="display: none;"> </select> -->
                        </th>
                       <!--  <th style="display: none;">sent proposals
                        <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th> -->
                        <th class="proposal_senton1 created_by_TH hasFilter">created by
                        <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                     <!--    <select multiple="true" class="filter_check" id="" style="display: none;"> </select> -->
                        </th>
                       <!--  <th style="display: none;">sent proposals
                        <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th> -->
                        <th class="send_on_TH hasFilter">sent on
                        <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                       <!--  <select multiple="true" class="filter_check" id="" style="display: none;"> </select> -->
                        </th>
                      <!--   <th style="display: none;">sent proposals
                        <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th> -->
                        <th class="status_TH hasFilter">status
                        <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                       <!--  <select multiple="true" class="filter_check" id="" style="display: none;"> </select> -->
                        </th>
                       <!--   <th style="display: none;">status
                        <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th> -->

                     </tr>
                  </thead>


       <!--            <tfoot>
                    <tr class="table-header">
                    <th></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th></th>
                    </tr>
                  </tfoot> -->
                  <tbody>
                     <?php 

                    // if(count($proposal)>0) {
                        foreach ($proposal as $key => $value) { ?>
                     <tr id="<?php echo $value['id'];?>">
                        <td>
                          <label class="custom_checkbox1">
                              <input type="checkbox" class="proposal_checkbox start_tab_checkbox" data-proposal-id="<?php echo $value['id'];?>">
                              <i></i>
                              </label>
                         
                        </td>
                        <td data-search="<?php echo $value['proposal_name']; if($value['company_id']==1){ echo $value['receiver_mail_id']; } else{ echo $value['company_name'];
                                      }?>">
                           <div class="first-info">
                              <span class="info-num"><?php echo $value['proposal_no'] ?></span>
                              <p>  <?php $randomstring=generateRandomString('100'); ?>
                                      <a href="<?php echo base_url(); ?>proposal_page/copy_proposal/<?php echo $randomstring; ?>---<?php echo $value['id']; ?>" class="edit-option"><?php echo $value['proposal_name'];?></a></p>                            
                                      <a href="<?php echo base_url(); ?>proposal_page/copy_proposal/<?php echo $randomstring; ?>---<?php echo $value['id']; ?>" class="edit-option">[edit]</a>
                            <!--   <a href="javascript:;" class="edit-option">[archive]</a> -->
                           </div>

                      <?php if($value['company_id']==1){ ?>
                      <p class="com-name"><?php echo $value['receiver_mail_id'];?> </p>
                      <?php }else{ ?>
                      <p class="com-name"><a href="<?php echo base_url(); ?>client/client_infomation/<?php echo $value['company_id'];?>" target="_blank"><?php echo $value['company_name'];?> </a></p>

                      <?php   } ?>
                        </td>
                        <!--  <td style="display: none;">
                          <?php $proposal_no=$value['proposal_no'];
                         $proposal_name=$value['proposal_name'];
                                 if($value['company_id']==1){ ?>
                                      <?php $comp= $value['receiver_mail_id'];?> 
                                 <?php }else{ ?>
                                    <?php $comp= $value['company_name'];  ?> 
                               <?php   }
                               echo $proposal_no.'-'.$proposal_name.'-'.$comp; ?>
                        </td> -->
                        <td data-search="<?php echo $value['grand_total']; ?>">
                        <span class="amount">
                                      <?php if($value['currency']!=1 && $value['currency']!=''){ 
                                         echo $currency_symbols[$value['currency']];  ?>
                                      <?php  }else{ ?>
                                      <i class="fa fa-gbp" aria-hidden="true"></i> 
                                      <?php } ?><?php 
                                        echo $value['grand_total'];?></span></td>

                <!--         <td style="display: none;"><?php 
                                        echo $value['grand_total'];?></td> -->

                        <td data-search="<?php if($value['sender_company_name']==''){ 
                                         echo 'accountants & tax consultants';
                                        }else{ 
                                          echo $value['sender_company_name']; 
                                         } ?>">
                           <div class="acc-tax">
                             <?php if($value['sender_company_name']==''){ ?>

                                        <p>accountants & tax consultants</p>
                                      <?php }else{ ?>
                                   
                                        <p><?php echo $value['sender_company_name']; ?></p>
                                        <?php } ?>
                           </div>
                        </td>

                     <!--     <td style="display: none;">                          
                                    <?php if($value['sender_company_name']==''){ ?>
                                    <p>accountants & tax consultants</p>
                                    <?php }else{ ?>                                   
                                    <p><?php echo $value['sender_company_name']; ?></p>
                                    <?php } ?>                        
                        </td> -->

                         <?php //echo $value['created_at'];
                              $timestamp = strtotime($value['created_at']);
                              $newDate = date('d F Y H:i', $timestamp); 
                            ?>
                        <td data-search="<?php echo $newDate;?>" data-sort="<?php echo $newDate;?>">
                           <span class="sent-month"> <?php echo $newDate;?> </span>
                        </td>

                       <!--   <td style="display: none;">
                          <?php //echo $value['created_at'];
                              $timestamp = strtotime($value['created_at']);
                              $newDate = date('d F Y H:i', $timestamp); 
                              echo $newDate; //outputs 02-March-2011
                              ?>
                        </td> -->

                        <td data-search="<?php 
                                            $status=$value['status'];
                                            if($status=='sent')
                                            {
                                              $res='sent';
                                            }
                                            if($status=='opened')
                                            {
                                              $res='viewed';
                                            }
                                            else
                                            {
                                              $res=$status;
                                            }
                                            echo ucfirst($res);
                                            ?>">
                          <div class="sentclsbutton">
                           <i class="fa fa-check" aria-hidden="true"></i>
                           <span class="accept"><?php 
                              $status=$value['status'];
                              if($status=='send')
                              {
                                $res='sent';
                              }
                              if($status=='opened')
                              {
                                $res='viewed';
                              }
                              else
                              {
                                $res=$status;
                              }
                              echo ucfirst($res);
                              ?></span>
                            </div>
                        </td>
                    <!--      <td style="display: none;">
                         <?php 
                              $status=$value['status'];
                              if($status=='send')
                              {
                                $res='sent';
                              }
                              if($status=='opened')
                              {
                                $res='viewed';
                              }
                              else
                              {
                                $res=$status;
                              }
                              echo ucfirst($res);
                              ?>
                        </td> -->
                     </tr>
       
                     <?php } 
                      ?>
                  </tbody>
               </table>
                <input type="hidden" class="rows_selected" id="select_proposal_count" >
                <!-- <span class="rows_selected" id="select_proposal_count">0 Selected</span> -->
               <!-- table -->   
            </div>
            <!-- All Tabs -->

              <div id="all-items" class="commonctab tab-pane fade button_visibility">
               <div class="annual-head floating_set">
                  <div class="annual-lefts pull-left">
                     <h2> proposals</h2>
                     <!-- <a type="button" id="delete_send_records" class="btn btn-primary">Delete</a> -->
                  </div>
                  <!--  <div class="annual-rights pull-right">
                     <a href="javascript:;" data-toggle="modal" data-target="#import-service">Import</a>/                      
                     <a href="javascript:;" onclick="exportexcel()">Export</a>
                     </div> -->
               </div>
               <table id="display_service10" class="display nowrap" style="width:100%">
                  <thead>
                     <tr class="table-header">
                        <th> 
                          <label class="custom_checkbox1">
                          <input type="checkbox" id="select_all">
                         <i></i> 
                          </label>
                        </th>
                        <th>proposals
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                         <th style="display: none;">sent proposals
                           <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>total cost
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                         <th style="display: none;">sent proposals
                           <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>created by
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                         <th style="display: none;">sent proposals
                           <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>sent on
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                         <th style="display: none;">sent proposals
                           <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>status
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                          <th style="display: none;">status
                        <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                     </tr>
                  </thead>
                   <tfoot>
                    <tr class="table-header">
                    <th></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th></th>
                    </tr>
                  </tfoot>
                  <tbody>
              <?php //if(count($proposal)>0) { 
                     foreach ($proposal as $key => $value) { ?>
                         <tr id="<?php echo $value['id'];?>">
                         <td>
                           <label class="custom_checkbox1">
                              <input type="checkbox" class="all_checkbox all-items_checkbox" data-proposal-id="<?php echo $value['id'];?>">
                            <i></i>
                              </label>
                          
                          </td>
                          <td>
                           <div class="first-info">
                              <span class="info-num"><?php echo $value['proposal_no'] ?></span>
                              <p>  <?php $randomstring=generateRandomString('100'); ?>
                                      <a href="<?php echo base_url(); ?>proposal_page/copy_proposal/<?php echo $randomstring; ?>---<?php echo $value['id']; ?>" class="edit-option"><?php echo $value['proposal_name'];?></a></p>
                           
                                       <a href="<?php echo base_url(); ?>proposal_page/copy_proposal/<?php echo $randomstring; ?>---<?php echo $value['id']; ?>" class="edit-option">[edit]</a>
                             <!--  <a href="javascript:;" class="edit-option">[archive]</a> -->
                           </div>
                           <?php if($value['company_id']==1){ ?>
                                       <p class="com-name"><?php echo $value['receiver_mail_id'];?> </p>
                                   <?php }else{ ?>
                                     <p class="com-name"><a href="<?php echo base_url(); ?>client/client_infomation/<?php echo $value['company_id'];?>" target="_blank"><?php echo $value['company_name'];?> </a></p>

                                   <?php   } ?>
                        </td>

                         <td style="display: none;">
                          <?php $proposal_no=$value['proposal_no'];
                         $proposal_name=$value['proposal_name'];
                                 if($value['company_id']==1){ ?>
                                      <?php $comp= $value['receiver_mail_id'];?> 
                                 <?php }else{ ?>
                                    <?php $comp= $value['company_name'];  ?> 
                               <?php   }
                               echo $proposal_no.'-'.$proposal_name.'-'.$comp; ?>
                        </td>
                        <td><span class="amount">
                                      <?php if($value['currency']!=1 && $value['currency']!=''){ 
                                         echo $currency_symbols[$value['currency']];  ?>
                                      <?php  }else{ ?>
                                      <i class="fa fa-gbp" aria-hidden="true"></i> 
                                      <?php } ?><?php                                    

                                     
                                        echo $value['grand_total'];?></span></td>
                       
                          <td style="display: none;"><?php 
                                        echo $value['grand_total'];?></td>
                                        <td>
                           <div class="acc-tax">
                            <?php if($value['sender_company_name']==''){ ?>

                                        <p>accountants & tax consultants</p>
                                      <?php }else{ ?>
                                   
                                        <p><?php echo $value['sender_company_name']; ?></p>
                                        <?php } ?>
                           </div>
                        </td>
                         <td style="display: none;">                          
                                    <?php if($value['sender_company_name']==''){ ?>
                                    <p>accountants & tax consultants</p>
                                    <?php }else{ ?>                                   
                                    <p><?php echo $value['sender_company_name']; ?></p>
                                    <?php } ?>                        
                        </td>
                            <?php //echo $value['created_at'];
                              $timestamp = strtotime($value['created_at']);
                              $newDate = date('d F Y H:i', $timestamp); 
                            ?>
                        <td data-search="<?php echo $newDate;?>" data-sort="<?php echo $newDate;?>">
                           <span class="sent-month"> <?php echo $newDate;?> </span>
                        </td>
                         <td style="display: none;">
                          <?php //echo $value['created_at'];
                              $timestamp = strtotime($value['created_at']);
                              $newDate = date('d F Y H:i', $timestamp); 
                              echo $newDate; //outputs 02-March-2011
                              ?>
                        </td>
                        <td>
                          <div class="sentclsbutton">
                           <i class="fa fa-check" aria-hidden="true"></i>
                           <span class="accept"><?php 
                              $status=$value['status'];
                              if($status=='send')
                              {
                                $res='sent';
                              }
                              if($status=='opened')
                              {
                                $res='viewed';
                              }
                              else
                              {
                                $res=$status;
                              }
                              echo ucfirst($res);
                              ?></span>
                            </div>
                        </td>
                         <td style="display: none;">
                         <?php 
                              $status=$value['status'];
                              if($status=='send')
                              {
                                $res='sent';
                              }
                              if($status=='opened')
                              {
                                $res='viewed';
                              }
                              else
                              {
                                $res=$status;
                              }
                              echo ucfirst($res);
                              ?>
                        </td>
                     </tr>
                  <div class="modal-alertsuccess alert alert-success" id="delete_send_id<?php echo $value['id'];?>" style="display:none;">
                  <div class="newupdate_alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <div class="pop-realted1">
                  <div class="position-alert1">
                  Are you sure want to delete <b><a href="#" class="delete_send"> Yes </a></b> OR <b><a href="#" id="close">No</a></b></div>
                  </div></div>  </div>            
                     <?php } ?>
             <?php  //}else{ ?>
                   <!--   <tr>
                        <td>No Record Found</td>
                     </tr> -->
                     <?php //} ?>
                  </tbody>
               </table>
               <input type="hidden" class="rows_selected" id="select_all_count" >
               <!-- table -->   
            </div>





            <!-- All tabs -->
            <div id="sent-items" class="commonctab tab-pane fade">
               <div class="annual-head floating_set">
                  <div class="annual-lefts pull-left">
                     <h2>Sent proposals</h2>
                     <!-- <a type="button" id="delete_send_records" class="btn btn-primary">Delete</a> -->
                  </div>
                  <!--  <div class="annual-rights pull-right">
                     <a href="javascript:;" data-toggle="modal" data-target="#import-service">Import</a>/                      
                     <a href="javascript:;" onclick="exportexcel()">Export</a>
                     </div> -->
               </div>
               <table id="display_service2" class="display nowrap" style="width:100%">
                  <thead>
                     <tr class="table-header">
                        <th> 
                          <label class="custom_checkbox1">
                          <input type="checkbox" id="select_all_send">
                       <i></i>
                          </label>
                         </th>
                         <th>sent proposals
                           <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                         <th style="display: none;">sent proposals
                           <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>total cost
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                         <th style="display: none;">sent proposals
                           <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>created by
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                         <th style="display: none;">sent proposals
                           <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>sent on
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                         <th style="display: none;">sent proposals
                           <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>status
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                          <th style="display: none;">status
                        <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                     </tr>
                  </thead>

                   <tfoot>
                    <tr class="table-header">
                    <th></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th></th>
                    </tr>
                  </tfoot>
                  <tbody>
              <?php //if(count($send)>0) { 
                     foreach ($send as $key => $value) { ?>
                         <tr id="<?php echo $value['id'];?>">
                         <td>
                         <label class="custom_checkbox1">
                              <input type="checkbox" class="send_checkbox sent-items_checkbox" data-proposal-id="<?php echo $value['id'];?>">
                              <i></i>
                              </label>
                            
                          </td>
                          <td>
                           <div class="first-info">
                              <span class="info-num"><?php echo $value['proposal_no'] ?></span>
                              <p>  <?php $randomstring=generateRandomString('100'); ?>
                                      <a href="<?php echo base_url(); ?>proposal_page/copy_proposal/<?php echo $randomstring; ?>---<?php echo $value['id']; ?>" class="edit-option"><?php echo $value['proposal_name'];?></a></p>
                           
                                       <a href="<?php echo base_url(); ?>proposal_page/copy_proposal/<?php echo $randomstring; ?>---<?php echo $value['id']; ?>" class="edit-option">[edit]</a>
                             <!--  <a href="javascript:;" class="edit-option">[archive]</a> -->
                           </div>
                           <?php if($value['company_id']==1){ ?>
                                       <p class="com-name"><?php echo $value['receiver_mail_id'];?> </p>
                                   <?php }else{ ?>
                                     <p class="com-name"><a href="<?php echo base_url(); ?>client/client_infomation/<?php echo $value['company_id'];?>" target="_blank"><?php echo $value['company_name'];?> </a></p>

                                   <?php   } ?>
                        </td>

                         <td style="display: none;">
                          <?php $proposal_no=$value['proposal_no'];
                         $proposal_name=$value['proposal_name'];
                                 if($value['company_id']==1){ ?>
                                      <?php $comp= $value['receiver_mail_id'];?> 
                                 <?php }else{ ?>
                                    <?php $comp= $value['company_name'];  ?> 
                               <?php   }
                               echo $proposal_no.'-'.$proposal_name.'-'.$comp; ?>
                        </td>


                        <td><span class="amount">
                                      <?php if($value['currency']!=1 && $value['currency']!=''){ 
                                         echo $currency_symbols[$value['currency']];  ?>
                                      <?php  }else{ ?>
                                      <i class="fa fa-gbp" aria-hidden="true"></i> 
                                      <?php } ?><?php 
                                     //   $ex_price=explode(',',$value['price']);
                                     //   $integerIDs = array_sum(array_map('intval', explode(',', $value['price'])));

                                     
                                        echo $value['grand_total'];?></span></td>

                                        <td style="display: none;"><?php 
                                        echo $value['grand_total'];?></td>
                        <td>
                           <div class="acc-tax">
                            <?php if($value['sender_company_name']==''){ ?>

                                        <p>accountants & tax consultants</p>
                                      <?php }else{ ?>
                                   
                                        <p><?php echo $value['sender_company_name']; ?></p>
                                        <?php } ?>
                           </div>
                        </td>

                         <td style="display: none;">                          
                                    <?php if($value['sender_company_name']==''){ ?>
                                    <p>accountants & tax consultants</p>
                                    <?php }else{ ?>                                   
                                    <p><?php echo $value['sender_company_name']; ?></p>
                                    <?php } ?>                        
                        </td>
                        <td>
                           <span class="sent-month"><?php //echo $value['created_at'];
                              $timestamp = strtotime($value['created_at']);
                              $newDate = date('d F Y H:i', $timestamp); 
                              echo $newDate; //outputs 02-March-2011
                              ?></span>
                        </td>

                         <td style="display: none;">
                          <?php //echo $value['created_at'];
                              $timestamp = strtotime($value['created_at']);
                              $newDate = date('d F Y H:i', $timestamp); 
                              echo $newDate; //outputs 02-March-2011
                              ?>
                        </td>
                        <td>
                          <div class="sentclsbutton">
                           <i class="fa fa-check" aria-hidden="true"></i>
                           <span class="accept"><?php 
                              $status=$value['status'];
                              if($status=='send')
                              {
                                $res='sent';
                              }
                              if($status=='opened')
                              {
                                $res='viewed';
                              }
                              else
                              {
                                $res=$status;
                              }
                              echo ucfirst($res);
                              ?></span>
                            </div>
                        </td>
                         <td style="display: none;">
                         <?php 
                              $status=$value['status'];
                              if($status=='send')
                              {
                                $res='sent';
                              }
                              if($status=='opened')
                              {
                                $res='viewed';
                              }
                              else
                              {
                                $res=$status;
                              }
                              echo ucfirst($res);
                              ?>
                        </td>
                     </tr>
                  <div class="modal-alertsuccess alert alert-success" id="delete_send_id<?php echo $value['id'];?>" style="display:none;">
                  <div class="newupdate_alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <div class="pop-realted1">
                  <div class="position-alert1">
                  Are you sure want to delete <b><a href="#" class="delete_send"> Yes </a></b> OR <b><a href="#" id="close">No</a></b></div>
                  </div></div>      </div>        
                     <?php } ?>
           
                  </tbody>
               </table>
               <input type="hidden" class="rows_selected" id="select_send_count" >
               <!-- table -->   
            </div>
            <div id="viewed-items" class="commonctab tab-pane fade">
               <div class="annual-head floating_set">
                  <div class="annual-lefts pull-left">
                     <h2>View proposals</h2>
                     <!-- <a type="button" id="delete_view_records" class="btn btn-primary">Delete</a> -->
                  </div>
               </div>
               <table id="display_service3" class="display nowrap" style="width:100%">
                  <thead>
                     <tr class="table-header">
                        <th> 
                          <label class="custom_checkbox1">
                          <input type="checkbox" id="select_all_view">
                          <i></i>
                          </label>
                         </th>
                        <th>View proposals
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                         <th style="display: none;">sent proposals
                           <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>total cost
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                         <th style="display: none;">sent proposals
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>created by
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                         <th style="display: none;">sent proposals
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>sent on
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                         <th style="display: none;">sent proposals
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>status
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                          <th style="display: none;">status
                        <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                     </tr>
                  </thead>

                   <tfoot>
                    <tr class="table-header">
                    <th></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th></th>
                    </tr>
                  </tfoot>
                  <tbody>
               <?php //if(count($opened)>0) {
                        foreach ($opened as $key => $value) { ?>
                     <tr id="<?php echo $value['id'];?>">
                        <td>
                        <label class="custom_checkbox1">
                              <input type="checkbox" class="view_checkbox viewed-items_checkbox" data-proposal-id="<?php echo $value['id'];?>">
                             <i></i>
                              </label>
                            
                        </td>
                        <td>
                           <div class="first-info">
                              <span class="info-num"><?php echo $value['proposal_no'] ?></span>
                              <p> <?php $randomstring=generateRandomString('100'); ?>
                                      <a href="<?php echo base_url(); ?>proposal_page/copy_proposal/<?php echo $randomstring; ?>---<?php echo $value['id']; ?>" class="edit-option"><?php echo $value['proposal_name'];?></a></p>
                               <a href="<?php echo base_url(); ?>proposal_page/copy_proposal/<?php echo $randomstring; ?>---<?php echo $value['id']; ?>" class="edit-option">[edit]</a>
<!--                               <a href="javascript:;" class="edit-option">[archive]</a> -->
                           </div>
                          <?php if($value['company_id']==1){ ?>
                                       <p class="com-name"><?php echo $value['receiver_mail_id'];?> </p>
                                   <?php }else{ ?>
                                     <p class="com-name"><a href="<?php echo base_url(); ?>client/client_infomation/<?php echo $value['company_id'];?>" target="_blank"><?php echo $value['company_name'];?> </a></p>

                                   <?php   } ?>
                        </td>

                        <td style="display: none;">
                          <?php $proposal_no=$value['proposal_no'];
                         $proposal_name=$value['proposal_name'];
                                 if($value['company_id']==1){ ?>
                                      <?php $comp= $value['receiver_mail_id'];?> 
                                 <?php }else{ ?>
                                    <?php $comp= $value['company_name'];  ?> 
                               <?php   }
                               echo $proposal_no.'-'.$proposal_name.'-'.$comp; ?>
                        </td>
                        <td><span class="amount">
                                      <?php if($value['currency']!=1 && $value['currency']!=''){ 
                                         echo $currency_symbols[$value['currency']];  ?>
                                      <?php  }else{ ?>
                                      <i class="fa fa-gbp" aria-hidden="true"></i> 
                                      <?php } ?><?php 
                                     //   $ex_price=explode(',',$value['price']);
                                     //   $integerIDs = array_sum(array_map('intval', explode(',', $value['price'])));

                                     
                                        echo $value['grand_total'];?></span></td>

                                           <td style="display: none;"><?php 
                                        echo $value['grand_total'];?></td>
                        <td>
                           <div class="acc-tax">
                            <?php if($value['sender_company_name']==''){ ?>

                                        <p>accountants & tax consultants</p>
                                      <?php }else{ ?>
                                   
                                        <p><?php echo $value['sender_company_name']; ?></p>
                                        <?php } ?>
                           </div>
                        </td>
                          <td style="display: none;">                          
                                    <?php if($value['sender_company_name']==''){ ?>
                                    <p>accountants & tax consultants</p>
                                    <?php }else{ ?>                                   
                                    <p><?php echo $value['sender_company_name']; ?></p>
                                    <?php } ?>                        
                        </td>
                        <td>
                           <span class="sent-month"><?php //echo $value['created_at'];
                              $timestamp = strtotime($value['created_at']);
                              $newDate = date('d F Y H:i', $timestamp); 
                              echo $newDate; //outputs 02-March-2011
                              ?></span>
                        </td>

                           <td style="display: none;">
                          <?php //echo $value['created_at'];
                              $timestamp = strtotime($value['created_at']);
                              $newDate = date('d F Y H:i', $timestamp); 
                              echo $newDate; //outputs 02-March-2011
                              ?>
                        </td>
                        <td>
                          <div class="sentclsbutton">
                           <i class="fa fa-check" aria-hidden="true"></i>
                           <span class="accept"><?php 
                              $status=$value['status'];
                              if($status=='send')
                              {
                                $res='sent';
                              }
                              if($status=='opened')
                              {
                                $res='viewed';
                              }
                              else
                              {
                                $res=$status;
                              }
                              echo ucfirst($res);
                              ?></span>
                            </div>
                        </td>
                         <td style="display: none;">
                         <?php 
                              $status=$value['status'];
                              if($status=='send')
                              {
                                $res='sent';
                              }
                              if($status=='opened')
                              {
                                $res='viewed';
                              }
                              else
                              {
                                $res=$status;
                              }
                              echo ucfirst($res);
                              ?>
                        </td>
                     </tr>
                   <div class="modal-alertsuccess alert alert-success" id="delete_view_id<?php echo $value['id'];?>" style="display:none;">
                <div class="newupdate_alert">  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times; </a>
                  <div class="pop-realted1">
                  <div class="position-alert1">
                  Are you sure want to delete <b><a href="#" class="delete_view"> Yes </a></b> OR <b><a href="#" id="close">No</a></b></div>
                  </div></div></div>             
                     <?php }
                     // } ?>
             
                  </tbody>
               </table>
               <input type="hidden" class="rows_selected" id="select_view_count" >
               <!-- table -->   
            </div>
            <div id="accepted-items" class="commonctab tab-pane fade">
               <div class="annual-head floating_set">
                  <div class="annual-lefts pull-left">
                     <h2>Accept proposals</h2>
                     <!-- <a type="button" id="delete_accept_records" class="btn btn-primary">Delete</a> -->
                  </div>
               </div>
               <table id="display_service4" class="display nowrap" style="width:100%">
                  <thead>
                     <tr class="table-header">
                        <th> 
                          <label class="custom_checkbox1">
                          <input type="checkbox" id="select_all_accept">
                          <i></i>
                          </label>
                         </th>
                        <th>Accept proposals
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th style="display: none;">sent proposals
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>total cost
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                         <th style="display: none;">sent proposals
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>created by
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                         <th style="display: none;">sent proposals
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>sent on
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                         <th style="display: none;">sent proposals
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>status
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                          <th style="display: none;">status
                        <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                     </tr>
                  </thead>

                   <tfoot>
                    <tr class="table-header">
                    <th></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th></th>
                    </tr>
                  </tfoot>


                  <tbody>
               <?php //if(count($accepted)>0) {
                        foreach ($accepted as $key => $value) { ?>
                     <tr id="<?php echo $value['id'];?>">
                        <td>
                           <label class="custom_checkbox1">
                              <input type="checkbox" class="accept_checkbox accepted-items_checkbox" data-proposal-id="<?php echo $value['id'];?>">
                              <i></i>
                              </label>
                           </div>
                        </td>
                        <td>
                           <div class="first-info">
                              <span class="info-num"><?php echo $value['proposal_no'] ?></span>
                              <p><?php $randomstring=generateRandomString('100'); ?>
                                      <a href="<?php echo base_url(); ?>proposal_page/copy_proposal/<?php echo $randomstring; ?>---<?php echo $value['id']; ?>" class="edit-option"> <?php echo $value['proposal_name'];?></a></p>
                              <a href="<?php echo base_url(); ?>proposal_page/copy_proposal/<?php echo $randomstring; ?>---<?php echo $value['id']; ?>" class="edit-option">[edit]</a>
                              <!-- <a href="javascript:;" class="edit-option">[archive]</a> -->
                           </div>
                          <?php if($value['company_id']==1){ ?>
                                       <p class="com-name"><?php echo $value['receiver_mail_id'];?> </p>
                                   <?php }else{ ?>
                                     <p class="com-name"><a href="<?php echo base_url(); ?>client/client_infomation/<?php echo $value['company_id'];?>" target="_blank"><?php echo $value['company_name'];?> </a></p>

                                   <?php   } ?>
                        </td>

                         <td style="display: none;">
                          <?php $proposal_no=$value['proposal_no'];
                         $proposal_name=$value['proposal_name'];
                                 if($value['company_id']==1){ ?>
                                      <?php $comp= $value['receiver_mail_id'];?> 
                                 <?php }else{ ?>
                                    <?php $comp= $value['company_name'];  ?> 
                               <?php   }
                               echo $proposal_no.'-'.$proposal_name.'-'.$comp; ?>
                        </td>
                        <td><span class="amount">
                                      <?php if($value['currency']!=1 && $value['currency']!=''){ 
                                         echo $currency_symbols[$value['currency']];  ?>
                                      <?php  }else{ ?>
                                      <i class="fa fa-gbp" aria-hidden="true"></i> 
                                      <?php } ?><?php 
                                     //   $ex_price=explode(',',$value['price']);
                                     //   $integerIDs = array_sum(array_map('intval', explode(',', $value['price'])));

                                     
                                        echo $value['grand_total'];?></span></td>
                           <td style="display: none;"><?php 
                                        echo $value['grand_total'];?></td>                                        

                        <td>
                           <div class="acc-tax">
                            <?php if($value['sender_company_name']==''){ ?>

                                        <p>accountants & tax consultants</p>
                                      <?php }else{ ?>
                                   
                                        <p><?php echo $value['sender_company_name']; ?></p>
                                        <?php } ?>
                           </div>
                        </td>

                          <td style="display: none;">                          
                                    <?php if($value['sender_company_name']==''){ ?>
                                    <p>accountants & tax consultants</p>
                                    <?php }else{ ?>                                   
                                    <p><?php echo $value['sender_company_name']; ?></p>
                                    <?php } ?>                        
                        </td>
                        <td>
                           <span class="sent-month"><?php //echo $value['created_at'];
                              $timestamp = strtotime($value['created_at']);
                              $newDate = date('d F Y H:i', $timestamp); 
                              echo $newDate; //outputs 02-March-2011
                              ?></span>
                        </td>
                         <td style="display: none;">
                          <?php //echo $value['created_at'];
                              $timestamp = strtotime($value['created_at']);
                              $newDate = date('d F Y H:i', $timestamp); 
                              echo $newDate; //outputs 02-March-2011
                              ?>
                        </td>
                        <td>
                          <div class="sentclsbutton">
                           <i class="fa fa-check" aria-hidden="true"></i>
                           <span class="accept"><?php 
                              $status=$value['status'];
                              if($status=='send')
                              {
                                $res='sent';
                              }
                              if($status=='opened')
                              {
                                $res='viewed';
                              }
                              else
                              {
                                $res=$status;
                              }
                              echo ucfirst($res);
                              ?></span>
                            </div>
                        </td>
                         <td style="display: none;">
                         <?php 
                              $status=$value['status'];
                              if($status=='send')
                              {
                                $res='sent';
                              }
                              if($status=='opened')
                              {
                                $res='viewed';
                              }
                              else
                              {
                                $res=$status;
                              }
                              echo ucfirst($res);
                              ?>
                        </td>
                     </tr>
        <div class="modal-alertsuccess alert alert-success" id="delete_accept_id<?php echo $value['id'];?>" style="display:none;">
                  <div class="newupdate_alert">
   <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <div class="pop-realted1">
                  <div class="position-alert1">
                  Are you sure want to delete <b><a href="#" class="delete_accept"> Yes </a></b> OR <b><a href="#" id="close">No</a></b></div></div>
                  </div></div>               
                     <?php }  ?>
                  </tbody>
               </table>
               <input type="hidden" class="rows_selected" id="select_accept_count" >
               <!-- table -->   
            </div>
            <div id="declined-items" class="commonctab tab-pane fade">
               <div class="annual-head floating_set">
                  <div class="annual-lefts pull-left">
                     <h2>Decline proposals</h2>
                     <!-- <a type="button" id="delete_decline_records" class="btn btn-primary">Delete</a> -->
                  </div>
               </div>
               <table id="display_service5" class="display nowrap" style="width:100%">
                  <thead>
                     <tr class="table-header">
                        <th> 
                          <label class="custom_checkbox1">
                          <input type="checkbox" id="select_all_decline">
                          <i></i>
                          </label>
                         </th>
                        <th>Decline proposals
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                       <th style="display: none;">sent proposals
                        <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>total cost
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                         <th style="display: none;">sent proposals
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>created by
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                         <th style="display: none;">sent proposals
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>sent on
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                         <th style="display: none;">sent proposals
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>status
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                          <th style="display: none;">status
                        <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                     </tr>
                  </thead>
                     <tfoot>
                    <tr class="table-header">
                    <th></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th></th>
                    </tr>
                  </tfoot>
                  <tbody>
               <?php 
                        foreach ($decline as $key => $value) { ?>
                     <tr id="<?php echo $value['id'];?>">
                        <td>
                           <label class="custom_checkbox1">
                              <input type="checkbox" class="decline_checkbox declined-items_checkbox" data-proposal-id="<?php echo $value['id'];?>">
<i></i>                              </label>
                           
                        </td>
                        <td>
                           <div class="first-info">
                              <span class="info-num"><?php echo $value['proposal_no'] ?></span>
                              <p> <?php $randomstring=generateRandomString('100'); ?>
                                      <a href="<?php echo base_url(); ?>proposal_page/copy_proposal/<?php echo $randomstring; ?>---<?php echo $value['id']; ?>" class="edit-option"><?php echo $value['proposal_name'];?></a></p>
                               <a href="<?php echo base_url(); ?>proposal_page/copy_proposal/<?php echo $randomstring; ?>---<?php echo $value['id']; ?>" class="edit-option">[edit]</a>
<!--                               <a href="javascript:;" class="edit-option">[archive]</a> -->
                           </div>
                          <?php if($value['company_id']==1){ ?>
                                       <p class="com-name"><?php echo $value['receiver_mail_id'];?> </p>
                                   <?php }else{ ?>
                                     <p class="com-name"><a href="<?php echo base_url(); ?>client/client_infomation/<?php echo $value['company_id'];?>" target="_blank"><?php echo $value['company_name'];?> </a></p>

                                   <?php   } ?>
                        </td>

                          <td style="display: none;">
                          <?php $proposal_no=$value['proposal_no'];
                         $proposal_name=$value['proposal_name'];
                                 if($value['company_id']==1){ ?>
                                      <?php $comp= $value['receiver_mail_id'];?> 
                                 <?php }else{ ?>
                                    <?php $comp= $value['company_name'];  ?> 
                               <?php   }
                               echo $proposal_no.'-'.$proposal_name.'-'.$comp; ?>
                        </td>

                        <td><span class="amount">
                                      <?php if($value['currency']!=1 && $value['currency']!=''){ 
                                         echo $currency_symbols[$value['currency']];  ?>
                                      <?php  }else{ ?>
                                      <i class="fa fa-gbp" aria-hidden="true"></i> 
                                      <?php } ?><?php 
                                     //   $ex_price=explode(',',$value['price']);
                                     //   $integerIDs = array_sum(array_map('intval', explode(',', $value['price'])));

                                     
                                        echo $value['grand_total'];?></span></td>

                                          <td style="display: none;"><?php 
                                        echo $value['grand_total'];?></td>

                        <td>
                           <div class="acc-tax">
                             <?php if($value['sender_company_name']==''){ ?>

                                        <p>accountants & tax consultants</p>
                                      <?php }else{ ?>
                                   
                                        <p><?php echo $value['sender_company_name']; ?></p>
                                        <?php } ?>
                           </div>
                        </td>

                          <td style="display: none;">                          
                                    <?php if($value['sender_company_name']==''){ ?>
                                    <p>accountants & tax consultants</p>
                                    <?php }else{ ?>                                   
                                    <p><?php echo $value['sender_company_name']; ?></p>
                                    <?php } ?>                        
                        </td>
                        <td>
                           <span class="sent-month"><?php //echo $value['created_at'];
                              $timestamp = strtotime($value['created_at']);
                              $newDate = date('d F Y H:i', $timestamp); 
                              echo $newDate; //outputs 02-March-2011
                              ?></span>
                        </td>
                        <td style="display: none;">
                          <?php //echo $value['created_at'];
                              $timestamp = strtotime($value['created_at']);
                              $newDate = date('d F Y H:i', $timestamp); 
                              echo $newDate; //outputs 02-March-2011
                              ?>
                        </td>
                        <td>
                          <div class="sentclsbutton">
                           <i class="fa fa-check" aria-hidden="true"></i>
                           <span class="accept"><?php 
                              $status=$value['status'];
                              if($status=='send')
                              {
                                $res='sent';
                              }
                              if($status=='opened')
                              {
                                $res='viewed';
                              }
                              else
                              {
                                $res=$status;
                              }
                              echo ucfirst($res);
                              ?></span>
                            </div>
                        </td>
                         <td style="display: none;">
                         <?php 
                              $status=$value['status'];
                              if($status=='send')
                              {
                                $res='sent';
                              }
                              if($status=='opened')
                              {
                                $res='viewed';
                              }
                              else
                              {
                                $res=$status;
                              }
                              echo ucfirst($res);
                              ?>
                        </td>
                     </tr>
                 <div class="modal-alertsuccess alert alert-success" id="delete_decline_id<?php echo $value['id'];?>" style="display:none;">
                  <div class="newupdate_alert">
   <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <div class="pop-realted1">
                  <div class="position-alert1">
                  Are you sure want to delete <b><a href="#" class="delete_decline"> Yes </a></b> OR <b><a href="#" id="close">No</a></b></div></div>
                  </div></div>             
                     <?php }   ?>
                  </tbody>
               </table>
               <input type="hidden" class="rows_selected" id="select_decline_count" >
               <!-- table -->   
            </div>
            <!-- 1tab -->
            <!--  <div id="product_tab" class="tab-pane fade">
               </div> --> <!-- 2tab -->
            <div id="discussion_tab" class="commonctab tab-pane fade">
               <div class="annual-head floating_set">
                  <div class="annual-lefts pull-left">
                     <h2>In Discussion proposals</h2>
                     <!-- <a type="button" id="delete_discussion_records" class="btn btn-primary">Delete</a> -->
                  </div>
               </div>
               <table id="display_service6" class="display nowrap" style="width:100%">
                  <thead>
                     <tr class="table-header">
                        <th> 
                          <label class="custom_checkbox1">
                          <input type="checkbox" id="select_all_discussion">
                          <i></i>
                          </label>
                        </th>
                        <th>Indiscussion proposals
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th style="display: none;">sent proposals
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                        <th>total cost
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                         <th style="display: none;">sent proposals
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>created by
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                         <th style="display: none;">sent proposals
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>sent on
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                         <th style="display: none;">sent proposals
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>status
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                          <th style="display: none;">status
                        <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                     </tr>
                  </thead>
                    <tfoot>
                    <tr class="table-header">
                    <th></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th></th>
                    </tr>
                  </tfoot>
                  <tbody>
             <?php //if(count($indiscussion)>0){
                        foreach ($indiscussion as $key => $value) { ?>
                     <tr id="<?php echo $value['id'];?>">
                        <td>
                         <label class="custom_checkbox1">
                              <input type="checkbox" class="discussion_checkbox discussion_tab_checkbox" data-proposal-id="<?php echo $value['id'];?>">
                              <i></i>
                              </label>
                        </td>
                        <td>
                           <div class="first-info">
                              <span class="info-num"><?php echo $value['proposal_no'] ?></span>
                              <p> <?php $randomstring=generateRandomString('100'); ?>
                                      <a href="<?php echo base_url(); ?>proposal_page/copy_proposal/<?php echo $randomstring; ?>---<?php echo $value['id']; ?>" class="edit-option"><?php echo $value['proposal_name'];?></a></p>
                             <a href="<?php echo base_url(); ?>proposal_page/copy_proposal/<?php echo $randomstring; ?>---<?php echo $value['id']; ?>" class="edit-option">[edit]</a>  <!-- <a href="javascript:;" class="edit-option">[archive]</a> -->
                           </div>
                          <?php if($value['company_id']==1){ ?>
                                       <p class="com-name"><?php echo $value['receiver_mail_id'];?> </p>
                                   <?php }else{ ?>
                                     <p class="com-name"><a href="<?php echo base_url(); ?>client/client_infomation/<?php echo $value['company_id'];?>" target="_blank"><?php echo $value['company_name'];?> </a></p>

                                   <?php   } ?>
                        </td>

                          <td style="display: none;">
                          <?php $proposal_no=$value['proposal_no'];
                         $proposal_name=$value['proposal_name'];
                                 if($value['company_id']==1){ ?>
                                      <?php $comp= $value['receiver_mail_id'];?> 
                                 <?php }else{ ?>
                                    <?php $comp= $value['company_name'];  ?> 
                               <?php   }
                               echo $proposal_no.'-'.$proposal_name.'-'.$comp; ?>
                        </td>
                        <td><span class="amount">
                                      <?php if($value['currency']!=1 && $value['currency']!=''){ 
                                         echo $currency_symbols[$value['currency']];  ?>
                                      <?php  }else{ ?>
                                      <i class="fa fa-gbp" aria-hidden="true"></i> 
                                      <?php } ?><?php 
                                     //   $ex_price=explode(',',$value['price']);
                                     //   $integerIDs = array_sum(array_map('intval', explode(',', $value['price'])));

                                     
                                        echo $value['grand_total'];?></span></td>

                                         <td style="display: none;"><?php 
                                        echo $value['grand_total'];?></td>
                        <td>
                           <div class="acc-tax">
                             <?php if($value['sender_company_name']==''){ ?>

                                        <p>accountants & tax consultants</p>
                                      <?php }else{ ?>
                                   
                                        <p><?php echo $value['sender_company_name']; ?></p>
                                        <?php } ?>
                           </div>
                        </td>

                          <td style="display: none;">                          
                                    <?php if($value['sender_company_name']==''){ ?>
                                    <p>accountants & tax consultants</p>
                                    <?php }else{ ?>                                   
                                    <p><?php echo $value['sender_company_name']; ?></p>
                                    <?php } ?>                        
                        </td>
                        <td>
                           <span class="sent-month"><?php //echo $value['created_at'];
                              $timestamp = strtotime($value['created_at']);
                              $newDate = date('d F Y H:i', $timestamp); 
                              echo $newDate; //outputs 02-March-2011
                              ?></span>
                        </td>

                          <td style="display: none;">
                          <?php //echo $value['created_at'];
                              $timestamp = strtotime($value['created_at']);
                              $newDate = date('d F Y H:i', $timestamp); 
                              echo $newDate; //outputs 02-March-2011
                              ?>
                        </td>
                        <td>
                          <div class="sentclsbutton">
                           <i class="fa fa-check" aria-hidden="true"></i>
                           <span class="accept"><?php 
                              $status=$value['status'];
                              if($status=='send')
                              {
                                $res='sent';
                              }
                              if($status=='opened')
                              {
                                $res='viewed';
                              }
                              else
                              {
                                $res=$status;
                              }
                              echo ucfirst($res);
                              ?></span>
                            </div>
                        </td>
                           <td style="display: none;">
                         <?php 
                              $status=$value['status'];
                              if($status=='send')
                              {
                                $res='sent';
                              }
                              if($status=='opened')
                              {
                                $res='viewed';
                              }
                              else
                              {
                                $res=$status;
                              }
                              echo ucfirst($res);
                              ?>
                        </td>
                     </tr>
                  <div class="modal-alertsuccess alert alert-success" id="delete_discussion_id<?php echo $value['id'];?>" style="display:none;">
                 <div class="newupdate_alert"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <div class="pop-realted1">
                  <div class="position-alert1">
                  Are you sure want to delete <b><a href="#" class="delete_discussion"> Yes </a></b> OR <b><a href="#" id="close">No</a></b></div>
                  </div></div>   </div>        
                     <?php }  ?>
                  </tbody>
               </table>
               <input type="hidden" class="rows_selected" id="select_discussion_count" >
               <!-- table -->   
            </div>
            <!-- tab -->
            <div id="draft_tab" class="commonctab tab-pane fade">
               <div class="annual-head floating_set">
                  <div class="annual-lefts pull-left">
                     <h2>Draft proposals</h2>
                     <!-- <a type="button" id="delete_draft_records" class="btn btn-primary">Delete</a> -->
                  </div>
               </div>
               <table id="display_service7" class="display nowrap" style="width:100%">
                  <thead>
                     <tr class="table-header">
                        <th> 
                          <label class="custom_checkbox1">
                          <input type="checkbox" id="select_all_draft">
                          <i></i>
                          </label>
                        </th>
                        <th>Draft proposals
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                        <th style="display: none;">sent proposals
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                        <th>total cost
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                         <th style="display: none;">sent proposals
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                        <th>created by
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                         <th style="display: none;">sent proposals
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                        <th>sent on
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                         <th style="display: none;">sent proposals
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                        <th>status
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                          <th style="display: none;">status
                        <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                     </tr>
                  </thead>

                    <tfoot>
                    <tr class="table-header">
                    <th></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th></th>
                    </tr>
                  </tfoot>


                  <tbody>
              <?php //if(count($draft)>0){
                        foreach ($draft as $key => $value) { ?>
                     <tr id="<?php echo $value['id'];?>">
                        <td>
                           <label class="custom_checkbox1">
                              <input type="checkbox" class="draft_checkbox draft_tab_checkbox" data-proposal-id="<?php echo $value['id'];?>">
                              <i></i>
                              </label>
                           
                        </td>
                        <td>
                           <div class="first-info">
                              <span class="info-num"><?php echo $value['proposal_no'] ?></span>
                              <p>  <?php $randomstring=generateRandomString('100'); ?>
                                      <a href="<?php echo base_url(); ?>proposal_page/copy_proposal/<?php echo $randomstring; ?>---<?php echo $value['id']; ?>" class="edit-option"><?php echo $value['proposal_name'];?></a></p>
                            <a href="<?php echo base_url(); ?>proposal_page/copy_proposal/<?php echo $randomstring; ?>---<?php echo $value['id']; ?>" class="edit-option">[edit]</a>
                            <!--   <a href="javascript:;" class="edit-option">[archive]</a> -->
                           </div>
                         <?php if($value['company_id']==1){ ?>
                                       <p class="com-name"><?php echo $value['receiver_mail_id'];?> </p>
                                   <?php }else{ ?>
                                     <p class="com-name"><a href="<?php echo base_url(); ?>client/client_infomation/<?php echo $value['company_id'];?>" target="_blank"><?php echo $value['company_name'];?> </a></p>

                                   <?php   } ?>
                        </td>

                         <td style="display: none;">
                          <?php $proposal_no=$value['proposal_no'];
                         $proposal_name=$value['proposal_name'];
                                 if($value['company_id']==1){ ?>
                                      <?php $comp= $value['receiver_mail_id'];?> 
                                 <?php }else{ ?>
                                    <?php $comp= $value['company_name'];  ?> 
                               <?php   }
                               echo $proposal_no.'-'.$proposal_name.'-'.$comp; ?>
                        </td>
                        <td><span class="amount">
                                      <?php if($value['currency']!=1 && $value['currency']!=''){ 
                                         echo $currency_symbols[$value['currency']];  ?>
                                      <?php  }else{ ?>
                                      <i class="fa fa-gbp" aria-hidden="true"></i> 
                                      <?php } ?><?php 
                                     //   $ex_price=explode(',',$value['price']);
                                     //   $integerIDs = array_sum(array_map('intval', explode(',', $value['price'])));

                                     
                                        echo $value['grand_total'];?></span></td>

                                         <td style="display: none;"><?php 
                                        echo $value['grand_total'];?></td>

                        <td>
                           <div class="acc-tax">
                             <?php if($value['sender_company_name']==''){ ?>

                                        <p>accountants & tax consultants</p>
                                      <?php }else{ ?>
                                   
                                        <p><?php echo $value['sender_company_name']; ?></p>
                                        <?php } ?>
                           </div>
                        </td>
                        <td style="display: none;">                          
                                    <?php if($value['sender_company_name']==''){ ?>
                                    <p>accountants & tax consultants</p>
                                    <?php }else{ ?>                                   
                                    <p><?php echo $value['sender_company_name']; ?></p>
                                    <?php } ?>                        
                        </td>
                      
                        <td>
                           <span class="sent-month"><?php //echo $value['created_at'];
                              $timestamp = strtotime($value['created_at']);
                              $newDate = date('d F Y H:i', $timestamp); 
                              echo $newDate; //outputs 02-March-2011
                              ?></span>
                        </td>

                        <td style="display: none;">
                          <?php //echo $value['created_at'];
                              $timestamp = strtotime($value['created_at']);
                              $newDate = date('d F Y H:i', $timestamp); 
                              echo $newDate; //outputs 02-March-2011
                              ?>
                        </td>
                        <td>
                          <div class="sentclsbutton">
                           <i class="fa fa-check" aria-hidden="true"></i>
                           <span class="accept"><?php 
                              $status=$value['status'];
                              if($status=='send')
                              {
                                $res='sent';
                              }
                              if($status=='opened')
                              {
                                $res='viewed';
                              }
                              else
                              {
                                $res=$status;
                              }
                              echo ucfirst($res);
                              ?></span>
                            </div>
                        </td>
                           <td style="display: none;">
                         <?php 
                              $status=$value['status'];
                              if($status=='send')
                              {
                                $res='sent';
                              }
                              if($status=='opened')
                              {
                                $res='viewed';
                              }
                              else
                              {
                                $res=$status;
                              }
                              echo ucfirst($res);
                              ?>
                        </td>
                     </tr>
                 <div class="modal-alertsuccess alert alert-success" id="delete_draft_id<?php echo $value['id'];?>" style="display:none;">
                <div class="newupdate_alert">  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <div class="pop-realted1">
                  <div class="position-alert1">
                  Are you sure want to delete <b><a href="#" class="delete_draft"> Yes </a></b> OR <b><a href="#" id="close">No</a></b></div>,/
                  </div></div>           
                     <?php }   ?>
                  </tbody>
               </table>
               <input type="hidden" class="rows_selected" id="select_draft_count" >
               <!-- table -->   
            </div>

            <!-- tab -->
            <div id="expired_tab" class="commonctab tab-pane fade">
               <div class="annual-head floating_set">
                  <div class="annual-lefts pull-left">
                     <h2>Expired proposals</h2>
                     <!-- <a type="button" id="delete_expired_records" class="btn btn-primary">Delete</a> -->
                  </div>
               </div>
               <table id="display_service8" class="display nowrap" style="width:100%">
                  <thead>
                     <tr class="table-header">
                        <th> 
                          <label class="custom_checkbox1">
                          <input type="checkbox" id="select_all_expired">
                          <i></i>
                          </label>
                        </th>
                        <th>Expired proposals
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                     <th style="display: none;">sent proposals
                      <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                        <th>total cost
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                         <th style="display: none;">sent proposals
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                        <th>created by
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                         <th style="display: none;">sent proposals
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                        <th>sent on <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                         <th style="display: none;">sent proposals
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>status
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                          <th style="display: none;">status
                        <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                     </tr>
                  </thead>

                   <tfoot>
                    <tr class="table-header">
                    <th></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th></th>
                    </tr>
                  </tfoot>
                  <tbody>
             <?php //if(count($expired)>0){
                        foreach ($expired as $key => $value) { ?>
                     <tr id="<?php echo $value['id'];?>">
                        <td>
                           <label class="custom_checkbox1">
                              <input type="checkbox" class="expired_checkbox expired_tab_checkbox" data-proposal-id="<?php echo $value['id'];?>">
                              <i></i>
                              </label>
                            
                        </td>
                        <td>
                           <div class="first-info">
                              <span class="info-num"><?php echo $value['proposal_no'] ?></span>
                              <p>  <?php $randomstring=generateRandomString('100'); ?>
                                      <a href="<?php echo base_url(); ?>proposal_page/copy_proposal/<?php echo $randomstring; ?>---<?php echo $value['id']; ?>" class="edit-option"><?php echo $value['proposal_name'];?></a></p>
                            <a href="<?php echo base_url(); ?>proposal_page/copy_proposal/<?php echo $randomstring; ?>---<?php echo $value['id']; ?>" class="edit-option">[edit]</a>
<!--                               <a href="javascript:;" class="edit-option">[archive]</a> -->
                           </div>
                          <?php if($value['company_id']==1){ ?>
                                       <p class="com-name"><?php echo $value['receiver_mail_id'];?> </p>
                                   <?php }else{ ?>
                                     <p class="com-name"><a href="<?php echo base_url(); ?>client/client_infomation/<?php echo $value['company_id'];?>" target="_blank"><?php echo $value['company_name'];?> </a></p>

                                   <?php   } ?>
                        </td>

                          <td style="display: none;">
                          <?php $proposal_no=$value['proposal_no'];
                         $proposal_name=$value['proposal_name'];
                                 if($value['company_id']==1){ ?>
                                      <?php $comp= $value['receiver_mail_id'];?> 
                                 <?php }else{ ?>
                                    <?php $comp= $value['company_name'];  ?> 
                               <?php   }
                               echo $proposal_no.'-'.$proposal_name.'-'.$comp; ?>
                        </td>
                        <td><span class="amount">
                                      <?php if($value['currency']!=1 && $value['currency']!=''){ 
                                         echo $currency_symbols[$value['currency']];  ?>
                                      <?php  }else{ ?>
                                      <i class="fa fa-gbp" aria-hidden="true"></i> 
                                      <?php } ?><?php 
                                     //   $ex_price=explode(',',$value['price']);
                                     //   $integerIDs = array_sum(array_map('intval', explode(',', $value['price'])));

                                     
                                        echo $value['grand_total'];?></span></td>

                                        <td style="display: none;"><?php 
                                        echo $value['grand_total'];?></td>
                        <td>
                           <div class="acc-tax">
                             <?php if($value['sender_company_name']==''){ ?>

                                        <p>accountants & tax consultants</p>
                                      <?php }else{ ?>
                                   
                                        <p><?php echo $value['sender_company_name']; ?></p>
                                        <?php } ?>
                           </div>
                        </td>

                         <td style="display: none;">                          
                                    <?php if($value['sender_company_name']==''){ ?>
                                    <p>accountants & tax consultants</p>
                                    <?php }else{ ?>                                   
                                    <p><?php echo $value['sender_company_name']; ?></p>
                                    <?php } ?>                        
                        </td>
                        <td>
                           <span class="sent-month"><?php //echo $value['created_at'];
                              $timestamp = strtotime($value['created_at']);
                              $newDate = date('d F Y H:i', $timestamp); 
                              echo $newDate; //outputs 02-March-2011
                              ?></span>
                        </td>


                         <td style="display: none;">
                          <?php //echo $value['created_at'];
                              $timestamp = strtotime($value['created_at']);
                              $newDate = date('d F Y H:i', $timestamp); 
                              echo $newDate; //outputs 02-March-2011
                              ?>
                        </td>
                        <td>
                          <div class="sentclsbutton">
                           <i class="fa fa-check" aria-hidden="true"></i>
                           <span class="accept"><?php 
                              $status=$value['status'];
                              if($status=='send')
                              {
                                $res='sent';
                              }
                              if($status=='opened')
                              {
                                $res='viewed';
                              }
                              else
                              {
                                $res=$status;
                              }
                              echo ucfirst($res);
                              ?></span>
                            </div>
                        </td>
                           <td style="display: none;">
                         <?php 
                              $status=$value['status'];
                              if($status=='send')
                              {
                                $res='sent';
                              }
                              if($status=='opened')
                              {
                                $res='viewed';
                              }
                              else
                              {
                                $res=$status;
                              }
                              echo ucfirst($res);
                              ?>
                        </td>
                     </tr>
                  <div class="modal-alertsuccess alert alert-success" id="delete_expired_id<?php echo $value['id'];?>" style="display:none;"><div class="newupdate_alert">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <div class="pop-realted1">
                  <div class="position-alert1">
                  Are you sure want to delete <b><a href="#" class="delete_expired"> Yes </a></b> OR <b><a href="#" id="close">No</a></b></div></div>
                  </div></div>             
                     <?php }   ?>
                  </tbody>
               </table>
               <input type="hidden" class="rows_selected" id="select_expired_count" >
               <!-- table -->   
            </div>
            <!-- tab -->
            <div id="archive_tab" class="commonctab tab-pane fade">
               <div class="annual-head floating_set">
                  <div class="annual-lefts pull-left">
                     <h2>Archive proposals</h2>
                     <!-- <a type="button" id="delete_archive_records" class="btn btn-primary">Delete</a> -->
                  </div>
               </div>
               <table id="display_service9" class="display nowrap" style="width:100%">
                  <thead>
                     <tr class="table-header">
                        <th> 
                          <label class="custom_checkbox1">
                          <input type="checkbox" id="select_all_archive">
                          <i></i>
                          </label>
                        </th>
                        <th>sent proposals
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th style="display: none;">sent proposals
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>total cost
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                         <th style="display: none;">sent proposals
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>created by
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                         <th style="display: none;">sent proposals
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>sent on
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                         <th style="display: none;">sent proposals
                          <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                        <th>status
                         <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                          <th style="display: none;">status
                        <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
                        <select multiple="true" class="filter_check" id="" style="display: none;"> </select>
                        </th>
                     </tr>
                  </thead>

                   <tfoot>
                    <tr class="table-header">
                    <th></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th style="display: none;"><select multiple="true" class="filter_check" id="" style="display: none;"> </select></th>
                    <th></th>
                    </tr>
                  </tfoot>
                  <tbody>
                     <?php //if(count($archive)>0){
                        foreach ($archive as $key => $value) { ?>
                     
                     <tr id="<?php echo $value['id'];?>">
                        <td>
                           <label class="custom_checkbox1">
                              <input type="checkbox" class="archive_checkbox archive_tab_checkbox" data-proposal-id="<?php echo $value['id'];?>">
                              <i></i>
                              </label>
                            
                        </td>
                        <td>
                           <div class="first-info">
                              <span class="info-num"><?php echo $value['proposal_no'] ?></span>
                              <p>  <?php $randomstring=generateRandomString('100'); ?>
                                      <a href="<?php echo base_url(); ?>proposal_page/copy_proposal/<?php echo $randomstring; ?>---<?php echo $value['id']; ?>" class="edit-option"><?php echo $value['proposal_name'];?></a></p>
                           <a href="<?php echo base_url(); ?>proposal_page/copy_proposal/<?php echo $randomstring; ?>---<?php echo $value['id']; ?>" class="edit-option">[edit]</a>
                           <a href="javascript:;" id="<?php echo $value['id']; ?>" class="edit-option unarchive">[unarchive]</a> 
                           </div>
                          <?php if($value['company_id']==1){ ?>
                                       <p class="com-name"><?php echo $value['receiver_mail_id'];?> </p>
                                   <?php }else{ ?>
                                     <p class="com-name"><a href="<?php echo base_url(); ?>client/client_infomation/<?php echo $value['company_id'];?>" target="_blank"><?php echo $value['company_name'];?> </a></p>

                                   <?php   } ?>
                        </td>

                         <td style="display: none;">
                          <?php $proposal_no=$value['proposal_no'];
                         $proposal_name=$value['proposal_name'];
                                 if($value['company_id']==1){ ?>
                                      <?php $comp= $value['receiver_mail_id'];?> 
                                 <?php }else{ ?>
                                    <?php $comp= $value['company_name'];  ?> 
                               <?php   }
                               echo $proposal_no.'-'.$proposal_name.'-'.$comp; ?>
                        </td>
                        <td><span class="amount">
                                      <?php if($value['currency']!=1 && $value['currency']!=''){ 
                                         echo $currency_symbols[$value['currency']];  ?>
                                      <?php  }else{ ?>
                                      <i class="fa fa-gbp" aria-hidden="true"></i> 
                                      <?php } ?><?php 
                                     //   $ex_price=explode(',',$value['price']);
                                     //   $integerIDs = array_sum(array_map('intval', explode(',', $value['price'])));

                                     
                                        echo $value['grand_total'];?></span></td>

                                        <td style="display: none;"><?php 
                                        echo $value['grand_total'];?></td>
                        <td>
                           <div class="acc-tax">
                              <?php if($value['sender_company_name']==''){ ?>

                                        <p>accountants & tax consultants</p>
                                      <?php }else{ ?>
                                   
                                        <p><?php echo $value['sender_company_name']; ?></p>
                                        <?php } ?>
                           </div>
                        </td>

                         <td style="display: none;">                          
                                    <?php if($value['sender_company_name']==''){ ?>
                                    <p>accountants & tax consultants</p>
                                    <?php }else{ ?>                                   
                                    <p><?php echo $value['sender_company_name']; ?></p>
                                    <?php } ?>                        
                        </td>

                        <td>
                           <span class="sent-month"><?php //echo $value['created_at'];
                              $timestamp = strtotime($value['created_at']);
                              $newDate = date('d F Y H:i', $timestamp); 
                              echo $newDate; //outputs 02-March-2011
                              ?></span>
                        </td>
                         <td style="display: none;">
                          <?php 
                              $timestamp = strtotime($value['created_at']);
                              $newDate = date('d F Y H:i', $timestamp); 
                              echo $newDate; //outputs 02-March-2011
                              ?>
                        </td>
                        <td>
                          <div class="sentclsbutton">
                           <i class="fa fa-check" aria-hidden="true"></i>
                           <span class="accept"><?php 
                              $status=$value['status'];
                              if($status=='send')
                              {
                                $res='sent';
                              }
                              if($status=='opened')
                              {
                                $res='viewed';
                              }
                              else
                              {
                                $res=$status;
                              }
                              echo ucfirst($res);
                              ?></span>
                            </div>
                        </td>

                         <td style="display: none;">
                         <?php 
                              $status=$value['status'];
                              if($status=='send')
                              {
                                $res='sent';
                              }
                              if($status=='opened')
                              {
                                $res='viewed';
                              }
                              else
                              {
                                $res=$status;
                              }
                              echo ucfirst($res);
                              ?>
                        </td>
                     </tr>
          <div class="modal-alertsuccess alert alert-success" id="delete_archive_id<?php echo $value['id'];?>" style="display:none;">
                  <div class="newupdate_alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <div class="pop-realted1">
                  <div class="position-alert1">
                  Are you sure want to delete <b><a href="#" class="delete_archive"> Yes </a></b> OR <b><a href="#" id="close">No</a></b></div></div>
                  </div></div>             
                     <?php }  ?>
                     
                  </tbody>
               </table>
               <input type="hidden" class="rows_selected" id="select_archive_count" >
               <!-- table -->   
            </div>
          
               </div>
               <!-- table -->   
            </div>
            <!-- 3tab -->
         </div>
       </div>
      </div>
      <!-- tabcontent -->
   </div>
   <!-- right -->   
   <!-- modal --> 
   <?php //include('popup_sections.php'); ?>
   <!-- modal -->    
</div>
<!-- common-reduce -->
<?php $this->load->view('includes/session_timeout');?>
<?php $this->load->view('includes/footer');?>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>

<script type="text/javascript">
 var check=0;
    var check1=0;
var display_service1;
var display_service10;
var display_service2;

var display_service3;

var display_service4;

var display_service5;

var display_service6;


var display_service7;

var display_service8;

var display_service9;
$(document).on('click', '.Settings_Button', AddClass_SettingPopup);
$(document).on('click', '.Button_List', AddClass_SettingPopup );

$(document).on('click','.dt-button.close',function()
{
  $('.dt-buttons').find('.dt-button-collection').detach();
  $('.dt-buttons').find('.dt-button-background').detach();
});
function AddClass_SettingPopup()
{
  $('button.Button_List').closest('.dt-button-collection').addClass('Settings_List');
  $('button.buttons-columnVisibility').closest('.dt-button-collection').addClass('columnVisibility_List');
}

function initialize_datatable()
{
  $.fn.dataTable.moment( 'DD MMMM YYYY HH:mm' ) ;
   <?php 
          $column_setting = Firm_column_settings('proposal_template');

        ?>
var column_order = <?php echo $column_setting['order'] ?>;
        
        var hidden_coulmns = <?php echo $column_setting['hidden'] ?>;

         var column_ordering = [];

        $.each(column_order,function(i,v){

          var index = $('#display_service1 thead th').index($('.'+v));

          if(index!==-1)
          {
            column_ordering.push(index);
          }
        });



  var numCols = $('#display_service1 thead th').length;

        display_service1 = $('#display_service1').DataTable({
       "dom": '<"toolbar-table" B>lfrtip',
           buttons: [
            {
                extend: 'collection',
                background:false,
                text: '<i class="fa fa-cog" aria-hidden="true"></i>',
                className:'Settings_Button',
                buttons:[
                        { 
                          text: '<i class="fa fa-filter" aria-hidden="true"></i>Reset Filter',
                          className:'Button_List',
                          action: function ( e, dt, node, config )
                          {
                              Trigger_To_Reset_Filter();
                          }
                        },
                        {
                            extend: 'colvis',
                            text: '<i class="fa fa-cogs" aria-hidden="true"></i>Column Visibility',              
                            columns: ':not(.Exc-colvis)',
                            className:'Button_List',
                            prefixButtons:[
                            {text:'X',className:'close'}]
                        }
                ]
            }
          ],
          order: [],//ITS FOR DISABLE SORTING
          columnDefs: 
          [
            {"orderable": false,"targets": ['lead_chk_TH','action_TH']}
          ],

        
          fixedHeader: {
            header: true,
            footer: true
        },
         
         colReorder: 
          {
            realtime: false,
            order:column_ordering,
            fixedColumnsLeft : 2,
            fixedColumnsRight:1

          },
       initComplete: function () { 

         var api = this.api();

              api.columns('.hasFilter').every( function () {

                        var column = this;
                        var TH = $(column.header());
                        var Filter_Select = TH.find('.filter_check');
                        ///alert(Filter_Select.length);
                        // alert('check');
                    if( !Filter_Select.length )
                    {
                         // alert('check1');
                      Filter_Select = $('<select multiple="true" searchable="true" class="filter_check" style="display:none"></select>').appendTo( TH );
                      var unique_data = [];
                        column.nodes().each( function ( d, j ) { 
                          var dataSearch = $(d).attr('data-search');
                          
                            if( jQuery.inArray(dataSearch, unique_data) === -1 )
                            {
                              //console.log(d);
                              Filter_Select.append( '<option  value="'+dataSearch+'">'+dataSearch+'</option>' );
                              unique_data.push( dataSearch );
                            }

                        });
                    }
                    

                    Filter_Select.on( 'change', function () {

                        var search =  $(this).val(); 
                        //console.log( search );
                        if(search.length) search= '^('+search.join('|') +')$'; 

                       var  class_name = $(this).closest('th').attr('class').match(/\w+(?=_TH)/);  

                       var cur_column = api.column( '.'+class_name+'_TH' );
                       
                        cur_column.search( search, true, false ).draw();
                    } );
                      
                        //console.log(select.attr('style'));
                        Filter_Select.formSelect(); 

                   
                      }); 
                
        }
    });


        ColVis_Hide( display_service1 , hidden_coulmns );

      Filter_IconWrap();

      Change_Sorting_Event( display_service1 ); 

      ColReorder_Backend_Update ( display_service1 , 'proposal_template' );

      ColVis_Backend_Update ( display_service1 , 'proposal_template' );

      for(j=1;j<numCols;j++){  
          $('#all_'+j).on('change', function(){ 
            var result=$(this).attr('id').split('_');      
             var c=result[1];
              var search = [];              
              $.each($('#all_'+c+ ' option:selected'), function(){                
              search.push($(this).val());
              });      
              search = search.join('|');    
              if(c==1){               
              c=Number(c) + 1;
              }         

              if(c==3){               
              c=Number(c) + 1;
              }         


              if(c==5){               
              c=Number(c) + 1;
              }         


              if(c==7){               
              c=Number(c) + 1;
              }         

              display_service1.column(c).search(search, true, false).draw();  
          });
       }

                 // Reset js by Ram//

     /* var Reset_filter = "<li><button id='reset_filter_button' >Reset Filter</button></li>";
      
$("div.toolbar-table").append('<br><br><div class="filter-task1"><h2>Filter:</h2><ul>'+Reset_filter+'</ul></div> ');

$('#reset_filter_button').click(Trigger_To_Reset_Filter);*/

      // End Reset//
      


}


  $(document).ready(function(){
   $.fn.dataTable.moment( 'DD MMMM YYYY HH:mm' );

    initialize_datatable();

    var check=0;
    var check1=0;
    var numCols = $('#display_service10 thead th').length;


         display_service10 = $('#display_service10').DataTable({
        "dom": '<"top"fl>rt<"bottom"ip><"clear">',
       initComplete: function () { 
                  var q=1;
                     $('#display_service10 thead th').find('.filter_check').each( function(){
                     // alert('ok');
                     $(this).attr('searchable',true);
                       $(this).attr('id',"allpro_"+q);
                       q++;
                     });
                  for(i=1;i<numCols;i++){ 
                        if(i==1){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#allpro_1"); 
                        }else if(i==3){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#allpro_3"); 
                        }else  if(i==5){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#allpro_5"); 
                        }else  if(i==7){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#allpro_7"); 
                        }else  if(i==9){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#allpro_9"); 
                        }else{
                        var select = $("#allpro_"+i); 
                        }
                      this.api().columns([i]).every( function () {
                        var column = this;
                        column.data().unique().sort().each( function ( d, j ) {    
                      //    console.log(d);         
                          select.append( '<option value="'+d+'">'+d+'</option>' )
                        });
                      }); 

                     if(check=='1'){                 
                       i=Number(i) - 1;
                       check=0;
                    }
           
                     $("#allpro_"+i).formSelect();  
                  }
        }
    });
      for(j=1;j<numCols;j++){  
          $('#allpro_'+j).on('change', function(){ 
            var result=$(this).attr('id').split('_');      
             var c=result[1];
              var search = [];              
              $.each($('#allpro_'+c+ ' option:selected'), function(){                
              search.push($(this).val());
              });      
              search = search.join('|');    
              if(c==1){               
              c=Number(c) + 1;
              }         

              if(c==3){               
              c=Number(c) + 1;
              }         


              if(c==5){               
              c=Number(c) + 1;
              }         


              if(c==7){               
              c=Number(c) + 1;
              }         

              display_service10.column(c).search(search, true, false).draw();  
          });
       }
       /*2*/
    var check=0;
    var check1=0;
    var numCols = $('#display_service2 thead th').length;
        display_service2 = $('#display_service2').DataTable({
        "dom": '<"top"fl>rt<"bottom"ip><"clear">',
       initComplete: function () { 
                  var q=1;
                     $('#display_service2 thead th').find('.filter_check').each( function(){
                     // alert('ok');
                        $(this).attr('searchable',true);
                       $(this).attr('id',"send_"+q);
                       q++;
                     });
                  for(i=1;i<numCols;i++){ 
                        if(i==1){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#send_1"); 
                        }else if(i==3){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#send_3"); 
                        }else  if(i==5){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#send_5"); 
                        }else  if(i==7){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#send_7"); 
                        }else  if(i==9){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#send_9"); 
                        }else{
                        var select = $("#send_"+i); 
                        }
                      this.api().columns([i]).every( function () {
                        var column = this;
                        column.data().unique().sort().each( function ( d, j ) {    
                       //   console.log(d);         
                          select.append( '<option value="'+d+'">'+d+'</option>' )
                        });
                      }); 

                     if(check=='1'){                 
                       i=Number(i) - 1;
                       check=0;
                    }
           
                     $("#send_"+i).formSelect();  
                  }
        }
    });
      for(j=1;j<numCols;j++){  
          $('#send_'+j).on('change', function(){ 
            var result=$(this).attr('id').split('_');      
             var c=result[1];
              var search = [];              
              $.each($('#send_'+c+ ' option:selected'), function(){                
              search.push($(this).val());
              });      
              search = search.join('|');    
              if(c==1){               
              c=Number(c) + 1;
              }         

              if(c==3){               
              c=Number(c) + 1;
              } 
              if(c==5){               
              c=Number(c) + 1;
              } 
              if(c==7){               
              c=Number(c) + 1;
              }         

              display_service2.column(c).search(search, true, false).draw();  
          });
       }
       /*3*/

       var check=0;
    var check1=0;
    var numCols = $('#display_service3 thead th').length;
         display_service3 = $('#display_service3').DataTable({
        "dom": '<"top"fl>rt<"bottom"ip><"clear">',
       initComplete: function () { 
                  var q=1;
                     $('#display_service3 thead th').find('.filter_check').each( function(){
                     // alert('ok');
                      $(this).attr('searchable',true);
                       $(this).attr('id',"view_"+q);
                       q++;
                     });
                  for(i=1;i<numCols;i++){ 
                        if(i==1){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#view_1"); 
                        }else if(i==3){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#view_3"); 
                        }else  if(i==5){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#view_5"); 
                        }else  if(i==7){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#view_7"); 
                        }else  if(i==9){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#view_9"); 
                        }else{
                        var select = $("#view_"+i); 
                        }
                      this.api().columns([i]).every( function () {
                        var column = this;
                        column.data().unique().sort().each( function ( d, j ) {    
                       //   console.log(d);         
                          select.append( '<option value="'+d+'">'+d+'</option>' )
                        });
                      }); 

                     if(check=='1'){                 
                       i=Number(i) - 1;
                       check=0;
                    }
           
                     $("#view_"+i).formSelect();  
                  }
        }
    });
      for(j=1;j<numCols;j++){  
          $('#view_'+j).on('change', function(){ 
            var result=$(this).attr('id').split('_');      
             var c=result[1];
              var search = [];              
              $.each($('#view_'+c+ ' option:selected'), function(){                
              search.push($(this).val());
              });      
              search = search.join('|');    
              if(c==1){               
              c=Number(c) + 1;
              }   
              if(c==3){               
              c=Number(c) + 1;
              } 
              if(c==5){               
              c=Number(c) + 1;
              }  
              if(c==7){               
              c=Number(c) + 1;
              }         

              display_service3.column(c).search(search, true, false).draw();  
          });
       }

       /*4*/
       var check=0;
    var check1=0;
    var numCols = $('#display_service4 thead th').length;
        display_service4 = $('#display_service4').DataTable({
        "dom": '<"top"fl>rt<"bottom"ip><"clear">',
       initComplete: function () { 
                  var q=1;
                     $('#display_service4 thead th').find('.filter_check').each( function(){
                     // alert('ok');
                     $(this).attr('searchable',true);
                       $(this).attr('id',"accept_"+q);
                       q++;
                     });
                  for(i=1;i<numCols;i++){ 
                        if(i==1){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#accept_1"); 
                        }else if(i==3){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#accept_3"); 
                        }else  if(i==5){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#accept_5"); 
                        }else  if(i==7){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#accept_7"); 
                        }else  if(i==9){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#accept_9"); 
                        }else{
                        var select = $("#accept_"+i); 
                        }
                      this.api().columns([i]).every( function () {
                        var column = this;
                        column.data().unique().sort().each( function ( d, j ) {    
                       //   console.log(d);         
                          select.append( '<option value="'+d+'">'+d+'</option>' )
                        });
                      }); 

                     if(check=='1'){                 
                       i=Number(i) - 1;
                       check=0;
                    }
           
                     $("#accept_"+i).formSelect();  
                  }
        }
    });
      for(j=1;j<numCols;j++){  
          $('#accept_'+j).on('change', function(){ 
            var result=$(this).attr('id').split('_');      
             var c=result[1];
           //  alert(c);
              var search = [];              
              $.each($('#accept_'+c+ ' option:selected'), function(){                
              search.push($(this).val());
              });      
              search = search.join('|');    
              if(c==1){               
              c=Number(c) + 1;
              }         

              if(c==3){               
              c=Number(c) + 1;
              }         


              if(c==5){               
              c=Number(c) + 1;
              }         


              if(c==7){               
              c=Number(c) + 1;
              }         

              display_service4.column(c).search(search, true, false).draw();  
          });
       }
       /*5*/
       var check=0;
    var check1=0;
    var numCols = $('#display_service5 thead th').length;
        display_service5 = $('#display_service5').DataTable({
        "dom": '<"top"fl>rt<"bottom"ip><"clear">',
       initComplete: function () { 
                  var q=1;
                     $('#display_service5 thead th').find('.filter_check').each( function(){
                     // alert('ok');
                     $(this).attr('searchable',true);
                       $(this).attr('id',"decline_"+q);
                       q++;
                     });
                  for(i=1;i<numCols;i++){ 
                        if(i==1){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#decline_1"); 
                        }else if(i==3){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#decline_3"); 
                        }else  if(i==5){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#decline_5"); 
                        }else  if(i==7){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#decline_7"); 
                        }else  if(i==9){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#decline_9"); 
                        }else{
                        var select = $("#decline_"+i); 
                        }
                      this.api().columns([i]).every( function () {
                        var column = this;
                        column.data().unique().sort().each( function ( d, j ) {    
                       //   console.log(d);         
                          select.append( '<option value="'+d+'">'+d+'</option>' )
                        });
                      }); 

                     if(check=='1'){                 
                       i=Number(i) - 1;
                       check=0;
                    }
           
                     $("#decline_"+i).formSelect();  
                  }
        }
    });
      for(j=1;j<numCols;j++){  
          $('#decline_'+j).on('change', function(){ 
            var result=$(this).attr('id').split('_');      
             var c=result[1];
            //    alert(c);
              var search = [];              
              $.each($('#decline_'+c+ ' option:selected'), function(){                
              search.push($(this).val());
              });      
              search = search.join('|');    
              if(c==1){               
              c=Number(c) + 1;
              }         

              if(c==3){               
              c=Number(c) + 1;
              }         


              if(c==5){               
              c=Number(c) + 1;
              }         


              if(c==7){               
              c=Number(c) + 1;
              }         

              display_service5.column(c).search(search, true, false).draw();  
          });
       }
        var check=0;
    var check1=0;
    var numCols = $('#display_service6 thead th').length;
        display_service6 = $('#display_service6').DataTable({
        "dom": '<"top"fl>rt<"bottom"ip><"clear">',
       initComplete: function () { 
                  var q=1;
                     $('#display_service6 thead th').find('.filter_check').each( function(){
                     // alert('ok');
                     $(this).attr('searchable',true);
                       $(this).attr('id',"indis_"+q);
                       q++;
                     });
                  for(i=1;i<numCols;i++){ 
                        if(i==1){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#indis_1"); 
                        }else if(i==3){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#indis_3"); 
                        }else  if(i==5){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#indis_5"); 
                        }else  if(i==7){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#indis_7"); 
                        }else  if(i==9){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#indis_9"); 
                        }else{
                        var select = $("#indis_"+i); 
                        }
                      this.api().columns([i]).every( function () {
                        var column = this;
                        column.data().unique().sort().each( function ( d, j ) {    
                        //  console.log(d);         
                          select.append( '<option value="'+d+'">'+d+'</option>' )
                        });
                      }); 

                     if(check=='1'){                 
                       i=Number(i) - 1;
                       check=0;
                    }
           
                     $("#indis_"+i).formSelect();  
                  }
        }
    });
      for(j=1;j<numCols;j++){  
          $('#indis_'+j).on('change', function(){ 
            var result=$(this).attr('id').split('_');      
             var c=result[1];
              //  alert(c);
              var search = [];              
              $.each($('#indis_'+c+ ' option:selected'), function(){                
              search.push($(this).val());
              });      
              search = search.join('|');    
              if(c==1){               
              c=Number(c) + 1;
              }         

              if(c==3){               
              c=Number(c) + 1;
              }         


              if(c==5){               
              c=Number(c) + 1;
              }         


              if(c==7){               
              c=Number(c) + 1;
              }         

              display_service6.column(c).search(search, true, false).draw();  
          });
       }


         var check=0;
    var check1=0;
    var numCols = $('#display_service7 thead th').length;
       display_service7 = $('#display_service7').DataTable({
       "dom": '<"top"fl>rt<"bottom"ip><"clear">',
       initComplete: function () { 
                  var q=1;
                     $('#display_service7 thead th').find('.filter_check').each( function(){
                     // alert('ok');
                     $(this).attr('searchable',true);
                       $(this).attr('id',"draft_"+q);
                       q++;
                     });
                  for(i=1;i<numCols;i++){ 
                        if(i==1){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#draft_1"); 
                        }else if(i==3){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#draft_3"); 
                        }else  if(i==5){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#draft_5"); 
                        }else  if(i==7){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#draft_7"); 
                        }else  if(i==9){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#draft_9"); 
                        }else{
                        var select = $("#draft_"+i); 
                        }
                      this.api().columns([i]).every( function () {
                        var column = this;
                        column.data().unique().sort().each( function ( d, j ) {    
                      //    console.log(d);         
                          select.append( '<option value="'+d+'">'+d+'</option>' )
                        });
                      }); 

                     if(check=='1'){                 
                       i=Number(i) - 1;
                       check=0;
                    }
           
                     $("#draft_"+i).formSelect();  
                  }
        }
    });
      for(j=1;j<numCols;j++){  
          $('#draft_'+j).on('change', function(){ 
            var result=$(this).attr('id').split('_');      
             var c=result[1];
        //        alert(c);
              var search = [];              
              $.each($('#draft_'+c+ ' option:selected'), function(){                
              search.push($(this).val());
              });      
              search = search.join('|');    
              if(c==1){               
              c=Number(c) + 1;
              }         

              if(c==3){               
              c=Number(c) + 1;
              }         


              if(c==5){               
              c=Number(c) + 1;
              }         


              if(c==7){               
              c=Number(c) + 1;
              }         

              display_service7.column(c).search(search, true, false).draw();  
          });
       }



        var check=0;
    var check1=0;
    var numCols = $('#display_service8 thead th').length;
        display_service8 = $('#display_service8').DataTable({
        "dom": '<"top"fl>rt<"bottom"ip><"clear">',
       initComplete: function () { 
                  var q=1;
                     $('#display_service8 thead th').find('.filter_check').each( function(){
                     // alert('ok');
                     $(this).attr('searchable',true);
                       $(this).attr('id',"exp_"+q);
                       q++;
                     });
                  for(i=1;i<numCols;i++){ 
                        if(i==1){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#exp_1"); 
                        }else if(i==3){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#exp_3"); 
                        }else  if(i==5){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#exp_5"); 
                        }else  if(i==7){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#exp_7"); 
                        }else  if(i==9){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#exp_9"); 
                        }else{
                        var select = $("#exp_"+i); 
                        }
                      this.api().columns([i]).every( function () {
                        var column = this;
                        column.data().unique().sort().each( function ( d, j ) {    
                     //     console.log(d);         
                          select.append( '<option value="'+d+'">'+d+'</option>' )
                        });
                      }); 

                     if(check=='1'){                 
                       i=Number(i) - 1;
                       check=0;
                    }
           
                     $("#exp_"+i).formSelect();  
                  }
        }
    });
      for(j=1;j<numCols;j++){  
          $('#exp_'+j).on('change', function(){ 
            var result=$(this).attr('id').split('_');      
             var c=result[1];
            //    alert(c);
              var search = [];              
              $.each($('#exp_'+c+ ' option:selected'), function(){                
              search.push($(this).val());
              });      
              search = search.join('|');    
              if(c==1){               
              c=Number(c) + 1;
              }         

              if(c==3){               
              c=Number(c) + 1;
              }         


              if(c==5){               
              c=Number(c) + 1;
              }         


              if(c==7){               
              c=Number(c) + 1;
              }         

              display_service8.column(c).search(search, true, false).draw();  
          });
       }
      // display_service10


        var check=0;
    var check1=0;
    var numCols = $('#display_service9 thead th').length;
        display_service9 = $('#display_service9').DataTable({
        "dom": '<"top"fl>rt<"bottom"ip><"clear">',
       initComplete: function () { 
                  var q=1;
                     $('#display_service9 thead th').find('.filter_check').each( function(){
                     // alert('ok');
                     $(this).attr('searchable',true);
                       $(this).attr('id',"arch_"+q);
                       q++;
                     });
                  for(i=1;i<numCols;i++){ 
                        if(i==1){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#arch_1"); 
                        }else if(i==3){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#arch_3"); 
                        }else  if(i==5){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#arch_5"); 
                        }else  if(i==7){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#arch_7"); 
                        }else  if(i==9){
                        check=1;          
                        i=Number(i) + 1;
                        var select = $("#arch_9"); 
                        }else{
                        var select = $("#arch_"+i); 
                        }
                      this.api().columns([i]).every( function () {
                        var column = this;
                        column.data().unique().sort().each( function ( d, j ) {    
                        //  console.log(d);         
                          select.append( '<option value="'+d+'">'+d+'</option>' )
                        });
                      }); 

                     if(check=='1'){                 
                       i=Number(i) - 1;
                       check=0;
                    }
           
                     $("#arch_"+i).formSelect();  
                  }
        }
    });
      for(j=1;j<numCols;j++){  
          $('#arch_'+j).on('change', function(){ 
            var result=$(this).attr('id').split('_');      
             var c=result[1];
              //  alert(c);
              var search = [];              
              $.each($('#arch_'+c+ ' option:selected'), function(){                
              search.push($(this).val());
              });      
              search = search.join('|');    
              if(c==1){               
              c=Number(c) + 1;
              }         

              if(c==3){               
              c=Number(c) + 1;
              }         


              if(c==5){               
              c=Number(c) + 1;
              }         


              if(c==7){               
              c=Number(c) + 1;
              }         

              display_service9.column(c).search(search, true, false).draw();  
          });
       }

});
   
   
   
   
      $(' .tab-leftside .nav-tabs > li a').not('.sub-categories1 a').click(function(){
       var $this = $(this);
       var child = $this.closest('a').next('.sub-categories1'),
         rest = $('.sub-categories1:visible').not(child);
        $this.next('.sub-categories1').slideToggle(300);
        rest.slideUp();

      });
      
      $(' .tab-leftside .nav-tabs li a').click(function(){
       $('li a').removeClass("active-previous1");
       $(this).addClass("active-previous1");
           });
   //    $('#login_form').submit(function() {
   //     $('.LoadingImage').show();
   // });
   
   //    $('.decimal').keyup(function(){
   //     var val = $(this).val();
   //     if(isNaN(val)){
   //          val = val.replace(/[^0-9\.]/g,'');
   //          if(val.split('.').length>2) 
   //              val =val.replace(/\.+$/,"");
   //     }
   //     $(this).val(val); 
   // });
</script>

<script>


$(document).ready(function(){

   $(".unarchive").click(function(){
  //  alert('ok');
     var id=$(this).attr('id'); 
    // alert(id);    
    $.ajax({
        type: "POST",
        // dataType: "JSON",
        url: "<?php echo base_url().'Proposal_pdf/Unarchive_status';?>",
        data: {"id":id},
         beforeSend: function() {
                 $(".LoadingImage").show();
              },
        success: function(data){
          $(".LoadingImage").hide();
          $('.alert-success1').show();
          setTimeout(function(){  $('.alert-success').hide(); location.reload(); }, 3000);
          //alert(data);
        }

      });
  });

   });
</script>
<script type="text/javascript">
 //$(document).ready(function(){
  // $('#select_all_proposal').on('click', function() {
  //   if($(this).is(':checked', true)) {
  //      $("#proposal_delete_items").show();    
  //     $(".proposal_checkbox").prop('checked', true);
  //   } else {

  //     $(".proposal_checkbox").prop('checked', false);

  //     console.log($("input.proposal_checkbox:checked").length);
  //      if($("input.proposal_checkbox:checked").length ==0) {

  //        $("#proposal_delete_items").hide(); 
  //      }     
  //   }    
  //   $("#select_proposal_count").val($("input.proposal_checkbox:checked").length+" Selected");
  // });

   
    // $(document).on('click','.proposal_checkbox', function() {
    //   var len=$("input.proposal_checkbox:checked").length;
    //     if(len > 0) {
    //         $("#proposal_delete_items").show(); 
    //     }else{        
    //         $("#proposal_delete_items").hide(); 
    //     }      
    //   $("#select_proposal_count").val($("input.proposal_checkbox:checked").length+" Selected");
    // });  


/*$(document).on('click','#proposal_delete_items', function() {

  //var table_id;

   var ref = $("ul.proposal_tabs").find("li").find('.active').attr('href');
   var href = ref.substring(ref.indexOf("#") + 1); 
   

    var table_id=$("#"+href).find('table').attr('id');


 var table_obj;
      if(table_id=="display_service1")table_obj=display_service1;
  else if (table_id=="display_service10")table_obj=display_service10;
  else if(table_id=="display_service2")table_obj=display_service2;
  else if(table_id=="display_service3")table_obj=display_service3;
  else if(table_id=="display_service4")table_obj=display_service4;
   else if(table_id=="display_service5")table_obj=display_service5;
  else if(table_id=="display_service6")table_obj=display_service6;
  else if(table_id=="display_service7")table_obj=display_service7;
  else if(table_id=="display_service8")table_obj=display_service8;
  else if(table_id=="display_service9")table_obj=display_service9;
  //  console.log(table_obj);
   var proposal = [];

   table_obj.column(0).nodes().to$().each(function(index) {
      if($(this).find("."+href+"_checkbox").is(":checked"))
      {

        console.log(href);

//        console.log($(this).find("."+href+"_checkbox").attr('class'));
        proposal.push($(this).find("."+href+"_checkbox").data('proposal-id'));
      }
    });


  // alert(proposal);

  // $("."+href+"_checkbox:checked").each(function() {
  //   proposal.push($(this).data('proposal-id'));
  // });

  if(proposal.length <=0) {
     $('.alert-danger-check_proposal').show();
  } else {

    $("#delete_ids").val(proposal);
    $('#delete_proposal_id').show();
   
  }
}); */ 
 
 /*Shashmethah*/
 
    $(document).ready(function()
    {
      window.allproposal = []; 
    });

    function getSelectedRow(id)
    {              
       allproposal.push(id);   
       return $.unique(allproposal);
    }

    function return_indice(id)
    { 
       for(var i=0;i<allproposal.length;i++)
       {
          if(id == allproposal[i])
          {
             return i;
          }          
       }
    }

    function dounset(id)
    {  
       allproposal = $.unique(allproposal);
       var indice = return_indice(id);
        
       if(indice > -1) 
       {
          allproposal.splice(indice,1);
       }
 
       return allproposal;
    }

   $('.custom_checkbox1 input[type="checkbox"]').click(function()
   {  
       var wholeid;
       var whole_id;
       var all_prop;

       if($(this).attr('id')) 
       {      
         whole_id = $(this).attr('id');
         wholeid = whole_id.search('select_all'); 
       }       

       if(wholeid != 0)
       { 
           var id = $(this).data('proposal-id');

           if($(this).is(":checked"))
           {         
              all_prop = getSelectedRow(id); 
              $("#delete_ids").val(JSON.stringify(all_prop));     
           }
           else
           {  
              var props = dounset(id); 
              $("#delete_ids").val(JSON.stringify(props));  
           }   
       }
       else if(wholeid == 0)
       { 
          if($('#'+whole_id).is(':checked'))
          {
             var tbl = $('#'+whole_id).parent('label').parent('th').parent('tr').parent('thead').parent('table').attr('id');

             $('#'+tbl+' tbody tr').each(function(index)
             {
                 var tr_id = $(this).attr('id');
                 all_prop = getSelectedRow(tr_id);                  
             });

             $("#delete_ids").val(JSON.stringify(all_prop));
          }
          else
          { 
             allproposal = [];
             $("#delete_ids").val('');
          }
       }   
   });  
   
   $(document).on('click','.delete_proposal',function() 
   {
      var selected_proposal_values = $("#delete_ids").val();
     
      $.ajax(
      {
          type: "POST",
          url: "<?php echo base_url().'proposal_page/deleteRecords';?>",
          cache: false,
          data: {'data_id':selected_proposal_values},

          beforeSend: function() 
          {
            $(".LoadingImage").show();
          },

          success: function(response) 
          { 
            $(".LoadingImage").hide();
            
            if(response == 1)  
            {
               $(".alert-success-delete").show();
               setTimeout(function(){location.reload(); }, 2000);     
            }   
          }
      });
   });   


/*Shashmethah*/

$(document).on('change','.archive_checkbox', function() {

    if($(this).is(':checked', true)) {  

       $("#proposal_delete_items").show(); 

      }else{
      $("#select_all_archive").prop('checked',false);
      if($("input.archive_checkbox:checked").length==0){    
         $("#proposal_delete_items").hide();         
        }
      }
      $("#select_archive_count").val($("input.archive_checkbox:checked").length+" Selected");
   });



   $(document).on('change','.all_checkbox', function() {

    if($(this).is(':checked', true)) {  

       $("#proposal_delete_items").show(); 

      }else{
      $("#select_all").prop('checked',false);
      if($("input.all_checkbox:checked").length==0){    
         $("#proposal_delete_items").hide();         
        }
      }
      $("#select_archive_count").val($("input.select_all_count:checked").length+" Selected");
   });


 $(document).on('change','.expired_checkbox', function() {
  if($(this).is(':checked', true)) {  

       $("#proposal_delete_items").show(); 

      }else{
        $("#select_all_expired").prop('checked',false);
        if($("input.expired_checkbox:checked").length== 0){
           $("#proposal_delete_items").hide(); 
        }
        }
   $("#select_expired_count").val($("input.expired_checkbox:checked").length+" Selected");
 }); 


 $(document).on('change','.draft_checkbox', function() {

    if($(this).is(':checked', true)) { 
       $("#proposal_delete_items").show(); 
      }else{
        $("#select_all_draft").prop('checked',false);
        if($("input.draft_checkbox:checked").length== 0){
           $("#proposal_delete_items").hide(); 
        }
      }
   $("#select_draft_count").val($("input.draft_checkbox:checked").length+" Selected");
 }); 


  $(document).on('change','.discussion_checkbox', function() {
  if($(this).is(':checked', true)) { 
       $("#proposal_delete_items").show(); 
      }else{
      $("#select_all_discussion").prop('checked',false);
        if($("input.discussion_checkbox:checked").length== 0){
           $("#proposal_delete_items").hide(); 
        }
      }
     $("#select_discussion_count").val($("input.discussion_checkbox:checked").length+" Selected");
   }); 



 $(document).on('change','.accept_checkbox', function() {
  if($(this).is(':checked', true)) { 
       $("#proposal_delete_items").show(); 
      }else{
      $("#select_all_accept").prop('checked',false);
        if($("input.accept_checkbox:checked").length== 0){
           $("#proposal_delete_items").hide(); 
        }
      }
      $("#select_decline_count").val($("input.decline_checkbox:checked").length+" Selected");
    }); 

     

  $(document).on('click','.view_checkbox', function() {
      var len=$("input.view_checkbox:checked").length;
      if(len>0)
      {
         $("#proposal_delete_items").show();
      }else{
         $("#proposal_delete_items").hide();
      }
      $("#select_view_count").val($("input.view_checkbox:checked").length+" Selected");
    });  


      $(document).on('click','.send_checkbox', function() {
      var len=$("input.send_checkbox:checked").length;
      if(len > 0){
         $("#proposal_delete_items").show();
      }else{
         $("#proposal_delete_items").hide();
      } 
      $("#select_send_count").val($("input.send_checkbox:checked").length+" Selected");
    });  


    $(document).on('change','.proposal_checkbox', function() {
    if($(this).is(':checked', true)) { 
       $("#proposal_delete_items").show(); 
    }else{
        $("#select_all_proposal").prop('checked',false);
        if($("input.proposal_checkbox:checked").length==0){   
           $("#proposal_delete_items").hide();         
         }
      }      
   });


 $(document).ready(function() {
 $(document).on('click','#close', function() {
  //alert('ok');
  $(".modal-alertsuccess").hide();
});

 $(document).on('click','#del_close', function(){

     ('#delete_proposal_id').hide();
 });

 });


 $(document).ready(function(){        
        <?php
        if(isset($_SESSION['firm_seen'])){ ?>
          <?php
        if($_SESSION['firm_seen']=='archieve'){ ?>
          $("li a.archive").trigger('click');
       <?php } ?>
         <?php } ?>

});


$(document).ready(function() {
$(document).on('change','#select_all_send', function(event) {  //on click                         
var checked = this.checked;
display_service2.column(0).nodes().to$().each(function(index) {    
  if (checked) {    
      $(this).find('.send_checkbox').prop('checked', 'checked'); 
      $(".delete").hide();
      $("#proposal_delete_items").show();
  } else {
      $(this).find('.send_checkbox').prop('checked', false);              
      $("#proposal_delete_items").hide();      
  }
});
}); 

$(document).on('change','#select_all_view',function(event) {  //on click   
var checked = this.checked;
display_service3.column(0).nodes().to$().each(function(index) {    
  if (checked) {
     $(this).find('.view_checkbox').prop('checked', 'checked');
     $("#proposal_delete_items").show();
  } else {
     $(this).find('.view_checkbox').removeProp('checked');                  
     $("#proposal_delete_items").hide();      
  }
});
});

$(document).on('change','#select_all_proposal',function(event) {  //on click  
var checked = this.checked;
display_service1.column(0).nodes().to$().each(function(index) {    
if (checked) {
    $(this).find('.proposal_checkbox').prop('checked',true); 
    $("#proposal_delete_items").show();
} else {
    $(this).find('.proposal_checkbox').prop('checked', false);            
    $("#proposal_delete_items").hide();      
}
});
}); 
$(document).on('change','#select_all_archive', function() {
var checked = this.checked;
display_service9.column(0).nodes().to$().each(function(index) {    
if (checked) {
    $(this).find(".archive_checkbox").prop('checked', true);
    $("#proposal_delete_items").show();     
  } else {    
     $(this).find(".archive_checkbox").prop('checked', false);
     $("#proposal_delete_items").hide();     
  }
});
});


$(document).on('change','#select_all_discussion', function() {
  var checked = this.checked;
  display_service6.column(0).nodes().to$().each(function(index) {    
  if (checked) {
      $(this).find(".discussion_checkbox").prop('checked', true);
       $("#proposal_delete_items").show();  
    } else {
      $(this).find(".discussion_checkbox").prop('checked', false);
       $("#proposal_delete_items").hide();  
    }
   
  });
});


$(document).on('change','#select_all_decline', function() {
  var checked = this.checked;
  display_service5.column(0).nodes().to$().each(function(index) {    
  if (checked) {
      $(this).find(".decline_checkbox").prop('checked', true);
       $("#proposal_delete_items").show();  
    } else {
      $(this).find(".decline_checkbox").prop('checked', false);
       $("#proposal_delete_items").hide();  
    }   
    
  });
});


$(document).on('change','#select_all_accept', function() {
     var checked = this.checked;
  display_service4.column(0).nodes().to$().each(function(index) {    
  if (checked) {
      $(this).find(".accept_checkbox").prop('checked', true);
       $("#proposal_delete_items").show(); 
    } else {
      $(this).find(".accept_checkbox").prop('checked', false);
       $("#proposal_delete_items").hide(); 
    }  
   
  });
});


$(document).on('change','#select_all_draft', function() {
  var checked = this.checked;
  display_service7.column(0).nodes().to$().each(function(index) {    
  if (checked) {
    $(this).find(".draft_checkbox").prop('checked', true);
     $("#proposal_delete_items").show(); 
  } else {
    $(this).find(".draft_checkbox").prop('checked', false);
     $("#proposal_delete_items").hide(); 
  } 
});
});

 $(document).on('change','#select_all_expired', function() {
   var checked = this.checked;
  display_service8.column(0).nodes().to$().each(function(index) {    
  if (checked) { 
    $(this).find(".expired_checkbox").prop('checked', true);
      $("#proposal_delete_items").show(); 
  } else {
    $(this).find(".expired_checkbox").prop('checked', false);
      $("#proposal_delete_items").hide(); 
  }  
});
});  


  $(document).on('change','#select_all', function() {
   var checked = this.checked;
  display_service10.column(0).nodes().to$().each(function(index) {    
  if (checked) { 
    $(this).find(".all_checkbox").prop('checked', true);
      $("#proposal_delete_items").show(); 
  } else {
    $(this).find(".all_checkbox").prop('checked', false);
      $("#proposal_delete_items").hide(); 
  }  
});
}); 

});

  $(document).on('click','.DT', function (e) {
        if (!$(e.target).hasClass('sortMask')) {      
            e.stopImmediatePropagation();
        }
    });

$('th .themicond').on('click', function(e) {
  if ($(this).parent().find(".dropdown-content").hasClass("Show_content")) {
  $(this).parent().find('.dropdown-content').removeClass('Show_content');
  }else{
   $('.dropdown-content').removeClass('Show_content');
   $(this).parent().find('.dropdown-content').addClass('Show_content');
  }
  $(this).parent().find('.select-wrapper').toggleClass('special');
      if( $(this).parent().find('.select-dropdown span input[type="checkbox"]').parent().hasClass('custom_checkbox1')){
      }else{
        $(this).parent().find('.select-dropdown span input[type="checkbox"]').wrap( '<label class="custom_checkbox1"></label>');
        $(this).parent().find('.custom_checkbox1 input').after( "<i></i>" );

      }
});

 $(document).on('click', 'th .themicond', function(){
      if ($(this).parent().find(".dropdown-content").hasClass("Show_content")) {
         $(this).parent().find('.dropdown-content').removeClass('Show_content');
      }else{
         $('.dropdown-content').removeClass('Show_content');
         $(this).parent().find('.dropdown-content').addClass('Show_content');
      }
          $(this).parent().find('.select-wrapper').toggleClass('special');
      if( $(this).parent().find('.select-dropdown span input[type="checkbox"]').parent().hasClass('custom_checkbox1')){ 
      }else{
          $(this).parent().find('.select-dropdown span input[type="checkbox"]').wrap( '<label class="custom_checkbox1"></label>');
          $(this).parent().find('.custom_checkbox1 input').after( "<i></i>" );
      }
});


   $("th").on("click.DT", function (e) {      
        if (!$(e.target).hasClass('sortMask')) {       
            e.stopImmediatePropagation();
        }
    });



$(document).on('click','.close',function(){
  $("#delete_proposal_id").css('display','none');
});


 $(document).on('click', '.task_status_val', function(){


          
          var col = $.trim( $(this).attr('data-searchCol') );
          var val = $.trim( $(this).attr('data-id') );
          
          Trigger_To_Reset_Filter();
         

          MainStatus_Tab = val;

          display_service1.column('.'+col).search(val,true,false).draw();
        });


       function  Trigger_To_Reset_Filter()
              {

                  $('#select_all_proposal').prop('checked',false);
                  $('#select_all_proposal').trigger('change');
                  

                  $('div.toolbar-table .toolbar-search li.dropdown-chose').each(function(){
                    $(this).trigger('click');
                  });


                  MainStatus_Tab ="all_task";

                  Redraw_Table(display_service1);

                 // Redraw_Table(TaskTable_Instance);
                   
              }
</script>

<!--- *********************************************** -->
 <!--   Datatable JS by Ram -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script src="<?php echo base_url()?>assets/js/user_page/materialize.js"></script> 

<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>

<script src="<?php echo base_url()?>assets/js/custom/buttons.colVis.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>
<script src="<?php echo base_url()?>assets/js/custom/datatable_extension.js"></script>
 <!--   End JS by Ram -->

 <!-- For Date fields sorting  -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
<script src="<?php echo base_url()?>assets/js/custom/datatable_cus_sorting.js"></script>
<!-- For Date fields sorting  -->