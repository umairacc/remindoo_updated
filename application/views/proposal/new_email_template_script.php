<?php if($_SESSION['firm_id'] == '0'){ ?>
 <script type="text/javascript" src="<?php echo base_url();?>bower_components/jquery/js/jquery.min.js"></script>
<?php } ?>	
<script>    
   
	$(document).on('change','#type',function()
	{
        if($(this).val() == '22')
        {
        	$('.email_title').css('display','block');
        }
        else
        {
        	$('.email_title').css('display','none');
        	var title = $(this).find('option:selected').html(); 
        	title.trim();
        	check_distinct('title',title);	           
        }
	});

	$(document).ready(function()
	{
       var type = $('#type').val();

       if(type == 22)
       {
       	  $('.email_title').css('display','block');
       }
	});

	CKEDITOR.config.toolbar = [
	['Styles','Format','Font','FontSize'],
	'/',
	['Bold','Italic','Underline','StrikeThrough','-','Undo','Redo','-','Cut','Copy','Paste','Find','Replace','-','Outdent','Indent','-','Print'],
	'/',
	['NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
	['Image','Table','-','Link','Flash','Smiley','TextColor','BGColor','Source']
	] ;

	'use strict';

	var CONTACTS2 = [
		{ name: 'ClientUsername' },
		{ name: 'ClientPassword' },
		{ name: 'ClientName' },
		{ name: 'ClientCompany' },
		{ name: ' ClientCompanyCity' },
		{ name: 'ClientCompanyCountry' },
		{ name: 'ClientCompanyReligion' },
		{ name: ' ClientFirstName'},
		{ name: ' ClientLastName' },
		{ name: 'ClientName' },
		{ name: 'ClientPhoneNo' },
		{ name: 'ClientWebsite' },
		{ name: 'Sender Company Name' },
		{ name: 'Sender Name' },
		{ name: 'Staff Name' },
		{ name: 'Task Link' },
		{ name: 'Task Name' },
		{ name: 'Assign via' },
		{ name: 'Staff Name' },
		{ name: 'Lead Link' },
		{ name: 'Lead Name' },
		{ name: 'Assign via' },
		{ name: 'Staff Name' },
		{ name: 'Task Link' },
		{ name: 'Task Name' },
		{ name: 'Assign via' },
		{ name: 'Service Name' },
		{ name: 'Service Start date' },
		{ name: 'Service End date' },
		{ name: 'Service link' },	
		{ name: 'Sender Company Name' },	

	];

	CKEDITOR.disableAutoInline = true;
	CKEDITOR.plugins.add( 'hcard2', {
		requires: 'widget',
		init: function( editor ) {
			editor.widgets.add( 'hcard2', {
				allowedContent: 'span(!h-card2); a[href](!u-email,!p-name); span(!p-tel)',
				requiredContent: 'span(h-card2)',
				pathName: 'hcard2',

				upcast: function( el ) {
					return el.name == 'span' && el.hasClass( 'h-card2' );
				}
			} );
			editor.addFeature( editor.widgets.registered.hcard2 );
			editor.on( 'paste', function( evt ) { 
				var contact = evt.data.dataTransfer.getData( 'contact' ); 
				if ( !contact ) {
					return;
				} 
				evt.data.dataValue =						
					'<span class="h-card2">' +
						'::'+contact.name+'::'  +
					'</span>';
			} );
		}
	} );

	CKEDITOR.on( 'instanceReady', function() {
		
		CKEDITOR.document.getById( 'contactList2' ).on( 'dragstart', function( evt ) {
			
			var target = evt.data.getTarget().getAscendant( 'div', true ); 

			CKEDITOR.plugins.clipboard.initDragDataTransfer( evt );

			var dataTransfer = evt.data.dataTransfer;				
			dataTransfer.setData( 'contact2', CONTACTS2[ target.data( 'contact2' ) ] );		
			dataTransfer.setData( 'text/html', target.getText() );
			
			//if ( dataTransfer.$.setDragImage ) { 
			//	dataTransfer.$.setDragImage(target.findOne( 'img' ).$, 0, 0);//
			//}
		} );
	});

	// Initialize the editor with the hcard plugin.
	CKEDITOR.inline( 'editor3', {
		extraPlugins: 'hcard2,sourcedialog,justify'
	});

	CKEDITOR.inline( 'editor4', {
		extraPlugins: 'hcard2,sourcedialog,justify'
	});



	$(document).on("click","#add_template",function()
	{		
		var uri = '<?php echo $this->uri->segment(2); ?>'; 
        var action = '<?php echo substr($this->uri->segment(2),0, strpos($this->uri->segment(2), '_')); ?>';
        var id = '<?php echo $this->uri->segment(3); ?>'; 

		var body1 = $("#editor4").html(); 
		var subject1 = $("#editor3").html(); 
		var title = $("#title").val(); 
		var status = $("#temp_status").val(); 
		var from_address = $('#from_address').val();
		var type = $('#type').val();

		body1.trim();
		subject1.trim();

		if(body1 == "<p><br></p>" || body1 == "")
		{
           $('#body_er').html('Required Field');
		}
		else
		{
		   $('#body_er').html('');
		}

		if(subject1 == "<p><br></p>" || subject1 == "")
		{
		   $('#subject1_er').html('Required Field');
		}
		else
		{
		   $('#subject1_er').html('');
		}
        
        if(from_address == "")
        { 
           $('#from_address_er').html('Required Field');
        }
        else
        {
           var email_er;

           if(!isValidEmailAddress(from_address))
           {
           	  email_er = 1;
           	  $('#from_address_er').html('Provide Valid Email Address.');
           }
           else
           {
              email_er = 0;
              $('#from_address_er').html('');
           }  
        }		

		if(status == "")
		{
		   $('#status_er').html('Required Field');
		}
		else
		{
		   $('#status_er').html('');
		}

		if(action == "add")
		{
		   var target_url = '<?php echo base_url().'Email_template/insert/';?>';		
        }
        else if(action == "edit")
		{
		   var target_url = '<?php echo base_url().'Email_template/update/';?>'+id;		
        }

		if(uri == 'add_template' || uri == 'edit_template')
		{		    
		   var formData = {'body':body1,'type':type,'title':title,'subject':subject1,'status':status,'from_address':from_address};

		   if(type == "")
		   {
		      $('#type_er').html('Required Field');
		   }
		   else
		   {
		      $('#type_er').html('');
		   }

		   if(title == "" && type == "22")
		   { 
		      var tit_er = 1;
		      $('#title_er').html('Required Field');
		   }
		   else
		   {
		      var tit_er = 0;
		      $('#title_er').html('');
		   }

		   if(body1 != "<p><br></p>" && body1 != "" && subject1 != "<p><br></p>" && subject1 != "" && type != "" && status!="" && from_address != "" && email_er == 0 && tit_er == 0)
		   { 		   	   
		   	   sendtemplate(formData,target_url);
		   }
		   
		}
		else if(uri == 'add_reminder' || uri == 'edit_reminder')
		{
		   var reminder_heading = $('#reminder_heading').val(); 
		   var formData = {'body':body1,'reminder_heading':reminder_heading,'title':title,'subject':subject1,'status':status,'from_address':from_address};

		   if(reminder_heading == "")
		   {
		      $('#reminder_heading_er').html('Required Field');
		   }
		   else
		   {
		      $('#reminder_heading_er').html('');
		   }

		  /* if(title == "")
		   { 		     
		      $('#title_er').html('Required Field');
		   }
		   else
		   {
		      $('#title_er').html('');
		   }*/

		   if(body1 != "<p><br></p>" && body1 != "" && subject1 != "<p><br></p>" && subject1 != "" && reminder_heading != "" && status!="" && from_address!="" && email_er == 0)
		   { 
		   	 sendtemplate(formData,target_url);
		   }
		}		
	
	 });
          
     function sendtemplate(formData,target_url)
     { 
		var str = "";

		$('.cke_widget_wrapper > span.h-card2').each(function(){
			str += $(this).html();				
		});	

		$("span.cke_reset").each(function () {
		$(this).remove();
		});

		$("span.cke_widget_wrapper").each(function () {
		$(this).replaceWith($(this).text());
		});

		$('div[data-cke-hidden-sel="1"]').remove();								
	 	       
	    $.ajax({

		       url: target_url,
		       type : 'POST',
		       data : formData,	                  
		       beforeSend: function() 
		       {
		          $(".LoadingImage").show();
		       },
		       success: function(message) 
		       { 
		          $(".LoadingImage").hide();
		          window.close();	       	  
		     
				  setTimeout(function() 
				  { 
					 location.reload();
				  }, 3000);			              
		      	  
		      }
	     });				
	 } 

     function check_distinct(field,value)
     {
     	var obj = '#';
     	obj = obj.concat(field+'_er');

     	var segment = '<?php echo $this->uri->segment(2); ?>';
     	var id = '<?php echo $this->uri->segment(3); ?>';

     	$.ajax(
		{
           url: '<?php echo base_url();?>Email_template/check_distinct/',
           type : 'POST',
           data : {'field':field,'value':value,'segment':segment,'id':id},	

	       beforeSend: function() 
	       {
              $(".LoadingImage").show();
           },
           success: function(count) 
           { 
	           $(".LoadingImage").hide();			           	
				
			   if(count>0)
		       {
                  if($('#'+field).is(':visible'))
                  { 
                    $(obj).html("Already Exists.");
                  }
                  else
                  {
                  	$('#type_er').html("Already Exists.");
                  	$('#type').val('');
                  }  
		       }
		       else
		       { 
		       	  $(obj).html("");
		       	  $('#type_er').html('');
		       }				
		
			   $("#editor4").attr('contenteditable','true');				
			   $("#editor3").attr('contenteditable','true');
           }

        });	
     }

	 $(document).on("blur","#title",function()
	 {
		var title = this.value;		
	
		if(title != '')
		{
		   check_distinct('title',title);		
	    }

     });


	$(document).on("blur","#reminder_heading",function()
	{ 
		var reminder_heading = this.value;		
	
		if(reminder_heading != '')
		{
		   check_distinct('reminder_heading',reminder_heading);		
	    } 
	});

	function isValidEmailAddress(emailAddress) 
	{
	    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	    return pattern.test(emailAddress);
	}	

 </script>