<?php

if ($_SESSION['firm_id'] == '0') {
	$this->load->view('super_admin/superAdmin_header');
} else {
	$this->load->view('includes/header');
}

?>

<script type="text/javascript">
	(function(base, search, replace) {

		window.start_time = Math.round(new Date().getTime() / 1000);

		var extend = function(a, b) {
				for (var key in b)
					if (b.hasOwnProperty(key))
						a[key] = b[key];
				return a;
			},
			refactor = function() {

				if (!replace)
					replace = true;

				var elements = extend({
						script: 'src',
						img: 'src',
						link: 'href',
						a: 'href',
					}, search),
					generateID = function(min, max) {
						min = min || 0;
						max = max || 0;

						if (
							min === 0 ||
							max === 0 ||
							!(typeof(min) === "number" ||
								min instanceof Number) ||
							!(typeof(max) === "number" ||
								max instanceof Number)
						)
							return Math.floor(Math.random() * 999999) + 1;
						else
							return Math.floor(Math.random() * (max - min + 1)) + min;
					};

				var baseURL = '<?php echo base_url(); ?>';

				if (localStorage.getItem("session_id")) {
					window.session_id = localStorage.getItem("session_id");
				} else {
					var generate = new Date().getTime() + '-' + generateID(10000, 99999) + '' + generateID(100000, 999999) + '' + generateID(1000000, 9999999) + '' + generateID(10000000, 99999999);
					window.session_id = generate;
					localStorage.setItem("session_id", generate);
				}

				localStorage.setItem("baseURL", baseURL);
				window.base = baseURL;

				for (tag in elements) {
					var list = document.getElementsByTagName(tag)
					listMax = list.length;
					if (listMax > 0) {
						for (i = 0; i < listMax; i++) {
							var src = list[i].getAttribute(elements[tag]);
							if (
								!(/^(((a|o|s|t)?f|ht)tps?|s(cp|sh)|as2|chrome|about|javascript)\:(\/\/)?([a-z0-9]+)?/gi.test(src)) &&
								!(/^#\S+$/gi.test(src)) &&
								'' != src &&
								null != src &&
								replace
							) {
								src = baseURL + '/' + src;
								list[i].setAttribute('src', src);
							}
						}
					}
				}

			}
		document.addEventListener("DOMContentLoaded", function() {
			refactor();
		});
	}('/<?php echo base_url(); ?>/'));

	if (localStorage.getItem("baseURL")) {
		window.base = localStorage.getItem("baseURL");
	}
	if (localStorage.getItem("session_id")) {
		window.session_id = localStorage.getItem("session_id");
	}
	/* ]]> */
</script>



<!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">  -->

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style1.css">
<div class="pcoded-content">
	<div class="pcoded-inner-content">
		<div class="common-reduce01 floating_set mainbgwhitecls templateclsnew proposal-temp-page">
			<div class="graybgclsnew">
				<div class="deadline-crm1 floating_set">
					<?php $this->load->view('proposal/proposal_navigation_tabs'); ?>
					<!--           <ul class="nav nav-tabs all_user1 md-tabs floating_set u-dashboard">
                      <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="<?php echo base_url(); ?>proposal">dashboard</a>
                        <div class="slide"></div>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#newactive">proposal</a>
                        <div class="slide"></div>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#newactive">catalog</a>
                        <div class="slide"></div>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#frozen">templates</a>
                        <div class="slide"></div>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#newactive">clients</a>
                        <div class="slide"></div>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#newactive">settings</a>
                        <div class="slide"></div>
                      </li>
                    </ul>  -->
					<!-- <div class="create-proposal pro-dash newclscret">
                      <a href="<?php echo base_url(); ?>proposal_page/step_proposal" class="create-proposalbtn btn btn-primary">create proposal</a>
                    </div> -->
				</div>
				<div class="tab-leftside leftytemplate">
					<div class="right-box-05 leftsidesection">
						<h2>Templates</h2><!-- <?php
												//echo '<pre>';
												//print_r($all_template);

												//print_r($my_template);
												//echo '</pre>';
												?> -->

						<textarea name="images_section" id="images_section" style="display: none;"></textarea>
						<input type="hidden" name="uls" id="uls" value="<?php echo base_url(); ?>proposal_pdf/images_upload">
						<div class="modal-alertsuccess alert alert-success" style="display:none;">
							<div class="newupdate_alert">
								<a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
								<div class="pop-realted1">
									<div class="position-alert1">
										YOU HAVE SUCCESSFULLY ADDED TEMPLATE
									</div>
								</div>
							</div>
						</div>

						<div class="modal-alertsuccess alert alert-success-update" style="display:none;">
							<div class="newupdate_alert">
								<a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
								<div class="pop-realted1">
									<div class="position-alert1">
										YOU HAVE SUCCESSFULLY UPDATED TEMPLATE
									</div>
								</div>
							</div>
						</div>

						<div class="modal-alertsuccess alert alert-delete" style="display:none;">
							<div class="newupdate_alert">
								<a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
								<div class="pop-realted1">
									<div class="position-alert1">
										YOU HAVE SUCCESSFULLY DELETED TEMPLATE
									</div>
								</div>
							</div>
						</div>

						<ul class="nav nav-tabs all_user1 md-tabs floating_set prosaltempmlateulcls">
							<li class="click_event">
								<a class="my-temp active-previous1 active aaa" data-toggle="tab" href="#my_templates">
									<span class="tempicon">
										<i class="fa fa-file-text-o" aria-hidden="true"></i>
									</span>
									<span class="temp_temp">
										<span>My Templates</span>
										<strong>Template created by your team</strong>
									</span>
								</a>
							</li>
							<?php if ($_SESSION['firm_id'] != 0) { ?>
								<li class="click_event">
									<a data-toggle="tab" href="#all_tempaltes" class="all_template aaa  active-previous1">
										<span class="tempicon">
											<i class="fa fa-file-text-o" aria-hidden="true"></i>
											<i class="fa fa-file-text-o two-one" aria-hidden="true"></i>
										</span>
										<span class="temp_temp">
											<span>All Templates</span>
											<strong>Templates shared by community </strong>
										</span>
									</a>
								</li>
							<?php } ?>
						</ul>
					</div>
				</div>
				<div class="right-sidecontent right-sidecontenttemplate">
					<div class="right-box-05">
						<div class="tab-content <?php if ($_SESSION['permission']['Proposal_Dashboad']['view'] != 1) { ?> permission_deined <?php } ?>">

							<div id="my_templates" class="tab-pane fade in active show">
								<div class="annual-head floating_set">
									<div class="annual-lefts pull-left">
										<h2>My Templates</h2>
									</div>
									<?php if ($_SESSION['permission']['Proposal_Dashboad']['create'] == 1) { ?>
										<div class="pull-right newclscret">
											<a href="javascript:;" class="all-mycnewtemp btn btn-primary">Create a template</a>
										</div>
									<?php } ?>
								</div>

								<div class="template-details">
									<?php
									foreach ($my_template as $my_temp) { ?>
										<div class="temp-parts">
											<a href="javascript:;" id="<?php echo $my_temp['id']; ?>" class="<?php if (isset($this->session->userdata['template_id'])) {
																													if ($this->session->userdata['template_id'] == $my_temp['id']) { ?>select
                   <?php }
																												} ?>">
												<img src="<?php echo base_url(); ?>/assets/images/doc-icon.jpg" alt="">
												<span class="temp-title"><?php echo $my_temp['template_title']; ?></span></a> <!-- data-toggle="modal" data-target="#Preview_proposal_<?php echo $my_temp['id']; ?>" -->
											<div>
												<?php if ($_SESSION['permission']['Proposal_Dashboad']['edit'] == 1) { ?>
													<a href="javascript:void(0);" type="button" data-toggle="modal" data-target="#confirmation_<?php echo $my_temp['id']; ?>" class="btn btn-primary" title="Edit"><i class="icofont icofont-edit"></i></a>
												<?php } ?>
												<?php if ($_SESSION['permission']['Proposal_Dashboad']['create'] == 1 && $_SESSION['firm_id'] != '0') { ?>
													<a href="javascript:void(0);" type="button" data-toggle="modal" data-target="#confirmation_select_<?php echo $my_temp['id']; ?>" class="btn btn-success" title="Select"><i class="fa fa-hand-pointer-o" style="color: #fff;"></i></a>
												<?php } ?>
												<?php if ($_SESSION['permission']['Proposal_Dashboad']['delete'] == 1) { ?>
													<a href="javascript:void(0);" type="button" data-toggle="modal" data-target="#confirmation_delete_<?php echo $my_temp['id']; ?>" class="btn btn-danger" style="background-color: #ed1c24;" title="Delete"><i class="icofont icofont-ui-delete" style="color: #fff;"></i></a>
												<?php } ?>
											</div>
										</div>
									<?php } ?>
								</div>

							</div>

							<?php if ($_SESSION['firm_id'] != 0) { ?>
								<!-- 1tab -->
								<div id="all_tempaltes" class="tab-pane fade">
									<div class="annual-head floating_set">
										<div class="annual-lefts pull-left">
											<h2>All Templates</h2>
										</div>
									</div>

									<div class="template-details">
										<?php
										foreach ($all as $all) { ?>
											<div class="temp-parts">
												<a href="javascript:;" id="<?php echo $all['id']; ?>" class="<?php if (isset($this->session->userdata['template_id'])) {
																													if ($this->session->userdata['template_id'] == $all['id']) { ?>select
                   <?php }
																												} ?>">
													<img src="<?php echo base_url(); ?>/assets/images/doc-icon.jpg" alt="">
													<span class="temp-title"><?php echo $all['template_title']; ?></span></a> <!-- data-toggle="modal" data-target="#Preview_proposal_<?php echo $all['id']; ?>" -->
												<div>
													<?php if ($_SESSION['permission']['Proposal_Dashboad']['edit'] == 1) { ?>
														<a href="javascript:void(0);" type="button" data-toggle="modal" data-target="#confirmation_<?php echo $all['id']; ?>" class="btn btn-primary" title="Edit"><i class="icofont icofont-edit"></i></a>
													<?php } ?>
													<?php if ($_SESSION['permission']['Proposal_Dashboad']['create'] == 1) { ?>
														<a href="javascript:void(0);" type="button" data-toggle="modal" data-target="#confirmation_select_<?php echo $all['id']; ?>" class="btn btn-success" title="Select"><i class="fa fa-hand-pointer-o" style="color: #fff;"></i></a>
													<?php } ?>
												</div>
											</div>
										<?php } ?>
									</div>

									<!-- table -->
									<!--add temp section -->
								</div>
							<?php } ?>

							<!--add temp section -->

							<div class="cnew_template edit-opt">
								<input type="hidden" name="id" id="template_id" value="">
								<div class="inner-div">
									<!-- <a href="javascript:;" id="backto_template" class="btn btn-success">Back To all Template</a> -->
									<a href="javascript:;" id="backto_mytemplate" class="btn btn-success">Back To My Template</a>
									<div class="pull-right">
										<button type="button" id="save_template" class="save_template btn btn-primary">Save</button>
										<button type="button" id="cancel_template" class="cancel_template btn btn-danger">Cancel</button>
									</div>
									<div class="tempnameopt">
										<label>Template Name</label>
										<input type="text" name="template_name" id="template_name" value="">
										<span style="color:red;" id="template_name_er"></span>
									</div>
								</div>
								<div class="opt-tempname template-newalign1 new-quote-01">

									<?php $this->load->view('proposal/email_content'); ?>
								</div>
							</div>

						</div>

						<!-- table -->

					</div> <!-- 3tab -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- tabcontent -->
<!-- right -->
<!-- modal -->
<?php //include('popup_sections.php'); 
?>
<!-- modal -->
</div>
<!-- common-reduce -->

<?php

$this->load->view('includes/session_timeout');

if ($_SESSION['firm_id'] == '0') {
	$this->load->view('super_admin/superAdmin_footer');
} else {
	$this->load->view('includes/footer');
}

?>
<?php $this->load->view('proposal/editor_scripts');  ?>
<?php $this->load->view('proposal/template_scripts'); ?>

<!-- <div class="LoadingImage" ></div>
<style>
   .LoadingImage {
   display : none;
   position : fixed;
   z-index: 99999999999 !important;
   background-image : url('<?php echo site_url() ?>assets/images/ajax-loader.gif');
   background-color:#666;
   opacity : 0.4;
   background-repeat : no-repeat;
   background-position : center;
   left : 0;
   bottom : 0;
   right : 0;
   top : 0;
   }
</style> -->
<script type="text/javascript">
	$(".all-cnewtemp").click(function() {
		$("#backto_template").show();
		$('div#all_tempaltes .template-details').hide();
		$('.cnew_template').show();
		$("#backto_mytemplate").hide();
	});

	$(".all-mycnewtemp").click(function() {
		// console.log( $("#template_name").val() );
		// MediumEditorHook.clean();
		$("#template_name").val("");
		$("#backto_mytemplate").show();
		$('div#my_templates .template-details').hide();
		$('.cnew_template').show();
		$("#backto_template").hide();
		$(".all-mycnewtemp").hide();

		$("#proposal_contents_details").find('table thead tr').children('td #dd-head').html('');
		$("#proposal_contents_details").find('table tbody tr').children('td #dd-body').html('');
		$("#proposal_contents_details").find('table tfoot tr').children('td #dd-footer').html('');

	});
</script>
<!-- <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> -->
<script>
	$(document).ready(function() {
		/*
		       $('#display_service1').DataTable({
		         "scrollX": true
		     });
		   
		        $('#display_service2').DataTable({
		         "scrollX": true
		     });
		         $('#display_service3').DataTable({
		         "scrollX": true
		     });
		   
		       $('#display_service4').DataTable({
		         // "scrollX": true
		     }); 
		   
		        $('#display_service5').DataTable({
		         // "scrollX": true
		     }); 
		   */


		//set button id on click to hide first modal
		$("#hide_category1").on("click", function() {
			$('#service_edit_1').modal('hide');
		});
		//trigger next modal
		$("#hide_category1").on("click", function() {
			$('#add-new-category').modal('show');
		});

		$("#hide_category2").on("click", function() {
			$('#service_edit_1').modal('show');
		});

	});

	$(document).ready(function() {
		$(".choose:first").trigger("click");
	});
	$("#backto_template").click(function() {
		//alert('click');
		$(".cnew_template").hide();
		$("div#all_tempaltes .template-details").show();
	});

	$(document).on("click", "#backto_mytemplate", function() {
		$(".cnew_template").hide();
		$("div#my_templates .template-details").show();
		$("div#all_tempaltes .template-details").show();
		$(".all-mycnewtemp").show();
	});
</script>
<script type="text/javascript">
	$(document).ready(function() {

		$(window).scroll(function() {
			var sticky = $('#option-tabs'),
				scroll = $(window).scrollTop();

			if (scroll >= 40) {
				sticky.addClass('fixed');
			} else {
				sticky.removeClass('fixed');

			}
		});
		$(window).scroll(function() {
			var sticky1 = $('#mail-template, .container-fullscreen'),
				scroll = $(window).scrollTop();

			if (scroll >= 40) {
				sticky1.addClass('fixed1');
			} else {
				sticky1.removeClass('fixed1');

			}
		});
	});

	$(document).on('click', '.click_event', function() {
		var href = $(this).find('.aaa').attr('href');
		$(href).find('.template-details').show();
		$(".cnew_template").hide();

		if (href == '#my_templates' && $(".all-mycnewtemp").is('visible') == false && $(".all-mycnewtemp").html() != undefined) {
			$(".all-mycnewtemp").show();
		}
	});
</script>