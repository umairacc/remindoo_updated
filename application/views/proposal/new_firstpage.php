<!-- <html>
<head>
  <style>
    @page 
    { 
      margin: 150px 15px; 
    }

@media print 
{ 
  p
  { 
    text-align: justify;
    display: block;    
  }
 
}

  </style>
</head>
<body>
<div style="margin-bottom: 20% !important;">
 <h2 style="text-align: center;"><center><?php echo $proposal_name; ?></center></h2>
</div>
 <div>
 <p>Prepared For</p>
   <ul style="list-style-type: none;">
    <li><p><?php echo $client_name; ?><p></li>
  </ul>
  <p>Created By</p>
  <?php $rec = $this->Common_mdl->select_record('firm','firm_id',$_SESSION['firm_id']); ?>
   <ul style="list-style-type: none;"> 
    <li><p><?php echo $rec['crm_company_name']; ?><p></li>
    <li><p><?php echo $rec['sender_address']; ?><p></li>
    <li><p><?php echo $rec['firm_mailid']; ?><p></li>
    <li><p><?php echo base_url(); ?><p></li>
  </ul>
</div>
</body>
</html> -->

<!DOCTYPE html>
<html>
<head>
  <style type="text/css">
@import url('https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900&display=swap');

@import url('https://fonts.googleapis.com/css?family=Lato:300,400,700,900&display=swap');

@import url('https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap');
@import url('https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900&display=swap');


/*
font-family: 'Poppins', sans-serif;
font-family: 'Playfair Display', serif;
font-family: 'Lato', sans-serif;
font-family: 'Montserrat', sans-serif;

*/


h1,h2,h3{
  font-family: 'Lato', sans-serif;

}
body{
  font-family: 'Poppins', sans-serif;
}
.header-top{
  padding-bottom: 10px;
  position: fixed;
  top: -45px;
  left: -45px;
  width: 100%;
}
body{
  padding-top: 100px;
}
.account-tax{
  width: 200px;
  height: 70px;
  display: table;
  margin:0 auto;
  position: relative;
}
.pdf-image{
  display: table-cell;
  text-align: center;
  vertical-align: middle;
}
.pdf-title{
  padding:220px 30px 15px;
  text-align: center;
}
.prepared{
  margin: 70px 0 0;
  color: #777;
}
.prepared {
    text-align: right;
    float: right;
    padding-right: 0;
}
.inline-table{
  max-width: 540px;
    text-align: left;
}
.pdf-title h2 {
    margin-bottom: 50px;
    font-size: 25px;
    font-weight: 600;
    color: #000;
}
p{
  font-size: 14px;
  line-height: 18px;
  margin: 6px 0;
}
.first-blog h3 {
    margin-bottom: 8px;
    color: #333;
}
.first-blog  a{
  color: #333;
}
.first-blog p{
  margin: 3px 0;
  padding-left: 25px;
}


.pdf-table h2 {
    font-weight: 600;
    font-size: 22px;
    margin: 0 0 12px;
}

.table-scroll {
    border: 2px solid #e3e0e1;
}

span.service-title {
    background: #d8d8d8;
    width: 100%;
    display: block;
    padding: 10px 15px;
    font-weight: 600;
    color: #333;
}

table {
    width: 100%;
}
</style>
</head>
<body>
<?php ?>

<div class="pages" style="page-break-after: always;">
<div class="top-blueline header-top ">
  <img src="http://remindoo.co/assets/images/top_line-blue.png" alt="img">
</div>

<div class="account-tax">
  <div class="pdf-image">
   <?php // echo firm_mail_header()  ?>

    <img src="http://remindoo.co/assets/images/newpdf1.png" alt="img">


  </div>
</div>

<div class="pdf-title">
  <h2>Letter of Engagement</h2>
  <div class="prepared">
    <div class="inline-table"><div class="first-blog">
      <h3>Prepared for</h3>
      <p>Mahroof pulpadan</p>
      <p>AL SEMAT UK LIMITED</p>
    </div>
    <div class="first-blog">
      <h3>Created By</h3>
      <p>ACCOTAX Accountants & Tax Consultants</p>
      <p>Accountax Limited</p>  
      <p>0223441258</p>
      <p>info@accotax.co.uk</p>
      <p><a href="#">http://www.accotax.co.il</a></p>
    </div>
  </div>
  </div>
</div>
</div>

</body>
</html>