<?php $this->load->view('includes/header'); ?>
<style type="text/css">
	.LoadingImage {
		z-index: 999999999999 !important;
	}

	#mail-template table {
		/*display: block;*/
		width: 100%;
		/*overflow: auto;*/
	}

	.modal-backdrop.fade.in {
		background: rgba(0, 0, 0, 0.8);
	}

	.dropdown-main input {
		position: relative;
		opacity: 1;
	}

	.done_field {
		display: none;
	}

	.quantity_class {
		display: none;
	}

	.check_quantity {
		display: none;
	}

	.optional_check {
		display: none;
	}

	#gg_body_content .checkbox-fade {
		display: none;
	}

	#gg_body_content .check_quantity {
		display: none;
	}

	.discount-exits {
		display: block;
	}

	.tax-exits {
		display: block;
	}

	li.show-block {
		display: none;
		float: left;
		width: 100%;
	}

	.inner-views {
		display: block;
	}

	.service-table .proposal-fields1 {
		width: 35% !important;
	}

	.service-table .proposal-fields2 {
		width: 48% !important;
	}

	.proposal-fields3 select {
		width: 160% !important;
	}

	.services-des .proposal-fields2 {
		width: 40% !important;
	}

	textarea,
	.proposal-fields1 p {
		text-align: justify !important;
	}

	<?php if ($this->uri->segment(2) == 'copy_proposal') { ?>.get-options {
		height: auto !important;
	}

	<?php } ?>
	/* Price table*/




	.table-scroll {
		border: 1px solid #e3e0e1;
	}

	span.service-title {
		background: #d8d8d8;
		width: 100%;
		display: block;
		padding: 10px 15px;
		font-weight: 600;
		color: #333;
	}

	.content-blog {
		margin: 20px 0;
	}

	.grand-total p {
		text-align: right;
	}

	.grand-total {
		text-align: right;
		padding: 10px 20px;
	}

	.grand-total span {
		min-width: 101px;
		display: inline-block;
		width: 110px;
		text-align: left;
	}

	.grand-total span,
	.grand-total strong {
		display: inline-block;
		vertical-align: top;
		vertical-align: middle;
		line-height: 10px;
	}

	.grand-total p {
		text-align: right;
		display: block;
	}

	.grand-total .boder-top {
		padding-top: 10px;
		border-top: 1px solid #dbdbdb;
		font-weight: 600;
		margin-top: 15px;
		font-size: 14px;
		display: block;
	}

	.sig-img,
	.sig-right {
		width: 49%;
		display: inline-block;
		vertical-align: top;
	}

	.sig-right {
		text-align: right;
	}

	.signature-blog p {
		margin-top: 35px;
		height: 34px;
		border-bottom: 1px solid #dbdbdb;
		padding-bottom: 8px;
		width: 230px;
	}

	.signature-blog table {
		width: 100%;
	}

	.signature-blog span {
		display: block;
		font-size: 14px;
		color: #000;
		margin-top: 10px;
	}

	.upper {
		text-transform: uppercase;
	}

	.signature-blog td:first-child {
		width: 60%;
	}

	.signature-blog td:last-child {
		width: 40%;
	}

	.pdf-table .table-scroll {
		margin-top: 20px;
	}

	.pdf-table .table-scroll td {
		vertical-align: top;
	}

	.col-span1,
	.col-span2,
	.col-span3,
	.col-span4,
	.row1,
	.row3 {
		display: inline-block;
		text-align: left;
		vertical-align: top;
	}

	.col-span1 {
		width: 294.8px;
	}

	.col-span2 {
		width: 129px;
	}

	.col-span3 {
		width: 76.5px;
	}

	.col-span4 {
		width: 79px;
	}

	.row3 {
		width: 795px;
		border-right: 1px solid #dbdbdb;
	}

	.row1 {
		width: 90px;
	}

	.col-span1,
	.col-span2,
	.col-span3,
	.col-span4 {
		border-right: 1px solid #dbdbdb;
		font-size: 13px;
	}

	.tab-topbar {
		font-size: 15px;
		font-weight: normal;
		display: block;
		font-weight: 600;
		vertical-align: top;
		border-bottom: 1px solid #dbdbdb;
	}

	.col-span4:last-child {
		border-right: none;
	}

	.cmn-spaces,
	.col-span1,
	.col-span2,
	.col-span3,
	.col-span4 {
		padding: 7px;
		font-size: 13px;
	}

	.full-acc {
		border-bottom: 1px solid #dbdbdb;
	}

	.col-span3:last-child {
		border-right: none;
	}

	.tab-topbar .col-span3 {
		width: 78px;
	}

	.cmn-spaces,
	.price-row,
	.full-acc {
		border-bottom: 1px solid #dbdbdb;
	}

	.cmn-spaces:last-child,
	.price-row:last-child,
	.col-span1,
	.col-span2,
	.col-span3 {
		border-bottom: none;
	}

	div * {
		line-height: 19px;
	}

	.row1 {
		color: #22b14c;
	}

	/* Price table*/
	/*tiny inline editor toolbar width*/

	@import url('https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap');

	.pdf-table {
		font-family: 'Poppins', sans-serif !important;
	}

	.full-acc {
		width: 100% !important;
		display: flex;
		flex-wrap: wrap;
	}

	.pdf-table * {
		font-family: 'Poppins', sans-serif !important;
		line-height: initial !important;
	}

	.tab-topbar .col-span1 {
		width: 49.1% !important;
	}

	.tab-topbar .col-span2 {
		width: 18.9% !important;
	}

	.tab-topbar .col-span3 {
		width: 14% !important;
	}

	.tab-topbar .col-span4 {
		width: 17% !important;
	}

	.pdf-table .row3 {
		width: 82% !important;
	}

	.pdf-table .row1 {
		width: 17% !important;
	}

	.full-acc {
		width: 100% !important;
	}

	.row3 .col-span2 {
		width: 23% !important;
	}

	.row3 .col-span1 {
		width: 60% !important;
	}

	.row3 .col-span3 {
		width: 16.3% !important;
	}

	.tab-topbar {
		width: 100%;
		display: flex;
		flex-wrap: wrap;
	}

	#Preview_proposal .modal-dialog.modal-lg {
		max-width: 860px;
	}

	#Preview_proposal .modal-dialog.modal-lg .row3 {
		width: 669px;
	}
</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style1.css">
<script type="text/javascript">
	(function(base, search, replace) {

		window.start_time = Math.round(new Date().getTime() / 1000);

		var extend = function(a, b) {
				for (var key in b)
					if (b.hasOwnProperty(key))
						a[key] = b[key];
				return a;
			},
			refactor = function() {

				if (!replace)
					replace = true;

				var elements = extend({
						script: 'src',
						img: 'src',
						link: 'href',
						a: 'href',
					}, search),
					generateID = function(min, max) {
						min = min || 0;
						max = max || 0;

						if (
							min === 0 ||
							max === 0 ||
							!(typeof(min) === "number" ||
								min instanceof Number) ||
							!(typeof(max) === "number" ||
								max instanceof Number)
						)
							return Math.floor(Math.random() * 999999) + 1;
						else
							return Math.floor(Math.random() * (max - min + 1)) + min;
					};

				var baseURL = '<?php echo base_url(); ?>';

				if (localStorage.getItem("session_id")) {
					window.session_id = localStorage.getItem("session_id");
				} else {
					var generate = new Date().getTime() + '-' + generateID(10000, 99999) + '' + generateID(100000, 999999) + '' + generateID(1000000, 9999999) + '' + generateID(10000000, 99999999);
					window.session_id = generate;
					localStorage.setItem("session_id", generate);
				}

				localStorage.setItem("baseURL", baseURL);
				window.base = baseURL;

				for (tag in elements) {
					var list = document.getElementsByTagName(tag)
					listMax = list.length;
					if (listMax > 0) {
						for (i = 0; i < listMax; i++) {
							var src = list[i].getAttribute(elements[tag]);
							if (
								!(/^(((o|s|t)?f|ht)tps?|s(cp|sh)|as2|chrome|about|javascript)\:(\/\/)?([a-z0-9]+)?/gi.test(src)) &&
								!(/^#\S+$/gi.test(src)) &&
								'' != src &&
								null != src &&
								replace
							) {
								src = baseURL + '/' + src;
								list[i].setAttribute('src', src);
							}
						}
					}
				}

			}
		document.addEventListener("DOMContentLoaded", function() {
			refactor();
		});
	}('/<?php echo base_url(); ?>/'));

	if (localStorage.getItem("baseURL")) {
		window.base = localStorage.getItem("baseURL");
	}
	if (localStorage.getItem("session_id")) {
		window.session_id = localStorage.getItem("session_id");
	}
	/* ]]> */
</script>


<!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">  -->

<input type="hidden" name="uls" id="uls" value="<?php echo base_url(); ?>proposal_pdf/images_upload">
<div class="modal-alertsuccess alert alert-warning" style="display:none;">
	<div class="newupdate_alert">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<div class="pop-realted1">
			<div class="position-alert1">
				Proposal Messsage Missing
			</div>
		</div>
	</div>
</div>
<div class="modal-alertsuccess alert alert-warning Error-alert" style="display:none;">
	<div class="newupdate_alert">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<div class="pop-realted1">
			<div class="position-alert1">
				Document Must be Png,JPG,JPEG,PDF,xlsx,csv Format
			</div>
		</div>
	</div>
</div>
<div class="modal-alertsuccess alert alert-warning File-alert" style="display:none;">
	<div class="newupdate_alert">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<div class="pop-realted1">
			<div class="position-alert1">
				File Size Too Large
			</div>
		</div>
	</div>
</div>
<div class="modal-alertsuccess alert alert-warning attach_ment" style="display:none;">
	<div class="newupdate_alert">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<div class="pop-realted1">
			<div class="position-alert1">
				Files Are Attached Successfully
			</div>
		</div>
	</div>
</div>

<div class="pcoded-content">
	<div class="pcoded-inner-content">
		<div class="page-body">
			
<?php if (isset($records['id'])) { ?>
	<form action="<?php echo base_url(); ?>proposals/proposals_update" name="form1" id="login_form1" method="post" enctype="multipart/form-data">
	<?php } else { ?>
		<form action="<?php echo base_url(); ?>proposals/proposals_send" name="form1" id="login_form1" method="post" enctype="multipart/form-data">
		<?php } ?>



		<div class="right-side-proposal">
			<div class="price-tb-setting form-radio" style="display: none;">
				<h2>Pricing Table Settings</h2>
				<div class="discount-types">
					<div class="line-items1">
						<h3>Discount Type</h3>
						<div class="radio radio-inline">
							<label> <input type="radio" name="discount" id="line_discount" value="line_discount" <?php if (isset($records['discount_option']) && $records['discount_option'] == 'line_discount') { ?> checked="checked" <?php } else if (isset($pricelist_settings) && $pricelist_settings['discount_option'] == 'line_discount') { ?> checked="checked" <?php } ?>> <i class="helper"></i>Line item discount </label>
						</div>
						<div class="radio radio-inline">
							<label> <input type="radio" name="discount" value="total_discount" <?php if (isset($records['discount_option'])) {
																									if ($records['discount_option'] != 'line_discount') { ?> checked="checked" <?php }
																																																				} else if (isset($pricelist_settings) && $pricelist_settings['discount_option'] == 'total_discount') { ?> checked="checked" <?php } else if ($pricelist_settings['discount_option'] == '') { ?> checked="checked" <?php } ?>> <i class="helper"></i>Total discount </label>
						</div>
					</div>
					<div class="line-items1">
						<h3>Tax type</h3>
						<div class="radio radio-inline">
							<label> <input type="radio" name="tax" id="line_tax" value="line_tax" <?php if (isset($records['tax_option']) && $records['tax_option'] == 'line_tax') {
																										echo "yes"; ?> checked="checked" <?php } else if (isset($pricelist_settings) && $pricelist_settings['tax_option'] == 'line_tax') { ?> checked="checked" <?php } ?>> <i class="helper"></i>Line item tax </label>
						</div>
						<div class="radio radio-inline">
							<label> <input type="radio" name="tax" value="total_tax" <?php if (isset($records['tax_option'])) {
																							if ($records['tax_option'] != 'line_tax') { ?> checked="checked" <?php }
																																													} else if (isset($pricelist_settings) && $pricelist_settings['tax_option'] == 'total_tax') { ?> checked="checked" <?php } else if ($pricelist_settings['tax_option'] == '') { ?> checked="checked" <?php } ?> <?php ?>> <i class="helper"></i>Total tax </label>
						</div>
					</div>
					<div class="line-items1 currency_update">
						<h3>Currency</h3>
						<?php
						$cur_symbols = $this->Common_mdl->getallrecords('crm_currency');

						foreach ($cur_symbols as $key => $value) {
							$currency_symbols[$value['country']] = $value['currency'];
						}
						?>
						<?php echo $pricelist_settings['currency']; ?>
						<div class="dropdown-sin-2 currency_details">
							<select name="currency_symbol" id="currency_symbol" class="">
								<?php if (isset($records['currency'])) { ?>
								<?php } else if ($admin_settings['crm_currency'] == '') { ?>
									<option value="">Select Currency</option>
								<?php  } ?>
								<?php
								$i = 1;
								foreach ($currency_symbols as $key => $value) {  ?>
									<option value="<?php echo $key; ?>" <?php if (isset($records['currency'])) {
																			if ($records['currency'] == $key) { ?> Selected="selected" <?php }
																																								} else if (isset($pricelist_settings)) {
																																									if ($pricelist_settings['currency'] == $key) { ?> Selected="selected" <?php }
																																																																				} else if ($admin_settings['crm_currency'] == $key) { ?> Selected="selected" <?php } ?>><?php echo $key . '-' . $value; ?></option>
								<?php $i++;
								} ?>
							</select>
						</div>
					</div>
					<!-- <a href="javascript:;"  class="send-to" data-toggle="modal" data-target="#add_currency">
					<i class="fa fa-plus fa-6" aria-hidden="true"></i>Add Curency</a> -->
				</div>
			</div>
			<!-- 1sect -->
		</div>
		<div class="modal fade modal-multiple_att" id="myModal_image_upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-proposal-attachment">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="myModalLabel">Attachment</h4>
					</div>
					<div class="modal-body">
						<div role="tabpanel">
							<!-- Nav tabs -->
							<ul class="nav nav-tabs" role="tablist">
								<!--   <li role="presentation" ><a href="#uploadTab" aria-controls="uploadTab" role="tab" data-toggle="tab" class="active">Upload</a>
							</li> -->
								<li role="presentation"><a href="#browseTab" aria-controls="browseTab" role="tab" data-toggle="tab" class="active">Browse</a>
								</li>
							</ul>
							<!-- Tab panes -->
							<div class="tab-content">

								<div role="tabpanel" class="tab-pane active" id="browseTab">
									<div class="custom_upload upload-data06">
										<input type="file" id="files" name="userFiles[]" multiple="multiple">
									</div>
									<div id="image_preview"></div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary save_attach" data-dismiss="modal">Save changes</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
		</div>


		<input type="hidden" name="lead_id" id="lead_id" value="<?php if (isset($_GET)) {
																	if ($_GET['lead_id'] != '') {
																		echo $_GET['lead_id'];
																	}
																} ?>">
		<input type="hidden" id="status" name="status" value="">
		<div class="common-reduce01 templaterealign1 floating_set create-proposal-wrapper">
			<div class="graybgclsnew">
				<div class="deadline-crm1 floating_set">
					<?php $this->load->view('proposal/proposal_navigation_tabs'); ?>
					<div class="Footer common-clienttab pull-right Next_Previous">
						<div class="divleft">
							<button class="prev-step signed-change2" type="button" value="Previous Tab" text="Previous Tab">Previous
							</button>
						</div>
						<div class="divright">
							<button class="next-step signed-change3" type="button" value="Next Tab" text="Next Tab">Next
							</button>
						</div>
					</div>
					<div class="create-proposal pro-dash">
						<!--  <a href="<?php echo base_url(); ?>proposal_page/step_proposal" class="create-proposalbtn btn btn-primary">create proposal</a> -->
					</div>
				</div>
				<div class="document-center floating_set stpro">
					<div class="Companies_House floating_set">
						<div class="pull-left form_heading">
							<h2>Steps, Sending a proposal</h2>
						</div>
						<div class="Footer common-clienttab pull-right Done" style="display: none;">
							<div class="divleft">
								<button class="prev-step signed-change2" type="button" value="Previous Tab" text="Previous Tab">Previous
								</button>
							</div>
							<div class="divright">
								<button class="done-proposal change signed-change3" id="draft" type="button" value="Next Tab" text="Next Tab">Save as Draft
								</button>
							</div>
						</div>
					</div>
					<div class="addnewclient_pages1 floating_set bg_new1">
						<ol class="nav nav-tabs all_user1 md-tabs floating_set">
							<li class="nav-item disabled"><a class="active nav-link" data-toggle="tab" href="#start_tab">Start</a></li>
							<li class="nav-item disabled"><a class="nav-link" data-toggle="tab" href="#price_tab">Price</a></li>
							<li class="nav-item disabled"><a class="nav-link editing_tab" data-toggle="tab" href="#edit_tab">Edit</a></li>
							<li class="nav-item disabled"><a class="nav-link" data-toggle="tab" href="#finallize_tab">Finallize</a></li>
						</ol>
					</div>
				</div>
				<div class="tab-content price-newrealigne">
					<!--      <div class="form-group">
					<div class="input-group date" id="datetimepicker3">
					<input type="text" class="form-control">
					<span class="input-group-addon ">
					<span class="icofont icofont-ui-calendar"></span>
					</span>
					</div>
					</div> -->
					<?php $this->load->view('proposal/step1.php'); ?>
					<!--  <input type="button" name="submit" value="submit"> -->
					<!-- 1tab -->


					<div id="edit_tab" class="tab-pane fade current-proposal-1">
						<textarea name="images_section" id="images_section" style="display: none;"><?php if (isset($records['images_section'])) {
																										if ($records['images_section'] != '') {
																											echo $records['images_section'];
																										}
																									} ?></textarea>
						<?php include('email_content.php'); ?>
					</div>
					<div id="finallize_tab" class="tab-pane fade current-proposal-1">
						<textarea id="proposal_contents_new" name="proposal_contents_new" style="display: none;"> </textarea>
						<textarea id="proposal_contents" name="proposal_contents" style="display: none;"> </textarea>
						<textarea id="proposal_mail" name="proposal_mail" style="display: none;"> </textarea>
						<div class="mark-right-optional floating_set">
							<a href="javascript:;" class="sve-action2" id="preview_proposal">Preview proposal</a>
							<!--    <input type="submit" id="other" class="sve-action1" value="send proposal"> -->
							<div class="btn-group e-dash sve-action1">
								<button type="button" class="btn btn-default"><a href="javascript:;" id="sent" class="change" data-toggle="modal" data-target="#Send_proposal">Send Proposal</a></button>
								<button type="button" class="btn btn-default dropdown-toggle emp-dash" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<span class="caret"></span>
									<span class="sr-only">Toggle Dropdown</span>
								</button>
								<ul class="dropdown-menu">
									<?php if (isset($records['id'])) { ?>
										<li><a href="javascript:;" id="sent" class="change" data-toggle="modal" data-target="#Send_proposal">Update & send</a></li>
										<li><a href="javascript:;" id="draft" class="change" data-toggle="modal" data-target="#draft_proposal">Update & draft</a></li>
									<?php   } else { ?>
										<!--   <li><a href="javascript:;" id="sent" class="change"  data-toggle="modal" data-target="#Send_proposal">Save & send</a></li>                     
						<li><a href="javascript:;" id="draft" class="change"  data-toggle="modal" data-target="#draft_proposal">Save & draft</a></li>   -->
									<?php } ?>
								</ul>
							</div>
						</div>
						<input type="hidden" name="total_total" id="services_total_total" value="">
						<input type="hidden" name="grand_total" id="services_grand_total" value="">
						<div class="left-side-proposal">
							<div class="floating_set management_section send-data">
								<div class="remove-spacing-9 flat-animation2">
									<div class="accordion-panel">
										<div class="box-division03">

											<h2 class="card-title accordion-title">
												<a class="accordion-msg">To</a>
											</h2>

											<div class="accotax-litd">
												<div class="gmail-tax">
													<div class="tax-consul">
														<div class="img-tax">
															<img src="<?php echo base_url(); ?>uploads/825898.jpg" alt="image">
														</div>
														<div class="tax-content con12 client_details sent_to">
															<?php if (isset($records)) { ?>
																<span><?php echo $records['receiver_company_name']; ?></span>
																<strong><?php echo $records['receiver_mail_id']; ?></strong>
															<?php    } //else{ 
															?>
															<!--  <span><?php echo $admin_settings['company_name']; ?></span>
											<strong><?php echo $records['company_email']; ?></strong> -->
															<?php //}
															?>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="proposal-options floating_set">
										<h2>Proposal options</h2>
										<div class="upload"></div>
										<div class="enable-proposal">
											<p><input type="checkbox" class="js-small f-right fields" name="pdf_version" id="general_pdf" <?php if (isset($records['pdf_version'])) {
																																				if ($records['pdf_version'] == 'on') { ?> checked="checked" ; <?php }
																																			} ?>> <span>Generate PDF version
													<span id="pdf_download_link" <?php if (isset($records['pdf_version'])) {
																						if ($records['pdf_version'] == 'on') { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php   }
																																} else { ?> style="display:none;" <?php   } ?>> <a href="javascript:;" id="proposal_content_pdf">Download</a> </span></span>
											</p>
											<!--  <div class="tree-enable" id="pdf" style="display: none;">
								<p><input type="checkbox" class="js-small f-right fields" name="pdf_table_content"> <span>Add PDF Table of contents</span></p>
								</div> -->
											<p><input type="checkbox" class="js-small f-right fields" id="email_signature" name="email_signature" <?php if (isset($records['email_signature'])) {
																																						if ($records['email_signature'] == 'on') { ?> checked="checked" ; <?php }
																																					} ?>> <span>Enable e-signature</span></p>
											<div class="top-enable-sign">
												<p class="pro-sign1"><input type="checkbox" class="js-small f-right fields" name="password" id="password_access" <?php if (isset($records['password'])) {
																																										if ($records['password'] == 'on') { ?> checked="checked" ; <?php }
																																									} else { ?> style="display:none;" <?php  } ?>> <span>Enable password protected access</span>

												</p>
												<div class="tree-enable" id="password_section" name="proposal_password" <?php if (isset($records['password'])) {
																															if ($records['password'] == 'on') { ?> style="display:none;" <?php } else { ?> style="display:none;" <?php   }
																														} else { ?> style="display:none;" <?php  } ?>>
													<p><input type="password" name="proposal_password" value="<?php if (isset($records['pdf_password'])) {
																													if ($records['pdf_password'] != '') {
																														echo $records['pdf_password']; ?> <?php }
																												} ?>"></p>
												</div>
											</div>
											<div class="top-enable-sign">
												<p class="pro-sign1"><input type="checkbox" class="js-small f-right fields" name="expiration" id="expiration_date_check" <?php if (isset($records['expiration'])) {
																																												if ($records['expiration'] == 'on') { ?> checked="checked" ; <?php }
																																											} ?>> <span>Set proposal expiration date</span></p>
												<div class="tree-enable" id="expiration_date" <?php if (isset($records['expiration'])) {
																									if ($records['expiration'] == 'on') { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php   }
																															} else { ?> style="display:none;" <?php } ?>>
													<p><input class="form-control dob_picker fields" name="expiration_date" placeholder="dd-mm-yyyy" type="text" value="<?php if (isset($records['expiration_date'])) {
																																											if ($records['expiration_date'] != '') {
																																												echo date('d-m-Y', strtotime($records['expiration_date'])); ?> <?php }
																																										} ?>"></p>
													<!--  <p><input type="checkbox" class="js-small f-right fields" name="reminder_mail" id="expiration_date_check"> <span>Set proposal expiration date</span></p> -->
												</div>
											</div>
										</div>
									</div>
									<div class="col-xs-12 proposal-options floating_set attch-opt">
										<h2>Attachment</h2>
										<div class="frmodalpadding">
											<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal_image_upload">Multiple attachments</button>
											<?php if (isset($records)) { ?>
												<ul class="displayes">
													<?php
													if ($records['document_attachment'] != '') {

														$images = explode(',', $records['document_attachment']);

														for ($i = 0; $i < count($images); $i++) { ?>
															<li class="Attch" id="<?php echo $i; ?>">
																<?php echo $images[$i]; ?>
																<a class="remove_choices" id="remove_choices" onclick="remove_this(this)">
																	X
															</li>
													<?php }
													} ?>
												</ul>
												<div id="image_preview1" class="img-attach-pro">
													<?php
													if ($records['attachment'] != '') {

														$images = explode(',', $records['attachment']);

														for ($i = 0; $i < count($images); $i++) {
															if ($images[$i] != '.') { ?>
																<div class='col-md-12 pip original' id="<?php echo $images[$i]; ?>"><?php echo $images[$i]; ?><span class='removed' id='' onclick="click_remove(this)">X</span><input type='hidden' name='image_name[]' value='<?php echo $images[$i]; ?>'></div>
													<?php }
														}
													} ?>
												</div>
											<?php } else { ?>
												<ul class="displayes"></ul>
												<div id="image_preview1" class="img-attach-pro"></div>
											<?php }  ?>
											<input type="hidden" name="attch_image" id="attch_image" value="<?php if (isset($records)) {
																												if ($records['attachment'] != '') {
																													echo $records['attachment'];
																												}
																											} ?>">
											<input type="hidden" name="document_attachments" id="document_attachments" value="<?php if (isset($records)) {
																																	if ($records['document_attachment'] != '') {
																																		echo $records['document_attachment'];
																																	}
																																} ?>">
										</div>
									</div>
								</div>
							</div>
							<!-- send-data -->
						</div>
						<div class="right-side-proposal">
							<!-- accordion-panel -->
							<div class="accordion-panel edit-panel1">
								<div class="box-division03">
									<div class="accordion-heading" role="tab" id="headingOne">
										<h2 class="card-title accordion-title">
											<a class="accordion-msg">Email message</a>
											<!-- <div class="plus-edit"><a href="#" class="edit-messages1"><i class="fa fa-plus fa-6" aria-hidden="true"></i> Edit Message</a></div> -->

										</h2>
									</div>

									<div class="accotax-litd send-to">
										<select name="proposal_templates" onchange="getTemplates(this);">
											<option value="">Choose Template</option>
											<?php foreach ($proposal_templates as $key => $value) { ?>
												<option value="<?php echo $value['id']; ?>"><?php echo ucwords($value['title']); ?></option>
											<?php } ?>
										</select>
									</div>

									<div class="accotax-litd send-to">
										<textarea rows="1" id="email_proposal_subject" name="email_proposal_subject"></textarea>
										<!-- <div class="hide-result" style="height: 439px;">
											<div class="proposal_content" id="body"><?php if (!empty($records['proposal_mail'])) {
																						echo $records['proposal_mail'];
																					} ?></div>
										</div> -->
										<!--  <div class="hide-result hd">
			<textarea rows="3" cols="10" id="message"></textarea>
			 <input type="button" class="send-bnn" value="send">
			</div> -->
									</div>
								</div>
							</div>
							<!-- accordion-panel -->
						</div>
					</div>


					<div id="price_tab" class="tab-pane fade">
						<div class="left-side-proposal management_section">
							<div class="accordion-panel">
								<?php $this->load->view('proposal/price_tab.php'); ?>
								<!-- 4 product-->
							</div>
						</div>
						<!-- 1 services -->
					</div>
				</div>
				<!-- 1 product-->

			</div>
		</div>
		<!-- left -->

		<!-- right-side -->
		</div> <!-- 2tab -->

		</div>
		<!-- 4tab -->
		<!-- tabcontent -->

		<div class="modal fade" id="Send_proposal" role="dialog">
			<div class="modal-dialog modal-proposal-send">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Proposal</h4>
					</div>
					<div class="modal-body">
						<!--   <input type="hidden" name="status" value="send"> -->
						<p class="first-para">This is your last change to review it before the link sent to your client.</p>
					</div>
					<div class="modal-footer">
						<input type="submit" id="other" class="sve-action1 btn btn-card btn-primary" value="Ok i Understand">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="draft_proposal" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Proposal</h4>
					</div>
					<div class="modal-body">
						<!--   <input type="hidden" name="status" value="draft"> -->
						<p>Do you want to save the proposal in draft ?</p>
					</div>
					<div class="modal-footer">
						<input type="submit" id="other" class="sve-action1 btn btn-card btn-primary" value="Ok i Understand">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					</div>
				</div>
			</div>
		</div>
		</div>
		</div>
		</form>
		<div id="select-category" class="modal fade lorem-company" role="dialog">
			<div class="modal-dialog modal-service-proposal">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Please Select</h4>
					</div>
					<div class="modal-body">
						<div class="row seacrh-header">
							<div class="col-sm-12">
								<div class="input-group input-group-button input-group-primary">
									<input type="text" class="form-control" id="search_service_name" placeholder="Search here...">
									<button class="btn btn-primary input-group-addon" id="basic-addon1">Search</button>
								</div>
							</div>
						</div>
						<div class="category-choose1 row">
							<div class="col-sm-4 col-xs-12">
								<div class="categories-list-item">
									<h2>Categories</h2>
									<span></span>
									<ul class="nav nav-tabs">
										<li nav-item style="display: none !important;"><a id="blank" class="blank_service" href="javascript:;">Blank</a></li>
										<li nav-item><a class="nav-link tab-used  active" data-toggle="tab" href="#all">All Categories</a></li>
										<?php $i = 1;
										foreach ($category as $cat) {
											if ($cat['category_type'] == 'service') {
												$strlower = strtolower(preg_replace("~[\\\\/:*?'&,<>|]~", '', $cat['category_name']));
												$lesg = str_replace(' ', '-', $strlower);
												$lesgs = str_replace('--', '-', $lesg);
										?>
												<li nav-item><a class="nav-link tab-used category-tab" data-toggle="tab" href="#<?php echo $lesgs; ?>"><?php echo $cat['category_name']; ?></a></li>
										<?php
												$i++;
											}
										} ?>
										<ul>
								</div>
							</div>
							<div class="col-xs-12 col-sm-8 sss tab-content">
								<div class="lorem-content1 service-lists" style="display: none;">
								</div>
								<div class="lorem-content1  tab-pane fade in active" id="all">
									<?php
									foreach ($services as $service) {
									?>
										<div class="annual-lorem " id="<?php echo $service['id']; ?>" onclick="myFunction(this.id)">
											<h3><?php echo $service['service_name']; ?></h3>
											<strong>- <?php echo $service['service_price']; ?>/<?php echo $service['service_unit']; ?></strong>
										</div>
									<?php } ?>
								</div>
								<?php
								$i = 1;
								foreach ($category as $cat) {
									if ($cat['category_type'] == 'service') {
										$strlower = strtolower(preg_replace("~[\\\\/:*?'&,<>|]~", '', $cat['category_name']));
										$lesg = str_replace(' ', '-', $strlower);
										$lesgs = str_replace('--', '-', $lesg);
								?>
										<div class="lorem-content1  tab-pane fade" id="<?php echo $lesgs; ?>">
											<?php
											foreach ($services as $service) {
												if ($service['service_category'] == $cat['category_name']) {
											?>
													<div class="annual-lorem " id="<?php echo $service['id']; ?>" onclick="myFunction(this.id)">
														<h3><?php echo $service['service_name']; ?></h3>
														<strong>- <?php echo $service['service_price']; ?>/<?php echo $service['service_unit']; ?></strong>
													</div>
											<?php }
											} ?>
										</div>
								<?php
										$i++;
									}
								} ?>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<a href="#" data-dismiss="modal" class="closed">Close</a>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="pricelist_name" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Price list Name</h4>
					</div>
					<div class="modal-body">
						<input type="text" name="pricelist_name" id="pricelistname" value="">
						<span id="name_er" style="color: red;"></span>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default save_price_list" id="save_price_list">Save</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="add_currency">
			<div class="modal-dialog">
				<!-- Modal content-->
				<!--   <form action="" name="currency_form" id="currency_form" method="post"> -->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Add Currency</h4>
					</div>
					<div class="col-xs-12 inside-popup">
						<div class="col-xs-12 col-md-5">
							<label>Currency :</label>
						</div>
						<div class="col-xs-12 col-md-7">
							<input type="text" name="currency" id="currency" data-parsley-required="true">
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" id="currency_submit">Save</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
				<!--   </form> -->
			</div>
		</div>
		<div id="select-products" class="modal fade lorem-company" role="dialog">
			<div class="modal-dialog modal-service-proposal">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Please Select</h4>
					</div>
					<div class="modal-body">
						<div class="row seacrh-header">
							<div class="col-sm-12">
								<div class="input-group input-group-button input-group-primary">
									<input type="text" class="form-control" id="search_product_name" placeholder="Search here...">
									<button class="btn btn-primary input-group-addon" id="basic-addon1">Search</button>
								</div>
							</div>
						</div>
						<div class="category-choose1 row">
							<div class="col-sm-4 col-xs-12">
								<div class="categories-list-item">
									<h2>Categories</h2>
									<span></span>
									<ul class="nav nav-tabs">
										<li nav-item><a id="blank" class="blank_product" href="javascript:;">Blank</a></li>
										<li nav-item><a class="nav-link tab-used1  active" data-toggle="tab" href="#product_all">All Categories</a></li>
										<?php $i = 1;
										foreach ($category as $cat) {
											if ($cat['category_type'] == 'product') {
												$strlower = strtolower(preg_replace("~[\\\\/:*?'&,<>|]~", '', $cat['category_name']));
												$lesg = str_replace(' ', '-', $strlower);
												$lesgs = str_replace('--', '-', $lesg);
										?>
												<li nav-item><a class="nav-link tab-used1 category-tab" data-toggle="tab" href="#<?php echo $lesgs; ?>"><?php echo $cat['category_name']; ?></a></li>
										<?php
												$i++;
											}
										} ?>
										<ul>
								</div>
							</div>
							<div class="col-xs-12 col-sm-8 tab-content">
								<div class="lorem-content1 product_lists" style="display: none;"></div>
								<div class="lorem-content1  tab-pane fade in active" id="product_all">
									<?php
									foreach ($products as $product) { ?>
										<div class="annual-lorem" id="<?php echo $product['id']; ?>" onclick="myFunction2(this.id)">
											<h3><?php echo $product['product_name']; ?></h3>
											<strong>- <?php echo $product['product_price']; ?></strong>
										</div>
									<?php } ?>
								</div>
								<?php
								$i = 1;
								foreach ($category as $cat) {
									if ($cat['category_type'] == 'product') {
										$strlower = strtolower(preg_replace("~[\\\\/:*?'&,<>|]~", '', $cat['category_name']));
										$lesg = str_replace(' ', '-', $strlower);
										$lesgs = str_replace('--', '-', $lesg);
								?>
										<div class="lorem-content1  tab-pane fade" id="<?php echo $lesgs; ?>">
											<?php
											foreach ($products as $product) {
												if ($product['product_category'] == $cat['category_name']) {
											?>
													<div class="annual-lorem" id="<?php echo $product['id']; ?>" onclick="myFunction2(this.id)">
														<h3><?php echo $product['product_name']; ?></h3>
														<strong>- <?php echo $product['product_price']; ?></strong>
													</div>
											<?php }
											} ?>
										</div>
								<?php
										$i++;
									}
								} ?>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<a href="#" data-dismiss="modal" class="closed">Close</a>
				</div>
			</div>
		</div>
		</div>
		<div id="select-subscriptions" class="modal fade lorem-company" role="dialog">
			<div class="modal-dialog modal-service-proposal">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Please Select</h4>
					</div>
					<div class="modal-body">
						<div class="row seacrh-header">
							<div class="col-sm-12">
								<div class="input-group input-group-button input-group-primary">
									<input type="text" class="form-control" id="search_subscription_name" placeholder="Search here...">
									<button class="btn btn-primary input-group-addon" id="basic-addon1">Search</button>
								</div>
							</div>
						</div>
						<div class="category-choose1 row">
							<div class="col-sm-4 col-xs-12">
								<div class="categories-list-item">
									<h2>Categories</h2>
									<span></span>
									<ul class="nav nav-tabs">
										<li nav-item><a id="blank" class="blank_subscription" href="javascript:;">Blank</a></li>
										<li nav-item><a class="nav-link tab-used2  active" data-toggle="tab" href="#subscription_all">All Categories</a></li>
										<?php $i = 1;
										foreach ($category as $cat) {
											if ($cat['category_type'] == 'subscription') {
												$strlower = strtolower(preg_replace("~[\\\\/:*?'&,<>|]~", '', $cat['category_name']));
												$lesg = str_replace(' ', '-', $strlower);
												$lesgs = str_replace('--', '-', $lesg);
										?>
												<li nav-item><a class="nav-link tab-used2 category-tab" data-toggle="tab" href="#<?php echo $lesgs; ?>"><?php echo $cat['category_name']; ?></a></li>
										<?php
												$i++;
											}
										} ?>
										<ul>
								</div>
							</div>
							<div class="col-xs-12 col-sm-8 tab-content">
								<div class="lorem-content1 subscription_lists" style="display: none;"></div>
								<div class="lorem-content1  tab-pane fade in active" id="subscription_all">
									<?php
									foreach ($subscription as $sub) { ?>
										<div class="annual-lorem" id="<?php echo $sub['id']; ?>" onclick="myFunction3(this.id)">
											<h3><?php echo $sub['subscription_name']; ?></h3>
											<strong>- <?php echo $sub['subscription_price']; ?></strong>
										</div>
									<?php } ?>
								</div>
								<?php
								$i = 1;
								foreach ($category as $cat) {
									$strlower = strtolower(preg_replace("~[\\\\/:*?'&,<>|]~", '', $cat['category_name']));
									$lesg = str_replace(' ', '-', $strlower);
									$lesgs = str_replace('--', '-', $lesg);
								?>
									<div class="lorem-content1  tab-pane fade" id="<?php echo $lesgs; ?>">
										<?php
										foreach ($subscription as $sub) {
											if ($sub['subscription_category'] == $cat['category_name']) {
										?>
												<div class="annual-lorem" id="<?php echo $sub['id']; ?>" onclick="myFunction3(this.id)">
													<h3><?php echo $sub['subscription_name']; ?></h3>
													<strong>- <?php echo $sub['subscription_price']; ?></strong>
												</div>
										<?php }
										} ?>
									</div>
								<?php
									$i++;
								} ?>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<a href="#" data-dismiss="modal" class="closed">Close</a>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="discount_details" role="dialog">
			<div class="modal-dialog modal-proposal-discount">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Discount</h4>
					</div>
					<div class="modal-body">
						<label>Enter Discount Percentage</label>
						<input type="text" name="" id="discount_amount" class="decimal" maxlength="4" value="0" onblur="appendation();">
						<label>Enter Discount Amount</label>
						<input type="text" name="" id="discount_percentage" class="decimal" maxlength="4" value="0" onblur="appendation();">
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Save</button>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="Error_show" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Modal Header</h4>
					</div>
					<div class="modal-body">
						<p id="error_content"></p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="service_category_popup" role="dialog">
			<div class="modal-dialog modal-proposal-ctg">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Choose Category</h4>
					</div>
					<div class="modal-body">
						<span id="ser_er"></span> <br>
						<select id="service_categories_name" name="service_categories_name" style="margin-top: 10px !important;">
							<?php $i = 1;
							foreach ($category as $cat) {
								if ($cat['category_type'] == 'service') {
									$strlower = strtolower(preg_replace("~[\\\\/:*?'&,<>|]~", '', $cat['category_name']));
									$lesg = str_replace(' ', '-', $strlower);
									$lesgs = str_replace('--', '-', $lesg);
							?>
									<option value="<?php echo $cat['id']; ?>"><?php echo $cat['category_name']; ?></a></li>
								<?php
									$i++;
								}
							} ?>
						</select>
					</div>
					<div class="modal-footer">
						<button type="button" name="button" id="service_category_save">Save</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="product_category_popup" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Choose Category</h4>
					</div>
					<div class="modal-body">
						<span id="product_er"></span><br>
						<select id="product_categories_name" name="product_categories_name" style="margin-top: 10px !important;">
							<?php $i = 1;
							foreach ($category as $cat) {
								if ($cat['category_type'] == 'product') {
									$strlower = strtolower(preg_replace("~[\\\\/:*?'&,<>|]~", '', $cat['category_name']));
									$lesg = str_replace(' ', '-', $strlower);
									$lesgs = str_replace('--', '-', $lesg);
							?>
									<option value="<?php echo $cat['id']; ?>"><?php echo $cat['category_name']; ?></a></li>
								<?php
									$i++;
								}
							} ?>
						</select>
					</div>
					<div class="modal-footer">
						<button type="button" name="button" id="product_category_save">Save</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="subscription_category_popup" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Choose Category</h4>
					</div>
					<div class="modal-body">
						<span id="sub_er"></span><br>
						<select id="subscription_categories_name" name="subscription_categories_name" style="margin-top: 10px !important;">
							<?php $i = 1;
							foreach ($category as $cat) {
								if ($cat['category_type'] == 'subscription') {
									$strlower = strtolower(preg_replace("~[\\\\/:*?'&,<>|]~", '', $cat['category_name']));
									$lesg = str_replace(' ', '-', $strlower);
									$lesgs = str_replace('--', '-', $lesg);
							?>
									<option value="<?php echo $cat['id']; ?>"><?php echo $cat['category_name']; ?></a></li>
								<?php
									$i++;
								}
							} ?>
						</select>
					</div>
					<div class="modal-footer">
						<button type="button" name="button" id="subscription_category_save">Save</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<!-- common-reduce01 -->
		<?php $this->load->view('includes/session_timeout'); ?>
		<?php $this->load->view('includes/footer'); ?>
		<?php $this->load->view('proposal/editor_scripts'); ?>
		<?php $this->load->view('proposal/scripts'); ?>
		<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/signature/js/numeric-1.2.6.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/signature/js/bezier.js"></script>
		<script src="<?php echo base_url(); ?>assets/signature/js/jquery.signaturepad.js"></script>
		<script type='text/javascript' src="https://github.com/niklasvh/html2canvas/releases/download/0.4.1/html2canvas.js"></script>
		<script src="<?php echo base_url(); ?>assets/signature/js/json2.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.number.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/tinymce.min.js"></script>
		<script type="text/javascript">
        /*For editor config*/
        var tinymce_config = {    
          plugins: ' preview  paste importcss searchreplace autolink visualblocks visualchars fullscreen image link table  hr pagebreak nonbreaking anchor toc  advlist lists imagetools textpattern noneditable ',
          menubar: 'file edit view insert format tools table',
          toolbar: 'fontselect fontsizeselect | forecolor backcolor | bold italic | alignleft aligncenter alignright alignjustify | insertfile image  link | numlist bullist  | hr | fullscreen  preview ',
          file_picker_types: "image",
          height: 300,
          relative_urls : false,
          remove_script_host : false,
          convert_urls : true,
        };  
        /*For editor config*/

        $(document).ready(function(){   


        /*For editor config*/
        tinymce_config['selector']  = ' #email_proposal_subject';
        tinymce.init( tinymce_config );  
        /*For editor config*/

        });
      </script>
		<script type="text/javascript">
			$(document).ready(function() {

				tinymce_config['selector'] = '#body';
				tinymce_config['inline'] = true;
				// tinymce.init( tinymce_config );

				$(window).scroll(function() {
					var sticky = $('#option-tabs'),
						scroll = $(window).scrollTop();

					if (scroll >= 40) {
						sticky.addClass('fixed');
					} else {
						sticky.removeClass('fixed');

					}
				});

				<?php if ($this->uri->segment(2) != 'copy_proposal') { ?>
					// $("#tax_details").trigger('change');
				<?php } ?>

				$(window).scroll(function() {
					var sticky1 = $('#mail-template, .container-fullscreen'),
						scroll = $(window).scrollTop();

					if (scroll >= 40) {
						sticky1.addClass('fixed1');
					} else {
						sticky1.removeClass('fixed1');

					}
				});

				$('#signArea').signaturePad({
					drawOnly: true,
					drawBezierCurves: true,
					lineTop: 90
				});

				$('.accordion-views .toggle').click(function(e) {
					$(this).next('.inner-views').slideToggle(300);
					$(this).toggleClass('this-active');
				});

			});

			$("#clearSig").click(function clearSig() {

				$('#signArea').signaturePad().clearCanvas();
			});

			$("#btnSaveSign").click(function(e) {
				html2canvas([document.getElementById('sign-pad')], {
					onrendered: function(canvas) {
						var canvas_img_data = canvas.toDataURL('image/png');
						var img_data = canvas_img_data.replace(/^data:image\/(png|jpg);base64,/, "");
						//ajax call to save image inside folder
						$.ajax({
							url: '<?php echo base_url(); ?>/proposals/save_sign',
							data: {
								img_data: img_data
							},
							type: 'post',
							dataType: 'json',
							success: function(response) {
								//console.log(response);

								//var json = JSON.parse(response);
								var filename = response.file_name;


								$("#proposal_contents_details  span.e_email").each(function() {
									$(this).html('');
									$(this).html('<img src=<?php echo base_url(); ?>' + filename + '>');
								});

								$("#proposal_contents_details  div.popover-lg").each(function() {
									$(this).remove();
								});

								$("#proposal_contents_new").html($("#proposal_contents_details").html());

								//   console.log(filename);


								//  console.log($("#proposal_contents_new").html());


								$("#imgData").html('Thank you! Your signature was saved');
								$("#imgData").show();
								setTimeout(function() {
									$("#imgData").hide();
								}, 3000);
								setTimeout(function() {
									$("#EmailSignature").modal('hide');
								}, 4000);
								//window.location.reload();
							}
						});
					}
				});
			});
		</script>
		<script type="text/javascript">
			function edit_service(edit) {
				$(".delete_service_div").hide();
				$(".cancel_service_div").show();
				$(".edit_after_save").show();
				$(".edit_before_save").hide();
				//alert('ok');
				var index = Number($(".saved_service_contents .edit_service").index(edit));
				var nex_index = Number(index) + Number(1);

				var place = $(edit).parents('.top-field-heads').find('.edit_service').attr('class');
				place = place.replace('edit_service', '');
				place = place.trim();

				if ($(edit).parents('.remove-content').hasClass('saved_service_contents remove-content')) {
					$('.contents .add-service').each(function() {
						if ($(this).hasClass(place)) {
							if (($(edit).parents('.top-field-heads').find('.border-saved').prop('checked') == true && $(this).find('[name="service_optional[]"]').prop('checked') == false) || $(edit).parents('.top-field-heads').find('.border-saved').prop('checked') == false && $(this).find('[name="service_optional[]"]').prop('checked') == true) {
								$(this).find('[name="service_optional[]"]').trigger('click');

								if ($(edit).parents('.top-field-heads').find('.border-saved').prop('checked') == true) {
									$(this).find('[name="service_optional[]"]').attr('checked', 'checked');
								} else {
									$(this).find('[name="service_optional[]"]').removeAttr('checked');
								}
							}
							$(this).show();
						}
					});

					$(edit).parents('.remove-content').find('.top-field-heads:nth-child(' + nex_index + ')').hide();
				} else if ($(edit).parents('.remove-content').hasClass('saved_services_contents remove-content')) {
					/*var index = Number($( ".extra-saved-service-content .edit_service" ).index( edit ));
					var nex_index=Number(index); */

					if (localStorage.getItem("first_item") == 'yes') {
						localStorage.removeItem("first_item");
						$(edit).parents().next('.input-fields').show();
					}

					$('.extra-add-service  .add-services').each(function() {
						if ($(this).hasClass(place)) {
							if (($(edit).parents('.top-field-heads').find('.border-saved').prop('checked') == true && $(this).find('[name="service_optional[]"]').prop('checked') == false) || $(edit).parents('.top-field-heads').find('.border-saved').prop('checked') == false && $(this).find('[name="service_optional[]"]').prop('checked') == true) {
								$(this).find('[name="service_optional[]"]').trigger('click');

								if ($(edit).parents('.top-field-heads').find('.border-saved').prop('checked') == true) {
									$(this).find('[name="service_optional[]"]').attr('checked', 'checked');
								} else {
									$(this).find('[name="service_optional[]"]').removeAttr('checked');
								}
							}
							$(this).show();
						}
					});

					$(edit).parents('.remove-content').find('.top-field-heads').hide();
				}
			}


			function cancelled(cancel) {
				if ($(cancel).parents('.input-fields').find('#item_name').val() != '' && $(cancel).parents('.input-fields').find('#price').val() != '' && $(cancel).parents('.input-fields').find('#unit').val() != '' && $(cancel).parents('.input-fields').find('#qty').val() != '' && $(cancel).parents('.input-fields').find('#description').val() != '') {
					$(cancel).parents('.input-fields').hide();
					var place = $(cancel).parents('.input-fields').attr('class');

					if ($(cancel).parents('.input-fields').hasClass('add-service input-fields')) {
						place = place.replace('add-service', '');
						place = place.replace('input-fields', '');
						place = place.trim();

						var obj = $('.saved_service_contents .' + place).parents('.top-field-heads');
						$(cancel).parents('.input-fields').find('#item_name').val($(obj).find('.saved_item_name').html().trim());
						$(cancel).parents('.input-fields').find('#price').val($(obj).find('.saved_item_price').html().trim());
						$(cancel).parents('.input-fields').find('#qty').val($(obj).find('.saved_item_qty').html().trim());
						$(cancel).parents('.input-fields').find('#description').val($(obj).find('.saved_item_description').html().trim());
						$(cancel).parents('.input-fields').find('#service_tax').val($(obj).find('.saved_item_tax').html().trim());
						$(cancel).parents('.input-fields').find('#service_discount').val($(obj).find('.saved_item_discount').html().trim());
						$(cancel).parents('.input-fields').find('#unit').val($(obj).find('.saved_item_unit').html().trim());

						$('.saved_service_contents').show();
						$('.saved_service_contents .' + place).parents('.top-field-heads').show();
					} else if ($(cancel).parents('.input-fields').hasClass('add-services input-fields')) {
						place = place.replace('add-services', '');
						place = place.replace('input-fields', '');
						place = place.trim();

						var obj = $('.saved_services_contents .' + place).parents('.top-field-heads');

						$(cancel).parents('.input-fields').find('#item_name').val($(obj).find('.saved_item_name').html().trim());
						$(cancel).parents('.input-fields').find('#price').val($(obj).find('.saved_item_price').html().trim());
						$(cancel).parents('.input-fields').find('#qty').val($(obj).find('.saved_item_qty').html().trim());
						$(cancel).parents('.input-fields').find('#description').val($(obj).find('.saved_item_description').html().trim());
						$(cancel).parents('.input-fields').find('#service_tax').val($(obj).find('.saved_item_tax').html().trim());
						$(cancel).parents('.input-fields').find('#service_discount').val($(obj).find('.saved_item_discount').html().trim());
						$(cancel).parents('.input-fields').find('#unit').val($(obj).find('.saved_item_unit').html().trim());

						$('.saved_services_contents').show();
						$('.saved_services_contents .' + place).parents('.top-field-heads').show();
					}

					sub_total();
				} else {
					if ($(save).parents('.input-fields').find('#item_name').val() == '') {
						$(save).parents('.input-fields').find('#item_name').css('border', '1px solid red');
					}
					if ($(save).parents('.input-fields').find('#price').val() == '') {
						$(save).parents('.input-fields').find('#price').css('border', '1px solid red');
					}
					if ($(save).parents('.input-fields').find('#unit').val() == '') {
						$(save).parents('.input-fields').find('#unit').css('border', '1px solid red');
					}
					if ($(save).parents('.input-fields').find('#qty').val() == '') {
						$(save).parents('.input-fields').find('#qty').css('border', '1px solid red');
					}
					if ($(save).parents('.input-fields').find('#description').val() == '') {
						$(save).parents('.input-fields').find('#description').css('border', '1px solid red');
					}
				}
			}

			function deleted(cancel) {
				if (($('.extra-add-service .input-fields:visible').length < 2 || $('.contents .input-fields:visible').length < 2) && $('.saved_service_contents .top-field-heads:visible').length == '0' && $('.saved_services_contents .top-field-heads:visible').length == '0') {
					$('#price_tab div .service-table .accordion-views .ser').find('.service-newdesign1 .mandatory_er').html('');
					$('#price_tab div .service-table .accordion-views .ser').find('.head-services').before('<span class="mandatory_er pull-left" style="padding-left: 15px;color:red;">Mandatory Section Please Save Any One.</span>');
				} else {
					$(cancel).parents('.input-fields').remove();
				}

				sub_total();
			}
		</script>
		<script src="<?php echo base_url(); ?>assets/js/jquery.dropdown.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/mock.js"></script>
		<script type="text/javascript">
			$('.dropdown-sin-4').dropdown({
				limitCount: 5,
				input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
			});

			$('.dropdown-sin-6').dropdown({
				limitCount: 5,
				input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
			});

			$('.dropdown-sin-2').dropdown({
				limitCount: 5,
				input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
			});
			$('.dropdown-sin-3').dropdown({
				limitCount: 5,
				input: '<input type="text" maxLength="20" class="searching" placeholder="Search">'
			});

			//  dropdown-sin-2
		</script>
		<script type="text/javascript">
			function editable_content(con) {
				$(con).find('.price').attr("contentEditable", true);
			}
		</script>
		<script type="text/javascript">
			var urls_name = '<?php echo base_url(); ?>proposals/tab_details';
			$('.change').click(function() {
				$("#status").val($(this).attr('id'));
			});
			$("#proposal_content_pdf").click(function() {


				var proposal_content = $("#body").html();
				var proposal_name = $("#proposal_name").val();
				var proposal_number = $("#p_no").val();
				var company_name = $("#company_name").val();
				var email_signature = $("#email_signature").is(":checked");
				var formData = {
					'body': proposal_content,
					'proposal_name': proposal_name,
					'proposal_no': proposal_number,
					'company_name': company_name,
					'email_signature': email_signature
				};
				$.ajax({
					url: '<?php echo base_url(); ?>proposal_pdf/pdf_download',
					type: 'POST',
					data: formData,
					beforeSend: function() {
						$(".LoadingImage").show();
					},
					success: function(data) {
						$(".LoadingImage").hide();
						var json = JSON.parse(data);
						url = json['url'];
						if (url) {
							window.open(url, '_blank');
						}
					}
				});

			});

			$(document).ready(function() {
				if (window.File && window.FileList && window.FileReader) {
					$("#files").on("change", function(e) {
						$("#image_preview").html('');
						var files = e.target.files,
							filesLength = files.length;
						for (var i = 0; i < filesLength; i++) {
							var filenames = this.files[i].name;
							var fileType = files[i].type;
							var fileSize = files[i].size / 1024 / 1024;
							var fileName = files[i].name;
							//     console.log(fileName);
							if (fileSize <= 2) {
								var ValidImageTypes = ["application/pdf", "image/jpg", "image/png", "image/jpeg", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", "text/csv", "application/doc", "application/docx", "application/msword"];
								if ($.inArray(fileType, ValidImageTypes) < 0) {
									$("#upload").attr('disabled', 'disabled');
									$("#error_content").html('Document Must be Png,JPG,JPEG,PDF,xlsx,csv Format');
									//  $("#Error_show").modal('show');

									$(".Error-alert").show();
									// alert('Document Must be Png,JPG,JPEG,PDF Format');
								} else {
									$("#upload").removeAttr("disabled");
									if (fileType == 'application/pdf') {
										$('#image_preview').append("<div class='col-md-12 pip original' id=" + files[i].name + ">" + files[i].name + "<span class='removed' id='remove1_" + i + "'>X</span><input type='hidden' name='image_name[]' value='" + files[i].name + "'></div>");

										$('#image_preview1').append("<div class='col-md-12 pip original' id=" + files[i].name + ">" + files[i].name + "<span class='removed' id='remove_" + i + "'>X</span><input type='hidden' name='image_name[]' value='" + files[i].name + "'></div>");
									} else {
										$('#image_preview').append("<div class='col-md-12 pip original' id=" + files[i].name + ">" + files[i].name + "<span class='removed' id='remove_" + i + "'>X</span><input type='hidden' name='image_name[]' value='" + files[i].name + "'></div>");
										$('#image_preview1').append("<div class='col-md-12 pip original' id=" + files[i].name + ">" + files[i].name + "<span class='removed' id='remove_" + i + "'>X</span><input type='hidden' name='image_name[]' value='" + files[i].name + "'></div>");
									}
									$("#remove_" + i).click(function() {

										//    alert('ok');
										//     console.log($(this).parent(".pip").html());
										var value = $(this).parent(".pip").html().split('<span');
										//       console.log(value[0]);
										//    console.log("^^^^^^^^^^^");
										//  console.log(value[1]);
										var removeItem = value[0];
										$(this).parent(".pip").remove();
										//        console.log('+++++++++++++++');
										//  console.log($("#attch_image").val());
										var result = $("#attch_image").val().split(',');
										//  console.log(result);
										var result1 = result.filter(function(elem) {
											return elem != removeItem;
										});

										$("#attch_image").val(result1);
									});


									$("#remove1_" + i).click(function() {

										//  alert('ok1');
										//   console.log($(this).parent(".pip").html());
										var value = $(this).parent(".pip").html().split('<span');
										//     console.log(value[0]);
										//    console.log("^^^^^^^^^^^");
										//  console.log(value[1]);
										var removeItem = value[0];
										$(this).parent(".pip").remove();
										//         console.log('+++++++++++++++');
										//  console.log($("#attch_image").val());
										var result = $("#attch_image").val().split(',');
										//   console.log(result);
										var result1 = result.filter(function(elem) {
											return elem != removeItem;
										});

										$("#attch_image").val(result1);
									});
								}
							} else {
								$("#upload").attr('disabled', 'disabled');
								// alert('FileSize Is Too Large');

								$(".File-alert").show();
							}
						}
					});
				} else {
					alert("Your browser doesn't support to File API")
				}
			});

			$('.decimal').keyup(function() {
				var val = $(this).val();
				if (isNaN(val)) {
					val = val.replace(/[^0-9\.]/g, '');
					if (val.split('.').length > 2)
						val = val.replace(/\.+$/, "");
				}
				$(this).val(val);
			});


			$("#tax_details").change(function() {
				var id = $(this).val();

				if (id != "" && id != null) {
					$.ajax({
						url: '<?php echo base_url(); ?>proposal_pdf/tax_rate/',
						type: 'POST',
						data: {
							'id': id
						},
						beforeSend: function() {
							$(".LoadingImage").show();
						},
						success: function(data) {
							// alert(data);
							$(".LoadingImage").hide();
							var json = JSON.parse(data);
							tax = json.tax;
							$(".tax_rate").html('');
							$(".tax_rate").append(tax);
							$("#tax_amount").val(tax);
							$("#tax_old").val(tax);
							$("#tax_value").val(id);
							sub_total();
						}
					});
				}
			});

			// $(".searching").keydown(function(){
			//   var drop_value=$("div.company_search .dropdown-option").length;
			//  // alert(drop_value);
			//   if(drop_value=='0'){
			//     $("#popup_company_name").val('');
			//     $("#popup_company_name").val($(".searching").val());
			//     $(".linking").trigger('click');
			//     //$("#send-to").modal('show');
			//   }
			//   //alert(drop_value);
			// });

			$('.required').keypress(function(e) {
				if (e.which == 13) return false;
			});


			var inputQuantity = [];
			$(function() {
				$(".decimal_qty").each(function(i) {
					inputQuantity[i] = this.defaultValue;
					$(this).data("idx", i); // save this field's index to access later
				});
				$(".decimal_qty").on("keyup", function(e) {
					var $field = $(this),
						val = this.value,
						$thisIndex = parseInt($field.data("idx"), 10); // retrieve the index
					//        window.console && console.log($field.is(":invalid"));
					//  $field.is(":invalid") is for Safari, it must be the last to not error in IE8
					if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid")) {
						this.value = inputQuantity[$thisIndex];
						return;
					}
					if (val.length > Number($field.attr("maxlength"))) {
						val = val.slice(0, 4);
						$field.val(val);
					}
					inputQuantity[$thisIndex] = val;
				});
			});



			$("#currency_submit").click(function() {
				var currency = $("#currency").val();
				if (currency != '') {
					$.ajax({
						url: '<?php echo base_url() ?>proposal/addCurrency',
						type: 'POST',
						data: {
							'currency': currency
						},
						beforeSend: function() {
							$(".LoadingImage").show();
						},
						success: function(data) {
							//  alert(data);
							$(".LoadingImage").hide();
							var json = JSON.parse(data);
							content = json['content'];
							$(".currency_update").html('');
							$(".currency_update").append(content);
							$("#add_currency").modal('hide');
							// sub_total();
						}

					});
				} else {
					$("#currency").css('border', 'solid 1px red');
				}
			});


			$(".save_attach").click(function() {
				$(".LoadingImage").show();


				var accept = [];
				//var content = [];
				var i = 0;
				$("#image_preview1 .original").each(function() {
					var value = $(this).html().split('<span');
					var removeItem = value[0];
					accept.push(removeItem);
					i++;
				});
				$("#attch_image").val(accept);
				setTimeout(function() {
					$(".attach_ment").show();
					$(".LoadingImage").hide();
				}, 2000);

				setTimeout(function() {
					$(".attach_ment").hide();
				}, 3000);

			});



			function click_remove(click) {


				//  console.log($(click).parent(".pip").html());
				var value = $(click).parent(".pip").html().split('<span');
				//    console.log(value[0]);
				//  console.log(value[1]);
				var removeItem = value[0];
				$(click).parent(".pip").remove();

				//   console.log($("#attch_image").val());
				var result = $("#attch_image").val().split(',');
				//    console.log(result);
				var result1 = result.filter(function(elem) {
					return elem != removeItem;
				});

				$("#attch_image").val(result1);
			};


			$(".category-tab").click(function() {

				$("#all").css('display', 'none');

			});

			/*
			function proposal_fun(){
			var $grid11 = $('.masonry-container').masonry({
						 itemSelector: '  .services-des',
						 percentPosition: true,
						 columnWidth: '  .grid-sizer' 
					  });
			  }
			  
			  proposal_fun();
			  
			  $(document).on('click', '.nav-link,.common-clienttab button,.add-price-cab a,.pricing_listing select', function(){
				proposal_fun();
			  }):
			*/
		</script>

		<!-- <script type="text/javascript">
			$(document).ready(function(){
						if ($(window).width() < 768) {
						



						$(document).on("click",".common-clienttab .divleft .prev-step",function(){
								$('.pricing-radio.add-price-cab').hide();
							});

						$(document).on("click",".common-clienttab .divright .next-step",function(){
								$('.pricing-radio.add-price-cab').show();
								$('.pricing-radio.add-price-cab').appendTo(".Footer.common-clienttab.pull-right.Next_Previous");
							});

						}
			})
		</script> -->
		</div>
	</div>
</div>
