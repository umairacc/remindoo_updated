<?php
// The function header by sending raw excel
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=Subscription-list.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<table>
<tr>
<th>S.no</th>
<th>Subscription Name</th>
<th>Subscription Price</th>
<th>Subscription Unit</th>
<th>Subscription Description</th>
<th>Subscription Category</th>
</tr>
<?php $i=1;
foreach($records as $data)
	{ ?>
<tr>
<td><?php echo $i; ?></td>
<td><?php echo $data['subscription_name']; ?></td>
<td><?php echo $data['subscription_price']; ?></td>
<td><?php echo $data['subscription_unit']; ?></td>
<td><?php echo $data['subscription_description']; ?></td>
<td><?php echo $data['subscription_category']; ?></td>
</tr>
<?php  $i++;  } ?>


</table>