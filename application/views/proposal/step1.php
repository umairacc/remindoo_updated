<style type="text/css">
	p.error {
		background-color: #fff;
		color: #f33;
	}

	optgroup {
		font-weight: 600;
		color: #353c4e;
	}
</style>

<?php
$client_id = $_GET['client_id'] ?? false;
$succ = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
?>


<div class="modal-alertsuccess alert alert-danger pricelist-exists" style="display:none;">
	<div class="newupdate_alert">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<div class="pop-realted1">
			<div class="position-alert1">
				Price List Contents Empty
			</div>
		</div>
	</div>
</div>


<?php $this->session->unset_userdata('client_section'); ?>

<div id="start_tab" class="tab-pane fade in <?php if ($this->uri->segment(2) == 'step_proposal' && $this->uri->segment(3) != '') {
												echo "";
											} else {
												echo "active";
											} ?>">
	<div class="main-pane-border1 select-constants select-constants1">
		<div class="pane-border1">
			<div class="management_form1 management_accordion">
				<!-- <form>-->

				<div class="blank-pro">
					<label class=" col-form-label">choose a template below or start with a blank proposal:</label>
					<div class="giving-input">
						<div class="dropdown-sin-1">
							<select name="proposal_type" id="proposal_type" required="required" class="form-control required" style="    border-radius: 50px;">

								<option value="blank" <?php
														if (isset($records['proposal_type'])) {
															if ($records['proposal_type'] == 'blank') { ?> selected="selected" <?php }
																						} ?>>Blank Proposal</option>
								<optgroup label="My Template(s)">
									<?php
									foreach ($my_template as $template) { ?>
										<option value="<?php echo $template['id']; ?>" <?php
																						if (isset($records['proposal_type'])) {
																							if ($records['proposal_type'] == $template['id']) { ?> selected="selected" <?php }
																								} else {
																									if (isset($this->session->userdata['template_id'])) {
																										if ($this->session->userdata['template_id'] == $template['id']) { ?>selected="selected" <?php }
																											}
																										} ?>><?php echo $template['template_title']; ?></option>
									<?php } ?>
								</optgroup>
								<optgroup label="All">
									<?php
									foreach ($all as $template) { ?>
										<option value="<?php echo $template['id']; ?>" <?php
																						if (isset($records['proposal_type'])) {
																							if ($records['proposal_type'] == $template['id']) { ?> selected="selected" <?php }
																								} else {
																									if (isset($this->session->userdata['template_id'])) {
																										if ($this->session->userdata['template_id'] == $template['id']) { ?>selected="selected" <?php }
																											}
																										} ?>><?php echo $template['template_title']; ?></option>
									<?php } ?>
								</optgroup>
							</select>
						</div>
					</div>
				</div>

				<div class="blank-pro">
					<label class=" col-form-label">let's begin by giving your proposal a name</label>
					<div class="giving-input">
						<input type="text" name="proposal_name" id="proposal_name" required="required" class="proposal_name required" value="<?php echo $records['proposal_name']; ?>">
					</div>
				</div>
				<!-- primary info -->
			</div>
		</div>
		<div class="lf-rg">
			<div class="left-side-proposal">
				<div class="floating_set management_section send-data">
					<div class="remove-spacing-9  flat-animation2">
						<div class="accordion-panel">
							<div class="box-division03">
								<div class="accordion-heading" role="tab" id="headingOne">
									<h3 class="card-title accordion-title">
										<a class="accordion-msg">Send From:</a>
									</h3>
								</div>
								<div class="accotax-litd send-to">
									<h4 class="send_to_name"><?php
										if ((isset($records['sender_company_name']) && $records['sender_company_name'] != '')) {  ?>
											<?php echo $records['sender_company_name']; ?>
										<?php } else { ?>
											<?php echo $admin_settings['company_name']; ?>
										<?php } ?>
									</h4>
									<div class="gmail-tax">
										<div class="tax-consul-new">
											<a href="javascript:;" class="" data-toggle="modal" data-target="#Company_details"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i>Change Sender Details</a>
										</div>
										<div class="cmn-tax-div">
											<div class="tax-consul">
												<div class="img-tax">
													<?php
													$crm_profile_pic = $this->Common_mdl->get_price('user', 'id', $_SESSION['id'], 'crm_profile_pic');

													if (!empty($crm_profile_pic)) {
														$image_url = base_url() . 'uploads/' . $crm_profile_pic;
													} else {
														$image_url = base_url() . 'uploads/825898.png';
													}

													?>
													<img src="<?php echo $image_url; ?>" alt="image">
												</div>
												<div class="tax-content sender_details_section"> <?php
														if ((isset($records['sender_company_name']) && $records['sender_company_name'] != '') || (isset($records['sender_company_mailid']) && $records['sender_company_mailid'] != '')) { ?>
														<input type="hidden" name="sender_company" value="<?php echo $records['sender_company_name']; ?>">
														<span><?php echo $records['sender_company_name']; ?></span>
														<input type="hidden" name="sender_mail_id" value="<?php echo $records['sender_company_mailid']; ?>">
														<strong><?php echo $records['sender_company_mailid']; ?></strong>

													<?php  } else {   ?>
														<span><?php echo $admin_settings['company_name']; ?></span>
														<input type="hidden" name="sender_company" value="<?php echo $admin_settings['company_name']; ?>">
														<input type="hidden" name="sender_mail_id" value="<?php echo $admin_settings['company_email']; ?>">
														<strong><?php echo $admin_settings['company_email']; ?></strong>
													<?php } ?>
												</div>
												<!--  <a href="#"><b>ph:</b> 798465130</a> -->

											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- accordion-panel -->
						<div class="accordion-panel">
							<div class="box-division03">
								<div class="accordion-heading" role="tab" id="headingOne">
									<h3 class="card-title accordion-title">
										<a class="accordion-msg">Send To: <?php //echo $records['company_id']; 
																			?></a>
									</h3>
								</div>
								<div class="accotax-litd send-to company_add" id="test">

									<?php //echo $records['company_id']; 
									?>
									<div class="dropdown-sin-4 company_search">

										<select name="company_name" id="company_name" class="required">
											<option value="">Select Company</option>
											<?php
											if (count($clients) > 0) {
												foreach ($clients as $key => $val) {  
													$is_selected = '';
													if ($client_id && $client_id == $val['user_id']) {
														$is_selected = "selected";
													}
													if (isset($records['company_id'])) {
														if ($records['company_id'] == $val['user_id']) { 
															$is_selected = "selected";
														}
													}
													?>
													<option value="<?php echo $val['user_id']; ?>" <?php echo $is_selected ?>> <?php echo $val["crm_company_name"]; ?> </option>
												<?php }
											}  ?>
											<?php

											if (count($leads) > 0) {
												foreach ($leads as $key => $value) { ?>
													<option value="<?php echo 'Leads_' . $value['id']; ?>" <?php if ($value['id'] == $get_lead_id) {
																												echo "selected";
																											} else if ($records['lead_id'] == $value['id']) {
																												echo "selected";
																											} ?>><?php echo $value['name']; ?></option>
											<?php }
											} ?>

										</select>
									</div>
									<!--   <a href="#"  data-toggle="modal" data-target="#email_add" class="linking"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> Add Email</a> -->



									<div class="gmail-tax">

										<div class="tax-consul-new">
											<a href="#" data-toggle="modal" data-target="#send-to" class="linking"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i> Add Company</a>
										</div>


										<?php
										if (isset($records['company_id']) && $records['company_id'] != 1) {
											// echo $records['company_id'];
											$query = $this->Proposal_model->company_details($records['company_id']);

											// echo '<pre>';
											// print_r($query);
											// echo '</pre>'; 
										?>
											<div class="cmn-tax-div">
												<div class="tax-consul">


													<div class="img-tax profile_pics">
														<img src="<?php echo base_url(); ?>uploads/825898.jpg" alt="image">
													</div>
													<div class="tax-content con12 client_details">
														<span><?php echo $query['crm_company_name']; ?></span>
														<input type="hidden" name="receiver_company_name" id="receiver_company_name" value="<?php echo $query['crm_company_name']; ?>">
														<strong><?php if (!empty($records['receiver_mail_id'])) {
																	echo $records['receiver_mail_id'];
																} else {
																	echo $query['crm_email'];
																} ?></strong>
														<input type="hidden" name="receiver_mail_id" id="receiver_mail_id" value="<?php if (!empty($records['receiver_mail_id'])) {
																																		echo $records['receiver_mail_id'];
																																	} else {
																																		echo $query['crm_email'];
																																	} ?>">
													</div>

												</div>
											</div>

										<?php  } else if ($records['company_id'] == 1) { ?>
											<div class="cmn-tax-div">
												<div class="tax-consul">
													<div class="img-tax profile_pics">
														<img src="<?php echo base_url(); ?>uploads/825898.jpg" alt="image">
													</div>
													<div class="tax-content con12 client_details">

														<span><?php echo $records['receiver_company_name']; ?></span>
														<strong><?php echo $records['receiver_mail_id']; ?></strong>

														<input type="hidden" name="receiver_company_name" id="receiver_company_name" value="<?php echo $records['receiver_company_name']; ?>">
														<input type="hidden" name="receiver_company_name_old" id="receiver_company_name_old" value="<?php echo $records['receiver_company_name']; ?>">
														<input type="hidden" name="receiver_mail_id" id="receiver_mail_id" value="<?php echo $records['receiver_mail_id']; ?>">
														<input type="hidden" name="receiver_mail_id_old" id="receiver_mail_id_old" value="<?php echo $records['receiver_mail_id']; ?>">
														<!-- <a href="#"><b>ph:</b> 798465130</a> -->
													</div>
												</div>
											</div>


										<?php } else {
										?>
											<div class="cmn-tax-div">
												<div class="tax-consul">
													<div class="img-tax profile_pics"> </div>
													<div class="tax-content con12 client_details"></div>
												</div>
											</div>
										<?php } ?>
									</div>
								</div>

							</div>
						</div>
						<!-- accordion-panel -->
					</div>
				</div>

			</div>
		</div>
		<!-- send-data -->
	</div>
	<div class="right-side-proposal"></div>

</div>

<div id="send-to" class="modal fade new-sent-company ui-front" role="dialog">

	<div class="modal-dialog modal-add-proposal-company">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Adding Company</h4>
			</div>
			<form id="Company_form" method="post">
				<div class="modal-body">

					<!--      <a href="<?php //echo base_url(); 
										?>/proposal_pdf/proposalclient_companyadd" class="btn btn-primary">Add from company house</a>

          <a href="<?php //echo base_url(); 
					?>/proposal_pdf/proposalclient_manualadd" class="btn btn-primary">Manual Add</a> -->

					<!--   <a href="javascript:;" class="btn btn-primary" data-toggle="modal" data-target="#email_add">Email Add</a> -->


					<h3>Add a new company</h3>
					<div class="form-group">
						<label>Company name</label>
						<input type="text" name="popup_company_name" id="popup_company_name" class="autocomplete_client" required>
						<p class="errors" style="display: none; color: red">Please Enter Company name</p>
					</div>
					<hr>

					<h3>Add a contact</h3>
					<div class="form-group">
						<label>First Name:</label>
						<input type="text" name="text" name="first_name" id="first_name" required>
						<p class="errors" style="display: none; color: red">Please Enter First name</p>
					</div>
					<div class="form-group">
						<label>Last Name</label>
						<input type="text" name="text" name="last_name" id="last_name">
					</div>
					<div class="form-group">
						<label>Email</label>
						<input type="email" name="email" id="email" required>
						<p class="errors" style="display: none; color: red">Please Enter Email</p>

					</div>

					<div class="hide-fields1 hide_section"><a href="javascript:;" class="hide_field">Hide fields</a></div>
					<div class="hide_fields_form" style="display: block;">
						<div class="form-group">
							<label>Phone</label>
							<input type="text" name="phone" id="phone" class="decimal">
						</div>
						<!--  <div class="form-group">
               <label>Fax</label>
               <input type="text" name="text" name="fax" id="fax" class="decimal">
            </div> -->
						<div class="form-group">
							<label>Country</label>
							<select name="country" id="country">
								<option value=''>Select</option>
								<?php
								foreach ($countries as $count) { ?>
									<option><?php echo $count['name']; ?></option>
								<?php }  ?>

							</select>
						</div>
						<div class="form-group">
							<label>State/Province</label>
							<input type="text" name="text" name="state" id="state">
						</div>
						<div class="form-group">
							<label>Zip/Postal Code</label>
							<input type="text" name="text" name="postal_code" id="postal_code">
						</div>
						<div class="form-group">
							<label>Address</label>
							<input type="text" name="text" name="address" id="address">
						</div>
						<div class="form-group">
							<label>Website</label>
							<input type="text" name="text" name="website" id="website">
						</div>
					</div>


					<div class="hide-fields1 show_more"><a href="javascript:;" class="show_field">Show more fields</a></div>
				</div>

				<div class="modal-footer">
					<button type="button" name="save" id="company_added" value="Save">Save</button>
					<a href="#" data-dismiss="modal">Close</a>
				</div>
			</form>
		</div>
	</div>
</div>


<div class="modal fade" id="email_add" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add Mail Id</h4>
			</div>
			<div class="modal-body">
				<center>
					<p id="email_error_section"></p>
				</center>
				<label>Sender Company Name</label>

				<input type="text" name="receiver_company_name_old" value="" id="receiver_company_name_old">
				<label>Sender Company Name</label>
				<input type="text" name="receiver_mail_id_old" value="" id="receiver_mail_id_old">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="add_mail">Add Mail id</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>


<div class="modal fade" id="Company_details" role="dialog">
	<div class="modal-dialog modal-edit-proposal">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit Company Details</h4>
			</div>
			<div class="modal-body input-renew">
				<div class="inputbx">
					<label>Company Name</label>
					<input type="text" name="sender_company_name" id="sender_company_name" value="">
				</div>
				<div class="inputbx">
					<label>Company Mail Id</label>
					<input type="text" name="sender_company_mailid" id="sender_company_mailid" value="">
				</div>
				<!--     <label>Company Phone Number</label>
             <input type="text" name="sender_company_mailid" id="sender_company_mailid" value=""> -->
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="sender_details">Save</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>