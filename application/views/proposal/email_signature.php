<?php 
error_reporting('0');
$this->load->view('includes/header');?>
<style type="text/css">		
			#btnSaveSign {
				color: #fff;
				background: #f99a0b;
				padding: 5px;
				border: none;
				border-radius: 5px;
				font-size: 20px;
				margin-top: 10px;
			}
			#signArea{
				width:304px;
				margin: 50px auto;
			}
			.sign-container {
				width: 60%;
				margin: auto;
			}
			.sign-preview {
				width: 150px;
				height: 50px;
				border: solid 1px #CFCFCF;
				margin: 10px 5px;
			}
			.tag-ingo {
				font-family: cursive;
				font-size: 12px;
				text-align: left;
				font-style: oblique;
			}
		</style>
		<link href="<?php echo base_url(); ?>assets/signature/css/jquery.signaturepad.css" rel="stylesheet">
		 
		
		


		
		
		<div id="signArea" >
			<h2 class="tag-ingo">Put signature below,</h2>
			<div class="sig sigWrapper" style="height:auto;">
				<div class="typed"></div>
				<canvas class="sign-pad" id="sign-pad" width="300" height="100"></canvas>
			</div>
		</div>
		
		<button id="btnSaveSign">Save Signature</button>
		 <button  id="clearSig" type="button">Clear Signature</button>

		<div class="sign-container">
		<?php
		$image_list = glob("./doc_signs/*.png");
		foreach($image_list as $image){
			//echo $image;
		?>
		<img src="<?php echo $image; ?>" class="sign-preview" />
		<?php
		
		}
		?>
		</div>
		
	
		<?php $this->load->view('includes/footer');?>
		<script type="text/javascript" src="http://remindoo.org/CRMTool/bower_components/jquery/js/jquery.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/signature/js/numeric-1.2.6.min.js"></script> 
		<script src="<?php echo base_url(); ?>assets/signature/js/bezier.js"></script>
		<script src="<?php echo base_url(); ?>assets/signature/js/jquery.signaturepad.js"></script> 		
		<script type='text/javascript' src="https://github.com/niklasvh/html2canvas/releases/download/0.4.1/html2canvas.js"></script>
		<script src="<?php echo base_url(); ?>assets/signature/js/json2.min.js"></script>


		<script>
			$(document).ready(function() {
				$('#signArea').signaturePad({drawOnly:true, drawBezierCurves:true, lineTop:90});
			});


				$("#clearSig").click(function clearSig() {

				$('#signArea').signaturePad().clearCanvas ();
				});
			
		   $("#btnSaveSign").click(function(e){
          html2canvas([document.getElementById('sign-pad')], {
          onrendered: function (canvas) {
            var canvas_img_data = canvas.toDataURL('image/png');
            var img_data = canvas_img_data.replace(/^data:image\/(png|jpg);base64,/, "");
            //ajax call to save image inside folder
            $.ajax({
              url: '<?php echo base_url(); ?>/proposals/save_sign',
              data: { img_data:img_data },
              type: 'post',
              dataType: 'json',
              success: function (response) {            
                $("#imgData").html('Thank you! Your signature was saved');
                setTimeout(function(){ $("#EmailSignature").modal('hide'); }, 3000);
                 //window.location.reload();
              }
            });
          }
        });
      });
		  </script> 

		  <?php $this->load->view('proposal/scripts.php');?>	
	</body>
</html>
