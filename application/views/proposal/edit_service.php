 <?php 

 $services_list = $this->Common_mdl->getServicelist();
 $service_name=array_column($services_list, 'service_name');
 // print_r($service_name);

 $other=1;
 $style_for_other='display:block';
 $otherservice_field_value=$service['service_name'];

 if(in_array($service['service_name'],  $service_name))
 {
  $other=0;
   $style_for_other='display:none';
   $otherservice_field_value="";
 }

 // echo $other;
  ?>

<input type="hidden" name="id" id="edit_service_id" value="<?php if(isset($service['id'])){ echo $service['id']; } ?>">

 <div class="unit-annual floating_set">
   <div class="common-annual lead-annual">
      <div class="spacing-annual accotax-litd send-to choosclsnewtop">
       <div class="choosclsnew" style="display: block;width: 100%;">
         <label>Choose a service</label>
         <div class="">                  
            <select name="service_name" id="service_name">
              <option value="">Select</option>  
              <?php foreach ($services_list as $key => $value){ 

               ?>                                
              <option value="<?php echo $value['service_name']; ?>" <?php if($service['service_name'] == $value['service_name']){ echo "selected"; } ?>><?php echo $value['service_name']; ?></option>
              <?php }  ?>
              <option value="other" <?php if($other==1){ echo "selected"; } ?> >Other</option>
           </select>
          </div>
      </div>
         <div class="choosclsnew other_service_edit" style="<?php echo $style_for_other ?>">
      <label>Service Name</label>
      <div class=""> 
        <input type="text" name="edit_other_service_name" value="<?php echo $otherservice_field_value; ?>">
      </div>
    </div>

      </div>
      <div class="annual-field3 add-input-service"> 
         <label>Price </label>		
         <input type="text" name="service_price" id="service_price" class="decimal" required="required" value="<?php if(isset($service['service_price'])){ echo $service['service_price']; } ?>">
      </div>
      <div class="annual-field3 add-input-service">
      <label>Qty</label>
      <input type="text" name="service_qty" id="service_qty"  placeholder="1.00" required="required" class="decimal" value="<?php if(isset($service['service_qty'])){ echo $service['service_qty']; } ?>"></div>
      <div class="annual-field3 add-input-service">
         <label>Unit</label>
         <select name="service_unit" id="service_unit">
            <option value="per_service" <?php if(isset($service['service_unit'])){ if($service['service_unit']=='per_service'){ ?> selected="selected" <?php }} ?>>Per Service</option>                               
                  <option value="per_day" <?php if(isset($service['service_unit'])){ if($service['service_unit']=='per_day'){ ?> selected="selected" <?php }} ?>>Per Day</option>
                  <option value="per_month" <?php if(isset($service['service_unit'])){ if($service['service_unit']=='per_month'){ ?> selected="selected" <?php }} ?>>Per Month</option>
                  <option value="per_2month" <?php if(isset($service['service_unit'])){ if($service['service_unit']=='per_2month'){ ?> selected="selected" <?php }} ?>>Per 2Months</option>
                  <option value="per_6month" <?php if(isset($service['service_unit'])){ if($service['service_unit']=='per_6month'){ ?> selected="selected" <?php }} ?>>Per 6Month</option>
                  <option value="per_year" <?php if(isset($service['service_unit'])){ if($service['service_unit']=='per_year'){ ?> selected="selected" <?php }} ?>>Per Year</option>
         </select>
      </div>
      <div class=" spacing-annual  add-input-service">
         <label>Description</label>
         <textarea rows="3" name="service_description" id="edit_service_description" required="required">
            <?php if(isset($service['service_description'])){ echo $service['service_description']; } ?>
         </textarea>
      </div>
   
      <div class="spacing-annual">
         <?php //echo $service['service_category_id']; ?>
      </div>
      <div class="spacing-annual accotax-litd send-to choosclsnewtop">
         <div class="choosclsnew" style="display: block;width: 100%;">
            <label>Choose a category</label>
            <div class="category_add">
            <select name="service_category" required="required" id="service_category">
            <option value="">Select </option>
            <?php
            if(count($category)>0){
            foreach($category as $cat){ 
               if($cat['category_type']=='service'){ ?>
               <option value="<?php echo $cat['id']; ?>" <?php if(isset($service['service_category_id'])){ 
                  if($service['service_category_id']==$cat['id']){ ?> selected="selected" <?php }} ?>   ><?php echo $cat['category_name']; ?></option>
          <?php   }}} ?>
            </select>
            </div>
         </div>
         <a href="javascript:;" data-toggle="modal" data-target="#add-new-category_service"><i class="fa fa-plus fa-6" aria-hidden="true"></i> Add a new category</a>
      </div>
   </div>
</div>