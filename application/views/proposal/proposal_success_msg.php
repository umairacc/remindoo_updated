<?php 
error_reporting( -1 );
 $cur_symbols = $this->Common_mdl->getallrecords('crm_currency');

 foreach ($cur_symbols as $key => $value) 
 {
    $currency_symbols[$value['country']] = $value['currency'];
 }

?>
<?php

$send_array=array();
$send_count=0;
$send_amount=0;
$view_array=array();
$view_count=0;
$view_amount=0;
$draft_array=array();
$draft_count=0;
$draft_amount=0;
$indiscussion_array=array();
$indiscussion_count=0;
$indiscussion_amount=0;
$decline_array=array();
$decline_count=0;
$decline_amount=0;
$accepted_array=array();
$accepted_count=0;
$accepted_amount=0;
$archive_count =0;
   foreach($proposal as $key => $value ){
    if($value['status']=='sent')
    {
      $send_count+=1;
      if($value['grand_total']==''){ $values=0; }else{ $values=$value['grand_total']; }
      $send_amount+= (int)$values;
      array_push($send_array, $value['price']);
  
    }
    if($value['status']=='opened')
    {
      $view_count+=1;
      if($value['grand_total']==''){ $values=0; }else{ $values=$value['grand_total']; }
      $view_amount+= (int)$values;
      array_push($view_array, $value['price']);
  
    }
    if($value['status']=='draft')
    {
      $draft_count+=1;
      if($value['grand_total']==''){ $values=0; }else{ $values=$value['grand_total']; }
      $draft_amount+= (int)$values;
      array_push($draft_array, $value['price']);
  
    }
    if($value['status']=='in discussion')
    {
      $indiscussion_count+=1;

      if($value['grand_total']==''){ $values=0; }else{ $values=$value['grand_total']; }
      $indiscussion_amount+= (int)$values;
      array_push($indiscussion_array, $value['price']);
  
    }
    if($value['status']=='declined')
    {
      $decline_count+=1;
      if($value['grand_total']==''){ $values=0; }else{ $values=$value['grand_total']; }

      $decline_amount+= (int)$values;
      array_push($decline_array, $value['price']);

      // $view_count+=1;
      // if($value['grand_total']==''){ $values=0; }else{ $values=$value['grand_total']; }
      // $view_amount+= (int)$values;
      // array_push($view_array, $value['price']);                    
    }
      if($value['status']=='accepted')
    {
      $accepted_count+=1;
      if($value['grand_total']==''){ $values=0; }else{ $values=$value['grand_total']; }

      $accepted_amount+= (int)$values;
      array_push($accepted_array, $value['price']);

      // $view_count+=1;
      // if($value['grand_total']==''){ $values=0; }else{ $values=$value['grand_total']; }
      // $view_amount+= (int)$values;
      // array_push($view_array, $value['price']);
  
    }
    if( $value['status'] == 'archive')
    {
      $archive_count+=1;
    }                    
   } 

                     // $send_amount= array_sum(array_map('intval', explode(',', $send_array)));
// $view_amount= array_sum(array_map('intval', explode(',', $view_array)));
// $draft_amount= array_sum(array_map('intval', explode(',', $draft_array)));
// $indiscussion_amount= array_sum(array_map('intval', explode(',', $indiscussion_array)));
// $decline_amount= array_sum(array_map('intval', explode(',', $decline_array)));
// $accepted_amount= array_sum(array_map('intval', explode(',', $accepted_array)));

 ?>
            
<div class="inner-tab" id="tabs12">
<div class="proposal-commontab col-md-12">
  <div class="delbuttoncls assign_delete" style="margin-top: -30px !important; margin-left: 240px;">
  <?php if($_SESSION['permission']['Proposal_Dashboad']['delete']==1){ ?>
  <button type="button" id="delete_proposal_records" class="delete_proposal_records del-tsk12 f-right"  style="display: none;"><i class="fa fa-trash fa-6" aria-hidden="true"></i>Delete</button>
  <?php } ?>
  <?php if($_SESSION['permission']['Proposal_Dashboad']['edit']==1){ ?>
   <button type="button" id="archive_proposal_records" class="del-tsk12 f-right" style="display: none;" ><i class="fa fa-archive fa-6" aria-hidden="true"></i>Archive</button>
   <button type="button" id="unarchive_proposal_records" class="del-tsk12 f-right" style="display: none;"><i class="fa fa-archive fa-6" aria-hidden="true"></i>Un Archive</button>
   <?php } ?>
  </div> 
 <div class="tab-content common_currency">
  <ul class="nav nav-tabs md-tabs proposalulcls proposal_tab new_aug5" id="proposal_dashboard" style="margin-top: 20px;">

            <li class="nav-item">
             <a class="nav-link sent_result sent cor1 status_tab active"  href="javascript:;" data-id="all" data-searchCol="status_TH" data-toggle="tab">
               <input type="hidden" name="proposal_status" id="status_id" value="0">
               <h4 class="value-text value-textnew">
                 All
                   <span class="inner-value inner-valuenew"><?php echo count( $proposal );?></span>
               </h4>
             </a>
           </li>
           <li class="nav-item">
             <a class="nav-link sent_result sent cor1 status_tab" href="javascript:;" data-id="Sent" data-searchCol="status_TH" data-toggle="tab">
               <input type="hidden" name="proposal_status" id="status_id" value="1">
               <h4 class="value-text value-textnew">
                 sent
                   <span class="inner-value inner-valuenew"><?php echo $send_count;?></span>
               </h4>
              
             </a>
           </li>
           <li class="nav-item">
             <a class="nav-link view cor2 status_tab"  href="javascript:;" data-toggle="tab"  data-id="Viewed" data-searchCol="status_TH">
               <input type="hidden" name="proposal_status" id="status_id" value="2">
               <h4 class="value-text v1 value-textnew">
                 viewed
                   <span class="inner-value inner-valuenew"><?php echo $view_count;?></span>
               </h4>
          
             </a>
           </li>
           <li class="nav-item">
             <a class="nav-link indiscussion cor3 status_tab" href="javascript:;" data-toggle="tab"  data-id="Discussion" data-searchCol="status_TH">
               <input type="hidden" name="proposal_status" id="status_id" value="3">
               <h4 class="value-text v2 value-textnew">
                 discussion
                   <span class="inner-value inner-valuenew"><?php echo $indiscussion_count;?></span>
               </h4>
               
             </a>
           </li>
           <li class="nav-item">
             <a class="nav-link accept cor4 status_tab" href="javascript:;" data-toggle="tab"  data-id="Accepted" data-searchCol="status_TH">
               <input type="hidden" name="proposal_status" id="status_id" value="4">
               <h4 class="value-text v3 value-textnew">
                 accepted
                   <span class="inner-value inner-valuenew"><?php echo $accepted_count; ?></span>
               </h4>
             
             </a>
           </li>
           <li class="nav-item">

             <a class="nav-link decline cor5 status_tab" href="javascript:;" data-toggle="tab" data-id="Declined" data-searchCol="status_TH">
               <input type="hidden" name="proposal_status" id="status_id" value="5">
               <h4 class="value-text v4 value-textnew">
                 declined
                   <span class="inner-value inner-valuenew status_filter" data-id="<?php if(isset($admin_settings['crm_currency'])){ echo $currency_symbols[$admin_settings['crm_currency']]; } ?>" ><?php echo $decline_count;?></span>
               </h4>                                         
             </a>
           </li>
            <li class="nav-item">
             <a class="nav-link draft cor6 status_tab" href="javascript:;" data-toggle="tab"  data-id="Draft" data-searchCol="status_TH">
               <input type="hidden" name="proposal_status" id="status_id" value="6">
               <h4 class="value-text v5 value-textnew">
                 draft
                   <span class="inner-value inner-valuenew"><?php echo $draft_count;?></span>
               </h4>
             </a>
           </li>                               
           <li class="nav-item">
             <a class="nav-link draft cor6 status_tab" href="javascript:;" data-toggle="tab"  data-id="Archive" data-searchCol="status_TH">
               <input type="hidden" name="proposal_status" id="status_id" value="6">
               <h4 class="value-text v5 value-textnew">
                 Archive
                   <span class="inner-value inner-valuenew"><?php echo $archive_count;?></span>
               </h4>                                         
             </a>
           </li>
         </ul>
       </div>   
</div>

  <div class="tab-content <?php if($_SESSION['permission']['Proposal_Dashboad']['view']!=1){ ?> permission_deined <?php 
  } ?>">
  <div id="sent" class="proposal_send tab-pane fade active">

    <!-- For filter section -->
    <div class="filter-data for-reportdash">
      <div class="filter-head">
        <h4>FILTERED DATA</h4>
        <button class="btn btn-danger f-right" id="clear_container">clear</button>
        <div id="container2" class="panel-body box-container">
        </div>
      </div>                                   
    </div>
    <!-- For filter section -->

    <div class="proposal-info">
      <div class="sample_section sample_sectioncls">
      <!--   <button type="button" class="print_button btn btn-primary" id="send_table_print_report">Print</button> -->
        <div class="export_option">                                 
          <div class="btn-group e-dash sve-action1 frpropos">
           <!--  <button type="button" class="btn btn-default"><a href="javascript:;" id="sent" class="change" data-toggle="modal" data-target="#Send_proposal">Select</a></button> -->
            <button type="button" class="btn btn-default dropdown-toggle emp-dash" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
           select
            </button>
            <ul class="dropdown-menu" >
              <li id="buttons"></li>
            </ul>
          </div>                                  
        </div>
      </div>
      <table id="send_tables" class="display nowrap" style="width:100%">
        <thead>
          <tr class="table-header">
            <th class="propsal_chk_TH Exc-colvis">                                  
              <label class="custom_checkbox1">
                <input type="checkbox" id="select_all_proposal">
                <i></i>
              </label>                                        
            </th>
            <th class="sent_display1 send_propsal_TH hasFilter">Proposals 
              <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" /><div class="sortMask"></div>
            </th> 
                                    
            <th class="total_cost_TH hasFilter">Total Cost
              <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
              <div class="sortMask"></div>
            </th>
    
            <th class="created_by_TH hasFilter">Created By
              <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
              <div class="sortMask"></div>
            </th>
         
            <th class="send_on_TH hasFilter">Sent On
              <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
              <div class="sortMask"></div>
            </th>
      
            <th class="status_TH hasFilter">Status  
              <img class="themicond" src="<?php echo base_url();?>assets/images/filter.png" alt="themeicon" />  
              <div class="sortMask"></div>
            </th>
            <th class="action_TH Exc-colvis"> Actions </th>
          </tr>
        </thead>

        <tbody>
          <?php 
            foreach($proposal as $key => $value ){ ?>
          <tr id="<?php echo $value['id'];?>">
            <td>
              <label class="custom_checkbox1">
                <input type="checkbox" class="proposal_checkbox sent_checkbox" data-proposal-id="<?php echo $value['id'];?>" value="<?php echo $value['id'];?>">
                <i></i>  
                </label>
            </td>
            <td  data-search=" <?php echo $value['proposal_name']; if($value['company_id']==1){ echo $value['receiver_mail_id']; } else{ echo $value['company_name'];
            }?>">
              <div class="first-info">
                <span class="info-num"><?php echo $value['proposal_no'] ?></span>

                <?php $randomstring=generateRandomString('100');
                  if($_SESSION['permission']['Proposal_Dashboad']['view']==1)
                  { 
                    $href = base_url()."proposal_page/step_proposal/".$randomstring."---".$value['id'];
                  }
                  else
                  {
                    $href = "javascript:;";
                  }
                ?>
                <span class="proposal name">
                  <p>
                    <a href="<?php echo $href;?>">
                      <?php echo $value['proposal_name'];?>
                    </a>
                  </p>
                </span>
              </div>
              <?php if($value['company_id']==1){ ?>
              <p class="com-name"><?php echo $value['receiver_mail_id'];?> </p>
              <?php }else{ ?>
              <p class="com-name"><a href="<?php echo base_url(); ?>client/client_infomation/<?php echo $value['company_id'];?>" target="_blank"><?php echo $value['company_name'];?> </a></p>
              <?php   } ?>                                  
            </td>
            <td data-search="<?php echo $value['grand_total']; ?>" >
              <span class="amount">                                      
                <i class="fa fa-gbp" aria-hidden="true"></i> 
                <?php 
                //   $ex_price=explode(',',$value['price']);
                //   $integerIDs = array_sum(array_map('intval', explode(',', $value['price'])));
                echo $value['grand_total'];
                ?>
              </span>
            </td>
            <td data-search="<?php if($value['sender_company_name']==''){ 
               echo 'accountants & tax consultants';
              }else{ 
                echo $value['sender_company_name']; 
               } ?>">
              <div class="acc-tax">
                <?php if($value['sender_company_name']==''){ ?>
                <p>accountants & tax consultants</p>
                <?php }else{ ?>
                <p><?php echo $value['sender_company_name']; ?></p>
                <?php } ?>
              </div>
            </td>
      
            <?php //echo $value['created_at'];
              $timestamp = strtotime($value['created_at']);
              $newDate = date('d-F-Y H:i', $timestamp); 
              
              ?>
          <td class="send_td" data-search="<?php echo $newDate;?>" data-sort="<?php echo $newDate;?>">
            <span class="sent-month">
            <?php //echo $value['created_at'];
              echo $newDate; //outputs 02-March-2011
              ?>
            </span>                                     
          </td>

                <?php 
                  $status=$value['status'];
                  if($status=='sent')
                  {
                    $res='Sent';
                  }
                  if($status=='opened')
                  {
                    $res='viewed';
                  }
                  else
                  {
                    $res=$status;
                  }
                  $res = ucfirst($res);
                  ?>
            <td  data-search="<?php echo $res;?> ">
              <div class="sentclsbutton">
                <i class="fa fa-check" aria-hidden="true"></i>
                <span class="accept"><?php echo $res;?></span> 
              </div>
            </td>
    
             <td>
              <div class="sentclsbuttonbot">
               <?php if($_SESSION['permission']['Proposal_Dashboad']['edit']==1){ ?>

                <a href="<?php echo base_url(); ?>proposal_page/copy_proposal/<?php echo $randomstring; ?>---<?php echo $value['id']; ?>" class="edit-option"><i class="fa fa-pencil-square-o fa-6" aria-hidden="true"></i></a>

                <?php if( $value['status'] != 'archive') {?>
                  <a href="javascript:void(0);" id="<?php echo $value['id']; ?>" class="edit-option archived"><i class="fa fa-archive" aria-hidden="true"></i></a>
                <?php } ?>

                <?php } ?>
                 <?php if($_SESSION['permission']['Proposal_Dashboad']['delete']==1){ ?>
                <a href="javascript:;" id="<?php echo $value['id']; ?>" class="edit-option deleteproposals"><i class="fa fa-trash" aria-hidden="true"></i></a>
                <?php } ?>
             </div>
            </td>
          </tr>
          <?php
          }
          ?>
        </tbody>
      </table>
      <input type="hidden" class="rows_selected" id="select_proposal_count" >                                
    </div>
  </div>
 </div>
  <!-- end of draft section -->
</div>